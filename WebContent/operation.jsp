<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.HashMap"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%@page import="com.common.util.TDSConstants"%><html>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/cssverticalmenu.css" />
<script type="text/javascript">
 
</script>

<script type="text/javascript" src="js/cssverticalmenu.js"></script>
<style type="text/css">
.glossymenu, .glossymenu li ul{
list-style-type: none;
margin: 0;
padding: 0; 
width: 250px; /*WIDTH OF MAIN MENU ITEMS*/
border: 1px solid black;
}

.glossymenu li{
position: relative;
}

.glossymenu li a{
background: white url(images/glossyback.gif) repeat-x bottom left;
font: bold 12px Verdana, Helvetica, sans-serif;
color: white;
display: block;
width: auto;
padding: 5px 0;
padding-left: 10px;
text-decoration: none;
}

.glossymenu li ul{ /*SUB MENU STYLE*/
position: absolute;
width: 240px; /*WIDTH OF SUB MENU ITEMS*/
left: 0;
top: 0;
display: none;
}

.glossymenu li ul li{
float: left;
}

.glossymenu li ul a{
width: 240px; /*WIDTH OF SUB MENU ITEMS - 10px padding-left for A elements */
}

.glossymenu .arrowdiv{
position: absolute;
right: 2px;
background: transparent url(images/arrow.gif) no-repeat center right;
}

.glossymenu li a:visited, .glossymenu li a:active{
color: white;
}

.glossymenu li a:hover{
background-image: url(images/glossyback2.gif);
font: bold 12px Verdana, Helvetica, sans-serif;
color: white;
}

/* Holly Hack for IE \*/
* html .glossymenu li { float: left; height: 1%; }
* html .glossymenu li a { height: 1%; }
/* End */
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table  border="1"  align="left"   width="40%"   >
	<Tr>
	<Tr>
		<Td>   
			<div id="dis"> 
		<ul class='vertMenu'>
 		<li><a href='control?action=admin&event=driverTempDeActive' title='Deactivation Driver'>Deactivation Driver</a></li>
		 <li><a href='control?action=admin&event=getDeActiveSummary' title='Activate Driver'>Activate Driver</a></li>
		 <li><a href='control?action=registration&event=createLostandFound' title='Create Lost and Found'>Create Lost and Found</a></li>
		 <li><a href='control?action=registration&event=lostandFoundSummary' title='Search Edit Lost and Found' >Search Edit Lost and Found</a></li>
		 <li><a href='control?action=registration&event=driverPenalityMaster' title='Driver Credit/Charge' >Driver Credit/Charge</a></li>
		 <li><a href='control?action=registration&event=createMaintence' title='Create Cab Maintenance'>Create Cab Maintenance</a></li>
		 <li><a href='control?action=registration&event=maintenanceSummary' title='Cab Maintenance Summary'>Cab Maintenance Summary</a></li>
		 <li><a href='control?action=registration&event=ZoneEntry' title='Zone Login'>Zone Login</a></li>
		 <li><a href='control?action=registration&event=custInfo' title='Customer Service'>Customer Service</a></li>
		 <li><a href='control?action=openrequest&event=ServiceSummary' title='Service Summary' class='last'>Service Summary</a></li>
 		</ul>
			</div>
			<div id="oper"></div>
			<div id="fin"></div>    
			<div id="system"></div>   
  		</Td>
	</Tr>
</table>
 
</body>
</html>