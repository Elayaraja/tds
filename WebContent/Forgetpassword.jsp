<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ForgetPassword</title>
</head>
<body>
<form action ="control" method="get">
	<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
	<input type="hidden" name="<%= TDSConstants.eventParam %>" value="forgetpasswordinsert">
	<input type="hidden" name="subevent" value="insert">
<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
        	 
            
            <div class="contentDV">
            <div class="contentDVInWrap">
            <div class="contentDVIn">
            	<div class="leftCol">
                	<div class="innerPage">
                    </div>
                </div>
                
                <div class="rightCol">
				<div class="rightColIn">
                	<div class="loginBX">
                	<div class="grayBX">
                    	<div class="topLCur"><div class="topRCur"><div class="topMCur"><div class="topMCur1"></div></div></div></div>
                        <div class="mid"><div class="mid1"><div class="mid2"><div class="mid3">
                        	<div class="formDV">
                        		<h2></h2>
                        		<div id ="errorpage">
                        		        <%
											if(request.getAttribute("errors") != null ) {
											out.println(request.getAttribute("errors"));
											} 
											System.out.println(request.getAttribute("errors"));	
										%>
										</div>
                        		
                                	<div class="fieldDv padB3">
                                    <fieldset>
                                    <label1 for="User id">User&nbsp;Name</label1>
                                    <div class="div4"><div class="div4In"><div class="inputBXWrap">
                                    <input type="text" tabindex="1" name="username" value="" class="inputBX">
                                    </div></div></div>
									</fieldset>
                                    
                                	                                    
                                    <fieldset>
                                    <div class="div4"><div class="div4In">
                                    		<div class="fltLft padT10">
                                              <div class="btnBlue">
                                                   <div class="rht">
                                                   <input type="submit" value="Submit" tabindex="3" class="lft" >
                                                   </div>
                                              </div>
                                          	</div>
                                            
                                    </div></div>
									</fieldset>
                                    </div>
                                    
                                
                                        
                                
                        	</div>
                        </div></div></div></div>
                        <div class="btmLCur"><div class="btmRCur"><div class="btmMCur"><div class="btmMCur1"><div class="btmMCur2"><div class="btmMCur3"><div class="btmMCur4"><div class="btmMCur5"></div></div></div></div></div></div></div></div>
                    </div>
                    </div>
                </div>    
              </div>
            	
                <div class="clrBth"></div>
            </div>
            </div>
            </div>
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
		</div>
	</div>
</div>
</body>
</html>
