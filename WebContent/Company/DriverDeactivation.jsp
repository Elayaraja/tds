<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript">

<%
		   
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
	//String pageFor = request.getAttribute("pagefor").toString();
	DriverDeactivation deactivation = (DriverDeactivation)request.getAttribute("deactivation")==null?new DriverDeactivation():(DriverDeactivation)request.getAttribute("deactivation");
%> 

 


function cal(){

	date = new Date();
	var month = (date.getMonth()+1)<=9?("0"+(date.getMonth()+1)):(date.getMonth()+1);
	var day = date.getDate()<=9?("0"+date.getDate()):date.getDate();
	var year = date.getFullYear();   
	var fromdate=document.getElementById("from_date").value;
	var todate=document.getElementById("to_date").value;
	var curDate = month+"/"+day+"/"+year;
 
		curDate = curDate.substring(6,10)+curDate.substring(0, 2)+curDate.substring(3, 5);
		fromdate = fromdate.substring(6,10)+fromdate.substring(0, 2)+fromdate.substring(3, 5);
		todate = todate.substring(6,10)+todate.substring(0, 2)+todate.substring(3, 5); 
		if(document.getElementById("inDefinite").checked==true){
			return true;
		}
		if(Number(fromdate)>Number(todate)) {
			alert("Pls Check FromDate And Todate");
			return false;
		}
		 
		else {
		return true; 
		}
} 
function loadDriverDate() { 
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
	var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
	var hrsmin = hors+""+min;  
	//alert(cMonth+"/"+cDate+"/"+cYear); 
	document.getElementById("from_date").value= cMonth+"/"+cDate+"/"+cYear;
	document.getElementById("to_date").value= cMonth+"/"+cDate+"/"+cYear;
	document.getElementById("from_time").value= hrsmin;
	document.getElementById("to_time").value= hrsmin;  
 }
 
</script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
</head>

<body    onload="loadDriverDate()" >  
		<form name="DriverForm" action="control" method="post" onsubmit="return cal()"> 
		<input type="hidden" name="action" value="admin"/>
		<input type="hidden" name="event" value="driverTempDeActive"/>
		<input type="hidden" name="operation" value="2"/>
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />

<div class="rightCol">
<div class="rightColIn">
			<h2><center>Driver&nbsp;Temporary&nbsp;Deactivation</center></h2>
								<%									
									String error = (String) request.getAttribute("error");
			 						if(request.getParameter("error")!=null)
									{
										error = request.getParameter("error");
									}
								%>
								<div id="errorpage" style="color: red">								
								<%= (error !=null && error.length()>0)?""+error :"" %>
								</div>
                        		                         		 
 			<table width="80%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab" align="center">
                    <tr>
                    <td>
                    <table id="bodypage" width="80%" align="center">
                    <tr> 
                    	<td width="10%">Driver&nbsp;Id</td>
                    	<td width="50%" align="left"> 
                                    <input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername','dName')" id="dd_driver_id" name="dd_driver_id" size="10"     autocomplete="off" value="<%=deactivation.getDriver_id()==null?"":deactivation.getDriver_id() %>" class="form-autocomplete"/>
                                    <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 										
								  <input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>"/>								 
                    	</td>
                    </tr>
                    <tr>
                    	<td width="10%">From&nbsp;Date&nbsp;-&nbsp;Time</td>
                    	<td width="50%" align="left">
                                <input type="text" name="from_date" id="from_date" onfocus="showCalendarControl(this);" value="<%=deactivation.getFrom_date()==null?"":deactivation.getFrom_date() %>" readonly="readonly"/> <input type="text" name="from_time"   id="from_time"   size="5" value="<%=deactivation.getFrom_time()==null?"":deactivation.getFrom_time() %>" />Hrs                                 
                     	</td>
                    </tr>
                       <tr>
                    	<td width="10%">To&nbsp;Date&nbsp;-&nbsp;Time </td>
                    	<td width="50%" align="left">
                                  <input type="text" name="to_date"   id="to_date" onfocus="showCalendarControl(this);" value="<%=deactivation.getTo_date()==null?"":deactivation.getTo_date() %>" readonly="readonly"/> <input type="text" name="to_time"   id="to_time" size="5"   value="<%=deactivation.getTo_time()==null?"":deactivation.getTo_time() %>"  />Hrs 
                     			  Indefinite To Date:<input type="checkbox" name="inDefinite" id="inDefinite" value="In Definite"/>
                     	</td>
                    </tr>
                      <tr>
                    	<td width="10%">Description</td>
                    	<td width="50%" align="left">
                    	<div class="div2"><div class="div2In">
                                    <textarea name="desc"  cols="20" rows="8"><%=deactivation.getDesc()==null?"":deactivation.getDesc() %></textarea>
                                    </div></div>
                    	</td>
                    </tr>                                        	
 					<tr>
					<td colspan="2" align="center">
						<div class="wid60 marAuto padT10">
                        	   <input name="Button" type="submit" value="Submit"   />
                        	 </div>                                                    	
					</td>
					</tr>                    
                    </table>                   
                    </td>
                    </tr>
                    </table>
                 </div>
                 </div>            	                          
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>                      
            </form>
            
            </body>
</html>
