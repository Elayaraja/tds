<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.TransactionSummaryBean"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html>
<head>
<link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">
function makeActive(driverid)
{
	 window.location.href = "/TDS/control?action=admin&event=activeDriverDeact&driverid="+driverid;
}
 
</script>
</head>
<body>
<form method="post" action="control" name="masterForm">
<%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
%>

<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.getMCCProcess %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="getSummarytoAlter">
<table>
<tr>
<td>
<table id="pagebox">
	<tr> 
		<td>
			<table id="bodypage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Deactivation&nbsp;Summary</b></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	</td>
	</tr>
		<td>
			<%=request.getAttribute("page")==null?"":request.getAttribute("page") %>
		</td>
	<tr>
	<td>
	<table id="pagebox">
	 
	<%if(request.getAttribute("al_list")!=null){ 
		ArrayList al_list=(ArrayList)request.getAttribute("al_list");
	%>
	<tr>
		<td>
			<table id="bodypage" border="1" align="center" cellpadding="0" cellspacing="0" width="600">
				<tr align="center">
					<td>S.No</td>
					<td>Driver ID</td>
					<td>From Date</td>
					<td>To Date</td>
					<td>Entered Date</td>
					<td>Entered By</td>
					<td>Description</td>
					<td></td>
				</tr>
				<%for(int count=0;count<al_list.size();count++){
					DriverDeactivation deactivation = (DriverDeactivation)al_list.get(count);
				%>
				<tr align="center">
					<td><%=count+1%></td>
					<td><%=deactivation.getDriver_name() %></td>
					<td><%=deactivation.getFrom_date() %></td>
					<td><%=deactivation.getTo_date() %></td>
					<td><%=deactivation.getEntered_date()%></td>
					<td><%=deactivation.getUser()%></td>
					<td><%=deactivation.getDesc()%></td>
					<td>
						<input type="button" value="Activate" onclick="makeActive('<%=deactivation.getDriver_id() %>')">
						
					</td>
				</tr>
				<%} %>
			</table>
			</td>			 
		</tr>
	<%} %>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>