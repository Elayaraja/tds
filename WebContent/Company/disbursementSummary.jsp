<%@page import="com.tds.cmp.bean.DisbursementBean"%>
<%@page import="com.tds.cmp.bean.ComplaintsBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
h2{
font-weight:bold; 
font-style:italic;
color:grey;
}
</style>
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%ArrayList<DisbursementBean> disbursement;
if(request.getAttribute("disbursementSummary")!=null){
	disbursement = (ArrayList<DisbursementBean>)request.getAttribute("disbursementSummary");
}
else {
	disbursement = new ArrayList<DisbursementBean>();
}
%>
<title>TDS(Taxi Dispatch System)</title>
<script type="text/javascript">
	
	function dateValidation() {
		var fromdate = document.getElementById("TagId").value;
		var todate = document.getElementById("EndDate").value;
		if (fromdate != "" || todate != "") {
			var fromDate = fromdate.substring(6, 10) + fromdate.substring(0, 2)
					+ fromdate.substring(3, 5);
			var toDate = todate.substring(6, 10) + todate.substring(0, 2)
					+ todate.substring(3, 5);
			if (Number(toDate) > Number(fromDate)) {
				document.getElementById('details').value = "Get Details";
				document.DisbursementSummary.submit();
				return true;
			} else {
				alert("End Date Should Be Greater Than Start Date");
				document.getElementById('TagId').value = "";
				document.getElementById('EndDate').value = "";
				return false;
			}
		} else {
			document.getElementById('details').value = "Get Details";
			document.DisbursementSummary.submit();
		}
	}
</script>

</head>
<body>
<form name="DisbursementSummary"  method="post" action="control" onsubmit="retutn showProcess()">
<input type="hidden" name="action" value="registration"/>
		<input type="hidden" name="event" value="disbursementMessageSummary"/>
		<input type="hidden" name="details" id="details" value=""/>
		<input type="hidden" name="operation" value="2"/>
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div>
<div>	
<h2><center>Driver Disbursement Message Summary</center></h2>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
        <td>
        <table id="bodypage" width="750" >
        <tr>
		<td>Start Date</td>
		<td><input type="text" name="StartDate" id="TagId" value="" readonly="readonly" onfocus="showCalendarControl(this);" /></td> 
		<iframe width="20" height="178" name="gToday:normal:agenda.startdate" id="gToday:normal:agenda.startdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
		</iframe>
		<td>End Date</td>
		<td><input type="text" name="EndDate" id="EndDate" value="" readonly="readonly" onfocus="showCalendarControl(this);" /></td>
		<iframe width="20" height="178" name="gToday:normal:agenda.startdate" id="gToday:normal:agenda.startdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
		</iframe>
		<tr>
		<td colspan="7" align="center"> <input type="button" name="details" value="Get Details" style="width:150px; height:25px; border:2px blue; background: skyblue; font-weight: bold" align="middle" onclick="dateValidation()"/></td>
		</tr>
		</tr>
		</table>
		<%  if(disbursement!=null&&disbursement.size()>0){%>             	   
                
<table  width="100%" border="0" cellspacing="2" cellpadding="0">
<tr>
	<td>
	<table id="bodypage" width="540">
 	<tr>

		<td style="background-color:lightgrey">StartDate</td>
		<td style="background-color:lightgrey">EndDate</td>
		<td style="background-color:lightgrey">ShortDesc</td>
		
	</tr>
<% 
	boolean colorLightGreen = true;
	String colorPattern;%>
	<%for(int i=0;i<disbursement.size();i++) { 
		colorLightGreen = !colorLightGreen;
		if(colorLightGreen){
			colorPattern="style=\"background-color:lightgreen\""; 
			}
		else{
			colorPattern="";
		}
	%>
	<tr <%=colorPattern%>>

		<td align="center"><%=disbursement.get(i).getStartDate() %>  </td>
		<td align="center"><%=disbursement.get(i).getEndDate() %>  </td>
		<td align="center"><%=disbursement.get(i).getShortDesc() %>  </td>
		<td> <a href="control?action=registration&event=disbursementmessage&module=operationView&Edit=Yes&disbursementNum=<%=disbursement.get(i).getDisbursementNumber()%>&stdate=<%= disbursement.get(i).getStartDate()%>&eddate=<%= disbursement.get(i).getEndDate()%>&shortdesc=<%= disbursement.get(i).getShortDesc()%>&longDesc=<%=disbursement.get(i).getLongDesc()%>" onclick="showProcess()">Edit</a></td> 
	</tr>
	<%} }%>
</table> 
</td>
</tr>
</table>
</td>
</tr>
</table>
</html>