<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="java.util.ArrayList"%>
    <%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@ page errorPage="ExceptionHandler.jsp" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.math.BigDecimal"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
</script>
<%ArrayList al_List = (ArrayList)request.getAttribute("al_list");%>
</head>
<body><div id="page">
 <% if(al_List !=null && !al_List.isEmpty()) { %>
                   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">  
                  <tr>
                        <th width="30%" class="firstCol">Driver Name</th>
                        <th width="20%">Payment Date</th>
                        <th width="15%">Check NO</th>
                  <th width="20%">Amount</th>
                        <th width="20%">Print</th>
                        <th width="30%">Description</th>
                      </tr>
                      <%String pre_d="",curr_d=""; %>
							<%double amount=0; %>
							<%for(int i=0;i<al_List.size();i++) {%>
								<% DriverDisbursment disbursment = (DriverDisbursment)al_List.get(i); %>
								<%curr_d = disbursment.getDriverid(); %>
								<%if(i==0 || curr_d.equals(pre_d)){ %>
									<%amount = amount + Double.parseDouble(disbursment.getAmount()); %>
								<%} %>
							 	<%if((!curr_d.equals(pre_d) && i!=0)) { %> 
							 	
							 	<tr>
										<td colspan="3"  style="background-color:#FCDFFF">Total</td>
										<td align="right" style="background-color:#FCDFFF"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
										<td style="background-color:#FCDFFF"></td>
										<td style="background-color:#FCDFFF"></td>
										<%amount = Double.parseDouble(disbursment.getAmount()); %>
					 				</tr> 
							 	<%} %>
							 	<%if(i%2==0){ %>
							 	<tr>
									<td>
										 <%=disbursment.getDrivername() %>
									</td>
									<td>
										<%=disbursment.getProcess_date() %>
									</td>
  									<td>
										<%=disbursment.getCheckno() %>
									</td>
									<td>
										<a href="" onclick="window.open('/TDS/control?action=registration&event=driverDisbursementSummary&subevent=showDisDetailSummary&module=financeView&pss_key=<%=disbursment.getPayment_id() %>')"><%="$ "+new BigDecimal(disbursment.getAmount()).setScale(2,5) %></a><br>
									</td>
									<td>
										<a href="#" onclick="window.open('<%=request.getContextPath()%>/frameset?__report=BIRTReport/DriverDisburshment.rptdesign&__format=pdf&pay_id=<%=disbursment.getPayment_id() %>')"><i>Print</i></a>
									</td>
								 	 <td>
										<%=disbursment.getDescr() %>										
									</td>
							</tr>	
							<%}else{ %>
							<tr>
									<td style="background-color:lightgreen">
										 <%=disbursment.getDrivername() %>
									</td>
									<td style="background-color:lightgreen">
										<%=disbursment.getProcess_date() %>
									</td>
  									<td style="background-color:lightgreen">
										<%=disbursment.getCheckno() %>
									</td>
									<td style="background-color:lightgreen">
										<a href="#" onclick="window.open('/TDS/control?action=registration&event=driverDisbursementSummary&module=financeView&subevent=showDisDetailSummary&pss_key=<%=disbursment.getPayment_id() %>')"><%="$ "+new BigDecimal(disbursment.getAmount()).setScale(2,5) %></a><br>
									</td>
									<td style="background-color:lightgreen">
										<a href="#" onclick="window.open('<%=request.getContextPath()%>/frameset?__report=BIRTReport/DriverDisburshment.rptdesign&__format=pdf&pay_id=<%=disbursment.getPayment_id() %>')"><i>Print</i></a>
									</td>
								 	 <td style="background-color:lightgreen">
										<%=disbursment.getDescr() %>										
									</td>
							</tr>	
								<%} %>
							<%if(i==al_List.size()-1) { %>
									<tr>
										<td colspan="3" style="background-color:#FCDFFF">Total</td>
										<td align="right" style="background-color:#FCDFFF"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
										<td style="background-color:#FCDFFF"></td>
										<td style="background-color:#FCDFFF"></td>
										<%amount = 0; %>
								  </tr>
								<%} %>
								<%pre_d = curr_d; %>
							<%} %>
						</table>
						<%} %></div>
</body>
</html>
