<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%> 
<%@page import="com.tds.cmp.bean.CmpPaySeteledBean"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
function idrow(row,trid1) {
		for(var i=0;i<7;i++){
			document.getElementById("sch_driverchstatus"+i).value="0";
				//document.getElementById("trid"+i).style.background = "#ACA8CB";
			}	
		 //document.getElementById(trid1).style.background = "gray";
  		 document.getElementById("sch_driverchstatus"+row).value="1";
  }
  
  function checkboxvalue(){
	  showProcess();
var checkvalue="";
	for(var j=0;j<document.masterForm.sch_statuscheck.length;j++){ 
		if(document.masterForm.sch_statuscheck[j].checked)
		{
		checkvalue+="1;";
		} else 	{
		checkvalue+="2;";
		}
		
	}
	
	document.getElementById("checkvalue").value=checkvalue;
	return true;
}
  function success(){
	 /*  document.getElementById("errorpage").innerHTML ="";*/
	  document.getElementById("succ").innerHTML ="";
}
  function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
  </script>
  <style type="text/css">
	.tableproperty
{
	text-align:"center";

 }
  </style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
<%
 	ArrayList al_list = (ArrayList)request.getAttribute("al_list");	   
 	String pageFor = request.getAttribute("pagefor").toString();	
 %> 
   <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" /> 
</head>
<body>
<form name="masterForm" action="control" method="post" onsubmit="return checkboxvalue()">
<input type="hidden" name="action" value="registration">
<input type="hidden" name="event" value="cmpPaymentSettelmentSch">
<input type="hidden" id="checkvalue" name="checkvalue" value="">
<input type="hidden" name="systemsetup" value="6">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />

                	<c class="nav-header"><center>RUN DRIVER CHARGES</center></c><br></br>
 					<div  class="tableproperty">
                    <table width="80%" border="0" cellspacing="0" cellpadding="2" align="center">
                    <%
					StringBuffer error = (StringBuffer) request.getAttribute("error");
					String success = (String) request.getAttribute("success");
					%>
					<tr style="text-align:center">
					<td colspan="7"><center>
						<div id="errorpage">
							<%= (error !=null && error.length()>0 )?"<font color=\"red\">You must fullfilled the following error</font><br>"+error :"" %>
						</div>
						<font color="blue"> <div id="succ"><%= ( success!=null && success.length()>0 )?""+success :"" %></div></font></center>
					</td>
					</tr></table>
					<table width="80%" border="1" cellspacing="0" cellpadding="2" align="center">
						<tr style="text-align:center;background-color:#116969;color:#ffffff" >
                        <th ><center>Day</center></th>
                        <th ><center>Time (HH:MM)</center></th>
                        <th><center>AM/PM</center></th>
                        <th><center>Active Status</center></th>
                        <th><center>Driver Charges Run</center></th>
                      </tr>
                      <%if(al_list==null || al_list.isEmpty()) { %>
                      <tr>
                        <td><center>Monday</center>
                        	<input type="hidden" name="sch_day0"  value="MON">
							<input type="hidden" name="sch_day_name0" value="Monday">
							<input type="hidden" size="5" name="sch_driverchstatus0" id="sch_driverchstatus0" value="2" >
                       </td> 	
                       <td> <center><input type="text" size="3" name="sch_time0" value="" onkeypress="success();"></input></center></td>
                        <td>
                          <select name="AMPM0" id="select">
                          	<option value="AM">AM</option>
                            <option value="PM">PM</option>
                          </select>
                       </td>
                        <td><center><input type="checkbox" name="sch_statuscheck" id="sch_statuscheck" value="2" onkeypress="success();"></center></td>
                        <td><center><input type="radio" name="sch_driver" id="0" onclick="idrow(id,'trid0');" onkeypress="success();"></center></td>                    
                        
                      </tr>
                      <tr align="center">
                        <td><center>Tuesday</center>
                        	<input type="hidden" name="sch_day1" value="TUE">
							<input type="hidden" size="5" name="sch_day_name1" value="Tuesday">
							<input type="hidden" size="5" name="sch_driverchstatus1" id="sch_driverchstatus1" value="2" >
						</td>
						<td>
                       		<center><input type="text" size="5" name="sch_time1" value="" onkeypress="success()"></center>
                        </td>	
                        <td><select name="AMPM1" id="select">
                          	<option value="AM">AM</option>
                            <option value="PM">PM</option>
                          </select></td>
                        <td><center><input type="checkbox" name="sch_statuscheck" id="sch_statuscheck" value="2" onkeypress="success()"></center></td>
                        <td><center><input type="radio" name="sch_driver" id="1" onclick="idrow(id,'trid1');" align="center" onkeypress="success()"></center></td>
                     	
                      </tr>
                      <tr>
                        <td ><center>Wednesday</center>
                            <input type="hidden" name="sch_day2" value="WED">
							<input type="hidden" size="5" name="sch_day_name2" value="Wednesday">
							<input type="hidden" size="5" name="sch_driverchstatus2" id="sch_driverchstatus2" value="2" >
						</td>
                       
                        <td><center><input type="text" size="5" name="sch_time2" value="" onkeypress="success()"></center>
                        </td>
                        <td><select name="AMPM2" id="select">
                          	<option value="AM">AM</option>
                            <option value="PM">PM</option>
                          </select></td>
                        <td><input type="checkbox" name="sch_statuscheck" id="sch_statuscheck" value="2" onkeypress="success()"></td>
                        <td><input type="radio" name="sch_driver" id="2" onclick="idrow(id,'trid2');" onkeypress="success()"></td>
                      </tr>
                      <tr>
                        <td><center>Thursday</center>
                        <input type="hidden" name="sch_day3" value="THU">
						<input type="hidden" size="5" name="sch_day_name3" value="Thursday">
						<input type="hidden" size="5" name="sch_driverchstatus3" id="sch_driverchstatus3" value="2" >
                        </td>
                       <td><center><input type="text" size="5" name="sch_time3" value="" onkeypress="success()"></center>
                        
                       <td ><center><select name="AMPM3" id="select">
                          	<option value="AM">AM</option>
                            <option value="PM">PM</option>
                          </select></center></td>
                        <td><input type="checkbox" name="sch_statuscheck" id="sch_statuscheck" value="2"onkeypress="success()" ></td>
                        <td><input type="radio" name="sch_driver" id="3" onclick="idrow(id,'trid3');" onkeypress="success()"></td>
                      </tr>
                      <tr>
                        <td><center>Friday</center>
                        <input type="hidden" name="sch_day4" value="FRI">
						<input type="hidden" size="5" name="sch_day_name4" value="Friday">
						<input type="hidden" size="5" name="sch_driverchstatus4" id="sch_driverchstatus4" value="2" >
                        </td>
                        <td><center><input type="text" size="5" name="sch_time4" value="" onkeypress="success()"></center></td>
                        <td><center><select name="AMPM4" id="select">
                          	<option value="AM">AM</option>
                            <option value="PM">PM</option>
                          </select><center></td>
                        <td style="text-align:center"><input type="checkbox" name="sch_statuscheck" id="sch_statuscheck" value="2" onkeypress="success()"></td>
                        <td><input type="radio" name="sch_driver" id="4" onclick="idrow(id,'trid4');" onkeypress="success()"></td>
                      </tr>
                      <tr>
                        <td><center>Saturday</center>
                        <input type="hidden" name="sch_day5" value="SAT">
						<input type="hidden" size="5" name="sch_day_name5" value="Saturday">
						<input type="hidden" size="5" name="sch_driverchstatus5" id="sch_driverchstatus5" value="2" >
                        </td>
                        <td><center><input type="text" size="5" name="sch_time5" value="" onkeypress="success()"></center></td>
                        <td><center><select name="AMPM5" id="select">
                          	<option value="AM">AM</option>
                            <option value="PM">PM</option>
                          </select></center></td>
                        <td><input type="checkbox" name="sch_statuscheck" id="sch_statuscheck" value="2" onkeypress="success()"></td>
                        <td><input type="radio" name="sch_driver" id="5" onclick="idrow(id,'trid5');" onkeypress="success()"></td>
                      </tr>
                      <tr>
                        <td><center>Sunday</center>
                        <input type="hidden" name="sch_day6" value="SUN">
						<input type="hidden" size="5" name="sch_day_name6" value="Sunday">
						<input type="hidden" size="5" name="sch_driverchstatus6" id="sch_driverchstatus6" value="2" >
                        </td>
                        <td><center><input type="text"  size="5" name="sch_time6" value="" onkeypress="success()"></center></td>
                       <td><center><select name="AMPM6" id="select">
                          	<option value="AM">AM</option>
                            <option value="PM">PM</option>
                          </select></center></td>
                        <td><input type="checkbox" name="sch_statuscheck" id="sch_statuscheck" value="2" onkeypress="success()"></td>
                        <td><input type="radio" name="sch_driver" id="6" onclick="idrow(id,'trid6');" onkeypress="success()"></td>
                      </tr>
                      <%} else { %>
								<%for(int i=0;i<al_list.size();i++) {%>
									<%CmpPaySeteledBean seteledBean = (CmpPaySeteledBean)al_list.get(i); %>
									<%if(i%2==0){ %>
							<tr id="trid<%=i %>" align="center">
							<td><%=seteledBean.getC_sch_day_name() %>
							<input type="hidden" name="sch_day<%=i %>" value="<%=seteledBean.getC_sch_day() %>">
							<input type="hidden" name="sch_day_name<%=i %>" value="<%=seteledBean.getC_sch_day_name() %>">
							<input type="hidden" size="5" name="sch_driverchstatus<%=i %>" id="sch_driverchstatus<%=i %>" value="<%=seteledBean.getDchargesrunstatus() %>" >
							</td>
							<td><input type="text"  name="sch_time<%=i %>" value="<%=seteledBean.getC_sch_time() %>"></td>
                    		<td><select name="AMPM<%=i %>" id="select"><option value="AM" <%=seteledBean.getC_AMPM().equalsIgnoreCase("AM")?"Selected":"" %>>AM</option>
							<option <%=seteledBean.getC_AMPM().equalsIgnoreCase("PM")?"Selected":"" %>>PM</option>
							</select></td>
							<td><input type="checkbox"  name="sch_statuscheck" id="sch_statuscheck"  <%=seteledBean.getC_sch_status().equalsIgnoreCase("1")?"checked":""   %>></td>
                    		<td><input  type="radio"   name="sch_driver" id="<%=i %>" / <%=seteledBean.getDchargesrunstatus().equalsIgnoreCase("1")?"checked":"" %>></td>
                    	</tr>
                    	<%}else{ %>
                    	<tr id="trid<%=i %>" align="center">
							<td style="background-color:rgb(166, 170, 178)"><%=seteledBean.getC_sch_day_name() %>
							<input type="hidden" name="sch_day<%=i %>" value="<%=seteledBean.getC_sch_day() %>">
							<input type="hidden" name="sch_day_name<%=i %>" value="<%=seteledBean.getC_sch_day_name() %>">
							<input type="hidden" size="5" name="sch_driverchstatus<%=i %>" id="sch_driverchstatus<%=i %>" value="<%=seteledBean.getDchargesrunstatus() %>" >
							</td>
							<td style="background-color:rgb(166, 170, 178)"><input type="text"  name="sch_time<%=i %>" class="inputDV" value="<%=seteledBean.getC_sch_time() %>"></td>
                    		<td style="background-color:rgb(166, 170, 178)"><select name="AMPM<%=i %>" id="select"><option value="AM" <%=seteledBean.getC_AMPM().equalsIgnoreCase("AM")?"Selected":"" %>>AM</option>
							<option <%=seteledBean.getC_AMPM().equalsIgnoreCase("PM")?"Selected":"" %>>PM</option>
							</select></td>
							<td style="background-color:rgb(166, 170, 178)"><input type="checkbox"  name="sch_statuscheck" id="sch_statuscheck"  <%=seteledBean.getC_sch_status().equalsIgnoreCase("1")?"checked":""   %>></td>
                    		<td style="background-color:rgb(166, 170, 178)"><input  type="radio"   name="sch_driver" id="<%=i %>" onclick="idrow(id,'trid<%=i %>');"  <%=seteledBean.getDchargesrunstatus().equalsIgnoreCase("1")?"checked":"" %>></td>
                    	</tr>
								<%}} %>
							<%} %>
                    </table>
                    <%if(pageFor.equalsIgnoreCase("Submit")) {%>
                        	<center><input type="submit" name="Button"  value="Submit" ></center>
                   <%} else { %> 

                        	<center><input type="submit" name="Button"  value="Update"></center>
                    <%} %>      </div>      
           <center><br></br> <div class="footerDV">Copyright &copy; 2010 Get A Cab</div></center>
 
</body>
</html>
