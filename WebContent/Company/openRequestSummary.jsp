
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<%@page import="java.util.HashMap"%>
<%@page import= "java.util.Set"%>
<%@page import="com.tds.cmp.bean.DriverVehicleBean"%>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/OpenRequestDetails.js?vNo=<%=TDSConstants.versionNo%>"></script>

<script type = "text/javascript">
var acdata="";

function todayDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
	var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
	var hrsmin = hors+""+min;  
	
		document.getElementById("fDate").value =cMonth+"/"+cDate+"/"+cYear;
		document.getElementById("tDate").value =cMonth+"/"+cDate+"/"+cYear;
		document.getElementById("keyWord").value ="";

	//document.masterForm.shrs.value  = hrsmin; 
 }
function showCommentsForReservation(row){
			//alert(document.getElementById("commentButton_"+row).value);
			
			$("#comments_"+row).show("slow");
			document.getElementById("rcomments_"+row).style.display = "block";
			$("#commentButton1_"+row).hide("slow");
			$("#commentButton2_"+row).show("slow");
}
		
function hideCommentsForReservation(row){
	//alert(document.getElementById("commentButton_"+row).value);
	document.getElementById("rcomments_"+row).style.display = "none";
	$("#comments_"+row).hide("slow");
	$("#commentButton2_"+row).hide("slow");
	$("#commentButton1_"+row).show("slow");
}

function changeJobFleet(tripid){
	var x=confirm("Do you want to Change the Fleet?");
	if(x==true){
		var fleetNo=document.getElementById("jobFleetNumber").value;
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = '/TDS/DashBoard?event=changeJobToAnotherFleet&tripId=' + tripid + '&fleetNo=' + fleetNo;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == 1) {
			alert("Job Send to to another Fleet");
		}
		
	}	
}


function ChangeFleetToAnotherSum() {
	var fleetNo = document.getElementById("fleetVSum").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = '/TDS/DashBoard?event=changeMultipleJobToAnotherFleet&tripId=' + document.getElementById('tripForanother1').value + '&fleetNo=' + fleetNo	;
	//alert(url);
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		$("#changeFleet").hide();
		alert("Job Send to to another Fleet");
		acdata=0;
		ORSummary(0);
	}
}

function changeJobFleet1(tripid,no){
     acdata=acdata+tripid+";";
     document.getElementById('tripForanother1').value = acdata;
	 $("#changeFleet").show();
	 $("#tripForanother1").val(acdata);
}


function hideJobLogs(row){
	$("#logsReservation_"+row).hide('slow');
	$("#hideJobLogs_"+row).hide();
	$("#jobLogs_"+row).show();
	
}

function showFullDetailsSummary(tripId){
	document.getElementById("individualTable").innerHTML = "";
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getOpenRequest&tripId='+tripId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var jsonObj ="";
	var obj ="";
	var text = xmlhttp.responseText;
	jsonObj = "{\"address\":"+text+"}" ;
	obj = JSON.parse(jsonObj.toString());
	var root =document.getElementById("dialog");
	var table = document.getElementById("individualTable");
	var row="";
	var rowCount="";
	var cell="";
	rowCount = table.rows.length;
	row = table.insertRow(rowCount);
	cell = row.insertCell(0);
	var cw1 = document.createElement("colDet");
	var stw1= document.createTextNode("x");
	cw1.appendChild(stw1);
	cell.style.color="red" ;
	//cell.setAttribute('color', 'red');
	cell.setAttribute('onClick', 'hideFullDetails()');
	cell.appendChild(cw1);
	table.appendChild(cell);
	for(var j=0;j<obj.address.length;j++){
		cell = row.insertCell(0);
		cell.className ="label";
		var nam1 =document.createTextNode("Phone:");
		var c1 = document.createElement("a");
		var st1= document.createTextNode(obj.address[j].Phone);
		c1.appendChild(st1);
		cell.appendChild(nam1);
		cell.appendChild(c1);
		var b1 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam2 =document.createTextNode("Name:");
		var c2 = document.createElement("a");
		var st2= document.createTextNode(obj.address[j].Name);
		c2.appendChild(st2);
		cell.appendChild(b1);
		cell.appendChild(nam2);
		cell.appendChild(c2);
		var b2 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam10 =document.createTextNode("Date:");
		var c10 = document.createElement("a");
		var st10= document.createTextNode(obj.address[j].Date);
		c10.appendChild(st10);
		cell.appendChild(b2);
		cell.appendChild(nam10);
		cell.appendChild(c10);
		var b10 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam11 =document.createTextNode("Time:");
		var c11 = document.createElement("a");
		var st11= document.createTextNode(obj.address[j].Time);
		c11.appendChild(st11);
		cell.appendChild(b10);
		cell.appendChild(nam11);
		cell.appendChild(c11);

		var b9 =document.createElement("br");
		var nam3 =document.createTextNode("P/U Address:");
		var c3 = document.createElement("a");
		var st3= document.createTextNode(obj.address[j].Add1+" "+obj.address[j].Add2+" "+obj.address[j].City);
		c3.appendChild(st3);
		cell.appendChild(b9);
		cell.appendChild(nam3);
		cell.appendChild(c3);
		var b15 =document.createElement("br");
		var nam15 =document.createTextNode("D/O Address:");
		var c15 = document.createElement("a");
		var st15= document.createTextNode(obj.address[j].EAdd1+" "+obj.address[j].EAdd2+" "+obj.address[j].ECity);
		c15.appendChild(st15);
		cell.appendChild(b15);
		cell.appendChild(nam15);
		cell.appendChild(c15);
		var b21 =document.createElement("br");
		var nam22 =document.createTextNode("Pay Type:");
		var c31 = document.createElement("a");
		var st31= document.createTextNode(obj.address[j].PayType);
		c31.appendChild(st31);
		cell.appendChild(b21);
		cell.appendChild(nam22);
		cell.appendChild(c31);
		var b22 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam23 =document.createTextNode("Account:");
		var c41 = document.createElement("a");
		var st41= document.createTextNode(obj.address[j].Acct);
		c41.appendChild(st41);
		cell.appendChild(b22);
		cell.appendChild(nam23);
		cell.appendChild(c41);
		var b23 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam24 =document.createTextNode("Zone:");
		var c51 = document.createElement("a");
		var st51= document.createTextNode(obj.address[j].Zone);
		c51.appendChild(st51);
		cell.appendChild(b23);
		cell.appendChild(nam24);
		cell.appendChild(c51);

		var b24 =document.createElement("br");
		var nam26 =document.createTextNode("Driver:");
		var c71 = document.createElement("a");
		var st71= document.createTextNode(obj.address[j].Driver);
		c71.appendChild(st71);
		cell.appendChild(b24);
		cell.appendChild(nam26);
		cell.appendChild(c71);
		var b13 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam14 =document.createTextNode("Trip ID:");
		var c14 = document.createElement("a");
		var st14= document.createTextNode(obj.address[j].tripId);
		c14.appendChild(st14);
		cell.appendChild(b13);
		cell.appendChild(nam14);
		cell.appendChild(c14);
		var b111 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam25 =document.createTextNode("LandMark:");
		var c61 = document.createElement("a");
		var st61= document.createTextNode(obj.address[j].LandMark);
		c61.appendChild(st61);
		cell.appendChild(b111);
		cell.appendChild(nam25);
		cell.appendChild(c61);

		var b12 =document.createElement("br");				
		var nam12 =document.createTextNode("Dispatch Comments:");
		var c12 = document.createElement("a");
		var st12= document.createTextNode(obj.address[j].Comments);
		c12.appendChild(st12);
		cell.appendChild(b12);
		cell.appendChild(nam12);
		cell.appendChild(c12);
		var b26 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam13 =document.createTextNode("Cab Comments:");
		var c13 = document.createElement("a");
		var st13= document.createTextNode(obj.address[j].SplIns);
		c13.appendChild(st13);
		cell.appendChild(b26);
		cell.appendChild(nam13);
		cell.appendChild(c13);
		var b116 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam113 =document.createTextNode("Cab No:");
		var c113 = document.createElement("a");
		var st113= document.createTextNode(obj.address[j].Vehicle);
		c113.appendChild(st113);
		cell.appendChild(b116);
		cell.appendChild(nam113);
		cell.appendChild(c113);

		var b316 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
		var nam313 =document.createTextNode("Special Flags:");
		var c313 = document.createElement("a");
		var st313= document.createTextNode(obj.address[j].SFL);
		c313.appendChild(st313);
		cell.appendChild(b316);
		cell.appendChild(nam313);
		cell.appendChild(c313);

//		var b421 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
//		var nam421 =document.createTextNode("Call Driver");
//		var c421 = document.createElement("button");
//		c421.onclick=function(){callDriverForStatus(tripId);};
//		var st421= document.createTextNode("CBD");
//		c421.appendChild(st421);
//		cell.appendChild(b421);
//		cell.appendChild(nam421);
//		cell.appendChild(c421);


		var b216 =document.createElement("br");
		var nam213 =document.createTextNode("Status:");
		var c213 = document.createElement("a");
		var st213="";
		if(obj.address[j].status=="30"){
			st213= document.createTextNode("Broadcast");
		}else if(obj.address[j].status=="40"){
			st213= document.createTextNode("Allocated");
		}else if(obj.address[j].status=="43"){
			st213= document.createTextNode("OnRoute To Pickup");
		}else if(obj.address[j].status=="49"){
			st213= document.createTextNode("Soon To Clear");
		}else if(obj.address[j].status=="50"){
			st213= document.createTextNode("Customer Canceled Request");
		}else if(obj.address[j].status=="51"){
			st213= document.createTextNode("No Show");
		}else if(obj.address[j].status=="4"){
			if(obj.address[j].dontDispatch=="1"){
				st213= document.createTextNode("Dont Dispatch");	
			}else{
				st213= document.createTextNode("Dispatching");
			}
		}else if(obj.address[j].status=="0"){
			st213= document.createTextNode("Unknown");
		}else if(obj.address[j].status=="47"){
			st213= document.createTextNode("Trip Started");
		}else if(obj.address[j].status=="99"){
			st213= document.createTextNode("Trip On Hold");
		}else if(obj.address[j].status=="3"){
			st213= document.createTextNode("Couldn't Find Drivers");
		}else if(obj.address[j].status=="48"){
			st213= document.createTextNode("Driver Reported No Show");
		}else if(obj.address[j].status=="55"){
			st213= document.createTextNode("Operator Reported No Show");
		}else if(obj.address[j].status=="52"){
			st213= document.createTextNode("Company Couldn't Service");
		}else if(obj.address[j].status=="45"){
			st213= document.createTextNode("Driver Onsite");
		}else if(obj.address[j].status=="90"){
			st213= document.createTextNode("SrHold Job");
		}else{
			st213= document.createTextNode(" Status Not Found!");
		}
		c213.appendChild(st213);
		cell.appendChild(b216);
		cell.appendChild(nam213);
		cell.appendChild(c213);

		var b456 =document.createTextNode("\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0");
		var nam453 =document.createTextNode("Flight Info:");
		var c453 = document.createElement("a");
		if(obj.address[j].ANAM!=""){
			var st453= document.createTextNode(obj.address[j].ANAM+"-"+obj.address[j].AN+"("+obj.address[j].AFROM+"-"+obj.address[j].ATO+")");
		} else {
			var st453=document.createTextNode("");
		}
		c453.appendChild(st453);
		cell.appendChild(b456);
		cell.appendChild(nam453);
		cell.appendChild(c453);
	}
	table.appendChild(cell);
	root.appendChild(table);
	$('#dialog').jqm();
	$('#dialog').jqmShow();
}
function printReservationJob(tripId){
	window.open("ReportController?event=reportEvent&ReportName=jobMail&output=pdf&tripId="+tripId+"&type=basedMail&assoccode="+document.getElementById("assoccodeSum").value+"&timeOffset="+document.getElementById("timeZoneSum").value);
}


$(document).ready(function(){
  	 todayDate();
  });  
function changeFleet(fleet,i) {
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = 'SystemSetupAjax?event=changeFleet&fleet=' + fleet;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);

	var text = xmlhttp.responseText;
	if(text.indexOf("Successfully")<0){
		alert(text);
	}
	//readCookieForCenter();
	document.getElementById("changedFleet").value=fleet;
	//onloadDash();
	
	$(".fleetbutton").css('backgroundColor', '');
	$("#fleet_"+i).css('backgroundColor', 'green');
	document.getElementById("fleetForOR").value=document.getElementById("changedFleet").value;		
	//$("#color").val($("#fleetcolor_"+i).val());
	//document.getElementById().style.backgroundColor = color;
	document.getElementById("top-header").style.backgroundColor = "#"+$("#fleetcolor_"+i).val();			
	//changeCompanyLogo();
	//location.reload();
}
 	

</script>
	<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>

	<%if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>
	<%
			ArrayList<OpenRequestBO> openReqSummary = (ArrayList<OpenRequestBO>) request.getAttribute("openRequestSummary");
			HashMap<String,DriverVehicleBean> companyFlags=(HashMap<String,DriverVehicleBean>) request.getAttribute("allFlags");
		%>
	<%
			AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
		%>
	<input type="hidden" name="action" value="openrequest" /> 
	<input type="hidden" name="event" value="openRequestSummary" />
	<input type="hidden" name="tripForanother1" id="tripForanother1" />	
	<input type="hidden" name="assoccode" id="assoccodeSum" value="<%=adminBo.getMasterAssociateCode()%>" />	
	<input type="hidden" name="timeZone" id="timeZoneSum" value="<%=adminBo.getTimeZone()%>" />	
	<input type="hidden" name="fleetSizeR" id="fleetSizeR" value=<%=fleetList.size()%> />
	<form>
	<div id="ORRedispatch">
		<table width="100%" style="background-color: #EFEFFB" cellspacing="1" cellpadding="0" class="driverChargTab">
			<tr class="orSummaryRow" style="cursor: move">
				<td colspan='15' align='center' style="background-color: #CEECF5; size: 1022px"><h5>
						<center>  
							Reservation Summary&nbsp;<a style="margin-left: 60px" href="#">
								<img alt="" src="images/Dashboard/close.png"
								onclick="removeORSummary();hideCalendarControl()">
							</a>
						</center>
						<a href="#">
					</h5></td>
			</tr>
			<tr><td>	</td>
				<%if( fleetList!=null && fleetList.size()>0) {%>
							<td>Select Fleet : 
							<select name="fleetNoR" id="fleetNoR" >
							<option value="all" >All</option>								
		                      <%for(int j=0;j<fleetList.size();j++){ %>
										<option value="<%=fleetList.get(j).getFleetNumber()%>" ><%=fleetList.get(j).getFleetName() %></option>
										<%=fleetList.get(j).getFleetNumber()%>
									<%
								}%> 
		                       </select>
		                       </td>
				<%}%>
				<td><font color="black">Search:</font> <input type="text"
					name="keyWord" id="keyWord" size="10" value=""
					onblur="ORSummary(0)" onfocus="hideCalendarControl();" /></td>

				<td><font color="black">From Date:</font> <input type="text"
					name="fDate" id="fDate" size="10" value=""
					onfocus="hideCalendarControl();showCalendarControl(this);" /></td>
				<td><font color="black">To Date:</font> <input type="text"
					name="tDate" id="tDate" size="10" value=""
					onfocus="hideCalendarControl();showCalendarControl(this);" /></td>
				<td><input type="radio" name="account" id="account" size="10"
					onfocus="hideCalendarControl();" /><font color="black">Account</font>
				</td>
				<td><input type="radio" name="account" id="repeatJobs"
					size="10" onfocus="hideCalendarControl();" /><font color="black">Repeat
						Trips</font></td>
				<td><input type="radio" name="account" id="all" size="10"
					value="" onfocus="hideCalendarControl();" /><font color="black">All</font>
				</td>
				<td><input type="button" name="submit" id="submit"
					value="search" onclick="ORSummary(0)"
					onfocus="hideCalendarControl();" /></td>
				<td><input type="button" name="download" id="download"
					value="Download" onclick="ORSummary(10)"
					onfocus="hideCalendarControl();" /></td>
			</tr>
			<%
		if (openReqSummary != null && openReqSummary.size() > 0) {
						%>
			<table width="100%" id="ORSummaryResult"
				style="width: 100%; background-color: #CEECF5" cellspacing="1"
				cellpadding="0" class="driverChargTab">

				<tr>
					<td>
						<div class='ScrollFormGrid'>
							<table width="100%" class="jobsSummary"
								style="background-color: #CEECF5">

								<tr>
									<th align="center" style="background-color: #EFEFFB"><font
										size="2" color="black">TripId</font></th>
									<th align="center" style="background-color: #EFEFFB"><font
										size="2" color="black">Allocate Driver</font></th>
									<th align="center" style="background-color: #EFEFFB"><font
										size="2" color="black">Allocate Cab</font></th>
									<th align="center" style="background-color: #EFEFFB"><font
										size="2" color="black">Lookup</font></th>
									<th align="center" style="background-color: #EFEFFB"><font
										size="2" color="black">Time</font></th>
									<th align="center" style="background-color: #EFEFFB">
										<%if(adminBo.getDispatchBasedOnVehicleOrDriver()==1){%> <font
										size="2" color="black">DriverId</font> <%}else{ %> <font
										size="2" color="black">Cab No</font> <% }%>
									</th>
									<th align="center" style="background-color: #EFEFFB"><font
										size="2" color="black">Phone</font></th>
									<th align="center" style="background-color: #EFEFFB"><font size="2"
										color="black">Name</font></th>

									<th align="center" style="background-color: #EFEFFB"><font
										size="2" color="black">Pick up Address</font></th>

									<th align="center" style="background-color: #EFEFFB"><font size="2"
										color="black">Drop Address</font></th>

									<th align="center"
										style="background-color: #EFEFFB"><font size="2"
										color="black">Comments</font></th>
									<th align="center" style="background-color: #EFEFFB"><font size="2"
										color="black">Special Flags</font></th>
									<th align="center" style="background-color: #EFEFFB"><font size="2"
										color="black">Created By</font></th>
										
									<th align="center" style="background-color: #EFEFFB"><font size="2"
										color="black">Pass#</font></th>
									<th align="center" style="background-color: #EFEFFB"><font size="2"
										color="black">Ref. No#</font></th>
										
									<th align="center" style="background-color: #EFEFFB"><font size="2"
										color="black">Payment Type</font></th>
										
									<%if(fleetList!=null && fleetList.size()>0){ %>
									<th align="center" style="background-color: #EFEFFB"><font size="2"
										color="black">Fleet Name</font></th>
									<%} %>
								</tr>

								<%
				boolean colorLightGreen = true;
				String colorPattern;
				for (int i = 0; i < openReqSummary.size(); i++) {
					String specialFlags="";
					colorLightGreen = !colorLightGreen;
					if (colorLightGreen) {
						colorPattern = "style=\"background-color:#EFEFFB;white-space: nowrap;\"";
					} else {
						colorPattern = "style=\"background-color:#EFEFFB;white-space: nowrap;\"";
					}
					
					if(companyFlags!=null){
						for(int j=0;j<openReqSummary.get(i).getDrProfile().length();j++){
							specialFlags = specialFlags + companyFlags.get(openReqSummary.get(i).getDrProfile().substring(j,j+1)).getShortDesc() + ";";                     
							colorPattern = "style=\"background-color:#FF9933;white-space: nowrap;\"";
						}
					}

					%>
					<tr id="row" class='jobSummaryRow'>
						<td style="display: none" id="jobSummaryTripid"><%=openReqSummary.get(i).getTripid()%></td>
						
						<td align="center" <%=colorPattern%> style="width:10%">
						<font size="2" color="black"><%=openReqSummary.get(i).getTripid()==null?"":openReqSummary.get(i).getTripid()%></font>
						<input type="hidden" name="tripId" id="tripIdSummary<%=i%>" value=<%=openReqSummary.get(i).getTripid()%>></input>
						</td>
						
						<td align="center" style="background-color: #EFEFFB; width:10%">
						<input type="text" id="driverId<%=i%>" value="<%=openReqSummary.get(i).getDriverid()==null?"":openReqSummary.get(i).getDriverid()%>"
							onblur="allocateDriver(<%=i%>,<%=openReqSummary.get(i).getTripid()%>,1,'<%=openReqSummary.get(i).getAssociateCode() %>','<%=adminBo.getAssociateCode() %>','<%=openReqSummary.get(i).getDontDispatch() %>')"
							size="5" /></td>
						<input type="hidden" id="assoccodeSummary<%=i %>" value="<%=openReqSummary.get(i).getAssociateCode()%>" />
						<input type="hidden" id="assoccodeMain<%=i %>" value="<%=adminBo.getAssociateCode()%>" />
						<td align="center" style="background-color: #EFEFFB; width:6%">
						<input type="text" id="vehicleNo<%=i%>"	value="<%=openReqSummary.get(i).getVehicleNo()==null?"":openReqSummary.get(i).getVehicleNo()%>"
							onblur="allocateDriver(<%=i%>,<%=openReqSummary.get(i).getTripid()%>,2,'<%=openReqSummary.get(i).getAssociateCode() %>','<%=adminBo.getAssociateCode() %>','<%=openReqSummary.get(i).getDontDispatch() %>')"
							size="3" /></td>
						<td align="center" style="background-color: #EFEFFB;width:6%">
						<input type="button" id="lookUp" value="LookUp" onclick="suggestDriverSummary(<%=i%>,<%=openReqSummary.get(i).getDontDispatch()%>)" />
						</td>
						<td align="center" <%=colorPattern%> style="width:15%">
						<font size="2" color="black"><%=openReqSummary.get(i).getStartTimeStamp()==null?"":openReqSummary.get(i).getStartTimeStamp()%></font></td>
						<td align="center" <%=colorPattern%> style="width:8%">
						<font size="2" color="black"> 
						<%if(adminBo.getDispatchBasedOnVehicleOrDriver()==1){%>
								<%=openReqSummary.get(i).getDriverid()==null?"":openReqSummary.get(i).getDriverid()%>
								<%}else{ %> <%=openReqSummary.get(i).getVehicleNo()==null?"":openReqSummary.get(i).getVehicleNo()%>
						<% }%>
						</font></td>
						<td align="center" <%=colorPattern%> style="width:15%">
						<font size="2" color="black"><%=openReqSummary.get(i).getPhone()==null?"":openReqSummary.get(i).getPhone()%></font>
						</td>
						<td align="center" <%=colorPattern%> style="width:15%">
						<font size="2" color="black"><%=openReqSummary.get(i).getName()==null?"":openReqSummary.get(i).getName()%></font>
						</td>
						<td align="center" <%=colorPattern%> title="<%=openReqSummary.get(i).getSadd1()==null?"":openReqSummary.get(i).getSadd1()%> <%=openReqSummary.get(i).getSadd2()==null?"":openReqSummary.get(i).getSadd2()%> <%=openReqSummary.get(i).getScity()==null?"":openReqSummary.get(i).getScity()%> <%=openReqSummary.get(i).getSstate()==null?"":openReqSummary.get(i).getSstate()%> <%=openReqSummary.get(i).getSzip()==null?"":openReqSummary.get(i).getSzip()%>"  style="width:15%">
						<font size="2" color="black"><%=(openReqSummary.get(i).getSlandmark()!=null && !openReqSummary.get(i).getSlandmark().equals(""))?openReqSummary.get(i).getSlandmark():(openReqSummary.get(i).getSadd1()==null?"":(openReqSummary.get(i).getSadd1().length()>25?openReqSummary.get(i).getSadd1().substring(0,25):openReqSummary.get(i).getSadd1()))%>
						</font>	
						</td>
						<td align="center" <%=colorPattern%> title="<%=openReqSummary.get(i).getEadd1()==null?"":openReqSummary.get(i).getEadd1()%> <%=openReqSummary.get(i).getEadd2()==null?"":openReqSummary.get(i).getEadd2()%> <%=openReqSummary.get(i).getEcity()==null?"":openReqSummary.get(i).getEcity()%> <%=openReqSummary.get(i).getEstate()==null?"":openReqSummary.get(i).getEstate()%> <%=openReqSummary.get(i).getEzip()==null?"":openReqSummary.get(i).getEzip()%>"  style="width:15%">
						<font size="2" color="black"><%=(openReqSummary.get(i).getElandmark()!=null && !openReqSummary.get(i).getElandmark().equals(""))?openReqSummary.get(i).getElandmark():(openReqSummary.get(i).getEadd1()==null?"":(openReqSummary.get(i).getEadd1().length()>25?openReqSummary.get(i).getEadd1().substring(0,25):openReqSummary.get(i).getEadd1()))%>
						</font></td>	
						<td align="center" <%=colorPattern%> style="width:10%">
						<font size="2" color="black"> 
						<%if((openReqSummary.get(i).getComments()!=null && !openReqSummary.get(i).getComments().equals("")) || (openReqSummary.get(i).getSpecialIns()!=null && !openReqSummary.get(i).getSpecialIns().equals("")) ){%>
						<input type="button" id="commentButton1_<%=i %>" value="Show Comments" onclick="showCommentsForReservation(<%=i%>)" /> 
						<input type="button" id="commentButton2_<%=i %>" style="display: none;" value="Hide Comments" onclick="hideCommentsForReservation(<%=i%>)" />
						<%--	<img style="height: 15px; width: 20px;" alt="" id="imgReplace_<%=i %>" src="images/2Downarrow.png" align="right" onclick="showCommentsForReservation(<%=i%>)"> --%> 						
						<%}else{ %> NO comments <%} %>
						</font></td>
						<td align="center" <%=colorPattern%> style="width:6%">
						<font size="2" color="black"><%=specialFlags==null?"":specialFlags%></font>
						</td>
						<td align="center" <%=colorPattern%> style="width:10%">
						<font size="2" color="black"><%=openReqSummary.get(i).getCreatedBy()==null?"":openReqSummary.get(i).getCreatedBy()%></font>
						</td>
						
						<td align="center" <%=colorPattern%> style="width:6%">
						<font size="2" color="black"><%=openReqSummary.get(i).getNumOfPassengers()<1?1:openReqSummary.get(i).getNumOfPassengers()%></font>
						</td>
						<td align="center" <%=colorPattern%> style="width:10%">
						<font size="2" color="black"><%=openReqSummary.get(i).getRefNumber()==null?"":openReqSummary.get(i).getRefNumber()%></font>
						</td>
						
						<td align="center" <%=colorPattern%> style="width:8%">
						<font size="2" color="black">
						<%if(openReqSummary.get(i).getPaytype().equalsIgnoreCase("VC")){ %>
							VC(<%=openReqSummary.get(i).getAcct()%>)
						<%}else if(openReqSummary.get(i).getPaytype().equalsIgnoreCase("Cash")){%>
							Cash
						<%}else{ %>
							<%=openReqSummary.get(i).getPaytype() %>
						<%} %>
						</font>
						</td>
						
						<%if(fleetList!=null && fleetList.size()>0){ %>
						<td align="center" <%=colorPattern%> style="width:10%">
						<select id="jobFleetNumber" onchange="changeJobFleet(<%=openReqSummary.get(i).getTripid()%>)">
						<%for(int j=0;j<fleetList.size();j++){ %>
						<option value="<%=fleetList.get(j).getFleetNumber() %>"
							<%=fleetList.get(j).getFleetNumber().equals(openReqSummary.get(i).getAssociateCode())?"selected=selected":"" %>><%=fleetList.get(j).getFleetName()%>
						</option>
							<%} %>
						</select></td>
						<%} %>
						<td style="width:5%" align="center"><input type="checkbox" name="resjob" id="resjob" value="resjob" onclick="changeJobFleet1(<%=openReqSummary.get(i).getTripid()%>,<%=i%>)"> </input></td>
					
						<td align="center" <%=colorPattern%> style="width:10%">
	 						<input type="button" value="Logs" id="jobLogs_<%=i %>" onclick="jobreservationLogs(<%=openReqSummary.get(i).getTripid()%>,<%=i%>)" />
 							<input type="button" value="Hide Logs" style="display: none;" id="hideJobLogs_<%=i %>" onclick="hideJobLogs(<%=i%>)" />
 						</td>
						<td align="center" <%=colorPattern %> style="width:10%">
 							<input type="button" value="Details" id="detailShow_<%=i %>" onclick="showFullDetailsSummary('<%=openReqSummary.get(i).getTripid()%>')" />	
						</td>
						<td align="center" <%=colorPattern%> style="width:10%"> 
						<input type="button" value="Edit" id="editJob_<%=i %>" onclick="openOR1(<%=openReqSummary.get(i).getTripid()%>,'Dashboard')" />
						</td>
						<td align="center" <%=colorPattern%> style="width:10%">
						<input type="button" value="Cancel" onclick="deleteJobsSummary(<%=openReqSummary.get(i).getTripid()%>);" />
						</td>
						<td align="center" <%=colorPattern%> style="width:10%">
						<input type="button" value="Print" onclick="printReservationJob(<%=openReqSummary.get(i).getTripid()%>);" />
						</td>
					</tr>
					
					<tr>
						<td colspan="16" id="comments_<%=i%>"  style="background-color: lightgray; white-space: nowrap;">
							<div id="rcomments_<%=i%>" style="display: none;">
							<font size="2" color="black" title="Dispatch and Cab Comments">
								DC:<%=openReqSummary.get(i).getComments()==null?"":openReqSummary.get(i).getComments()%>
								CC:<%=openReqSummary.get(i).getSpecialIns()==null?"":openReqSummary.get(i).getSpecialIns()%>
						</font></div>
						</td>
					</tr>
					</tr>
					<tr>
						<td colspan="16" id="logsReservation_<%=i%>" style="background-color: lightgray; white-space: nowrap;">
						<div id="rJobLogsPopUp_<%=i%>" >
						</div>
						</td>
					</tr>

<% }}%>						
		</table>
		</div>
		</td>
		</tr>

	<div id="changeFleetSum" style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right; width: 120px; height: 30px; position: absolute; top: 150pt; left: 300pt;">
       <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeFleetScreen()">
                  <select name="fleetVSum" id="fleetVSum" >
                      <%if(fleetList!=null){
       for(int k=0;k<fleetList.size();k++){ %>
       <option value="<%=fleetList.get(k).getFleetNumber()%>" ><%=fleetList.get(k).getFleetName() %></option>
<% }}%>
        </select>
 <input type="hidden" name="tripForanother" id="tripForanother" />
 <input type="hidden" name="no" id="no" value=""/>
 
 <input type="button" name="changeFleetD" id="changeFleetD" value="Change" onclick="ChangeFleetToAnotherSum()"/>
  </div>
 		
		</table>
		
		</table>
		</div>
		</form>