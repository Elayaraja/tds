<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@ page errorPage="ExceptionHandler.jsp" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.math.BigDecimal"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<title>Insert title here</title>
<%
	ArrayList al_List = (ArrayList)request.getAttribute("al_list");
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
%>

<script type="text/javascript">

 </script>
</head>
<body>
<form name="masterForm" action="control" method="post">
	<input type="hidden" name="action" value="registration">
	<input type="hidden" name="event" value="driverDisbursementSummary">
	<input type="hidden" name="size" id="size" value="<%=al_List!=null?al_List.size():"0" %>">
	<table id="pagebox">	
		<tr>
		<td>  
			<table id="bodypage">	
				<tr>
					<td colspan="7" align="center">		
						<div id="title">
							<h2>Driver Disbursement Summary</h2>
						</div>
					</td>		
				</tr>
				 
			 			 
						<tr>
							<td>&nbsp;</td>
							<td>From Date</td>
							<td>&nbsp;</td>
							<td>
								 <input type="text" name="from_date" onfocus="showCalendarControl(this);" size="10" value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly">
								 
							</td>
							<td>&nbsp;</td>
							<td>To Date</td>
							<td>&nbsp;</td>
							<td>
								 <input type="text" name="to_date" size="10" onfocus="showCalendarControl(this);" value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly">
								 
							</td>
						</tr>	
						 
					 
				<%if(!((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equalsIgnoreCase("Driver")) {%>
				 		<tr>
							<td>&nbsp;</td>
							<td>Driver ID</td>
							<td>&nbsp;</td>
							<td>
						      <input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername','dName')" id="dd_driver_id" name="dd_driver_id" size="15"   autocomplete="off" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>" class="form-autocomplete">
								
						  		 <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 			
							 		  
							</td>
									<%--<select name="dd_driver_id" id="dd_driver_id"  onchange="caldid(id,'drivername'),enableButton();">
									<option>Select</option>
										<% for(int j=0 ;j<al_driver.size();j=j+2){ %>
											<option value="<%=al_driver.get(j).toString() %>" <%=(al_driver.get(j).toString().equalsIgnoreCase(request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id")))?"selected":"" %> ><%=al_driver.get(j+1) %></option>
										<%} %>
								</select> --%>
							
							</tr>
							<tr>
							<td>&nbsp;</td>
							<td></td>
							<td>&nbsp;</td>
							
							<td> 
								  <input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">		
							 	  <div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
							 </td>
						</tr>	
				 
						<%} %>
				<tr>
					<TD colspan="3" align="center">
						<input type="submit" name="Button"  value="Get" tabindex="8" >&nbsp;&nbsp;&nbsp;
					</TD>
				</tr>		
			</table>
		</td>
	</tr>
	
	<% if(al_List !=null && !al_List.isEmpty()) { %>
		<tr>
			<td>	
				<table id="bodypage" border="1">
						<tr>
							<td>Driver Name</td><td>Payment Date</td><td>Check No</td><td>Amount</td><td>Description</td>
						</tr>	
							<%String pre_d="",curr_d=""; %>
							<%double amount=0; %>
							<%for(int i=0;i<al_List.size();i++) {%>
								<% DriverDisbursment disbursment = (DriverDisbursment)al_List.get(i); %>
								<%curr_d = disbursment.getDriverid(); %>
								<%if(i==0 || curr_d.equals(pre_d)){ %>
									<%amount = amount + Double.parseDouble(disbursment.getAmount()); %>
								<%} %>
							 	<%if((!curr_d.equals(pre_d) && i!=0)) { %>
									<tr>
										<td colspan="3">Total</td>
										<td align="right"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
										<td></td>
										<%amount = Double.parseDouble(disbursment.getAmount()); %>
					 				</tr> 
							 	<%} %>
								<tr>
									<td>
										 <%=disbursment.getDrivername() %>
									</td>
									<td>
										<%=disbursment.getProcess_date() %>
									</td>
  									<td>
										<%=disbursment.getCheckno() %>
									</td>
									<td>
										<a href="#" onclick="window.open('/TDS/control?action=registration&event=driverDisbursementSummary&subevent=showDisDetailSummary&pss_key=<%=disbursment.getPayment_id() %>')"><%="$ "+new BigDecimal(disbursment.getAmount()).setScale(2,5) %></a><br>
									 	<a href="#" onclick="window.open('<%=request.getContextPath()%>/frameset?__report=BIRTReport/DriverDisburshment.rptdesign&__format=pdf&pay_id=<%=disbursment.getPayment_id() %>')"><i>Print</i></a>
									</td>
								 	 <td>
										<%=disbursment.getDescr() %>										
									</td>
							 	</tr>	
								<%if(i==al_List.size()-1) { %>
									<tr>
										<td colspan="3">Total</td>
										<td align="right"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
										<td></td>
										<%amount = 0; %>
				 					</tr>
								<%} %>
								<%pre_d = curr_d; %>
							<%} %>
							 
						
				</table>
			</td>
		</tr>
 	<%} %>
	
</table>
</form>
</body>
</html>