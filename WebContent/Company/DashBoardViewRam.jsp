<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.dao.AdministrationDAO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.DriverLocationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.common.util.TDSProperties"%>



<html>
<head>
<style type="text/css">

#ele_li li
{
background-image:url(images/li_img.png);
list-style-type:none;
padding-left:18px;
background-repeat:no-repeat;
margin-top:3px;
}
</style>
 <%
 	ArrayList<OpenRequestBO> jobs = (ArrayList<OpenRequestBO>)request.getAttribute("Jobs");		
	ArrayList<DriverLocationBO> drivers = (ArrayList<DriverLocationBO>)request.getAttribute("Drivers");	

 %>
  


<link href="css/newtdsstyles.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Control Center</title>
<script src='<%= TDSProperties.getValue("googleMap") %>' type="text/javascript"></script>

<script type="text/javascript">
var mins,secs,TimerRunning,TimerID,TimerID1;
TimerRunning=false,i=1;

function Init() //call the Init function when u need to start the timer
{
   mins=1;
   secs=0;
   StopTimer();
}

function StopTimer()
{
	 
   if(TimerRunning)
      clearTimeout(TimerID);
   TimerRunning=false;

   //mins=document.getElementById('sec').value;
   secs=document.getElementById('sec').value;
   StartTimer();
}

function StartTimer()
{
 
   TimerRunning=true;
   //window.status="Time Remaining "+Pad(mins)+":"+Pad(secs);
   document.getElementById('tid').innerHTML = "Time Remaining :"+Pad(secs);
   TimerID=self.setTimeout("StartTimer()",1000);
   
   //Check();
   
   //if(mins==0 && secs==0)
   if(secs==0)
   {
      StopTimer();
      dashBoardAjax2();
   }
   //if(secs==0)
   //{
   //   mins--;
   //   secs=60;
   //}
   secs--;

}

function Check()
{
   if(mins==5 && secs==0)
      alert("You have only five minutes remaining");
   else if(mins==0 && secs==0)
   {
      alert("Your alloted time is over.");
   }
}

function Pad(number) //pads the mins/secs with a 0 if its less than 10
{
   if(number<10)
      number=0+""+number;
   return number;
}

function setOpacity( value ) {
	document.getElementById("styled_popup").style.opacity = value / 10;
	document.getElementById("styled_popup").style.filter = 'alpha(opacity=' + value * 10 + ')';
}

function fadeInMyPopup() {
	for( var i = 0 ; i <= 100 ; i++ )
		setTimeout( 'setOpacity(' + (i / 10) + ')' , 8 * i );
}

function fadeOutMyPopup() {
	for( var i = 0 ; i <= 100 ; i++ ) {
		setTimeout( 'setOpacity(' + (10 - i / 10) + ')' , 8 * i );
	}

	setTimeout('closeMyPopup()', 800 );
}

function closeMyPopup() {
	document.getElementById("styled_popup").style.display = "none"
}

function fireMyPopup() {
	setOpacity( 0 );
	document.getElementById("styled_popup").style.display = "block";
	fadeInMyPopup();
}
	
function initialize() {
		   
		      if (GBrowserIsCompatible() ) { 
					map = new GMap2(document.getElementById("map"));
					map.setCenter(new GLatLng(39.6,-104.86), 4);
					map.setMapType(G_NORMAL_MAP);
					map.addControl(new GLargeMapControl());
					map.addControl(new MapTypeControl());
					map.addControl(new GScaleControl());
					map.enableScrollWheelZoom();
					map.disableDoubleClickZoom();
					geocoder = new GClientGeocoder();

					marker = new GMarker(new GLatLng(39.6, -104.86), {
						draggable : true
					});
					map.addOverlay(marker);

					GEvent.addListener(map, 'click', function(overlay, point) {
						if (overlay) {
						} else {
							posset = 1;
							fc(point);
							if (zm == 0) {
								map.setCenter(point);
								zm = 1;
							} else {
								map.setCenter(point);
							}
							computepos(point);
							checkZone();
						}
					});

					GEvent
							.addListener(map, 'singlerightclick',
									function(point, src, overlay) {
										if (overlay) {
											if (overlay != marker) {
												map.removeOverlay(overlay)
												document.getElementById("latbox"+ clickCount).value = '';
												document.getElementById("lonbox"+ clickCount).value = '';
												table.deleteRow(clickCount);
												
											}
										} else {
										}
									});

					GEvent.addListener(marker, "dragend", function() {
						var point = marker.getLatLng();
						posset = 1;

						if (zm == 0) {
							map.setCenter(point);
							zm = 1;
						} else {
							map.setCenter(point);
						}
					});

					GEvent.addListener(marker, "click", function() {
						var point = marker.getLatLng();
						marker.openInfoWindowHtml(marker.getLatLng().toUrlValue(6));
						computepos(point);
					});

				}
		      
}

</script>
<script type="text/javascript" src="Ajax/DashBoard.js"></script>
</head>
<body  onload="initialize()" topmargin=0 bottommargin=0 leftmargin=0 rightmargin=0>

<form name="masterForm" action="control" method="post">
<input type="hidden" name="assoccode" id="assoccode" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>">
<div class="dashboardDV">
<table width="100%" class="dashboardTable">


			<tr>
		<td width="750" colspan="2" align="right" valign="top">
	 
			<div id="wid"></div>
			<div id="tid"></div>
		</td>
		</tr>		 
	<tr> 
		<td> 
<table width="100%">	

	<tr>		
	 	<table>
 			<tr>
 				<td valign="top" width="20%"> 
					<div id="openjobs">
						<%for(int i =0;i<jobs.size();i++){ %>
							<table>
								<tr>
									<td>
										<%=jobs.get(i).getScity() %>
									</td>
									<td>
										<%=jobs.get(i).getSadd1() %>
									</td>
									<td>
										<%=jobs.get(i).getDriverid() %>
									</td>
								</tr>
							</table>
						<%} %>
					</div>
				</td>			
 				<td valign="top" width="70%">
			  		<div id="map" style="width: 800px; height: 600px"></div>
					</div>
				</td>	
				<td valign="top" width="10%">
					<div id="drivers">
					<%for(int i =0;i<drivers.size();i++){ %>
					
							<table>
								<tr>
									<td>
										<%=drivers.get(i).getDriverid() %>
									</td>
									<td>
										<%=drivers.get(i).getStatus() %>
									</td>
									<td>
										<%=drivers.get(i).getProfile()%>
									</td>
								</tr>
							</table>
							<%} %>
							
					</div>
				</td> 			
 			</tr>
 			</table>
 	</tr>
</table>
</td>
</tr>
</table>	
</div>
</form>

</body>
</html>