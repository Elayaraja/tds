<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%><html>
<head>
<%
	
	ArrayList al_trip = (ArrayList)request.getAttribute("al_trip");
 	
%>
<link href="css/newtdsstyles.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>

<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
 function autocom(i)
 {
	 autocomplete("driverid"+i, "model-popup2", "driverid"+i, "autocomplete.view", "DRIVERID", null, "throbbing",null);
 }
</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form name="masterForm" action="control" method="post">
<input type="hidden" name="action" value="admin">
<input type="hidden" name="event" value="shortPath">
<input type="hidden" name="size" id="size" value="<%=al_trip==null?"0":al_trip.size() %>">
 

<table id="pagebox" width="100%">
	<tr>
		<td>
			<%if(al_trip!=null && !al_trip.isEmpty()){ %>
			<table id="bodypage">
				<tr>
					<td colspan="7" align="center">		
						<div id="title">
							<h2>Open Request</h2>
						</div>
					</td>		
				</tr>
				<tr bgcolor="grey">
					<td>Trip No</td><td>Rider Name</td><td>Phone No</td><td>Start Address</td><td>Trip Date</td><td>Driver id</td>
				</tr>	
				<%for(int i=0;i<al_trip.size();i++){ %>
				<% OpenRequestBO openRequestBO = (OpenRequestBO)al_trip.get(i); %>
					<tr>
						<td>
							<input type="hidden" name="stlatitude<%=i %>" value="<%=openRequestBO.getSlat() %>">	
							<input type="hidden" name="stlongitude<%=i %>" value="<%=openRequestBO.getSlong() %>">
							<input type="hidden" name="tripidh<%=i %>" id="tripidh<%=i %>" value="<%=openRequestBO.getTripid() %>" >
							<input type="checkbox" name="tripid<%=i %>" id="tripid<%=i %>" value="<%=openRequestBO.getTripid() %>" ><%=openRequestBO.getTripid() %>
						</td>
						<td><%=openRequestBO.getName() %></td>
						<td><%=openRequestBO.getPhone() %></td>
						<td><%=openRequestBO.getSadd1()+','+openRequestBO.getScity() %></td>
						<td><%=openRequestBO.getSdate() %></td>
						<td>
						 
							<input type="text" size="15" name="driverid<%=i%>" id="driverid<%=i%>" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)"  onblur="caldid(id,'drivername<%=i %>','dName<%=i %>')"  value="<%=openRequestBO.getDriverid() %>" class="form-autocomplete" onkeypress="autocom(<%=i %>)">
							<div id="model-popup2" class="autocomplete"></div>		
							<input type="hidden" name="dName<%=i %>" id="dName<%=i %>" value="<%=request.getParameter("dName"+i)==null?"":request.getParameter("dName"+i) %>">
							<div id="drivername<%=i %>"><%=request.getParameter("dName"+i)==null?"":request.getParameter("dName"+i) %></div> 								  
						</td>
			 		</tr>
				<%} %>
			</table>
			<%}  %>
		 
		</td>
	</tr>
	<%if(al_trip!=null){ %>
	<tr>
		<td colspan="7" align="center"><input type="submit" name="Button" value="Allocate" ></td>
	</tr>
	<%} %>
</table>
</form>
</body>
</html>