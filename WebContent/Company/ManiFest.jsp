<!DOCTYPE html>
<%@page import="com.tds.security.bean.TagSystemBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.FleetBO"%>

<html>
<head>
<%-- <%   AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
 %>
 --%> <%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
 	<%if(session.getAttribute("fleetList")!=null) {
	fleetList=(ArrayList)session.getAttribute("fleetList");
	} %>
<%	AdminRegistrationBO adminBO=(AdminRegistrationBO)session.getAttribute("user");%>

 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 <meta name="viewport" content = "width = device-width, initial-scale = 1.0, minimum-scale = 1.0, maximum-scale = 1.0, user-scalable = no" />
 <link rel="stylesheet" type="text/css" href="jqueryUI/layout-default-latest.css" />
  <link rel="stylesheet" type="text/css" href="css/Manifest.css" />
 
 
 <style type="text/css" title="currentStyle">
	@import "dataTable/css/demo_page.css";
	@import "dataTable/css/demo_table_jui.css";
	@import "dataTable/css/jquery-ui-1.8.4.custom.css";
	@import "dataTable/css/TableTools_JUI.css";
</style>
	
    <script src="jqueryUI/jquery.min.js" type="text/javascript"></script> 
   	<script src="https://maps.googleapis.com/maps/api/js?libraries=geometry,places&sensor=false&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>	
   	<script type="text/javascript" src="js/label.js"></script>
   	<script type="text/javascript" src="js/Manifest.js"></script>
 	<script type="text/javascript" src="jqueryUI/jquery-ui-latest.js"></script>
 	<script type="text/javascript" src="jqueryUI/jquery.layout-latest.js"></script>
	<script type="text/javascript" src="jqueryUI/jquery.layout.resizePaneAccordions-latest.js"></script>
    <script type="text/javascript" src="jqueryUI/themeswitchertool.js"></script> 
	<script type="text/javascript" src="jqueryUI/debug.js"></script> 
	<script type="text/javascript">
	function changeFleetHeader(fleet,i){
		  var xmlhttp=null;
			if (window.XMLHttpRequest){
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		 url='SystemSetupAjax?event=changeFleet&fleet='+fleet;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		//alert(text);
		$(".fleetbutton").css('backgroundColor', '');
		$("#fleet_"+i).css('backgroundColor', 'green');
	}

	
	</script>
  
  
	<script type="text/javascript" charset="utf-8"src="dataTable/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8"src="dataTable/js/ZeroClipboard.js"></script>
	<script type="text/javascript" charset="utf-8"src="dataTable/js/TableTools.js"></script>
	<script type="text/javascript" src="js/markerwithlabel.js"></script>
	<script type="text/javascript" src="js/Util.js"></script>


	<%String driverOrCab="";
	if(adminBO.getDispatchBasedOnVehicleOrDriver()==2){
					 driverOrCab = "Cab";	
				}else
					 driverOrCab="Driver";%>
					 <%ArrayList <TagSystemBean> cookieValues = (ArrayList<TagSystemBean>)request.getAttribute("CookieValues"); %>
					 
<title>TDS-SharedRide</title>
</head>
<body onload="jobDetails();sharedJobDetails();driverDetails();initializeMapHTML()">
	<div class="ui-layout-south ui-widget-content ui-state-error" style="display: none;height:10px;border-color: white;"></div>
	 <div class="ui-layout-center" style="display: none;"> 
	
	<div id="accordion1">
	<h3 align="center">Shared Ride Jobs</h3>
	<div>
	<p align="left">
	<%if(fleetList!=null&&fleetList.size()>0){%>
                         	<br>	
							  <%for(int i=0;i<fleetList.size();i++){ %>
									<td>
										<input type="button" name="fleet_<%=i%>" class="fleetbutton"  value ="<%=fleetList.get(i).getFleetName()%>" id ="fleet_<%=i%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBO.getAssociateCode())?"style='background-color:green'":""%> onclick="changeFleetHeader('<%=fleetList.get(i).getFleetNumber()%>','<%=i%>')" />
	 								</td>
							  <%} %>
  							<%} %>
	</p>
	 <p align="right">
	   
	 <input type="text" id="customSearch" name="customSearch" value="" placeholder="Search the values" onkeyup="customSearchOfaTable('sharedRide','customSearch','row')">
	           <input type="text" name="dateForFilter" id="dateForFilter" value=""  onchange="jobDetails" size="10"  > 
	         &nbsp;&nbsp;	<img src="images/Dashboard/loadingStatic.png" onclick="jobDetails()" id="imgReplace">&nbsp;&nbsp;
	 
	 
        <span id="timer" style="background-color: yellow;color: black;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;box-shadow:3px 3px 1px grey;padding:3px">00</span>        <input type="text" id="timeSec"  value="300" size="2" onblur="checkTime()">
    	
     	<a  onclick="showAllUnManifestedJobs()" style="cursor:pointer;text-decoration:none;background-color:lightgrey;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;box-shadow:3px 3px 1px grey;padding:3px;hover:red;color: #06C;">UnManifested</a>&nbsp;<a  style="cursor:pointer;text-decoration:none;;background-color:lightgrey;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;box-shadow:3px 3px 1px grey;padding:3px;hover:red;color:#06C;"  onclick="showAlljobs()">All</a>
     	<a href="control" style="text-decoration:none;background-color:lightgrey;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;box-shadow:3px 3px 1px grey;padding:3px;cursor:pointer;hover:red">Home</a><a href="DashBoard?event=dispatchValues" style="cursor:pointer;text-decoration:none;;background-color:lightgrey;-webkit-border-radius: 3px;-moz-border-radius: 3px;border-radius: 3px;box-shadow:3px 3px 1px grey;padding:3px;hover:red">Dashboard</a>
    </p>
    
    
   
    <div id="shJobs">
    <table  class="footable" data-filter="#filter" data-filter-text-only="true" id="sharedRide"  data-page-size="5" > 
    </table>
  	
    </div>
    	
	</div>
	<h3 align="center">Manifest Details</h3>
    <div style="width: 850px; min-height: 133px; height: 133px;overflow-y:scroll">
	    <div id="sharedSummary"  title="Manifest Details" style="display:none"></div>
	</div>
	</div></div>
	<div class="ui-layout-west" style="display: none;">
	<div id="accordion">
			<h3 align="center">Shared Ride Details</h3>
			<div>
			<p align="right">
         <input type="text" name="dateForFilter1" id="dateForFilter1" value=""   size="10"  > 
        <img  src="images/Dashboard/loadingStatic.png"  id="imgReplaceSh" onclick="sharedJobDetails()">&nbsp;&nbsp;
       </p>
			<div id="shDetails">
			<table id="sharedRideDetails" class="footable" data-filter="#filter1" data-filter-text-only="true" data-page-size="5">
		
			</table>
			</div>
			
			</div>
			<h3 align="center">Driver Details</h3>
			<div>
				<table id="driverDetailDisplay"></table>
			</div>
						<h3 align="center">Show Selected Items</h3>
						<div id="tripIDValues"><table id="orderPopUpSub"></table>
						<div style="margin-left:12px;background-color:gainsboro;width:619px;height:30px"><%=driverOrCab %> No :
						<input type="text" size="10" name="driverIdForShared" id="driverIdForShared" value=""><br/>
			<input type="button" class="appleButtons" name="createGroup" id="createGroup" value="Create Group" onclick="submitOfSharedJobs()">
			<input type="button" name="splitTrips" id="splitTrips" value="Split Trips" class="appleButtons" onclick="splitTrips()" style="display:none">
				
			<br/><input type="button" name="individualTrips" id="individualTrips" value="Individual Trips" class="appleButtons" onclick="createIndividualTrips()">
<br/> <input type="checkbox" name="indPay" id="indPay" value="">&nbsp;Individual&nbsp;Payment
<!-- 						<img src="images/Dashboard/loading_icon.gif">
 -->						</div>
						
						</div>
						<h3><center>Select Fields to show SharedRide Details</center></h3>
<div>  
<form name="cookieSubmit" id="cookieSubmit" action="DashBoard?event=submitCookies" method="post">
<input type="hidden" name="defaultLati" id="defaultLati" value="<%=adminBO.getDefaultLati()%>"/>
		<input type="hidden" name="defaultLongi" id="defaultLongi" value="<%=adminBO.getDefaultLogi() %>"/>
    <div id="cookieValueHidden">

<%if(cookieValues!=null&&cookieValues.size()>0){
	for(int i=0;i<cookieValues.size();i++){ if(cookieValues.get(i).getName().indexOf("GAC-D-SHJ-")!=-1){%>
<input type="hidden" name="<%=cookieValues.get(i).getName() %>" id="<%=cookieValues.get(i).getName() %>" value ="<%=cookieValues.get(i).getValue() %>">
<%}}}%></div>
<table><tr><td>
Phone No </td><td><input type="checkbox" name="showShPhone" id="showShPhone" value="" onclick="hideDetails()"></td></tr><tr><td>
Name </td><td><input type="checkbox" name="showShName" id="showShName" value="" onclick="hideDetails()"></td></tr><tr><td>
Pickup Address </td><td><input type="checkbox" name="showShPAddress" id="showShPAddress" value="" onclick="hideDetails()"></td></tr><tr><td>
Dropoff Address </td><td><input type="checkbox" name="showShDAddress" id="showShDAddress" value="" onclick="hideDetails()"></td></tr><tr><td>
Scity </td><td><input type="checkbox" name="showShScity" id="showShScity" value="" onclick="hideDetails()"></td></tr><tr><td>
Ecity </td><td><input type="checkbox" name="showShEcity" id="showShEcity" value="" onclick="hideDetails()"></td></tr><tr><td>
Date</td><td><input type="checkbox" name="showShDate" id="showShDate" value="" onclick="hideDetails()"></td></tr><tr><td>
VehicleNo</td><td><input type="checkbox" name="showShVno" id="showShVno" value="" onclick="hideDetails()"></td></tr><tr><td>
DriverId</td><td><input type="checkbox" name="showShDrId" id="showShDrId" value="" onclick="hideDetails()"></td></tr><tr><td>
Zones</td><td><input type="checkbox" name="showShStZone" id="showShStZone" value="" onclick="hideDetails()"></td></tr><tr><td>
SLandMark</td><td><input type="checkbox" name="showShSLandmark" id="showShSLandmark" value="" onclick="hideDetails()"></td></tr><tr><td>
ELandMark</td><td><input type="checkbox" name="showShELandmark" id="showShELandmark" value="" onclick="hideDetails()"></td></tr><tr><td>
Comments</td><td><input type="checkbox" name="showShComments" id="showShComments" value="" onclick="hideDetails()"></td></tr><tr><td> 


Ref No.</td><td><input type="checkbox" name="showShRef" id="showShRef" value="" onclick="hideDetails()"></td></tr><tr><td>
Ref No1.</td><td><input type="checkbox" name="showShRef1" id="showShRef1" value="" onclick="hideDetails()"></td></tr><tr><td>
Ref No2.</td><td><input type="checkbox" name="showShRef2" id="showShRef2" value="" onclick="hideDetails()"></td></tr><tr><td>
Ref No3.</td><td><input type="checkbox" name="showShRef3" id="showShRef3" value="" onclick="hideDetails()"></td></tr><tr><td>

Trip ID</td><td><input type="checkbox" name="showShTripId" id="showShTripId" value=""onclick="hideDetails()"></td></tr><tr><td>
CustomField</td><td><input type="checkbox" name="customField" id="customField" value="" onclick="hideDetails()"></td></tr> 

</table><input type="submit" name="submitCookies" id="submitCookies" value="SubmitValues"></form></div>
<h3><center>Driver  and Job Location</center></h3>
<div id="map_canvas"> </div>
</div>
	</div>
<input type="hidden" name="dispatchBasedOn" id="dispatchBasedOn" value="<%=adminBO.getDispatchBasedOnVehicleOrDriver() %>"/>
<input type="hidden" name="refShared" id="refShared" value="">
<input type="hidden" name="notAnumber" id="notAnumber" value="">
<input type="hidden" name="notAnumberB" id="notAnumberB" value="">
  <div id="autoPopulated1" style="display: none;width: 100%;">
	<jsp:include page="/jsp/AllocateSharedRide.jsp"/>
  </div>
	
</body>
</html> 