<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.cmp.bean.PenalityBean"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
 function autocom(i)
 {
	 autocomplete("DP_DRIVER_ID"+i, "model-popup2", "DP_DRIVER_ID"+i, "autocomplete.view", "DRIVERID", null, "throbbing",null);
 }
</script>

<script>


var temp=1;
function cal(){
	var i=Number(document.getElementById("size").value)+1;
	var table = document.getElementById("add123");
	  var rowCount = table.rows.length;
	  var row = table.insertRow(rowCount);
	  var cell = row.insertCell(0);
	  var cell1 = row.insertCell(1);
	  var cell2 = row.insertCell(2);
	  var cell3 = row.insertCell(3);
	  var cell4 = row.insertCell(4);
	  
	  var element = document.createElement("input");
	  element.type = "text";
	  element.name="DP_DRIVER_ID"+i;
	  element.id="DP_DRIVER_ID"+i;
	  element.size = '12';
	  element.className="form-autocomplete";
	  element.onkeypress = function(){
		  	autocom(i);
	  }
	  element.onblur = function(){
		  	caldid("DP_DRIVER_ID"+i,'popup2'+i+'','dName'+i+'');
	  }
	  element.onfocus = function(){
		  var assocode= <%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>;
		  appendAssocode(assocode,"DP_DRIVER_ID"+i);
	  }
	  
	   cell.appendChild(element);
	  element = document.createElement("div");
	  element.className="autocomplete";
	  element.id ="model-popup2"; 
	  cell.appendChild(element);
	  element = document.createElement("div");
	  element.id ="popup2"+i; 
	  cell.appendChild(element);
	  element = document.createElement("hidden");
	  element.id="dName"+i;
	  element.name="dName"+i;
	   cell.appendChild(element);
	  
	  
		var element1 = document.createElement("textarea");
		element1.name="DP_DESC"+i;
		element1.size="5";
		cell1.appendChild(element1);
		
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.style.textAlign = "right";
		element2.name="DP_AMOUNT"+i;
		element2.id="DP_AMOUNT"+i;
		element2.size = '5';
		element2.onblur=function() {insert(element2.id);checkpenaltybonus('btype'+i+'',element2.id);};
		cell2.appendChild(element2);
		
		var element3 = document.createElement("input");
	 	element3.type = "text";
		 element3.name="DP_PENALITY_DATE"+i;
		 element3.readOnly=true;
		element3.size = '7';
		element3.onfocus=new Function("showCalendarControl(this)");
		cell3.appendChild(element3);
		
		var element4 = document.createElement("select");
	 	 element4.name="btype"+i;
		 element4.id="btype"+i;
		 cell4.appendChild(element4);
		document.getElementById("btype"+i).options[0]=new Option("credit", "1", true, false);
		document.getElementById("btype"+i).options[1]=new Option("charge", "2", false, false);
		 
	 	 
		document.getElementById("size").value=i;
}
function removesel(){
var checkvalue="";
	for(var j=0;j<document.masterForm.checkedval.length;j++){
		if(document.masterForm.checkedval[j].checked)
		{
		checkvalue+=document.masterForm.checkedval[j].value+";";
		
		}
	}
	if(checkvalue == ""){
		if(document.getElementById("checkedval").checked)
			checkvalue+=document.getElementById("checkedval").value+";";
	}
	document.getElementById("checkedval1").value=checkvalue;
	
	
}
function checkpenaltybonus(typeid,valueid){
	var valued=document.getElementById(valueid).value;
	try{
		if(document.getElementById(typeid).value=='1' && (isNaN(valued.split(" ")[1]) || valued.split(" ")[1]<0)){
			alert("Bonus Should not minus");
			document.getElementById(valueid).value="$ 0.00";
		}else if(document.getElementById(typeid).value=='2'  && (valued.split(" ")[1]<0)){
			alert("Type penalty amt Need Not Mension");
			document.getElementById(valueid).value="$ "+valued.split("-")[1];
		}
	}catch(e){}
		
}
function delrow(){
	try {
	var table = document.getElementById("add123");
	var rowCount = table.rows.length;
	var temp=(Number(document.getElementById("size").value));
    if(Number(document.getElementById("size").value) != 0){
	if(Number(document.getElementById("size").value)+1<rowCount){
		rowCount--;
		table.deleteRow(rowCount);
	
	document.getElementById("size").value=Number(document.getElementById("size").value)-1;
	}
    }
	}catch(e){alert(e.message+"1");}
	
	
}
</script>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <%
 	ArrayList al_list = (ArrayList)request.getAttribute("al_list");	   
 	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
 	ArrayList al_view = (ArrayList)request.getAttribute("al_view");
 	String pageFor = request.getAttribute("pagefor") == null?"Submit":request.getAttribute("pagefor").toString();	
 %> 
 
 
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
        
        
</head>
<body > 
	<form name="masterForm" action="control" method="get">
	<input type="hidden" name="action" value="registration">
	<input type="hidden" name="event" value="driverPenalityMaster">
	<input type="hidden" name="pagefor" value="<%=pageFor %>">
	<input type="hidden" id="size" name="size" value="<%=al_view==null?"0":(""+(al_view.size()-1)) %>">
	 	
	<table id="pagebox">	
		<tr>
		<td>  
			<table id="bodypage">	
				<tr>
					<td colspan="7" align="center">		
						<div id="title">
							<h2>Driver Credit/Charge Entry</h2>
						</div>
					</td>		
				</tr>
				<%
									
					StringBuffer error = (StringBuffer) request.getAttribute("error");
					
					String sucess= (String)request.getAttribute("sucess");
					String remove= (String)request.getAttribute("remove");
				%>
				<tr>
					<td colspan="7">
						<div id="errorpage">
							<%= (error !=null && error.length()>0)?"You must fullfilled the following error<br>"+error :"" %>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<%= (sucess !=null && sucess.length()>0)?""+sucess :"" %>
					</td>
				</tr>
				
				<tr> 
					<td>
						<table id="add123" border="1" >
						<tr><td>Driver ID </td><td>Description</td><td>Amount</td><td>Effective Date</td><td>Type</td></tr>
						<%if(al_view == null) { %>
					 
								<tr>
									<td>
										 <input type="text" id="DP_DRIVER_ID0" name="DP_DRIVER_ID0" size="12" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername0','dName0')"  autocomplete="off" value="" class="form-autocomplete">
										 <ajax:autocomplete
						  					fieldId="DP_DRIVER_ID0"
						  					popupId="model-popup1"
						  					targetId="DP_DRIVER_ID0"
						  					baseUrl="autocomplete.view"
						  					paramName="DRIVERID"
						  					className="autocomplete"
						  					progressStyle="throbbing" />
						  					<input type="hidden" name="dName0" id='dName0' value="<%=request.getParameter("dName0")==null?"":request.getParameter("dName0") %>">		
							 	  <div id="drivername0"><%=request.getParameter("dName0")==null?"":request.getParameter("dName0") %></div> 
									</td>
									 
									<td>
										<input type="hidden" size="20" name="DP_TRANS_ID0" value="">
										<textarea name="DP_DESC0"></textarea>
									</td>
									 
									<td>
										<input type="text" name="DP_AMOUNT0" id="DP_AMOUNT0" style="text-align: right" size="5" onblur = "insert(id);checkpenaltybonus('selecttype',id)" >
									</td>
									<td>
										 <input type="text" name="DP_PENALITY_DATE0" size="10" readonly="readonly" value=""  onfocus="showCalendarControl(this);">
									</td>
									 <td>
										<select name="btype0"  id='selecttype'>
										   	<option value="1">credit</option>
											<option value="2">charge</option>
										 <!-- <option value="3">Miscellaneous</option> -->
										</select>
									</td>
								</tr>
							 
						<%} else { %> 
								<%
									for(int i=0;i<al_view.size();i++) {
									PenalityBean pBean = (PenalityBean)al_view.get(i);
								%>
							<tr <%=pBean.getBtype().equalsIgnoreCase("2")?"background='gyan'":""%>>
									<td>
										 <input type="text"  name="DP_DRIVER_ID<%=i%>" size="12" id="DP_DRIVER_ID<%=i%>" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername<%=i %>','dName<%=i %>')"  value="<%=pBean.getB_driver_id() %>" class="form-autocomplete" onkeypress="autocom(<%=i %>)" autocomplete="off" >
										 <input type="hidden" name="dName<%=i %>"  id="dName<%=i %>" value="<%=request.getParameter("dName"+i)==null?"":request.getParameter("dName"+i) %>">
											<div id="drivername<%=i %>"><%=request.getParameter("dName"+i)==null?"":request.getParameter("dName"+i) %></div>
										  <div id="model-popup2" class="autocomplete"></div>										  
									</td>
									<td>
										<input type="hidden" size="20" name="DP_TRANS_ID<%=i %>" value="<%=pBean.getB_trans() %>">
										<textarea name="DP_DESC<%=i %>"><%=pBean.getB_desc()%></textarea>
									</td>
									 
									<td>
										<input type="text" style="text-align: right" name="DP_AMOUNT<%=i %>" id="DP_AMOUNT<%=i %>" size="5" onblur = "insert(id);checkpenaltybonus('btype<%=i %>',id);"  value="$ <%=pBean.getB_amount() %>" >
										
									</td>
									<td>
										 <input type="text"  size="10" name="DP_PENALITY_DATE<%=i %>" size="10" readonly="readonly" value="<%=pBean.getB_p_date() %>"  onfocus="showCalendarControl(this);">
									</td>
									<td>
										<select name="btype<%=i %>" id="btype<%=i %>">
										   <option value="1" <%=pBean.getBtype().equalsIgnoreCase("1")?"selected":"" %>>credit</option>
											<option value="2" <%=pBean.getBtype().equalsIgnoreCase("2")?"selected":"" %>>charge</option>
											<%--  <option value="3" <%=pBean.getBtype().equalsIgnoreCase("3")?"selected":"" %>>Miscellaneous</option>--%>
										</select>
									</td>
 								</tr>
								<%} %>
						<%} %>
						
						</table>
					</td>
				</tr>
				 <tr align="center">
					<TD colspan="3" align="center">
					<input type="button" name="Button"  value="Add" onclick="cal()" >				 
					<input type="button" name="Button"  value="Delete" onclick="delrow();" >
						<%if(pageFor.equalsIgnoreCase("Submit")) {%>
 								<input type="submit" name="Button"  value="Submit"  >&nbsp;&nbsp;&nbsp;
 						<%} else { %>	 
 								<input type="submit" name="Button"  value="Submit"  >&nbsp;&nbsp;&nbsp;
 						<%} %>
							
					</TD>
					
				</tr>
				<tr></tr>
				<tr>
					<td align="center">
					<div id="ee">
							<%=(remove !=null && remove.length()>0)?remove :"" %>
						</div>
					 	<table border=1>	
					 		<%if(al_list!=null){ %>
							<tr><td>Driver Name</td><td>Description</td><td>Amount</td><td>Credit/Charge Date</td><td>Type</td><td>remove</td></tr>
							
								<% for(int i=0;i<al_list.size();i++) { 
									PenalityBean penalityBean = (PenalityBean)al_list.get(i);								
								%>
									<tr>
										<td style="text-align: center">
											<% for(int j=0 ;j<al_driver.size();j=j+2){ %>
												<%=(al_driver.get(j).toString().equalsIgnoreCase(penalityBean.getB_driver_id()))?al_driver.get(j+1):""%>
											<%} %>
										</td>
										<td style="text-align: center"><%=penalityBean.getB_desc()%></td>
										<td style="text-align: right">$<%=penalityBean.getB_amount() %></td>
										<td style="text-align: center"><%=penalityBean.getB_p_date() %></td>
										<td><%=penalityBean.getBtype().equalsIgnoreCase("1")?"Credit":penalityBean.getBtype().equalsIgnoreCase("2")?"Charge":"Miscellaneous"%></td>
										 <td>
										 <input type="checkbox" id='checkedval' name="checkedval" value="<%=penalityBean.getB_trans()%>"  >
										 
										 </td>
									</tr>
								<%} %>
								<tr><td colspan="5" align="center">
								<input type="hidden" id='checkedval1' name="checkedval1" value="" >
							 <input type="submit" name="Button" value="Remove Selected" onclick="removesel();">
					       </td></tr>
							<%} %>
							
						</table>
					</td>
				</tr>	
				
			</table>
		</td>
		</tr>
	</table>
	</form>
</body>
</html>
