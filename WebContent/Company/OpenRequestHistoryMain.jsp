<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.FleetBO"%>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link rel="stylesheet" href="css/ui.all.css" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
	<script src="js/CalendarControl.js" language="javascript"></script>
	<script type="text/javascript"
		src="js/OpenRequestDetails.js?vNo=<%=TDSConstants.versionNo%>"></script>



	<script type="text/javascript">
	
	function showCommentsForHistory(row){
		 var img=document.getElementById("commentButton_"+row).value;
			if(img.indexOf("Show Comments")!= -1){
				document.getElementById("commentButton_"+row).value="Hide Comments";
				$("#comments_"+row).show("slow");
			}else{
				document.getElementById("commentButton_"+row).value="Show Comments";
				$("#comments_"+row).hide("slow");
			}

	}
	
	 $(document).ready(function(){
		 currentDate();
		}); 
	
	 function currentDate() {
			var curdate = new Date();
			var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
			var cDateFrom = (curdate.getDate()-7) >= 10 ? curdate.getDate()-7 : "0"+curdate.getDate()-7;
			var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
			var cYear = curdate.getFullYear();
			var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
			var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
			var hrsmin = hors+""+min;  
			document.getElementById("fromDate").value =cMonth+"/"+(cDateFrom>=10?cDateFrom:"0"+cDateFrom)+"/"+cYear;
			document.getElementById("toDate").value =cMonth+"/"+cDate+"/"+cYear;
			 document.getElementById("value").value="";

			//document.masterForm.shrs.value  = hrsmin; 
		 }
		

	
	
	</script>

</head>
<body>
	<%
		ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>();
	%>

	<%
		if(session.getAttribute("fleetList")!=null) {
			
		fleetList=(ArrayList)session.getAttribute("fleetList");
		
		}
	%>
	<%
		ArrayList<OpenRequestBO> openReqHistory = (ArrayList<OpenRequestBO>) request.getAttribute("openRequestHistory");
	%>
	<%
		AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
	%>
	<input type="hidden" name="action" value="openrequest" />
	<input type="hidden" name="event" value="openRequestHistory" />
	<input type="hidden" name="screenValue" id="screenValue" value="1" />		
	<input type="hidden" name="module" id="module"
		value="<%=request.getParameter("module") == null ? "" : request.getParameter("module")%>" />
	<table width="100%" style="background-color: #EFEFFB" cellspacing="1"
		cellpadding="0" class="driverChargTab">
		<tr class="headerRowORH" style="cursor: move">
			<td colspan='7' align='center'
				style="background-color: #CEECF5; size: 500px"><h5>
					<center>
						Jobs History&nbsp;<a style="margin-left: 60px" href="#"><img
							alt="" src="images/Dashboard/close.png"
							onclick="removeORHistory();hideCalendarControl()">
						</a>
					</center>
					<a href="#">
				</h5>
		</tr>
		<tr>
			</td>

			<td><font color="black">Search:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<t></t>
			</font> <input type="text" name="value" id="value" size="10" value=""
				onfocus="hideCalendarControl();" />
			</td>


			<td><font color="black">From Date:</font> <input type="text"
				name="fromDate" id="fromDate" size="10" value=""
				onfocus="hideCalendarControl();showCalendarControl(this);" /></td>


			<td><font color="black">To
					Date:&nbsp;&nbsp;&nbsp;&nbsp;</font> <input type="text" name="toDate"
				id="toDate" size="10" value=""
				onfocus="hideCalendarControl();showCalendarControl(this);" /></td>

			<td><input type="button" name="search" id="search" size="10"
				value="Search" onfocus="hideCalendarControl();"
				onclick="ORHistory()" /></td>

		</tr>
		<tr>
			<td>

				<div id="reDispatchMessage" style="display: none;"></div></td>
		</tr>

		<%
			if (openReqHistory != null && openReqHistory.size() > 0) {
		%>
		<table width="100%" id="ORHistoryResult"
			style="width: 1185px; background-color: #000000" cellspacing="1"
			cellpadding="0" class="driverChargTab">

			<tr>
				<td>
					<div class='ScrollFormGrid'>
						<table width="1155" style="background-color: #000000">
							<tr>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">TripId</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Time</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">DriverId</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Cab</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Phone</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Name</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Start Address</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">End Address</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Trip Status</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Pmt</font>
								</td>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Amount</font>
								</td>
								<%
									if(fleetList!=null && fleetList.size()>0){
								%>
								<td align="center" style="background-color: #EFEFFB"><font
									size="2" color="black">Fleet Name</font>
								</td>

								<%
									}
								%>
							</tr>

							<%
								boolean colorLightGreen = true;
																	String colorPattern;
							%>
							<%
								for (int i = 0; i < openReqHistory.size(); i++) {
																		colorLightGreen = !colorLightGreen;
																		if (colorLightGreen) {
																			colorPattern = "style=\"background-color:#EFEFFB;white-space: nowrap;\"";
																		} else {
																			colorPattern = "style=\"background-color:#EFEFFB;white-space: nowrap;\"";
																		}
							%>
							<tr>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=openReqHistory.get(i).getTripid()==null?"":openReqHistory.get(i).getTripid()%></font>
									<input type="hidden" name="tripId" id="tripId<%=i%>"
									value=<%=openReqHistory.get(i).getTripid()%>></input></td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=openReqHistory.get(i).getSdate()==null?"":openReqHistory.get(i).getSdate()%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=openReqHistory.get(i).getDriverid()==null?"":openReqHistory.get(i).getDriverid()%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=openReqHistory.get(i).getVehicleNo()==null?"":openReqHistory.get(i).getVehicleNo()%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=openReqHistory.get(i).getPhone()==null?"":openReqHistory.get(i).getPhone()%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=openReqHistory.get(i).getName()==null?"":openReqHistory.get(i).getName()%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"
									title="<%=openReqHistory.get(i).getSadd1()==null?"":openReqHistory.get(i).getSadd1()%> <%=openReqHistory.get(i).getSadd2()==null?"":openReqHistory.get(i).getSadd2()%> <%=openReqHistory.get(i).getScity()==null?"":openReqHistory.get(i).getScity()%> <%=openReqHistory.get(i).getSstate()==null?"":openReqHistory.get(i).getSstate()%> <%=openReqHistory.get(i).getSzip()==null?"":openReqHistory.get(i).getSzip()%>"><%=openReqHistory.get(i).getSadd1()==null?"":(openReqHistory.get(i).getSadd1().length()>25?openReqHistory.get(i).getSadd1().substring(0,25):openReqHistory.get(i).getSadd1())%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"
									title="<%=openReqHistory.get(i).getEadd1()==null?"":openReqHistory.get(i).getEadd1()%> <%=openReqHistory.get(i).getEadd2()==null?"":openReqHistory.get(i).getEadd2()%> <%=openReqHistory.get(i).getEcity()==null?"":openReqHistory.get(i).getEcity()%> <%=openReqHistory.get(i).getEstate()==null?"":openReqHistory.get(i).getEstate()%> <%=openReqHistory.get(i).getEzip()==null?"":openReqHistory.get(i).getEzip()%>"><%=openReqHistory.get(i).getEadd1()==null?"":(openReqHistory.get(i).getEadd1().length()>25?openReqHistory.get(i).getEadd1().substring(0,25):openReqHistory.get(i).getEadd1())%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=openReqHistory.get(i).getTripStatus()==null?"":openReqHistory.get(i).getTripStatus()%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=openReqHistory.get(i).getPaytype()==null?"":openReqHistory.get(i).getPaytype()%></font>
								</td>
								<td align="center" <%=colorPattern%>><font size="2"
									color="black"><%=adminBo.getCurrencyPrefix()==1?"$":adminBo.getCurrencyPrefix()==2?"&euro;":adminBo.getCurrencyPrefix()==3?"&pound;":"&#x20B9;"%>
										<%=openReqHistory.get(i).getAmt()==null?"":((openReqHistory.get(i).getAmt()+"").contains(".")?openReqHistory.get(i).getAmt():openReqHistory.get(i).getAmt()+".00")%></font>
								</td>
								<%
									if(fleetList!=null && fleetList.size()>0){
								%>
								<td align="center" <%=colorPattern%>>
									<%
										for(int j=0;j<fleetList.size();j++){
									%> <font size="2"
									color="black"><%=fleetList.get(j).getFleetNumber().equals(openReqHistory.get(i).getAssociateCode())?fleetList.get(j).getFleetName():""%>
								</font> <%
 	}
 %>
								</td>
								<%
									}
								%>
								<td align="center" colspan="3" <%=colorPattern%>><input
									type="button" id="detailShow_<%=i%>" value="Details"
									onclick="openORDetails(<%=openReqHistory.get(i).getTripid()%>)" />
								</td>
								<td align="center" colspan="3" <%=colorPattern%>><font
									size="2" color="black"> <%
 	if((openReqHistory.get(i).getComments()!=null && !openReqHistory.get(i).getComments().equals("")) || (openReqHistory.get(i).getSpecialIns()!=null && !openReqHistory.get(i).getSpecialIns().equals("")) ){
 %>
										<input type="button" id="commentButton_<%=i%>"
										value="Show Comments" onclick="showCommentsForHistory(<%=i%>)" />
										<%
											}else{
										%> NO comments <%
											}
										%>
								</font>
								</td>

								<td align="center" <%=colorPattern%>><input type="button"
									name="button" id="reDispatchButton_<%=i%>" class="lft"
									value="ReDispatch" onclick="reDispatch(<%=i%>)" /></td>
							</tr>
							<tr id="comments_<%=i%>" style="display: none;">
								<td colspan="17"
									style="background-color: lightgray; white-space: nowrap;"><font
									size="3" color="black" title="Dispatch and Cab Comments">
										DC:<%=openReqHistory.get(i).getComments()==null?"":openReqHistory.get(i).getComments()%>
										CC:<%=openReqHistory.get(i).getSpecialIns()==null?"":openReqHistory.get(i).getSpecialIns()%>
								</font>
								</td>
							</tr>
							<%
								}
							}
							%>
						</table>
					</div>
				</td>



			</tr>
		</table>
</body>
</html>
