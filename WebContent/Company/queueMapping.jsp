<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment" %>
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<%DriverDisbursment driverid=(DriverDisbursment)request.getAttribute("disbo") == null ? new DriverDisbursment():(DriverDisbursment)request.getAttribute("disbo") ;
	

%>
<script type="text/javascript"> 

 function addRow(){ 
	 
		var i=Number(document.getElementById("size").value)+1;
		var table = document.getElementById("tabid");
		  var rowCount = table.rows.length;
		  var row = table.insertRow(rowCount);
		  var cell = row.insertCell(0);
		  var cell1 = row.insertCell(1); 
	 
		  var element = document.createElement("input");
		  element.type = "text";
		  element.name="longitude"+i;
		  element.id="longitude"+i;
		  element.size = '12'; 
		  cell.appendChild(element); 
		  
		var element1 = document.createElement("input");
		element1.type = "text";
		element1.name="latitude"+i;
		element1.id="latitude"+i;
		element1.size = '12';
		cell1.appendChild(element1); 
			 
		document.getElementById("size").value=i;
		//alert(document.getElementById("size").value);
	   
	    
	  }

</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Queue Mapping</title>
</head>

<body>
<% 
	ArrayList al_view = (ArrayList)request.getAttribute("al_list"); 
%>
<form  name="masterForm"  action="control" method="post">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="cmpyQueueMap"/>
<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<input type="hidden" id="size" name="size" value="<%=al_view==null?"0":(""+(al_view.size()-1)) %>"/>
<input type="hidden" name="operation" value="2"/>



 	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div class="rightCol">
<div class="rightColIn">
<h1>Queue Mapping</h1>
<% String error = (String)request.getAttribute("error");  %>

<div id="errorpage" >
<%=(error  !=null && error.length()>0)?""+error :"" %> 
</div>
                        		 
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
                    <td>
                    <table id="bodypage" width="440"    align="center" >
                     <tr> 
                     	<td class="firstCol" >Queue Name</td>
                    	<td>  <input type="text"       name="queuename"  id="queuename"     value="<%=request.getAttribute("queueName")==null?"":request.getAttribute("queueName") %>"     /> </td>
                    	<td class="firstCol" >Description</td>
                    	<td>  <input type="text"         name="description"  id="description"  value="<%=request.getAttribute("description")==null?"":request.getAttribute("description") %>"  /> </td>
                     </tr>
                     <tr> 
                     	<td class="firstCol" >Order</td>
                    	<td>  <input type="text"       name="order"  id="order"    value="<%=request.getAttribute("order")==null?"":request.getAttribute("order") %>" /> </td>
                    	<td class="firstCol" >DateTime</td>
                    	<td>  <input type="text"          name="datetime"  id="datetime"    value="<%=request.getAttribute("date")==null?"":request.getAttribute("date") %>"  /> </td>
                     </tr>
                     <tr> 
                     	<td class="firstCol" >Start Time</td>
                    	<td>  <input type="text"       name="stTime"  id="stTime"    value="<%=request.getAttribute("Sttime")==null?"":request.getAttribute("Sttime") %>" /> </td>
                    	<td class="firstCol" >End Time</td>
                    	<td>  <input type="text"          name="edTime"  id="edTime"    value="<%=request.getAttribute("endtime")==null?"":request.getAttribute("endtime") %>"  /> </td>
                     </tr>
                     
                     <tr> 
                     	<td class="firstCol" >Day of Week</td>
                    	<td>  <input type="text"       name="dayweek"  id="dayweek"    value="<%=request.getAttribute("dayweek")==null?"":request.getAttribute("dayweek") %>" /> </td>
                    	<td class="firstCol" >Dispatch Pipe</td>
                    	<td>
                    	 <select  name="dppipe"    >Select
                    	<option value="SD" >Shortest Dispatch</option>
                    	<option value="Q"  >Queue</option>
                    	<option value="BR"  >Broadcast</option>
                    	<option value="DD"  >Demand Dispatch</option> 
                    	  </select>  
                    	</td>
                     </tr>
                    
                     
                     
                     <tr> 
                     	<td class="firstCol" >Sms Time</td>
                    	<td>  <input type="text"      name="smstime"  id="smstime"   value="<%=request.getAttribute("smsTime")==null?"":request.getAttribute("smsTime") %>"    /> </td>
                    	<td class="firstCol" >Login Switch</td>
                    	<td>   <select  name="loginSwitch"    >Select
                    	<option value="E" >Allow Every One</option>
                    	<option value="R"  >Allow Selected Only</option> 
                    	
                    	  </select>  </td>
                     </tr>
                      <tr> 
                     	<td class="firstCol" >BroadCast Request Before  Mns</td>
                    	<td>  <input type="text"       name="brdcstrqbre"  id="brdcstrqbre"    value="<%=request.getAttribute("brdreqbefr")==null?"":request.getAttribute("brdreqbefr") %>" />(Min) </td>
                    	 
                     </tr>
                     
                     <tr>
                     <td>
                 
                     </td>
                     </tr>
                     </table>
                         <table id="tabid"   width="440">
                     <tbody> 
                     
                     <tr  class="firstCol">
                     	<td>Longitude</td>
                     	<td>Latitude</td> 
                     </tr>
                     <%if(al_view == null) { %>
                     <tr>
                     
                      
                     	<td><input type="text"   name="longitude0"  id="longitude"/></td>
                     	<td><input type="text"   name="latitude0"  id="latitude"/></td>
                     	</tr>
                     	 
                     <%} else { %>
                     <%
											for(int i=0;i<al_view.size();i++) {
 										%>
										<tr>
                     <td><input type="text"   name="longitude"  id="longitude"/></td>
                     	<td><input type="text"   name="latitude"  id="latitude"/></td>
                     	
                     </tr>
                     <%} }%>  
                      </tbody>
                     </table>
                  
                     
                     
                     						<table   width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab"  align="center"    >
								<tr    align="center">
							 <td   colspan="4"  align="center" >
                                                   <input type="button" value="Add One More" onclick="addRow()"/>
                                              
                                              
                                                   <%if(request.getAttribute("buttonflag")=="submit") {%>
 													<input type="submit"  name="button" class="lft"   value="Submit" />
 													<%} else { %>	 
 													<input type="submit"  name="button" class="lft"  value="Update" />
 													<%} %>
                                        
                                							
									
							 </td>
							</tr>
							
							</table> 
                 
                    </td>
                    </tr>
                    </table>
                    

</div>
</div>

  
              
            	
                <div class="clrBth"></div>
       
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>