<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.security.bean.TagSystemBean"%>
<%@page import="com.tds.cmp.bean.ZoneTableBeanSP"%>
<%@page import="com.lowagie.text.Document"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Operator Dashboard</title>
<%@page import="com.tds.dao.AdministrationDAO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
 <%@page import="com.tds.tdsBO.FleetBO"%>
<%@page import="com.tds.tdsBO.DriverLocationBO"%>
<%@page import="com.tds.process.QueueProcess"%>
<%@page import="java.util.ArrayList"%>
<%@page import=" java.util.Timer"%>
<%@page import="com.common.util.TDSProperties"%>
<%ArrayList<ZoneTableBeanSP> allZones=(ArrayList<ZoneTableBeanSP>) request.getAttribute("zones");%> 
<%ArrayList Latitudes= (ArrayList) request.getAttribute("latitude"); %>
<%ArrayList Longitudes= (ArrayList) request.getAttribute("longitude");%> 
<%ArrayList centerLat= (ArrayList) request.getAttribute("centreLatitude");%> 
<%ArrayList centerLon= (ArrayList) request.getAttribute("centreLongitude");%> 
<%String defaultZone =(String)request.getAttribute("DefaultZone"); %>
<%ArrayList<TagSystemBean> cookieValues = (ArrayList<TagSystemBean>)request.getAttribute("CookieValues"); %>
 <%ArrayList<OpenRequestBO> jobs = (ArrayList<OpenRequestBO>)request.getAttribute("Jobs");		
   ArrayList<DriverLocationBO> drivers = (ArrayList<DriverLocationBO>)request.getAttribute("Drivers");
   AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
   String driverOrCab="";
  if(adminBO.getDispatchBasedOnVehicleOrDriver()==1){
     driverOrCab="Driver";}else{driverOrCab="Cab";
   }%>
   
<%ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getMasterAssociateCode() +"Zones"));
 %>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link rel="stylesheet" type="text/css" href="jqueryUI/layout-default-latest.css" />
 
 <style type="text/css" title="currentStyle">
	@import "dataTable/css/demo_page.css";
	@import "dataTable/css/demo_table_jui.css";
	@import "dataTable/css/jquery-ui-1.8.4.custom.css";
	@import "dataTable/css/TableTools_JUI.css";
	
</style>
<script type="text/javascript" src="Ajax/SystemUtilAjax.js?vNo=<%=TDSConstants.versionNo %>"></script>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="DashboardAjax/DispatchGoogle.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="DashboardAjax/DispatchCommon.js?vNo=<%=TDSConstants.versionNo%>"></script>
<script type="text/javascript" src="DashboardAjax/DispatchCommon1.js?vNo=<%=TDSConstants.versionNo%>"></script>
<script type="text/javascript" src="DashboardAjax/dragtable.js"></script>

<script type="text/javascript" src="js/MapForORDetails.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="js/OpenRequestDetails.js?vNo=<%=TDSConstants.versionNo %>""></script>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

<script src='<%=TDSProperties.getValue("googleMapV3_"+((AdminRegistrationBO)session.getAttribute("user")).getDefaultLanguage())%>'></script>

<%-- <%if(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode().equalsIgnoreCase("103")) {%>
	<script src='<%=TDSProperties.getValue("googleMapV3_ar")%>'></script>
<%}else{ %>
	<script src='<%=TDSProperties.getValue("googleMapV3")%>'></script>
<%} %> --%>

<script type="text/javascript" src="js/label.js"></script>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<link href="css/newtdsstyles.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>
<link type="text/css" rel="stylesheet"href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css"media="screen"></link>
<script type="text/javascript"src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" src="Ajax/DashBoard.js?vNo=<%=TDSConstants.versionNo %>"></script>
<link rel="stylesheet" href="css/DashboardNew.css?vNo=<%=TDSConstants.versionNo %>">
<script type="text/javascript" src="Ajax/Common.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script src="js/jquery.contextMenu.js"></script>
<link rel="stylesheet" href="css/jquery.contextMenu.css"></link>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="js/gmap3.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery.bpopup-0.7.0.min.js"></script>
<script src="js/code.js"></script>
<script type="text/javascript" src="js/colorpicker.js"></script>
<script type="text/javascript" src="js/eye.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/ContextMenu.js"></script>
<link rel="stylesheet" href="css/colorpicker.css" type="text/css" />
<script type="text/javascript" charset="utf-8"src="dataTable/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8"src="dataTable/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8"src="dataTable/js/TableTools.js"></script>
<script type="text/javascript" src="js/Util.js"></script>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>
	

<style type="text/css">

#top-header {
    width: 100%;
    height: 62.4px;
    background-color: gray; 
 }
.more{
    display:inline-block;
    width: 50px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
     color: rgb(119, 119, 131);;
}
.more:hover{
    white-space:initial;
    cursor:pointer;
    width:100%;
    word-wrap:break-word;
     color: blue;
} 
.ccButtons{
	-moz-border-radius:8px 8px 8px 8px;
	background-color: #FDD017;
	color: #342826;
	width: 60px;
	height: 40px;
	font-size: medium;
	position:fixed;
}
.ccButtonsAct{
	-moz-border-radius:8px 8px 8px 8px;
	background-color: #008000;
	color: #FFFFFF;	
	width: 60px;
	height: 40px;
	font-size: medium;
	position:fixed;
}
.ccButtonsPkd{
	-moz-border-radius:8px 8px 8px 8px;
	background-color: #0000FF;
	color: #FFFFFF;
	width: 60px;
	height: 40px;
	font-size: medium;
	position:fixed;
}
 
</style>

<script type = "text/javascript">
$(document).ready ( function(){
	$("#footerDiv").slideToggle("slow");
	if($("#footerDiv").is(':visible')){
		checkMessages();
		 }
       checkMobmessages();
	onloadDash();getMeterTypesForDash();
	$("#footerDiv").hide("slow");
    $('.dashboard').css('height', $(window).height()/1.17+'px');
/*         $('#outer-div').css('height', $(window).height()+'px'); */
    $('#map').css('height', $(window).height()/1.12+'px');
//$('#sendSms').css('height',$(window).height)/3+'px');
    //$('#top-header').css('height', $(window).height()/30+'px'); 
   // $('#showHover').css('margin-left:', $(window).height()/9+'px');
    $('#showHover').css('margin-top:', $(window).height()/10+'px'); 
    	$('#popUpAddress').css('margin-top:', $(window).height()/17+'px');
   // $('#popUpAddress').css('margin-left:', $(window).height()/12+'px'); top: 252px;
    	//$('#bullHorn').css('margin-top:', -$(window).height()/4+'px');
    
    		
}
);


document.writeln("<div class=\"jqmWindow\" id = \"pbar\" ></div>");

var percent = 10;   // adjust starting value to suit
var timePeriod = 100;  // adjust milliseconds to suit

function getBar() {
var retBar = '';
for (i = 0; i < percent; i++) {
retBar += "";
}
return retBar;
}

function progressBar() {
if (percent < 100) {
percent = percent + 1;
$("#pbar").jqm();
$("#pbar").jqmShow();
document.getElementById("pbar").innerHTML = "<center><h6><font color=\"blue\">WelCome to Operator Dashboard</h6></b></center></br>Please Wait...<font color=\"lime\"size=\"2px\"> Loading : <img src=\"images/Dashboard/progress2.gif\">"+ "<font size=\"1px\" ><b><a>" + getBar()+"</a></b></font></font></font>";
window.status = "Loading : " + percent + "%" + " " + getBar();
google.maps.event.addListener(map, 'idle', function() { 
	percent=100;
	setTimeout ("progressBar()", timePeriod); 
	
	} );
setTimeout ("progressBar()", timePeriod); 

}
else {
$("#pbar").jqmHide();
document.getElementById("pbar").innerHTML = "";
window.status = "Please Wait the Page was Loading...";
document.body.style.display = "";
}
}
progressBar();
function showNotificationsms(){
	checkMobmessages();
}
function showPopUp(sender,msg){
	if(document.getElementById("test")!=null){
		document.getElementById("test").innerHTML="";
	}
    var popup = document.createElement('div');
    popup.className = 'popup';
    popup.id = 'test';
    var cancel = document.createElement('div');
    cancel.className = 'cancel';
    cancel.innerHTML = 'close';
    cancel.onclick = function (e) { popup.parentNode.removeChild(popup) };
    var message = document.createElement('span');
    message.innerHTML ="<a style='color:green'><div style='max-height:100px;max-width:200px;overflow:auto' >"+msg+"</div></a>sender : <a style='color:red'>"+sender+"</a> ";
    var reply = document.createElement('span');
    reply.innerHTML ="<textarea align='center'  id='msgValue' placeholder='Reply to this Message' onFocus='clearStatus()' ></textarea><input type='button' value='Send' align='center' onclick='sendReplySms()'/>";

    var driverValue = document.createElement("input");
    driverValue.type="hidden";
    driverValue.id ="driver_Reply";
    driverValue.value =sender;
    
    var resp = document.createElement("a");
    resp.id="replyStatus";
    resp.innerHTMl ="";
    
    popup.appendChild(message);
    //var areEqual = sender.toUpperCase() === sender.toUpperCase();
   // if(sender!="Operator"){
    	//alert("Baskar checking Operator value:"+sender);
    	popup.appendChild(reply);
    //}
    popup.appendChild(driverValue);
    popup.appendChild(resp);
    popup.appendChild(cancel);


    document.body.appendChild(popup);
    }
    function clearStatus(){
    	document.getElementById('replyStatus').innerHTML="";
    }
function increaseSpeed(){
	document.getElementById("showNotiMsg").scrollAmount = parseInt(document.getElementById("showNotiMsg").scrollAmount) +1;
}
function decreaseSpeed(){
	if(document.getElementById("showNotiMsg").scrollAmount>0){
		document.getElementById("showNotiMsg").scrollAmount = parseInt(document.getElementById("showNotiMsg").scrollAmount) -1;
	}
}
if($('#sendmsg').is(':visible')){
	if(screenValue=="10"){	
		$("#sendmsg" ).dialog( "close" );
		$("#sendmsg").hide();
	} else {
		$("#sendmsg").jqmHide();
	}
}


function messageforDrivers(){
	$("#sendmsg").jqm();
	$("#sendmsg").jqmShow();
}

function appendJobMessage(){
	//message1 jobMessage
	document.getElementById("message1").value = document.getElementById("jobMessage").value;
}
</script>


</head>
<body id="dashboardBody" onload="getAllValuesForDashboard('11111')" >

<%if(fleetList!=null && fleetList.size()>0){
	for(int i=0;i<fleetList.size();i++){%>
<input type="hidden" name="fleetNo_<%=i%>" id="fleetNo_<%=i%>" value="<%=fleetList.get(i).getFleetNumber() %>">
<input type="hidden" name="fleetName_<%=i%>" id="fleetName_<%=i%>" value="<%=fleetList.get(i).getFleetName() %>">
<%}}%>


<input type="hidden" name="dispatch" id="dispatch" value="<%=adminBO.getDispatchBasedOnVehicleOrDriver()%>">
<input type="hidden" name="assoccode" id="assoccode" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>">
<input type="hidden" name="fleetSize" id="fleetSize" value="<%=fleetList.size()%>">
<input type="hidden" name="defaultLatitude" id="defaultLatitude" value="<%=adminBO.getDefaultLati() %>">
<input type="hidden" name="defaultLongitude" id="defaultLongitude" value="<%=adminBO.getDefaultLogi() %>">
<input type="hidden" name="defaultState" id="defaultState" value="<%=adminBO.getState() %>">
<input type="hidden" name="defaultCountry" id="defaultCountry" value="<%=adminBO.getCountry() %>">
<input type="hidden" name="assoccode" id="assoccodeMain" value="<%=adminBO.getMasterAssociateCode()%>" />	
<input type="hidden" name="timeZone" id="timeZoneMain" value="<%=adminBO.getTimeZone()%>" />	
<input type="hidden" name="changeHappeningJob" id="changeHappeningJob" value="false">
<input type="hidden" name="changeHappeningSharedRide" id="changeHappeningSharedRide" value="false">
<input type="hidden" name="changeHappeningDriver" id="changeHappeningDriver" value="false">
<input type="hidden" name="latForRightclick" id="latForRightclick" value=""/>
<input type="hidden" name="longForRightclick" id="longForRightclick" value=""/>
<input type="hidden" name="changeHappeningZone" id="changeHappeningZone" value="false">
<input type="hidden" name="currentJobinJObs" id="currentJobinJObs" value="">
<input type="hidden" name="currentJobinDrivers" id="currentJobinDrivers" value="">
<input type="hidden" name="currentDriverinJObs" id="currentDriverinJObs" value="">
<input type="hidden" name="currentDriverinDrivers" id="currentDriverinDrivers" value="">
<input type="hidden" name="latJobs" id="latJobs" value="">
<input type="hidden" name="longJobs" id="longJobs" value="">
<input type="hidden" name="latDriver" id="latDriver" value="">
<input type="hidden" name="longDriver" id="longDriver" value="">
<input type="hidden" name="deactivateDriver" id="deactivateDriver" value="">
<input type="hidden" name="notAnumber" id="notAnumber" value="">
<input type="hidden" name="notAnumberB" id="notAnumberB" value="">
<input type="hidden" name="startLat" id="startLat" value="">
<input type="hidden" name="startLong" id="startLong" value="">
<input type="hidden" name="ciBtnType" id="ciBtnType" value="<%=adminBO.getCallerIdType()%>"/>
<input type="hidden" name="fleetDispatch" id="fleetDispatch" value="<%=adminBO.getFleetDispatch()%>"/>
<!-- <input type="hidden" name="phone0" id="phone0" value=""></input>        
<input type="hidden" name="phone1" id="phone1" value=""></input>        
<input type="hidden" name="phone2" id="phone2" value=""></input>        
<input type="hidden" name="phone3" id="phone3" value=""></input>   
<input type="hidden" name="phone4" id="phone4" value=""></input>
<input type="hidden" name="phone5" id="phone5" value=""></input>
<input type="hidden" name="phone6" id="phone6" value=""></input>
<input type="hidden" name="phone7" id="phone7" value=""></input>
<input type="hidden" name="phone8" id="phone8" value=""></input>
<input type="hidden" name="phone9" id="phone9" value=""></input>
<input type="hidden" name="name0" id="name0" value=""></input>
<input type="hidden" name="name1" id="name1" value=""></input>
<input type="hidden" name="name2" id="name2" value=""></input>
<input type="hidden" name="name3" id="name3" value=""></input>
<input type="hidden" name="name4" id="name4" value=""></input>
<input type="hidden" name="name5" id="name5" value=""></input>
<input type="hidden" name="name6" id="name6" value=""></input>
<input type="hidden" name="name7" id="name7" value=""></input>
<input type="hidden" name="name8" id="name8" value=""></input>
<input type="hidden" name="name9" id="name9" value=""></input>
 -->
 <input type="hidden" name="neverShowFooter" id="neverShowFooter" value="0"/>
<input type="hidden" name="driverShowCheck" id="driverShowCheck" value=""></input>
<input type="hidden" name="mas" id="mas" value="<%=adminBO.getMasterAssociateCode()%>"/>
<input type="hidden" name="callerIdCheck" id="callerIdCheck" value="<%=adminBO.getCallerId() %>"/>
<input type="hidden" name="driverOrCabCheck" id="driverOrCabCheck" value="<%=driverOrCab%>"/>
<input type="hidden" name="dispType" id="dispType" value="<%=adminBO.getDispatchMethod()%>">
<input type="hidden" name="refShared" id="refShared" value="true">
<input type="hidden" name ="distanceBasedOn" id="distanceBasedOn" value="<%=adminBO.getDistanceBasedOn()%>"/>
<input type="hidden" name="noOfDriver" id="noOfDriver" value=""/>
<%if(adminBO.getRoles().contains("1022")){ %>
<input type="hidden" name="accessHistory" id="accessHistory" value="Yes"/>
<%}else{ %>
<input type="hidden" name="accessHistory" id="accessHistory" value="No"/>
<%} %>

<%if(adminBO.getRoles().contains("1023")){ %>
<input type="hidden" name="accessSummary" id="accessSummary" value="Yes"/>
<%}else{ %>
<input type="hidden" name="accessSummary" id="accessSummary" value="No"/>
<%} %>

<div id="outer-div">
<div id="top-header">
	<div class="time">
		<div id="tid" style="position: absolute;margin-left:350px"></div>
				<div id="tid1" style="display:none;position: absolute;margin-left:350px"></div>
		
	</div>
	<div id="primary-links">
		    <div class="menu-link-block" style=" margin-left:432px;">
			<div class="menu-link-name"> <a href="#" class="Jobs" onclick="createCookieForJobs()"> Jobs </a></div>
			<a href="#" id="jobs-icon" class="Jobs"><img alt="" src="images/Dashboard/jobs-icon.png" style="height: 45px;width:65px;" onclick="createCookieForJobs()"> </a> </div>
			
			<div class="menu-link-block" >
			<div class="menu-link-name"> <a href="#" class="create" onclick="createJobORDash()">Create</a></div>
			<div class="menu-link-icon"> <a href="#" class="create" onclick="createJobORDash()"> <img alt="" src="images/Dashboard/dispatch-icon.png" style="height: 45px;width:65px;"> </a> </div>
			</div>
			<div class="menu-link-block">
			<div class="menu-link-name"> <a href="#"onclick=""  class="ORHistory">Jobs History</a></div>
			<a href="#" id="zones-icon" class="ORHistory"><img alt="" src="images/Dashboard/History.png"  style="height: 45px;width:65px;"> </a> </div>
			
			<div class="menu-link-block">
			<div class="menu-link-name"> <a href="#"onclick="createCookieForReserve()" class="ORSummary">Reservation</a></div>
			<a href="#" id="zones-icon" class="ORSummary"> <img alt="" src="images/Dashboard/EditReservation.png" onclick="createCookieForReserve()" style="height: 45px;width:65px;"> </a> </div> 
	        
	         <div class="menu-link-block"  >
			<div class="menu-link-name"> <a href="#" class="zones" onclick="createCookieForZones()"> Zones </a></div>
			<a href="#" id="zones-icon" class="zones"><img alt="" src="images/Dashboard/zones-icon.png"  onclick="createCookieForZones()" style="height: 45px;width:65px;"> </a> </div>
			
			 <div class="menu-link-block">
			<div class="menu-link-name"> <a href="#" class="operationStatistics" onclick="createCookieForStatistics()">Statistics</a></div>
			<a href="#" id="drivers-icon"class="operationStatistics"> <img alt="" src="images/Dashboard/statistics.png" onclick="createCookieForStatistics()"  style="height: 45px;width:65px;"> </a> </div>
			
			<div class="menu-link-block">
			<div class="menu-link-name"> <a href="#" class="Driver" onclick="createCookieForDrivers()"> <%=driverOrCab %></a></div>
			<a href="#" id="drivers-icon" class="Driver"> <img alt="" src="images/Dashboard/drivers-icon.png"  style="height: 45px;width:65px;" onclick="createCookieForDrivers()"> </a> </div>
			
			<div class="menu-link-block">
			<div class="menu-link-name"> <a href="#" class="options" onclick="createCookieForOptions()">Options</a></div>
			<a href="#" id="zones-icon" class="options"><img alt="" src="images/Dashboard/options.png" onclick="createCookieForOptions()" style="height: 45px;width:65px;"> </a> </div>	

			<div class="menu-link-block">
			<div class="menu-link-name"> <a href="#" class="Message" onclick="createCookieForMsg()"> Message</a></div>
			<a href="#" id="zones-icon" class="Message"><img alt="" src="images/Dashboard/sendSMS.png" onclick="createCookieForMsg()" style="height: 45px;width:65px;"> </a> </div>
			
			<div class="menu-link-block">
			<div class="menu-link-name"> <a href="javascript: sumbitValueFromDash()" class="home">Home</a></div>
			<a href="javascript: sumbitValueFromDash()"  id="zones-icon" class="home"> <img alt="" src="images/Dashboard/Home.png" style="height: 45px;width:65px;"> </a> </div>
		
			<div class="menu-link-block">
			<div class="menu-link-name"> <a href="#" class="help" onclick="showInstructions()">Help</a></div>
			<a href="#"  id="zones-icon" onclick="showInstructions()" class="help"> <img alt="" src="images/Dashboard/help1.png" style="height: 45px;width:65px;"> </a> </div>

<!-- 			<div class="menu-link-block">
			<div class="menu-link-name"> <a href="#" class="help" onclick="showInstructions()">Search</a></div>
			<a href="#"  id="zones-icon" onclick="showSearchDash(1)" class="help"> <img alt="" src="images/zoomer.png" style="height: 25px;width:35px;"> </a> </div>
 -->			
					<a href="#"  id="zones-icon" onclick="showSearchDash(1)" class="help"> <img alt="" src="images/zoomer.png" style="height: 25px;width:35px;"> </a>
				</div>
	</div> 
	<div id="main-content">
	<div id="gmap">
   <div id="map" style=" width:100%; top: 0pt;   top: 0pt;  "></div>
   </div>
		<ul id="markers"></ul>
		</div>
	
      <%--  <div id="lastJobRunTime" style="size: 1.6px; position: absolute; margin-left: 460px; margin-top: 10px; color: green; top: 0pt; left: 0pt;"></div>
        <input type="image" name="StartOrStopTimer"id="StartOrStopTimer" style="text-decoration: blink;display:none; text-shadow: gray; color: black;position: absolute; margin-left: 470px; margin-top: 30px;top: 0pt; left: 0pt;" value="" />
      <%
													AdminRegistrationBO adminBo = (AdminRegistrationBO) request
															.getSession().getAttribute("user");
													String stopAssoccode = adminBO.getAssociateCode();
													Timer stopTimer = (Timer) getServletConfig().getServletContext()
															.getAttribute(stopAssoccode + "Timer");
													QueueProcess queueProcess = (QueueProcess) getServletConfig().getServletContext()
													.getAttribute(stopAssoccode + "Process");

												if(queueProcess.isDispatchingJobs()==false){
													
												%>
												<input name="btnStartOrStopTimer" id="btnStartOrStopTimer"
													type="button" class="lft" onclick="startOrStopTimerForDashboard()" style="size: 1.6px; position: absolute; margin-left: 470px; margin-top: 25px; color: green; top: 0pt; left: 0pt;"
													value="Start Dispatch"/>
												<%}else{ %>
											<input name="btnStartOrStopTimer" id="btnStartOrStopTimer"
													type="button" class="lft" onclick="startOrStopTimerForDashboard()" style="size: 1.6px; position: absolute; margin-left: 470px; margin-top: 25px; color: green; top: 0pt; left: 0pt;"
													value="Stop Dispatch"/>
													<%} %> --%>
	<!-- 	  <div class="menu-link-block"style=" position: absolute; margin-left: 590px; margin-top: 2px;  top: 0pt; left: 0pt;" >
			<a href="#"  ><img id="compLogo" alt="" src="DashBoard?event=getCompanyLogo" style="height: 65px; width: 105px; position: relative; left: -42px; top: -5px;"/></a></div>
 -->      <!-- <div id="console" style="size:1.6px; position: absolute;margin-left:690px;margin-top:10px;color:green;top:0;left:0;">
   </div>-->
    <div id="console1" style=" position: absolute; margin-left: 370px; margin-top: 13px; color: yellow; top: 0pt; left: 0pt;">
   J:<f id="totalJobs"></f><br/>D:<g id="totalDrivers"></g>
   </div>
         <div id="LogoCompany" style=" -moz-border-radius: 8px 8px 8px 8px;align: right; width: 180px; height: 22px; position: absolute; top: 0px; left: 10pt;">
   			<img alt="" src="DashBoard?event=getCompanyLogo" style="height: 65px;width:132px;"/>
   </div>
  <%--     <div id="FleetChnage" style=" -moz-border-radius: 8px 8px 8px 8px;align: right; width: 180px; height: 22px; position: absolute; top: 18px; left: 200pt;">
			 <%
			 if(fleetList!=null && fleetList.size()>0){%>
			                         <select name="fleet" id="fleet" >
                       <%if(fleetList!=null){
	for(int i=0;i<fleetList.size();i++){ 
	%>
								<option value="<%=fleetList.get(i).getFleetNumber()%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBO.getAssociateCode())?"selected=selected":""%> ><%=fleetList.get(i).getFleetName()%></option>
<% }}%> 
                        </select>
                        <input type="button" name="fleet" id="fleet" value="Change Fleet" onclick="changeFleet()"/>
                      <%  } else {%>
                      	<input type="hidden" name="fleet" id="fleet" value=""/>
                      <%} %>
                       </div> --%>
                       
       <%if(fleetList!=null&&fleetList.size()>0){%>
      <div id="fleetDiv" style="  -moz-border-radius: 8px 8px 8px 8px;align: right; width: 180px; height: 62px; position: absolute; top: 0px;left: 120pt;background-color:khaki;max-height:62px;overflow-y:scroll;overflow-x:auto">
          <table><tr>
	<%for(int i=0;i<fleetList.size();i++){ 
		if(i%2==0){%>
			<tr>
	    <%}%>
				<td>
					<input type="button" name="fleet_<%=i%>" class="fleetbutton"  value ="<%=fleetList.get(i).getFleetName()%>" id ="fleet_<%=i%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBO.getAssociateCode())?"style='background-color:green'":""%> onclick="changeFleetDash('<%=fleetList.get(i).getFleetNumber()%>','<%=i%>')"  />
					<input type="hidden" name="fleetNumberForCookie<%=i %>" id="fleetNumberForCookie<%=i %>" value="<%=fleetList.get(i).getFleetNumber()%>"/>
					<input type="hidden" name="changedFleet" id="changedFleet" value=""/>
	 			</td>
	<%} %>
          </tr>
         </table></div>
  <%} %>
                       
     <div id="cancelButton" width="145" height="85" style="display: none;margin-left: 680px;margin-top:10px;align: right;width: 10px;height: 10px;  position: absolute; top: 0;left: 0;filter:alpha(opacity=80); opacity: 0.7;">
   <input type="button" value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;" onclick="cancelMap()" style="background-color:red; color: black; font-size:80%;margin-top:60px;width:130px;height:45px; -moz-border-radius: 8px 8px 8px 8px; "class="centerB">
   </div>
   <div id="queueid"  style="display: none;align: right;width: 10px;height: 10px;  position: absolute; top: 0;left: 0;">
  <jsp:include page="/Company/ZoneWaiting.jsp" /> 
  </div>
  <div id="drivers1"  style="display: none; align: right;width: 10px;height: 10px; position: absolute; top: 0;left: 0;">
  <jsp:include page="/Company/DriverDetails.jsp" />
  </div>
 <div id="statistics"  style="display: none;width: 10px; height: 10px; position: absolute; top: 109px; left: 789px; ">
  <table border="1" bgcolor="#efeffb" style="-moz-border-radius: 8px 8px 8px 8px;border:solid lightgrey;color:black"><tr><td align="center" bgcolor="#ceecf5"style="white-space:nowrap;">Dispatch Statistics</td><td bgcolor="#ceecf5"align="right"><img src="images/Dashboard/refresh1.png" onclick="jobCount()"/></td><td><img src="images/Dashboard/close.png"onclick="removeStatistics();removeCookieForStatistics()"/></td></right>
  </tr><tr><td colspan="3"   style="white-space:nowrap;"><div id="statisticsConsole"></div></td></tr>
  </table>
  </div>
  <div id="sendSms" class="jqmWindow" style=" display: none; align: right;width: 1px;height: 1px;  position: absolute; top:0;left:0;">
  <jsp:include page="/Company/SMSSend.jsp" /> 
  </div>
  <div id="ORHistory" class="jqmWindow" style=" display: none; align: center;left:5px;  position: absolute;">
	<jsp:include page="/Company/OpenRequestHistory.jsp" /> 
  </div>
  <div id="ORSummary" class="jqmWindow" style=" display: none; align: center;  position: absolute; ">
	<jsp:include page="/Company/openRequestSummary.jsp" />
  </div>
	<div class="jqmWindow" id="searchForJobDash" style="font-weight: bolder;color:#7E2217;background-color:#BDEDFF;border-color:#FF3333;border-width: 3px;-moz-border-radius:12px 12px 12px 12px;display: none;width: 70%;">
		<input type="text" name="jobSear" id="jobSear" value=""/>
		<input type="button" name="Find" id="Find" value="Find" onclick="showSearchDash(2)"/>
		<img src="images/Dashboard/close.png" onclick="removeSearchScreenDash()"/>
		<table id="myJobsToSearchDash" style="width: 100%;overflow: scroll;height: 100px;"></table>
	</div>
  
 <div id="getDriver"style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right; width: 180px; height: 50px; position: absolute; top: 150pt; left: 300pt;">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeGetDriver()">
 <%if(adminBO.getDispatchBasedOnVehicleOrDriver()==1){ %>
 <input type="text" name="maDriverId" id="maDriverId" value="" placeholder="Enter the DriverID"> 
 <input type="hidden" name="maCabId" id="maCabId" value=""  placeholder="Enter the Cab No"> 
 <%}else{ %>
 <input type="text" name="maCabId" id="maCabId" value=""  placeholder="Enter the Cab No"> 
 <input type="hidden" name="maDriverId" id="maDriverId" value="" placeholder="Enter the DriverID"> 

 <%} %>
 <center><input type="button" name="locate" id="locate" value="Position the Driver" onclick="locateDriver()"/></center>
  </div>
  <div id="showzoneRates"  style="display: none; align: right;width: 10px;height: 10px; position: absolute; top: 0;left: 0;">
  <jsp:include page="/Company/ZoneRates.jsp" />
  </div>
  <div id="SuggestionsToAllocate"style="position: absolute;background-color:lightgrey;" class="jqmWindow">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeSuggestions()"> 
<h6 style="background-color:grey">Drivers&nbsp;&nbsp;Sorted&nbsp;&nbsp;by&nbsp;&nbsp;Shortest&nbsp;&nbsp;Distance</h6>
 <table id="suggAll" style="-moz-border-radius: 8px 8px 8px 8px;width:100%;" bgcolor="lightgrey" border="1"></table>
 <h6 style="background-color:grey">Drivers&nbsp;&nbsp;Sorted&nbsp;&nbsp;by&nbsp;&nbsp;Clearing&nbsp;&nbsp;the&nbsp;&nbsp;Zones</h6>
 <table id="sortedID"  style="-moz-border-radius: 8px 8px 8px 8px;width:100%" bgcolor="lightgrey" border="1"></table>
  </div>
  
    <div id="sendMsg"style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right; width: 240px; height: 130px; position: absolute; top: 150pt; left: 300pt;">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeMessageToIndividual()"> <textarea rows="3" cols="30" name="message" id="message" ></textarea> <input type="hidden" name="sendTripId" id="sendTripId" value=""><input type="hidden" name="sendDriver" id="sendDriver" value=""><input type="hidden" name="sendRiderName" id="sendRiderName" value=""><br>Send Voice Message<input type="checkbox" name="sendVoiceMsgToDriver" id="sendVoiceMsgToDriver" value=""><center><input type="button" name="send" id="send" value="Send Message" onclick="sendSmsToDriver()"></center>
  </div>
  <div id="changeFleet"style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right; width: 120px; height: 30px; position: absolute; top: 150pt; left: 300pt;">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeFleetScreen()">
                  <select name="fleetV" id="fleetV" >
                      <%if(fleetList!=null){
       for(int i=0;i<fleetList.size();i++){ %>
       <option value="<%=fleetList.get(i).getFleetNumber()%>" ><%=fleetList.get(i).getFleetName() %></option>
<% }}%>
        </select>
 <input type="hidden" name="tripForanother" id="tripForanother" value=""/>
 <input type="button" name="changeFleetD" id="changeFleetD" value="Change" onclick="ChangeFleetToAnother()"/>
  </div>
  
   <div id="jobLogsDiv"  style="background-color:#ceecf5;display:none;position: absolute;	top: 304px;left: 527px;"><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeJobsLogsPopUp()">
  <div id="jobLogsPopUp"></div></div>
  <div id="jobHistoryDiv"  style="background-color:#ceecf5;display:none;position: absolute;	top: 405px;left: 5px;"><img alt="" align="left" src="images/Dashboard/close.png" onclick="removeJobsPopUp()">
  <div id="jobhistoryPopUp"></div></div>
    <div id="callDrivers"style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right; width: 240px; height: 140px; position: absolute; top: 150pt; left: 300pt;z-index:3000000">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeMsgToNearestDrivers()"> Distance :&nbsp;<input type="text" name="distance"id="distance" value="" size="4">miles <br><textarea rows="3" cols="30" name="reasonForMessage" id="reasonForMessage" ></textarea><br>Send Voice Message<input type="checkbox" name="sendVoiceMsgToNearestDriver" id="sendVoiceMsgToNearestDriver" value=""><center><input type="button" align="middle"name="sendToDriver" id="sendToDriver" value="Send" onclick="calculateAndSendMsg()"/></center>
  </div>
   <div id="vMessagePopUp"style="-moz-border-radius: 8px 8px 8px 8px;display: none;align: right;width: 215px;height:138px; position: absolute; top: 180pt; left: 500pt;">
  <img align="left" class ="dragMike"alt="" src="images/Dashboard/Drag.png" width="23px" height="23px">
 <img align="left" alt="" src="images/Dashboard/close.png" onclick="removeVMessageBullHorn()"><div id="vMessageContent"></div> </div>
  <div id="bookToZone"style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right;width: 25%; height: 22%;  position: absolute; top: 150pt; left: 300pt;">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="rempoveLogintToZone()"> <table><tr><td id="zoneName" style="white-space:nowrap"></td></tr><tr><td id="zoneDescription"style="white-space:nowrap" ></td></tr><tr><td id="driverZone" style="display:none"></td></tr>
 <tr><td style="white-space:nowrap"></td></tr>
 <tr><td>Select Current Zone</td><td><input type="checkbox" name="selectCurrentZone" id="selectCurrentZone"value=""></td></tr>
 <tr><td>Select Zone</td><td><select id="zoneList" name="zoneList">
 <%for(int i=0;i<allZonesForCompany.size();i++){ %>
 <option value="<%=allZonesForCompany.get(i).getZoneKey()%>">
 <%=allZonesForCompany.get(i).getZoneDesc()%></option><%} %>
 </select></td></tr>
 <tr><td align="center" colspan="3"><input type="button" name="book" id="book" value="Book To Zone" onclick="logintToZone()"></td></tr>
 </table>
  </div>

  <div id="zoneLatLongs" style="display:none"><table><tr>
  <%for(int i=0;i<allZonesForCompany.size();i++){ %>
  <td>
  	<input type="hidden" name="zoneDesForDash" id="zoneDes" value="<%=allZonesForCompany.get(i).getZoneDesc()%>"/> 
  	<input type="hidden" name="zoneIdForDash" id="zoneIdForDash" value="<%=allZonesForCompany.get(i).getZoneKey()%>"/> 
  	<input type="hidden" name="zoneLatEastForDash" id="zoneLatEastForDash" value="<%=allZonesForCompany.get(i).getEastLongitude()%>"/>
  	<input type="hidden" name="zoneLatWestForDash" id="zoneLatWestForDash" value="<%=allZonesForCompany.get(i).getWestLongitude()%>"/>
  	<input type="hidden" name="zoneLatNorthForDash" id="zoneLatNorthForDash" value="<%=allZonesForCompany.get(i).getNorthLatitude()%>"/>
  	<input type="hidden" name="zoneLatSouthForDash" id="zoneLatSouthForDash" value="<%=allZonesForCompany.get(i).getSouthLatitude()%>"/>
 	<input type="hidden" name="centerlat" id="centerlat" value="<%=(allZonesForCompany.get(i).getNorthLatitude()+allZonesForCompany.get(i).getSouthLatitude())/2%>">
 	<input type="hidden" name="centerlong" id="centerlong" value="<%=(allZonesForCompany.get(i).getEastLongitude()+allZonesForCompany.get(i).getWestLongitude())/2%>">
 	
 	
 </td> <%} %></tr></table>
  </div> 
  
 <div id="bullHorn" style="position: absolute;margin-top:-200px;left: 1032px;">
   <img align="left" class ="dragBullHorn"alt="" src="images/Dashboard/Drag.png" width="23px" height="23px" >
          <img src="images/Dashboard/close.png"onclick="removeBullHorn()" align="left"/>
          <div id="speaker"style="" >
          <center> <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="240" height="170" id="voiceMessageID">
               <object type="application/x-shockwave-flash" data="js/f211.swf" style="width: 260px;height:190px;">
				<PARAM NAME=FlashVars VALUE="runtimer=true">
				<param name="wmode" value="transparent">
				             </object>
           </object></center>
  </div></div>
  <div id="openjobs"style="display: none;align: right; width: 10px;height: 10px;  position: absolute; top:0;left:0;">
  <jsp:include page="/Company/JobDetails.jsp" /> 
  </div>
<table id="orderPopUpSub"  style="z-index: auto;position: absolute; top:0;margin-left: 350px;margin-top: 48px" align="right"></table>
    <div  id="callerIdHistory" style="z-index: auto;position: absolute; top:45%;margin-left: 68%;display: none;background-color: white;filter:alpha(opacity=80); opacity: 0.9;width: 30%;overflow: scroll;height: 20%;"></div>
  <div id="sharedRidePopUp"style="display: none;width: 60%; height: 50%; position: absolute; top: 25%; left: 35%;height:70%;overflow:auto;">
  <jsp:include page="/Company/SharedRide.jsp" /> 
  </div>
  <div id="sharedDetailsPopUp"style="display: none;align: right; width: 10px;height: 10px;  position: absolute; top:0;left:0;">
  <jsp:include page="/Company/SharedRideDetails.jsp" /> 
  </div>

  <div class="block-topShared" id="addRoutePopup" style="display: none;width: 60%; height: 50%; position: absolute; top: 25%; left: 35%;height:70%;overflow:auto;">
  <jsp:include page="/Company/AddToRoute.jsp" /> 
  </div>

  <div id="OptionsForAll" class="jqmWindow" style="display: none;align: right; width: 10px;height: 10px;  position: absolute; top:0;left:0;">
  <div id="allopts" style="background-color: white;filter:alpha(opacity=80); opacity: 0.9;">
				<div class="block-top55" >&nbsp;&nbsp;Options<a style="margin-left:120px" href="#"></a>&nbsp;&nbsp;<a href="#"><img alt=""  src="images/Dashboard/close.png" onclick="removeOptions();removeCookieForOptions()"></a></div>
					<div class="job-avail-mid">
					<div class="job-inner1">
					<table>		
					<tr><td>Auto Refreshtime</td><td><input type="text" name="sec1"id="sec1" value="10" size="1" onchange="createCookieForAutoRefresh();timeSetup()"></td></tr>
					<tr><td>Map Zoom Level </td><td><select name="zoom"id="zoom" onchange="createCookieForZoom();zoomMap()">
                        		<option value="1" >1</option>
								<option value="2" >2</option>
								<option value="3" >3</option><option value="4" >4</option><option value="5" >5</option>
								<option value="6" >6</option><option value="7" >7</option><option value="8" >8</option>
								<option value="9" >9</option><option value="10" >10</option><option value="11" >11</option>
								<option value="12" selected="selected" >12</option><option value="13">13</option><option value="14"   >14</option>
								<option value="15" >15</option><option value="16" >16</option><option value="17" >17</option>
								<option value="18" >18</option><option value="19" >19</option><option value="20" >20</option>                        	
                        	</select> </td><td><input type="text" size="1" name="currentZoom" id="currentZoom" value=""/></td></tr>
                   	<tr><td>Individual Zoom Level </td><td><select name="individualZoom"id="individualZoom" onchange="createCookieForIndividualZoom();individualZoom()">
                        		<option value="1" >1</option>
								<option value="2" >2</option>
								<option value="3" >3</option><option value="4" >4</option><option value="5" >5</option>
								<option value="6" >6</option><option value="7" >7</option><option value="8" >8</option>
								<option value="9" >9</option><option value="10" >10</option><option value="11" >11</option>
								<option value="12" >12</option><option value="13" selected="selected"  >13</option><option value="14" >14</option>
								<option value="15" >15</option><option value="16" >16</option><option value="17" >17</option>
								<option value="18" >18</option><option value="19" >19</option><option value="20" >20</option>                        	
                        	</select> </td></tr>
			<tr>
					<td>AutoRefresh</td> 
					<td><input type="button" value="On" onclick="createCookieForAutoRefreshOnOff('on');Init()"></td>
					<td><input type="button" value="Off" onclick="createCookieForAutoRefreshOnOff('off');autoRefreshOff()"></td></tr>
		<input type="hidden"name="latFromMap"id="latFromMap"value="" size="13"><input type="hidden"name="longFromMap"id="longFromMap"value="" size="13" />
			<tr><td colspan="1"><input type="button" name="setDefaultCenter" id="setDefaultCenter" value="Set Default Location" style="font-size: 10.1px;"onclick="createCookieForCenter()"></td><td><input type="button" name="removeCookie"id="removeCookie" size="4"value="Clear"onclick="removeCookieForDefault()"/></td></tr>
						<tr><td colspan="2"align="center"><input type="button" name="showBullHorn" id="showBullHorn" value="Show Push To Talk Horn" onclick="showVoiceMessageScreen()"/></td></tr>
			
<%-- 			</tr><tr><td colspan="2" align="center"> <input type="image" name="StartOrStopTimer"id="StartOrStopTimer" style="text-decoration: blink;display:none; text-shadow: gray; color: black;" value="" />
      <%
													AdminRegistrationBO adminBo = (AdminRegistrationBO) request
															.getSession().getAttribute("user");
													String stopAssoccode = adminBO.getAssociateCode();
													Timer stopTimer = (Timer) getServletConfig().getServletContext()
															.getAttribute(stopAssoccode + "Timer");

												if(stopTimer==null){
												%>
												<input name="btnStartOrStopTimer" id="btnStartOrStopTimer"
													type="button" class="lft" onclick="startOrStopTimerForDashboard()" style="size: 1.6px;  color: green; top: 0pt; left: 0pt;"
													value="Start Dispatch"/>
												<%}else{ %>
											<input name="btnStartOrStopTimer" id="btnStartOrStopTimer"
													type="button" class="lft" onclick="startOrStopTimerForDashboard()" style="size: 1.6px;  color: green; top: 0pt; left: 0pt;"
													value="Stop Dispatch"/>
													<%} %></td></tr>
 --%>			</table>
					<div id="lastJobRunTime" style="padding-left:18px;size: 1.6px; color: green;"></div>
					<div id="more" align="center" onclick="showAdvancedOptions()" style="cursor:pointer;color:red">- Less</div>
				<div id="advancedOptions" style="max-height:250px;overflow-y:auto"><table id="colorchage"> 
					<tr><td align="left"> VIP </td> <td align="right"> <input type="text" name="vipColor" id="vipColor" value="24f511" size="4" style="background-color:#24f511;cursor:pointer"/></td></tr>
					<tr><td align="left"> Timed Call  </td> <td align="right"> <input type="text" name="timeColor" id="timeColor" value="f5a211" size="4" style="background-color:#f5a211;cursor:pointer"/></td></tr>
					<tr><td align="left"> Voucher </td> <td align="right"> <input type="text" name="voucherColor" id="voucherColor" value="f20c0c" size="4" style="background-color:#f20c0c;cursor:pointer"/></td></tr>
					<tr><td align="left"> Cash </td> <td align="right"> <input type="text" name="cashColor" id="cashColor" value="0930f2" size="4" style="background-color:#0930f2;cursor:pointer"/></td></tr>
					<tr><td align="left"> Available </td> <td align="right"><input type="text" name="avColor" id="avColor" value="66FF66" size="4"style="background-color:#66FF66;cursor:pointer"/></td></tr>
					<tr><td align="left"> No Update </td> <td align="right"><input type="text" name="noUpdate" id="noUpdate" value="ebda1e" size="4" style="background-color:#ebda1e;cursor:pointer"/></td></tr>
					<tr><td align="left"> Not Available </td> <td align="right"><input type="text" name="navColor" id="navColor" value="FF8888" size="4" style="background-color:#FF8888;cursor:pointer"/></td></tr>	
  					<tr><td colspan="2">
  					<%if(fleetList.size()>0){ %>
  					 <b>FleetList</b></td></tr>		 
					<%for(int i=0;i<fleetList.size();i++){ %>	
					<tr><td><%=fleetList.get(i).getFleetName()%> </td> 
					<td align="right"><input type="text" class="fleetlist" name="fleetcolor_<%=i%>" id="fleetcolor_<%=i%>" value="66FF66" size="4" style="background-color:#66FF66;cursor:pointer"/></td></tr>
  			<%}} %></table>
 			<table><tr><td colspan="2">Show Zones</td><td><input type="image"  width="25" height="25" id="zoneCheckImage"src="images/Dashboard/zoneLogin.gif" onclick="showAllZonesForDash();"></td></tr>
		<tr><td colspan="2">Zone Colors</td></tr>

		<tr><td><input type="text" name="zoneColor1" id="zoneColor1" value="FFA500" size="4" style="background-color:#FFA500;cursor:pointer" /></td>
		<td><input type="text" name="zoneColor2" id="zoneColor2" value="FF00FF" size="4" style="background-color:#FF00FF;cursor:pointer" /></td>
		<td><input type="text" name="zoneColor3" id="zoneColor3" value="FF0000" size="4" style="background-color:#FF0000;cursor:pointer" /></td></tr>

		<tr>
		<td><input type="text" name="zoneColor4" id="zoneColor4" value="00FF00" size="4" style="background-color:#00FF00;cursor:pointer" /></td>
		<td><input type="text" name="zoneColor5" id="zoneColor5" value="808000" size="4" style="background-color:#808000;cursor:pointer" /></td>
		<td><input type="text" name="zoneColor6" id="zoneColor6" value="616D7E" size="4" style="background-color:#616D7E;cursor:pointer" /></td></tr>

		<tr>
		<td><input type="text" name="zoneColor7" id="zoneColor7" value="306EFF" size="4" style="background-color:#306EFF;cursor:pointer" /></td>
		<td><input type="text" name="zoneColor8" id="zoneColor8" value="8D38C9" size="4" style="background-color:#8D38C9;cursor:pointer" /></td>
		<td><input type="text" name="zoneColor9" id="zoneColor9" value="F52887" size="4" style="background-color:#F52887;cursor:pointer"/></td></tr>

		<tr>
		<td><input type="text" name="zoneColor10" id="zoneColor10" value="00FF00" size="4" style="background-color:#00FF00;cursor:pointer" /></td></tr>
		</table> 
				<input type="button" name="swOld" id="swOld" value="Switch Map" onclick="switchMapScreen('osm')"/>
				<input type="button" name="ciHistoy" id="ciHistoy" value="CallerId History" onclick="showCIHistory()"/>
				<input type="button" name="showNotifiMsgs" id="showNotifiMsgs" value="Show Notification Messages" onclick="showNotifications()">
				<input type="button" name="callActiveDrivers" id="callActiveDrivers" value="Call Drivers For Job" onclick="callActiveDrivers()">
				<input type="button" name="messageForDrivers" id="messageForDrivers" value="Message For Drivers" onclick="messageforDrivers()">
				<input type="button" name="zoneRates" class="zoneRates" value="Zone Rates" onclick="createCookieForZoneRates()"/>
			    <input type="button" name="smsHistory" class="smsHistory" value="smsHistory" onclick="showsmsHistory()"/>
				
			 <!--   <input type="button" name="creditcard" id="creditcard" value="creditcard" size="15" onclick="showCreditCardScreen()">
			 -->	
				</div>
				</div>
					</div><img alt="Jobs available" src="images/Dashboard/jobs-avail-bot.png"></div>
					
  </div>

<input type="hidden" name="clickCount" id="clickCount" value="">
<input type="hidden" name="totalDrivers" id="totalDrivers" value=""> 
<input type="hidden" name="sec"id="sec" value="20">
<input type="hidden" name="secPopUP"id="secPopUP" value="15">
<input type="hidden" name="zoomValue"id="zoomValue"value="12">
	<input type="hidden" name ="singlejobClicked" id="singlejobClicked" value="">
<input type="hidden" name="individualZoomValue"id="individualZoomValue"value="13">
<input type="hidden" name="autoRefreshOnOff" id="autoRefreshOnOff" value=""/>
<input type="hidden" name="dragend" id="dragend" value="false"/>
<%if(adminBO.getRoles().contains("1038")){ %>
<input type="hidden" name="driverPermission" id="driverPermission" value="yes">
<%}else{%>
<input type="hidden" name="driverPermission" id="driverPermission" value="no">
<%}if(adminBO.getRoles().contains("1037")){ %>
<input type="hidden" name="jobsPermission" id="jobsPermission" value="yes">
<%}else{%>
<input type="hidden" name="jobsPermission" id="jobsPermission" value="no">
<%}if(adminBO.getRoles().contains("1039")){ %>
<input type="hidden" name="zonesPermission" id="zonesPermission" value="yes">
<%}else{%>
<input type="hidden" name="zonesPermission" id="zonesPermission" value="no">
 <%} %>

<div id="callerId" style="margin-left: 350px; top:0;left:0;margin-top: -10%"  >
<input type="button" value="ERROR" id="refreshCIButton" name="refreshCIButton" disabled="disabled" style="-moz-border-radius:8px 8px 8px 8px;background-color: Red;color: #FFFFFF;width: 200px;height: 40px;font-size: large;display:none;position: fixed;"/>
<table width="100%" id="tblCallerIdDash">
</table>
</div>
	
		<div id="popUpOR" style="z-index:3000">
		<table id="tabForDetails" style="display: none;z-index: 3000; width: 550px; cursor: pointer;margin-top:-3%;margin-left:21%">
		<tr style="background-color: black;">
		<td id="detailsOR" onclick="openORDetails('')">
		<font id="detailsORFont" color="white">Details</font>
		</td> 
		<td id="detailsLogs" onclick="showLogs()">
		<font id="detailsLogsFont" color="white">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
	 <div id="ORDash" class="jqmWindow" style="display: none;height:330px;width: 550px;">
			<jsp:include page="/jsp/OpenRequestForDashBoard.jsp" />
		</div></div>
 				
		<div id="jobDetailsMapDash" class="jqmWindow" style="display: none;height:430px;width: 750px;margin-left:10%;z-index:3000;">
		
				<div id="jobDetailsMapDashDiv" style="height:330px;width: 550px;margin-left:10%;">
	
	</div>
		</div>
		
			<div id="paymentDetailsDash" class="jqmWindow" style="display: none;height:330px;width: 550px;margin-left:10%;">
			
				<div id="paymentDetailsDashDiv" style="height:330px;width: 550px;margin-left:10%;">
	</div>
		</div>
   <div id="jobDetailsLogsDiv"  style="background-color:#ceecf5;display:none;position: absolute;	 top: 4%; left: 20%; z-index: 3000;"><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeJobsDetailsLogsPopUp()">
  <div id="jobDetailsLogsPopUp"></div></div>

		
	
 				
		<div id="jobDetailsMapDash" class="jqmWindow" style="display: none;height:430px;width: 750px;margin-left:10%;z-index:3000;">
		
				<div id="jobDetailsMapDashDiv" style="height:330px;width: 550px;margin-left:10%;">
	
	</div>
		</div>
		
			<div id="paymentDetailsDash" class="jqmWindow" style="display: none;height:330px;width: 550px;margin-left:10%;">
				<div id="paymentDetailsDashDiv" style="height:330px;width: 550px;margin-left:10%;">
	</div>
		</div>
   <div id="jobDetailsLogsDiv"  style="background-color:#ceecf5;display:none;position: absolute;	 top: 4%; left: 20%; z-index: 3000;"><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeJobsLogsPopUp()">
  <div id="jobDetailsLogsPopUp"></div></div>

		<div id="popUpDriver" style="z-index: 3000" >	
		<div id="ORDashDriver" style="display: none;z-index: 3000">
			<jsp:include page="/Company/driverSummary.jsp" />
		</div>	</div>
		<div id="errorForStatus" style="z-index: 3000" >	
 		 <div id="ErrorStatus" style="display: none;font-weight: bolder;color:#FF3333;background-color:#FFFF33;z-index: 3000"></div></div>
		<div id="error" style="z-index: 3000" >	
 		 <div id="Error" style="display: none;font-weight: bolder;color:#FF3333;background-color:#FFFF33;z-index: 3000"></div></div>
		<div id="popUpAddress" style="z-index: 3000;margin-left:180px;margin-top: 3%;" >	
 		<div id="ORDashAddress" style="z-index: 3000" >
			</div></div>
			<div id="popUpLandMark" style="z-index: 3000" >	
 		<div id="ORDashLandMark" style="display: none;z-index: 3000">
			<jsp:include page="/jsp/landmarkDash.jsp" />
			</div></div>		
		 <div id="hoverEffect" style="z-index: 3000;margin-top: 3%;">	
		<div id="showHover" style="display: none;margin-left:100px;position:absolute; ;background-color: #4EE2EC;width:532px;-moz-border-radius:12px 12px 12px 12px;z-index: 3000">	
			<table style="background-color: #4EE2EC;-moz-border-radius:12px 12px 12px 12px;">
					<tr>
						<td style="-moz-border-radius:8px 8px 8px 8px;"><input type="button" style="color: black; font-size:small;font-weight: bold;background-color: #4EE2EC;" id="ph.Number" name="ph.Number" value="" size="2" onclick="appendPhone()"/></td>
						<td colspan='5' align='center' style="background-color:gray ;size: 300px;-moz-border-radius:8px 8px 8px 8px;">
						<input style="background-color: #4EE2EC;color: black;font-style: italic;font-size:small;font-weight: bold;-moz-border-radius:12px 12px 12px 12px;" id="hoverValue" readonly="readonly" value="" size="40"/></td>
						<td style="-moz-border-radius:8px 8px 8px 8px;"><input type="button" style="color: black;font-size:small;font-weight: bold;background-color: #4EE2EC;" id="futureJobs" name="futeureJobs" onclick="createCookieForReserve(); ORSummary(1)" value="Reservation" size="1"/></td>
						<td style="-moz-border-radius:8px 8px 8px 8px;"><input type="button" name="conference" style="color: black;font-size:small;font-weight: bold;background-color: #4EE2EC;" value="Driv-Pass Conf" onclick="conferenceCallDash()"></input></td>
						<td><a href="#"><img alt="" src="images/Dashboard/close.png" onclick="removeAddress()"></a><a href="#"></a></td>
					</tr>
			</table>
			</div></div>  
			<div id="provider" style="z-index: 3000" >	
					<div  id="phoneProvider" style="width: 50%;z-index: 3000;display: none;">
				 	<table border=2 BORDERCOLOR=green valign=top style="width: 30%;margin-left: 10%;"><tr style='color: blue'>
				 	<td style="width: 15%;"><select name="provider" id="providerValue">
					<option value="">Select</option>
					<option value="@txt.att.net">ATT</option>
					<option value="@myboostmobile.com">Boost Mobile</option>
					<option value="@sms.mycricket.com">Cricket</option>
					<option value="@mymetropcs.com">MetroPCS</option>
					<option value="@smtext.com">Simple Mobile</option>
					<option value="@tmomail.net">TMobile</option>
					<option value="@vtext.com">Verizon</option>
					<option value="@vmobl.com">Virgin</option>
					<option value="@messaging.sprintpcs.com">Sprint</option>
					</select></td>
				 	<td style="width: 15%;"><input type="button" value="Confirm" id="confirm" onclick="enterEmail()"/></td>
				 	</tr></table>
					</div></div>
		<div class="jqmWindow" id="ahowAllZoneNames"  style="display:none;width:520px" >
		<TABLE><tr><td>
  <input type="image" name="closeZon" id="closeZon" src="images/Dashboard/close.png" onclick="closeZoneList()"align ="right"style="Cursor:pointer;color:red;">
 <center><h3><a style="color:green">Select Zones</a></h3></center>
 <div style="height: 400px;width:320px; overflow-y: scroll;overflow-x:hidden;">
 
  <table id="zonesForOperator" style="height: 400px;width:300px; overflow-y: scroll;overflow-x:hidden;">
  <%for(int i=0;i<allZonesForCompany.size();i++){ %>
  <tr><td><%=allZonesForCompany.get(i).getZoneDesc()%></td>
 <td><input type="checkbox" name="selectZones_<%=i%>" id="selectZones_<%=i%>" value="<%=allZonesForCompany.get(i).getZoneKey()%>"/>
  </td></tr>
  <%} %>
 </table></div>
 <table> 
 <tr><td>Default Zone :</td><td><input type="checkbox" name="defZone" id="defZone" value="<%=defaultZone%>"></td>
  <tr><td align="center"><input type="button" id="selectZonesForOp" value="Create List" value="Select Zones" onclick="createZonesList()"></td>
  <td><input type="button" name="selectAll" id="selectAll" value="Select All" onclick="selectAllZonesList()"></td>  <td><input type="button" name="unSelect" id="unSelect" value="UnSelect All" onclick="unSelectAllZonesList()"></td>
  <td><input type="button" name="resetAll" id="resetAll" value="Reset" onclick="resetZoneList()"></td></tr>
  </table>
</td>
<td>
<h6>Select Fields to show in the Job Details</h6>
<table><tr><td>
# --> S.No    </td><td><input type="checkbox" name="showJobNo" id="showJobNo" value="" onclick="hideDetails()"></td></tr><tr><td>
Address </td><td><input type="checkbox" name="showAddress" id="showAddress" value="" onclick="hideDetails()"></td></tr><tr><td>
Drop Add </td><td><input type="checkbox" name="showDAddress" id="showDAddress" value="" onclick="hideDetails()"></td></tr><tr><td>
Driver </td><td><input type="checkbox" name="showDriverAllocate" id="showDriverAllocate" value="" onclick="hideDetails()"></td></tr><tr><td>
Age </td><td><input type="checkbox" name="showAge" id="showAge" value="" onclick="hideDetails()"></td></tr><tr><td>
Ph --> phone No </td><td><input type="checkbox" name="showPhone" id="showPhone" value="" onclick="hideDetails()"></td></tr><tr><td>
Name </td><td><input type="checkbox" name="showName" id="showName" value="" onclick="hideDetails()"></td></tr><tr><td>
Time --> Pickup Time </td><td><input type="checkbox" name="showPtime" id="showPtime" value="" onclick="hideDetails()"></td></tr><tr><td>
Flags --> Special Flags </td><td><input type="checkbox" name="showSplFlags" id="showSplFlags" value="" onclick="hideDetails()"></td></tr><tr><td>
Show Landmark</td><td><input type="checkbox" name="showLandmark" id="showLandmark" value="" onclick="hideDetails()"></td></tr><tr><td>
SCity</td><td><input type="checkbox" name="showScity" id="showScity" value=""onclick="hideDetails()"></td></tr><tr><td>
ECity</td><td><input type="checkbox" name="showEcity" id="showEcity" value=""onclick="hideDetails()"></td></tr><tr><td>
SZ --> Start Zone </td><td><input type="checkbox" name="showStZone" id="showStZone" value="" onclick="hideDetails()"></td></tr><tr><td>
EZ --> End Zone </td><td><input type="checkbox" name="showEdZone" id="showEdZone" value=""onclick="hideDetails()"></td></tr><tr><td>
Trip ID</td><td><input type="checkbox" name="showTripId" id="showTripId" value=""onclick="hideDetails()"></td></tr><tr><td>
Trip Source</td><td><input type="checkbox" name="showTripSource" id="showTripSource" value="" onclick="hideDetails()"></tr><tr><td>
Route Number</td><td><input type="checkbox" name="showRouteNum" id="showRouteNum" value="" onclick="hideDetails()"></tr><tr><td>
Flight Info</td><td><input type="checkbox" name="showFlight" id="showFlight" value="" onclick="hideDetails()"></tr><tr><td>
Account</td><td><input type="checkbox" name="showVoucher" id="showVoucher" value="" onclick="hideDetails()"></tr><tr><td>
Amount</td><td><input type="checkbox" name="showAmount" id="showAmount" value="" onclick="hideDetails()"></tr><tr><td>
<!-- Show checkbox</td><td><input type="checkbox" name="showcheck" id="showcheck" value="" onclick="hideDetails()"></tr><tr><td> -->


</tr> </table>
</td>
</tr></TABLE>
 </div>
  
<div id="setEndZone"style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right;width: 25%; height: 22%;  position: absolute; top: 150pt; left: 300pt;">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="rempoveEndZone()"> 
 <table>
 <tr><td ><input type="hidden" name="tripIdForEndZone" id="tripIdForEndZone" value="" ></td></tr>
 <tr><td>Select Zone</td><td><select id="zoneListforEndZone" name="zoneListforEndZone">
 <%for(int i=0;i<allZonesForCompany.size();i++){ %>
 <option value="<%=allZonesForCompany.get(i).getZoneKey()%>">
 <%=allZonesForCompany.get(i).getZoneDesc()%></option><%} %>
 </select></td></tr>
 <tr><td align="center" colspan="3"><input type="button" name="book" id="book" value="Set EndZone" onclick="setEndZone()"></td></tr>
 </table>
  </div>
 <div class="jqmWindow" id="sharedRideSettings"  style="display:none;width:520px" >
					 <h6>Select Fields to show in the SharedRide </h6>
<table><tr><td>
TripId </td><td><input type="checkbox" name="showDShTripId" id="showDShTripId" value="" onclick="hideSharedRide()"></td></tr><tr><td>
Name </td><td><input type="checkbox" name="showDSHName" id="showDSHName" value="" onclick="hideSharedRide()"></td></tr><tr><td>
Date </td><td><input type="checkbox" name="showDSHDate" id="showDSHDate" value="" onclick="hideSharedRide()"></td></tr><tr><td>
Address</td><td><input type="checkbox" name="showDSHAddress" id="showDSHAddress" value="" onclick="hideSharedRide()"></td></tr><tr><td>
Strat Zone</td><td><input type="checkbox" name="showDSHSZone" id="showDSHSZone" value="" onclick="hideSharedRide()"></td></tr><tr><td>
End Address</td><td><input type="checkbox" name="showDSHEDAddress" id="showDSHEDAddress" value="" onclick="hideSharedRide()"></td></tr><tr><td>
End Zone</td><td><input type="checkbox" name="showDSHEDZone" id="showDSHEDZone" value="" onclick="hideSharedRide()"></td></tr><tr><td>
Trip Source</td><td><input type="checkbox" name="showDShtripSource" id="showDShtripSource" value="" onclick="hideSharedRide()"></td></tr><tr><td>

RefNo</td><td><input type="checkbox" name="showDSHRef" id="showDSHRef" value="" onclick="hideSharedRide()"></td></tr><tr><td>
RefNo1</td><td><input type="checkbox" name="showDSHRef1" id="showDSHRef1" value="" onclick="hideSharedRide()"></td></tr><tr><td>
RefNo2</td><td><input type="checkbox" name="showDSHRef2" id="showDSHRef2" value="" onclick="hideSharedRide()"></td></tr><tr><td>
RefNo3</td><td><input type="checkbox" name="showDSHRef3" id="showDSHRef3" value="" onclick="hideSharedRide()"></td></tr><tr><td>
RouteNo</td><td><input type="checkbox" name="showDSHRouteNo" id="showDSHRouteNo" value="" onclick="hideSharedRide()"></td></tr>
</table>
</div>
 <div id="Msgnotification"   style="display:none;background-color:lightblue;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;width:50%;height:auto;border:8px solid black;margin-left: -18%;" >
   <table><tr style="background-color:#81DAF5;"><td>SMS Notifications</td><td> <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeSms()"></td></tr></table>
  <div id="msNotification" style="border-radius:8px solid black;width:140%;"></div>
<input type="hidden" name="msgIdSms" id="msgIdSms" value="">
<input type="hidden" name="lastNotifiSMSValue" id="lastNotifiSMSValue" value="0"/>
               
                         <table id="SmsDetails"></table>
                

</div>
 

		 <div class="jqmWindow" id="dialog">
		  <table id="individualTable"></table></div> 
	<div id="jobsOnThatDay" style="background-color:#ceecf5;display:none;position: absolute;	top: 405px;left: 5px;"><img alt="" align="left" src="images/Dashboard/close.png" onclick="removeJobsForDay()">
	<div id="wait_show" ></div>
	<div id="jobsForToday" ></div></div>
	<div id="instructions">
        <span class="button bClose"><span>X</span></span>
      
     <center>  <h3>Instructions For Dashboard</h3></center>
       <div id="scrollIntructions" style="overflow:scroll;overflow-x:hidden;max-height:450px;max-width:1200px;" >
       <table ><tr><td>
       <table style="border-right:solid lightgrey;overflow:scroll;max-height:450px;overflow-x:hidden"><tr><td>
       <h5>Shortcuts For Open Request</h5>
       * Key press should be on the input box<br/>
       * You can type  Capital or Small letters <br/>
       * After entered commands press the tab<br/>
       
       <table  >
       <tr><th align="center">S.No</th><th align="center"> Action </th><th align="center"> Shortcuts </th></tr>
       <tr><td>1</td><td align="center"> Operator cancel</td><td align="center">  OC </td></tr>
       <tr><td>2</td><td align="center"> Customer Canceled </td><td align="center"> CC</td></tr>
       <tr><td>3</td><td align="center"> No Show Request </td> <td align="center">NS </td></tr>
       <tr><td>4</td><td align="center"> Driver Completed </td><td align="center"> DC</td></tr>
       <tr><td>5</td><td align="center"> Re-Dispatch a Job </td><td align="center">RD </td></tr>
       <tr><td>6</td><td align="center"> Hold the Job</td><td align="center">  H</td></tr>
       <tr><td>7</td><td align="center"> Couldn't Service</td><td align="center"> CS </td></tr>
       <tr><td>8</td><td align="center"> Broadcast Request</td><td align="center"> BC</td></tr>
       <tr><td>9</td><td align="center"> Resend the Job </td><td align="center"> RS</td></tr>
       <tr><td>10</td><td align="center">Allocate Driver</td><td align="center">(DriverID or Cab)</td></tr>
       <tr><td>11</td><td align="center">Force Allocate Driver </td><td align="center">F(DriverID or CabNo)</td></tr>
       <tr><td>12</td><td align="center">Offer the Job With IVR Call</td><td align="center">OP(DriverID or CabNo)</td></tr>
       </table>
       <br/>
       <h6>Acronyms</h6>
       *<b>Ph</b>-Phone Number&#8194;*<b>St</b>-Status&#8194;
       *<b>DC</b>-Dispatch Comments&#8194;<br/>*<b>CC</b>-Cab Comments
       *<b>SR</b>-Shared Ride&#8194;*<b>RJ</b>-Route(Shared) Job<br/>
       *<b>LO</b>-Log Out&#8194;*<b>VVM</b>-Voice Message
       *<b>J</b>-Jobs&#8194;*<b>D</b>-Driver<br/><br/>
       <h6>Other Commands</h6>
       * Right Click/Double click on the map to Zoom<br/>
       * Right Click on the Map , you can send text/voice Message to drivers in a particular distance<br/>
       * Go to your place on the map,right click to create a job, it will find your address and open the create job options dialogue box<br/> 
       * Right click on the job to get the contextMenu<br/>
       * Click on the Address to get the infowindow on the Map<br/>
       * Click on the status icon to get the full details of the job<br/>
       * click the right Arrow to see the Expand screen<br/>
       * Uncheck the checkbox on the top, to see only unallocated Jobs 	<br/>
       <br/>
       * Press Escape to Close this Instrouctions Box<br/>
       </td></tr></table></td>
  <td><table style="border-right:solid lightgrey;overflow:scroll;max-height:450px;overflow-x:hidden" valign="top"><tr><td> 
  <h5>Status Icons</h5></td></tr>
	<tr><td style="width: 2%;height: 2%;"><input type="button" style="background-color: yellow;width: 28px;height: 26px;cursor: pointer;"/>-Dispatching Job</tr>
	<tr><td><img alt="" src="images/Dashboard/operatorYellow.png" style="width: 28px;height: 26px;"/>-Dispatching Shared Jobs</tr>
	<tr><td><input type="button" style="background-color: #FF0000;width: 28px;height: 26px;cursor: pointer;"/>-Can't Find Drivers</tr>
	<tr><td><img alt="" src="images/Dashboard/operatorRed.png" style="width: 28px;height: 26px;"/>-Cant Find Drivers For Shared Jobs</tr>
	<tr><td><img alt="" src="images/Dashboard/AllocatedJob.png" style="width: 28px;height: 26px;"/>-Job Allocated</tr>
	<tr><td><img alt="" src="images/Dashboard/OnRouteJob.png" style="width: 28px;height: 26px;"/>-On Route To Pickup</tr>
	<tr><td><img alt="" src="images/Dashboard/OnSiteJob.png" style="width: 28px;height: 26px;"/>-Driver OnSite</tr>
	<tr><td><img alt="" src="images/Dashboard/tripStatrtedJob.png" style="width: 28px;height: 26px;"/>-Trip Started</tr>
	<tr><td><img alt="" src="images/Dashboard/soonToComplete.png" style="width: 28px;height: 26px;"/>-Soon To Complete</tr>
	<tr><td><img alt="" src="images/Dashboard/redphone.gif" style="width: 28px;height: 26px;"/>-Driver Reported NoShow</tr>
	<tr><td><img alt="" src="images/Dashboard/h.png" style="width: 28px;height: 26px;"/>-Allocated but driver not available now</tr>
	<tr><td><img alt="" src="images/Dashboard/megaphone_red.png" style="width: 28px;height: 26px;"/>-Job Broadcasted</tr>
  <tr><td><h5>Details For Driver Details</h5></td></tr>
  <tr><td>* Right Click the Cab/Driver and get the ContextMenu</td></tr>
  <tr><td>* To Know More , click right Arrow/Left Arrow at the Top</td></tr>
  <tr><td>* To get the total No of Flags, Click get Details button</td></tr>
  <tr><td>* To get the Total Jobs For the driver on that day, click the grey button at the top</td></tr>
  <tr><td>* To Send multiple text Message/Voice message, select Check Box and send</td></tr>
  <tr><td>* If you want to search based on driver or cab, type it and click the search icon</td></tr>
  </table></td>
<!--   <td><table style="border-right:solid lightgrey;overflow:scroll;max-height:450px;overflow-x:hidden"><tr><td><h5>Details For Zone Waiting</h5></td></tr></table></td>
 --><!--   <td><table style="border-right:solid lightgrey;overflow:scroll;max-height:450px;overflow-x:hidden"><tr><td><h5>Details For Caller ID</h5></td></tr></table></td> -->
  <td><table style="overflow:scroll;max-height:450px;overflow-x:hidden"><tr><td>
  <h5>Details For Options </h5></td></tr>
  <tr><td>* Type the seconds in the Autorefresh Time and press Tab</td></tr>
  <tr><td>* To change the Map Zoom Level, choose the number in the dropdown box</td></tr>
  <tr><td>* To change individual Zoom level(with infowindow), choose the value in the drop down box</td></tr>
  <tr><td>* Press On or off to switch the AutoRefresh time of the Dashboard which is on the leftmost corner</td></tr>
  <tr><td>* To set the Default Location, go to the place and zoom it, and click the button</td></tr>
  <tr><td>* If you want to clear all the values to Default, Click clear button</td></tr>
  <tr><td>* To Show Voice Message, Click Show BullHorn button</td></tr>
   </table></td></tr></table>
       </div>
    </div>
  <form name="cookieSubmit" id="cookieSubmit" action="DashBoard?event=submitCookies" method="post">
    <div id="cookieValueHidden"></div>
		 <div class="jqmWindow" id="showFullRoute"></div>
<%if(cookieValues!=null&&cookieValues.size()>0){
	for(int i=0;i<cookieValues.size();i++){%>
<input type="hidden" name="<%=cookieValues.get(i).getName() %>" id="<%=cookieValues.get(i).getName() %>" value ="<%=cookieValues.get(i).getValue() %>">
<%}} %>
</form>
<div id ="consoleUp" style="position: absolute; margin-left: 20px; margin-top: 320px; left: -14px; top: 251px;display:none">
	<img id="dragConsole" src="images/Dashboard/icon_drag.png" align="right" style="cursor: move;width:13px">
	<div id="console" style="font-size:11.0px; width: 230px; color: green; height:31px;overflow-y:auto;"></div>
   </div>
      <div id="messageTemplates" class="jqmWindow" style="width:700px;z-index:3000"><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeMessageTemplate()"></div>
  
  <div id="EmergencyDiv" style="display:none;background-color:lightblue;-webkit-border-radius: 20px;-moz-border-radius: 20px;border-radius: 20px;width:300px;height:120px;border:8px solid black;margin-left: -30%;height: 30%;" >
<p><img src="images/Dashboard/e-lightbar.gif" style="height:80px"></p>
<!-- <div id="mapForDriverEmc" style="width:900px;height:400px"></div>
 -->
<div id="emcDetails" style="background-color:lightgray;border-radius:8px solid black"></div>
<textarea id="replyToEmc" placeholder="Response to this Driver !!"></textarea>
<input type="hidden" name="driverEmc" id="driverEmc" value="">
<input type="hidden" name="msgIdEmc" id="msgIdEmc" value="">
<input type="button" name="sendRes" id="sendRes" value="Send" onclick="sendEmcSms()">
<a id="statusResEmc"></a>
<input type="hidden" name="emcSender" id="emcSender" value="">
<input type="hidden" name="emcReciever" id="emcReciever" value="">
<marquee id="showIndividualMsg" behavior="scroll" scrollAmount="3"  onmouseout="this.start()"  onmouseover="this.stop()"  onmousedown="this.stop();" onmouseup="this.start();" hspace="0" scrolldelay="0" ></marquee>
<input type="button" name="closeEMC" id="closeEMC" value ="close" onclick="closePopUpEMC();cancelMap();Init()">
</div>
   <input type="hidden" name="lastNotifiMsgValue" id="lastNotifiMsgValue" value="0"/>

  <div id="footerDiv" style="height:7%;width:100%;display:none">
   <table style="width: 100%;height:100%"><tr><td width="8%"  bgcolor=" #3FBD13" align="center"   style="border-right:thick solid gold;-webkit-border-top-left-radius: 10px;-webkit-border-bottom-left-radius: 10px;-oz-border-radius-topleft: 10px;-moz-border-radius-bottomleft: 10px;border-top-left-radius: 10px;border-bottom-left-radius: 10px;"><img align="right" alt="" src="images/Dashboard/close.png" onclick="removeFooter()"><img src="images/Dashboard/messageHistory.png" class="Message" style="width:34px;height:36px"><img src="images/Dashboard/settingsForMsg.png"  style="width:34px;height:30px" class="options" onclick="createCookieForOptions()"></td>
 	<td align="center"><div style="background-color: #3FBDD6;height:100%" id="marqueueTd"><marquee behavior="scroll" scrollAmount="3"  id="showNotiMsg" onmouseout="this.start()"  onmouseover="this.stop()"  onmousedown="this.stop();" onmouseup="this.start();" hspace="0" scrolldelay="0"></marquee></div></td><td width="8%" bgcolor="#3FBD13" style="border-left:thick solid gold;-webkit-border-top-right-radius: 10px;-webkit-border-bottom-right-radius: 10px;-moz-border-radius-topright: 10px;-moz-border-radius-bottomright: 10px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;"><img src="images/Dashboard/showOrHideMarqueue.png" onclick="showOrHideMarqueue()"  style="width:24px;height:20px">   <img src="images/Dashboard/decrease.png" onclick="decreaseSpeed()" style="width:14px;height:16px"><img src="images/Dashboard/increase.png" onclick="increaseSpeed()" style="width:14px;height:16px"><img src="images/Dashboard/stop.png" onClick="javascript:document.getElementById('showNotiMsg').stop();" style="width:14px;height:16px"><img src="images/Dashboard/start.png" onClick="javascript:document.getElementById('showNotiMsg').start();" style="width:14px;height:16px"></td></tr></table>
 </div>
 
 
   
<div id="sendmsg" class="jqmWindow" style="display:none;height:200px;width:250px">
     <center><h3>Message For Drivers</h3></center>
     <input type="hidden" name="jobMessage" id ="jobMessage" value="ATTENTION-URGENT!! ALL AVAILABLE DRIVERS, PLEASE LOG IN!! HOLDING CALLS NEED HELP!! THANKS FOR YOUR SUPPORT!!">
     <table style="width: 100%;background-color: #3FBDD6;">
     <tr><input type="button"  value="Append Job Message" align="center" onclick="appendJobMessage()"/></tr>
<tr>
<textarea align='center'  rows="5" maxlength="110" id="message1" value="message" placeholder='Type your Message here' onFocus='clearStatus()' ></textarea>
</tr>
<tr>
<input type="button"  value="Send" align="center" onclick="sendSmsforDrivers()"/>
</tr></table>
</div>	
</div>
   <div id="option" style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right; width: 152px; height: 193px; position: absolute; top: 55pt; left: 637pt;">
     <table><tr style="background-color:#81DAF5;"><td>Multiple Trips</td><td> <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeScreen()"></td></tr>
   <tr><td><input type="button" name="fleetMtrip" id="fleetMtrip" value="Change fleet" onclick="changeMfleet()"/></td></tr>
   <tr><td><input type="button" name="rDispatchMtrip" id="rDispatchMtrip" value="ReDispatch" onclick="reDispatchjob()"/></td></tr>
   <tr><td><input type="button" name="zoneMtrip" id="zoneMtrip" value="Change Zone" onclick="Changezone()"/></td></tr>
   <tr><td><input type="button" name="cCancelMtrip" id="cCancelMtrip" value="CustomerCancel" onclick="cCancel()"/></td></tr>
   <tr><td><input type="button" name="oCancelMtrip" id="oCancelMtrip" value="OperatorCancel" onclick="opeCancel()"/></td></tr>
   <tr><td><input type="button" name="driverCompleteMtrip" id="driverCompleteMtrip" value="DriverCompleted" onclick="driverCompleted()"/></td></tr>
   <tr><td><input type="button" name="removedntdisp" id="removedntdisp" value="Dont Dispatch" onclick="remDntDispatchjobs()"/></td></tr>
   <tr><td><input type="button" name="ChangeRating" id="ChangeRating" value="ChangeRating for Jobs" onclick="changeRatingJobs()"/></td></tr>
  
    </table></div>
   <div id="changeFleetjob" style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right; width: 120px; height: 30px; position: absolute; top: 206pt; left: 540pt;">
    <img align="right" alt="" src="images/Dashboard/close.png" onclick="remFleet()">
                  <select name="fleetCV" id="fleetCV" >
                      <%if(fleetList!=null){
       for(int k=0;k<fleetList.size();k++){ %>
       <option value="<%=fleetList.get(k).getFleetNumber()%>" ><%=fleetList.get(k).getFleetName() %></option>
<% }}%>
        </select>
     <input type="hidden" name="tripForanother" id="tripForanother" />
 <input type="hidden" name="no" id="no" value=""/>
 
 <input type="button" name="changeFleetD" id="changeFleetD" value="Change" onclick="ChangeFleetToMulJob()"/>
  </div>
      <div id="changeRatings" style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right; width: 120px; height: 30px; position: absolute; top: 206pt; left: 540pt;">
    <img align="right" alt="" src="images/Dashboard/close.png" onclick="remFleet()">
                <table><tr><td>Rating
				<select  id="jobRates">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				</select></td></tr></table>
  <button onclick="changeRating()">submit</button>
 				</div>    
   
    <div id="ChangeZone"style="background-color:lightgray;-moz-border-radius: 8px 8px 8px 8px;display: none;align: right;width: 21%; height: 12%;  position: absolute; top: 243pt; left: 471pt;">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="remZone()"> 
 <table><tr><td id="zoneName" style="white-space:nowrap"></td></tr>
 <tr><td style="white-space:nowrap"></td></tr>
 <tr><td>Select Zone</td><td><select id="zone" name="zone">
 <%for(int i=0;i<allZonesForCompany.size();i++){ %>
 <option value="<%=allZonesForCompany.get(i).getZoneKey()%>">
 <%=allZonesForCompany.get(i).getZoneDesc()%></option><%} %>
 </select></td></tr>
 <tr><td align="center" colspan="3"><input type="button" name="book" id="book" value="change Zone" onclick="changeZone()"></td></tr>
 </table>
  </div>
   <div id="ORJobHistory" class="jqmWindow" style=" display: none; align: center; height:330px;width: 70%; position: absolute;overflow:auto;">
   <table id="historyValues" style="background-color: #4EE2EC;-moz-border-radius:12px 12px 12px 12px;align: center;left:5px; height:330px;width: 550px;">
     <img align="right" alt="" src="images/Dashboard/close.png" onclick="remjobhistory()"/> 
   </table>			
   </div>

   <div id="drZoneActivity" class="jqmWindow" style=" display: none; align: center; height:330px;width: 70%; position: absolute;overflow:auto;">
   <table id="activityValues" style="background-color: #4EE2EC;-moz-border-radius:12px 12px 12px 12px;align: center;left:5px; height:330px;width: 550px;">
     <img align="right" alt="" src="images/Dashboard/close.png" onclick="remZnActivity()"/> 
   </table>			
   </div>
 
</body>
</html>
