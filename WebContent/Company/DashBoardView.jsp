<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.dao.AdministrationDAO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%><html>
<head>
<style type="text/css">
#ele_li li {
	background-image: url(images/li_img.png);
	list-style-type: none;
	padding-left: 18px;
	background-repeat: no-repeat;
	margin-top: 3px;
}
</style>
<link href="css/newtdsstyles.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Control Center</title>
<script type="text/javascript">
	var mins, secs, TimerRunning, TimerID, TimerID1;
	TimerRunning = false, i = 1;

	function Init() 
	{
			mins = 1;
		secs = 0;
		StopTimer();
	}
	function StopTimer() {
		if (TimerRunning)
			clearTimeout(TimerID);
		TimerRunning = false;
		secs = document.getElementById('sec').value;
		StartTimer();
	}

	function StartTimer() {
		TimerRunning = true;
		document.getElementById('tid').innerHTML = "<tr>Time Remaining:"+ Pad(secs) + "</tr>";
		TimerID = self.setTimeout("StartTimer()", 1000);
		if (secs == 0) {
			StopTimer();
		}
		secs--;
	}
	function Check() {
		if (mins == 5 && secs == 0)
			alert("You have only five minutes remaining");
		else if (mins == 0 && secs == 0) {
			alert("Your alloted time is over.");
		}
	}
	function Pad(number)
	{
		if (number < 10)
			number = 0 + "" + number;
		return number;
	}
	function setOpacity(value) {
		document.getElementById("styled_popup").style.opacity = value / 10;
		document.getElementById("styled_popup").style.filter = 'alpha(opacity='
				+ value * 10 + ')';
	}
	function fadeInMyPopup() {
		for ( var i = 0; i <= 100; i++)
			setTimeout('setOpacity(' + (i / 10) + ')', 8 * i);
	}
	function fadeOutMyPopup() {
		for ( var i = 0; i <= 100; i++) {
			setTimeout('setOpacity(' + (10 - i / 10) + ')', 8 * i);
		}
		setTimeout('closeMyPopup()', 800);
	}
	function closeMyPopup() {
		document.getElementById("styled_popup").style.display = "none"
	}
	function fireMyPopup() {
		setOpacity(0);
		document.getElementById("styled_popup").style.display = "block";
		fadeInMyPopup();
	}
	function removeTime(){
		clearTimeout(TimerID);
		document.getElementById('tid').innerHTML="";
	}
</script>
<script type="text/javascript" src="Ajax/DashBoard.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
function loginTime(){
	$(".zone a").hover(function() {
		$(this).find("em").animate({
			opacity : "show",
			left:"45"
		}, "slow");
	},  function() {
		$(this).find("em").animate({
			opacity : "hide",
			left:"65"
		}, "fast");
	} );
}

	$(document).ready(function() {
		$(".menu a").hover(function() {
			$(this).next("em").animate({
				opacity : "show",
				top : "-75"
			}, "slow");
		}, function() {
			$(this).next("em").animate({
				opacity : "hide",
				top : "-85"
			}, "fast");
		});
	});
	$(document).ready(function() {
		$("#navigation a").hover(function() {
			$(this).find("em").animate({
				opacity : "show",
				left: "115"
			}, "slow");
		}, function() {
			$(this).find("em").animate({
				opacity : "hide",
				left: "105"
			}, "fast");
		});
	});


</script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/dashboardHeader.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="css/dasboardLeftNav.css" type="text/css"charset="utf-8" />
<script type="text/javascript" src="Ajax/DashBoard.js"></script>
<style type="text/css">
</style>
</head>
<body>
	<form name="masterForm" action="control" method="post">
		<input type="hidden" name="assoccode" id="assoccode"
			value="<%=((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode()%>">
		<div class="dashboardDV">
			<ul class="menu">
				<li><a href="" onClick='openCC()'>Process&nbsp;Credit&nbsp;Card</a>
					<em>Click to Process the Credit Card </em>
				</li>
				<li><a href="" onClick='openSMS()'>Send&nbsp;Message</a> 
					<em>Click to Send the SMS and Email</em>
				</li>
				<li><a href="" onClick='openDD()'>Driver&nbsp;Deactivation</a>
					<em>Click to Deactivate the Driver temporarily</em>
				</li>
				<li><a href="" onClick='openOR()'>Open&nbsp;Request</a> 
					<em>Click to create a Open Request</em>
				</li>
				<li><a href="" onClick='openReport()'>Report</a> 
					<em>Click to Create the Report</em>
				</li>
				<li><a href="" onClick='javascript:window.close();'>Close</a> 
					<em>Click to Close the Dashboard</em>
				</li>
				<li><a href="" onClick='openDriverLocation()'>Driver&nbsp;Location</a>
					<em>Click to Find the driver location</em>
				</li>
				<li><a href="" onClick='fireMyPopup()'>Driver&nbsp;Shortest&nbsp;Path</a>
					<em>Click to find the Shortest Path</em>
				</li>
				<li><img src="images/taxy.jpg" height="60" width="60"> <em>GetACab</em>
				</li>
			</ul>
			
			<ul id="navigation">
				<li class="Waiting"><a><input type="button" align="middle"
						name="Waiting" id="Waiting" onclick="Init(); dashBoardAjax('1');">
						<em>Zone Waiting</em>
				</a>
				</li>
				<li class="OpenRequest"><a><input type="button"
						align="middle" id="OpenRequest" onclick="removeTime();dashBoardAjax('2');" >
				<em>OpenRequest</em>
				</a>
				</li>
				<li class="AllRequest"><a href="/TDS/DashBoard?event=dispatchValues"></a><input type="button" align="middle" id="AllRequest" ">
				<em>Other Request</em>
				</a>
				</li>
			</ul>
			<tr>
				<td width="750" colspan="2" align="right" valign="top">
					<div id="wid"></div>
					<div id="tid"></div></td>
			</tr>
			<table width="85%" align="center" style="margin-left:100px; ">
				<tr>
					<td><input type="hidden" name="sec" id="sec" value="20">
					</td>
				</tr>
				<tr>
					<td align="right"><div id='rid'></div>
				</tr>
				<tr>
					<td valign="top" align="center" width="100%">
						<div class="dashboardDataTable">
							<div id="queueid"></div>
						</div></td>
				</tr>
				
				<tr>
					<td>
						<div id='styled_popup' name='styled_popup'
							style='width: 380px; height: 300px; display: none; position: absolute; top: 150px; left: 50px; zoom: 1'>
							<table width='380' cellpadding='0' cellspacing='0' border='0'>
								<tr>
									<td><img height='23' width='356'
										src='images/x11_title.gif'>
									</td>
									<td>
										<div style="margin-left: -25px;">
											<a href='javascript:fadeOutMyPopup();'> <img height='23'
												width='24' src='images/x11_close.gif' border='0'>
											</a>
										</div></td>
								</tr>
								<tr>
									<td>
										<div
											style='background: url("images/x11_body2.gif") no-repeat top left; width: 380px; height: 277px;'>
											<table>
												<tr>
													<td colspan='2'>
														<ul compact="compact" type="disc">
															<div id="ele_li">
																<li>Stop Zone Process</li>
																<li>Remove Drivers From Zone Login</li>
															</div>
														</ul></td>
												</tr>
												<tr>
													<td colspan="2" align="center"><input type="button"
														value="Yes" class="lft"
														onclick="openDriverPath();fadeOutMyPopup();"><input
														type="button" value="No" class="lft"
														onclick="fadeOutMyPopup();">
													</td>
												</tr>
											</table>
										</div></td>
								</tr>
							</table>
						</div></td>
				</tr>
				<tr>
				</tr>
			</table>
		</div>
	</form>

</body>
</html>