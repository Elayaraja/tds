<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.TransactionSummaryBean"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO;"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" rel="stylesheet"
	href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css"
	media="screen"></link>
<script type="text/javascript"
	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>TDS (Taxi Dispatch System)</title>

<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>


<script type="text/javascript">
function makeActive(driverid)
{
	 window.location.href = "/TDS/control?action=admin&event=getDeActiveSummary&activate=yes&module=operationView&driverid="+driverid;
}
 
</script>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var deactivatedriver = document.getElementById("deactivatedriver");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title> Deactivation Summary Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(deactivatedriver.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>

</head>
<body>
	<form method="post" action="control" name="masterFormdriverSummary">
		<%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
%>
<div align="center">
		<input type="hidden" name="action" id="action" value="admin" /> 
		<input type="hidden" name="event" id="event" value="getDeActiveSummary" /> 
		<input type="hidden" name="module" id="module" value="<%=request.getParameter("module") %>" />
		
			<c class="nav-header"><center> Deactivation Summary</center></c>
				<br /><br />
				<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
						<td>Driver ID</td>
						<td>
						<input type="text" name="driverId" id="driverId" value="" autocomplete="off" onchange="hidePopup()" />							 
							<ajax:autocomplete
				  					fieldId="driverId"
				  					popupId="model-popup1"
				  					targetId="driverId"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing"
				  					 />  
                    	</td>
						</tr>
						<tr>
						<td>From Date</td>
						<td><input type="text" name="fromDate" id="fromDate"  onfocus="showCalendarControl(this);" />
						</td>
						</tr>
						<tr>					
						<td>To Date</td>
						<td><input type="text" name="toDate" id="toDate" value="" onfocus="showCalendarControl(this);" />
						</td>
						</tr>
						<tr>
							<td align="center"><input type="submit" name="search" value="Search" />
	                             		<input type="button" value="Save/Print" id="btnPrint" />
	
							</td>
		
  
						</tr>					
				</table>				
				<br /><br />
				<%=request.getAttribute("page")==null?"":request.getAttribute("page") %>
				<%if(request.getAttribute("al_list")!=null){ 
					ArrayList<DriverDeactivation> al_list=(ArrayList<DriverDeactivation>)request.getAttribute("al_list");
					%>
				<table width="90%" border="1" cellspacing="0" cellpadding="0" id="deactivatedriver" style="background-color:white">
					<tr align="center" style="background-color: lightblue">
						<th width="5%">S.No</th>
						<th width="15%">Driver ID</th>
						<th width="15%">From Date</th>
						<th width="15%">To Date</th>
						<th width="15%">Entered Date</th>
						<th width="15%">Entered By</th>
						<th width="25%">Description</th>
						<th width="25%">Details</th>
					</tr>
					<%boolean colorblue=true;
					String color;
					if(al_list.size()>0){						
					for(int count=0;count<al_list.size();count++){
						colorblue=!colorblue;
						if(!colorblue){
							color="style=\"background-color:lightblue\"";							
						}else{
							color="";
						}
					//DriverDeactivation deactivation = (DriverDeactivation)al_list.get(count);
					al_list.get(count);
					%>
					<tr <%=color %> align="center">
						<td><%=count+1%></td>
						<td><%=al_list.get(count).getDriver_id() %></td>
						<td><%=al_list.get(count).getFrom_date() %></td>
						<td><%=al_list.get(count).getTo_date() %></td>
						<td><%=al_list.get(count).getEntered_date()%></td>
						<td><%=al_list.get(count).getUser()%></td>
						<td><%=al_list.get(count).getDesc()%></td>
						<td>
							<div class="wid60 marAuto padT10">								
										<input type="button" value="Activate" class="lft"
											onclick="makeActive('<%=al_list.get(count).getDriver_id() %>')" />
									</div>									
						</td>
					</tr>
					
					<%} }
					
					else{
					%>
					<tr></tr>
					<tr>
					<td colspan="8" align="center">	No Records Found
					</td>
					</tr>
				<%} }%>
				</table>

	</div>
	<div>	
		<c>Copyright &copy; 2010 Get A Cab</c>
	</div>
	</form>
</body>
</html>
