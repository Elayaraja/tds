<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.security.bean.TagSystemBean"%>
<%@page import="com.tds.cmp.bean.ZoneTableBeanSP"%>
<%@page import="com.lowagie.text.Document"%>
<%@page import="com.tds.dao.AdministrationDAO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<%@page import="com.tds.tdsBO.DriverLocationBO"%>
<%@page import="com.tds.process.QueueProcess"%>
<%@page import="java.util.ArrayList"%>
<%@page import=" java.util.Timer"%>
<%@page import="com.common.util.TDSProperties"%>
<%
	ArrayList<ZoneTableBeanSP> allZones=(ArrayList<ZoneTableBeanSP>) request.getAttribute("zones");
%>
<%
	ArrayList Latitudes= (ArrayList) request.getAttribute("latitude");
%>
<%
	ArrayList Longitudes= (ArrayList) request.getAttribute("longitude");
%>
<%
	ArrayList centerLat= (ArrayList) request.getAttribute("centreLatitude");
%>
<%
	ArrayList centerLon= (ArrayList) request.getAttribute("centreLongitude");
%>
<%
	String defaultZone =(String)request.getAttribute("DefaultZone");
%>
<%
	ArrayList<TagSystemBean> cookieValues = (ArrayList<TagSystemBean>)request.getAttribute("CookieValues");
%>
<%
	ArrayList<OpenRequestBO> jobs = (ArrayList<OpenRequestBO>)request.getAttribute("Jobs");		
   ArrayList<DriverLocationBO> drivers = (ArrayList<DriverLocationBO>)request.getAttribute("Drivers");
   AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
   String driverOrCab="";
  if(adminBO.getDispatchBasedOnVehicleOrDriver()==1){
     driverOrCab="Driver";}else{driverOrCab="Cab";
   }
%>
<%
	ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getAssociateCode() +"Zones"));
%>
<%String dashboardCheck ="";
if(request.getAttribute("dashboard")!=null){
dashboardCheck = (String) request.getAttribute("dashboard");} %>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<html>
<head>
<title>TDS Dashboard</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- All css links -->
<link rel="stylesheet" href="css/jquery.contextMenu.css"></link>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link href="css/newtdsstyles.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/DashboardNew.css?vNo=<%=TDSConstants.versionNo %>">
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link type="text/css" rel="stylesheet" 	href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" 	media="screen"></link>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<link rel="stylesheet" href="css/colorpicker.css" type="text/css" />
<!-- All Js Files -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript" src="Ajax/SystemUtilAjax.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/ContextMenu.js"></script>
<script type="text/javascript" src="DashboardAjax/DispatchOSM.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="DashboardAjax/DispatchCommon.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="DashboardAjax/DispatchCommon1.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" src="Ajax/DashBoard.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="Ajax/Common.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script src="js/jquery.contextMenu.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/jquery.bpopup-0.7.0.min.js"></script>
<script type="text/javascript" src="js/colorpicker.js"></script>
<script type="text/javascript" src="js/eye.js"></script>
<script type="text/javascript" src="js/utils.js"></script>
<script type="text/javascript" src="js/layout.js?ver=1.0.2"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/OpenLayers.js"></script>


<%
	ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>();
%>
<%
	if(session.getAttribute("fleetList")!=null) {
	fleetList=(ArrayList)session.getAttribute("fleetList");
	}
%>

<!-- All External Css -->

<script type="text/javascript">
	/* function sumbitValueFromDash(){
	 document.forms["cookieSubmit"].submit();
	 $("#pbar").jqm();
	 $("#pbar").jqmShow();
	 document.getElementById("pbar").innerHTML = "<center><h6><font color=\"blue\">Saving Your Preferences!!</h6></b></center></br>Please Wait...<font color=\"lime\"size=\"2px\"> Storing : <img src=\"images/Dashboard/progress2.gif\">"+ "<font size=\"1px\" ><b><a>" + getBar()+"</a></b></font></font></font>";

	 window.status = "Please Wait the Page was Loading...";
	 //document.cookieSubmit.submit();
	 }
	 document.writeln("<div class=\"jqmWindow\" id = \"pbar\" ></div>");

	 var percent = 10;   // adjust starting value to suit
	 var timePeriod = 100;  // adjust milliseconds to suit

	 function getBar() {
	 var retBar = '';
	 for (i = 0; i < percent; i++) {
	 retBar += "";
	 }
	 return retBar;
	 }

	 function progressBar() {
	 if (percent < 100) {
	 percent = percent + 1;
	 $("#pbar").jqm();
	 $("#pbar").jqmShow();
	 document.getElementById("pbar").innerHTML = "<center><h6><font color=\"blue\">WelCome to Operator Dashboard</h6></b></center></br>Please Wait...<font color=\"lime\"size=\"2px\"> Loading : <img src=\"images/Dashboard/progress2.gif\">"+ "<font size=\"1px\" ><b><a>" + getBar()+"</a></b></font></font></font>";
	 window.status = "Loading : " + percent + "%" + " " + getBar();
	 google.maps.event.addListener(map, 'idle', function() { 
	 percent=100;
	 setTimeout ("progressBar()", timePeriod); 
	
	 } );
	 setTimeout ("progressBar()", timePeriod); 

	 }
	 else {
	 $("#pbar").jqmHide();
	 document.getElementById("pbar").innerHTML = "";
	 window.status = "Please Wait the Page was Loading...";
	 document.body.style.display = "";
	 }
	 }
	 progressBar(); */
	$(document).ready(function() {
		onloadOSM();
		 geocoder = new google.maps.Geocoder();

		$("#addressDash").autocomplete(
				{
					//This bit uses the geocoder to fetch address values
					source : function(request, response) {
						geocoder.geocode({
							'address' : request.term + ',' + defaultState + ','
									+ defaultCountry
						}, function(results, status) {
							response($.map(results, function(item) {
								return {
									label : item.formatted_address,
									value : item.formatted_address,
									latitude : item.geometry.location.lat(),
									longitude : item.geometry.location.lng(),
									city : item.postal_code,
									addcomp : item.address_components
								//nhd: item.address_components_of_type("neighborhood")
								}
							}));
						})
					},
					//This bit is executed upon selection of an address
					select : function(event, ui) {

						var arrAddress = ui.item.addcomp;
						var streetnum = "";
						var route = "";

						// iterate through address_component array
						$.each(arrAddress, function(i, address_component) {

							if (address_component.types[0] == "street_number") {
								$("#sadd1Dash").val(address_component.long_name);
								streetnum = address_component.long_name;
								//console.log('HK'+document.getElementById("sadd1").value);
							}

							if (address_component.types[0] == "locality") {
								$("#scityDash").val(address_component.long_name);
							}

							if (address_component.types[0] == "route") {
								route = address_component.long_name;
							}
							if (address_component.types[0] == "country") {
								itemCountry = address_component.long_name;
							}

							if (address_component.types[0] == "postal_code") {
								itemPc = address_component.long_name;
								$("#szipDash").val(address_component.long_name);
							}

							//return false; // break the loop

						});

						$("#sadd1Dash").val(streetnum + " " + route);
						$("#sLatitudeDash").val(ui.item.latitude);
						$("#sLongitudeDash").val(ui.item.longitude);

						//$("#street").val(ui.item.street);
						//$("#state").val(ui.item.address_components_of_type("neighborhood"));
						$("#addressDash").autocomplete("close");
						document.getElementById("addressKey").value = "";
					}
				}); 
		geocoder = new google.maps.Geocoder();
		$("#DropAddressDash").autocomplete({
			//This bit uses the geocoder to fetch address values
			source: function(request, response) {
				geocoder.geocode( {'address': request.term +','+defaultState+','+defaultCountry  }, function(results, status) {
					response($.map(results, function(item) {
						return {
							label: item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							city: item.postal_code,
							addcomp: item.address_components
							//nhd: item.address_components_of_type("neighborhood")
						}
					}));
				})
			},
			//This bit is executed upon selection of an address
			select: function(event, ui) {

				var arrAddress =ui.item.addcomp;
				var streetnum= "";
				var route = "";


				// iterate through address_component array
				$.each(arrAddress, function (i, address_component) {

					if (address_component.types[0] == "street_number"){
						$("#eadd1Dash").val(address_component.long_name);
						streetnum = address_component.long_name;
						//console.log('HK'+document.getElementById("sadd1").value);
					}

					if (address_component.types[0] == "locality"){
						$("#ecityDash").val(address_component.long_name);
					}

					if (address_component.types[0] == "route"){ 
						route = address_component.long_name;
					}
					if (address_component.types[0] == "country"){ 
						itemCountry = address_component.long_name;
					}


					if (address_component.types[0] == "postal_code"){ 
						itemPc = address_component.long_name;
						$("#ezipDash").val(address_component.long_name);
					}

					//return false; // break the loop

				});

				$("#eadd1Dash").val(streetnum + " " + route);
				$("#eLatitude").val(ui.item.latitude);
				$("#eLongitude").val(ui.item.longitude);
				$("#DropAddressDash").autocomplete("close");
				//$("#street").val(ui.item.street);
				//$("#state").val(ui.item.address_components_of_type("neighborhood"));
			}
		});
	});
	 
	 $(document).ready ( function(){
			onloadDash();
		    $('.dashboard').css('height', $(window).height()/1.17+'px');
		/*         $('#outer-div').css('height', $(window).height()+'px'); */
		    $('#map').css('height', $(window).height()/1.12+'px');
		//$('#sendSms').css('height',$(window).height)/3+'px');
		    //$('#top-header').css('height', $(window).height()/30+'px'); 
		   // $('#showHover').css('margin-left:', $(window).height()/9+'px');
		    $('#showHover').css('margin-top:', $(window).height()/10+'px'); 
		    	$('#popUpAddress').css('margin-top:', $(window).height()/17+'px');
		   // $('#popUpAddress').css('margin-left:', $(window).height()/12+'px'); top: 252px;
		    	//$('#bullHorn').css('margin-top:', -$(window).height()/4+'px');
		}
		);

		
	 
</script>
</head>
<body onload="onloadOSM();getAllValuesForDashboard('11111')">
	<!-- All hidden Fields -->
	<input type="hidden" name="dispatch" id="dispatch"
		value="<%=adminBO.getDispatchBasedOnVehicleOrDriver()%>">
	<input type="hidden" name="assoccode" id="assoccode"
		value="<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()%>">
	<input type="hidden" name="defaultLatitude" id="defaultLatitude"
		value="<%=adminBO.getDefaultLati()%>">
	<input type="hidden" name="defaultLongitude" id="defaultLongitude"
		value="<%=adminBO.getDefaultLogi()%>">
	<input type="hidden" name="defaultState" id="defaultState"
		value="<%=adminBO.getState()%>">
	<input type="hidden" name="defaultCountry" id="defaultCountry"
		value="<%=adminBO.getCountry()%>">
	<input type="hidden" name="changeHappeningJob" id="changeHappeningJob"
		value="false">
	<input type="hidden" name="changeHappeningSharedRide" id="changeHappeningSharedRide" value="false">
	<input type="hidden" name="changeHappeningDriver"
		id="changeHappeningDriver" value="false">
	<input type="hidden" name="latForRightclick" id="latForRightclick"
		value="" />
	<input type="hidden" name="longForRightclick" id="longForRightclick"
		value="" />
	<input type="hidden" name="changeHappeningZone"
		id="changeHappeningZone" value="false">
	<input type="hidden" name="currentJobinJObs" id="currentJobinJObs"
		value="">
	<input type="hidden" name="currentJobinDrivers"
		id="currentJobinDrivers" value="">
	<input type="hidden" name="currentDriverinJObs"
		id="currentDriverinJObs" value="">
	<input type="hidden" name="currentDriverinDrivers"
		id="currentDriverinDrivers" value="">
	<input type="hidden" name="latJobs" id="latJobs" value="">
	<input type="hidden" name="longJobs" id="longJobs" value="">
	<input type="hidden" name="latDriver" id="latDriver" value="">
	<input type="hidden" name="longDriver" id="longDriver" value="">
	<input type="hidden" name="deactivateDriver" id="deactivateDriver"
		value="">
	<input type="hidden" name="notAnumber" id="notAnumber" value="">
	<input type="hidden" name="notAnumberB" id="notAnumberB" value="">
	<input type="hidden" name="startLat" id="startLat" value="">
	<input type="hidden" name="startLong" id="startLong" value="">
	<input type="hidden" name="phone0" id="phone0" value=""></input>
	<input type="hidden" name="phone1" id="phone1" value=""></input>
	<input type="hidden" name="phone2" id="phone2" value=""></input>
	<input type="hidden" name="phone3" id="phone3" value=""></input>
	<input type="hidden" name="phone4" id="phone4" value=""></input>
	<input type="hidden" name="phone5" id="phone5" value=""></input>
	<input type="hidden" name="phone6" id="phone6" value=""></input>
	<input type="hidden" name="phone7" id="phone7" value=""></input>
	<input type="hidden" name="phone8" id="phone8" value=""></input>
	<input type="hidden" name="phone9" id="phone9" value=""></input>
	<input type="hidden" name="name0" id="name0" value=""></input>
	<input type="hidden" name="name1" id="name1" value=""></input>
	<input type="hidden" name="name2" id="name2" value=""></input>
	<input type="hidden" name="name3" id="name3" value=""></input>
	<input type="hidden" name="name4" id="name4" value=""></input>
	<input type="hidden" name="name5" id="name5" value=""></input>
	<input type="hidden" name="name6" id="name6" value=""></input>
	<input type="hidden" name="name7" id="name7" value=""></input>
	<input type="hidden" name="name8" id="name8" value=""></input>
	<input type="hidden" name="name9" id="name9" value=""></input>
	<input type="hidden" name="refShared" id="refShared" value="true">
	<input type="hidden" name="dashCheck"  id="dashCheck" value="<%=dashboardCheck %>">
	<input type="hidden" name="callerIdCheck" id="callerIdCheck"
		value="<%=adminBO.getCallerId()%>" />
	<input type="hidden" name="driverOrCabCheck" id="driverOrCabCheck"
		value="<%=driverOrCab%>" />
	<input type="hidden" name="dispType" id="dispType"
		value="<%=adminBO.getDispatchMethod()%>">
	<input type="hidden" name="clickCount" id="clickCount" value="">
	<input type="hidden" name="totalDrivers" id="totalDrivers" value="">
	<input type="hidden" name="sec" id="sec" value="20">
	<input type="hidden" name="secPopUP" id="secPopUP" value="15">
	<input type="hidden" name="zoomValue" id="zoomValue" value="12">
	<input type="hidden" name="singlejobClicked" id="singlejobClicked"
		value="">
	<input type="hidden" name="individualZoomValue"
		id="individualZoomValue" value="20">
	<input type="hidden" name="autoRefreshOnOff" id="autoRefreshOnOff"
		value="" />
	<input type="hidden" name="dragend" id="dragend" value="false" />
	<%
		if(adminBO.getRoles().contains("1038")){
	%>
	<input type="hidden" name="driverPermission" id="driverPermission"
		value="yes">
	<%
		}else{
	%>
	<input type="hidden" name="driverPermission" id="driverPermission"
		value="no">
	<%
		}if(adminBO.getRoles().contains("1037")){
	%>
	<input type="hidden" name="jobsPermission" id="jobsPermission"
		value="yes">
	<%
		}else{
	%>
	<input type="hidden" name="jobsPermission" id="jobsPermission"
		value="no">
	<%
		}if(adminBO.getRoles().contains("1039")){
	%>
	<input type="hidden" name="zonesPermission" id="zonesPermission"
		value="yes">
	<%
		}else{
	%>
	<input type="hidden" name="zonesPermission" id="zonesPermission"
		value="no">
	<%
		}
	%>
	<input type="hidden" name="driverShowCheck" id="driverShowCheck" value=""></input>
	<input type="hidden" name ="distanceBasedOn" id="distanceBasedOn" value="<%=adminBO.getDistanceBasedOn()%>"/>
	
	
	<div id="lastJobRunTime"
		style="display: none; padding-left: 18px; size: 1.6px; color: green;"></div>
	<div id="OptionsForAll"
		style="display: none; z-index: 3000; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0;">
		<div id="all"
			style="background-color: white; filter: alpha(opacity = 80); opacity: 0.9;">
			<div class="block-top55">
				&nbsp;&nbsp;Options<a style="margin-left: 120px" href="#"></a>&nbsp;&nbsp;<a
					href="#"><img alt="" src="images/Dashboard/close.png"
					onclick="removeOptions();removeCookieForOptions()">
				</a>
			</div>
			<div class="job-avail-mid">
				<div class="job-inner1">
					<table>
						<tr>
							<td>Auto Refreshtime</td>
							<td><input type="text" name="sec1" id="sec1" value="20"
								size="1" onchange="createCookieForAutoRefresh();timeSetup()">
							</td>
						</tr>
						<tr>
							<td>Map Zoom Level</td>
							<td><select name="zoom" id="zoom"
								onchange="createCookieForZoom();zoomMap()">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12" selected="selected">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
							</select></td>
							<td><input type="text" size="1" name="currentZoom"
								id="currentZoom" value="" />
							</td>
						</tr>
						<tr>
							<td>Individual Zoom Level</td>
							<td><select name="individualZoom" id="individualZoom"
								onchange="createCookieForIndividualZoom();individualZoom()">
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13" selected="selected">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
							</select></td>
						</tr>
						<tr>
							<td>AutoRefresh</td>
							<td><input type="button" value="On"
								onclick="createCookieForAutoRefreshOnOff('on');Init()">
							</td>
							<td><input type="button" value="Off"
								onclick="createCookieForAutoRefreshOnOff('off');autoRefreshOff()">
							</td>
						</tr>
						<input type="hidden" name="latFromMap" id="latFromMap" value=""
							size="13">
						<input type="hidden" name="longFromMap" id="longFromMap" value=""
							size="13">
						<tr>
							<td colspan="1"><input type="button" name="setDefaultCenter"
								id="setDefaultCenter" value="Set Default Location"
								style="font-size: 10.1px;" onclick="createCookieForCenter()">
							</td>
							<td><input type="button" name="removeCookie"
								id="removeCookie" size="4" value="Clear"
								onclick="removeCookieForDefault()" />
							</td>
						</tr>
						<tr>
							<td colspan="1" align="center"><input type="button"
								name="showBullHorn" id="showBullHorn" value="Show Horn"
								onclick="showVoiceMessageScreen()" />
							</td>

							<td colspan="2" align="center"><input type="image"
								name="StartOrStopTimer" id="StartOrStopTimer"
								style="text-decoration: blink; display: none; text-shadow: gray; color: black;"
								value="" /> <%
 	AdminRegistrationBO adminBo = (AdminRegistrationBO) request
 													.getSession().getAttribute("user");
 											String stopAssoccode = adminBO.getAssociateCode();
 											Timer stopTimer = (Timer) getServletConfig().getServletContext()
 													.getAttribute(stopAssoccode + "Timer");

 										if(stopTimer==null){
 %> <input name="btnStartOrStopTimer"
								id="btnStartOrStopTimer" type="button" class="lft"
								onclick="startOrStopTimerForDashboard()"
								style="size: 1.6px; color: green; top: 0pt; left: 0pt;"
								value="Start Dispatch" /> <%
 	}else{
 %> <input
								name="btnStartOrStopTimer" id="btnStartOrStopTimer"
								type="button" class="lft"
								onclick="startOrStopTimerForDashboard()"
								style="size: 1.6px; color: green; top: 0pt; left: 0pt;"
								value="Stop Dispatch" /> <%
 	}
 %>
							</td>
						</tr>
					</table>
					<div id="lastJobRunTime"
						style="padding-left: 18px; size: 1.6px; color: green;"></div>
					<div id="more" align="center" onclick="showAdvancedOptions()"
						style="cursor: pointer; color: red">- Less</div>
					<div id="advancedOptions"
						style="max-height: 250px; overflow-y: auto">
						<table id="colorchage">
							<tr>
								<td align="left">VIP</td>
								<td align="right"><input type="text" name="vipColor"
									id="vipColor" value="24f511" size="4"
									style="background-color: #24f511; cursor: pointer" />
								</td>
							</tr>
							<tr>
								<td align="left">Timed Call</td>
								<td align="right"><input type="text" name="timeColor"
									id="timeColor" value="f5a211" size="4"
									style="background-color: #f5a211; cursor: pointer" />
								</td>
							</tr>
							<tr>
								<td align="left">Voucher</td>
								<td align="right"><input type="text" name="voucherColor"
									id="voucherColor" value="f20c0c" size="4"
									style="background-color: #f20c0c; cursor: pointer" />
								</td>
							</tr>
							<tr>
								<td align="left">Cash</td>
								<td align="right"><input type="text" name="cashColor"
									id="cashColor" value="0930f2" size="4"
									style="background-color: #0930f2; cursor: pointer" />
								</td>
							</tr>
							<tr>
								<td align="left">Available</td>
								<td align="right"><input type="text" name="avColor"
									id="avColor" value="66FF66" size="4"
									style="background-color: #66FF66; cursor: pointer" />
								</td>
							</tr>
							<tr>
								<td align="left">No Update</td>
								<td align="right"><input type="text" name="noUpdate"
									id="noUpdate" value="ebda1e" size="4"
									style="background-color: #ebda1e; cursor: pointer" />
								</td>
							</tr>
							<tr>
								<td align="left">Not Available</td>
								<td align="right"><input type="text" name="navColor"
									id="navColor" value="FF8888" size="4"
									style="background-color: #FF8888; cursor: pointer" />
								</td>
							</tr>
							<!-- 					<tr><td colspan="2"><input type="button" name="createCookieColors" id="createCookieColors" value="Remember Colors" onclick="createCookieForColors()"></td></tr>
 -->
						</table>
						<table>
							<tr>
								<td colspan="2">Show Zones</td>
								<td><input type="image" width="25" height="25"
									id="zoneCheckImage" src="images/Dashboard/zoneLogin.gif"
									onclick="showAllZonesForDash();">
								</td>
							</tr>
							<tr>
								<td colspan="2">Zone Colors</td>
							</tr>

							<tr>
								<td><input type="text" name="zoneColor1" id="zoneColor1"
									value="FFA500" size="4"
									style="background-color: #FFA500; cursor: pointer" />
								</td>
								<td><input type="text" name="zoneColor2" id="zoneColor2"
									value="FF00FF" size="4"
									style="background-color: #FF00FF; cursor: pointer" />
								</td>
								<td><input type="text" name="zoneColor3" id="zoneColor3"
									value="FF0000" size="4"
									style="background-color: #FF0000; cursor: pointer" />
								</td>
							</tr>

							<tr>
								<td><input type="text" name="zoneColor4" id="zoneColor4"
									value="00FF00" size="4"
									style="background-color: #00FF00; cursor: pointer" />
								</td>
								<td><input type="text" name="zoneColor5" id="zoneColor5"
									value="808000" size="4"
									style="background-color: #808000; cursor: pointer" />
								</td>
								<td><input type="text" name="zoneColor6" id="zoneColor6"
									value="616D7E" size="4"
									style="background-color: #616D7E; cursor: pointer" />
								</td>
							</tr>

							<tr>
								<td><input type="text" name="zoneColor7" id="zoneColor7"
									value="306EFF" size="4"
									style="background-color: #306EFF; cursor: pointer" />
								</td>
								<td><input type="text" name="zoneColor8" id="zoneColor8"
									value="8D38C9" size="4"
									style="background-color: #8D38C9; cursor: pointer" />
								</td>
								<td><input type="text" name="zoneColor9" id="zoneColor9"
									value="F52887" size="4"
									style="background-color: #F52887; cursor: pointer" />
								</td>
							</tr>

							<tr>
								<td><input type="text" name="zoneColor10" id="zoneColor10"
									value="00FF00" size="4"
									style="background-color: #00FF00; cursor: pointer" />
								</td>
							</tr>
						</table>
						<input type="button" name="swOld" id="swOld" value="Switch Map" onclick="switchMapScreen('google')"/>
					</div>
				</div>
				
			</div>
			<img alt="Jobs available" src="images/Dashboard/jobs-avail-bot.png">
		</div>

	</div>




	<table style="width: 100%; height: 100%" cellspacing="0">
		<tr height="10%">
			<td align="center" style="background-color: #46AABD"><img alt=""
				src="DashBoard?event=getCompanyLogo" width="105px" height="55px" />
			</td>

			<td>

				<table id="header" style="width: 100%; height: 100%" cellspacing="0">
					<tr bgcolor="#2E9AFE">
						<td align="center" class="Jobs">Jobs</td>
						<td align="center">Create</td>
						<td align="center" class="ORHistory">Job History</td>
						<td align="center" class="ORSummary">Reservation</td>
						<td align="center" class="zones">Zones</td>
						<td align="center" class="operationStatistics">Statistics</td>
						<td align="center" class="Driver">Cab</td>
						<td align="center" class="options">Options</td>
						<td align="center" class="Message">Message</td>
						<td align="center">Home</td>
						<td align="center" onclick="showInstructions()">Help</td>
						<td colspan="3"><div id="tid" align="left" style=""></div>
							<div id="tid1" align="left"></div>
						</td>
					</tr>
					<tr bgcolor="#46AABD">
						<td align="center" style="border-right: inset blue" class="Jobs"><img
							alt="" src="images/Dashboard/jobs-icon.png"
							onclick="createCookieForJobs()">
						</td>
						<td align="center" style="border-right: inset blue"><img
							alt="" src="images/Dashboard/dispatch-icon.png"
							onclick="createJobORDash()"></td>
						<td align="center" style="border-right: inset blue"
							class="ORHistory"><img alt=""
							src="images/Dashboard/History.png">
						</td>
						<td align="center" style="border-right: inset blue"
							class="ORSummary"><img alt=""
							src="images/Dashboard/EditReservation.png"
							onclick="createCookieForReserve()"></td>
						<td align="center" style="border-right: inset blue" class="zones"><img
							alt="" src="images/Dashboard/zones-icon.png"
							onclick="createCookieForZones()">
						</td>
						<td align="center" style="border-right: inset blue"
							class="operationStatistics"><img alt=""
							src="images/Dashboard/statistics.png"
							onclick="createCookieForStatistics()"></td>
						<td align="center" style="border-right: inset blue"
							onClick="showDriverScreen()"><img alt=""
							src="images/Dashboard/drivers-icon.png"
							onclick="createCookieForDrivers()"></td>
						<td align="center" style="border-right: inset blue"
							class="options"><img alt=""
							src="images/Dashboard/options.png"
							onclick="createCookieForOptions()">
						</td>
						<td align="center" style="border-right: inset blue"
							class="Message"><img alt=""
							src="images/Dashboard/sendSMS.png" onclick="createCookieForMsg()">
						</td>
						<td align="center" style="border-right: inset blue"><a
							href="control"><img alt="" src="images/Dashboard/Home.png">
						</a></td>
						<td align="center" onclick="showInstructions()"><img alt=""
							src="images/Dashboard/help1.png">
						</td>
						<td align="left" id="console1">J:<f id="totalJobs"></f><br />
						</td>
						<%-- <td align="center" id="FleetChnage">
							<%
								if(fleetList!=null && fleetList.size()>0){
							%> <select name="fleet"
							id="fleet">
								<%
									if(fleetList!=null){
									for(int i=0;i<fleetList.size();i++){
								%>
								<option value="<%=fleetList.get(i).getFleetNumber()%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBO.getAssociateCode())?"selected=selected":""%> ><%=fleetList.get(i).getFleetName()%></option>
								<%
									}}
								%>
						</select> <input type="button" name="fleet" id="fleet" value="Change Fleet"
							onclick="changeFleet()" /> <%
 	}
 %>
						</td> --%>
						 <%if(fleetList!=null&&fleetList.size()>0){%>
      <div id="fleetDiv" style="  -moz-border-radius: 8px 8px 8px 8px;align: right; width: 180px; height: 62px; position: absolute; top: 0px;left: 120pt;background-color:khaki;max-height:62px;overflow-y:scroll;overflow-x:auto">
          <table><tr>
	<%for(int i=0;i<fleetList.size();i++){ 
		if(i%2==0){%>
			<tr>
	    <%}%>
				<td>
					<input type="button" name="fleet_<%=i%>" class="fleetbutton"  value ="<%=fleetList.get(i).getFleetName()%>" id ="fleet_<%=i%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBO.getAssociateCode())?"style='background-color:khaki'":""%> onclick="changeFleet('<%=fleetList.get(i).getFleetNumber()%>','<%=i%>')" />
	 			</td>
	<%} %>
          </tr>
         </table></div>
  <%} %>
						
					</tr>

				</table></td>
		</tr>
		<tr height="90%">
			<td id="mapdiv" colspan="2"></td>
		</tr>
	</table>
	<div id="openjobs"
		style="display: none; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0; z-index: 2000">
		<jsp:include page="/Company/JobDetails.jsp" />
	</div>
	<div id="drivers1"
		style="display: none; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0; z-index: 2000">
		<jsp:include page="/Company/DriverDetails.jsp" />
	</div>
	<div id="cancelButton" width="130" height="60"
		style="display: none; z-index: 3000; margin-left: 680px; margin-top: 10px; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0; filter: alpha(opacity = 80); opacity: 0.5;">
		<input type="button"
			value="&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;"
			onclick="cancelMap()"
			style="background-color: red; color: black; font-size: 80%; margin-top: 60px; width: 100px; height: 25px; -moz-border-radius: 8px 8px 8px 8px;"
			class="centerB">
	</div>
	<div id="queueid"
		style="display: none; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0; z-index: 2000">
		<jsp:include page="/Company/ZoneWaiting.jsp" />
	</div>
	<div id="changeFleet"
		style="background-color: lightgray; -moz-border-radius: 8px 8px 8px 8px; display: none; align: right; width: 120px; height: 30px; position: absolute; top: 150pt; left: 300pt; z-index: 2000"">
		<img align="right" alt="" src="images/Dashboard/close.png"
			onclick="removeFleetScreen()"> <select name="fleetV"
			id="fleetV">
			<%
				if(fleetList!=null){
			       for(int i=0;i<fleetList.size();i++){
			%>
			<option value="<%=fleetList.get(i).getFleetNumber()%>"><%=fleetList.get(i).getFleetName()%></option>
			<%
				}}
			%>
		</select> <input type="hidden" name="tripForanother" id="tripForanother"
			value="" /> <input type="button" name="changeFleetD"
			id="changeFleetD" value="Change" onclick="ChangeFleetToAnother()" />
	</div>

	<div id="jobLogsDiv"
		style="background-color: #ceecf5; z-index: 2000; display: none; position: absolute; top: 405px; left: 5px;">
		<img alt="" align="right" src="images/Dashboard/close.png"
			onclick="removeJobsLogsPopUp()">
		<div id="jobLogsPopUp"></div>
	</div>
	<div id="jobHistoryDiv"
		style="background-color: #ceecf5; z-index: 2000; display: none; position: absolute; top: 405px; left: 5px;">
		<img alt="" align="left" src="images/Dashboard/close.png"
			onclick="removeJobsPopUp()">
		<div id="jobhistoryPopUp"></div>
	</div>
	<div id="callDrivers"
		style="background-color: lightgray; -moz-border-radius: 8px 8px 8px 8px; z-index: 2000; display: none; align: right; width: 240px; height: 140px; position: absolute; top: 150pt; left: 300pt;">
		<img align="right" alt="" src="images/Dashboard/close.png"
			onclick="removeMsgToNearestDrivers()"> Distance :&nbsp;<input
			type="text" name="distance" id="distance" value="" size="4">miles
		<br>
		<textarea rows="3" cols="30" name="reasonForMessage"
			id="reasonForMessage"></textarea>
		<br>Send Voice Message<input type="checkbox"
			name="sendVoiceMsgToNearestDriver" id="sendVoiceMsgToNearestDriver"
			value="">
		<center>
			<input type="button" align="middle" name="sendToDriver"
				id="sendToDriver" value="Send" onclick="calculateAndSendMsg()" />
		</center>
	</div>
	<div id="vMessagePopUp"
		style="-moz-border-radius: 8px 8px 8px 8px; z-index: 2000; display: none; align: right; width: 215px; height: 138px; position: absolute; top: 180pt; left: 500pt;">
		<img align="left" class="dragMike" alt=""
			src="images/Dashboard/Drag.png" width="23px" height="23px"> <img
			align="left" alt="" src="images/Dashboard/close.png"
			onclick="removeVMessageBullHorn()">
		<div id="vMessageContent"></div>
	</div>
	<div id="bookToZone"
		style="background-color: lightgray; z-index: 3000; -moz-border-radius: 8px 8px 8px 8px; display: none; align: right; width: 25%; height: 22%; position: absolute; top: 150pt; left: 300pt;">
		<img align="right" alt="" src="images/Dashboard/close.png"
			onclick="rempoveLogintToZone()">
		<table>
			<tr>
				<td id="zoneName" style="white-space: nowrap"></td>
			</tr>
			<tr>
				<td id="zoneDescription" style="white-space: nowrap"></td>
			</tr>
			<tr>
				<td id="driverZone" style="display: none"></td>
			</tr>
			<tr>
				<td style="white-space: nowrap"></td>
			</tr>
			<tr>
				<td>Select Current Zone</td>
				<td><input type="checkbox" name="selectCurrentZone"
					id="selectCurrentZone" value="">
				</td>
			</tr>
			<tr>
				<td>Select Zone</td>
				<td><select id="zoneList" name="zoneList">
						<%
							for(int i=0;i<allZonesForCompany.size();i++){
						%>
						<option value="<%=allZonesForCompany.get(i).getZoneKey()%>">
							<%=allZonesForCompany.get(i).getZoneDesc()%></option>
						<%
							}
						%>
				</select>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="3"><input type="button" name="book"
					id="book" value="Book To Zone" onclick="logintToZone()">
				</td>
			</tr>
		</table>
	</div>

	<div id="zoneLatLongs" style="display: none; z-index: 3000;">
		<table>
			<tr>
				<%
					for(int i=0;i<allZonesForCompany.size();i++){
				%>
				<td><input type="hidden" name="zoneDesForDash" id="zoneDes"
					value="<%=allZonesForCompany.get(i).getZoneDesc()%>" /> <input
					type="hidden" name="zoneIdForDash" id="zoneIdForDash"
					value="<%=allZonesForCompany.get(i).getZoneKey()%>" /> <input
					type="hidden" name="zoneLatEastForDash" id="zoneLatEastForDash"
					value="<%=allZonesForCompany.get(i).getEastLongitude()%>" /> <input
					type="hidden" name="zoneLatWestForDash" id="zoneLatWestForDash"
					value="<%=allZonesForCompany.get(i).getWestLongitude()%>" /> <input
					type="hidden" name="zoneLatNorthForDash" id="zoneLatNorthForDash"
					value="<%=allZonesForCompany.get(i).getNorthLatitude()%>" /> <input
					type="hidden" name="zoneLatSouthForDash" id="zoneLatSouthForDash"
					value="<%=allZonesForCompany.get(i).getSouthLatitude()%>" /> <input
					type="hidden" name="centerlat" id="centerlat"
					value="<%=(allZonesForCompany.get(i).getNorthLatitude()+allZonesForCompany.get(i).getSouthLatitude())/2%>">
					<input type="hidden" name="centerlong" id="centerlong"
					value="<%=(allZonesForCompany.get(i).getEastLongitude()+allZonesForCompany.get(i).getWestLongitude())/2%>">


				</td>
				<%
					}
				%>
			</tr>
		</table>
	</div>
	<div id="statistics"
		style="display: none; z-index: 2000; width: 10px; height: 10px; position: absolute; top: 109px; left: 789px;">
		<table border="1" bgcolor="#efeffb"
			style="-moz-border-radius: 8px 8px 8px 8px; border: solid lightgrey; color: black">
			<tr>
				<td align="center" bgcolor="#ceecf5" style="white-space: nowrap;">Dispatch
					Statistics</td>
				<td bgcolor="#ceecf5" align="right"><img
					src="images/Dashboard/refresh1.png" onclick="jobCount()" />
				</td>
				<td><img src="images/Dashboard/close.png"
					onclick="removeStatistics();removeCookieForStatistics()" />
				</td>
				</right>
			</tr>
			<tr>
				<td colspan="3" style="white-space: nowrap;"><div
						id="statisticsConsole"></div>
				</td>
			</tr>
		</table>
	</div>
	<div id="sendSms"
		style="display: none; z-index: 2000; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0;">
		<jsp:include page="/Company/SMSSend.jsp" />
	</div>
	<div id="ORHistory" class="jqmWindow"
		style="display: none; z-index: 2500; align: center; left: 5px; position: absolute;">
		<jsp:include page="/Company/OpenRequestHistory.jsp" />
	</div>
	<div id="ORSummary" class="jqmWindow"
		style="display: none; z-index: 2500; position: absolute;">
		<jsp:include page="/Company/openRequestSummary.jsp" />
	</div>
	<div id="getDriver"
		style="background-color: lightgray; z-index: 3000; -moz-border-radius: 8px 8px 8px 8px; display: none; align: right; width: 180px; height: 50px; position: absolute; top: 150pt; left: 300pt;">
		<img align="right" alt="" src="images/Dashboard/close.png"
			onclick="removeGetDriver()">
		<%
			if(adminBO.getDispatchBasedOnVehicleOrDriver()==1){
		%>
		<input type="text" name="maDriverId" id="maDriverId" value=""
			placeholder="Enter the DriverID"> <input type="hidden"
			name="maCabId" id="maCabId" value="" placeholder="Enter the Cab No">
		<%
			}else{
		%>
		<input type="text" name="maCabId" id="maCabId" value=""
			placeholder="Enter the Cab No"> <input type="hidden"
			name="maDriverId" id="maDriverId" value=""
			placeholder="Enter the DriverID">

		<%
			}
		%>
		<center>
			<input type="button" name="locate" id="locate"
				value="Position the Driver" onclick="locateDriver()" />
		</center>
	</div>
	
   <div id="SuggestionsToAllocate"style="display: none;align: right;  position: absolute; top: 150pt; left: 450pt;background-color:lightgrey;z-index:2000">
 <img align="right" alt="" src="images/Dashboard/close.png" onclick="removeSuggestions()"> 
<h6 style="background-color:grey">Drivers&nbsp;&nbsp;Sorted&nbsp;&nbsp;by&nbsp;&nbsp;Shortest&nbsp;&nbsp;Distance</h6>
 <table id="suggAll" style="-moz-border-radius: 8px 8px 8px 8px;" bgcolor="lightgrey" border="1"></table>
 <h6 style="background-color:grey">Drivers&nbsp;&nbsp;Sorted&nbsp;&nbsp;by&nbsp;&nbsp;Clearing&nbsp;&nbsp;the&nbsp;&nbsp;Zones</h6>
 <table id="sortedID"  style="-moz-border-radius: 8px 8px 8px 8px;" bgcolor="lightgrey" border="1"></table>
  </div>
	
	<div id="sendMsg"
		style="background-color: lightgray; -moz-border-radius: 8px 8px 8px 8px; display: none; align: right; width: 240px; height: 130px; position: absolute; top: 150pt; left: 300pt;">
		<img align="right" alt="" src="images/Dashboard/close.png"
			onclick="removeMessageToIndividual()">
		<textarea rows="3" cols="30" name="message" id="message"></textarea>
		<input type="hidden" name="sendTripId" id="sendTripId" value=""><input
			type="hidden" name="sendDriver" id="sendDriver" value=""><input
			type="hidden" name="sendRiderName" id="sendRiderName" value=""><br>Send
		Voice Message<input type="checkbox" name="sendVoiceMsgToDriver"
			id="sendVoiceMsgToDriver" value="">
		<center>
			<input type="button" name="send" id="send" value="Send Message"
				onclick="sendSmsToDriver()">
		</center>
	</div>
	<div id="bullHorn"
		style="position: absolute; margin-top: -200px; left: 1032px; z-index: 2000;">
		<img align="left" class="dragBullHorn" alt=""
			src="images/Dashboard/Drag.png" width="23px" height="23px"> <img
			src="images/Dashboard/close.png" onclick="removeBullHorn()"
			align="left" />
		<div id="speaker" style="">
			<center>
				<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
					width="240" height="170" id="voiceMessageID">
					<object type="application/x-shockwave-flash" data="js/f211.swf"
						style="width: 260px; height: 190px;">
						<PARAM NAME=FlashVars VALUE="runtimer=true">
						<param name="wmode" value="transparent">
					</object>
				</object>
			</center>
		</div>
	</div>
	<div id="sharedRidePopUp"
		style="display: none; z-index: 2000; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0;">
		<jsp:include page="/Company/SharedRide.jsp" />
	</div>
	<div id="sharedDetailsPopUp"
		style="display: none; z-index: 2000; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0;">
		<jsp:include page="/Company/SharedRideDetails.jsp" />
	</div>
	<div id="OptionsForAll"
		style="display: none; z-index: 2000; align: right; width: 10px; height: 10px; position: absolute; top: 0; left: 0;">
		<div id="callerId" style="margin-left: 350px; top: 0; left: 0;">
			<table width="100%">
				<tr>
							<td style="width: 1%;"><input type="button" value="ER" id="refreshCIButton" name="refreshCIButton" disabled="disabled" style="-moz-border-radius:8px 8px 8px 8px;margin-top:-70px;background-color: Red;color: #FFFFFF;width: 40px;height: 40px;font-size: large;position:absolute;display:none;"/></td>			<td style="width: 2%;"><div id=button_0 style="display: none;position:absolute;"><input id=acceptor0 type="hidden" value=""></input><input type="button" value="" id="callerIdButton0" name="callerIdButton0" onclick="openDetailsDash(0,0)" style="-moz-border-radius:8px 8px 8px 8px;margin-top:-70px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;font-size: large;position:absolute" onmouseover="getPreviousAddress(0,0)" onmouseout="timerStart()"></input></div>
					<td style="width: 2%;"><div id=button_0
							style="display: none; position: absolute;">
							<input id=acceptor0 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton0"
								name="callerIdButton0" onclick="openDetailsDash(0,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,0)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_0 style="display: none; position: absolute;">
							<input id=acceptors0 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons0"
								name="callerIdButtons0" onclick="openDetailsDash(0,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,0)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_1
							style="display: none; position: absolute;">
							<input id=acceptor1 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton1"
								name="callerIdButton2" onclick="openDetailsDash(1,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,1)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_1 style="display: none; position: absolute;">
							<input id=acceptors1 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons1"
								name="callerIdButtons2" onclick="openDetailsDash(1,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,1)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_2
							style="display: none; position: absolute;">
							<input id=acceptor2 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton2"
								name="callerIdButton3" onclick="openDetailsDash(2,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,2)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_2 style="display: none; position: absolute;">
							<input id=acceptors2 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons2"
								name="callerIdButtons3" onclick="openDetailsDash(2,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px;; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,2)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_3
							style="display: none; position: absolute;">
							<input id=acceptor3 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton3"
								name="callerIdButton4" onclick="openDetailsDash(3,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,3)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_3 style="display: none; position: absolute;">
							<input id=acceptors3 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons3"
								name="callerIdButtons4" onclick="openDetailsDash(3,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px;; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,3)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_4
							style="display: none; position: absolute;">
							<input id=acceptor4 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton4"
								name="callerIdButton5" onclick="openDetailsDash(4,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px;; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,4)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_4 style="display: none; position: absolute;">
							<input id=acceptors4 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons4"
								name="callerIdButtons5" onclick="openDetailsDash(4,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px;; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,4)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_5
							style="display: none; position: absolute;">
							<input id=acceptor5 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton5"
								name="callerIdButton6" onclick="openDetailsDash(5,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,5)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_5 style="display: none; position: absolute;">
							<input id=acceptors5 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons5"
								name="callerIdButtons6" onclick="openDetailsDash(5,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,5)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_6
							style="display: none; position: absolute;">
							<input id=acceptor6 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton6"
								name="callerIdButton7" onclick="openDetailsDash(6,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,6)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_6 style="display: none; position: absolute;">
							<input id=acceptors6 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons6"
								name="callerIdButtons7" onclick="openDetailsDash(6,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,6)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_7
							style="display: none; position: absolute;">
							<input id=acceptor7 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton7"
								name="callerIdButton8" onclick="openDetailsDash(7,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,7)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_7 style="display: none; position: absolute;">
							<input id=acceptors7 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons7"
								name="callerIdButtons8" onclick="openDetailsDash(7,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,7)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_8
							style="display: none; position: absolute;">
							<input id=acceptor8 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton8"
								name="callerIdButton9" onclick="openDetailsDash(8,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,8);" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_8 style="display: none; position: absolute;">
							<input id=acceptors8 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons8"
								name="callerIdButtons9" onclick="openDetailsDash(8,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,8)" onmouseout="timerStart()"></input>
						</div>
					</td>
					<td style="width: 2%;"><div id=button_9
							style="display: none; position: absolute;">
							<input id=acceptor9 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButton9"
								name="callerIdButton10" onclick="openDetailsDash(9,0)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #FDD017; color: #342826; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(0,9)" onmouseout="timerStart()"></input>
						</div>
						<div id=buttons_9 style="display: none; position: absolute;">
							<input id=acceptors9 type="hidden" value=""></input><input
								type="button" value="" id="callerIdButtons9"
								name="callerIdButtons10" onclick="openDetailsDash(9,1)"
								style="-moz-border-radius: 8px 8px 8px 8px; margin-top: -70px; background-color: #008000; color: #FFFFFF; width: 40px; height: 40px; font-size: large; position: absolute"
								onmouseover="getPreviousAddress(1,9)" onmouseout="timerStart()"></input>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div id="popUpOR" style="z-index: 3000;">
		<div id="ORDash" class="jqmWindow"
			style="display: none; height: 330px; z-index: 3000; width: 550px">
			<jsp:include page="/jsp/OpenRequestForDashBoard.jsp" />
		</div>
	</div>
	<div id="error" style="z-index: 3000">
		<div id="Error"
			style="display: none; font-weight: bolder; color: #FF3333; background-color: #FFFF33; z-index: 3000"></div>
	</div>
	<div id="popUpAddress" style="z-index: 3000; margin-left: 180px;">
		<div id="ORDashAddress" style="z-index: 3000"></div>
	</div>
	<div id="popUpLandMark" style="z-index: 3000">
		<div id="ORDashLandMark" style="display: none; z-index: 3000">
			<jsp:include page="/jsp/landmarkDash.jsp" />
		</div>
	</div>
	<div id="hoverEffect" style="z-index: 3000;">
		<div id="showHover"
			style="display: none; margin-left: 100px; position: absolute;; background-color: #4EE2EC; width: 532px; -moz-border-radius: 12px 12px 12px 12px; z-index: 3000">
			<table
				style="background-color: #4EE2EC; -moz-border-radius: 12px 12px 12px 12px;">
				<tr>
					<td style="-moz-border-radius: 8px 8px 8px 8px;"><input
						type="button"
						style="color: black; font-size: small; font-weight: bold; background-color: #4EE2EC;"
						id="ph.Number" name="ph.Number" value="" size="2"
						onclick="appendPhone()" />
					</td>
					<td colspan='5' align='center'
						style="background-color: gray; size: 300px; -moz-border-radius: 8px 8px 8px 8px;">
						<input
						style="background-color: #4EE2EC; color: black; font-style: italic; font-size: small; font-weight: bold; -moz-border-radius: 12px 12px 12px 12px;"
						id="hoverValue" readonly="readonly" value="" size="40" />
					</td>
					<td style="-moz-border-radius: 8px 8px 8px 8px;"><input
						type="button"
						style="color: black; font-size: small; font-weight: bold; background-color: #4EE2EC;"
						id="futureJobs" name="futeureJobs" onclick="ORSummary(1)"
						value="Reservation" size="1" />
					</td>
					<td><a href="#"><img alt=""
							src="images/Dashboard/close.png" onclick="removeAddress()">
					</a><a href="#"></a>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div id="provider" style="z-index: 3000">
		<div id="phoneProvider"
			style="width: 50%; z-index: 3000; display: none;">
			<table border=2 BORDERCOLOR=green valign=top
				style="width: 30%; margin-left: 10%;">
				<tr style='color: blue'>
					<td style="width: 15%;"><select name="provider"
						id="providerValue">
							<option value="">Select</option>
							<option value="@txt.att.net">ATT</option>
							<option value="@myboostmobile.com">Boost Mobile</option>
							<option value="@sms.mycricket.com">Cricket</option>
							<option value="@mymetropcs.com">MetroPCS</option>
							<option value="@smtext.com">Simple Mobile</option>
							<option value="@tmomail.net">TMobile</option>
							<option value="@vtext.com">Verizon</option>
							<option value="@vmobl.com">Virgin</option>
							<option value="@messaging.sprintpcs.com">Sprint</option>
					</select>
					</td>
					<td style="width: 15%;"><input type="button" value="Confirm"
						id="confirm" onclick="enterEmail()" />
					</td>
				</tr>
			</table>
		</div>
	</div>
			<TABLE><tr><td>
  <input type="image" name="closeZon" id="closeZon" src="images/Dashboard/close.png" onclick="closeZoneList()"align ="right"style="Cursor:pointer;color:red;">
 <center><h3><a style="color:green">Select Zones</a></h3></center>
 <div style="height: 400px;width:320px; overflow-y: scroll;overflow-x:hidden;">
 
  <table id="zonesForOperator" style="height: 400px;width:300px; overflow-y: scroll;overflow-x:hidden;">
  <%for(int i=0;i<allZonesForCompany.size();i++){ %>
  <tr><td><%=allZonesForCompany.get(i).getZoneDesc()%></td>
 <td><input type="checkbox" name="selectZones_<%=i%>" id="selectZones_<%=i%>" value="<%=allZonesForCompany.get(i).getZoneKey()%>"/>
  </td></tr>
  <%} %>
 </table></div>
 <table> 
 <tr><td>Default Zone :</td><td><input type="checkbox" name="defZone" id="defZone" value="<%=defaultZone%>"></td>
  <tr><td align="center"><input type="button" id="selectZonesForOp" value="Create List" value="Select Zones" onclick="createZonesList()"></td>
  <td><input type="button" name="selectAll" id="selectAll" value="Select All" onclick="selectAllZonesList()"></td>  <td><input type="button" name="unSelect" id="unSelect" value="UnSelect All" onclick="unSelectAllZonesList()"></td>
  <td><input type="button" name="resetAll" id="resetAll" value="Reset" onclick="resetZoneList()"></td></tr>
  </table>
</td>
	
	<div class="jqmWindow" id="ahowAllZoneNames"
		style="display: none; width: 520px">
		<TABLE>
			<tr>
				<td><input type="image" name="closeZon" id="closeZon"
					src="images/Dashboard/close.png" onclick="closeZoneList()"
					align="right" style="Cursor: pointer; color: red;">
					<center>
						<h3>
							<a style="color: green">Select Zones</a>
						</h3>
					</center>
					<div
						style="height: 400px; width: 320px; overflow-y: scroll; overflow-x: hidden;">

						<table id="zonesForOperator"
							style="height: 400px; width: 300px; z-index: 3000; overflow-y: scroll; overflow-x: hidden;">
							<%
								for(int i=0;i<allZonesForCompany.size();i++){
							%>
							<tr>
								<td><%=allZonesForCompany.get(i).getZoneDesc()%></td>
								<td><input type="checkbox" name="selectZones_<%=i%>"
									id="selectZones_<%=i%>"
									value="<%=allZonesForCompany.get(i).getZoneKey()%>" /></td>
							</tr>
							<%
								}
							%>
						</table>
					</div>
					<table>
						<tr>
							<td>Default Zone :</td>
							<td><input type="checkbox" name="defZone" id="defZone"
								value="<%=defaultZone%>">
							</td>
						<tr>
							<td align="center"><input type="button"
								id="selectZonesForOp" value="Create List" value="Select Zones"
								onclick="createZonesList()">
							</td>
							<td><input type="button" name="selectAll" id="selectAll"
								value="Select All" onclick="selectAllZonesList()">
							</td>
							<td><input type="button" name="unSelect" id="unSelect"
								value="UnSelect All" onclick="unSelectAllZonesList()">
							</td>
							<td><input type="button" name="resetAll" id="resetAll"
								value="Reset" onclick="resetZoneList()">
							</td>
						</tr>
					</table></td>
				<td>
					<h6>Select Fields to show in the Job Details</h6>
					<table>
						<tr>
							<td>S.No</td>
							<td><input type="checkbox" name="showJobNo" id="showJobNo"
								value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Address</td>
							<td><input type="checkbox" name="showAddress"
								id="showAddress" value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Drop Add</td>
							<td><input type="checkbox" name="showDAddress"
								id="showDAddress" value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Driver</td>
							<td><input type="checkbox" name="showDriverAllocate"
								id="showDriverAllocate" value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Age</td>
							<td><input type="checkbox" name="showAge" id="showAge"
								value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>phone No</td>
							<td><input type="checkbox" name="showPhone" id="showPhone"
								value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Name</td>
							<td><input type="checkbox" name="showName" id="showName"
								value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Pickup Time</td>
							<td><input type="checkbox" name="showPtime" id="showPtime"
								value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Special Flags</td>
							<td><input type="checkbox" name="showSplFlags"
								id="showSplFlags" value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Start Zone</td>
							<td><input type="checkbox" name="showStZone" id="showStZone"
								value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Show Landmark</td>
							<td><input type="checkbox" name="showLandmark"
								id="showLandmark" value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>End Zone</td>
							<td><input type="checkbox" name="showEdZone" id="showEdZone"
								value="" onclick="hideDetails()">
							</td>
						</tr>
						<tr>
							<td>Trip ID</td>
							<td><input type="checkbox" name="showTripId" id="showTripId" value=""onclick="hideDetails()"></td>
						</tr>
						<tr>
							<td>Trip Source</td>
							<td><input type="checkbox" name="showTripSource" id="showTripSource" value="" onclick="hideDetails()"></tr>
						<tr>
							<td>Route Number</td>
							<td><input type="checkbox" name="showRouteNum" id="showRouteNum" value="" onclick="hideDetails()"></tr>
						<tr>
							<td>Flight Info</td>
							<td><input type="checkbox" name="showFlight" id="showFlight" value="" onclick="hideDetails()"></tr>
						<tr>
							<td>Account</td>
							<td><input type="checkbox" name="showVoucher" id="showVoucher" value="" onclick="hideDetails()"></tr>
						<tr>
							<td>Amount</td>
							<td><input type="checkbox" name="showAmount" id="showAmount" value="" onclick="hideDetails()"></tr>
					</table></td>
			</tr>
		</TABLE>
	</div>
	<div id="setEndZone"
		style="background-color: lightgray; -moz-border-radius: 8px 8px 8px 8px; display: none; align: right; width: 25%; height: 22%; position: absolute; top: 150pt; left: 300pt;">
		<img align="right" alt="" src="images/Dashboard/close.png"
			onclick="rempoveEndZone()">
		<table>
			<tr>
				<td><input type="hidden" name="tripIdForEndZone"
					id="tripIdForEndZone" value="">
				</td>
			</tr>
			<tr>
				<td>Select Zone</td>
				<td><select id="zoneListforEndZone" name="zoneListforEndZone">
						<%
							for(int i=0;i<allZonesForCompany.size();i++){
						%>
						<option value="<%=allZonesForCompany.get(i).getZoneKey()%>">
							<%=allZonesForCompany.get(i).getZoneDesc()%></option>
						<%
							}
						%>
				</select>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="3"><input type="button" name="book"
					id="book" value="Set EndZone" onclick="setEndZone()">
				</td>
			</tr>
		</table>
	</div>

	<div class="jqmWindow" id="dialog">
		<table id="individualTable"></table>
	</div>
	<div id="jobsOnThatDay"
		style="background-color: #ceecf5; display: none; position: absolute; top: 405px; left: 5px;">
		<img alt="" align="left" src="images/Dashboard/close.png"
			onclick="removeJobsForDay()">
		<div id="jobsForToday"></div>
	</div>

	<div id="instructions" style="z-index: 3000">
		<span class="button bClose"><span>X</span>
		</span>

		<center>
			<h3>Instructions For Dashboard</h3>
		</center>
		<div id="scrollIntructions"
			style="overflow: scroll; overflow-x: hidden; max-height: 450px; max-width: 1200px;">
			<table>
				<tr>
					<td>
						<table
							style="border-right: solid lightgrey; overflow: scroll; max-height: 450px; overflow-x: hidden">
							<tr>
								<td>
									<h5>Shortcuts For Open Request</h5> * Key press should be on
									the input box<br /> * You can type Capital or Small letters <br />
									* After entered commands press the tab<br />

									<table>
										<tr>
											<th align="center">S.No</th>
											<th align="center">Action</th>
											<th align="center">Shortcuts</th>
										</tr>
										<tr>
											<td>1</td>
											<td align="center">Operator cancel</td>
											<td align="center">OC</td>
										</tr>
										<tr>
											<td>2</td>
											<td align="center">Customer Canceled</td>
											<td align="center">CC</td>
										</tr>
										<tr>
											<td>3</td>
											<td align="center">No Show Request</td>
											<td align="center">NS</td>
										</tr>
										<tr>
											<td>4</td>
											<td align="center">Driver Completed</td>
											<td align="center">DC</td>
										</tr>
										<tr>
											<td>5</td>
											<td align="center">Re-Dispatch a Job</td>
											<td align="center">RD</td>
										</tr>
										<tr>
											<td>6</td>
											<td align="center">Hold the Job</td>
											<td align="center">H</td>
										</tr>
										<tr>
											<td>7</td>
											<td align="center">Couldn't Service</td>
											<td align="center">CS</td>
										</tr>
										<tr>
											<td>8</td>
											<td align="center">Broadcast Request</td>
											<td align="center">BC</td>
										</tr>
										<tr>
											<td>9</td>
											<td align="center">Resend the Job</td>
											<td align="center">RS</td>
										</tr>
										<tr>
											<td>10</td>
											<td align="center">Allocate Driver</td>
											<td align="center">(DriverID or Cab)</td>
										</tr>
										<tr>
											<td>11</td>
											<td align="center">Force Allocate Driver</td>
											<td align="center">F(DriverID or CabNo)</td>
										</tr>
										<tr>
											<td>12</td>
											<td align="center">Offer the Job With IVR Call</td>
											<td align="center">OP(DriverID or CabNo)</td>
										</tr>
									</table> <br />
									<h6>Other Commands</h6> * Right Click/Double click on the map
									to Zoom<br /> * Right Click on the Map , you can send
									text/voice Message to drivers in a particular distance<br /> *
									Go to your place on the map,right click to create a job, it
									will find your address and open the create job options dialogue
									box<br /> * Right click on the job to get the contextMenu<br />
									* Click on the Address to get the infowindow on the Map<br /> *
									Click on the status icon to get the full details of the job<br />
									* click the right Arrow to see the Expand screen<br /> *
									Uncheck the checkbox on the top, to see only unallocated Jobs <br />
									<br /> * Press Escape to Close this Instrouctions Box<br /></td>
							</tr>
						</table>
					</td>
					<td><table
							style="border-right: solid lightgrey; overflow: scroll; max-height: 450px; overflow-x: hidden"
							valign="top">
							<tr>
								<td>
									<h5>Details For Driver Details</h5>
								</td>
							</tr>
							<tr>
								<td>* Right Click the Cab/Driver and get the ContextMenu</td>
							</tr>
							<tr>
								<td>* To Know More , click right Arrow/Left Arrow at the
									Top</td>
							</tr>
							<tr>
								<td>* To get the total No of Flags, Click get Details
									button</td>
							</tr>
							<tr>
								<td>* To get the Total Jobs For the driver on that day,
									click the grey button at the top</td>
							</tr>
							<tr>
								<td>* To Send multiple text Message/Voice message, select
									Check Box and send</td>
							</tr>
							<tr>
								<td>* If you want to search based on driver or cab, type it
									and click the search icon</td>
							</tr>
						</table>
					</td>
					<!--   <td><table style="border-right:solid lightgrey;overflow:scroll;max-height:450px;overflow-x:hidden"><tr><td><h5>Details For Zone Waiting</h5></td></tr></table></td>
 -->
					<!--   <td><table style="border-right:solid lightgrey;overflow:scroll;max-height:450px;overflow-x:hidden"><tr><td><h5>Details For Caller ID</h5></td></tr></table></td> -->
					<td><table
							style="overflow: scroll; max-height: 450px; overflow-x: hidden">
							<tr>
								<td>
									<h5>Details For Options</h5>
								</td>
							</tr>
							<tr>
								<td>* Type the seconds in the Autorefresh Time and press
									Tab</td>
							</tr>
							<tr>
								<td>* To change the Map Zoom Level, choose the number in
									the dropdown box</td>
							</tr>
							<tr>
								<td>* To change individual Zoom level(with infowindow),
									choose the value in the drop down box</td>
							</tr>
							<tr>
								<td>* Press On or off to switch the AutoRefresh time of the
									Dashboard which is on the leftmost corner</td>
							</tr>
							<tr>
								<td>* To set the Default Location, go to the place and zoom
									it, and click the button</td>
							</tr>
							<tr>
								<td>* If you want to clear all the values to Default, Click
									clear button</td>
							</tr>
							<tr>
								<td>* To Show Voice Message, Click Show BullHorn button</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	 <div id="callerId" style="margin-left: 350px; top:0;left:0;margin-top:-10%"  >
	<table width="100%" id="tblCallerIdDash">
	</table>
	</div>
	<form name="cookieSubmit" id="cookieSubmit"
		action="DashBoard?event=submitCookies" method="post">
		<div id="cookieValueHidden"></div>
		<%
			if(cookieValues!=null&&cookieValues.size()>0){
			for(int i=0;i<cookieValues.size();i++){
		%>
		<input type="hidden" name="<%=cookieValues.get(i).getName()%>"
			id="<%=cookieValues.get(i).getName()%>"
			value="<%=cookieValues.get(i).getValue()%>">
		<%
			}}
		%>
	</form>

	<div id="consoleUp"
		style="position: absolute; margin-left: 20px; margin-top: 320px; left: -14px; top: 251px; display: none">
		<img id="dragConsole" src="images/Dashboard/icon_drag.png"
			align="right" style="cursor: move; width: 13px">
		<div id="console"
			style="font-size: 11.0px; width: 230px; color: green; height: 31px; overflow-y: auto;"></div>
	</div>
	<script>
		var mapOSM, mappingLayer, vectorLayer, selectMarkerControl, selectedFeature,epsg4326,projectTo;
		var latitude = "";
		var longitude = "";
		var arrMarkerJobs = [];
		var arrInfoWindowsJobs = [];
		var arrLabelJob = [];
		var data;
		var city = "";
		var arrFeature = [];
		var arrVector = [];

		function onloadOSM() {
			if (document.getElementById("latFromMap").value != "") {
				latitude = document.getElementById("latFromMap").value;
				longitude = document.getElementById("longFromMap").value;
			} else {
				latitude = document.getElementById("defaultLatitude").value;
				longitude = document.getElementById("defaultLongitude").value;
			}
			var defaultState = document.getElementById("defaultState").value;
			var defaultCountry = document.getElementById("defaultCountry").value;
			var zo = document.getElementById("zoomValue").value;

			mapOSM = new OpenLayers.Map("mapdiv");
			mappingLayer = new OpenLayers.Layer.OSM("Simple OSM Map");
			mapOSM.addLayer(mappingLayer);

			epsg4326 = new OpenLayers.Projection("EPSG:4326"); //WGS 1984 projection
			projectTo = mapOSM.getProjectionObject(); //The map projection (Spherical Mercator)

			var lonLat = new OpenLayers.LonLat(longitude, latitude).transform(
					epsg4326, projectTo);

			var zoom = zo;
			mapOSM.setCenter(lonLat, zoom);

			vectorLayer = new OpenLayers.Layer.Vector("Vector Layer", epsg4326);
			selectMarkerControl = new OpenLayers.Control.SelectFeature(
					vectorLayer, {
						onSelect : onFeatureSelect,
						onUnselect : onFeatureUnselect
					});
			mapOSM.addControl(selectMarkerControl);
			selectMarkerControl.activate();
			mapOSM.addLayer(vectorLayer);
		}

		function mapInitJobs(m) {
			console.log("about to plot job details");
			//while (mapOSM.popups.length) {
				//mapOSM.removePopup(mapOSM.popups[0]);
			//}
			vectorLayer.destroyFeatures();
			arrMarkerJobs = [];
			arrInfoWindowsJobs = [];
			arrLabelJob = [];
			var driverSelectedDontZoom = false;
			var trip = document.getElementById("currentJobinJObs").value;
			var driverIDSelected = document
					.getElementById("currentDriverinDrivers").value;

			var singleJob = false;
			var mapHasBeenDragged = document.getElementById("dragend").value;
			if (trip != "") {
				singleJob = true;
			}
			if (driverIDSelected != "") {
				driverSelectedDontZoom = true;
			}
			console.log("single job = " + singleJob);
			console.log("single job details for trip" + trip);
			latitude = document.getElementById("defaultLatitude").value;
			longitude = document.getElementById("defaultLongitude").value;
			var dragend = document.getElementById("dragend").value;

			//var centerCoord = new google.maps.LatLng(latitude,longitude); 
			if (!singleJob && !driverSelectedDontZoom
					&& mapHasBeenDragged == "false") {
				console.log("job details. Zooming to dflt loc and zoom");

				//map.setCenter(centerCoord);
				var zoomValue = document.getElementById("zoomValue").value;
			//	mapOSM.setCenter(new OpenLayers.LonLat(longitude, latitude).transform(epsg4326, projectTo), parseInt(zoomValue));

				//map.setZoom(parseInt(zoomValue));
			}
			var driverId = document.getElementById("currentDriverinJObs").value;
			var lat3 = document.getElementById("latJobs").value;
			var long3 = document.getElementById("longJobs").value;
			var oRows1 = document.getElementById("driverDetailDisplay")
					.getElementsByTagName("td");
			var iRowCount1 = oRows1.length;
			for ( var i = 0; i < iRowCount1; i++) {
				driver = oRows1[i].getElementsByTagName("input")[0].value;
				if (driver == driverId) {
					lat = oRows1[i].getElementsByTagName("input")[1].value;
					longi = oRows1[i].getElementsByTagName("input")[2].value;
					if (document.getElementById("startLat").value == "") {
						document.getElementById("startLat").value = lat;
						document.getElementById("startLong").value = longi;
					}
					break;
				}
			}
			var oRows = document.getElementById("customerTable")
					.getElementsByTagName("tr");
			var iRowCount = oRows.length;
			document.getElementById("totalJobs").innerHTML = iRowCount - 1;
			for ( var i = 1; i < iRowCount; i++) {
				var tripID = oRows[i].getElementsByTagName("input")[3].value;
				city = oRows[i].getElementsByTagName("input")[6].value;
				var time = oRows[i].getElementsByTagName("input")[2].value;
				var status = oRows[i].getElementsByTagName("input")[8].value;
				var phone = oRows[i].getElementsByTagName("input")[9].value;
				var name = oRows[i].getElementsByTagName("input")[10].value;
				var statusValue = oRows[i].getElementsByTagName("input")[12].value;
				var allocatedDriverID = oRows[i].getElementsByTagName("input")[11].value;
				//console.log("driver will be focussed: "+allocatedDriverID);

				lat1 = oRows[i].getElementsByTagName("input")[4].value;
				longi1 = oRows[i].getElementsByTagName("input")[5].value;
				//var latLng = new google.maps.LatLng(lat1, longi1);
				var image = "";
				if (statusValue != "47") {
					console.log("job status not 47" + tripID);

					if (statusValue == "40" || statusValue == "43"
							|| statusValue == "4") {
						image = "images/Dashboard/jobGreen.png";
					} else if (statusValue == "50" || statusValue == "51"
							|| statusValue == "52" || statusValue == "55"
							|| statusValue == "61" || statusValue == "99"
							|| statusValue == "25") {
						image = "images/Dashboard/jobRed.png";
					} else {
						image = "images/Dashboard/jobYellow.png";
					}
					feature = new OpenLayers.Feature.Vector(
							new OpenLayers.Geometry.Point(longi1, lat1)
									.transform(epsg4326, projectTo), {
								info : 'jobs',
								description : city,
								cab : allocatedDriverID,
								TripID : tripID,
								PT : time,
								stat : status,
								pNam : name
							}, {
								externalGraphic : image,
								graphicHeight : 35,
								graphicWidth : 31,
								graphicXOffset : -12,
								graphicYOffset : -25
							});
					vectorLayer.addFeatures(feature);
					if (singleJob == true && tripID == trip
							&& mapHasBeenDragged == "false") {
						var zoomValue = document
								.getElementById("individualZoomValue").value;
						mapOSM.setCenter(new OpenLayers.LonLat(longi1, lat1)
								.transform(epsg4326, projectTo),
								parseInt(zoomValue));

						//console.log("about to plot single job details for trip"+tripID);	
						var popup = new OpenLayers.Popup.FramedCloud(
								"tempId",
								new OpenLayers.LonLat(longi1, lat1).transform(
										epsg4326, projectTo),
								null,
								'<DIV id = \"infoWindowJob\" style=\"background-color: LightYellow; overflow: hidden;color:black\"><font color=\"Orange\">Address:</font><h3><font color=\"red\">'
										+ city
										+ "</h3></font><br/><font color=\"Orange\">Cab:</font>"
										+ allocatedDriverID
										+ " <br/><font color=\"Orange\">TripID:</font>"
										+ tripID
										+ "<br/><font color=\"Orange\">P Time:</font>"
										+ time
										+ "<br/><font color=\"Orange\">Status:</font>"
										+ statusValue
										+ "<br/><font color=\"Orange\">P Name:</font>"
										+ name + "<br/>", null, true);
						feature.popup = popup;
						mapOSM.addPopup(popup);
					}
				}
			}

		}
		function onFeatureSelect(feature) {
			selectedFeature = feature;
			if (selectedFeature.attributes.info == "jobs") {
				popup = new OpenLayers.Popup.FramedCloud(
						"tempId",
						feature.geometry.getBounds().getCenterLonLat(),
						null,
						'<DIV id = \"infoWindowJob\" style=\"background-color: LightYellow; overflow: hidden;color:black\"><font color=\"Orange\">Address:</font><h3><font color=\"red\">'
								+ selectedFeature.attributes.description
								+ "</h3></font><br/><font color=\"Orange\">Cab:</font>"
								+ selectedFeature.attributes.cab
								+ " <br/><font color=\"Orange\">TripID:</font>"
								+ selectedFeature.attributes.TripID
								+ "<br/><font color=\"Orange\">P Time:</font>"
								+ selectedFeature.attributes.PT
								+ "<br/><font color=\"Orange\">Status:</font>"
								+ selectedFeature.attributes.stat
								+ "<br/><font color=\"Orange\">P Name:</font>"
								+ selectedFeature.attributes.pNam + "<br/>",
						null, true);
			} else {
				popup = new OpenLayers.Popup.FramedCloud(
						"tempId",
						feature.geometry.getBounds().getCenterLonLat(),
						null,
						'<DIV id = \"infoWindowJob\" style=\"background-color: LightYellow; overflow: hidden;color:black\"><font color=\"Orange\">D Name:</font><h3><font color=\"red\">'
								+ selectedFeature.attributes.drName
								+ "</h3></font><br/><font color=\"Orange\">Cab:</font>"
								+ selectedFeature.attributes.cab
								+ " <br/><font color=\"Orange\">TripID:</font>"
								+ selectedFeature.attributes.tripId
								+ "<br/><font color=\"Orange\">Status:</font>"
								+ selectedFeature.attributes.stat
								+ "<br/><font color=\"Orange\">Address:</font>"
								+ selectedFeature.attributes.address + "<br/>",
						null, true);

			}
			feature.popup = popup;
			mapOSM.addPopup(popup);
		}

		function onFeatureUnselect(feature) {
			mapOSM.removePopup(feature.popup);
			feature.popup.destroy();
			feature.popup = null;
		}
		function mapPlotDriver(n) {
			var driverJob = "";
			var status = "";
			var tripId = "";
		/* 	while (mapOSM.popups.length) {
				mapOSM.removePopup(mapOSM.popups[0]);
			} */
			var mapHasBeenDragged = document.getElementById("dragend").value;
			var currentDriver = document
					.getElementById("currentDriverinDrivers").value;
			var currentJobClicked = document.getElementById("currentJobinJObs").value;
			var currentDriverAllocatedToJob = document
					.getElementById("currentDriverinJObs").value;

			var singleDriver = false;
			var currentJobInFocus = false;
			var currentJobClickedStarted = false;
			var singleDriverArrayLocation = 0;
			var singleJobArrayLocation = 0;

			if (currentJobClicked != "" && currentDriver == "") {
				currentJobInFocus = true;
			}
			if (currentDriver != "") {
				singleDriver = true;
			}

			if (!singleDriver && mapHasBeenDragged == "false"
					&& !currentJobInFocus) {
				latitude = document.getElementById("defaultLatitude").value;
				longitude = document.getElementById("defaultLongitude").value;
				var zo = document.getElementById("zoomValue").value;
/* 				mapOSM.setCenter(new OpenLayers.LonLat(longitude, latitude).transform(epsg4326, projectTo), parseInt(zo));
 */				console.log("Driver-->setting center from driver module to default"+ latitude + ":" + longitude);
			}

			var oRows = document.getElementById("driverDetailDisplay")
					.getElementsByTagName("td");
			var iRowCount = oRows.length;
			var oRowsJobs = document.getElementById("customerTable")
					.getElementsByTagName("tr");
			var iRowCountJobs = oRowsJobs.length;
			if (singleDriver == true) {
				for ( var k = 1; k < iRowCountJobs; k++) {
					var driverInJobs = oRowsJobs[k]
							.getElementsByTagName("input")[11].value;
					if (driverInJobs == currentDriver) {
						driverJob = oRowsJobs[k].getElementsByTagName("input")[6].value;
						status = oRowsJobs[k].getElementsByTagName("input")[8].value;
						tripId = oRowsJobs[k].getElementsByTagName("input")[3].value;
						continue;
					}
				}
			}
			for ( var j = 0; j < iRowCount; j++) {
				driver = oRows[j].getElementsByTagName("input")[0].value;
				cab = oRows[j].getElementsByTagName("input")[3].value;
				lat = oRows[j].getElementsByTagName("input")[1].value;
				longi = oRows[j].getElementsByTagName("input")[2].value;
				var statusDriver = oRows[j].getElementsByTagName("input")[4].value;
				var driverName = oRows[j].getElementsByTagName("input")[7].value;
				var image = "";
				if (statusDriver == "Y") {
					image = "images/Dashboard/taxiGreen.png";
				} else {
					image = "images/Dashboard/taxiRed.png";
				}

				feature = new OpenLayers.Feature.Vector(
						new OpenLayers.Geometry.Point(longi, lat).transform(epsg4326, projectTo), {
							info : 'driver',
							drName : driverName,
							cab : cab,
							address : driverJob,
							stat : status,
							tripId : tripId
						}, {
							externalGraphic : image,
							graphicHeight : 35,
							graphicWidth : 31,
							graphicXOffset : -12,
							graphicYOffset : -25
						});
				vectorLayer.addFeatures(feature);

				if (singleDriver == true && driver == currentDriver) {
					if (mapHasBeenDragged == "false") {
						var popup = new OpenLayers.Popup.FramedCloud(
								"tempId",
								new OpenLayers.LonLat(longi, lat).transform(
										epsg4326, projectTo),
								null,
								'<DIV id = \"infoWindowJob\" style=\"background-color: LightYellow; overflow: hidden;color:black\"><font color=\"Orange\">D Name:</font><h3><font color=\"red\">'
										+ driverName
										+ "</h3></font><br/><font color=\"Orange\">Cab:</font>"
										+ cab
										+ " <br/><font color=\"Orange\">TripID:</font>"
										+ tripId
										+ "<br/><font color=\"Orange\">Status:</font>"
										+ status
										+ "<br/><font color=\"Orange\">Address:</font>"
										+ driverJob + "<br/>", null, true);
						feature.popup = popup;
						mapOSM.addPopup(popup);
						var zoomValue = document.getElementById("individualZoomValue").value;

						mapOSM.setCenter(new OpenLayers.LonLat(longi, lat).transform(epsg4326, projectTo),parseInt(zoomValue));
					}
				}

			}
		}
		
	</script>
   <div id="messageTemplates" class="jqmWindow" style="width:700px;z-index:3000"><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeMessageTemplate()"></div>

<div id="footerDiv" style="height:7%;width:100%">
   <input type="hidden" name="lastNotifiMsgValue" id="lastNotifiMsgValue" value="0"/>
   <table style="width: 100%;height:100%"  ><tr><td width="8%"  bgcolor=" #3FBD13" align="center"   style="border-right:thick solid gold;-webkit-border-top-left-radius: 10px;-webkit-border-bottom-left-radius: 10px;-oz-border-radius-topleft: 10px;-moz-border-radius-bottomleft: 10px;border-top-left-radius: 10px;border-bottom-left-radius: 10px;"><img src="images/Dashboard/messageHistory.png" class="Message" style="width:34px;height:36px"><img src="images/Dashboard/settings.png"  style="width:34px;height:30px" class="options" onclick="createCookieForOptions()"></td>
  <td  width="84%" ><div style="background-color: #3FBDD6;height:100%" id="marqueueTd"><marquee behavior="scroll" scrollAmount="3"  id="showNotiMsg" onmouseout="this.start()"  onmouseover="this.stop()"  onmousedown="this.stop();" onmouseup="this.start();" hspace="0" scrolldelay="0" ></marquee></div></td><td width="8%" bgcolor="#3FBD13" style="border-left:thick solid gold;-webkit-border-top-right-radius: 10px;-webkit-border-bottom-right-radius: 10px;-moz-border-radius-topright: 10px;-moz-border-radius-bottomright: 10px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;"><img src="images/Dashboard/showOrHideMarqueue.png" onclick="showOrHideMarqueue()"  style="width:24px;height:20px">   <img src="images/Dashboard/decrease.png" onclick="decreaseSpeed()" style="width:14px;height:16px"><img src="images/Dashboard/increase.png" onclick="increaseSpeed()" style="width:14px;height:16px"><img src="images/Dashboard/stop.png" onClick="javascript:document.getElementById('showNotiMsg').stop();" style="width:14px;height:16px"><img src="images/Dashboard/start.png" onClick="javascript:document.getElementById('showNotiMsg').start();" style="width:14px;height:16px"></td></tr></table></div>
     
</div>

</body>
</html>
