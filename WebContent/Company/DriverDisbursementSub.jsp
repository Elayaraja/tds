<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.pp.bean.PaymentProcessBean"%>
<%@page import="com.ibm.icu.math.BigDecimal"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form  name="masterForm" action="control" method="post" onsubmit="return submitThis()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
 	 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
		<tr>
		<td>  uiuu
		
		  <div class="rightCol"> 
		<div class="rightColIn">
                	<h2><center>Driver Disbursement Details</center></h2>
			<table id="bodypage" >	
			 
 	<%
 	   ArrayList al_List = (ArrayList)request.getAttribute("al_list");
 	%>		 	
	<% if(al_List !=null && !al_List.isEmpty()) { %>
		<tr> 
			<td colspan="4" align="center">	
			  <table width="100%" border="2" cellspacing="0" cellpadding="0" class="driverChargTab">  
				<tr align="center">
					<th width="10%">Category</th>
					<th width="10%">Driver</th>
					<th width="10%">Amount</th>
					<th width="10%">Tip</th> 
					<th width="10%">Processing Date</th>
					<th width="10%">Txn Amount</th>
					<th width="10%">Txn %</th>
					<th width="10%" >Orginal Total</th>
					<th width="10%">Disbursement Adj Amount</th>
					<th width="10%">Reason</th>
					 
				</tr>
				<%double amount=0,adjamount=0; %>
				<%for(int i=0;i<al_List.size();i++) {%>
				<% PaymentProcessBean processBean = (PaymentProcessBean)al_List.get(i); %>
				
				<%if(i%2==0) { %>
					<tr align="center">
						<td>
							<%if(processBean.getType().equals("1")){out.print("Settlement");} else if(processBean.getType().equals("2")){out.print("Charges");}else if(processBean.getType().equals("3")){out.print("Credit/Charge");}else{out.print("Voucher");} %>
						</td>
						<td><%=processBean.getB_driverid() %></td>
						<td align="right"><%="$ "+new BigDecimal(processBean.getB_amount()).setScale(2,5) %></td>
						<td align="right"><%="$ "+new BigDecimal(processBean.getB_tip_amt()).setScale(2,5) %></td>
						 <td ><%=processBean.getProcess_date() %></td>
					 	<td align="right"><%="$ "+new BigDecimal(processBean.getTxn_amt()).setScale(2,5) %></td>
						<td><%=processBean.getTxn_prec() %></td>
						<td align="right">
							<a href="#" onclick="window.open('/TDS/control?action=registration&module=operationView&event=driverDisbursementSummary&subevent=showDisDetail&type=<%=processBean.getType() %>&pss_key=<%=processBean.getB_trans_id() %>')"><%="$ "+processBean.getTxn_total()%></a></td>
						<td align="right"><%="$ "+new BigDecimal(processBean.getAdj_amount()).setScale(2,5)%></td>
						<td><%=processBean.getReason() %></td>
						<%amount = amount + Double.parseDouble(processBean.getTxn_total()); %>
						<%adjamount = adjamount + Double.parseDouble(processBean.getAdj_amount()); %>
					</tr>
					<%} else { %>
					
					<tr align="center">
						<td style="background-color:lightgreen" >
							<%if(processBean.getType().equals("1")){out.print("Settlement");} else if(processBean.getType().equals("2")){out.print("Charges");}else if(processBean.getType().equals("3")){out.print("Credit/Charge");}else{out.print("Voucher");} %>
						</td>
						<td  style="background-color:lightgreen"><%=processBean.getB_driverid() %></td>
						<td align="right"  style="background-color:lightgreen"><%="$ "+new BigDecimal(processBean.getB_amount()).setScale(2,5) %></td>
						<td align="right"  style="background-color:lightgreen"><%="$ "+new BigDecimal(processBean.getB_tip_amt()).setScale(2,5) %></td>
						<td   style="background-color:lightgreen"><%=processBean.getProcess_date() %></td>
					 	<td align="right"  style="background-color:lightgreen"><%="$ "+new BigDecimal(processBean.getTxn_amt()).setScale(2,5) %></td>
						<td  style="background-color:lightgreen"><%=processBean.getTxn_prec() %></td>
						<td align="right"  style="background-color:lightgreen">
							<a href="#" onclick="window.open('/TDS/control?action=registration&module=operationView&event=driverDisbursementSummary&subevent=showDisDetail&type=<%=processBean.getType() %>&pss_key=<%=processBean.getB_trans_id() %>')"><%="$ "+processBean.getTxn_total()%></a></td>
						<td align="right" style="background-color:lightgreen"><%="$ "+new BigDecimal(processBean.getAdj_amount()).setScale(2,5)%></td>
						<td  style="background-color:lightgreen"><%=processBean.getReason() %></td>
						<%amount = amount + Double.parseDouble(processBean.getTxn_total()); %>
						<%adjamount = adjamount + Double.parseDouble(processBean.getAdj_amount()); %>
					</tr>
					
			 	<%} %>
			 	<%} %>
			 	<tr>
			 		<td></td><td></td><td></td> 
					<td></td><td></td><td></td><td><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
					 <td><%="$ "+new BigDecimal(adjamount).setScale(2,5) %></td><td></td><td></td>
			 		 
			 	</tr>				 						
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" colspan="5">
				<input type="Button"  value="Close" onclick="javascript:window.close();">
			</td>
		</tr>
	<%} %>
	
</table>
</div>
</div>
</td></tr>
</table>
</form>

</body>
</html>