<%@page import="com.tds.tdsBO.CompanyccsetupDO"%>
<%@page import="com.charges.bean.ChargesBO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>CC Templates</title>
<%ArrayList<ChargesBO> charges =(ArrayList<ChargesBO>)request.getAttribute("charges"); 
ArrayList<CompanyccsetupDO> al_list = (ArrayList<CompanyccsetupDO>)request.getAttribute("al_list");
%>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
	function delrow() {
		try {
			var table = document.getElementById("chargesTable");
			var rowCount = table.rows.length;
			var size=document.getElementById('fieldSize').value;
			if(Number(size)>0){
				var temp = (Number(size) - 1);
				document.getElementById('fieldSize').value = temp;
				rowCount--;
				table.deleteRow(rowCount);
			}
		} catch (e) {
			alert(e.message);
		}
	}
	 function chkVoucher(pointer,target)
	 {
	 	if(document.getElementById(pointer).value == '1')
	 	{
	 		document.getElementById(target).value = 'C';   	
	 	} 
	 	else
	 	{
	 		document.getElementById(target).value = 'V';
	 	}
	 	 
	 }

	function cal() {
		var i = Number(document.getElementById("fieldSize").value) + 1;
		document.getElementById('fieldSize').value = i;
		var table = document.getElementById("chargesTable");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);
		var cell3 = row.insertCell(3);
		var cell4 = row.insertCell(4);
		var cell5 = row.insertCell(5);

		var element = document.createElement("select");
		element.name = "templateType" + i;
		element.id = "templateType" + i;
		element.id = "templateType" + i;
		<%if(charges!=null && charges.size()>0){
			for(int i=0;i<charges.size();i++){%>
		var theOption = document.createElement("option");
		theOption.text = "<%=charges.get(i).getPayTemplateDesc()%>";
		theOption.value = "<%=charges.get(i).getPayTemplateKey()%>";
		element.options.add(theOption);
		<%}}%>
		cell.appendChild(element);

		var elementCI = document.createElement("select");
		elementCI.id="CCC_CARD_ISSUER"+i;
		elementCI.name="CCC_CARD_ISSUER"+i;
		var theOption = document.createElement("option");
		theOption.text="Credit Card";
		theOption.value="1";
		elementCI.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Voucher";
		theOption.value="2";
		elementCI.options.add(theOption);
		elementCI.onchange=function() {chkVoucher(elementCI.id,elementCT.id);};
		cell1.appendChild(elementCI);

		var element1 = document.createElement("input");
		element1.type = "text";
		element1.name="CCC_TXN_AMT_PERCENTAGE"+i;
		element1.id="CCC_TXN_AMT_PERCENTAGE"+i;
		element1.style.textAlign = "right";
		element1.size = "5";  
		cell2.appendChild(element1);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name="CCC_TXN_AMOUNT"+i;
		element2.id="CCC_TXN_AMOUNT"+i;
		element2.style.textAlign = "right";
		element2.size = "5";
		element2.onblur=function() {insert(element2.id);};
		cell3.appendChild(element2);

		var elementCT = document.createElement("input");
		elementCT.type = "hidden"; 
		elementCT.style.display = "none";
		elementCT.id="CCC_CARD_TYPE"+i;
		elementCT.name="CCC_CARD_TYPE"+i;
		cell4.appendChild(elementCT);

		var elementCS = document.createElement("select");
		elementCS.style.display = "none";
		elementCS.id="CCC_SWIPE_TYPE"+i;
		elementCS.name="CCC_SWIPE_TYPE"+i;
		var theOption = document.createElement("option");
		theOption.text="Swiped";
		theOption.value="A";
		elementCS.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Manual";
		theOption.value="M";
		elementCS.options.add(theOption);
		cell5.appendChild(elementCS);
		
		chkVoucher(elementCT.id,elementCI.id);
	}
</script>
</head>
<body>
<form name="chargeType" action="control" method="post">
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="companyccsetup"/>
<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
<input type="hidden" name="fieldSize" id="fieldSize" value="<%=al_list==null?"":al_list.size()%>"/> 	
<c class="nav-header"><center>CreditCard Payment Template</center></c>
<table id="chargesTable1" style="width: 100%;" id="bodypage1">
<tr>
<%
	String error = "";
	if (request.getAttribute("error") != null) {
		error = (String) request.getAttribute("error");
%>
<td colspan="7">
	<div id="errorpage" style="font-size: x-large;">
	<%if(error.equals("CC/Voucher template successfully inserted"))
		{%>
		<font color="blue"><center><%=error.length() > 0 ? "" + error : ""%></center></font>
		<%}else
		{
		%>
		<font color="red"><center><%=error.length() > 0 ? "" + error : ""%></center></font><%} %>
	</div></td>
<%
	}
%>
</tr>
<table id="chargesTable" style="width: 100%;" id="bodypage" border="1">
<tr style="text-align:center;background-color:#116969;color:#ffffff">          
	<th style="width: 25%;">TEMPLATE</th>
    <th width="25%">CARD ISSUER</th>
    <th width="25%">% AMOUNT</th>
    <th width="25%">TXN AMOUNT</th>
    <th style="display:none">CARD TYPE</th>
    <th style="display:none">SWIPE TYPE</th>
</tr>
<% if(al_list != null && al_list.size() >0  ) {  
	for(int count = 0; count < al_list.size(); count++) { 
%>
<tr>
	<td><Select name="templateType<%=count+1 %>">
	<%if(charges != null && charges.size()>0) {
		for(int i=0;i<charges.size();i++){%>
		<option value="<%=charges.get(i).getPayTemplateKey()%>" <%=al_list.get(count).getCCC_TEMPLATE_TYPE()==charges.get(i).getPayTemplateKey()?"selected":"" %>><%=charges.get(i).getPayTemplateDesc() %></option>	
	<%} }%></Select></td>
							    <td>
								 <select name="CCC_CARD_ISSUER<%=count+1 %>" id="CCC_CARD_ISSUER<%=count+1 %>" onchange="chkVoucher(this,'CCC_CARD_TYPE<%=count+1%>')">
								 <option value="1" selected="selected">Credit Card</option>
								 <option value="2" <%=al_list.get(count).getCCC_CARD_ISSUER().equals("2")?"selected":"" %>>Voucher</option>
								 </select>
						   </td>
						  <td>
								 <input type="text" style="text-align: right" name="CCC_TXN_AMT_PERCENTAGE<%=count+1 %>" id="CCC_TXN_AMT_PERCENTAGE<%=count+1 %>"  size="5" value="<%=al_list.get(count).getCCC_TXN_AMT_PERCENTAGE() %>" >
						 </td>
						 <td>
								<input type="text" name="CCC_TXN_AMOUNT<%=count+1 %>" style="text-align: right" id="CCC_TXN_AMOUNT<%=count+1 %>" size="5" onblur="insert(id)" value="<%=al_list.get(count).getCCC_TXN_AMOUNT() %>">
						 </td>
						  <td style="display:none">
								 <select name="CCC_CARD_TYPE<%=count+1 %>" id="CCC_CARD_TYPE<%=count+1 %>" >
								 <option value="C" <%=al_list.get(count).getCCC_CARD_TYPE().equals("C")?"selected":"" %>>Credit</option>
								 <option value="V" <%=al_list.get(count).getCCC_CARD_TYPE().equals("V")?"selected":"" %>>Voucher</option>
								 </select>						  
						  </td>
						 <td style="display:none">
								<select name="CCC_SWIPE_TYPE<%=count+1 %>">
								<option value="A" <%=al_list.get(count).getCCC_SWIPE_TYPE().equals("A")?"selected":"" %>>Swiped</option>
								<option value="M" <%=al_list.get(count).getCCC_SWIPE_TYPE().equals("M")?"selected":"" %>>Manual</option>									
						    	</select>
						</td>
	</tr>
<%}} %>
</table>

           <center><input type="button" name="add" value="ADD" onclick="cal()" class="lft">

                  <input type="button" name="remove" value="Remove"  onclick="delrow()" class="lft" >

                                 <input type="submit" name="buttonSubmit" class="lft" value="Submit"/></center> 
</form>
</body>
</html>