<%@page import="com.lowagie.text.Document"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@ page errorPage="ExceptionHandler.jsp" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.math.BigDecimal"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="Ajax/PageNo.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<title>Insert title here</title>
<%
	ArrayList al_List = (ArrayList)request.getAttribute("al_list");
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
%>
<script type="text/javascript">
 function page(div_id,button){
	 //alert(div_id);
	 $('#pageNo').fadeOut(1000);
	 $('#pageId').fadeIn(1000);
	// document.getElementById('pageNo').innerHTML = "";
 }
</script>
 <style type="text/css">
</style>
</head>

<body>
<form name="masterForm" action="control" method="post">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	<input type="hidden" name="action" value="registration">
	<input type="hidden" name="event" value="driverDisbursementSummary">
	<input type="hidden" name="size" id="size" value="<%=al_List!=null?al_List.size():"0" %>">
	<input type="hidden" name="financial" value="3">

            	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
		<div class="rightColIn">
                	<h1>Driver Disbursement Summary</h1>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                   <tr>
                    <td>
                    <table id="bodypage" width="540" >
                    <tr>
					<td class="firstCol">From&nbsp;Date</td>
					<td>
					<input type="text" name="from_date" id="from_date" onfocus="showCalendarControl(this);"  value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly">
					</td>
					<td class="firstCol">To&nbsp;Date</td>
					<td>
					<input type="text" name="to_date" id="to_date" onfocus="showCalendarControl(this);" value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly">
					</td>
					</tr>
					
					<%if(!((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equalsIgnoreCase("Driver")) {%>
					<tr>
					<td class="firstCol">Driver&nbsp;Id</td>
					<td colspan="7" align="center">
					<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)"  onblur="caldid(id,'drivername','dName');enableButton();disableDIV();" id="dd_driver_id" name="dd_driver_id" size="15"   autocomplete="off" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>" class="form-autocomplete">
								 <input type="hidden" name="dd_drv_id" id="dd_drv_id" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>">
						  		 <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 
				  					<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
							  		<div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>	 
					</td>
					</tr>
					<%} %>
				<tr>
					<td colspan="7" align="center">
						<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="submit" name="Button"  value="Get" class="lft" >
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    	</div>			
				    </td>
				</tr> 
				</table>
                    </td></tr>
                    </table>             
                </div>    
              </div>
             
              <div class="rightCol padT10">
			<div class="rightColIn padT10">
			 <div class="pageNo" id="pageNo">
              <% if(al_List !=null && !al_List.isEmpty()) { %>
                   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">  
                  <tr>
                        <th width="30%" class="firstCol">Driver Name</th>
                        <th width="20%">Payment Date</th>
                        <th width="15%">Check NO</th>
                        <th width="20%">Amount</th>
                        <th width="20%">Print</th>
                        <th width="30%">Description</th>
                      </tr>
                      <%String pre_d="",curr_d=""; %>
							<%double amount=0; %>
							<%for(int i=0;i<al_List.size();i++) {%>
								<% DriverDisbursment disbursment = (DriverDisbursment)al_List.get(i); %>
								<%curr_d = disbursment.getDriverid(); %>
								<%if(i==0 || curr_d.equals(pre_d)){ %>
									<%amount = amount + Double.parseDouble(disbursment.getAmount()); %>
								<%} %>
							 	<%if((!curr_d.equals(pre_d) && i!=0)) { %> 
							 	
							 	<tr>
										<td colspan="3"  style="background-color:#FCDFFF">Total</td>
										<td align="right" style="background-color:#FCDFFF"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
										<td style="background-color:#FCDFFF"></td>
										<td style="background-color:#FCDFFF"></td>
										<%amount = Double.parseDouble(disbursment.getAmount()); %>
					 				</tr> 
							 	<%} %>
							 	<%if(i%2==0){ %>
							 	<tr>
									<td>
										 <%=disbursment.getDrivername() %>
									</td>
									<td>
										<%=disbursment.getProcess_date() %>
									</td>
  									<td>
										<%=disbursment.getCheckno() %>
									</td>
									<td>
										<a href="" onclick="window.open('/TDS/control?action=registration&event=driverDisbursementSummary&subevent=showDisDetailSummary&module=financeView&pss_key=<%=disbursment.getPayment_id() %>')"><%="$ "+new BigDecimal(disbursment.getAmount()).setScale(2,5) %></a><br>
									</td>
									<td>
										<a href="#" onclick="window.open('<%=request.getContextPath()%>/frameset?__report=BIRTReport/DriverDisburshment.rptdesign&__format=pdf&pay_id=<%=disbursment.getPayment_id() %>')"><i>Print</i></a>
									</td>
								 	 <td>
										<%=disbursment.getDescr() %>										
									</td>
							</tr>	
							<%}else{ %>
							<tr>
									<td style="background-color:lightgreen">
										 <%=disbursment.getDrivername() %>
									</td>
									<td style="background-color:lightgreen">
										<%=disbursment.getProcess_date() %>
									</td>
  									<td style="background-color:lightgreen">
										<%=disbursment.getCheckno() %>
									</td>
									<td style="background-color:lightgreen">
										<a href="#" onclick="window.open('/TDS/control?action=registration&event=driverDisbursementSummary&module=financeView&subevent=showDisDetailSummary&pss_key=<%=disbursment.getPayment_id() %>')"><%="$ "+new BigDecimal(disbursment.getAmount()).setScale(2,5) %></a><br>
									</td>
									<td style="background-color:lightgreen">
										<a href="#" onclick="window.open('<%=request.getContextPath()%>/frameset?__report=BIRTReport/DriverDisburshment.rptdesign&__format=pdf&pay_id=<%=disbursment.getPayment_id() %>')"><i>Print</i></a>
									</td>
								 	 <td style="background-color:lightgreen">
										<%=disbursment.getDescr() %>										
									</td>
							</tr>	
								<%} %>
							<%if(i==al_List.size()-1) { %>
									<tr>
										<td colspan="3" style="background-color:#FCDFFF">Total</td>
										<td align="right" style="background-color:#FCDFFF"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
										<td style="background-color:#FCDFFF"></td>
										<td style="background-color:#FCDFFF"></td>
										<%amount = 0; %>
								  </tr>
								<%} %>
								<%pre_d = curr_d; %>
							<%} %>
						</table></div>
						<%} %>
						<table>
							<tr>
							<td valign="top" align="center" width="100%">
							<div id="pageId" style="display:none;">
							<%if(request.getParameter("pageButton")!=null){%>
								<jsp:include page="/Company/Disbursement.jsp" />
							</div><%} %>
							</td>
		</tr> 
        <tr>
				</tr></table><table id="nextPage">
						<tr><td>
						<%if(al_List !=null && !al_List.isEmpty()) { 
					String rowCount=(String)request.getAttribute("count");
					int count = Integer.parseInt(rowCount);
					for(int i=1;i<=count;i++){%>
					<input type="button"value="<%=i %>"onclick="page('pageNo',this);pageNo(<%=i%>,'<%=request.getParameter("from_date")%>','<%=request.getParameter("to_date")%>','<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>','<%=request.getParameter("dd_driver_id")%>');" />
										<%} }%>
						</td></tr></table>
                  </div>    
              </div>
             
    </div>  
</form>
</body>
</html>
   
