<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.cmp.bean.MerchantProfile"%>
<%@page import="com.common.util.TDSConstants"%>
<html>
<head>
<script type="text/javascript">
function phoneIns()
{
	var phone_no = document.getElementById('phone_no').value;
	
	if(phone_no.length ==3)
	{
		document.getElementById('phone_no').value = phone_no+" "; 
	} 
	
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form name="companyForm" action="control" method="post">
<%
	MerchantProfile mProfile = (MerchantProfile)request.getAttribute("mProfile");
	StringBuffer error = (StringBuffer)request.getAttribute("error")==null?new StringBuffer():(StringBuffer)request.getAttribute("error");
%>

<input type="hidden" name="<%= TDSConstants.actionParam %>" value="admin">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="getMerchant">
<input type="hidden" name="assocode" value="<%=request.getParameter("assocode") %>">

<table id="pagebox" cellspacing="0" cellpadding="0" >
	<tr>
		<td>
<table id="bodypage" border="1">	
	<tr>
		<td colspan="4" align="center">		
			<div id="title">
				<h2>Merchant&nbsp;Profile&nbsp;Setting</h2>
			</div>
		</td>		
	</tr>
	<tr>
		<td colspan="4" align="center">
		<font color="red"><%=error.toString().length()>0?error.toString():"" %></font>
		</td>
	</tr>
	
 	<tr>
		<td>Merchant Name </td>
		<td><input type="text" name="merch_name" id="merch_name" size="15" value="<%=mProfile.getMerch_name() %>"></td>
		<td>Street</td>
		<td><input type="text" name="street" id="street" size="15" value="<%=mProfile.getStreet() %>"> </td>
	</tr>
	<tr>
		<td>City </td>
		<td><input type="text" name="city" id="city" size="15" value="<%=mProfile.getCity()%>"></td>
		<td>State</td>
		<td>
			<select name="state" >
			  	<option value="AK" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >Alaska</option>
			 	<option value="AL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AL")?"selected":"" %> >Alabama</option>
			 	<option value="AR" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AR")?"selected":"" %> >Arkansas</option>
			 	<option value="AS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AS")?"selected":"" %> >American Samoa</option>
			 	<option value="AZ" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AZ")?"selected":"" %> >Arizona</option>
			 	<option value="CA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CA")?"selected":"" %> >California</option>
			 	<option value="CO" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CO")?"selected":"" %> >Colorado</option>
			 	<option value="CT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CT")?"selected":"" %> >Connecticut</option>
			 	<option value="DC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("DC")?"selected":"" %> >District of Columbia</option>
			 	<option value="DE" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("DE")?"selected":"" %> >Delaware</option>
			 	<option value="FL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("FL")?"selected":"" %> >Florida</option>
			 	<option value="GA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("GA")?"selected":"" %> >Georgia</option>
			 	<option value="HI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("HI")?"selected":"" %> >Hawaii</option>
			 	<option value="IA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("IA")?"selected":"" %> >Iowa</option>
			 	<option value="ID" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ID")?"selected":"" %> >Idaho</option>
			 	<option value="IL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >Illinois</option>
			 	<option value="IN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("IN")?"selected":"" %> >Indiana</option>
			 	<option value="KS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("KS")?"selected":"" %> >Kansas</option>
			 	<option value="KY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("KY")?"selected":"" %> >Kentucky</option>
			 	<option value="LA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("LA")?"selected":"" %> >Louisiana</option>
			 	<option value="MA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MA")?"selected":"" %> >Massachusetts</option>
			 	<option value="MD" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MD")?"selected":"" %> >Maryland</option>
			 	<option value="ME" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ME")?"selected":"" %> >Maine</option>
			 	<option value="MI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MI")?"selected":"" %> >Michigan</option>
			 	<option value="MN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MN")?"selected":"" %> >Minnesota</option>
			 	<option value="MO" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MO")?"selected":"" %> >Missouri</option>
			 	<option value="MP" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MP")?"selected":"" %> >Northern Mariana Islands</option>
			 	<option value="MS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MS")?"selected":"" %> >Mississippi</option>
			 	<option value="MT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MT")?"selected":"" %> >Montana</option>
			 	<option value="NC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NC")?"selected":"" %> >North Carolina</option>
			 	<option value="ND" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ND")?"selected":"" %> >North Dakota</option>
			 	<option value="NE" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NE")?"selected":"" %> >Nebraska</option>
			 	<option value="NH" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NH")?"selected":"" %> >New Hampshire</option>
			 	<option value="NJ" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NJ")?"selected":"" %> >New Jersey</option>
			 	<option value="NM" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >New Mexico</option>
			 	<option value="NV" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NV")?"selected":"" %> >Nevada</option>
			 	<option value="NY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NY")?"selected":"" %> >New York</option>
			 	<option value="OH" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OH")?"selected":"" %> >Ohio</option>
			 	<option value="OK" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OK")?"selected":"" %> >Oklahoma</option>
			 	<option value="OR" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OR")?"selected":"" %> >Oregon</option>
			 	<option value="PA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("PA")?"selected":"" %> >Pennsylvania</option>
			 	<option value="RI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("RI")?"selected":"" %> >Rhode Island</option>
			 	<option value="SC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("SC")?"selected":"" %> >South Carolina</option>
			 	<option value="SD" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("SD")?"selected":"" %> >South Dakota</option>
			 	<option value="TN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("TN")?"selected":"" %> >Tennessee</option>
			 	<option value="TX" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("TX")?"selected":"" %> >Texas</option>
			 	<option value="UT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("UT")?"selected":"" %> >Utah</option>
			 	<option value="VA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("VA")?"selected":"" %> >Virginia</option>
			 	<option value="VT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("VT")?"selected":"" %> >Vermont</option>
			 	<option value="WA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WA")?"selected":"" %> >Washington</option>
			 	<option value="WI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WI")?"selected":"" %> >Wisconsin</option>
			 	<option value="WV" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WV")?"selected":"" %> >West Virginia</option>
			 	<option value="WY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WY")?"selected":"" %> >Wyoming</option>
			</select>
		
		</td>
	</tr>
	 
	<tr>
		<td>Zip&nbsp;code : </td>
		<td><input type="text" name="zipcode" id="zipcode" size="15" value="<%=mProfile.getZipcode()%>"></td>
		<td>Phone&nbsp;No : </td>
		<td><input type="text" name="phone_no" id="phone_no" size="15" value="<%=mProfile.getPhone_no()%>" maxlength="11" onkeyup="phoneIns()"></td>
 	</tr>
	<tr>
		<td>MCC/SIC </td>
		<td><input type="text" name="sic" id="sic" size="15" value="<%=mProfile.getSic()%>"></td>
		<td>Client No </td>
		<td><input type="text" name="client_no" id="client_no" size="15" value="<%=mProfile.getClient_no()%>"></td>
	</tr>
	<tr>
		<td>Merchant ID </td>
		<td><input type="text" name="merch_id" id="merch_id" size="15" value="<%=mProfile.getMerch_id()%>"></td>
		<td>Terminal ID </td>
		<td>
			<input type="text" name="terminal_id" id="terminal_id" size="15" value="<%=mProfile.getTerminal_id()%>">
		</td>
	</tr>
	<tr>
		 <td>Service ID</td>
		 <td>
		 	<select name="service_id" id="service_id">
		 		<%--  <option value="786F400001" <%=mProfile.getService_id().equalsIgnoreCase("786F400001")?"selected":"" %>>786F400001</option>
		 		<option value="A656D00001" <%=mProfile.getService_id().equalsIgnoreCase("A656D00001")?"selected":"" %>>A656D00001</option>
		 		--%>
		 		<option value="3257B1300C" <%=mProfile.getService_id().equalsIgnoreCase("3257B1300C")?"selected":"" %>>3257B1300C</option>
		 		
		 	</select>
		 </td>
		 <td>Merchant Profile ID</td>
		 <td><input type="text" name="profile_id" id="profile_id" size="15" value="<%=mProfile.getProfile_id()%>"></td>
	</tr>
 	<tr>
		<td colspan="4" align="center">
			<input type="submit" value="Submit" name="Button" >
		</td>
	</tr>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>