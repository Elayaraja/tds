<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@ page errorPage="ExceptionHandler.jsp" %>
<%@page import="com.ibm.icu.text.Bidi"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.CashSettlement" %>
<% CashSettlement  cashSettlementBean = (CashSettlement)request.getAttribute("CashSettlement");%>   

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script>
   window.history.forward(-1);
</script>
<title>Insert title here</title>
<%
	ArrayList al_List = (ArrayList)request.getAttribute("al_list");
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver") == null ? new ArrayList():(ArrayList)request.getAttribute("al_driver");
   
%>

<script type="text/javascript">

function enableChkNo(option)
{
   if(option == 1)
   {
		if(document.getElementById('manual_adj').value.indexOf("-") < 0){
			document.getElementById('check_no').value = document.getElementById('Closingcheckno').value;
			document.getElementById('check_no').readOnly = "";
		}
	   else{
	   	document.getElementById('check_no').value ="";
	   	document.getElementById('check_no').readOnly = "";   
	   }
	}  else { 
	   	document.getElementById('check_no').value = "";
   		document.getElementById('check_no').readOnly = "readOnly";
	}		
}

function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
function chckAdj(amt,adj)
{
   
  if(isNaN(adj.split(" ")[1])){
  	alert("Adjusment Amt Not A number");
	document.getElementById('manual_adj').value="$ 0.00";
  	
  	var size = document.getElementById('size').value;
  	 
    for(var i=0;i<Number(size);i++)
	{
		document.getElementById('set'+i).checked = false;
	}
  	return false;
  }else	if(Number(adj) > Number(amt))
  {
	alert("Adjustment Cannot be greater than Actual");
	document.getElementById('manual_adj').value="$ 0.00";
	var size = document.getElementById('size').value;
	for(var i=0;i<Number(size);i++)
	{
		document.getElementById('set'+i).checked = false;
	}
	return false;
  } else {
	return true;
  }
}
 
function chckThis()
{
	if(document.getElementById('check_cash').checked)
	{
		if(document.getElementById('manual_adj').value.indexOf("-") < 0){
		if(document.getElementById('check_no').value != document.getElementById('Closingcheckno').value){
			return confirm ("Do you really want to change the cheque no?")
		}}

		if(isNaN(document.getElementById('check_no').value) || document.getElementById('check_no').value==""){
			alert("Pls provide Check No");
			document.getElementById('check_no').value="";
			return false;
		} else {
			return true;
		}
	} else {
		return true;
	}
}
function enableButton()
{
	if(document.getElementById('dd_driver_id').value!="Select")
	{
		document.getElementById('Button').disabled=false;
	}
}

function calcAdjustment(target,row)
{
	var size = document.getElementById('size').value;
	var amount =0;
	for(var i=0;i<Number(size);i++)
	{
			if(document.getElementById('set'+i).checked){
				if(!(isNaN((document.getElementById("adj"+i).value).split(" ")[1]))){
		 			amount = roundNumber((amount + Number((document.getElementById("adj"+i).value).split(" ")[1])),2);
		 			document.getElementById('row'+i).setAttribute("bgcolor","orange");
		 		} else{
		 				alert("Not A Number");
		 				document.getElementById('edit'+i).checked=false;
		 				document.getElementById("adj"+i).value="$ "+document.getElementById("act_amt"+i).value;
		 				amount = roundNumber((amount + Number((document.getElementById("adj"+i).value).split(" ")[1])),2);
		 		}
			} else {
				document.getElementById('row'+i).setAttribute("bgcolor","");
			}
	}
	
	document.getElementById(target).value ="$ "+amount;
	
}

function calcTotal(target,row)
{
	
	var size = document.getElementById('size').value;
	var amount =0;
	
	var adj = document.getElementById('adj'+row);
	var descr = document.getElementById('descr'+row);
	if(document.getElementById('set'+row).checked){
		adj.value ="$ "+ roundNumber(Number(document.getElementById("act_amt"+row).value),2);
	} else {
		document.getElementById('edit'+row).checked=false;
		descr.value = "";
		descr.readonly="readonly";
		
 	}
 	
 	for(var i=0;i<Number(size);i++)
	{
		if(document.getElementById('set'+i).checked){
	 		 document.getElementById('row'+i).setAttribute("bgcolor","orange");
		} else {
			document.getElementById("adj"+i).value ="$ 0.00";
			document.getElementById('row'+i).setAttribute("bgcolor","");
		}
	
		amount = roundNumber((amount + Number(document.getElementById("adj"+i).value.split(" ")[1])),2);
	}
   document.getElementById(target).value ="$ "+amount;
   

}
function cpyAmount(source,amount,orgin,row,chkno)
{
	if(document.getElementById(orgin.id).checked){
		if(Number(amount) >= 0){
			document.getElementById(source).readOnly="";		
		}
		document.getElementById(chkno).value = "0";
	 	document.getElementById(source).value = amount;
		document.getElementById(row).setAttribute("bgcolor","orange");
	}else{
		document.getElementById(source).readOnly="readOnly";
		document.getElementById(source).value = "0";
		document.getElementById(chkno).value = "0";
		document.getElementById(row).setAttribute("bgcolor","");
	} 
}
function submitThis()
{
	var a = true,b=true;
	a  = chckThis();
	b = chckAdj(document.getElementById('total1').value,document.getElementById('manual_adj').value);
	
 	if(a== true && b == true)
		return true;
	else
		return false;
}

function makeRead(row)
{
	if(document.getElementById('set'+row).checked)
	{
	  if(document.getElementById('edit'+row).checked)
	  {
		document.getElementById('adj'+row).readOnly="";
		document.getElementById('descr'+row).readOnly="";
	  } else {
	    var adj = document.getElementById('adj'+row);
		adj.value ="$ "+ roundNumber(Number(document.getElementById("act_amt"+row).value),2);
	  	document.getElementById('adj'+row).readOnly="readonly";
	  	document.getElementById('descr'+row).readOnly="readonly";
	  	document.getElementById('descr'+row).value ="";
	  }
	} else {
		document.getElementById('edit'+row).checked = false;
	}
}

function disableDIV()
{
	if(document.getElementById('dd_drv_id').value!=document.getElementById('dd_driver_id').value)
	{
		//alert("Hai");
		document.getElementById('disdetail').innerHTML = "";
	}
} 

function showRepot(repo,key)
{
	 

	
	if(repo == 1)
	{
		//var path = <%=request.getContextPath()%>;
		window.open('<%=request.getContextPath()%>/frameset?__report=BIRTReport/DriverDisburshment.rptdesign&__format=pdf&pay_id='+key);
	} else {
		
	}
}

</script>
<script type="text/javascript">
	showRepot('<%=request.getParameter("showRepo")==null?"":"1" %>',<%=request.getParameter("key")==null?"0":request.getParameter("key") %>);
</script>
<body>
<form  name="masterForm" action="control" method="post" onsubmit="return submitThis()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	<input type="hidden" name="action" value="registration"/>
	<input type="hidden" name="event" value="driverDisbursement"/> 
	<input type="hidden" name="size" id="size" value="<%=al_List!=null?al_List.size():"0" %>"/>
	<input type="hidden" name="financial" value="3"/>

 
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<h1>Driver Disbursement </h1>
                	<%	
					String error = (String) request.getAttribute("error");
					%>
					<div id="errorpage">
					<%= (error !=null && error.length()>0)?""+error :"" %>
					</div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
                    <td>
                    <table id="bodypage" width="540" >
                    <tr><td  class="firstCol">Current&nbsp;Cash</td>
					<td> <input type="text" alignment="right" readonly name ="Closingcash" id="Closingcash" value="<%=cashSettlementBean.getClosingCash() %>"/> </td> 
					<td  class="firstCol">Next&nbsp;Check&nbsp;No</td>
					<td> <input type="text" alignment="right" readonly name ="Closingcheckno" id="Closingcheckno" value="<%=cashSettlementBean.getClosingCheckNo() %>"/> </td>
					</tr>
                    <tr>
					<td class="firstCol">From&nbsp;Date</td>
					<td>
					<input type="text" name="from_date" onfocus="showCalendarControl(this);"  value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly">
					</td>
					<td class="firstCol">To&nbsp;Date</td>
					<td>
					<input type="text" name="to_date"  onfocus="showCalendarControl(this);" value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly">
					</td>
					</tr>
					
					<%if(!((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equalsIgnoreCase("Driver")) {%>
					<tr>
					<td class="firstCol">Driver&nbsp;Id</td>
					<td colspan="7" align="center">
					<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)"  onblur="caldid(id,'drivername','dName');enableButton();disableDIV();" id="dd_driver_id" name="dd_driver_id" size="15"   autocomplete="off" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>" class="form-autocomplete">
								 <input type="hidden" name="dd_drv_id" id="dd_drv_id" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>">
						  		 <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 
				  					<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
							  		<div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>	 
					</td>
					</tr>
					<%} %>
				<tr>
					<td colspan="7" align="center">
						<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="submit" name="Button" id="Button" class="lft" style="text-align:center"  value="Get" <%if(!((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equalsIgnoreCase("Driver")) {%> disabled="disabled" <%} %> >&nbsp;&nbsp;&nbsp;
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    	</div>			
				    </td>
				</tr> 
				</table>
                    </td></tr>
                    </table>             
                </div>    
              </div> 
              
            <div class="rightCol padT10">
			<div class="rightColIn padT10">
			<% if(al_List !=null && !al_List.isEmpty()) { %>
			<div id='disdetail'>
			<h5 align="center">Category</h5>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
			<%double amount=0; %>
			<%for(int i=0;i<al_List.size();i++) {%>
			<% DriverDisbursment disbursment = (DriverDisbursment)al_List.get(i); %>
			<%amount = amount + Double.parseDouble(disbursment.getAmount()); %>
			<%if(i==0){ %>
			<tr>
			
			<tr>
			<td></td>
			<td>
			<label for="Driver Name">Driver Name:</label>
			</td>
			<td colspan="5"><font color="blue"><%=disbursment.getDrivername() %></font>
			</td>
			</tr>
			<tr>
	        <th class="firstCol">Type</th>
	        <th  class="firstCol">Settled Date</th>
	        <th class="firstCol">Amount</th>
	        <th class="firstCol">Include</th>
	        <th class="firstCol">Edit</th>
	        <th class="firstCol">Adjustment</th>
	        <th class="firstCol">Description</th> 
			</tr>
			<%} %> 
			
			<tr id="row<%=i %>" >
			<td>
			<%if(disbursment.getType().equals("1")){out.print("Settlement");} else if(disbursment.getType().equals("2")){out.print("Charges");}else if(disbursment.getType().equals("3")){out.print("Credit/Charge");}else{out.print("Voucher");} %>
            <input type="hidden" name="type<%=i %>" id="type<%=i %>" value="<%=disbursment.getType() %>">
			</td>
			
			<td>
			<input type="hidden" name="pss_id<%=i %>" id="pss_id<%=i %>" value="<%=disbursment.getTrans_id() %>">
			<input type="hidden" name="driverid" id="driverid" value="<%=disbursment.getDriverid() %>">
			<%=disbursment.getProcess_date() %>
			</td>
			
			<td align="right">
			<a href="#" onclick="window.open('/TDS/control?action=registration&module=financeView&event=driverDisbursement&subevent=showDisDetail&type=<%=disbursment.getType() %>&pss_key=<%=disbursment.getTrans_id() %>')"><%="$ "+new BigDecimal(disbursment.getAmount()).setScale(2,5) %></a>
			<input type="hidden" name="act_amt<%=i %>" id="act_amt<%=i %>" value="<%=disbursment.getAmount() %>">
			</td>
			
			<td align="center">	
			<input type="checkbox" id="set<%=i %>" name="set<%=i %>" value='1' <%if(disbursment.getSet()!=null && disbursment.getSet().equals("1")){out.print("selected");} %> onclick="calcTotal('manual_adj',<%=i %>);">
			</td>
			
			<td align="center">
			<input type="checkbox" name="edit<%=i %>" id="edit<%=i %>" value="1" <%=(request.getParameter("edit"+i)!=null)?"checked":""  %> onclick="makeRead(<%=i %>);calcAdjustment('manual_adj',<%=i %>);" disabled="disabled ">
			</td>
								
			<td>
			<input type="text" size=10 style="text-align: right;" name="adj<%=i %>" id="adj<%=i %>" <%=(request.getParameter("edit"+i)==null)?"readonly":""  %>  value="<%=request.getParameter("adj"+i)==null?"":request.getParameter("adj"+i) %>" onblur="insert(id);calcAdjustment('manual_adj',<%=i %>);" >
			</td>
			
			<td>
			<input type="text" name="descr<%=i %>" id="descr<%=i %>" <%=(request.getParameter("edit"+i)==null)?"readonly":""  %>  value="<%=request.getParameter("descr"+i)==null?"":request.getParameter("descr"+i) %>">
			</td>
			</tr>
			
				<%} %>
				<tr>
				<td colspan="7" align="center">
				<table>
				<tr>	
					 <td align="left">Total</td>
					 <td align="left"><input type="text" style="text-align: right;" readonly="readonly" size="10" id="total" name="total" value="$ <%=request.getParameter("total")==null?new BigDecimal(amount).setScale(2,5).toString():request.getParameter("total") %> ">
					 <input type="hidden"  size="10" id="total1" name="total1" value="<%=request.getParameter("total")==null?new BigDecimal(amount).setScale(2,5).toString():request.getParameter("total") %> ">
					 </td>
					
					<td>
					&nbsp;
					</td>
					<td align="left">Manual adjustment</td>
					<td align="left"><input type="text" style="text-align: right;" name="manual_adj" size="10" id="manual_adj" readonly="readonly" value="$ <%=request.getParameter("manual_adj")==null?"0":request.getParameter("manual_adj") %> " onblur="insert(id);"></td>
					<td>&nbsp;</td>						 						
			  </tr>
					 				 	 
			<tr>
				 <td align="left"><input type="radio" name="check_cash" id="check_cash" value="1" <%=request.getParameter("check_cash")!=null && request.getParameter("check_cash").equals("1")?"checked":"" %> onclick="enableChkNo(1)" > Check</td>
				 <td align="left"><input type="radio" id="check_cash1" name="check_cash" value="2" <%=request.getParameter("check_cash")==null || request.getParameter("check_cash").equals("2")?"checked":"" %> onclick="enableChkNo(2)">Cash</td>
				 <td>
				 &nbsp;
				</td>
				<td align="left">Check number</td>
				<td align="left"><input type="text" id="check_no" readonly="readonly" name="check_no" size="10" <%=request.getParameter("check_cash")!=null && request.getParameter("check_cash").equals("2") ?"readonly":"" %>  value="<%=request.getParameter("check_no")==null?"":request.getParameter("check_no") %>"></td>
				<td>&nbsp;</td>
				</tr>
					 					
 				<tr>
				<td align="left" colspan="2">Description</td>
				<td>
			    &nbsp;
				</td>
			    <td align="left" colspan="2"><textarea name="desc" cols="20" rows="5"><%=request.getParameter("desc")==null?"":request.getParameter("desc")%></textarea></td>
				<td>
				&nbsp;
				</td>
				</tr>
				</table>
				</td>
				</tr>
			    <tr>
					<td colspan="7" align="center">
						<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="submit" name="Button" value="Record Payment"  class="lft">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    	</div>			
				    </td>
				</tr> 
				
		   </table>       
             </div>  
             <%} else if(request.getParameter("Button")!=null) {%>
		
				No Records for Driver Disbursement
			
			<%} %>   
                    
                </div>    
              </div>            
            	
                <div class="clrBth"></div>
       
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
