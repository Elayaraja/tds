<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.cmp.bean.MerchantProfile"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
function phoneIns()
{
	var phone_no = document.getElementById('phone_no').value;
	
	if(phone_no.length ==3)
	{
		document.getElementById('phone_no').value = phone_no+" "; 
	} 
	
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form name="companyForm" action="control" method="post">
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="admin">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="getMerchant">
<input type="hidden" name="assocode" value="<%=request.getParameter("assocode") %>">
<input type="hidden" name="systemsetup" value="6">
 <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
         
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<h1>Merchant Profile Setting</h1>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab" id="bodypage">
                    <%
					MerchantProfile mProfile = (MerchantProfile)request.getAttribute("mProfile");
					StringBuffer error = (StringBuffer)request.getAttribute("error")==null?new StringBuffer():(StringBuffer)request.getAttribute("error");
					%>
                    <tr>
                    <td colspan="4">
                    <div id="errorpage">
                    <%=error.toString().length()>0?error.toString():"" %>
                    </div>
                    <table border="1" width="540">
                    <tr>
                    <td class="firstCol">Merchant Name</td>
                    <td>
                    <input type="text" name="merch_name" id="merch_name" size="15" value="<%=mProfile.getMerch_name() %>">
                    </td>
                    <td class="firstCol">Street</td>
                    <td>
                    <input type="text" name="street" id="street" size="15" value="<%=mProfile.getStreet() %>">
                    </td>
                    </tr>
                    
                    <tr>
                    <td class="firstCol">City</td>
                    <td>
                    <input type="text" name="city" id="city" size="15" value="<%=mProfile.getCity()%>">
                    </td>
                    <td class="firstCol">State</td>
                    <td>
                   
                    <div class="div2"><div class="div2In">
                    <select name="state" width="50%" >
			  		<option value="AK" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >Alaska</option>
			 		<option value="AL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AL")?"selected":"" %> >Alabama</option>
			 		<option value="AR" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AR")?"selected":"" %> >Arkansas</option>
			 		<option value="AS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AS")?"selected":"" %> >American Samoa</option>
			 		<option value="AZ" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AZ")?"selected":"" %> >Arizona</option>
			 		<option value="CA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CA")?"selected":"" %> >California</option>
			 		<option value="CO" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CO")?"selected":"" %> >Colorado</option>
			 		<option value="CT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CT")?"selected":"" %> >Connecticut</option>
			 		<option value="DC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("DC")?"selected":"" %> >District of Columbia</option>
			 		<option value="DE" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("DE")?"selected":"" %> >Delaware</option>
			 		<option value="FL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("FL")?"selected":"" %> >Florida</option>
			 		<option value="GA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("GA")?"selected":"" %> >Georgia</option>
			 		<option value="HI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("HI")?"selected":"" %> >Hawaii</option>
			 		<option value="IA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("IA")?"selected":"" %> >Iowa</option>
			 		<option value="ID" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ID")?"selected":"" %> >Idaho</option>
			 		<option value="IL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >Illinois</option>
			 		<option value="IN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("IN")?"selected":"" %> >Indiana</option>
			 		<option value="KS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("KS")?"selected":"" %> >Kansas</option>
			 		<option value="KY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("KY")?"selected":"" %> >Kentucky</option>
			 		<option value="LA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("LA")?"selected":"" %> >Louisiana</option>
			 		<option value="MA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MA")?"selected":"" %> >Massachusetts</option>
			 		<option value="MD" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MD")?"selected":"" %> >Maryland</option>
			 		<option value="ME" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ME")?"selected":"" %> >Maine</option>
			 		<option value="MI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MI")?"selected":"" %> >Michigan</option>
			 		<option value="MN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MN")?"selected":"" %> >Minnesota</option>
				 	<option value="MO" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MO")?"selected":"" %> >Missouri</option>
				 	<option value="MP" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MP")?"selected":"" %> >Northern Mariana Islands</option>
			 		<option value="MS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MS")?"selected":"" %> >Mississippi</option>
			 		<option value="MT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MT")?"selected":"" %> >Montana</option>
			 		<option value="NC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NC")?"selected":"" %> >North Carolina</option>
			 		<option value="ND" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ND")?"selected":"" %> >North Dakota</option>
			 		<option value="NE" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NE")?"selected":"" %> >Nebraska</option>
			 		<option value="NH" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NH")?"selected":"" %> >New Hampshire</option>
			 		<option value="NJ" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NJ")?"selected":"" %> >New Jersey</option>
			 		<option value="NM" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >New Mexico</option>
				 	<option value="NV" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NV")?"selected":"" %> >Nevada</option>
			 		<option value="NY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NY")?"selected":"" %> >New York</option>
			 		<option value="OH" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OH")?"selected":"" %> >Ohio</option>
			 		<option value="OK" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OK")?"selected":"" %> >Oklahoma</option>
			 		<option value="OR" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OR")?"selected":"" %> >Oregon</option>
			 		<option value="PA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("PA")?"selected":"" %> >Pennsylvania</option>
			 		<option value="RI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("RI")?"selected":"" %> >Rhode Island</option>
			 		<option value="SC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("SC")?"selected":"" %> >South Carolina</option>
			 		<option value="SD" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("SD")?"selected":"" %> >South Dakota</option>
			 		<option value="TN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("TN")?"selected":"" %> >Tennessee</option>
			 		<option value="TX" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("TX")?"selected":"" %> >Texas</option>
			 		<option value="UT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("UT")?"selected":"" %> >Utah</option>
			 		<option value="VA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("VA")?"selected":"" %> >Virginia</option>
			 		<option value="VT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("VT")?"selected":"" %> >Vermont</option>
			 		<option value="WA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WA")?"selected":"" %> >Washington</option>
			 		<option value="WI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WI")?"selected":"" %> >Wisconsin</option>
			 		<option value="WV" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WV")?"selected":"" %> >West Virginia</option>
			 		<option value="WY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WY")?"selected":"" %> >Wyoming</option>
					</select>
					</div></div>
                    </td>
                    </tr>
                    
                    <tr>
                    <td class="firstCol">Zip&nbsp;code</td>
                    <td>
                    <input type="text" name="zipcode" id="zipcode" size="15" value="<%=mProfile.getZipcode()%>">
                    </td> 
                    <td class="firstCol">Phone&nbsp;No</td>
                    <td>
                    <input type="text" name="phone_no" id="phone_no" size="15" value="<%=mProfile.getPhone_no()%>" maxlength="11" onkeyup="phoneIns()">
                    </td>
                    </tr>
                    
                    <tr>
                    <td class="firstCol">MCC/SIC</td>
                    <td>
                    <input type="text" name="sic" id="sic" size="15" value="<%=mProfile.getSic()%>">
                    </td>
                    <td class="firstCol">Client&nbsp;NO</td>
                    <td>
                    <input type="text" name="client_no" id="client_no" size="15" value="<%=mProfile.getClient_no()%>">
                    </td>
                    </tr>
                    
                    <tr>
                    <td class="firstCol">Merchant Id</td>
                    <td>
                    <input type="text" name="merch_id" id="merch_id" size="15" value="<%=mProfile.getMerch_id()%>">
                    </td>   
                    <td class="firstCol">Terminal No</td>
                    <td>
                    <input type="text" name="terminal_id" id="terminal_id" size="15" value="<%=mProfile.getTerminal_id()%>">
                    </td> 
                    </tr>
                    
                    <tr>
                    <td class="firstCol">Service ID</td>
                    <td>
                    <div class="div2"><div class="div2In">
                    <select name="service_id" id="service_id" width="50%">
		 			<%--  <option value="786F400001" <%=mProfile.getService_id().equalsIgnoreCase("786F400001")?"selected":"" %>>786F400001</option>
		 			<option value="A656D00001" <%=mProfile.getService_id().equalsIgnoreCase("A656D00001")?"selected":"" %>>A656D00001</option>
		 			--%>
		 			<option value="3257B1300C" <%=mProfile.getService_id().equalsIgnoreCase("3257B1300C")?"selected":"" %>>3257B1300C</option>
		 	    	</select>
		 	    	</div></div>
                    </td>
                    <td class="firstCol">Merchant Profile ID</td>
                    <td><input type="text" name="profile_id" id="profile_id" size="15" value="<%=mProfile.getProfile_id()%>">
                    </td>
                    </tr>
                    
                      <tr>
                        <td colspan="6" align="center">
						<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        <div class="rht">
                        	<input type="submit" value="Submit" name="Button" class="lft"  >
                        </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    	</div>
                    </td>
                      </tr>
                      </table>
                      </td>
                      </tr>
               </table>
               </div>    
              </div>
            	
                <div class="clrBth"></div>
        
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
           
            