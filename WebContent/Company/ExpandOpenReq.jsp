<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript">
 function autocom(i)
 {
	 autocomplete("driver_id"+i, "model-popup2", "driver_id"+i, "autocomplete.view", "DRIVERID", null, "throbbing",null);
 }
 function rFocus(i)
 {
	
	 document.getElementById('queue_no'+i).focus();
 }
</script>

<link href="css/dashboard.css" rel="stylesheet" type="text/css">
<%
	ArrayList al_list = (ArrayList)request.getAttribute("al_list");
	ArrayList al_q =  (ArrayList)request.getAttribute("al_q");
	ArrayList al_d = (ArrayList)request.getAttribute("al_d");
%>
<script type="text/javascript" src="Ajax/DashBoard.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Open Request In Zone</title>
</head> 
 
<body>
<form name="OpenRequestUnAlloc" action="control" method="post">
<input type="hidden" name="action" value="admin">
<input type="hidden" name="event" value="allocate">
<input type="hidden" name="size" id="size" value="<%=al_list==null?"0":al_list.size() %>"> 
<table border=1> 
<tr><td colspan='8' align='center'><font color='blue'>Open Request in Queue</font></td></tr>
<tr bgcolor='#A5A5A5'>
	<td></td><td><font size='2'>Trip Id</font><td><font size='2'>Rider Name</font></td><td><font size='2'>Phone No</font></td><td><font size='2'>Driver id</font></td><td><font size='2'>Zone</font></td><td><font size='2'>Status</font></td><td><font size='2'>Pickup Addr</font></td>
	<td><font size='2'>Pickup Time</font></td>
</tr>
<%if(al_list!=null && !al_list.isEmpty()) {%>
	<%
	for(int i=0;i<al_list.size();i++)
			{
				OpenRequestBO openRequestBO =(OpenRequestBO)al_list.get(i);
				
				if(i%2 == 0)
					out.print("<tr id='evenrow'>");
				else
					out.print("<tr id='oddrow'>");
				
				out.print("<td><input type='checkbox' name='chck"+i+"' id='chck"+i+"' value='"+openRequestBO.getTripid()+"'>");	
				out.print("<td><font size='1.8' color='#FFFFFF'>"+openRequestBO.getTripid()+"</td><td><font size='1.8' color='#FFFFFF'>"+openRequestBO.getName()+"</td><td><font size='1.8' color='#FFFFFF'>"+openRequestBO.getPhone()+"</td>");
				out.print("<td>");
				out.print(" <input type=\"text\" size=\"15\" name=\"driver_id"+i+"\" id=\"driver_id"+i+"\"  value='"+openRequestBO.getDriverid()+"' \" class=\"form-autocomplete\" onkeydown=\"autocom("+i+")\" onblur='hidePopup()'>");
				out.print("<div id=\"model-popup2\" class=\"autocomplete\"></div>");
				
				//out.print("<select name='driver_id"+i+"' id='driver_id"+i+"'>");
				//out.print("<option value=''>No Driver</option>");
				//for(int j=0;j<al_d.size();j+=2){
				//	if(al_d.get(j).toString().equalsIgnoreCase(openRequestBO.getDriverid()))
				//		out.print("<option value='"+al_d.get(j).toString()+"' selected='selected'>"+al_d.get(j+1).toString()+"</option>");
				//	else	
				//		out.print("<option value='"+al_d.get(j).toString()+"'>"+al_d.get(j+1).toString()+"</option>");
				//}
				out.print("</select>");
				
				out.print("</td>");
				
				
				out.print("<td><font size='1.8' color='#FFFFFF'>");
				out.print("<select name='queue_no"+i+"' id='queue_no"+i+"'>");
				out.print("<option value=''>No Zone</option>");
				for(int j=0;j<al_q.size();j+=2){
					if(al_q.get(j).toString().equalsIgnoreCase(openRequestBO.getQueueno()))
						out.print("<option value='"+al_q.get(j).toString()+"' selected='selected'>"+al_q.get(j+1).toString()+"</option>");
					else	
						out.print("<option value='"+al_q.get(j).toString()+"'>"+al_q.get(j+1).toString()+"</option>");
				}
				out.print("</select>");
				out.print("</td>");  
				
				//out.print(openRequestBO.getQueueno()+"</td>");
				out.print("<td><font size='1.8' color='#FFFFFF'>"+openRequestBO.getTripStatus()+"</td><td><font size='1.8' color='#FFFFFF'>"+openRequestBO.getScity()+"</td>");
				out.print("<td>"+openRequestBO.getRequestTime()+"</td>");
				out.print("</tr>");
		}
	%>
	<tr><td colspan="9" align="center">
		<input type="button"  value="Allocate" class="button" onClick="allocateOQ()">&nbsp;&nbsp;<input type="button" value="Close" onclick="javascript:opener.StartTimer();window.self.close();"  class="button"></td></tr>
<%} %>	
</table>
</form>
</body>
</html>