<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.tdsBO.OpenRequestBO" %>
<%@page import="java.util.ArrayList"%>

<%ArrayList al_list = (ArrayList)request.getAttribute("driverDetails");
String positionValue=(String)request.getAttribute("position");
%>
<% if(al_list!=null && al_list.size()>0) {%>
   	 	<NumRows>NumRows=2</NumRows>###
   	 <%} else { %>
   	 	<NumRows>NumRows=1</NumRows>###
   	 <%} %>	

<%if(al_list!=null && !al_list.isEmpty()) {%> 
<script src="DashboardAjax/DispatchCommon.js" language="javascript"></script>
<body>
<form name="driverToSelect" >
 <table align="center" border="1" style="width:100%;background-color: white;">
 <tr>
	<td colspan="4" align='center' style="background-color:#d3d3d3;">
		<h5>
			<center><font size="3">Drivers&nbsp;</font><a style="margin-left:60px" href="#"><img alt="" src="images/Dashboard/close.png" onclick="removeDriverPopup()"></a> </center>
		</h5>
	</td>		
</tr> 
	<% 		boolean colorLightGreen = true;
		String colorPattern;%>
 	<%for(int i=0;i<al_list.size();i+=2) {
 			colorLightGreen = !colorLightGreen;
 			if(colorLightGreen){
 				colorPattern="style=\"background-color:lightgreen\""; 
 				}
 			else{
 				colorPattern="";
 			}
 		%>
 	<tr>
 		<td <%=colorPattern%>><%=al_list.get(i)%>(<%=al_list.get(i+1)%>)</td>
		<td <%=colorPattern%>><input type="button" name="Force" id="forceAllocate" value="Force" onclick="allocateDriverFromPopup(<%=i%>,1)"/></td>
		<td <%=colorPattern%>><input type="button" name="allocate" id="allocate" value="Offer" onclick="allocateDriverFromPopup(<%=i%>,2)"/></td>
		<input type="hidden" id="driverIdPopup<%=i%>" value="<%=al_list.get(i)%>" 	/>
 	</tr>
 	<%} %>
 	<%} %>
 	 	<input type="hidden"  id="positionDriver"  name="positionDriver" value="<%=positionValue%>" > 
 </table>
</form>
</body>
