<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.tds.tdsBO.DriverLocationBO"%>
<%@page import="com.tds.cmp.bean.DriverVehicleBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.Iterator"%>
<%@page import= "java.util.Map"%>
<%@page import= "java.util.Set"%>
<title>Jobs History</title>
<%AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user"); %>
<%	ArrayList<DriverLocationBO> drivers = (ArrayList<DriverLocationBO>)request.getAttribute("Drivers");	
 %>
 <%ArrayList<DriverVehicleBean> dvList=new ArrayList<DriverVehicleBean>();
HashMap<String,DriverVehicleBean> companyFlags=(HashMap<String,DriverVehicleBean>) request.getAttribute("companyFlags");
Set hashMapEntries = companyFlags.entrySet();
Iterator it = hashMapEntries.iterator();
while(it.hasNext()){
	Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
	dvList.add(companyFlag.getValue());
}%>
 <style type="text/css">
 #jobsForDayHeader tr{
background-color:#ceecf5;
white-space:nowrap;
}
#jobsForDayContent tr{
background-color:#EFEFFB;
white-space:nowrap;
}
 
 .drivers  em {
	font-weight: normal;
	background: url(images/yellow.png) no-repeat center center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 10px;
	font-style: normal;
	z-index: 2;
	display: none;
}
 #available, #unavailable,#noFeed {
    -moz-border-radius: 4px 4px 4px;
	border-radius: 4px;
	box-shadow:3px 3px 1px grey;
    color: #222222;
    cursor: pointer;
    float: left;
    margin-bottom: 2px;
    margin-right: 4px;
    opacity: 0.9;
    padding: 3px;
}
   #available {
    background-color: #66FF66;
}

#unavailable {
    background-color: #FF8888;
    color: #444444;
}
#noFeed {
    background-color: #ebda1e;
    color: #444444;
}


</style>
</head>
<body>

 <%String title;
 	String number; 
 if(adminBO.getDispatchBasedOnVehicleOrDriver()==2){
	  title = "Cab Details";
	  number = "CabNo";
 }else{
	  title = "Driver Details";
 	  number = "DriverId";
 } %>
 <input type="hidden" name="dispatchBasedOn" id="dispatchBasedOn" value="<%=adminBO.getDispatchBasedOnVehicleOrDriver()%>">
 <div id="right-block" style="filter:alpha(opacity=80); opacity: 0.9;">
	<div class="block-top" style="cursor:move">&nbsp;&nbsp;<%=title%><a style="margin-left:35px" href="#">
	<img alt="" id="imgReplace1" src="images/Dashboard/leftarrow.png"  onclick="driverRightArrow();">&nbsp;&nbsp;
	<img alt=""  id="unloadDrivers"alt=""  src="images/Dashboard/refresh1.png" width="14" height="14"onclick="getAllValuesForDashboard('01000')" ></a>&nbsp;&nbsp;
	<a href="#"><img alt=""  src="images/Dashboard/close.png" onclick="removeDriver();removeCookieForDrivers()"></a></div>
		<div class="job-avail-mid">
		<div id="tabsOnDrivers"style="width:100%;height:100%;padding:5px;">
		<div id="tabs">
		<nav> 
		    <ul>
		        <li><a href="#tab1" id="li">LoggedIn</a></li>
		        <li><a href="#tab2" id="lo">LoggedOut</a></li>
		        <li><a href="#tab3" id="al">All</a></li>
		    </ul>
		</nav></div>
		</div>
		 <div class="job-inner1" id="permission">
		 <% if(adminBO.getDispatchMethod().equalsIgnoreCase("m")){ %>
			<table>
			<tr><td>
			<input type="text" size="9" name="driverIdForLogin" id="driverIdForLogin" placeholder="DriverID" value=""/>  </td>
			<td><input type="text" size="8" name="cabForLogin" id="cabForLogin" value="" placeholder="CabNo"></td><td>
			<input type="button" name="loginDriver" id="loginDriver" value="Login" onclick="loginDriverOrCab()"/></td>
			</tr></table>
         <%} %>  
			<tr bgcolor="#EFEFFB">
			<td >
			<input type="text" size="15" name="driver_id" id="driver_id" placeholder="Enter the <%=number%>"  onkeypress="changeHappeningDriver()" />
			<span id="searchButton" onclick="getAllValuesForDashboard('01000');"></span></td>
			</tr>
			<div id="scrollView" style="width: 200px; min-height: 50px; max-height: 400px; overflow-y: auto; overflow-x: hidden;">
			<table id="driverDetailDisplay" >
			</table>
			<input type="button" name="clickTotal" id="clickTotal" onclick="calculateTotalNoOfFlags()" value="Get Total"/></div>
			</div>
	    </div>
		<img alt="Jobs available" src="images/Dashboard/jobs-avail-bot.png">
</div>
	<div id="deactivate" style="display: none; position: absolute;margin-left: 1000px; margin-top: 200px; top:0;left:0;"> <table bgcolor="#EFEFFB">
	<tr bgcolor="#ceecf5">
	<td align="right"colspan="2"><img alt=""  src="images/Dashboard/close.png" onclick="removeDeactivate()"></td>
	</tr>
   	<tr  class="hourBased">
   	<td width="3px">Hour</td>
   	<td> <input type="text" name="hour" id="hour" size="2" value="">Hrs </td>
   	</tr>
   	<tr><td colspan="2">
   	<input class="dt" type="button" value="Based on Date/Time" name="basedOnhour"id="basedOnhour" onclick="basedOnDate()"></td></tr>
   	<tr class="basedDate" style="display:none">
   	<td  >From&nbsp;Date&nbsp;&nbsp;Time</td>
    <td><input type="text" name="from_date"   id="from_date" onfocus="showCalendarControl(this);" size="10" value="" readonly="readonly"/> 
    <input type="text" name="from_time"   id="from_time"   size="5" value="" />Hrs </td>
    </tr>
       <tr class="basedDate" style="display:none">
    	<td >To&nbsp;Date&nbsp;&nbsp;Time </td>
    	<td>
                  <input type="text" name="to_date"   id="to_date" size="10" onfocus="showCalendarControl(this);" value="" readonly="readonly"/> 
                  <input type="text" name="to_time"   id="to_time" size="5"   value=""  />Hrs 
     	</td></tr>
     	<tr><td class="hr"style="display:none"><input type="button" value="Based on Hour"name="basedHr" id="basedHr" onclick="basedOnHr()"></td></tr>
     	<tr>
     	<td><input type="button" value="Submit" onclick="deactivateDetail()"></td>
    </tr></table></div>
    		<div id="right-block5" style="display:none; height: 550px">
<div style="background-color:#ceecf5;" >&nbsp;&nbsp;<font color="black"><e style="margin-left:90px;"><%=title%></e></font>
<a style="margin-left:483px" href="#"><input type="button" name="checkCount" value=" " onclick="checkCount()"style="width:20px;height:20px;"/>
<img alt=""  src="images/Dashboard/rightarrow.png"  id="imgReplace"  onclick="driverRightArrow();">&nbsp;&nbsp;
<img alt=""  id="unloadDrivers"alt=""  src="images/Dashboard/refresh1.png" width="14" height="14" onclick="getAllValuesForDashboard('01000');" ></a>&nbsp;&nbsp;
<a href="#"><img alt=""  src="images/Dashboard/close.png" onclick="removeDriver()"></a></div>	
<div id="expandDiv" style="overflow-x:hidden;overflow-y:auto;min-height:50px;max-height:480px;width:811px">
<div id="tabsOnDrivers"style="width:752px;height:100%">
<header>
	<h1></h1>
</header>
<div id="tabDiv">
<nav> 
    <ul>
        <li ><a href="#tabD1" id="li">LoggedIn</a></li>
        <li><a href="#tabD2" id="lo">LoggedOut</a></li>
         <li><a href="#tabD3" id="al">All</a></li>
    </ul>
</nav></div>
</div>
		<div id="permission">
		<%if(adminBO.getRoles().contains("1038")){ %>		
			<section class="tab" id="tabD2">
				<p></p>
			</section>
			<section class="tab" id="tabD3">
				<p></p>
			</section>
			<section class="tab" id="tabD1"></section>
	<table id="driverExpand"bgcolor="#ceecf5" border="0"></table>
			<%}else{ %>Permission Denied<%} %></div>
			<div id="msgDriver"></div>
			</div>
			</div>
	<div id="totalFlags" style="display:none;width:100%;hight:100%z-index: auto;position: absolute;top: 65px;left: 700px;">
	<input type="hidden" name="totalNoOfFlags" id="totalNoOfFlags" value="<%=companyFlags.size()%>"/>
	<%if(dvList!=null &&  dvList.size()>0) {%>
	<img alt=""  src="images/Dashboard/close.png" onclick="removeTotalFlags()" align="right"><h3>Special&nbsp;Request</h3>	
	<table border="1" id="totalFlagsForCompany" style="background-color:lightgrey;"><tr>
		<%int driverCounter=0;
		  int vehicleCounter=0;%>
		<%for(int i=0;i<dvList.size();i++){ %>
			<%if(dvList.get(i).getDriverVehicleSW()==1){ %>	
				<th><input type="hidden" name="shortDescForTotal<%=i%>" id="shortDescForTotal<%=i%>" value="<%=dvList.get(i).getShortDesc() %>"><%=dvList.get(i).getShortDesc() %>
				<br/><a id="shortValueForTotal<%=i%>"/>0</th>
 				<%driverCounter++; %>
 				
 			<%}else if(dvList.get(i).getDriverVehicleSW()==2) { %>
				<th><input type="hidden" name="shortDescForTotal<%=i%>" id="shortDescForTotal<%=i%>" value="<%=dvList.get(i).getShortDesc() %>"><%=dvList.get(i).getShortDesc()%>
					<br/><a id="shortValueForTotal<%=i%>"/>0</th>
				<%vehicleCounter++; %>
 			<%} %>
		<%} %>
		<th>No&nbsp;Flag<br/><a id="noFlag">0</a></th>	
	</tr></table>
<%} %></div>
 				
</body></html>