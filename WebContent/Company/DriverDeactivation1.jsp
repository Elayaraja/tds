<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html>
<head>
<script type="text/javascript">
function cal(){
var fromdate=document.getElementById("from_date").value;
var todate=document.getElementById("to_date").value;
		fromdate = fromdate.substring(6,10)+fromdate.substring(0, 2)+fromdate.substring(3, 5);
		todate = todate.substring(6,10)+todate.substring(0, 2)+todate.substring(3, 5);
		if(Number(fromdate)>Number(todate)) {
			alert("Pls Check FromDate And Todate");
			return false;
		}else {
return true;
}

}
</script>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

 <%
 		   
 	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
 	//String pageFor = request.getAttribute("pagefor").toString();
 	DriverDeactivation deactivation = (DriverDeactivation)request.getAttribute("deactivation")==null?new DriverDeactivation():(DriverDeactivation)request.getAttribute("deactivation");
 %> 
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form name="masterForm" action="control" method="post" onsubmit="return cal()">
	<input type="hidden" name="action" value="admin">
	<input type="hidden" name="event" value="driverTempDeActive">
	
	<table id="pagebox">	
		<tr>
		<td>  
			<table id="bodypage">	
				<tr>
					<td colspan="7" align="center">		
						<div id="title">
							<h2>Driver Temporary Deactivation </h2>
						</div>
					</td>		
				</tr>
				<%
									
					String error = (String) request.getAttribute("error");
			 		if(request.getParameter("error")!=null)
					{
						error = request.getParameter("error");
					}
				%>
				<tr>
					<td colspan="7">
						<div id="errorpage">
							<%= (error !=null && error.length()>0)?""+error :"" %>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<table>	
						<tr>
							<td>&nbsp;</td>
							<td>Driver ID</td>
							<td>&nbsp;</td>
							<td>
								 
								<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername','dName')" id="dd_driver_id" name="dd_driver_id" size="15"   autocomplete="off" value="<%=deactivation.getDriver_id()==null?"":deactivation.getDriver_id() %>" class="form-autocomplete">
								
						  		 <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 			
							
								  <input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">		
							 	  <div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td>From Date</td>
							<td>&nbsp;</td>
							<td>
								 <input type="text" name="from_date" id="from_date" onfocus="showCalendarControl(this);" size="10" value="<%=deactivation.getFrom_date()==null?"":deactivation.getFrom_date() %>" readonly="readonly">
								 
							</td>
							<td>&nbsp;</td>
							<td>To Date</td>
							<td>&nbsp;</td>
							<td>
								 <input type="text" name="to_date" id="to_date" size="10" onfocus="showCalendarControl(this);" value="<%=deactivation.getTo_date()==null?"":deactivation.getTo_date() %>" readonly="readonly">
								 
							</td>
						</tr>	
						<tr>
							<td>&nbsp;</td>
							<td>Description</td>
							<td>&nbsp;</td>
							<td colspan="6" align="center"><textarea name="desc" cols="25" rows="5"><%=deactivation.getDesc()==null?"":deactivation.getDesc() %></textarea></td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<TD colspan="3" align="center">
						<input type="submit" name="Button"  value="Submit"  >&nbsp;&nbsp;&nbsp;
					</TD>
				</tr>		
			</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>