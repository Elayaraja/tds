<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.tds.tdsBO.*"%>
<%@page import="java.util.ArrayList"%>
<html>
<head>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <%
 	ArrayList al_list = (ArrayList)request.getAttribute("al_list") == null ? new ArrayList():(ArrayList)request.getAttribute("al_list");	   
 	String pageFor = request.getAttribute("pagefor").toString() == null ? " ":request.getAttribute("pagefor").toString();	
 %> 
 <script type="text/javascript">
 function chkVoucher(pointer,target)
 {
 	 
 	 
 	  
 	if(document.getElementById(pointer.id).value == 'V')
 	{
 		document.getElementById(target).value = 'V';   	
 	} 
 	if(document.getElementById(target).value == 'V' && document.getElementById(pointer.id).value!='V')
 	{
 		alert("You need to change Card Issuer/Type");
 	}
 	 
 }
 
 function chckAD(pointer,target)
 {
                
 	 
 	if(document.getElementById(pointer.id).value == '3' || document.getElementById(pointer.id).value == '6')
 	{
 		document.getElementById(target).value = 'C';
 	}
 }
 function chckCardIssuer(pointer,target)
 {
 	if(document.getElementById(pointer.id).value != 'C')
 	{	
 		 
 		if(document.getElementById(target).value == '3' || document.getElementById(target).value == '6')
 		{
 			alert("Amex and Discover having only Regular cards");
 			document.getElementById(pointer.id).value ='C';	
 		}
 	}
 	
 }
 function addOneRow()
 {
	 	var i = Number(document.getElementById("size").value);
		var table = document.getElementById("tabid");
		 
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);
		var cell3 = row.insertCell(3);
		var cell4 = row.insertCell(4);
		var cell5 = row.insertCell(5);

		var e1 = document.createElement('input');
	  	e1.type = 'checkbox';
	  	e1.name = 'chck'+i;
	  	e1.id = 'chck'+i;
	    cell.appendChild(e1);
	    
		var element = document.createElement("input");
		element.type = "hidden";
		element.name="CCC_COMPANY_ID"+i;
		element.id="CCC_COMPANY_ID"+i;
		cell.appendChild(element);
		
		var target = 'CCC_CARD_TYPE'+i;
		var element = document.createElement("select");
		element.id="CCC_CARD_ISSUER"+i;
		element.name="CCC_CARD_ISSUER"+i;
		
		element.onchange=function() {chkVoucher(this,target);chckAD(this,target);};
		var theOption = document.createElement("option");
		theOption.text="Master";
		theOption.value="5";
		element.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Visa"; 
		theOption.value="4";
		element.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Amex";
		theOption.value="3";
		element.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Discover";
		theOption.value="6";
		element.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Voucher";
		theOption.value="V";
		element.options.add(theOption);
		cell1.appendChild(element);

		element = document.createElement("select");
		element.id="CCC_CARD_TYPE"+i;
		element.name="CCC_CARD_TYPE"+i;
		var target1 = 'CCC_CARD_ISSUER'+i;
		element.onchange=function() {chkVoucher(this,target1);chckCardIssuer(this,target1);};
		var theOption = document.createElement("option");
		theOption.text="Debit";
		theOption.value="D";
		element.options.add(theOption);
		theOption = document.createElement("option"); 
		theOption.text="Credit";
		theOption.value="C";
		element.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Rewards";
		theOption.value="R";
		element.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Voucher";
		theOption.value="V";
		element.options.add(theOption);
		cell2.appendChild(element);

		

		var element1 = document.createElement("input");
		element1.type = "text";
		element1.name="CCC_TXN_AMT_PERCENTAGE"+i;
		element1.id="CCC_TXN_AMT_PERCENTAGE"+i;
		element1.style.textAlign = "right";
		 
		element1.size = "5";  
		cell3.appendChild(element1);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name="CCC_TXN_AMOUNT"+i;
		element2.id="CCC_TXN_AMOUNT"+i;
		element2.style.textAlign = "right";
		element2.size = "5";
		element2.onblur=function() {insert(element2.id);};
		cell4.appendChild(element2);

		element = document.createElement("select");
		element.id="CCC_SWIPE_TYPE"+i;
		element.name="CCC_SWIPE_TYPE"+i;
		var theOption = document.createElement("option");
		theOption.text="Swiped";
		theOption.value="A";
		element.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text="Manual";
		theOption.value="M";
		element.options.add(theOption);
		cell5.appendChild(element);
		 i = Number(i)+1;
		  
		document.getElementById("size").value=i;
		
		
 }
 </script>
</head> 
<body>
	<form name="masterForm" action="control" method="post">
	<input type="hidden" name="action" value="registration">
	<input type="hidden" name="event" value="companyccsetup">
	<input type="hidden" name="pagefor" value="<%=pageFor %>">
	<input type="hidden" name="size" id="size" value="<%=al_list.size() %>">
	<table id="pagebox">	
		<tr>
		<td>  
			<table id="bodypage">	
				<tr>
					<td colspan="7" align="center">		
						<div id="title">
							<h2>Company CC Setup</h2>
						</div>
					</td>		
				</tr>
				<%
									
					StringBuffer error = (StringBuffer) request.getAttribute("error");
					
					
				%>
				<tr>
					<td colspan="7">
						<div id="errorpage">
							<%= (error !=null && error.length()>0)?"You must fullfilled the following error<br>"+error :"" %>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="7">
						  <%= (request.getAttribute("page") !=null)?request.getAttribute("page").toString():"" %>
					 </td>
				</tr>
				<tr>
					<td>
						<table border=1 id="tabid">	
							<tr><td></td><td>CARD ISSUER</td><td>CARD TYPE</td><td>% AMOUNT</td><td>TXN AMOUNT</td><td>SWIPE TYPE</td></tr>
							<% for(int i=0;i<al_list.size();i++) { 
								CompanyccsetupDO CompanyccsetupDO = (CompanyccsetupDO)al_list.get(i);								
							%>
								<tr>
									<td>
										<input  type="checkbox" name="chck<%=i %>" value="1" <%=request.getParameter("chck"+i)!=null?"":"" %>>
										<input type="hidden" size="10" name="CCC_COMPANY_ID<%=i %>" value="<%=CompanyccsetupDO.getCCC_COMPANY_ID() %>">
									</td>
									<td>
									<select name="CCC_CARD_ISSUER<%=i %>" id="CCC_CARD_ISSUER<%=i %>" onchange="chkVoucher(this,'CCC_CARD_TYPE<%=i %>');chckAD(this,'CCC_CARD_TYPE<%=i %>')">
											<option value="5" <%=CompanyccsetupDO.getCCC_CARD_ISSUER().equals("5")?"selected":"" %>>Master</option>
											<option value="4" <%=CompanyccsetupDO.getCCC_CARD_ISSUER().equals("4")?"selected":"" %>>Visa</option>
											<option value="3" <%=CompanyccsetupDO.getCCC_CARD_ISSUER().equals("3")?"selected":"" %>>Amex</option>
											<option value="6" <%=CompanyccsetupDO.getCCC_CARD_ISSUER().equals("6")?"selected":"" %>>Discover</option>
											<option value="V" <%=CompanyccsetupDO.getCCC_CARD_ISSUER().equals("V")?"selected":"" %>>Voucher</option>
									</select>
										
									</td>
									
									<td>
									<select name="CCC_CARD_TYPE<%=i %>" id="CCC_CARD_TYPE<%=i %>" onchange="chkVoucher(this,'CCC_CARD_ISSUER<%=i %>');chckCardIssuer(this,'CCC_CARD_ISSUER<%=i %>');">
											<option value="D" <%=CompanyccsetupDO.getCCC_CARD_TYPE().equals("D")?"selected":"" %>>Debit</option>
											<option value="C" <%=CompanyccsetupDO.getCCC_CARD_TYPE().equals("C")?"selected":"" %>>Credit</option>
											<option value="R" <%=CompanyccsetupDO.getCCC_CARD_TYPE().equals("R")?"selected":"" %>>Rewards</option>
											<option value="V" <%=CompanyccsetupDO.getCCC_CARD_TYPE().equals("V")?"selected":"" %>>Voucher</option>
									</select>
											
									</td>
									
									<td>
										<input type="text" style="text-align: right" name="CCC_TXN_AMT_PERCENTAGE<%=i %>" id="CCC_TXN_AMT_PERCENTAGE<%=i %>"  size="5" value=" <%=CompanyccsetupDO.getCCC_TXN_AMT_PERCENTAGE() %>" >
									</td>
									<td>
										<input type="text" name="CCC_TXN_AMOUNT<%=i %>" style="text-align: right" id="CCC_TXN_AMOUNT<%=i %>" size="5" onblur="insert(id)" value="$ <%=CompanyccsetupDO.getCCC_TXN_AMOUNT() %>">
									</td>
									<td>
									<select name="CCC_SWIPE_TYPE<%=i %>">
											<option value="A" <%=CompanyccsetupDO.getCCC_SWIPE_TYPE().equals("A")?"selected":"" %>>Swiped</option>
											<option value="M" <%=CompanyccsetupDO.getCCC_SWIPE_TYPE().equals("M")?"selected":"" %>>Manual</option>
																					
										</select>
									
									
										
									</td>
								</tr>
							<%} %>
						</table>
					</td>
				</tr>	
				<tr>
					<TD colspan="3" align="center">
						<input type="button" name="Button"  value="ADD"  onClick='addOneRow()' >&nbsp;&nbsp;&nbsp;
					 
						<%if(pageFor.equalsIgnoreCase("Submit")) {%>
							 
								<input type="submit" name="Button"  value="Submit" >&nbsp;&nbsp;&nbsp;
							 
						<%} else { %>	 
						 
								<input type="submit" name="Button" value="Update"  >&nbsp;&nbsp;&nbsp;
							
						<%} %>	
					</TD>
				</tr>
				<tr>
					<td colspan="3" align="center">
					<input type="submit" name="Button" value="Delete Selected"  >&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
		</td>
		</tr>
	</table>
	</form>
</body>
</html>