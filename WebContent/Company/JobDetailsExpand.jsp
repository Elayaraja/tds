<!DOCTYPE html>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<html>
<head>
<%@page import="java.util.ArrayList"%>

    <title>TDS (Taxi Dispatching System)</title>
<script type="text/javascript">
var mins,secs,TimerRunning,TimerID;
function Pad(number) //pads the mins/secs with a 0 if its less than 10
{
	if(number<10)
		number=0+""+number;
	return number;
}
function Init()//call the Init function when u need to start the timer
{
	mins=1;
	secs=0;
	StopTimer();
}
function StopTimer()
{
	if(TimerRunning)
	clearTimeout(TimerID);
	TimerRunning=false;
	secs= document.getElementById('sec').value;
	StartTimer();
}
function StartTimer(){	
	TimerRunning=true;
	document.getElementById('tid').innerHTML = "Time Remaining for Refresh :"+Pad(secs);
	TimerID=self.setTimeout("StartTimer()",1000);
	if(secs==0)
	{
			StopTimer();
			location.reload(true);
		}
	secs--;
}
</script> 
 <%ArrayList<OpenRequestBO> jobList= (ArrayList<OpenRequestBO>) request.getAttribute("jobList"); %>
    <style type="text/css">
 
        /*** central column on page ***/
        div#divContainer
        {
            margin: 0 auto;
            font-family: Calibri;
            padding: 0.5em 1em 1em 1em;
 
            /* rounded corners */
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
 
            /* add gradient */
            background-color: #808080;
            background: -webkit-gradient(linear, left top, left bottom, from(#606060), to(#808080));
            background: -moz-linear-gradient(top, #606060, #808080);
 
            /* add box shadows */
            -moz-box-shadow: 5px 5px 10px rgba(0,0,0,0.3);
            -webkit-box-shadow: 5px 5px 10px rgba(0,0,0,0.3);
            box-shadow: 5px 5px 10px rgba(0,0,0,0.3);
        }
 
        h1 {color:#FFE47A; font-size:1.5em;}
 
        /*** sample table to demonstrate CSS3 formatting ***/
        table.formatHTML5 {
            width: 100%;
            border-collapse:collapse;
            text-align:left;
            color: #606060;
        }
 
        /*** table's thead section, head row style ***/
        table.formatHTML5 thead tr td  {
            background-color: White;
            vertical-align:middle;
            padding: 0.6em;
            font-size:0.8em;
        }
 
        /*** table's thead section, coulmns header style ***/
        table.formatHTML5 thead tr th
        {
            padding: 0.5em;
            /* add gradient */
            background-color: #808080;
            background: -webkit-gradient(linear, left top, left bottom, from(#606060), to(#909090));
            background: -moz-linear-gradient(top, #606060, #909090);
            color: #dadada;
        }
 
        /*** table's tbody section, odd rows style ***/
        table.formatHTML5 tbody tr:nth-child(odd) {
           background-color: #fafafa;
        }
 
        /*** hover effect to table's tbody odd rows ***/
        table.formatHTML5 tbody tr:nth-child(odd):hover
        {
            cursor:pointer;
            /* add gradient */
            background-color: #808080;
            background: -webkit-gradient(linear, left top, left bottom, from(#606060), to(#909090));
            background: -moz-linear-gradient(top, #606060, #909090);
            color: #dadada;
        }
 
        /*** table's tbody section, even rows style ***/
        table.formatHTML5 tbody tr:nth-child(even) {
            background-color: #efefef;
        }
 
        /*** hover effect to apply to table's tbody section, even rows ***/
        table.formatHTML5 tbody tr:nth-child(even):hover
        {
            cursor:pointer;
            /* add gradient */
            background-color: #808080;
            background: -webkit-gradient(linear, left top, left bottom, from(#606060), to(#909090));
            background: -moz-linear-gradient(top, #606060, #909090);
            color: #dadada;
        }
 
       /*** table's tbody section, last row style ***/
        table.formatHTML5 tbody tr:last-child {
             border-bottom: solid 1px #404040;
        }
 
        /*** table's tbody section, separator row pseudo-class ***/
        table.formatHTML5 tbody tr.separator {
            /* add gradient */
            background-color: #808080;
            background: -webkit-gradient(linear, left top, left bottom, from(#606060), to(#909090));
            background: -moz-linear-gradient(top, #606060, #909090);
            color: #dadada;
        }
 
        /*** table's td element, all section ***/
        table.formatHTML5 td {
           vertical-align:middle;
           padding: 0.5em;
        }
 
        /*** table's tfoot section ***/
        table.formatHTML5 tfoot{
            text-align:center;
            color:#303030;
            text-shadow: 0 1px 1px rgba(255,255,255,0.3);
        }
        /*   table thead { display:block; } */
  		  /* table tbody { height:450px; overflow-y:scroll;overflow-x:hidden;width:100%;} */
        /* JS disabled styles */

/* base nav styles */
nav 
{
    margin: 0 auto 20px;
    position: relative;
    background-color: #FFFFFF;
    font: 16px Tahoma, Sans-serif;
}

/*Temporaly Solution*/
nav ul a 
{ 
   border-bottom: thin; 
   border-bottom-style: solid;      
   border-bottom-color: transparent;
}
/*Solution Ended*/

nav ul 
{ 
    padding:0; 
    margin:0; 
}

nav li 
{ 
    position:relative; 
    float:left; 
    list-style-type:none; 
}

nav ul:after 
{ 
    content:"."; 
    display:block; 
    height:0; 
    clear:both; 
    visibility:hidden; 
}

nav ul a:hover, nav ul > li:hover > a 
{ 
    color: #F90; 
}

nav li a 
{
    display: block;
    padding: 10px 20px;
    color: #333;
    text-decoration: none;
    font-weight: 600;
}

nav li a:focus 
{ 
    outline:none; 
    text-decoration:underline; 
}


</style>
</head>
<body onload="Init()">
 <input type="hidden" name="sec" id="sec" value="10">
    <!-- CENTTERED COLUMN ON THE PAGE-->
    <div id="divContainer"  style="height:550px; overflow-y:scroll;overflow-x:hidden">
        <!-- HTML5 TABLE FORMATTED VIA CSS3-->
        <table class="formatHTML5" >
 
            <!-- TABLE HEADER-->
            <thead>
            <tr><td><a href="control" style="text-decoration:none">Home</a></td><td colspan="13" align="right">
             <div id="tid"></div> </td></tr>
                <tr>
                  <th></th><th>TripID</th><th>P.Time</th><th>Age(m)</th><th>Date</th><th>Name</th><th>Ph.No</th><th>P.Address</th><th>D.Address</th><th>S.Zone</th><th>E.Zone<th>Trip Status</th><th>Cab</th><th>Driver</th>
              
                </tr>
            </thead>
            <!-- TABLE BODY: MAIN CONTENT-->
            <tbody>
            <%for(int i=0;i<jobList.size();i++) {%>
                <tr>
                <%String status="";
					if(jobList.get(i).getTripStatus().equals("30")){
						status="Broadcast";
			        }else if(jobList.get(i).getTripStatus().equals("40")){status="Allocated";
					}else if(jobList.get(i).getTripStatus().equals("43")){status="On Rotue To Pickup";
					}else if(jobList.get(i).getTripStatus().equals("50")){status="Customer Cancelled Request";
					}else if(jobList.get(i).getTripStatus().equals("51")){status="No Show";
					}else if(jobList.get(i).getTripStatus().equals("4")){status="Dispatching";
					}else if(jobList.get(i).getTripStatus().equals("0")){status="Unknown";
					}else if(jobList.get(i).getTripStatus().equals("47")){status="Trip Started";
					}else if(jobList.get(i).getTripStatus().equals("99")){status="Trip On Hold";
					}else if(jobList.get(i).getTripStatus().equals("3")){status="Couldnt Find Drivers";
					}else if(jobList.get(i).getTripStatus().equals("48")){status="Driver Reporting NoShow";
					}else if(jobList.get(i).getTripStatus().equals("49")){status="Soon To Clear";
					}else{
						status=jobList.get(i).getTripStatus();
					}%>
					<td><%=i+1+")"%></td>
                	<td><%=jobList.get(i).getTripid() %></td>
                	<td><%=jobList.get(i).getRequestTime() %></td>
                	<td><%=jobList.get(i).getPendingTime() %></td>
                	<td><%=jobList.get(i).getSdate() %></td>
                	<td><%=jobList.get(i).getName() %></td>
                	<td><%=jobList.get(i).getPhone() %></td>
					<td><%=jobList.get(i).getScity() %></td>
					<td><%=jobList.get(i).getEcity() %></td>
					<td><%=jobList.get(i).getQueueno()%></td>
					<td><%=jobList.get(i).getEndQueueno() %></td>
					<td><%=status %></td> 
					<td><%=jobList.get(i).getVehicleNo() %></td>
					<td><%=jobList.get(i).getDriverid()%></td>
				</tr>
 			<%}%>
            </tbody>
 
           <tfoot>
                <tr><td colspan="12"> </td></tr>
            </tfoot> 
        </table>
    </div>
    
</body>
</html>