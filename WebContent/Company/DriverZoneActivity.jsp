<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.ZoneTableBeanSP" %>
<%@page import="com.tds.cmp.bean.DriverCabQueueBean" %>
<%
String driver="";
String assocode = ((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode();
System.out.println("asssocede:"+assocode);
ArrayList<ZoneTableBeanSP> zones =(ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute( assocode+ "Zones");
ArrayList<DriverCabQueueBean> driversZone = new ArrayList<DriverCabQueueBean>();
if(request.getAttribute("al_list")!=null){
	driversZone = (ArrayList<DriverCabQueueBean>) request.getAttribute("al_list");
}else{
	driversZone=null;
}
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl3.js" language="javascript"></script>
<script type="text/javascript">
   $(document).ready(function() {
		setDate();
   });
   </script>
   <style type="text/css">

#dza th {
	background-color:rgb(46, 100, 121);
      color:white;
}
#dza table{
	background-color:white;
}

#plain1{ min-width:520px;display: inline-block;  background-color: #81DAF5;  }
        #plainJobsDiv { max-height: 450px;min-width:350px; overflow-x: hidden;overflow-y:auto; min-width:100px;display: inline-block; }
        .b { width: 100%;  min-width: 350px;}
        
 .block-top1 {
background: url("../images/Dashboard/block-top-bg.png") no-repeat;
font-size: 13px;
font-weight: bold;
color: #000000;
padding: 9px 0px 9px 18px;
}

.job-avail-dza {
background: url("../images/Dashboard/jobs-avail-mid.png") repeat-y;
width: 100%;
clear: both;
}

</style>
   <script type="text/javascript">
   
  /*  function searchByZone(){
	   alert("Search By Zone click");
   }
   function searchByDriver(){
	   alert("Search By Driver click");
   } */
   
   function setDate(){
	   var curdate = new Date();
		var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();		
		var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
		var cYear = curdate.getFullYear();
		
	 	document.getElementById("dzaFromDate").value =cMonth+"/"+cDate+"/"+cYear;
		document.getElementById("dzaToDate").value =cMonth+"/"+cDate+"/"+cYear;
   }
   
   function searchByBoth(n){
	   //alert("Search By Both click");
	   document.getElementById("showByBoth").innerHTML ="";
	   callOnchange();
	   
	   var zoneid=document.getElementById("zoneIdDZA").value;
	   var driverid=document.getElementById("driverIdDZA").value;
	   if(n==3){
		   if(zoneid!="" && driverid!=""){
			   goToServer(zoneid,driverid);
		   }else{
			   $('#showByBoth').append("ZoneId & DriverID are mandatory");
		   }
	   }else if(n==1){
		   driverid="";
		   document.getElementById("driverIdDZA").value="";
		   if(zoneid!=""){
			   goToServer(zoneid,driverid);
		   }else{
			   $('#showByBoth').append("ZoneId is mandatory");
		   }
	   }else{
		   zoneid="";
		   if(driverid!=""){
			   goToServer(zoneid,driverid);
		   }else{
			   $('#showByBoth').append("DrievrID is mandatory");
		   }
	   }
		
   }
   
   function goToServer(zoneid,driverid){
	   var fDate=document.getElementById("dzaFromDate").value;
	   var tDate=document.getElementById("dzaToDate").value;
	   if(fDate!=null && fDate!="" && tDate!=null && tDate!=""){
	   if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'DashBoard?event=getDriverZoneActivity&from=dash&zoneId='+zoneid+'&driverId='+driverid+'&fromDate='+fDate+'&toDate='+tDate;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var $tbl = $('<table align="Center" width="100%">').attr('id', 'dza');
		$tbl.append($('<tr align="Center">').append(
				$('<th width="12%">').text("ZoneID"),
				$('<th width="14%">').text("DriverId"),
				$('<th width="17%">').text("LoginTime"),
				$('<th width="17%">').text("LogOutTime"),
				$('<th width="40%">').text("Reason")));
		if(text!=""){
			var obj = "{\"driverZoneActivity\":"+text+"}";
			jsonObj = JSON.parse(obj.toString());
			
			if(jsonObj.driverZoneActivity.length>0){
				for(var i=0;i<jsonObj.driverZoneActivity.length;i++){
					if(i%2==0){
						$tbl.append($('<tr align="Center" style="background-color: white">').append(
							$('<td>').text(jsonObj.driverZoneActivity[i].Z),
							$('<td>').text(jsonObj.driverZoneActivity[i].D),
							$('<td>').text(jsonObj.driverZoneActivity[i].LI),
							$('<td>').text((jsonObj.driverZoneActivity[i].LO!="")?jsonObj.driverZoneActivity[i].LO:"Present"),
							$('<td>').text(jsonObj.driverZoneActivity[i].R)
							));
					}else{
						$tbl.append($('<tr align="Center" style="background-color: lime">').append(
								$('<td>').text(jsonObj.driverZoneActivity[i].Z),
								$('<td>').text(jsonObj.driverZoneActivity[i].D),
								$('<td>').text(jsonObj.driverZoneActivity[i].LI),
							//$('<td>').text(jsonObj.driverZoneActivity[i].LO),
								$('<td>').text((jsonObj.driverZoneActivity[i].LO!="")?jsonObj.driverZoneActivity[i].LO:"Present"),
								$('<td>').text(jsonObj.driverZoneActivity[i].R)
							));
					}
				}
			}else{
				$tbl.append($('<tr align="Center">').append(
						$('<th colspan="5" align="center">').text("No Record Found")
				));
			}
		}else{
			$tbl.append($('<tr align="Center">').append(
					$('<th colspan="5" align="center">').text("No Record Found")
			));
		}
		$('#showByBoth').append($tbl);
	   }else{
		   alert("From Date & To Date cannot be empty");
		   $('#showByBoth').append("From-Date & To-Date cannot be empty");
	   }
   }
   
   function callOnchange(){
	   //document.getElementById('sample').style.height='1px';
	   if(document.getElementById("sample")!=null){
		   //alert("sample not null change");
		   //document.getElementById("sample").innerHTML ="";
		   document.getElementById("sample").style.display='none';
	   }
   }
   
   function callOnClick(){
	   //document.getElementById('sample').style.height='1px';
	   if(document.getElementById("sample")!=null){
		   //alert("sample not null click");
		   document.getElementById("sample").innerHTML ="";
		   document.getElementById("sample").style.display='block';
	   }
   }
   
   </script>
</head>
<body>
	<div id="plain1">
	<div style="cursor:move; width: 100%;">
	<b><m style="margin-left:160px">Driver Zone Activity</m>&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;
		<img alt=""  src="images/Dashboard/close.png" onclick="removeDriverZoneActivity(); removeCookieForDriverZoneActivity();">
	</b>
	</div>
	<div class="job-avail-dza">
	<b>
	<table width="auto" border="0" cellspacing="0" cellpadding="0" align="center">
 	<tr align="center">
 		<td align="center">ZoneId:<select name="zoneIdDZA" id="zoneIdDZA">
		<%if(zones!=null){
			for(int i=0; i<zones.size() ;i++){ %>
			<option	<%=zones.get(i).getZoneKey()%> 	value="<%=zones.get(i).getZoneKey()%>" >  <%=zones.get(i).getZoneKey()+" ("+zones.get(i).getZoneDesc()+" )"%></option>
			<%}
		}else{%>
			<option	value="No" >No zones found</option>
		<%} %>	
		</select></td>
		<td align="center">DriverID:
		 <input  type="text"  autocomplete="off" name="driverIdDZA"  id="driverIdDZA"  value="<%=driver %>" onblur="callOnchange()" onclick="callOnClick()"/> 
		</td>
	</tr>
	
	<tr align="left">
 		<td><font color="black">From Date:</font> 
 		<input type="text" name="dzaFromDate" id="dzaFromDate" size="10" value="" onfocus="hideCalendarControl();showCalendarControl(this);" />
 		</td>
	
		<td><font color="black">To Date:&nbsp;&nbsp;&nbsp;&nbsp;</font> 
		<input type="text" name="dzaToDate" id="dzaToDate" size="10" value="" onfocus="hideCalendarControl();showCalendarControl(this);" />
		</td>
	</tr>
    <tr align="center">
    <td colspan="2" align="center"> Search: 
    <input type="button" name="button" id="button" value="By Zone" onclick="searchByBoth('1')"/>
    <input type="button" name="button" id="button" value="By Driver" onclick="searchByBoth('2')"/>
    <input type="button" name="button" id="button" value="Both" onclick="searchByBoth('3')"/></td>
    </tr>
 </table>
 </b>
 </div>
 <ajax:autocomplete
			fieldId="driverIdDZA"
			popupId="sample"
			targetId="driverIdDZA"
			baseUrl="autocomplete.view"
			paramName="DRIVERID"
			className="autocompleteDurai"
  			progressStyle="throbbing" />
	<div class="job-avail-dza" style="width: 100%;">
		<!-- <div  style="overflow:auto;max-height:450px;overflow-x:hidden"> -->
		<div name="showByBoth" id="showByBoth" style="overflow:auto;max-height:450px;overflow-x:hidden; font-size: smaller;" ></div>
		</div>
	<!-- </div> -->
	</div>
	
</body>
</html>
