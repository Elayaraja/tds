<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
   $(document).ready(function() {
		getZones();
   });
   </script>
   <style type="text/css">

#zoneRate th {
	background-color:rgb(46, 100, 121);
      color:white;
}
#zoneRate table{
	background-color:white;
}

</style>
   <script type="text/javascript">
   function getZones(){
	   
		document.getElementById("showRates").innerHTML ="";
		
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'DashBoard?event=getZonerates&from=dash';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"zoneRate\":"+text+"}";
		jsonObj = JSON.parse(obj.toString());
		var $tbl = $('<table align="Center" width="100%">').attr('id', 'zoneRate');
		$tbl.append($('<tr align="Center">').append(
				$('<th width="35%">').text("Start Zone"),
				$('<th width="35%">').text("EndZone"),
				$('<th width="30%">').text("Rate")));
		if(jsonObj.zoneRate.length>0){
			for(var i=0;i<jsonObj.zoneRate.length;i++){
				if(i%2==0){
					$tbl.append($('<tr align="Center" style="background-color: yellow">').append(
						$('<td>').text(jsonObj.zoneRate[i].SZ+" ("+jsonObj.zoneRate[i].SZDesc+")"),
						$('<td>').text(jsonObj.zoneRate[i].EZ+" ("+jsonObj.zoneRate[i].EZDesc+")"),
						$('<td align="Justify">').text("$"+jsonObj.zoneRate[i].Rate)
						));
				}else{
					$tbl.append($('<tr align="Center" style="background-color: lime">').append(
						$('<td>').text(jsonObj.zoneRate[i].SZ+" ("+jsonObj.zoneRate[i].SZDesc+")"),
						$('<td>').text(jsonObj.zoneRate[i].EZ+" ("+jsonObj.zoneRate[i].EZDesc+")"),
						$('<td align="Justify">').text("$"+jsonObj.zoneRate[i].Rate)
						));
				}
			}
		}else{
			$tbl.append($('<tr align="Center">').append(
					$('<th colspan="3" align="center">').text("No Record Found")
			));
		}
		$('#showRates').append($tbl);
	   
   }
   
   </script>
</head>
<body>
	<div id="right-block" style="filter:alpha(opacity=80); opacity: 0.9;">
	<div class="block-top1" style="cursor:move;background-color: white;"><m style="margin-left:65px">Zone Rates</m>&nbsp;&nbsp;&nbsp;
		<img alt="" width="14" height="14" id="refreshZoneRates"  src="images/Dashboard/refresh1.png" onclick="getZones()" >
		&nbsp;&nbsp;
		<img alt=""  src="images/Dashboard/close.png" onclick="removeZoneRateOR()">
	</div>
	<div class="job-avail-mid">
		<div class="job-inner6" style="overflow:auto;max-height:450px;overflow-x:hidden">
		<div name="showRates" id="showRates"></div>
		</div>
	</div>
	</div>
	
</body>
</html>
