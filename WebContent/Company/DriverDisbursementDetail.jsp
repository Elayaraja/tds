<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.pp.bean.PaymentProcessBean"%>
<%@page import="java.math.BigDecimal"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%
	String type = request.getAttribute("type").toString();
%>

<form  name="masterForm" action="control" method="post" onsubmit="return submitThis()">
 <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        
            	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div>
                <div class="rightCol">
		<div class="rightColIn">
		<h2><center>Driver Disbursement Details</center></h2>
 	<table align="center">	
		<tr>
		<td>  
			 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">  
				 
 	<%
 	   ArrayList al_List = (ArrayList)request.getAttribute("al_list");
 	%>		 	
	<% if(al_List !=null && !al_List.isEmpty()) { %>
		<tr> 
			<td colspan="4" align="center">				
				 <table width="100%" align="center" border="2" cellspacing="0" cellpadding="0" class="driverChargTab">  
				<tr align="center">
					 
					<th width="20%">Driver</th>
					<%if(type.equals("1") || type.equals("4")) { %><th>Trip id</th><th>Card No</th><th>Amount</th><th>Tip</th><%} %>
					<%if(type.equals("3")) {%>Description<%} %>
					<th width="20%">Processing Date</th>
					<%if(type.equals("1")){%>
					<th  width="20%">Approval Status</th>
					<th  width="20%">Approval Code</th>
					<%} %>
					<%if(type.equals("1") || type.equals("4")) { %><th>Txn Amount</th><th>Txn %</th><%} %>
					<th  width="20%">Total</th>
				</tr>
				<%double amount=0; %>
				<%for(int i=0;i<al_List.size();i++) {%>
				<% PaymentProcessBean processBean = (PaymentProcessBean)al_List.get(i); %>
				
				<%if(i%2==0) { %>
					<tr align="center">
						<td><%=processBean.getB_driverid() %></td>
						<%if(type.equals("1") || type.equals("4")) { %>
							<td><%=processBean.getB_trip_id() %></td>
							<td><%=processBean.getB_card_no() %></td>
							<td align="right"><%="$ "+new BigDecimal(processBean.getB_amount()).setScale(2,5) %></td>
							<td align="right"><%="$ "+new BigDecimal(processBean.getB_tip_amt()).setScale(2,5) %></td>
						<%} %>
						<%if(type.equals("3")) {%>
							<TD><%=processBean.getB_desc() %></TD>
						<%} %>
						<td ><%=processBean.getProcess_date() %></td>
						<%if(type.equals("1")){%>
							<td><%=processBean.getB_approval_status() %></td>
							<td ><%=processBean.getB_approval_code() %></td>
						<%} %>
						<%if(type.equals("1") || type.equals("4")) { %>
							<td align="right"><%="$ "+processBean.getTxn_amt() %></td>
							<td><%=processBean.getTxn_prec() %></td>
						<%} %>
						<td align="right"><%="$ "+new BigDecimal(processBean.getTxn_total()).setScale(2,5)%></td>
						<%amount = amount + Double.parseDouble(processBean.getTxn_total()); %>
					</tr>
					<%}else { %>
					
						<tr align="center">
						<td style="background-color:lightgreen"><%=processBean.getB_driverid() %></td>
						<%if(type.equals("1") || type.equals("4")) { %>
							<td style="background-color:lightgreen"><%=processBean.getB_trip_id() %></td>
							<td style="background-color:lightgreen"><%=processBean.getB_card_no() %></td>
							<td align="right" style="background-color:lightgreen"><%="$ "+new BigDecimal(processBean.getB_amount()).setScale(2,5) %></td>
							<td align="right" style="background-color:lightgreen"><%="$ "+new BigDecimal(processBean.getB_tip_amt()).setScale(2,5) %></td>
						<%} %>
						<%if(type.equals("3")) {%>
							<TD  style="background-color:lightgreen"><%=processBean.getB_desc() %></TD>
						<%} %>
						<td  style="background-color:lightgreen" ><%=processBean.getProcess_date() %></td>
						<%if(type.equals("1")){%>
							<td  style="background-color:lightgreen"><%=processBean.getB_approval_status() %></td>
							<td  style="background-color:lightgreen" ><%=processBean.getB_approval_code() %></td>
						<%} %>
						<%if(type.equals("1") || type.equals("4")) { %>
							<td align="right"  style="background-color:lightgreen"><%="$ "+processBean.getTxn_amt() %></td>
							<td  style="background-color:lightgreen"><%=processBean.getTxn_prec() %></td>
						<%} %>
						<td align="right"  style="background-color:lightgreen"><%="$ "+new BigDecimal(processBean.getTxn_total()).setScale(2,5)%></td>
						<%amount = amount + Double.parseDouble(processBean.getTxn_total()); %>
					</tr>
					<%} %>
			 	<%} %>
			 	<tr>
			 		<td></td>
			 		<%if(type.equals("1") || type.equals("4")) { %><td></td><td></td><td></td><td></td><%} %>
					<td></td>
					<%if(type.equals("1")){%>
					<td></td><td></td>
					<%} %>
					<%if(type.equals("1") || type.equals("4")) { %><td></td><td></td><%} %>
			 		<td align="right"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
			 	</tr>				 						
				</table>
			</td>
		</tr>
		<tr>
					<tr align="center">
				<td colspan="5" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	   <input type="Button"  value="Close" onclick="javascript:window.close();">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
				</td>
			 
		</tr>
	<%} %>
	
</table>
</td></tr>
</table>
</div>
</div>
</form>
</body>
</html>