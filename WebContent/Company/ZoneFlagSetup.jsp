<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.tds.cmp.bean.CompanyFlagBean"%>
<%@page import="com.tds.tdsBO.ZoneFlagBean"%>

<%@page import="java.util.ArrayList"%><html>
<head>
<style type="text/css">
input,textarea,.uneditable-input{
width:120px !important;
}
</style>
<script type="text/javascript">

   function addRow(id){ 	    

	var i=Number(document.getElementById("size").value)+1;
	var table = document.getElementById('tabid');  
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);

	 document.getElementById('size').value = i;

		var j= Number(document.getElementById('size').value) -1;
	//alert(j)
	//rowCount = rowCount-1; 
 	
    var tbody = document.getElementById(id).getElementsByTagName("TBODY")[0];
    var row = document.createElement("TR"); 
    var td1 = document.createElement("TD"); 
    td1.appendChild(document.createTextNode("Flag"+i)); 
    
     
    var td2 = document.createElement("TD");
	var element1 = document.createElement("input");
	element1.type = "text";
	element1.id = "flag"+j+"_value";
	element1.name = "flag"+j+"_value";
	element1.maxlength = '1';
	element1.size='2';
	td2.appendChild(element1); 

	var td3 = document.createElement("TD");
	element1 = document.createElement("input");
	element1.type = "text";
 	element1.id = "flag"+j;
	element1.name = "flag"+j;
	element1.maxLength = '3'; 
	element1.size='5';
	td3.appendChild(element1); 
    
	var td4 = document.createElement("TD");
	element1 = document.createElement("input");
	element1.type = "text";
	element1.size = '15';
	element1.id = "flag"+j+"_lng_desc";
	element1.name = "flag"+j+"_lng_desc";
 	td4.appendChild(element1); 
     
	
 	row.appendChild(td1);
    row.appendChild(td2);
    row.appendChild(td3);
    row.appendChild(td4); 
    tbody.appendChild(row);
  }
	function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
function deleteFlagForCompany(flagKey,x){
    var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url='systemsetup?event=deletezoneFlag&module=systemsetupView&flagValue='+flagKey;
	alert(url);
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	alert(text);
	if(text=="1"){
		var table=document.getElementById("tabid");
		table.deleteRow(x.parentNode.parentNode.rowIndex);
	} else {
		alert("Flag cannot be deleted");
	}
 }
</script>


<%

	String pageFor = (String)request.getAttribute("pageFor")==null?"":(String)request.getAttribute("pageFor");
	ArrayList al_list = (ArrayList)request.getAttribute("al_list");

	
	
%>
 

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
		<form name="masterForm" action="control" method="post" onsubmit="return showProcess()">
		<input type="hidden" name="action" value="registration">
		<input type="hidden" name="event" value="zoneFlagSetup">
		<input type="hidden" name="pageFor" value="<%=pageFor %>">
		<input type="hidden" name="systemsetup" value="6">		
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
                	<c class="nav-header"><center>Zone Flag Setup</center></c>
                    <table width="100%" style="background-color:rgb(248, 242, 242);" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <%
						String error = (String) request.getAttribute("error")==null?"":(String)request.getAttribute("error");	
					%>
					<tr>
					<td colspan="7">
							 <div id="errorpage">
								<%=(error !=null && error.length()>0)?"<br><font color=\"blue\">"+error +"</font>":"" %>
							</div>
							 <%= (request.getAttribute("page") !=null)?request.getAttribute("page").toString():"" %> 
					</td>
					</tr>
						
					<tr>
					<td>
					<%
								int size = request.getParameter("size")==null?al_list.isEmpty()?1:al_list.size():Integer.parseInt(request.getParameter("size"));
					%></td></tr></table>
					<table width="100%" border="1" height="auto" class="table-striped" align="center" id="tabid">
					<TBODY>
						<tr style="background-color:rgb(102, 255, 204);color:black;">
						 	<TD width="12%"> Flag<input type="hidden" name="size" id="size" value="<%=size %>"></TD> 
						 	<TD width="12%">Value</TD>
						 	<TD width="16%">Short Desc</TD>
						 	<TD width="22%">Long Desc</TD>
						</tr>
						<%if(al_list.isEmpty()) {%>
							<TR>
									<TD>Flag1</TD>
					            	<TD><input type="text" name="flag0_value" id="flag0_value" value="" maxlength="1" size="2"></TD>
					             	<TD><input type="text" name="flag0" id="flag0" value="" size="5" style="text-align: left" maxlength="3" ></TD>
					             	<TD><input type="text" name="flag0_lng_desc" id="flag0_lng_desc" value="" size="15" style="text-align: left"  ></TD>
					    			
								</TR>
							
						<%} else { %>
							<%for(int i=0;i<size;i++) {%>
							<%
							ZoneFlagBean znFlagBean = (ZoneFlagBean)al_list.get(i);
							%>
								<TR style="background-color:rgb(102, 255, 204)">
									<TD>Flag<%=i+1 %></TD>
					            	<TD><input type="text" name="flag<%=i %>_value" id="flag<%=i %>_value" value="<%=znFlagBean.getFlag1_value() %>" maxlength="1" size="2"></TD>
					             	<TD><input type="text" name="flag<%=i %>" id="flag<%=i %>" value="<%=znFlagBean.getFlag1() %>" size="5" style="text-align: left" maxlength="3" ></TD>
					             	<TD><input type="text" name="flag<%=i %>_lng_desc" id="flag<%=i %>_lng_desc" value="<%=znFlagBean.getFlag1_lng_desc() %>" size="15" style="text-align: left"  ></TD>
					    			<td><input type="button" name="deleteFlag" id="deleteFlag" value="Delete" onclick="deleteFlagForCompany('<%=znFlagBean.getFlag1_value()%>',this)">
								</TR>
							<%} %>
						<%} %>
						</TBODY>
						</table>
			            <table align="center">
                          <tr>
                          <td>
                          	<input type="button" value="Add One More" onclick="addRow('tabid')">
                          </td>
                          <td colspan="3" align="center">
                           <%if(pageFor.equalsIgnoreCase("Submit")) {%>
                				 <input type="submit" name="Button" value="Submit" class="lft" >
                    			  <%} else { %>	
                				 <input type="submit" name="Button" value="Update" class="lft"  >
                     <%} %>	
				</td>
                          </tr>
                          
                          </table>
            <div style="background-color:rgb(29, 58, 71);text-align:center;color:white;">Copyright &copy; 2010 Get A Cab</div>
</form>
</body>
</html>