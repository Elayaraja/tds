
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.charges.bean.ChargesBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<%@page import="com.tds.security.bean.*" %>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link rel="stylesheet" href="css/ui.all.css" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<%	ArrayList<ChargesBO	> chargeType =(ArrayList<ChargesBO>)request.getAttribute("chargeTypes");
		ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>();
		if(session.getAttribute("fleetList")!=null) {
			fleetList=(ArrayList)session.getAttribute("fleetList");
		}
		AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
		String value=adminBo.getUsertype(); 
		ArrayList rights= new ArrayList();
		rights=adminBo.getRoles();
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/OpenRequestDetails.js?vNo=<%=TDSConstants.versionNo%>"></script>
 


	<script type="text/javascript">
	
	function showCommentsForHistory(row){
		 var img=document.getElementById("commentButton_"+row).value;
			if(img.indexOf("Show Comments")!= -1){
				document.getElementById("commentButton_"+row).value="Hide Comments";
				$("#comments_"+row).show("slow");
			}else{
				document.getElementById("commentButton_"+row).value="Show Comments";
				$("#comments_"+row).hide("slow");
			}

	}
	
	function showMeterDataForHistory(row){
		var img=document.getElementById("meterData_"+row).value;
		if(img.indexOf("Show MeterData")!= -1){
			document.getElementById("meterData_"+row).value="Hide MeterData";
			$("#meterValues_"+row).show("slow");
		}else{
			document.getElementById("meterData_"+row).value="Show MeterData";
			$("#meterValues_"+row).hide("slow");
		}
	}
	
	 $(document).ready(function(){
		 currentDate(0);
		}); 
	
	 function printHistoryJob(tripId){
			window.open("ReportController?event=reportEvent&ReportName=passengerPayment&output=pdf&tripId="+tripId+"&type=basedMail&assoccode="+document.getElementById("assoccodeHis").value+"&timeOffset="+document.getElementById("timeZoneHis").value);
	 }

	 function currentDate(str) {
		 	//Getting current date for TO date on screen
		 	if( isNaN(str) ){
		 		str=0;
		 	}
			var curdate = new Date();
			var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();		
			var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
			var cYear = curdate.getFullYear();
			
		 	//Getting current date and sbtracting n days to provide n days of history and populate in FROM date on screen
			var prevDate = new Date();
			prevDate.setDate(prevDate.getDate() - str);
			var fDate = prevDate.getDate() >= 10 ? prevDate.getDate() : "0"+prevDate.getDate();		
			var fMonth = prevDate.getMonth()+1 >=10 ? prevDate.getMonth()+1 : "0"+(prevDate.getMonth()+1);
			var fYear = prevDate.getFullYear();

			document.getElementById("fromDate").value =fMonth+"/"+fDate+"/"+fYear;
			document.getElementById("toDate").value =cMonth+"/"+cDate+"/"+cYear;
//			document.getElementById("value").value="";

			getHistoryValues();
			//document.masterForm.shrs.value  = hrsmin; 
		 }
	
	 function startDate() {
		 	//Getting current date for TO date on screen		 	
			var curdate = new Date();
			var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();		
			var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
			var cYear = curdate.getFullYear();					 
			document.getElementById("fromDate").value =cMonth+"/"+cDate+"/"+cYear;
			document.getElementById("toDate").value =cMonth+"/"+cDate+"/"+cYear;
			document.getElementById("value").value="";		
			//getHistoryValues();
		 } 

	 function currentDateByClick(str) {
			currentDate(str);			
			//getHistoryValues();
			//document.masterForm.shrs.value  = hrsmin; 
		 }
		
	 function getHistoryValues(){
		 var fleetNo="";
			if(document.getElementById("fleetSizeH")!=null){
				//alert("fleetsize:"+document.getElementById("fleetSize").value);
				if(document.getElementById("fleetSizeH").value!=0){
					//alert("fleetNo:"+document.getElementById("fleetNo").value);
					if(document.getElementById("fleetNoH").value!="all"){
						fleetNo=document.getElementById("fleetNoH").value;
					}
				}
			}
		 document.getElementById("ORHistoryBody").innerHTML="";
		 if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'RegistrationAjax?event=getHistoryJobs&value='+document.getElementById("value").value+'&fromDate='+document.getElementById("fromDate").value+'&toDate='+document.getElementById("toDate").value+'&download=1'+'&fleetNo='+fleetNo;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if(text!=""){
			var jsonObj = "{\"address\":"+text+"}";
			var objectNew = JSON.parse(jsonObj.toString());
					var $tbl = $('<table style="width:100%">').attr('id', 'historyValues');
					$tbl.append($('<tr>').append(
							$('<th>').text("TripId"),
							$('<th>').text("Trip Date"),
							$('<th>').text("Name"),
							$('<th>').text("Driver ID"),
							$('<th>').text("Vehicle No"),
							$('<th>').text("Phone"),
							$('<th>').text("Start Address"),
							$('<th>').text("End Address"),
							$('<th>').text("Trip Status"),
							$('<th>').text("Amount"),
							$('<th>').text("Pass#"),
							$('<th>').text("Ref No#"),
							$('<th>').text("Meter Data"),
							
							<%if(fleetList!=null && fleetList.size()>0){%>
							$('<th>').text("Fleet"),
							<%} %>
							$('<th>').text("Comments")
					)
					);
					for(var i = 0; i < objectNew.address.length; i++){
							$tbl.append($('<input/>').attr('type', 'hidden').attr('name', 'tripId').attr('id','tripId'+i).val(objectNew.address[i].TI));
							$tbl.append($('<tr>').append(
									$('<td style="width:5%" align="center">').text(objectNew.address[i].TI),
									$('<td style="width:10%" align="center">').text(objectNew.address[i].TD),
									$('<td style="width:5%" align="center">').text(objectNew.address[i].NA),
									$('<td style="width:5%" align="center">').text(objectNew.address[i].DR),
									$('<td style="width:3%" align="center">').text(objectNew.address[i].VN),
									$('<td style="width:7%" align="center">').text(objectNew.address[i].PH),
									$('<td style="width:18%" align="center">').text(objectNew.address[i].SA+""+objectNew.address[i].SA2+""+objectNew.address[i].SC),
									$('<td style="width:18%" align="center">').text(objectNew.address[i].EA+""+objectNew.address[i].EA2+""+objectNew.address[i].EC),
									$('<td style="width:12%" align="center">').text(objectNew.address[i].TS),
									$('<td style="width:3%" align="center">').text(document.getElementById("currecyPrefix").value+""+objectNew.address[i].AM),
									$('<td style="width:4%" align="center">').text(objectNew.address[i].NP),
									$('<td style="width:7%" align="center">').text(objectNew.address[i].RF),
									
									//$('<td style="width:3%" align="center">').text( (objectNew.address[i].GPS=="Yes")?document.getElementById("currecyPrefix").value+""+objectNew.address[i].MP:"-"),
									$('<td style="width:3%" align="center">').append( (objectNew.address[i].GPS=="Yes")?$('<input/>').attr('type', 'button').attr('name', 'meterData_'+i).attr('id','meterData_'+i).val("Show MeterData").attr('onclick','showMeterDataForHistory('+i+')'):"-"),
									
									<%if(fleetList!=null && fleetList.size()>0){%>
									$('<td style="width:5%" align="center">').text(objectNew.address[i].FL),
									<%} %>
									$('<td style="width:5%" align="center">').append($('<input/>').attr('type', 'button').attr('name', 'commentButton_'+i).attr('id','commentButton_'+i).val("Show Comments").attr('onclick','showCommentsForHistory('+i+')')),
									$('<td style="width:5%">').append($('<input/>').attr('type', 'button').attr('name', 'printJobButton_'+i).attr('id','printJobButton_'+i).val("Print").attr('onclick','printHistoryJob('+objectNew.address[i].TI+')')),
									$('<td style="width:5%">').append($('<input/>').attr('type', 'button').attr('name', 'detailShow_'+i).attr('id','detailShow_'+i).val("Details").attr('onclick','openORDetails('+objectNew.address[i].TI+')')),
									$('<td style="width:5%">').append($('<input/>').attr('type', 'button').attr('name', 'reDispatchButton_'+i).attr('id','reDispatchButton_'+i).val("ReDispatch").attr('onclick','reDispatch('+i+')'))
						)
						);
						$tbl.append($('<tr id="comments_'+i+'" style="display:none;">').append(
								$('<td colspan="8" style="font-weight: bolder;">').text('DC:'+objectNew.address[i].CM+' CC:'+objectNew.address[i].SI)
						)
						);
						if(objectNew.address[i].GPS=="Yes"){
						$tbl.append($('<tr id="meterValues_'+i+'" style="display:none;">').append(
								$('<td colspan="14" style="font-weight: bolder;">').text('Meter Amount:'+objectNew.address[i].MP+' - Is Hidden:'+objectNew.address[i].GHidden+' - Time Accured (secs):'+objectNew.address[i].GTAIS+' - Distance Accured (miles):'+objectNew.address[i].GDAIM+' - Amount Calculated For Time:'+objectNew.address[i].GTC
										+' - Amount Calculated For Distance:'+objectNew.address[i].GDC+' - Start Amount:'+objectNew.address[i].GST+' - Minimum Speed:'+objectNew.address[i].GMS)
						)
						);
						}
	 				}
					$("#ORHistoryBody").append($tbl);
					$("#ORHistoryBody").show("slow");
				}
		 	}
	 
	 
	 
	 
	</script>
 <script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var JobHistory = $("#ORHistoryBody").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            printWindow.document.write('<html><head><title>Job History</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(JobHistory);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
</head>
<body>
	<form name="masterFormHistory" id="masterFormHistory" action="control" method="post">
	<input type="hidden" name="action" value="openrequest" /> 
	<input type="hidden" name="event" value="openRequestHistoryWithLogs" /> 
	<input type="hidden" name="module" id="module" value="dispatchView" />
	<input type="hidden" name="transId" id="transId" value="" />
	<input type="hidden" id="currecyPrefix" value="<%=adminBo.getCurrencyPrefix()==1?"$":adminBo.getCurrencyPrefix()==2?"&euro;":adminBo.getCurrencyPrefix()==3?"&pound;":"&#x20B9;"%>"/>
	<input type="hidden" name="screenValue" id="screenValue" value="1" />	
	<input type="hidden" name="fleetSizeH" id="fleetSizeH" value="<%=fleetList.size()%>" />	
	<input type="hidden" name="assoccode" id="assoccodeHis" value="<%=adminBo.getMasterAssociateCode()%>" />	
	<input type="hidden" name="timeZone" id="timeZoneHis" value="<%=adminBo.getTimeZone()%>" />	
	<div id="historyHeader">			      
	<%-- <%if(fleetList!=null&&fleetList.size()>0){%>
      <div id="fleetDiv" style="  -moz-border-radius: 8px 8px 8px 8px;align: right; width: 180px; height: 62px; position: absolute; top: 0px;left: 120pt;">
          <table><tr>
	<%for(int i=0;i<fleetList.size();i++){ 
		if(i%4==0){%>
			<tr>
	    <%}%>
				<td>
					<input type="button" name="fleet_<%=i%>" class="fleetbutton"  value ="<%=fleetList.get(i).getFleetName()%>" id ="fleet_<%=i%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBo.getAssociateCode())?"style='background-color:green'":""%> onclick="changeFleet('<%=fleetList.get(i).getFleetNumber()%>','<%=i%>')"  />
					<input type="hidden" name="fleetNumberForCookie<%=i %>" id="fleetNumberForCookie<%=i %>" value="<%=fleetList.get(i).getFleetNumber()%>"/>
					<input type="hidden" name="changedFleet" id="changedFleet" value=""/>
	 			</td>
	<%} %>
          </tr>
         </table></div>
  <%} %> --%>
			
		<table width="100%" style="background-color: #EFEFFB" cellspacing="1" cellpadding="0" class="driverChargTab">
			<tr class="headerRowORH" style="cursor: move">
				<td colspan="7" align="center" style="background-color: #CEECF5;"><h5>
						<center>
							Jobs History&nbsp;<a style="margin-left: 60px" href="#"><img
								alt="" src="images/Dashboard/close.png" style="size: 20px;"
								onclick="removeORHistory();hideCalendarControl()" />							
							</a>
						</center>
					</h5>
					</td>
				</tr>
				<tr>
				
				<%-- <%
				if(rights.contains("5103")) {%>
				<%} %> --%>
				<%if( fleetList!=null && fleetList.size()>0) {%>
							<td>Select Fleet : 
							<select name="fleetNoH" id="fleetNoH" >
							
							<%if(rights.contains("5103")) {%>
							<option value="all" >All</option>
		                      <%for(int j=0;j<fleetList.size();j++){ %>
										<option value="<%=fleetList.get(j).getFleetNumber()%>" ><%=fleetList.get(j).getFleetName() %></option>
										<%=fleetList.get(j).getFleetNumber()%>
							  <%}%> 
							<%}else{ 
								for(int j=0;j<fleetList.size();j++){
									if(fleetList.get(j).getFleetNumber().equalsIgnoreCase(adminBo.getAssociateCode())){%>
										<option value="<%=fleetList.get(j).getFleetNumber()%>" ><%=fleetList.get(j).getFleetName() %></option>
												<%=fleetList.get(j).getFleetNumber()%>
									<%break;} }%>
							<%} %>
		                       </select>
		                       </td>
				<%}%>
								
				<td><font color="black">Search
				</font> <input type="text" name="value" id="value" size="10" value=""  onfocus="hideCalendarControl();" />
				</td>	
				<td><font color="black">From Date:</font> <input type="text"
					name="fromDate" id="fromDate" size="10" value=""
					onfocus="hideCalendarControl();showCalendarControl(this);" /></td>
	
				<td><font color="black">To
						Date:&nbsp;&nbsp;&nbsp;&nbsp;</font> <input type="text" name="toDate"
					id="toDate" size="10" value=""
					onfocus="hideCalendarControl();showCalendarControl(this);" /></td>
				
				<td><input type="button" name="lastOneWeek" id="lastOneWeek" size="10%"
					value="Last One Week" onclick="currentDateByClick(7)"/></td>
					
				<td><input type="button" name="lastTwoWeek" id="lastTwoWeek" size="10%"
					value="Last Two Weeks" onclick="currentDateByClick(14)"/></td>
				
				<td><input type="button" name="search" id="search" size="10%"
					value="Search" onfocus="hideCalendarControl();"
					onclick="getHistoryValues()" /></td>					
				<%-- <%} else{%>				
				<td><input type="hidden" name="value" id="value" size="10" value="" onblur="currentDate(0)" />
				</td>
				
				<td align="right"><font color="black">From Date:</font> 
				<input type="text"  readonly="readonly" name="fromDate" id="fromDate" size="10"  /></td>
				<td></td>
				<td><font color="black">To Date:</font> 
				<input type="text"  readonly="readonly" name="toDate" id="toDate" size="10" value="" /></td> --%>
					 		
				</tr>		
				 <tr><td>
			 <input type="button" value="Print" id="btnPrint" />
         	</td></tr>
         	 <tr><td>
				<div id="reDispatchMessage" style="display: none;"></div>
				</td></tr> 
			</table>
		</div>
		<div id="ORHistoryBody"  style="display: none;overflow-y: scroll;max-height: 420px;"></div>
		</form>
</body>
	
</html>
