<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="js/jquery.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<script type="text/javascript" src="js/jqModal.js"></script>

<script type="text/javascript" src="js/jspdf.js"></script>
<title>Error Reports</title>
<script>
function downLoadErrorReport(){
var doc = new jsPDF();

// We'll make our own renderer to skip this editor
var specialElementHandlers = {
	'#editor': function(element, renderer){
		return true;
	}
};

// All units are in the set measurement for the document
// This can be changed to "pt" (points), "mm" (Default), "cm", "in"
doc.fromHTML($('body').get(0), 15, 15, {
	'width': 170, 
	'elementHandlers': specialElementHandlers
});
}
function showStackTrace(values){
	document.getElementById("showDetails").innerHTML ="";
	document.getElementById("showDetails").innerHTML =document.getElementById("st"+values).innerHTML;
	$('#showDetails').jqm();
	$('#showDetails').jqmShow();
}
function showlogCat(values){
	document.getElementById("showDetails").innerHTML ="";
	document.getElementById("showDetails").innerHTML =document.getElementById("lc"+values).innerHTML;
	$('#showDetails').jqm();
	$('#showDetails').jqmShow();
}
</script>
<style>
td:hover{
	opacity: 0.9;
	filter: Alpha(opacity =   90); /* IE8 and earlier */
	cursor: pointer;
}
#showDetails{
background-color: #F3F3F3;
    border: 1px solid #CCCCCC;
    height: 29em;
    left: 20%;
    margin-left: -15em;
    margin-top: -9em;
    position: fixed;
    top: 10%;
    width: 75em;
    overflow: auto;
    margin-top: -10px;
}
#headerBar{
	 position: fixed;
	 width:100%;
}
</style>
<%ArrayList<String> alist=null;
	 if(request.getAttribute("alist")!=null){
	 alist = (ArrayList<String>)request.getAttribute("alist");} %>
</head>
<body>
<!-- <input type="button" onclick="downLoadErrorReport()" value="Print Report"/> -->
<table style="border-style: inherit;">
<tr bgcolor="#777" style="color:white; margin-top: -10px;" id="headerBar"><th style="width:3%; ">A.V.No</th><th style="width:60%">Stack Trace </th><th style="width:50%">Log Cat</th></tr>
<%for(int i=0;i<alist.size();i=i+4){ %>
<tr><td><%=alist.get(i)%></td>
	<td bgcolor="lightgrey" onclick="showStackTrace('<%=i%>' )" id="st<%=i%>"><br/><b><%=alist.get(i+3)%></b><br/><%=alist.get(i+1) %></td>
	<td bgcolor="#EFEFFB" onclick="showlogCat('<%=i%>')"  id="lc<%=i%>"><%=alist.get(i+2) %></td>
</tr>
<%} %>
</table>
	<div class="jqmWindow" id="showDetails" ></div>
</body>
</html>