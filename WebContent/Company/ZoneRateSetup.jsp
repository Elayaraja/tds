<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.tds.cmp.bean.CompanyFlagBean"%>
<%@page import="com.tds.tdsBO.ZoneRateBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.QueueCoordinatesBO"%>
<%
	ArrayList<QueueCoordinatesBO> zones =(ArrayList<QueueCoordinatesBO>)request.getAttribute("allzones");
%>
<%@page import="java.util.ArrayList"%><html>
<head>
<style type="text/css">
input,textarea,.uneditable-input{
width:120px !important;
}
</style>
<script type="text/javascript">
   var text="";
   $(document).ready(function() {
		getZones();
   });
   
   
   function submitCharges() {
		var size = document.getElementById("size").value;
		if (size == "0" || size=="") {
			alert("Add any new charges and then submit");
			return false;
		} else {
	    	document.masterForm.submit();
		}
   }
	
   
   function addRow(){
	var i=Number(document.getElementById("size").value)+1;
	document.getElementById('size').value = i;
	var table = document.getElementById("zoneRateCharge");
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);

 	var jsonObj = "{\"flagDetails\":"+text+"}";
	var obj = JSON.parse(jsonObj.toString());

    var element1 = document.createElement("select");
	element1.id = "rate"+i+"_stzone";
	element1.name = "rate"+i+"_stzone";
	for(var j=0;j<obj.flagDetails.length;j++){
	 	var theOption = document.createElement("option");
		theOption.text = obj.flagDetails[j].QID+"["+obj.flagDetails[j].QDES+"]";
		theOption.value = obj.flagDetails[j].QID;
		element1.maxLength = '150'; 
		element1.options.add(theOption);
	}
 	cell1.appendChild(element1);
 
	var element2 = document.createElement("select");
    element2.id = "rate"+i+"_endzone";
	element2.name = "rate"+i+"_endzone";
	for(var j=0;j<obj.flagDetails.length;j++){
	 	var theOption1 = document.createElement("option");
		theOption1.text = obj.flagDetails[j].QID+"["+obj.flagDetails[j].QDES+"]";
		theOption1.value = obj.flagDetails[j].QID;
		element2.maxLength = '150'; 
		element2.options.add(theOption1);
	}
 	cell2.appendChild(element2);
	var element3 = document.createElement("input");
	element3.type = "text";
	element3.size = '15';
	element3.id = "rate"+i+"_amount";
	element3.name = "rate"+i+"_amount";
 	cell3.appendChild(element3);
   }
  function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
	function deleteRateForZone(szone,ezone,currentRow){
	    var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url='systemsetup?event=deleteZoneRate&startzone='+szone+"&endzone="+ezone;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text2 = xmlhttp.responseText;
		if(text2=="001"){
			var table = document.getElementById("zoneRateCharge");
			table.deleteRow(currentRow.parentNode.parentNode.rowIndex);
			document.getElementById('size').value = "0";
		} else {
			alert("Zone cannot be deleted");
		}
	 }
	function getZones() {
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'systemsetup?event=getZones';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		text = xmlhttp.responseText;
	}
</script>
<%
	String pageFor = (String)request.getAttribute("pageFor")==null?"":(String)request.getAttribute("pageFor");
	ArrayList<ZoneRateBean> al_list = (ArrayList<ZoneRateBean>)request.getAttribute("al_list");
%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
		<form name="masterForm" action="control" method="post" onsubmit="return showProcess()">
		<input type="hidden" name="action" value="registration">
		<input type="hidden" name="event" value="rateforZone">
		<input type="hidden" name="pageFor" value="<%=pageFor %>">
		<input type="hidden" name="systemsetup" value="6">		
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <c class="nav-header"><center>Zone Rate Setup</center></c>
                    <table width="45%" style="background-color:rgb(248, 242, 242);" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <%
						String error = (String) request.getAttribute("error")==null?"":(String)request.getAttribute("error");	
					%>
					<tr>
					<td colspan="7">
							 <div id="errorpage">
								<%=(error !=null && error.length()>0)?"<br><font color=\"blue\">"+error +"</font>":"" %>
							</div>
							 <%= (request.getAttribute("page") !=null)?request.getAttribute("page").toString():"" %> 
					</td>
					</tr>
					<tr>
					<td>
					<%
					int size = request.getParameter("size")==null?al_list.isEmpty()?0:al_list.size():Integer.parseInt(request.getParameter("size"));
					%></td></tr></table>
					<table width="52%" border="1" height="auto" class="table-striped" align="center" id="zoneRateCharge">
					<TBODY>
						<tr style="background-color:rgb(102, 255, 204);color:black;">
						 	<input type="hidden" name="size" id="size" value="<%=size %>"> 
						 	<TD width="12%">Start Zone</TD>
						 	<TD width="16%">End Zone</TD>
						 	<TD width="22%">Amount</TD>
						</tr>
						<%if(al_list.size()>0) {%>
							<%for(int i=0;i<al_list.size();i++) {%>
								<TR style="background-color:rgb(102, 255, 204)">
					            	<TD><input type="text"  id="rate<%=i %>_stzone" value="<%=al_list.get(i).getStartzone() %>"  readonly></TD>
					             	<TD><input type="text"  id="rate<%=i %>_endzone" value="<%=al_list.get(i).getEndzone() %>"   readonly ></TD>
					             	<TD><input type="text"  id="rate<%=i %>_amount" value="<%=al_list.get(i).getAmount() %>" readonly></TD>
					    			<td><input type="button" name="deleteFlag" id="deleteFlag" value="Delete" onclick="deleteRateForZone('<%=al_list.get(i).getStartzone()%>','<%=al_list.get(i).getEndzone() %>',this)">
					    		</TR>
							<%} %>
						<%} %>
						</TBODY>
						</table>
			            <table align="center">
                          <tr>
                          <td>
                          	<input type="button" value="Add One More" onclick="addRow()">
                          </td>
                          <td colspan="3" align="center">
                				 <input type="submit" name="Button" value="Submit" onclick="return submitCharges()" >
						</td>
                          </tr>
                          </table>
            <div style="background-color:rgb(29, 58, 71);text-align:center;color:white;">Copyright &copy; 2010 Get A Cab</div>
</form>
</body>
</html>