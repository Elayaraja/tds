<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.cmp.bean.CompanyFlagBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.QueueBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.dao.AdministrationDAO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>


<%-- <jsp:useBean id="adminBO"   class="com.tds.tdsBO.AdminRegistrationBO" /> --%>
<%
	AdminRegistrationBO adminBO = (AdminRegistrationBO) request
			.getSession().getAttribute("user");
%>
<%-- <%		QueueBean queue = (QueueBean)request.getSession().getAttribute("user"); %> --%>



<html>
<head>
<%-- <jsp:useBean id="queue"   class="com.tds.tdsBO.QueueBean" />  --%>

<link href="css/dashboardHeader.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Ajax/DashBoard.js"></script>

<%
	ArrayList<QueueBean> al_list = (ArrayList<QueueBean>) request
			.getAttribute("al_list");
%>

<%
	QueueBean queue = new QueueBean();
%>
<style type="text/css">
#ele_li li {
	background-image: url(images/li_img.png);
	list-style-type: none;
	padding-left: 18px;
	background-repeat: no-repeat;
	margin-top: 3px;
}
</style>
<link href="css/newtdsstyles.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<html>
<body>
	<div class='ScrollFormDriverGrid'>
		<div class='rightCol'>
			<div class='rightColIn'>

				<div class='dashboardDV'>
					 <table align="right" width='10%'>
						<input type="button" name="Refresh"  value="Refresh"
							onclick="dashBoardAjax2();Init()" />
					</table> 
					<p><table border=1 align=\ "left\" class='dashboardDataTable'
						width='50%' onload="Init();">
					<!-- <td  align='right' width='100%' ><input type="button" name="Refresh"  value="Refresh"
							onclick="dashBoardAjax2();Init()" /></td>
 -->
						}


						<tr>
							<td colspan='4' align='center'><h1>
									Zone Waiting <br />
									<br /> Driver in the zones:<%=al_list.get(0).getQU_WAITING()%>
									Jobs in the zones:<%=al_list.get(0).getQU_JOBWAITING()%>
								</h1>
							</td>
						</tr>
						


						<table align="center" border="1" >
						<tr><td>
						<%
							if (al_list != null && !al_list.isEmpty()) {
								String cur_n, prev_n = "";

								boolean colorLightGreen = true;
								String colorPattern;
								for (int i = 0, j = 0; i < al_list.size()- 1;i++, j++) {
									colorLightGreen = !colorLightGreen;
									if (colorLightGreen) {
										colorPattern = "style=\"background-color:lightgreen\"";
									} else {
										colorPattern = "style=\"background-color:white\"";
									}

									int flg = al_list.get(i).getFlg();
									String driver = al_list.get(i).getQU_DRIVERID();
						%>
						<%
							cur_n = al_list.get(i).getQU_NAME();
									if (!cur_n.equalsIgnoreCase(prev_n)) {
						%>
						<table align="left" border="1" >
						<tr>
							<td colspan='4' align='center' style="background-color: skyblue">
								<font color='black' size='3.5'><B><%=al_list.get(i).getQU_NAME()%></B>
							</font></td>
						</tr>
						<tr>
						<th width="100px"><B>Driver</B>
							</th>
							<th width="100px"><B>Driver Profile</B>
							</th>
							<th width="100px"><B>Delete From Queue</B>
							</th>
							<th width="100px"><B>Move To Top</B>
							</th></tr>
						<%
							j = 0;
									}

									if (i % 2 == 0) {
										if (flg == 1
												&& driver.equalsIgnoreCase(al_list.get(i)
														.getQU_DRIVERID())) {
						%>
						<tr id='highlightrow'>
							<%
								} else {
							%>
						
						<tr class='even'>
							<%
								}
										} else {
											if (flg == 1
													&& driver.equalsIgnoreCase(al_list.get(i)
															.getQU_DRIVERID())) {
							%>
						
						<tr id='highlightrow'>
							<%
								} else {
							%>
						
						<tr class='odd'>
							<%
								}
										}

										if (al_list.get(i).getQU_DRIVERNAME().equals("")) {
							%>
							<td></td>
							<td></td>
							<td></td>
							<%
								} else {
							%>
							
<td  <%=colorPattern%>class="zone"><font color='black'>
								<a  onmouseover="loginTime()"> <%=al_list.get(i).getQU_DRIVERNAME()%>
									<em> <%=al_list.get(i).getQU_LOGINTIME() %></em> </a></font>
							</td>
									
							<td <%=colorPattern%>><font size='2.5' color='black'>
									<%=al_list.get(i).getDriverProfile()%> </font>
							</td>
							<td <%=colorPattern%>><div class='btnBlue'>
									<div class='rht'>
										<input type='button' class='lft' value='Delete'
											onclick="removeFromQ('<%=al_list.get(i).getQU_DRIVERID()%>','<%=adminBO.getAssociateCode()%>')">
									</div>
								</div>
							</td>

							<%
								if (j != 0) {
							%>
							<td <%=colorPattern%>><div class='btnBlue'>
									<div class='rht'>
										<input type='button' class='lft' value='Move To Top'
											onclick="changeFromQ('<%=al_list.get(i).getQU_DRIVERID()%>','<%=adminBO.getAssociateCode()%>')">
									</div>
								</div>
							</td>
						</tr>

						<%
							}
										prev_n = al_list.get(i).getQU_NAME();
									}
								}
							}
						%>
					</table> </td></table></table></p>
				</div>
			</div>
		</div>
	</div>

</body>
</html>