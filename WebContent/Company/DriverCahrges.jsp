<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.cmp.bean.DriverChargeBean"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%><html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
.ss 
{
text-align:right;
}

</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
 <%
 	ArrayList al_list = (ArrayList)request.getAttribute("al_list");	   
 String pageFor = request.getAttribute("pagefor") == null?"Submit":request.getAttribute("pagefor").toString();
 %> 
</head>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
function delrow(){
	try {
	var table = document.getElementById("add");
	var rowCount = table.rows.length;
	var temp=(Number(document.getElementById("sizevalue").value)+1);
	rowCount--;
	if((Number(document.getElementById("sizevalue").value)+1)<rowCount){
	table.deleteRow(rowCount);
	document.getElementById("size").value=rowCount-2;
	}
	}catch(e){alert(e.message);}
	
	
}

var temp=1;
function cal(){
	var i=Number(document.getElementById("size").value)+1;
	
	var table = document.getElementById("add");
	 
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	 var cell = row.insertCell(0);
	  var cell1 = row.insertCell(1);
	  var cell2 = row.insertCell(2);
	  var cell3 = row.insertCell(3);
	  var cell4 = row.insertCell(4);
	   
	   
	  var element = document.createElement("input");
	  element.type = "text";
	  element.name="DC_DESC"+i;
	  element.align='right'; 
	  element.size = '10';
	  element.type = "text";
	  element.name="DC_DESC"+i;
	  element.align='right';
	  cell.appendChild(element);
	  
		
		
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name="DC_AMOUNT"+i;
		element2.id="DC_AMOUNT"+i;
		element2.onblur=function() {insert(element2.id);};
		element2.size = '5';
		element2.className= 'ss';
		cell1.appendChild(element2);
		
		var element3 = document.createElement("select");
	 	 element3.name="DC_STATUS"+i;
		 element3.id="DC_STATUS"+i;
		 cell2.appendChild(element3);
		document.getElementById("DC_STATUS"+i).options[0]=new Option("Active", "1", true, false);
		document.getElementById("DC_STATUS"+i).options[1]=new Option("InActive", "2", false, false);
		var fre="DC_FREQUECY"+i;
		var day='freq_day'+i+'';
		
		var element1 =  document.createElement("select");
		element1.id="DC_FREQUECY"+i;
		element1.name="DC_FREQUECY"+i;
		element1.onchange=function() {onselectcal(element1.id,day,i);};
		cell3.appendChild(element1);

		document.getElementById("DC_FREQUECY"+i).options[0] = new Option("Weekly", "1", false, false);
		document.getElementById("DC_FREQUECY"+i).options[1] = new Option("Monthly", "2", false, false);

		var element12 = document.createElement("Div");
	 	element12.id="freq_day"+i;
		cell4.appendChild(element12);
		
		var element5 = document.createElement("input");
	 	element5.type = "hidden";
		 element5.name="DC_CHARGE_NO"+i;
		element5.value="";
		cell3.appendChild(element5);
		document.getElementById("size").value=i;
	
	}
function onselectcal(idv,dropv,row){
	
	   if(document.getElementById(idv).value == "2") {
	  	document.getElementById(dropv).innerHTML="<select name='lday"+row+"'><option value='1'>First Day</option><option value='2'>Last Day</option></select>";
   		}else{
 	  			document.getElementById(dropv).innerHTML="";
		 }
	
}
function checkval(){
	
			//document.masterForm.action = "/TDS/control?action=registration&event=driverChargesMaster";
			document.masterForm.submit();
	
	
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}

</script>
<body>
<form name="masterForm" action="control" method="get" onsubmit="return showProcess()">
	<input type="hidden" name="action" value="registration">
	<input type="hidden" name="event" value="driverChargesMaster">
	<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	<input type="hidden" id="size" name="size" value="<%=al_list==null?"0":(""+(al_list.size()-1)) %>">
	<input type="hidden" id="sizevalue" name="sizeValue" value="<%=al_list==null?"0":(""+(al_list.size()-1)) %>">
	<input type="hidden" name="systemsetup" value="6">

                	<c class="nav-header"><center>Driver Charges</center></c><br>
					<div id="errorpage">
                    <table width="80%" border="0" cellspacing="0" cellpadding="0" id="add2">
                    <%
						StringBuffer error = (StringBuffer) request.getAttribute("error");
						String success  = (String) request.getAttribute("success");
					%>
					<tr>
					<td colspan="7">
							<center><font color="red"><%= (error !=null && error.length()>0)?"*You must fullfilled the following error<br>"+error :"" %></font></center>
							<font color="blue"><center><%= (success !=null && success.length()>0)?success:"" %></center></font>
							
						
					</td>
				  </tr></table></div>
				  <table width="80%" border="1" cellspacing="0" cellpadding="0" id="add">
                      <tr style="text-align:center;background-color:#116969;color:#ffffff">
                        <th width="15%">Description</th>
                        <th width="15%">Amount</th>
                        <th width="15%">Status</th>
                        <th width="20%">Frequency</th>
                        <th width="20%">Day Option</th>
                      </tr>
                      <%
							for(int i=0;i<al_list.size();i++) { 
							DriverChargeBean chargeBean = (DriverChargeBean)al_list.get(i);								
					  %>
                      
                      <tr>
							<td> 
								<input type="hidden" size="10" name="DC_CHARGE_NO<%=i %>" value="<%=chargeBean.getDC_CHARGE_NO() %>">
								<input type="text"  style="text-align: left" size="10" name="DC_DESC<%=i %>" value="<%=chargeBean.getDC_DESC() %>">
							</td>
									
							<td>
								<%String ss=chargeBean.getDC_AMOUNT().split(" ")[0];%> 
								<%if(pageFor.equalsIgnoreCase("Submit") && i==0) {%>
								<input type="text"  style="text-align: right" onblur = "insert(id);" id="DC_AMOUNT<%=i %>" name="DC_AMOUNT<%=i %>" size="5" value="<%=chargeBean.getDC_AMOUNT().equals("")?"":"$ "+chargeBean.getDC_AMOUNT() %>">
								<%} else { %>
								<input type="text" style="text-align: right" name="DC_AMOUNT<%=i %>" onblur = "insert(id);" id="DC_AMOUNT<%=i %>" size="5" value="<%=ss.equals("$")?chargeBean.getDC_AMOUNT():"$ "+chargeBean.getDC_AMOUNT() %>">
								<%} %>
							</td>
							<td>
								<select name="DC_STATUS<%=i %>">
								<option value="1" <%=chargeBean.getDC_STATUS().equalsIgnoreCase("1")?"selected":"" %> >Active</option>
							    <option value="2" <%=chargeBean.getDC_STATUS().equalsIgnoreCase("2")?"selected":"" %>>Inactive</option>
								</select>
							</td>
							<td>
	                                         								
								<select name="DC_FREQUECY<%=i %>" id="DC_FREQUECY<%=i %>" onchange="onselectcal(id,'displayday<%=i %>',<%=i %>)">
								<%--<option value="1" <%=chargeBean.getDC_FREQUECY().equals("1")?"selected":"" %>>Daily</option> --%>
							    <option value="1" <%=chargeBean.getDC_FREQUECY().equals("1")?"selected":"" %>>Weekly</option>
								<option value="2" <%=chargeBean.getDC_FREQUECY().equals("2")?"selected":"" %>>Monthly</option>
								<%--
								<option value="4" <%=chargeBean.getDC_FREQUECY().equals("4")?"selected":"" %>>Fortnightly</option>
								<option value="5" <%=chargeBean.getDC_FREQUECY().equals("5")?"selected":"" %>>Quarterly</option>
								<option value="6" <%=chargeBean.getDC_FREQUECY().equals("6")?"selected":"" %>>HalfYearly</option>
								<option value="7" <%=chargeBean.getDC_FREQUECY().equals("7")?"selected":"" %>>Yearly</option> --%>
								</select>
							<td>
								<div id="displayday<%=i %>">
								<%if(chargeBean.getDC_FREQUECY().equals("2")){%>
								<select name='lday<%=i %>'>
								<option <%=chargeBean.getDC_LDAY().equalsIgnoreCase("1")?"selected":"" %> value='1'>First Day</option>
								<option <%=chargeBean.getDC_LDAY().equalsIgnoreCase("2")?"selected":"" %> value='2'>Last Day</option>
								</select>
								<%}%>	
								</div>
								</td>	
						</tr>
							<%} %>
							</table>
                            <center><input type="button" name="Button" value="ADD" onclick="cal()" class="lft">

                          	<input type="button" name="Button" value="Remove" onclick="delrow()">

                                                   <%if(pageFor.equalsIgnoreCase("Submit")) {%>
							 						<input type="submit" name="Button"  value="Submit"></input>
							 					   <%} else { %>	 
													<input type="hidden" name="Button" value="Update" ></input>
												    <input type="button" name="Button" onclick="checkval();" value="Update"></input>
												    <%} %><br>
			
            
            <footer>Copyright &copy; 2010 Get A Cab</center></footer>
 
</body>
</html>
