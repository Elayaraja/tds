<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
h2{
font-family:italic;
color:grey;
}
#foo{
font-weight:bold;
color:blue
}
#errorpage{
position:bottom;
color:Red;
}
</style>
<script type="text/javascript">
function removeError(){
	  document.getElementById("errorpage").innerHTML ="";
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript'src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="jqueryNew/bootstrap.js"></script>
<script type="text/javascript" src="jqueryNew/bootstrap.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS(Taxi Dispatch System)</title>
</head>
<body>
	<form name="masterForm" action="control" method="get" onsubmit="return showProcess()"/>

	<input type="hidden" name="action" value="registration" />
	<input type="hidden" name="event" value="disbursementmessage" />
	<input type="hidden" name="operation" value="2" />
	<input type="hidden" name="module" id="module"
		value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />

	<div>

	<% 
	if (request.getParameter("Edit") == null) {
%>
			
				<h2><center>Driver Disbursement Message</center></h2>
			
		
		<%
	} else {
%>
		<h2>Update Driver Disbursement Message</h2>
		<%
	}
%>
</div>
	

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<%
		String error = (String) request.getAttribute("error");
	if (request.getParameter("error") != null) {
		error = request.getParameter("error");

	}
%>
        <tr><td>
			<div id="errorpage"><%=(error != null && error.length() > 0) ? "" + error : ""%>
			</div>
					<table id="bodypage" width="440">
						<tr>
							<td>Start Date</td>

							<td><input type="text" name="startDate" id="startDate"
								onfocus="showCalendarControl(this);" size="10"
								value="<%=(request.getParameter("stdate") == null) ? "" : request
					.getParameter("stdate")%>"
								onkeypress="removeError()" /></td>
						</tr>
						<tr>
							<td>End Date</td>
							<td><input type="text" name="endDate" id="endDate"
								onfocus="showCalendarControl(this);" size="10"
								value="<%=(request.getParameter("eddate")) == null ? "" : request
					.getParameter("eddate")%>" />
							</td>
						</tr>

						<td><input type="hidden" name="disbursementNumber"
							id="disbursementNumber" size="10"
							value="<%=(request.getParameter("disbursementNum")) == null ? "" : request.getParameter("disbursementNum")%>" />
						</td>
						<tr>
							<td>Short Description</td>
							<td>
								<div>
									<input type="text" name="shortDesc" cols="15" rows="2"
										value="<%=(request.getParameter("shortdesc")) == null ? ""
					: request.getParameter("shortdesc")%>"
										onkeypress="removeError()" />
								</div>
							</td>
						</tr>
						<tr>
							<td>Long Description</td>
							<td>
								<div>
									<textarea name="longDesc" cols="15" rows="5"><%=(request.getParameter("longDesc")) == null ? "" : request.getParameter("longDesc")%></textarea>
								</div>
							</td>
						</tr>


						<tr>
							<td colspan="2" align="center">
								<div class="wid60 marAuto padT10">
									<div class="btnBlue">
										<div class="rht">
											<%
                        		if (request.getParameter("Edit") == null) {
                        	%>
											<input name="Button" type="submit" class="lft" value="Submit" />
											<%
                        		} else {
                        	%>
											<input name="update" type="submit" class="lft" value="Update" />

											<%
                        	                        	 	}
                        	                        	 %>
										</div>
										<div class="clrBth"></div>
									</div>
									<div class="clrBth"></div>
								</div>
							</td>
						</tr>

					</table>

				</td>
			</tr>




		</table>

		
	</div>
	</div>

	<footer>Copyright &copy; 2010 Get A Cab</footer>

</body>
</html>