<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form action ="control" method="post">
	<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
	<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.checkUserRegistration %>">
	<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
        	<div class="headerDV">
            	<div class="headerDVInner">
                	<h1 class="logoLft"><a href="#" title="Get-A-Cab"></a></h1>
                    <div class="headerRhtDV">
                    <div class="headerRhtDVInner">	
                        <div class="loginName">
                        <div class="rhtCol">
                    	<h1 class="loginNameDV"><span></span> </h1>
                        </div>
                        <div class="lftCol"></div>
                        <div class="clrBth"></div>
                        </div>
                        <div>
                        <div class="rhtCol">
                    		<div class="searchDV">
                                <span>Search:</span>
                                <div class="inputBXLC"><div class="inputBXRC"><input name="" type="text" class="inputBX" /></div></div>
                                <input class="searchBtn" name="" type="button" />
                            </div>
                        </div>
                        <div class="lftCol">
                        	<ul class="topNav">
                            	<li class="last"><a href="#" title="Contact Us">Contact Us</a></li>
                                <li><a href="#" title="About Us">About Us</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="clrBth"></div>
                    
                    <div class="mainNav1">
                    	<div class="mainNav1L">
                        	<div class="mainNav1R">
                            	<ul>
                                	<li><a href="control?action=registration&event=saveDriver&subevent=load" class="dispatch" title="Diver Reg">Diver Reg</a></li>
                                    <li><a href="control?action=registration&event=savecompany" class="operations" title="Company Reg">Company Reg</a></li>
                                    <li><a href="control?action=openrequest&event=saveRequest" class="finance" title="Open Request">Open Request</a></li>
                                    <li><a href="control?action=openrequest&event=getreceipt" class="reports" title="Get Receipt">Get Receipt</a></li>
                                    <li><a href="control?action=registration&event=checkUser" class="logout" title="Login">Login</a></li>
                                    <li><a href="control?action=registration&event=showsActiveUser" class="systemSetup" title="Active User">Active User</a></li>
                                </ul>
                                
                            </div>
                    	</div>
                    </div>
                    <div class="clrBth"></div>
                </div>
            </div>
            
            <div class="contentDV">
            <div class="contentDVInWrap">
            <div class="contentDVIn">
            	<div class="leftCol">
                	<div class="innerPage">
                    </div>
                </div>
                
                <div class="rightCol">
				<div class="rightColIn">
                	<div class="loginBX">
                	<div class="grayBX">
                    	<div class="topLCur"><div class="topRCur"><div class="topMCur"><div class="topMCur1"></div></div></div></div>
                        <div class="mid"><div class="mid1"><div class="mid2"><div class="mid3">
                        	<div class="formDV">
                        		<h2>Login</h2>
                        		<div id="errorpage">
                        		<%
									if(request.getAttribute("errors") != null ) {
									out.println(request.getAttribute("errors"));
									}
									System.out.println(request.getAttribute("errors"));	
									%>
									</div>
                                	<div class="fieldDv padB3">
                                    <fieldset>
                                    <label1 for="User id">User id:</label1>
                                    <div class="div4"><div class="div4In"><div class="inputBXWrap errorDV">
                                 	<input type="text" tabindex="1" name="loginName" class="inputBX"  value="">
                                    </div></div></div>
									</fieldset>
                                    
                                	<fieldset>
                                    <label1 for="Password">Password:</label1>
                                    <div class="div4"><div class="div4In"><div class="inputBXWrap">
                                    <input type="password" tabindex="2" name="password" class="inputBX"  value="">
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <div class="div4"><div class="div4In">
                                    		<div class="fltRht padT10">
                                              <div class="btnBlue">
                                                   <div class="rht">
                                                 <input type="button" value="Login" tabindex="3" class="lft" onclick="javascript:submit()">
                                                   </div>
                                              </div>
                                          	</div>
                                            <div class="clrBth"></div>
                                             <p class="forgetPass">
                                    		<% if(request.getAttribute("errors") != null ) {
                                    			out.println	("<a href ='control?action=registration&event=forgetpasswordinsert' >ForgetPassword</a>");
											} %>
                                            </p>
                                    	    </div></div>
									</fieldset>
                                   </div>
                                    
                                
                                        
                                
                        	</div>
                        </div></div></div></div>
                        <div class="btmLCur"><div class="btmRCur"><div class="btmMCur"><div class="btmMCur1"><div class="btmMCur2"><div class="btmMCur3"><div class="btmMCur4"><div class="btmMCur5"></div></div></div></div></div></div></div></div>
                    </div>
                    </div>
                </div>    
              </div>
            	
                <div class="clrBth"></div>
            </div>
            </div>
            </div>
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
		</div>
	</div>
</div>
</body>
</html>
