<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.HashMap"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%@page import="com.common.util.TDSConstants"%><html>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/cssverticalmenu.css" />
<script type="text/javascript">
function openDashBoard()
{
		//alert(""); 
		//var q=(document.location.href);
		//open('/TDS/control?action=admin&event=dashBoard'+escape(q),'','top=0,left=0,resizable=no,toolbar=no,menubar=�no,statu=no,location=yes,fullscreen=true,scrollbars=yes');
 

		var q=(document.location.href);
		q=q+'?action=admin&event=dashBoard';
		 
		window.open('/TDS/control?action=admin&event=dashBoard','','top=0,left=0,resizable=no,toolbar=no,addressbar=no,menubar=�no,statu=no,location=yes,fullscreen=yes,scrollbars=yes');
		
		//var q1 = open(q,'','top=0,left=0,resizable=no,toolbar=no,menubar=�no,statu=no,location=yes,fullscreen=true,scrollbars=yes');

		
}  
</script>

<script type="text/javascript" src="js/cssverticalmenu.js"></script>
<style type="text/css">
.glossymenu, .glossymenu li ul{
list-style-type: none;
margin: 0;
padding: 0; 
width: 250px; /*WIDTH OF MAIN MENU ITEMS*/
border: 1px solid black;
}

.glossymenu li{
position: relative;
}

.glossymenu li a{
background: white url(images/glossyback.gif) repeat-x bottom left;
font: bold 12px Verdana, Helvetica, sans-serif;
color: white;
display: block;
width: auto;
padding: 5px 0;
padding-left: 10px;
text-decoration: none;
}

.glossymenu li ul{ /*SUB MENU STYLE*/
position: absolute;
width: 240px; /*WIDTH OF SUB MENU ITEMS*/
left: 0;
top: 0;
display: none;
}

.glossymenu li ul li{
float: left;
}

.glossymenu li ul a{
width: 240px; /*WIDTH OF SUB MENU ITEMS - 10px padding-left for A elements */
}

.glossymenu .arrowdiv{
position: absolute;
right: 2px;
background: transparent url(images/arrow.gif) no-repeat center right;
}

.glossymenu li a:visited, .glossymenu li a:active{
color: white;
}

.glossymenu li a:hover{
background-image: url(images/glossyback2.gif);
font: bold 12px Verdana, Helvetica, sans-serif;
color: white;
}

/* Holly Hack for IE \*/
* html .glossymenu li { float: left; height: 1%; }
* html .glossymenu li a { height: 1%; }
/* End */
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<ul id="verticalmenu" class="glossymenu">

<% 	AdminRegistrationBO adminBO = null;
			if(session.getAttribute("user") != null) { 
				adminBO= (AdminRegistrationBO)session.getAttribute("user");
%>

<%if(adminBO.getRoles().contains("1000")) { %>
	<li><a href="#">Dispatch</a>
	    <ul>
	    <%if(adminBO.getRoles().contains("1001")) { %>
	    	<li>
	    		<a href="#" onclick="openDashBoard()" >Operator&nbsp;Dash&nbsp;Board</a>
	    	</li>
	    <%} %>
	    <%if(adminBO.getRoles().contains("1003")) { %>
	    <li>
	    	<a href="control?action=openrequest&event=saveRequest" >Create&nbsp;A&nbsp;Job&nbsp;Request</a>
	    </li>
		<%}if(adminBO.getRoles().contains("1004")) { %>	
	    <li> 
	      	<a href="control?action=openrequest&event=getopenCriteria" >Change&nbsp;Open&nbsp;Request</a>
	    </li>
	    <%}if(adminBO.getRoles().contains("1005")) { %>
		<li>
			<a href="control?action=openrequest&event=getAcceptCriteria" >Change&nbsp;Accepted&nbsp;Request</a>
		</li>
		<%}if(adminBO.getRoles().contains("1006")) { %>
		<li>
			<a href="control?action=openrequest&event=sendSMS" >Send&nbsp;Message&nbsp;to&nbsp;Drivers</a>
		</li>
		<%}%>
	    </ul>
	</li>
<%} %>
<%if(adminBO.getRoles().contains("2000")) { %>
<li>
	<a href="#" >Operations</a> 
    <ul>
    <%if(adminBO.getRoles().contains("2015"))  {%> 
    <li><a href="control?action=registration&event=driverSave&subevent=driver">Driver&nbsp;Registration</a></li> 
    <%}if(adminBO.getRoles().contains("2001")) { %>
    	<li><a href="control?action=admin&event=driverTempDeActive" >Deactivate&nbsp;Driver</a></li>
       <%}if(adminBO.getRoles().contains("2002")) { %>	
    	<li><a href="control?action=admin&event=getDeActiveSummary" >Activate&nbsp;Driver</a></li>
    <%}if(adminBO.getRoles().contains("2007")) { %>	
     	<li><a href="control?action=registration&event=createLostandFound" >Create&nbsp;Lost&nbsp;and&nbsp;Found</a></li>
    <%}if(adminBO.getRoles().contains("2003")) { %>	
     	<li><a href="control?action=registration&event=lostandFoundSummary" >Search&nbsp;Edit&nbsp;Lost&nbsp;and&nbsp;Found</a></li>
    <%}if(adminBO.getRoles().contains("2004")) { %>	
    	<li><a href="control?action=registration&event=driverPenalityMaster" >Driver&nbsp;Credit/Charge</a></li>
    <%}if(adminBO.getRoles().contains("2005")) { %>	
    	<li><a href="control?action=registration&event=createMaintence" >Create&nbsp;Cab&nbsp;Maintenance</a></li>
    <%}if(adminBO.getRoles().contains("2006")) { %>	
    	<li><a href="control?action=registration&event=maintenanceSummary" >Cab&nbsp;Maintenance&nbsp;Summary</a></li>
    <%}if(adminBO.getRoles().contains("2008")) { %>	
    	<li><a href="control?action=registration&event=ZoneEntry" >Zone&nbsp;Login</a></li>
    <%}if(adminBO.getRoles().contains("2013")) { %>	
	<li><a href="control?action=registration&event=custInfo" >Customer&nbsp;Services</a></li>
	<%}if(adminBO.getRoles().contains("2014")) { %>	
	<li><a href="control?action=openrequest&event=ServiceSummary" >Service&nbsp;Summary</a></li>
	<%}%>
     </ul>
</li>
<%} %>
<%if(adminBO.getRoles().contains("3000")) { %>
<li><a href="#" >Financial</a>
    <ul>
    <%if(adminBO.getRoles().contains("3001")) { %>
    	<li><a href="control?action=manualccp&event=manualCCProcess" >Process&nbsp;Credit&nbsp;Card</a></li>
    <%}if(adminBO.getRoles().contains("3002")) { %>
    	<li><a href="control?action=manualccp&event=getSummarytoAlter" >Reverse&nbsp;Credit&nbsp;Card&nbsp;Transactions</a></li>
     <%}if(adminBO.getRoles().contains("3021")) { %>
    	<li><a href="control?action=manualccp&event=getSummarytoAlterlimit" >Transaction Summary</a></li>
     <%}if(adminBO.getRoles().contains("3003")) { %>	
    	<li><a href="control?action=registration&event=createVoucher" >Create&nbsp;Voucher</a></li>
    <%}if(adminBO.getRoles().contains("3004")) { %>	
    	<li><a href="control?action=registration&event=getVoucherEditPage" >Change&nbsp;Voucher</a></li>
    	<%}if(adminBO.getRoles().contains("3005")) { %>	    		
    	<li><a href="control?action=manualccp&event=getUntxnSummary" >Reverse&nbsp;Un Capture&nbsp;CC&nbsp;Transactions</a></li>
    <%}if(adminBO.getRoles().contains("3006")) { %>	    		
    	<li><a href="control?action=registration&event=uncapturedamtdetails" >Tag&nbsp;Voucher&nbsp;Payment</a></li>
    <%}if(adminBO.getRoles().contains("3007")) { %>	
    	<li><a href="control?action=registration&event=driverDisbursement" >Driver Disbursement</a></li>
    <%}if(adminBO.getRoles().contains("3008")) { %>	
    	<li><a href="control?action=registration&event=driverDisbursementSummary" >Driver Disbursement Summary</a></li>
    <%}if(adminBO.getRoles().contains("6000")) { %>	
		<li><a href="#" style="color: #3333FF;text-decoration: none; "><B>CSV Upload</B></a></li>
	<%if(adminBO.getRoles().contains("6000")) { %>
		<li><a href="control?action=CSV&event=csvUploadStart" >Driver Charge/Credit Upload</a></li>
	<%} %>
    <%}if(adminBO.getRoles().contains("3022")) { %>
		<li><a href="control?action=manualccp&event=manualSettle" >Manual Settlement</a></li>	 
    <%} %>
   
    </ul>
</li>
<%} %>
<%if(adminBO.getRoles().contains("4000")) { %>
	<li><a href="control?action=openrequest&event=Report"  >Reports</a></li>
<%} %>
<li><a href="control?action=registration&event=logout" >Logout</a></li>
<%if(adminBO.getRoles().contains("5000")) { %>
	<li><a href="#" >System Setup</a>
	<ul>
		<%if(adminBO.getRoles().contains("5100")) { %>
		<li><a href="#" style="color: #3333FF;text-decoration: none; "><B>User Admin</B></a></li>
			
			<%if(adminBO.getRoles().contains("5101")) { %>
				<li><a href="control?action=registration&event=adminregis" >User&nbsp;Registration</a></li>
			<%}if(adminBO.getRoles().contains("5102")) { %>
		    	<li><a href="control?action=registration&event=summaryDR" >Driver&nbsp;Update</a></li>
		    <%}if(adminBO.getRoles().contains("5103")) { %>
		    	<li><a href="control?action=userrolerequest&event=getUserList&subevent=access" >View/Change User&nbsp;Permissions</a></li>
		    <%}if(adminBO.getRoles().contains("5104")) { %><!--
		    	<li><a href="control?action=registration&event=companyentry" >Company&nbsp;Registration</a></li>
		     --><%}if(adminBO.getRoles().contains("5105")) { %>
		    	<li><a href="control?action=registration&event=savecompany&submit=View" >View/Edit Company</a></li>
		     <%}if(adminBO.getRoles().contains("5106")) { %>
		    	<li><a href="control?action=registration&event=Adminview" >Admin&nbsp;MasterView</a></li>
		     <%}if(adminBO.getRoles().contains("5117")) { %>	
		    	<li><a href="control?action=registration&event=cabRegistration" >Cab Registration </a></li>
		    <%}if(adminBO.getRoles().contains("5118")) { %>	
		    	<li><a href="control?action=registration&event=cabRegistrationSummary" >Cab Registration Summary</a></li>
		    <%}if(adminBO.getRoles().contains("5119")) { %>	
		    	<li><a href="control?action=registration&event=CabDriverMapping" >Cab Driver Mapping </a></li>
		    <%}if(adminBO.getRoles().contains("5120")) { %>	
		    	<li><a href="control?action=registration&event=CabDriverMappingSummary" >Cab Driver Mapping Summary</a></li>
		    <%}%>

			    
    
		<%} %>	
		<%if(adminBO.getRoles().contains("5200")) { %>
				<li><a href="#" style="color: #3333FF;text-decoration: none; " ><B>Zone</B></a></li>
			<%if(adminBO.getRoles().contains("5201")) { %>
				<li><a href="control?action=registration&event=createqueueCoordinate" >Create&nbsp;Zone&nbsp;Co-Ordinates</a></li>
			<%}if(adminBO.getRoles().contains("5202")) { %>	
			    <li><a href="control?action=registration&event=queuesummary" >Zone&nbsp;Co-Ordinates&nbsp;Summary</a></li>
			  <%} %>  
			
		<%} %>
		<%if(adminBO.getRoles().contains("5300")) { %>
			<%if(adminBO.getRoles().contains("5301")) { %>
			<li><a href="#" style="color: #3333FF;text-decoration: none; "><B>Credit Card</B></a></li>
			<%if(adminBO.getRoles().contains("5302")) { %>
				<li><a href="control?action=registration&event=companyccsetup" >Credit Card Charges</a></li>
			<%} %>	
		<%} %>
		<%if(adminBO.getRoles().contains("5400")) { %>	
		<li><a href="#" style="color: #3333FF;text-decoration: none; "><B>Financial</B></a></li>
			<%if(adminBO.getRoles().contains("5401")) { %>
				<li><a href="control?action=registration&event=cmpPaymentSettelmentSch" >Payment Settlement Schedule</a></li>
			<%}if(adminBO.getRoles().contains("5402")) { %>	
			    <li><a href="control?action=registration&event=driverChargesMaster" >Recurring Driver Charges</a>	</li>
			<%} %>
		<%} %>
		
		
	</ul>
	</li>
	<%} %>
<%} %>
<%}else{ %>

 	<li>
		<a href="control?action=registration&event=saveDriver&subevent=load">Driver&nbsp;Registration</a>
	</li>
	<li>
		<a href="control?action=registration&event=savecompany" >Company&nbsp;Registration</a>
	</li>

	<li>
		<a href="control?action=openrequest&event=saveRequest" >Open&nbsp;Request&nbsp;Entry</a>
	</li>
 
	<li>
		<a href="control?action=openrequest&event=getreceipt" >Get&nbsp;Receipt</a>			
	</li>

	<li>
		<a href="control?action=registration&event=showsActiveUser" >Active User</a>			
	</li>
 
 	<li>
		<a href="control?action=registration&event=checkUser" >Login</a>
	</li>
		
<%} %>

</ul>
</body>
</html>