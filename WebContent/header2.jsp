<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
    <%@page import="com.tds.tdsBO.FleetBO"%>
    <%@page import="java.util.ArrayList"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script type="text/javascript" src="js/jquery.js"></script>
<link rel="stylesheet" href="css/client/navbar.css"></link>
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	
%>

<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>
	
<script type="text/javascript">
 function changeFleetHeader(fleet,i){
	 //var fleet=document.getElementById("fleet").value;
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		<%	AdminRegistrationBO adminBO=(AdminRegistrationBO)session.getAttribute("user");%>
	url='SystemSetupAjax?event=changeFleet&fleet='+fleet;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	alert(text);
	$(".fleetbutton").css('backgroundColor', '');
	$("#fleet_"+i).css('backgroundColor', 'khaki');
 }
</script>

<script type="text/javascript">



$(document).ready(function(){
	function openEmail(){
		document.getElementById("sendMail").style.display="block";
		document.getElementById("showOptions").style.display="none";
	}

	<%if(adminBo.getShiftMode()==0){ %>
	$("#mode0").show();
	<%}else if(adminBo.getShiftMode()==1){ %>
	$("#mode1").show();
	<%}else if(adminBo.getShiftMode()==2){ %>
	$("#mode2").show();
	<%} %>
	$("nav ul li").hover(function() {
    $(this).addClass("active");
    $(this).find("ul").show().animate({opacity: 1}, 600);
    },function() {
    $(this).find("ul").hide().animate({opacity: 0}, 200);
    $(this).removeClass("active");
});

// Requried: Addtional styling elements
$('nav ul li ul li:first-child').prepend('<li class="arrow"></li>');
$('nav ul li:first-child').addClass('first');
$('nav ul li:last-child').addClass('last');
$('nav ul li ul').parent().append('<span class="dropdown"></span>').addClass('drop');

});
(function($){
	
	//cache nav
	var nav = $("#topNav");
	//add indicator and hovers to submenu parents
	nav.find("li").each(function() {
		alert("Find");
		if ($(this).find("ul").length > 0) {
			$("<span>").text("").appendTo($(this).children(":first"));

			//show subnav on hover
			$(this).mouseenter(function() {
				$(this).find("ul").stop(true, true).slideDown();
			});
			
			//hide submenus on exit
			$(this).mouseleave(function() {
				$(this).find("ul").stop(true, true).slideUp();
			});
		}
	});
})(jQuery);
function shiftOpen(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'control?action=openrequest&event=operatorShift&openShift=Open&responseForMain=YES';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!="0"){
		$("#mode0").hide();
		$("#mode1").show();
	}
}
function shiftBreak(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getShiftKey';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
if(text!="" && text!=0){	
	var url = 'control?action=openrequest&event=operatorShift&startBreak=Break&responseForMain=YES&key='+text;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="true"){
		$("#mode1").hide();
		$("#mode2").show();
	}
}
}
function resumeShift(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getShiftKey';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
if(text!="" && text!=0){	
	var url = 'control?action=openrequest&event=operatorShift&endBreak=Break&responseForMain=YES&key='+text;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="true"){
		$("#mode2").hide();
		$("#mode1").show();
	}
}
}
function endShift(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getShiftKey';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
if(text!="" && text!=0){	
	var url = 'control?action=openrequest&event=operatorShift&CloseShift=Break&responseForMain=YES&key='+text;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="true"){
		$("#mode1").hide();
		$("#mode0").show();
	}
}
}

</script>
</head>
<body>

<input type="hidden" id="logButton" value=""/>
<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
        	 <div class="headerDV" style="height: 20%;">
            	<div class="headerDVInner">
                	<h1 class="logoLft a"><a href="#" title="Get-A-Cab"></a></h1>
                	 	<div id="mode0" style="display: none;">
                       	                         	 <input type="button"  name="button" id="button"   class="lft" value="Shift Open" onclick="shiftOpen()" />
                       	
                       	</div>
                       	<div id="mode1" style="display: none;">
                       	
                         	                          	 <input type="button"  name="button" id="button"   class="lft" value="Break" onclick="shiftBreak()"/>
                         	                          	 <input type="button"  name="button" id="button"   class="lft" value="Close Shift" onclick="endShift()" />
                         	                          	 </div>
                        <div id="mode2" style="display: none;">
                                  	
                         	                          	 <input type="button"  name="button" id="button"   class="lft" value="Resume Shift" onclick="resumeShift()"/>
                         	                          	 <input type="button"  name="button" id="button"   class="lft" value="Close Shift" onclick="endShift()" />
                        </div>
                         <%if(fleetList!=null&&fleetList.size()>0){%>
      <div id="fleetDiv" style="-moz-border-radius: 8px 8px 8px 8px;align: right; width: 150px; height: 62px; position: absolute; top: 0px;left: 190pt;background-color:khaki;max-height:62px;overflow-y:scroll;overflow-x:auto">
          <table><tr>
	<%for(int i=0;i<fleetList.size();i++){ 
		if(i%2==0){%>
			<tr>
	    <%}%>
				<td>
					<input type="button" name="fleet_<%=i%>" class="fleetbutton"  value ="<%=fleetList.get(i).getFleetName()%>" id ="fleet_<%=i%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBO.getAssociateCode())?"style='background-color:khaki'":""%> onclick="changeFleetHeader('<%=fleetList.get(i).getFleetNumber()%>','<%=i%>')" />
	 			</td>
	<%} %>
          </tr>
         </table></div>
  <%} %>
                       
                        
                     <div class="headerRhtDV">
                    <div class="headerRhtDVInner">	
                        <div>
 
                        <div style="margin-left: 50%;margin-top:2px;">
                        <nav class="dark"> 
                    	<ul class="clear" id="topNav">
	                    	<li><span style="color: black;font-size: small;font-family: fantasy;font-style: italic;font-stretch: ultra-condensed;">Welcome&nbsp;</span><span style="color: white;font-size: 15px;"><%=((AdminRegistrationBO)session.getAttribute("user")).getUserNameDisplay() %></span>  
								<ul style="margin-top: -20px;margin-left: 75px;"><li>					
									<a href="control?action=systemsetup&event=userProfile&module=systemsetupView"><font style="color: white;font-size: small;font-weight: bold;">User Profile</font></a>
								</li><li>
									<a href="control?action=registration&event=logout"><font style="color: white;font-size: small;font-weight: bold;">Logout</font></a>
								</li></ul>
							</li>
                     	</ul>
                      	</nav>
                         </div>
                    	<div id="wid"></div>
						<div id="tid" style="margin-left:89%;margin-top:-37px;font-size: small;"></div> 
                        
                       </div>
                        <div id="aboutContact" style="margin-left:25%;margin-top:-37px;font-size: small;width:5%">
                         <div>
                        	<ul>
                            	<li><a href="RegistrationAjax?event=contactUs" title="Contact Us">Contact&nbsp;Us</a></li>
                            </ul>
                        </div></div>
                       
                        </div></div></div>
                       
                        
                    <div class="clrBth"></div> 
                    
                   <div class="mainNav">
                    	<div class="mainNavL">
                        	<div class="mainNavR">
                            	<ul>
                            	   	<li><a href="#" id="dispatch" title="Dispatch" onclick="dispatch('dispatchView');">Dispatch</a></li>
                                    <li><a href="#" class="operations" title="Operations" onclick="dispatch('operationView');">Operations</a></li>
                                     <li><a href="#" class="finance" title="Finance" onclick="dispatch('financeView');">Finance</a></li>
                                    <li><a href="control?action=openrequest&event=Report" class="reports" title="Reports">Reports</a></li>
                                    <li><a href="#" class="systemSetup" title="System Setup" onclick="dispatch('systemsetupView');">System Setup</a></li>
                                </ul>
                            </div>
                    	</div>
                    </div>                
                    <div class="clrBth"></div>
                </div>
            </div>
           
 		</div>
	</div>
</body>
</html>
