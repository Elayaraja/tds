var globalAllValues=[];
var globalDrivers=[];
var allExistingDrivers=[];
var acdata="";
function sharedRidePopulate(jsonObj){
//	var obj = JSON.parse(jsonObj.toString());
	var tbRows = "";
	if(document.getElementById("orderPopUpSub")!=null){
		document.getElementById("orderPopUpSub").innerHTML="";
		totalOptions=1;
	}
	tbRows = tbRows + '<thead><tr><th>#</th><th></th><th class="showDShTripId">TripId</th><th class="showDShtripSource">Trip Source</th><th class="showDSHName">Name</th><th class="showDSHDate">Date</th><th class="showDSHAddress">St&nbsp;Address</th><th class="showDSHSZone">S Zone</th><th class="showDSHEDAddress">Ed&nbsp;Address</th><th class="showDSHEDZone">E Zone</th><th class="showDSHRouteNo">R&nbsp;No.</th><th class="showDSHRef">RefNo.</th><th class="showDSHRef1">RefNo1.</th><th class="showDSHRef2">RefNo2.</th ><th class="showDSHRef3">RefNo3.</th></tr></thead><tbody>';
	
	if(jsonObj!=null&&jsonObj!=""&&jsonObj.address[0].shList.length>0){
		for(var i=0;i<jsonObj.address[0].shList.length;i++){
			// alert("check trip:"+jsonObj.address[0].shList[i].T);
			
			var status="";
			var rowColor = "";
			if(jsonObj.address[0].shList.AC!=null&&jsonObj.address[0].shList.AC.equalsIgnoreCase("cc")){
				rowColor="blue";
			}else if(jsonObj.address[0].shList.AC!=null&&jsonObj.address[0].shList.AC.equalsIgnoreCase("cash")){
				rowColor="#01DFD7";
			}else if(jsonObj.address[0].shList.AC!=null&&jsonObj.address[0].shList.AC.equalsIgnoreCase("voucher")){
				rowColor="#FF0040";
			}else{
				rowColor="";
			}
			tbRows = tbRows + '<tr><td align="center" >'+ (i + 1) +")"+'</td>';
			tbRows = tbRows + '<td><input type="checkbox" name="markSh'+i+'" id="markSh'+i+'" value="" onclick="showListOfJobs('+i+','+jsonObj.address[0].shList[i].T+','+"'"+jsonObj.address[0].shList[i].Q+"'"+','+"'"+edQueue+"'"+')"/>';
			tbRows = tbRows + '<td align="center" class="showDShTripId">'+jsonObj.address[0].shList[i].T+'</td>';
			tbRows = tbRows + '<td align="center" class="showDShtripSource">Trip Source</td>';
			tbRows = tbRows + '<td align="center" class="showDSHName">'+jsonObj.address[0].shList[i].N+'</td>';
			tbRows = tbRows + '<td align="center" class="showDSHDate">'+jsonObj.address[0].shList[i].SD+" : "+jsonObj.address[0].shList[i].ST.substring(0,2)+":"+jsonObj.address[0].shList[i].ST.substring(2,4) +'</td>';
			tbRows = tbRows + '<td  class="showDSHAddress"><font color="green" style="white-space:nowrap">'+jsonObj.address[0].shList[i].A+'</font></td>';
			tbRows = tbRows + '<td class="showDSHSZone"><font color="green" style="white-space:nowrap" >'+jsonObj.address[0].shList[i].Q+'</font></td>';
			tbRows = tbRows + '<td class="showDSHEDAddress"><font color="red" style="white-space:nowrap" >'+jsonObj.address[0].shList[i].EA+'</font></td>';
			tbRows = tbRows + '<td class="showDSHEDZone"><font color="red" style="white-space:nowrap">'+jsonObj.address[0].shList[i].EDQ+'</font></td>' ;
			tbRows = tbRows + '<td  align="center" id="exRouteNo'+i+'" class="showDSHRouteNo">'+jsonObj.address[0].shList[i].RT+'</td>';
			
			tbRows = tbRows + '<td  align="center" id="shRefNo'+i+'" class="showDSHRef">'+jsonObj.address[0].shList[i].REF+'</td>';
			tbRows = tbRows + '<td  align="center" id="shRefNo1'+i+'" class="showDSHRef1">'+jsonObj.address[0].shList[i].REF1+'</td>';
			tbRows = tbRows + '<td  align="center" id="shRefNo2'+i+'" class="showDSHRef2">'+jsonObj.address[0].shList[i].REF2+'</td>';
			tbRows = tbRows + '<td  align="center" id="shRefNo3'+i+'" class="showDSHRef3">'+jsonObj.address[0].shList[i].REF3+'</td>';
			var edQueue="000";
			if(jsonObj.address[0].shList[i].EDQ!=""){
				edQueue=jsonObj.address[0].shList[i].EDQ;
			}
			
			tbRows = tbRows + '<input type="hidden" name="tripIdForShared'+i+'" id="tripIdForShared<'+i+'"value="'+jsonObj.address[0].shList[i].T+'">';
			tbRows = tbRows + '<input type="hidden" name="typeOfRide'+i+'" id="typeOfRide'+i+'" value="'+jsonObj.address[0].shList[i].SR+'"/>';
			tbRows = tbRows + '<input type="hidden" name="routeNo'+i+'" id="routeNo'+i+'" value="'+jsonObj.address[0].shList[i].RT+'"/></td>';
		}
		var DriverOrCab ="";
		var basedOn = document.getElementById("dispatchBasedOn").value;
		if (basedOn == "1") {
			DriverOrCab = "Driver";
		}else{
			DriverOrCab = "Cab";
		}
		
	}
	tbRows = tbRows + '</tr></tbody>';
	//alert("sh:"+tbRows);
	document.getElementById("customerTableShared").innerHTML = tbRows; 
	
//	var oTable = $("#customerTableShared").dataTable(
//			{
//				"bJQueryUI": true,
//				"bDestroy":true 
//				"bInfo": false,
//				"bFilter":false,
//				'sDom': 't', 
//				 "aoColumnDefs": [
//				                  { "iDataSort": 1, "aTargets": [ 0 ] }
//				                ],
//				"sPaginationType": "full_numbers",
//				"iDisplayLength": 10,
//				 "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
//			});
	//oTable = $('#myTable').dataTable();
//	$('#myInputTextField').keypress(function(){
//	      oTable.fnFilter( $(this).val() );
//	});
	sharedRideSelectedCheck();
	//$("#customerTableShared").html(tbRows);
}
function changeSRStstatus(){
	document.getElementById("changeHappeningSharedRide").value="true";
}
function sharedRideDetailsPopulate(jsonObj){
	var rowTb ="";
	rowTb = rowTb + '<tr><th>R.No.</th><th>Desc</th><th>Created&nbsp;Time</th> <th>Created&nbsp;By</th><th>Allocate</th>';

	if(jsonObj!=null&&jsonObj!=""&&jsonObj.address[0].shDList.length>0){
		
		for(var i=0;i<jsonObj.address[0].shDList.length;i++){
			var basedOn = document.getElementById("dispatchBasedOn").value;
			if (basedOn == "1") {
				DriverOrCab = jsonObj.address[0].shDList[i].DRI;
			}else{
				DriverOrCab = jsonObj.address[0].shDList[i].V;
			}
			var testAllocate=DriverOrCab;
			if(testAllocate==""){
				testAllocate="1110000";
			}
			rowTb = rowTb + '<tr class="shareRideContextMenu" style="cursor:pointer">';
			rowTb = rowTb + '<td  align="center" id="routeNoValue"><img alt="" src="images/Dashboard/pdf.png" onclick="downloadFile('+jsonObj.address[0].shDList[i].RT+')"/>'+jsonObj.address[0].shDList[i].RT+'</td>';
			rowTb = rowTb + '<td align="center">'+jsonObj.address[0].shDList[i].DES+'</td>';
			rowTb = rowTb + '<td align="center">'+jsonObj.address[0].shDList[i].D+"&nbsp;"+jsonObj.address[0].shDList[i].T+'</td>';
			rowTb = rowTb + '<td align="center"> '+jsonObj.address[0].shDList[i].CB+'</td>';
			rowTb = rowTb + '<td align="center"><input type="text" size="5" name="driverForShare'+i+'" id="driverForShare'+i+'" value="'+DriverOrCab+'" onfocus="changeSRStstatus()" onblur="allocateDriverforShared('+i+','+jsonObj.address[0].shDList[i].RT+','+testAllocate+')"></td>';
			rowTb = rowTb + '<td><img alt="" id="imgReplace1_'+i+'" src="images/Dashboard/plus.png"  onclick="showOrHideJobs('+i+','+jsonObj.address[0].shDList[i].RT+');">';

			if(jsonObj.address[0].shDList[i].STA=="40"||jsonObj.address[0].shDList[i].STA=="43"||jsonObj.address[0].shDList[i].STA=="47"||jsonObj.address[0].shDList[i].STA=="48") {
				rowTb = rowTb + '<img src="images/Dashboard/greencar1.png"  style="width: 30px;height: 20px;cursor: pointer;">'; 
			}else if(jsonObj.address[0].shDList[i].STA=="30"){
				rowTb = rowTb + '<img src="images/Dashboard/megaphone_red.png"  style="width: 30px;height: 20px;cursor: pointer;"/>';
			}else if(jsonObj.address[0].shDList[i].STA=="61"){
				rowTb = rowTb + '<img src="images/Dashboard/completed.png"  style="width: 30px;height: 20px;cursor: pointer;"/>';
			}else if(jsonObj.address[0].shDList[i].STA=="4"){ 
				rowTb = rowTb + '<input type="button" style="background-color: yellow;width:30px;cursor: pointer;"/><br/>';
			}else{
				rowTb = rowTb + '<input type="button" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}
			rowTb = rowTb + '<td>';
			//Status
			rowTb = rowTb + '</td></tr><tr><td colspan="6"><div id="ajaxShared'+i+'"></div></td></tr>';
		}
		
	}
	rowTb = rowTb + '</table>';
	document.getElementById("shRideDetailsTb").innerHTML = rowTb;
}
colorLightGreen = true;

function zoneDetailsPopulate(jsonObj){
	document.getElementById("zoneDetailsDiv").innerHTML="";
	document.getElementById("zoneDetailsExpand").innerHTML ="";
	if(jsonObj!=null&&jsonObj!=""&&jsonObj.address[0].zonesList.length>0){
		var colorPattern;
		var currentZone = "";
		var driverCount = 0;
		var cur_n, prev_n = "";

		var fields ='<table id="zoneTableDetails">';
		var fieldsEx ="";

		for(var i=0;i<jsonObj.address[0].zonesList.length;i++){
			colorLightGreen = !colorLightGreen;
			if (colorLightGreen) {
				colorPattern = "style=\"background-color:lightgreen\"";
			} else {
				colorPattern = "style=\"background-color:lightgreen\"";
			}
			var basedOn = document.getElementById("dispatchBasedOn").value;
			if (basedOn == "1") {
				DriverOrCab = jsonObj.address[0].zonesList[i].Dr;
			}else{
				DriverOrCab = jsonObj.address[0].zonesList[i].Veh;

			} 
			if(currentZone!=jsonObj.address[0].zonesList[i].QD){
				driverCount = 0;
				currentZone = jsonObj.address[0].zonesList[i].QD;
				fields = fields + '</tr> <tr bgcolor="#EFEFFB" class="zone-label" style="background-color:#ff99ff"><td width="65px" align="center"colspan="3" style="white-space:nowrap">'+jsonObj.address[0].zonesList[i].QD+"("+jsonObj.address[0].zonesList[i].QN+")"+'</td>';
			}
			fields = fields + '<a><td  style="background-color:lightgreen;-moz-border-radius: 8px 8px 8px 8px;" class="label" width="65px" '+colorPattern+' onmouseover="loginTime()"  align="center"><d id="zone" >'+DriverOrCab+'</d></td></a>';	
			driverCount++; 
			if(driverCount%3==0){
				fields = fields+'</td></tr>' ;
			} 
		fieldsEx = fieldsEx + '</table>';
		}
		//fields = fields + '</tr></table>';
		
		document.getElementById("zoneDetailsDiv").innerHTML = fields;
		
		var colorPattern;
		var currentZone = "";
		var driverCount = 0;
		var cur_n, prev_n = "";
		for(var i=0;i<jsonObj.address[0].zonesList.length;i++){
			colorLightGreen = !colorLightGreen;
			if (colorLightGreen) {
				colorPattern = "style=\"background-color:lightgreen\"";
			} else {
				colorPattern = "style=\"background-color:lightgreen\"";
			}
			var basedOn = document.getElementById("dispatchBasedOn").value;
			if (basedOn == "1") {
				DriverOrCab = jsonObj.address[0].zonesList[i].Dr;
			}else{
				DriverOrCab = jsonObj.address[0].zonesList[i].Veh;
			} 
			fieldsEx = fieldsEx + '</tr>';
			if(currentZone!=jsonObj.address[0].zonesList[i].QD){
				fieldsEx = fieldsEx + '<table bgcolor="#ceecf5" border="0"style="font-size:12px" id="ZoneExpandDetails"><tr bgcolor="#EFEFFB" class="zone-label" style="background-color:#ff99ff"><td width="65px" align="center"colspan="3" style="white-space:nowrap">'+jsonObj.address[0].zonesList[i].QD+"("+jsonObj.address[0].zonesList[i].QN+")"+'</td></tr><tr>';
				driverCount = 0;
				currentZone = jsonObj.address[0].zonesList[i].QD;
			}							
			fieldsEx = fieldsEx + '<a><td style="background-color:lightgreen;-moz-border-radius: 8px 8px 8px 8px;" align="center"class="label" width="65px" '+colorPattern+' onmouseover="loginTime()" ><d id="zone">'+DriverOrCab+'</d><em> '+jsonObj.address[0].zonesList[i].LT+'</em></td></a>';	
			driverCount++; 
			if(driverCount%3==0){
				fields = fields+'</td></tr>' ;
			}
		}
		fieldsEx = fieldsEx + '</table>';
		document.getElementById("zoneDetailsExpand").innerHTML = fieldsEx;

	}
}
var allParams="";
function getAllValuesForDashboard(params){
	var url="";
	var zoneList ="";
	var statusOfJobToPull = "40";
	var statusOfRtJobToPull ="1";
	var statusOfRouteToPull ="1";
	var shDate ="";
	var shDDate ="";
	var drStatus ="";
	var searchDriver ="";
	var zoneSearchDriver ="";
	allParams=params;
	if(document.getElementById("dateForShared").value!=null&&document.getElementById("dateForShared").value!=""){
		shDate = document.getElementById("dateForShared").value;
	}
	if(document.getElementById("datePickerForSh").value!=null&&document.getElementById("datePickerForSh").value!=""){
		shDDate = document.getElementById("datePickerForSh").value;
	}
	if(document.getElementById("All")!=null&&document.getElementById("All").checked){
		statusOfJobToPull = "40";
	}else{
		statusOfJobToPull = "0";
	}
	if(document.getElementById("rtJobs")!=null&&document.getElementById("rtJobs").checked){
		statusOfRtJobToPull = "1";
	}else{
		statusOfRtJobToPull = "0";
	}if(document.getElementById("groupJobs")!=null&&document.getElementById("groupJobs").checked){
		statusOfRouteToPull = "100";
	}	
	//document.getElementById("driverShowCheck").value = "lin";
	if(document.getElementById("driverShowCheck").value == "lin"){
		drStatus ="1";
	}else if(document.getElementById("driverShowCheck").value == "lo"){
		drStatus ="3";
	}else if(document.getElementById("driverShowCheck").value == "all"){
		drStatus ="5";
	}else{
		drStatus ="1";
	}
	if(document.getElementById("driver_id").value!=""){
		searchDriver = document.getElementById("driver_id").value;
	}
	if(document.getElementById("driverid").value!=""){
		zoneSearchDriver = document.getElementById("driverid").value;
	}
	var urlValue='spring/SpringDashboard?status='+statusOfJobToPull+'&rtJobStatus='+statusOfRtJobToPull+'&routeJob='+statusOfRouteToPull+'&zoneList='+zoneList+'&drStatus='+drStatus+'&params='+params+'&shDate='+shDate+'&shDDate='+shDDate+'&searchDriver='+searchDriver+'&zoneSearchDriver='+zoneSearchDriver;
	$.ajax({ 
		url :urlValue, // JQuery loads serverside.php
		dataType: 'json', 
		success: function(data) 
		{
			var stringVar = JSON.stringify(data);
			var jobsJsonObj = "{\"address\":"+stringVar+"}" ;
			var obj = JSON.parse(jobsJsonObj.toString());
			//alert(obj.address[0].refArr.check);
			var parms=[];
			for(var k=0;k<obj.address[0].refArr.check.length;k++){
				params[k]=obj.address[0].refArr.check.charAt(i);
			}
			if(params[0]=="1"){
				jobDetailsAll(obj);
			}
			if(params[1]=="1"){
				driversFromTotal(obj);
			}
			if(params[2]=="1"){
				zoneDetailsPopulate(obj);
			}
			if(params[3]=="1"){
				sharedRidePopulate(obj);
			}
			if(params[4]=="1"){
				sharedRideDetailsPopulate(obj);
			}
			globalAllValues=obj;
		
		}
	});

}
function driversFromTotal(jsonObj){
	document.getElementById("driverDetailDisplay").innerHTML ="";
	document.getElementById("msgDriver").innerHTML ="";
	document.getElementById("driverExpand").innerHTML ="";
	document.getElementById("lastJobRunTime").innerHTML="Job Run:"+jsonObj.address[0].lastRun.lastRunTime+"s ago";
	if(jsonObj.address[0].driverList!=null&&jsonObj.address[0].driverList!=""&&jsonObj.address[0].driverList.length>0){
		globalDrivers=jsonObj.address[0];
		
		document.getElementById("unloadDrivers").src="images/Dashboard/refresh1.png";
		var table = document.getElementById("driverDetailDisplay");
		table.width="200px";
		var row="";
		var rowCount="";
		rowCount = table.rows.length;
		row = table.insertRow(rowCount);
		//if(driverForSearch==null||driverForSearch==""){
		//}
		var index = "row";
		
		/*var jobsTable = document.getElementById("customerTable");
		var jrow = jobsTable.getElementsByTagName("tr");;
		for(var j=0;j<jrow.length;j++){
			if(jrow[j].id==index){
				oRows[j] = jrow[j];
				alert(""+jrow[j].id);
			}
		}*/
		
		var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
		var iRowCount= oRows.length;
		//alert("length:"+iRowCount);
		document.getElementById("noOfDriver").value=jsonObj.address[0].driverList.length;
		for(var i=0;i<jsonObj.address[0].driverList.length;i++){
			stZone ="";
			edZone ="";
			for(var k = 1; k < iRowCount; k++) {
				//alert(""+k+":"+oRows[k].id);
				if(oRows[k].id==index){
					driverJob = oRows[k].getElementsByTagName("input")[11].value;
					if(driverJob == jsonObj.address[0].driverList[i].varDr){
						stZone = oRows[k].getElementsByTagName("input")[14].value;
						edZone= oRows[k].getElementsByTagName("input")[15].value;
						cityV= oRows[k].getElementsByTagName("input")[10].value;
						//alert(cityV);
						if(cityV.indexOf("Flag")!=-1){
						//if(cityV.includes("Flag")){
							//alert("Yes flag");
							flagtrip = "yes";
						}else{
							flagtrip = "No";
						}
					}
				}
			}

			var lastUpdate = 0;
			var cell = row.insertCell(0);
			lastUpdate =  jsonObj.address[0].driverList[i].lastUpDate;
			if(jsonObj.address[0].driverList[i].st == 1){
				cell.className="detail rowstraight2";
			}else{
				cell.className="detail rowstraight";
			}

			var v = document.createTextNode(jsonObj.address[0].driverList[i].varDr);
			var b = document.createElement("z");
			b.id="row";
			b.style.display="none";
			b.appendChild(v);
			cell.appendChild(b);

			var ve = document.createTextNode(jsonObj.address[0].driverList[i].varVNo);
			var be = document.createElement("y");
			be.id="vehicleValue";
			be.style.display="none";
			be.appendChild(ve);
			cell.appendChild(be);

			var vs = document.createTextNode(jsonObj.address[0].driverList[i].varSwh);
			var bs = document.createElement("x");
			bs.id="switchValue";
			bs.style.display="none";
			bs.appendChild(vs);
			cell.appendChild(bs);

			var vlat = document.createTextNode(jsonObj.address[0].driverList[i].varLt);
			var blat = document.createElement("n");
			blat.id="latForDriverForZone";
			blat.style.display="none";
			blat.appendChild(vlat);
			cell.appendChild(blat);
			var vlong = document.createTextNode(jsonObj.address[0].driverList[i].varLg);
			var blong = document.createElement("o");
			blong.id="longForDriverForZone";
			blong.style.display="none";
			blong.appendChild(vlong);
			cell.appendChild(blong);
			var e1 ="";
			if(document.getElementById("dispatchBasedOn").value==1){
				if(jsonObj.address[0].driverList[i].varLt.substring(0,3)!=0.0){
					if(lastUpdate>10){
						if(jsonObj.address[0].driverList[i].varSwh=="Y" || jsonObj.address[0].driverList[i].varSwh=="Z"){
							e1 = document.createTextNode(jsonObj.address[0].driverList[i].varDr);	
						}else if(jsonObj.address[0].driverList[i].varSwh=="N"){
							if(edZone!=""){
								e1 = document.createTextNode(jsonObj.address[0].driverList[i].varDr+"("+edZone+")");	
							}else{
								e1 = document.createTextNode(jsonObj.address[0].driverList[i].varDr);	
							}
						}else{
							e1 = document.createTextNode(jsonObj.address[0].driverList[i].varDr+"*");
						}
					}else{
						if(jsonObj.address[0].driverList[i].varSwh=="Y" || jsonObj.address[0].driverList[i].varSwh=="Z"){
							e1 = document.createTextNode(jsonObj.address[0].driverList[i].varDr);	
						}else if(jsonObj.address[0].driverList[i].varSwh=="N"){
							if(edZone!=""){
								e1 = document.createTextNode(jsonObj.address[0].driverList[i].varDr+"("+edZone+")");	
							}else{
								e1 = document.createTextNode(jsonObj.address[0].driverList[i].varDr);	
							}
						}else{
							e1 = document.createTextNode(jsonObj.address[0].driverList[i].varDr);	
						}	
					}
				}else{
					e1 = document.createTextNode("!"+jsonObj.address[0].driverList[i].varDr);	
				}
				cell.setAttribute('onClick', 'currentDriver('+jsonObj.address[0].driverList[i].varDr+','+jsonObj.address[0].driverList[i].varLt+','+jsonObj.address[0].driverList[i].varLg+')');
				if(jsonObj.address[0].driverList[i].DD=="NF"){
					cell.setAttribute('title',jsonObj.address[0].driverList[i].varLT+'    AppVersion:'+jsonObj.address[0].driverList[i].varAppV);
				}else{
					cell.setAttribute('title',jsonObj.address[0].driverList[i].varLT+'   Distance:'+jsonObj.address[0].driverList[i].DD);
				}

			}else{
				if(jsonObj.address[0].driverList[i].varLt.substring(0,3)!=0.0){
					if(lastUpdate>10){
						if(jsonObj.address[0].driverList[i].varSwh=="Y" || jsonObj.address[0].driverList[i].varSwh=="Z"){
							e1 = document.createTextNode(jsonObj.address[0].driverList[i].varVNo);
						}else if(jsonObj.address[0].driverList[i].varSwh=="N"){
							if(edZone!=""){
								e1 = document.createTextNode(jsonObj.address[0].driverList[i].varVNo+"("+edZone+")");	
							}else{
								e1 = document.createTextNode(jsonObj.address[0].driverList[i].varVNo);	
							}
						}else{
							e1 = document.createTextNode(jsonObj.address[0].driverList[i].varVNo+"*");		
						}
					}else{
						if(jsonObj.address[0].driverList[i].varSwh=="Y" || jsonObj.address[0].driverList[i].varSwh=="Z"){
							e1 = document.createTextNode(jsonObj.address[0].driverList[i].varVNo);
						}else if(jsonObj.address[0].driverList[i].varSwh=="N"){
							if(edZone!=""){
								e1 = document.createTextNode(jsonObj.address[0].driverList[i].varVNo+"("+edZone+")");	
							}else{
								e1 = document.createTextNode(jsonObj.address[0].driverList[i].varVNo);	
							}
						}else{
							e1 = document.createTextNode(jsonObj.address[0].driverList[i].varVNo);	
						}
					}
				}else{
					e1 = document.createTextNode("!"+jsonObj.address[0].driverList[i].varVNo);
				}

				cell.setAttribute('onClick', 'currentDriver('+jsonObj.address[0].driverList[i].varDr+','+jsonObj.address[0].driverList[i].varLt+','+jsonObj.address[0].driverList[i].varLg+')');
				cell.setAttribute('title',jsonObj.address[0].driverList[i].varLT);
				
				if(jsonObj.address[0].driverList[i].DD=="NF"){
					cell.setAttribute('title',jsonObj.address[0].driverList[i].varLT+'    AppVersion:'+jsonObj.address[0].driverList[i].varAppV);
				}else{
					cell.setAttribute('title',jsonObj.address[0].driverList[i].varLT+'   Distance:'+jsonObj.address[0].driverList[i].DD);
				}
			}
			
			var e2 = document.createElement("input");
			e2.type = "hidden";
			e2.name="driveridDetails"+i;
			e2.id="driveridDetails"+i;
			e2.align='right'; 
			e2.value=jsonObj.address[0].driverList[i].varDr;
			

			var e3 = document.createElement("input");
			e3.type = "hidden";
			e3.name="la_"+i;
			e3.id="la_"+i;
			e3.align='right'; 
			e3.value=jsonObj.address[0].driverList[i].varLt;

			var e4 = document.createElement("input");
			e4.type = "hidden";
			e4.name="lo_"+i;
			e4.id="lo_"+i;
			e4.align='right'; 
			e4.value=jsonObj.address[0].driverList[i].varLg;

			var e5 = document.createElement("input");
			e5.type = "hidden";
			e5.name="cab_"+i;
			e5.id="cab_"+i;
			e5.align='right'; 
			if(document.getElementById("dispatchBasedOn").value==1){
				e5.value=jsonObj.address[0].driverList[i].varDr; 
			}else{
				e5.value=jsonObj.address[0].driverList[i].varVNo;
			}
			var e6 = document.createElement("input");
			e6.type = "hidden";
			e6.name="avl_"+i;
			e6.id="avl_"+i;
			e6.align='right'; 
			e6.value=jsonObj.address[0].driverList[i].varSwh;

			var e7 = document.createElement("input");
			e7.type = "hidden";
			e7.name="cab";
			e7.id="cab";
			e7.align='right'; 
			e7.value=jsonObj.address[0].driverList[i].varVNo;

			var e8 = document.createElement("input");
			e8.type = "hidden";
			e8.name="driverProfile"+i;
			e8.id="driverProfile"+i;
			e8.align='right'; 
			e8.size="2";
			e8.value=jsonObj.address[0].driverList[i].varDp;

			var e9 = document.createElement("input");
			e9.type = "hidden";
			e9.name="drName"+i;
			e9.id="drName"+i;
			e9.align='right'; 
			e9.size="2";
			e9.value=jsonObj.address[0].driverList[i].varDrNa;

			var e10 = document.createElement("input");
			e10.type = "hidden";
			e10.name="drPhone"+i;
			e10.id="drPhone"+i;
			e10.align='right'; 
			e10.size="2";
			e10.value=jsonObj.address[0].driverList[i].varPh;
			
			var eDist9 = document.createElement("input");
			eDist9.type = "hidden";
			eDist9.name="drDist"+i;
			eDist9.id="drDist"+i;
			eDist9.align='right'; 
			eDist9.size="2";
			eDist9.value=jsonObj.address[0].driverList[i].DD;
			
			b.appendChild(v);
			cell.appendChild(b);
			
			if(lastUpdate>15){
				cell.id="noFeed";
			}else if(jsonObj.address[0].driverList[i].varSwh=="Y" || jsonObj.address[0].driverList[i].varSwh=="Z"){
				cell.id="available";
			}else {
				cell.id="unavailable";
			}
			if(jsonObj.address[0].driverList[i].varAvS=="B"){
				cell.className="break";
			}
			cell.appendChild(e2);
			cell.appendChild(e1);
			cell.appendChild(e3);
			cell.appendChild(e4);
			cell.appendChild(e5);
			cell.appendChild(e6);
			cell.appendChild(e7);
			cell.appendChild(e8);
			cell.appendChild(e9);
			cell.appendChild(e10);
			cell.appendChild(eDist9);
			var av = document.getElementById("avColor").value;
			$("#available").css('backgroundColor', '#' + av);

			var nav = document.getElementById("navColor").value;
			$("#unavailable").css('backgroundColor', '#' + nav);

			$(".break").css('backgroundColor','#FF99FF');

			var nFeed = document.getElementById("noUpdate").value;
			$("#noFeed").css('backgroundColor', '#' + nFeed);
			document.getElementById("unloadDrivers").src="images/Dashboard/refresh1.png";
		}

	
		var root=document.getElementById('driverExpand');
		var tbo=document.createElement('tbody');
		var rowHead=document.createElement('tr');
		rowHead.style.background="#EFEFFB";
		var header1=document.createElement("th");
		var h1 = document.createTextNode("Driver");
		var header2=document.createElement("th");
		var h2 = document.createTextNode("Cab");
		var header3=document.createElement("th");
		var header20=document.createElement("th");
		var h20 = document.createTextNode("Name");
		var h3 = document.createTextNode("Status");
		var header4=document.createElement("th");
		var h4 = document.createTextNode("PhoneNo.");
		var header6=document.createElement("th");
		var h6 = document.createTextNode("Profile");
		var header8=document.createElement("th");
		var h8 = document.createTextNode("Total Jobs");
		/*var header7=document.createElement("th");
		var h7 = document.createTextNode("");
		 */
		var header9=document.createElement("th");
		var h9 = document.createTextNode("Start Zone");

		var header10=document.createElement("th");
		var h10 = document.createTextNode("End Zone");

		var header11=document.createElement("th");
		var h11 = document.createTextNode("Flag Trip (Y/N)");

		var headerDist=document.createElement("th");
		var hDist = document.createTextNode("Distance");
		
		var header99=document.createElement("th");
		var h99 = document.createTextNode("App Version");

		header1.appendChild(h1);
		header2.appendChild(h2);
		header20.appendChild(h20);
		header3.appendChild(h3);
		header4.appendChild(h4);
		header6.appendChild(h6);
		header8.appendChild(h8);

		//header7.appendChild(h7);

		header9.appendChild(h9);
		header10.appendChild(h10);
		header11.appendChild(h11);
		headerDist.appendChild(hDist);
		header99.appendChild(h99);

		rowHead.appendChild(header1);
		rowHead.appendChild(header2);
		rowHead.appendChild(header20);
		rowHead.appendChild(header3);
		rowHead.appendChild(header4);
		rowHead.appendChild(header6);
		rowHead.appendChild(header8);
		//rowHead.appendChild(header7);
		rowHead.appendChild(header9);
		rowHead.appendChild(header10);
		rowHead.appendChild(header11);
		
		rowHead.appendChild(headerDist);
		rowHead.appendChild(header99);
		tbo.appendChild(rowHead);
		var row, cell1,cell2,cell3,cell4,cell6;
		//var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
		//var iRowCount= oRows.length;
		for(var i=0;i<jsonObj.address[0].driverList.length;i++){
			var flagtrip ="No Jobs";
			for(var k = 1; k < iRowCount; k++) {
				if(oRows[k].id==index){
					driverJob = oRows[k].getElementsByTagName("input")[11].value;
					if(driverJob == jsonObj.address[0].driverList[i].varDr){
						stZone = oRows[k].getElementsByTagName("input")[14].value;
						edZone= oRows[k].getElementsByTagName("input")[15].value;
						cityV= oRows[k].getElementsByTagName("input")[10].value;
						if(cityV.indexOf("Flag")!=-1){
							flagtrip = "yes";
						}else{
							flagtrip = "No";
						}
					}
				}
			}
			row=document.createElement('tr');
			row.style.background="#EFEFFB";
			row.id="expandRow";
			cell1=document.createElement('td');
			cell1.id="driveridDetailsExpand"+i;
			var inp = document.createTextNode(jsonObj.address[0].driverList[i].varDr+'');
			inp.value=jsonObj.address[0].driverList[i].varDr;
			cell1.appendChild(inp);
			cell2=document.createElement('td');
			var v = document.createTextNode(jsonObj.address[0].driverList[i].varVNo+'');
			cell2.appendChild(v);

			cell40=document.createElement('td');
			var p10 = document.createTextNode(jsonObj.address[0].driverList[i].varDrNa+'');
			cell40.appendChild(p10);

			cell3=document.createElement('td');
			var s;
			if(jsonObj.address[0].driverList[i].varSwh=="Y" || jsonObj.address[0].driverList[i].varSwh=="Z"){
				s = document.createTextNode("Av");
			}else{
				s = document.createTextNode("NAv");
			}
			cell3.appendChild(s);
			cell4=document.createElement('td');
			var p = document.createTextNode(jsonObj.address[0].driverList[i].varPh+'');
			cell4.appendChild(p);
			cell6=document.createElement('td');
			var pr = document.createTextNode(jsonObj.address[0].driverList[i].varDp+'');
			cell6.appendChild(pr);

			var totalCount= document.createElement("td");
			totalCount.id="countValue"+i;
			var count = document.createTextNode("N/A");
			totalCount.appendChild(count);

			cell16=document.createElement('td');
			var stZ = document.createTextNode(stZone+'');
			cell16.appendChild(stZ);

			cell26=document.createElement('td');
			var edZ = document.createTextNode(edZone+'');
			cell26.appendChild(edZ);

			cell27=document.createElement('td');
			var flt = document.createTextNode(flagtrip+'');
			cell27.appendChild(flt);

			cellDist=document.createElement('td');
			var dist = document.createTextNode(jsonObj.address[0].driverList[i].DD+'');
			cellDist.appendChild(dist);

			var cellcheck= document.createElement("td");
			var check = document.createElement("input");
			check.type="checkbox";
			check.id="selectDriver"+i;
			check.value=jsonObj.address[0].driverList[i].varDr;
			cellcheck.appendChild(check);

			cell99=document.createElement('td');
			var appV = document.createTextNode(jsonObj.address[0].driverList[i].varAppV+'');
			cell99.appendChild(appV);

			row.appendChild(cell1);
			row.appendChild(cell2);
			row.appendChild(cell40);
			row.appendChild(cell3);
			row.appendChild(cell4);
			row.appendChild(cell6);
			row.appendChild(totalCount);
			row.appendChild(cell16);
			row.appendChild(cell26);
			row.appendChild(cell27);
			row.appendChild(cellDist);
			row.appendChild(cell99);
			row.appendChild(cellcheck);

			tbo.appendChild(row);

		}

		root.appendChild(tbo);
		var tbRow = document.createElement("tr");
		var tbCell = document.createElement("td");
		var textMsgbutton = document.createElement("input");
		textMsgbutton.type="button";
		textMsgbutton.id="sendTxtMsg";
		textMsgbutton.value="Send Text Message";
		textMsgbutton.onclick=sendMsgToSelectedDrivers;
		tbCell.appendChild(textMsgbutton);
		tbRow.appendChild(tbCell);
		var tbRow1 = document.createElement("tr");
		var tbCell1 = document.createElement("td");
		var vMsgbutton = document.createElement("input");
		vMsgbutton.type="button";
		vMsgbutton.id="sendVMsg";
		vMsgbutton.value="Send Voice Message";
		vMsgbutton.onclick=sendvMsgToSelectedDrivers;
		tbCell1.appendChild(vMsgbutton);
		tbRow.appendChild(tbCell1);

		var expButtons = document.getElementById("msgDriver");
		expButtons.appendChild(textMsgbutton);
		expButtons.appendChild(vMsgbutton);
	}
	mapPlotDriver('1');
}
function jobDetailsAll(obj) {
	var driverOrCab;
	var dispatchBasedOn=document.getElementById("dispatch").value;
	if(dispatchBasedOn==2){
		driverOrCab = "Cab";	
	}else{
		driverOrCab="Driver";
	}
	var tableAppend=  document.getElementById("customerTable");
	var customerExpand = document.getElementById("customerExpand");

	var tableRows='<tr id="row" class="customerRow"> ';
	tableRows=tableRows+'<th class="showJobNo">#</th><th width="20px"class="showAddress">Address</th><th width="20px"class="showScity">SCity</th><th width="20px"class="showDAddress">Drop Add</th><th width="20px"class="showEcity" >ECity</th><th class="showAge">Age</th><th class="showTripId">Trip ID</th><th class="showDriverAllocate">'+driverOrCab+'</th><th class="showPhone">Ph</th><th class="showName">Name</th><th class="showStatus">St</th><th class="showTripSource">Trip Src</th><th style="width:10%" class="showRouteNum">Route Num</th><th class="showStZone">S Z</th><th class="showEdZone">E Z</th><th class="showPtime">Time</th><th class="showSplFlags">Flags</th><th class="showFlight">Flight Num</th><th class="showVoucher">Account</th><th class="showAmount">Amount</th><th class="showcheck">*</th><th style="display:none" class="showDRId">*</th></tr>';
	var tableRowsEx='<tr id="row" class="customerRow">';
	tableRowsEx=tableRowsEx+'<tr bgcolor="#EFEFFB"><th>#</th><th >Address</th><th >'+driverOrCab+'</th><th >Time</th><th>PhoneNO</th><th>Name</th><th>Cab#</th><th>Cab&nbsp;Flags</th><th>QueueNo.</th><th>Driver</th></tr>';

	//var flag = ((document.getElementById("fleetSize").value!=0) && (document.getElementById("mas").value==document.getElementById("assoccode").value) && (document.getElementById("fleetDispatch").value==1) )?"disabled":"";
	var flag = "0";
	if(obj.address[0].jobsList!=null&&obj.address[0].jobsList!=""&&obj.address[0].jobsList.length>0){
	if(obj=="lo"){
		window.open('control','_self');
	}
	//console.log("Processing job details");	

	if(document.getElementById("jobsPermission").value!="yes"){
		document.getElementById("jobScreen").innerHTML ="Permission Denied";
		document.getElementById("unload").src="images/Dashboard/refresh1.png";
		return;
	}
	if(allParams!="10000"){
		allExistingDrivers=JSON.stringify(obj.address[0].driverList);
	}
	var fl_size = document.getElementById("fleetSize").value;
	for(var i=0;i<obj.address[0].jobsList.length;i++){
		var tripAmt = "$0.00";
		if(obj.address[0].jobsList[i].AMT!="0"){
			tripAmt = "$"+obj.address[0].jobsList[i].AMT;
		}
			if((fl_size!=0) && (document.getElementById("mas").value==document.getElementById("assoccode").value) && (document.getElementById("fleetDispatch").value==1)){
				//alert("master:"+document.getElementById("mas").value+"  ass:"+document.getElementById("assoccode").value);
				flag="1";
				if(i!=0){
					if(obj.address[0].jobsList[i].CC != obj.address[0].jobsList[i-1].CC){
						var fleetname = "";
						var fleetcolor = "gray";
						for(var fl=0;fl<fl_size;fl++){
							if(document.getElementById("fleetNo_"+fl).value==obj.address[0].jobsList[i].CC){
								fleetname = document.getElementById("fleetName_"+fl).value;
								fleetcolor = document.getElementById("fleetcolor_"+fl).value;
								break;
							}
						}
					tableRows=tableRows+'<tr id="row_'+fl+'" align="left" bgcolor="'+fleetcolor+'"> <td colspan="20"><b>'+fleetname+'</b></td></tr>'; 
					}
				}else if(i==0){
					var fleetname = "";
					var fleetcolor = "gray";
					for(var fl=0;fl<fl_size;fl++){
						if(document.getElementById("fleetNo_"+fl).value==obj.address[0].jobsList[i].CC){
							fleetname = document.getElementById("fleetName_"+fl).value;
							fleetcolor = document.getElementById("fleetcolor_"+fl).value;
							break;
						}
					}
					tableRows=tableRows+'<tr id="row_'+fl+'" align="left" bgcolor="'+fleetcolor+'"> <td colspan="20"><b>'+fleetname+'</b></td></tr>'; 
				}
			}
		var driverCab="";
		var addressToShow=obj.address[0].jobsList[i].A.substring(0,25);
		var dAddressToShow=obj.address[0].jobsList[i].DA.substring(0,25);
		if(dispatchBasedOn==2){
			driverCab= obj.address[0].jobsList[i].V;
		}else{
			driverCab= obj.address[0].jobsList[i].D;
		}
		var rowColor = "";
		if(obj.address[0].jobsList[i].AC!=null && obj.address[0].jobsList[i].AC=="cc"){
			rowColor="";
		}else if(obj.address[0].jobsList[i].AC!=null && obj.address[0].jobsList[i].AC=="cash"){
			rowColor='#'+document.getElementById("cashColor").value;
		}else if(obj.address[0].jobsList[i].AC !=null && obj.address[0].jobsList[i].AC=="VC"){
			rowColor='#'+document.getElementById("voucherColor").value;
		}else{
			rowColor="";
		}
		var colorForVip ="";

		if(obj.address[0].jobsList[i].PC == 1){
			colorForVip="#"+document.getElementById("vipColor").value;
		}else if(obj.address[0].jobsList[i].PC == 2){
			colorForVip="#"+document.getElementById("timeColor").value;
		}
		var j=Number(i)+1;
//		tableRows=tableRows+' <tr id="row"   bgcolor="'+colorForVip+'"  class="customerRow">'; 
		if((document.getElementById("fleetSize").value!=0) && (document.getElementById("mas").value==document.getElementById("assoccode").value) && (document.getElementById("fleetDispatch").value==1) && (obj.address[0].jobsList[i].CC!=document.getElementById("assoccode").value)){
			tableRows=tableRows+' <tr id="row"   bgcolor="'+colorForVip+'">';
		}else{
			tableRows=tableRows+' <tr id="row"   bgcolor="'+colorForVip+'" class="customerRow">';
		}
		if(obj.address[0].jobsList[i].SR=="1"){
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+'<font color="red">S</font>)</td>';
		}else{
			if(obj.address[0].jobsList[i].CMT=="" && obj.address[0].jobsList[i].SPL==""){
				tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+')</td>';
			}else{
				//alert("CMT"+obj.address[0].jobsList[i].CMT+" --- spl"+obj.address[0].jobsList[i].SPL);
				tableRows=tableRows+'<td id="jobNo" class="showJobNo" background=images/Dashboard/comment-alt.png>'+j+')</td>';
			}
		} if(obj.address[0].jobsList[i].LMN!=""){
			addressToShow=obj.address[0].jobsList[i].LMN;
		} if(obj.address[0].jobsList[i].ELM!=""){
			dAddressToShow=obj.address[0].jobsList[i].ELM;
		}
		tableRows=tableRows+'<td class="showAddress" width="180px"id="customerid" onclick="currentDetails(\''+obj.address[0].jobsList[i].D+'\','+obj.address[0].jobsList[i].T+','+obj.address[0].jobsList[i].LA+','+obj.address[0].jobsList[i].LO+','+obj.address[0].jobsList[i].S+');mapInitJobs('+0+');mapPlotDriver(0)">'+addressToShow+'</td>';
		tableRows=tableRows+'<td class="showScity"  width="180px" id="showscity">'+obj.address[0].jobsList[i].SCTY+'</td>';
		tableRows=tableRows+'<td class="showDAddress" width="180px"id="customerid" style="white-space:nowrap" onclick="currentDetails(\''+obj.address[0].jobsList[i].D+'\','+obj.address[0].jobsList[i].T+','+obj.address[0].jobsList[i].LA+','+obj.address[0].jobsList[i].LO+','+obj.address[0].jobsList[i].S+');mapInitJobs('+0+');mapPlotDriver(0)">'+dAddressToShow+'</td>';
		tableRows=tableRows+'<td class="showEcity"  width="180px" id="showecity">'+obj.address[0].jobsList[i].ECTY+'</td>';
		tableRows=tableRows+'<td class="showAge"><font color="green"><center>'+obj.address[0].jobsList[i].PT+'m</font></td>';
		tableRows=tableRows+'<td class="showTripId"  style="display:none"id="tripid">'+obj.address[0].jobsList[i].T+'</td>';

		tableRows=tableRows+'<input type="hidden" name="queue_no" id="queue_no'+i+'" value='+obj.address[0].jobsList[i].Q+'>';
		if(obj.address[0].jobsList[i].N=="Flag Trip"){
			tableRows=tableRows+'<td class="showDriverAllocate" id="contactname"><input type="text" readonly="readonly"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[0].jobsList[i].T+',\''+driverCab+'\',\''+obj.address[0].jobsList[i].RT+'\')"  onfocus="changeHappeningJob()" /></td>';		tableRows=tableRows+'<input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[0].jobsList[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[0].jobsList[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/>';
		}else{
			if((document.getElementById("fleetSize").value!=0) && (document.getElementById("mas").value==document.getElementById("assoccode").value) && (document.getElementById("fleetDispatch").value==1) && (obj.address[0].jobsList[i].CC!=document.getElementById("assoccode").value)){
				tableRows=tableRows+'<td  class="showDriverAllocate" id="contactname"><input type="text" readonly="readonly"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[0].jobsList[i].T+',\''+driverCab+'\',\''+obj.address[0].jobsList[i].RT+'\')"  onfocus="changeHappeningJob('+i+')" /></td>';		tableRows=tableRows+'<input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[0].jobsList[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[0].jobsList[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/>';
			}else{
				tableRows=tableRows+'<td  class="showDriverAllocate" id="contactname"><input type="text"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[0].jobsList[i].T+',\''+driverCab+'\',\''+obj.address[0].jobsList[i].RT+'\')"  onfocus="changeHappeningJob('+i+')" /></td>';		tableRows=tableRows+'<input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[0].jobsList[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[0].jobsList[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/>';
			}
			//tableRows=tableRows+'<td  class="showDriverAllocate" id="contactname"><input type="text"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[0].jobsList[i].T+',\''+driverCab+'\',\''+obj.address[0].jobsList[i].RT+'\')"  onfocus="changeHappeningJob('+i+')" /></td>';		tableRows=tableRows+'<input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[0].jobsList[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[0].jobsList[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/>';
		}
		tableRows=tableRows+'<input type="hidden"id="tripid'+i+'" value="'+obj.address[0].jobsList[i].T+'"/>';
		//tableRows=tableRows+'<td class="showDriverId" style="display:none"id="driverId">'+obj.address[0].jobsList[i].D+'</td>';
		tableRows=tableRows+'<td class="showPhone" style="white-space:nowrap" id="phoneNoInJobs">'+obj.address[0].jobsList[i].P+'</td>';
		tableRows=tableRows+'<td class="showName"style=""id="riderName">'+obj.address[0].jobsList[i].N+'</td>';
		tableRows=tableRows+'<input type="hidden" id="lan'+i+'" value="'+obj.address[0].jobsList[i].LA+'"/>';
		tableRows=tableRows+'<input type="hidden" id="lon'+i+'" value="'+obj.address[0].jobsList[i].LO+'"/>';
		tableRows=tableRows+'<input type="hidden" id="city'+i+'" value="'+obj.address[0].jobsList[i].A+'">';
		tableRows=tableRows+'<input type="hidden" name="serviceDate'+i+'" id="serviceDate'+i+'"value="'+obj.address[0].jobsList[i].SD+'">';


		var status="";
		if(obj.address[0].jobsList[i].S=="30"){status="Broadcast";
		}else if(obj.address[0].jobsList[i].S=="40"){status="Allocated";
		}else if(obj.address[0].jobsList[i].S=="43"){status="On Rotue To Pickup";
		}else if(obj.address[0].jobsList[i].S=="50"){status="Customer Cancelled Request";
		}else if(obj.address[0].jobsList[i].S=="51"){status="No Show";
		}else if(obj.address[0].jobsList[i].S=="49"){status="Soon To Clear";
		}else if(obj.address[0].jobsList[i].S=="5"){status="Manual Allocation";
		}else if(obj.address[0].jobsList[i].S=="4"){
			if(obj.address[0].jobsList[i].DD=="1"){
				status="Don't Dispatch";
			}else{
				status="Dispatching";
			}
		}else if(obj.address[0].jobsList[i].S=="0"){status="Unknown";
		}else if(obj.address[0].jobsList[i].S=="47"){status="Trip Started";
		}else if(obj.address[0].jobsList[i].S=="99"){status="Trip On Hold";
		}else if(obj.address[0].jobsList[i].S=="3"){status="Couldnt Find Drivers";
		}else if(obj.address[0].jobsList[i].S=="90"){status="SrHold Job";
		}else if(obj.address[0].jobsList[i].S=="48"){status="Driver Reporting NoShow";}

		tableRows=tableRows+'<input type="hidden" name="status'+i+'" id="status'+i+'" value="'+status+'">';
		tableRows=tableRows+'<input type="hidden" name="phone'+i+'" id="phone'+i+'" value='+obj.address[0].jobsList[i].P+'>';
		tableRows=tableRows+'<input type="hidden" name="passname'+i+'" id="passname'+i+'" value='+obj.address[0].jobsList[i].N+'>';
		tableRows=tableRows+'<input type="hidden" name="driver" id="driver" value='+obj.address[0].jobsList[i].D+'>';
		tableRows=tableRows+'<input type="hidden" name="statusvalue" id="statusvalue" value='+obj.address[0].jobsList[i].S+'>';
		tableRows=tableRows+'<input type="hidden"id="driverIdForDetails'+i+'" name="driverIdForDetails'+i+'"value='+obj.address[0].jobsList[i].D+'>';
		tableRows=tableRows+'<input type="hidden"id="stZone'+i+'" name="stZone'+i+'"value='+obj.address[0].jobsList[i].Q+'>';
		tableRows=tableRows+'<input type="hidden"id="edZone'+i+'" name="edZone'+i+'"value='+obj.address[0].jobsList[i].EDQ+'>';
		tableRows=tableRows+'<input type="hidden"id="OrComments'+i+'" name="OrComments'+i+'"value='+obj.address[0].jobsList[i].OC+'>';
		tableRows=tableRows+'<input type="hidden"id="OrSplIns'+i+'" name="OrSplIns'+i+'"value='+obj.address[0].jobsList[i].SPL+'>';
		tableRows=tableRows+'<input type="hidden"id="ORPty'+i+'" name="ORPty'+i+'"value='+obj.address[0].jobsList[i].PTY+'>';
		tableRows=tableRows+'<input type="hidden"id="OrAmt'+i+'" name="OrAmt'+i+'"value='+obj.address[0].jobsList[i].AMT+'>';
		tableRows=tableRows+'<input type="hidden"id="vehiNo'+i+'" name="vehiNo'+i+'"value='+obj.address[0].jobsList[i].V+'>';

		tableRows=tableRows+'<td class="showStatus">';
		if(obj.address[0].jobsList[i].SR=="1"){
			if(Number(obj.address[0].jobsList[i].S) >=40 && allExistingDrivers.indexOf(obj.address[0].jobsList[i].D)<0){
				tableRows=tableRows+'<img src="images/Dashboard/h.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			} else {
				if(obj.address[0].jobsList[i].S== "40"){
					tableRows=tableRows+'<img src="images/Dashboard/operatorGreen.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="4"||obj.address[0].jobsList[i].S=="8"||obj.address[0].jobsList[i].S== "90"){
					tableRows=tableRows+'<img src="images/Dashboard/operatorYellow.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="3"){
					tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S== "30"){
					tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S== "49"){
					tableRows=tableRows+'<img src="images/Dashboard/soonToComplete.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="5"){
					tableRows=tableRows+'<img class="jqModal" src="images/Dashboard/manualAllocation.png" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else{
					tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}
			}
		}else{
			if(Number(obj.address[0].jobsList[i].S) >=40 && allExistingDrivers.indexOf(obj.address[0].jobsList[i].D)<0){
				tableRows=tableRows+'<img src="images/Dashboard/h.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			} else {
				if(obj.address[0].jobsList[i].S== "30"){
					tableRows=tableRows+'<img src="images/Dashboard/megaphone_red.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 25px;height: 21px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S== "49"){
					tableRows=tableRows+'<img src="images/Dashboard/soonToComplete.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S== "40"){
					tableRows=tableRows+'<img src="images/Dashboard/AllocatedJob.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S== "43"){
					tableRows=tableRows+'<img src="images/Dashboard/OnRouteJob.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="50"){
					tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="51"){
					tableRows=tableRows+'<input type="button" class="jqModal"onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="4"||obj.address[0].jobsList[i].S=="8"||obj.address[0].jobsList[i].S== "90"){
					tableRows=tableRows+'<input type="button"class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="background-color: yellow;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="0"){
					tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="47"){
					tableRows=tableRows+'<img class="jqModal"src="images/Dashboard/tripStatrtedJob.png" onclick="showDialog();showFullDetails('+i+')"style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="5"){
					tableRows=tableRows+'<img class="jqModal"src="images/Dashboard/manualAllocation.png" onclick="showDialog();showFullDetails('+i+')"style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="99"){
					tableRows=tableRows+'&nbsp;<img  class="jqModal" src="images/Dashboard/h.png" onclick="showDialog();showFullDetails('+i+')"style="width: 20px;height: 16px;cursor: pointer;"/><br/>'; 
				}else if(obj.address[0].jobsList[i].S=="3"){
					tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="48"){
					tableRows=tableRows+'&nbsp;<img class="jqModal" src="images/Dashboard/redphone.gif"onclick="showDialog();showFullDetails('+i+')" style="width: 21px;height: 16px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S== "45"){
					tableRows=tableRows+'<img src="images/Dashboard/OnSiteJob.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else{
					tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}
			}
		}
		tableRows=tableRows+'</td>';
		if(obj.address[0].jobsList[i].TS==3){
			tableRows=tableRows+'<td class="showTripSource" style="display:none" id="stripsource"><font color="green">O</font></td>';
		}else if(obj.address[0].jobsList[i].TS==0){
			tableRows=tableRows+'<td class="showTripSource" style="display:none" id="stripsource"><font color="green">C-A</font></td>';
		}else if(obj.address[0].jobsList[i].TS==2){
			tableRows=tableRows+'<td class="showTripSource" style="display:none" id="stripsource"><font color="green">D</font></td>';
		}else if(obj.address[0].jobsList[i].TS==4){
			tableRows=tableRows+'<td class="showTripSource" style="display:none" id="stripsource"><font color="green">C-I</font></td>';
		}else if(obj.address[0].jobsList[i].TS==5){
			tableRows=tableRows+'<td class="showTripSource" style="display:none" id="stripsource"><font color="green">U-F</font></td>';
		}else if(obj.address[0].jobsList[i].TS==6){
			tableRows=tableRows+'<td class="showTripSource" style="display:none" id="stripsource"><font color="green">A-O</font></td>';
		}else{
			tableRows=tableRows+'<td class="showTripSource" style="display:none" id="stripsource"><font color="green">N/A</font></td>';
		}
//		if(obj.address[0].jobsList[i].RT!=""){
//			tableRows=tableRows+'<td style="width:10%;white-space: nowrap;" class="showRouteNum"style=""id="routeNum" onclick="showAllRouteJobs('+obj.address[0].jobsList[i].RT+')" ><img src="images/Dashboard/folder.png" class="jqModal" style="width: 30px;height: 22px;cursor: pointer;"/>'+obj.address[0].jobsList[i].RT+'</td>';
//		} else {
//			tableRows=tableRows+'<td style="width:10%;white-space: nowrap;" class="showRouteNum"style=""id="routeNum">'+obj.address[0].jobsList[i].RT+'</td>';
//		}
		tableRows=tableRows+'<td class="showRouteNum" style="width:10%;white-space: nowrap;" id="routeNum">'+obj.address[0].jobsList[i].RT+'</td>';
		tableRows=tableRows+'<td class="showStZone"style=""id="stZone">'+obj.address[0].jobsList[i].Q+'</td>';
		tableRows=tableRows+'<td class="showEdZone"style=""id="edZone">'+obj.address[0].jobsList[i].EDQ+'</td>';
		tableRows=tableRows+'<td class="showPtime"style=""id="pickupTime">'+obj.address[0].jobsList[i].ST+'</td>';
		tableRows=tableRows+'<td class="showSplFlags"style=""id="sSplFlags">'+obj.address[0].jobsList[i].DP+'</td>';
		tableRows=tableRows+'<td class="showFlight"style=""id="sFlight">'+obj.address[0].jobsList[i].AIR+'</td>';
		tableRows=tableRows+'<td class="showVoucher"style=""id="sVoucher">'+obj.address[0].jobsList[i].VC+'</td>';
		tableRows=tableRows+'<td class="showAmount"style=""id="sAmount">'+tripAmt+'</td>';
		tableRows=tableRows+'<td class="showcheck"style=""id="scheck"><input type="checkbox" name="" name="scheck_'+i+'" id="scheck_'+i+'" onclick="checkjobs('+obj.address[0].jobsList[i].T+')"></input></td>';
		
		//tableRows=tableRows+'<td class="showcheck"style=""id="showcheck"><input type="checkbox" name="showcheck" id="showcheck"></input></td>';
		
//		tableRows=tableRows+'<td class="showLandmark"style=""id="ssLandmark">'+obj.address[0].jobsList[i].LMN+'</td>'	
		tableRows=tableRows+'<td class="showDRId"  style="display:none"id="driverId">'+obj.address[0].jobsList[i].D+'</td>';
	}
	
	//divPlain.innerHTML =tableRows;

	
	for(var i=0;i<obj.address[0].jobsList.length;i++){
		var driverCab="";
		if(document.getElementById("dispatch").value==2)
		{
			driverCab= obj.address[0].jobsList[i].V;

		}else{
			driverCab= obj.address[0].jobsList[i].D;
		}
		var j=Number(i)+1;
		tableRowsEx=tableRowsEx+'<tr bgcolor="#EFEFFB"><td>'+j+'</td>';
		tableRowsEx=tableRowsEx+'<td style="white-space:nowrap">'+obj.address[0].jobsList[i].A+'</td>';
		tableRowsEx=tableRowsEx+'<td><input type="text" size="5" id="driver_id'+i+'"value="'+driverCab+'" onblur="updateDriver1('+i+','+obj.address[0].jobsList[i].T+','+obj.address[0].jobsList[i].D+')"onkeypress="changeHappeningJob()"/></td>';
		tableRowsEx=tableRowsEx+'<td><input type="text"id="pickup'+i+'" size="3"value="'+obj.address[0].jobsList[i].ST+'" onblur="updateDriver('+i+','+obj.address[0].jobsList[i].T+',123)"onkeypress="changeHappeningJob()"></td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[0].jobsList[i].P+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[0].jobsList[i].N+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[0].jobsList[i].V+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[0].jobsList[i].DP+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[0].jobsList[i].Q+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[0].jobsList[i].D+'</td>';
		tableRowsEx=tableRowsEx+'	</tr>';
	}

}
	tableAppend.innerHTML=tableRows;
	if(flag=="1"){
		//alert("boom");
		hideDetails();
	}else{
		dragtable.makeDraggable($('#customerTable')[0]);
	}
	
	customerExpand.innerHTML=tableRowsEx;
	document.getElementById("unload").src="images/Dashboard/refresh1.png";
	mapInitJobs('1');
	var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
	var iRowCount= oRows.length;
	document.getElementById("totalJobs").innerHTML=obj.address[0].jobsList.length;
	jobDetailsSelectedCheck();
}
function showAllRouteJobs(routeNum){
	if(routeNum!=""){
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url ='DashBoard?event=sharedRideAjax&routeNo='+routeNum;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("showFullRoute").innerHTML =text;
		$("#showFullRoute").jqm();
		$("#showFullRoute").jqmShow();
	}
}

function sharedRideSelectedCheck(){
	if(!document.getElementById("showDShTripId").checked){
		$('.showDShTripId').hide();
	}else{
		$('.showDShTripId').show();
	}
	if(!document.getElementById("showDShtripSource").checked){
		$('.showDShtripSource').hide();
	}else{
		$('.showDShtripSource').show();
	}
	if(!document.getElementById("showDSHName").checked){
		$('.showDSHName').hide();
	}else{
		$('.showDSHName').show();
	}
	if(!document.getElementById("showDSHDate").checked){
		$('.showDSHDate').hide();
	}else{
		$('.showDSHDate').show();
	}
	if(!document.getElementById("showDSHAddress").checked){
		$('.showDSHAddress').hide();
	}else{
		$('.showDSHAddress').show();
	}
	if(!document.getElementById("showDSHSZone").checked){
		$('.showDSHSZone').hide();
	}else{
		$('.showDSHSZone').show();
	}
	if(!document.getElementById("showDSHEDAddress").checked){
		$('.showDSHEDAddress').hide();
	}else{
		$('.showDSHEDAddress').show();
	}if(!document.getElementById("showDSHEDZone").checked){
		$('.showDSHEDZone').hide();
	}else{
		$('.showDSHEDZone').show();
	}if(!document.getElementById("showDSHRouteNo").checked){
		$('.showDSHRouteNo').hide();
	}else{
		$('.showDSHRouteNo').show();
	}if(!document.getElementById("showDSHRef").checked){
		$('.showDSHRef').hide();
	}else{
		$('.showDSHRef').show();
	}
	if(!document.getElementById("showDSHRef1").checked){
		$('.showDSHRef1').hide();
	}else{
		$('.showDSHRef1').show();
	}
	if(!document.getElementById("showDSHRef2").checked){
		$('.showDSHRef2').hide();
	}else{
		$('.showDSHRef2').show();
	}if(!document.getElementById("showDSHRef3").checked){
		$('.showDSHRef3').hide();
	}else{
		$('.showDSHRef3').show();
	}

}

function hideSharedRide(){
	value ="0";
	value1 ="1";
	if(!document.getElementById("showDShTripId").checked){
		$('.showDShTripId').hide();
		name="GAC-D-DSH-showDShTripId";
		if($("#GAC-D-DSH-showDShTripId").val()!=null){
			$("#GAC-D-DSH-showDShTripId").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDShTripId').show();
		name="GAC-D-DSH-showDShTripId";
		if($("#GAC-D-DSH-showDShTripId").val()!=null){
			$("#GAC-D-DSH-showDShTripId").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}

	if(!document.getElementById("showDSHName").checked){
		$('.showDSHName').hide();
		name="GAC-D-DSH-showDSHName";
		if($("#GAC-D-DSH-showDSHName").val()!=null){
			$("#GAC-D-DSH-showDSHName").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showDSHName').show();
		name="GAC-D-DSH-showDSHName";
		if($("#GAC-D-DSH-showDSHName").val()!=null){
			$("#GAC-D-DSH-showDSHName").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}
	if(!document.getElementById("showDSHDate").checked){
		$('.showDSHDate').hide();
		name="GAC-D-DSH-showDSHDate";
		if($("#GAC-D-DSH-showDSHDate").val()!=null){
			$("#GAC-D-DSH-showDSHDate").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showAddress').show();
		name="GAC-D-DSH-showDSHDate";
		if($("#GAC-D-DSH-showDSHDate").val()!=null){
			$("#GAC-D-DSH-showDSHDate").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showDSHAddress").checked){
		$('.showDSHAddress').hide();
		name="GAC-D-DSH-showDSHAddress";
		if($("#GAC-D-DSH-showDSHAddress").val()!=null){
			$("#GAC-D-DSH-showDSHAddress").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showDSHAddress').show();
		name="GAC-D-DSH-showDSHAddress";
		if($("#GAC-D-DSH-showDSHAddress").val()!=null){
			$("#GAC-D-DSH-showDSHAddress").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showDSHSZone").checked){
		$('.showDSHSZone').hide();
		name="GAC-D-DSH-showDSHSZone";
		if($("#GAC-D-DSH-showDSHSZone").val()!=null){
			$("#GAC-D-DSH-showDSHSZone").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDSHSZone').show();
		name="GAC-D-DSH-showDSHSZone";
		if($("#GAC-D-DSH-showDSHSZone").val()!=null){
			$("#GAC-D-DSH-showDSHSZone").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}
	if(!document.getElementById("showDSHEDAddress").checked){
		$('.showDSHEDAddress').hide();
		name="GAC-D-DSH-showDSHEDAddress";
		if($("#GAC-D-DSH-showDSHEDAddress").val()!=null){
			$("#GAC-D-DSH-showDSHEDAddress").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDSHEDAddress').show();
		name="GAC-D-DSH-showDSHEDAddress";
		if($("#GAC-D-DSH-showDSHEDAddress").val()!=null){
			$("#GAC-D-DSH-showDSHEDAddress").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showDSHEDZone").checked){
		$('.showDSHEDZone').hide();
		name="GAC-D-DSH-showDSHEDZone";
		if($("#GAC-D-DSH-showDSHEDZone").val()!=null){
			$("#GAC-D-DSH-showDSHEDZone").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDSHEDZone').show();
		name="GAC-D-DSH-showDSHEDZone";
		if($("#GAC-D-DSH-showDSHEDZone").val()!=null){
			$("#GAC-D-DSH-showDSHEDZone").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}if(!document.getElementById("showDSHRouteNo").checked){
		$('.showDSHRouteNo').hide();
		name="GAC-D-DSH-showDSHRouteNo";
		if($("#GAC-D-DSH-showDSHRouteNo").val()!=null){
			$("#GAC-D-DSH-showDSHRouteNo").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDSHRouteNo').show();
		name="GAC-D-DSH-showDSHRouteNo";
		if($("#GAC-D-DSH-showDSHRouteNo").val()!=null){
			$("#GAC-D-DSH-showDSHRouteNo").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}if(!document.getElementById("showDSHRef").checked){
		$('.showDSHRef').hide();
		name="GAC-D-DSH-showDSHRef";
		if($("#GAC-D-DSH-showDSHRef").val()!=null){
			$("#GAC-D-DSH-showDSHRef").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDSHRef').show();
		name="GAC-D-DSH-showDSHRef";
		if($("#GAC-D-DSH-showDSHRef").val()!=null){
			$("#GAC-D-DSH-showDSHRef").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}if(!document.getElementById("showDSHRef1").checked){
		$('.showDSHRef1').hide();
		name="GAC-D-DSH-showDSHRef1";
		if($("#GAC-D-DSH-showDSHRef1").val()!=null){
			$("#GAC-D-DSH-showDSHRef1").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDSHRef1').show();
		name="GAC-D-DSH-showDSHRef1";
		if($("#GAC-D-DSH-showDSHRef1").val()!=null){
			$("#GAC-D-DSH-showDSHRef1").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}if(!document.getElementById("showDSHRef2").checked){
		$('.showDSHRef2').hide();
		name="GAC-D-DSH-showDSHRef2";
		if($("#GAC-D-DSH-showDSHRef2").val()!=null){
			$("#GAC-D-DSH-showDSHRef2").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDSHRef2').show();
		name="GAC-D-DSH-showDSHRef2";
		if($("#GAC-D-DSH-showDSHRef2").val()!=null){
			$("#GAC-D-DSH-showDSHRef2").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showDSHRef3").checked){
		$('.showDSHRef3').hide();
		name="GAC-D-DSH-showDSHRef3";
		if($("#GAC-D-DSH-showDSHRef3").val()!=null){
			$("#GAC-D-DSH-showDSHRef3").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDSHRef3').show();
		name="GAC-D-DSH-showDSHRef3";
		if($("#GAC-D-DSH-showDSHRef3").val()!=null){
			$("#GAC-D-DSH-showDSHRef3").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showDShtripSource").checked){
		$('.showDShtripSource').hide();
		name="GAC-D-DSH-showDShtripSource";
		if($("#GAC-D-DSH-showDShtripSource").val()!=null){
			$("#GAC-D-DSH-showDShtripSource").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showDShtripSource').show();
		name="GAC-D-DSH-showDShtripSource";
		if($("#GAC-D-DSH-showDShtripSource").val()!=null){
			$("#GAC-D-DSH-showDShtripSource").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}


}
function showSharedRideSettings(){
	$("#sharedRideSettings").jqm();
	$("#sharedRideSettings").jqmShow();
}
function stopRefreshing(searchVal){
	setTimeout(function(){
		var sValue = document.getElementById(searchVal).value;
		if(sValue!=""){
	document.getElementById("changeHappeningJob").value="true";
		}else{
			document.getElementById("changeHappeningJob").value="false";
		}
	},0);
}
function stopRefreshingSharedRide(searchVal){
	setTimeout(function(){

	},0);
}

function checkjobs(tripid){
	autoRefreshOff();
	acdata=acdata+tripid+";";
    document.getElementById('tripForanother1').value = acdata;
	 $("#option").show();
	 $("#tripForanother1").val(acdata);

}

function removeScreen(){
	 $("#option").hide();
	 createCookieForAutoRefreshOnOff('on');Init();
	}

function changeMfleet(){
	 $("#changeFleetjob").show();
}
function ChangeFleetToMulJob() {
	var fleetNo = document.getElementById("fleetCV").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = '/TDS/DashBoard?event=changeMultipleJobToAnotherFleet&tripId=' + acdata + '&fleetNo=' + fleetNo	;
	//alert(url);
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		$("#changeFleetjob").hide();
		 $("#option").hide();
		 createCookieForAutoRefreshOnOff('on');Init();
		alert("Job Send to to another Fleet");
		acdata=0;
	}
}
function Changezone(){
	$("#ChangeZone").show();

}

