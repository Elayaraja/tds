//For Tabs in Driver Details

var acdata="";
var tempObj ="null";
var driverLen="null";
jQuery(function($) {

	tabs = function(options) {

		var defaults = {
				selector : '.tabs',
				selectedClass : 'selected'
		};

		if (typeof options == 'string')
			defaults.selector = options;
		var options = $.extend(defaults, options);

		return $(options.selector).each(function() {
			var obj = this;
			var targets = Array();

			function show(i) {
				$.each(targets, function(index, value) {
					$(value).hide();
				});
				$(targets[i]).fadeIn('fast');
				if (targets[i] == "#tab1") {
					document.getElementById("driverShowCheck").value = "lin";
					//driverDetails('lin');
					getAllValuesForDashboard('01000');
				} else if (targets[i] == "#tab2") {
					document.getElementById("driverShowCheck").value = "lo";
					//driverDetails('lo');
					getAllValuesForDashboard('01000');
				} else if (targets[i] == "#tab3") {
					document.getElementById("driverShowCheck").value = "all";
					//driverDetails('all');
					getAllValuesForDashboard('01000');
				}
				$(obj).children().removeClass(options.selectedClass);
				selected = $(obj).children().get(i);
				$(selected).addClass(options.selectedClass);
			}
			;

			$('a', this).each(function(i) {
				targets.push($(this).attr('href'));
				$(this).click(function(e) {
					e.preventDefault();
					show(i);
				});
			});

			show(0);

		});
	};
	tabs('#tabs nav ul');

});
//For tabs in DriverDetailsExpandScreen
jQuery(function($) {

	tabs = function(options) {

		var defaults = {
				selector : '.tabs',
				selectedClass : 'selected'
		};

		if (typeof options == 'string')
			defaults.selector = options;
		var options = $.extend(defaults, options);

		return $(options.selector).each(function() {

			var obj = this;
			var targets = Array();

			function show(i) {
				$.each(targets, function(index, value) {
					$(value).hide();
				});
				$(targets[i]).fadeIn('fast');
				if (targets[i] == "#tabD1") {
					document.getElementById("driverShowCheck").value = "lin";
					//driverDetails('lin');
					getAllValuesForDashboard('01000');
				} else if (targets[i] == "#tabD2") {
					document.getElementById("driverShowCheck").value = "lo";
					//driverDetails('lo');
					getAllValuesForDashboard('01000');
				} else if (targets[i] == "#tabD3") {
					document.getElementById("driverShowCheck").value = "all";
					//driverDetails('all');
					getAllValuesForDashboard('01000');
				}
				$(obj).children().removeClass(options.selectedClass);
				selected = $(obj).children().get(i);
				$(selected).addClass(options.selectedClass);
			}
			;

			$('a', this).each(function(i) {
				targets.push($(this).attr('href'));
				$(this).click(function(e) {
					e.preventDefault();
					show(i);
				});
			});

			show(0);

		});
	};
	tabs('#tabDiv nav ul');

});
//For textMessage
function createMsg() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'DashBoard?event=cannedMsg&from=dash';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	document.getElementById("messageTemplates").innerHTML = text;
	$("#messageTemplates").jqm();
	$("#messageTemplates").jqmShow();
}
function appendMsg(msg) {
	document.getElementById("msg").value = msg;
	document.getElementById("msg").readOnly = true;
	$("#messageTemplates").jqmHide();
}
function removeMsgTemp() {
	document.getElementById("msg").value = "";
	document.getElementById("msg").readOnly = false;
}
function removeMessageTemplate() {
	$("#messageTemplates").jqmHide();
}
//*******************
function switchMapScreen(checkSwitch) {
	if(checkSwitch=="osm"){
		window.open('DashBoard?event=dispatchValues&map=osm', '_self');
	}else{
		window.open('DashBoard?event=dispatchValues', '_self');
	}
}


//*****************
function onloadDash() {
	var cName = "";
	var cValue = "";
	//notifyMessage();
	//checkMessages();
	$(":input:hidden").each(function() {
		cName = $(this).attr('id') + "";
		cValue = $(this).val() + "";
		var lengthC = cName.length;
		if (cName.indexOf("GAC-D-J") != -1) {
			var nameC = cName.substring(8, lengthC);
			if (cValue == "1") {
				$("#" + nameC).attr('checked', true)
			} else {
				$("#" + nameC).attr('checked', false)
			}
		}else if (cName.indexOf("GAC-D-DSH") != -1) {
			var nameC = cName.substring(10, lengthC);
			if (cValue == "1") {
				$("#" + nameC).attr('checked', true)
			} else {
				$("#" + nameC).attr('checked', false)
			}
		}else if (cName.indexOf("GAC-D-ZC") != -1) {
			var nameC = cName.substring(9, lengthC);
			if (nameC == "z1") {
				document.getElementById("zoneColor1").value = cValue;
				$("#zoneColor1").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z2") {
				document.getElementById("zoneColor2").value = cValue;
				$("#zoneColor2").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z3") {
				document.getElementById("zoneColor3").value = cValue;
				$("#zoneColor3").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z4") {
				document.getElementById("zoneColor4").value = cValue;
				$("#zoneColor4").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z5") {
				document.getElementById("zoneColor5").value = cValue;
				$("#zoneColor5").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z6") {
				document.getElementById("zoneColor6").value = cValue;
				$("#zoneColor6").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z7") {
				document.getElementById("zoneColor7").value = cValue;
				$("#zoneColor7").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z8") {
				document.getElementById("zoneColor8").value = cValue;
				$("#zoneColor8").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z9") {
				document.getElementById("zoneColor9").value = cValue;
				$("#zoneColor9").css('backgroundColor', '#' + cValue);
			} else if (nameC == "z10") {
				document.getElementById("zoneColor10").value = cValue;
				$("#zoneColor10").css('backgroundColor', '#' + cValue);
			}
		} else if (cName.indexOf("GAC-D-C") != -1) {
			var nameC = cName.substring(8, lengthC);

			if (nameC == "vipColor") {
				document.getElementById("vipColor").value = cValue;
				$("#vipColor").css('backgroundColor', '#' + cValue);
			} else if (nameC == "timeColor") {
				document.getElementById("timeColor").value = cValue;
				$("#timeColor").css('backgroundColor', '#' + cValue);
			} else if (nameC == "voucherColor") {
				document.getElementById("voucherColor").value = cValue;
				$("#voucherColor").css('backgroundColor', '#' + cValue);
			} else if (nameC == "cashColor") {
				document.getElementById("cashColor").value = cValue;
				$("#cashColor").css('backgroundColor', '#' + cValue);
			} else if (nameC == "avColor") {
				document.getElementById("avColor").value = cValue;
				$("#avColor").css('backgroundColor', '#' + cValue);
			} else if (nameC == "noUpdate") {
				document.getElementById("noUpdate").value = cValue;
				$("#noUpdate").css('backgroundColor', '#' + cValue);
			} else if (nameC == "navColor") {								
				document.getElementById("navColor").value = cValue;
				$("#navColor").css('backgroundColor', '#' + cValue);
			}else if(nameC.indexOf("FC")!=-1){
				var fleetIdentifier=document.getElementById("fleetSize").value;
				for(var i=0;i<fleetIdentifier;i++){					
					if (nameC == "FC"+i) {						
						document.getElementById("fleetcolor_"+i).value = cValue;
						$("#fleetcolor_"+i).css('backgroundColor', '#' + cValue);
						continue;
					}
					if (document.getElementById("fleetNumberForCookie"+i).value==document.getElementById("assoccode").value){
						document.getElementById("top-header").style.backgroundColor = "#"+$("#fleetcolor_"+i).val();

					}
				}				
			}
		} else if (cName.indexOf("GAC-D") != -1) {
			var nameC = cName.substring(6, lengthC);
			if (nameC == "zoomValue") {
				document.getElementById("zoomValue").value = cValue;
				document.getElementById("zoom").value = cValue;
			} else if (nameC == "autoRefreshTime") {
				document.getElementById("sec").value = cValue;
				document.getElementById("sec1").value = cValue;
			} else if (nameC == "individualZoom") {
				document.getElementById("individualZoomValue").value = cValue;
				document.getElementById("individualZoom").value = cValue;
			} else if (nameC == "lat") {
				document.getElementById("latFromMap").value = cValue;
			} else if (nameC == "long") {
				document.getElementById("longFromMap").value = cValue;
			} else if (nameC == "opnReq") {
				if (cValue == "1") {
					$("#openjobs").show("slow");
					$("#plain").draggable({
						handle : '#jobsTop',
						scroll : false
					});
				}
			}  else if (nameC == "cabDetails") {
				if (cValue == "1") {
					$("#drivers1").show("slow");
					$("#right-block").draggable({
						handle : '.block-top',
						scroll : false
					});
				}
			} else if (nameC == "zneWait") {
				if (cValue == "1") {
					$("#queueid").show("slow");
					$("#queueid").draggable({
						handle : '.block-top1',
						scroll : false
					});
				}
			} else if (nameC == "zneRate") {
				if (cValue == "1") {
					$("#showzoneRates").show("slow");
					$("#showzoneRates").draggable({
						handle : '.block-topRate',
						scroll : false
					});
				}
			} else if (nameC == "options") {
				if (cValue == "1") {
					$("#OptionsForAll").jqm();
					$("#OptionsForAll").jqmShow();
//					$("#OptionsForAll").draggable({
//						handle : '.block-top55',
//						scroll : false
//					});
				}
			} else if (nameC == "statistics") {
				if (cValue == "1") {
					jobCount();
					$("#statistics").show("slow");
					$("#statistics").draggable({
						scroll : false
					});
				}
			} else if (nameC == "txtMsg") {
				if (cValue == "1") {
					$("#sendSms").jqm();
					$("#sendSms").jqmShow();
//					$("#sendSms").show("slow");
//					$("#sendSms").draggable({
//						scroll : false
//					});
				}
			}
		}
	});
	readRefreshValue();

}
function readRefreshValue() {
	if (document.getElementById("GAC-D-autoRefreshOn") != null
			&& document.getElementById("GAC-D-autoRefreshOn").value == "on") {
		Init();
	} else if (document.getElementById("GAC-D-autoRefreshOn") == null) {
		Init();
	}
}

function readCookieForCenter() {
	if ($("#GAC-D-lat").val() != null) {
		document.getElementById("latFromMap").value = $("#GAC-D-lat").val();
	}
	if ($("#GAC-D-long").val() != null) {
		document.getElementById("longFromMap").value = $("#GAC-D-long").val();
	}
}
function rempoveEndZone() {
	$("#setEndZone").hide();
}
function setEndZone() {
	$("#setEndZone").hide();
	var zoneKey = document.getElementById("zoneListforEndZone").value;
	var tripId = document.getElementById("tripIdForEndZone").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=setEndZone&tripId=' + tripId + '&zoneKey='
	+ zoneKey;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
}
//*****************
function removeGetDriver() {
	$("#getDriver").hide();
}
function removeFleetScreen() {
	$("#changeFleet").hide();
}
//***************
function ShowJqm() {
	$('#ahowAllZoneNames').jqm();
}
function createZonesList() {
	var zoneList = "";
	var oRows = document.getElementById("zonesForOperator")
	.getElementsByTagName("tr");
	var iRowCount = oRows.length;
	for ( var j = 0; j < iRowCount; j++) {
		if (document.getElementById("selectZones_" + j).checked) {
			var z1 = oRows[j].getElementsByTagName("input")[0].value;
			if (z1 != null) {
				zoneList += z1 + ";";
			}
		}
	}
	if (document.getElementById("defZone").checked) {
		zoneList += document.getElementById("defZone").value;
	}
	document.getElementById("unload").src = "images/Dashboard/loading_icon.gif";
	var url = "";
	if (document.getElementById("All").checked == true) {
		url = 'DashBoard?event=jobDetails&status=40&zoneList=' + zoneList;
	} else {
		url = 'DashBoard?event=jobDetails&status=0&zoneList=' + zoneList;
	}
	$.cookie("GAC-zoneList", zoneList, {
		expires : 365
	});
	$('#ahowAllZoneNames').jqmHide();
	var ajax = new AjaxInteraction(url, jobDetails1);
	ajax.doGet();
}

function resetZoneList() {
	$.cookie("GAC-zoneList", null);
	var oRows = document.getElementById("zonesForOperator")
	.getElementsByTagName("tr");
	var iRowCount = oRows.length;
	for ( var j = 0; j < iRowCount; j++) {
		document.getElementById("selectZones_" + j).checked = false;
	}
}
function selectAllZonesList() {
	var oRows = document.getElementById("zonesForOperator")
	.getElementsByTagName("tr");
	var iRowCount = oRows.length;
	for ( var j = 0; j < iRowCount; j++) {
		document.getElementById("selectZones_" + j).checked = true;
	}
}
function unSelectAllZonesList() {
	var oRows = document.getElementById("zonesForOperator")
	.getElementsByTagName("tr");
	var iRowCount = oRows.length;
	for ( var j = 0; j < iRowCount; j++) {
		document.getElementById("selectZones_" + j).checked = false;
	}
}
function closeZoneList() {
	$("#ahowAllZoneNames").jqmHide();
}
function locateDriver() {
	var driver = "";
	if (document.getElementById("maDriverId").value != null
			&& document.getElementById("maDriverId").value != "") {
		driver = document.getElementById("maDriverId").value;
	} else {
		var cab = document.getElementById("maCabId").value;
		if (cab != null && cab != "") {
			for ( var i = 0; i < globalDrivers.driverList.length; i++) {
				if (globalDrivers.driverList[i].varVNo == cab) {
					driver = globalDrivers.driverList[i].varDr;
					break;
				}
			}
		}
	}
	$("#getDriver").hide();
	var lattitude = document.getElementById("latForRightclick").value;
	var longitude = document.getElementById("longForRightclick").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = '/TDS/DashBoard?event=positioningDriver&driverId=' + driver
	+ '&lat=' + lattitude + '&longi=' + longitude;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == "1") {
		var cell = document.createTextNode("" + currentTimeCalculate()
				+ ":location Updated for Driver/Cab:" + driver);
		var cellBr = document.createElement("br");
		document.getElementById("console").appendChild(cellBr);
		document.getElementById("console").appendChild(cell);
	}
}

function ShowZonesList() {
	$('#ahowAllZoneNames').jqmShow();
	var oRows = document.getElementById("zonesForOperator")
	.getElementsByTagName("tr");
	var iRowCount = oRows.length;
	if ($.cookie("GAC-zoneList") != null && $.cookie("GAC-zoneList") != "") {
		zoneList = $.cookie("GAC-zoneList");
		var zoneList1 = zoneList.split(';');
		for ( var k = 0; k < zoneList1.length; k++) {
			for ( var l = 0; l < iRowCount; l++) {
				var z1 = oRows[l].getElementsByTagName("input")[0].value;
				if (zoneList1[k] == z1) {
					document.getElementById("selectZones_" + l).checked = true;
				}
			}
			if (document.getElementById("defZone").value == zoneList1[k]) {
				document.getElementById("defZone").checked = true;
			}
		}
	}
}

function dashBoardAjax(n) {
	// console.log("About to get zone data");
	document.getElementById("refreshZone").src = "images/Dashboard/loading_icon.gif";
	// url =
	// '/TDS/DashBoard?event=zoneDetailsForDash&assoccode='+document.getElementById('assoccode').value+'&stat='+n;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=zoneDetailsForDash&assoccode='
		+ document.getElementById('assoccode').value + '&stat=' + n;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	document.getElementById("refreshZone").src = "images/Dashboard/refresh1.png";
	document.getElementById("queueid").innerHTML = text;
	// console.log("after get zone data");
	// console.log("after get zone data again");
	document.getElementById("refreshZone").src = "images/Dashboard/refresh1.png";
}
function getDashBoardValues(responseText) {
	// console.log("About to display zone data");
	document.getElementById("refreshZone").src = "images/Dashboard/refresh1.png";
	document.getElementById("queueid").innerHTML = responseText;
}
function loginDriverOrCab() {
	var driverIdForLogin = document.getElementById("driverIdForLogin").value;
	var cabNoForLogin = document.getElementById("cabForLogin").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = '/TDS/DashBoard?event=loginDriverFromDashboard&driverId='
		+ driverIdForLogin + '&cabNo=' + cabNoForLogin;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != 0) {
		getAllValuesForDashboard('01000');
		document.getElementById("driverIdForLogin").value = "";
		document.getElementById("cabForLogin").value = "";
		var cell = document.createTextNode("" + currentTimeCalculate() + ""
				+ driverIdForLogin + " : Driver LoggedIn from Dashboard");
		var cellBr = document.createElement("br");
		document.getElementById("console").appendChild(cellBr);
		document.getElementById("console").appendChild(cell);
	}
}


function createCookieForFleet(){
	var fleetIdentifier=document.getElementById("fleetSize").value;
	for(var i=0;i<fleetIdentifier;i++){					
		if ($("#GAC-D-C-FC"+i).val() != null) {
			$("#GAC-D-C-FC"+i).val($("#fleetcolor_"+i).val());
		} else {
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-C-FC"+i).attr('id', "GAC-D-C-FC"+i)	.val($("#fleetcolor_"+i).val()));
		}
	}
}


function createCookieForZoneColors() {
	if ($("#GAC-D-ZC-z1").val() != null) {
		$("#GAC-D-ZC-z1").val($("#zoneColor1").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z1").attr('id', "GAC-D-ZC-z1")
				.val($("#zoneColor1").val()));
	}
	if ($("#GAC-D-ZC-z2").val() != null) {
		$("#GAC-D-ZC-z2").val($("#zoneColor2").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z2").attr('id', "GAC-D-ZC-z2")
				.val($("#zoneColor2").val()));
	}
	if ($("#GAC-D-ZC-z3").val() != null) {
		$("#GAC-D-ZC-z3").val($("#zoneColor3").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z3").attr('id', "GAC-D-ZC-z3")
				.val($("#zoneColor3").val()));
	}
	if ($("#GAC-D-ZC-z4").val() != null) {
		$("#GAC-D-ZC-z4").val($("#zoneColor4").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z4").attr('id', "GAC-D-ZC-z4")
				.val($("#zoneColor4").val()));
	}
	if ($("#GAC-D-ZC-z5").val() != null) {
		$("#GAC-D-ZC-z5").val($("#zoneColor5").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z5").attr('id', "GAC-D-ZC-z5")
				.val($("#zoneColor5").val()));
	}
	if ($("#GAC-D-ZC-z6").val() != null) {
		$("#GAC-D-ZC-z6").val($("#zoneColor6").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z6").attr('id', "GAC-D-ZC-z6")
				.val($("#zoneColor6").val()));
	}
	if ($("#GAC-D-ZC-z7").val() != null) {
		$("#GAC-D-ZC-z7").val($("#zoneColor7").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z7").attr('id', "GAC-D-ZC-z7")
				.val($("#zoneColor7").val()));
	}
	if ($("#GAC-D-ZC-z8").val() != null) {
		$("#GAC-D-ZC-z8").val($("#zoneColor8").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z8").attr('id', "GAC-D-ZC-z8")
				.val($("#zoneColor8").val()));
	}
	if ($("#GAC-D-ZC-z9").val() != null) {
		$("#GAC-D-ZC-z9").val($("#zoneColor9").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden')
				.attr('name', "GAC-D-ZC-z9").attr('id', "GAC-D-ZC-z9")
				.val($("#zoneColor9").val()));
	}
	if ($("#GAC-D-ZC-z10").val() != null) {
		$("#GAC-D-ZC-z10").val($("#zoneColor10").val());
	} else {
		$('#cookieValueHidden').append(
				$('<input/>').attr('type', 'hidden').attr('name',
				"GAC-D-ZC-z10").attr('id', "GAC-D-ZC-z10").val(
						$("#zoneColor10").val()));
	}
}
function removeSuggestions(){
	$("#SuggestionsToAllocate").jqmHide();
}
//*************************************
//COntextmenu For job details
function toRad(Value) {
	/** Converts numeric degrees to radians */
	return Value * Math.PI / 180;
}
function distance(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var radlon1 = Math.PI * lon1/180
	var radlon2 = Math.PI * lon2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	if (unit=="KM") {
		dist = dist * 1.609344;
		dist = Math.round(dist*1000)/1000;
	}
	if (unit=="Miles") { dist = dist * 0.8684;dist = Math.round(dist*1000)/1000; }
	return dist;
} 
function allocateDriverFromSuggestion(driver,jobNo,trip,type){
	$("#SuggestionsToAllocate").hide();
	var jobNOvalue = parseInt(jobNo) -1;
	document.getElementById('driver_id'+jobNOvalue).value=driver;
	var r=confirm("Are you Sure!");
	if (r==true) {
		updateDriver(jobNOvalue,trip,driver,type);
	}

}


//*************************************
//COntextmenu For job details

function contextMenuWork(key, trip, driver, phone, jobNo, riderName) {
	if (riderName != "Flag Trip") {
		switch (key) {
		case "delete": {
			deleteJobs(trip);
			break;
		}case "callDriver": {
			callDriverForStatus(trip);
			break;
		}case "allocateDriver" :{
			distanceBasedOn = "";
			if(document.getElementById("distanceBasedOn").value=="0"){
				distanceBasedOn ='Miles';
			}else{
				distanceBasedOn ='KM';
			}
			var lat1,lon1,lat2,lon2,driverAllocate,cab,stZone,availability;
			$("#SuggestionsToAllocate").jqm();
			$("#SuggestionsToAllocate").jqmShow();
			var index = "row";
			var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
			var iRowCount= oRows.length;
			for(var k = 1; k < iRowCount; k++) {
				if(oRows[k].id==index){
					if(trip ==  oRows[k].getElementsByTagName("input")[3].value){
						lat1 = oRows[k].getElementsByTagName("input")[4].value;
						lon1 = oRows[k].getElementsByTagName("input")[5].value;
						stZone = oRows[k].getElementsByTagName("input")[14].value;
						//get lat and long
						break;
					}
				}
			}
			document.getElementById("suggAll").innerHTML ="<tr bgcolor='orange'><th>DriverID</th><th>CabNo</th><th>Distance</th><th>Offer/Force</th>";
			document.getElementById("sortedID").innerHTML ="<tr bgcolor='orange'><th>DriverID</th><th>CabNo</th><th>Distance</th>";
			var sortDistances = [];
			var listOfVal = [];

			var clrZones =[];
			var listZones =[];
			for ( var i = 0; i < globalDrivers.driverList.length; i++) {
				lat2 = globalDrivers.driverList[i].varLt;
				lon2 = globalDrivers.driverList[i].varLg;
				driverAllocate = globalDrivers.driverList[i].varDr;
				cab = globalDrivers.driverList[i].varVNo;
				availability = globalDrivers.driverList[i].varSwh;

				var d = distance(lat1,lon1,lat2,lon2,distanceBasedOn);
				if(availability!="L"){
					listOfVal.push({ 
						"dr" : driverAllocate,
						"cab"  : cab,
						"dis" :  d,
						"avl" : availability
					});
				}
			}
			listOfVal.sort(function(a,b) { return parseFloat(a.dis) - parseFloat(b.dis) });
			var driverOrCab ="";
			var color ="";
			for(var p=0;p<listOfVal.length;p++){
				var drColor ="";
				var vhColor ="";
				disp = document.getElementById("dispatch").value;

				if(listOfVal[p].avl=="Y"){
					color = "available"; 
				}else if(listOfVal[p].avl=="N"){
					color = "unavailable";
				}else{
					color = "noFeed";
				}
				if(disp==2){
					driverOrCab = listOfVal[p].cab;
					vhColor = color;
				}else{
					driverOrCab = listOfVal[p].dr;
					drColor = color;
				}
				document.getElementById("suggAll").innerHTML +="<tr ><td id="+drColor+" align='center'>"+listOfVal[p].dr+"</td><td  id="+vhColor+" align='center'>"+listOfVal[p].cab+"</td><td align='center'>"+listOfVal[p].dis+"&nbsp;&nbsp;"+distanceBasedOn+"</td><td align='center'><input type='radio' name='allocatedDr' id='allocatedDr' value='"+listOfVal[p].dr+"' onclick='allocateDriverFromSuggestion("+driverOrCab+","+jobNo.substring(0,jobNo.length-1)+","+trip+","+'1'+")'><input type='radio' name='forceAllocatedDr' id='forceAllocatedDr' value='"+listOfVal[p].dr+"' onclick='allocateDriverFromSuggestion("+driverOrCab+","+jobNo.substring(0,jobNo.length-1)+","+trip+","+'2'+")'></td></tr>";
			}
			var index = "row";
			var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
			var iRowCount= oRows.length;
			edZone ="";
			var cabJob="";
			var lat2="";
			var lon2 ="";
			var driverOrCab ="";
			var drCab ="";
			var statusDriver ="";
			for(var k = 1; k < iRowCount; k++) {
				if(oRows[k].id==index){
					edZone= oRows[k].getElementsByTagName("input")[15].value;
					if(stZone=edZone){
						driverJob = oRows[k].getElementsByTagName("input")[11].value;
						cabJob = oRows[k].getElementsByTagName("input")[20].value;
						lat2 =  oRows[k].getElementsByTagName("input")[4].value;
						lon2 = oRows[k].getElementsByTagName("input")[5].value;
						var d = distance(lat1,lon1,lat2,lon2,distanceBasedOn);
						if(driverJob!=null&&driverJob!=""){
							var oRowsDr = document.getElementById("driverDetailDisplay").getElementsByTagName("td");
							var iRowCountDr = oRowsDr.length;
							for(var m =0;m<iRowCountDr;m++){
								if(disp==2){
									driverOrCab = cabJob;
									drCab = oRowsDr[m].getElementsByTagName("input")[3].value;
								}else{
									driverOrCab = driverJob;
									drCab = oRowsDr[m].getElementsByTagName("input")[0].value;
								}
								if(driverOrCab==drCab){
									statusDriver = oRowsDr[m].getElementsByTagName("input")[4].value;
									break;
	
								}
							}
							if(statusDriver!="L"){
	
								clrZones.push({
									"dr" : driverJob,
									"cab" :cabJob,
									"dis":d,
									"avl" : statusDriver
								})
							}
						}
					}
				}
			}
			clrZones.sort(function(a,b) { return parseFloat(a.dis) - parseFloat(b.dis) });
			var driverOrCab ="";

			for(var l=0;l<clrZones.length;l++){
				var drColor ="";
				var vhColor ="";
				disp = document.getElementById("dispatch").value;

				if(clrZones[l].avl=="Y"){
					drColor = "available"; 
				}else if(clrZones[l].avl=="N"){
					color = "unavailable";
				}else{
					color = "noFeed";
				}
				if(disp==2){
					driverOrCab = clrZones[l].cab;
					vhColor =color;
				}else{
					driverOrCab = clrZones[l].dr;
					drColor = color;
				}
				document.getElementById("sortedID").innerHTML +="<tr><td id="+drColor+" align='center'>"+clrZones[l].dr+"</td><td  id="+vhColor+" align='center'>"+clrZones[l].cab+"</td><td align='center'>"+clrZones[l].dis+"&nbsp;&nbsp;"+distanceBasedOn+"</td><td align='center'><input type='radio' name='allocatedDr' id='allocatedDr' value='"+clrZones[l].dr+"' onclick='allocateDriverFromSuggestion("+driverOrCab+","+jobNo.substring(0,jobNo.length-1)+","+trip+","+'1'+")'><input type='radio' name='forceAllocatedDr' id='forceAllocatedDr' value='"+clrZones[l].dr+"' onclick='allocateDriverFromSuggestion("+driverOrCab+","+jobNo.substring(0,jobNo.length-1)+","+trip+","+'2'+")'></td></tr>";
			}
			break;
		}

		case "changeFleet": {
			$("#changeFleet").show();
			document.getElementById("tripForanother").value = trip;
			break;
		}
		case "edit": {
			Dash = "Dashboard";
			openOR1(trip, Dash);
			break;
		}
		case "cancel": {
			changeJobStatus("cancel", trip);
			break;
		}
		case "endZone": {
			document.getElementById("tripIdForEndZone").value = trip;
			$("#setEndZone").show();
			break;
		}
		case "noShow": {
			changeJobStatus("noShow", trip);
			break;
		}
		case "couldntService": {
			changeJobStatus("couldntService", trip);
			break;
		}
		case "driverCompleted": {
			changeJobStatus("driverCompleted", trip);
			break;
		}
		case "onBoard": {
			changeJobStatus("onBoard", trip);
			break;
		}
		case "onRoute": {
			changeJobStatus("onRoute", trip);
			break;
		}
		case "onSite": {
			changeJobStatus("onSite", trip);
			break;
		}
		case "endTrip": {
			changeJobStatus("endTrip", trip);
			break;
		}
		case "reDispatch": {
			changeJobStatus("reDispatch", trip);
			break;
		}
		case "broadCast": {
			changeJobStatus("broadCast", trip);
			break;
		}
		case "reStart": {
			url = 'DashBoard?event=jobCommands&command=reStart&tripId=' + trip
			+ '&driverId=' + driver;
			var ajax = new AjaxInteraction(url, jobDetails1);
			ajax.doGet();
			break;
		}
		case "jobOnHold": {
			changeJobStatus("jobOnHold", trip);
			break;
		}
		case "addWithSR": {
			addWithExistingRoute(trip);
			break;
		}
		case "printThisJob": {
			window.open("ReportController?event=reportEvent&ReportName=jobMail&output=pdf&tripId="+trip+"&type=basedMail&assoccode="+document.getElementById("assoccodeMain").value+"&timeOffset="+document.getElementById("timeZoneMain").value);
			break;
		}
		case "jobHistory": {
			jobHistoryDetails(phone);
			$("#jobHistoryDiv").show("slow");
			break;
		}
		case "jobLogs": {
			jobHistoryLogs(trip, jobNo);
			$("#jobLogsDiv").show("slow");
			break;
		}
		case "sendMsgToDriver": {
			document.getElementById("sendTripId").value = trip;
			document.getElementById("sendDriver").value = driver;
			document.getElementById("sendRiderName").value = riderName;
			if (driver != null && driver != "") {
				$("#sendMsg").show();
			} else {
				alert("Please Allocate the Driver and send the Message");
			}
			// changeJobStatus("driverCompleted",trip);
			break;
		}
		}
	} else {
		switch (key) {
		case "driverCompleted": {
			changeJobStatus("driverCompleted", trip);
			break;
		}
		case "jobHistory": {
			jobHistoryDetails(phone);
			$("#jobHistoryDiv").show("slow");
			break;
		}
		case "jobLogs": {
			jobHistoryLogs(trip, jobNo);
			$("#jobLogsDiv").show("slow");
			break;
		}
		case "sendMsgToDriver": {
			document.getElementById("sendTripId").value = trip;
			document.getElementById("sendDriver").value = driver;
			document.getElementById("sendRiderName").value = riderName;
			if (driver != null && driver != "") {
				$("#sendMsg").show();
			} else {
				alert("Please Allocate the Driver and send the Message");
			}
			// changeJobStatus("driverCompleted",trip);
			break;
		}
		default:
			alert("Oops!!, This is a Flag Trip,You can't Change !!!");
		}
	}
}
function addWithExistingRoute(tripId){
	$("#addRoutePopup").show();
	document.getElementById("temporaryTripForRoute").value=tripId;
}
function removeWithExistingRoute(){
	$("#addRoutePopup").hide();
	document.getElementById("temporaryTripForRoute").value="";
}
function addNewTripWithRoute(){
	var tripId=document.getElementById("temporaryTripForRoute").value;
	var routeNo=document.getElementById("routeFromPopup").value;
	var pickUpOrder=document.getElementById("pickUpFromPopup").value;
	var dropOffOrder=document.getElementById("dropOffFromPopup").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=addTripToGroup&tripId='+tripId+'&routeNo='+routeNo+'&pOrder='+pickUpOrder+'&dOrder='+dropOffOrder;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(Number(text)>0){
		$("#addRoutePopup").hide();
	} else {
		alert("Route not added.Check your pick up & drop off order");
	}
}

function contextMenuWork1(action, driver, latForDriver, longForDriver, vehicle,	switchvalue) {
	switch (action) {
	case "delete": {
		logoutDriver(driver);
		mapPlotDriver();
		break;
	}
	case "deactivate": {
		document.getElementById("deactivateDriver").value = driver;
		changeHappeningZone();
		$("#deactivate").show();
		document.getElementById("changeHappeningDriver").value = true;
		break;
	}
	case "activate": {
		//alert("UnderConstruction"+driver);

		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=activateDriver&driverId=' +driver;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text =xmlhttp.responseText;
		if(text=='Success'){
			alert("Re-Activated Successfully");
		}else{
			alert("Re-Activated Failed");
		}
		getAllValuesForDashboard('01000');
		break;
	}
	case "driverAvailability": {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=setDriverAvailability&driverId=' + driver
		+ '&cabNo=' + vehicle + '&status=' + switchvalue;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		getAllValuesForDashboard('01000');
		break;
	}
	case "flagTrip": {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=flagTripFromDash&driverId=' + driver
		+ '&cabNo=' + vehicle;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		getAllValuesForDashboard('10000');
		break;
	}
	case "onRoute": {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=onRouteFromDash&driverId=' + driver
		+ '&cabNo=' + vehicle;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		getAllValuesForDashboard('10000');
		break;
	}
	case "jobHistoryMenu": {
		$("#jobsOnThatDay").show();
		jobsForCurrentDay(driver);
		break;
	}	
	case "jobDetails": {
		document.getElementById("ORJobHistory").innerHTML="";
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'RegistrationAjax?event=getHistoryJobs&value='+document.getElementById("value").value+'&fromDate='+document.getElementById("fromDate").value+'&toDate='+document.getElementById("toDate").value+'&driverid='+driver;

		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text!=""){
			var jsonObj = "{\"address\":"+text+"}";
			var objectNew = JSON.parse(jsonObj.toString());
			var $tbl = $('<table style="width:100%;border:1px;">').attr('id', 'historyValues');
			$tbl.append($('<tr>').append(
					$('<th>').text("TripId"),
					$('<th>').text("Trip Date"),
					$('<th>').text("Name"),
					$('<th>').text("Driver ID"),
					$('<th>').text("Vehicle No"),
					$('<th>').text("Phone"),
					$('<th>').text("Start Address"),
					$('<th>').text("End Address"),
					$('<th>').text("Trip Status"),
					$('<th>').text("Amount"),
					$('<th>').text("Comments")
			)
			);
			for(var i = 0; i < objectNew.address.length; i++){
				$tbl.append($('<input/>').attr('type', 'hidden').attr('name', 'tripId').attr('id','tripId'+i).val(objectNew.address[i].TI));
				$tbl.append($('<tr>').append(
						$('<td style="width:5%" align="center">').text(objectNew.address[i].TI),
						$('<td style="width:10%" align="center">').text(objectNew.address[i].TD),
						$('<td style="width:5%" align="center">').text(objectNew.address[i].NA),
						$('<td style="width:5%" align="center">').text(objectNew.address[i].DR),
						$('<td style="width:3%" align="center">').text(objectNew.address[i].VN),
						$('<td style="width:7%" align="center">').text(objectNew.address[i].PH),
						$('<td style="width:18%" align="center">').text(objectNew.address[i].SA+""+objectNew.address[i].SA2+""+objectNew.address[i].SC),
						$('<td style="width:18%" align="center">').text(objectNew.address[i].EA+""+objectNew.address[i].EA2+""+objectNew.address[i].EC),
						$('<td style="width:12%" align="center">').text(objectNew.address[i].TS),
						$('<td style="width:3%" align="center">').text(document.getElementById("currecyPrefix").value+""+objectNew.address[i].AM),
						$('<td style="width:3%" align="center">').text(document.getElementById("currecyPrefix").value+""+objectNew.address[i].MP),
						$('<td style="width:5%" align="center">').text(objectNew.address[i].FL)
				)
				);

			}
			$("#ORJobHistory").append($tbl);
			//$("#ORJobHistory").show("slow");
		}
		$("#ORJobHistory").jqm();
		$("#ORJobHistory").jqmShow();
		break;
	}

	case "sendMsgToDriver": {
		$("#sendSms").jqm();
		$("#sendSms").jqmShow();
//		$("sendSms").show("slow");
		var basedOn = document.getElementById("dispatchBasedOn").value;
		if (basedOn == "1") {
			document.getElementById("driver_idSMS").value = driver;
		} else {
			var oRows = document.getElementById("driverDetailDisplay")
			.getElementsByTagName("td");
			var iRowCount = oRows.length;
			for ( var j = 0; j < iRowCount; j++) {
				driver_id = oRows[j].getElementsByTagName("input")[0].value;
				if (driver_id == driver) {
					cab = oRows[j].getElementsByTagName("input")[3].value;
					continue;
				}

			}

			document.getElementById("cab_idSMS").value = cab;
		}
		break;
	}
	case "sendVMsgToDriver": {
		document.getElementById("vMessageContent").innerHTML = "<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\"230\" height=\"160\" id=\"voiceMessageID\">"
			+ "<object type='application/x-shockwave-flash' data='js/f211.swf' width='390' height='260'>"
			+ "<PARAM NAME=FlashVars VALUE='runtimer=false&driverID="
			+ driver
			+ "'/><param name='wmode' value='transparent'/></object></object></div>";
		$("#vMessagePopUp").show();
		$("#bullHorn").hide();
		break;
	}
	case "resetDriverApp":{
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=resetDriverApp&driverId=' + driver
		+ '&cabNo=' + vehicle ;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		//getAllValuesForDashboard('01000');
		break;
	}


	case "approve":{
		var x=prompt("Enter a Cab Number to Tag",+vehicle);
		var vehicleNumber="";
		if(x!=null){
			if(x!=""){
				vehicleNumber=x;
			}else{
				vehicleNumber=vehicle;
				alert("Default Cab Number Taken");
			}
			var xmlhttp = null;
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			url = '/TDS/DashBoard?event=approveCabNumberToDevice&driverId=' + driver+ '&cabNo=' + vehicleNumber ;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text =xmlhttp.responseText;
			//alert("text"+text);
			if(text==0){
				alert("You Doesn't have this access");
			}else{
				alert("Tagged device to cab");
			}
			//getAllValuesForDashboard('01000');

		}else{
			alert("Cab Number Approve Cancelled");
		}
		break;
	}

	case "zoneLogin": {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'mobilerequest?event=driverLocation&latitude=' + latForDriver
		+ '&longitude=' + longForDriver;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var resp = text.split(";");
		$("#bookToZone").show();
		if (resp[1] != "" && resp[1] != null) {
			var zoneDes = resp[1].split("=");
			document.getElementById("zoneDescription").innerHTML = "Zone Description: "
				+ zoneDes[1];
		}
		if (resp[0] != "" && resp[0] != null) {
			var zoneKey = resp[0].split("=");
			document.getElementById("zoneName").innerHTML = "Zone Name:"
				+ zoneKey[1];
		}
		document.getElementById("driverZone").innerHTML = driver;
		break;
	}
	case "zoneActivity": {
		document.getElementById("drZoneActivity").innerHTML="";
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'RegistrationAjax?event=drZoneActivity&drNum=' + driver;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text!=""){
			var jsonObj = "{\"address\":"+text+"}";
			var objectNew = JSON.parse(jsonObj.toString());
			var $tbl = $('<table style="width:100%;border:1px;">').attr('id', 'activityValues');
			$tbl.append($('<tr>').append(
					$('<th>').text("Zone Id"),
					$('<th>').text("Driver"),
					$('<th>').text("Vehicle"),
					$('<th>').text("Login Time"),
					$('<th>').text("Logout Time"),
					$('<th>').text("Reason")
			)
			);
			for(var i = 0; i < objectNew.address.length; i++){
				var statusLo = "Present";
				if(objectNew.address[i].S == "2"){
					statusLo = objectNew.address[i].Lo;
				}
				$tbl.append($('<tr>').append(
						$('<td style="width:5%" align="center">').text(objectNew.address[i].Z),
						$('<td style="width:10%" align="center">').text(objectNew.address[i].D),
						$('<td style="width:5%" align="center">').text(objectNew.address[i].C),
						$('<td style="width:5%" align="center">').text(objectNew.address[i].L),
						$('<td style="width:3%" align="center">').text(objectNew.address[i].Lo),
						$('<td style="width:7%" align="center">').text(objectNew.address[i].R)
				)
				);
			}
			$("#drZoneActivity").append($tbl);
		}
		$("#drZoneActivity").jqm();
		$("#drZoneActivity").jqmShow();
		break;
	}
	}
}
function contextMenuWorkForShared(action,routeNo) {
	switch (action) {
	case "shBroadcast":
	{
		changeSharedJobStatus("30",routeNo);
		break;
	}case "shComplete":
	{
		changeSharedJobStatus("61",routeNo);
		break;
	}case "shDelete":
	{
		deleteGroups(routeNo);
		break;
	}
	}
} 
function contextMenuWork2(action, zoneDriver) {
	switch (action) {
	case "delete":
	{
		removeFromQ(zoneDriver);
		break;
	}
	case "move":
	{
		changeFromQ(zoneDriver);
		break;
	}
	}
}
function ChangeFleetToAnother() {
	var fleetNo = document.getElementById("fleetV").value;
	var tripid = document.getElementById("tripForanother").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = '/TDS/DashBoard?event=changeJobToAnotherFleet&tripId=' + tripid
	+ '&fleetNo=' + fleetNo;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		alert("Job Send to to another Fleet");
	}
	$("#changeFleet").hide();
}
function createCookieForOnloadScreens(){
}
function createCookieForJobs(){
	if($("#GAC-D-opnReq").val()!=null){
		$("#GAC-D-opnReq").val("1");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-opnReq").attr('id',"GAC-D-opnReq").val("1"));
	}

}
function createCookieForDrivers(){
	if($("#GAC-D-cabDetails").val()!=null){
		$("#GAC-D-cabDetails").val("1");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-cabDetails").attr('id',"GAC-D-cabDetails").val("1"));
	}
}
function createCookieForZones(){
	if($("#GAC-D-zneWait").val()!=null){
		$("#GAC-D-zneWait").val("1");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-zneWait").attr('id',"GAC-D-zneWait").val("1"));
	}
}

function createCookieForZoneRates(){
	if($("#GAC-D-zneRate").val()!=null){
		$("#GAC-D-zneRate").val("1");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-zneRate").attr('id',"GAC-D-zneRate").val("1"));
	}
}

function createCookieForOptions(){
	if($("#GAC-D-options").val()!=null){
		$("#GAC-D-options").val("1");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-options").attr('id',"GAC-D-options").val("1"));
	}
}
function createCookieForStatistics(){
	if($("#GAC-D-statistics").val()!=null){
		$("#GAC-D-statistics").val("1");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-statistics").attr('id',"GAC-D-statistics").val("1"));
	}
}
function createCookieForMsg(){
	if($("#GAC-D-txtMsg").val()!=null){
		$("#GAC-D-txtMsg").val("1");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-txtMsg").attr('id',"GAC-D-txtMsg").val("1"));
	}
}
/*function createCookiesFleet(){
	if($("#GAC-G-fcolor").val()!=null){
		$("#GAC-G-fcolor").val($('#fcolor').val());
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-G-fcolor").attr('id',"GAC-G-fcolor").val($('#fcolor').val()));
	}
	}*/

function createCookieForReserve(){
	if($("#GAC-D-reserve").val()!=null){
		$("#GAC-D-reserve").val("1");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-reserve").attr('id',"GAC-D-reserve").val("1"));
	}
}
function removeCookieForJobs(){
	if($("#GAC-D-opnReq").val()!=null){
		$("#GAC-D-opnReq").val("0");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-opnReq").attr('id',"GAC-D-opnReq").val("0"));
	}
}
function removeCookieForDrivers(){
	if($("#GAC-D-cabDetails").val()!=null){
		$("#GAC-D-cabDetails").val("0");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-cabDetails").attr('id',"GAC-D-cabDetails").val("0"));
	}
}
function removeCookieForZones(){
	if($("#GAC-D-zneWait").val()!=null){
		$("#GAC-D-zneWait").val("0");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-zneWait").attr('id',"GAC-D-zneWait").val("0"));
	}
}

function removeCookieForZoneRate(){
	if($("#GAC-D-zneRate").val()!=null){
		$("#GAC-D-zneRate").val("0");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-zneRate").attr('id',"GAC-D-zneRate").val("0"));
	}
}

function removeCookieForOptions(){
	if($("#GAC-D-options").val()!=null){
		$("#GAC-D-options").val("0");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-options").attr('id',"GAC-D-options").val("0"));
	}
}
function removeCookieForStatistics(){
	if($("#GAC-D-statistics").val()!=null){
		$("#GAC-D-statistics").val("0");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-statistics").attr('id',"GAC-D-statistics").val("0"));
	}
}
function removeCookieForMsg(){
	if($("#GAC-D-txtMsg").val()!=null){
		$("#GAC-D-txtMsg").val("0");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-txtMsg").attr('id',"GAC-D-txtMsg").val("0"));
	}
}
function removeCookieForReserve(){
	if($("#GAC-D-reserve").val()!=null){
		$("#GAC-D-reserve").val("0");
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-reserve").attr('id',"GAC-D-reserve").val("0"));
	}
}

function showAdvancedOptions(){
	if($('#advancedOptions').is(':visible')){
		$("#advancedOptions").fadeOut(500);
		document.getElementById("more").innerHTML="+ More";
	}else{
		$("#advancedOptions").fadeIn(500);
		document.getElementById("more").innerHTML="- Less";
	}
}

//Shared Ride Functions Are completed
function showInstructions(){
	$('#instructions').bPopup({
		fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
		followSpeed: 1500, //can be a string ('slow'/'fast') or int
		modalColor: 'grey'
	});
}
function createCookieForColors(){

	if($("#GAC-D-C-vipColor").val()!=null){
		$("#GAC-D-C-vipColor").val($('#vipColor').val());
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-C-vipColor").attr('id',"GAC-D-C-vipColor").val($('#vipColor').val()));
	}
	if($("#GAC-D-C-timeColor").val()!=null){
		$("#GAC-D-C-timeColor").val($('#timeColor').val());
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-C-timeColor").attr('id',"GAC-D-C-timeColor").val($('#timeColor').val()));
	}
	if($("#GAC-D-C-voucherColor").val()!=null){
		$("#GAC-D-C-voucherColor").val($('#voucherColor').val());
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-C-voucherColor").attr('id',"GAC-D-C-voucherColor").val($('#voucherColor').val()));
	}
	if($("#GAC-D-C-cashColor").val()!=null){
		$("#GAC-D-C-cashColor").val($('#cashColor').val());
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-C-cashColor").attr('id',"GAC-D-C-cashColor").val($('#cashColor').val()));
	}
	if($("#GAC-D-C-avColor").val()!=null){
		$("#GAC-D-C-avColor").val($('#avColor').val());
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-C-avColor").attr('id',"GAC-D-C-avColor").val($('#avColor').val()));
	}
	if($("#GAC-D-C-noUpdate").val()!=null){
		$("#GAC-D-C-noUpdate").val($('#noUpdate').val());
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-C-noUpdate").attr('id',"GAC-D-C-noUpdate").val($('#noUpdate').val()));
	}
	if($("#GAC-D-C-navColor").val()!=null){
		$("#GAC-D-C-navColor").val($('#navColor').val());
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-C-navColor").attr('id',"GAC-D-C-navColor").val($('#navColor').val()));
	}
}
function submitOfSharedJobs(){
	var tripIdWithOrder = "";
	var routeNoList = "";
	var cabNo ="";
	var driver ="";
	var driverIdValue = document.getElementById("driverIdForShared").value;
	var dateValue =document.getElementById("dateForShared").value;             
	var oRows1=document.getElementById("driverDetailDisplay").getElementsByTagName("td");
	var iRowCount1= oRows1.length;
	var dispatchBasedOn=document.getElementById("dispatch").value;
	var indPayCheck= "0";
	if(document.getElementById("indpay").checked==true){
		indPayCheck = "1";
	}
	if(dispatchBasedOn=="2"){
		for(var i=0;i<iRowCount1;i++){
			cabValue=oRows1[i].getElementsByTagName("input")[5].value;
			if(cabValue==driverIdValue){
				driver =oRows1[i].getElementsByTagName("input")[0].value;
				cabNo = document.getElementById("driverIdForShared").value;
				break;
			}
		}
	}else{

		if(driverIdValue!=""){
			for(var i=0;i<iRowCount1;i++){
				driverId=oRows1[i].getElementsByTagName("input")[0].value;
				if(driverId==driverIdValue){
					cabNo =oRows1[i].getElementsByTagName("input")[5].value;
					driver =document.getElementById("driverIdForShared").value;
					break;
				}
			}
		}
	}
	var totalEx = document.getElementById("customerTableShared").getElementsByTagName("tr");
	var exCount = totalEx.length;
	for(var p=1;p<exCount-2;p++){
		if(totalEx[p].getElementsByTagName("input")[0].checked==true){
			var routeNo = totalEx[p].getElementsByTagName("input")[3].value;
			if(routeNo!=null&&routeNo!="0"&&routeNo!=""){
				routeNoList+=routeNo+",";
			}
		}
	}
	var totalSelect = document.getElementById("orderPopUpSub").getElementsByTagName("tr");
	var selectcount = totalSelect.length;
	var sameOrder =  document.getElementById("sameOrder").checked;
	var	pickupSw ="";
	if(sameOrder){
		for(var k=0;k<selectcount;k++){
			var tripID = totalSelect[k].getElementsByTagName("td")[0].innerHTML;
			if(tripID.substring(0,1)=="P"){
				pickupSw = 0;
			}else{
				pickupSw = 1;
			}
			var orderNo = k+1;
			tripID = tripID.substring(2,tripID.lenght);
			tripIdWithOrder+=tripID+";"+orderNo+";"+pickupSw+",";
		}
		if(driver==""||cabNo==""){
			driver ="";
			cabNo ="";
		}
		url ='DashBoard?event=sharedRide&tripId='+tripIdWithOrder+'&routeNo='+routeNoList+'&driverId='+driver+'&cabNo='+cabNo+'&dateValue='+dateValue+'&indPay='+indPayCheck;
		var ajax = new AjaxInteraction(url, sharedRideResponse);
		ajax.doGet();
		var table = document.getElementById("customerTableShared").getElementsByTagName("tr");
		var rowCount = table.length;
		for(var i=1;i<rowCount-2;i++){
			table[i].getElementsByTagName("input")[0].checked = false;
		}
		totalOptions=1;
		document.getElementById("orderPopUpSub").innerHTML="";
	}
	else{
		for(var k=0;k<selectcount;k++){
			var tripID = totalSelect[k].getElementsByTagName("td")[0].innerHTML;
			var orderNo = totalSelect[k].getElementsByTagName("select")[0].value;
			if(orderNo!="0"){
				if(tripID.substring(0,1)=="P"){
					pickupSw = 0;
				}else{
					pickupSw = 1;
				}

				//var orderNo = k+1;
				tripID = tripID.substring(2,tripID.lenght);
				tripIdWithOrder+=tripID+";"+orderNo+";"+pickupSw+",";
			}else{
				alert("Please Arrange all the trips");
				tripIdWithOrder="";
				break;
			}
		}
		if(tripIdWithOrder!=""){
			if(driver==""||cabNo==""){
				driver ="";
				cabNo ="";
			}
			url ='DashBoard?event=sharedRide&tripId='+tripIdWithOrder+'&routeNo='+routeNoList+'&driverId='+driver+'&cabNo='+cabNo+'&dateValue='+dateValue;
			var ajax = new AjaxInteraction(url, sharedRideResponse);
			ajax.doGet();
			var table = document.getElementById("customerTableShared").getElementsByTagName("tr");
			var rowCount = table.length;
			for(var i=1;i<rowCount-2;i++){
				table[i].getElementsByTagName("input")[0].checked = false;
			}
			totalOptions = 1;
			document.getElementById("orderPopUpSub").innerHTML="";
		}
	}
	document.getElementById("refShared").value = true;
	document.getElementById("changeHappeningSharedRide").value="false";
}

function showListOfJobs(i,tripId,stZone,edZone){
	document.getElementById("changeHappeningSharedRide").value="true";
	var check = document.getElementById("markSh"+i).checked;
	var table = document.getElementById("orderPopUpSub");
	var row="";
	if(check){
		document.getElementById("refShared").value = false;
		if(totalOptions<3){
			var sameOrderText = document.createTextNode("Same\u00A0\u00A0\Order");
			var nbsp = document.createTextNode("\u00A0\u00A0");
			var sameOrder = document.createElement("input");
			sameOrder.type="checkbox";
			sameOrder.id="sameOrder";
			sameOrder.checked = "checked";
			table.appendChild(sameOrderText);
			table.appendChild(sameOrder);
		}
		totalOptions = totalOptions+2;
		row = document.createElement("tr");
		row.id="rowId"+i;
		var cell = document.createElement("td");
		cell.id="tripValue"+i;
		var v = document.createTextNode("P-"+tripId);
		cell.appendChild(v);


		var cellStart = document.createElement("td");

		var combo = document.createElement("select");
		var option = document.createElement("option");
		option.text = "select";
		option.value = "0";
		combo.appendChild(option);
		combo.setAttribute('onclick','disableCheckbox()');
		cellStart.appendChild(combo);

		var cellSZone = document.createElement("td");
		cellSZone.id="startZone"+i;
		var v = document.createTextNode(stZone);
		cellSZone.appendChild(v);

		rowE = document.createElement("tr");
		rowE.id="rowIdE"+i;
		var cellE = document.createElement("td");
		cellE.id="tripEnd"+i;
		var vE = document.createTextNode("D-"+tripId);
		cellE.appendChild(vE);

		var cellEnd = document.createElement("td");
		var combo = document.createElement("select");
		var option = document.createElement("option");
		option.text = "select";
		option.value = "0";
		combo.appendChild(option);
		combo.setAttribute('onclick','disableCheckbox()');
		cellEnd.appendChild(combo);


		for(var j=1;j<totalOptions;j=j+2){
			var option = document.createElement("option");
			option.text = j;
			option.value = j;
			combo.appendChild(option);
			var option = document.createElement("option");
			option.text = j+1;
			option.value = j+1;
			combo.appendChild(option);

		}
		cellStart.appendChild(combo);
		cellEnd.appendChild(combo);

		var cellEZone = document.createElement("td");
		cellEZone.id="endZone"+i;
		var v = document.createTextNode(edZone);
		cellEZone.appendChild(v);

		row.appendChild(cell);
		row.appendChild(cellStart);
		row.appendChild(cellSZone);
		rowE.appendChild(cellE);
		rowE.appendChild(cellEnd);
		rowE.appendChild(cellEZone);
		table.appendChild(row);
		table.appendChild(rowE);
		var totalSelect = document.getElementById("orderPopUpSub").getElementsByTagName("tr");
		var selectcount = totalSelect.length ;

		for(var k=0;k<selectcount;k++){
			totalSelect[k].getElementsByTagName("select")[0].innerHTML = combo.innerHTML;
			totalSelect[k].getElementsByTagName("select")[0].value = k+1;
		}

	}else{
		var allRows = document.getElementById("customerTableShared").getElementsByTagName("tr");
		var countRows = allRows.length;
		document.getElementById("refShared").value = true;
		for(var p=1;p<countRows;p++){
			if(allRows[p].getElementsByTagName("input")[0].checked){
				document.getElementById("refShared").value = false;
			}
		}
		totalOptions = totalOptions-2;
		var combo = document.createElement("select");
		var option = document.createElement("option");
		option.text = "select";
		option.value = "0";
		combo.appendChild(option);
		for(var j=1;j<totalOptions;j=j+2){
			var option = document.createElement("option");
			option.text = j;
			option.value = j;
			combo.appendChild(option);
			var option = document.createElement("option");
			option.text = j+1;
			option.value = j+1;
			combo.appendChild(option);
		}
		$("#rowId"+i).remove();
		$("#rowIdE"+i).remove();
		var totalSelect = document.getElementById("orderPopUpSub").getElementsByTagName("tr");
		var selectcount = totalSelect.length;
		for(var k=0;k<selectcount;k++){
			totalSelect[k].getElementsByTagName("select")[0].innerHTML = combo.innerHTML;
			totalSelect[k].getElementsByTagName("select")[0].value = k+1;
		}
		if(totalOptions<2){
			document.getElementById("orderPopUpSub").innerHTML = "";
			totalOptions=1;
		}
	}
}

function removeListOfJobs(){
	$("#showListOfJobSForGroup").hide("slow");
}
function showSharedRide(){
	$("#sharedRidePopUp").show("slow");
	if(totalOptions>1){
		document.getElementById("orderPopUpSub").innerHTML="";
		totalOptions=1;
	}
	getAllValuesForDashboard('00010');
	//getSharedJobs();
}
function removeSharedJobs(){
	$("#sharedRidePopUp").hide("slow");
	var table = document.getElementById("customerTableShared").getElementsByTagName("tr");
	var rowCount = table.length;
	for(var i=1;i<rowCount-2;i++){
		table[i].getElementsByTagName("input")[0].checked = false;
	}
	document.getElementById("orderPopUpSub").innerHTML="";
	document.getElementById("changeHappeningSharedRide").value="false";
	totalOptions=1;
}
function showSharedSummary(){
	document.getElementById("unloadSharedDetails").src="images/Dashboard/loading_icon.gif";
	url = '/TDS/DashBoard?event=getSharedSummary&date='+document.getElementById("datePickerForSh").value;
	var ajax = new AjaxInteraction(url, responseDetails);
	ajax.doGet();

}
function showSummory(){ 
	$("#sharedDetailsPopUp").show("slow");
	//showSharedSummary();
	getAllValuesForDashboard('00001');
}
function responseDetails(responseText) {
	document.getElementById("sharedDetailsPopUp").innerHTML = responseText;
	document.getElementById("unloadSharedDetails").src="images/Dashboard/refresh1.png";
	$("#sharedDetailsPopUp").ready(function(){
	});
}

function changeSharedJobStatus(status,routeNo){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url ='DashBoard?event=updateSharedStatus&routeNo='+routeNo+'&status='+status;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="1"){
		showSharedSummary();
	}else{
		alert("update Failed");
	}
}
function removeSharedDetails(){
	$("#sharedDetailsPopUp").hide("slow");
}
function showOrHideJobs(i,routeNo){
	var img=document.getElementById("imgReplace1_"+i).src;
	if(img.indexOf("plus.png")!= -1){
		document.getElementById("imgReplace1_"+i).src="images/Dashboard/Minus.png";
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url ='DashBoard?event=sharedRideAjax&routeNo='+routeNo;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("ajaxShared"+i).innerHTML =text;
		$("#ajaxShared"+i).show('slow');

	}else{

		document.getElementById("imgReplace1_"+i).src="images/Dashboard/plus.png";
		document.getElementById("ajaxShared"+i).innerHTML ="";
		$("#ajaxShared"+i).hide('slow');
	}
}
function allocateDriverforShared(k,routeNo,oldDriver){
	var cabNo = "";
	var drivervalue = document.getElementById("driverForShare"+k).value.replace("F","");
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	//datePickerForSh
	if(drivervalue!=""){
		var oRows1=document.getElementById("driverDetailDisplay").getElementsByTagName("td");
		var iRowCount1= oRows1.length;
		var dispatchBasedOn=document.getElementById("dispatch").value;
		if(dispatchBasedOn=="2"){
			for(var i=0;i<iRowCount1;i++){
				cabValue=oRows1[i].getElementsByTagName("input")[5].value;
				if(cabValue!="" && cabValue==drivervalue){
					driver =oRows1[i].getElementsByTagName("input")[0].value;
					cabNo = document.getElementById("driverForShare"+k).value;
					if(isNaN(cabNo.substring(0))){
						driver="F"+driver;
						cabNo=cabNo.substring(1);
//						cabNo=cabNo.substring(2);
					}
					break;
				}
			}
		}else{
			if(drivervalue!=""){
				for(var i=0;i<iRowCount1;i++){
					driverId=oRows1[i].getElementsByTagName("input")[0].value;
					if(driverId==drivervalue){
						cabNo =oRows1[i].getElementsByTagName("input")[5].value;
						driver = document.getElementById("driverForShare"+k).value;
						break;
					}
				}
			}
		}
	} else {
		driver="";
		cabNo="";
	}
	var date=document.getElementById("datePickerForSh").value;
	document.getElementById("changeHappeningSharedRide").value="false";
	if(oldDriver!=document.getElementById("driverForShare"+k).value ||  (document.getElementById("driverForShare"+k).value.length=="0" && oldDriver!="1110000")){
		url ='DashBoard?event=updateDriverForShared&routeNo='+routeNo+'&driverId='+driver+'&cabNo='+cabNo+'&date='+date;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("sharedDetailsPopUp").innerHTML = text;
		document.getElementById("datePickerForSh").value=date;
		$("#sharedDetailsPopUp").ready(function(){
			getAllValuesForDashboard('00001');
		});
	}
	//getAllValuesForDashboard('00001');
}

function getSharedJobs() {
	if(document.getElementById("refShared").value !="true"){
		return;
	}
	var date ="";
	if(document.getElementById("dateForShared").value!=null){
		date =  document.getElementById("dateForShared").value;
	}
	document.getElementById("unloadShared").src="images/Dashboard/loading_icon.gif";
	var url="";
	url ='DashBoard?event=getSharedRide&status=0&date='+date;
	var ajax = new AjaxInteraction(url, sharedRideResponse);
	ajax.doGet();
}
function sharedRideResponse(responseText) {
	document.getElementById("sharedRidePopUp").innerHTML = responseText;
	document.getElementById("unloadShared").src="images/Dashboard/refresh1.png";
}
function deleteGroups(routeNo){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url ='DashBoard?event=deleteGroup&routeNo='+routeNo;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	document.getElementById("sharedDetailsPopUp").innerHTML = text;
	getSharedJobs();
	$("#sharedDetailsPopUp").ready(function(){
	});
}
function showVoiceMessageScreen(){
	$("#bullHorn").show();
}
function messageHistoryShow(m){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(m=="1"){
		var url = 'DashBoard?event=messageHistory';
	}else{
		var lastKey = document.getElementById("lastKey").value;
		var url = 'DashBoard?event=messageHistory&update=1&lastKey='+lastKey;
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;
	//alert("txt:"+text);
	obj = "{\"msg\":"+text+"}" ;
	
	var jsonObj = JSON.parse(obj.toString());
	if(lastKey==null){
		document.getElementById("MessageHistory").innerHTML="";
		var $tbl = $('<table>').attr('id', 'messageHistoryTable1');
		/*$tbl.append(
			$('<center>').append(
			$('<h3>').text("Message History")));*/
		$tbl.append($('<tr>').append(
				$('<th>').text("Date"),
				$('<th>').text("Time"),
				$('<th>').text("Sent By"),
				$('<th>').text("Driver ID"),
				$('<th>').text("Message")
		)
		);
		for(var i=0;i<jsonObj.msg.length;i++){
			$tbl.css("white-space","pre-line");
			$tbl.append($('<tr>').append(
					$('<td>').text(jsonObj.msg[i].date),
					$('<td>').text(jsonObj.msg[i].time),
					$('<td>').text(jsonObj.msg[i].sender),
					$('<td>').text(jsonObj.msg[i].dri),
					$('<td >').html(jsonObj.msg[i].msgText)));		
		}
		if(jsonObj.msg.length>0){
			document.getElementById("lastKey").value=jsonObj.msg[jsonObj.msg.length-1].key;
		}
		$("#MessageHistory").append($tbl);
	}else{
		for(var i=0;i<jsonObj.msg.length;i++){
			var $tbl = $("#messageHistoryTable1");
			$tbl.css("white-space","pre-line");
			$tbl.append($('<tr>').append(
					$('<td>').text(jsonObj.msg[i].date),
					$('<td>').text(jsonObj.msg[i].time),
					$('<td>').text(jsonObj.msg[i].sender),
					$('<td>').text(jsonObj.msg[i].dri),
					$('<td>').text(jsonObj.msg[i].msgText.replace("/&#32;/g"," "))
			));	
		}
		if(jsonObj.msg.length>0){
			document.getElementById("lastKey").value=jsonObj.msg[jsonObj.msg.length-1].key;
		}
		var delTable=document.getElementById("messageHistoryTable1").getElementsByTagName("tr");
		var rowCount=delTable.length;
		var delTableInt =parseInt(rowCount);
		if(delTableInt>10){
			for(var i=0;i<jsonObj.msg.length;i++){
				document.getElementById("messageHistoryTable1").deleteRow(i+1);
				//$("#messageHistoryTable1").remove(delTable[i].getElementsByTagName("tr"));
			}
		}
	}
	var textarea = document.getElementById('MessageHistory');
	textarea.scrollTop = textarea.scrollHeight
}

function removeMessageStatus(){
	document.getElementById("msgResponse").innerHTML="";
}
function jobHistoryDetails(phoneNo){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'DashBoard?event=jobHistory&phoneNo='+phoneNo;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;
	obj = "{\"jobs\":"+text+"}" ;
	var jsonObj = JSON.parse(obj.toString());
	document.getElementById("jobhistoryPopUp").innerHTML="";
	var $tbl = $('<table>').attr('id', 'jobHistoryTable1');
	$tbl.append($('<tr>').append(
			$('<th>').text("Ph.NO"),
			$('<th>').text("P.Name"),
			$('<th>').text("TripId"),
			$('<th>').text("DriverID"),
			$('<th>').text("SAddress"),
			$('<th>').text("Date"),
			$('<th>').text("Time"),
			/*$('<th>').text("Sadd2"),
				$('<th>').text("Scity"),*/
			$('<th>').text("SLandMark"),
			$('<th>').text("Q.NO"),
			$('<th>').text("EAdd"),
			$('<th>').text("DropTime"),
			/*$('<th>').text("Eadd2"),
				$('<th>').text("Ecity"),*/
			$('<th>').text("ELandmark"),
			$('<th>').text("Dr.Profile"),
			$('<th>').text("Instructions")
	)
	);
	for(var i=0;i<jsonObj.jobs.length;i++){
		$tbl.css("white-space","pre-line");
		$tbl.append($('<tr>').append(
				$('<td>').text(jsonObj.jobs[i].phone),
				$('<td>').text(jsonObj.jobs[i].name),
				$('<td>').text(jsonObj.jobs[i].tripId),
				$('<td>').text(jsonObj.jobs[i].driverId),
				$('<td>').text(jsonObj.jobs[i].sadd1+""+jsonObj.jobs[i].sadd2+""+jsonObj.jobs[i].scity),
				$('<td >').text(jsonObj.jobs[i].date),
				$('<td>').text(jsonObj.jobs[i].time),
				$('<td>').text(jsonObj.jobs[i].land),
				$('<td>').text(jsonObj.jobs[i].queueNo),
				$('<td>').text(jsonObj.jobs[i].eadd1+""+jsonObj.jobs[i].eadd2+""+jsonObj.jobs[i].ecity),
				$('<td>').text(jsonObj.jobs[i].drop),
				/*$('<td>').text(jsonObj.jobs[i].eadd2),
					$('<td>').text(jsonObj.jobs[i].ecity),*/
				$('<td>').text(jsonObj.jobs[i].eland),
				$('<td>').text(jsonObj.jobs[i].drProf),
				$('<td>').text(jsonObj.jobs[i].sspl)));
	}
	$("#jobhistoryPopUp").append($tbl);
}
function removeJobsPopUp(){
	$("#jobHistoryDiv").hide("slow");
}
function jobHistoryLogs(trip,jobNo){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=logsDetail&tripId='+trip;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;
	document.getElementById("jobLogsPopUp").innerHTML="";
	$("#jobLogsPopUp").append("               "+jobNo);
//	$("#jobLogsPopUp").append("Trip ID:"+trip);
	$("#jobLogsPopUp").append(text);

}
function jobreservationLogs(trip,jobNo){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=logsDetail&tripId='+trip+'&number='+jobNo;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;

	$("#logsReservation_"+jobNo).show('slow');
	document.getElementById("rJobLogsPopUp_"+jobNo).innerHTML="";
	$("#rJobLogsPopUp_"+jobNo).append(text);
	$("#hideJobLogs_"+jobNo).show();
	$("#jobLogs_"+jobNo).hide();
}

function removeJobsLogsPopUp(){
	$("#jobLogsDiv").hide("slow");
}
//SharedRide Functions Are Starting
function createIndividualTrips(){
	var tripIdW="";
	var totalSelect = document.getElementById("orderPopUpSub").getElementsByTagName("tr");
	var selectcount = totalSelect.length;
	var sameOrder =  document.getElementById("sameOrder").checked;
	var dateValue =document.getElementById("dateForShared").value; 
	if(sameOrder){
		for(var k=0;k<selectcount;k++){
			var tripID = totalSelect[k].getElementsByTagName("td")[0].innerHTML;
			tripIdW+=tripID + ',';
		}
		url ='DashBoard?event=createIndividualTrips&tripId='+tripIdW+'&dateValue='+dateValue;
		var ajax = new AjaxInteraction(url, sharedRideResponse);
		ajax.doGet();
		var table = document.getElementById("customerTableShared").getElementsByTagName("tr");
		var rowCount = table.length;
		for(var i=1;i<rowCount-2;i++){
			table[i].getElementsByTagName("input")[0].checked = false;
		}
		totalOptions=1;
		document.getElementById("orderPopUpSub").innerHTML="";
	}
}
function logintToZone(){
	var zoneName="";
	var currentZone=false;
	currentZone = document.getElementById("selectCurrentZone").checked;
	var selectZone =document.getElementById("zoneList").value;
	if(currentZone!=false&&document.getElementById("zoneName").innerHTML!=null&&document.getElementById("zoneName").innerHTML!=""){
		zoneName =document.getElementById("zoneName").innerHTML; 
	}else if(selectZone!=null&&selectZone!=""&&currentZone!=true){
		zoneName = selectZone;
	}
	if(zoneName!=null&&zoneName!=""){
		var driver = document.getElementById("driverZone").innerHTML;
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'DashBoard?event=bookToZone&zoneName='+zoneName+'&driverZone='+driver;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text =xmlhttp.responseText;
		//if(text==1){
		$("#bookToZone").hide();
		//}
	}else{
		alert("No Current Zone");
		document.getElementById("selectCurrentZone").checked=false;
	}
}		 function rempoveLogintToZone(){
	$("#bookToZone").hide();
}function rempoveLogintToZone(){
	$("#bookToZone").hide();
}
function checkCabOrDriverDash(){
	var driver="";
	var cab="";
	if(document.getElementById("driverORcabDash").value=="Driver"){
		driver=document.getElementById("driverDashOR").value;
	} else {
		cab=document.getElementById("driverDashOR").value;
	}
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(driver!="" || cab!=""){
		var url = 'RegistrationAjax';
		var parameters='event=checkDriverOrCab&driverId='+driver+'&cabNo='+cab;
		xmlhttp.open("POST",url, false);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("Content-length", parameters.length);
		xmlhttp.setRequestHeader("Connection", "close");
		xmlhttp.send(parameters);
		var text = xmlhttp.responseText;
		var resp=text.split("###");
		var result=resp[0].split(";");
		document.getElementById("Error").innerHTML=result[1];
		document.getElementById("Error").style.display="block";
	}
}
function sendvMsgToSelectedDrivers(){
	var oRows=document.getElementById("driverExpand").getElementsByTagName("tr");
	var iRowCount= oRows.length;
	var  driverID=[];
	for(var i=1,k=0;i<iRowCount-1;i++){
		if(document.getElementById("selectDriver"+i).checked==true){
			driverID[k]=document.getElementById("selectDriver"+i).value+";";
			k++;
		}
	}
	var driverValues="";
	for(var j=0;j<driverID.length;j++){
		if(j<driverID.length-1){
			driverValues +=driverID[j];
		}else{
			var temp=driverID[j].split(";");
			driverValues =driverValues+temp[0];
		}
	}
	document.getElementById("vMessageContent").innerHTML="<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" width=\"230\" height=\"160\" id=\"voiceMessageID\">"
		+"<object type='application/x-shockwave-flash' data='js/f211.swf' width='390' height='260'>"
		+"<PARAM NAME=FlashVars VALUE='runtimer=false&driverID="+driverValues+"'/><param name='wmode' value='transparent'></object></object></div>";
	$("#vMessagePopUp").show();
	$("#bullHorn").hide();
	//document.getElementById("driver_idSMS").value=driverID;
	//$("#sendSms").show();
}
function hideFullDetails(){
	$('#dialog').jqmHide();
}
function showFullDetails(i){
	var tripId = document.getElementById("tripid"+i).value;
	document.getElementById("individualTable").innerHTML = "";
//	if (window.XMLHttpRequest)
//	{
//	xmlhttp = new XMLHttpRequest();
//	} else {
//	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//	}
//	var url = 'OpenRequestAjax?event=getOpenRequest&tripId='+tripId;
//	xmlhttp.open("GET", url, false);
//	xmlhttp.send(null);
//	var jsonObj ="";
	var obj =globalAllValues.address[0];
//	var text = xmlhttp.responseText;
//	jsonObj = "{\"address\":"+text+"}" ;
//	obj = JSON.parse(jsonObj.toString());
	var root =document.getElementById("dialog");
	var table = document.getElementById("individualTable");
	var row="";
	var rowCount="";
	var cell="";
	rowCount = table.rows.length;
	row = table.insertRow(rowCount);
	cell = row.insertCell(0);
	var cw1 = document.createElement("colDet");
	var stw1= document.createTextNode("x");
	cw1.appendChild(stw1);
	cell.style.color="red" ;
	//cell.setAttribute('color', 'red');
	cell.setAttribute('onClick', 'hideFullDetails()');
	cell.appendChild(cw1);
	table.appendChild(cell);
	for(var j=0;j<obj.jobsList.length;j++){
		if(tripId==obj.jobsList[j].T){
			cell = row.insertCell(0);
			cell.className ="label";
			var nam1 =document.createTextNode("Phone:");
			var c1 = document.createElement("a");
			var st1= document.createTextNode(obj.jobsList[j].P);
			c1.appendChild(st1);
			cell.appendChild(nam1);
			cell.appendChild(c1);
			var b1 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam2 =document.createTextNode("Name:");
			var c2 = document.createElement("a");
			var st2= document.createTextNode(obj.jobsList[j].N);
			c2.appendChild(st2);
			cell.appendChild(b1);
			cell.appendChild(nam2);
			cell.appendChild(c2);
			var b2 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam10 =document.createTextNode("Date:");
			var c10 = document.createElement("a");
			var st10= document.createTextNode(obj.jobsList[j].SD);
			c10.appendChild(st10);
			cell.appendChild(b2);
			cell.appendChild(nam10);
			cell.appendChild(c10);
			var b10 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam11 =document.createTextNode("Time:");
			var c11 = document.createElement("a");
			var st11= document.createTextNode(obj.jobsList[j].ST);
			c11.appendChild(st11);
			cell.appendChild(b10);
			cell.appendChild(nam11);
			cell.appendChild(c11);

			var b9 =document.createElement("br");
			var nam3 =document.createTextNode("P/U jobsList:");
			var c3 = document.createElement("a");
			var st3= document.createTextNode(obj.jobsList[j].A+" "+obj.jobsList[j].SCTY);
			c3.appendChild(st3);
			cell.appendChild(b9);
			cell.appendChild(nam3);
			cell.appendChild(c3);
			var b15 =document.createElement("br");
			var nam15 =document.createTextNode("D/O Address:");
			var c15 = document.createElement("a");
			var st15= document.createTextNode(obj.jobsList[j].DA+" "+obj.jobsList[j].ECTY);
			c15.appendChild(st15);
			cell.appendChild(b15);
			cell.appendChild(nam15);
			cell.appendChild(c15);
			var b21 =document.createElement("br");
			var nam22 =document.createTextNode("Pay Type:");
			var c31 = document.createElement("a");
			var st31= document.createTextNode(obj.jobsList[j].AC);
			c31.appendChild(st31);
			cell.appendChild(b21);
			cell.appendChild(nam22);
			cell.appendChild(c31);
			var b22 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam23 =document.createTextNode("Account:");
			var c41 = document.createElement("a");
			var st41= document.createTextNode(obj.jobsList[j].VC);
			c41.appendChild(st41);
			cell.appendChild(b22);
			cell.appendChild(nam23);
			cell.appendChild(c41);

			var b23 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam24 =document.createTextNode("Zone:");
			var c51 = document.createElement("a");
			var st51= document.createTextNode(obj.jobsList[j].Q);
			c51.appendChild(st51);
			cell.appendChild(b23);
			cell.appendChild(nam24);
			cell.appendChild(c51);

			var b3131 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam3131 =document.createTextNode("Fleet:");
			var c3131 = document.createElement("a");
			var st3131= document.createTextNode(obj.jobsList[j].FL);
			c3131.appendChild(st3131);
			cell.appendChild(b3131);
			cell.appendChild(nam3131);
			cell.appendChild(c3131);

			var b24 =document.createElement("br");
			var nam26 =document.createTextNode("Driver:");
			var c71 = document.createElement("a");
			var st71= document.createTextNode(obj.jobsList[j].D);
			c71.appendChild(st71);
			cell.appendChild(b24);
			cell.appendChild(nam26);
			cell.appendChild(c71);
			var b13 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam14 =document.createTextNode("Trip ID:");
			var c14 = document.createElement("a");
			var st14= document.createTextNode(obj.jobsList[j].T);
			c14.appendChild(st14);
			cell.appendChild(b13);
			cell.appendChild(nam14);
			cell.appendChild(c14);
			var b111 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam25 =document.createTextNode("LandMark:");
			var c61 = document.createElement("a");
			var st61= document.createTextNode(obj.jobsList[j].LMN);
			c61.appendChild(st61);
			cell.appendChild(b111);
			cell.appendChild(nam25);
			cell.appendChild(c61);

			var b12 =document.createElement("br");				
			var nam12 =document.createTextNode("Dispatch Comments:");
			var c12 = document.createElement("a");
			var st12= document.createTextNode(obj.jobsList[j].CMT);
			c12.appendChild(st12);
			cell.appendChild(b12);
			cell.appendChild(nam12);
			cell.appendChild(c12);
			var b26 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam13 =document.createTextNode("Cab Comments:");
			var c13 = document.createElement("a");
			var st13= document.createTextNode(obj.jobsList[j].SPL);
			c13.appendChild(st13);
			cell.appendChild(b26);
			cell.appendChild(nam13);
			cell.appendChild(c13);
			var b116 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam113 =document.createTextNode("Cab No:");
			var c113 = document.createElement("a");
			var st113= document.createTextNode(obj.jobsList[j].V);
			c113.appendChild(st113);
			cell.appendChild(b116);
			cell.appendChild(nam113);
			cell.appendChild(c113);

			var b316 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			var nam313 =document.createTextNode("Special Flags:");
			var c313 = document.createElement("a");
			var st313= document.createTextNode(obj.jobsList[j].DP);
			c313.appendChild(st313);
			cell.appendChild(b316);
			cell.appendChild(nam313);
			cell.appendChild(c313);

			//		var b421 =document.createTextNode('\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0');
			//		var nam421 =document.createTextNode("Call Driver");
			//		var c421 = document.createElement("button");
			//		c421.onclick=function(){callDriverForStatus(tripId);};
			//		var st421= document.createTextNode("CBD");
			//		c421.appendChild(st421);
			//		cell.appendChild(b421);
			//		cell.appendChild(nam421);
			//		cell.appendChild(c421);


			var b216 =document.createElement("br");
			var nam213 =document.createTextNode("Status:");
			var c213 = document.createElement("a");
			var st213="";
			if(obj.jobsList[j].S=="30"){
				st213= document.createTextNode("Broadcast");
			}else if(obj.jobsList[j].S=="40"){
				st213= document.createTextNode("Allocated");
			}else if(obj.jobsList[j].S=="43"){
				st213= document.createTextNode("OnRoute To Pickup");
			}else if(obj.jobsList[j].S=="49"){
				st213= document.createTextNode("Soon To Clear");
			}else if(obj.jobsList[j].S=="50"){
				st213= document.createTextNode("Customer Canceled Request");
			}else if(obj.jobsList[j].S=="51"){
				st213= document.createTextNode("No Show");
			}else if(obj.jobsList[j].S=="4"){
				if(obj.jobsList[j].dontDispatch=="1"){
					st213= document.createTextNode("Dont Dispatch");	
				}else{
					st213= document.createTextNode("Dispatching");
				}
			}else if(obj.jobsList[j].S=="0"){
				st213= document.createTextNode("Unknown");
			}else if(obj.jobsList[j].S=="47"){
				st213= document.createTextNode("Trip Started");
			}else if(obj.jobsList[j].S=="5"){
				st213= document.createTextNode("Manual Allocation");
			}else if(obj.jobsList[j].S=="99"){
				st213= document.createTextNode("Trip On Hold");
			}else if(obj.jobsList[j].S=="3"){
				st213= document.createTextNode("Couldn't Find Drivers");
			}else if(obj.jobsList[j].S=="48"){
				st213= document.createTextNode("Driver Reported No Show");
			}else if(obj.jobsList[j].S=="55"){
				st213= document.createTextNode("Operator Reported No Show");
			}else if(obj.jobsList[j].S=="52"){
				st213= document.createTextNode("Company Couldn't Service");
			}else if(obj.jobsList[j].S=="45"){
				st213= document.createTextNode("Driver Onsite");
			}else if(obj.jobsList[j].S=="90"){
				st213= document.createTextNode("SrHold Job");
			}else{
				st213= document.createTextNode(" Status Not Found!");
			}
			c213.appendChild(st213);
			cell.appendChild(b216);
			cell.appendChild(nam213);
			cell.appendChild(c213);

			var b456 =document.createTextNode("\u00A0\u00A0\u00A0\u00A0\u00A0\u00A0");
			var nam453 =document.createTextNode("Flight Info:");
			var c453 = document.createElement("a");
			if(obj.jobsList[j].AIR!=""){
				var st453= document.createTextNode(obj.jobsList[j].AIR);
			} else {
				var st453=document.createTextNode("");
			}
			c453.appendChild(st453);
			cell.appendChild(b456);
			cell.appendChild(nam453);
			cell.appendChild(c453);
			break;
		}
	}
	table.appendChild(cell);
	root.appendChild(table);
	$('#dialog').jqmShow();
}

var tripIdForStatus="";
var secsStatus="",TimerIDStatus;

function callDriverForStatus(tripId){
	if(secsStatus=="0" || secsStatus==""){
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'OpenRequestAjax?event=callDriver&tripId='+tripId;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		document.getElementById("ErrorStatus").innerHTML="";
		StopTimerStatus();
		tripIdForStatus=tripId;
	} else {
		alert("Your previous request is processing.Plz wait");
	}
}
function StopTimerStatus()
{
	secsStatus= "20";
	clearTimeout(TimerIDStatus);
	StartTimerStatus();
}
function StartTimerStatus()
{
	if(secsStatus!=""){
		secsStatus--;
		document.getElementById('ErrorStatus').innerHTML = "The Status Of Trip "+tripIdForStatus+" Will Come Here In "+secsStatus+" Secs";
		document.getElementById("ErrorStatus").style.display="block";
		TimerIDStatus=self.setTimeout("StartTimerStatus()",1000);
		if(secsStatus==0)
		{
			getJobStatusFromDriver();
		}
	}
}
function getJobStatusFromDriver(){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=readJobStatus&tripId='+tripIdForStatus;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var resp=text.split("###");
	var result=resp[0];
	if(result==""){
		result="Driver didnt reply/Internet failure error";
	}
	secsStatus="";
	document.getElementById("ErrorStatus").innerHTML="";
	$("#ErrorStatus").append(resp[0]+'<a style="margin-left:60px" href="#"><img alt="" src="images/Dashboard/close.png" onclick="removeError()"></a>');
	document.getElementById("ErrorStatus").style.display="block";
}



function showDialog(){
	$('#dialog').jqm();
}
function removeBullHorn(){
	$("#bullHorn").hide();
}
function removeMessageToIndividual(){
	$("#sendMsg").hide();
	document.getElementById("message").value="";
	document.getElementById("sendTripId").value="";
	document.getElementById("sendDriver").value="";
	document.getElementById("sendRiderName").value="";
	document.getElementById("sendVoiceMsgToDriver").checked=false;
}
function removeMsgToNearestDrivers(){
	$("#callDrivers").hide();
	document.getElementById("distance").value="";
	document.getElementById("reasonForMessage").value="";
	document.getElementById("sendVoiceMsgToNearestDriver").checked=false;
}
function jobsForCurrentDay(driver){
	var message_to_show = ' Please wait.. Loading job history for Driver:'+driver;
	document.getElementById("wait_show").innerHTML = message_to_show;
	$("#wait_show").show();
	document.getElementById("jobsForToday").innerHTML = "";
	
	var url = 'DashBoard?event=jobsForCurrentDay&driverId='+driver;
	var ajax = new AjaxInteraction_moreTime(url, jobsForDay);
	ajax.doGet();
}

function jobsForDay(responseText){
	var obj = "{\"jobs\":"+responseText+"}";
	if(document.getElementById("jobsForToday").innerHTML!=null){
		document.getElementById("jobsForToday").innerHTML = "";
	}
	
	var jsonObj = JSON.parse(obj.toString());
	//var $tbh = $('<table>').attr('id', 'jobsForDayHeader');
	var $tbl = $('<table>').attr('id', 'jobsForDayContent');
	$tbl.append($('<tr>').append(
			$('<th>').text("Driver ID"),
			$('<th>').text("Vehicle No"),
			$('<th>').text("Name"),
			$('<th>').text("Trip ID"),
			$('<th>').text("Queue No"),
			$('<th>').text("Flag"),
			$('<th>').text("Address"),
			$('<th>').text("Status"),
			$('<th>').text("Service Time"),
			$('<th>').text("Date"))
	);
	for(var i=0;i<jsonObj.jobs.length;i++){
		var status="";
		if(jsonObj.jobs[i].Stat=="30"){status="Broadcast";
		}else if(jsonObj.jobs[i].Stat=="40"){status="Allocated";
		}else if(jsonObj.jobs[i].Stat=="43"){status="On Rotue To Pickup";
		}else if(jsonObj.jobs[i].Stat=="50"){status="Customer Cancelled Request";
		}else if(jsonObj.jobs[i].Stat=="51"){status="No Show";
		}else if(jsonObj.jobs[i].Stat=="4"){status="Dispatching";
		}else if(jsonObj.jobs[i].Stat=="0"){status="Unknown";
		}else if(jsonObj.jobs[i].Stat=="47"){status="Trip Started";
		}else if(jsonObj.jobs[i].Stat=="99"){status="Trip On Hold";
		}else if(jsonObj.jobs[i].Stat=="3"){status="Couldnt Find Drivers";
		}else if(jsonObj.jobs[i].Stat=="61"){status="Trip Completed";
		}else if(jsonObj.jobs[i].Stat=="55"){status="operator Canceled Request";
		}else if(jsonObj.jobs[i].Stat=="52"){status="Company Couldn't Service";
		}else if(jsonObj.jobs[i].Stat=="50"){status="Customer Canceled Request";
		}else if(jsonObj.jobs[i].Stat=="48"){status="Driver Reporting NoShow";}

		$tbl.append($('<tr>').append(
				$('<td>').text(jsonObj.jobs[i].DrId),
				$('<td>').text(jsonObj.jobs[i].VehNo),
				$('<td>').text(jsonObj.jobs[i].Nam),
				$('<td>').text(jsonObj.jobs[i].Trip),
				$('<td>').text(jsonObj.jobs[i].Qno),
				$('<td>').text(jsonObj.jobs[i].Flag),
				$('<td>').text(jsonObj.jobs[i].Add1),
				$('<td>').text(status),
				$('<td>').text(jsonObj.jobs[i].SerTime),
				/*$('<td>').text(jsonObj.jobs[i].Lat),*/
				/*$('<td>').text(jsonObj.jobs[i].Long),*/
				$('<td>').text(jsonObj.jobs[i].SerDat))
		);
	}
	//$('#jobsForToday').append($tbh);
	$('#jobsForToday').append($tbl);
	
	document.getElementById("wait_show").innerHTML = "";
	$("#wait_show").hide();
}

function removeJobsForDay(){
	$("#jobsOnThatDay").hide();
}
function checkCount(){
	url = '/TDS/DashBoard?event=checkCount';
	var ajax = new AjaxInteraction(url, getCount);
	ajax.doGet();
}
function getCount(responseText){
	var table = document.getElementById("driverExpand").getElementsByTagName("tr");
	var rowCount =table.length;
	var obj = "{\"driver\":"+responseText+"}";
	var jsonObj = JSON.parse(obj.toString());
	for(var i=1;i<rowCount-1;i++){
		if(document.getElementById("driveridDetailsExpand"+i).innerHTML!=null){
			var driver = document.getElementById("driveridDetailsExpand"+i).innerHTML;
		}else{
			return;
		}
		for(var j=0;j<jsonObj.driver.length;j++){
			if(driver==jsonObj.driver[j].DrId){
				document.getElementById("countValue"+i).innerHTML=jsonObj.driver[j].totalJobs;
			}
		}
	}
}

function removeVMessageBullHorn(){
	$("#vMessagePopUp").hide();
	$("#bullHorn").show();
}
function sendMsgToSelectedDrivers(){
	var oRows=document.getElementById("driverExpand").getElementsByTagName("tr");
	var iRowCount= oRows.length;
	var  driverID=[];
	for(var i=1,k=0;i<iRowCount;i++){
		if(document.getElementById("selectDriver"+i).checked==true){
			driverID[k]=document.getElementById("selectDriver"+i).value+";";
			k++;
		}
	}
	var driverValues="";
	for(var j=0;j<driverID.length;j++){
		if(j<driverID.length-1){
			driverValues +=driverID[j];
		}else{
			var temp=driverID[j].split(";");
			driverValues =driverValues+temp[0];
		}
	}
	$("#sendSms").jqm();
	$("#sendSms").jqmShow();
//	$("#sendSms").show("slow");
	var dispatchBasedOn=document.getElementById("dispatch").value;
	if(dispatchBasedOn=="2"){
		document.getElementById("cab_idSMS").value=driverValues;
	}else{
		document.getElementById("driver_idSMS").value=driverValues;
	}
}


function createJobORDashboard(phone){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'DashBoard?event=jobHistory&phoneNo='+phone;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;
	obj = "{\"jobs\":"+text+"}" ;
	var jsonObj = JSON.parse(obj.toString());
	if(jsonObj.jobs.length>0){
		for(var i=0;i<jsonObj.jobs.length;i++){
			alert("This " +jsonObj.jobs[i].phone+"  has already job and Trip id is "+jsonObj.jobs[i].tripId);
		}
	}
	else{
		$('#ORDash').bPopup({
			modalClose: true,
			opacity: 0.2,
			follow: [false, false], //x, y
			position: [483,18] //x, y
		// positionStyle:'fixed' //'fixed' or 'absolute'
		});
		document.getElementById('phoneDash').focus();
		document.getElementById('phoneDash').value=phone;
	}
}


function createJobORDash(){
	$("#shrs option[value=NCH]").hide();
	$('#ORDash').jqm();
	$("#ORDash").jqmShow();
}

function removeSms(){
	var bPopup = $("#Msgnotification").bPopup();
	bPopup.close();
}


function calculateAndSendMsg(){
	$("#callDrivers").hide();
	var lattitude =document.getElementById("latForRightclick").value;
	var longitude =document.getElementById("longForRightclick").value;
	var distance= document.getElementById("distance").value;
	var msg =document.getElementById("reasonForMessage").value;
	var voiceMsg =document.getElementById("sendVoiceMsgToDriver").checked;
	if(msg!=""&&msg!=null){
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'DashBoard?event=sendToNearestDrivers&lat='+lattitude+'&longi='+longitude+'&distance='+distance+'&msg='+msg+'&vMsg='+voiceMsg;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
	}else{
		alert("please type some message");
		$("#callDrivers").show();
	}
	document.getElementById("distance").value="";
	document.getElementById("reasonForMessage").value="";
	document.getElementById("sendVoiceMsgToNearestDriver").checked=false;
	messageHistoryShow();
}
function removeCookieForDefault(){
	var defLat = document.getElementById("defaultLatitude").value;
	var defLong = document.getElementById("defaultLongitude").value;
	document.getElementById("latFromMap").value=defLat;
	document.getElementById("longFromMap").value=defLong;
	name="GAC-D-lat";
	name1="GAC-D-long";
	if($("#GAC-D-lat").val()!=null){
		$("#GAC-D-lat").val(defLat);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val(defLat));
	}
	if($("#GAC-D-long").val()!=null){
		$("#GAC-D-long").val(defLong);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val(defLong));
	}
	mapInitJobs();
	mapPlotDriver();
}
function changeJobStatus(command,trip){
	var r=confirm("Are You Sure");
	var all = document.getElementById("All").checked;
	var status=0;
	if(all==true){
		status=40;
	}else{
		status=0;
	}
	if (r==true)
	{
		//var answer = prompt("Reason ?","");
		//if(answer!=null&&answer!=""){
		url = 'DashBoard?event=jobCommands&command='+command+'&tripId='+trip+'&all='+all+'&status='+status;
		var ajax = new AjaxInteraction(url, deleteJob);
		ajax.doGet();
		//}
	}
}



function autoComplete() {
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'DashBoard?event=driverAutoComplete';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	data = text.split("^^^");
	$("#driver_id").autocomplete(data);
}
function dragend(){
	document.getElementById("dragend").value = "true";
	cancel();
}

function zoneDetails() {
	$("#queueid").slideToggle("slow");
}
function zoneDetails1(){
	$(".zoneDetails1").hover(function() {
		$(this).find("em1").animate({
			opacity : "show",
			left:"45"
		}, "slow");
	},  function() {
		$(this).find("em1").animate({
			opacity : "hide",
			left:"65"
		}, "fast");
	} );
}
function loginTime(){
	$(".label").hover(function() {
		$(this).find("em").animate({
			opacity : "show",
			left:"45"
		}, "slow");
	},  function() {
		$(this).find("em").animate({
			opacity : "hide",
			left:"65"
		}, "fast");
	} );
}
function details() {	
	$(".detail").hover(function() {
		$(this).find("em").animate({
			opacity : "show",
			left:"85"
		}, "slow");
	},  function() {
		$(this).find("em").animate({
			opacity : "hide",
			left:"75"
		}, "fast");
	} );
} 

function sendSmsforDrivers(){
	var msg=document.getElementById("message1").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=messagefordrivers&message='+msg;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	alert("Your Message has been sent successfully");
	$("#sendmsg").jqmHide();
	document.getElementById("message").value="";

}


function sendSmsToDriver(){
	var msg=document.getElementById("message").value;
	var trip=document.getElementById("sendTripId").value;
	var driverID = document.getElementById("sendDriver").value;
	var riderName =document.getElementById("sendRiderName").value;
	var vMsg=document.getElementById("sendVoiceMsgToDriver").checked;
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=sendSMS&tripId='+trip+'&driverId='+driverID+'&msg='+msg+'&riderName='+riderName+'&vMsg='+vMsg;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	//var text = xmlhttp.responseText;
	$("#sendMsg").hide();
	document.getElementById("message").value="";
	document.getElementById("sendTripId").value="";
	document.getElementById("sendDriver").value="";
	document.getElementById("sendRiderName").value="";
	document.getElementById("sendVoiceMsgToDriver").checked=false;
	// document.getElementById("message").innerHTMl="";
	messageHistoryShow();
}
function createCookieForCenter(){
	var center = map.getCenter();
	var lat=center.lat();
	var longv=center.lng();
	name="GAC-D-lat";
	name1="GAC-D-long";
	if($("#GAC-D-lat").val()!=null){
		$("#GAC-D-lat").val(lat);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val(lat));
	}
	if($("#GAC-D-long").val()!=null){
		$("#GAC-D-long").val(longv);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name1).attr('id',name1).val(longv));
	}
	value=document.getElementById("currentZoom").value;
	document.getElementById("zoom").value = value;
	if($("#GAC-D-zoomValue").val()!=null){
		$("#GAC-D-zoomValue").val(value);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', "GAC-D-zoomValue").attr('id',"GAC-D-zoomValue").val(value));
	}
}
function zoneRightArrow(){
	var img=document.getElementById("imgReplace2").src;
	if(img.indexOf("leftarrow.png")!= -1){
		autoRefreshOff();
		$("#right-block1").fadeOut("slow");
		$("#right-block15").fadeIn("slow");
		document.getElementById("imgReplace2").src="images/Dashboard/rightarrow.png";
	}else{
		$("#right-block15").fadeOut("slow");
		$("#right-block1").fadeIn("slow");
		document.getElementById("imgReplace2").src="images/Dashboard/leftarrow.png";
		Init();
	}
}
function deactivateDetail(){
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	$("#deactivate").hide();
	document.getElementById("changeHappeningDriver").value=false;
	var driver=document.getElementById("deactivateDriver").value;
	var hr=document.getElementById("hour").value;
	var answer="Deactivate Driver";
	//var answer = prompt("Reason for deactivate ?","");
	var ttime=document.getElementById("to_time").value;
	var ftime=document.getElementById("from_time").value;
	var fdate=document.getElementById("from_date").value;
	var tdate=document.getElementById("to_date").value;
	if(document.getElementById("hour").value!=null && document.getElementById("hour").value!=""){
		url = '/TDS/DashBoard?event=deactivate&driver='+driver+'&ttime='+ttime+"&ftime="+ftime+'&fdate='+fdate+'&tdate='+tdate+'&answer='+answer+'&hour='+hr;
	}  else if(document.getElementById("from_date").value!=null && document.getElementById("to_date").value!=null && document.getElementById("to_date").value!="" && document.getElementById("from_date").value!=""){
		url = '/TDS/DashBoard?event=deactivate&driver='+driver+'&ttime='+ttime+"&ftime="+ftime+'&fdate='+fdate+'&tdate='+tdate+'&answer='+answer+'&hour='+hr;
	} else {
		alert("OOPS!Hours or From and To Date are Mandatory..");
	}

	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	//if(res!=null){
	logoutDriver(driver);
	//}
}
var dontDispatch=0;
function suggestDriverSummary(rowValue,dispatchStatus){
	var xmlhttp = null;
	dontDispatch=dispatchStatus;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = 'RegistrationAjax?event=getDrivers&driver='+document.getElementById("driverId"+rowValue).value+'&position='+rowValue;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text=xmlhttp.responseText;
	var resp=text.split("###");
	document.getElementById("ORDashDriver").innerHTML=resp[1];
	document.getElementById("ORDashDriver").style.display="block";
	if(resp[0].indexOf('NumRows=2')<0){
		document.getElementById("ORDashDriver").style.display="none";
	} 
}
function removeDriverPopup(){
	$("#ORDashDriver").hide("slow");
}

function allocateDriverFromPopup(rowValue,type){
	var driver = document.getElementById("driverIdPopup"+rowValue).value;
	var position=document.getElementById("positionDriver").value;
	if(type==1){
		document.getElementById("driverId"+position).value="F"+driver;
	} else {
		document.getElementById("driverId"+position).value=driver;
	}
	allocateDriver(position, '', 3, '', '',dontDispatch);
	removeDriverPopup();
}
function allocateDriver(row,tripId,type,jobFleet,companyCode,dispatchStatus){
	if(jobFleet==companyCode){
		var driverId=document.getElementById("driverId"+row).value;
		var cabNumber= document.getElementById("vehicleNo"+row).value;
		if(driverId!="" || cabNumber!=""){
			if(dispatchStatus==0 || (dispatchStatus==1 &&(driverId.indexOf("F")>=0 || cabNumber.indexOf("F")>=0 || driverId.indexOf("f")>=0 || cabNumber.indexOf("f")>=0))){
				if(type=="2"){
					for(var i=0;i<globalDrivers.driverList.length;i++){
						if(globalDrivers.driverList[i].varVNo==cabNumber.replace("F", "").replace("f","")){
							driverId=globalDrivers.driverList[i].varDr;
							i=globalDrivers.driverList.length;
						} else {
							driverId="";
						}
					}
				} else {
					for(var i=0;i<globalDrivers.driverList.length;i++){
						if(globalDrivers.driverList[i].varDr==driverId.replace("F", "").replace("f","")){
							cabNumber=globalDrivers.driverList[i].varVNo;
							i=globalDrivers.driverList.length;
						} else {
							cabNumber="";
						}
					}
					if(type=="3"){
						tripId=document.getElementById("tripIdSummary"+row).value;
						jobFleet=document.getElementById("assoccodeSummary"+row).value;
						companyCode=document.getElementById("assoccodeMain"+row).value;
					} 
				}
				var xmlhttp=null;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				var url='RegistrationAjax?event=updateOpenRequestDriverId&driverId='+driverId+'&cabNumber='+cabNumber+'&tripId='+tripId;
				xmlhttp.open("GET",url,false);
				xmlhttp.send(null);
				var text=xmlhttp.responseText;
				var resp=text.split("###");
				if(resp[0]!="" && resp[0]!=0 && resp[0]!=-1){
					document.getElementById("editJob_"+row).focus();
					var cell = document.createTextNode(""+currentTimeCalculate()+":>The trip "+tripId+" is allocated to "+driverId+"  successfully");
					var cellBr = document.createElement("br");
					document.getElementById("console").appendChild(cellBr);
					document.getElementById("console").appendChild(cell);
					ORSummary(0);
				}else{
					ORSummary(0);
				}
			}else{
				document.getElementById("driverId"+row).value="";
				document.getElementById("vehicleNo"+row).value="";
				alert("Trip is in Dont Dispatch status.So please Force the job to driver");
			}
		} else {
			alert("Driver/Cab is not available");
		}
	}else{
		alert("You are not in the fleet to allocate this job");
	}
}

function basedOnDate(){
	$(".basedDate").show();
	$(".hourBased").hide();
	$(".dt").hide();
	$(".hr").show();
}
function removeDeactivate(){
	$("#deactivate").hide();
}
function basedOnHr(){
	$(".hourBased").show();
	$(".basedDate").hide();
	$(".hr").hide();
	$(".dt").show();
}
function check(){
	url = '/TDS/DashBoard?event=driverDetails&status='+4;
	var ajax = new AjaxInteraction(url, drivers);
	ajax.doGet();
}
function check1(){
	url = '/TDS/DashBoard?event=driverDetails&status='+3;
	var ajax = new AjaxInteraction(url, drivers);
	ajax.doGet();
}
function check2(){
	url = '/TDS/DashBoard?event=driverDetails&status='+1;
	var ajax = new AjaxInteraction(url, drivers);
	ajax.doGet();
}
function landmarkPending(responseText) {
	var resp = responseText.split('###');
	document.getElementById("bodypage").innerHTML = resp[0];

}
function createCookieForZoom() {
	name="GAC-D-zoomValue";
	value=document.getElementById("zoom").value;
	if($("#GAC-D-zoomValue").val()!=null){
		$("#GAC-D-zoomValue").val(value);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val(value));
	}
}
function createCookieForAutoRefresh(){
	name="GAC-D-autoRefreshTime";
	value=document.getElementById("sec1").value;
	if($("#GAC-D-autoRefreshTime").val()!=null){
		$("#GAC-D-autoRefreshTime").val(value);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val(value));
	}
}
function createCookieForIndividualZoom(){
	name="GAC-D-individualZoom";
	value=document.getElementById("individualZoom").value;
	if($("#GAC-D-individualZoom").val()!=null){
		$("#GAC-D-individualZoom").val(value);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val(value));
	}	
}
function createCookieForAutoRefreshOnOff(onOrOff){
	name="GAC-D-autoRefreshOn";
	value=onOrOff;
	if($("#GAC-D-autoRefreshOn").val()!=null){
		$("#GAC-D-autoRefreshOn").val(value);
	}else{
		$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val(value));
	}
}
function todayDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
	var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
	var hrsmin = hors+""+min;  

	document.getElementById("fDate").value =cMonth+"/"+cDate+"/"+cYear;
	document.getElementById("tDate").value =cMonth+"/"+cDate+"/"+cYear;
	document.getElementById("keyWord").value ="";

	//document.masterForm.shrs.value  = hrsmin; 
}
function jobCount(){
	url = 'DashBoard?event=jobCount';
	var ajax = new AjaxInteraction(url, jobCountConsole);
	ajax.doGet();
}
function jobCountConsole(responseText) {
	var resp=responseText.split(";");
	document.getElementById("statisticsConsole").innerHTML = resp[0]+"<br/>";
}
function removeStatistics(){
	$("#statistics").hide();
	$(".operationStatistics").css(cssObj);
}



function updateDriver1(i,trip,driver,routeNo){
	var driver1=document.getElementById("driver_id"+i).value;
	var all = document.getElementById("All").checked;
	var status=0;
	if(all==true){
		status=40;
	}else{
		status=0;
	}
	//var r=confirm("Are You Sure");
	//if (r==true)
	//  {string1.toUpperCase() === string2.toUpperCase()
	if(acdata==0 && acdata==""){

		if(driver1=="ns"||driver1=="NS"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="cc"||driver1=="CC"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="oc"||driver1=="OC"){
			url = '/TDS/DashBoard?event=deleteJob&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="dc"||driver1=="DC"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="rs"||driver1=="RS"){
			var driverIdValue = document.getElementById("driverIdForDetails"+i).value;
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&driverId='+driverIdValue+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="cs"||driver1=="CS"){
			var driverIdValue = document.getElementById("driverIdForDetails"+i).value;
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&driverId='+driverIdValue+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="rd"||driver1=="RD"||(driver1==""&&driver1!=driver)){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status+'&routeNo='+routeNo;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="H"||driver1=="h"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="OB"||driver1=="ob"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="OR"||driver1=="or"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="OS"||driver1=="os"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="ET"||driver1=="et"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="bc"||driver1=="BC"){
			url = '/TDS/DashBoard?event=jobCommands&command='+driver1+'&tripId='+trip+'&all='+all+'&status='+status;
			var ajax = new AjaxInteraction(url, deleteJob);
			ajax.doGet();
		}else if(driver1=="jl"||driver1=="JL"){
			jobHistoryLogs(trip, "");
			$("#jobLogsDiv").show("slow");
			getAllValuesForDashboard('10000');
		}else if(driver1!=driver){
			updateDriver(i,trip,driver,1);
		}
		document.getElementById("changeHappeningJob").value="false";
		$("#jobsTop").css('background-color','#81DAF5');
	}
	else{

		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		if(driver1=="ns"||driver1=="NS"){
			url = '/TDS/DashBoard?event=multiplejobCommands&command='+driver1+'&tripid='+acdata+'&all='+all+'&status='+status;
		}else if(driver1=="cc"||driver1=="CC"){
			url = '/TDS/DashBoard?event=multiplejobCommands&command='+driver1+'&tripid='+acdata+'&all='+all+'&status='+status;
		}else if(driver1=="oc"||driver1=="OC"){
			url = '/TDS/DashBoard?event=deleteMJob&tripid='+acdata+'&all='+all+'&status='+status;
		}else if(driver1=="dc"||driver1=="DC"){
			url = '/TDS/DashBoard?event=multiplejobCommands&command='+driver1+'&tripid='+acdata+'&all='+all+'&status='+status;
		}else if(driver1=="cs"||driver1=="CS"){
			var driverIdValue = document.getElementById("driverIdForDetails"+i).value;
			url = '/TDS/DashBoard?event=multiplejobCommands&command='+driver1+'&tripid='+acdata+'&driverId='+driverIdValue+'&all='+all+'&status='+status;
		}else if(driver1=="rd"||driver1=="RD"){
			url = '/TDS/DashBoard?event=redispatchMjob&command='+driver1+'&tripid='+acdata+'&all='+all+'&status='+status;
		}else if(driver1=="bc"||driver1=="BC"){
			url = '/TDS/DashBoard?event=multiplejobCommands&command='+driver1+'&tripid='+acdata;
		}
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text==1){
			acdata=0;
			createCookieForAutoRefreshOnOff('on');Init();
			$("#changeFleetjob").hide();
			document.getElementById("changeHappeningJob").value="false";
			$("#jobsTop").css('background-color','#81DAF5');
		}
		else{
			createCookieForAutoRefreshOnOff('on');Init();
			document.getElementById("changeHappeningJob").value="false";
			$("#jobsTop").css('background-color','#81DAF5');
			acdata=0;
		}
		document.getElementById("changeHappeningJob").value="false";
		$("#jobsTop").css('background-color','#81DAF5');

	}
}




function cmd(rows,trip){
	var id=document.getElementById("driver_id"+rows).value;
	url = '/TDS/DashBoard?event=jobCommands&command='+id+'&tripId='+trip;
	var ajax = new AjaxInteraction(url, delJob);
	ajax.doGet();
}
function delJob(responseText) {
	var resp = responseText.split('###');
	if(resp[0]==1){
	}else{
		alert('Trip is not deleted');
	}
}
function updateDriver2(i,trip,time,assocode){
	var pickupTime = document.getElementById("pickup"+i).value;
	if(pickupTime!=time){
		updateDriver3(i,trip,assocode,time);
	}
}
function timeSetup(){
	var time=document.getElementById("sec1").value;
	document.getElementById("sec").value = time;
	var cell = document.createTextNode(""+currentTimeCalculate()+":> New AutoRefresh Time is:"+time);
	var cellBr = document.createElement("br");
	document.getElementById("console").appendChild(cellBr);
	document.getElementById("console").appendChild(cell);
}
function zoomMap(){
	var zoom=document.getElementById("zoom").value;
	document.getElementById("zoomValue").value = zoom;
	var cell = document.createTextNode(""+currentTimeCalculate()+":> New Zoom Level is:"+zoom);
	var cellBr = document.createElement("br");
	document.getElementById("console").appendChild(cellBr);
	document.getElementById("console").appendChild(cell);
	//document.getElementById("console").innerHTML="New Zoom Level is:"+zoom;
}
function individualZoom(){
	var individualZoom = document.getElementById("individualZoom").value;
	document.getElementById("individualZoomValue").value = individualZoom;
	var cell = document.createTextNode(""+currentTimeCalculate()+":> New individual Zoom Level is:"+individualZoom);
	var cellBr = document.createElement("br");
	document.getElementById("console").appendChild(cellBr);
	document.getElementById("console").appendChild(cell);
	//document.getElementById("console").innerHTML="New individual Zoom Level is:"+individualZoom;
}
function currentTimeCalculate(){
	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var seconds = currentTime.getSeconds();
	var timeString ="";
	if (minutes < 10){
		minutes = "0" + minutes;
	}
	if (seconds < 10){
		seconds = "0" + seconds;
	}
	timeString = hours + ":" + minutes + ":" +seconds;
	if(hours > 11){
		timeString = timeString + " PM ";
	} else {
		timeString = timeString + " AM ";
	}
	return timeString;
}

function currentDetails(driverID,trip,lat,longitude, status){
	document.getElementById("currentJobinJObs").value="";
	document.getElementById("currentDriverinJObs").value="";
	document.getElementById("currentDriverinDrivers").value="";
	document.getElementById("dragend").value="false";
	$("#cancelButton").show();
	document.getElementById("currentDriverinJObs").value=driverID;

	if(status=='47'){
		document.getElementById("currentDriverinDrivers").value= driverID;
	} else {
		document.getElementById("currentJobinJObs").value=trip;

	}
	document.getElementById("latJobs").value=lat;
	document.getElementById("longJobs").value=longitude;
	document.getElementById("singlejobClicked").clicked = true;
	mapInitJobs();
}
function currentDriver(driver,lat,longitude){
	document.getElementById("currentJobinJObs").value="";
	document.getElementById("currentDriverinJObs").value="";
	document.getElementById("currentDriverinDrivers").value="";
	document.getElementById("dragend").value="false";
	$("#cancelButton").show();
	if(document.getElementById("currentJobinJObs").value!=null&&document.getElementById("currentJobinJObs").value!=""){
		document.getElementById("currentDriverinJObs").value="";
		document.getElementById("currentJobinJObs").value="";
		document.getElementById("latJobs").value="";
		document.getElementById("longJobs").value="";
	}
	document.getElementById("currentDriverinDrivers").value=driver;
	document.getElementById("latDriver").value=lat;
	document.getElementById("longDriver").value=longitude;
	mapPlotDriver('0');
}
function cancel(){
	$("#cancelButton").show();
}
function jobRightArrow(){
	var img=document.getElementById("imgReplace").src;
	if(img.indexOf("rightarrow.png")!= -1){
		autoRefreshOff();
		$("#plain").fadeOut("slow");
		$("#left-block5").fadeIn("slow");
		document.getElementById("imgReplace").src="images/Dashboard/leftarrow.png";
	}
	else{
		$("#left-block5").fadeOut("slow");
		$("#plain").fadeIn("slow");
		document.getElementById("imgReplace").src="images/Dashboard/rightarrow.png";
		Init();
	}
}
function driverRightArrow(){
	document.getElementById("driverShowCheck").value = "li";
	//driverDetails('lo');
	getAllValuesForDashboard('01000');
	var img=document.getElementById("imgReplace1").src;
	if(img.indexOf("leftarrow.png")!= -1){
		autoRefreshOff();
		$("#right-block").fadeOut("slow");
		$("#right-block5").fadeIn("slow");
		document.getElementById("imgReplace1").src="images/Dashboard/rightarrow.png";
		tabs = function(options) {

			var defaults = {  
					selector: '.tabs',
					selectedClass: 'selected'
			};  

			if(typeof options == 'string') defaults.selector = options;
			var options = $.extend(defaults, options); 

			return $(options.selector).each(function(){

				var obj = this;	
				var targets = Array();

				function show(i){
					$.each(targets,function(index,value){
						$(value).hide();
					});
					$(targets[i]).fadeIn('fast');
					$(obj).children().removeClass(options.selectedClass);
					selected = $(obj).children().get(i);
					$(selected).addClass(options.selectedClass);
				};

				$('a',this).each(function(i){	
					targets.push($(this).attr('href'));
					$(this).click(function(e){
						e.preventDefault();
						show(i);
					});
				});

				show(0);

			});	
		}
		tabs('#tabDiv nav ul');
	}
	else{
		$("#right-block5").fadeOut("slow");
		$("#right-block").fadeIn("slow");
		document.getElementById("imgReplace1").src="images/Dashboard/leftarrow.png";
		//Init();
		tabs = function(options) {

			var defaults = {  
					selector: '.tabs',
					selectedClass: 'selected'
			};  

			if(typeof options == 'string') defaults.selector = options;
			var options = $.extend(defaults, options); 

			return $(options.selector).each(function(){

				var obj = this;	
				var targets = Array();

				function show(i){
					$.each(targets,function(index,value){
						$(value).hide();
					});
					$(targets[i]).fadeIn('fast');
					$(obj).children().removeClass(options.selectedClass);
					selected = $(obj).children().get(i);
					$(selected).addClass(options.selectedClass);
				};

				$('a',this).each(function(i){	
					targets.push($(this).attr('href'));
					$(this).click(function(e){
						e.preventDefault();
						show(i);
					});
				});

				show(0);

			});	
		}
		// initialize the function
		// as a parameter we are sending a selector. For this particular script we must select the unordered (or ordered) list item element 
		tabs('#tabs nav ul');
		Init();
	}
}
function showMoreAddress(){
	$("#addressStart").hide('slow');
	$("#previousButton").show('slow');


}
function showPreviousAddress(){
	$("#addressStart").show('slow');
	$("#previousButton").hide('slow');


}
function Check()
{
	if(mins==5 && secs==0)
		alert("You have only five minutes remaining");
	else if(mins==0 && secs==0)
	{
		alert("Your alloted time is over.");
	}
}
function Pad(number) //pads the mins/secs with a 0 if its less than 10
{
	if(number<10)
		number=0+""+number;
	return number;
}
function setOpacity( value ) {
	document.getElementById("styled_popup").style.opacity = value / 10;
	document.getElementById("styled_popup").style.filter = 'alpha(opacity=' + value * 10 + ')';
}
function fadeInMyPopup() {
	for( var i = 0 ; i <= 100 ; i++ )
		setTimeout( 'setOpacity(' + (i / 10) + ')' , 8 * i );
}
function fadeOutMyPopup() {
	for( var i = 0 ; i <= 100 ; i++ ) {
		setTimeout( 'setOpacity(' + (10 - i / 10) + ')' , 8 * i );
	}
	setTimeout('closeMyPopup()', 800 );
}
function closeMyPopup() {
	document.getElementById("styled_popup").style.display = "none";
}
function fireMyPopup() {
	setOpacity( 0 );
	document.getElementById("styled_popup").style.display = "block";
	fadeInMyPopup();
}
function removePopup(){
	$("#zoneDetails1").hide("slow");
}
function removeOptions(){
	$("#OptionsForAll").jqmHide();
//	$("#OptionsForAll").hide("slow");
	$(".options").css(cssObj);
}
function autoRefreshOff(){
	document.getElementById('tid').innerHTML="";
	clearTimeout(TimerID);
	TimerRunning=false;
}
function sendSms(){
	var driver_id="";
	var basedOn = document.getElementById("dispatchBasedOn").value;
	if(basedOn=="1"){
		driver_id=document.getElementById("driver_idSMS").value;
	}else{
		cab_id=document.getElementById("cab_idSMS").value;
		var oRows = document.getElementById("driverDetailDisplay").getElementsByTagName("td");
		var iRowCount = oRows.length;
		for ( var j = 0; j < iRowCount; j++) {
			cab = oRows[j].getElementsByTagName("input")[3].value;
			if ( cab_id == cab) {
				driver_id = oRows[j].getElementsByTagName("input")[0].value;
				continue;
			}
		}

	}

	//var selectAll=document.getElementById("Sall").value; 
	//var loggedout=document.getElementById("logout").value;
	//var voiceMsg=document.getElementById("voiceMsg").value;
	var message=document.getElementById("msg").value;
	var url = 'control?action=openrequest&event=sendSMS';
	if(driver_id!=""){
		url=url+'&driver_id='+driver_id+'';
	}
	if(document.getElementById("Sall").checked==true){
		url=url+'&selectAll=on';
	}
	if(document.getElementById("logoutSMS").checked==true){
		url=url+'&loggedout=on';
	}
	if(document.getElementById("voiceMsgSMS").checked==true){
		url=url+'&voiceMsg=on';
	}
	if(message!=""){
		url=url+'&message='+message;
	}
	var ajax = new AjaxInteraction(url,responseSms);
	ajax.doGet();
	messageHistoryShow();
}
function responseSms(responseText){
	messageHistoryShow();
	if(responseText=="1"){
		document.getElementById("msgResponse").innerHTML="message sent Successfully";
		document.getElementById("msg").value="";
	}else{
		document.getElementById("msgResponse").innerHTML="message sending Failed";
	}
}
function removeMessage(){
	$("#sendSms").jqmHide();
//	$("#sendSms").hide("slow");
	$(".Message").css(cssObj);
}

function ORHistory(){
	value=document.getElementById("value").value;
	fromDate=document.getElementById("fromDate").value;
	toDate=document.getElementById("toDate").value;
	url = 'control?action=openrequest&event=openRequestHistory&button=YES&value='+value+'&fromDate='+fromDate+'&toDate='+toDate;
	var ajax = new AjaxInteraction(url,responseHistory);
	ajax.doGet();
}
function responseHistory(responseText){
	value=document.getElementById("value").value;
	fromDate=document.getElementById("fromDate").value;
	toDate=document.getElementById("toDate").value;
	if(document.getElementById("reDispatchMessage").innerHTML!="" && document.getElementById("reDispatchMessage").innerHTML!=null){
		message=document.getElementById("reDispatchMessage").innerHTML;
	}

	document.getElementById("ORHistory").innerHTML=responseText;
	document.getElementById("value").value=value;
	document.getElementById("reDispatchMessage").innerHTML=message;
	document.getElementById("fromDate").value=fromDate;
	document.getElementById("toDate").value=toDate;

}
var account;
var repeatJobs;
function ORSummary(status){
	var keyWord;
	var fDate;
	var tDate;
	if(status==0){
		keyWord=document.getElementById("keyWord").value;
		fDate=document.getElementById("fDate").value;
		tDate=document.getElementById("tDate").value;
		$("#ORSummary").jqm();
		$("#ORSummary").jqmShow();
	} else if(status==10) {
		keyWord=document.getElementById("keyWord").value;
		fDate=document.getElementById("fDate").value;
		tDate=document.getElementById("tDate").value;
	} else {
		keyWord=document.getElementById("ph.Number").value;
		document.getElementById("keyWord").value=keyWord;
		fDate="";
		tDate="";
		document.getElementById("fDate").value="";
		document.getElementById("tDate").value="";
		
		$("#ORSummary").draggable({
			handle: '.orSummaryRow',scroll: false 
		});
		
		$("#ORSummary").jqm();
		$("#ORSummary").jqmShow();
	}
	var fleetNo="";
	if(document.getElementById("fleetSizeR")!=null){
		//alert("fleetsize:"+document.getElementById("fleetSize").value);
		if(document.getElementById("fleetSizeR").value!=0){
			//alert("fleetNoR:"+document.getElementById("fleetNoR").value);
			if(document.getElementById("fleetNoR").value!="all"){
				fleetNo=document.getElementById("fleetNoR").value;
			}
		}
	}
	var url = 'control?action=openrequest&event=openRequestSummary&button=YES&search='+keyWord+'&fDate='+fDate+'&tDate='+tDate+'&status='+status+'&fleetNo='+fleetNo;
	if(document.getElementById("account").checked){
		url=url+"&account=YES";
		account=1;
	}else if(document.getElementById("repeatJobs").checked){
		url=url+"&repeatJobs=YES";
		repeatJobs=1;
	}
	if(status!=10){
		var ajax = new AjaxInteraction(url,responseSummary);
		ajax.doGet();
	} else {
		window.open(url, 'reservedJobs');
	}
}
function responseSummary(responseText){
	keyWord=document.getElementById("keyWord").value;
	fDate=document.getElementById("fDate").value;
	tDate=document.getElementById("tDate").value;
	document.getElementById("ORSummary").innerHTML=responseText;
	document.getElementById("keyWord").value=keyWord;
	document.getElementById("fDate").value=fDate;
	document.getElementById("tDate").value=tDate;
	if(account==1){
		document.getElementById("account").checked=true;
		account=0;
	}else if(repeatJobs==1){
		document.getElementById("repeatJobs").checked=true;
		repeatJobs=0;
	}else{
		document.getElementById("all").checked=true;
	}
}

var searchValue;
var fromDateHistory;
var toDateHistory;
var rowForHistory="";
function reDispatch(row){
	rowForHistory=row;
	document.getElementById("reDispatchButton_"+row).value="ReDispatching";
	searchValue=document.getElementById("value").value;
	fromDateHistory=document.getElementById("fromDate").value;
	toDateHistory=document.getElementById("toDate").value;
	tripId=document.getElementById("tripId"+row).value;
	var time=getCurrentTime();
	url = 'RegistrationAjax?event=reDispatchJob&button=No&reDispatch=YES&tripIdHistory='+tripId+'&time='+time;
	var ajax = new AjaxInteraction(url,responseDispatch);
	ajax.doGet();
}
function responseDispatch(responseText){
	document.getElementById("fromDate").value=fromDateHistory;
	document.getElementById("toDate").value=toDateHistory;
	document.getElementById("value").value=searchValue;
	document.getElementById("reDispatchMessage").innerHTML=responseText;
	$("#reDispatchMessage").show();
	document.getElementById("reDispatchButton_"+rowForHistory).value="ReDispatch";
	$("#reDispatchMessage").fadeIn(1000).fadeOut(8000).fadeIn(1000).fadeOut(8000);
	$("#reDispatchMessage").hide("slow");
//	ORHistory();
}
//function reDispatchFromOR(){
//document.getElementById("reDispatchInORDashboard").value="ReDispatching";
//document.getElementById("reDispatchInORDashboard").disabled=true;
//searchValue=document.getElementById("value").value;
//fromDateHistory=document.getElementById("fromDate").value;
//toDateHistory=document.getElementById("toDate").value;
//tripId=document.getElementById('tripId').value;
//var time=getCurrentTime();
//url = 'control?action=openrequest&event=openRequestHistory&button=No&reDispatch=YES&tripIdHistory='+tripId+'&time='+time;
//var ajax = new AjaxInteraction(url,responseDispatchFromOR);
//ajax.doGet();
//}
//function responseDispatchFromOR(responseText){

//document.getElementById("fromDate").value=fromDateHistory;
//document.getElementById("toDate").value=toDateHistory;
//document.getElementById("value").value=searchValue;
//if(responseText!=null && responseText!=""){
//document.getElementById("reDispatchMessage").innerHTML="ReDispatched Successfully";
//$("#reDispatchMessage").show();
//alert("ReDispatched Successfully");
//document.getElementById("reDispatchInORDashboard").value="ReDispatch";
//document.getElementById("reDispatchInORDashboard").disabled=false;
//$("#ORDash").jqmHide();
//$("input[type=text]").attr('readonly',false);
//$("input[type=checkbox]").attr('disabled',false);
//$(".buttonORDashboard").show();
//$("#reDispatchInORDashboard").hide();
//$("select").attr('disabled',false);
//clearOR();

//}else{
//document.getElementById("reDispatchMessage").innerHTML="ReDispatching UnSuccessful";
//$("#reDispatchMessage").show();
//alert("ReDispatch UnSuccessful");
//document.getElementById("reDispatchInORDashboard").value="ReDispatch";
//document.getElementById("reDispatchInORDashboard").disabled=false;
//}
//document.getElementById("reDispatchButton_"+rowForHistory).value="ReDispatch";
//$("#reDispatchMessage").fadeIn(3000);
//$("#reDispatchMessage").fadeOut(1000);
//$("#reDispatchMessage").hide("slow");
////ORHistory();
//}


function responseHistory(responseText){
	value=document.getElementById("value").value;
	fromDate=document.getElementById("fromDate").value;
	toDate=document.getElementById("toDate").value;
	var message="";
	if(document.getElementById("reDispatchMessage").innerHTML!="" && document.getElementById("reDispatchMessage").innerHTML!=null){
		message=document.getElementById("reDispatchMessage").innerHTML;
	}
	document.getElementById("ORHistory").innerHTML=responseText;
	document.getElementById("value").value=value;
	document.getElementById("reDispatchMessage").innerHTML=message;
	document.getElementById("fromDate").value=fromDate;
	document.getElementById("toDate").value=toDate;

}
function removeORHistory(){
	$("#ORHistoryResult").hide("slow");
	$('#reDispatchMessage').hide("slow");
	$("#ORHistory").jqmHide();
	$(".ORHistory").css(cssObj);
	$("input[type=text]").attr('readonly',false);
	$("input[type=checkbox]").attr('disabled',false);
	$(".buttonORDashboard").show();
	$("#reDispatchInORDashboard").hide();
	$("select").attr('disabled',false);
	$("textarea").attr('readonly',false);
	clearOR();
	currentDate(0);
}


function removeORSummary(){
	$("#ORSummaryResult").hide();
	$("#ORSummary").jqmHide();
	$(".ORSummary").css(cssObj);
	todayDate();
}

function changeHappeningJob(i){
	//if(document.getElementById("driver_id"+i).value.length>0){
	document.getElementById("changeHappeningJob").value="true";
	$("#jobsTop").css('background-color','red');
	//}else{
	//document.getElementById("changeHappeningJob").value="false";
	//}
}
function changeHappeningDriver(){
	if(document.getElementById("driver_id").value.length>0){
		document.getElementById("changeHappeningDriver").value="true";
	}else{
		document.getElementById("changeHappeningDriver").value="false";
	}
}
function changeHappeningZone(){
	if(document.getElementById("driverid").value.length>0){
		document.getElementById("changeHappeningZone").value="true";
	}else{
		document.getElementById("changeHappeningZone").value="false";
	}
}
function removeDriver(){
	$("#drivers1").hide("slow");
	$(".Driver").css(cssObj);
	//Init();
}
function removeJob(){
	$("#openjobs").hide("slow");
	$(".Jobs").css(cssObj);
	//Init();
}
function removeZone(){
	$("#queueid").hide("slow");
	$(".zones").css(cssObj);
	//Init();
}

function removeZoneRate(){
	$("#showzoneRates").hide("slow");
	$(".zoneRates").css(cssObj);
	//Init();
}

function removeDriverZoneActivity(){
	$("#showDriverZoneActivity").hide("slow");
	$(".DriverZoneActivity").css(cssObj);
}

var mins,secs,TimerRunning,TimerID,TimerID1,thisJobIsRunning,thisJobIsRunningForDriver;
TimerRunning=false,i=1;

function Init(OR)//call the Init function when u need to start the timer
{
	mins=1;
	secs=0;
	StopTimer(OR);
}

function StopTimer(OR)
{
	if(TimerRunning){
		clearTimeout(TimerID);
	}
	TimerRunning=false;
	secs= document.getElementById('sec').value;
	StartTimer(OR);
}
function StartTimer(OR)
{	

	if(OR=="ORHide"){
		secs=0;
	}else{
		TimerRunning=true;
		document.getElementById('tid').innerHTML = "Time Remaining :"+Pad(secs);
		TimerID=self.setTimeout("StartTimer()",1000);
	}
	if(secs!=0){
		thisJobIsRunning = false;
		thisJobIsRunningForDriver = false;
		secs--;
		return;
	}
	//console.log("Inside Secs == 0");	
	if(thisJobIsRunning==true || thisJobIsRunningForDriver==true){
		return;
	}
	StopTimer();
	if( $('#ORDash').is(':visible') ) {
		return;
	}
	readCookieForCenter();
	if(document.getElementById("changeHappeningJob").value=="false" && document.getElementById("changeHappeningDriver").value=="false" && document.getElementById("changeHappeningZone").value=="false"&& document.getElementById("changeHappeningSharedRide").value=="false"){
		if( $('#queueid').is(':visible') ) {
			getAllValuesForDashboard('11111');
		}else{
			getAllValuesForDashboard('11000');
		}
	}
	thisJobIsRunning = false;
	thisJobIsRunningForDriver = false;
}

function disableCheckbox(){
	if(document.getElementById("sameOrder")!=null){
		document.getElementById("sameOrder").checked=false;}
}


function  displayJobOfDaysDash()
{
	var ele = document.getElementById("hideJobsOnDays");
	var text = document.getElementById("displayJobsOnDays");
	if(ele.style.display == "block") {
		ele.style.display = "none";
		//text.innerHTML = "Jobs On Days";
	}
	else {
		ele.style.display = "block";
		//text.innerHTML = "Jobs On Days";
	}
} 


function jobDetailsSelectedCheck(){
	if(!document.getElementById("showJobNo").checked){
		$('.showJobNo').hide();
	}else{
		$('.showJobNo').show();
	}
	if(!document.getElementById("showAddress").checked){
		$('.showAddress').hide();
	}else{
		$('.showAddress').show();
	}
	if(!document.getElementById("showScity").checked){
		$('.showScity').hide();
	}else{
		$('.showScity').show();
	}if(!document.getElementById("showEcity").checked){
		$('.showEcity').hide();
	}else{
		$('.showEcity').show();
	}
	if(!document.getElementById("showDAddress").checked){
		$('.showDAddress').hide();
	}else{
		$('.showADddress').show();
	}
	if(!document.getElementById("showDriverAllocate").checked){
		$('.showDriverAllocate').hide();
	}else{
		$('.showDriverAllocate').show();
	}
	if(!document.getElementById("showAge").checked){
		$('.showAge').hide();
	}else{
		$('.showAge').show();
	}
	if(!document.getElementById("showPhone").checked){
		$('.showPhone').hide();
	}else{
		$('.showPhone').show();
	}
	if(!document.getElementById("showName").checked){
		$('.showName').hide();
	}else{
		$('.showName').show();
	}if(!document.getElementById("showStZone").checked){
		$('.showStZone').hide();
	}else{
		$('.showStZone').show();
	}if(!document.getElementById("showEdZone").checked){
		$('.showEdZone').hide();
	}else{
		$('.showEdZone').show();
	}if(!document.getElementById("showPtime").checked){
		$('.showPtime').hide();
	}else{
		$('.showPtime').show();
	}
	if(!document.getElementById("showSplFlags").checked){
		$('.showSplFlags').hide();
	}else{
		$('.showSplFlags').show();
	}
	if(!document.getElementById("showLandmark").checked){
		$('.showLandmark').hide();
	}else{
		$('.showLandmark').show();
	}	
	if(!document.getElementById("showTripId").checked){
		$('.showTripId').hide();
	}else{
		$('.showTripId').show();
	}if(!document.getElementById("showTripSource").checked){
		$('.showTripSource').hide();
	}else{
		$('.showTripSource').show();
	}if(!document.getElementById("showRouteNum").checked){
		$('.showRouteNum').hide();
	}else{
		$('.showRouteNum').show();
	}if(!document.getElementById("showFlight").checked){
		$('.showFlight').hide();
	}else{
		$('.showFlight').show();
	}if(!document.getElementById("showVoucher").checked){
		$('.showVoucher').hide();
	}else{
		$('.showVoucher').show();
	}if(!document.getElementById("showAmount").checked){
		$('.showAmount').hide();
	}else{
		$('.showAmount').show();
	}if(!document.getElementById("showcheck").checked){
		$('.showcheck').hide();
	}else{
		$('.showcheck').show();
	}
}

function hideDetails(){
	value ="0";
	value1 ="1";
	if(!document.getElementById("showSplFlags").checked){
		$('.showSplFlags').hide();
		name="GAC-D-J-showSplFlags";
		if($("#GAC-D-J-showSplFlags").val()!=null){
			$("#GAC-D-J-showSplFlags").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showSplFlags').show();
		name="GAC-D-J-showSplFlags";
		if($("#GAC-D-J-showSplFlags").val()!=null){
			$("#GAC-D-J-showSplFlags").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}

	if(!document.getElementById("showJobNo").checked){
		$('.showJobNo').hide();
		name="GAC-D-J-showJobNo";
		if($("#GAC-D-J-showJobNo").val()!=null){
			$("#GAC-D-J-showJobNo").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showJobNo').show();
		name="GAC-D-J-showJobNo";
		if($("#GAC-D-J-showJobNo").val()!=null){
			$("#GAC-D-J-showJobNo").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}
	if(!document.getElementById("showAddress").checked){
		$('.showAddress').hide();
		name="GAC-D-J-showAddress";
		if($("#GAC-D-J-showAddress").val()!=null){
			$("#GAC-D-J-showAddress").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showAddress').show();
		name="GAC-D-J-showAddress";
		if($("#GAC-D-J-showAddress").val()!=null){
			$("#GAC-D-J-showAddress").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}if(!document.getElementById("showScity").checked){
		$('.showScity').hide();
		name="GAC-D-J-showScity";
		if($("#GAC-D-J-showScity").val()!=null){
			$("#GAC-D-J-showScity").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showScity').show();
		name="GAC-D-J-showScity";
		if($("#GAC-D-J-showScity").val()!=null){
			$("#GAC-D-J-showScity").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}

	if(!document.getElementById("showDAddress").checked){
		$('.showDAddress').hide();
		name="GAC-D-J-showDAddress";
		if($("#GAC-D-J-showDAddress").val()!=null){
			$("#GAC-D-J-showDAddress").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showDAddress').show();
		name="GAC-D-J-showDAddress";
		if($("#GAC-D-J-showDAddress").val()!=null){
			$("#GAC-D-J-showDAddress").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}
	if(!document.getElementById("showEcity").checked){
		$('.showEcity').hide();
		name="GAC-D-J-showEcity";
		if($("#GAC-D-J-showEcity").val()!=null){
			$("#GAC-D-J-showEcity").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showEcity').show();
		name="GAC-D-J-showEcity";
		if($("#GAC-D-J-showEcity").val()!=null){
			$("#GAC-D-J-showEcity").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}

	if(!document.getElementById("showDriverAllocate").checked){
		$('.showDriverAllocate').hide();
		name="GAC-D-J-showDriverAllocate";
		if($("#GAC-D-J-showDriverAllocate").val()!=null){
			$("#GAC-D-J-showDriverAllocate").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showDriverAllocate').show();
		name="GAC-D-J-showDriverAllocate";
		if($("#GAC-D-J-showDriverAllocate").val()!=null){
			$("#GAC-D-J-showDriverAllocate").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showAge").checked){
		$('.showAge').hide();
		name="GAC-D-J-showAge";
		if($("#GAC-D-J-showAge").val()!=null){
			$("#GAC-D-J-showAge").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showAge').show();
		name="GAC-D-J-showAge";
		if($("#GAC-D-J-showAge").val()!=null){
			$("#GAC-D-J-showAge").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}
	if(!document.getElementById("showPhone").checked){
		$('.showPhone').hide();
		name="GAC-D-J-showPhone";
		if($("#GAC-D-J-showPhone").val()!=null){
			$("#GAC-D-J-showPhone").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showPhone').show();
		name="GAC-D-J-showPhone";
		if($("#GAC-D-J-showPhone").val()!=null){
			$("#GAC-D-J-showPhone").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showName").checked){
		$('.showName').hide();
		name="GAC-D-J-showName";
		if($("#GAC-D-J-showName").val()!=null){
			$("#GAC-D-J-showName").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showName').show();
		name="GAC-D-J-showName";
		if($("#GAC-D-J-showName").val()!=null){
			$("#GAC-D-J-showName").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}if(!document.getElementById("showStZone").checked){
		$('.showStZone').hide();
		name="GAC-D-J-showStZone";
		if($("#GAC-D-J-showStZone").val()!=null){
			$("#GAC-D-J-showStZone").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showStZone').show();
		name="GAC-D-J-showStZone";
		if($("#GAC-D-J-showStZone").val()!=null){
			$("#GAC-D-J-showStZone").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}if(!document.getElementById("showEdZone").checked){
		$('.showEdZone').hide();
		name="GAC-D-J-showEdZone";
		if($("#GAC-D-J-showEdZone").val()!=null){
			$("#GAC-D-J-showEdZone").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showEdZone').show();
		name="GAC-D-J-showEdZone";
		if($("#GAC-D-J-showEdZone").val()!=null){
			$("#GAC-D-J-showEdZone").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}if(!document.getElementById("showPtime").checked){
		$('.showPtime').hide();
		name="GAC-D-J-showPtime";
		if($("#GAC-D-J-showPtime").val()!=null){
			$("#GAC-D-J-showPtime").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showPtime').show();
		name="GAC-D-J-showPtime";
		if($("#GAC-D-J-showPtime").val()!=null){
			$("#GAC-D-J-showPtime").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}if(!document.getElementById("showTripId").checked){
		$('.showTripId').hide();
		name="GAC-D-J-showTripId";
		if($("#GAC-D-J-showTripId").val()!=null){
			$("#GAC-D-J-showTripId").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showTripId').show();
		name="GAC-D-J-showTripId";
		if($("#GAC-D-J-showTripId").val()!=null){
			$("#GAC-D-J-showTripId").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showTripSource").checked){
		$('.showTripSource').hide();
		name="GAC-D-J-showTripSource";
		if($("#GAC-D-J-showTripSource").val()!=null){
			$("#GAC-D-J-showTripSource").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showTripSource').show();
		name="GAC-D-J-showTripSource";
		if($("#GAC-D-J-showTripSource").val()!=null){
			$("#GAC-D-J-showTripSource").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showRouteNum").checked){
		$('.showRouteNum').hide();
		name="GAC-D-J-showRouteNum";
		if($("#GAC-D-J-showRouteNum").val()!=null){
			$("#GAC-D-J-showRouteNum").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showRouteNum').show();
		name="GAC-D-J-showRouteNum";
		if($("#GAC-D-J-showRouteNum").val()!=null){
			$("#GAC-D-J-showRouteNum").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}
	if(!document.getElementById("showLandmark").checked){
		$('.showLandmark').hide();
		name="GAC-D-J-showLandmark";
		if($("#GAC-D-J-showLandmark").val()!=null){
			$("#GAC-D-J-showLandmark").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showLandmark').show();
		name="GAC-D-J-showLandmark";
		if($("#GAC-D-J-showLandmark").val()!=null){
			$("#GAC-D-J-showLandmark").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}	
	if(!document.getElementById("showFlight").checked){
		$('.showFlight').hide();
		name="GAC-D-J-showFlight";
		if($("#GAC-D-J-showFlight").val()!=null){
			$("#GAC-D-J-showFlight").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));

		}
	}else{
		$('.showFlight').show();
		name="GAC-D-J-showFlight";
		if($("#GAC-D-J-showFlight").val()!=null){
			$("#GAC-D-J-showFlight").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));

		}
	}
	if(!document.getElementById("showVoucher").checked){
		$('.showVoucher').hide();
		name="GAC-D-J-showVoucher";
		if($("#GAC-D-J-showVoucher").val()!=null){
			$("#GAC-D-J-showVoucher").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showVoucher').show();
		name="GAC-D-J-showVoucher";
		if($("#GAC-D-J-showVoucher").val()!=null){
			$("#GAC-D-J-showVoucher").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}
	if(!document.getElementById("showAmount").checked){
		$('.showAmount').hide();
		name="GAC-D-J-showAmount";
		if($("#GAC-D-J-showAmount").val()!=null){
			$("#GAC-D-J-showAmount").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showAmount').show();
		name="GAC-D-J-showAmount";
		if($("#GAC-D-J-showAmount").val()!=null){
			$("#GAC-D-J-showAmount").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}
	}
	if(!document.getElementById("showcheck").checked){
		$('.showcheck').hide();
		name="GAC-D-J-showcheck";
		if($("#GAC-D-J-showcheck").val()!=null){

			$("#GAC-D-J-showcheck").val("0");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('0'));
		}
	}else{
		$('.showcheck').show();
		name="GAC-D-J-showcheck";
		if($("#GAC-D-J-showcheck").val()!=null){
			$("#GAC-D-J-showcheck").val("1");
		}else{
			$('#cookieValueHidden').append($('<input/>').attr('type', 'hidden').attr('name', name).attr('id',name).val('1'));
		}

	}



}

function searchFromQ() {
	s = document.getElementById("driverid").value;
	url = '/TDS/DashBoard?event=search&driver=' + s;
	var ajax = new AjaxInteraction(url, search);
	ajax.doGet();
}
function search(responseText) {
	var resp = responseText.split('###');
	document.getElementById("queueid").innerHTML = resp[0];
	document.getElementById("changeHappeningZone").value="false";
}




function drivers5(responseText) {
	var resp = responseText.split('###');
	document.getElementById("drivers1").innerHTML = resp[0];
	$("#right-block").remove();
	$("#right-block5").show("slow");
}
function driverDetails1() {
	url = '/TDS/DashBoard?event=driverDetails&status=1';
	var ajax = new AjaxInteraction(url, drivers5);
	ajax.doGet();
}
function searchLoggedInDriver(){
	driver5 = document.getElementById("driver_id").value;
	if(driver5!=null&&driver5!=""){
		for(var i=0;i<globalDrivers.driverList.length;i++){
			if(globalDrivers.driverList[i].varDr==driver5){
				jsonObj.driver[i].varDr;
			}
		}
		url = '/TDS/DashBoard?event=searchDriver&driver='+driver5;
		var ajax = new AjaxInteraction(url, search3);
		ajax.doGet();
	}
}
function search3(responseText) {
	var resp = responseText.split('###');
	document.getElementById("drivers1").innerHTML = resp[0];
	document.getElementById("changeHappeningDriver").value="false";
}
function updateDriver(row, trip_id,driver3,typeOfAllocate) {
	document.getElementById("changeHappeningJob").value="false";
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var basedOn = document.getElementById("dispatchBasedOn").value;

	disp = document.getElementById("dispatch").value;
	if(disp==2){
		cabNo=document.getElementById('driver_id'+row).value;
		if(isNaN(cabNo)){
			if(isNaN(cabNo.substring(1))){
				n=cabNo.substring(0,2);
				cabNo=cabNo.substring(2);
			}else{
				n=cabNo.substring(0,1);
				cabNo=cabNo.substring(1);
			}
			document.getElementById("notAnumber").value=n;
		}
		var oRows=document.getElementById("driverDetailDisplay").getElementsByTagName("td");
		var iRowCount= oRows.length;
		for(var i=0;i<iRowCount;i++){
			var driver="";
			cab=oRows[i].getElementsByTagName("input")[3].value;
			if(cabNo==cab){
				driver=oRows[i].getElementsByTagName("input")[0].value;
				if(document.getElementById("notAnumber").value!=null&&document.getElementById("notAnumber").value!=""){
					var p=document.getElementById("notAnumber").value;
					document.getElementById("notAnumber").value="";
					driver=p+driver;
				}
				break;
			}
		}
	}
	else{
		driver=document.getElementById('driver_id'+row).value;
		if(isNaN(driver)){
			if(isNaN(river.substring(1))){
				n=driver.substring(0,2);
				driver=driver.substring(2);
			}else{
				n=driver.substring(0,1);
				driver=driver.substring(1);
			}
			document.getElementById("notAnumberB").value=n;
		}
		var oRows=document.getElementById("driverDetailDisplay").getElementsByTagName("td");
		var iRowCount= oRows.length;
		for(var i=0;i<iRowCount;i++){
			driver1=oRows[i].getElementsByTagName("input")[3].value;
			if(driver==driver1){
				cab=oRows[i].getElementsByTagName("input")[5].value;
				if(document.getElementById("notAnumberB").value!=null&&document.getElementById("notAnumberB").value!=""){
					var p=document.getElementById("notAnumberB").value;
					document.getElementById("notAnumberB").value="";
					driver=p+driver;
				}  }
			break;
		}
	}
	var answer = "";
	if(driver!=""){
		if(typeOfAllocate=="2"){
			driver="F"+driver;
		}
		var url = '/TDS/AjaxClass?event=updateDriver&type=1&driver_id='+driver+'&trip_id='+trip_id+'&answer='+ answer+'&cab='+cab+'&pickup='+document.getElementById("pickup"+row).value+'&prevCab='+driver3;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var resp = text.split('###');
		if (resp[0] >= "1"){
			//document.getElementById("openjobs").innerHTML = resp[1];
			getAllValuesForDashboard('10000');
			var dispatchBasedOn=document.getElementById("dispatch").value;
			if(dispatchBasedOn==2){
				driverOrCab = "CabNo:" + cab;	
			}else{
				driverOrCab="Driver ID:"+driver;
			}
			var cell = document.createTextNode(""+currentTimeCalculate()+":> "+driverOrCab+" is allocated Successfully to TripID:"+trip_id);
			var cellBr = document.createElement("br");
			document.getElementById("console").appendChild(cellBr);
			document.getElementById("console").appendChild(cell);
			$(document).ready(function() {
			});
		} else {
			var error = resp[0].split(";");
			alert(error[1]);
		}
	}

	mapInitJobs('1');
	driver= document.getElementById('driver_id' + row).value;

}

function updateDriver3(i, trip_id, assocode,time) {
	disp = document.getElementById("dispatch").value;
	var answer = "";
	var url = '/TDS/AjaxClass?event=updateDriver&type=1&driver_id='+ driver + '&trip_id='+ trip_id + '&assocode=' + assocode + '&answer=' + answer;
	var ajax = new AJAXInteraction(url, doUpdateOQValues);
	ajax.doGet();
	driver= document.getElementById('driver_id' + i).value;
	document.getElementById("changeHappeningJob").value="false";
}
function updateTime(i,tripID,assocode,pickup){
	time = document.getElementById("pickup"+i).value;
	stime = document.getElementById("serviceDate"+i).value;
	d=stime.substring(0,11);
	t=time.substring(0,2)+":"+time.substring(2,4)+":00";
	sd=d+t;
	if(pickup!=time){
		var url = '/TDS/DashBoard?event=TimeUpdate&trip_id='+tripID+'&pickup='+sd;
		var ajax = new AJAXInteraction(url, doUpdateOQValues);
		ajax.doGet();
	}
}
function doUpdateOQValues(responseText) {
	var resp = responseText.split('###');
	if (resp[0] == "1"){
		//document.getElementById("openid").innerHTML = resp[1];
		$(document).ready(function() {
		});
	}
}
function logoutDriver(driverid) {
	var answer = prompt("Reason for delete ?","");
	var driverStatus=2+"";
	if(document.getElementById("driverShowCheck").value == "lin"){
		driverStatus=1+"";
	}
	//alert("driverStatus:"+driverStatus);
	url = '/TDS/DashBoard?event=logout&driverid=' + driverid+'&answer='+answer+'&driverStatus='+driverStatus;
	var ajax = new AjaxInteraction(url, drivers);
	ajax.doGet();
	document.getElementById("changeHappeningDriver").value="false";

}
function deactivateDriver(driverid) {
	var answer = prompt("Reason for deactivate ?","");
	var fdate = prompt("From Date?","");
	var tdate = prompt("To Date?","");
	url = '/TDS/DashBoard?event=deactivate&driverid=' + driverid+'&answer='+answer+"&fdate="+fdate+"&tdate="+tdate;
	var ajax = new AjaxInteraction(url, drivers);
	ajax.doGet();
	document.getElementById("changeHappeningDriver").value="false";
}
function deleteJobs(tripId) {
	var answer = prompt("Reason for cancelling this job ?","");
	if(answer!=null&&answer!=""){
		url = '/TDS/DashBoard?event=deleteJob&tripId='+ tripId+'&answer='+answer;
		var ajax = new AjaxInteraction(url, deleteJob);
		ajax.doGet();
	}
}
function deleteJob(responseText) {
	getAllValuesForDashboard('10000');
	document.getElementById("changeHappeningJob").value="false";
}
var fromDateReservation;
var toDateReservation;
var searchReservation;
function deleteJobsSummary(tripId) {
	var confirmation=confirm("Do you want to cancel this Trip");
	if (confirmation==true) {
		searchReservation=document.getElementById("keyWord").value;
		fromDateReservation=document.getElementById("fDate").value;
		toDateReservation=document.getElementById("tDate").value;
		url = '/TDS/DashBoard?event=deleteJobSummary&tripId=' + tripId;
		var ajax = new AjaxInteraction(url, deleteJobSummary);
		ajax.doGet();
		getAllValuesForDashboard('10000');  
	}
}
function deleteJobSummary(responseText) {
	document.getElementById("fDate").value=fromDateReservation;
	document.getElementById("tDate").value=toDateReservation;
	document.getElementById("keyWord").value=searchReservation;
	/*var resp = responseText.split('###');
		document.getElementById("ORSummary").innerHTML = resp[0];*/
	ORSummary(0);
}

function driverDetails() {
	//console.log("getting job details");	

	document.getElementById("unload").src="images/Dashboard/loading_icon.gif";
	var url="";
	var zoneList ="";
	if($.cookie("GAC-zoneList")!=null&&$.cookie("GAC-zoneList")!=""){
		zoneList = $.cookie("GAC-zoneList");
	}
	var statusOfJobToPull = "40";
	var statusOfRtJobToPull ="1";
	if(document.getElementById("All").checked==false){
		statusOfJobToPull = "0";
	}

	if(document.getElementById("rtJobs").checked==false){
		statusOfRtJobToPull = "0";
	}
	url ='DashBoard?event=jobDetails&status='+statusOfJobToPull+'&rtJobStatus='+statusOfRtJobToPull+'&zoneList='+zoneList;

	var ajax = new AjaxInteraction(url, jobDetails1);
	ajax.doGet();
}

function jobDetails1(responseText) {
	if(responseText=="lo"){
		window.open('control','_self');
	}
	//console.log("Processing job details");	

	if(document.getElementById("jobsPermission").value!="yes"){
		document.getElementById("jobScreen").innerHTML ="Permission Denied";
		document.getElementById("unload").src="images/Dashboard/refresh1.png";
		return;
	}
	var jobsJsonObj = "{\"address\":"+responseText+"}" ;
	var obj = JSON.parse(jobsJsonObj.toString());
	var driverOrCab;
	var dispatchBasedOn=document.getElementById("dispatch").value;
	if(dispatchBasedOn==2){
		driverOrCab = "Cab";	
	}else{
		driverOrCab="Driver";
	}
	var tableAppend=  document.getElementById("customerTable");
	//var divPlain = document.getElementById("plainJobs");
	var tableRows='<tr id="row" class="customerRow"> ';
	tableRows=tableRows+'<th class="showJobNo">#</th><th width="20px"class="showAddress">Address</th><th class="showscity">SCity</th><th width="20px"class="showDAddress">Drop Add</th><th class="showecity">ECity</th><th class="showAge">Age</th><th class="showTripId">Trip ID</th><th class="showTripSource">Trip Source</th><th class="showRouteNum">Route Num</th><th class="showDriverAllocate">'+driverOrCab+'</th><th></th><th class="showPhone">Ph</th><th class="showName">Name</th><th class="showStatus">St</th><th class="showStZone">S Z</th><th class="showEdZone">E Z</th><th class="showPtime">Time</th><th class="showSplFlags">Flags</th><th class="showFlight">Flight Num</th><th class="showVoucher">Account</th><th class="showAmount">Amount</th><th class="showcheck">*</th>';
	for(var i=0;i<obj.address.length;i++){
		var tripAmt="$0.00";
		if(obj.address[i].AMT!="0"){
			tripAmt = "$"+obj.address[i].AMT;
		}
		var driverCab="";
		var addressToShow=obj.address[i].A.substring(0,25);
		var dAddressToShow=obj.address[i].DA.substring(0,25);
		if(dispatchBasedOn==2){
			driverCab= obj.address[i].V;
		}else{
			driverCab= obj.address[i].D;
		}
		var rowColor = "";
		if(obj.address[i].AC!=null && obj.address[i].AC=="cc"){
			rowColor="";
		}else if(obj.address[i].AC!=null && obj.address[i].AC=="cash"){
			rowColor='#'+document.getElementById("cashColor").value;
		}else if(obj.address[i].AC !=null && obj.address[i].AC=="VC"){
			rowColor='#'+document.getElementById("voucherColor").value;
		}else{
			rowColor="";
		}
		var colorForVip ="";

		if(obj.address[i].PC == 1){
			colorForVip="#"+document.getElementById("vipColor").value;
		}else if(obj.address[i].PC == 2){
			colorForVip="#"+document.getElementById("timeColor").value;
		}
		var j=Number(i)+1;
		tableRows=tableRows+' <tr id="row"   bgcolor="'+colorForVip+'"  class="customerRow">'; 
		if(obj.address[i].SR=="1"){
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+'<font color="red">S</font>)</td>';
		}else{
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+')</td>';
		} if(obj.address[i].LMN!=""){
			addressToShow=obj.address[i].LMN;
		}if(obj.address[i].ELM!=""){
			dAddressToShow=obj.address[i].ELM;
		} 
		tableRows=tableRows+'<td class="showAddress" width="180px"id="customerid" onclick="currentDetails(\''+obj.address[i].D+'\','+obj.address[i].T+','+obj.address[i].LA+','+obj.address[i].LO+','+obj.address[i].S+');mapInitJobs('+0+');mapPlotDriver(0)">'+addressToShow+'</td>';
		tableRows=tableRows+'<td class="showScity"style=""id="showscity">'+obj.address[i].SCTY+'</td>';
		tableRows=tableRows+'<td class="showDAddress" style="white-space:nowrap" width="180px"id="customerid" onclick="currentDetails(\''+obj.address[i].D+'\','+obj.address[i].T+','+obj.address[i].LA+','+obj.address[i].LO+','+obj.address[i].S+');mapInitJobs('+0+');mapPlotDriver(0)">'+dAddressToShow+'</td>';
		tableRows=tableRows+'<td class="showEcity"style=""id="showecity">'+obj.address[i].ECTY+'</td>';

		tableRows=tableRows+'<td class="showAge"><font color="green"><center>'+obj.address[i].PT+'m</font></td>';
		tableRows=tableRows+'<td class="showTripId" style="display:none"id="tripid">'+obj.address[i].T+'</td>';
		tableRows=tableRows+'<td class="showTripSource" style="display:none" id="stripsource"><font color="green">TripSource</font></td>';
		tableRows=tableRows+'<td class="showRouteNum" style="display:none" id="routeNum"><font color="green">RouteNum</font></td>';

		tableRows=tableRows+'<input type="hidden" name="queue_no" id="queue_no'+i+'" value='+obj.address[i].Q+'>';
		if(obj.address[i].N=="Flag Trip"){
			tableRows=tableRows+'<td class="showDriverAllocate" id="contactname"><input type="text" readonly="readonly"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[i].T+',\''+driverCab+'\',\''+obj.address[i].RT+'\')"  onfocus="changeHappeningJob()" /></td>';		tableRows=tableRows+'<td><input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/></td>';
		}else{
			tableRows=tableRows+'<td  class="showDriverAllocate" id="contactname"><input type="text"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[i].T+',\''+driverCab+'\',\''+obj.address[i].RT+'\')"  onfocus="changeHappeningJob('+i+')" /></td>';		tableRows=tableRows+'<td><input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/></td>';
		}
		tableRows=tableRows+'<input type="hidden"id="tripid'+i+'" value="'+obj.address[i].T+'"/>';
		tableRows=tableRows+'<td class="showDriverId" style="display:none"id="driverId">'+obj.address[i].D+'</td>';
		tableRows=tableRows+'<td class="showPhone" style="white-space:nowrap" id="phoneNoInJobs">'+obj.address[i].P+'</td>';
		tableRows=tableRows+'<td class="showName"style=""id="riderName">'+obj.address[i].N+'</td>';
		tableRows=tableRows+'<input type="hidden" id="lan'+i+'" value="'+obj.address[i].LA+'"/>';
		tableRows=tableRows+'<input type="hidden" id="lon'+i+'" value="'+obj.address[i].LO+'"/>';
		tableRows=tableRows+'<input type="hidden" id="city'+i+'" value="'+obj.address[i].A+'">';
		tableRows=tableRows+'<input type="hidden" name="serviceDate'+i+'" id="serviceDate'+i+'"value="'+obj.address[i].SD+'">';


		var status="";
		if(obj.address[i].S=="30"){status="Broadcast";
		}else if(obj.address[i].S=="40"){status="Allocated";
		}else if(obj.address[i].S=="43"){status="On Rotue To Pickup";
		}else if(obj.address[i].S=="50"){status="Customer Cancelled Request";
		}else if(obj.address[i].S=="51"){status="No Show";
		}else if(obj.address[i].S=="49"){status="Soon To Clear";
		}else if(obj.address[i].S=="4"){
			if(obj.address[i].DD=="1"){
				status="Don't Dispatch";
			}else{
				status="Dispatching";
			}
		}else if(obj.address[i].S=="0"){status="Unknown";
		}else if(obj.address[i].S=="47"){status="Trip Started";
		}else if(obj.address[i].S=="99"){status="Trip On Hold";
		}else if(obj.address[i].S=="3"){status="Couldnt Find Drivers";
		}else if(obj.address[i].S=="90"){status="SrHold Job";
		}else if(obj.address[i].S=="48"){status="Driver Reporting NoShow";}

		tableRows=tableRows+'<input type="hidden" name="status'+i+'" id="status'+i+'" value="'+status+'">';
		tableRows=tableRows+'<input type="hidden" name="phone'+i+'" id="phone'+i+'" value='+obj.address[i].P+'>';
		tableRows=tableRows+'<input type="hidden" name="passname'+i+'" id="passname'+i+'" value='+obj.address[i].N+'>';
		tableRows=tableRows+'<input type="hidden" name="driver" id="driver" value='+obj.address[i].D+'>';
		tableRows=tableRows+'<input type="hidden" name="statusvalue" id="statusvalue" value='+obj.address[i].S+'>';
		tableRows=tableRows+'<input type="hidden"id="driverIdForDetails'+i+'" name="driverIdForDetails'+i+'"value='+obj.address[i].D+'>';
		tableRows=tableRows+'<input type="hidden"id="stZone'+i+'" name="stZone'+i+'"value='+obj.address[i].Q+'>';
		tableRows=tableRows+'<input type="hidden"id="edZone'+i+'" name="edZone'+i+'"value='+obj.address[i].EDQ+'>';
		tableRows=tableRows+'<input type="hidden"id="OrComments'+i+'" name="OrComments'+i+'"value='+obj.address[i].OC+'>';
		tableRows=tableRows+'<input type="hidden"id="OrSplIns'+i+'" name="OrSplIns'+i+'"value='+obj.address[i].SPL+'>';
		tableRows=tableRows+'<input type="hidden"id="ORPty'+i+'" name="ORPty'+i+'"value='+obj.address[i].PTY+'>';
		tableRows=tableRows+'<input type="hidden"id="OrAmt'+i+'" name="OrAmt'+i+'"value='+obj.address[i].AMT+'>';
		tableRows=tableRows+'<input type="hidden"id="vehiNo'+i+'" name="vehiNo'+i+'"value='+obj.address[i].V+'>';

		tableRows=tableRows+'<td class="showStatus">';
		var driverAvailable="";
		if(Number(obj.address[i].S) >=40){
			for(var j=0;j<globalDrivers.driverList.length;j++){
				if((obj.address[i].D==globalDrivers.driverList[j].varDr || obj.address[i].V==globalDrivers.driverList[j].varVNo) && globalDrivers.driverList[j].varSwh !="L"){
					driverAvailable = "true";
				}
			}
		}
		if(obj.address[i].SR=="1"){
			if(Number(obj.address[0].jobsList[i].S) >=40 && driverAvailable==""){
				tableRows=tableRows+'<img src="images/Dashboard/h.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			} else {
				if(obj.address[i].S== "40"){
					tableRows=tableRows+'<img src="images/Dashboard/allocated.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="4"||obj.address[i].S=="8"||obj.address[i].S== "90"){
					tableRows=tableRows+'<img src="images/Dashboard/operatorYellow.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="3"){
					tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S== "30"){
					tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S== "49"){
					tableRows=tableRows+'<img src="images/Dashboard/soonToComplete.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else{
					tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}
			}
		}else{
			if(Number(obj.address[0].jobsList[i].S) >=40 && driverAvailable==""){
				tableRows=tableRows+'<img src="images/Dashboard/h.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			} else {
				if(obj.address[i].S== "30"){
					tableRows=tableRows+'<img src="images/Dashboard/megaphone_red.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 25px;height: 21px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S== "49"){
					tableRows=tableRows+'<img src="images/Dashboard/soonToComplete.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S== "40"){
					tableRows=tableRows+'<img src="images/Dashboard/allocated.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S== "43"){
					tableRows=tableRows+'<img src="images/Dashboard/onroute.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="50"){
					tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="51"){
					tableRows=tableRows+'<input type="button" class="jqModal"onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="4"||obj.address[i].S=="8"||obj.address[i].S== "90"){
					tableRows=tableRows+'<input type="button"class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="background-color: yellow;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="0"){
					tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="47"){
					tableRows=tableRows+'<img class="jqModal"src="images/Dashboard/onboard.png" onclick="showDialog();showFullDetails('+i+')"style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="99"){
					tableRows=tableRows+'&nbsp;<img  class="jqModal" src="images/Dashboard/h.png" onclick="showDialog();showFullDetails('+i+')"style="width: 20px;height: 16px;cursor: pointer;"/><br/>'; 
				}else if(obj.address[i].S=="3"){
					tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S=="48"){
					tableRows=tableRows+'&nbsp;<img class="jqModal" src="images/Dashboard/redphone.gif"onclick="showDialog();showFullDetails('+i+')" style="width: 21px;height: 16px;cursor: pointer;"/><br/>';
				}else if(obj.address[0].jobsList[i].S=="5"){
					tableRows=tableRows+'<img class="jqModal"src="images/Dashboard/manualAllocation.png" onclick="showDialog();showFullDetails('+i+')"style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else if(obj.address[i].S== "45"){
					tableRows=tableRows+'<img src="images/Dashboard/onsite.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
				}else{
					tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
				}
			}
		}
		tableRows=tableRows+'</td>';
		tableRows=tableRows+'<td class="showStZone"style=""id="stZone">'+obj.address[i].Q+'</td>';
		tableRows=tableRows+'<td class="showEdZone"style=""id="edZone">'+obj.address[i].EDQ+'</td>';
		tableRows=tableRows+'<td class="showPtime"style=""id="pickupTime">'+obj.address[i].ST+'</td>';
		tableRows=tableRows+'<td class="showSplFlags"style=""id="sSplFlags">'+obj.address[i].DP+'</td>';
		tableRows=tableRows+'<td class="showFlight"style=""id="sFlight">'+obj.address[i].AIR+'</td>';
		tableRows=tableRows+'<td class="showVoucher"style=""id="sVoucher">'+obj.address[i].VC+'</td>';
		tableRows=tableRows+'<td class="showAmount"style=""id="sAmount">'+tripAmt+'</td>';
		tableRows=tableRows+'<td class="showcheck"style=""id="scheck"><input type="checkbox" name="" name="scheck_'+i+'" id="scheck_'+i+'" onclick="checkjobs('+obj.address[i].T+')"></input></td>';

//		tableRows=tableRows+'<td class="showLandmark"style=""id="ssLandmark">'+obj.address[i].LMN+'</td>'	
	}
	tableAppend.innerHTML=tableRows;
	//divPlain.innerHTML =tableRows;
	var customerExpand = document.getElementById("customerExpand");

	var tableRowsEx='</tr>';
	var driverOrCabTitle="";
	if(dispatchBasedOn==2)
	{
		driverOrCabTitle="Cab";
	}else{
		driverOrCabTitle="Driver";
	}
	tableRowsEx=tableRowsEx+'<tr bgcolor="#EFEFFB"><th>#</th><th >Address</th><th >'+driverOrCabTitle+'</th><th >Time</th><th>PhoneNO</th><th>Name</th><th>Cab#</th><th>Cab&nbsp;Flags</th><th>QueueNo.</th><th>Driver</th></tr>';
	for(var i=0;i<obj.address.length;i++){
		var driverCab="";
		if(document.getElementById("dispatch").value==2)
		{
			driverCab= obj.address[i].V;

		}else{
			driverCab= obj.address[i].D;
		}
		var j=Number(i)+1;
		tableRowsEx=tableRowsEx+'<tr bgcolor="#EFEFFB"><td>'+j+'</td>';
		tableRowsEx=tableRowsEx+'<td style="white-space:nowrap">'+obj.address[i].A+'</td>';
		tableRowsEx=tableRowsEx+'<td><input type="text" size="5" id="driver_id'+i+'"value="'+driverCab+'" onblur="updateDriver1('+i+','+obj.address[i].T+','+obj.address[i].D+')"onkeypress="changeHappeningJob()"/></td>';
		tableRowsEx=tableRowsEx+'<td><input type="text"id="pickup'+i+'" size="3"value="'+obj.address[i].ST+'" onblur="updateDriver('+i+','+obj.address[i].T+',123,1)"onkeypress="changeHappeningJob()"></td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[i].P+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[i].N+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[i].V+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[i].DP+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[i].Q+'</td>';
		tableRowsEx=tableRowsEx+'<td>'+obj.address[i].D+'</td>';
		tableRowsEx=tableRowsEx+'	</tr>';
	}
	customerExpand.innerHTML=tableRowsEx;
	document.getElementById("unload").src="images/Dashboard/refresh1.png";
	mapInitJobs('1');
	var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
	var iRowCount= oRows.length;
	document.getElementById("totalJobs").innerHTML=obj.address.length;
	jobDetailsSelectedCheck();
}


function driverDetails(checkStatus) {
	var driverJob ="";
	var stZone ="";
	var edZone ="";
	var cityV ="";
	var flagtrip ="No Jobs";
	if(document.getElementById("driverPermission").value!="yes"){
		document.getElementById("permission").innerHTML ="Permission Denied";
		document.getElementById("expandDiv").innerHTML ="Permission Denied";
		return;
	}

	document.getElementById("unloadDrivers").src="images/Dashboard/loading_icon.gif";
	var driverForSearch = document.getElementById("driver_id").value;
	var jsonObj ="";
	if(driverForSearch!=null&&driverForSearch!=""){
		for(var i=0;i<globalDrivers.driverList.length;i++){
			if(document.getElementById("dispatchBasedOn").value==1){
				if(driverForSearch==globalDrivers.driverList[i].varDr){
					var jsonObj1 = JSON.stringify(globalDrivers.driverList[i]);
					var obj = "{\"driver\":[\"1\","+jsonObj1+"]}";
					jsonObj = JSON.parse(obj.toString());
					break;
				}
			}else{
				if(driverForSearch==globalDrivers.driverList[i].varVNo){
					var jsonObj1 = JSON.stringify(globalDrivers.driverList[i]);
					var obj = "{\"driver\":[\"1\","+jsonObj1+"]}";
					jsonObj = JSON.parse(obj.toString());
					break;
				}
			}
		}
	}else{
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url ="";
		if(document.getElementById("driverShowCheck").value!=null&&document.getElementById("driverShowCheck").value!=""){
			checkStatus = document.getElementById("driverShowCheck").value;
		}
		if(checkStatus=="lin"){
			url = '/TDS/DashBoard?event=driverDetails&status=1';
		}else if(checkStatus=="lo"){
			url = '/TDS/DashBoard?event=driverDetails&status=3';
		}else if(checkStatus=="all"){
			url = '/TDS/DashBoard?event=driverDetails&status=5';
		}else{
			url = '/TDS/DashBoard?event=driverDetails&status=1';
		}
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("unloadDrivers").src="images/Dashboard/refresh1.png";
		var obj = "{\"driver\":"+text+"}";
		jsonObj = JSON.parse(obj.toString());
		globalJsonForDriver = jsonObj;
	}
	if(jsonObj!=""&&jsonObj!=null){
		document.getElementById("driverDetailDisplay").innerHTML ="";
		document.getElementById("unloadDrivers").src="images/Dashboard/refresh1.png";
		var table = document.getElementById("driverDetailDisplay");
		table.width="200px";
		var row="";
		var rowCount="";
		rowCount = table.rows.length;
		row = table.insertRow(rowCount);
		if(driverForSearch==null||driverForSearch==""){
			document.getElementById("lastJobRunTime").innerHTML="Job Run:"+jsonObj.driver[0].lastRunTime+"s ago";
		}
		var index = "row";
		var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
		var iRowCount= oRows.length;
		for(var i=1;i<jsonObj.driver.length;i++){
			stZone ="";
			edZone ="";
			for(var k = 1; k < iRowCount; k++) {
				if(oRows[k].id==index){
					driverJob = oRows[k].getElementsByTagName("input")[11].value;
					if(driverJob == jsonObj.driver[i].varDr){
						stZone = oRows[k].getElementsByTagName("input")[14].value;
						edZone= oRows[k].getElementsByTagName("input")[15].value;
						cityV= oRows[k].getElementsByTagName("input")[6].value;
						if(cityV.indexOf("Flag Trip")!=-1){
							flagtrip = "yes";
						}else{
							flagtrip = "No";
						}
					}
				}
			}

			var lastUpdate = 0;
			var cell = row.insertCell(0);
			lastUpdate =  jsonObj.driver[i].lastUpDate;
			cell.className="detail rowstraight";

			var v = document.createTextNode(jsonObj.driver[i].varDr);
			var b = document.createElement("z");
			b.id="row";
			b.style.display="none";
			b.appendChild(v);
			cell.appendChild(b);

			var ve = document.createTextNode(jsonObj.driver[i].varVNo);
			var be = document.createElement("y");
			be.id="vehicleValue";
			be.style.display="none";
			be.appendChild(ve);
			cell.appendChild(be);

			var vs = document.createTextNode(jsonObj.driver[i].varSwh);
			var bs = document.createElement("x");
			bs.id="switchValue";
			bs.style.display="none";
			bs.appendChild(vs);
			cell.appendChild(bs);

			var vlat = document.createTextNode(jsonObj.driver[i].varLt);
			var blat = document.createElement("n");
			blat.id="latForDriverForZone";
			blat.style.display="none";
			blat.appendChild(vlat);
			cell.appendChild(blat);
			var vlong = document.createTextNode(jsonObj.driver[i].varLg);
			var blong = document.createElement("o");
			blong.id="longForDriverForZone";
			blong.style.display="none";
			blong.appendChild(vlong);
			cell.appendChild(blong);
			var e1 ="";
			if(document.getElementById("dispatchBasedOn").value==1){
				if(jsonObj.driver[i].varLt.substring(0,3)!=0.0){
					if(lastUpdate>10){
						if(jsonObj.driver[i].varSwh=="Y"){
							e1 = document.createTextNode(jsonObj.driver[i].varDr);	
						}else if(jsonObj.driver[i].varSwh=="N"){
							if(edZone!=""){
								e1 = document.createTextNode(jsonObj.driver[i].varDr+"("+edZone+")");	
							}else{
								e1 = document.createTextNode(jsonObj.driver[i].varDr);	
							}
						}else{
							e1 = document.createTextNode(jsonObj.driver[i].varDr+"*");
						}
					}else{
						if(jsonObj.driver[i].varSwh=="Y"){
							e1 = document.createTextNode(jsonObj.driver[i].varDr);	
						}else if(jsonObj.driver[i].varSwh=="N"){
							if(edZone!=""){
								e1 = document.createTextNode(jsonObj.driver[i].varDr+"("+edZone+")");	
							}else{
								e1 = document.createTextNode(jsonObj.driver[i].varDr);	
							}
						}else{
							e1 = document.createTextNode(jsonObj.driver[i].varDr);	
						}	
					}
				}else{
					e1 = document.createTextNode("!"+jsonObj.driver[i].varDr);	
				}
				cell.setAttribute('onClick', 'currentDriver('+jsonObj.driver[i].varDr+','+jsonObj.driver[i].varLt+','+jsonObj.driver[i].varLg+')');
				if(jsonObj.driver[i].DD=="NF"){
					cell.setAttribute('title',jsonObj.driver[i].varLT+'    AppVersion:'+jsonObj.driver[i].varAppV);
				}else{
					cell.setAttribute('title',jsonObj.driver[i].varLT+'   Distance:'+jsonObj.driver[i].DD);
				}
			}else{
				if(jsonObj.driver[i].varLt.substring(0,3)!=0.0){
					if(lastUpdate>10){
						if(jsonObj.driver[i].varSwh=="Y"){
							e1 = document.createTextNode(jsonObj.driver[i].varVNo);
						}else if(jsonObj.driver[i].varSwh=="N"){
							if(edZone!=""){
								e1 = document.createTextNode(jsonObj.driver[i].varVNo+"("+edZone+")");	
							}else{
								e1 = document.createTextNode(jsonObj.driver[i].varVNo);	
							}
						}else{
							e1 = document.createTextNode(jsonObj.driver[i].varVNo+"*");		
						}
					}else{
						if(jsonObj.driver[i].varSwh=="Y"){
							e1 = document.createTextNode(jsonObj.driver[i].varVNo);
						}else if(jsonObj.driver[i].varSwh=="N"){
							if(edZone!=""){
								e1 = document.createTextNode(jsonObj.driver[i].varVNo+"("+edZone+")");	
							}else{
								e1 = document.createTextNode(jsonObj.driver[i].varVNo);	
							}
						}else{
							e1 = document.createTextNode(jsonObj.driver[i].varVNo);	
						}
					}
				}else{
					e1 = document.createTextNode("!"+jsonObj.driver[i].varVNo);
				}

				cell.setAttribute('onClick', 'currentDriver('+jsonObj.driver[i].varDr+','+jsonObj.driver[i].varLt+','+jsonObj.driver[i].varLg+')');
				cell.setAttribute('title',jsonObj.driver[i].varLT);
				
				if(jsonObj.driver[i].DD=="NF"){
					cell.setAttribute('title',jsonObj.driver[i].varLT+'    AppVersion:'+jsonObj.driver[i].varAppV);
				}else{
					cell.setAttribute('title',jsonObj.driver[i].varLT+'   Distance:'+jsonObj.driver[i].DD);
				}

			}

			var e2 = document.createElement("input");
			e2.type = "hidden";
			e2.name="driveridDetails"+i;
			e2.id="driveridDetails"+i;
			e2.align='right'; 
			e2.value=jsonObj.driver[i].varDr;

			var e3 = document.createElement("input");
			e3.type = "hidden";
			e3.name="la_"+i;
			e3.id="la_"+i;
			e3.align='right'; 
			e3.value=jsonObj.driver[i].varLt;

			var e4 = document.createElement("input");
			e4.type = "hidden";
			e4.name="lo_"+i;
			e4.id="lo_"+i;
			e4.align='right'; 
			e4.value=jsonObj.driver[i].varLg;

			var e5 = document.createElement("input");
			e5.type = "hidden";
			e5.name="cab_"+i;
			e5.id="cab_"+i;
			e5.align='right'; 
			if(document.getElementById("dispatchBasedOn").value==1){
				e5.value=jsonObj.driver[i].varDr; 
			}else{
				e5.value=jsonObj.driver[i].varVNo;
			}
			var e6 = document.createElement("input");
			e6.type = "hidden";
			e6.name="avl_"+i;
			e6.id="avl_"+i;
			e6.align='right'; 
			e6.value=jsonObj.driver[i].varSwh;

			var e7 = document.createElement("input");
			e7.type = "hidden";
			e7.name="cab";
			e7.id="cab";
			e7.align='right'; 
			e7.value=jsonObj.driver[i].varVNo;

			var e8 = document.createElement("input");
			e8.type = "hidden";
			e8.name="driverProfile"+i;
			e8.id="driverProfile"+i;
			e8.align='right'; 
			e8.size="2";
			e8.value=jsonObj.driver[i].varDp;

			var e9 = document.createElement("input");
			e9.type = "hidden";
			e9.name="drName"+i;
			e9.id="drName"+i;
			e9.align='right'; 
			e9.size="2";
			e9.value=jsonObj.driver[i].varDrNa;

			var e10 = document.createElement("input");
			e10.type = "hidden";
			e10.name="drPhone"+i;
			e10.id="drPhone"+i;
			e10.align='right'; 
			e10.size="2";
			e10.value=jsonObj.driver[i].varPh;
			
			var eDist9 = document.createElement("input");
			eDist9.type = "hidden";
			eDist9.name="drDist"+i;
			eDist9.id="drDist"+i;
			eDist9.align='right'; 
			eDist9.size="2";
			eDist9.value=jsonObj.driver[i].DD;
			
			b.appendChild(v);
			cell.appendChild(b);

			if(lastUpdate>3){
				cell.id="noFeed";
			}else if(jsonObj.driver[i].varSwh=="Y"){
				cell.id="available";
			}else {
				cell.id="unavailable";
			}
			cell.appendChild(e2);
			cell.appendChild(e1);
			cell.appendChild(e3);
			cell.appendChild(e4);
			cell.appendChild(e5);
			cell.appendChild(e6);
			cell.appendChild(e7);
			cell.appendChild(e8);
			cell.appendChild(e9);
			cell.appendChild(e10);
			
			cell.appendChild(eDist9);
			
			var av = document.getElementById("avColor").value;
			$("#available").css('backgroundColor', '#' + av);

			var nav = document.getElementById("navColor").value;
			$("#unavailable").css('backgroundColor', '#' + nav);

			var nFeed = document.getElementById("noUpdate").value;
			$("#noFeed").css('backgroundColor', '#' + nFeed);
			document.getElementById("unloadDrivers").src="images/Dashboard/refresh1.png";
		}

		document.getElementById("msgDriver").innerHTML ="";
		document.getElementById("driverExpand").innerHTML ="";
		var root=document.getElementById('driverExpand');
		var tbo=document.createElement('tbody');
		var rowHead=document.createElement('tr');
		rowHead.style.background="#EFEFFB";
		var header1=document.createElement("th");
		var h1 = document.createTextNode("Driver");
		var header2=document.createElement("th");
		var h2 = document.createTextNode("Cab");
		var header3=document.createElement("th");
		var header20=document.createElement("th");
		var h20 = document.createTextNode("Name");
		var h3 = document.createTextNode("Status");
		var header4=document.createElement("th");
		var h4 = document.createTextNode("PhoneNo.");
		var header6=document.createElement("th");
		var h6 = document.createTextNode("Profile");
		var header8=document.createElement("th");
		var h8 = document.createTextNode("Total Jobs");
		/*var header7=document.createElement("th");
		var h7 = document.createTextNode("");
		 */
		var header9=document.createElement("th");
		var h9 = document.createTextNode("Start Zone");

		var header10=document.createElement("th");
		var h10 = document.createTextNode("End Zone");

		var header11=document.createElement("th");
		var h11 = document.createTextNode("Flag Trip (Y/N)");
		
		var headerDist=document.createElement("th");
		var hDist = document.createTextNode("Distance");
		
		var header99=document.createElement("th");
		var h99 = document.createTextNode("App Version");
		
		header1.appendChild(h1);
		header2.appendChild(h2);
		header20.appendChild(h20);
		header3.appendChild(h3);
		header4.appendChild(h4);
		header6.appendChild(h6);
		header8.appendChild(h8);

		//header7.appendChild(h7);

		header9.appendChild(h9);
		header10.appendChild(h10);
		header11.appendChild(h11);
		header99.appendChild(h99);
		
		headerDist.appendChild(hDist);
		
		rowHead.appendChild(header1);
		rowHead.appendChild(header2);
		rowHead.appendChild(header20);
		rowHead.appendChild(header3);
		rowHead.appendChild(header4);
		rowHead.appendChild(header6);
		rowHead.appendChild(header8);
		//rowHead.appendChild(header7);
		rowHead.appendChild(header9);
		rowHead.appendChild(header10);
		rowHead.appendChild(header11);
		rowHead.appendChild(headerDist);
		rowHead.appendChild(header99);
		tbo.appendChild(rowHead);
		var row, cell1,cell2,cell3,cell4,cell6;
		var index = "row";
		var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
		var iRowCount= oRows.length;
		for(var i=1;i<jsonObj.driver.length;i++){
			var flagtrip ="No Jobs";
			for(var k = 1; k < iRowCount; k++) {
				if(oRows[k].id==index){
					driverJob = oRows[k].getElementsByTagName("input")[11].value;
					if(driverJob == jsonObj.driver[i].varDr){
						stZone = oRows[k].getElementsByTagName("input")[14].value;
						edZone= oRows[k].getElementsByTagName("input")[15].value;
						cityV= oRows[k].getElementsByTagName("input")[6].value;
						if(cityV.indexOf("Flag Trip")!=-1){
							flagtrip = "yes";
						}else{
							flagtrip = "No";
						}
					}
				}
			}

			row=document.createElement('tr');
			row.style.background="#EFEFFB";
			row.id="expandRow";
			cell1=document.createElement('td');
			cell1.id="driveridDetailsExpand"+i;
			var inp = document.createTextNode(jsonObj.driver[i].varDr+'');
			inp.value=jsonObj.driver[i].varDr;
			cell1.appendChild(inp);
			cell2=document.createElement('td');
			var v = document.createTextNode(jsonObj.driver[i].varVNo+'');
			cell2.appendChild(v);

			cell40=document.createElement('td');
			var p10 = document.createTextNode(jsonObj.driver[i].varDrNa+'');
			cell40.appendChild(p10);

			cell3=document.createElement('td');
			var s;
			if(jsonObj.driver[i].varSwh=="Y"){
				s = document.createTextNode("Av");
			}else{
				s = document.createTextNode("NAv");
			}
			cell3.appendChild(s);
			cell4=document.createElement('td');
			var p = document.createTextNode(jsonObj.driver[i].varPh+'');
			cell4.appendChild(p);
			cell6=document.createElement('td');
			var pr = document.createTextNode(jsonObj.driver[i].varDp+'');
			cell6.appendChild(pr);

			var totalCount= document.createElement("td");
			totalCount.id="countValue"+i;
			var count = document.createTextNode("N/A");
			totalCount.appendChild(count);

			cell16=document.createElement('td');
			var stZ = document.createTextNode(stZone+'');
			cell16.appendChild(stZ);

			cell26=document.createElement('td');
			var edZ = document.createTextNode(edZone+'');
			cell26.appendChild(edZ);

			cell27=document.createElement('td');
			var flt = document.createTextNode(flagtrip+'');
			cell27.appendChild(flt);

			var cellcheck= document.createElement("td");
			var check = document.createElement("input");
			check.type="checkbox";
			check.id="selectDriver"+i;
			check.value=jsonObj.driver[i].varDr;
			cellcheck.appendChild(check);
			
			cellDist=document.createElement('td');
			var dist = document.createTextNode(jsonObj.driver[i].DD+'');
			cellDist.appendChild(dist);
			
			cell99=document.createElement('td');
			var appV = document.createTextNode(jsonObj.driver[i].varAppV+'');
			cell99.appendChild(appV);

			row.appendChild(cell1);
			row.appendChild(cell2);
			row.appendChild(cell40);
			row.appendChild(cell3);
			row.appendChild(cell4);
			row.appendChild(cell6);
			row.appendChild(totalCount);
			row.appendChild(cell16);
			row.appendChild(cell26);
			row.appendChild(cell27);
			
			row.appendChild(cellDist);
			
			row.appendChild(cell99);
			row.appendChild(cellcheck);

			tbo.appendChild(row);

		}

		root.appendChild(tbo);
		var tbRow = document.createElement("tr");
		var tbCell = document.createElement("td");
		var textMsgbutton = document.createElement("input");
		textMsgbutton.type="button";
		textMsgbutton.id="sendTxtMsg";
		textMsgbutton.value="Send Text Message";
		textMsgbutton.onclick=sendMsgToSelectedDrivers;
		tbCell.appendChild(textMsgbutton);
		tbRow.appendChild(tbCell);
		var tbRow1 = document.createElement("tr");
		var tbCell1 = document.createElement("td");
		var vMsgbutton = document.createElement("input");
		vMsgbutton.type="button";
		vMsgbutton.id="sendVMsg";
		vMsgbutton.value="Send Voice Message";
		vMsgbutton.onclick=sendvMsgToSelectedDrivers;
		tbCell1.appendChild(vMsgbutton);
		tbRow.appendChild(tbCell1);

		var expButtons = document.getElementById("msgDriver");
		expButtons.appendChild(textMsgbutton);
		expButtons.appendChild(vMsgbutton);

		mapPlotDriver('1');
	}else{
		document.getElementById("driverDetailDisplay").innerHTML ="";
	}
	document.getElementById("unloadDrivers").src="images/Dashboard/refresh1.png";
}
function drivers(responseText) {
	//var resp = responseText.split('###');
	//document.getElementById("drivers1").innerHTML = resp[0];
	getAllValuesForDashboard('01000');
	mapInitJobs('1');
	mapPlotDriver('1');
}
function removeTotalFlags(){
	$("#totalFlags").hide("slow");
}
function calculateTotalNoOfFlags(){
	$("#totalFlags").show("slow");
	var oRowsForTotal=document.getElementById("totalFlagsForCompany").getElementsByTagName("th");
	var iRowCountTotal= oRowsForTotal.length;
	var arraylistForTotal =[];
	for(var k=0;k<iRowCountTotal-1;k++){
		arraylistForTotal[k] =  oRowsForTotal[k].getElementsByTagName("input")[0].value;
	}
	for(var j=0;j<arraylistForTotal.length;j++){
		document.getElementById("shortValueForTotal"+j).innerHTML=0;
	}
	document.getElementById("noFlag").innerHTML=0;
	var oRows=document.getElementById("driverDetailDisplay").getElementsByTagName("td");
	var iRowCount= oRows.length;
	var flagName="";
	for(var i=0;i<iRowCount;i++){
		flagName=oRows[i].getElementsByTagName("input")[6].value;
		var flagNameArray = flagName.split(";");
		if(flagNameArray.length<2){
			var increValue = parseInt(document.getElementById("noFlag").innerHTML);
			document.getElementById("noFlag").innerHTML=increValue+1;
		}
		for(var p=0;p<flagNameArray.length;p++){
			for(var j=0;j<arraylistForTotal.length;j++){
				if(arraylistForTotal[j]==flagNameArray[p]){
					var valueForIncrement=0;
					valueForIncrement =parseInt(document.getElementById("shortValueForTotal"+j).innerHTML);
					document.getElementById("shortValueForTotal"+j).innerHTML = valueForIncrement+1;
				}
			}
		}
	}

}
var callerIdRefreshTimeOut;
function InitCI()
{
	callerIdRefreshTimeOut=setInterval(function(){callerIdDash();},3000);
}

function callerIdDash(){
	var ciType = document.getElementById("ciBtnType").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest){
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	document.getElementById("refreshCIButton").style.display="block";
	var tableAppend=document.getElementById("tblCallerIdDash");
	tableAppend.innerHTML="";
	var url = 'AjaxClass?event=callerId&status=yes';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	document.getElementById("refreshCIButton").style.display="none";
	var oRows = document.getElementById("driverDetailDisplay").getElementsByTagName("td");
	var iRowCount = oRows.length;
	var tableRows='<tr id="row" class="customerRow"> ';
	var btnValue="";
	for(var j=0;j<obj.address.length;j++){
		var phoneNumber=obj.address[j].P;
		if(ciType=="0"){
			btnValue = obj.address[j].L;
		} else {
			btnValue = obj.address[j].P;
		}
		tableRows=tableRows+'<td>';
		tableRows=tableRows+'<input type="hidden" id="phoneCI'+j+'" value="'+obj.address[j].P+'"/>';
		tableRows=tableRows+'<input type="hidden" id="nameCI'+j+'" value="'+obj.address[j].N+'"/>';
		var driverCalling = 0;
//		for ( var k = 0; k < iRowCount; k++) {
//		var driverPhone = oRows[k].getElementsByTagName("input")[8].value;
//		if(phoneNumber==driverPhone){
//		tableRows=tableRows+'<input type="hidden" id="driver'+j+'" value="'+oRows[k].getElementsByTagName('input')[0].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="drivers'+j+'" value="'+oRows[k].getElementsByTagName('input')[0].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="driverPicked'+j+'" value="'+oRows[k].getElementsByTagName('input')[0].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="lat'+j+'" value="'+oRows[k].getElementsByTagName('input')[1].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="lon'+j+'" value="'+oRows[k].getElementsByTagName('input')[1].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="latPicked'+j+'" value="'+oRows[k].getElementsByTagName('input')[1].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="lonPicked'+j+'" value="'+oRows[k].getElementsByTagName('input')[2].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="lats'+j+'" value="'+oRows[k].getElementsByTagName('input')[1].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="lons'+j+'" value="'+oRows[k].getElementsByTagName('input')[2].value+'"/>';
//		tableRows=tableRows+'<input type="hidden" id="lineNew'+j+'" value="'+obj.address[j].L+'"/>';
//		driverCalling=1;
//		k = iRowCount;
//		}
//		} 
		if(driverCalling==0){
			if(obj.address[j].S==1){
				tableRows=tableRows+'<input class="ccButtonsAct" type="button" id="callerIdButton'+j+'" value="'+btnValue+'" onclick="openDetailsDash('+j+','+obj.address[j].S+')" onmouseover="getPreviousAddress('+obj.address[j].S+','+j+')" onmouseout="timerStart()">';
				tableRows=tableRows+'<input type="hidden" id="acceptors'+j+'" value="'+obj.address[j].P+';'+obj.address[j].N+';'+obj.address[j].T+';'+obj.address[j].AT+';'+obj.address[j].A+'"/>';
				tableRows=tableRows+'<input type="hidden" id="lineNew'+j+'" value="'+obj.address[j].L+'"/>';
			} else if(obj.address[j].S==0){
				tableRows=tableRows+'<input class="ccButtons" type="button" id="callerIdButton'+j+'" value="'+btnValue+'" onclick="openDetailsDash('+j+','+obj.address[j].S+')" onmouseover="getPreviousAddress('+obj.address[j].S+','+j+')" onmouseout="timerStart()">';
				tableRows=tableRows+'<input type="hidden" id="acceptor'+j+'" value="'+obj.address[j].P+';'+obj.address[j].N+';'+obj.address[j].T+'"/>';
				tableRows=tableRows+'<input type="hidden" id="lineNew'+j+'" value="'+obj.address[j].L+'"/>';
			}else if(obj.address[j].S==9){
				tableRows=tableRows+'<input class="ccButtonsPkd" type="button" id="callerIdButton'+j+'" value="'+btnValue+'" onclick="openDetailsDash('+j+','+obj.address[j].S+')" onmouseover="getPreviousAddress('+obj.address[j].S+','+j+')" onmouseout="timerStart()">';
				tableRows=tableRows+'<input type="hidden" id="acceptorPicked'+j+'" value="'+obj.address[j].P+';'+obj.address[j].N+';'+obj.address[j].T+'"/>';
				tableRows=tableRows+'<input type="hidden" id="lineNew'+j+'" value="'+obj.address[j].L+'"/>';
			}
		} else {
			tableRows=tableRows+'<input class="ccButtons" type="button" id="callerIdButton'+j+'" value="'+btnValue+'('+oRows[k].getElementsByTagName('input')[3].value+')'+'" onclick="openDetailsDash('+j+','+obj.address[j].S+')" onmouseover="getPreviousAddress('+obj.address[j].S+','+j+')" onmouseout="timerStart()">';
		}
		tableRows=tableRows+'</td>';
	}
	tableRows=tableRows+'</tr>';
	tableAppend.innerHTML=tableRows;
}	
function showDriverScreen() {
	$("#drivers1").slideToggle("slow");
	$("#right-block").draggable({
		handle : '.block-top',
		scroll : false
	});
}

idleTime = 0;
$(document).ready(function() {
	//Increment the idle time counter every minute.
	var idleInterval = setInterval("timerIncrement()", 60000); // 1 minute
	//Zero the idle timer on mouse movement.
	$(this).mousemove(function(e) {
		idleTime = 0;
	});
	$(this).keypress(function(e) {
		idleTime = 0;
	});
});
function timerIncrement() {
	idleTime = idleTime + 1;
	if (idleTime > 30) { // 20 minutes
		sumbitValueFromDash();
	}
}
function splitFromGroup(tripId,routeNo,pickup){
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=splitTripFromGroup&tripId='+tripId+'&routeNo='+routeNo+'&pickup='+pickup;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!=0){
		showSharedSummary();
	}
}

function changeFleetDash(fleet,i) {
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = 'SystemSetupAjax?event=changeFleet&fleet=' + fleet;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text.indexOf("Successfully")<0){
		alert(text);
	}
	window.location.reload();
	return;
	/*readCookieForCenter();
	document.getElementById("changedFleet").value=fleet;
	document.getElementById("assoccode").value=fleet;
	//onloadDash();
//	if(document.getElementById("changeHappeningJob").value=="false" && document.getElementById("changeHappeningDriver").value=="false" && document.getElementById("changeHappeningZone").value=="false"){
	Init();
	getAllValuesForDashboard('11111');  
//	getAllValuesForDashboard('01000');  
//	if( $('#queueid').is(':visible') ) {
//		getAllValuesForDashboard('00100');
//	}
	if($('#sharedDetailsPopUp').is(':visible')){
		showSharedSummary();
	}
	if($('#sharedRidePopUp').is(':visible')){
		getSharedJobs();
	}
//	}
	$(".fleetbutton").css('backgroundColor', '');
	$("#fleet_"+i).css('backgroundColor', 'green');
	document.getElementById("fleetForOR").value=document.getElementById("changedFleet").value;		
	//$("#color").val($("#fleetcolor_"+i).val());
	//document.getElementById().style.backgroundColor = color;
	document.getElementById("top-header").style.backgroundColor = "#"+$("#fleetcolor_"+i).val();*/			
	//changeCompanyLogo();
	//location.reload();
}

$("#right-block").draggable({
	handle : '.block-top',
	scroll : false
});
function changeFleetWithRefresh(){


}
function animateCircle() {
	var count = 0;
	window.setInterval(function() {
		count = (count + 1) % 200;

		var icons = myCity.get('icons');
		icons[0].offset = (count / 2) + '%';
		myCity.set('icons', icons);
	}, 20);
}
var myCity;
var stopUpAgain="0";
function notifyMessage(){

	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	var url = 'DashBoard?event=messageHistory&update=1&lastKey='+document.getElementById("lastNotifiMsgValue").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;
	//alert(text);
	obj = "{\"msg\":"+text+"}" ;
	var jsonObj = JSON.parse(obj.toString());
	var $tbl =document.getElementById("showNotiMsg");
	//$tbl.innerHTML ="";
	if(jsonObj.msg[0]!=null){
		if(jsonObj.msg[0].EMC!=null&&jsonObj.msg[0].EMC!=1){
			/*if(!$("#marqImage").is(':visible')){
		$("#showNotiMsg").append('<a style="vertical-align:text-top"><img id="marqImage" src="images/Dashboard/getACab.gif" style="width:63px;height:33px;" /></a></t>');
	}*/
			for(var i=0;i<jsonObj.msg.length;i++){
				if(i<3){
					$("#showNotiMsg").append( "<f style='background-color:lightgray;border:1px solid gray;border-radius:8px;height:100%'><a style='background-color:#64FE2E;color:red;text-shadow:2px 2px yellow;vertical-align:text-top;font-size:10px'>New!</a><a style='background-color:yellow;color:black'>"+jsonObj.msg[i].sender+"</a><b color='yellow'>-</b><a style='background-color:lightgreen;color:black' onClick=showPopUp('"+jsonObj.msg[i].sender+"','"+jsonObj.msg[i].msgText+"')>"+jsonObj.msg[i].msgText+"</a><b color='yellow'>-</b><a style='background-color:#95B9C7;color:black;'>"+jsonObj.msg[i].dri+"</a> </t></f>" );
				}else{
					$("#showNotiMsg").append( "<f style='background-color:lightgray;border:1px solid gray;border-radius:8px;height:100%'> <a style='background-color:yellow;color:black'>"+jsonObj.msg[i].sender+"</a><b color='yellow'>-</b><a style='background-color:lightgreen;color:black' onClick=showPopUp('"+jsonObj.msg[i].sender+"','"+jsonObj.msg[i].msgText+"')>"+jsonObj.msg[i].msgText+"</a><b color='yellow'>-</b><a style='background-color:#95B9C7;color:black;'>"+jsonObj.msg[i].dri+"</a> </t> </f>" );
				}
			}
			//alert("check key value:"+jsonObj.msg[0].key);

			if(jsonObj.msg.length>0){
				if(lastNotifiKeyValue==jsonObj.msg[0].key){
					lastNotifiCount = lastNotifiCount +1;
				}else{
					if(document.getElementById("neverShowFooter").value=="0"){
						$("#footerDiv").show();
					}
					document.getElementById("lastNotifiMsgValue").value=jsonObj.msg[0].key;
					lastNotifiKeyValue = jsonObj.msg[0].key;
					lastNotifiCount =0;
				}
			}else{
				lastNotifiCount = lastNotifiCount +1;
			}
			if(lastNotifiCount>180){
				//alert("Timer for Notification Messages are stopped !!,Please click the show icon to start again !!!");
				$('#marqueueTd').hide("slide", 1000 );

				//clearInterval(myTimer);
			}	
		}else{
			
			//alert(text);
			//$("#EmergencyDiv").jqm();
			//$("#EmergencyDiv").jqmShow();
			if($("#EmergencyDiv").is(':visible') || stopUpAgain=="1"){
				return;
			}
			
			var mapDr ;
			var cab ="null";
			var lat ="null";
			var longi ="null";
			document.getElementById("msgIdEmc").value = jsonObj.msg[0].key;
			var image = "images/Dashboard/taxiDriver.png";
			$("#driverEmc").val(jsonObj.msg[0].sender);
			document.getElementById("emcSender").value = jsonObj.msg[0].sender;
			document.getElementById("emcReciever").value = jsonObj.msg[0].dri;
			var oRows = document.getElementById("driverDetailDisplay").getElementsByTagName("td");
			var iRowCount = oRows.length;
			for ( var j = 0; j < iRowCount; j++) {
				driver = oRows[j].getElementsByTagName("input")[0].value;
				if(jsonObj.msg[0].sender==driver){
					cab = oRows[j].getElementsByTagName("input")[3].value;
					lat = oRows[j].getElementsByTagName("input")[1].value;
					longi = oRows[j].getElementsByTagName("input")[2].value;
					continue;
				}
			}

			document.getElementById("dragend").value="false";
			if(lat != "null" || longi !="null"){
				emergencyZoom(lat,longi);
			}
			
			autoRefreshOff();
			tempObj=jsonObj.msg[0].sender;
			$("#emcDetails").html("<a style='color:red'>"+jsonObj.msg[0].msgText+"</a></t><a style='color:green'>Sender:</a><input type=\"button\" id=\"driverLoc\" value=\""+jsonObj.msg[0].sender+"\" onclick=\"getDriverLoc()\"/>");
			var contentString = $("#emcDetails").html();
			timerForindividualMSgs();

			$('#EmergencyDiv').bPopup({
				modalClose: false,
				opacity: 0.2,
				positionStyle: 'fixed'
			});
			
			//alert("Emergency shows :"+contentString);
			//document.getElementById("dashboardBody").innerHTML ="Driver has Emergency:"+jsonObj.msg[0].sender;

		}
	}
	//if(!$("#footerDiv").is(':visible')){
	//clearInterval(myTimer);
	//}

}
function venki(driver){
	for ( var i = 0; i < globalDrivers.driverList.length; i++) {
		if (globalDrivers.driverList[i].varDr == driver) {
			cur(globalDrivers.driverList[i].varDr,globalDrivers.driverList[i].vatLat,globalDrivers.driverList[i].varLon);
			break;
		}
	}

}

function getDriverLoc(){
	var sender=tempObj.split("-");
	tempObj="";
	stopUpAgain="1";
	var emcMsgDriver=sender[0];
	clearInterval(indTimer);cancelMap();Init();
	for(var i=0;i<document.getElementById("noOfDriver").value;i++){
		if(emcMsgDriver == document.getElementById("driveridDetails"+i).value){
			setTimeout(startTime,1000);
			break;
		}
	}
}

function startTime(){
	currentDriver(document.getElementById("driveridDetails"+i).value, document.getElementById("la_"+i).value, document.getElementById("lo_"+i).value);

}
function notifyMobmessage(){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	var url = 'DashBoard?event=mobilesms&update=1&lastKey='+document.getElementById("lastNotifiSMSValue").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;
	obj = "{\"msg\":"+text+"}" ;
	//alert(obj);
	var jsonObj = JSON.parse(obj.toString());
	if(jsonObj.msg[0]!=null){
		if($("#Msgnotification").is(':visible')){
			return;
		}
		document.getElementById("msgIdSms").value = jsonObj.msg[0].key;
		var tbl;
		$(document).ready(function(){
			$("#SmsDetails").remove();
			$("#msNotification").html(" ");
			$("#msNotification").append('<table border="1" width="70%" cellspacing="1" cellpadding="1" id="SmsDetails">');
			$("#SmsDetails").append('<thead bgcolor="#00FFFF"><tr><th>PNO</th><th>Message</th><th>Create</th><th>Status</th></tr></thead>');
		});
		for(var i=0;i<jsonObj.msg.length;i++){

			tbl=$('<tr/>');
			tbl.append('<td bgcolor="#F0F8FF" style="max-width:10%;width:5px;">'+jsonObj.msg[i].Pno+'</td>');				
			// tbl.append('<td bgcolor="#F0F8FF" style="max-width:100px"><a><span class="more">'+jsonObj.msg[i].mess+'</span></a></td>');
			tbl.append('<td bgcolor="#F0F8FF" style="max-width:100%">'+jsonObj.msg[i].mess+'</td>');
			tbl.append('<td bgcolor="#F0F8FF" style="max-width:10%;width:15px;"><input type="button" name="create" id="create" value="createJob"  onclick="createJobORDashboard('+jsonObj.msg[i].Pno+')"></input></td>');				
			tbl.append('<td bgcolor="#F0F8FF" style="max-width:10%;width:5px;"><input type="button" name="closeSMS" id="closeSMS" value ="Ok" onclick="closePopUpSMS('+jsonObj.msg[0].key+');"></td>');
			$('#SmsDetails').append(tbl);
		}
		var contentString = $("#msNotification").html();
		$('#Msgnotification').bPopup({
			modalClose: true,
			opacity: 0.2,
			positionStyle:'fixed' //'fixed' or 'absolute'
		});

	}
}


function showsmsHistory(){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	var url = 'DashBoard?event=mobilesmsHistory';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;
	obj = "{\"msg\":"+text+"}" ;
	//alert(obj);
	var jsonObj = JSON.parse(obj.toString());
	if(jsonObj.msg[0]!=null){
		if($("#Msgnotification").is(':visible')){
			return;
		}
		var tbl;
		$(document).ready(function(){
			$("#SmsDetails").remove();
			$("#msNotification").html(" ");
			$("#msNotification").append('<table border="1" width="70%" cellspacing="1" cellpadding="1" id="SmsDetails">');
			$("#SmsDetails").append('<thead bgcolor="#00FFFF"><tr><th>PNO</th><th>Message</th><th>Entered Time</th></tr></thead>');
		});
		for(var i=0;i<jsonObj.msg.length;i++){
			tbl=$('<tr/>');
			tbl.append('<td bgcolor="#F0F8FF" style="max-width:10%;width:5px;">'+jsonObj.msg[i].Pno+'</td>');				
			tbl.append('<td bgcolor="#F0F8FF" style="max-width:100%">'+jsonObj.msg[i].mess+'</td>');
			tbl.append('<td bgcolor="#F0F8FF">'+jsonObj.msg[i].time+'</td>');
			$('#SmsDetails').append(tbl);
		}
		var contentString = $("#msNotification").html();
		$('#Msgnotification').bPopup({
			modalClose: true,
			opacity: 0.2,
			positionStyle:'fixed' //'fixed' or 'absolute'
		});

	}
}

function sendReplySms(){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	var driver_id=document.getElementById("driver_Reply").value;
	var message=document.getElementById("msgValue").value;
	var url = 'control?action=openrequest&event=sendSMS';
	if(driver_id!=""){
		url=url+'&driver_id='+driver_id+'';
	}

	if(message!=""){
		url=url+'&message='+message;

		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text=="1"){
			document.getElementById("msgValue").value="";
			$("#replyStatus").html("Message Sent SuccessFully !!");
		}else{
			$("#replyStatus").html("Message Sending Failed !! ");
		}
		//var ajax = new AjaxInteraction(url,responseSms);
		//ajax.doGet();
		//messageHistoryShow();
	}
}

function showNotifications(){
	$("#footerDiv").slideToggle("slow");
	document.getElementById("neverShowFooter").value="0";
	if($("#footerDiv").is(':visible')){
		checkMessages();
	}
}
function removeFooter(){
	$("#footerDiv").slideToggle("slow");
	document.getElementById("neverShowFooter").value="1";
}


function sendEmcSms(){
	var xmlhttp = null;	
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	var driver_id=document.getElementById("driverEmc").value;
	var message=document.getElementById("replyToEmc").value;
	var url = 'control?action=openrequest&event=sendSMS';
	if(driver_id!=""){
		url=url+'&driver_id='+driver_id+'';
	}

	if(message!=""){
		url=url+'&message='+message;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text=="1"){
			document.getElementById("replyToEmc").value="";
			$("#statusResEmc").html("Message Sent SuccessFully !!");
		}else{
			$("#statusResEmc").html("Message Sending Failed !! ");
		}
		//var ajax = new AjaxInteraction(url,responseSms);
		//ajax.doGet();
		//messageHistoryShow();
	}
}

function sendReplySms(){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	var driver_id=document.getElementById("driver_Reply").value;
	var message=document.getElementById("msgValue").value;
	var url = 'control?action=openrequest&event=sendSMS';
	if(driver_id!=""){
		url=url+'&driver_id='+driver_id+'';
	}

	if(message!=""){
		url=url+'&message='+message;
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="1"){
		document.getElementById("msgValue").value="";
		$("#replyStatus").html("Message Sent SuccessFully !!");
	}else{
		$("#replyStatus").html("Message Sending Failed !! ");
	}
	//var ajax = new AjaxInteraction(url,responseSms);
	//ajax.doGet();
	//messageHistoryShow();
}

function showOrHideMarqueue(){
//	if($('#marqueueTd').is(':visible')){
//	$('#marqueueTd').hide("slide", 1000 );
//	clearInterval(myTimer);
//	}else{
//	checkMessages();
//	$('#marqueueTd').show("slide", 1000 );
//	}
	$("#footerDiv").slideToggle("slow");
	if($("#footerDiv").is(':visible')){
		checkMessages();
	}
}

var lastNotifiKeyValue =0;
var lastNotifiCount =0;
var myTimer =0;
function closePopUpEMC(){
	clearInterval(indTimer);
	stopUpAgain="0";
	$('#EmergencyDiv').bPopup().close();
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
		}
	}
	var url = 'DashBoard?event=makeMsgRead&msgId='+document.getElementById("msgIdEmc").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
}


function checkMessages(){
	if($("#footerDiv").is(':visible')){
		myTimer = setInterval(function(){notifyMessage()},4000);

	}
}

function checkMobmessages(){
	myTimer = setInterval(function(){notifyMobmessage()},5000);
}

function closePopUpSMS(key){
	clearInterval(indTimer);
	$('#Msgnotification').bPopup().close();
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'DashBoard?event=makeSmsRead&msgId='+key;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;

}


function timerForindividualMSgs(){
	indTimer = setInterval(function(){showIndividualMessages()},60000);
}
function showIndividualMessages(){
	alert("showIndividualMessages");
	document.getElementById("showIndividualMsg").innerHTML ="";
	var xmlhttp = null;	
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = '/TDS/DashBoard?event=individualMsgHistory&sender='+document.getElementById("emcSender").value+'&receiver='+document.getElementById("emcReciever").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	obj = "{\"msg\":"+text+"}" ;
	var jsonObj = JSON.parse(obj.toString());
	var $tbl =document.getElementById("showIndividualMsg");
	for(var i=0;i<jsonObj.msg.length;i++){
		$("#showIndividualMsg").append( "<f style='background-color:lightgray;border:1px solid gray;border-radius:8px;height:100%'><a style='background-color:#64FE2E;color:red;text-shadow:2px 2px yellow;vertical-align:text-top;font-size:10px'>New!</a><a style='background-color:yellow;color:black'>"+jsonObj.msg[i].sender+"</a><b color='yellow'>-</b><a style='background-color:lightgreen;color:black' onClick=showPopUp('"+jsonObj.msg[i].sender+"','"+jsonObj.msg[i].msgText+"')>"+jsonObj.msg[i].msgText+"</a><b color='yellow'>-</b><a style='background-color:#95B9C7;color:black;'>"+jsonObj.msg[i].dri+"</a> </t></f>" );
	}
}
function callActiveDrivers(){
	var xmlhttp = null;	
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'RegistrationAjax?event=callActiveDrivers';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
}

function downloadFile(routeNum){
	window.open("ReportController?event=reportEvent&ReportName=manifestJobs&routeNum="+routeNum+"&output=pdf");
}

var indTimer;
var lastNotifiKeyValue =0;
var lastNotifiCount =0;
var myTimer =0;




function checkjobs(tripid){
	autoRefreshOff();
	acdata=acdata+tripid+";";
	document.getElementById('tripForanother1').value = acdata;
	$("#tripForanother1").val(acdata);
	$("#option").show();
}



function removeScreen(){
	$("#option").hide();
	createCookieForAutoRefreshOnOff('on');Init();

}


function changeMfleet(){
	$("#changeFleetjob").show();
}

function ChangeFleetToMulJob() {
	var fleetNo = document.getElementById("fleetCV").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = '/TDS/DashBoard?event=changeMultipleJobToAnotherFleet&tripId=' + acdata + '&fleetNo=' + fleetNo	;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		$("#changeFleetjob").hide();
		$("#option").hide();
		createCookieForAutoRefreshOnOff('on');Init();
		alert("Job Send to to another Fleet");
		acdata=0;
		//ORSummary(0);
	}

}

function Changezone(){
	$("#ChangeZone").show();
	//acdata=0;

}
function changeZone(){
	var zoneid = document.getElementById("zone").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=changeZoneForDash&tripid='+ acdata + '&zoneId=' + zoneid;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		$("#ChangeZone").hide();
		$("#option").hide();
		createCookieForAutoRefreshOnOff('on');Init();
		alert("zone Changed successfully");
		acdata=0;
		//ORSummary(0);
	}

}
function remZone(){
	$("#ChangeZone").hide();
}
function remFleet(){
	$("#changeFleetjob").hide();
}

function reDispatchjob(){
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=redispatchMjob&tripid='+ acdata ;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		$("#option").hide();
		createCookieForAutoRefreshOnOff('on');Init();
		//alert("success");
		acdata=0;
	}else{
		createCookieForAutoRefreshOnOff('on');Init();
		acdata=0;

	}
	createCookieForAutoRefreshOnOff('on');Init();
	acdata=0;

}
function cCancel(){
	var answer = prompt("Reason for cancelling this job ?","");
	if(answer!=null&&answer!=""){
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = '/TDS/DashBoard?event=deleteMJob&tripid='+ acdata+ '&answer='+answer;
		//alert(url);
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == 1) {
			$("#option").hide();
			acdata=0;
			createCookieForAutoRefreshOnOff('on');Init();

		}else{
			createCookieForAutoRefreshOnOff('on');Init();
			acdata=0;

		}  }
	createCookieForAutoRefreshOnOff('on');Init();
	acdata=0;

}
function opeCancel(){
	var answer = prompt("Reason for cancelling this job ?","");
	if(answer!=null&&answer!=""){

		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=operCancel&tripId='+ acdata+ '&answer='+answer ;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == 1) {
			$("#option").hide();
			acdata=0;
			createCookieForAutoRefreshOnOff('on');Init();

		}  else{
			createCookieForAutoRefreshOnOff('on');Init();
			acdata=0;

		}	
	}    	
}

function driverCompleted(){
	var answer = prompt("Reason for cancelling this job ?","");
	if(answer!=null&&answer!=""){
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=drivComplete&tripId='+ acdata+ '&answer='+answer ;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == 1) {
			$("#option").hide();
			createCookieForAutoRefreshOnOff('on');Init();
			acdata=0;
		}else{ 	
			createCookieForAutoRefreshOnOff('on');Init();
			acdata=0;
		}
	}
}
function changeRatingJobs(){
	$("#changeRatings").show();

}
function changeRating(){
	var rating=document.getElementById("jobRates").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=changeRatingJobs&tripid='+ acdata+ '&ratings='+rating ;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		$("#option").hide();
		$("#changeRatings").hide();

		createCookieForAutoRefreshOnOff('on');Init();
		//alert("success");
		acdata=0;
	}else{
		createCookieForAutoRefreshOnOff('on');Init();
		acdata=0;

	}
	createCookieForAutoRefreshOnOff('on');Init();
	acdata=0;


}
function remDntDispatchjobs(){
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();	
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=remDntDispatchjobs&tripid='+ acdata ;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		$("#option").hide();
		createCookieForAutoRefreshOnOff('on');Init();
		//alert("success");
		acdata=0;
	}else{
		createCookieForAutoRefreshOnOff('on');Init();
		acdata=0;

	}
	createCookieForAutoRefreshOnOff('on');Init();
	acdata=0;

}
function showSearchDash(type){
	if(type=="1"){
		document.getElementById("jobSear").value="";
		$("#searchForJobDash" ).jqm();
		$("#searchForJobDash" ).jqmShow();
	} else {
		if(document.getElementById("jobSear").value!=""){
			var curdate = new Date();
			var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();		
			var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
			var cYear = curdate.getFullYear();

			var prevDate = new Date();
			prevDate.setDate(prevDate.getDate() - 7);
			var fDate = prevDate.getDate() >= 10 ? prevDate.getDate() : "0"+prevDate.getDate();		
			var fMonth = prevDate.getMonth()+1 >=10 ? prevDate.getMonth()+1 : "0"+(prevDate.getMonth()+1);
			var fYear = prevDate.getFullYear();

			var fromDate =fMonth+"/"+fDate+"/"+fYear;
			var toDate =cMonth+"/"+cDate+"/"+cYear;

			var xmlhttp = null;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'RegistrationAjax?event=searchJobs&value='+document.getElementById("jobSear").value+'&fD='+fromDate+'&tD='+toDate;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			var jsonObj = "{\"address\":"+text+"}" ;
			var obj = JSON.parse(jsonObj.toString());
			var tbRows = "";
			var tableAppend=  document.getElementById("myJobsToSearchDash");
			document.getElementById("myJobsToSearchDash").innerHTML="";
			if(obj.address[0].summ.length>0){
				tbRows = tbRows + '<thead><tr><th colspan="7"><center>Current & Future Jobs</center></th><tr><th>TripId</th><th>Name</th><th>Date</th><th>PU&nbsp;Address</th><th>DO&nbsp;Address</th><th>R&nbsp;No.</th><th>Driver</th><th>Status</th></tr></thead><tbody>';
				for(var i=0;i<obj.address[0].summ.length;i++){
					tbRows=tbRows+'<tr>';
					tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].TI+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].N+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].D+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].SA+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].EA+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].RN+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].DID+'-'+obj.address[0].summ[i].VNO+'</td></center>';
					var status="";
					if(obj.address[0].summ[i].S=="30"){status="Broadcast";
					}else if(obj.address[0].summ[i].S=="4"){status="Dispatching";
					}else if(obj.address[0].summ[i].S=="8"){status="Dispatch Not Started";
					}else if(obj.address[0].summ[i].S=="40"){status="Allocated";
					}else if(obj.address[0].summ[i].S=="43"){status="On Rotue To Pickup";
					}else if(obj.address[0].summ[i].S=="50"){status="Customer Cancelled Request";
					}else if(obj.address[0].summ[i].S=="51"){status="No Show";
					}else if(obj.address[0].summ[i].S=="49"){status="Soon To Clear";
					}else if(obj.address[0].summ[i].S=="4"){status="Dispatching";
					}else if(obj.address[0].summ[i].S=="0"){status="Unknown";
					}else if(obj.address[0].summ[i].S=="47"){status="Trip Started";
					}else if(obj.address[0].summ[i].S=="99"){status="Trip On Hold";
					}else if(obj.address[0].summ[i].S=="3"){status="Couldnt Find Drivers";
					}else if(obj.address[0].summ[i].S=="70"){status="Payment Received";
					}else if(obj.address[0].summ[i].S=="90"){status="SrHold Job";
					}else if(obj.address[0].summ[i].S=="48"){status="Driver Reporting NoShow";}

					tbRows=tbRows+'<td><center>'+status+'</td></center>';
					tbRows=tbRows+'</tr>';
				}
			}
			if(obj.address[0].histy.length>0){
				tbRows = tbRows + '<thead><tr><td colspan="7"><center>History Jobs</center></td><tr><th>TripId</th><th>Name</th><th>Date</th><th>St&nbsp;Address</th><th>Ed&nbsp;Address</th><th>R&nbsp;No.</th><th>Driver</th><th>Status</th></tr></thead><tbody>';
				for(var i=0;i<obj.address[0].histy.length;i++){
					tbRows=tbRows+'<tr>';
					tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].TI+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].N+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].D+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].SA+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].EA+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].RN+'</td></center>';
					tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].DID+'-'+obj.address[0].histy[i].VNO+'</td></center>';
					var status="";
					if(obj.address[0].histy[i].S=="30"){status="Broadcast";
					}else if(obj.address[0].histy[i].S=="40"){status="Allocated";
					}else if(obj.address[0].histy[i].S=="43"){status="On Rotue To Pickup";
					}else if(obj.address[0].histy[i].S=="50"){status="Customer Cancelled Request";
					}else if(obj.address[0].histy[i].S=="51"){status="No Show";
					}else if(obj.address[0].histy[i].S=="49"){status="Soon To Clear";
					}else if(obj.address[0].histy[i].S=="4"){status="Dispatching";
					}else if(obj.address[0].histy[i].S=="0"){status="Unknown";
					}else if(obj.address[0].histy[i].S=="47"){status="Trip Started";
					}else if(obj.address[0].histy[i].S=="99"){status="Trip On Hold";
					}else if(obj.address[0].histy[i].S=="3"){status="Couldnt Find Drivers";
					}else if(obj.address[0].histy[i].S=="70"){status="Payment Received";
					}else if(obj.address[0].histy[i].S=="90"){status="SrHold Job";
					}else if(obj.address[0].histy[i].S=="61"){status="Trip Completed";
					}else if(obj.address[0].histy[i].S=="48"){status="Driver Reporting NoShow";}

					tbRows=tbRows+'<td><center>'+status+'</td></center>';
					tbRows=tbRows+'</tr>';
				}
			}
			tableAppend.innerHTML=tbRows;
		} else {
			alert("Please give some value & search");
		}
	} 
}
function removeSearchScreenDash(){
	document.getElementById("myJobsToSearchDash").innerHTML="";
	$("#searchForJobDash").jqmHide();
}
