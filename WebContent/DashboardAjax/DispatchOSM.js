

var polygonGlobal=[];
	var labelGlobal=[];
	var totalOptions = 1;
	var reqCounter =0;
	var globalJsonForDriver = "";
function AjaxInteraction(url, callback) {
	var req = init();
	req.onreadystatechange = processRequest;
	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	function processRequest() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				if (callback)
					callback(req.responseText);
			}
		}

	}
	this.doGet = function() {
		req.open("GET", url, true);
		req.timeout = 5000;
		reqCounter++;
		if(reqCounter>3){
			req.ontimeout = timeoutFired;
			reqCounter=0;
		}
		req.send(null);
	}
	function timeoutFired(){
		//alert("request time out");
		var cell = document.createTextNode(""+currentTimeCalculate()+":>Request Time Out:");
		var cellBr = document.createElement("br");
		document.getElementById("console").appendChild(cellBr);
		document.getElementById("console").appendChild(cell);
	}
}


function map(lat, lang) {
}

	    function cancelMap(){
	    	document.getElementById("currentJobinJObs").value="";
	    	document.getElementById("currentDriverinJObs").value="";
	    	document.getElementById("currentDriverinDrivers").value="";
	    	document.getElementById("dragend").value="false";
	    	$("#cancelButton").hide("slow");
	    	//readCookieForCenter();
	    	mapInitJobs('2');
	    	mapPlotDriver('2');
	    }

	    var cssObj = {
	    		'opacity' : '0.9',
	    		'filter' : 'alpha(opacity=50)'
	    };
	    var cssHowerObj = {
	    		'font-weight' : 'bolder',
	    		'opacity' : '0.5',
	    		'filter' : 'alpha(opacity=50)'
	    };
		$(document).ready(function() {
			$("#consoleUp").draggable({
				handle: '#dragConsole',scroll: false
			});
			$("#bullHorn").draggable({handle:'.dragBullHorn',scroll: false });
			$("#vMessagePopUp").draggable({handle:'.dragMike',scroll: false });
			$("#expandDiv").draggable({ scroll: false }).resizable();
			$("#left-block5").draggable({ scroll: false }).resizable();
			$("#right-block5").draggable({ scroll: false }).resizable();
			$("#orderPopUpSub").draggable({ scroll: false });
			$("#sharedDetailsPopUp").draggable({handle:'.block-topShDetails',scroll: false });
			$("#sharedRidePopUp").draggable({handle:'.block-topShared',scroll: false});
			$("#totalFlags").draggable({ scroll: false });
			$("#jobHistoryDiv").draggable({ scroll: false });
			$("#jobLogsDiv").draggable({ scroll: false });
			$("#bookToZone").draggable({ scroll: false });
		});
		
		$(document).ready(function() {
			loadDate();
			//$("#vMessagePopUp").draggable({
				//handle: '.bullHornV'
			//});
			//$("#bullHorn").draggable({
				//handle: '.bullHorn'
			//});
			$("#jobsOnThatDay").draggable({ scroll: false });
			$("#compLogo").draggable({ scroll: false });
			
			$(".Jobs").click(function(){
				$("#openjobs").slideToggle("slow");
				$("#plain").draggable({ 
					handle: '#jobsTopDrag',scroll: false 
				});
			});
			$(".Driver").click(function(){
				$("#drivers1").slideToggle("slow");
				$("#right-block").draggable({
					handle: '.block-top',scroll: false 
				});
			}); 
			$(".zones").click(function(){
				$("#queueid").slideToggle("slow");
				$("#queueid").draggable({
					handle: '.block-top1',scroll: false 
				});
			});
			$(".ORHistory").click(function(){
				$("#ORHistory").jqm({modal:true});
				$("#ORHistory").jqmShow();
				$("#ORHistory").draggable({
					containment:'#dashboardBody',handle: '.headerRowORH',scroll: false 
				});
			});
			$(".ORSummary").click(function(){
				/*$("#ORSummary").jqm();
				$("#ORSummary").jqmShow();*/
				ORSummary(0);
				$("#ORSummary").draggable({
					handle: '.orSummaryRow',scroll: false 
				});
			});

			$(".options").click(function(){
//				$("#OptionsForAll").slideToggle("slow");
//				$("#OptionsForAll").draggable({
//					handle:'.block-top55',scroll: false });
				$("#OptionsForAll").jqm();
				$("#OptionsForAll").jqmShow();
			});
			$(".Message").click(function(){
				$("#sendSms").slideToggle("slow");
				$("#sendSms").draggable({scroll: false });
			});
			$(".operationStatistics").click(function(){
				jobCount();
				$("#statistics").slideToggle("slow");
				$("#statistics").draggable({scroll: false });
			});
			$(".openjobs a").hover(function() {
				$(this).find("em").animate({
					opacity : "show",
					left: "115"
				}, "slow");
			}, function() {
				$(this).find("em").animate({
					opacity : "hide",
					left: "105"
				}, "fast");
			});
			
			InitCI();
		
		});
		
			
var polygonZonesGlobal =[];
var labelGlobal =[];
var latLongList ="";
function showAllZonesForDash(){
	var img=document.getElementById("zoneCheckImage").src;
	var fillColors =[];
	if(img.indexOf("zoneLoginRed.png")!= -1){
		document.getElementById("zoneCheckImage").src="images/Dashboard/zoneLogin.gif";
		for(var i=0;i<polygonZonesGlobal.length;i++){
			polygonZonesGlobal[i].setMap(null);
			labelGlobal[i].setMap(null);
		}
		polygonZonesGlobal =[];
		labelGlobal =[];
	}else{
		var zone1 = document.getElementById("zoneColor1").value;
		var zone2 = document.getElementById("zoneColor2").value;
		var zone3 = document.getElementById("zoneColor3").value;
		var zone4 = document.getElementById("zoneColor4").value;
		var zone5 = document.getElementById("zoneColor5").value;
		var zone6 = document.getElementById("zoneColor6").value;
		var zone7 = document.getElementById("zoneColor7").value;
		var zone8 = document.getElementById("zoneColor8").value;
		var zone9 = document.getElementById("zoneColor9").value;
		var zone10 = document.getElementById("zoneColor10").value;
		fillColors.push(zone1);
		fillColors.push(zone2);
		fillColors.push(zone3);
		fillColors.push(zone4);
		fillColors.push(zone5);
		fillColors.push(zone6);
		fillColors.push(zone7);
		fillColors.push(zone8);
		fillColors.push(zone9);
		fillColors.push(zone10);
		var xmlhttp = null;
		//alert(latLongList.latLong.length);
		if(latLongList!=null&&latLongList!=""){
		}else{
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
	
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			url = '/TDS/DashBoard?event=allZonesForDash';
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			var obj = "{\"latLong\":"+text+"}";
			latLongList = JSON.parse(obj.toString());
		}
		for(var i=0;i<latLongList.latLong.length;i++){
			var latLng =[];
			for(var j=0;j<latLongList.latLong[i].ZC.length;j++){
				latLng.push(new google.maps.LatLng(latLongList.latLong[i].ZC[j].latitude ,latLongList.latLong[i].ZC[j].longitude));
			}
			var polygon = new google.maps.Polygon({paths: latLng,
				strokeColor: "#000000",
				strokeOpacity: 0.6,
				strokeWeight: 1.5,
				fillColor: "#"+fillColors[i%10],
				fillOpacity: 0.45
			});
			polygon.setMap(map);
			polygonZonesGlobal.push(polygon);
			var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
			jobLabel.set('position', new google.maps.LatLng(latLongList.latLong[i].CLAT,latLongList.latLong[i].CLON));
			jobLabel.set('text',latLongList.latLong[i].ZD+'('+latLongList.latLong[i].ZK+')'); 
			labelGlobal.push(jobLabel);
		}
		
		document.getElementById("zoneCheckImage").src="images/Dashboard/zoneLoginRed.png";
	}
}

$(function(){
    $.contextMenu({
        selector: '.label', 
        callback: function(key, options,el,pos) {
        	var zoneDriver = $(this).find("#zone").html();
        	contextMenuWork2(key,zoneDriver);
        },
        items: {
            "delete": {"name": "Logout", "icon": "delete"},
            "move": {"name": "Move to Top", "icon": "move"}
        }
    });
});

$(function(){
    $.contextMenu({
        selector: '.shareRideContextMenu', 
        callback: function(key, options,el,pos) {
    		var routeNo = $(this).find("#routeNoValue").html();
    		contextMenuWorkForShared(key,routeNo);
        },
        items: {
            "shBroadcast": {"name": "BroadCast", "icon": "shBroadcast"},
            "shComplete": {"name": "Complete", "icon": "shComplete"},
            "sep1": "---------",
            "shDelete": {"name": "UnRoute trips/Delete Group", "icon": "shDelete"}
        }
    });
});

$(function(){
    $.contextMenu({
        selector: '.rowstraight', 
        callback: function(key, options,el,pos) {
        	var driver = $(this).find("#row").html();
        	var vehicle = $(this).find("#vehicleValue").html();
        	var switchvalue = $(this).find("#switchValue").html();
        	var latForDriver = $(this).find("#latForDriverForZone").html();
    		var longForDriver = $(this).find("#longForDriverForZone").html();
        	contextMenuWork1(key,driver,latForDriver,longForDriver,vehicle,switchvalue);
        },
        items: {
            "jobHistoryMenu": {"name": "Job History", "icon": "jobHistoryMenu"},
            "sep1": "---------",
            "sendMsgToDriver": {"name": "Text Message", "icon": "sendMsgToDriver"},
            "sendVMsgToDriver": {"name": "Voice Message", "icon": "sendVMsgToDriver"},
            "sep2": "---------",
            "zoneLogin": {"name": "Zone Login", "icon": "zoneLogin"},
            "zoneActivity": {"name": "Zone Activity", "icon": "zoneLogin"},
            "sep3": "---------",
            "delete": {"name": "Logout", "icon": "delete"},
            "deactivate": {"name": "Deactivate", "icon": "deactivate"},
            "sep4": "---------",
            "flagTrip": {"name": "Flag Trip", "icon": "flagTrip"},
            "onRoute": {"name": "OnRoute", "icon": "OnRoute"},
            "driverAvailability": {"name": "Change Availability", "icon": "driverAvailability"}
        }
    });
});
$(function(){
	if(document.getElementById("dispType").value=="m"){
		$.contextMenu({
			selector: '.customerRow', 
			callback: function(key, options,el,pos) {
				var tripId =$(this).find('#tripid').html(); 
				var driver =  $(this).find("#driverId").html();
				var phone = $(this).find("#phoneNoInJobs").html();
				var jobNo = $(this).find("#jobNo").html();
				var riderName = $(this).find("#riderName").html();
				contextMenuWork(key,tripId,driver,phone,jobNo,riderName);
			},
			items: {
				"allocateDriver": {"name": "Suggestions to Allocate       ", "icon": "allocateDriver"},
				"onBoard": {"name": "OnBoard       OB", "icon": "onBoard"},
				"onRoute": {"name": "OnRoute       OR", "icon": "onRoute"},
				"onSite": {"name": "OnSite         OS", "icon": "onSite"},
				"endTrip": {"name": "EndTrip       ET", "icon": "endTrip"},
				"sep0": "---------",
				"edit": {"name": "Edit", "icon": "edit"},
				"sep1": "---------",
				"delete": {"name": "Operator Cancel      oc", "icon": "delete"},
				"cancel": {"name": "Customer Cancel      cc", "icon": "cancel"},
				"noShow": {"name": "No Show     ns", "icon": "noShow"},
				"driverCompleted": {"name": "Driver Completed   dc", "icon": "driverCompleted"},
				"couldntService": {"name": "Couldn't Service'       cs", "icon": "couldntService"},
				"jobOnHold": {"name": "Hold the Job       h", "icon": "jobOnHold"},
				"sep2": "---------",
				"reDispatch": {"name": "ReDispatch rd", "icon": "reDispatch"},
				"broadCast": {"name": "Broadcast   bc", "icon": "broadCast"},
				"reStart": {"name": "ReSend  rs", "icon": "reStart"},
				"sep3": "---------",
				"addWithSR": {"name": "Tag With Route", "icon": "addWithSR"},
				"jobHistory": {"name": "Job History", "icon": "jobHistory"},
				"jobLogs": {"name": "Job Logs", "icon": "jobLogs"},
				"sep4": "---------",
				"sendMsgToDriver": {"name": "sendMsgToDriver", "icon": "sendMsgToDriver"},
				"printThisJob": {"name": "Print Job", "icon": "printThisJob"},
				"sep5": "---------",
				"changeFleet": {"name": "changeFleet", "icon": "changeFleet"},
				 "endZone": {"name": "Set EndZone", "icon": "endZone"},
				 "callDriver": {"name": "Call Driver", "icon": "callDriver"}
			}
		});}else{
			$.contextMenu({
				selector: '.customerRow', 
				callback: function(key, options,el,pos) {
					var tripId =$(this).find('#tripid').html(); 
					var driver =  $(this).find("#driverId").html();
					var phone = $(this).find("#phoneNoInJobs").html();
					var jobNo = $(this).find("#jobNo").html();
					var riderName = $(this).find("#riderName").html();
					contextMenuWork(key,tripId,driver,phone,jobNo,riderName);
				},
				items: {
					"allocateDriver": {"name": "Suggestions to Allocate       ", "icon": "allocateDriver"},
					"edit": {"name": "Edit", "icon": "edit"},
					"sep1": "---------",
					"delete": {"name": "Operator Cancel      oc", "icon": "delete"},
					"cancel": {"name": "Customer Cancel      cc", "icon": "cancel"},
					"noShow": {"name": "No Show     ns", "icon": "noShow"},
					"driverCompleted": {"name": "Driver Completed   dc", "icon": "driverCompleted"},
					"couldntService": {"name": "Couldn't Service'       cs", "icon": "couldntService"},
					"jobOnHold": {"name": "Hold the Job       h", "icon": "jobOnHold"},
					"sep2": "---------",
					"reDispatch": {"name": "ReDispatch rd", "icon": "reDispatch"},
					"broadCast": {"name": "Broadcast   bc", "icon": "broadCast"},
					"reStart": {"name": "ReSend  rs", "icon": "reStart"},
					"sep3": "---------",
					"addWithSR": {"name": "Tag With Route", "icon": "addWithSR"},
					"jobHistory": {"name": "Job History", "icon": "jobHistory"},
					"jobLogs": {"name": "Job Logs", "icon": "jobLogs"},
					"sep4": "---------",
					"sendMsgToDriver": {"name": "sendMsgToDriver", "icon": "sendMsgToDriver"},
					"printThisJob": {"name": "Print Job", "icon": "printThisJob"},
					"sep5": "---------",
					"changeFleet": {"name": "changeFleet", "icon": "changeFleet"},
					"callDriver": {"name": "Call Driver", "icon": "callDriver"}

				}
			});
		}
});
// *********************************
// From jSp
function sumbitValueFromDash(){
	document.forms["cookieSubmit"].submit();
	//$("#pbar").jqm();
	//$("#pbar").jqmShow();
	//document.getElementById("pbar").innerHTML = "<center><h6><font color=\"blue\">Saving Your Preferences!!</h6></b></center></br>Please Wait...<font color=\"lime\"size=\"2px\"> Storing : <img src=\"images/Dashboard/progress2.gif\">"+ "<font size=\"1px\" ><b><a>" + getBar()+"</a></b></font></font></font>";

	window.status = "Please Wait the Page was Loading...";
	//document.cookieSubmit.submit();
}