var polygonGlobal=[];
var labelGlobal=[];
var totalOptions = 1;
var reqCounter =0;
var globalJsonForDriver = "";
function AjaxInteraction(url, callback) {
	var req = init();
	req.onreadystatechange = processRequest;
	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	function processRequest() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				if (callback)
					callback(req.responseText);
			}
		}

	}
	this.doGet = function() {
		req.open("GET", url, true);
		req.timeout = 5000;
		reqCounter++;
		if(reqCounter>3){
			req.ontimeout = timeoutFired;
			reqCounter=0;
		}
		req.send(null);
	}
	function timeoutFired(){
		//alert("request time out");
		var cell = document.createTextNode(""+currentTimeCalculate()+":>Request Time Out:");
		var cellBr = document.createElement("br");
		document.getElementById("console").appendChild(cellBr);
		document.getElementById("console").appendChild(cell);
	}
}

function AjaxInteraction_moreTime(url, callback) {
	//alert("commn");
	var req = init();
	req.onreadystatechange = processRequest;
	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	function processRequest() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				if (callback)
					callback(req.responseText);
			}
		}

	}
	this.doGet = function() {
		req.open("GET", url, true);
		req.timeout = 50000;
		reqCounter++;
		if(reqCounter>3){
			req.ontimeout = timeoutFired;
			reqCounter=0;
		}
		req.send(null);
	}
	function timeoutFired(){
		//alert("request time out");
		var cell = document.createTextNode(""+currentTimeCalculate()+":>Request Time Out:");
		var cellBr = document.createElement("br");
		document.getElementById("console").appendChild(cellBr);
		document.getElementById("console").appendChild(cell);
	}
}


function map(lat, lang) {
}

function infoCallback(infowindow, marker) { 
	return function() { 
		infowindow.open(map, marker);
	}; 
}

function cancelMap(){
	document.getElementById("currentJobinJObs").value="";
	document.getElementById("currentDriverinJObs").value="";
	document.getElementById("currentDriverinDrivers").value="";
	document.getElementById("dragend").value="false";
	$("#cancelButton").hide("slow");
	readCookieForCenter();
	mapInitJobs('2');
	mapPlotDriver('2');
}

var cssObj = {
		'opacity' : '0.9',
		'filter' : 'alpha(opacity=50)'
};
var cssHowerObj = {
		'font-weight' : 'bolder',
		'opacity' : '0.5',
		'filter' : 'alpha(opacity=50)'
};
$(document).ready(function() {
	$("#consoleUp").draggable({
		handle: '#dragConsole',scroll: false
	});
	$("#bullHorn").draggable({handle:'.dragBullHorn',scroll: false });
	$("#vMessagePopUp").draggable({handle:'.dragMike',scroll: false });
	//$("#expandDiv").draggable({ scroll: false }).resizable();
	$("#left-block5").draggable({ scroll: false }).resizable();
	$("#right-block5").draggable({ scroll: false }).resizable();
	$("#orderPopUpSub").draggable({ scroll: false });
	$("#sharedDetailsPopUp").draggable({handle:'.block-topShDetails',scroll: false });
	$("#sharedRidePopUp").draggable({handle:'.block-topShared',scroll: false});
	$("#totalFlags").draggable({ scroll: false });
	$("#jobHistoryDiv").draggable({ scroll: false });
	$("#jobLogsDiv").draggable({ scroll: false });
	$("#bookToZone").draggable({ scroll: false });
	$(".Jobs").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".create").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".ORHistory").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".ORSummary").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".zones").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".operationStatistics").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".Driver").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".options").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".Message").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
	$(".home").hover(function() {
		$(this).css(cssHowerObj);
	}, function() {
		$(this).css(cssObj);
	});
});
$(document).ready(function() {
	loadDate();
	$("#jobsOnThatDay").draggable({ scroll: false });
	$("#compLogo").draggable({ scroll: false });

	$(".Jobs").click(function(){
		$(".Jobs").css(cssHowerObj);
		$("#openjobs").slideToggle("slow");
		$("#plain").draggable({ 
			handle: '#jobsTopDrag',scroll: false 
		});
	});
	$(".Driver").click(function(){
		$(".Driver").css(cssHowerObj);
		$("#drivers1").slideToggle("slow");
		$("#right-block").draggable({
			handle: '.block-top',scroll: false 
		});
	}); 
	$(".zones").click(function(){
		$(".zones").css(cssHowerObj);
		$("#queueid").slideToggle("slow");
		$("#queueid").draggable({
			handle: '.block-top1',scroll: false 
		});
	});

	$(".zoneRates").click(function(){
		$(".zoneRates").css(cssHowerObj);
		$("#showzoneRates").slideToggle("slow");
		$("#showzoneRates").draggable({
			handle: '.block-topRate',scroll: false 
		});
	});
	$(".ORHistory").click(function(){
		if(document.getElementById("accessHistory").value=="Yes"){
			$(".ORHistory").css(cssHowerObj);
			$("#ORHistory").jqm({modal:true});
			$("#ORHistory").jqmShow();
			$("#ORHistory").draggable({
				containment:'#dashboardBody',handle: '.headerRowORH',scroll: false 
			});
		}else{
			alert("You don't have access. Please contact Administrator..");
		}
	});
	$(".ORSummary").click(function(){
		if(document.getElementById("accessSummary").value=="Yes"){
			$(".ORSummary").css(cssHowerObj);
			/*$("#ORSummary").jqm();
					$("#ORSummary").jqmShow();*/
			ORSummary(0);
			$("#ORSummary").draggable({
				handle: '.orSummaryRow',scroll: false 
			});
		}else{
			alert("You don't have access. Please contact Administrator..");
		}
	});

	$(".options").click(function(){
		$(".options").css(cssHowerObj);
//		$("#OptionsForAll").slideToggle("slow");
//		$("#OptionsForAll").draggable({
//		handle:'.block-top55',scroll: false });
		$("#OptionsForAll").jqm();
		$("#OptionsForAll").jqmShow();
	});
	$(".Message").click(function(){
		$(".Message").css(cssHowerObj);
//		$("#sendSms").slideToggle("slow");
//		$("#sendSms").draggable({scroll: false });
		$("#sendSms").jqm();
		$("#sendSms").jqmShow();
	});
	$(".operationStatistics").click(function(){
		$(".operationStatistics").css(cssHowerObj);
		jobCount();
		$("#statistics").slideToggle("slow");
		$("#statistics").draggable({scroll: false });
	});
	$(".openjobs a").hover(function() {
		$(this).find("em").animate({
			opacity : "show",
			left: "115"
		}, "slow");
	}, function() {
		$(this).find("em").animate({
			opacity : "hide",
			left: "105"
		}, "fast");
	});

	if($.browser.mozilla){
		document.getElementById("map").style.left='-1187px';
		document.getElementById("map").style.top='-1.8px';}
	InitCI();
	mapInitOnLoad();
	mapInitJobs('0');
	mapPlotDriver('0');
	readCookieForCenter();
	messageHistoryShow("1");
	document.getElementById('shrs').value="Now";
	google.maps.event.addDomListener(window, 'load', mapInitOnLoad());
});
var arrMarkerDriver = [];
var arrInfoWindowsDriver = [];
var arrLabelDriver = [];

function mapPlotDriver(n) {
	//console.log("Driver--> plot drivers");
	//if (document.getElementById("currentDriverinDrivers").value== "") {
	for (i in arrMarkerDriver) {
		arrMarkerDriver[i].setMap(null);
		arrInfoWindowsDriver[i].setMap(null);
		arrLabelDriver[i].setMap(null);
	}
	//}
	arrMarkerDriver = [];
	arrInfoWindowsDriver=[];
	arrLabelDriver=[];
	var mapHasBeenDragged = document.getElementById("dragend").value;
	var currentDriver = document.getElementById("currentDriverinDrivers").value;
	var currentJobClicked = document.getElementById("currentJobinJObs").value;
	var currentDriverAllocatedToJob = document.getElementById("currentDriverinJObs").value;

	var singleDriver = false;
	var currentJobInFocus = false;
	var currentJobClickedStarted = false;
	var singleDriverArrayLocation = 0;
	var singleJobArrayLocation = 0;

	if (currentJobClicked!="" && currentDriver ==""){
		currentJobInFocus = true;
	}
	if (currentDriver != "") {
		singleDriver = true;
	}

	if(!singleDriver && mapHasBeenDragged=="false" && !currentJobInFocus){
		if($('#GAC-D-long').val()!=null){
			latitude=$('#GAC-D-lat').val();
			longitude=$('#GAC-D-long').val();
		} else {
			latitude=document.getElementById("defaultLatitude").value;
			longitude=document.getElementById("defaultLongitude").value;
		}
		var zo = document.getElementById("zoomValue").value;
		map.setZoom(parseInt(zo));

		var centerCoord = new google.maps.LatLng(latitude,longitude);
		map.setCenter(centerCoord);
		//console.log("Driver-->setting center from driver module to default"+latitude+":"+longitude);
	}

	var oRows = document.getElementById("driverDetailDisplay").getElementsByTagName("td");
	var iRowCount = oRows.length;
	var driverJob="";
	var status ="";
	var tripId="";
	document.getElementById("totalDrivers").innerHTML = iRowCount;
	var index = "row";
	var oRowsJobs=document.getElementById("customerTable").getElementsByTagName("tr");
	var iRowCountJobs= oRowsJobs.length;
	
	for ( var j = 0; j < iRowCount; j++) {
		driver = oRows[j].getElementsByTagName("input")[0].value;
		
		if(singleDriver == true && driver == currentDriver){
			//console.log("driver in main loop="+driver+"currentDriver="+currentDriver);
			driverJob ="";
			status ="";
			tripId ="";
			for(var k = 1; k < iRowCountJobs; k++) {
				if(oRowsJobs[k].id==index){
					var driverInJobs = oRowsJobs[k].getElementsByTagName("input")[11].value;
					//console.log("driver in loop="+driverInJobs+"currentDriver="+currentDriver);
					if(driverInJobs==currentDriver){
						//console.log("True in loop="+driverInJobs+"currentDriver="+currentDriver);
						driverJob = oRowsJobs[k].getElementsByTagName("input")[6].value;
						status = oRowsJobs[k].getElementsByTagName("input")[8].value;
						tripId = oRowsJobs[k].getElementsByTagName("input")[3].value;					
						continue;
					}
				}
			}
		}else{
			driverJob ="";
			status ="";
			tripId ="";
			for(var p = 1; p < iRowCountJobs; p++) {
				if(oRowsJobs[p].id==index){
					var driverInJobs = oRowsJobs[p].getElementsByTagName("input")[11].value;
					if(driverInJobs==driver){
						driverJob = oRowsJobs[p].getElementsByTagName("input")[6].value;
						status = oRowsJobs[p].getElementsByTagName("input")[8].value;
						tripId = oRowsJobs[p].getElementsByTagName("input")[3].value;					
						continue;
					} 
				}
			}
		}
		
		//console.log("driver in loop="+driver+"currentDriver="+currentDriver);
		
		cab = oRows[j].getElementsByTagName("input")[3].value;
		lat = oRows[j].getElementsByTagName("input")[1].value;
		longi = oRows[j].getElementsByTagName("input")[2].value;
		var statusDriver = oRows[j].getElementsByTagName("input")[4].value;
		var driverName = oRows[j].getElementsByTagName("input")[7].value;
		var latLng = new google.maps.LatLng(lat, longi);
		//?? Check. Added by Ram to get the current latitude and longitude of driver thats in focus
		//document.getElementById("latDriver").value = 
		var image="";
		if(statusDriver=="Y"){
			image = "images/Dashboard/carGreen.png";
		}else{
			image = "images/Dashboard/carRed.png";
		}
		markerDriver = new google.maps.Marker({
			position : latLng,
			map : map,
			icon : image
		});
		arrMarkerDriver.push(markerDriver); 
		var driverLabel = new Label({
			map : map
		});
		driverLabel.set('position', new google.maps.LatLng(lat, longi));
		driverLabel.set('text', cab);
		/*var infoWindow2 = new google.maps.InfoWindow({
			content : "<DIV id = \"infoWindowJob\" style=\"background-color: LightYellow; overflow: hidden;color:black\"><font color=\"Orange\">D Name:</font><h3><font color=\"red\">"+driverName+"</font></h3><br/><h5><font color=\"orange\">Cab No :</font><font color=\"green\">" + cab + "</font></h5><br/><h6><a href='javascript:void(0)' onclick='logoutDriver("+currentDriver+")'><input type='button' style=\"border-radius: 8px 8px 8px 8px; background-color: Tomato;color: highlighttext;\" value='Logout Driver'></a></h6>"
			+"<font color=\"orange\">Job Address:</font>"+driverJob+"<br/><font color=\"orange\">Status:</font>"+status+"<br/><font color=\"orange\">Trip Id:</font>"+tripId+"</div>"});
		arrLabelDriver.push(driverLabel);*/
		
		var infoWindow2;
		var dist = oRows[j].getElementsByTagName("input")[9].value;
		if(dist!=null && dist!="" && dist!="NF"){
			infoWindow2 = new google.maps.InfoWindow({
					content : "<DIV id = \"infoWindowJob\" style=\"background-color: LightYellow; overflow: hidden;color:black\"><font color=\"Orange\">D Name:</font><h3><font color=\"red\">"+driverName+"</font></h3><br/><h5><font color=\"orange\">Cab No :</font><font color=\"green\">" + cab + "</font></h5><br/><h6><a href='javascript:void(0)' onclick='logoutDriver("+currentDriver+")'><input type='button' style=\"border-radius: 8px 8px 8px 8px; background-color: Tomato;color: highlighttext;\" value='Logout Driver'></a></h6>"
					+"<font color=\"orange\">Job Address:</font>"+driverJob+"<br/><font color=\"orange\">Status:</font>"+status+"<br/><font color=\"orange\">Trip Id:</font>"+tripId+"<br/><font color=\"orange\">Distance:</font>"+dist+"</div>"});
		}else{
			infoWindow2 = new google.maps.InfoWindow({
					content : "<DIV id = \"infoWindowJob\" style=\"background-color: LightYellow; overflow: hidden;color:black\"><font color=\"Orange\">D Name:</font><h3><font color=\"red\">"+driverName+"</font></h3><br/><h5><font color=\"orange\">Cab No :</font><font color=\"green\">" + cab + "</font></h5><br/><h6><a href='javascript:void(0)' onclick='logoutDriver("+currentDriver+")'><input type='button' style=\"border-radius: 8px 8px 8px 8px; background-color: Tomato;color: highlighttext;\" value='Logout Driver'></a></h6>"
					+"<font color=\"orange\">Job Address:</font>"+driverJob+"<br/><font color=\"orange\">Status:</font>"+status+"<br/><font color=\"orange\">Trip Id:</font>"+tripId+"</div>"});
		}
		
		arrLabelDriver.push(driverLabel);
		arrInfoWindowsDriver.push(infoWindow2); 
		google.maps.event.addListener(markerDriver, 'click',infoCallback(infoWindow2,markerDriver));
		if (singleDriver == true && driver == currentDriver) {
			console.log("Driver--> Inside Showinfowindow loop");
			if(mapHasBeenDragged =="false"){
				infoWindow2.open(map, markerDriver);
				map.panTo(latLng);
				var zoom = document.getElementById("individualZoomValue").value;
				map.setZoom(parseInt(zoom));
				//console.log("setting center from driver specific"+latLng);
			}
		}

	}
}


//var map;
var arrMarkerJobs = [];
var arrInfoWindowsJobs = [];
var arrLabelJob = [];
var data;
function mapInitJobs(m){
	//console.log("about to plot job details");	
	for(i in arrMarkerJobs){
		arrMarkerJobs[i].setMap(null);
		arrInfoWindowsJobs[i].setMap(null);
		arrLabelJob[i].setMap(null);
	}
	arrMarkerJobs = [];
	arrInfoWindowsJobs = [];
	arrLabelJob=[];
	var driverSelectedDontZoom = false;
	var trip= document.getElementById("currentJobinJObs").value;
	var driverIDSelected= document.getElementById("currentDriverinDrivers").value;

	var singleJob = false;
	var mapHasBeenDragged = document.getElementById("dragend").value;
	if(trip!=""){
		singleJob = true;
	}
	if(driverIDSelected!=""){
		driverSelectedDontZoom = true;
	}
	//console.log("single job = "+ singleJob);	

	//console.log("single job details for trip"+trip);
	if($('#GAC-D-long').val()!=null){
		latitude=$('#GAC-D-lat').val();
		longitude=$('#GAC-D-long').val();
	} else {
		latitude=document.getElementById("defaultLatitude").value;
		longitude=document.getElementById("defaultLongitude").value;
	}
	var dragend = document.getElementById("dragend").value;


	var centerCoord = new google.maps.LatLng(latitude,longitude); 
	if(!singleJob && !driverSelectedDontZoom && mapHasBeenDragged=="false"){
		//console.log("job details. Zooming to dflt loc and zoom");	

		map.setCenter(centerCoord);
		var zoomValue = document.getElementById("zoomValue").value;
		map.setZoom(parseInt(zoomValue));
	}
	var driverId=document.getElementById("currentDriverinJObs").value;
	var lat3=document.getElementById("latJobs").value;
	var long3=document.getElementById("longJobs").value;
	var oRows1=document.getElementById("driverDetailDisplay").getElementsByTagName("td");
	var iRowCount1= oRows1.length;
	for(var i=0;i<iRowCount1;i++){
		driver=oRows1[i].getElementsByTagName("input")[0].value;
		if(driver==driverId){
			lat = oRows1[i].getElementsByTagName("input")[1].value;
			longi = oRows1[i].getElementsByTagName("input")[2].value;
			if(document.getElementById("startLat").value==""){
				document.getElementById("startLat").value=lat;
				document.getElementById("startLong").value=longi;
			}
			break;
		}
	}
	var index = "row";
	var oRows=document.getElementById("customerTable").getElementsByTagName("tr");
	var iRowCount= oRows.length;
//	document.getElementById("totalJobs").innerHTML=iRowCount-1;
	for(var i = 1; i < iRowCount; i++) {
		if(oRows[i].id==index){
			var tripID = oRows[i].getElementsByTagName("input")[3].value;
			var city = oRows[i].getElementsByTagName("input")[6].value;
			var time = oRows[i].getElementsByTagName("input")[2].value;
			var status = oRows[i].getElementsByTagName("input")[8].value;
			var phone = oRows[i].getElementsByTagName("input")[9].value;
			var name = oRows[i].getElementsByTagName("input")[10].value;
			var statusValue= oRows[i].getElementsByTagName("input")[12].value;
			var allocatedDriverID = oRows[i].getElementsByTagName("input")[11].value;
			//console.log("driver will be focussed: "+allocatedDriverID);

			lat1 = oRows[i].getElementsByTagName("input")[4].value;
			longi1 = oRows[i].getElementsByTagName("input")[5].value;
			var latLng = new google.maps.LatLng(lat1, longi1);
			var image="";
			if(statusValue!="47"){
				//console.log("job status not 47"+tripID);	
				if(statusValue=="40"){
					image = "images/Dashboard/jobAllocated.png";
				}else if(statusValue=="43"){
					image = "images/Dashboard/jobAllocated.png";
				}else if(statusValue=="45"){
					image = "images/Dashboard/jobAllocated.png";
				}else if(statusValue=="4"){
					image = "images/Dashboard/jobAllocated.png";
				}else if(statusValue=="49"){
					image = "images/Dashboard/jobAllocated.png";
				}else if(statusValue=="50"||statusValue=="51"||statusValue=="52"||statusValue=="55"||statusValue=="61"||statusValue=="99"||statusValue=="25"){
					image = "images/Dashboard/job.png";	
				}else{
					image = "images/Dashboard/job.png";	
				}

				var markerJob = new google.maps.Marker({
					position : latLng,
					map : map,
					icon : image
				});
				arrMarkerJobs.push(markerJob);
				var jobLabel = new Label({
					map : map
				});
				jobLabel.set('position', new google.maps.LatLng(lat1, longi1));
				jobLabel.set('text', i);
				var infowindow1 = new google.maps.InfoWindow({
					content : "<div  id = \"infoWindowDriver\" style=\"background-color: LightYellow; overflow: hidden;color:black\"><h5><font color=\"orange\">Address : </font><font color=\"green\">"+city+ "</font></h5><br/><h6><a href='javascript:void(0)' onclick='deleteJobs("+tripID+")'><input type='button'style=\"display:none;border-radius: 8px 8px 8px 8px; background-color: Tomato;color: highlighttext;\" value='Cancel Job'></a>"+
					"</h6><font color=\"orange\">"+document.getElementById("driverOrCabCheck").value+"</font>:"+driverId+"<br/><font color=\"orange\">Trip Id:</font>"+tripID+"<br/><font color=\"orange\">PickupTime:</font>"+time+"<br/><font color=\"orange\">Status:</font>"+status+"<br/><font color=\"orange\">P.Name:</font>"+name+"<br/><font color=\"orange\">P.Ph.No:</font>"+phone+"</div>"
				});
				arrLabelJob.push(jobLabel);
				arrInfoWindowsJobs.push(infowindow1);
				google.maps.event.addListener(markerJob, 'click',infoCallback(infowindow1,markerJob));
				if (singleJob == true && tripID == trip && mapHasBeenDragged=="false") {
					//console.log("about to plot single job details for trip"+tripID);	

					infowindow1.open(map, markerJob);
					var zoomValue = document.getElementById("individualZoomValue").value;
					map.setZoom(parseInt(zoomValue));
					var centerPoint = new google.maps.LatLng(lat1, longi1);
					map.setCenter(centerPoint);
					//console.log("latlng for single job= "+lat1 +":"+longi1);	

				}
			}
		} 
	}

}	

function emergencyZoom(x,y){
	var zo = "18";
	map.setZoom(parseInt(zo));

	var centerCoord = new google.maps.LatLng(x,y);
	map.setCenter(centerCoord);
}	

var geocoder;
function extractAddressFromPlaceResult(place, fieldBackup) {
	var street_no = null, street_name = null, postal = null, locality = null;

	if(place == undefined || place == null || place.address_components == undefined || place.address_components == null)
		return {"valid":false};

		for(var i=0;place.address_components.length>i;i++)
			for(var j=0;place.address_components[i].types.length>j;j++)
			{
				var t = place.address_components[i].types[j];
				if(t == 'route')
					street_name = place.address_components[i].long_name;
				else if(t == 'street_number')
					street_no = place.address_components[i].long_name;
				else if(t == 'postal_code')
					postal = place.address_components[i].long_name;
				else if(t == 'locality')
					locality = place.address_components[i].long_name;
			}

		if(!(street_no != null || fieldBackup == null))
		{
			var m = jQuery(fieldBackup).val().match(/^([0-9][0-9]*)[^0-9]/);
			if(m == null || m.length != 2)
				; // do nothing
			else
				street_no = m[1];
		}

		return {
			"valid": street_name != null,
			"street_no": street_no,
			"street_name": street_name,
			"locality": locality,
			"postal": postal };
}
function myFunction(type)
{
	if(type==1){
		setTimeout(function(){initializeMap(type);markerDrag();},300);
	} else {
		setTimeout(function(){initializeMap(type);markerDragTo();},300);
	}
}

function mapInitOnLoad(){
	var latitude="";
	var longitude="";
	if(document.getElementById("latFromMap").value!=""){
		latitude=document.getElementById("latFromMap").value;
		longitude=document.getElementById("longFromMap").value;
	}else{ 
		latitude=document.getElementById("defaultLatitude").value;
		longitude=document.getElementById("defaultLongitude").value;
	}
	var defaultState=document.getElementById("defaultState").value;
	var defaultCountry=document.getElementById("defaultCountry").value;
	var zo = document.getElementById("zoomValue").value;
	var centerCoord = new google.maps.LatLng(latitude,longitude); // Puerto Rico
	var mapOptions = {
			zoom: +zo,
			center: centerCoord,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map"), mapOptions);
	google.maps.event.addListener(map, 'dragend', function() { dragend(); } );
	google.maps.event.addListener(map, 'zoom_changed', function() {
		zoomLevel = map.getZoom();
		document.getElementById("currentZoom").value=zoomLevel; 
	});
	/*google.maps.event.addListener(map, "dragend", function() {
                    var center = map.getCenter();
                   document.getElementById("latFromMap").value=center.lat();
                   document.getElementById("longFromMap").value=center.lng();
                });*/
	var point="";
	var lat="";
	var longi="";
	var contextMenuOptions={};
	contextMenuOptions.classNames={menu:'context_menu', menuSeparator:'context_menu_separator'};
	var menuItems=[];
	menuItems.push({className:'context_menu_item', eventName:'sendMessage', id:'directionsOriginItem', label:'Send Message'});
	menuItems.push({className:'context_menu_item', eventName:'createJob', id:'createJob', label:'Create Job'});
	if(document.getElementById("dispType").value=="m"){
		menuItems.push({className:'context_menu_item', eventName:'posDriver', id:'posDriver', label:'Pin A Cab'});
	}
	contextMenuOptions.menuItems=menuItems;
	var contextMenu=new ContextMenu(map, contextMenuOptions);
	google.maps.event.addListener(map, 'rightclick', function(mouseEvent){
		contextMenu.show(mouseEvent.latLng);
		point = mouseEvent.latLng;
		lat=point.lat();
		longi=point.lng();
		document.getElementById("latForRightclick").value=lat;
		document.getElementById("longForRightclick").value=longi;
	});
	google.maps.event.addListener(contextMenu, 'menu_item_selected', function(latLng, eventName){
		switch(eventName){
		case 'sendMessage':
		{
			$("#callDrivers").show();
			break;
		}
		case 'posDriver':
		{
			$("#getDriver").show();
			break;
		}
		case 'createJob':
		{
			createJobORDash();
			var lattitude =document.getElementById("latForRightclick").value;
			var longitude =document.getElementById("longForRightclick").value;
			var point = new google.maps.LatLng(lattitude, longitude);
			var geocoder = new google.maps.Geocoder();
			geocoder.geocode({latLng: point}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						var arrAddress=  results[0].address_components;
						$('#addressDash').val(results[0].formatted_address);
						$('#sLatitudeDash').val(lattitude);
						$('#sLongitudeDash').val(longitude);
						$.each(arrAddress, function (i, address_component) {
							if (address_component.types[0] == "subpremise"){
								$("#sadd2Dash").val(address_component.long_name);
							}
							if (address_component.types[0] == "street_number"){
								$("#sadd1Dash").val(address_component.long_name);
								streetnum = address_component.long_name;
							}
							if (address_component.types[0] == "locality"){
								$("#scityDash").val(address_component.long_name);
							}
							if (address_component.types[0] == "route"){ 
								route = address_component.long_name;
							}
							if (address_component.types[0] == "country"){ 
								itemCountry = address_component.long_name;
							}
							if (address_component.types[0] == "postal_code"){ 
								itemPc = address_component.long_name;
								$("#szipDash").val(address_component.long_name);
							}
						});
						checkOldAddressDash(1);
					}
				}
			});
			break;
		}
		}
	});
	if(document.getElementById("addressCheck").value=="2"){
		var input = document.getElementById('DropAddressDash');
		var autocomplete = new google.maps.places.Autocomplete(input);
//		var geolocation = new google.maps.LatLng(
//				document.getElementById("defaultLatitude").value, document.getElementById("defaultLongitude").value);
//		var circle = new google.maps.Circle({
//			center: geolocation,
//			radius: 2
//		});
//		autocomplete.setBounds(circle.getBounds());
		autocomplete.bindTo('bounds', map);
		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			if (place.address_components) {
				var address_details = extractAddressFromPlaceResult(place, '#DropAddressDash');
				$('#eadd1Dash').val(address_details.street_no+" "+address_details.street_name);
				$('#ezipDash').val(address_details.postal);
				$('#ecityDash').val(address_details.locality);
				var address = address_details.street_no + ' ' + address_details.street_name + ', ' + address_details.locality+ ', US';
				geocoder = new google.maps.Geocoder();
				geocoder.geocode({'address': address}, function(results, status) {
					if(status == google.maps.GeocoderStatus.OK)
					{
						$("#eLatitude").val(results[0].geometry.location.lat());
						$("#eLongitude").val(results[0].geometry.location.lng());
					}
					checkOldAddressDash(2);
				});
			}
		});
	} else {

		geocoder = new google.maps.Geocoder();
		$("#DropAddressDash").autocomplete({
			//This bit uses the geocoder to fetch address values
			source: function(request, response) {
				geocoder.geocode( {'address': request.term +','+defaultState+','+defaultCountry  }, function(results, status) {
					response($.map(results, function(item) {
						return {
							label: item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							city: item.postal_code,
							addcomp: item.address_components
							//nhd: item.address_components_of_type("neighborhood")
						}
					}));
				})
			},
			//This bit is executed upon selection of an address
			select: function(event, ui) {

				var arrAddress =ui.item.addcomp;
				var streetnum= "";
				var route = "";


				// iterate through address_component array
				$.each(arrAddress, function (i, address_component) {
					if (address_component.types[0] == "subpremise"){
						$("#eadd2Dash").val(address_component.long_name);
					}
					if (address_component.types[0] == "street_number"){
						$("#eadd1Dash").val(address_component.long_name);
						streetnum = address_component.long_name;
						//console.log('HK'+document.getElementById("sadd1").value);
					}

					if (address_component.types[0] == "locality"){
						$("#ecityDash").val(address_component.long_name);
					}

					if (address_component.types[0] == "route"){ 
						route = address_component.long_name;
					}
					if (address_component.types[0] == "country"){ 
						itemCountry = address_component.long_name;
					}


					if (address_component.types[0] == "postal_code"){ 
						itemPc = address_component.long_name;
						$("#ezipDash").val(address_component.long_name);
					}

					//return false; // break the loop

				});

				$("#eadd1Dash").val(streetnum + " " + route);
				$("#eLatitude").val(ui.item.latitude);
				$("#eLongitude").val(ui.item.longitude);
				$("#DropAddressDash").autocomplete("close");
				checkOldAddressDash(2);
				//$("#street").val(ui.item.street);
				//$("#state").val(ui.item.address_components_of_type("neighborhood"));
			}
		});
	}
}
$(document).ready(function() {
	$(function() {
		var defaultState=document.getElementById("defaultState").value;
		var defaultCountry=document.getElementById("defaultCountry").value;

		if(document.getElementById("addressCheck").value=="2"){
			var input = document.getElementById('addressDash');
			var autocomplete = new google.maps.places.Autocomplete(input);
//			var geolocation = new google.maps.LatLng(
//					document.getElementById("defaultLatitude").value, document.getElementById("defaultLongitude").value);
//			var circle = new google.maps.Circle({
//				center: geolocation,
//				radius: 2
//			});
//			autocomplete.setBounds(circle.getBounds());
			autocomplete.bindTo('bounds', map);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				if (place.address_components) {
					var address_details = extractAddressFromPlaceResult(place, '#addressDash');
					$('#sadd1Dash').val(address_details.street_no+" "+address_details.street_name);
					$('#szipDash').val(address_details.postal);
					$('#scityDash').val(address_details.locality);
					var address = address_details.street_no + ' ' + address_details.street_name + ', ' + address_details.locality+ ', US';
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({'address': address}, function(results, status) {
						if(status == google.maps.GeocoderStatus.OK)
						{
							$("#sLatitudeDash").val(results[0].geometry.location.lat());
							$("#sLongitudeDash").val(results[0].geometry.location.lng());
						}
						checkOldAddressDash(1);
					});
				}
			});
		} else {
			geocoder = new google.maps.Geocoder();
			$("#addressDash").autocomplete({
				//This bit uses the geocoder to fetch address values
				source: function(request, response) {
					geocoder.geocode( {'address': request.term +','+defaultState+','+defaultCountry }, function(results, status) {
						response($.map(results, function(item) {
							return {
								label: item.formatted_address,
								value: item.formatted_address,
								latitude: item.geometry.location.lat(),
								longitude: item.geometry.location.lng(),
								city: item.postal_code,
								addcomp: item.address_components
								//nhd: item.address_components_of_type("neighborhood")
							}
						}));
					})
				},
				//This bit is executed upon selection of an address
				select: function(event, ui) {

					var arrAddress =ui.item.addcomp;
					var streetnum= "";
					var route = "";


					// iterate through address_component array
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "subpremise"){
							$("#sadd2Dash").val(address_component.long_name);
						}
						if (address_component.types[0] == "street_number"){
							$("#sadd1Dash").val(address_component.long_name);
							streetnum = address_component.long_name;
							//console.log('HK'+document.getElementById("sadd1").value);
						}

						if (address_component.types[0] == "locality"){
							$("#scityDash").val(address_component.long_name);
						}

						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}


						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#szipDash").val(address_component.long_name);
						}

						//return false; // break the loop

					});

					$("#sadd1Dash").val(streetnum + " " + route);
					$("#sLatitudeDash").val(ui.item.latitude);
					$("#sLongitudeDash").val(ui.item.longitude);

					//$("#street").val(ui.item.street);
					//$("#state").val(ui.item.address_components_of_type("neighborhood"));
					$("#addressDash").autocomplete("close");
					document.getElementById("addressKey").value="";
					checkOldAddressDash(1);
				}
			});
		}
	});
});

var polygonZonesGlobal =[];
var labelGlobal =[];
var latLongList ="";
function showAllZonesForDash(){
	var img=document.getElementById("zoneCheckImage").src;
	var fillColors =[];
	if(img.indexOf("zoneLoginRed.png")!= -1){
		document.getElementById("zoneCheckImage").src="images/Dashboard/zoneLogin.gif";
		for(var i=0;i<polygonZonesGlobal.length;i++){
			polygonZonesGlobal[i].setMap(null);
			labelGlobal[i].setMap(null);
		}
		polygonZonesGlobal =[];
		labelGlobal =[];
	}else{
		var zone1 = document.getElementById("zoneColor1").value;
		var zone2 = document.getElementById("zoneColor2").value;
		var zone3 = document.getElementById("zoneColor3").value;
		var zone4 = document.getElementById("zoneColor4").value;
		var zone5 = document.getElementById("zoneColor5").value;
		var zone6 = document.getElementById("zoneColor6").value;
		var zone7 = document.getElementById("zoneColor7").value;
		var zone8 = document.getElementById("zoneColor8").value;
		var zone9 = document.getElementById("zoneColor9").value;
		var zone10 = document.getElementById("zoneColor10").value;
		fillColors.push(zone1);
		fillColors.push(zone2);
		fillColors.push(zone3);
		fillColors.push(zone4);
		fillColors.push(zone5);
		fillColors.push(zone6);
		fillColors.push(zone7);
		fillColors.push(zone8);
		fillColors.push(zone9);
		fillColors.push(zone10);
		var xmlhttp = null;
		//alert(latLongList.latLong.length);
		if(latLongList!=null&&latLongList!=""){
		}else{
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			url = '/TDS/DashBoard?event=allZonesForDash';
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			var obj = "{\"latLong\":"+text+"}";
			latLongList = JSON.parse(obj.toString());
		}
		for(var i=0;i<latLongList.latLong.length;i++){
			var latLng =[];
			for(var j=0;j<latLongList.latLong[i].ZC.length;j++){
				latLng.push(new google.maps.LatLng(latLongList.latLong[i].ZC[j].latitude ,latLongList.latLong[i].ZC[j].longitude));
			}
			var polygon = new google.maps.Polygon({paths: latLng,
				strokeColor: "#000000",
				strokeOpacity: 0.6,
				strokeWeight: 1.5,
				fillColor: "#"+fillColors[i%10],
				fillOpacity: 0.45
			});
			polygon.setMap(map);
			polygonZonesGlobal.push(polygon);
			var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
			jobLabel.set('position', new google.maps.LatLng(latLongList.latLong[i].CLAT,latLongList.latLong[i].CLON));
			jobLabel.set('text',latLongList.latLong[i].ZD+'('+latLongList.latLong[i].ZK+')'); 
			labelGlobal.push(jobLabel);
		}

		document.getElementById("zoneCheckImage").src="images/Dashboard/zoneLoginRed.png";
	}
}


$(function(){
	$.contextMenu({
		selector: '.label', 
		callback: function(key, options,el,pos) {
			var zoneDriver = $(this).find("#zone").html();
			contextMenuWork2(key,zoneDriver);
		},
		items: {
			"delete": {"name": "Logout", "icon": "delete"},
			"move": {"name": "Move to Top", "icon": "move"}
		}
	});
});

$(function(){
	$.contextMenu({
		selector: '.shareRideContextMenu', 
		callback: function(key, options,el,pos) {
			var routeNo = $(this).find("#routeNoValue").html();
			contextMenuWorkForShared(key,routeNo);
		},
		items: {
			"shBroadcast": {"name": "BroadCast", "icon": "shBroadcast"},
			"shComplete": {"name": "Complete", "icon": "shComplete"},
			"sep1": "---------",
			"shDelete": {"name": "UnRoute trips/Delete Group", "icon": "shDelete"}
		}
	});
});

$(function(){
	$.contextMenu({
		selector: '.rowstraight', 
		callback: function(key, options,el,pos) {
			var driver = $(this).find("#row").html();
			var vehicle = $(this).find("#vehicleValue").html();
			var switchvalue = $(this).find("#switchValue").html();
			var latForDriver = $(this).find("#latForDriverForZone").html();
			var longForDriver = $(this).find("#longForDriverForZone").html();
			contextMenuWork1(key,driver,latForDriver,longForDriver,vehicle,switchvalue);
		},
		items: {
			"jobHistoryMenu": {"name": "Job History", "icon": "jobHistoryMenu"},
			"sep1": "---------",
			"sendMsgToDriver": {"name": "Text Message", "icon": "sendMsgToDriver"},
			"sendVMsgToDriver": {"name": "Voice Message", "icon": "sendVMsgToDriver"},
			"sep2": "---------",
			"zoneLogin": {"name": "Zone Login", "icon": "zoneLogin"},
			"zoneActivity": {"name": "Zone Activity", "icon": "zoneLogin"},
			"sep3": "---------",
			"delete": {"name": "Logout", "icon": "delete"},
			"deactivate": {"name": "Deactivate", "icon": "deactivate"},
			//"jobDetails":{"name":"Show JobDetails","icon":"jobDetails"},
			"sep4": "---------",
			"flagTrip": {"name": "Flag Trip", "icon": "flagTrip"},
			"onRoute": {"name": "OnRoute", "icon": "OnRoute"},
			"driverAvailability": {"name": "Change Availability", "icon": "driverAvailability"},
			"resetDriverApp": {"name": "Reset Driver App", "icon": "resetDriverApp"},
			"approve":{"name":"Approve Cab Number to Device","icon":"approve"}
		}
	});
});

$(function(){
	$.contextMenu({
		selector: '.rowstraight2', 
		callback: function(key, options,el,pos) {
			var driver = $(this).find("#row").html();
			var vehicle = $(this).find("#vehicleValue").html();
			var switchvalue = $(this).find("#switchValue").html();
			var latForDriver = $(this).find("#latForDriverForZone").html();
			var longForDriver = $(this).find("#longForDriverForZone").html();
			contextMenuWork1(key,driver,latForDriver,longForDriver,vehicle,switchvalue);
		},
		items: {
			"jobHistoryMenu": {"name": "Job History", "icon": "jobHistoryMenu"},
			//"jobDetails": {"name": "Show JobDetails", "icon": "jobLogs"},
			"sep1": "---------",
			"sendMsgToDriver": {"name": "Text Message", "icon": "sendMsgToDriver"},
			"sendVMsgToDriver": {"name": "Voice Message", "icon": "sendVMsgToDriver"},
			"sep2": "---------",
			"zoneLogin": {"name": "Zone Login", "icon": "zoneLogin"},
			"zoneActivity": {"name": "Zone Activity", "icon": "zoneLogin"},
			"sep3": "---------",
			"delete": {"name": "Logout", "icon": "delete"},
			"activate": {"name": "Activate", "icon": "activate"},
			"sep4": "---------",
			"flagTrip": {"name": "Flag Trip", "icon": "flagTrip"},
			"onRoute": {"name": "OnRoute", "icon": "OnRoute"},
			"driverAvailability": {"name": "Change Availability", "icon": "driverAvailability"},
			"resetDriverApp": {"name": "Reset Driver App", "icon": "resetDriverApp"},
			"approve":{"name":"Approve Cab Number to Device","icon":"approve"}
		}
	});
});

$(function(){
	if(document.getElementById("dispType").value=="m"){
		$.contextMenu({
			selector: '.customerRow', 
			callback: function(key, options,el,pos) {
				var tripId =$(this).find('#tripid').html(); 
				var driver =  $(this).find("#driverId").html();
				var phone = $(this).find("#phoneNoInJobs").html();
				var jobNo = $(this).find("#jobNo").html();
				var riderName = $(this).find("#riderName").html();
				contextMenuWork(key,tripId,driver,phone,jobNo,riderName);
			},
			items: {
				"allocateDriver": {"name": "Suggestions to Allocate       ", "icon": "allocateDriver"},
				"sep0": "---------",
				"edit": {"name": "Edit", "icon": "edit"},
				"sep1": "---------",
				"delete": {"name": "Operator Cancel      oc", "icon": "delete"},
				"cancel": {"name": "Customer Cancel      cc", "icon": "cancel"},
				"noShow": {"name": "No Show     ns", "icon": "noShow"},
				"driverCompleted": {"name": "Driver Completed   dc", "icon": "driverCompleted"},
				"couldntService": {"name": "Couldn't Service'       cs", "icon": "couldntService"},
				"jobOnHold": {"name": "Hold the Job       h", "icon": "jobOnHold"},
				"sep2": "---------",
				"reDispatch": {"name": "ReDispatch rd", "icon": "reDispatch"},
				"broadCast": {"name": "Broadcast   bc", "icon": "broadCast"},
				"reStart": {"name": "ReSend  rs", "icon": "reStart"},
				"sep3": "---------",
				"jobLogs": {"name": "Job Logs", "icon": "jobLogs"},
				"sep3": "---------",
				"advancedOption":{
					"name": "Advanced Option",
					"items" :{
						"addWithSR": {"name": "Tag With Route", "icon": "addWithSR"},
						"jobHistory": {"name": "Job History", "icon": "jobHistory"},
						"sep4": "---------",
						"sendMsgToDriver": {"name": "sendMsgToDriver", "icon": "sendMsgToDriver"},
						"printThisJob": {"name": "Print Job", "icon": "sendMsgToDriver"},
						"sep5": "---------",
						"changeFleet": {"name": "changeFleet", "icon": "changeFleet"},
						"endZone": {"name": "Set EndZone", "icon": "endZone"},
						"callDriver": {"name": "Call Driver", "icon": "callDriver"},
						"sep6": "---------",
						"advancedOption":{
							"name": "Advanced Option",
							"items" :{
								"onBoard": {"name": "OnBoard       OB", "icon": "onBoard"},
								"onRoute": {"name": "OnRoute       OR", "icon": "onRoute"},
								"onSite": {"name": "OnSite         OS", "icon": "onSite"},
								"endTrip": {"name": "EndTrip       ET", "icon": "endTrip"},
							}
						}
					}
				}
			}
		});}else{
			$.contextMenu({
				selector: '.customerRow', 
				callback: function(key, options,el,pos) {
					var tripId =$(this).find('#tripid').html(); 
					var driver =  $(this).find("#driverId").html();
					var phone = $(this).find("#phoneNoInJobs").html();
					var jobNo = $(this).find("#jobNo").html();
					var riderName = $(this).find("#riderName").html();
					contextMenuWork(key,tripId,driver,phone,jobNo,riderName);
				},
				items: {
					"allocateDriver": {"name": "Suggestions to Allocate       ", "icon": "allocateDriver"},
					"edit": {"name": "Edit", "icon": "edit"},
					"sep1": "---------",
					"delete": {"name": "Operator Cancel      oc", "icon": "delete"},
					"cancel": {"name": "Customer Cancel      cc", "icon": "cancel"},
					"noShow": {"name": "No Show     ns", "icon": "noShow"},
					"driverCompleted": {"name": "Driver Completed   dc", "icon": "driverCompleted"},
					"couldntService": {"name": "Couldn't Service'       cs", "icon": "couldntService"},
					"jobOnHold": {"name": "Hold the Job       h", "icon": "jobOnHold"},
					"sep2": "---------",
					"reDispatch": {"name": "ReDispatch rd", "icon": "reDispatch"},
					"broadCast": {"name": "Broadcast   bc", "icon": "broadCast"},
					"reStart": {"name": "ReSend  rs", "icon": "reStart"},
					"sep3": "---------",
					"jobLogs": {"name": "Job Logs", "icon": "jobLogs"},
					"sep3": "---------",
					"advancedOption":{
						"name": "Advanced Option",
						"items" :{
							"addWithSR": {"name": "Tag With Route", "icon": "addWithSR"},
							"jobHistory": {"name": "Job History", "icon": "jobHistory"},
							"sep4": "---------",
							"sendMsgToDriver": {"name": "sendMsgToDriver", "icon": "sendMsgToDriver"},
							"printThisJob": {"name": "Print Job", "icon": "sendMsgToDriver"},
							"sep5": "---------",
							"changeFleet": {"name": "changeFleet", "icon": "changeFleet"},
							"endZone": {"name": "Set EndZone", "icon": "endZone"},
							"callDriver": {"name": "Call Driver", "icon": "callDriver"},
							"sep6": "---------",
							"advancedOption":{
								"name": "Advanced Option",
								"items" :{
									"onBoard": {"name": "OnBoard       OB", "icon": "onBoard"},
									"onRoute": {"name": "OnRoute       OR", "icon": "onRoute"},
									"onSite": {"name": "OnSite         OS", "icon": "onSite"},
									"endTrip": {"name": "EndTrip       ET", "icon": "endTrip"},
								}
							}
						}
					}
				}
			});
		}
});
function sumbitValueFromDash(){
	document.forms["cookieSubmit"].submit();
	$("#pbar").jqm();
	$("#pbar").jqmShow();
	document.getElementById("pbar").innerHTML = "<center><h6><font color=\"blue\">Saving Your Preferences!!</h6></b></center></br>Please Wait...<font color=\"lime\"size=\"2px\"> Storing : <img src=\"images/Dashboard/progress2.gif\">"+ "<font size=\"1px\" ><b><a>" + getBar()+"</a></b></font></font></font>";

	window.status = "Please Wait the Page was Loading...";
	//document.cookieSubmit.submit();
}
