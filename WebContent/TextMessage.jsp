<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
  <%@page import="com.common.util.TDSConstants"%>
<html>
<head>
<link href="css/TextMessage.css" rel="stylesheet" type="text/css"/>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></link>
<script type="text/javascript" src="js/jquery.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="DashboardAjax/DispatchCommon.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script src="js/CalendarControl2.js" type='text/javascript'></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="Ajax/SystemUtilAjax.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<title>Text Message</title>
<script>
function loader()
{
	var xmlhttp=null;
	var fromDate;
	var toDate;
	var url;
	document.getElementById("msgid").value="";
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	fromDate=document.getElementById("fromDate").value;
	toDate=document.getElementById("toDate").value;
	if(fromDate == "" && toDate == "")
	{
		url='SystemSetupAjax?event=getMessageList';
	}else{
		url='SystemSetupAjax?event=getMessageList&fromDate='+fromDate+'&toDate='+toDate+'';
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"details\":"+text+"}";
	var obj = JSON.parse(jsonObj.toString());
	document.getElementById("messageTable").innerHTML="";
	var $tbl = $('<br>	<table style="width:100%">').attr('id', 'historyValues');
	$tbl.append($('<tr>').append($('<td colspan="5" style="width:100%; background-color:#4D6670; color:#ffffff;" align="center">').text("Message History")));
	$tbl.append($('<tr>').append(
	$('<td style="width:5%" align="center">').text("Id"),
	$('<td style="width:15%" align="center">').text("Sent Date"),
	$('<td style="width:15%" align="center">').text("Send By"),
	$('<td style="width:15%" align="center">').text("Received By"),
	$('<td style="width:65%" align="center">').text("Message")
	));
	for(var i=0;i<obj.details.length;i++)
	{
		$tbl.append($('<tr>').append(
				$('<td style="width:5%" align="center">').text(obj.details[i].msgId),
				$('<td style="width:15%" align="center">').text(obj.details[i].sent),
				$('<td style="width:15%" align="center">').text(obj.details[i].uName),
				$('<td style="width:15%" align="center">').text(obj.details[i].drId),
				$('<td style="width:65%" align="center" onclick="driver('+obj.details[i].msgId+')">').text(obj.details[i].msg)
			)
		);
	}
		$("#messageTable").append($tbl); 
}

function driver(msgid)
{
	var xmlhttp=null;
	var url;
	document.getElementById("msgid").value=msgid;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var driverId=document.getElementById("driverId").value;
	document.getElementById("driverId").value="";
	if(driverId=="")
	{
		url='SystemSetupAjax?event=getDriverList&msgId='+msgid;
	}else
		{
		url='SystemSetupAjax?event=getDriverList&msgId='+msgid+'&driverId='+driverId;
		}
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"details\":"+text+"}";
	var obj = JSON.parse(jsonObj.toString());
	document.getElementById("driverList").innerHTML=" ";
	var $tbl = " ";
	$tbl=$('<br><table style="width:100%">').attr('id', 'historyValues');
	$tbl.append($('<tr>').append($('<td colspan="4" style="width:100%; background-color:#4D6670; color:#ffffff;" align="center">').text("Driver List")));
	$tbl.append($('<tr>').append(
	$('<td style="width:20%" align="center">').text("DriverId"),
	$('<td style="width:20%" align="center">').text("MessageID"),
	$('<td style="width:25%" align="center">').text("Sent Time"),
	$('<td style="width:25%" align="center">').text("Response Time")
	));
	for(var i=0;i<obj.details.length;i++)
	{
		$tbl.append($('<tr>').append(
				$('<td style="width:20%" align="center">').text(obj.details[i].driverId),
				$('<td style="width:20%" align="center">').text(obj.details[i].msgId),
				$('<td style="width:25%" align="center">').text(obj.details[i].sendTime),
				$('<td style="width:25%" align="center">').text(obj.details[i].resTime)
			)
		);
	}
		$("#driverList").append($tbl); 
}
function getDriver()
{
	driver(document.getElementById("msgid").value);
}
function sendMessage()
{
	$("#pagetwo").jqm();
	$("#pagetwo").jqmShow();
}

function sendSms(){
	var driver_id="";
	var xmlhttp;
	driver_id=document.getElementById("driver_idSMS").value;
	var message=document.getElementById("msg").value;
	var delayDate=document.getElementById("delayDate").value;
	var delayTime=document.getElementById("delayTime").value;
	if(delayDate!=" ")
	{
		message=message+"&date="+delayDate;
	}
	if(delayTime!=" ")
	{
		message=message+"&time="+delayTime;
	}
	var url = 'control?action=openrequest&event=sendSMS';
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(driver_id!=""){
		url=url+'&driver_id='+driver_id+'';
	}
	if(message!=""){
		url=url+'&message='+message;
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text==1)
		{
		 	document.getElementById("driver_idSMS").value="";
		 	document.getElementById("delayDate").value="";
		 	document.getElementById("delayTime").value="";
		 	document.getElementById("msg").value="";
		 	document.getElementById("report").innerHTML="Message Sent Successfully";
		}
}

function reSet()
{
	document.getElementById("report").innerHTML="";
}
function closePage(){
	window.location.href="/TDS/control";
}

</script>
</head>
<body>
<form action="control" method="post" name="textMsg" id="textMsg">
<div data-role="page" id="pageone">
<input type='hidden' id='msgid'/>
<div class="header"><center>Message History</center></div>
 <div class="container">
 <center><input type="button" value="SendMessage" onclick="sendMessage()"/></center>
	<div ID="leftContent" style="height:650px;overflow: scroll;">
	<center><img alt="" src="images/home.png" onclick="closePage()"/> &nbsp;
	<input type="text" placeholder="Enter From Date" id="fromDate" name="fromDate" onclick="displayCalendar(document.textMsg.fromDate,'mm/dd/yyyy',this);"/> &nbsp; 
	<input type="text" placeholder="Enter To Date" id="toDate" name="toDate" onclick="displayCalendar(document.textMsg.toDate,'mm/dd/yyyy',this);"/> &nbsp; 
	<input type="button" value="Search" onclick="loader()""/>
	</center>
	<b id="messageTable">
	</b>
	</div>
<!-- 	<div class="rightContent" ID="rightContent", style="height:100%">
	<center><input type="text" placeholder="Enter Driver Id" id="driverId" name="driverId"/> &nbsp; <input type="button" value="Search" onclick="getDriver()"/></center>
	<b id="driverList">
	</b>
	</div>
 --></div>
</div>
<div date-role="page" data-dialog="true" id="pagetwo" class="jqmWindow" style="width:50%">
<center><h3>New Message</h3></center>
<center><input type="text" placeholder="Enter DriverId" name="delayDriverId" id="driver_idSMS" onkeydown="reSet()"/> &nbsp; <input type="text"  size="15" placeholder="Date YYYY-MM-DD" id="delayDate" name="delayDate" onfocus="showCalendarControl(this)" onkeydown="reSet()"/> 
&nbsp; Time
<select name=delayTime id="delayTime">
<option value="00:00:00">12:00 AM</option>
<option value="00:15:00">12:15 AM</option>
<option value="00:30:00">12:30 AM</option>
<option value="00:45:00">12:45 AM</option>
<option value="01:00:00">01:00 AM</option>
<option value="01:15:00">01:15 AM</option>
<option value="01:30:00">01:30 AM</option>
<option value="01:45:00">01:45 AM</option>
<option value="02:00:00">02:00 AM</option>
<option value="02:15:00">02:15 AM</option>
<option value="02:30:00">02:30 AM</option>
<option value="02:45:00">02:45 AM</option>
<option value="03:00:00">03:00 AM</option>
<option value="03:15:00">03:15 AM</option>
<option value="03:30:00">03:30 AM</option>
<option value="03:45:00">03:45 AM</option>
<option value="00:00:00">04:00 AM</option>
<option value="04:15:00">04:15 AM</option>
<option value="04:30:00">04:30 AM</option>
<option value="04:45:00">04:45 AM</option>
<option value="05:00:00">05:00 AM</option>
<option value="05:15:00">05:15 AM</option>
<option value="05:30:00">05:30 AM</option>
<option value="05:45:00">05:45 AM</option>
<option value="06:00:00">06:00 AM</option>
<option value="06:15:00">06:15 AM</option>
<option value="06:30:00">06:30 AM</option>
<option value="06:45:00">06:45 AM</option>
<option value="07:00:00">07:00 AM</option>
<option value="07:15:00">07:15 AM</option>
<option value="07:30:00">07:30 AM</option>
<option value="07:45:00">07:45 AM</option>
<option value="08:00:00">08:00 AM</option>
<option value="08:15:00">08:15 AM</option>
<option value="08:30:00">08:30 AM</option>
<option value="08:45:00">08:45 AM</option>
<option value="09:00:00">09:00 AM</option>
<option value="09:15:00">09:15 AM</option>
<option value="09:30:00">09:30 AM</option>
<option value="09:45:00">09:45 AM</option>
<option value="10:00:00">10:00 AM</option>
<option value="10:15:00">10:15 AM</option>
<option value="10:30:00">10:30 AM</option>
<option value="10:45:00">10:45 AM</option>
<option value="11:00:00">11:00 AM</option>
<option value="11:15:00">11:15 AM</option>
<option value="11:30:00">11:30 AM</option>
<option value="11:45:00">11:45 AM</option>
<option value="12:00:00">12:00 PM</option>
<option value="12:15:00">12:15 PM</option>
<option value="12:30:00">12:30 PM</option>
<option value="12:45:00">12:45 PM</option>
<option value="13:00:00">01:00 PM</option>
<option value="13:15:00">01:15 PM</option>
<option value="13:30:00">01:30 PM</option>
<option value="13:45:00">01:45 PM</option>
<option value="14:00:00">02:00 PM</option>
<option value="14:15:00">02:15 PM</option>
<option value="14:30:00">02:30 PM</option>
<option value="14:45:00">02:45 PM</option>
<option value="15:00:00">03:00 PM</option>
<option value="15:15:00">03:15 PM</option>
<option value="15:30:00">03:30 PM</option>
<option value="15:45:00">03:45 PM</option>
<option value="16:00:00">04:00 PM</option>
<option value="16:15:00">04:15 PM</option>
<option value="16:30:00">04:30 PM</option>
<option value="16:45:00">04:45 PM</option>
<option value="17:00:00">05:00 PM</option>
<option value="17:15:00">05:15 PM</option>
<option value="17:30:00">05:30 PM</option>
<option value="17:45:00">05:45 PM</option>
<option value="18:00:00">06:00 PM</option>
<option value="18:15:00">06:15 PM</option>
<option value="18:30:00">06:30 PM</option>
<option value="18:45:00">06:45 PM</option>
<option value="19:00:00">07:00 PM</option>
<option value="19:15:00">07:15 PM</option>
<option value="19:30:00">07:30 PM</option>
<option value="19:45:00">07:45 PM</option>
<option value="20:00:00">08:00 PM</option>
<option value="20:15:00">08:15 PM</option>
<option value="20:30:00">08:30 PM</option>
<option value="20:45:00">08:45 PM</option>
<option value="21:00:00">09:00 PM</option>
<option value="21:15:00">09:15 PM</option>
<option value="21:30:00">09:30 PM</option>
<option value="21:45:00">09:45 PM</option>
<option value="22:00:00">10:00 PM</option>
<option value="22:15:00">10:15 PM</option>
<option value="22:30:00">10:30 PM</option>
<option value="22:45:00">10:45 PM</option>
<option value="23:00:00">11:00 PM</option>
<option value="23:15:00">11:15 PM</option>
<option value="23:30:00">11:30 PM</option>
<option value="23:45:00">11:45 PM</option>
</select>
<!-- <input type="text" size="15" placeholder="Time HH:MM:SS" id="delayTime" name="delayTime" onkeydown="reSet()"/> -->
<textarea name="textMsg" id="msg" cols="56" rows="5" onkeydown="reSet()"></textarea></center>
<center><input type="button" value="Send" onclick="sendSms()"/></center>
<br><center><p id="report" style="color:blue;" value="Message Send Success Fully"></p></center>
</div>
</form>
</body>
</html>