<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.DriverRegistrationBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.passengerBO" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TDS (Taxi Dispatch System)</title>
<script type="text/javascript">
function paytypeOpen() { 
	 
	if(document.getElementById("payment").value=='V') {
		document.getElementById("payType").style.display='block';
		document.getElementById("payA/c").style.display='block'; 	
	} else   { 
		document.getElementById("payType").style.display='none';
		document.getElementById("payA/c").style.display='none'; 
 	}
} 

</script>
</head>
<%
	String frequen[] 
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			= {"No","Yes"};
 
%>
<body    onload="paytypeOpen()"     >
<form name="masterForm" action="control" method="post">
<jsp:useBean id="driverBO"   class="com.tds.tdsBO.DriverRegistrationBO" scope="request"  />
<jsp:useBean id="pass"   class="com.tds.tdsBO.passengerBO" scope="request"  />
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<!--<jsp:setProperty name="driverBO"  property="*"/>
-->

<%
  final char paysatus[] = {'0','V','C'};
   final String paystatusvalues[]={"Select","Voucher","CreditCard"};
%>
<%
if(request.getAttribute("passengerDetails")!=null){
	pass = (passengerBO)request.getAttribute("passengerDetails");
}
%>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"/>
<%if(request.getAttribute("edit")!=null){ %>
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.passengerEntry %>"/>
<!--<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.passengerEditUpdate %>"/>
-->
<input type="hidden" name="key" value="<%=pass.getPass_key() %>"/>
<%}else{ %>
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.passengerEntry %>"/>

<%} %>
<%
String flagValue="";
if(request.getParameter("flag")!=null)
flagValue= request.getParameter("flag");
%>
<input  type="hidden"   name="flag"   value="<%=flagValue %>" />
<input  type="hidden"  name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
 
                
<div class="rightCol">
<div class="rightColIn">
	 <h1>Passenger Entry</h1>
					<%
						String error ="";
						if(request.getAttribute("errors") != null) {
						error = (String) request.getAttribute("errors");
						}
					%>
					<% %>
					<div id="errorpage">
						<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %>
					</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab"> 
			<tr>
			<td>
				<table id="bodypage" width="540" >
				
				<tr>
					<td class="firstCol">First&nbsp;Name
					
					</td>
 					<td><input   type="text"  name="fname"  value="<%=pass.getFname() %>" /></td>
					<td class="firstCol">Last&nbsp;Name</td>
					<td><input type="text" name="lname" value="<%=pass.getLname() %>" /></td>
				</tr> 
				<tr>
					<td class="firstCol">Address1</td>
					<td> <input type="text" name="add1" value="<%= pass.getAdd1()  %>" /> </td>
					<td class="firstCol">Address2</td> 
					<td> <input type="text" name="add2" value="<%= pass.getAdd2()  %>" /> </td>
				</tr> 
				<tr>
					<td class="firstCol">City</td>
					<td> <input type="text" name="city" value="<%= pass.getCity() %>" /> </td>
					<td class="firstCol">State</td>
					<td> <input type="text" name="state" value="<%= pass.getState()  %>"/> </td>
				</tr> 
				<tr>
					<td class="firstCol">Zip&nbsp;Code</td>
					<td> <input type="text" name="zip"        value="<%= pass.getZip()%>"            onblur="zipCheck(this)"  /> </td>
					<td class="firstCol">Phone&nbsp;No</td>
					<td> <input type="text" name="phone" value="<%= pass.getPhone()%>"   onkeyup="setPhone(this)"  onblur="checkLength(this)"  /> </td> 
				</tr> 
				<tr>
					<td class="firstCol">Payment&nbsp;Type</td>
					 
					<td>
					<select  name="payment"     id="payment"            onchange="paytypeOpen()"    > 
					<%
						for(int counter = 0; counter < paysatus.length; counter ++) {
				    if(pass.getPayment_type() == paysatus[counter]) {
							 
 					%>
						<option value="<%= paysatus[counter] %>" selected="selected"><%= paystatusvalues[counter] %></option>
					<%
					} else {
					%>
						<option value="<%= paysatus[counter] %>"><%= paystatusvalues[counter] %></option>
					<%
					}
					}
					%>
					</select>
					</td>
					 				
					<td class="firstCol">
					<div  id="payType"    style="display:none">
					Payment&nbsp;A/c
					</div></td>
					<td>
					<div  id="payA/c"  style="display:none" >
					<input type="text" name="paymenta/c" value="<%= pass.getPayment_ac()  %>" /> 
					</div>
					</td> 
					
				</tr> 
				<tr>
					<td class="firstCol">User&nbsp;Id</td>
					<%if(request.getAttribute("edit")!=null){ %>
					<td colspan="4"> <input type="text" name="uid" value="<%= pass.getUid()  %>"  readonly="readonly" />  </td>
					<%}else { %>
					<td colspan="4"> <input type="text" name="uid" value="<%= pass.getUid()  %>"   />  </td>
					<%} %>
				</tr>
				<tr>
					<td class="firstCol">Password</td>
					<td>
					<%if(request.getAttribute("edit")!=null){ %>
					<input type="password" name="password" value="<%= pass.getPassword() %>" />
					<%}else{ %>
					<input type="password" name="password" value="" />
					<%} %>
					</td>
					<td class="firstCol">RE-Type&nbsp;Password</td>
					<td>
					<%if(request.getAttribute("edit")!=null){ %>
					<input type="password" name="repassword" value="<%= pass.getPassword() %>"/>
					<%}else{ %>
					<input type="password" name="repassword" value="" />
					<%} %>
					</td>
				</tr>
				<tr>
					<td colspan="7" align="center">
					<div class="wid60 marAuto padT10">
					<div class="btnBlue">
					<div class="rht">
					<input type="button" value="Submit" onclick="javascript:submit()" class="lft" />
					</div>
					<div class="clrBth"></div>
					</div>
					<div class="clrBth"></div>
					</div>			
					</td>
				</tr>  
				 </table>
			</td>
			</tr>
		</table>    
</div>    
</div>
           
           
           
           
              <div class="clrBth"></div>
     
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
                
             
