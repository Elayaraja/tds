 <%@page import="java.text.DecimalFormat"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.PaymentSettledDetail"%>
<%@page import="com.tds.tdsBO.InvoiceBO"%>
<%@page import="com.tds.tdsBO.VoucherBO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
th{
background-color: lightgreen;
}

#tab_color{
      background-color:rgb(46, 100, 121);
      color:black;
      
      }
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />

<title>Voucher Process</title> 
<%ArrayList<PaymentSettledDetail> voucher = (ArrayList<PaymentSettledDetail>)request.getAttribute("voucher"); 
PaymentSettledDetail invoiceBO = new PaymentSettledDetail();
AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user"); 
%>
<script type="text/javascript">
function printPage(){
	var vNo=document.getElementById("invoiceNo").value;
	var invoiceType =  document.getElementById("invoiceType").value;
	if(parseInt(invoiceType)==2){
		window.open("ReportController?event=reportEvent&ReportName=invoiceWithOdoMeter&output=pdf&invoiceNo="+vNo);
	}else{
		window.open("ReportController?event=reportEvent&ReportName=invoice&output=pdf&invoiceNo="+vNo);
	}
	}
function unlinkVoucher(transID,voucherNumber){
	var x=confirm("Do You want to Unlink this account from Invoice?");
	if(x==true){
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'RegistrationAjax?event=unlinkVoucher&transId='+transID+'&invoiceNo='+document.getElementById('invoiceNo').value+'&voucherNumber='+voucherNumber;	
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text==1){
			alert("Unlinked Successfully");
		}else{
			alert("Unlink Unsuccessful");
		}
	}
}
	
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
	</script>
</head>
<body>
<input type="hidden" name="action" value="finance" /> <input
			type="hidden" name="event" value="generateInvoice" /> <input
			type="hidden" name="module" id="module"
			value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />
            	
                	<% if(!voucher.equals(null) && !voucher.equals("")){%>
                	<div class="rightCol">
			<div class="rightColIn">
			<center><h4>Invoice Details</h4></center> 
	<table id="bodypage" width="100%" border="1"  align="center">
	<tr align="center" id="tab_color">
		<th width="9%">Account&nbsp;No</th>
		<th width="9%">Trip&nbsp;ID</th>
		<th width="10%"> Rider&nbsp;Name</th>
		<th width="8%">Cost&nbsp;Center</th>
		<th width="8%">Amount</th>
		<th width="8%">CreditCard Processing Amount</th>
		<th width="10%">Expiry&nbsp;Date</th>
		<th width="25%">Description</th>
		<th width="8%">Contact</th>
  		<th width="8%">Process</th>
	</tr>
	<%boolean colorlightGreen=true;
		String color;%>
		
	<%for(int i=0;i<voucher.size();i++) {
		colorlightGreen=!colorlightGreen;
		if(colorlightGreen){
	color="style=\"background-color:rgb(232, 250, 250);text-align:center;\"";
		}
		else
	color="style=\"background-color:white;text-align:center;\"";
	%>
	
	<tr <%=color%>>
		<td  ><%=voucher.get(i).getVoucherNo() %>  </td>
		<td  ><%=voucher.get(i).getTripId() %>  </td>
	<input type="hidden" id="invoiceNo" name="invoiceNo" value="<%=request.getParameter("invoiceNo")!=null?request.getParameter("invoiceNo"):""%>"/>
		<td ><%=voucher.get(i).getRiderName() %>  </td>
		<td ><%=voucher.get(i).getCostCenter() %>  </td>
		<td ><%DecimalFormat twoDForm = new DecimalFormat("#.00");%>
			<%=twoDForm.format(Double.parseDouble(voucher.get(i).getAmount()))%>  </td>
		<td ><%=voucher.get(i).getCc_Prcnt_Amt()%>  </td>
		<td ><%=voucher.get(i).getExpDate() %>  </td>
		<td ><%=voucher.get(i).getDescription() %>  </td> 
		<%-- <td ><%=voucher.get(i).getDelayDays() %>  </td>
		<td ><%=voucher.get(i).getFreq() %>  </td>  --%>
		<td ><%=voucher.get(i).getContact() %></td>
		<td><input type="button" name="unLink" id="unLink" value="Unlink" onclick="unlinkVoucher(<%=voucher.get(i).getTransId()%>,<%=voucher.get(i).getVoucherNo()%>)"/></td>
		
	</tr>
	<%} %>
<tr align="center"><td colspan="10"><input type="submit" id="print"name="print" value="print" onclick="printPage()" /></td></tr>
<%} %>
</table> </div></div>
<input type="hidden" name="totalRow"id="totalRow" value="<%=voucher.size()%>">
<input type="hidden" name="invoiceType" id="invoiceType" value ="<%=voucher.get(0).getVoucherFormat()%>"/>
</body>
</html>