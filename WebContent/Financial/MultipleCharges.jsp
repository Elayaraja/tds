<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.ManualSettleBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>


<html>
<head>

<script type="text/javascript">

function hideDriver(status) {
	if(status) { 
	document.getElementById('fromDriver').readOnly = true;
	document.getElementById('toDriver').readOnly = true; 
	}
	else {
		document.getElementById('fromDriver').readOnly = false;
		document.getElementById('toDriver').readOnly = false;
	} 	
}


function selAll()
{
	if(document.getElementById('all').checked)
	{
		document.getElementById('CreditCard').checked = true;
		document.getElementById('Penalty').checked = true;
		document.getElementById('Voucher').checked = true;
		
	} else {
		document.getElementById('CreditCard').checked = false;
		document.getElementById('Penalty').checked = false;
		document.getElementById('Voucher').checked = false;
		
	}
}

function chckAll(size)
{

  if(document.getElementById('chck_all').checked)
  {	
	for(var i=0;i<Number(size);i++)
	{
  	 document.getElementById('Settle'+i).checked = true;
 	}
  } else {
	  for(var i=0;i<Number(size);i++)
		{
	  	  document.getElementById('Settle'+i).checked = false;
	 	}
  }
}

</script>

<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
	
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manual Settle</title>
</head>
<body>
<form method="post" action="control" name="masterForm" onsubmit="return caldatevalidation();">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />

<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="manualccp">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="multiCharges">


            	<div class="leftCol">
                	<div class="clrBth">
                    </div>
                </div>
                <div class="rightCol">
<div class="rightColIn">
               					<c class="nav-header"><center>Multiple Charges</center></c>

 <div id="errorpage" style="color:red">
 <%=(request.getAttribute("error")!=null)?request.getAttribute("error"):"" %>
 </div>
 
 <div  id="errorpage" style="color:blue" align="center">
 
 <%=(request.getAttribute("message")!=null)?request.getAttribute("message"):"" %>
 </div>
 
<!-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
         <td> -->
         <center><br><br>
         	<table id="bodypage" width="80%" align=center>
          		<tr>
         			<td  width="15%" > <input type="checkbox"    style="vertical-align: middle"   name="ApplyToAllDrivers" id="ApplyToAllDrivers" <%=request.getParameter("ApplyToAllDrivers")==null?"":"selected" %>   onclick="hideDriver(this.checked)"    >Apply To All Drivers
					</td>
					<td  width="15%"> <input type="checkbox"  style="vertical-align: middle"   name="ApplyToRangeOfDrivers" id="ApplyToRangeOfDrivers" <%=request.getParameter("Penalty")==null?"":"selected" %>>Apply To Range Of Drivers
					</td>
					<td  width="15%" colspan="2"  > <input type="checkbox"  width="100%"    style="vertical-align: middle"  name="ApplyToListOfDrivers" id="ApplyToListOfDrivers" <%=request.getParameter("Voucher")==null?"":"selected" %>>Apply To List Of Drivers
					</td>
					 
				</tr> </table><br>
				<table width="80%">
				<tr style="background-color:#CDD5DB">
				<td class="firstCol" >Description</td>
				<td><input type="text" size=10 name="Description" value="<%=request.getAttribute("Description")==null?"":request.getAttribute("Description") %>" ></td>
				<td class="firstCol" >Effective Date</td>
				<td><input type="text" size=10 name="EffectiveDate" value="<%=request.getAttribute("EffectiveDate")==null?"":request.getAttribute("EffectiveDate") %>"   onfocus="showCalendarControl(this);"  readonly="readonly"  ></td>
			</tr>
			<tr style="background-color:#CDD5DB">
				<td class="firstCol">From Driver</td>
				 <td><input type="text" size=10 name="fromDriver"         id="fromDriver"   value="<%=request.getAttribute("fromDriver")==null?"":request.getAttribute("fromDriver") %>" ></td>
				<td class="firstCol">To Driver</td>
				<td><input type="text" size=10 name="toDriver"   id="toDriver"  value="<%=request.getAttribute("toDriver")==null?"":request.getAttribute("toDriver") %>" ></td>
			</tr>
			<tr style="background-color:#CDD5DB">
				<TD class="firstCol">
					Amount
				</TD>
				<td>
					<input type="text" size=10 name="amt" value="<%=request.getAttribute("amt")==null?"":request.getAttribute("amt") %>" >
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr style="background-color:#CDD5DB">
				<td> <input type="checkbox"        style="vertical-align: middle"      name="applyToMisc" id="ApplyToAllDrivers" <%=request.getAttribute("ApplyToAllDrivers")==null?"":"selected" %>> Apply To Misc
				</td>
				<td> <input type="checkbox"    style="vertical-align: middle"   name="applyToCharges" id="ApplyToRangeOfDrivers" <%=request.getAttribute("Penalty")==null?"":"selected" %>> Apply To Charges
				</td>
				<td></td>
				<td   colspan="4" > 
										<select name="ChargeType"  id='selecttype'> 
										   	<option value="Credit">Credit</option> 
											<option value="Charge">Charge</option>  
										</select> 
				</td> 
			</tr>
				
			<tr>
				
				<td></td>
				<td></td>
				<td></td>
				 
				</tr>
			 <tr><td></td>
				<td></td>
				<td></td>
				<td></td></tr>
				 <tr>
				<td colspan="4" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="ProcessButton" value="Process"   class="lft"  /> 
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
				</td>
			</tr> 
			</table>
<!-- </td>
</tr>
</table> --> </center>
  </div>
 </div>
 
                <div class="clrBth"></div>
           
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
</form>
</body>
</html>
