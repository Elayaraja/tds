
<%@page import="com.charges.bean.ChargesBO"%>
<%@page import="com.tds.tdsBO.PaymentSettledDetail"%>
<%@page import="com.tds.tdsBO.InvoiceBO"%>
<%@page import="com.tds.tdsBO.VoucherBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.common.util.TDSProperties"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Voucher Process</title>
<script type="text/javascript" src=<%=TDSProperties.getValue("googleMapV3") %>></script>
<script type="text/javascript" src="js/MapForORDetails.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<script type="text/javascript" src="js/OpenRequestDetails.js"></script>
<%@page import="com.tds.tdsBO.FleetBO"%>
<style type="text/css">
#unInvoicedVouchers th{
	background-color: lightGrey;
	border-color: silver;
}
#unInvoicedVouchers table {
	background-color: palegoldenrod;
}
#newCcUnInVoice th {
	background-color:rgb(46, 100, 121);
      color:white;
}
#newCcUnInVoice table{
	background-color:white;
}
#tab_color{
      background-color:rgb(46, 100, 121);
      color:white;
      }
</style>
<script type="text/javascript" src="js/jqModal.js"></script>

<%
ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>();
if(session.getAttribute("fleetList")!=null) {
	fleetList=(ArrayList)session.getAttribute("fleetList");
}
 ArrayList<PaymentSettledDetail> voucher =new ArrayList<PaymentSettledDetail>();
String voucherNumber="";
int verifyCheck =0;
if(request.getAttribute("voucher")!=null){
 voucher = (ArrayList<PaymentSettledDetail>)request.getAttribute("voucher");
}
if(request.getAttribute("voucherNo")!=null){
	voucherNumber=(String)request.getAttribute("voucherNo");
}
if(request.getAttribute("verifyCheck")!=null){
	verifyCheck= (Integer)request.getAttribute("verifyCheck");
}
 PaymentSettledDetail invoiceBO = new PaymentSettledDetail();
AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");%>
<% int invoiceNo = request.getAttribute("invoiceNo") == null?0:Integer.parseInt( request.getAttribute("invoiceNo").toString()); %>
<script type="text/javascript">
function loadFile(docId){
	document.getElementById("docLoader").src='control?action=finance&event=generateInvoice&docId='+docId;
	$('#viewSign').jqm();
	$('#viewSign').jqmShow();
}
function hidePopup(){
		$("#viewSign").jqmHide();
	}
	function validateForm() {
		//alert("hai");
		if (document.getElementById('voucherNo').value == ''
				&& document.getElementById('cccenter').value == ''&& document.getElementById('ccode').value == '') {
			alert("Please provide Voucher No or Cost Ceneter or company Code");
			return false;
		} else {
			//document.forms["voucherProcess"].submit();
			return true;
		}
	}

	checked = false;
	function checkedAll() {
		if (checked == false) {
			checked = true;
		} else {
			checked = false;
		}
		for ( var i = 0; i < document.generateInvoice.generate.length; i++) {
			document.generateInvoice.generate[i].checked = checked;
		}
	}
	function fixValues(){
		var accountValue=document.getElementById("voucherNo1").value;
		document.getElementById("voucherNo").value=accountValue;
	}
	function searchDocuments(){
		if(document.getElementById('voucherNo1').value =="" && document.getElementById('cccenter').value =="" && document.getElementById('ccode').value ==""&&  document.getElementById('fDate').value ==""&&  document.getElementById('tDate').value ==""){
			document.getElementById("errorPageScript").innerHTML="Please provide atleast one field value";
			document.getElementById("errorPageScript").style.display="block";
			return false;
		} else{
			document.getElementById('search').value="search";
			document.getElementById("errorPageScript").innerHTML="";
			document.getElementById("errorPageScript").style.display="none";
			var accountValue=document.getElementById("voucherNo1").value;
			document.getElementById("voucherNo").value=accountValue;
	    	document.voucherProcess.submit();
		}
			
	}
	function verifySingleVoucher(i,transId){
	//if(document.getElementById("generate"+i).checked){
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'finance?event=verifyVoucher&transId='+transId;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text!="0"){
		document.getElementById("stat"+i).innerHTML ="Operator verified";
		document.getElementById("generate"+i).checked =true;
		document.getElementById("verifySingle"+i).disabled =true;
		} 
		
	//}
	}
	
function selectedVoucherForInvoice(totalSize){
		var vouchersToMark="";
		for(var i=0;i<totalSize;i++){
			if(document.getElementById("voucherSelect"+i).checked==true){
				vouchersToMark=vouchersToMark+document.getElementById("voucherSelect"+i).value+";";
			}
		}
		if(vouchersToMark!=""){
			vouchersToMark=vouchersToMark.substring(0,vouchersToMark.length-1);
			var xmlhttp = null;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {

				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'finance?event=generateUninvoice&voucher='+vouchersToMark;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if(text!=""&&text!="0"){
				var jsonObj = "{\"invoiceNumbers\":"+text+"}" ;
				var obj = JSON.parse(jsonObj.toString());
				if(obj.invoiceNumbers.length>0){
					for(var j=0;j<obj.invoiceNumbers.length;j++){
						document.getElementById("unInvoicedVouchers").innerHTML ="";
						window.open("ReportController?event=reportEvent&ReportName=invoice&output=pdf&invoiceNo="+obj.invoiceNumbers[j].IN);
					}
				}
			} else {
				alert("Sorry cannot generate invoice. Please call administrator!!");
			}
		} else {
			alert("Please Select Some Vouchers");
		}
		
	}
	
	
	function getUnInvoiceVouchers(){
		document.getElementById("unInvoicedVouchers").innerHTML ="";
		var verifyOption=document.getElementById("verifyCheck").value;
		var assoccode = "";
		if(document.getElementById('fleetSearch')!=null){
			if(document.getElementById('fleetSearch').value!="all"){
				assoccode = document.getElementById('fleetSearch').value;
			}
		}
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'finance?event=getUnInvoice&verify='+verifyOption+'&assoccode='+assoccode;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"unInvoice\":"+text+"}";
		jsonObj = JSON.parse(obj.toString());
		var $tbl = $('<table>').attr('id', 'unInvoiceTable');
		$tbl.append($('<tr align="Center">').append(
				$('<th>').text("Select"),
				$('<th>').text("AccountNo"),
				$('<th>').text("RiderName"),
				$('<th>').text("Select"),
				$('<th>').text("Total")));
		for(var i=0;i<jsonObj.unInvoice.length;i++){
			$tbl.append($('<tr align="Center">').append(
					$('<input />', { type: 'radio', id: 'cb',name:'cb', value: jsonObj.unInvoice[i].VN,onClick:'selectedVoucher('+jsonObj.unInvoice[i].VN+')' }),
					$('<td>').text(jsonObj.unInvoice[i].VN),
					$('<td>').text(jsonObj.unInvoice[i].RN),
					$('<td>').text(jsonObj.unInvoice[i].T),
					$('<input />', { type: 'checkbox', id: 'voucherSelect'+i,name:'voucherSelect'+i, value: jsonObj.unInvoice[i].VN })));
		}
		$tbl.append($('<tr>').append(
				$('<input />', { type: 'button', id: 'generateShort',name:'generateShort', value:'Generate', onClick:'selectedVoucherForInvoice('+jsonObj.unInvoice.length+')'})));
		$('#unInvoicedVouchers').append($tbl);
	}

	
	function startSSE(){
		var n=0;
		document.getElementById("newCcUnInVoice").innerHTML ="";
		document.getElementById("noOfVouchersPending").innerHTML ="";
		document.getElementById("manulMailButton").innerHTML ="";
		/* var allInvoiceNo=""; */
		
		var fdate=document.getElementById('fDate').value;
		var tdate=document.getElementById('tDate').value;		
		var verifyOption=document.getElementById("verifyCheck").value;
		var assoccode = "";
		if(document.getElementById('fleetSearch')!=null){
			if(document.getElementById('fleetSearch').value!="all"){
				assoccode = document.getElementById('fleetSearch').value;
			}
		}
		//alert("fleetNo:"+assoccode);
		
		var manualInvoices = [];
		var manualPrint="";
		
		var closed=0;
		var $tbl = $('<table align="Center" width="80%" >').attr('id', 'newCcUnInVoice');
	    document.getElementById('getCcInvoiceVerified').disabled=true;
	    if(verifyOption==1){
	    	$tbl.append($('<tr align="center">').append($('<th colspan="9" align="center">').text("Driver Verified Invoices")));
	    }else if(verifyOption==4){
	    	$tbl.append($('<tr align="center">').append($('<th colspan="9" align="center">').text("Operator Verified Invoices")));
	    }else if(verifyOption==2){
	    	$tbl.append($('<tr align="center">').append($('<th colspan="9" align="center">').text("All Verified Invoices")));
	    }else if(verifyOption==0){
	    	$tbl.append($('<tr align="center">').append($('<th colspan="9" align="center">').text("All Verified Invoices")));
	    }else{
	    	$tbl.append($('<tr align="center">').append($('<th colspan="9" align="center">').text("All UnVerified Invoices")));
	    }
	
		$('#newCcUnInVoice').append($tbl);
		
		$tbl.append($('<tr align="Center">').append(
				$('<th width="8%">').text("AccountNo"),
				$('<th width="15%">').text("Description"),
				$('<th width="8%">').text("No. of Vouchers"),
				/*$('<th width="15%">').text("InvoiceGenerated"), */
				$('<th width="8%">').text("Invoice Number"),
				$('<th width="8%" align="justify">').text("Amount"),
				/* $('<th width="10%">').text("InvoiceType"), */
				$('<th width="15%">').text("Account Payment"),
				$('<th width="15%">').text("Email"),
				$('<th width="10%">').text("")
				));
		$('#newCcUnInVoice').append($tbl);
		if(closed==0){
	        if (typeof(EventSource) !== "undefined") {
	            //var msg = document.getElementById("textID").value;
	        	var eventSource = new EventSource("Invoiceprocess?fdate="+fdate+"&tdate="+tdate+"&verified="+verifyOption+"&assoccode="+assoccode);
			 	eventSource.onopen = function(openevent){
				    // do something when the connection opens
				};
			 
		        eventSource.onmessage = function(event) {
		        	//alert(event.data);
		            //document.getElementById('newCcUnInVoice').innerHTML = event.data;
		            var obj = "{\"unInvoice\":"+event.data+"}";
		    		jsonObj = JSON.parse(obj.toString());
		    				    		
					for(var i=0;i<jsonObj.unInvoice.length;i++){
						/* allInvoiceNo = allInvoiceNo + jsonObj.unInvoice[i].IN+";";  */						
						if(jsonObj.unInvoice[i].EMS=="Manual"){
							manualInvoices.push(jsonObj.unInvoice[i].IN);
							if(n==0){
								manualPrint = manualPrint + jsonObj.unInvoice[i].IN;
							}else{
								manualPrint = manualPrint +",";
								manualPrint = manualPrint + jsonObj.unInvoice[i].IN;
							}
							n=1;
						}
						
						$tbl.append($('<tr align="Center">').append(
								$('<td>').text(jsonObj.unInvoice[i].Ac),
								$('<td>').text((jsonObj.unInvoice[i].AN==null || jsonObj.unInvoice[i].AN=="")?"N/A":jsonObj.unInvoice[i].AN),
								$('<td>').text(jsonObj.unInvoice[i].NV),
								/*$('<td>').text(jsonObj.unInvoice[i].MSG), */
								$('<td>').text(jsonObj.unInvoice[i].IN),
								$('<td align="justify">').text("$ "+jsonObj.unInvoice[i].Am),
								/* $('<td>').text(jsonObj.unInvoice[i].PT), */
								//+" - "+manualPrint
								$('<td>').text(jsonObj.unInvoice[i].CC),
								$('<td>').text(jsonObj.unInvoice[i].EM),
								$('<input />', { type: 'button', id: 'inVoice_'+jsonObj.unInvoice[i].IN,name:'inVoice_'+jsonObj.unInvoice[i].IN, value:'Print_'+jsonObj.unInvoice[i].IN, onClick:'print('+jsonObj.unInvoice[i].IN+')'})
								));
						$('#newCcUnInVoice').append($tbl);
					}
		        };
		        
		        eventSource.addEventListener('close', function(event) {
		              //output.innerHTML += event.data + "<hr/>";
		              //alert("number of manually mailed invoices:"+manualInvoices.length+" manualInvoices:"+manualPrint);
		              closed=1;
		      	      document.getElementById('getCcInvoiceVerified').disabled=false;
		              if(event.data==0){
		              	document.getElementById("noOfVouchersPending").innerHTML ="No Accounts Found";
		              }else{
		            	  document.getElementById("noOfVouchersPending").innerHTML ="All Accounts Processed Successfully";
		            	  /* $tbl.append($('<tr align="center">').append(
		            			 $('<th colspan="6" align="center">').text(""),
		            			 $('<input />', { type: 'button', id: 'all_InVoice',name:'all_InVoice', value:'Print_All', onClick:'printAll('+allInvoiceNo+')'})
		            			  ));
		            	  $('#newCcUnInVoice').append($tbl); */
		              }
		              if(manualInvoices.length>0){
		            	  var element = document.createElement("input");
		            	    //Assign different attributes to the element.
		            	    element.type = 'button'; //type
		            	    element.id = 'manulMailButton'; //id
		            	    element.value = 'PrintAll_Manual Mail Invoices'; // default value to be the type string
		            	    element.name = 'manulMailButton';  //name
		            	    element.onclick = function() { // Note this is a function
		            	    	alert("Generating Reports of "+manualInvoices.length+" Invoices: "+manualPrint);
		            			window.open("ReportController?event=reportEvent&ReportName=MultipleInvoice&output=pdf&multipleInvoiceNo="+manualPrint);
		            	    };

		            	    var reference = document.getElementById("manulMailButton");
		            	    //Append the element in page (in span)
		            	    reference.appendChild(element);
		              }else{
		            	  document.getElementById("manulMailButton").innerHTML ="No Manual Mailed Invoices";
		              }
		              eventSource.close();
		              //document.getElementById('getCcInvoice').disabled=false;
		              return;
		              }, false);
		        
		        eventSource.addEventListener('size', function(event) {
	              //output.innerHTML += event.data + "<hr/>";
	              document.getElementById("noOfVouchersPending").innerHTML =event.data;
	              return;
	              }, false);
	            
		        
	          } else {
	        	  document.getElementById("newCcUnInVoice").innerHTML = "Sorry, Server-Sent Events are not supported in your browser";
	          }
		}
		
	    return;
	}
	

	/* function printAll(allInvoiceNo){
	alert("InvoiceNo:"+allInvoiceNo);
	var vNo = allInvoiceNo.split(";"); 
	for(var i=0; i<vNo.length;i++){
		alert("InvoiceNo:"+vNo[i]);
	}
		//window.open("ReportController?event=reportEvent&ReportName=invoice&output=pdf&invoiceNo="+vNo);
	} */
	
	
	function print(vNo){
		alert("InvoiceNo:"+vNo);
			window.open("ReportController?event=reportEvent&ReportName=invoice&output=pdf&invoiceNo="+vNo);
		}
	 	 
	function selectedVoucher(voucher){
		document.getElementById("voucherNo1").value=voucher;
	}
	function deleteFile(transId,tripId,x){
        var xmlhttp = null;
        if (window.XMLHttpRequest)
        {
                xmlhttp = new XMLHttpRequest();
        } else {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = 'control';
        var parameters='action=finance&event=generateInvoice&delete=YES&transId='+transId+'&tripId='+tripId;
        xmlhttp.open("POST",url, false);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.setRequestHeader("Content-length", parameters.length);
        xmlhttp.setRequestHeader("Connection", "close");
        xmlhttp.send(parameters);
        var text = xmlhttp.responseText;
        if(text>=1){ 
                var table = document.getElementById("bodypage");
                table.deleteRow(x.parentNode.parentNode.rowIndex);
         } 
}
	/* $("#cb").click(function() {
		var selected = $("#cb input[type='radio']:checked").val();
		alert("selected:"+selected);
	}); */
</script>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript">
/* function showReportOnLoad()
{ */
	function plotVoucerhNo(i){
	if(document.getElementById("generate"+i).checked){
		document.getElementById("voucherValue"+i).checked = true;
	}else{
		document.getElementById("voucherValue"+i).checked = false;
	}
}

	
	$(document).ready ( function(){
	var assocode="<%=adminBO.getAssociateCode()%>";
	var invoiceNo = "<%=invoiceNo%>";
	if( typeof invoiceNo != undefined && invoiceNo != 0){
			window.open("ReportController?event=reportEvent&ReportName=invoice&output=pdf&invoiceNo="+invoiceNo);
			}
	});/* } */</script>
</head>

<body ><%
			String error = (String) request.getAttribute("error");
			if (request.getParameter("error") != null) {
				error = request.getParameter("error");

			}
		%>
		<div id="errorpage">
			<%=(error != null && error.length() > 0) ? "" + error : ""%>
		</div>
		<div id="errorPageScript"  style="display: none;color:#E41B17;size: 20px;font-weight: bold;">
		</div>
		
<!-- <script>window.onload=showReportOnLoad</script> -->
<form name="voucherProcess" id="voucherProcess" method="post" action="control" onsubmit="fixValues()">
		<input type="hidden" name="action" value="finance" /> <input
			type="hidden" name="event" value="generateInvoice" /> <input
			type="hidden" name="module" id="module"
			value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />
					<input type="hidden" name="screenValue" id="screenValue" value="1" />		
			<input
			type="hidden" name="search" id="search" value="" />		
		<div class="leftCol">
			<div class="clrBth"></div>
		</div>
		<div class="rightCol">
			<div class="rightColIn">
				<h2><center>Generate Invoice</center></h2>
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="driverChargTab">
					<tr>
						<td class="firstCol">Voucher NO</td>
						<td><input type="text" name="voucherNo1" size="10" 
							id="voucherNo1" value="<%=voucherNumber==null?"":voucherNumber%>" autocomplete="off"/>
								<ajax:autocomplete 
  					fieldId="voucherNo1"
  					popupId="model-popup1"
  					targetId="voucherNo1"
  					baseUrl="autocomplete.view"
  					paramName="getAccountList"
  					className="autocomplete"
  					progressStyle="throbbing"/>
							<input type="hidden" name="voucherNo" id="voucherNo" value="<%=voucherNumber==null?"":voucherNumber%>"/>
						</input></td>
						<td class="firstCol">Cost center</td>
						<td><input type="text" name="cccenter" size="10"
							id="cccenter" />
						</td>
					</tr>
					
					<tr>
					<td class="firstCol">From Date</td>
						<td><input type="text" name="fDate" size="10" id="fDate" readonly="readonly" onfocus="showCalendarControl(this);" />
						</td>
						
						
						<td class="firstCol">To Date</td>
						<td><input type="text" name="tDate" size="10" id="tDate" readonly="readonly"  onfocus="showCalendarControl(this);" />
						</td>
						
					</tr>
					
					<tr>
					<td class="firstCol">Company Code</td>
						<td><input type="text" name="ccode" size="10"
							id="ccode" />
						</td>
					<td>Verification Process</td>
					<td class="firstCol">
					<select id="verifyCheck" name="verifyCheck">
					<option  value=0 selected="selected">All</option>
					<option  value=1 <%=verifyCheck==1?"selected":"" %>>Driver Verified</option>
					<option  value=2 <%=verifyCheck==2?"selected":"" %>>Verified</option>
					<option  value=3 <%=verifyCheck==3?"selected":"" %>>UnVerified</option>
					<option  value=4 <%=verifyCheck==4?"selected":"" %>>Operator Verified</option>
					</select>	</td>	
					</tr>
					
					<%if( fleetList!=null && fleetList.size()>0) {%>
					<tr><td></td>
					<td align ="center">Company Name</td>
					<td> <select name="fleetSearch" id="fleetSearch" >
					<option value="all" >All</option>								
                      <%if(fleetList!=null){
							for(int j=0;j<fleetList.size();j++){ %>
								<option value="<%=fleetList.get(j).getFleetNumber()%>" ><%=fleetList.get(j).getFleetName() %></option>
								<%=fleetList.get(j).getFleetNumber()%>
							<% }
						}%> 
                       </select></td>
                       <td></td>
                       </tr>
					<%}%>
					
					<tr>
						<td align="center">
							<input type="button" name="search" value="search" onclick="return searchDocuments()"/>
						</td>
						<td align="center">
							<input type="button" name="getUnInvoice"  id="getUnInvoice" value="UnInvoiced Vouchers" onclick="getUnInvoiceVouchers()"/>
						</td>
						<td align="center">
							<input type="button" name="getCcInvoiceVerified"  id="getCcInvoiceVerified" value="Generate UnInvoiced Vouchers in SSE" onclick="startSSE()"/>
						</td>
						
					</tr>
				</table></div></div>
	</form>
	<form name="generateInvoice" method="post" action="control" >
		<input type="hidden" name="action" value="finance" /> <input
			type="hidden" name="event" value="generateInvoice" /> <input
			type="hidden" name="module" id="module"
			value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />

		<%
			if (!voucher.equals(null) && !voucher.isEmpty()) {
		%>
		<div class="rightCol">
			<div class="rightColIn">
		<table width="100%" border="0" cellspacing="0" cellpadding="0" align="left"
			class="driverChargTab">
			<tr>
				<td>
					<table id="bodypage" border="1"  align="center">
						<tr id="tab_color" align="center">
							<td width="9%">Trip ID</td>
							<td width="9%">Account No.</td>
							<td width="8%">Rider Name</td>
							<td width="6%">Cost Center</td>
							<td width="6%">Amount</td>
							<td width="9%">Service Date</td>
<!-- 							<td class="firstCol">Description</td>
 -->							<!-- <td class="firstCol">Delay Days</td> -->
							<!-- <td class="firstCol">Frequent</td> -->
							<td width="7%">Contact</td>
							<td width="5%">Generate Invoice</td>
							<td width="12%">Payment Received Status</td> 
							<td width="7%">Driver Payment Status</td>
							<td width="5%">View Sign</td>
							<td width="5%">Details</td>
							<td width="5%">Delete</td>
							<td width="9%">Verified/ Un-Verified</td>
						</tr>
						<%
							boolean colorlightGreen = true;
								String color;
						%>

						<%
							for (int i = 0; i < voucher.size(); i++) {
									colorlightGreen = !colorlightGreen;
									if (colorlightGreen)
										color="style=\"background-color:rgb(242, 250, 250);text-align:center;\"";
									else
										color="style=\"background-color:white;text-align:center;\"";
						%>
						<tr>
							<td <%=color%>><a href="control?action=finance&event=generateInvoice&module=financeView&edit=yes&voucherNo=<%=voucher.get(i).getVoucherNo()%>&transId=<%=voucher.get(i).getTransId()%>&tripId=<%=voucher.get(i).getTripId()%>"><%=voucher.get(i).getTripId()%></a></td>
							<td <%=color%>><%=voucher.get(i).getVoucherNo()%></td>
							<td <%=color%>><%=voucher.get(i).getRiderName()%></td>
							<td <%=color%>><%=voucher.get(i).getCostCenter()%></td>
							<td <%=color%>><%=voucher.get(i).getAmount()%></td>
							<td <%=color%>><%=voucher.get(i).getServiceDate()%></td>
							<%-- <td <%=color%>><%=voucher.get(i).getDescription()%></td> --%>
							<%-- <td <%=color%>><%=voucher.get(i).getDelayDays()%></td>
							<td <%=color%>><%=voucher.get(i).getFreq()%></td> --%>
							<td <%=color%>><%=voucher.get(i).getContact()%></td>
							<td <%=color%>><input type="checkbox" name="generate"
								id="generate<%=i%>"  onclick="plotVoucerhNo(<%=i%>)"  value="<%=voucher.get(i).getTransId()%>" <%=voucher.get(i).getPaymentReceivedStatus()!=2?"disabled":"" %> <%=voucher.get(i).getVerified()>0?"checked=checked":"" %>>
							<input type="checkbox" name="voucherValue" id="voucherValue<%=i%>"  value="<%=voucher.get(i).getVoucherNo()%>" style="display:none"></td>
							
							<td  <%=color%>><a id="stat<%=i%>"><%String status= "";
							if(voucher.get(i).getPaymentReceivedStatus()!=2){
								status = "Unsettled & ";
							}else{
								status = "Settled & ";
							}
								if(voucher.get(i).getVerified()==0){
									status +="Unverified";
								}else if(voucher.get(i).getVerified()==1){
									status += "Driver Verified"; 
								}else if(voucher.get(i).getVerified()==4){
									status += "Operator verified";
								}else{
									status += "Verified";
								}%>
								<%=status %></a>
								<%String  btnDisable ="";
								if(voucher.get(i).getPaymentReceivedStatus()!=2){
									btnDisable = "disabled=disabled";
								}else if(voucher.get(i).getVerified()>1){
									btnDisable = "disabled=disabled";
								}%>
							</td>
						<td <%=color%>> <%=voucher.get(i).getDriverPayedStatus()==2?"paid":"Unpaid"%></td>
						<td <%=color%>><input type="button" name="sign" id="sign" value="View" onclick="loadFile(<%=voucher.get(i).getTransId()%>)"></td>
						<td <%=color%>><input type="button" id="detailShow_<%=i %>" value="Details" onclick="openORDetails(<%=voucher.get(i).getTripId()%>)"/></td>
						<td <%=color%>><input type="button" name="sign" id="sign" value="Delete" onclick="deleteFile(<%=voucher.get(i).getTransId()%>,<%=voucher.get(i).getTripId()%>,this)"></td>	
						<td <%=color%>><input type="button" name="verifySingle" id="verifySingle<%=i%>" value="Mark as verified"<%=btnDisable %> onclick="verifySingleVoucher(<%=i%>,<%=voucher.get(i).getTransId()%>)"></td>
						
						</tr>


						<%
							}
						%>

						<!-- <input type="button" name="CheckAll" value="Check All"onClick="checkAll(document.generateInvoice.generate)"/>
<input type="button" name="UnCheckAll" value="Uncheck All"onClick="uncheckAll(document.generateInvoice.generate)"/> -->
						<tr id="tab_color" align="center">
							<td colspan="7"><input type="submit" id="generateInvoice" name="generateInvoice" value="generateInvoice" /></td>
							<td colspan="1">Select All:<input type='checkbox' name='checkall' onclick='checkedAll();'>
							<td colspan="6"><input type="submit" id="verify" name="verify" value="Verify" /></td>
						</tr>
						<%
							} else if (request.getParameter("search") != null) {
						%>
						<tr>
							<td><font color="red">No records Found</font>
							</td>
						</tr>
						<%
							}
						%>
					</table> <input type="hidden" name="totalRow" id="totalRow"
					value="<%=voucher.size()%>">
				</td>
			</tr>
		</table>	</div></div>
		<br/>
		<div id="noOfVouchersPending"  align="center" style="font: bolder; color:green; vertical-align: middle; font-size: medium;"></div>
		<br/>
		<div id="newCcUnInVoice" style="vertical-align: middle;"></div>
		<br/>
		<div id="manulMailButton" align="center" style="font: bolder; color:green; vertical-align: middle; font-size: medium;"></div>
		<br/>
		<div id="unInvoicedVouchers" style="vertical-align: middle;"></div>
		<br/>	
		<div id="viewSign" class="jqmWindow" style="display: none;height:202px;width: 350px;margin-left:10%;z-index:3000;">
		<img src="images/close.png" onclick="hidePopup()" />
		<div class="">
		<iframe  src="" id="docLoader" name="docLoader"  style="width:99%;height:60%;">
		</iframe></div>
	     </div>
		<div id="jobDetailsMapDash" class="jqmWindow" style="display: none;height:430px;width: 750px;margin-left:10%;z-index:3000;">
		<table id="tabForDetails" style="z-index: 3000; width: 750px; cursor: pointer;">
		<tr style="background-color: black;">
		<td id="detailsOR" onclick="openORDetails('')">
		<font id="detailsORFont" color="white">Details</font>
		</td> 
		<td id="detailsLogs" onclick="showLogs()">
		<font id="detailsLogsFont" color="white">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
				<div id="jobDetailsMapDashDiv" style="height:330px;width: 550px;margin-left:10%;">
	
	</div>
		</div>
		
			<div id="paymentDetailsDash" class="jqmWindow" style="display: none;height:330px;width: 550px;margin-left:10%;">
			<table id="tabForDetails" style="z-index: 3000; width: 550px; cursor: pointer;">
		<tr style="background-color: black;">
		<td id="detailsOR" onclick="openORDetails('')">
		<font id="detailsORFont" color="white">Details</font>
		</td> 
		<td id="detailsLogs" onclick="showLogs()">
		<font id="detailsLogsFont" color="white">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
				<div id="paymentDetailsDashDiv" style="height:330px;width: 550px;margin-left:10%;">
	
	</div>
		</div>
		   <div id="jobDetailsLogsDiv"  style="background-color:#ceecf5;display:none;position: absolute; z-index: 3000;"><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeJobsDetailsLogsPopUp()">
   <table id="tabForDetails" style="z-index: 3000; width: 750px; cursor: pointer;">
		<tr style="background-color: black;">
		<td id="detailsOR" onclick="openORDetails('')">
		<font id="detailsORFont" color="white">Details</font>
		</td> 
		<td id="detailsLogs" style="background-color: white;" onclick="showLogs()">
		<font id="detailsLogsFont" color="black">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
		
  <div id="jobDetailsLogsPopUp"></div></div>
 <div class="jqmWindow" id="paymentDetails">
					<div id="paymentFromServer" style="font-weight: bolder;color:#7E2217;background-color:#BDEDFF;border-color:#FF3333;border-width: 3px;-moz-border-radius:12px 12px 12px 12px;"></div>
					<table bgcolor="#BDEDFF" style="width: 100%;">
						<tr><td><center><input type="button" name="reverse" id="reverse" value="Reverse" onclick="reverseTransaction()"></input></center></td></tr>
					</table>
					</div>
  <div id="popUpOR" style="z-index:3000">
 		 <div id="ORDash" class="jqmWindow" style="display: none;height:330px;width: 550px;margin-left:10%">
 		 <table id="tabForDetails" style="z-index: 3000; width: 550px; cursor: pointer;margin-top:-3%;">
		<tr style="background-color: black;">
		<td id="detailsOR" style="background-color: white;" onclick="openORDetails('')">
		<font id="detailsORFont" color="black">Details</font>
		</td> 
		<td id="detailsLogs" onclick="showLogs()">
		<font id="detailsLogsFont" color="white">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
			<jsp:include page="/jsp/OpenRequestForDetails.jsp" />
		</div>
</div>
	</form>
</body>
</html>