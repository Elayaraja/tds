<%@page import="com.charges.bean.ChargesBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="com.tds.tdsBO.VoucherBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.InvoiceBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.PaymentSettledDetail"%>
<%@page import="com.tds.tdsBO.InvoiceBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%String vNo=(String) request.getAttribute("voucherNo"); 
	PaymentSettledDetail voucher = (PaymentSettledDetail)request.getAttribute("voucher"); 
	ArrayList<ChargesBO> charges = new ArrayList<ChargesBO>();
	ArrayList<ChargesBO> chargeType =(ArrayList<ChargesBO>)request.getAttribute("chargeTypes");
   AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");

	%>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript">
function goTotal(){
	 var totalAmt = 0;
	for(var i =0;i<Number(document.getElementById("fieldSize").value);i++){
		totalAmt += parseFloat(document.getElementById("chAmt_"+i).value);
	}
	document.getElementById("totalValue").value = totalAmt;
}
function calCharges() {
	var i = Number(document.getElementById("fieldSize").value);
	document.getElementById('fieldSize').value = i+1;
	var table = document.getElementById("chargesTable");
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);

	var element1 = document.createElement("select");
	element1.name = "chKey_" + i;
	element1.id = "chKey_" + i;
	<%if(chargeType!=null && chargeType.size()>0){
		for(int i=0;i<chargeType.size();i++){%>
	var theOption = document.createElement("option");
	theOption.text = "<%=chargeType.get(i).getPayTypeDesc()%>";
	theOption.value = "<%=chargeType.get(i).getPayTypeKey()%>";
	element1.options.add(theOption);
	<%}}%>
	cell1.appendChild(element1);

	var element2 = document.createElement("input");
	element2.type = "text";
	element2.name = "chAmt_" + i;
	element2.id = "chAmt_" + i;
	element2.size = '7';
	element2.onchange=function(){goTotal();};
	cell2.appendChild(element2);
} 

	function addCharges() {
	 	clearChargesVoucher();
		var tripId="<%=voucher.getTripId()%>";
	 	if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'RegistrationAjax?event=getVoucherCharges&tripId='+tripId;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var jsonObj = "{\"address\":"+text+"}" ;
		var obj = JSON.parse(jsonObj.toString());
		for(var j=0;j<obj.address.length;j++){
			calCharges();
			var type=obj.address[j].T;
			var amount=obj.address[j].A;
			document.getElementById("chKey_"+j).value=type;
			document.getElementById("chAmt_"+j).value=amount;
			goTotal();
		}
	}

	$(document).ready(function() {

		//iterate through each textboxes and add keyup
		//handler to trigger sum event
		addCharges();
	});
	 function clearChargesVoucher(){
		 var table = document.getElementById("chargesTable");
		 var rowCount = table.rows.length;
		 var size=document.getElementById('fieldSize').value;
		 for(var i=0;i<Number(size);i++){
			 rowCount--;
			 table.deleteRow(rowCount);
		 }
		 document.getElementById("fieldSize").value="0";
		document.getElementById("totalValue").value = "";
	 }

	function checkSettle() {
		if (document.getElementById("paymentStatus").value != "2") {
			var check = confirm("Do you want to settle this transaction?");
			if (check == true) {
				document.getElementById('paymentStatus').value = 2;
			} else {
				document.getElementById('paymentStatus').value = 0;
			}
		}
		document.voucherProcess.submit();
	}
/* 	function validateForm(){
		if(document.getElementById("fieldSize").value==""){
			alert("Please provide atleast one charge");
			return false;
		}
	}
 */
 </script>
</head>

<body>
<form name="voucherProcess" method="post" action="control" onsubmit="return validateForm()">
		<input type="hidden" name="action" value="finance" /> <input
			type="hidden" name="event" value="generateInvoice" /> <input
			type="hidden" name="module" id="module"
			value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />
			<input type="hidden" name="paymentStatus" id="paymentStatus" value="<%=voucher.getPaymentReceivedStatus()%>"/>		
			<input type="hidden" name="update" id="update" value="update"/>		
			<input type="hidden" name="fieldSize" id="fieldSize" value="0"/>		
						<c class="nav-header"><center>Edit Voucher Details</center></c>
					
<div class="rightCol">
			<div class="rightColIn">
			
				<table width="100%" border="0" cellspacing="0" cellpadding="0"
					class="driverChargTab">
					<tr><td class="firstCol">RiderName</td><td><input type="text" name="riderName" id="riderName" value="<%=voucher.getRiderName()%>"/></td>
					<td class="firstCol">Cost Center</td><td><input type="text" name="costCenter" id="costCenter" value="<%=voucher.getCostCenter()%>"/></td></tr>
					
					
					<tr><td class="firstCol">Invoice No.</td><td><input type="text" readonly="readonly" name="invoiceNo" id="invoiceNo" value="<%=voucher.getInvoiceNumber()%>"/></td>
						<td class="firstCol">Creation Date</td><td><input type="text" name="creationDate" id="creationDate" value="<%=voucher.getCreationDate()%>" onfocus="showCalendarControl(this);" value="" readonly="readonly"/></td></tr>

					<tr><td class="firstCol">Company Code</td><td><input type="text" name="companyCode" id="companyCode" value="<%=voucher.getCompanyCode()%>"/></td>
					<td class="firstCol">Trip ID</td><td><input type="text" readonly="readonly" name="tripId" id="tripId" value="<%=voucher.getTripId()%>"/></td></tr>
					
					<tr><td class="firstCol">Contact</td><td><input type="text" name="contact" id="contact" value="<%=voucher.getContact()%>"/></td>
					<td class="firstCol">Delay Days</td><td><input type="text" name="delayDays" id="delayDays" value="<%=voucher.getDelayDays()%>"/></td></tr>
					
					<tr><td class="firstCol">Frequent</td><td><input type="text" name="frequent" id="frequent" value="<%=voucher.getFreq()%>"/></td>
					<td class="firstCol">Service Date</td><td><input type="text" name="serviceDate" id="serviceDate" value="<%=voucher.getServiceDate()%>" onfocus="showCalendarControl(this);" value="" readonly="readonly"/></td></tr>
					
					<tr><td class="firstCol">Trans ID</td><td><input type="text" readonly="readonly"  name="transId" id="transId" value="<%=voucher.getTransId()%>"/></td>
					<td class="firstCol">Driver ID</td><td><input type="text" name="driverId" id="driverId" value="<%=voucher.getDriverId()%>"/></td></tr>
					
					<%--<tr> <td class="firstCol">Tip</td><td><input type="text" name="tip" id="tip" value="<%=voucher.getTip()!=null?voucher.getTip():""%>"/></td> 
				 <td class="firstCol">Ret Amount</td><td><input type="text" name="retAmount" id="retAmount" value="<%=voucher.getRetAmount()%>"/></td></tr>
					 --%>
					
					<tr><td class="firstCol">Description</td><td><textarea name="desc" id="desc"><%=voucher.getDescription() %></textarea></td>
					<tr ><%-- <td class="firstCol">Amount</td><td><input type="text" name="amount" id="amount" value="<%=voucher.getAmount()%>"/></td> --%>
					<td class="firstCol">Expiry Date</td><td><input type="text" name="expiryDate" id="expiryDate"  value="<%=voucher.getExpDate()%>" onfocus="showCalendarControl(this);" value="" readonly="readonly"/></td>
					<td class="firstCol">Account Number</td><td><input type="text" name="acctNum" id="acctNum"  value="<%=voucher.getVoucherNo()%>"/>
					<ajax:autocomplete 
  					fieldId="acctNum"
  					popupId="model-popup2"
  					targetId="acctNum"
  					baseUrl="autocomplete.view"
  					paramName="getAccountList"
  					className="autocomplete"
  					progressStyle="throbbing"/>
  					</td>
					</tr>
					<tr><td colspan="2"><h1><center>CHARGES</center></h1></td></tr>
<%--					<% double total =0;
					for(int i = 0;i<charges.size();i++){
					total +=Double.parseDouble(charges.get(i).getAmount());%>
 										<tr><td class="firstCol"><%=charges.get(i).getPayTypeDesc() %></td>
												<td><%=adminBO.getCurrencyPrefix()%><input type="text" size="5" class="txt" name="chAmt_<%=i%>" id="chAmt_<%=i%>" value ="<%=charges.get(i).getAmount() %>"  >
												<input type="hidden" name="chKey_<%=i%>" id="chkey_<%=i%>" value ="<%=charges.get(i).getPayTypeKey() %>"></td>
										</tr>
					<%} %> --%>
					<table id="chargesTable" style="width: 30%;" border="1" bgcolor="#f8f8ff" class="driverChargTab">
					<tr>
						<th style="width: 20%;background-color: #d3d3d3; ">Charge Type</th>
						<th style="width: 10%;background-color: #d3d3d3;">Amount</th>
					</tr>
					</table>
					<table  style="width: 30%;" border="1" bgcolor="#f8f8ff" class="driverChargTab">
					<tr><td class="firstCol">Total Amount</td><td><%=adminBO.getCurrencyPrefix()==1?"$":adminBO.getCurrencyPrefix()==2?"&euro;":adminBO.getCurrencyPrefix()==3?"&pound;":"&#x20B9;"%><input type="text" size="7" name="totalValue" id ="totalValue" value=""></td></tr>
					<tr><td>
			        	<input type="button" name="add" value="Add Row" onclick="calCharges()" class="lft"/></td><td>
			        	<input type="button" name="clear" value="Clear All" onclick="clearChargesVoucher()" class="lft"/>
					</td></tr>
					</table>
					<tr><td><input type="hidden" name="driverId" id="driverId" value="<%=voucher.getDriverId()%>"></td></tr>
					
					<tr> <td colspan="4" align="center" >
						<div class="wid60 marAuto padT10">
								<div class="btnBlue">
									<div class="rht">
										<input type="button" name="update" value="update" onclick="return checkSettle()"/>
									</div>
									<div class="clrBth"></div>
								</div>
								<div class="clrBth"></div>
							</div>
					</td></tr>
					
</table></div></div>			
</form>
</body></html>
