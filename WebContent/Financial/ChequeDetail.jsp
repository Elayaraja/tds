<%@page import="java.io.FileNotFoundException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.CashSettlement" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>

<%
CashSettlement  csregBO = (CashSettlement)request.getAttribute("csregBO");   
ArrayList<DriverDisbursment> disbursement;
if(request.getAttribute("ChequeDetails")!=null){
	disbursement = (ArrayList<DriverDisbursment>)request.getAttribute("ChequeDetails");
}
else {
	disbursement = new ArrayList<DriverDisbursment>();
}

DriverDisbursment  DriverDate = (DriverDisbursment)request.getAttribute("DriverDate");  
%> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>

<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript">
function Checkchar()
{
	var t1= document.getElementById('DriverId');
	t1Value = t1.value;

	if(t1.value!="")
		{
		if(isNaN(t1Value))
		  {
			alert("Enter Only Numbers ");
		    t1.value="";
		    return false;
		   }
		}}

 function show_prompt()
{
document.getElementById('bounceCharge').value = "-25";	
}
</script>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cheque Register Details</title>
</head>
<body>
		<form name="masterForm" action="finance" method="post"  >
   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %>
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <input type="hidden" name="action" value="finance"/>
		<input type="hidden" name="event" value="chequeDetails"/>
		<input type="hidden" name="operation" value="2"/>
		<input type="hidden" name="bounceCharge" value="-25"/>
		<input type="hidden" name="numberOfRows" value="<%=disbursement.size()%>"/>
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
		<input type="hidden"  name="key"  value="  <%=request.getAttribute("key") %>" /> 
<div class="clrBth"></div>
                               </div>
                <div class="rightCol">
				<div class="rightColIn">
                	<h2><center>Get&nbsp;Cheque&nbsp;Details</center></h2>
                 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<tr>
                    <td>
                    <table id="bodypage" width="750" >
                    <tr>
                    	<td  class="firstCol">Date</td>
                    	<td> <input type="text"  name="Date"  id="Date" onfocus="showCalendarControl(this);" value="<%=request.getParameter("Date")==null?"":request.getParameter("Date") %>"/> </td>
                    	<td  class="firstCol">Driver&nbsp;Id</td>
                    	<td> <input type="text"  name="DriverId"  id="DriverId" value="<%=request.getParameter("DriverId")==null?"":request.getParameter("DriverId") %>"  onkeypress= " return Checkchar()"/> 
                    	      <ajax:autocomplete
				  					fieldId="DriverId"
				  					popupId="model-popup1"
				  					targetId="DriverId"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 										
                    	</td>                    	
                    </tr>   	
                    <tr>
		                <td  class="firstCol">Received&nbsp;Cheque</td>
		                <td> <input type="checkbox" name="received" <%=request.getParameter("received")==null?"":"checked"%> id="received" value="1"></input></td>
		                <td  class="firstCol">Bounced&nbsp;Cheque</td>
		                <td> <input type="checkbox" name="bounced" id="bounced" <%=request.getParameter("bounced")==null?"":"checked"%> value="2"></input></td>
		            </tr>
                <tr>
				<td colspan="7" align="left">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="Submit" value="Get Details"  class="lft">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
				</td>
               
               
               
                    <% if(disbursement != null && disbursement.size() >0  ) {  %> 
                 <tr>  <table width="100%" border="3" cellspacing="3" cellpadding="3" class="driverChargTab"></tr>                    
            		
            <tr>
				<th width="15%">Driver Id</th>
				<th width="15%">Cheque&nbsp;Number</th>
				<th width="15%">Amount</th>
				<th width="15%">Cheque Detail</th>
				<th width="10%">Include</th>
			</tr>  
			<%for(int i=0;i<disbursement.size();i++){ %>	
			<tr>
				<td><%= disbursement.get(i).getDriverid() %></td>
				<td>
				<a href="control?action=registration&event=driverDisbursementSummary&subevent=showDisDetailSummary&module=financeView&pss_key=<%=disbursement.get(i).getPayment_id()%>">
				<%= disbursement.get(i).getCheckno() %>
				</a></td>
				<td><%= disbursement.get(i).getAmount() %></td>
      			<%if(disbursement.get(i).getChequeStatus().equals("0")){ %>
      			<td>Pending</td>
      			<%} else if(disbursement.get(i).getChequeStatus().equals("1")){ %>
      			<td>Received</td>
      			<%} else if(disbursement.get(i).getChequeStatus().equals("2")){ %>
      			<td>Bounced</td>
      			<%}%>
      			<td> <input type="checkbox"  name="Row_<%=i%>" id="Row_<%=i%>" <%=request.getParameter("received")==null?"":"disabled"%> <%=request.getParameter("bounced")==null?"":"disabled"%> value="<%=disbursement.get(i).getPayment_id()%>"/></td>
      			<%} %>
				</tr>  
        </table>
        
                <tr>
				<td colspan="7" align="left">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                            <div class="rht">
                        	 <input type="submit" name="Received" value="Received" class="lft">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="Bounced" value="Bounced" class="lft" onclick="show_prompt()">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </td>			
			  	</div>
				</tr>         
          <%}%> 
     </tr></tr></table></td></tr></tr></table></div></div></form></body></html>    	           	
                    	
                    	
                    	
                    	
                    