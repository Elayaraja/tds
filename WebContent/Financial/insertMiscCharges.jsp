<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
    <%@page import="java.util.ArrayList"%>
    <%@page import="com.charges.bean.ChargesBO"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Misc Setup</title>

</head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />

<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">

	function delrow() {
		try {
			var table = document.getElementById("miscSetup");
			var rowCount = table.rows.length;
			var temp = (Number(document.getElementById("size").value) - 1);
			document.getElementById('size').value = temp;
			rowCount--;
			table.deleteRow(rowCount);
		} catch (e) {
			alert(e.message);
		}

	}

	function cal() {
		var i = Number(document.getElementById("size").value) + 1;
		document.getElementById('size').value = i;
		var table = document.getElementById("miscSetup");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);
		var cell3 = row.insertCell(3);
		var cell4 = row.insertCell(4);
		var element = document.createElement("input");
		

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "miscDesc" + i;
		element2.id = "miscDesc" + i;
		element2.size = '10';
		cell.appendChild(element2);

		var element3 = document.createElement("input");
		element3.type = "text";
		element3.name = "amount" + i;
		element3.id = "amount" + i;
		element3.size = '10';
		cell1.appendChild(element3);
/* 
		var element4 = document.createElement("input");
		element4.type = "text";
		element4.name = "startRate" + i;
		element4.id = "startRate" + i;
		element4.size = '10';
		cell3.appendChild(element4);

		var element5 = document.createElement("input");
		element5.type = "text";
		element5.name = "ratePerMile" + i;
		element5.id = "ratePerMile" + i;
		element5.size = '10';
		cell4.appendChild(element5); */

	}
	
	function deleteMisc(serialNumber, x) {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'control?action=chargesAction&event=deleteMiscCharges&module=financeView&delete=YES&serialNumber='+ serialNumber;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == 1) {
			var table = document.getElementById("miscSetup");
			table.deleteRow(x.parentNode.parentNode.rowIndex);
		}
	}
</script>
<body>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<form method="post" action="control" name="queue">
 <input type="hidden" name="action" value="chargesAction">
<input type="hidden" name="event" value="addMiscCharges">
<input type="hidden" name="module" value="financeView">
<input type="hidden" id="size" name="size" value="">
<%ArrayList<ChargesBO> getMiscCharges= (ArrayList<ChargesBO>)(request.getAttribute("getMiscCharges")==null?new ArrayList<ChargesBO>():request.getAttribute("getMiscCharges"));%>

<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div class="rightCol">
<div class="rightColIn">
<h2><center>Misc Setup</center></h2>
                		 
 <table width=" 80%" border="0" cellspacing="5" cellpadding="4" class="driverChargTab" id="miscSetup">
 <tr>
						<%
							String error ="";
							if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
							} 
					%>
						<td colspan="7">
							<div id="errorpage">
							<%= error.length()>0?""+error :"" %>
					   		</div>
				   		</td>
				 </tr>
                      <tr>
                       <!--  <th width="15%" class="firstCol">Driver Id</th> -->
                        <th width="15%">Misc Desc</th>
                        <th width="15%">Amount</th>
                      </tr>
                    	<% 
	boolean colorLightGreen = true;
	String colorPattern;%>
								<%
								for(int i=0;i<getMiscCharges.size();i++){
								colorLightGreen = !colorLightGreen;
								if(colorLightGreen){
									colorPattern="style=\"background-color:lightgreen\""; 
									}
								else{
									colorPattern="";
								}
							%>
							<tr>

									<input type="hidden" size="10" readonly="readonly" id="doc_<%=i%>" value="<%=getMiscCharges.get(i).getDriverId()==null?"": getMiscCharges.get(i).getDriverId() %>"/>
									<td ><input type="text" size="10" readonly="readonly" id="doc_<%=i%>" value="<%=getMiscCharges.get(i).getMiscDesc()==null?"":getMiscCharges.get(i).getMiscDesc()  %>"/></td>
									<td ><input type="text" size="10" readonly="readonly" id="doc_<%=i%>" value="<%=getMiscCharges.get(i).getAmount()==null?"":getMiscCharges.get(i).getAmount()  %>"/></td>
									 <td>
									<input type="button"
										name="imgDelete" id="imgDelete" value="Delete"
										onclick="deleteMisc('<%=getMiscCharges.get(i).getKey() %>',this)"></input>
									</td> 
								</tr>
								
								<%} %>
                    </table>
                    <div align="center"><tr>
                                    		  

                     <td><input type="button" name="Button" value="ADD" onclick="cal()" class="lft"></td>
                                                

                    <td><input type="button" name="Button" value="Remove"  onclick="delrow()" class="lft" ></td>
                                                  
                    <td><input type="submit" name="button" class="lft" value="Submit"/></td>
                                                  
                                              </div></tr>
      																
                                                     
                
            	
                <div class="clrBth"></div>
           
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</body>
</html>