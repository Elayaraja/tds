<%@page import="java.io.FileNotFoundException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.CashSettlement" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>

<%
CashSettlement  csregBO = (CashSettlement)request.getAttribute("csregBO");   
ArrayList<DriverDisbursment> disbursement = (ArrayList<DriverDisbursment>)request.getAttribute("DisbursementDetails"); 
CashSettlement  cashSettlementBean = (CashSettlement)request.getAttribute("CashSettlement");   
%> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cash Register Master</title>
</head>
<body>
		<form name="masterForm" action="finance" method="post"  />
   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %>
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <input type="hidden" name="action" value="finance"/>
		<input type="hidden" name="event" value="cashSettlement"/>
		<input type="hidden" name="operation" value="2"/>
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
		<input type="hidden"  name="key"  value="  <%=request.getAttribute("key") %>" /> 

                	<h2><center>Cash&nbsp;Register&nbsp;Master</center></h2>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<tr>
                    <td>
                    <table id="bodypage" width="100%" >
                    <tr>
                    	<td  class="firstCol">Opetator&nbsp;Id</td>
                    	<td> <input type="text"  name="OperatorId" readonly  id="" value="<%=csregBO.getOperatorId() %>"/> </td>
                    	<td  class="firstCol">Open&nbsp;Cash</td>
                    	<td> <input type="text"  name="Opencash" readonly id="" value="<%=cashSettlementBean.getOpenCash() %>" /> </td>                    	
                    	</tr>
                    	<tr>
                    	<td  class="firstCol">Open&nbsp;Cheque&nbsp;Number</td>
                    	<td> <input type="text"  name="OpenChequeno"  readonly id="" value="<%=cashSettlementBean.getIntialChequeNo() %>"/> </td>
                    	<td  class="firstCol">Closing&nbsp;Cash</td>
                       	<td> <input type="text"  name="Closingcash" readonly id="" value="<%=cashSettlementBean.getClosingCash() %>"/> </td> 
                     	</tr>
                     	<tr>
                     	<td  class="firstCol">Closing&nbsp;Check&nbsp;No</td>
                    	<td> <input type="text"  name="Closingcheckno" readonly id="" value="<%=cashSettlementBean.getClosingCheckNo() %>"/> </td>
                    	<td  class="firstCol">Received&nbsp;Checques</td>
                    	<td> <input type="text"  name="receivedCheques" readonly id="" value="<%=cashSettlementBean.getReceivedCheques() %>"/> </td>
                    	</tr>
                  
                   <tr>
				<td colspan="7" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="CloseRegister" value="Close Register" class="lft">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
			  	</td>
				</tr>         
                    	
                  
                    <% if(disbursement != null && disbursement.size() >0  ) {  %> 
                   <table width="100%" border="1" cellspacing="0" cellpadding="0">                    
            
            <tr>
				<th align="center" width="20%">Driver Id</th>
				<th align="center" width="20%">Cheque&nbsp;Number</th>
				<th align="center" width="20%">Amount</th>
				<th align="center" width="20%">Pay ID</th>
				<th align="center" width="20%">Description</th>
			</tr>  
			<%for(int i=0;i<disbursement.size();i++){ %>	
			<tr align="center">
				
				<td><%= disbursement.get(i).getDriverid() %></td>
				<% if (!disbursement.get(i).getCheckno().equals("0")){ %>
				<td><%=disbursement.get(i).getCheckno() %></td>
				<% } else {%>
				<td>Cash</td>
				<%}%>
				<td> <%=disbursement.get(i).getAmount() %></td>
      			<td>
					<a href="control?action=registration&event=driverDisbursementSummary&subevent=showDisDetailSummary&module=financeView&pss_key=<%=disbursement.get(i).getPayment_id()%>">
					<%= disbursement.get(i).getPayment_id() %>
					</a>
				</td>
      			<td><%= disbursement.get(i).getDescr() %></td>
      		</tr>   
		<%}%>
        <%}%> 
        
                    	           	
                    	
                    	
                    	
                    	
                    