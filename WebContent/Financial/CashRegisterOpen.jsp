<%@page import="java.io.FileNotFoundException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.CashSettlement" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%
CashSettlement  csregBO = (CashSettlement)request.getAttribute("csregBO");   
%> 


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">


function Checkchar()
{
	var t1= document.getElementById('Opencash');
	var t2= document.getElementById('OpenChequeno');
	t1Value = t1.value;
	t2Value = t2.value;
	
	 if(t1.value!="")
		{
		if(isNaN(t1Value))
		  {
			alert("Enter Only Numbers ");
		    t1.value="";
		    return false;
		   }
		}
	 if(t2.value!="")
		 {
		 if(isNaN(t2Value))
			 {
			    alert("Enter Only Numbers ");
			    t2.value="";
			    return false;
			 }
		 }
}

function validation()

{
	var t1 =document.getElementById('Opencash');
	var t2 =document.getElementById('OpenChequeno');
	if(t1.value==""||t2.value=="") 
    {
    alert("Please enter some value in OpenCash/OpenChechNo ");
	  return false;
    }
	
}
</script>


<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cash Register Master</title>
</head>
<body>
		<form name="masterForm" action="finance" method="post"  />
		
   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %>
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <input type="hidden" name="action" value="finance"/>
		<input type="hidden" name="event" value="cashSettlement"/>
		<input type="hidden" name="operation" value="2"/>
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
		<input type="hidden"  name="key"  value="  <%=request.getAttribute("key") %>" /> 
			
			 <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
				<div class="rightColIn">
                	<h1 style="width: 100%; color: white; height: 40%; background-color: rgb(15, 51, 75); font-size: 130%; text-align: center;">Cash&nbsp;Register&nbsp;Master</h1>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<tr>
                    <td>
                    <table id="bodypage" width="750" >
                    <tr>
                    	<td  class="firstCol">Opetator&nbsp;Id</td>
                    	<td> <input type="text"  name="OperatorId" readonly id="OperatorId" value="<%=csregBO.getOperatorId() %>"/> </td>
                    	<td  class="firstCol">Open&nbsp;Cash</td>
                    	<td> <input type="text"  name="Opencash"  id="Opencash" value="<%=csregBO.getOpenCash() %>" onkeypress= " return Checkchar()"/> </td>                    	
                    </tr>
                    <tr>
                    	<td  class="firstCol">Open&nbsp;Cheque&nbsp;Number</td>
                    	<td> <input type="text"  name="OpenChequeno"  id="OpenChequeno" value="<%=csregBO.getIntialChequeNo() %>" onkeypress= " return Checkchar()"/> </td>
                     </tr>
                    
                    	
         <tr>
				<td colspan="7" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="OpenRegister" value="Open Register" class="lft" onclick="return validation()">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
			  	</td>
				</tr>         
                    	
                    	           	
                    	
                    	
                    	
                    	
                    