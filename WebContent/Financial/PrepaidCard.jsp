<%@page import="com.tds.tdsBO.DriverLocationHistoryBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment" %>
<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment" %>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.cmp.bean.CallerIDBean"%>
<%@page import="com.tds.tdsBO.PrepaidCardBO"%>

<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Driver Card Details</title>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/label.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
 
<script type="text/javascript">

function myCaptcha(value) {
	var a = Math.ceil(Math.random() * 10)+ '';
    var b = Math.ceil(Math.random() * 10)+ '';       
    var c = Math.ceil(Math.random() * 10)+ '';  
    var d = Math.ceil(Math.random() * 10)+ '';  
    var code = a + '' + b + '' + '' + c + '' + d ;
	document.getElementById("pinno"+value).value = code;
	document.getElementById("pinno"+value).setAttribute('readonly', 'readonly');
}
	
	function addCard() {
	
		var i= Number(document.getElementById('fieldSize').value)+1 ;
		document.getElementById('fieldSize').value = i;
		var table = document.getElementById("prepaiddetails");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);
        var cell3 =row.insertCell(3);
        
		var element = document.createElement("input");
		element.type = "text";
		//element.className = "form-autocomplete";
		element.name = "cardno" +i;
		element.id = "cardno" +i;
		element.align = 'right';
		element.size = '10';
		cell.appendChild(element);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "driverId"+i ;
		element2.id = "driverId"+i ;
		element2.size = '10';
		cell1.appendChild(element2);
		
		var element4 = document.createElement("input");
		element4.type = "text";
		element4.name = "amount" +i;
		element4.id = "amount" +i;
		element4.size = '10';
		cell2.appendChild(element4);
		
		var element6 = document.createElement("input");
		element6.type = "text";
		element6.name = "pinno" +i;
		element6.id = "pinno" +i;
		element6.size = '10';
		cell3.appendChild(element6);
		myCaptcha(i);
		
		document.getElementById("buttonSubmit").disabled=false;
}
</script>
<script type="text/javascript">
var nocount;
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
function carddetails(){
	$("#carddetails1").jqm();
	$("#carddetails1").jqmShow();
}


 function carddetails(){
	var cardNumber=document.getElementById("cardno").value;
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='finance?event=prepaidcard&cardNo='+cardNumber;
	//alert(url);
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text=xmlhttp.responseText;
	//alert(text);
	var jsonObj = "{\"details\":"+text+"}";
	//alert(jsonObj);'
	var obj = JSON.parse(jsonObj.toString());
	//document.getElementById("carddetails").style.display=block;
	document.getElementById("cardno2").value=obj.details[0].cno;
	//alert(obj.details[0].cno);
	document.getElementById("driverid").value=obj.details[0].allto;
	//alert(obj.details[0].allto);
	document.getElementById("amount").value=obj.details[0].amt;
	document.getElementById("pinno").value=obj.details[0].pin;
    if ($("#carddetails1").css('display') == 'none'){
        $("#carddetails1").show();
        $("#btnClick").val("Hide");
    }
    else{
        $("#carddetails1").hide();
        $("#button").val("Search");
    }
    $(this).oneTime(1000, function() {
        $("#carddetails1").hide();
      });

	}
 
 function checkValues(){
	 alert("values");
 }
</script>
</head>
<body>

<form  name="prepaidcard"  action="control" method="post" onsubmit="return showProcess()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="action" value="finance"/>
<input type="hidden" name="event" value="prepaidcard"/>
<input type="hidden" name="operation" value="2"/>

<input type="hidden" name="module" id="module" value="" />
<input type="hidden" name="fieldSize" id="fieldSize" value=""/> 
	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div class="rightCol">
<div class="rightColIn">	
<h1 style="width: 100%;color:white;height:40%;background-color:rgb(41, 53, 65);font-size:150%;text-align:center;">Driver Card Details</h1>
    
<div align="center"><table><form><tr>
    <td>Card no</td>
        <td><input type="text" name="cardno"  id="cardno" value=""/></td>
        <td><input type="button" name="cardno"  value="Search" onclick="carddetails()"/></td>
	    
	    </tr>
	    </form>
	    </table>	</div>
	    <div class="jqmWindow" id="carddetails1"  style="display:none;width:520px" >
		 <table id="carddetails">
  <tr><td>CardNo</td><td><input type="text" name="cardno2" id="cardno2" value="" readonly></td></tr>
  <tr><td>Allocatedto </td><td><input type="text" name="driverid" id="driverid" value="" ></td></tr>
  <tr><td>Amount</td><td><input type="text" name="amount" id="amount" value="" ></td></tr>
  <tr><td>PinNo</td><td><input type="text" name="pinno" id="pinno" value="" readonly></td></tr>
  
  </table><div align="center">          
            <input type="submit" name="buttonSubmit" class="lft" value="Update" />
         </div>
  
  </div>
 	    	</div>
	     	
 <table  id="prepaiddetails" align="center"  style="width:60%;background-color:rgb(209, 214, 214)" id="bodypage" >
<tr>
 </tr>
<tr style="background-color:rgb(186, 228, 226);" >          
	<th style="width: 15%;">Card no</th>
	<th style="width: 15%;">Allocate to</th>
	<th style="width: 15%;">Amount</th>
	<th style="width: 15%;">Pinno</th>
</tr>
<%-- <tr>
	<td><input type="text" name="asscode"  value=""/></td>
		<td>
							 <input type="text" name="driverId" id="driverId" value="" autocomplete="off"/>
							 <ajax:autocomplete
				  					fieldId="driverId"
				  					popupId="model-popup1"
				  					targetId="driverId"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 	
				  					</td>
				  	<td><input type="text" name="amount"  value=""/></td>
				  	 	<td><input type="text" name="pinno"  value="" readonly/></td>
 </td>
</tr>
 
 --%> 
			  
 </table>         <div align="center">          
            <input type="button" name="add" value="ADD" onclick="addCard()" class="lft">
            <input type="submit" disabled="disabled" id="buttonSubmit" name="buttonSubmit" class="lft" value="Submit" onclick="checkValues()"/>
                             </div>
        </div>
             
            </div>
    
</form>
           	 
       

</body>
</html>