<%@page import="com.tds.tdsBO.InvoiceBO"%>
<%@page import="com.tds.tdsBO.VoucherBO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Voucher Process</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>

<%
ArrayList<InvoiceBO> voucher = new ArrayList<InvoiceBO>();
if(request.getAttribute("voucher")!=null){
	 voucher = (ArrayList<InvoiceBO>)request.getAttribute("voucher"); 
} 
InvoiceBO invoiceBO = new InvoiceBO();
%>
<style type="text/css">
      div#pop-up {
        display: none;
        position: absolute;
        width: 280px;
        padding: 10px;
        background: #eeeeee;
        color: #000000;
        border: 1px solid #1a1a1a;
        font-size: 90%;
      }
      #color{
      font-size: 100%;
      color:white;
      text-align:center;
      background-color:rgb(46, 100, 121);
      height:40%;
      width:100%;
      }
      #tab_color{
      background-color:rgb(46, 100, 121);
      color:white;
      
      }
</style>

<script>
function showChequeNum(i){
	 if (document.getElementById('mark'+i).checked==true){
	$("#chequeNumber").show("slow");
	 }else{
		 $("#chequeNumber").hide("slow");
	 }
}

function deleteInvoice(invoiceNum){
	var deleteVoucher=confirm("Do you really want to delete the Invoice?");
	if(deleteVoucher==true){
		document.getElementById("delete").value="YES";
		document.getElementById("invoiceNo").value=invoiceNum;
		return true;
	} else {
		return false;
	}
}
function closePopup(){
	$("#chequeNumber").hide('slow');
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var invoiceprocess = document.getElementById("bodypage");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>Invoice process Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(invoiceprocess.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
    
</head>
<body>
	<form name="voucherProcess" method="post" action="control" onsubmit="return showProcess()">
		<input type="hidden" name="action" value="finance" /> <input
			type="hidden" name="event" value="invoicePayment" /> <input
			type="hidden" name="module" id="module"
			value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
		<input type="hidden" name="delete" id="delete" value=""/>
		<input type="hidden" name="invoiceNo" id="invoiceNo" value=""/>
		 <h2><center>Invoice Review</center></h2>
		 <table>
		 <form>
						
							<table  border="0" align="center" id="bgcolor" cellspacing="0" cellpadding="0">
                                  
                                   	<tr>
									<td>Account No</td><td>
							<input type="text" name="voucherNo" size="20"
										id="voucherNo" />&nbsp;&nbsp;
							
									Based On
									<select name="basedOn" size="1">
											<option value="0">UnPaid</option>
											<option value="1">Partially paid</option>
											<option value="2">Paid</option>
											<option value="3">cancelled</option></select>&nbsp;&nbsp;
								
								<input type="submit" name="search" value="search" /></td></tr>
					
		 
		<table width="80%" border="0" align="center" cellspacing="0" cellpadding="0">
			<tr>
				<td>

							<table width="100%" border="1" align="center" cellspacing="0" cellpadding="0">
								<%
									if(!voucher.equals(null) && !voucher.isEmpty()){
								%>


								<tr>
									<td>
										<table id="bodypage" border="1"  align="center" width="100%">
											<tr id="tab_color">
												<td width="5%">Invoice NO</td>
												<td width="15%">Account Number</td>
												<td width="15%"">Account Name</td>
												<td width="15%">Process Date</td>
												<td width="10%">Cheque No</td>
												<td width="10%">Total No.of Vouchers</td>
												<td width="5%">Total Amount</td>
												<td width="5%">Mark As Paid</td>
												<td width="10%">Invoice Details</td>
												<td width="15%">Delete Invoice</td>
											</tr>
											<%
												boolean colorlightGreen=true;
												String color;
											%>
											<%
												for(int i=0;i<voucher.size();i++) { 
													colorlightGreen=!colorlightGreen;
													if(colorlightGreen){
												color="style=\"background-color:rgb(242, 250, 250);text-align:center;\"";
													}
													else
												color="style=\"background-color:white;text-align:center;\"";
											%>
											<tr>
												<td <%=color%>><%=voucher.get(i).getInvoiceNumber()%></td>
												<td <%=color%>><%=voucher.get(i).getVoucherNo()==null?"":voucher.get(i).getVoucherNo()%></td>
												<td <%=color%>><%=voucher.get(i).getAccountName()==null?"":voucher.get(i).getAccountName()%></td>
												<td <%=color%>><%=voucher.get(i).getProcessDate()%></td>
												<td <%=color%>><%=voucher.get(i).getChequeNumber()==null?"":voucher.get(i).getChequeNumber()%></td>
												<td <%=color%>><%=voucher.get(i).getTotalNoOfVoucher()%>
												</td>
												<td <%=color%>><%=voucher.get(i).getTotalAmount()%></td>
												<%if(voucher.get(i).getPaymentStatus()!=9){ %>
												<td <%=color%>>
												<input type="checkbox"
													name="mark<%=i%>" id="mark<%=i%>"
													value="<%=voucher.get(i).getInvoiceNumber()%>"
													onclick="showChequeNum(<%=i%>)" />
												</td>
												<td <%=color%>><a
													href="control?action=finance&event=invoicePayment&Detail=Yes&invoiceNo=<%=voucher.get(i).getInvoiceNumber()%>">Details
												</a></td>
												<td <%=color%>><input type="submit" value="Delete Invoice" name="delete" id="delete" onclick="return deleteInvoice(<%=voucher.get(i).getInvoiceNumber()%>)"/>
												<%-- <a href="control?action=finance&event=invoicePayment&module=financeView&delete=Yes&invoiceNo=<%=voucher.get(i).getInvoiceNumber()%> " style="text-decoration: none">Delete Invoice</a> --%></td>
												<%}else{ %>
													<td <%=color%>>
												<input type="checkbox"
													name="mark<%=i%>" id="mark<%=i%>"
													value="<%=voucher.get(i).getInvoiceNumber()%>"
													disabled="disabled" />
												</td>
												<td <%=color%>>
												Details
												</td>
												<td <%=color%>><input type="submit" value="Delete Invoice" name="delete" id="delete" disabled="disabled"/>
												<%} %>
											</tr>
											<input type="hidden" name="totalInvoice" id="totalInvoice"
												value="<%=voucher.size()%>">
											<%
												}}
											%>

										</table>
								</tr>
							</table>
				</td>
				<td>
					<div id="chequeNumber" 
						style=" -moz-border-radius:8px,8px,8px,8px; display: none;  background-color: lightgrey; border: thick; border-color: gray;">
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							
				 			<%if(voucher.size()!=0){ %>
							 <%if(voucher.get(0).getPaymentStatus()!=2){ %> 
							<tr>
								<td colspan="3">Cheque No:<input type="text" name="chequeNum"
									size="10" id="chequeNum" /></td>
							</tr>
							<tr>
								<td colspan="1"><input type="Submit"
									name="pay" id="pay" value="Pay">
								</td>
								<td align="center" colspan="1"><input type="Submit"
									name="partlyPay" id="partlyPay" value="Partly Pay">
								</td>
								<td align="right" colspan="1"><input type="button"
									name="cancel" id="cancel" value="Close" onclick="closePopup()">
								</td>
							</tr>
							 <%}else{ %>
							
						<td ><input type="button"
									name="cancel" id="cancel" value="Close" onclick="closePopup()">
								</td>
							
							<%}} %>
						</table>	  
						
					</div>
				</td>
			</tr>

			
		
		</table>
		
		
	</form>
	 <input type="button" value="Save/Print" id="btnPrint" />
             
</body>
</html>