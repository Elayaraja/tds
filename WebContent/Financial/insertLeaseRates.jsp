<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
    <%@page import="java.util.ArrayList"%>
    <%@page import="com.charges.bean.ChargesBO"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lease Setup</title>

</head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />

<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">

	function delrow() {
		if(document.getElementById("size").value!="" && document.getElementById("size").value!="0"){
			try {
				var table = document.getElementById("leaseSetup");
				var rowCount = table.rows.length;
				var temp = (Number(document.getElementById("size").value) - 1);
				document.getElementById('size').value = temp;
				rowCount--;
				table.deleteRow(rowCount);
			} catch (e) {
				alert(e.message);
			}
		} else {
			alert("No new leases added");
		}
	}

	function cal() {
		var i = Number(document.getElementById("size").value) + 1;
		document.getElementById('size').value = i;
		var table = document.getElementById("leaseSetup");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);
		var cell3 = row.insertCell(3);
		var cell4 = row.insertCell(4);
		var element = document.createElement("input");
		element.type = "text";
		element.className = "form-autocomplete";
		element.name = "vehicleDescription" + i;
		element.id = "vehicleDescription" + i;
		element.align = 'right';
		element.size = '10';
		cell.appendChild(element);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "leaseAmount" + i;
		element2.id = "leaseAmount" + i;
		element2.size = '10';
		cell1.appendChild(element2);

		var element3 = document.createElement("input");
		element3.type = "text";
		element3.name = "numberOfPassengers" + i;
		element3.id = "numberOfPassengers" + i;
		element3.size = '10';
		cell2.appendChild(element3);

		var element4 = document.createElement("input");
		element4.type = "text"; 
		element4.name = "startRate" + i;
		element4.id = "startRate" + i;
		element4.size = '10';
		cell3.appendChild(element4);

		var element5 = document.createElement("input");
		element5.type = "text";
		element5.name = "ratePerMile" + i;
		element5.id = "ratePerMile" + i;
		element5.size = '10';
		cell4.appendChild(element5);

	}
	
	function deleteLease(serialNumber, x) {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'control?action=chargesAction&event=deleteLeaseRate&module=financeView&delete=YES&serialNumber='+ serialNumber;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == 1) {
			var table = document.getElementById("leaseSetup");
			table.deleteRow(x.parentNode.parentNode.rowIndex);
		}
	}
	function validationSize(){
		if(document.getElementById("size").value!="" && document.getElementById("size").value!="0"){
			return true;
		} else {
			alert("No new leases added");
			return false;
		}
	}
</script>
<body>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<form method="post" action="control" name="queue" onsubmit="return validationSize()">
 <input type="hidden" name="action" value="chargesAction">
<input type="hidden" name="event" value="addLeaseRates">
<input type="hidden" name="module" value="financeView">
<input type="hidden" id="size" name="size" value="">
<%ArrayList<ChargesBO> getLeaseRates= (ArrayList<ChargesBO>)(request.getAttribute("getLeaseRate")==null?new ArrayList<ChargesBO>():request.getAttribute("getLeaseRate"));%>

<div class="leftCol"> 
                    <div class="clrBth"></div> 
                </div> 
                <div class="rightCol">
<div class="rightColIn">
<h2><center>Lease Setup</center></h2>
                		 
 <table width="50%" border="0" cellspacing="2" cellpadding="2" class="driverChargTab" id="leaseSetup">
 <tr>
						<%
							String error ="";
							if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
							} 
					%>
						<td colspan="7">
							<div id="errorpage">
							<%= error.length()>0?""+error :"" %>
					   		</div> 
				   		</td>
				 </tr>
                      <tr>
                        <th width="15%">Vehicle Description</th>
                        <th width="15%">Lease Amount</th>
                        <th width="15%">No.Of.Passengers</th>
                        <th width="15%">Start Rate</th>
                        <th width="15%">Rate Per Mile</th>
                      </tr>
                    	<% 
	boolean colorLightGreen = true;
	String colorPattern;%>
								<%
								for(int i=0;i<getLeaseRates.size();i++){
								colorLightGreen = !colorLightGreen;
								if(colorLightGreen){
									colorPattern="style=\"background-color:lightgreen\""; 
									}
								else{
									colorPattern="";
								}
							%>
							<tr>

									<td ><input type="text" size="10" readonly="readonly" id="doc_<%=i%>" value="<%=getLeaseRates.get(i).getVehicleDesc()==null?"": getLeaseRates.get(i).getVehicleDesc() %>"/></td>
									<td ><input type="text" size="10" readonly="readonly" id="doc_<%=i%>" value="<%=getLeaseRates.get(i).getPayTypeAmount()==null?"":getLeaseRates.get(i).getPayTypeAmount()  %>"/></td>
									<td ><input type="text" size="10" readonly="readonly" id="doc_<%=i%>" value="<%=getLeaseRates.get(i).getNumberOfPassengers()==null?"":getLeaseRates.get(i).getNumberOfPassengers()  %>"/></td>
									<td ><input type="text" size="10" readonly="readonly" id="doc_<%=i%>" value="<%=getLeaseRates.get(i).getStartRate()==null?"": getLeaseRates.get(i).getStartRate()%>"/></td>
									<td ><input type="text" size="10" readonly="readonly" id="doc_<%=i%>" value="<%=getLeaseRates.get(i).getRatePerMile()==null?"": getLeaseRates.get(i).getRatePerMile()%>"/></td>
									 <td>
									<input type="button"
										name="imgDelete" id="imgDelete" value="Delete"
										onclick="deleteLease('<%=getLeaseRates.get(i).getKey() %>',this)"></input>
									</td> 
								</tr>
								
								<%} %>
                    </table>
                    <div align="center">
                                    		
                                                 <input type="button" name="Button" value="ADD" onclick="cal()" class="lft">
                                                  
                                                   <input type="button" name="Button" value="Remove"  onclick="delrow()" class="lft" >
                                                   
                                                     <input type="submit" name="button" class="lft" value="Submit"/>
                                                  
                                              </div>
                	 
																		
                                                      
                
   
           
            
            <footer>Copyright &copy; 2010 Get A Cab</footer>
 </div></div></form>
</body>
</html>