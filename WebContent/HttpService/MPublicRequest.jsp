<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page
	import="java.util.List,java.util.ArrayList,com.tds.tdsBO.OpenRequestBO"%>
<%! List m_publiRequestList = null; OpenRequestBO m_publicRequest = null; 
	int m_tripCounter;
%>
<%
	if(request.getAttribute("publicTrip") != null ) {
		m_publiRequestList = (ArrayList) request.getAttribute("publicTrip");
	}
%>
<%= "<xml version=\"1.0\">" %>
<%= "<PublicRequestTripDetails>" %>
<% if( m_publiRequestList != null && m_publiRequestList.size() > 0 ) { 
		for(m_tripCounter = 0; m_tripCounter < m_publiRequestList.size(); m_tripCounter++ ) {
			m_publicRequest = (OpenRequestBO)m_publiRequestList.get(m_tripCounter);
	%>
<%= "<tripID>" %>
<%=  m_publicRequest.getTripid()+"^"+m_publicRequest.getShrs()+":"+m_publicRequest.getSmin()+"^"+m_publicRequest.getSdate()+"^"+m_publicRequest.getSadd1()+"^"+m_publicRequest.getScity()+"^"+m_publicRequest.getSstate() %>
<%= "</tripID>" %>
<%
		}
	%>
<% } else { %>
<%= "<error>No Public Trip</error>" %>
<% } %>
<%= "</PublicRequestTripDetails>" %>