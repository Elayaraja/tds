
<%@page import="com.tds.tdsBO.OpenRequestBO"%><%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
 
<%!OpenRequestBO openRequestBO = null; %>
<%
	if(request.getAttribute("startTrip") != null) {
		openRequestBO = (OpenRequestBO)request.getAttribute("startTrip");
	}
%>
<%= "<?xml version=\"1.0\"?>" %>
<%= "<StartTrip>" %>
<%
	if(openRequestBO != null) { 
%>
<%= "<Status>"+ openRequestBO.getStatus_msg() +"</Status>" %>
<%		
	} else {
%>
<%= "<Status>Failed in Start Trip</Status>" %>
<% 
	}
%>
<%= "</StartTrip>" %>