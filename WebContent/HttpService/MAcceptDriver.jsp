<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.OpenRequestBO,com.common.util.TDSValidation"%>
<%! String m_acceptedStatus = "";OpenRequestBO m_openRequestBO = null; %>
<%
	if(request.getAttribute("acceptedStatus") != null) {
		m_acceptedStatus = (String)request.getAttribute("acceptedStatus");
	}

	if(request.getAttribute("acceptedRequest") != null) {
		m_openRequestBO = (OpenRequestBO) request.getAttribute("acceptedRequest");
	}
%>

<%= "<xml version=\"1.0\" >" %>
<%= "<AcceptedRequest>" %>
<%= "<AcceptedStatus>"+m_acceptedStatus+"</AcceptedStatus>" %>
<%
	if(m_openRequestBO != null) {
%>
<%= "<Tripid>"+m_openRequestBO.getTripid()+"</Tripid>" %>
<%= "<PhoneNo>"+TDSValidation.getViewPhoneFormat(m_openRequestBO.getPhone())+"</PhoneNo>" %>
<%= "<ServiceDate>"+m_openRequestBO.getSdate()+"</ServiceDate>" %>
<%= "<ServiceTime>"+m_openRequestBO.getShrs()+":"+m_openRequestBO.getSmin()+"</ServiceTime>" %>
<%= "<Address1>"+m_openRequestBO.getSadd1()+"</Address1>" %>
<%= "<Address2>"+m_openRequestBO.getSadd2()+"</Address2>" %>
<%= "<State>"+m_openRequestBO.getSstate()+"</State>" %>
<%= "<City>"+m_openRequestBO.getScity()+"</City>" %>
<% 
	}
%>
<%= "</AcceptedRequest>" %>