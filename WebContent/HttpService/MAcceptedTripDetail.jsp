<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page
	import="java.util.List,java.util.ArrayList,com.tds.tdsBO.OpenRequestBO"%>
<%! List m_acceptedList = null; OpenRequestBO m_acceptedRequest = null; 
	int m_tripCounter;
%>
<%
	if(request.getAttribute("acceptedTrip") != null ) {
		m_acceptedList = (ArrayList) request.getAttribute("acceptedTrip");
	}
%>
<%= "<xml version=\"1.0\" >" %>
<%= "<AcceptedTripDetails>" %>
<% if( m_acceptedList != null && m_acceptedList.size() > 0 ) { 
		for(m_tripCounter = 0; m_tripCounter < m_acceptedList.size(); m_tripCounter++ ) {
			m_acceptedRequest = (OpenRequestBO) m_acceptedList.get(m_tripCounter);
	%>
<%= "<tripID"+m_acceptedRequest.getTripid()+">" %>
<%= m_acceptedRequest.getSttime()+"^"+m_acceptedRequest.getSdate()+"^"+m_acceptedRequest.getSadd1()+"^"+m_acceptedRequest.getScity()+"^"+m_acceptedRequest.getSstate() %>
<%= "</tripID"+m_acceptedRequest.getTripid()+">" %>
<%
		}
	%>
<% } else { %>
<%= "<error>No Accepted Trip</error>" %>
<% } %>
<%= "<AcceptedTripDetails>" %>