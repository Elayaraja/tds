<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page
	import="com.tds.tdsBO.AdminRegistrationBO,java.util.Map,java.util.HashMap,java.util.List,com.tds.tdsBO.OpenRequestBO"%>
<%! AdminRegistrationBO m_adminBo = null; Map m_driverMap = null; List m_publicRequestList = null;OpenRequestBO m_openRequestBO = null;  %>
<%= "<? xml version=\"1.0\" ?>" %>
<% 
		if(session.getAttribute("user") != null) {
			m_adminBo = (AdminRegistrationBO)session.getAttribute("user");
		}
		if(request.getAttribute("availablility") != null) {
			m_driverMap = (HashMap)request.getAttribute("availablility");
		}
		if(m_driverMap.containsKey("publicRequest")) {
			m_publicRequestList = (List)m_driverMap.get("publicRequest");
		}
%>
<%= "<DriverLocation>" %>
<%
	if(m_adminBo != null && m_adminBo.getUsertypeDesc().equalsIgnoreCase("Driver")) {
%>
<%= "<LocationStatus>" + m_driverMap.get("location").toString() + "</LocationStatus>" %>
<%= "<QueueId></QueueId>" %>
<%= "<QueuePosition></QueuePosition>" %>
<%= "<QueueStatus></QueueStatus>" %>
<%= "<Error></Error>" %>
<%= "<PublicRequests>" %>
<%
			if(m_publicRequestList != null && m_publicRequestList.size()  > 0) {
				for(int counter=0;counter<m_publicRequestList.size();counter++) {
					m_openRequestBO = (OpenRequestBO) m_publicRequestList.get(counter);
					out.println("<OpenRequest>");
					out.println("<RequestID>"+m_openRequestBO.getTripid()+"</RequestID>");
					out.println("<StartLO>"+m_openRequestBO.getScity()+" "+m_openRequestBO.getSstate()+"</StartLO>");
					out.println("<EndLO>"+m_openRequestBO.getEcity()+" "+m_openRequestBO.getEstate()+"</EndLO>");
					out.println("<Time>"+m_openRequestBO.getShrs()+":"+m_openRequestBO.getSmin()+"</Time>");
					out.println("<Date>"+m_openRequestBO.getSdate()+"</Date>");
					out.println("<Latitude>"+m_openRequestBO.getSlat()+","+m_openRequestBO.getEdlatitude()+"</Latitude>");
					out.println("<Longitude>"+m_openRequestBO.getSlong()+","+m_openRequestBO.getEdlongitude()+"</Longitude>");
					out.println("<NumPass></NumPass>");
					out.println("<StartAddress>"+m_openRequestBO.getSadd1()+" "+m_openRequestBO.getSadd2()+"<StartAddress>");
					out.println("<SpecialRequirement></SpecialRequirement>");
					out.println("<SpecialRide></SpecialRide>");
					out.println("<PaymentType></PaymentType>");
					out.println("</OpenRequest>");
				}
			}
		%>

<%= "</PublicRequests>" %>
<% 
	} else {
%>
<%= "<DriverStatus>Not a Driver</DrvierStatus>" %>
<%
	}
%>
<%= "</DriverLocation>" %>