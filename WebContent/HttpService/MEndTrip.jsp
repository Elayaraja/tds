<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.Map,java.util.HashMap"%>
<%!Map m_endMap = null; %>
<%
	if(request.getAttribute("endTrip") != null) {
		m_endMap = (HashMap)request.getAttribute("endTrip");
	}
%>
<%= "<?xml version=\"1.0\"?>" %>
<%= "<EndTrip>" %>
<%
	if(m_endMap != null) {
%>
<%= "<Status>"+ m_endMap.get("status") +"</Status>" %>
<%= "<Description>"+m_endMap.get("description") +"</Description>" %>
<%		
	} else {
%>
<%= "<Status>Failed in End Trip</Status>" %>
<% 
	}
%>
<%= "</EndTrip>" %>