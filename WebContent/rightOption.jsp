<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="com.common.util.TDSConstants"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<meta name="description" content="Login and Registration Form with HTML5 and CSS3" />
<meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />
<meta name="author" content="Codrops" />
<link rel="shortcut icon" href="../favicon.ico"> 
<link rel="stylesheet" type="text/css" href="jqueryNew/demo.css" />
<link rel="stylesheet" type="text/css" href="jqueryNew/style.css" />
<link rel="stylesheet" type="text/css" href="jqueryNew/animate-custom.css" />
<script type="text/javascript" src="js/jquery.js"></script>

<script language="javascript">
function Submit(e,value){
	var KeyID = (window.event) ? event.keyCode : e.keyCode; 
    if(KeyID == 13 ||value==2)
     {
    	
    	if(document.getElementById("TDSAppLoading")!=null){
    		$("#TDSAppLoading").show("fade");
    	}
    	if(document.getElementById("loginName").value!="" && document.getElementById("password").value!=""){	
    		document.submitForm.submit(); 
      	}else {
   			document.getElementById("errorpage").innerHTML="Enter Username & Password";
   		}
	} 
}

$(document).ready ( function(){
//	removeClass(0);
	var inputField=document.getElementById("loginName");
	inputField.focus();
});
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
</head>

<body>
<form name="submitForm" action ="control" method="post"/>
	<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"/>
	<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.checkUserRegistration %>"/>
	<div class="container" >
			<header>
                <h1>Welcome to GAC</h1>
            </header>
                        		<div id="errorpage" style="color:#b94a48;">
                        		<%
									if(request.getAttribute("errors") != null ) {
									out.println(request.getAttribute("errors"));
									}
									System.out.println(request.getAttribute("errors"));	
									%>
									</div>
							<section>	
                               <div id="wrapper">
                              <div id="login" class="animate form">
                                <h1>Log in</h1> 
                                <p> 
                                    <label class="uname" for="User id">User ID:</label>
                                 	<input type="text" tabindex="1" class="data-icon"  name="loginName" id="loginName" required="required"  onkeypress="return Submit(event,1)"  value=""/>
								</p>
                                <p>                                     
                                    <label class="youpasswd"  for="Password">Password:</label>
                                    <input type="password" tabindex="2" name="password" id="password" required="required" onkeypress="return Submit(event,1)"  value=""/>
								</p>                                    
								<p class="login button"> 
								     <input type="button" value="Login" tabindex="3"  onclick="return Submit(event,2)"/>
                                </p>
                                <p class="change_link">
                  					<a href="AssistantOperator?event=assistantLogin" style="white-space:none;text-decoration:none;font-size:12px;text-align:right">Login As Assistant Operator !</a>
                  				</p>
                                 </div>
                        	</div>
                        	</section>
                        </div>
	 
</body>
</html>
