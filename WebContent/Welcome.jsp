<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.tdsBO.AdminRegistrationBO;"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" href="css/style.css"></link>
<script type="text/javascript">
 
function operation()
{
document.getElementById("dis").style.display = 'none';
document.getElementById("fin").style.display = 'none';
document.getElementById("system").style.display = 'none';
 document.getElementById("oper").innerHTML="<div class='leftCol'>"
 +"<ul class='vertMenu'>"
 +"<li><a href='control?action=admin&event=driverTempDeActive' title='Deactivation Driver'>Deactivation Driver</a></li>"
 +"<li><a href='control?action=admin&event=getDeActiveSummary' title='Activate Driver'>Activate Driver</a></li>"
 +"<li><a href='control?action=registration&event=createLostandFound' title='Create Lost and Found'>Create Lost and Found</a></li>"
 +"<li><a href='control?action=registration&event=lostandFoundSummary' title='Search Edit Lost and Found' >Search Edit Lost and Found</a></li>"
 +"<li><a href='control?action=registration&event=driverPenalityMaster' title='Driver Credit/Charge' >Driver Credit/Charge</a></li>"
 +"<li><a href='control?action=registration&event=createMaintence' title='Create Cab Maintenance'>Create Cab Maintenance</a></li>"
 +"<li><a href='control?action=registration&event=maintenanceSummary' title='Cab Maintenance Summary'>Cab Maintenance Summary</a></li>"
 +"<li><a href='control?action=registration&event=ZoneEntry' title='Zone Login'>Zone Login</a></li>"
 +"<li><a href='control?action=registration&event=custInfo' title='Customer Service'>Customer Service</a></li>"
 +"<li><a href='control?action=openrequest&event=ServiceSummary' title='Service Summary' class='last'>Service Summary</a></li>"
 +"</ul>"
 +"</div>"
document.getElementById("oper").style.display = 'block';
}

function finance()
{
document.getElementById("dis").style.display = 'none';
document.getElementById("oper").style.display = 'none';
document.getElementById("system").style.display = 'none';
 document.getElementById("fin").innerHTML="<div class='leftCol'>"
 +"<ul class='vertMenu'>"
 +"<li><a href='control?action=manualccp&event=manualCCProcess' title='Process Credit Card'>Process Credit Card</a></li>"
 +"<li><a href='control?action=manualccp&event=getSummarytoAlter' title='Reverse&nbsp;Credit&nbsp;Card&nbsp;Transactions'>Reverse Credit Card Transactions</a></li>"
 +"<li><a href='control?action=manualccp&event=getSummarytoAlterlimit' title='Transaction Summary'>Transaction Summary</a></li>"
 +"<li><a href='control?action=registration&event=createVoucher' title='Create Voucher' >Create Voucher</a></li>"
 +"<li><a href='control?action=registration&event=getVoucherEditPage' title='Change Voucher' >Change Voucher</a></li>"
 +"<li><a href='control?action=manualccp&event=getUntxnSummary' title='Reverse Un&nbsp;Capture&nbsp;CC&nbsp;Transactions'>Reverse Un Capture CC Transactions</a></li>"
 +"<li><a href='control?action=registration&event=uncapturedamtdetails' title='Tag Voucher Payment'>Tag Voucher Payment</a></li>"
 +"<li><a href='control?action=registration&event=driverDisbursement' title='Driver Disbursement'>Driver Disbursement</a></li>"
 +"<li><a href='control?action=registration&event=driverDisbursementSummary' title='Driver&nbsp;Disbursement&nbsp;Summary'>Driver Disbursement Summary</a></li>"
 +"<li><a href='#'style='color: #3333FF' title='CSV Upload'>CSV Upload</a></li>"
 +"<li><a href='control?action=CSV&event=csvUploadStart' title='Driver Charge/Credit Upload'>Driver Charge/Credit Upload</a></li>"
 +"<li><a href='control?action=manualccp&event=manualSettle' title='Manual Settlement'>Manual Settlement</a></li>"
 +"</ul>"
 +"</div>"
document.getElementById("fin").style.display = 'block';
}

function systemsetup()
{
document.getElementById("dis").style.display = 'none';
document.getElementById("oper").style.display = 'none';
document.getElementById("fin").style.display = 'none';
 document.getElementById("system").innerHTML="<div class='leftCol'>"
 +"<ul class='vertMenu'>"
 +"<li><a href='#' style='color: #3333FF' title='User Admin'>User Admin</a></li>"
 +"<li><a href='control?action=registration&event=adminregis' title='User Registration'>User Registration</a></li>"
 +"<li><a href='control?action=registration&event=summaryDR' title='Driver Update'>Driver Update</a></li>"
 +"<li><a href='control?action=userrolerequest&event=getUserList&subevent=access' title='View/Change User Permissions' >View/Change&nbsp;User&nbsp;Permissions</a></li>"
 +"<li><a href='control?action=registration&event=savecompany&submit=View' title='View/Edit Company'>View/Edit Company</a></li>"
 +"<li><a href='control?action=registration&event=Adminview' title='Admin&nbsp;MasterView'>Admin MasterView</a></li>"
 +"<li><a href='control?action=registration&event=cabRegistration' title='Cab Registration'>Cab Registration</a></li>"
 +"<li><a href='control?action=registration&event=cabRegistrationSummary' title='Cab Registration Summary'>Cab Registration Summary</a></li>"
 +"<li><a href='control?action=registration&event=CabDriverMapping' title='Cab Driver Mapping'>Cab Driver Mapping</a></li>"
 +"<li><a href='control?action=registration&event=CabDriverMappingSummary' title='Cab Driver Mapping Summary'>Cab&nbsp;Driver&nbsp;Mapping&nbsp;Summary</a></li>"
 +"<li><a href='#' style='color:#3333FF' title='Zone'>Zone</a></li>"
 +"<li><a href='control?action=registration&event=createqueueCoordinate' title='Create Zone Co-Ordinates'>Create Zone Co-Ordinates</a></li>"
 +"<li><a href='control?action=registration&event=queuesummary' title='Zone Co-Ordinates Summary'>Zone&nbsp;Co-Ordinates&nbsp;Summary</a></li>"
 +"<li><a href='#' style='color:#3333FF' title='Credit Card'>Credit Card</a></li>"
 +"<li><a href='control?action=registration&event=companyccsetup' title='Credit Card Charges'>Credit Card Charges</a></li>"
 +"<li><a href='#' style='color:#3333FF' title='Financial'>Financial</a></li>"
 +"<li><a href='control?action=registration&event=cmpPaymentSettelmentSch' title='Payment Settlement Schedule'>Payment&nbsp;Settlement&nbsp;Schedule</a></li>"
 +"<li><a href='control?action=registration&event=driverChargesMaster' title='Recurring Driver Charges'>Recurring Driver Charges</a></li>"
 +"</ul>"
 +"</div>"
document.getElementById("system").style.display = 'block';
}


</script>
</head>
<body   bgcolor="#FDEEF4">
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	
%>

<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
    	
    	<input  type="hidden"   name="module"   id="module"   />
        	 <%String result="";
								if(request.getAttribute("page")!=null) {
									result = (String)request.getAttribute("page");
								}
								%>
								<div id="errorpage">
								<%=result.length()>0?"Sucessfully"+result:"" %>
								</div>
            
            	<div id="oper"></div>
            	<div id="fin"></div>    
            	<div id="system"></div>                          
<!--                 <div class="rightCol">
				<div class="rightColIn">
                	<div class="loginBX">
                	<div class="grayBX">
                    	<div class="topLCur"><div class="topRCur"><div class="topMCur"><div class="topMCur1"></div></div></div></div>
                        <div class="mid"><div class="mid1"><div class="mid2"><div class="mid3">
                        	<div class="formDV">
                        		<h1>Welcome</h1>      
                        	</div>
                        </div></div></div></div>
                        <div class="btmLCur"><div class="btmRCur"><div class="btmMCur"><div class="btmMCur1"><div class="btmMCur2"><div class="btmMCur3"><div class="btmMCur4"><div class="btmMCur5"></div></div></div></div></div></div></div></div>
                    </div>
                    </div>
                </div>    
              </div>
 --> 
            <div style="margin-left: 10%;margin-top: 2%;">
				<a href="control?action=openrequest&event=saveRequest&module=dispatchView"> <img alt="" src="images/CreateAJob.png"/><font style="margin-left: -8%;font-weight: bold;font-style: italic;color:#3BB9FF;font-size: medium;">Create A Job</font></a>
			</div>
             <div style="margin-left: 28%;margin-top: -6%;">
            	<a href="DashBoard?event=dispatchValues"> <img alt="" src="images/Dashboard.png"/><font style="margin-left: -9%;font-weight: bold;font-style: italic;color:#3BB9FF;font-size: medium;">Dashboard</font></a>
			</div>            	   		
            <div style="margin-left:46%;margin-top: -6%;">
            	<a href="control?action=openrequest&event=Report"> <img alt="" src="images/reportMain.png"/><font style="margin-left: -10%;font-weight: bold;font-style: italic;color:#3BB9FF;font-size: medium;">Reports</font></a>
            </div>
            <div style="margin-left: 10%;margin-top: 2%;">
            	<a href="control?action=systemsetup&event=systemProperties&module=systemsetupView"> <img alt="" src="images/SysSetup.png"/><font style="margin-left: -8%;font-weight: bold;font-style: italic;color:#3BB9FF;font-size: medium;">System Setup</font></a>
            </div>
            <div style="margin-left: 28%;margin-top: -6%;">
            	<a href="control?action=registration&event=custInfo&module=operationView"> <img alt="" src="images/ClientAcct.png"/><font style="margin-left: -10%;font-weight: bold;font-style: italic;color:#3BB9FF;font-size: medium;">Client Account</font></a>
            </div>
            <div style="margin-left: 46%;margin-top: -8%;">
            	<a href="control?action=registration&event=driverDisbursement&module=financeView"> <img alt="" src="images/Disbursement.png"/><font style="margin-left: -14%;font-weight: bold;font-style: italic;color:#3BB9FF;font-size: medium;">Disbursement</font></a>
            </div>

             <div class="clrBth"></div>
         
		</div>
	</div>
</div>
</body>
</html>
