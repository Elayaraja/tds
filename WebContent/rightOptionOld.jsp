<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript" src="js/jquery.js"></script>

<script language="javascript">
function Submit(e){
	
	var KeyID = (window.event) ? event.keyCode : e.keyCode; 
    if(KeyID == 13)
       {
    	document.submitForm.submit(); 
       }
}
function fixClass(switchKey){
	if(switchKey==1){
	document.getElementById("inputBXWrapName").style.border="inset red 2px";
	document.getElementById("loginName").style.color="#FF0000";
	
	}else{
		document.getElementById("inputBXWrapPass").style.border="inset red 2px";
		document.getElementById("password").style.color="#FF0000";	 
	}


}
function removeClass(switchKey){
	if(switchKey==1){
	document.getElementById("inputBXWrapName").style.border="inset black 1px";
	document.getElementById("loginName").style.color="black";
	
	}else if(switchKey==2){
		document.getElementById("inputBXWrapPass").style.border="inset black 1px";
		document.getElementById("password").style.color="black";	 
	}else{
		document.getElementById("inputBXWrapName").style.border="inset black 1px";
		document.getElementById("loginName").style.color="black";
		document.getElementById("inputBXWrapPass").style.border="inset black 1px";
		document.getElementById("password").style.color="black";	
	}
}

$(document).ready ( function(){
	removeClass(0);
	var inputField=document.getElementById("loginName");
	inputField.focus();
});
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form name="submitForm" action ="control" method="post"/>
	<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"/>
	<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.checkUserRegistration %>"/>
	<div id="login" style="margin-left:150px;">
	<div class="wrapper2">
   <div class="rightCol">
				<div class="rightColIn2">
                	<div class="loginBX">
                	<div class="grayBX">
                    	<div class="topLCur"><div class="topRCur"><div class="topMCur"><div class="topMCur1"></div></div></div></div>
                        <div class="mid"><div class="mid1"><div class="mid2"><div class="mid3">
                        	<div class="formDV">
                        		<h2   align="center" >Login</h2>
                        		<div id="errorpage">
                        		<%
									if(request.getAttribute("errors") != null ) {
									out.println(request.getAttribute("errors"));
									}
									System.out.println(request.getAttribute("errors"));	
									%>
									</div>
                                	<div class="fieldDv padB3">
                                    <fieldset>
                                    <label1 for="User id">User ID:</label1>
                                    <div class="div4"><div class="div4In"><div id="inputBXWrapName" class="inputBXWrap" >
                                 	<input type="text" tabindex="1"  name="loginName" id="loginName" class="inputBX" onfocus=" fixClass(1)" onblur="removeClass(1)" onkeypress="return Submit(event)"  value=""/>
                                    </div></div></div>
									</fieldset>
                                    
                                	<fieldset>
                                    <label1 for="Password">Password:</label1>
                                    <div class="div4"><div class="div4In"><div id="inputBXWrapPass" class="inputBXWrap">
                                    <input type="password" tabindex="2" name="password" id="password" class="inputBX"  onfocus="fixClass(2)" onblur="removeClass(2)" onkeypress="return Submit(event)"  value=""/>
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <div class="div4"><div class="div4In">
                                    		<div class="fltRht padT10">
                                              <div class="btnBlue">
                                                   <div class="rht">
                                                 <input type="button" value="Login" tabindex="3" class="lft" onclick="javascript:submit()"/>
                                                   </div>
                                              </div>
                                          	</div>
                                            <div class="clrBth"></div>
                                             <p class="forgetPass">
                                    		<% if(request.getAttribute("errors") != null ) {
                                    			out.println	("<a href ='control?action=registration&event=forgetpasswordinsert' >ForgotPassword</a>");
											} %>
                                            </p>
                                    	    </div></div>
									</fieldset>
                                   </div>
                                    
                  <a href="AssistantOperator?event=assistantLogin" style="white-space:none;text-decoration:none;font-size:12px;text-align:right">Login As Assistant Operator !</a>
                                        
                                
                        	</div>
                        </div></div></div></div>
                        <div class="btmLCur"><div class="btmRCur"><div class="btmMCur"><div class="btmMCur1"><div class="btmMCur2"><div class="btmMCur3"><div class="btmMCur4"><div class="btmMCur5"></div></div></div></div></div></div></div></div>
                    </div>
                    </div>
                </div>    
              </div>
            	</div>
                <div class="clrBth"></div>
            </div>
            <div class="footerDV" style="margin-left:160px;">Copyright &copy; 2010 Get A Cab</div>
	 
</body>
</html>
