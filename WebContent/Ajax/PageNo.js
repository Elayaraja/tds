function AjaxInteraction(url, callback) {
	var req = init();
	req.onreadystatechange = processRequest;
	function init() {
		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	function processRequest() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				if (callback)
					callback(req.responseText);
			}
		}
	}
	this.doGet = function() {
		req.open("GET", url, true);
		req.send(null);
	}
}
function pageNo(count, fdate, tdate, assocode, driver) {
	url = '/TDS/AjaxPageNo?event=getPage&rowCount=' + count + "&fdate=" + fdate+ "&tdate=" + tdate + "&assocode=" + assocode + "&driver=" + driver;
	var ajax = new AjaxInteraction(url, pageId);
	ajax.doGet();
}
function pageId(responseText) {
	var resp = responseText.split('###');
	document.getElementById("pageId").innerHTML = resp[0];
}
