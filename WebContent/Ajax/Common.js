/**
 * 
 */
function AJAXInteraction(url, callback) {

	var req = init();
	req.onreadystatechange = processRequest();

	function init() {

		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	} 

	function processRequest () {

		if (req.readyState == 4) {
			if (req.status == 200) {
				if (callback) callback(req.responseText);
			}
		}
	}

	this.doGet = function() {
		req.open("GET", url, true);
		req.send(null);
	};
}

//------ Ajax InterAction  Over    -------------------------------
function newDeleteDash(tripId,fleet){
	var repeatJob;
	var xmlhttp = null;
	if (window.XMLHttpRequest) 
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(document.getElementById('multiJobUpdate').value==1){
		repeatJob = '&multiJobUpdate='+tripId;
	} else {
		repeatJob = '&multiJobUpdate=';
	} if(fleet!=""){
		repeatJob=repeatJob+'&assoCode='+fleet;
	}
	var url = 'OpenRequestAjax?event=saveOpenRequest&delete=delete&source=OR&tripId='+tripId+repeatJob;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var resp=text.split("###");
	var result=resp[0].split(";");
	if(result[0].indexOf('001')>=0){
		if(resp[1].indexOf('003')>=0){
			window.open('DashBoard?event=dispatchValues',"_self");
		} else{
			document.getElementById("successPage").innerHTML=result[1];
			document.getElementById("successPage").style.display="block";
			document.getElementById("errorPage").style.display="none";
			clearOpenRequest();
		}
	} else {
		document.getElementById("errorPage").innerHTML=result[1];
		document.getElementById("errorPage").style.display="block";
		document.getElementById("successPage").style.display="none";
	}
}
function newDeleteDashOR(tripId,fleet){
	var repeatJob;
	var xmlhttp = null;
	if (window.XMLHttpRequest) 
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(document.getElementById('multiJobUpdateDash').value==1){
		repeatJob = '&multiJobUpdate='+tripId;
	} else {
		repeatJob = '&multiJobUpdate=';
		tripId = document.getElementById('tripId').value;
	} if(fleet!=""){
		repeatJob=repeatJob+'&assoCode='+fleet;
	}
	var url = 'OpenRequestAjax?event=saveOpenRequest&delete=delete&source=OR&tripId='+tripId+repeatJob;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	removeOR();
}

function removeOR(){
	$("#ORDash").jqmHide();
	$("#tabForDetails").hide();
	document.getElementById("Error").style.display="none";
	Init("ORHide");
	removeAddress();
	clearOR();
}
function removeAddress(){
	$("#ORDashAddress").hide("slow");
	$("#showHover").hide("slow");
}
function removeHover(){
	$("#showHover").hide("slow");
}
function removeLandMark(){
	$("#ORDashLandMark").hide("slow");
}
function removeError(){
	document.getElementById("ErrorStatus").style.display="none";
}

function dateValidationDash(){
	var fromdate=document.getElementById("fromDateDash").value;
	var todate=document.getElementById("toDateDash").value;
	if(document.getElementById("tripId").value==""){
		if(document.getElementById("landMarkDash").value!="" && document.getElementById('LMKeyDash').value=="" && document.getElementById("newLandMark").checked==false){
			document.getElementById("Error").innerHTML="Entered Landmark not found,select new Landmark or enter LM again and try";
			document.getElementById("Error").style.display="block";
			return false;
		}
		if(document.getElementById("toLandMarkDash").value!="" && document.getElementById('LMKeyDash').value=="" && document.getElementById("newToLandMark").checked==false){
			document.getElementById("Error").innerHTML="Entered D/O Landmark not found,select new Landmark or enter LM again and try";
			document.getElementById("Error").style.display="block";
			return false;
		}
	}
	if(document.getElementById("acctDash").value!="" && document.getElementById('accountSuccess').value == "" && document.getElementById("tripId")==""){
		document.getElementById("Error").innerHTML="The Voucher You Have Entered Is Not Valid";
		document.getElementById("Error").style.display="block";
		return false;
	}
	var spl="";  
	var spl1="";
	var cmt="";  
	var cmt1="";
	if(document.getElementById("specialInsDash").value !="Permanent Comments"){
		spl=document.getElementById("specialInsDash").value.length;
	}
	if(document.getElementById("specialInsDash1").value !="Temporary Comments"){
		spl1=document.getElementById("specialInsDash1").value.length;
	}
	if(spl+spl1 >249){
		document.getElementById("Error").innerHTML="Sorry Driver Comments Can't Exceed 250 Characters";
		document.getElementById("Error").style.display="block";
		return false;
	}
	if(document.getElementById("CommentsDash").value !="Permanent Comments"){
		cmt=document.getElementById("CommentsDash").value.length;
	}
	if(document.getElementById("CommentsDash1").value !="Temporary Comments"){
		cmt1=document.getElementById("CommentsDash1").value.length;
	}
	if(cmt+cmt1 >249){
		document.getElementById("Error").innerHTML="Sorry Operatot Comments Can't Exceed 250 Characters";
		document.getElementById("Error").style.display="block";
		return false;
	}
	if(document.getElementById('roundTrip').checked==true && document.getElementById('eadd1Dash').value==""){
		document.getElementById("Error").innerHTML="Round Trip Should Have D/O Address";
		document.getElementById("Error").style.display="block";
		return false;
	}
//	if(document.getElementById('roundTrip').checked==true && document.getElementById("shrs").value=="Now"){
//	document.getElementById("Error").innerHTML="For Round Trip You Should Change 'Now' Time";
//	document.getElementById("Error").style.display="block";
//	return false;
//	}
	if(document.getElementById("CommentsDash").value.indexOf(";")>=0 || document.getElementById("CommentsDash").value.indexOf("=")>=0){
		document.getElementById("Error").innerHTML="Semi colon and equal symbol are not allowed in driver & dispatch comments";
		document.getElementById("Error").style.display="block";
		return false;
	} if(document.getElementById("CommentsDash1").value.indexOf(";")>=0 || document.getElementById("CommentsDash1").value.indexOf("=")>=0){
		document.getElementById("Error").innerHTML="Semi colon and equal symbol are not allowed in driver & dispatch comments";
		document.getElementById("Error").style.display="block";
		return false;
	}
	if(document.getElementById("specialInsDash").value.indexOf(";")>=0 || document.getElementById("specialInsDash").value.indexOf("=")>=0){
		document.getElementById("Error").innerHTML="Semi colon and equal symbol are not allowed in driver & dispatch comments";
		document.getElementById("Error").style.display="block";
		return false;
	} if(document.getElementById("specialInsDash1").value.indexOf(";")>=0 || document.getElementById("specialInsDash1").value.indexOf("=")>=0){
		document.getElementById("Error").innerHTML="Semi colon and equal symbol are not allowed in driver & dispatch comments";
		document.getElementById("Error").style.display="block";
		return false;
	}	 if(document.getElementById('dispatchStatusDash').checked==true && document.getElementById("driverDashOR").value!="" && (document.getElementById('updateAll').value==0 || document.getElementById('updateAll').value=="")){
		document.getElementById("Error").innerHTML="You allocated a driver. Cannot give do not dispatch option";
		document.getElementById("Error").style.display="block";
		return false;
	}if(document.getElementById('phoneDash').value.length > 10){
		document.getElementById("Error").innerHTML="Ph.No should contain only 10 digits";
		document.getElementById("Error").style.display="block";
		return false;
	}
//	if(document.getElementById("airName").value !=""){
//		code=document.getElementById("airName").value;
//		var letters = /^[A-Za-z]+$/;
//		if(!code.match(letters) || code.length>2)  
//		{  
//			document.getElementById("Error").innerHTML="Airport code should contain only 2 digit character";
//			document.getElementById("Error").style.display="block";
//			return false;  
//		}  
//	}
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	if(month<10){
		month = "0"+month;
	}
	if(day<10){
		day = "0"+day;
	}
	var date= month+ "/" + day + "/" +year ;
	if(document.getElementById("shrs").value=="Now" && document.getElementById("sdate").value > date){
		document.getElementById("Error").innerHTML="For future dates you should change 'Now' time";
		document.getElementById("Error").style.display="block";
		return false;
	}
	if(fromdate!="" || todate!=""){
		if(document.getElementById("shrs").value=="Now"){
			document.getElementById("Error").innerHTML="For multi jobs you should change 'Now' time";
			document.getElementById("Error").style.display="block";
			return false;
		}
		var curdate = document.getElementById("sdate").value;
		var curDate = curdate.substring(6,10)+curdate.substring(0, 2)+curdate.substring(3, 5);
		var fromDate = fromdate.substring(6,10)+fromdate.substring(0, 2)+fromdate.substring(3, 5);
		var toDate = todate.substring(6,10)+todate.substring(0, 2)+todate.substring(3, 5); 
		if(Number(fromDate)>=Number(curDate) && Number(toDate)>Number(fromDate) ) {
			if(document.getElementById('repeatGroupDash').value !="" && document.getElementById('repeatGroupDash').value!=0){
				updateMultiJobDash("Do you want to update all the repeat job for this tripId?",1,"","");
			} else {
				enterOR();
			}
			return true; 
		} else {
			document.getElementById("Error").innerHTML="Check Your Repeat Job From And To Date";
			document.getElementById("Error").style.display="block";
			document.getElementById('fromDate').value= "";
			document.getElementById('toDate').value= "";
			return false;
		}
	} else {
		if(document.getElementById('repeatGroupDash').value !="" && document.getElementById('repeatGroupDash').value!=0){
			updateMultiJobDash("Do you want to update all the repeat job for this tripId?",1,"","");
		}else {
			enterOR();
		}
	}	
}
function deleteJobOR() {
	 if(document.getElementById('repeatGroupDash').value !="" && document.getElementById('repeatGroupDash').value!=0){
		 updateMultiJobDash("Do you want to delete all the repeat job for this tripId?",2,document.getElementById('repeatGroupDash').value,document.getElementById("fleetForOR").value);
	 } else {
		 newDeleteDashOR(document.getElementById("tripId").value,document.getElementById("fleetForOR").value);
	 }
}
function enterOR(){
	var phone = document.getElementById('phoneDash').value;
	var name = document.getElementById('nameDash').value;
	var eMail=	document.getElementById('eMailDash').value;
	var add1 = document.getElementById('sadd1Dash').value;
	var add2 = document.getElementById('sadd2Dash').value;
	var city = document.getElementById('scityDash').value;
	var state = document.getElementById('sstateDash').value;
	var zip = document.getElementById('szipDash').value;
	var lati = document.getElementById('sLatitudeDash').value;
	var longi = document.getElementById('sLongitudeDash').value;
	var slandmark=document.getElementById('landMarkDash').value;
	var elandmark=document.getElementById('toLandMarkDash').value;
	var date = document.getElementById('sdate').value;
	var tripStatus=document.getElementById('tripStatus').value;
	var premiumCustomer=document.getElementById('premiumNo').value;
	var updateAll=document.getElementById("updateAll").value;
	var time = '';
	if(document.getElementById('shrs').value=="Now"){
		time = '2525';
		premiumCustomer=0;
	} else if (document.getElementById('shrs').value=="NCH"){
		time=document.getElementById("timeTemporary").value;
	} else {
		time = document.getElementById('shrs').value;
		premiumCustomer=2;
	}
	var meterType = document.getElementById("meter").value;
	var dispComments="";
	var dispCommentsTemp="";
	if(document.getElementById('CommentsDash').value!="Permanent Comments"){
		dispComments=document.getElementById('CommentsDash').value;
	} if(document.getElementById('CommentsDash1').value!="Temporary Comments"){
		dispCommentsTemp=document.getElementById('CommentsDash1').value;
	}
	var comments = dispComments+""+dispCommentsTemp;
	comments=comments.replace("&","and").replace("%","pct");
	var updateStaticComment=dispComments.replace("&","and").replace("%","pct");
	var permComments="";
	var tempComments="";
	if(document.getElementById('specialInsDash').value!="Permanent Comments"){
		permComments=document.getElementById('specialInsDash').value;
	} if(document.getElementById('specialInsDash1').value!="Temporary Comments"){
		tempComments=document.getElementById('specialInsDash1').value;
	}
	if(document.getElementById("premiumCustomerDash").checked==true){
		premiumCustomer=1;
	}
	var splIns = permComments+""+tempComments;
	splIns=splIns.replace("&","and").replace("%","pct");
	var updateDriverComments=permComments.replace("&","and").replace("%","pct");
	var tripId = document.getElementById('tripId').value;
	var masterKey = document.getElementById('masterAddress').value;
	var addressKey= document.getElementById('addressKey').value;
	var addressKeyTo=document.getElementById('addressKeyTo').value;
	var landMarkKey="";
	if(addressKey!="000" && document.getElementById('newLandMark').checked==false && document.getElementById('newToLandMark').checked==false){
		landMarkKey=document.getElementById("LMKeyDash").value;
	}
	var callerPhone=document.getElementById("callerPhone").value;
	var calledInBy=document.getElementById("callerName").value;
	var eadd1 = document.getElementById('eadd1Dash').value;
	var eadd2 = document.getElementById('eadd2Dash').value;
	var ecity = document.getElementById('ecityDash').value;
	var estate = document.getElementById('estateDash').value;
	var ezip = document.getElementById('ezipDash').value;
	var elati = document.getElementById('eLatitude').value;
	var elongi = document.getElementById('eLongitude').value;
	var advTime= document.getElementById('advanceTime').value;
	var fromDate = document.getElementById('fromDateDash').value;
	var toDate = document.getElementById('toDateDash').value;
	var payType=document.getElementById('payTypeDash').value;
	var accountWithName=document.getElementById('acctDash').value;
	var amountTemp=document.getElementById('amtDash').value;
	var amount=amountTemp.replace("$","");
	var accounttoSplit=accountWithName.split("(");
	var account=accounttoSplit[0];
	if(account==""){
		payType="Cash";
	}
	var zone=document.getElementById("queuenoDash").value;
	var driverList=document.getElementById("drprofileSizeDash");
	var vehicleList=document.getElementById("vprofileSizeDash");
	var noOfPassengers=document.getElementById("numberOfPassengerDash").value;
	var numOfCabs=document.getElementById("numOfCabs").value;
	var driver="";
	var cab="";
	var jobRate=document.getElementById("jobRating").value;
	if(document.getElementById("driverORcabDash").value=="Driver"){
		driver=document.getElementById("driverDashOR").value;
		cab=document.getElementById('DriverCabSwitch').value;
	} else {
		cab=document.getElementById("driverDashOR").value;
		driver=document.getElementById('DriverCabSwitch').value;
	}
	var nextScreen='';
	var repeatJob="";
	var airlinename=document.getElementById("airName").value;
	var airlineno=document.getElementById("airNo").value;
	var airlinefrom=document.getElementById("airFrom").value;
	var airlineto=document.getElementById("airTo").value;
	
	var custRefNum = document.getElementById("refNum").value;
	var custRefNum1 = document.getElementById("refNum1").value;
	var custRefNum2 = document.getElementById("refNum2").value;
	var custRefNum3 = document.getElementById("refNum3").value;
	
	if(document.getElementById('Sun').checked==true){
		repeatJob = '&Sun=true';
	}
	if(document.getElementById('Mon').checked==true){
		repeatJob = repeatJob + '&Mon=true';
	}
	if(document.getElementById('Tue').checked==true){
		repeatJob = repeatJob + '&Tue=true';
	}
	if(document.getElementById('Wed').checked==true){
		repeatJob = repeatJob + '&Wed=true';
	}
	if(document.getElementById('Thu').checked==true){
		repeatJob = repeatJob + '&Thu=true';
	}
	if(document.getElementById('Fri').checked==true){
		repeatJob = repeatJob + '&Fri=true';
	}
	if(document.getElementById('Sat').checked==true){
		repeatJob = repeatJob + '&Sat=true';
	}
	if(document.getElementById('newLandMark').checked==true){
		repeatJob = repeatJob+'&newFromLandMark=true';
	}if(document.getElementById('newToLandMark').checked==true){
		repeatJob = repeatJob+'&newToLandMark=true';
	}if(document.getElementById('sharedRideDash').checked==true){
		repeatJob = repeatJob+'&sharedRide=1';
	}else {
		repeatJob = repeatJob+'&sharedRide=0';
	}if(document.getElementById('dispatchStatusDash').checked==true){
		repeatJob = repeatJob+'&dispatchStatus=true';
//		}if(document.getElementById('roundTrip').checked==true){
//		repeatJob = repeatJob+'&roundTrip=true&sdateTrip='+RTdate+'&shrsTrip='+RTtime;
	}if(document.getElementById("roundTripExist").value!=""){
		repeatJob=repeatJob+'&roundTripExist='+document.getElementById("roundTripExist").value;
	}
	if(typeof driverList != "" && driverList!=null){
		repeatJob=repeatJob+'&drprofileSize='+driverList.value;	
		for (var i=0; i<driverList.value;i++){
			if(document.getElementById('dchk'+i).checked==true){
				repeatJob=repeatJob+'&dchk'+i+'='+document.getElementById('dchk'+i).value;
			}
		}
	}
	if(typeof vehicleList != "" &&vehicleList!=null){
		repeatJob=repeatJob+'&vprofileSize='+vehicleList.value;	
		for (var i=0;i<vehicleList.value;i++){
			if(document.getElementById('vchk'+i).checked==true){
				repeatJob=repeatJob+'&vchk'+i+'='+document.getElementById('vchk'+i).value;
			}
		}
	}if(document.getElementById('multiJobUpdateDash').value==1){
		repeatJob = repeatJob+'&multiJobUpdate='+document.getElementById('repeatGroupDash').value;
	} else {
		repeatJob = repeatJob+'&multiJobUpdate=';
	}if(document.getElementById("fieldSize").value!=""){
		var chargeSize=document.getElementById("fieldSize").value;
		repeatJob = repeatJob+'&chargesLength='+chargeSize;
		for (var i=1; i<=chargeSize;i++){
			repeatJob=repeatJob+'&chargeKey'+i+'='+document.getElementById('payType'+i).value;
			repeatJob=repeatJob+'&chargeAmount'+i+'='+document.getElementById('amountDetail'+i).value;
		}
	} if(document.getElementById("fleetForOR").value!=""){
		repeatJob=repeatJob+'&assoCode='+document.getElementById("fleetForOR").value;
	}
	repeatJob=repeatJob+"&addPickUp="+document.getElementById("dontAddPickUp").value+"&addDropOff="+document.getElementById("dontAddDropOff").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(add1 != "" && lati!="" && longi!=""){
		var url = 'OpenRequestAjax';
		var parms = 'event=saveOpenRequest&source=dashBoard&saveOpenReq=saveOpenReq&phone='+phone+'&name='+name+'&sadd1='+add1+'&sadd2='+add2+'&scity='+city+'&sstate='+state+'&szip='+zip+'&sdate='+date+'&shrs='+time+'&sLongitude='+longi+'&sLatitude='+lati+'&tripId='+tripId+'&masterAddress='+masterKey+'&userAddress='+addressKey+'&userAddressTo='+addressKeyTo+'&landMarkKey='+landMarkKey+'&Comments='+comments+'&specialIns='+splIns+'&eadd1='+eadd1+'&eadd2='+eadd2+'&ecity='+ecity+'&estate='+estate+'&ezip='+ezip+'&edlongitude='+elongi+'&edlatitude='+elati+'&advanceTime='+advTime+'&fromDate='+fromDate+'&toDate='+toDate+'&payType='+payType+'&driver='+driver+'&cabNo='+cab+'&acct='+account+'&amt='+amount+'&nextScreen='+nextScreen+'&slandmark='+slandmark+'&elandmark='+elandmark+'&updateStaticComment='+updateStaticComment+'&updateDriverComment='+updateDriverComments+'&queueno='+zone+'&numberOfPassengers='+noOfPassengers+
		'&numOfCabs='+numOfCabs+'&callerPhone='+callerPhone+'&callerName='+calledInBy+'&premiumCustomer='+premiumCustomer+'&eMail='+eMail+'&updateAll='+updateAll+'&tripStatus='+tripStatus+'&jobRating='+jobRate+'&airName='+airlinename+'&airNo='+airlineno+'&airFrom='+airlinefrom+'&airTo='+airlineto+'&meterType='+meterType+'&custRefNum='+custRefNum+'&custRefNum1='+custRefNum1+'&custRefNum2='+custRefNum2+'&custRefNum3='+custRefNum3+repeatJob;
		alert(parms);
		xmlhttp.open("POST", url, false);
		xmlhttp.setRequestHeader("Content-length", parms.length);
		xmlhttp.setRequestHeader("Connection", "close");
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.send(parms);
		var text = xmlhttp.responseText;
		var resp=text.split("###");
		var result=resp[0].split(";");	 
		if(result[0].indexOf('001')>=0){
			var roundTripMessage="";
			var createdTrip=result[1].split("TripId=");
			if(document.getElementById('roundTrip').checked==true) {
				document.getElementById('sadd1Dash').value=eadd1;
				document.getElementById('sadd2Dash').value=eadd2;
				document.getElementById('scityDash').value=ecity;
				document.getElementById('sstateDash').value=estate;
				document.getElementById('szipDash').value=ezip;
				document.getElementById('sLatitudeDash').value=elati;
				document.getElementById('sLongitudeDash').value=elongi;
				document.getElementById('landMarkDash').value=elandmark;
				document.getElementById('eadd1Dash').value=add1;
				document.getElementById('eadd2Dash').value=add2;
				document.getElementById('ecityDash').value=city;
				document.getElementById('estateDash').value=state;
				document.getElementById('ezipDash').value=zip;
				document.getElementById('eLatitude').value=lati;
				document.getElementById('eLongitude').value=longi;
				document.getElementById('toLandMarkDash').value=slandmark;
				document.getElementById('roundTrip').checked=false;
				roundTripMessage="You Have Given RT, Change Time & Submit";
				document.getElementById("roundTripExist").value=createdTrip[1];
			} else {
				$("#ORDash").jqmHide();
				removeAddress();
				clearOR();
			}
			document.getElementById("Error").style.display="none";
			alert(result[1]+" "+roundTripMessage);
			Init("ORHide");
//			if(eMail!=""){
//				sendCustomerMailDash(eMail,createdTrip[1]);
//			}
		}else {
			document.getElementById("Error").innerHTML=result[1];
			document.getElementById("Error").style.display="block";
		}
	} else if(phone!=""){
		document.getElementById("Error").innerHTML="Please Provide Valid Address";
		document.getElementById("Error").style.display="block";
	}
}
//function sendCustomerMailDash(mailId,tripId){
//	 var xmlhttp = null;
//	 if (window.XMLHttpRequest)
//	 {
//		 xmlhttp = new XMLHttpRequest();
//	 } else {
//		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//	 }
//	 url='SystemSetupAjax?event=sendMail&mailId='+mailId+'&tripId='+tripId;
//	 xmlhttp.open("GET", url, true);
//	 xmlhttp.send(null);
//}

function clearSplInsDash(value){
	if(value==1 && document.getElementById("specialInsDash").value=="Permanent Comments"){
		document.getElementById("specialInsDash").value="";
	} else if(value==2 && document.getElementById("specialInsDash1").value=="Temporary Comments") {
		document.getElementById("specialInsDash1").value="";
	}
}
function clearCommentsDash(value){
	if(value==1 && document.getElementById("CommentsDash").value=="Permanent Comments"){
		document.getElementById("CommentsDash").value="";
	} else if(value==2 && document.getElementById("CommentsDash1").value=="Temporary Comments") {
		document.getElementById("CommentsDash1").value="";
	}
}
function checkSplInsLength(){
	var spl="";
	var spl1="";
	if(document.getElementById("specialInsDash").value !="Permanent Comments"){
		spl=document.getElementById("specialInsDash").value.length;
	} if(document.getElementById("specialInsDash1").value !="Temporary Comments"){
		spl1=document.getElementById("specialInsDash1").value.length;
	}
	if(spl+spl1 >249){
		alert("Sorry Comments Can't Exceed 250 Characters");
		return false;
	}
}
function checkCommentsLength(){
	var spl="";
	var spl1="";
	if(document.getElementById("CommentsDash").value !="Permanent Comments"){
		spl=document.getElementById("CommentsDash").value.length;
	} if(document.getElementById("CommentsDash1").value !="Temporary Comments"){
		spl1=document.getElementById("CommentsDash1").value.length;
	}
	if(spl+spl1 >249){
		alert("Sorry Comments Can't Exceed 250 Characters");
		return false;
	}
}
function checkDateTime(){
	document.getElementById("shrs").value="0000";
}
function getCurrentTime(){
	var dTime = new Date();
	var hours=dTime.getHours();
	var min=dTime.getMinutes();
	if(hours<10){
		hours="0"+hours;
	}if(min<10){
		min="0"+min;
	} /*else if(min>=15 && min<30){
			min="15";
		} else if(min>=30 && min<45){
			min="30";
		} else {
			min="45";
		}*/
	var time=hours+""+min;
	return time;
}
function addressAutoFill(row){
	document.getElementById('masterAddress').value = document.getElementById('masterAddressKey_'+row).value;
	document.getElementById('specialInsDash').value = document.getElementById('driverComments'+row).value;
	var finalComments = document.getElementById('specialInsDash').value;
	if(finalComments!="" && finalComments.indexOf("P/U")<0 && finalComments.indexOf("D/O")<0){
		document.getElementById('specialInsDash').value = "P/U:"+document.getElementById('specialInsDash').value;
	}
	document.getElementById('CommentsDash').value = document.getElementById('dispatchComments'+row).value;
	document.getElementById('advanceTime').value = document.getElementById('advanceTimeDash'+row).value;
	if(document.getElementById('premiumCustomerDash'+row).value==1){
		document.getElementById('premiumCustomerDash').checked=true;
	}
	document.getElementById('payTypeDash').value=document.getElementById('payType'+row).value;
	document.getElementById('acctDash').value=document.getElementById('acct'+row).value;
	if(document.getElementById('specialInsDash').value!=""){
		openDriverCommentsDash();
		document.getElementById("specialInsDash").style.color='#000000';
	}if( document.getElementById('CommentsDash').value!=""){
		openDispatchCommentsDash();
		document.getElementById("CommentsDash").style.color='#000000';
	}
	if(document.getElementById('acctDash').value!=""){
		voucherDetails(2);
	}
	document.getElementById('nameDash').value=document.getElementById('name_'+row).value;
	document.getElementById('eMailDash').value=document.getElementById('eMail_'+row).value;
	var driverList=document.getElementById("drprofileSizeDash").value;
	var vehicleList=document.getElementById("vprofileSizeDash").value;
	var drFlag=document.getElementById('FlagCustomer'+row).value;
	if(drFlag!=""){
		for (var i=0; i<driverList;i++){
			if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
				document.getElementById('dchk'+i).checked=true;
			}
		}
		for (var i=0;i<vehicleList;i++){
			if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
				document.getElementById('vchk'+i).checked=true;
			}
		}
		openSplReqDash();
	}	

}
function appendAddress(row,row_id)
{ 
	$('#ORDash').jqm();
	$("#ORDash").jqmShow();
	document.getElementById('nameDash').focus();
	if(row_id == 1) {
		if(document.getElementById("addressEnter").value==1 || document.getElementById("addressEnter").value==''){
			document.getElementById('sadd1Dash').value = document.getElementById('sadd1_'+row).value;
			document.getElementById('sadd2Dash').value = document.getElementById('sadd2_'+row).value;
			document.getElementById('scityDash').value = document.getElementById('scity'+row).value;
			document.getElementById('sstateDash').value = document.getElementById('sstate'+row).value;
			document.getElementById('szipDash').value = document.getElementById('szip'+row).value;
			document.getElementById('sLatitudeDash').value = document.getElementById('sLatitude'+row).value;
			document.getElementById('sLongitudeDash').value = document.getElementById('sLongitude'+row).value;
			document.getElementById('addressKey').value = document.getElementById('addressKey_'+row).value;
		} else {
			document.getElementById('eadd1Dash').value=document.getElementById('sadd1_'+row).value;
			document.getElementById('eadd2Dash').value=document.getElementById('sadd2_'+row).value;
			document.getElementById('ecityDash').value=document.getElementById('scity'+row).value;
			document.getElementById('estateDash').value=document.getElementById('sstate'+row).value;
			document.getElementById('ezipDash').value=document.getElementById('szip'+row).value;
			document.getElementById('eLatitude').value=document.getElementById('sLatitude'+row).value;
			document.getElementById('eLongitude').value=document.getElementById('sLongitude'+row).value;
			document.getElementById('addressKeyTo').value = document.getElementById('addressKey_'+row).value;
		}
		document.getElementById('phoneDash').value=document.getElementById('phoneAddress'+row).value;
		document.getElementById('eMailDash').value=document.getElementById('eMail_'+row).value;
		document.getElementById('masterAddress').value = document.getElementById('masterAddressKey_'+row).value;
		if(document.getElementById('specialInsDash1').value==""){
			document.getElementById("specialInsDash1").value=document.getElementById('driverComments'+row).value;
		}if( document.getElementById('CommentsDash1').value==""){
			document.getElementById("CommentsDash1").value=document.getElementById('dispatchComments'+row).value;
		}
		
		//landmark
		var element_lm_name = document.getElementById('nameLM_'+row);
		var element_lm_key = document.getElementById('Key'+row);
		if(element_lm_name!=null && element_lm_key!=null){
			document.getElementById('landMarkDash').value = element_lm_name.value;
			document.getElementById('LMKeyDash').value = element_lm_key.value;
		}
		
//		document.getElementById('specialInsDash').value = document.getElementById('specialInsDash').value+" "+document.getElementById('driverComments'+row).value;
//		document.getElementById('CommentsDash').value = document.getElementById('CommentsDash').value+" "+document.getElementById('dispatchComments'+row).value;
//		if(document.getElementById('acctDash').value!=""){
//		voucherDetails(2);
//		}
//		document.getElementById('specialInsDash1').value= "Temporary Comments";
//		document.getElementById('CommentsDash1').value = "Temporary Comments";
//		document.getElementById("specialInsDash1").style.color='gray';
//		document.getElementById("CommentsDash1").style.color='gray';
//		if(document.getElementById('specialInsDash').value!=""){
//		document.getElementById("specialInsDash").style.color='#000000';
//		}if( document.getElementById('CommentsDash').value!=""){
//		document.getElementById("CommentsDash").style.color='#000000';
//		}
		if(document.getElementById('premiumCustomerDash'+row).value==1){
			document.getElementById('premiumCustomerDash').checked=true;
		}
	} else if(row_id == 2){
		if(isNaN(document.getElementById('phoneReq'+row).value)){
			document.getElementById('phoneDash').value="";
		} else {
			document.getElementById('phoneDash').value=document.getElementById('phoneReq'+row).value;
		}

		document.getElementById('nameDash').value=document.getElementById('nameReq_'+row).value;
		document.getElementById('eMailDash').value=document.getElementById('eMailReq_'+row).value;
		document.getElementById('sadd1Dash').value = document.getElementById('sadd1Req_'+row).value;
		document.getElementById('sadd2Dash').value = document.getElementById('sadd2Req_'+row).value;
		document.getElementById('scityDash').value = document.getElementById('scityReq'+row).value;
		document.getElementById('sstateDash').value = document.getElementById('sstateReq'+row).value;
		document.getElementById('szipDash').value = document.getElementById('szipReq'+row).value;
		document.getElementById('sLatitudeDash').value = document.getElementById('sLatitudeReq'+row).value;
		document.getElementById('sLongitudeDash').value = document.getElementById('sLongitudeReq'+row).value;
		document.getElementById('tripId').value = document.getElementById('tripId'+row).value;
		document.getElementById('jobRating').value = document.getElementById('jobRateReq'+row).value;
		document.getElementById('specialInsDash1').value= document.getElementById('driverCommentsReq'+row).value;
		document.getElementById('CommentsDash1').value = document.getElementById('dispatchCommentsReq'+row).value;
		if(document.getElementById("driverORcabDash").value=="Driver"){
			document.getElementById("driverDashOR").value = document.getElementById('driver'+row).value;
			document.getElementById('DriverCabSwitch').value=document.getElementById('VehicleIdReq'+row).value;
		} else {
			document.getElementById("driverDashOR").value = document.getElementById('VehicleIdReq'+row).value;
			document.getElementById('DriverCabSwitch').value=document.getElementById('driver'+row).value;
		}
		document.getElementById('sdate').value = document.getElementById('dateReq'+row).value;
		document.getElementById("timeTemporary").value=document.getElementById('timeReq'+row).value;
		if(document.getElementById("timeType").value=="1"){
			document.getElementById("shrs").value = document.getElementById('timeReq'+row).value;
		} else {
			document.getElementById("shrs").value = "NCH";
		}
		document.getElementById('eadd1Dash').value=document.getElementById('prevEAdd1'+row).value;
		document.getElementById('eadd2Dash').value=document.getElementById('prevEAdd2'+row).value;
		document.getElementById('ecityDash').value=document.getElementById('prevECity'+row).value;
		document.getElementById('estateDash').value=document.getElementById('prevEState'+row).value;
		document.getElementById('ezipDash').value=document.getElementById('prevEZip'+row).value;
		document.getElementById('eLatitude').value=document.getElementById('prevELatitude'+row).value;
		document.getElementById('eLongitude').value=document.getElementById('prevELongitude'+row).value;
		document.getElementById("queuenoDash").value=document.getElementById('Zone'+row).value;
		document.getElementById('landMarkDash').value=document.getElementById('LandMark'+row).value;
		document.getElementById('toLandMarkDash').value=document.getElementById('ELandMark'+row).value;
		document.getElementById('repeatGroupDash').value=document.getElementById('repeatGroup'+row).value;
		document.getElementById('numberOfPassengerDash').value=document.getElementById('numOfPassDash'+row).value;
		document.getElementById('advanceTime').value=document.getElementById('advTimeDash'+row).value;
		document.getElementById('tripStatus').value=document.getElementById("tripStatusDash"+row).value;

		if(document.getElementById('premiumCustomerReq'+row).value==1){
			document.getElementById('premiumCustomerDash').checked=true;
		}
		checkPaysTrip(document.getElementById('tripId'+row).value);
		var driverList=document.getElementById("drprofileSizeDash").value;
		var vehicleList=document.getElementById("vprofileSizeDash").value;
		if(document.getElementById('sharedRide'+row).value==1){
			document.getElementById("sharedRideDash").checked=true;
		}
		var drFlag=document.getElementById('Flag'+row).value;
		if(drFlag!=""){
			for (var i=0; i<driverList;i++){
				if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
					document.getElementById('dchk'+i).checked=true;
				}
			}
			for (var i=0;i<vehicleList;i++){
				if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
					document.getElementById('vchk'+i).checked=true;
				}
			}
			openSplReqDash();
		}	
		if(document.getElementById('acctDash').value!=""){
			voucherDetails(2);
		}
		document.getElementById('specialInsDash').value= "Permanent Comments";
		document.getElementById('CommentsDash').value = "Permanent Comments";
		document.getElementById("specialInsDash").style.color='gray';
		document.getElementById("CommentsDash").style.color='gray';
		if(document.getElementById('specialInsDash1').value!=""){
			document.getElementById("specialInsDash1").style.color='#000000';
		}if( document.getElementById('CommentsDash1').value!=""){
			document.getElementById("CommentsDash1").style.color='#000000';
		}
		document.getElementById("fleetForOR").value=document.getElementById("fleetCodeReq"+row).value;
	} else if(row_id == 3){
		if(isNaN(document.getElementById('phoneLM'+row).value)){
			document.getElementById('phoneDash').value="";
		} else {
			document.getElementById('phoneDash').value=document.getElementById('phoneLM'+row).value;
		}
		document.getElementById('sadd1Dash').value = document.getElementById('sadd1LM_'+row).value;
		document.getElementById('sadd2Dash').value = document.getElementById('sadd2LM_'+row).value;
		document.getElementById('scityDash').value = document.getElementById('scityLM'+row).value;
		document.getElementById('sstateDash').value = document.getElementById('sstateLM'+row).value;
		document.getElementById('szipDash').value = document.getElementById('szipLM'+row).value;
		document.getElementById('sLatitudeDash').value = document.getElementById('sLatitudeLM'+row).value;
		document.getElementById('sLongitudeDash').value = document.getElementById('sLongitudeLM'+row).value;
		document.getElementById('landMarkDash').value = document.getElementById('nameLM_'+row).value;
		if((document.getElementById('specialInsDash1').value).indexOf(document.getElementById('commentsLM'+row).value)<=0){
			if(document.getElementById("specialInsDash1").value!="Temporary Comments" && document.getElementById("specialInsDash1").value!="Temporary Comments "){
				document.getElementById("specialInsDash1").value=document.getElementById("specialInsDash1").value+" "+document.getElementById('commentsLM'+row).value;
			} else {
				document.getElementById("specialInsDash1").value=document.getElementById('commentsLM'+row).value;
			}
		}
		if(document.getElementById('specialInsDash1').value!=""){
			var finalComments = document.getElementById('specialInsDash1').value;
			if(finalComments!="" && finalComments.indexOf("P/U")<0 && finalComments.indexOf("D/O")<0){
				document.getElementById('specialInsDash1').value = "P/U:"+document.getElementById('specialInsDash1').value;
			}
			document.getElementById("specialInsDash1").style.color='#000000';
		}
		document.getElementById('LMKeyDash').value = document.getElementById('Key'+row).value;
	}
	if((row_id.value == 4) || (row_id == 4)) {
		if(document.getElementById("addressEnter").value==1 || document.getElementById("addressEnter").value==''){
			document.getElementById('sadd1Dash').value = document.getElementById('sadd1PLM_'+row).value;
			document.getElementById('sadd2Dash').value = document.getElementById('sadd2PLM_'+row).value;
			document.getElementById('scityDash').value = document.getElementById('scityPLM'+row).value;
			document.getElementById('sstateDash').value = document.getElementById('sstatePLM'+row).value;
			document.getElementById('szipDash').value = document.getElementById('szipPLM'+row).value;
			document.getElementById('sLatitudeDash').value = document.getElementById('sLatitudePLM'+row).value;
			document.getElementById('sLongitudeDash').value = document.getElementById('sLongitudePLM'+row).value;
			if((document.getElementById('specialInsDash1').value).indexOf(document.getElementById('commentsPLM'+row).value)<=0){
				if(document.getElementById("specialInsDash1").value!="Temporary Comments" && document.getElementById("specialInsDash1").value!="Temporary Comments "){
					document.getElementById("specialInsDash1").value=document.getElementById("specialInsDash1").value+" "+document.getElementById('commentsPLM'+row).value;
				} else {
					document.getElementById("specialInsDash1").value=document.getElementById('commentsPLM'+row).value;
				}
			}
			if(document.getElementById('specialInsDash1').value!=""){
				var finalComments = document.getElementById('specialInsDash1').value;
				if(finalComments!="" && finalComments.indexOf("P/U")<0 && finalComments.indexOf("D/O")<0){
					document.getElementById('specialInsDash1').value = "P/U:"+document.getElementById('specialInsDash1').value;
				}
				document.getElementById("specialInsDash1").style.color='#000000';
			}
			document.getElementById('landMarkDash').value=document.getElementById('PLM'+row).value;
		} else {
			document.getElementById('eadd1Dash').value=document.getElementById('sadd1PLM_'+row).value;
			document.getElementById('eadd2Dash').value=document.getElementById('sadd2PLM_'+row).value;
			document.getElementById('ecityDash').value=document.getElementById('scityPLM'+row).value;
			document.getElementById('estateDash').value=document.getElementById('sstatePLM'+row).value;
			document.getElementById('ezipDash').value=document.getElementById('szipPLM'+row).value;
			document.getElementById('eLatitude').value=document.getElementById('sLatitudePLM'+row).value;
			document.getElementById('eLongitude').value=document.getElementById('sLongitudePLM'+row).value;
			document.getElementById('toLandMarkDash').value=document.getElementById('PLM'+row).value;
		}
		document.getElementById('LMKeyDash').value=document.getElementById('LMKey'+row).value;
		document.getElementById('addressKey').value ="000";
	}
}
function showProvider(){
	document.getElementById('phoneProvider').style.display='block';
}
function enterEmail(){
	if(document.getElementById("phoneDash").value=="" || document.getElementById("providerValue").value==""){
		document.getElementById("providerValue").value="";
		alert("Enter any phone number and select provider");
	} else {
		document.getElementById('eMailDash').value=document.getElementById("phoneDash").value+document.getElementById("providerValue").value;
		document.getElementById("providerValue").value="";
	}
}
function clearOR(){
	document.getElementById('phoneDash').value="";
	document.getElementById('nameDash').value="";
	document.getElementById('eMailDash').value="";
	document.getElementById('dispatchStatusDash').checked=false;
	document.getElementById('sdate').value="";
	document.getElementById('advanceTime').value="-1";
	document.getElementById('payTypeDash').value="Cash";
	document.getElementById('amtDash').value="";
	document.getElementById('masterAddress').value = "";
	document.getElementById('sadd1Dash').value = "";
	document.getElementById('sadd2Dash').value = "";
	document.getElementById('scityDash').value = "";
	document.getElementById('szipDash').value = "";
	document.getElementById('sLatitudeDash').value = "";
	document.getElementById('sLongitudeDash').value = "";
	document.getElementById('tripId').value = "";
	document.getElementById('specialInsDash').value = "";
	document.getElementById('CommentsDash').value = "Permanent Comments";
	document.getElementById('CommentsDash1').value="Temporary Comments";
	document.getElementById('accountSuccess').value = "";
	document.getElementById("driverDashOR").value = "";
	document.getElementById('DriverCabSwitch').value="";
	document.getElementById('eadd1Dash').value= "";
	document.getElementById('eadd2Dash').value= "";
	document.getElementById('ecityDash').value= "";
	document.getElementById('ezipDash').value= "";
	document.getElementById('eLatitude').value= "";
	document.getElementById('eLongitude').value= "";
	document.getElementById('fromDateDash').value= "";
	document.getElementById('toDateDash').value= "";
	document.getElementById('landMarkDash').value= "";
	document.getElementById('addressKey').value="";
	document.getElementById('addressKeyTo').value ="";
	document.getElementById('addressDash').value="";
	document.getElementById('DropAddressDash').value="";
	document.getElementById('landMarkDash').value="";
	document.getElementById('toLandMarkDash').value="";
	document.getElementById('acctDash').value="";
	document.getElementById("hideTableDash").style.display = "none";
	document.getElementById("hideJobsOnDays").style.display = "none";
	document.getElementById("hideTableDriverComments").style.display = "none";
	document.getElementById("hideTableComments").style.display = "none";
	document.getElementById("hideTableCalledBy").style.display="none";
	document.getElementById("callerPhone").value="";
	document.getElementById("callerName").value="";
	document.getElementById("repeatGroupDash").value='';
	document.getElementById("multiJobUpdateDash").value='';
	document.getElementById("roundTrip").checked=false;
	document.getElementById("noShow").checked=false;
	document.getElementById("sharedRideDash").checked=false;
	document.getElementById("driverDashOR").value='';
	document.getElementById("LMKeyDash").value='';
	document.getElementById("phoneVerificationDash").value="";
	document.getElementById('numberOfPassengerDash').value='';
	document.getElementById("numOfCabs").value="1";
	document.getElementById("queuenoDash").value='';
	document.getElementById("addressEnter").value='1';
	document.getElementById("specialInsDash1").value="Temporary Comments";
	document.getElementById("specialInsDash").value="Permanent Comments";
	document.getElementById('CommentsDash').value="";
	document.getElementById('CommentsDash1').value="";
	document.getElementById('phoneProvider').style.display='none';
	document.getElementById('tripStatus').value="";
	document.getElementById("updateAll").value="";
	document.getElementById("startAmtVoucher").value=document.getElementById("startAmtVoucher1").value;
	document.getElementById("rpmVoucher").value="";
	document.getElementById("jobRating").value="1";
	document.getElementById("totalAddress").value="";
	document.getElementById("dontAddPickUp").value="";
	document.getElementById("dontAddDropOff").value="";
	document.getElementById("roundTripExist").value="";
	document.getElementById("airName").value="";
	document.getElementById("airNo").value="";
	document.getElementById("airFrom").value="";
	document.getElementById("airTo").value="";
	
	document.getElementById('refNum').value="";
	document.getElementById('refNum1').value="";
	document.getElementById('refNum2').value="";
	document.getElementById('refNum3').value="";

	if(document.getElementById('defaultMeterDash').value!=""){
		document.getElementById('meter').value=document.getElementById('defaultMeterDash').value;
	}
	clearChargesDash();
	$('#dateTime').hide("slow");
	$('#ORDashLandMark').hide('slow');
	$('#Error').hide('slow');
	document.getElementById('premiumCustomerDash').checked=false;
	document.getElementById('Sun').checked=false;
	document.getElementById('Mon').checked=false;
	document.getElementById('Tue').checked=false;
	document.getElementById('Wed').checked=false;
	document.getElementById('Thu').checked=false;
	document.getElementById('Fri').checked=false;
	document.getElementById('Sat').checked=false;
	document.getElementById('newLandMark').checked=false;
	document.getElementById('newToLandMark').checked=false;
	if(document.getElementById("timeType").value==1){
		loadDate();
	} else {
		document.getElementById('shrs').value='Now';
		var currentTime = new Date();
		var month = currentTime.getMonth() + 1;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(month<10){
			month = "0"+month;
		}
		if(day<10){
			day = "0"+day;
		}
		var date= month+ "/" + day + "/" +year ;
		document.getElementById('sdate').value=date;
	}
	document.getElementById("specialInsDash").style.color='gray';
	document.getElementById("specialInsDash1").style.color='gray';
	document.getElementById('CommentsDash').style.color='gray';
	document.getElementById('CommentsDash1').style.color='gray';
	var driverList=document.getElementById("drprofileSizeDash");
	var vehicleList=document.getElementById("vprofileSizeDash");
	for (var i=0; i<driverList.value;i++){
		document.getElementById('dchk'+i).checked=false;
	}
	for (var i=0; i<vehicleList.value;i++){
		document.getElementById('vchk'+i).checked=false;
	}
	if($("#fleetDiv").is(':visible')){
		if(document.getElementById("changedFleet").value==""){
			document.getElementById("fleetForOR").value=document.getElementById("assoccode").value;
		} else {
			document.getElementById("fleetForOR").value=document.getElementById("changedFleet").value;
		}
	} else {
		document.getElementById("fleetForOR").value="";
	}
}
function showCIHistory(){
	document.getElementById("callerIdHistory").innerHTML ="";
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'AjaxClass?event=ciHistory';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var obj = "{\"ciHistory\":"+text+"}";
	jsonObj = JSON.parse(obj.toString());
	var $tbl = $('<table style="width:100%">').attr('id', 'ciHistoryTable');
	$tbl.append($('<tr style="background-color:#C0C0C0">').append(
			$('<td colspan="5" align="center" style="font-weight:bolder;">').text("Caller Id History"),
			$('<input />', { type: 'image', id: 'closeCI',name:'closeCI', src: "images/Dashboard/close.png",onClick:'closeCiHistory()'})
	));
	$tbl.append($('<tr style="background-color:#C0C0C0">').append(
			$('<th style="width:20%">').text("Phone"),
			$('<th style="width:20%">').text("Name"),
			$('<th style="width:20%">').text("Line"),
			$('<th style="width:20%">').text("Date"),	
			$('<th style="width:20%">').text("Time")));
	if(jsonObj.ciHistory.length>0){
		for(var i=0;i<jsonObj.ciHistory.length;i++){
			var colorPattern="";
			if(i%2==0){
				colorPattern="style=\"background-color:#FBB917\"";
			} else {
				colorPattern="style=\"background-color:#A0CFEC\"";
			}
			$tbl.append($('<tr '+colorPattern+'>').append(
					$('<td  align="center" style="width:20%">').text(jsonObj.ciHistory[i].PhN),
					$('<td  align="center" style="width:20%">').text(jsonObj.ciHistory[i].PaN),
					$('<td  align="center" style="width:20%">').text(jsonObj.ciHistory[i].L),
					$('<td  align="center" style="width:20%">').text(jsonObj.ciHistory[i].D),
					$('<td  align="center" style="width:20%">').text(jsonObj.ciHistory[i].T)));
		}
	} else {
		$tbl.append($('<tr>').append(
				$('<td colspan="5" align="center" style="font-weight:bolder;">').text("No Calls In Past 24 Hours")));
	}
	$('#callerIdHistory').append($tbl);
	document.getElementById("callerIdHistory").style.display="block";
}
function closeCiHistory(){
	document.getElementById("callerIdHistory").style.display="none";
}
function openDetailsDash(lineNumber,type) {
	var line = document.getElementById("lineNew"+lineNumber).value;
//	if(type==1){
//		line=document.getElementById("callerIdButtons"+lineNumber).value;
//	}if(type==9){
//		line=document.getElementById("callerIdButtonPicked"+lineNumber).value;
//	} else {
//		line=document.getElementById("callerIdButton"+lineNumber).value;
//	}
	var xmlhttp=null;	   
	if (window.XMLHttpRequest){	   
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}	
	var url = 'OpenRequestAjax?event=acceptCall&lineNumber='+line+"&phNum="+document.getElementById("phoneCI"+lineNumber).value;
	xmlhttp.open("GET",url,false);
	xmlhttp.send(null);
	var text=xmlhttp.responseText;
	if(text=="notUpdate"){
		return;
	}
//	var flag="0";
//	if(document.getElementById("button_"+lineNumber).style.display!="block"){
//		document.getElementById("buttonPicked_"+lineNumber).style.display="none";
//		flag=1;
//	}else{
//		document.getElementById("button_"+lineNumber).style.display="none";
//	}
//	document.getElementById("buttons_"+lineNumber).style.display="block";
//	if(flag=="1"){
//		document.getElementById("callerIdButtons"+lineNumber).value=document.getElementById("callerIdButton"+lineNumber).value;
//	}else{
//		document.getElementById("callerIdButtons"+lineNumber).value=document.getElementById("callerIdButtonPicked"+lineNumber).value;
//	}
	clearOR();
	if(isNaN(document.getElementById("phoneCI"+lineNumber).value)){
		document.getElementById('phoneDash').value="";
	} else {
		document.getElementById('phoneDash').value=document.getElementById("phoneCI"+lineNumber).value;
	}
	document.getElementById('nameDash').value=document.getElementById("nameCI"+lineNumber).value;
	$('#ORDash').jqm();
	$("#ORDash").jqmShow();
	document.getElementById('nameDash').focus();
	$('#showHover').show('slow');
	$('#popUpAddress').show('slow');
	callerIdDash();
} 

function checkOldAddressDash(type){
	var totalSize=document.getElementById("totalAddress").value;
	var add1="";var latiMain=""; var longiMain = "";
	if(type==1){
		add1 = document.getElementById("sadd1Dash").value;
		latiMain= document.getElementById("sLatitudeDash").value;
		longiMain=document.getElementById("sLongitudeDash").value;
	} else {
		add1 = document.getElementById("eadd1Dash").value;
		latiMain= document.getElementById("eLatitude").value;
		longiMain=document.getElementById("eLongitude").value;
	}
	var latiToCheck=latiMain.split(".");
	latiToCheck=latiToCheck[0]+"."+latiToCheck[1].substring(0,4);
	var longiToCheck=longiMain.split("."); 
	longiToCheck=longiToCheck[0]+"."+longiToCheck[1].substring(0,4);
	for(var i=0;i<totalSize;i++){
		var existLati = (document.getElementById("sLatitude"+i).value).split(".");
		var existLongi= (document.getElementById("sLongitude"+i).value).split(".");
		if(add1==document.getElementById("sadd1_"+i).value || (latiToCheck==existLati[0]+"."+existLati[1].substring(0,4)) && (longiToCheck==existLongi[0]+"."+existLongi[1].substring(0,4))){
			if(type==1){
				document.getElementById("dontAddPickUp").value="1";
				break;
			} else {
				document.getElementById("dontAddDropOff").value="1";
				break;
			}
		}
	}
}


function getLandMark(type) {
	var  slandmark="";
	if(type==1 && document.getElementById("newLandMark").checked==false){
		slandmark = document.getElementById('landMarkDash').value;
	} else if(type==2 && document.getElementById("newToLandMark").checked==false) {
		slandmark = document.getElementById('toLandMarkDash').value;
	}
	if(slandmark!="")
	{
		var xmlhttp=null;	   
		if (window.XMLHttpRequest){	   
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}	
		var url = 'RegistrationAjax?event=getLandMarkDetails&module=dispatchView&landMark='+slandmark+'&Screen=Dash&type='+type;
		xmlhttp.open("GET",url,false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;
		var resp=text.split("###");
		document.getElementById("ORDashLandMark").innerHTML=resp[1];
		document.getElementById("ORDashLandMark").style.display="block";
		if(resp[0].indexOf('NumRows=2')<0){
			enterLM(0,type);
			document.getElementById('sadd1Dash').focus();
			document.getElementById("ORDashLandMark").style.display="none";
		} 
	} 
}
function enterLM(row,type){
	if(document.getElementById("LMSelect").value==1){
		document.getElementById('sadd1Dash').value = document.getElementById('ladd1'+row).value;
		document.getElementById('sadd2Dash').value = document.getElementById('ladd2'+row).value;
		document.getElementById('scityDash').value = document.getElementById('lcity'+row).value;
		document.getElementById('sstateDash').value = document.getElementById('lstate'+row).value;
		document.getElementById('szipDash').value = document.getElementById('lzip'+row).value;
		document.getElementById('sLatitudeDash').value = document.getElementById('llatide'+row).value;
		document.getElementById('sLongitudeDash').value = document.getElementById('llogtude'+row).value;
		document.getElementById('landMarkDash').value = document.getElementById('landmarkS'+row).value;
		document.getElementById('LMKeyDash').value = document.getElementById('landmarkKey'+row).value;
	} else {
		document.getElementById('eadd1Dash').value = document.getElementById('ladd1'+row).value;
		document.getElementById('eadd2Dash').value = document.getElementById('ladd2'+row).value;
		document.getElementById('ecityDash').value = document.getElementById('lcity'+row).value;
		document.getElementById('estateDash').value = document.getElementById('lstate'+row).value;
		document.getElementById('ezipDash').value = document.getElementById('lzip'+row).value;
		document.getElementById('eLatitude').value = document.getElementById('llatide'+row).value;
		document.getElementById('eLongitude').value = document.getElementById('llogtude'+row).value;
		document.getElementById('toLandMarkDash').value = document.getElementById('landmarkS'+row).value;
		document.getElementById('LMKeyDash').value = document.getElementById('landmarkKey'+row).value;
	}
	if((document.getElementById('specialInsDash1').value).indexOf(document.getElementById('lcomments'+row).value)<=0){
		if(document.getElementById("specialInsDash1").value!="Temporary Comments" && document.getElementById("specialInsDash1").value!="Temporary Comments "){
			document.getElementById("specialInsDash1").value=document.getElementById("specialInsDash1").value+" "+document.getElementById('lcomments'+row).value;
		} else {
			document.getElementById("specialInsDash1").value=document.getElementById('lcomments'+row).value;
		}
	}	
	if(document.getElementById("specialInsDash1").value!=""){
		if(type==1){
			var finalComments = document.getElementById('specialInsDash1').value;
			if(finalComments!="" && finalComments.indexOf("P/U")<0 && finalComments.indexOf("D/O")<0){
				document.getElementById('specialInsDash1').value = "P/U:"+document.getElementById('specialInsDash1').value;
			}
		} else {
			var finalComments = document.getElementById('specialInsDash1').value;
			if(finalComments!="" && finalComments.indexOf("P/U")<0 && finalComments.indexOf("D/O")<0){
				document.getElementById('specialInsDash1').value = "D/O:"+document.getElementById('specialInsDash1').value;
			}
		}
		document.getElementById("specialInsDash1").style.color='#000000';
	}
}
function getPreviousAddress(Status,lineNumber) {
	var phone = document.getElementById("phoneCI"+lineNumber).value;
	if (phone != "") {
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'AjaxClass?event=previousRequest&phone='+phone+'&lineNumber='+document.getElementById("lineNew"+lineNumber).value;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var resp=text.split("###");
		document.getElementById("ORDashAddress").innerHTML=resp[1];
		var showAddress=document.getElementById("ORDashAddress");
		showAddress.style.display="block";
		if(Status==0){
			var response=document.getElementById("acceptor"+lineNumber).value;
			var phoneNumber =response.split(";");
			document.getElementById("ph.Number").value=phoneNumber[0];
			document.getElementById("hoverValue").value=phoneNumber[1]+" Not attended ";
			$("#showHover").show("slow");
		}else if(Status==9){
			var response=document.getElementById("acceptorPicked"+lineNumber).value;
			var phoneNumber =response.split(";");
			document.getElementById("ph.Number").value=phoneNumber[0];
			document.getElementById("hoverValue").value=phoneNumber[1]+" Not attended ";
			$("#showHover").show("slow");
		}else {
			var response=document.getElementById("acceptors"+lineNumber).value;
			var phoneNumber =response.split(";");
			document.getElementById("ph.Number").value=phoneNumber[0];
			document.getElementById("hoverValue").value=phoneNumber[1]+" Attended by "+phoneNumber[4]+" at "+phoneNumber[3];
			$("#showHover").show("slow");
		}
	}
}  
function appendPhone(){
	$('#ORDash').jqm();
	$("#ORDash").jqmShow();
	if(isNaN(document.getElementById("ph.Number").value)){
		document.getElementById('phoneDash').value="";
	} else {
		document.getElementById('phoneDash').value=document.getElementById("ph.Number").value;
	}
}
function openDispatchCommentsDash()
{
	var ele = document.getElementById("hideTableComments");
	if(ele.style.display == "block") {
		ele.style.display = "none";
	}
	else {
		ele.style.display = "block";
	}
}
function openFlightInformationDash()
{
	var ele = document.getElementById("hideFlightInformation");
	if(ele.style.display == "block") {
		ele.style.display = "none";
	}
	else {
		ele.style.display = "block";
	}
} 

function openDriverCommentsDash()
{
	var ele = document.getElementById("hideTableDriverComments");
	if(ele.style.display == "block") {
		ele.style.display = "none";
	}
	else {
		ele.style.display = "block";
	}
} 	       
function openSplReqDash()
{
	var ele = document.getElementById("hideTableDash");
	if(ele.style.display == "block") {
		ele.style.display = "none";
	}
	else {
		ele.style.display = "block";
	}
} 
function addressEnter(){
	var arrAddress =document.getElementById("addressDash").value;

	$.each(arrAddress, function (i, address_component) {
		if (address_component.types[0] == "subpremise"){
			$("#sadd2Dash").val(address_component.long_name);
		}
		if (address_component.types[0] == "street_number"){
			console.log(i+": street_number:"+address_component.long_name);
			$("#sadd1Dash").val(address_component.long_name);
			streetnum = address_component.long_name;
		}

		if (address_component.types[0] == "locality"){
			console.log("town:"+address_component.long_name);
			$("#scityDash").val(address_component.long_name);
		}

		if (address_component.types[0] == "route"){ 
			route = address_component.long_name;
		}
		if (address_component.types[0] == "country"){ 
			console.log("country:"+address_component.long_name); 
			itemCountry = address_component.long_name;
		}
		if (address_component.types[0] == "postal_code"){ 
			console.log("pc:"+address_component.long_name);  
			itemPc = address_component.long_name;
			$("#szipDash").val(address_component.long_name);
		}
	});
}
function currentDate(){
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	if(month<10){
		month = "0"+month;
	}
	if(day<10){
		day = "0"+day;
	}
	var date= month+ "/" + day + "/" +year ;
	document.getElementById('sdate').value=date;
}
function currentTime(){
	var dTime = new Date();
	var hours = dTime.getHours();
	var minute = dTime.getMinutes();
	var min1;
	if(minute < 53 && hours <= 23) {
		var min2=(minute +7) - ((minute+7) % 15);
		min1= hours+""+min2;
	}else  if(minute > 53 && hours < 23) {
		min1= hours+1+""+"00";

	}
	else{ 
		min1="2345";
	}
	if(min1<999){
		min1 = "0"+""+ min1;
	}
	document.getElementById("shrs").value=min1;

}
function loadDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	document.getElementById("sdate").value = cMonth+"/"+cDate+"/"+cYear;
	document.getElementById("shrs").value  = 'Now'; 
}	
function clearField(){
	document.getElementById("addressDash").value = "";
	document.getElementById("DropAddressDash").value = "";
}
function changeDash(type){
	if(type==1){
		document.getElementById("sLatitudeDash").value='';
		document.getElementById("sLongitudeDash").value='';
		document.getElementById("addressKey").value="";
	} else if(type==2){
		document.getElementById("eLatitude").value='';
		document.getElementById("eLongitude").value='';
	}
}

var minutes,seconds,TimerRunningOR,TimerIDOR,TimerID1OR;
TimerRunningOR=false,j=1;

function timerStart()//call the Init function when u need to start the timer
{
	minutes=1;
	seconds=0;
	StopTimerPopUp();
}
function StopTimerPopUp()
{
	if(TimerRunningOR)
		clearTimeout(TimerIDOR);
	TimerRunningOR=false;
	seconds= document.getElementById('secPopUP').value;
	StartTimerPopUp();
}
function clearTime(){
	clearTimeout(TimerIDOR);
	TimerRunningOR=false;
	document.getElementById('secPopUP').value=15;
}
function StartTimerPopUp()
{
	TimerRunningOR=true;
	TimerIDOR=self.setTimeout("StartTimerPopUp()",1000);
	if(seconds==0)
	{
		if( $('#ORDash').is(':visible') ) {
			removeHover();
		} else {
			removeAddress();
		}	
	}
	seconds--;
}
function accountClearDash(){
	document.getElementById('accountSuccess').value = "";
}
function voucherDetails(type){
	var account;
	if(document.getElementById('acctDash').value!=""){
		if(type==1){
			var accountValue=document.getElementById("acctDash").value;
			var split=accountValue.split("(");
			account=split[0];
		} else {
			account=document.getElementById("acctDash").value;
		}
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'OpenRequestAjax?event=getComments&account='+account;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;		
		var jsonObj = "{\"address\":"+text+"}" ;
		var obj = JSON.parse(jsonObj.toString());
		for(var j=0;j<obj.address.length;j++){
			var description=obj.address[j].Description;
			var name=obj.address[j].Name;
			var companyName=obj.address[j].CN;
			var startAmount=obj.address[j].SA;
			var ratePerMile=obj.address[j].RPM;
			var operatorComments=obj.address[j].OC;
			var driverComments=obj.address[j].DC;
			var drFlag=obj.address[j].SF;
			var driverList=document.getElementById("drprofileSizeDash").value;
			var vehicleList=document.getElementById("vprofileSizeDash").value;
			if(obj.address[j].FixedRate!=""){
				rate=obj.address[j].FixedRate+".00";
			}
			if(description=="Failed" || name=="Failed")
			{
				document.getElementById('Error').innerHTML = "Voucher Expired";
				document.getElementById('Error').style.display = 'block';
				document.getElementById("acctDash").value="";
				document.getElementById('payTypeDash').value="Cash";
				return true;
			} else {
				document.getElementById('Error').innerHTML = description+" ("+companyName+")";
				document.getElementById('Error').style.display = 'block';
				document.getElementById('payTypeDash').value="VC";
				document.getElementById('startAmtVoucher').value=startAmount;
				document.getElementById('rpmVoucher').value=ratePerMile;
				if(operatorComments!=""){
					openDispatchCommentsDash();
					document.getElementById("CommentsDash1").style.color="#000000";
					document.getElementById("CommentsDash1").value=operatorComments;
				}if(driverComments!=""){
					openDriverCommentsDash();
					document.getElementById("specialInsDash1").style.color="#000000";
					if((document.getElementById('specialInsDash1').value).indexOf(driverComments)<=0){
						if(document.getElementById("specialInsDash1").value!="Temporary Comments" && document.getElementById("specialInsDash1").value!="Temporary Comments "){
							document.getElementById("specialInsDash1").value=document.getElementById("specialInsDash1").value+" "+driverComments;
						} else {
							document.getElementById("specialInsDash1").value=driverComments;
						}
					}	
				}
				if(drFlag!=""){
					for (var i=0; i<driverList;i++){
						if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
							document.getElementById('dchk'+i).checked=true;
						}
					}
					for (var i=0;i<vehicleList;i++){
						if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
							document.getElementById('vchk'+i).checked=true;
						}
					}
					openSplReqDash();
				}	
				document.getElementById("accountSuccess").value="1";
				chargesDash(account);
				return true;
			}
		}
	}
}
function distanceDash(){
	var origin= document.getElementById("sLatitudeDash").value+","+document.getElementById("sLongitudeDash").value;
	var destination= document.getElementById("eLatitude").value+","+document.getElementById("eLongitude").value;
	if(document.getElementById("sLatitudeDash").value!="" && document.getElementById("eLatitude").value!=""){
		var amount=document.getElementById("tripAmount").value;
		var startAmt=document.getElementById("startAmtVoucher").value;
		var currencyPrefix=document.getElementById("currencyDash").value;
		 var dis1=document.getElementById("disN1").value;
		 var dis2=document.getElementById("disN2").value;
		 var dis3=document.getElementById("disN3").value;
		 var rate1=document.getElementById("rateN1").value;
		 var rate2=document.getElementById("rateN2").value;
		 var rate3=document.getElementById("rateN3").value;
		var extraData="";
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		var url = 'OpenRequestAjax?event=getDistFMQA&from='+origin+'&to='+destination;
		//var url = 'https://open.mapquestapi.com/directions/v1/route?key=ZX3FD32ZdJ8LfqzB2cKPMD6WDiuSbwTE&outFormat=json&routeType=shortest&timeType=1&enhancedNarrative=false&locale=en_US&unit=m&from='+origin+'&to='+destination+'&drivingStyle=2&highwayEfficiency=21.0';
		//Fmjtd%7Cluub2qub21%2C8l%3Do5-961gg4
		
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text=="" || text == "GRACIERROR"){
			$('#Error').show('slow');
			$('#Error').html( "Problem on getting distance" );
		}else if(text=="AUTH_FAILED"){
			$('#Error').show('slow');
			$('#Error').html( "Authentication failed for getting distance" );
		}else{
			var condition=text.split('"distance":');
			var finalDistance=condition[1].split(',');
			var distance=Math.round((finalDistance[0])*100)/100;
			if(document.getElementById("startAmtVoucher").value!=""){
				extraData=" (Start Amount "+currencyPrefix+document.getElementById("startAmtVoucher").value+")";
			} if(document.getElementById("rpmVoucher").value!=""){
				amount=Number(document.getElementById("rpmVoucher").value);
			}
			 var tripAmount="";
			 var disCalculated=false;
			 if(dis1!="0" && dis1!=""){
				amount=distance*rate1;
				disCalculated=true;
			 }
			if(distance-dis2>0 && dis2!="0" && dis2!=""){
				amount=amount-((distance-dis2)*rate2);
				disCalculated=true;
			}
			if(distance-dis3>0 && dis3!="0" && dis3!=""){
				amount=amount-((distance-dis3)*rate3);
				disCalculated=true;
			}
			if(disCalculated){
				tripAmount=Math.round((amount+Number(startAmt))*100)/100;
			} else {
				tripAmount=Math.round(((distance*amount)+Number(startAmt))*100)/100;
			}
			var newAmt=tripAmount.toString().split(".");
			var finalNum=Number(newAmt[1])%25;
			if(finalNum < 13 && finalNum!=0){
				finalNum=Math.floor(Number(newAmt[1])/25)*25;
			} else {
				finalNum=Math.ceil(Number(newAmt[1])/25)*25;
			}
			if(finalNum==100){
				tripAmount=Number(newAmt[0])+1+".00";
			} else {
				tripAmount=newAmt[0]+"."+finalNum;
			}
			amount=Math.round(amount*100)/100;
			var result=distance+" Miles & Approximate Amount "+currencyPrefix+tripAmount+" ("+currencyPrefix+amount+"/Mile)"+extraData;
			$('#Error').show('slow');
			$('#Error').html( result );
			if(document.getElementById("fareByDistanceDash").value=="1"){
				document.getElementById('amtDash').value=tripAmount;
				changeCharges();
			}
		}
	} else {
		$('#Error').show('slow');
		$('#Error').html( "Both Pickup and Dropoff Address Are Mandatory" );
	}
}
function getZoneChargesDash(){
	var sLat=document.getElementById("sLatitudeDash").value;
	var sLon=document.getElementById("sLongitudeDash").value;
	var eLat=document.getElementById("eLatitude").value;
	var eLon=document.getElementById("eLongitude").value;
	if(sLat!="" && sLon!="" && eLat!="" && eLon!=""){
		var url = 'SystemSetupAjax?event=getZoneCharges&sLat='+sLat+'&sLon='+sLon+'&eLat='+eLat+'&eLon='+eLon;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var jsonObj = "{\"address\":"+text+"}" ;
		var obj = JSON.parse(jsonObj.toString());
		if(obj.address.length>0){
			for(var j=0;j<obj.address.length;j++){
				var chargeForZone=obj.address[j].AM;
				$('#Error').show('slow');
				$('#Error').html("Charge From Zone "+obj.address[j].SZ+" To "+obj.address[j].EZ+" Is $"+chargeForZone);
				document.getElementById('amtDash').value=chargeForZone;
				changeCharges();
			}
		}
	} else {
		$('#Error').show('slow');
		$('#Error').html( "Both Pickup and Dropoff Address Are Mandatory" );
	}
}
function chargesDash(voucherId){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getVoucherCharges&account='+voucherId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	if(obj.address.length>0){
		clearChargesDash();
	}
	for(var j=0;j<obj.address.length;j++){
		calChargesDash();
		var type=obj.address[j].CK;
		var amount=obj.address[j].CA;
		var i=j+1;
		document.getElementById("payType"+i).value=type;
		document.getElementById("amountDetail"+i).value=amount;
//		checkPercentagesDash(i);
		var firstValue=document.getElementById('amtDash').value;
		if(firstValue==""){
			firstValue="0";
		}
		document.getElementById('amtDash').value=parseFloat(firstValue)+parseFloat(amount);
	}
}
function clearChargesDash(){
	var table = document.getElementById("chargesTable");
	var rowCount = table.rows.length;
	var size=document.getElementById('fieldSize').value;
	for(var i=0;i<Number(size);i++){
		rowCount--;
		table.deleteRow(rowCount);
	}
	document.getElementById("fieldSize").value="";
	document.getElementById('amtDash').value="";
}
function checkPaysTrip(tripId){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getCharges&tripId='+tripId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	for(var j=0;j<obj.address.length;j++){
		calChargesDash();
		var type=obj.address[j].T;
		var amount=obj.address[j].A;
		var i=j+1;
		document.getElementById("payType"+i).value=type;
		document.getElementById("amountDetail"+i).value=amount;
		callAmountDash();//checkPercentagesDash(i);
	}
}
function clearPUAddress(){
	document.getElementById('sadd1Dash').value = "";
	document.getElementById('sadd2Dash').value = "";
	document.getElementById('scityDash').value = "";
	document.getElementById('szipDash').value = "";
	document.getElementById('sLatitudeDash').value = "";
	document.getElementById('sLongitudeDash').value = "";
	document.getElementById("LMKeyDash").value='';
	document.getElementById('landMarkDash').value="";
	document.getElementById("queuenoDash").value='';
	document.getElementById('addressKey').value="";
	document.getElementById("addressEnter").value='1';
	document.getElementById("dontAddPickUp").value="";
}
function clearDOAddress(){
	document.getElementById('eadd1Dash').value= "";
	document.getElementById('eadd2Dash').value= "";
	document.getElementById('ecityDash').value= "";
	document.getElementById('ezipDash').value= "";
	document.getElementById('eLatitude').value= "";
	document.getElementById('eLongitude').value= "";
	document.getElementById("LMKeyDash").value='';
	document.getElementById('toLandMarkDash').value="";
	document.getElementById('addressKeyTo').value ="";
	document.getElementById("addressEnter").value='1';
	document.getElementById("dontAddDropOff").value="";
}
function voucherChange(type){
	if(type==1 && document.getElementById("payTypeDash").value!="VC"){
		document.getElementById("acctDash").value="";
		$('#Error').hide('slow');
	} else if(type==2 && document.getElementById("acctDash").value==""){
		document.getElementById("payTypeDash").value="Cash";
		$('#Error').hide('slow');
	}
}
function callAmountDash(){
	var size=document.getElementById('fieldSize').value;
	document.getElementById('amtDash').value="";
	for(var j=0;j<size;j++){
		var i=j+1;
		var firstValue=document.getElementById('amtDash').value;
		var newValue=document.getElementById("amountDetail"+i).value;
		if(firstValue==""){
			firstValue="0";
		}
		if(isNaN(newValue)){
			alert("Enter numeric value");
			document.getElementById("amountDetail"+i).value="";
		}
		document.getElementById('amtDash').value=parseFloat(firstValue)+parseFloat(newValue);
		document.getElementById('amtDash').value=Math.round((document.getElementById('amtDash').value)*100)/100;
	}
}
function changeCharges(){
	var table = document.getElementById("chargesTable");
	var rowCount = table.rows.length;
	var size=document.getElementById('fieldSize').value;
	for(var i=0;i<Number(size);i++){
		rowCount--;
		table.deleteRow(rowCount);
	}
	document.getElementById("fieldSize").value="";
	calChargesDash();
	document.getElementById("payType"+1).value="1";
	document.getElementById("amountDetail"+1).value=document.getElementById('amtDash').value;
//	checkPercentagesDash(1);
}
function doConfirm(msg,yesFn, noFn)
{
	var confirmBox = $("#confirmBox");
	confirmBox.find(".message").text(msg);
	confirmBox.find(".yes,.no").unbind().click(function()
			{
		confirmBox.hide();
			});
	confirmBox.find(".yes").click(yesFn);
	confirmBox.find(".no").click(noFn);
	confirmBox.show();
}
function doConfirmJobs(msg,yesFn, noFn)
{
	var confirmBox = $("#confirmBoxJobs");
	confirmBox.find(".message").text(msg);
	confirmBox.find(".yes,.no").unbind().click(function()
			{
		confirmBox.hide();
			});
	confirmBox.find(".yes").click(yesFn);
	confirmBox.find(".no").click(noFn);
	confirmBox.show();
}
function driverAllocationCheckDash(){
	
	if(document.getElementById('tripStatus').value !="" && ((document.getElementById('tripStatus').value>=40 && document.getElementById('tripStatus').value<50)||document.getElementById('tripStatus').value==37||document.getElementById('tripStatus').value==90) && document.getElementById('tripId').value!=""){
		doConfirm("This an allocated job. Do you want to clear the Driver/Cab information?", function yes()
		{
			document.getElementById('updateAll').value=1;
			dateValidationDash();
		}, function no()
		{
			document.getElementById('updateAll').value=0;
			dateValidationDash();
		});
	} else {
		dateValidationDash();
	}
}
function updateMultiJobDash(message,type,groupId,fleetCode){
	doConfirmJobs(message, function yes()
	{
		if(type==1){
			document.getElementById('multiJobUpdateDash').value=1;
			enterOR();
		} else {
			document.getElementById('multiJobUpdateDash').value=1;
			newDeleteDashOR(groupId,fleetCode);
		}
	}, function no()
	{
		if(type==1){
			document.getElementById('multiJobUpdateDash').value=0;
			enterOR();
		} else {
			document.getElementById('multiJobUpdateDash').value=0;
			newDeleteDashOR(type,fleetCode);
		}
	});
}
function openDriverDash(lineNumber,type) {
	var driverId="";
	var lati="";
	var longi="";
	var line="";
//	if(type==1){
//		driverId = document.getElementById("driver"+lineNumber).value;
//		lati = document.getElementById("lat"+lineNumber).value;
//		longi = document.getElementById("lon"+lineNumber).value;
//		var splitThis = document.getElementById("lineNew"+lineNumber).value.split("(");
//		line = splitThis[0];
//	} else {
	driverId = document.getElementById("drivers"+lineNumber).value;
	lati = document.getElementById("lats"+lineNumber).value;
	longi = document.getElementById("lons"+lineNumber).value;
	line = document.getElementById("lineNew"+lineNumber).value;
//	}
	var xmlhttp=null;	   
	if (window.XMLHttpRequest){	   
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}	
	var url = 'OpenRequestAjax?event=acceptCall&lineNumber='+line;
	xmlhttp.open("GET",url,false);
	xmlhttp.send(null);
	document.getElementById("driverButton_"+lineNumber).style.display="none";
	document.getElementById("driverButtons_"+lineNumber).style.display="block";
	currentDriver(driverId,lati,longi);
} 

var jsonObjects=[];
function getMeterTypesForDash(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'SystemSetupAjax?event=getMeterTypes';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!=""){
		var jsonObj = "{\"meterDetails\":"+text+"}";
		var obj = JSON.parse(jsonObj.toString());
		jsonObjects=JSON.parse(jsonObj.toString());
		var table = document.getElementById("forMeterDash");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);

		var element = document.createElement("label");
		element.name = "meterText";
		element.id = "meterText";
		element.appendChild(document.createTextNode("Meters:"));
		cell1.appendChild(element);

		var element1 = document.createElement("select");
		element1.name = "meter";
		element1.id = "meter";
		for(var j=0;j<obj.meterDetails.length;j++){
			var theOption = document.createElement("option");
			theOption.text = obj.meterDetails[j].MN;
			theOption.value =obj.meterDetails[j].MK;
			element1.options.add(theOption);
			if(obj.meterDetails[j].DF==1){
				document.getElementById('defaultMeterDash').value=obj.meterDetails[j].MK;
			}
		}
		element1.onchange=function(){changeFaresForDash();};
		cell2.appendChild(element1);
		document.getElementById('meter').value=document.getElementById('defaultMeterDash').value;
	} else {
		var table = document.getElementById("forMeterDash");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell1 = row.insertCell(0);
		var element = document.createElement("hidden");
		element.name = "meter";
		element.id = "meter";
		element.value="0";
		cell1.appendChild(element);
	}
	changeFaresForDash();
}

function changeFaresForDash(){
	if(document.getElementById("meter").value!=""){
		var j=Number(document.getElementById("meter").value)-1;
		document.getElementById("startAmtVoucher").value=jsonObjects.meterDetails[j].SA;
		document.getElementById("tripAmount").value=jsonObjects.meterDetails[j].RMl;
		document.getElementById("disN1").value=jsonObjects.meterDetails[j].D1;
		document.getElementById("disN2").value=jsonObjects.meterDetails[j].D2;
		document.getElementById("disN3").value=jsonObjects.meterDetails[j].D3;
		document.getElementById("rateN1").value=jsonObjects.meterDetails[j].R1;
		document.getElementById("rateN2").value=jsonObjects.meterDetails[j].R2;
		document.getElementById("rateN3").value=jsonObjects.meterDetails[j].R3;
	}
}


function conferenceCallDash(){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'RegistrationAjax?event=driverConference&phoneNumber='+document.getElementById('ph.Number').value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	$("#showHover").hide("slow");
	if(text.indexOf("001")>=0){
		document.getElementById("Error").innerHTML="Hang up the line. Drievr and passenger will receive call in 1 min"+'<a style="margin-left:60px" href="#"><img alt="" src="images/Dashboard/close.png" onclick="removeError()"></a>';
		document.getElementById("Error").style.display="block";
		phoneNumberPublic="";
	} else {
		document.getElementById("Error").innerHTML="Driver not allocated with the job / Invalid phoneNumber"+'<a style="margin-left:60px" href="#"><img alt="" src="images/Dashboard/close.png" onclick="removeError()"></a>';
		document.getElementById("Error").style.display="block";
	}
}
function checkGroupDash(type,row,groupId,longDesc){
	var drProf=$("#drprofileSizeDash").val();
	var veProf=$("#vprofileSizeDash").val();
	if(type==1 && groupId!="" && groupId!="0"){
		if(document.getElementById("dchk"+row).checked==true){
			for(var i=0;i<drProf;i++){
				if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
					document.getElementById("dchk"+row).checked=false;
					alert("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
					break;
				}
			}
			for(var i=0;i<veProf;i++){
				if(document.getElementById("vchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
					document.getElementById("dchk"+row).checked=false;
					alert("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
					break;
				}
			}
		}
	} else if(type==2 && groupId!="" && groupId!="0"){
		if(document.getElementById("vchk"+row).checked==true){
			for(var i=0;i<veProf;i++){
				if(document.getElementById("vchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
					document.getElementById("vchk"+row).checked=false;
					alert("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
					break;
				}
			}
			for(var i=0;i<drProf;i++){
				if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
					document.getElementById("vchk"+row).checked=false;
					alert("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
					break;
				}
			}
		}
	}
}
