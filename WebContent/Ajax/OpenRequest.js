$.ctrl = function(key, callback, args) {
	var isCtrl = false;
	$(document).keydown(function(e) {
		if(!args) args=[]; // IE barks when args is null

		if(e.ctrlKey) isCtrl = true;
		if(e.keyCode == key.charCodeAt(0) && isCtrl) {
			callback.apply(this, args);
			return false;
		}
	}).keyup(function(e) {
		if(e.ctrlKey) isCtrl = false;
	});
};

$.ctrl('S', function() {
});

$.ctrl('D', function(s) {
});


function colorChange(){
	var number,size,color;		
	size=document.getElementById("fleetSize1").value;
	for(var i=0;i<size;i++){
		number=document.getElementById("fleetNumber_"+i).value;
		color=document.getElementById("fleetcolor_"+i).value;
		if(document.getElementById("fleet").value==number){				
			document.getElementById("page-header").style.backgroundColor ="#"+color ;
		}
	}  
}


function AJAXInteraction(url, callback) {

	var req = init();
	req.onreadystatechange = processRequest();

	function init() {

		if (window.XMLHttpRequest) {
			return new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	} 

	function processRequest () {

		if (req.readyState == 4) {
			if (req.status == 200) {
				if (callback) callback(req.responseText);
			}
		}
	}

	this.doGet = function() {
		req.open("GET", url, true);
		req.send(null);
	}
}

function enterKeyPress(e)
{

	alert("enterKeyPress");

	if (window.event) { e = window.event; }
	if (e.keyCode == 13)
	{
		document.getElementById('submit').click();
	}
}
//------ Ajax InterAction  Over    -------------------------------

function callerId(){

	var xmlhttp = null;
	var blinkProp=false;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'AjaxClass?event=callerId&status=yes';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
//	for(var i=0;i<10;i++){
//		document.getElementById("button_"+i).style.display="none";
//		document.getElementById("buttonPicked_"+i).style.display="none";
//		document.getElementById("buttons_"+i).style.display="none";
//	}
	var tableAppend=document.getElementById("tblCallerId");
	var tableRows='<tr id="row" class="customerRow"> ';
	tableAppend.innerHTML="";
	for(var j=0;j<obj.address.length;j++){
		var phoneNumber=obj.address[j].P;
		if(obj.address[j].S==0 ){
			tableRows=tableRows+'<td>';
			tableRows=tableRows+'<input class="ccButtons" type="button" id="callerIdButton'+j+'" value="'+obj.address[j].L+'" onclick="callerIdPopulate('+j+');" onmouseover="callAccepted('+j+','+j+');" onmouseout="hideHover()">';
			tableRows=tableRows+'<div class="hover"><em><input id="acceptor'+j+'" value="Name:'+obj.address[j].N+' Called At:'+obj.address[j].T+' From:'+phoneNumber+'"></em></div>';
			tableRows=tableRows+'<div class="hover"><em><input id="acceptors'+j+'" value="Name:'+obj.address[j].N+' Called At:'+obj.address[j].T+' From:'+phoneNumber+'"></em></div>';
			tableRows=tableRows+'<input type="hidden" id="phone'+j+'" value="'+phoneNumber+'"/>';
			tableRows=tableRows+'<input type="hidden" id="name'+j+'" value="'+obj.address[j].N+'"/></td>';
		} else if(obj.address[j].S==1){
			tableRows=tableRows+'<td>';
			tableRows=tableRows+'<input class="ccButtonsAct" type="button" id="callerIdButton'+j+'" value="'+obj.address[j].L+'" onclick="callerIdPopulate('+j+');" onmouseover="callAccepted('+(j+1)+','+j+')" onmouseout="hideHover()">';
			tableRows=tableRows+'<div class="hover"><em><input id="acceptor'+j+'" value="Name:'+obj.address[j].N+' Called At:'+obj.address[j].T+' From:'+phoneNumber+' & Attended At:'+obj.address[j].AT+' By:'+obj.address[j].A+'"></em></div>';
			tableRows=tableRows+'<div class="hover"><em><input id="acceptors'+j+'" value="Name:'+obj.address[j].N+' Called At:'+obj.address[j].T+' From:'+phoneNumber+' & Attended At:'+obj.address[j].AT+' By:'+obj.address[j].A+'"></em></div>';
			tableRows=tableRows+'<input type="hidden" id="phone'+j+'" value="'+phoneNumber+'"/>';
			tableRows=tableRows+'<input type="hidden" id="name'+j+'" value="'+obj.address[j].N+'"/></td>';
		} else {
			tableRows=tableRows+'<td>';
			tableRows=tableRows+'<input class="ccButtonsPkd" type="button" id="callerIdButton'+j+'" value="'+obj.address[j].L+'" onclick="callerIdPopulate('+j+');" onmouseover="callAccepted('+(j+1)+','+j+')" onmouseout="hideHover()">';
			tableRows=tableRows+'<div class="hover"><em><input id="acceptorPicked'+j+'" value="Name:'+obj.address[j].N+' Called At:'+obj.address[j].T+' From:'+phoneNumber+'"></em></div>';
			tableRows=tableRows+'<div class="hover"><em><input id="acceptors'+j+'" value="Name:'+obj.address[j].N+' Called At:'+obj.address[j].T+' From:'+phoneNumber+'"></em></div>';
			tableRows=tableRows+'<input type="hidden" id="phone'+j+'" value="'+phoneNumber+'"/>';
			tableRows=tableRows+'<input type="hidden" id="name'+j+'" value="'+obj.address[j].N+'"/></td>';
		}
//		var phoneNumber=obj.address[j].P;
//		document.getElementById("phone"+j).value=phoneNumber;
//		document.getElementById("name"+j).value=obj.address[j].N;
//		if(obj.address[j].S==1){
//			document.getElementById("acceptor"+j).value="Name:"+obj.address[j].N+" Called At:"+obj.address[j].T+" From:"+phoneNumber+" & Attended At:"+obj.address[j].AT+" By:"+obj.address[j].A;
//			document.getElementById("acceptors"+j).value="Name:"+obj.address[j].N+" Called At:"+obj.address[j].T+" From:"+phoneNumber+" & Attended At:"+obj.address[j].AT+" By:"+obj.address[j].A;
//		} else if(obj.address[j].S==0){
//			document.getElementById("acceptor"+j).value="Name:"+obj.address[j].N+" Called At:"+obj.address[j].T+" From:"+phoneNumber;
//			document.getElementById("acceptors"+j).value="Name:"+obj.address[j].N+" Called At:"+obj.address[j].T+" From:"+phoneNumber;
//		}else if(obj.address[j].S==9){
//			document.getElementById("acceptorPicked"+j).value="Name:"+obj.address[j].N+" Called At:"+obj.address[j].T+" From:"+phoneNumber;
//			document.getElementById("acceptors"+j).value="Name:"+obj.address[j].N+" Called At:"+obj.address[j].T+" From:"+phoneNumber;
//		}
//		document.getElementById("callerIdButtons"+j).value=obj.address[j].L;
//		document.getElementById("callerIdButton"+j).value=obj.address[j].L;
//		document.getElementById("callerIdButtonPicked"+j).value=obj.address[j].L;
//		if(obj.address[j].S==0 ){
//			document.getElementById("button_"+j).style.display="block";
//		}else if(obj.address[j].S==9 ){
//			document.getElementById("buttonPicked_"+j).style.display="block";
//		} else {
//			document.getElementById("buttons_"+j).style.display="block";
//		}
	}
	tableRows=tableRows+'</tr>';
	tableAppend.innerHTML=tableRows;
}


function callerIdPopulate(lineNumber){
	var phoneNumber;
	phoneNumber=document.getElementById("phone"+lineNumber).value;
	if(isNaN(document.getElementById("phone"+lineNumber).value)){
		document.getElementById('phone').value="";
	} else {
		document.getElementById('phone').value=document.getElementById("phone"+lineNumber).value;
	}
	document.getElementById("name").value=document.getElementById("name"+lineNumber).value;
	var flag="0";
//	if(document.getElementById("button_"+lineNumber).style.display!="block"){
//		document.getElementById("buttonPicked_"+lineNumber).style.display="none";
//		flag="1";
//	}else{
//		document.getElementById("button_"+lineNumber).style.display="none";
//	}
//	document.getElementById("buttons_"+lineNumber).style.display="block";
//	if(flag=="1"){
//		document.getElementById("callerIdButtons"+lineNumber).value=document.getElementById("callerIdButtonPicked"+lineNumber).value;
//	}else{
//		document.getElementById("callerIdButtons"+lineNumber).value=document.getElementById("callerIdButton"+lineNumber).value;
//	}
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	var url = 'OpenRequestAjax?event=callerIdAddress&phoneNumber='+phoneNumber+'&lineNumber='+document.getElementById("callerIdButton"+lineNumber).value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	for(var j=0;j<obj.address.length;j++){
		/*		document.getElementById("sadd1").value=obj.address[j].Add1;
		document.getElementById("sadd2").value=obj.address[j].Add2;
		document.getElementById("scity").value=obj.address[j].City;
		document.getElementById("sstate").value=obj.address[j].State;
		document.getElementById("szip").value=obj.address[j].Zip;
		document.getElementById("payType").value=obj.address[j].PayType;
		document.getElementById("acct").value=obj.address[j].Acct;
		document.getElementById("sLatitude").value=obj.address[j].Lat;
		document.getElementById("sLongitude").value=obj.address[j].Lon;
		 */		
		document.getElementById("masterAddress").value=obj.address[j].M;
		//	document.getElementById("name").value=obj.address[j].Name;
		//previousAddress();
		openDetails(phoneNumber,'details');
	}
}
function openCompanySystem()
{
	//alert("Getting called ");

}
function appendLandMark(row,row_id)
{ 
	document.getElementById('fadd1temp').value = document.getElementById('sadd1LM_'+row).value;
	document.getElementById('fadd2temp').value = document.getElementById('sadd2LM_'+row).value;
	document.getElementById('fcitytemp').value = document.getElementById('scityLM'+row).value;
	document.getElementById('fstatetemp').value = document.getElementById('sstateLM'+row).value;
	document.getElementById('fziptemp').value = document.getElementById('szipLM'+row).value;
	document.getElementById('latitemp').value = document.getElementById('sLatitudeLM'+row).value;
	document.getElementById('longitemp').value = document.getElementById('sLongitudeLM'+row).value;
	document.getElementById('landmarkTemp').value = document.getElementById('landMarkLM_'+row).value;
	document.getElementById('LMKey').value = document.getElementById('Key'+row).value;
	document.getElementById('startTimeTemp').value="";
	document.getElementById('dateTemp').value="";
	document.getElementById('tripId').value="";
	document.getElementById('nameTemp').value="";
	document.getElementById('acct').value="";
	document.getElementById('payType').value="";
	document.getElementById('queueno').value="";
	document.getElementById('driver').value="";
	document.getElementById('repeatGroup').value="";
	document.getElementById('numberOfPassenger').value="";
	document.getElementById("numOfCabs").value="1";
	document.getElementById('advanceTime').value="-1";
	document.getElementById('tripStatus').value="";
	if((document.getElementById('specialIns1').value).indexOf(document.getElementById('commentsLM'+row).value)<=0){
		if(document.getElementById('specialIns1').value!="Temporary Comments" && document.getElementById('specialIns1').value!="Temporary Comments "){
			document.getElementById("specialIns1").value=document.getElementById("specialIns1").value+" "+document.getElementById('commentsLM'+row).value;
		} else {
			document.getElementById("specialIns1").value=document.getElementById('commentsLM'+row).value;
		}
	}	
	if(document.getElementById("specialIns1").value!=""){
		openDriverComments();
		document.getElementById("specialIns1").style.color='#000000';
	}
}
function appendLandMarkFromSingleAddress(row,row_id){
	if((row_id.value == "landMark") || (row_id == "landMark")) { 
		//if(document.getElementById("landMarkSelect").value==1){
		document.getElementById('sadd1').value = document.getElementById('sadd1LM_'+row).value;
		document.getElementById('sadd2').value = document.getElementById('sadd2LM_'+row).value;
		document.getElementById('scity').value = document.getElementById('scityLM'+row).value;
		document.getElementById('sstate').value = document.getElementById('sstateLM'+row).value;
		document.getElementById('szip').value = document.getElementById('szipLM'+row).value;
		document.getElementById('sLatitude').value = document.getElementById('sLatitudeLM'+row).value;
		document.getElementById('sLongitude').value = document.getElementById('sLongitudeLM'+row).value;
		document.getElementById('slandmark').value = document.getElementById('landMarkLM_'+row).value;
		document.getElementById('slandmarkSmall').value = document.getElementById('landMarkLM_'+row).value;
		document.getElementById('sadd1Small').value = document.getElementById('sadd1LM_'+row).value;
		document.getElementById('sadd2Small').value = document.getElementById('sadd2LM_'+row).value;
		document.getElementById('scitySmall').value = document.getElementById('scityLM'+row).value;
		document.getElementById('sstateSmall').value = document.getElementById('sstateLM'+row).value;
		document.getElementById('szipSmall').value = document.getElementById('szipLM'+row).value;
		document.getElementById('LMKey').value = document.getElementById('Key'+row).value;
		if((document.getElementById('specialIns1').value).indexOf(document.getElementById('commentsLM'+row).value)<=0){
			if(document.getElementById('specialIns1').value!="Temporary Comments" && document.getElementById('specialIns1').value!="Temporary Comments "){
				document.getElementById("specialIns1").value=document.getElementById("specialIns1").value+" "+document.getElementById('commentsLM'+row).value;
			} else {
				document.getElementById("specialIns1").value=document.getElementById('commentsLM'+row).value;
			}
		}	
		if(document.getElementById("specialIns1").value!=""){
			openDriverComments();
			document.getElementById("specialIns1").style.color='#000000';
		}
		document.getElementById('startTimeTemp').value="";
		document.getElementById('dateTemp').value="";
		/*} else {
			document.getElementById('eadd1').value = document.getElementById('sadd1_'+row).value;
			document.getElementById('eadd2').value = document.getElementById('sadd2_'+row).value;
			document.getElementById('ecity').value = document.getElementById('scity'+row).value;
			document.getElementById('estate').value = document.getElementById('sstate'+row).value;
			document.getElementById('ezip').value = document.getElementById('szip'+row).value;
			document.getElementById('eLatitude').value = document.getElementById('sLatitude'+row).value;
			document.getElementById('eLongitude').value = document.getElementById('sLongitude'+row).value;
			document.getElementById('elandmark').value = document.getElementById('landMark_'+row).value;

		} */
		initializeMap(1);markerDrag();	
	}
}

function appendPreviousAddress(row,row_id)
{
	document.getElementById('nameTemp').value=document.getElementById('Name_'+row).value;
	document.getElementById('eMail').value=document.getElementById('eMailReq_'+row).value;
	document.getElementById('startTimeTemp').value=document.getElementById('startTime_'+row).value;
	document.getElementById('dateTemp').value=document.getElementById('prevDate'+row).value;
	document.getElementById('fadd1temp').value = document.getElementById('prevAdd1'+row).value;
	document.getElementById('fadd2temp').value = document.getElementById('prevAdd2'+row).value;
	document.getElementById('fcitytemp').value = document.getElementById('prevCity'+row).value;
	document.getElementById('fstatetemp').value = document.getElementById('prevState'+row).value;
	document.getElementById('fziptemp').value = document.getElementById('prevZip'+row).value;
	document.getElementById('latitemp').value = document.getElementById('prevLatitude'+row).value;
	document.getElementById('longitemp').value = document.getElementById('prevLongitude'+row).value;
	document.getElementById('landmarkTemp').value=document.getElementById('LandMark'+row).value;
	document.getElementById('tripId').value = document.getElementById('tripId'+row).value;
	document.getElementById('Comments1').value=document.getElementById('comments'+row).value;
	document.getElementById('specialIns1').value=document.getElementById('SplIns'+row).value;
//	document.getElementById('CommentsTemporary').value=document.getElementById('comments'+row).value;
//	document.getElementById('Comments').value="Permanent Comments";
//	document.getElementById('specialIns').value="Permanent Comments";
	document.getElementById('tadd1temp').value = document.getElementById('prevEAdd1'+row).value;
	document.getElementById('tadd2temp').value = document.getElementById('prevEAdd2'+row).value;
	document.getElementById('tcitytemp').value = document.getElementById('prevECity'+row).value;
	document.getElementById('tstatetemp').value = document.getElementById('prevEState'+row).value;
	document.getElementById('tziptemp').value = document.getElementById('prevEZip'+row).value;
	document.getElementById('eLatitemp').value = document.getElementById('prevELatitude'+row).value;
	document.getElementById('eLongitemp').value = document.getElementById('prevELongitude'+row).value;
	document.getElementById('acct').value=document.getElementById('Acct'+row).value;
	document.getElementById('payType').value=document.getElementById('PayType'+row).value;
	document.getElementById('queueno').value=document.getElementById('Zone'+row).value;
	document.getElementById('landmarkTemp').value=document.getElementById('LandMark'+row).value;
	document.getElementById('elandmark').value=document.getElementById('ELandMark'+row).value;
	document.getElementById('phone').value=document.getElementById('Phone'+row).value;
	document.getElementById('jobRating').value=document.getElementById('jobRate'+row).value;
	document.getElementById('callerPhone').value=document.getElementById('callerPhone'+row).value;
	document.getElementById('callerName').value=document.getElementById('callerName'+row).value;
	document.getElementById('meter').value=document.getElementById('meterType'+row).value;
	if(document.getElementById("driverORcab").value=="Driver"){
		document.getElementById('driver').value=document.getElementById('DriverId_'+row).value;
		document.getElementById('DriverCabSwitch').value=document.getElementById('VehicleId'+row).value;
	} else {
		document.getElementById('driver').value=document.getElementById('VehicleId'+row).value;
		document.getElementById('DriverCabSwitch').value=document.getElementById('DriverId_'+row).value;
	}
	document.getElementById('repeatGroup').value=document.getElementById('repeatGroup'+row).value;
    document.getElementById('airName').value=document.getElementById('airNameReq_'+row).value;
    document.getElementById('airNo').value=document.getElementById('airNoReq_'+row).value;
    document.getElementById('airFrom').value=document.getElementById('airFromReq_'+row).value;
    document.getElementById('airTo').value=document.getElementById('airToReq_'+row).value;
    document.getElementById('numberOfPassenger').value=document.getElementById('numOfPass'+row).value;
	document.getElementById('advanceTime').value=document.getElementById("advTime"+row).value;
	document.getElementById('tripStatus').value=document.getElementById("tripStatusReq"+row).value;
	var driverList=document.getElementById("drprofileSize").value;
	var vehicleList=document.getElementById("vprofileSize").value;
	document.getElementById("premiumNum").value=document.getElementById("premiumCustomerReq"+row).value;
	if(document.getElementById("premiumCustomerReq"+row).value==1){
		document.getElementById("premiumCustomer").checked=true;
	}
	if(document.getElementById('sharedRide'+row).value==1){
		document.getElementById("sharedRide").checked=true;
	}
	if(document.getElementById('airName').value!="" && document.getElementById("hideFlightInformation").style.display=="none"){
		openFlightInformation();
	}
	var drFlag=document.getElementById('Flag'+row).value;
	if(drFlag!=""){
		for (var i=0; i<driverList;i++){
			if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
				document.getElementById('dchk'+i).checked=true;
			}
		}
		for (var i=0;i<vehicleList;i++){
			if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
				document.getElementById('vchk'+i).checked=true;
			}
		}
		openSplReq();
	}	
	document.getElementById("Comments").style.color='gray';
	document.getElementById("specialIns").style.color='gray';
	if(document.getElementById("Comments1").value!=""){
		document.getElementById("Comments1").style.color='#000000';
	}
	if(document.getElementById("specialIns1").value!=""){
		document.getElementById("specialIns1").style.color='#000000';
	}
	Driver();
	document.getElementById("fleet").value=document.getElementById("fleetCode"+row).value;
}


function appendParentFrom(row,row_id)
{ 
	if((row_id.value == "Fcheck") || (row_id == "Fcheck")) { 
		document.getElementById('nameTemp').value=document.getElementById('name_'+row).value;
		document.getElementById('masterAddress').value = document.getElementById('masterAddressKey_'+row).value;
//		document.getElementById('acct').value=document.getElementById('sAccount'+row).value;
		if(document.getElementById('Comments').value==""){
			document.getElementById('Comments').value=document.getElementById('commentsAdd'+row).value;
		}
		if(document.getElementById('specialIns').value==""){
			document.getElementById('specialIns').value=document.getElementById('commentsDriver'+row).value;
		}
//		document.getElementById('Comments1').value="Temporary Comments";
//		document.getElementById('specialIns1').value="Temporary Comments";
		document.getElementById('startTimeTemp').value="";
		document.getElementById('dateTemp').value="";
		document.getElementById('tripId').value="";
		document.getElementById('landmarkTemp').value="";
		if(document.getElementById("switchFromToAddress").value==1){
			document.getElementById('fadd1temp').value = document.getElementById('sadd1_'+row).value;
			document.getElementById('fadd2temp').value = document.getElementById('sadd2_'+row).value;
			document.getElementById('fcitytemp').value = document.getElementById('scity'+row).value;
			document.getElementById('fstatetemp').value = document.getElementById('sstate'+row).value;
			document.getElementById('fziptemp').value = document.getElementById('szip'+row).value;
			document.getElementById('latitemp').value = document.getElementById('sLatitude'+row).value;
			document.getElementById('longitemp').value = document.getElementById('sLongitude'+row).value;
			document.getElementById('userAddress').value = document.getElementById('addressKey_'+row).value;
			if(document.getElementById("eadd1").value!=""){
				document.getElementById('tadd1temp').value = "";
				document.getElementById('tadd2temp').value = "";
				document.getElementById('tcitytemp').value = "";
				document.getElementById('tstatetemp').value = "";
				document.getElementById('tziptemp').value = "";
				document.getElementById('eLatitemp').value = "";
				document.getElementById('eLongitemp').value = "";
			}
		} else {
			document.getElementById('tadd1temp').value = document.getElementById('sadd1_'+row).value;
			document.getElementById('tadd2temp').value = document.getElementById('sadd2_'+row).value;
			document.getElementById('tcitytemp').value = document.getElementById('scity'+row).value;
			document.getElementById('tstatetemp').value = document.getElementById('sstate'+row).value;
			document.getElementById('tziptemp').value = document.getElementById('szip'+row).value;
			document.getElementById('eLatitemp').value = document.getElementById('sLatitude'+row).value;
			document.getElementById('eLongitemp').value = document.getElementById('sLongitude'+row).value;
			document.getElementById('userAddressTo').value = document.getElementById('addressKey_'+row).value;
			if(document.getElementById("sadd1").value!=""){
				document.getElementById('fadd1temp').value = "";
				document.getElementById('fadd2temp').value = "";
				document.getElementById('fcitytemp').value = "";
				document.getElementById('fstatetemp').value = "";
				document.getElementById('fziptemp').value = "";
				document.getElementById('latitemp').value = "";
				document.getElementById('longitemp').value = "";
			}
		}
		document.getElementById('callerPhone').value="";
		document.getElementById('callerName').value="";
	}
	document.getElementById('driver').value = "";
	document.getElementById('DriverCabSwitch').value="";
	document.getElementById("Comments1").style.color='gray';
	document.getElementById("specialIns1").style.color='gray';
}

/*function appendParentTo(row,row_id)
{
	if((row_id.value == "Tcheck") || (row_id == "Tcheck")) { 
		document.getElementById('fadd1temp').value = "";
		document.getElementById('fadd2temp').value = "";
		document.getElementById('fcitytemp').value = "";
		document.getElementById('fstatetemp').value = "";
		document.getElementById('fziptemp').value = "";
		document.getElementById('latitemp').value = "";
		document.getElementById('longitemp').value = "";
		document.getElementById('nameTemp').value=document.getElementById('name_'+row).value;
		document.getElementById('userAddress').value = document.getElementById('addressKey_'+row).value;
		document.getElementById('masterAddress').value = document.getElementById('masterAddressKey_'+row).value;
		document.getElementById('acct').value=document.getElementById('sAccount'+row).value;
		document.getElementById('payType').value=document.getElementById('payType'+row).value;
		document.getElementById('Comments').value=document.getElementById('commentsAdd'+row).value;
		document.getElementById('specialIns').value=document.getElementById('commentsDriver'+row).value;
		//=document.getElementById('commentsAdd'+row).value;
		document.getElementById('startTimeTemp').value="";
		document.getElementById('dateTemp').value="";
		document.getElementById('tripId').value="";
		document.getElementById('landmarkTemp').value="";
	}
}*/
function appendPrevLM(row,row_id){
	if((row_id.value == "LMCheck") || (row_id == "LMCheck")) {
		if(document.getElementById("switchFromToAddress").value==1){
			document.getElementById('fadd1temp').value = document.getElementById('sadd1PLM_'+row).value;
			document.getElementById('fadd2temp').value = document.getElementById('sadd2PLM_'+row).value;
			document.getElementById('fcitytemp').value = document.getElementById('scityPLM'+row).value;
			document.getElementById('fstatetemp').value = document.getElementById('sstatePLM'+row).value;
			document.getElementById('fziptemp').value = document.getElementById('szipPLM'+row).value;
			document.getElementById('latitemp').value = document.getElementById('sLatitudePLM'+row).value;
			document.getElementById('longitemp').value = document.getElementById('sLongitudePLM'+row).value;
			document.getElementById('landmarkTemp').value=document.getElementById('PLM'+row).value;
			if(document.getElementById("eadd1").value==""){
				document.getElementById('tadd1temp').value = "";
				document.getElementById('tadd2temp').value = "";
				document.getElementById('tcitytemp').value = "";
				document.getElementById('tstatetemp').value = "";
				document.getElementById('tziptemp').value = "";
				document.getElementById('eLatitemp').value = "";
				document.getElementById('eLongitemp').value = "";
				document.getElementById('elandmark').value="";
			}
		} else {
			document.getElementById('tadd1temp').value = document.getElementById('sadd1PLM_'+row).value;
			document.getElementById('tadd2temp').value = document.getElementById('sadd2PLM_'+row).value;
			document.getElementById('tcitytemp').value = document.getElementById('scityPLM'+row).value;
			document.getElementById('tstatetemp').value = document.getElementById('sstatePLM'+row).value;
			document.getElementById('tziptemp').value = document.getElementById('szipPLM'+row).value;
			document.getElementById('eLatitemp').value = document.getElementById('sLatitudePLM'+row).value;
			document.getElementById('eLongitemp').value = document.getElementById('sLongitudePLM'+row).value;
			document.getElementById('elandmark').value=document.getElementById('PLM'+row).value;
			if(document.getElementById("sadd1").value==""){
				document.getElementById('fadd1temp').value = "";
				document.getElementById('fadd2temp').value = "";
				document.getElementById('fcitytemp').value = "";
				document.getElementById('fstatetemp').value = "";
				document.getElementById('fziptemp').value = "";
				document.getElementById('latitemp').value = "";
				document.getElementById('longitemp').value = "";
				document.getElementById('landmarkTemp').value="";
			}
		}
		if((document.getElementById('specialIns1').value).indexOf(document.getElementById('commentsPLM'+row).value)<=0){
			if(document.getElementById('specialIns1').value!="Temporary Comments" && document.getElementById('specialIns1').value!="Temporary Comments "){
				document.getElementById("specialIns1").value=document.getElementById("specialIns1").value+" "+document.getElementById('commentsPLM'+row).value;
			} else {
				document.getElementById("specialIns1").value=document.getElementById('commentsPLM'+row).value;
			}
		}	
		if(document.getElementById("specialIns1").value!=""){
			openDriverComments();
			document.getElementById("specialIns1").style.color='#000000';
		}
		document.getElementById('LMKey').value=document.getElementById('LMKey'+row).value;
		document.getElementById('userAddress').value ="000";
		document.getElementById('tripId').value="";
		document.getElementById('startTimeTemp').value="";
		document.getElementById('dateTemp').value="";
		document.getElementById('callerPhone').value="";
		document.getElementById('callerName').value="";
	}
}


function nameAutoFill(value){
	document.getElementById('payType').value=document.getElementById('payType'+value).value;
	document.getElementById('Comments').value=document.getElementById('commentsAdd'+value).value;
	document.getElementById('specialIns').value=document.getElementById('commentsDriver'+value).value;
	document.getElementById('advanceTime').value = document.getElementById('advanceTime'+value).value;
	document.getElementById('masterAddress').value= document.getElementById('masterAddressKey_'+value).value;

	if(document.getElementById("premiumCustomer"+value).value==1){
		document.getElementById("premiumCustomer").checked=true;
	}
	document.getElementById('acct').value=document.getElementById('sAccount'+value).value;
	if(document.getElementById('acct').value!=""){
		document.getElementById('payType').value="VC";
		document.getElementById("accountSuccess").value="1";
	} if(document.getElementById('Comments').value!=""){
		openDispatchComments();
		document.getElementById("Comments").style.color='#000000';
	} else {
		document.getElementById("Comments").style.color='gray';
		document.getElementById("Comments").value='';
	} if(document.getElementById("specialIns").value!=""){
		openDriverComments();
		document.getElementById("specialIns").style.color='#000000';
	}else {
		document.getElementById("specialIns").style.color='gray';
		document.getElementById("specialIns").value='';
	}
	document.getElementById('eMail').value=document.getElementById('eMail_'+value).value;
	document.getElementById('name').value=document.getElementById('name_'+value).value;
	document.getElementById('nameSmall').value=document.getElementById('name_'+value).value;
	
	document.getElementById('refNum').value=document.getElementById('refnum_'+value).value;
	document.getElementById('refNum1').value=document.getElementById('refnum1_'+value).value;
	document.getElementById('refNum2').value=document.getElementById('refnum2_'+value).value;
	document.getElementById('refNum3').value=document.getElementById('refnum3_'+value).value;
	
	var driverList=document.getElementById("drprofileSize").value;
	var vehicleList=document.getElementById("vprofileSize").value;
	var drFlag=document.getElementById('FlagCustomer'+value).value;
	if(drFlag!=""){
		for (var i=0; i<driverList;i++){
			if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
				document.getElementById('dchk'+i).checked=true;
			}
		}
		for (var i=0;i<vehicleList;i++){
			if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
				document.getElementById('vchk'+i).checked=true;
			}
		}
		openSplReq();
	}	
}
function appendParentFromSingleAddress(row,row_id){
	if((row_id.value == "Fcheck") || (row_id == "Fcheck")) { 
		document.getElementById('name').value=document.getElementById('name_'+row).value;
		document.getElementById('eMail').value=document.getElementById('eMail_'+row).value;
		document.getElementById('payType').value=document.getElementById('payType'+row).value;
		document.getElementById('Comments').value=document.getElementById('commentsAdd'+row).value;
		document.getElementById('specialIns').value=document.getElementById('commentsDriver'+row).value;
		document.getElementById('userAddress').value=document.getElementById('addressKey_'+row).value;
		document.getElementById('masterAddress').value= document.getElementById('masterAddressKey_'+row).value;
		document.getElementById('acct').value=document.getElementById('sAccount'+row).value;
		document.getElementById('sadd1').value = document.getElementById('sadd1_'+row).value;
		document.getElementById('sadd2').value = document.getElementById('sadd2_'+row).value;
		document.getElementById('scity').value = document.getElementById('scity'+row).value;
		document.getElementById('sstate').value = document.getElementById('sstate'+row).value;
		document.getElementById('szip').value = document.getElementById('szip'+row).value;
		document.getElementById('sLatitude').value = document.getElementById('sLatitude'+row).value;
		document.getElementById('sLongitude').value = document.getElementById('sLongitude'+row).value;
		document.getElementById('sadd1Small').value = document.getElementById('sadd1_'+row).value;
		document.getElementById('sadd2Small').value = document.getElementById('sadd2_'+row).value;
		document.getElementById('scitySmall').value = document.getElementById('scity'+row).value;
		document.getElementById('sstateSmall').value = document.getElementById('sstate'+row).value;
		document.getElementById('szipSmall').value = document.getElementById('szip'+row).value;

		document.getElementById('startTimeTemp').value="";
		document.getElementById('dateTemp').value="";
		if(document.getElementById("Comments").value!="" && document.getElementById("displayTableComments").style.display=="none" && document.getElementById('Comments').value!="null"){
			openDispatchComments();
			document.getElementById("Comments").style.color='#000000';
		}if(document.getElementById("specialIns").value!="" && document.getElementById("displayTableDriverComments").style.display=="none" && document.getElementById('specialIns').value!="null"){
			openDriverComments();
			document.getElementById("specialIns").style.color='#000000';
		}
		if( document.getElementById('acct').value!="" && document.getElementById("HideText").style.display=="none" && document.getElementById('acct').value!="null"){
			openPayment();
			//commentsDetail();
			showComments(2);
		}	
		var driverList=document.getElementById("drprofileSize").value;
		var vehicleList=document.getElementById("vprofileSize").value;
		var drFlag=document.getElementById('FlagCustomer'+row).value;
		if(drFlag!=""){
			for (var i=0; i<driverList;i++){
				if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
					document.getElementById('dchk'+i).checked=true;
				}
			}
			for (var i=0;i<vehicleList;i++){
				if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
					document.getElementById('vchk'+i).checked=true;
				}
			}
			openSplReq();
		}	

		initializeMap(1);markerDrag();
	} 
}

function appendFromToEnterValues(){
	if(document.getElementById("switchFromToAddress").value==1){
		document.getElementById('sadd1').value = document.getElementById('fadd1temp').value;
		document.getElementById('sadd2').value = document.getElementById('fadd2temp').value;
		document.getElementById('scity').value = document.getElementById('fcitytemp').value;
		document.getElementById('sstate').value = document.getElementById('fstatetemp').value;
		document.getElementById('szip').value = document.getElementById('fziptemp').value;
		document.getElementById('sLatitude').value = document.getElementById('latitemp').value;
		document.getElementById('sLongitude').value = document.getElementById('longitemp').value;
		document.getElementById('sadd1Small').value = document.getElementById('fadd1temp').value;
		document.getElementById('sadd2Small').value = document.getElementById('fadd2temp').value;
		document.getElementById('scitySmall').value = document.getElementById('fcitytemp').value;
		document.getElementById('sstateSmall').value = document.getElementById('fstatetemp').value;
		document.getElementById('szipSmall').value = document.getElementById('fziptemp').value;
		document.getElementById('slandmark').value = document.getElementById('landmarkTemp').value;
		document.getElementById('slandmarkSmall').value = document.getElementById('landmarkTemp').value;
	} else if(document.getElementById("switchFromToAddress").value==2){
		document.getElementById('eadd1').value = document.getElementById('tadd1temp').value;
		document.getElementById('eadd2').value = document.getElementById('tadd2temp').value;
		document.getElementById('ecity').value = document.getElementById('tcitytemp').value;
		document.getElementById('estate').value = document.getElementById('tstatetemp').value;
		document.getElementById('ezip').value = document.getElementById('tziptemp').value;
		document.getElementById('eLatitude').value = document.getElementById('eLatitemp').value;
		document.getElementById('eLongitude').value = document.getElementById('eLongitemp').value;
		document.getElementById('eadd1Small').value = document.getElementById('tadd1temp').value;
		document.getElementById('eadd2Small').value = document.getElementById('tadd2temp').value;
		document.getElementById('ecitySmall').value = document.getElementById('tcitytemp').value;
		document.getElementById('estateSmall').value = document.getElementById('tstatetemp').value;
		document.getElementById('ezipSmall').value = document.getElementById('tziptemp').value;
	}
//	if(document.getElementById('acct').value!="" && document.getElementById("switchFromToAddress").value==1){
//	showComments(2);
//	}
	if(document.getElementById("switchFromToAddress").value==1){
		initializeMap(1);markerDrag();
	} else {
		initializeMap(2);markerDragTo();
	}
	if(document.getElementById("tripId").value!=""){
		document.getElementById("tripIdAuto").value="";
		document.getElementById('name').value = document.getElementById('nameTemp').value;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'OpenRequestAjax?event=getCharges&tripId='+document.getElementById("tripId").value;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var jsonObj = "{\"address\":"+text+"}" ;
		var obj = JSON.parse(jsonObj.toString());
		for(var j=0;j<obj.address.length;j++){
			calCharges();
			var type=obj.address[j].T;
			var amount=obj.address[j].A;
			var i=j+1;
			document.getElementById("payType"+i).value=type;
			document.getElementById("amountDetail"+i).value=amount;
//			checkPercentages(i);
		}
		document.getElementById("successPage").style.display="none";
		document.getElementById("successPage1").style.display="none";
		if(document.getElementById('repeatGroup').value!="" && document.getElementById('repeatGroup').value!="0"){
			document.getElementById("successPage").innerHTML="This is a multi job.You cannot update date/time for all trips together.";
			document.getElementById("successPage").style.display="block";
		}
	} else {
		document.getElementById('repeatGroup').value="";
	} 
}

function deletePreviousRequest(row,row_id)
{
	var tripId=document.getElementById('tripId'+row).value;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'AjaxClass?event=deleteTrip&tripId=' + tripId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	return true;
}
function deletePreviousAddress(row,row_id)
{
	var userKey=document.getElementById('addressKey_'+row).value;
	var masterKey=document.getElementById('masterAddressKey_'+row).value;

	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'AjaxClass?event=deleteAddress&userKey='+userKey+'&masterKey='+masterKey;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	return true;
}

function landValues(row_id) {
	document.getElementById('fadd1temp').value = document.getElementById("ladd1"+row_id).value;
	document.getElementById('fadd2temp').value = document.getElementById("ladd2"+row_id).value;
	document.getElementById('fcitytemp').value= document.getElementById("lcity"+row_id).value;
	document.getElementById('fstatetemp').value = document.getElementById("lstate"+row_id).value;
	document.getElementById('fziptemp').value = document.getElementById("lzip"+row_id).value;
	document.getElementById('latitemp').value = document.getElementById("llatide"+row_id).value;
	document.getElementById('longitemp').value = document.getElementById("llogtude"+row_id).value;
	document.getElementById('LMKey').value=document.getElementById("landmarkKey"+row_id).value;
	document.getElementById('landmarkTemp').value = document.getElementById('landmarkS'+row_id).value;
	document.getElementById("specialIns1").value=document.getElementById("lcomments"+row_id).value;
	if(document.getElementById("specialIns1").value!=""){
		document.getElementById("specialIns1").style.color='#000000';
	}
}
function selectEnterLandValues(){
	if(document.getElementById("landMarkSelect").value==1){
		window.parent.document.getElementById("sadd1").value=document.getElementById('fadd1temp').value;
		window.parent.document.getElementById("sadd2").value=document.getElementById('fadd2temp').value;
		window.parent.document.getElementById("scity").value=document.getElementById('fcitytemp').value;
		window.parent.document.getElementById("sstate").value=document.getElementById('fstatetemp').value;
		document.getElementById("sLatitude").value=document.getElementById('latitemp').value;
		document.getElementById("sLongitude").value=document.getElementById('longitemp').value;
		document.getElementById("szip").value=document.getElementById('fziptemp').value;
		document.getElementById('slandmark').value=document.getElementById('landmarkTemp').value;
		document.getElementById('slandmarkSmall').value=document.getElementById('landmarkTemp').value;
		document.getElementById('sadd1Small').value = document.getElementById('fadd1temp').value;
		document.getElementById('sadd2Small').value = document.getElementById('fadd2temp').value;
		document.getElementById('scitySmall').value = document.getElementById('fcitytemp').value;
		document.getElementById('sstateSmall').value = document.getElementById('fstatetemp').value;
		document.getElementById('szipSmall').value = document.getElementById('fziptemp').value;
		initializeMap(1);markerDrag();
	} else if(document.getElementById("landMarkSelect").value==5) {
		window.parent.document.getElementById("cadd1").value = document.getElementById("fadd1temp").value;
		window.parent.document.getElementById("cadd2").value = document.getElementById("fadd2temp").value;
		window.parent.document.getElementById("ccity").value = document.getElementById("fcitytemp").value;
		window.parent.document.getElementById("cstate").value = document.getElementById("fstatetemp").value;
		window.parent.document.getElementById("czip").value = document.getElementById("fziptemp").value;
		document.getElementById("cLatitude").value = document.getElementById("latitemp").value;
		document.getElementById("cLongitude").value = document.getElementById("longitemp").value;
		document.getElementById('clandmark').value=document.getElementById('landmarkTemp').value;
	} else {
		window.parent.document.getElementById("eadd1").value = document.getElementById("fadd1temp").value;
		window.parent.document.getElementById("eadd2").value = document.getElementById("fadd2temp").value;
		window.parent.document.getElementById("ecity").value = document.getElementById("fcitytemp").value;
		window.parent.document.getElementById("estate").value = document.getElementById("fstatetemp").value;
		window.parent.document.getElementById("eLatitude").value = document.getElementById("latitemp").value;
		window.parent.document.getElementById("eLongitude").value = document.getElementById("longitemp").value;
		document.getElementById("ezip").value = document.getElementById("fziptemp").value;
		document.getElementById('elandmark').value=document.getElementById('landmarkTemp').value;
		document.getElementById('elandmarkSmall').value=document.getElementById('landmarkTemp').value;
		document.getElementById('eadd1Small').value = document.getElementById('fadd1temp').value;
		document.getElementById('eadd2Small').value = document.getElementById('fadd2temp').value;
		document.getElementById('ecitySmall').value = document.getElementById('fcitytemp').value;
		document.getElementById('estateSmall').value = document.getElementById('fstatetemp').value;
		document.getElementById('ezipSmall').value = document.getElementById('fziptemp').value;
		initializeMap(2);markerDragTo();
	}	
}
function landValuesSingleAddress(row_id,type) {
	if(document.getElementById("landMarkSelect").value==1){
		window.parent.document.getElementById("sadd1").value = document.getElementById("ladd1"+row_id).value;
		window.parent.document.getElementById("sadd2").value = document.getElementById("ladd2"+row_id).value;
		window.parent.document.getElementById("scity").value = document.getElementById("lcity"+row_id).value;
		window.parent.document.getElementById("sstate").value = document.getElementById("lstate"+row_id).value;
		window.parent.document.getElementById("szip").value = document.getElementById("lzip"+row_id).value;
		window.parent.document.getElementById("sLatitude").value = document.getElementById("llatide"+row_id).value;
		window.parent.document.getElementById("sLongitude").value = document.getElementById("llogtude"+row_id).value;
		document.getElementById('slandmark').value=document.getElementById('landmarkS'+row_id).value;
		document.getElementById('LMKey').value=document.getElementById('landmarkKey'+row_id).value;
		initializeMap(1);markerDrag();
	} else if(document.getElementById("landMarkSelect").value==2) {
		window.parent.document.getElementById("eadd1").value = document.getElementById("ladd1"+row_id).value;
		window.parent.document.getElementById("eadd2").value = document.getElementById("ladd2"+row_id).value;
		window.parent.document.getElementById("ecity").value = document.getElementById("lcity"+row_id).value;
		window.parent.document.getElementById("estate").value = document.getElementById("lstate"+row_id).value;
		window.parent.document.getElementById("ezip").value = document.getElementById("lzip"+row_id).value;
		window.parent.document.getElementById("eLatitude").value = document.getElementById("llatide"+row_id).value;
		window.parent.document.getElementById("eLongitude").value = document.getElementById("llogtude"+row_id).value;
		document.getElementById('elandmark').value=document.getElementById('landmarkS'+row_id).value;
		document.getElementById('LMKey').value=document.getElementById('landmarkKey'+row_id).value;
		initializeMap(2);markerDragTo();
	} else if(document.getElementById("landMarkSelect").value==3) {
		document.getElementById('sadd1Small').value = document.getElementById('ladd1'+row_id).value;
		document.getElementById('sadd2Small').value = document.getElementById('ladd2'+row_id).value;
		document.getElementById('scitySmall').value = document.getElementById('lcity'+row_id).value;
		document.getElementById('sstateSmall').value = document.getElementById('lstate'+row_id).value;
		document.getElementById('szipSmall').value = document.getElementById('lzip'+row_id).value;
		window.parent.document.getElementById("sLatitude").value = document.getElementById("llatide"+row_id).value;
		window.parent.document.getElementById("sLongitude").value = document.getElementById("llogtude"+row_id).value;
		document.getElementById('slandmarkSmall').value=document.getElementById('landmarkS'+row_id).value;
		document.getElementById('LMKey').value=document.getElementById('landmarkKey'+row_id).value;
	} else if(document.getElementById("landMarkSelect").value==5) {
		window.parent.document.getElementById("cadd1").value = document.getElementById("ladd1"+row_id).value;
		window.parent.document.getElementById("cadd2").value = document.getElementById("ladd2"+row_id).value;
		window.parent.document.getElementById("ccity").value = document.getElementById("lcity"+row_id).value;
		window.parent.document.getElementById("cstate").value = document.getElementById("lstate"+row_id).value;
		window.parent.document.getElementById("czip").value = document.getElementById("lzip"+row_id).value;
		document.getElementById("cLatitude").value = document.getElementById("llatide"+row_id).value;
		document.getElementById("cLongitude").value = document.getElementById("llogtude"+row_id).value;
		document.getElementById('clandmark').value=document.getElementById('landmarkS'+row_id).value;
		document.getElementById('LMKey').value=document.getElementById('landmarkKey'+row_id).value;
	}else {
		document.getElementById('elandmarkSmall').value=document.getElementById('landmarkS'+row_id).value;
		document.getElementById('eadd1Small').value = document.getElementById('ladd1'+row_id).value;
		document.getElementById('eadd2Small').value = document.getElementById('ladd2'+row_id).value;
		document.getElementById('ecitySmall').value = document.getElementById('lcity'+row_id).value;
		document.getElementById('estateSmall').value = document.getElementById('lstate'+row_id).value;
		document.getElementById('ezipSmall').value = document.getElementById('lzip'+row_id).value;
		window.parent.document.getElementById("eLatitude").value = document.getElementById("llatide"+row_id).value;
		window.parent.document.getElementById("eLongitude").value = document.getElementById("llogtude"+row_id).value;
		document.getElementById('LMKey').value=document.getElementById('landmarkKey'+row_id).value;
	}
	document.getElementById("specialIns1").value=document.getElementById("lcomments"+row_id).value;
	if(document.getElementById("specialIns1").value!=""){
		if(type==1){
			var finalComments = document.getElementById('specialIns1').value;
			if(finalComments!="" && finalComments.indexOf("P/U")<0 && finalComments.indexOf("D/O")<0){
				document.getElementById('specialIns1').value = "P/U:"+document.getElementById('specialIns1').value;
			}
		} else {
			var finalComments = document.getElementById('specialIns1').value;
			if(finalComments!="" && finalComments.indexOf("P/U")<0 && finalComments.indexOf("D/O")<0){
				document.getElementById('specialIns1').value = "D/O:"+document.getElementById('specialIns1').value;
			}
		}
		document.getElementById("specialIns1").style.color='#000000';
	}
}

function selectAdd(row_id,value) {
	document.getElementById('fadd1temp').value = document.getElementById('padd1_'+row_id).value; 
	document.getElementById('fcitytemp').value = document.getElementById('pcity_'+row_id).value;
	document.getElementById('fstatetemp').value = document.getElementById('pstate_'+row_id).value;
	document.getElementById('fziptemp').value = document.getElementById('pzip_'+row_id).value;
	document.getElementById('latitemp').value = document.getElementById('plat_'+row_id).value;
	document.getElementById('longitemp').value = document.getElementById('plong_'+row_id).value;

}function selectEnterMapDetails(){
	document.masterForm.sadd1.value = document.getElementById('fadd1temp').value;
	document.masterForm.scity.value = document.getElementById('fcitytemp').value;
	document.masterForm.sstate.value = document.getElementById('fstatetemp').value;
	document.masterForm.szip.value = document.getElementById('fziptemp').value;
	document.getElementById("sLatitude").value = document.getElementById('latitemp').value;
	document.getElementById("sLongitude").value = document.getElementById('longitemp').value;
	if(document.masterForm.sLatitude.length!=0)
	{ 
		addressverifycheck('check');
	}
}
function selectAddSingleAddress(row_id,value){
	document.masterForm.sadd1.value = document.getElementById('padd1_'+row_id).value; 
	document.masterForm.scity.value = document.getElementById('pcity_'+row_id).value;
	document.masterForm.sstate.value = document.getElementById('pstate_'+row_id).value;
	document.masterForm.szip.value = document.getElementById('pzip_'+row_id).value;
	document.masterForm.sLatitude.value = document.getElementById('plat_'+row_id).value;
	document.masterForm.sLongitude.value = document.getElementById('plong_'+row_id).value;

	if(document.masterForm.sLatitude.length!=0)
	{ 
		addressverifycheck('check');
	}	
}


function amtNumCheck()
{

	var NumAmt = document.getElementById('amt');

	NumAmtvalue = NumAmt.value;
	if(isNaN(NumAmtvalue))
	{
		alert("Please enter only numbers.");
		NumAmt.value="";
		return false;
	}	

	return true;

}

function hidePayment()
{

	var acc=document.getElementById('acct');

	if(acc.value!="")
	{

		var acctd = document.getElementById('acc1');
		acctd.style.display = 'none';

	}

}

function openMapdetailsSample(MapDetails)


{
	alert('inside openMapSample');
}

function openMapdetails_test(MapDetails,e)
{    
	document.getElementById('sLongitude').value  = "";
	document.getElementById('sLatitude').value  = "";
	document.getElementById('addressverify').style.background="yellow";

	var KeyID = (window.event) ? event.keyCode : e.keyCode; 
	if(KeyID == 119)
	{
		var zip = document.getElementById('szip');
		var add = document.getElementById('sadd1');
		var city = document.getElementById('scity');
		var state = document.getElementById('sstate');
		//	var mydivmap = document.getElementById('MapDetails1');

		var xmlhttp=null;

		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}

		var url = 'RegistrationAjax?event=getmapdetails&address='+add.value+'&city='+city.value+'&state='+state.value+'&zip='+zip.value;
		xmlhttp.open("GET",url,false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;


		document.getElementById(MapDetails).style.display = 'none';
		document.getElementById(MapDetails).innerHTML=text;

		if(document.getElementById("ArraySize").value>7)
		{
			document.getElementById(MapDetails).innerHTML=text;
			document.getElementById(MapDetails).style.display = '';
		}else if(document.getElementById("ArraySize").value=7)
		{

			document.masterForm.sadd1.value = document.getElementById('padd1_0').value; 
			document.masterForm.scity.value = document.getElementById('pcity_0').value;
			document.masterForm.sstate.value = document.getElementById('pstate_0').value;
			document.masterForm.szip.value = document.getElementById('pzip_0').value;
			document.masterForm.sLatitude.value = document.getElementById('plat_0').value;
			document.masterForm.sLongitude.value = document.getElementById('plong_0').value;

		}

		document.getElementById('addressverify').style.background="green";

		return true;
	} else {

		if(KeyID == 13){
			document.masterForm.submit(); 
		}
	}
}
function closeWindowZone()
{   
	document.getElementById("MapDetails1").style.display = "none";

}

function addVerified()
{
	var c1=document.masterForm.addverified;
	var c2=document.getElementById('sLatitude');
	var c3=document.getElementById('sLongitude');
	if(c1.checked==true)
	{ 
		c2.style.background="#808080";
		c3.style.background="#808080";
		c2.readOnly=true;
		c3.readOnly=true;
	} else {
		c2.readOnly=false;
		c3.readOnly=false;
		c2.style.background="#ffffff";
		c3.style.background="#ffffff";
	}
}

function openDetails(phones, detail) {
	var phone = document.getElementById('phone').value;
	if(phone==""){
		phone=document.getElementById('phoneSmall').value;
	}
	if (phone != "" && document.getElementById('phoneVerification').value=="") {
		document.getElementById("switchFromToAddress").value=1;
		var xmlhttp = null;
		var mydivland = document.getElementById('LandDetails');
		mydivland.style.display = 'none';
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'AjaxClass?event=previousRequest&phone='+phone+"&lineNumber="+0;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var resp = text.split('###');
		if(resp[0].indexOf('Error')>=0) {
			alert("Phone Number Not Valid");
			return false;
		}else if (resp[0].indexOf('NumRows=2')>=0) {
			document.getElementById('detailsAutoPopulated1').innerHTML = resp[1];
			document.getElementById('detailsAutoPopulated1').style.display = 'none';
			document.getElementById('tableCount').value="1";
			document.getElementById("totalAddress").value=document.getElementById("totalAddressSize").value;
			if(document.getElementById("ORFormat").value!="10"){
				$('#detailsAutoPopulated1').jqmShow();				
			} else {
				$( "#detailsAutoPopulated1" ).dialog({
					modal: true,
					width: 1030,
					height:700
				});
			}
			document.getElementById('phoneVerification').value="1";
			document.getElementById("callerExtra").style.display="block";
			nameAutoFill(0);
		} else {
			document.getElementById('detailsAutoPopulated1').innerHTML = resp[1];
			if (resp[0].indexOf('NumRows=1')>0) {
				document.getElementById('phoneVerification').value="1";
				appendParentFromSingleAddress(0,'Fcheck');
			} else if(resp[0].indexOf('NumRows=3')>0){
				document.getElementById('phoneVerification').value="1";
				appendLandMarkFromSingleAddress(0,'landMark');
			} 
		}
		document.getElementById('phoneVerification').value="1";
		document.getElementById("callerExtra").style.display="block";
	}
	return true;
}

function checkOldAddress(type){
	var totalSize=document.getElementById("totalAddress").value;
	var add1="";var latiMain=""; var longiMain = "";
	if(type==1){
		add1 = document.getElementById("sadd1").value;
		latiMain= document.getElementById("sLatitude").value;
		longiMain=document.getElementById("sLongitude").value;
	} else {
		add1 = document.getElementById("eadd1").value;
		latiMain= document.getElementById("eLatitude").value;
		longiMain=document.getElementById("eLongitude").value;
	}
	var latiToCheck=latiMain.split(".");
	latiToCheck=latiToCheck[0]+"."+latiToCheck[1].substring(0,4);
	var longiToCheck=longiMain.split("."); 
	longiToCheck=longiToCheck[0]+"."+longiToCheck[1].substring(0,4);
	for(var i=0;i<totalSize;i++){
		var existLati = (document.getElementById("sLatitude"+i).value).split(".");
		var existLongi= (document.getElementById("sLongitude"+i).value).split(".");
		if(add1==document.getElementById("sadd1_"+i).value || (latiToCheck==existLati[0]+"."+existLati[1].substring(0,4)) && (longiToCheck==existLongi[0]+"."+existLongi[1].existLongi(0,4))){
			if(type==1){
				document.getElementById("dontAddPickUp").value="1";
				break;
			} else {
				document.getElementById("dontAddDropOff").value="1";
				break;
			}
		}
	}
}

function Data(value,type){
	if(document.getElementById("detailsAutoPopulated1").innerHTML.indexOf("Current")>=0 || document.getElementById("detailsAutoPopulated1").innerHTML.indexOf("Previous")>=0  || document.getElementById("detailsAutoPopulated1").innerHTML.indexOf("LandMark")>=0){
		if(type==1 && document.getElementById("popUpPickUp").value==""){
			document.getElementById("switchFromToAddress").value=value;
			document.getElementById("popUpPickUp").value=type;
			document.getElementById('tableCount').value="1";
			if(document.getElementById("ORFormat").value!="10"){
				$('#detailsAutoPopulated1').jqmShow();
			} else {
				$( "#detailsAutoPopulated1" ).dialog({
					modal: true,
					width: 1030,
					height:700
				});
			}
		} else if(type==2) {
			document.getElementById("switchFromToAddress").value=value;
			document.getElementById('tableCount').value="1";
			if(document.getElementById("ORFormat").value!="10"){
				$('#detailsAutoPopulated1').jqmShow();
			} else {
				$( "#detailsAutoPopulated1" ).dialog({
					modal: true,
					width: 1030,
					height:700
				});
			}
		} else if(type==3 && document.getElementById("popUpDropOff").value==""){
			document.getElementById("switchFromToAddress").value=value;
			document.getElementById("popUpDropOff").value=type;
			document.getElementById('tableCount').value="1";
			if(document.getElementById("ORFormat").value!="10"){
				$('#detailsAutoPopulated1').jqmShow();
			} else {
				$( "#detailsAutoPopulated1" ).dialog({
					modal: true,
					width: 1030,
					height:700
				});
			}
		}
	}
}
function showComments(type){
	var account;
	if(type==1){
		var accountValue=document.getElementById("acct").value;
		var split=accountValue.split("(");
		account=split[0];
	} else {
		account=document.getElementById("acct").value;
	}
	if(account!=""){
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'OpenRequestAjax?event=getComments&account='+account;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var jsonObj = "{\"address\":"+text+"}";
		var obj = JSON.parse(jsonObj.toString());
		for(var j=0;j<obj.address.length;j++){
			var description=obj.address[j].Description;
			var name=obj.address[j].Name;
			var companyName=obj.address[j].CN;
			var startAmount=obj.address[j].SA;
			var ratePerMile=obj.address[j].RPM;
			var operatorComments=obj.address[j].OC;
			var driverComments=obj.address[j].DC;
			var drFlag=obj.address[j].SF;
			var driverList=document.getElementById("drprofileSize").value;
			var vehicleList=document.getElementById("vprofileSize").value;
			if(description=="Failed" || name=="Failed")
			{
				alert("Voucher Failed");
				//$('#commentsByAcct').jqmHide();
				//$("#commentsByAcct" ).dialog( "close" );
				$('#commentsByAcct').hide();
				document.getElementById("errorPage").innerHTML="Entered Voucher Is Expired/Inactive";
				document.getElementById("errorPage").style.display="block";
				document.getElementById("successPage").style.display="none";
				document.getElementById("acct").value="";
				document.getElementById('payType').value="Cash";
				return true;
			} else {
				document.getElementById("errorPage").style.display="none";
				document.getElementById('commentsByAcct').innerHTML = description+" ("+companyName+")";
				document.getElementById('payType').value="VC";
				document.getElementById('startAmtVoucher').value=startAmount;
				document.getElementById('rpmVoucher').value=ratePerMile;
				document.getElementById('tableCount').value="4";
				if(operatorComments!=""){
					openDispatchComments();
					document.getElementById("Comments1").value=operatorComments;
					document.getElementById("Comments1").style.color="#000000";
				}if(driverComments!=""){
					openDriverComments();
					if((document.getElementById('specialIns1').value).indexOf(driverComments)){
						if(document.getElementById('specialIns1').value!="Temporary Comments" && document.getElementById('specialIns1').value!="Temporary Comments "){
							document.getElementById("specialIns1").value=document.getElementById("specialIns1").value+" "+driverComments;
						} else {
							document.getElementById("specialIns1").value=driverComments;
						}
					}
					document.getElementById("specialIns1").style.color="#000000";
				}
				charges(account);
				if(document.getElementById("ORFormat").value!="10"){
					$('#commentsByAcct').jqmShow();
				} else {
					$( "#commentsByAcct" ).dialog({
						modal: true,
						width: 630
					});
				}
				if(drFlag!=""){
					for (var i=0; i<driverList;i++){
						if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
							document.getElementById('dchk'+i).checked=true;
						}
					}
					for (var i=0;i<vehicleList;i++){
						if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
							document.getElementById('vchk'+i).checked=true;
						}
					}
					openSplReq();
				}	
				document.getElementById("accountSuccess").value="1";
				return true;
			}
		}
		document.getElementById("sadd1").focus();
	}
}
function charges(voucherId){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getVoucherCharges&account='+voucherId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	if(obj.address.length>0){
		clearCharges();
	}
	for(var j=0;j<obj.address.length;j++){
		calCharges();
		var type=obj.address[j].CK;
		var amount=obj.address[j].CA;
		var i=j+1;
		document.getElementById("payType"+i).value=type;
		document.getElementById("amountDetail"+i).value=amount;
//		checkPercentages(i);
		var firstValue=document.getElementById('amt').value;
		if(firstValue==""){
			firstValue="0";
		}
		document.getElementById('amt').value=parseFloat(firstValue)+parseFloat(amount);
	}
}
function changeChargesOR(){
	var table = document.getElementById("chargesTable");
	var rowCount = table.rows.length;
	var size=document.getElementById('fieldSize').value;
	for(var i=0;i<Number(size);i++){
		rowCount--;
		table.deleteRow(rowCount);
	}
	document.getElementById("fieldSize").value="";
	calCharges();
	document.getElementById("payType"+1).value="1";
	document.getElementById("amountDetail"+1).value=document.getElementById('amt').value;
//	checkPercentages(1);
}
function openMapdetails() {

	var zip = document.getElementById('szip');
	var add = document.getElementById('sadd1');
	var city = document.getElementById('scity');
	var state = document.getElementById('sstate');
	var phone = document.getElementsByName('phone');
	if(phone.value!="")
	{
		var xmlhttp=null;	   
		var mydivland = document.getElementById('LandDetails');
		mydivland.style.display = 'none';

		if (window.XMLHttpRequest)	   

		{
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}	
		var url = 'RegistrationAjax?event=getmapdetails&address='+add.value+'&city='+city.value+'&state='+state.value+'&zip='+zip.value;
		xmlhttp.open("GET",url,false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;
		var resp = text.split('###');
		if (resp[0].indexOf('NumRows=2')>0) {
			document.getElementById('MapDetails').innerHTML=resp[1]; 
			document.getElementById('MapDetails').style.display = 'none';
			document.getElementById('tableCount').value="2";
			if(document.getElementById("ORFormat").value!="10"){
				$('#MapDetails').jqmShow();
			} else {
				$( "#MapDetails" ).dialog({
					modal: true,
					width: 630
				});
			}
		} else {
			document.getElementById('MapDetails').innerHTML=resp[1];
			selectAddSingleAddress(0,0);
		}
	} 
	return true;	
}

function openLanddetails(type) {
	var slandmark="";
	if(type==1 && document.getElementById("newLandMark1").checked==false){
		slandmark = document.getElementById('slandmark').value;
	} else if(type==2 && document.getElementById("newLandMark2").checked==false){
		slandmark = document.getElementById('elandmark').value;
	}else if(type==3 && document.getElementById("newLandMark1Small").checked==false){
		slandmark = document.getElementById('slandmarkSmall').value;
	}else if(type==4 && document.getElementById("newLandMark2Small").checked==false){
		slandmark = document.getElementById('elandmarkSmall').value;
	}else if(type==5){
		slandmark = document.getElementById('clandmark').value;
	}
	var phone = document.getElementById('phone');
	if(slandmark!="")
	{
		var xmlhttp=null;	   
		if (window.XMLHttpRequest){	   
			xmlhttp=new XMLHttpRequest();
		}
		else
		{
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}	
		var url = 'RegistrationAjax?event=getLandMarkDetails&module=dispatchView&landMark='+slandmark+'&type='+type+'&Screen=OR';
		xmlhttp.open("GET",url,false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;
		var resp = text.split('###');
		if (resp[0].indexOf('NumRows=2')>0) {
			document.getElementById('LandDetails').innerHTML=resp[1]; 	
			document.getElementById('LandDetails').style.display = '';
			document.getElementById('tableCount').value="3";
			if(document.getElementById("ORFormat").value!="10"){
				$('#LandDetails').jqmShow();
			} else {
				$( "#LandDetails" ).dialog({
					modal: true,
					width: 630
				});
			}
		} else {
			document.getElementById('LandDetails').innerHTML=resp[1];
			landValuesSingleAddress(0,type);
		}
	} 
	//return true;	
}
function openTripId(tripId) {
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'AjaxClass?event=previousTripId&tripId=' + tripId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	return true;
}

function openPopup(orid,row_id)
{  
	var mydiv = document.getElementById('adddetailsAutoPopulated');
	mydiv.style.display = 'none';
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}


	var url = 'AjaxClass?event=loadFromAndToAddrs&phone='+phone.value;
	xmlhttp.open("GET",url,false);
	xmlhttp.send(null);
	var text=xmlhttp.responseText;
	document.getElementById(row_id).innerHTML=text;
}

function loadPopup()
{

	window.open('child.html');

}


function loadFromToAddr(){


	fireMyPopup();
	var phone = document.getElementById('phone');
	var url = 'AjaxClass?event=loadFromToAddr&phone='+phone.value;
	var ajax = new AJAXInteraction(url,loadFromToAddrValues);
	ajax.doGet();
}

function loadFromToAddrValues(responseText)
{
	var resp = responseText.split('###');
	var sadd1 = document.getElementById('sadd1');
	var sadd2 = document.getElementById('sadd2');
	var sstate = document.getElementById('sstate');
	var scity = document.getElementById('scity');
	var szip = document.getElementById('szip');

	sadd1.value = resp[0];
	sadd2.value = "";
	scity.value = resp[1];
	sstate.value =resp[2];
	szip.value = resp[3];
	document.getElementById('stlatitude').value = resp[4];
	document.getElementById('stlongitude').value = resp[5];
	closeMyPopup();
}


function closeWindow() 

{  
	document.getElementById("details").style.display = "none"; 
}

function addressverifycheck(check)
{



	if (document.masterForm.addverified.type == "checkbox")
	{

		document.masterForm.addverified.checked = true;
	}

	if(document.masterForm.addverified.checked == true)
	{
		addVerified();
	}


}

function setFromAddress(flag,i)
{

	if(flag == "from") {
		document.masterForm.sadd1.value = document.getElementById('stadd1_'+i).value;
		document.masterForm.sadd2.value = document.getElementById('stadd2_'+i).value;
		document.masterForm.scity.value = document.getElementById('stcity'+i).value;
		document.masterForm.sstate.value = document.getElementById('ststate'+i).value;
		document.masterForm.szip.value = document.getElementById('stzip'+i).value;
	} else {
		document.masterForm.eadd1.value = document.getElementById('stadd1_'+i).value;
		document.masterForm.eadd2.value = document.getElementById('stadd2_'+i).value;
		document.masterForm.ecity.value = document.getElementById('stcity'+i).value;
		document.masterForm.estate.value = document.getElementById('ststate'+i).value;
		document.masterForm.ezip.value = document.getElementById('stzip'+i).value;
	}
}
function setToAddress(flag,i) {

	if(flag == "to") {
		document.masterForm.eadd1.value = document.getElementById('edadd1_'+i).value;
		document.masterForm.eadd2.value = document.getElementById('edadd2_'+i).value;
		document.masterForm.ecity.value = document.getElementById('edcity'+i).value;
		document.masterForm.estate.value = document.getElementById('edstate'+i).value;
		document.masterForm.ezip.value = document.getElementById('edzip'+i).value;	
	} else {

		document.masterForm.sadd1.value = document.getElementById('edadd1_'+i).value;
		document.masterForm.sadd2.value = document.getElementById('edadd2_'+i).value;
		document.masterForm.scity.value = document.getElementById('edcity'+i).value;
		document.masterForm.sstate.value = document.getElementById('edstate'+i).value;
		document.masterForm.szip.value = document.getElementById('edzip'+i).value;	
	}

}

function loadOtherData()
{

	if(document.getElementById('sesexit').value  == '1')
	{
		var phone = document.getElementById('phone');

		var url = 'AjaxClass?event=loadOther&phone='+phone.value+"&add1="+document.masterForm.sadd1.value+"&add2="+document.masterForm.sadd2.value+"&state="+document.masterForm.sstate.value+"&city="+document.masterForm.scity.value+"&zip="+document.masterForm.szip.value;
		var ajax = new AJAXInteraction(url,loadOtherDataValues);
		ajax.doGet();
	}
}

function loadOtherDataValues(responseText)
{

	var resp = responseText.split('###');
	if(resp[0]!="")
		//document.getElementById('sadd1').value=resp[0];
		if(resp[1]!="")
			document.getElementById('sadd2').value=resp[1];
	if(resp[2]!="")
		document.getElementById('scity').value=resp[2];
	if(resp[3]!="")
		document.getElementById('sstate').value=resp[3];
	if(resp[4]!="")
		document.getElementById('szip').value=resp[4];

}

function loadOtherData1()
{
	if(document.getElementById('sesexit').value  == '1')
	{
		var phone = document.getElementById('phone');
		var url = 'AjaxClass?event=loadOther&phone='+phone.value+"&add1="+document.masterForm.eadd1.value+"&add2="+document.masterForm.eadd2.value+"&state="+document.masterForm.estate.value+"&city="+document.masterForm.ecity.value+"&zip="+document.masterForm.ezip.value;
		var ajax = new AJAXInteraction(url,loadOtherDataValues1);
		ajax.doGet();
	}
}

function loadOtherDataValues1(responseText)
{
	var resp = responseText.split('###');
	if(resp[0]!="")
		//document.getElementById('eadd1').value=resp[0];
		if(resp[1]!="")
			document.getElementById('eadd2').value=resp[1];
	if(resp[2]!="")
		document.getElementById('ecity').value=resp[2];
	if(resp[3]!="")
		document.getElementById('estate').value=resp[3];
	if(resp[4]!="")
		document.getElementById('ezip').value=resp[4];

}
function loadQueue()
{
	fireMyPopup();
	var url = "http://maps.google.com/maps/api/geocode/xml?address="+document.getElementById("sadd1").value+","+document.getElementById("scity").value+","+document.getElementById("sstate").value+","+document.getElementById("szip").value+"&sensor=true";
	var ajax = new AJAXInteraction(url,loadQueueValues1); 	 
	ajax.doGet(); 
}

function loadQueue1()
{
	alert("loadQueue1 getting called "); 

	var url = 'RegistrationAjax?event=getmapdetails&address='+document.getElementById("sadd1").value+"&city="+document.getElementById("scity").value; 
	var ajax = new AJAXInteraction(url,QueueValues); 
	ajax.doGet();

}

function WebServiceFailedCallback(error)
{
	alert("Error function");
	var stackTrace = error.get_stackTrace();

	var message = error.get_message();

	var statusCode = error.get_statusCode();

	var exceptionType = error.get_exceptionType();

	var timedout = error.get_timedOut();




	var txtMessage = "{" +

	"Stack Trace: " + stackTrace + "; " +

	"Service Error: " + message + "; " +

	"Status Code: " + statusCode + "; " +

	"Exception Type: " + exceptionType + "; " +

	"Timedout: " + timedout

	+ "}";



	alert(txtMessage);

}
function QueueValues(responseText)
{ 	 
	var resp = responseText.split('###');
	closeMyPopup();  
	if(resp[0] =="14") { 
		document.getElementById('details').style.display ="block";
		document.getElementById('details').innerHTML = resp[1]; 
	} else {
		document.getElementById('queueid').innerHTML = resp[1]; 
	} 
}

function loadQueueValues(responseText)
{ 	 
	var resp = responseText.split('###');
	closeMyPopup(); 
	document.getElementById('queueid').innerHTML = resp[1]; 

}

function loadQueueValues1(responseXML)
{

	var url = 'AjaxClass?event=loadQueueValues&dcodeXML='+responseXML; 
	var ajax = new AJAXInteraction(url,loadQueueValues); 
	ajax.doGet();
}

function setPhone(p_phone) {
	if(p_phone.value.length == 3) {
		p_phone.value += "-";
	} else if(p_phone.value.length == 7) {
		p_phone.value += "-";
	} 
}

function checkLength(phone_no) {
	if(phone_no.value.length>12) {
		alert("Phone Number Allowed Only 10 Digit");
		phone_no.focus();
	}
}
/*function checkTime(value){
	if(document.getElementById("shrs").value=="Now"){
		document.getElementById("errorPage").innerHTML="For 'Now' time you cannot change date";
		document.getElementById("errorPage").style.display="block";
		document.getElementById("successPage").style.display="none";
	} else {
		displayCalendar(document.masterForm.sdate,'mm/dd/yyyy',value);
	}
}
 */function setOpacity( value ) {
	 document.getElementById("styled_popup").style.opacity = value / 10;
	 document.getElementById("styled_popup").style.filter = 'alpha(opacity=' + value * 10 + ')';
 }

 function fadeInMyPopup() {
	 for( var i = 0 ; i <= 100 ; i++ )
		 setTimeout( 'setOpacity(' + (i / 10) + ')' , 8 * i );
 }

 function fadeOutMyPopup() {
	 for( var i = 0 ; i <= 100 ; i++ ) {
		 setTimeout( 'setOpacity(' + (10 - i / 10) + ')' , 8 * i );
	 }

	 setTimeout('closeMyPopup()', 800 );
 }

 function closeMyPopup() {
	 document.getElementById("styled_popup").style.display = "none";
 }

 function fireMyPopup() {
	 setOpacity( 0 );
	 document.getElementById("styled_popup").style.display = "block";
	 fadeInMyPopup();
 }
 /*function returnScreen(){
	var Screen='1';
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	  {
		xmlhttp=new XMLHttpRequest();
	  }
	else
	  {		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }

	var url = 'AjaxClass?event=previousRequest&Screen='+Screen;
		xmlhttp.open("GET",url,false);
		xmlhttp.send(null);
}*/


 function details() {    
	 $("#LongDesc").hover(function() {
		 $(this).find("em").animate({
			 opacity : "show",
			 left:"85"
		 }, "slow");
	 },  function() {
		 $(this).find("em").animate({
			 opacity : "hide",
			 left:"75"
		 }, "fast");
	 } );
 }

 function details1() {  
	 $("#LongDesc1").hover(function() {
		 $(this).find("em").animate({
			 opacity : "show",
			 left:"85"
		 }, "slow");
	 },  function() {
		 $(this).find("em").animate({
			 opacity : "hide",
			 left:"75"
		 }, "fast");
	 } );
 }
 var phoneNumberPublic="";
 function callAccepted(Status,lineNumber) { 
	 if(Status==0){
		 document.getElementById("hoverValue").value=document.getElementById("acceptor"+lineNumber).value;
		 phoneNumberPublic=document.getElementById("phone"+lineNumber).value;
		 $("#showHover").show("slow");
	 }else if(Status==9){
		 document.getElementById("hoverValue").value=document.getElementById("acceptorPicked"+lineNumber).value;
		 phoneNumberPublic=document.getElementById("phone"+lineNumber).value;
		 $("#showHover").show("slow");
	 } else {
		 document.getElementById("hoverValue").value=document.getElementById("acceptors"+lineNumber).value;
		 phoneNumberPublic=document.getElementById("phone"+lineNumber).value;
		 $("#showHover").show("slow");
	 }
 }
 function hideHover(){
	 timerStartNew();
 }
 function openPayment()
 {
	 var ele = document.getElementById("HideText");
	 var text = document.getElementById("displayText");
	 if(ele.style.display == "block") {
		 ele.style.display = "none";
		 text.innerHTML = "Payment";
	 }
	 else {
		 ele.style.display = "block";
		 text.innerHTML = "Payment";
	 } 
 } 

 function openSplReq()
 {
	 var ele = document.getElementById("hideTable");
	 var text = document.getElementById("displayTable");
	 if(ele.style.display == "block") {
		 ele.style.display = "none";
		 text.innerHTML = "Special Request";
	 }
	 else {
		 ele.style.display = "block";
		 text.innerHTML = "Special Request";
	 }
 } 
 function shortcutNew(){
	 $("#showShortcut" ).dialog({
		 modal: true,
		 width:630
	 });
	 document.getElementById('tableCount').value="8";
 }
 function openDispatchComments()
 {
	 var ele = document.getElementById("hideTableComments");
	 var text = document.getElementById("displayTableComments");
	 if(ele.style.display == "block") {
		 ele.style.display = "none";
		 text.innerHTML = "Dispatch Comments";
	 }
	 else {
		 ele.style.display = "block";
		 text.innerHTML = "Dispatch Comments";
	 }
 }
 function openFlightInformation()
 {
	 var ele = document.getElementById("hideFlightInformation");
	 var text = document.getElementById("displayFlightInformation");
	 if(ele.style.display == "block") {
		 ele.style.display = "none";
		 text.innerHTML = "Flight Information";
	 }
	 else {
		 ele.style.display = "block";
		 text.innerHTML = "Flight Information";
	 }
 } 

 function openDriverComments()
 {
	 var ele = document.getElementById("hideTableDriverComments");
	 var text = document.getElementById("displayTableDriverComments");
	 if(ele.style.display == "block") {
		 ele.style.display = "none";
		 //	text.innerHTML = "Driver Comments";
	 }
	 else {
		 ele.style.display = "block";
		 //	text.innerHTML = "Driver Comments";
	 }
 } 
 function  displayJobOfDays()
 {
	 var ele = document.getElementById("hideJobsOnDays");
	 var text = document.getElementById("displayJobsOnDays");
	 if(ele.style.display == "block") {
		 ele.style.display = "none";
		 text.innerHTML = "Jobs On Days";
	 }
	 else {
		 ele.style.display = "block";
		 text.innerHTML = "Jobs On Days";
	 }
 } 

 function Driver()
 {
	 var ele = document.getElementById("hideDriver");
	 var text = document.getElementById("allotDriver");
	 if(ele.style.display == "block") {
		 ele.style.display = "none";
	 }
	 else {
		 ele.style.display = "block";
	 }
 } 
 function hideCallerId()
 {
	 var ele = document.getElementById("callerId");
	 var ele1= document.getElementById("tidOR");
	 ele.style.display = "none";
	 ele1.style.display="none";
 } 
 function submitFunctionForForm() {
	 if(document.getElementById('tableCount').value ==""){
		 return true;
	 } else
		 return false;
 }

 /*function focus(row_id) {
			 var field = row_id; 
			 document.getElementById(field).focus(); 
		 }*/

 function popUp(row_id) { 	  


	 var id = document.getElementById(row_id).value; 

	 if(id.length>0) {  

		 var url ='/TDS/AjaxClass?event=getlandmardDetails&module=dispatchView&landMark='+id; 

		 document.getElementById('slandmark').href=url; 
		 var a = document.getElementById('slandmark');
		 popup_window = window.open (url,"mywindow");

		 var t = a.title || a.name || null;
		 var l = a.href || a.alt;
		 var g = a.rel || false;
	 }  
 }

 function reqDetails(id) {
	 alert("this"+id+"end");
 }
 function change(type){
	 if(type==1){
		 document.getElementById("sLatitude").value='';
		 document.getElementById("sLongitude").value='';
		 document.getElementById("userAddress").value="";
	 } else if(type==2){
		 document.getElementById("eLatitude").value='';
		 document.getElementById("eLongitude").value='';
	 }
 } 

 function openDrop() { 
	 if(document.getElementById("smsStatus").checked) {  
		 document.getElementById("smsList").style.display='block'; 
	 }else {
		 document.getElementById("smsList").style.display='none';
		 document.getElementById("toSms").value="0";
	 }
 }


 function submitFN()
 {
	 document.getElementById("saveOpenReq").value = "saveOpenReq";
	 document.masterForm.submit();
 }

 function closePopup() { 

	 window.close();

 }	
 function clearLatNLong(){
	 document.getElementById('sLongitude').value  = "";
	 document.getElementById('sLatitude').value  = "";
	 document.getElementById('addressverify').style.background="yellow";

 }
 function previousAddress(){
	 if(document.getElementById("ORFormat").value!="10"){
		 $('#detailsAutoPopulated1').jqm();
	 }
 }
 function MapDetails(){
	 if(document.getElementById("ORFormat").value!="10"){
		 $('#MapDetails').jqm();
	 }
 }
 function landDetails(){
	 if(document.getElementById("ORFormat").value!="10"){
		 $('#LandDetails').jqm();
	 }
 }
 function commentsDetail(){
	 if(document.getElementById("ORFormat").value!="10"){
		 $('#commentsByAcct').jqm();
	 }
 }
 function completed(){
	 if(document.getElementById("ORFormat").value!="10"){
		 $('#completedJobs').jqm();
	 }
 }
 function showProviders(){
	 if(document.getElementById("ORFormat").value!="10"){
		 $( "#phoneProvider" ).jqm();
		 $( "#phoneProvider" ).jqmShow();
	 } else {
		 $( "#phoneProvider" ).dialog({
			 modal: true,
			 width: 630
		 });
	 }
	 document.getElementById('tableCount').value="6";
 }
 function changeScreen(method) {
	 var type="";
	 if(method=="1"){
		 type="newModel";
	 } else {
		 type="oldModel";
	 }
	 window.open('OpenRequestAjax?event=changeORScreen&switchScreen='+type);
 }
 function newDelete(){
	 var tripId = document.getElementById('tripId').value;
	 var repeatJob;
	 var xmlhttp = null;
	 if (window.XMLHttpRequest) 
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 if(document.getElementById('multiJobUpdate').value==1){
		 repeatJob = '&multiJobUpdate='+document.getElementById("repeatGroup").value;
	 } else {
		 repeatJob = '&multiJobUpdate=';
	 }if(document.getElementById("fleet").value!=""){
		 repeatJob=repeatJob+'&assoCode='+document.getElementById("fleet").value;
	 }
	 var url = 'OpenRequestAjax?event=saveOpenRequest&delete=delete&source=OR&tripId='+tripId+repeatJob;
	 xmlhttp.open("GET", url, false);
	 xmlhttp.send(null);
	 var text = xmlhttp.responseText;
	 var resp=text.split("###");
	 var result=resp[0].split(";");
	 if(result[0].indexOf('001')>=0){
		 if(resp[1].indexOf('003')>=0){
			 window.open('DashBoard?event=dispatchValues',"_self");
		 } else{
			 document.getElementById("successPage").innerHTML=result[1];
			 document.getElementById("successPage").style.display="block";
			 document.getElementById("errorPage").style.display="none";
			 document.getElementById("successPage1").innerHTML=result[1];
			 document.getElementById("successPage1").style.display="block";
			 document.getElementById("errorPage1").style.display="none";
			 clearOpenRequest();
		 }
	 } else {
		 document.getElementById("errorPage").innerHTML=result[1];
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 document.getElementById("errorPage1").innerHTML=result[1];
		 document.getElementById("errorPage1").style.display="block";
		 document.getElementById("successPage1").style.display="none";
	 }
 }
 function deleteOpenRequest(){
	 var tripId = document.getElementById('tripId').value;
	 if(tripId==""){
		 document.getElementById("errorPage").innerHTML="This is not an existing job";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 document.getElementById("errorPage1").innerHTML="This is not an existing job";
		 document.getElementById("errorPage1").style.display="block";
		 document.getElementById("successPage1").style.display="none";
	 } else {
		 if(document.getElementById('repeatGroup').value !="" && document.getElementById('repeatGroup').value!=0){
			 updateMultiJob("Do you want to delete all the repeat job for this tripId?",2);
		 } else {
			 newDelete();
		 }
	 }
 }
 function getCurrentTimeOR(){
	 var dTime = new Date();
	 var hours=dTime.getHours();
	 var min=dTime.getMinutes();
	 if(hours<10){
		 hours="0"+hours;
	 }if(min<10){
		 min="0"+min;
	 } /*else if(min>=15 && min<30){
		min="15";
	} else if(min>=30 && min<45){
		min="30";
	} else {
		min="45";
	}
	  */	var time=hours+""+min;
	  return time;
 }
 function enterEmailOR(){
	 if(document.getElementById("phone").value==""){
		 //$('#phoneProvider').jqmHide();
		 $("#phoneProvider" ).dialog( "close" );
		 $('#phoneProvider').hide();
		 document.getElementById('tableCount').value="6";
		 document.getElementById("provider").value="";
		 alert("Enter any phone number");
	 } else {
		 document.getElementById('eMail').value=document.getElementById("phone").value+document.getElementById("provider").value;
		 //$('#phoneProvider').jqmHide();
		 $("#phoneProvider" ).dialog( "close" );
		 $('#phoneProvider').hide();
		 document.getElementById('tableCount').value="6";
		 document.getElementById("provider").value="";
	 }
 }
 function enterOpenRequest(){
	 var phone = document.getElementById('phone').value;
	 var name = document.getElementById('name').value;
	 var email=document.getElementById('eMail').value;
	 var add1 = document.getElementById('sadd1').value;
	 var add2 = document.getElementById('sadd2').value;
	 var city = document.getElementById('scity').value;
	 var state = document.getElementById('sstate').value;
	 var zip = document.getElementById('szip').value;
	 var lati = document.getElementById('sLatitude').value;
	 var longi = document.getElementById('sLongitude').value;
	 var date = document.getElementById('sdate').value;
	 var tripStatus=document.getElementById('tripStatus').value;
	 var updateAll=document.getElementById("updateAll").value;
	 var time;
	 var premiumCustomer=document.getElementById('premiumNum').value;
	 if(document.getElementById('shrs').value=="Now"){
		 time = '2525';
		 premiumCustomer=0;
	 } else if (document.getElementById('shrs').value=="NCH"){
		 time = document.getElementById('startTimeTemp').value;
	 } else {
		 time = document.getElementById('shrs').value;
		 premiumCustomer=2;
	 }
	 var dispComments="";
	 var dispCommentsTemp="";
	 if(document.getElementById('Comments').value!=""){
		 dispComments=document.getElementById('Comments').value;
	 } if(document.getElementById('Comments1').value!=""){
		 dispCommentsTemp=document.getElementById('Comments1').value;
	 } 
	 var comments =dispComments+""+dispCommentsTemp;
	 comments=comments.replace("&","and").replace("%","pct");
	 var updateStaticComment=dispComments.replace("&","and").replace("%","pct");
	 var permComments="";
	 var tempComments="";
	 if(document.getElementById('specialIns').value!=""){
		 permComments=document.getElementById('specialIns').value;
	 } if(document.getElementById('specialIns1').value!=""){
		 tempComments=document.getElementById('specialIns1').value;
	 }
	 if(document.getElementById("premiumCustomer").checked==true){
		 premiumCustomer=1;
	 }
	 var splIns =permComments+""+tempComments;
	 splIns=splIns.replace("&","and").replace("%","pct");
	 var updateDriverComments=permComments.replace("&","and").replace("%","pct");
	 var tripId = document.getElementById('tripId').value;
	 var masterKey = document.getElementById('masterAddress').value;
	 var addressKey= document.getElementById('userAddress').value;
	 var addressKeyTo=document.getElementById('userAddressTo').value;
	 var eadd1 = document.getElementById('eadd1').value;
	 var eadd2 = document.getElementById('eadd2').value;
	 var ecity = document.getElementById('ecity').value;
	 var estate = document.getElementById('estate').value;
	 var ezip = document.getElementById('ezip').value;
	 var elati = document.getElementById('eLatitude').value;
	 var elongi = document.getElementById('eLongitude').value;
	 var advTime= document.getElementById('advanceTime').value;
	 var fromDate = document.getElementById('fromDate').value;
	 var toDate = document.getElementById('toDate').value;
	 var payType=document.getElementById('payType').value;
	 var accountWithName=document.getElementById('acct').value;
	 var amountTemp=document.getElementById('amt').value;
	 var amount=amountTemp.replace("$","");
	 var accounttoSplit=accountWithName.split("(");
	 var account=accounttoSplit[0];
	 if(account==""){
		 payType="Cash";
	 }
	 var zone=document.getElementById("queueno").value;
	 var driverList=document.getElementById("drprofileSize");
	 var vehicleList=document.getElementById("vprofileSize");
	 var landMarkKey="";
	 var slandmark=document.getElementById('slandmark').value;
	 var elandmark=document.getElementById('elandmark').value;
	 var callerPhone=document.getElementById("callerPhone").value;
	 var calledInBy=document.getElementById("callerName").value;
	 var custRefNum=document.getElementById("refNum").value;
	 var custRefNum1=document.getElementById("refNum1").value;
	 var custRefNum2=document.getElementById("refNum2").value;
	 var custRefNum3=document.getElementById("refNum3").value;
	 var airlineName=document.getElementById("airName").value;
	 var airlineNo=document.getElementById("airNo").value;
	 var airlineFrom=document.getElementById("airFrom").value;
	 var airlineTo=document.getElementById("airTo").value;
	 if(addressKey!="000" && document.getElementById('newLandMark1').checked==false && document.getElementById('newLandMark2').checked==false){
		 landMarkKey=document.getElementById("LMKey").value;
	 }
	 var meterType = document.getElementById("meter").value;
	 var noOfPassengers=document.getElementById("numberOfPassenger").value;
	 var numOfCabs=document.getElementById("numOfCabs").value;
	 var driver="";
	 var cab="";
	 var jobRate=document.getElementById("jobRating").value;
	 if(document.getElementById("driverORcab").value=="Driver"){
		 driver=document.getElementById("driver").value;
		 cab=document.getElementById('DriverCabSwitch').value;
	 } else {
		 cab=document.getElementById("driver").value;
		 driver=document.getElementById('DriverCabSwitch').value;
	 }
	 var nextScreen=document.getElementById("nextScreen").value;
	 var repeatJob="";
	 if(document.getElementById('Sun').checked==true){
		 repeatJob = '&Sun=true';
	 }
	 if(document.getElementById('Mon').checked==true){
		 repeatJob = repeatJob + '&Mon=true';
	 }
	 if(document.getElementById('Tue').checked==true){
		 repeatJob = repeatJob + '&Tue=true';
	 }
	 if(document.getElementById('Wed').checked==true){
		 repeatJob = repeatJob + '&Wed=true';
	 }
	 if(document.getElementById('Thu').checked==true){
		 repeatJob = repeatJob + '&Thu=true';
	 }
	 if(document.getElementById('Fri').checked==true){
		 repeatJob = repeatJob + '&Fri=true';
	 }
	 if(document.getElementById('Sat').checked==true){
		 repeatJob = repeatJob + '&Sat=true';
	 }
	 if(document.getElementById('newLandMark1').checked==true){
		 repeatJob = repeatJob+'&newFromLandMark=true';
	 }if(document.getElementById('newLandMark2').checked==true){
		 repeatJob = repeatJob+'&newToLandMark=true';
	 }if(document.getElementById('sharedRide').checked==true){
		 repeatJob = repeatJob+'&sharedRide=1';
	 }else {
		 repeatJob = repeatJob+'&sharedRide=0';
	 }
	 if(document.getElementById('dispatchStatus').checked==true){
		 repeatJob = repeatJob+'&dispatchStatus=true';
	 } if(document.getElementById("fieldSize").value!=""){
		 var chargeSize=document.getElementById("fieldSize").value;
		 repeatJob = repeatJob+'&chargesLength='+chargeSize;
		 for (var i=1; i<=chargeSize;i++){
			 repeatJob=repeatJob+'&chargeKey'+i+'='+document.getElementById('payType'+i).value;
			 repeatJob=repeatJob+'&chargeAmount'+i+'='+document.getElementById('amountDetail'+i).value;
		 }
	 } 
	 if(document.getElementById('tripIdAuto').value!=""){
		 repeatJob = repeatJob+'&tripIdAuto='+document.getElementById('tripIdAuto').value;
	 }if(document.getElementById('ccAuthCode').value!=""){
		 repeatJob = repeatJob+'&ccAuthCode='+document.getElementById('ccAuthCode').value;
	 }
	 if(typeof driverList != "" && driverList!=null){
		 repeatJob=repeatJob+'&drprofileSize='+driverList.value;	
		 for (var i=0; i<driverList.value;i++){
			 if(document.getElementById('dchk'+i).checked==true){
				 repeatJob=repeatJob+'&dchk'+i+'='+document.getElementById('dchk'+i).value;
			 }
		 }
	 }
	 if(typeof vehicleList != "" &&vehicleList!=null){
		 repeatJob=repeatJob+'&vprofileSize='+vehicleList.value;	
		 for (var i=0;i<vehicleList.value;i++){
			 if(document.getElementById('vchk'+i).checked==true){
				 repeatJob=repeatJob+'&vchk'+i+'='+document.getElementById('vchk'+i).value;
			 }
		 }
	 }if(document.getElementById('multiJobUpdate').value==1){
		 repeatJob = repeatJob+'&multiJobUpdate='+document.getElementById('repeatGroup').value;
	 } else {
		 repeatJob = repeatJob+'&multiJobUpdate=';
	 }if(document.getElementById("fleet").value!=""){
		 repeatJob=repeatJob+'&assoCode='+document.getElementById("fleet").value;
	 }if(document.getElementById("roundTripExist").value!=""){
		 repeatJob=repeatJob+'&roundTripExist='+document.getElementById("roundTripExist").value;
	 }
	 repeatJob=repeatJob+"&addPickUp="+document.getElementById("dontAddPickUp").value+"&addDropOff="+document.getElementById("dontAddDropOff").value;
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 if(add1!="" && lati!="" && lati!="0.000000" && longi!="" && longi!="0.000000"){
		 var url = 'OpenRequestAjax';
		 var parameters='event=saveOpenRequest&source=OR&saveOpenReq=saveOpenReq&phone='+phone+'&name='+name+'&sadd1='+add1+'&sadd2='+add2+'&scity='+city+'&sstate='+state+'&szip='+zip+'&sdate='+date+'&shrs='+time+'&sLongitude='+longi+'&sLatitude='+lati+'&tripId='+tripId+'&masterAddress='+masterKey+'&userAddress='+addressKey+'&userAddressTo='+addressKeyTo+'&Comments='+comments+'&specialIns='+splIns+'&eadd1='+eadd1+'&eadd2='+eadd2+'&ecity='+ecity+'&estate='+estate+'&ezip='+ezip+'&edlongitude='+elongi+'&edlatitude='+elati+'&advanceTime='+advTime+'&fromDate='+fromDate+'&toDate='+toDate+'&payType='+payType+'&acct='+account+'&amt='+amount+'&driver='+driver+'&cabNo='+cab+'&nextScreen='+nextScreen+'&landMarkKey='+landMarkKey+'&slandmark='+document.getElementById('slandmark').value+'&elandmark='+document.getElementById('elandmark').value+'&updateStaticComment='+updateStaticComment+'&updateDriverComment='+updateDriverComments+'&queueno='+zone+'&numberOfPassengers='+noOfPassengers+
		 '&numOfCabs='+numOfCabs+'&premiumCustomer='+premiumCustomer+'&eMail='+email+'&updateAll='+updateAll+'&tripStatus='+tripStatus+'&callerPhone='+callerPhone+'&callerName='+calledInBy+'&custRefNum='+custRefNum+'&custRefNum1='+custRefNum1+'&custRefNum2='+custRefNum2+'&custRefNum3='+custRefNum3+'&jobRating='+jobRate+'&meterType='+meterType+'&airName='+airlineName+'&airNo='+airlineNo+'&airFrom='+airlineFrom+'&airTo='+airlineTo+repeatJob;
		 xmlhttp.open("POST",url, false);
		 xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		 xmlhttp.setRequestHeader("Content-length", parameters.length);
		 xmlhttp.setRequestHeader("Connection", "close");
		 xmlhttp.send(parameters);
		 var text = xmlhttp.responseText;
		 var resp=text.split("###");
		 var result=resp[0].split(";");
		 if(result[0].indexOf('001')>=0){
			 var createdTrip=result[1].split("TripId=");
			 if(resp[1].indexOf('003')>=0){
				 window.open('DashBoard?event=dispatchValues',"_self");
			 } else{
				 var roundTripMessage="";
				 if(document.getElementById('roundTrip').checked==true) {
					 document.getElementById('sadd1').value=eadd1;
					 document.getElementById('sadd2').value=eadd2;
					 document.getElementById('scity').value=ecity;
					 document.getElementById('sstate').value=estate;
					 document.getElementById('szip').value=ezip;
					 document.getElementById('sLatitude').value=elati;
					 document.getElementById('sLongitude').value=elongi;
					 document.getElementById('slandmark').value=elandmark;
					 document.getElementById('eadd1').value=add1;
					 document.getElementById('eadd2').value=add2;
					 document.getElementById('ecity').value=city;
					 document.getElementById('estate').value=state;
					 document.getElementById('ezip').value=zip;
					 document.getElementById('eLatitude').value=lati;
					 document.getElementById('eLongitude').value=longi;
					 document.getElementById('elandmark').value=slandmark;
					 document.getElementById('roundTrip').checked=false;
					 roundTripMessage="You Have Given RT, Change Time & Submit";
				 }
				 document.getElementById("successPage").innerHTML=result[1]+"</br> "+roundTripMessage;
				 document.getElementById("errorPage").style.display="none";
				 document.getElementById("successPage1").innerHTML=result[1]+"</br> "+roundTripMessage;
				 document.getElementById("successPage1").style.display="block";
				 document.getElementById("errorPage1").style.display="none";
				 document.getElementById("roundTripExist").value=createdTrip[1];
				 if(document.getElementById("cloneJob").checked==false && roundTripMessage==""){
					 document.getElementById("ccAuthCode").value="";
					 clearOpenRequest();
//					 if(email!=""){
//						 sendCustomerMail(email,createdTrip[1]);
//					 }
				 }
				 document.getElementById("successPage").style.display="block";
				 document.getElementById("cloneJob").checked=false;
				 document.getElementById("preAuth").disabled=false;
				 $("#shrs option[value=NCH]").hide();
				 $("#shrsSmall option[value=NCH]").hide();
			 }
		 } else if(result[0].indexOf('002')>=0){
			 document.getElementById("errorPage").innerHTML=result[1];
			 document.getElementById("errorPage").style.display="block";
			 document.getElementById("successPage").style.display="none";
			 document.getElementById("errorPage1").innerHTML=result[1];
			 document.getElementById("errorPage1").style.display="block";
			 document.getElementById("successPage1").style.display="none";
		 }else{
			 document.getElementById("errorPage").innerHTML="Create/Update a job failed:Call Administrator";
			 document.getElementById("errorPage").style.display="block";
			 document.getElementById("successPage").style.display="none";
			 document.getElementById("errorPage1").innerHTML="Create/Update a job failed:Call Administrator";
			 document.getElementById("errorPage1").style.display="block";
			 document.getElementById("successPage1").style.display="none";
		 }
	 } else {
		 document.getElementById("errorPage").innerHTML="Please provide correct address";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 document.getElementById("errorPage1").innerHTML="Please provide correct address";
		 document.getElementById("errorPage1").style.display="block";
		 document.getElementById("successPage1").style.display="none";
	 }
 }
 
// function sendCustomerMail(mailId,tripId){
//	 var xmlhttp = null;
//	 if (window.XMLHttpRequest)
//	 {
//		 xmlhttp = new XMLHttpRequest();
//	 } else {
//		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//	 }
//	 url='SystemSetupAjax?event=sendMail&mailId='+mailId+'&tripId='+tripId;
//	 xmlhttp.open("GET", url, true);
//	 xmlhttp.send(null);
// }

 function appendCompletedAddress(tripId)
 {
	 //$('#completedJobs').jqmHide();
	 $("#completedJobs" ).dialog( "close" );
	 $('#completedJobs').hide();
	 var time = getCurrentTimeOR();
	 // var tripId=document.getElementById("tripId"+row).value;
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 url='control?action=openrequest&event=openRequestHistory&button=No&reDispatch=YES&tripIdHistory='+tripId+'&time='+time+'&source=createAJob';
	 xmlhttp.open("GET", url, false);
	 xmlhttp.send(null);
	 var text = xmlhttp.responseText;
	 if(text=="002"){
		 document.getElementById("errorPage").innerHTML="Failed to redispatch the job";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
	 } else {
		 var tripId=text.split(";");
		 document.getElementById("successPage").innerHTML="Job redispatched successfully and the "+tripId[0];
		 document.getElementById("successPage").style.display="block";
		 document.getElementById("errorPage").style.display="none";
		 clearOpenRequest();
	 }
 }
 function getTripAuto(){
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 url='OpenRequestAjax?event=getAutoTripId';
	 xmlhttp.open("GET", url, false);
	 xmlhttp.send(null);
	 var text = xmlhttp.responseText;
	 document.getElementById("tripIdAuto").value=text;
	 if(document.getElementById("tripIdAuto").value!=""){
		 document.getElementById("successPage").innerHTML="The tripid for this job is "+	document.getElementById("tripIdAuto").value;
		 document.getElementById("successPage").style.display="block";
		 document.getElementById("errorPage").style.display="none";
		 document.getElementById("successPage1").innerHTML="The tripid for this job is "+	document.getElementById("tripIdAuto").value;
		 document.getElementById("successPage1").style.display="block";
		 document.getElementById("errorPage1").style.display="none";
	 }
 }
 function clearPUAddressOR(){
	 document.getElementById('sadd1').value = "";
	 document.getElementById('sadd2').value = "";
	 document.getElementById('scity').value = "";
	 document.getElementById('szip').value = "";
	 document.getElementById('sLatitude').value = "";
	 document.getElementById('sLongitude').value = "";
	 document.getElementById('slandmark').value="";
	 document.getElementById("LMKey").value='';
	 document.getElementById("queueno").value='';
	 document.getElementById('userAddress').value ='';
	 document.getElementById("dontAddPickUp").value="";
	 initializeMap(3);markerDrag();
 }
 function clearDOAddressOR(){
	 document.getElementById('eadd1').value= "";
	 document.getElementById('eadd2').value= "";
	 document.getElementById('ecity').value= "";
	 document.getElementById('ezip').value= "";
	 document.getElementById('eLatitude').value= "";
	 document.getElementById('eLongitude').value= "";
	 document.getElementById('elandmark').value="";
	 document.getElementById("LMKey").value='';
	 document.getElementById('userAddressTo').value ='';
	 document.getElementById("dontAddDropOff").value="";
	 initializeMap(3);markerDragTo();
 }
 function clearOpenRequest(){
	 if(document.getElementById("ccAuthCode").value!=""){
		 var check=confirm("You have pending authorized CC. Want to void/return it?");
		 if(check==true){
			 var xmlhttp = null;
			 if (window.XMLHttpRequest)
			 {
				 xmlhttp = new XMLHttpRequest();
			 } else {
				 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			 }
			 var url = 'OpenRequestAjax';
			 var category="";
			 if(document.getElementById("transType").value=="Auth"){
				 category="&category=Void";
			 } else {
				 category="&category=Return";
			 }
			 var parameters='event=enterCCAuth&transId='+document.getElementById("ccAuthCode").value+category;
			 xmlhttp.open("POST",url, false);
			 xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			 xmlhttp.setRequestHeader("Content-length", parameters.length);
			 xmlhttp.setRequestHeader("Connection", "close");
			 xmlhttp.send(parameters);
			 var text = xmlhttp.responseText;
			 var resp=text.split("###");
			 var result=resp[0].split(";");
			 if(result[0].indexOf('Failed')>=0){
				 document.getElementById("errorPage").innerHTML="Failed to void transaction";
				 document.getElementById("errorPage").style.display="block";
				 document.getElementById("successPage").style.display="none";
				 return false;
			 } 
		 }
	 }
	 document.getElementById("successPage").style.display="none";
	 document.getElementById('phone').value="";
	 document.getElementById('name').value="";
	 document.getElementById('phoneSmall').value="";
	 document.getElementById('nameSmall').value="";
	 document.getElementById('eMail').value="";
	 document.getElementById('dispatchStatus').checked=false;
	 document.getElementById("provider").value="";
	 document.getElementById('sdate').value="";
	 document.getElementById('advanceTime').value="-1";
	 document.getElementById('payType').value="Cash";
	 document.getElementById('acct').value="";
	 document.getElementById('amt').value="";
	 document.getElementById('masterAddress').value = "";
	 document.getElementById('sadd1').value = "";
	 document.getElementById('sadd2').value = "";
	 document.getElementById('scity').value = "";
	 document.getElementById('szip').value = "";
	 document.getElementById('sLatitude').value = "";
	 document.getElementById('sLongitude').value = "";
	 document.getElementById('sadd1Small').value = "";
	 document.getElementById('sadd2Small').value = "";
	 document.getElementById('scitySmall').value = "";
	 document.getElementById('szipSmall').value = "";
	 document.getElementById('tripId').value = "";
	 document.getElementById('specialIns').value = "";
	 document.getElementById('specialIns1').value="";
	 document.getElementById("totalAddress").value="";
	 document.getElementById("dontAddPickUp").value="";
	 document.getElementById("dontAddDropOff").value="";
	 document.getElementById("roundTripExist").value="";
	 document.getElementById('Comments').value = "";
	 document.getElementById('Comments1').value = "";
	 document.getElementById('driver').value = "";
	 document.getElementById('DriverCabSwitch').value="";
	 document.getElementById('accountSuccess').value = "";
	 document.getElementById('eadd1').value= "";
	 document.getElementById('eadd2').value= "";
	 document.getElementById('ecity').value= "";
	 document.getElementById('ezip').value= "";
	 document.getElementById('eLatitude').value= "";
	 document.getElementById('eLongitude').value= "";
	 document.getElementById('eadd1Small').value= "";
	 document.getElementById('eadd2Small').value= "";
	 document.getElementById('ecitySmall').value= "";
	 document.getElementById('ezipSmall').value= "";
	 document.getElementById('fromDate').value= "";
	 document.getElementById('toDate').value= "";
	 document.getElementById('userAddress').value="";
	 document.getElementById('userAddressTo').value ='';
	 document.getElementById('slandmark').value="";
	 document.getElementById('elandmark').value="";
	 document.getElementById('slandmarkSmall').value="";
	 document.getElementById('elandmarkSmall').value="";
	 document.getElementById("hideTable").style.display = "none";
	 document.getElementById("hideJobsOnDays").style.display = "none";
	 document.getElementById("hideTableDriverComments").style.display = "none";
	 document.getElementById("hideTableComments").style.display = "none";
	 document.getElementById("callerExtra").style.display="none";
	 document.getElementById("hideDriver").style.display="none";
	 document.getElementById("roundTrip").checked=false;
	 document.getElementById("sharedRide").checked=false;
	 document.getElementById("distanceCalc").value="fastest";
//	 document.getElementById('sdateTrip').value="";
//	 document.getElementById('shrsTrip').value="";
	 document.getElementById('Sun').checked=false;
	 document.getElementById('Mon').checked=false;
	 document.getElementById('Tue').checked=false;
	 document.getElementById('Wed').checked=false;
	 document.getElementById('Thu').checked=false;
	 document.getElementById('Fri').checked=false;
	 document.getElementById('Sat').checked=false;
	 document.getElementById('cloneJob').checked=false;
	 document.getElementById('newLandMark1').checked=false;
	 document.getElementById('newLandMark2').checked=false;
	 document.getElementById("premiumCustomer").checked=false;
	 document.getElementById('noPhone').checked=false;
	 document.getElementById('keyPress').value="";
	 document.getElementById("sstate").value=document.getElementById("defaultState").value;
	 document.getElementById("estate").value=document.getElementById("defaultState").value;
	 document.getElementById("phoneError").style.display="none";
	 document.getElementById("repeatGroup").value='';
	 document.getElementById("multiJobUpdate").value='';
	 document.getElementById("driver").value='';
	 document.getElementById("LMKey").value='';
	 document.getElementById('numberOfPassenger').value='';
//	 document.getElementById('dropTime').value='0000';
	 document.getElementById('phoneVerification').value="";
	 document.getElementById("queueno").value='';
	 document.getElementById("switchFromToAddress").value='';
	 document.getElementById("detailsAutoPopulated1").innerHTML="";
	 document.getElementById("LandDetails").innerHTML="";
	 document.getElementById("completedJobs").innerHTML="";
	 document.getElementById("popUpPickUp").value="";
	 document.getElementById("popUpDropOff").value="";
	 document.getElementById('tripStatus').value="";
	 document.getElementById("updateAll").value="";
	 document.getElementById("numOfCabs").value="1";
	 document.getElementById("ccAuthCode").value="";
	 document.getElementById("tripIdAuto").value="";
	 document.getElementById("ccNumber").value="";
	 document.getElementById("expDate").value="";
	 document.getElementById("cvvCode").value="";
	 document.getElementById("nameCC").value="";
	 document.getElementById("zipCodeCC").value="";
	 document.getElementById("amtCC").value="";
	 document.getElementById("startAmtVoucher").value=document.getElementById("startAmtVoucher1").value;
	 document.getElementById("rpmVoucher").value="";
	 document.getElementById("callerPhone").value="";
	 document.getElementById("callerName").value="";
	 document.getElementById("refNum").value="";
	 document.getElementById("refNum1").value="";
	 document.getElementById("refNum2").value="";
	 document.getElementById("refNum3").value="";
	 document.getElementById("airName").value="";
	 document.getElementById("airNo").value="";
	 document.getElementById("airFrom").value="";
	 document.getElementById("airTo").value="";
	 document.getElementById("jobRating").value="1";
//	 document.getElementById("successPage").style.display="none";
	 document.getElementById("getTripId").disabled=false;
	 if(document.getElementById('defaultMeter').value!=""){
		 document.getElementById('meter').value=document.getElementById('defaultMeter').value;
	 }
	 $('#results').html("");
	 $('#roundTripTable').hide('slow');
	 if(document.getElementById("hideFlightInformation").style.display=="block"){
		 openFlightInformation();
	 }if(document.getElementById("HideText").style.display=="block"){
		 openPayment();
	 }if(document.getElementById("displayTableComments").style.display=="block"){
		 openDispatchComments();
	 }if(document.getElementById("timeTypeOR").value==1){
		 loadDate();
	 } else {
		 document.getElementById('shrs').value='Now';
		 document.getElementById('shrsSmall').value='Now';
		 var currentTime = new Date();
		 var month = currentTime.getMonth() + 1;
		 var day = currentTime.getDate();
		 var year = currentTime.getFullYear();
		 if(month<10){
			 month = "0"+month;
		 }
		 if(day<10){
			 day = "0"+day;
		 }
		 var date= month+ "/" + day + "/" +year ;
		 document.getElementById('sdate').value=date;
		 document.getElementById('sdateSmall').value=date;
	 }
	 document.getElementById('sdate').disabled=false;
	 document.getElementById('sdateSmall').disabled=false;
	 document.getElementById("phone").focus();
	 document.getElementById("specialIns1").style.color='gray';
	 document.getElementById("specialIns").style.color='gray';
	 document.getElementById("Comments").style.color='gray';
	 document.getElementById("Comments1").style.color='gray';
	 document.getElementById("fleet").value=document.getElementById("initialFleet").value;
//	 document.getElementById("successPage").style.display="none";
//	 document.getElementById("errorPage").style.display="none";
//	 document.getElementById("successPage1").style.display="none";
//	 document.getElementById("errorPage1").style.display="none";
//	 document.getElementById("phoneError").style.display="none";
	 $('#roundTripTable').hide('slow');
	 InitOR();clearAddress();clearCharges();initializeMap(3);markerDrag();changeFares();
	 var driverList=document.getElementById("drprofileSize");
	 var vehicleList=document.getElementById("vprofileSize");
	 for (var i=0; i<driverList.value;i++){
		 document.getElementById('dchk'+i).checked=false;
	 }
	 for (var i=0; i<vehicleList.value;i++){
		 document.getElementById('vchk'+i).checked=false;
	 }
	 colorChange();
 }
 function clearCharges(){
	 var table = document.getElementById("chargesTable");
	 var rowCount = table.rows.length;
	 var size=document.getElementById('fieldSize').value;
	 for(var i=0;i<Number(size);i++){
		 rowCount--;
		 table.deleteRow(rowCount);
	 }
	 document.getElementById("fieldSize").value="";
	 document.getElementById('amt').value="";
 }
 function clearSplIns(value){
	 if(value==1 && document.getElementById("specialIns").value=="Permanent Comments"){
		 document.getElementById("specialIns").value="";
	 } else if(value==2 && document.getElementById("specialIns1").value=="Temporary Comments") {
		 document.getElementById("specialIns1").value="";
	 }
 }
 function clearComments(value){
	 if(value==1 && document.getElementById("Comments").value=="Permanent Comments"){
		 document.getElementById("Comments").value="";
	 } else if(value==2 && document.getElementById("Comments1").value=="Temporary Comments") {
		 document.getElementById("Comments1").value="";
	 }
 }
 function voucherChangeOR(type){
	 if(type==1 && document.getElementById("payType").value!="VC"){
		 document.getElementById("acct").value="";
	 } else if(type==2 && document.getElementById("acct").value==""){
		 document.getElementById("payType").value="Cash";
	 }
	 document.getElementById('accountSuccess').value = "";
 }

 function clearAddress(){
	 document.getElementById('address').value="";
	 document.getElementById('endAddress').value="";
	 document.getElementById('addressRed').value="";
	 document.getElementById('endAddressRed').value="";
 }
 function dateValidation(){
	 var fromdate=document.getElementById("fromDate").value;
	 var todate=document.getElementById("toDate").value;
	 var curdate = document.getElementById("sdate").value;
	 if(document.getElementById("tripId").value==""){
		 if(document.getElementById("slandmark").value!="" && document.getElementById('LMKey').value=="" && document.getElementById("newLandMark1").checked==false){
			 document.getElementById("errorPage").innerHTML="Entered Landmark not found,select new Landmark or enter LM again and try";
			 document.getElementById("errorPage").style.display="block";
			 document.getElementById("successPage").style.display="none";
			 return false;
		 }
		 if(document.getElementById("elandmark").value!="" && document.getElementById('LMKey').value=="" && document.getElementById("newLandMark2").checked==false){
			 document.getElementById("errorPage").innerHTML="Entered D/O Landmark not found,select new Landmark or enter LM again and try";
			 document.getElementById("errorPage").style.display="block";
			 document.getElementById("successPage").style.display="none";
			 return false;
		 }
	 } 
	 if(document.getElementById("acct").value!="" && document.getElementById('accountSuccess').value == "" && document.getElementById("tripId")==""){
		 document.getElementById("errorPage").innerHTML="The Voucher You Have Entered Is Not Valid";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 return false;
	 }
//	 if(document.getElementById("airName").value !=""){
//		 code=document.getElementById("airName").value;
//		 var letters = /^[A-Za-z]+$/;  
//		 if(!code.match(letters) || code.length>2){  
//			 document.getElementById("errorPage").innerHTML="Airport code should contain only 2 digit character";
//			 document.getElementById("errorPage").style.display="block";
//			 document.getElementById("successPage").style.display="none";
//			 return false;  
//		 }  
//	 }
	 var spl="";  
	 var spl1="";
	 var cmt="";  
	 var cmt1="";
	 if(document.getElementById("specialIns").value !=""){
		 spl=document.getElementById("specialIns").value.length;
	 }
	 if(document.getElementById("specialIns1").value !=""){
		 spl1=document.getElementById("specialIns1").value.length;
	 }
	 if(spl+spl1 >249){
		 document.getElementById("errorPage").innerHTML="Sorry Driver Comments Can't Exceed 250 Characters";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 return false;
	 }
	 if(document.getElementById("Comments").value !=""){
		 cmt=document.getElementById("Comments").value.length;
	 }
	 if(document.getElementById("Comments1").value !=""){
		 cmt1=document.getElementById("Comments1").value.length;
	 }
	 if(cmt+cmt1 >249){
		 document.getElementById("errorPage").innerHTML="Sorry Operatot Comments Can't Exceed 250 Characters";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 return false;
	 }
	 if(document.getElementById('roundTrip').checked==true && document.getElementById('eadd1').value==""){
		 document.getElementById("errorPage").innerHTML="Round Trip Should Have D/O Address";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 return false;
	 }
//	 if(document.getElementById('roundTrip').checked==true && document.getElementById("shrs").value=="Now"){
//	 document.getElementById("errorPage").innerHTML="For round trip you should change 'Now' time";
//	 document.getElementById("errorPage").style.display="block";
//	 document.getElementById("successPage").style.display="none";
//	 return false;
//	 }

	 /* if(document.getElementById("airName").value !=""){
		 code=document.getElementById("airName").value;

		  var letters = /^[A-Za-z]+$/;  
		   if(code.match(letters) && document.getElementById('airName').value.length==2)  
		     {  
		      return true;  
		     }  
		   else  
		     {
		    document.getElementById("errorPage").innerHTML="Airport code should contain only 2 digit character";
		    document.getElementById("errorPage").style.display="block";
		    document.getElementById("errorPage").style.display="none";

			// alert("message");  
		     return false;  
		     } 
	 }*/
	 if(document.getElementById('dispatchStatus').checked==true && document.getElementById("driver").value!=""&& (document.getElementById('updateAll').value==0||document.getElementById('updateAll').value=="")){
		 document.getElementById("errorPage").innerHTML="You allocated a driver. Cannot give do not dispatch option";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 return false;
	 }	 
	 var currentTime = new Date();
	 var month = currentTime.getMonth() + 1;
	 var day = currentTime.getDate();
	 var year = currentTime.getFullYear();
	 if(month<10){
		 month = "0"+month;
	 }
	 if(day<10){
		 day = "0"+day;
	 }
	 var date= month+ "/" + day + "/" +year ;
	 if(document.getElementById("shrs").value=="Now" && curdate > date){
		 document.getElementById("errorPage").innerHTML="For future dates you should change 'Now' time";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 return false;
	 }
	 if(fromdate!="" || todate!=""){
		 if(document.getElementById("shrs").value=="Now"){
			 document.getElementById("errorPage").innerHTML="For multi jobs you should change 'Now' time";
			 document.getElementById("errorPage").style.display="block";
			 document.getElementById("successPage").style.display="none";
			 return false;
		 }
		 var curDate = curdate.substring(6,10)+curdate.substring(0, 2)+curdate.substring(3, 5);
		 var fromDate = fromdate.substring(6,10)+fromdate.substring(0, 2)+fromdate.substring(3, 5);
		 var toDate = todate.substring(6,10)+todate.substring(0, 2)+todate.substring(3, 5); 
		 if(Number(fromDate)>=Number(curDate) && Number(toDate)>Number(fromDate) ) {
			 if(document.getElementById('repeatGroup').value !="" && document.getElementById('repeatGroup').value!=0){
				 updateMultiJob("Do you want to update all the repeat job for this tripId?",1);
			 } else { 
				 enterOpenRequest();
			 }
			 return true; 
		 } else {
			 document.getElementById("errorPage").innerHTML="Check Your Repeat Job From And To Date";
			 document.getElementById("errorPage").style.display="block";
			 document.getElementById("successPage").style.display="none";
			 document.getElementById('fromDate').value= "";
			 document.getElementById('toDate').value= "";
			 return false;
		 }
	 } else {
		 if(document.getElementById('repeatGroup').value !="" && document.getElementById('repeatGroup').value!=0){
			 updateMultiJob("Do you want to update all the repeat job for this tripId?",1);
		 } else {
			 enterOpenRequest();
		 }
	 }	
 }
 function getZoneCharges(){
	 var sLat=document.getElementById("sLatitude").value;
	 var sLon=document.getElementById("sLongitude").value;
	 var eLat=document.getElementById("eLatitude").value;
	 var eLon=document.getElementById("eLongitude").value;
	 if(sLat!="" && sLon!="" && eLat!="" && eLon!=""){
		 var url = 'SystemSetupAjax?event=getZoneCharges&sLat='+sLat+'&sLon='+sLon+'&eLat='+eLat+'&eLon='+eLon;
		 xmlhttp.open("GET", url, false);
		 xmlhttp.send(null);
		 var text = xmlhttp.responseText;
		 var jsonObj = "{\"address\":"+text+"}" ;
		 var obj = JSON.parse(jsonObj.toString());
		 if(obj.address.length>0){
			 for(var j=0;j<obj.address.length;j++){
				 var chargeForZone=obj.address[j].AM;
				 $('#results').html("Charge From Zone "+obj.address[j].SZ+" To "+obj.address[j].EZ+" Is $"+chargeForZone);
				 document.getElementById('amt').value=chargeForZone;
				 changeChargesOR();
			 }
		 }
	 } else {
		 $('#results').html( "Both Pickup and Dropoff Address Are Mandatory" );
	 }
 }

 function checkKeyPress(){
	 document.getElementById('keyPress').value="1";
	 document.getElementById("errorPage").style.display="none";
	 document.getElementById("successPage").style.display="none";
	 document.getElementById("phoneError").style.display="none";
 }
 var mins,secs,TimerRunning,TimerID,TimerID1;
 TimerRunning=false,i=1;

 function InitOR()
 {
	 mins=1;
	 secs=0;
	 callerId();
	 StopTimerOR();
 }
 function StopTimerOR()
 {
	 if(TimerRunning)
		 clearTimeout(TimerID);
	 TimerRunning=false;
	 secs= document.getElementById('secOR').value;
	 StartTimerOR();
 }
 function StartTimerOR()
 {
	 TimerRunning=true;
	 secs--;
	 document.getElementById('tidOR').innerHTML = secs;
	 TimerID=self.setTimeout("StartTimerOR()",1000);
	 if(secs==0)
	 {
		 if(document.getElementById('keyPress').value==""){
			 StopTimerOR();
			 callerId();
		 }
		 else {
			 StopTimerOR();
		 }
	 }
 }
 function showLongDesc(description){
	 $("#phoneError").html(description).show().fadeIn("slow").fadeOut("slow").fadeIn("slow").fadeOut("slow");  
 }

 function noShowPhone(){
	 if(document.getElementById('noPhone').checked==true){
		 document.getElementById('phoneTemp').value=document.getElementById('phone').value;
		 document.getElementById('phone').value="0000000000";
	 } else {
		 document.getElementById('phone').value=document.getElementById('phoneTemp').value;
	 }

 }	 
 function checkCabOrDriver(){
	 var driver="";
	 var cab="";
	 if(document.getElementById("driverORcab").value=="Driver"){
		 driver=document.getElementById("driver").value;
	 } else {
		 cab=document.getElementById("driver").value;
	 }
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 if(driver!="" || cab!=""){
		 var url = 'RegistrationAjax';
		 var parameters='event=checkDriverOrCab&driverId='+driver+'&cabNo='+cab;
		 xmlhttp.open("POST",url, false);
		 xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		 xmlhttp.setRequestHeader("Content-length", parameters.length);
		 xmlhttp.setRequestHeader("Connection", "close");
		 xmlhttp.send(parameters);
		 var text = xmlhttp.responseText;
		 var resp=text.split("###");
		 var result=resp[0].split(";");
		 if(result[0].indexOf('001')>=0){
			 document.getElementById("successPage").innerHTML=result[1];
			 document.getElementById("successPage").style.display="block";
			 document.getElementById("errorPage").style.display="none";
		 }else {
			 document.getElementById("errorPage").innerHTML=result[1];
			 document.getElementById("errorPage").style.display="block";
			 document.getElementById("successPage").style.display="none";
		 }
	 }
 }
 function getCompletedJobs(){
	 var phone = document.getElementById('phone').value;
	 if (phone != "") {
		 var xmlhttp = null;
		 var mydivland = document.getElementById('LandDetails');
		 mydivland.style.display = 'none';
		 if (window.XMLHttpRequest)
		 {
			 xmlhttp = new XMLHttpRequest();
		 } else {
			 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var url = 'OpenRequestAjax?event=getCompletedJobs&phone='+phone;
		 xmlhttp.open("GET", url, false);
		 xmlhttp.send(null);
		 var text = xmlhttp.responseText;
		 var resp = text.split('###');
		 document.getElementById('completedJobs').innerHTML = resp[1];
		 if (resp[0].indexOf('NumRows=2')>0) {
			 document.getElementById('completedJobs').style.display = 'none';
			 document.getElementById('tableCount').value="5";
			 if(document.getElementById("ORFormat").value!="10"){
				 $('#completedJobs').jqmShow();
			 } else {
				 $( "#completedJobs" ).dialog({
					 modal: true,
					 width: 930
				 });
			 }
		 }
	 }
	 return true;
 }
 function enterComplaint(phoneNum){
	 if(document.getElementById("ORFormat").value!="10"){
		 $('#completedJobs').jqmHide();
	 } else {
		 $("#completedJobs" ).dialog( "close" );
	 }
	 window.open("control?action=CustomerService&event=createComplaints&module=operationView","_self");
	 document.getElementById("passengerPhone").value=phoneNum;
 }
 function enterCC(value){
	 if(document.getElementById("tripIdAuto").value==""){
		 document.getElementById("errorPage").innerHTML="Get TripId First";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
		 return false;
	 } else {
		 if(value==1){
			 if(document.getElementById("ORFormat").value!="10"){
				 $('#preAuthCC').jqm();
				 $('#preAuthCC').jqmShow();
			 } else {
				 $( "#preAuthCC" ).dialog({
					 modal: true,
					 width: 630
				 });
			 }
			 var amount1=document.getElementById("amt").value;
			 if(amount1!=""){
				 document.getElementById("amtCC").value=amount1;
			 }
		 } else {
			 var tripID=document.getElementById("tripIdAuto").value;
			 var ccNum=document.getElementById("ccNumber").value;
			 var expDate=document.getElementById("expDate").value;
			 var cvvCode=document.getElementById("cvvCode").value;
			 var name=document.getElementById("nameCC").value;
			 var zipCode=document.getElementById("zipCodeCC").value;
			 var amount=document.getElementById("amtCC").value;
			 var category=document.getElementById("category").value;
			 var transId="";
			 var amt=amount;
			 if(document.getElementById("ccAuthCode").value!=""){
				 transId="&transId="+document.getElementById("ccAuthCode").value;
			 }
			 var xmlhttp = null;
			 if (window.XMLHttpRequest)
			 {
				 xmlhttp = new XMLHttpRequest();
			 } else {
				 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			 }
			 var url = 'OpenRequestAjax';
			 var parameters='event=enterCCAuth&tripId='+tripID+'&ccNumber='+ccNum+'&cvvCode='+cvvCode+'&expDate='+expDate+'&name='+name+'&zipCode='+zipCode+'&amount='+amount+'&category='+category+transId;
			 xmlhttp.open("POST",url, false);
			 xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			 xmlhttp.setRequestHeader("Content-length", parameters.length);
			 xmlhttp.setRequestHeader("Connection", "close");
			 xmlhttp.send(parameters);
			 var text = xmlhttp.responseText;
			 var resp=text.split("###");
			 var result=resp[0].split(";");
			 if(result[0].indexOf('001')>=0){
				 document.getElementById("ccAuthCode").value=result[1];
				 document.getElementById("transType").value=category;
				 /*					 document.getElementById("tripIdAuto").value="";
					 document.getElementById("ccNumber").value="";
					 document.getElementById("expDate").value="";
					 document.getElementById("cvvCode").value="";
					 document.getElementById("nameCC").value="";
					 document.getElementById("zipCodeCC").value="";
					 document.getElementById("amtCC").value="";
				  */					 
				 document.getElementById("preAuth").disabled=true;
				 document.getElementById("getTripId").disabled=true;
				 document.getElementById("successPage").innerHTML="Your Card Processed Successfully";
				 document.getElementById("successPage").style.display="block";
				 document.getElementById("errorPage").style.display="none";
				 $("#phoneError").html("Authorised Amount Is: $" + amount).show();  
			 } else {
				 document.getElementById("errorPage").innerHTML=result[1];
				 document.getElementById("errorPage").style.display="block";
				 document.getElementById("successPage").style.display="none";
			 }
			 //$('#preAuthCC').jqmHide();
			 $("#preAuthCC" ).dialog( "close" );
			 $('#preAuthCC').hide();
		 }
	 }
 }
 function removeShowMap(){
	 if(document.getElementById("ORFormat").value!="10"){
			$('#finalMapScreen').jqmHide();		 
	 } else {
		 document.getElementById("finalMapScreen").style.display="none";
	 }
 }
 function getAdvScreen(){
	 document.getElementById("advancedScreen").style.display="block";
	 document.getElementById("reducedScreen").style.display="none";
	 document.getElementById('phone').value=document.getElementById('phoneSmall').value;
	 document.getElementById('name').value=document.getElementById('nameSmall').value;
	 document.getElementById('sadd1').value = document.getElementById('sadd1Small').value;
	 document.getElementById('sadd2').value = document.getElementById('sadd2Small').value;
	 document.getElementById('scity').value = document.getElementById('scitySmall').value;
	 document.getElementById('szip').value = document.getElementById('szipSmall').value;
	 document.getElementById('eadd1').value = document.getElementById('eadd1Small').value;
	 document.getElementById('eadd2').value = document.getElementById('eadd2Small').value;
	 document.getElementById('ecity').value = document.getElementById('ecitySmall').value;
	 document.getElementById('ezip').value = document.getElementById('ezipSmall').value;
	 document.getElementById('shrs').value = document.getElementById('shrsSmall').value;
	 document.getElementById('sdate').value = document.getElementById('sdateSmall').value;
	 document.getElementById('slandmark').value = document.getElementById('slandmarkSmall').value;
	 document.getElementById('elandmark').value = document.getElementById('elandmarkSmall').value;
	 initializeMap(3);markerDrag();InitOR();
 }	 
 function getSmallScreen(){
	 document.getElementById("advancedScreen").style.display="none";
	 document.getElementById("reducedScreen").style.display="block";
	 document.getElementById('phoneSmall').value=document.getElementById('phone').value;
	 document.getElementById('nameSmall').value=document.getElementById('name').value;
	 document.getElementById('sadd1Small').value = document.getElementById('sadd1').value;
	 document.getElementById('sadd2Small').value = document.getElementById('sadd2').value;
	 document.getElementById('scitySmall').value = document.getElementById('scity').value;
	 document.getElementById('szipSmall').value = document.getElementById('szip').value;
	 document.getElementById('eadd1Small').value = document.getElementById('eadd1').value;
	 document.getElementById('eadd2Small').value = document.getElementById('eadd2').value;
	 document.getElementById('ecitySmall').value = document.getElementById('ecity').value;
	 document.getElementById('ezipSmall').value = document.getElementById('ezip').value;
	 document.getElementById('shrsSmall').value = document.getElementById('shrs').value;
	 document.getElementById('sdateSmall').value = document.getElementById('sdate').value;
	 document.getElementById('slandmarkSmall').value = document.getElementById('slandmark').value;
	 document.getElementById('elandmarkSmall').value = document.getElementById('elandmark').value;
 }
 function submitSmall(){
	 document.getElementById('phone').value=document.getElementById('phoneSmall').value;
	 document.getElementById('name').value=document.getElementById('nameSmall').value;
	 document.getElementById('sadd1').value = document.getElementById('sadd1Small').value;
	 document.getElementById('sadd2').value = document.getElementById('sadd2Small').value;
	 document.getElementById('scity').value = document.getElementById('scitySmall').value;
	 document.getElementById('szip').value = document.getElementById('szipSmall').value;
	 document.getElementById('eadd1').value = document.getElementById('eadd1Small').value;
	 document.getElementById('eadd2').value = document.getElementById('eadd2Small').value;
	 document.getElementById('ecity').value = document.getElementById('ecitySmall').value;
	 document.getElementById('ezip').value = document.getElementById('ezipSmall').value;
	 document.getElementById('sdate').value = document.getElementById('sdateSmall').value;
	 document.getElementById('shrs').value = document.getElementById('shrsSmall').value;
	 document.getElementById('slandmark').value = document.getElementById('slandmarkSmall').value;
	 document.getElementById('elandmark').value = document.getElementById('elandmarkSmall').value;
	 dateValidation();
 }
 function showDropSmall(){
	 var img=document.getElementById("imgReplace").src;
	 if(img.indexOf("plus_icons.png")>=0){
		 document.getElementById("dropSmall").style.display="block";
		 document.getElementById("imgReplace").src="images/minus_icons.png";
	 } else {
		 document.getElementById("dropSmall").style.display="none";
		 document.getElementById("imgReplace").src="images/plus_icons.png";
	 }
 }
 function changeTime(){
	 document.getElementById('shrs').value = '0000';
 }
 function callAmount(){
	 var size=document.getElementById('fieldSize').value;
	 document.getElementById('amt').value="";
	 for(var j=0;j<size;j++){
		 var i=j+1;
		 var firstValue=document.getElementById('amt').value;
		 var newValue=document.getElementById("amountDetail"+i).value;
		 if(firstValue==""){
			 firstValue="0";
		 }
		 if(isNaN(newValue)){
			alert("Enter numeric value");
			document.getElementById("amountDetail"+i).value="";
		}
		 document.getElementById('amt').value=parseFloat(firstValue)+parseFloat(newValue);
		 document.getElementById('amt').value=Math.round((document.getElementById('amt').value)*100)/100;
	 }
 }
 function getTripForSplRequest(){
	 var date=document.getElementById("sdate").value;
	 var driverList=document.getElementById("drprofileSize");
	 var vehicleList=document.getElementById("vprofileSize");
	 var splFlag="";
	 if(typeof driverList != undefined && driverList!=null){
		 for (var i=0; i<driverList.value;i++){
			 if(document.getElementById('dchk'+i).checked==true){
				 splFlag=splFlag+document.getElementById('dchk'+i).value;
			 }
		 }
	 }
	 if(typeof vehicleList != undefined &&vehicleList!=null){
		 for (var i=0;i<vehicleList.value;i++){
			 if(document.getElementById('vchk'+i).checked==true){
				 splFlag=splFlag+document.getElementById('vchk'+i).value;
			 }
		 }
	 }
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 var url = 'OpenRequestAjax';
	 var parameters='event=jobsForThisSplFlag&splFlags='+splFlag+'&date='+date;
	 xmlhttp.open("POST",url, false);
	 xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	 xmlhttp.setRequestHeader("Content-length", parameters.length);
	 xmlhttp.setRequestHeader("Connection", "close");
	 xmlhttp.send(parameters);
	 var text = xmlhttp.responseText;
	 if(text==""){
		 return;
	 }
	 var obj = "{\"jobList\":" + text + "}";
	 jsonObj = JSON.parse(obj.toString());
	 document.getElementById("tripsForFlags").innerHTML="";
	 $("#tripsForFlags").append("<table style='width:100%'>");
	 $("#tripsForFlags").append("<tr  style='border:1px solid gray;'>");
	 $("#tripsForFlags").append("<th  style='border:1px solid gray;'>Trip ID</th>");
	 $("#tripsForFlags").append("<th  style='border:1px solid gray;'>Start Address</th>");
	 $("#tripsForFlags").append("<th  style='border:1px solid gray;'>End Address</th>");
	 $("#tripsForFlags").append("<th  style='border:1px solid gray;'>Time</th>");
	 $("#tripsForFlags").append("</tr>");
	 for ( var i = 0; i < jsonObj.jobList.length; i++) {
		 $("#tripsForFlags").append("<tr  style='border:1px solid gray;'>");
		 $("#tripsForFlags").append(
				 "<td  style='border:1px solid gray;'>" + jsonObj.jobList[i].TI+ "</td>");
		 $("#tripsForFlags").append(
				 "<td  style='border:1px solid gray;'>" + jsonObj.jobList[i].SA+ "</td>");
		 $("#tripsForFlags").append(
				 "<td  style='border:1px solid gray;'>" + jsonObj.jobList[i].EA+ "</td>");
		 $("#tripsForFlags").append(
				 "<td  style='border:1px solid gray;'>" + jsonObj.jobList[i].ST+ "</td>");
		 $("#tripsForFlags").append("</tr>");
	 }
	 $("#tripsForFlags").append("</table>");	
	 if(document.getElementById("ORFormat").value!="10"){
		 $("#tripsForFlags").jqm();
		 $("#tripsForFlags").jqmShow();
	 } else {
		 $( "#tripsForFlags" ).dialog({
			 modal: true,
			 width: 630
		 });
	 }
	 document.getElementById('tableCount').value ="7";

 }
 function doConfirm(msg, yesFn, noFn)
 {
	 var confirmBox = $("#confirmBox");
	 confirmBox.find(".message").text(msg);
	 confirmBox.find(".yes,.no").unbind().click(function()
			 {
		 confirmBox.hide();
			 });
	 confirmBox.find(".yes").click(yesFn);
	 confirmBox.find(".no").click(noFn);
	 confirmBox.show();
 }
 function doConfirmJobs(msg, yesFn, noFn)
 {
	 var confirmBox = $("#confirmBoxJobs");
	 confirmBox.find(".message").text(msg);
	 confirmBox.find(".yes,.no").unbind().click(function()
			 {
		 confirmBox.hide();
			 });
	 confirmBox.find(".yes").click(yesFn);
	 confirmBox.find(".no").click(noFn);
	 confirmBox.show();
 }
 function driverAllocationCheck(){
	 if(document.getElementById('tripStatus').value !="" && ((document.getElementById('tripStatus').value>=40 && document.getElementById('tripStatus').value<50)||document.getElementById('tripStatus').value==37||document.getElementById('tripStatus').value==90) && document.getElementById('tripId').value!=""){
		 doConfirm("This an allocated job. Do you want to clear the Driver/Cab information?", function yes()
		 {
			 document.getElementById('updateAll').value=1;
			 if(document.getElementById("advancedScreen").style.display == "none") {
				 submitSmall();
			 } else {
				 dateValidation();
			 }
		 }, function no()
		 {
			 document.getElementById('updateAll').value=0;
			 if(document.getElementById("advancedScreen").style.display == "none") {
				 submitSmall();
			 } else {
				 dateValidation();
			 }
		 });
	 } else {
		 if(document.getElementById("advancedScreen").style.display == "none") {
			 submitSmall();
		 } else {
			 dateValidation();
		 }
	 }
 }
 function updateMultiJob(message,type){
	 doConfirmJobs(message, function yes()
	 {
		 document.getElementById('multiJobUpdate').value=1;
		 if(type==1){
			 enterOpenRequest();
		 } else {
			 newDelete();
		 }
	 }, function no()
	 {
		 document.getElementById('multiJobUpdate').value=0;
		 if(type==1){
			 enterOpenRequest();
		 } else {
			 newDelete();
		 }
	 });
 }
 function deleteJobsPopUp(screen,tripId,row) {
	 if(screen==1 || screen.value==1){
		 tripId=document.getElementById("tripId"+row).value;
	 }
	 $("#detailsAutoPopulated1" ).dialog( "close" );
	 $("#detailsAutoPopulated1").hide();
	 if(tripId==""){
		 document.getElementById("errorPage").innerHTML="This is not an existing job";
		 document.getElementById("errorPage").style.display="block";
		 document.getElementById("successPage").style.display="none";
	 } else {
		 if(document.getElementById('repeatGroup'+row).value !="" && document.getElementById('repeatGroup'+row).value!=0){
			 document.getElementById('repeatGroup').value=document.getElementById('repeatGroup'+row).value;
			 updateMultiJob("Do you want to delete all the repeat job for this tripId?",2);
		 } else {
			 newDeleteDash(tripId,document.getElementById("fleetCode"+row).value);
		 }
	 }
 }


 var tripIdForStatus="";
 var secsStatus="",TimerIDStatus;

 function callDriverForStatusOR(tripId){
	 if(secsStatus=="0" || secsStatus==""){
		 var xmlhttp = null;
		 if (window.XMLHttpRequest)
		 {
			 xmlhttp = new XMLHttpRequest();
		 } else {
			 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		 }
		 var url = 'OpenRequestAjax?event=callDriver&tripId='+tripId;
		 xmlhttp.open("GET", url, false);
		 xmlhttp.send(null);
		 $("#detailsAutoPopulated1" ).dialog( "close" );
		 $("#detailsAutoPopulated1").hide();
		 document.getElementById("errorPage").innerHTML="";
		 StopTimerStatus();
		 tripIdForStatus=tripId;
	 } else {
		 alert("Your previous request is processing.Plz wait");
	 }
 }
 function StopTimerStatus()
 {
	 secsStatus= "20";
	 clearTimeout(TimerIDStatus);
	 StartTimerStatus();
 }
 function StartTimerStatus()
 {
	 if(secsStatus!=""){
		 secsStatus--;
		 document.getElementById('errorPage').innerHTML = "The Status Of Trip "+tripIdForStatus+" Will Come Here In "+secsStatus+" Secs";
		 document.getElementById("errorPage").style.display="block";
		 TimerIDStatus=self.setTimeout("StartTimerStatus()",1000);
		 if(secsStatus==0)
		 {
			 getJobStatusFromDriver();
		 }
	 }
 }
 function getJobStatusFromDriver(){
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 var url = 'OpenRequestAjax?event=readJobStatus&tripId='+tripIdForStatus;
	 xmlhttp.open("GET", url, false);
	 xmlhttp.send(null);
	 var text = xmlhttp.responseText;
	 var resp=text.split("###");
	 var result=resp[0];
	 if(result==""){
		 result="Driver didnt reply/Internet failure error";
	 }
	 secsStatus="";
	 document.getElementById("errorPage").innerHTML="";
	 $("#errorPage").append(resp[0]);
	 document.getElementById("errorPage").style.display="block";
 }
 function conferenceCall(){
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 var url = 'RegistrationAjax?event=driverConference&phoneNumber='+phoneNumberPublic;
	 xmlhttp.open("GET", url, false);
	 xmlhttp.send(null);
	 var text = xmlhttp.responseText;
	 $("#showHover").hide("slow");
	 if(text.indexOf("001")>=0){
		 document.getElementById("errorPage").innerHTML="Hang up the line. Drievr and passenger will receive call in 1 min";
		 document.getElementById("errorPage").style.display="block";
		 phoneNumberPublic="";
	 } else {
		 document.getElementById("errorPage").innerHTML="Driver not allocated with the job / Invalid phoneNumber";
		 document.getElementById("errorPage").style.display="block";
	 }
 }
 function checkFlagGroup(type,row,groupId,longDesc){
	 var drProf=$("#drprofileSize").val();
	 var veProf=$("#vprofileSize").val();
	 if(type==1 && groupId!="" && groupId!="0"){
		 if(document.getElementById("dchk"+row).checked==true){
			 for(var i=0;i<drProf;i++){
				 if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
					 document.getElementById("dchk"+row).checked=false;
					 alert("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
					 break;
				 }
			 }
			 for(var i=0;i<veProf;i++){
				 if(document.getElementById("vchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
					 document.getElementById("dchk"+row).checked=false;
					 alert("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
					 break;
				 }
			 }
		 }
	 } else if(type==2 && groupId!="" && groupId!="0"){
		 if(document.getElementById("vchk"+row).checked==true){
			 for(var i=0;i<veProf;i++){
				 if(document.getElementById("vchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
					 document.getElementById("vchk"+row).checked=false;
					 alert("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
					 break;
				 }
			 }
			 for(var i=0;i<drProf;i++){
				 if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
					 document.getElementById("vchk"+row).checked=false;
					 alert("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
					 break;
				 }
			 }
		 }
	 }
 }
 
 function checkZone(lati,longi,type){
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 var url = 'RegistrationAjax?event=checkZone&latitude='+lati+'&longitude='+longi;
	 xmlhttp.open("GET", url, false);
	 xmlhttp.send(null);
	 var text = xmlhttp.responseText;
	 if(text!=""){
		 if(type=="1"){
			 document.getElementById("queueno").value=text;
		 } else {
			 document.getElementById("endQueue").value=text;
		 }
	 }
 }
 
 var minutes,secondsNew,TimerRunningOR,TimerIDOR,TimerID1OR;
 TimerRunningOR=false,j=1;

 function timerStartNew()//call the Init function when u need to start the timer
 {
	 minutes=1;
	 seconds=0;
	 StopTimerNew();
 }
 function StopTimerNew()
 {
	 if(TimerRunningOR)
		 clearTimeout(TimerIDOR);
	 TimerRunningOR=false;
	 secondsNew= 10;
	 StartTimerNew();
 }
 function clearTime(){
	 clearTimeout(TimerIDOR);
	 TimerRunningOR=false;
 }
 function StartTimerNew()
 {
	 TimerRunningOR=true;
	 TimerIDOR=self.setTimeout("StartTimerNew()",1000);
	 if(secondsNew==0)
	 {
		 $("#showHover").hide("slow");
	 }
	 secondsNew--;
 }

 function ORSummary(status){
		var keyWord;
		var fDate;
		var tDate;
		if(status==0){
			keyWord=document.getElementById("keyWord").value;
			fDate=document.getElementById("fDate").value;
			tDate=document.getElementById("tDate").value;
			$("#ORSummary").jqm();
			$("#ORSummary").jqmShow();
		} else if(status==10) {
			keyWord=document.getElementById("keyWord").value;
			fDate=document.getElementById("fDate").value;
			tDate=document.getElementById("tDate").value;
		} else {
			keyWord=document.getElementById("ph.Number").value;
			document.getElementById("keyWord").value=keyWord;
			fDate="";
			tDate="";
			document.getElementById("fDate").value="";
			document.getElementById("tDate").value="";
			$("#ORSummary").jqm();
			$("#ORSummary").jqmShow();
		}
		var url = 'control?action=openrequest&event=openRequestSummary&button=YES&search='+keyWord+'&fDate='+fDate+'&tDate='+tDate+'&status='+status;
		if(document.getElementById("account").checked){
			url=url+"&account=YES";
			account=1;
		}else if(document.getElementById("repeatJobs").checked){
			url=url+"&repeatJobs=YES";
			repeatJobs=1;
		}
		if(status!=10){
			var ajax = new AjaxInteraction(url,responseSummary);
			ajax.doGet();
		} else {
			window.open(url, 'reservedJobs');
		}
	}
	function responseSummary(responseText){
		keyWord=document.getElementById("keyWord").value;
		fDate=document.getElementById("fDate").value;
		tDate=document.getElementById("tDate").value;
		document.getElementById("ORSummary").innerHTML=responseText;
		document.getElementById("keyWord").value=keyWord;
		document.getElementById("fDate").value=fDate;
		document.getElementById("tDate").value=tDate;
		if(account==1){
			document.getElementById("account").checked=true;
			account=0;
		}else if(repeatJobs==1){
			document.getElementById("repeatJobs").checked=true;
			repeatJobs=0;
		}else{
			document.getElementById("all").checked=true;
		}
	}

	function showHistory(type){
		$("#historyJobs").jqm();
		$("#historyJobs").jqmShow();
	}
	
	function showRates(){
		document.getElementById("showzoneRates").style.display="block";
	}
	
	function removeZoneRateOR(){
		document.getElementById("showzoneRates").style.display="none";
	}
	function printCurrentJob(tripId){
		window.open("ReportController?event=reportEvent&ReportName=jobMail&output=pdf&tripId="+tripId+"&type=basedMail&assoccode="+document.getElementById("masterAssoccode").value+"&timeOffset="+document.getElementById("timeZone").value);
	}
	function showDriverRoute(){
		distance();
		callDriverRoute();
	}
	var disVenki="";
	function distance(){
		var authenticated = "";
		var origin= document.getElementById("sLatitude").value+","+document.getElementById("sLongitude").value;
		var destination= document.getElementById("eLatitude").value+","+document.getElementById("eLongitude").value;
		if(document.getElementById("sLatitude").value!="" && document.getElementById("eLatitude").value!=""){
			var currencyPrefix=document.getElementById("currency").value;
			var amount=document.getElementById("tripAmount").value;
			var startAmt=document.getElementById("startAmtVoucher").value;
			var dis1=document.getElementById("disN1").value;
			var dis2=document.getElementById("disN2").value;
			var dis3=document.getElementById("disN3").value;
			var rate1=document.getElementById("rateN1").value;
			var rate2=document.getElementById("rateN2").value;
			var rate3=document.getElementById("rateN3").value;
			var extraData="";
			var distanceBy=document.getElementById("distanceCalc").value;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'OpenRequestAjax?event=getDistFMQA&from='+origin+'&to='+destination+'&routeType='+distanceBy;
			//var url = 'https://open.mapquestapi.com/directions/v1/route?key=ZX3FD32ZdJ8LfqzB2cKPMD6WDiuSbwTE&outFormat=json&routeType='+distanceBy+'&timeType=1&enhancedNarrative=false&locale=en_US&unit=m&from='+origin+'&to='+destination+'&drivingStyle=2&highwayEfficiency=21.0';
			if(document.getElementById("masterAssoccode").value=="104"){
				var service = new google.maps.DistanceMatrixService();
				service.getDistanceMatrix(
						{
							origins: [new google.maps.LatLng(document.getElementById("sLatitude").value,document.getElementById("sLongitude").value)],
							destinations: [new google.maps.LatLng(document.getElementById("eLatitude").value,document.getElementById("eLongitude").value)],
							travelMode: google.maps.TravelMode.DRIVING,
							unitSystem: google.maps.UnitSystem.METRIC,
							avoidHighways: false,
							avoidTolls: false
						}, callback);
			} else {
				xmlhttp.open("GET", url, false);
				xmlhttp.send(null);
				var text = xmlhttp.responseText;
				if(text=="" || text == "GRACIERROR" || text=="AUTH_FAILED"){
					authenticated = text;
					disVenki=0.0;
				}else{
					authenticated = "Yes"
					var condition=text.split('"distance":');
					var finalDistance=condition[1].split(',');
					disVenki=Math.round((finalDistance[0])*100)/100;
				}
			}
			setTimeout(function(){
				if(authenticated!="Yes"){
					if(authenticated=="" || authenticated=="GRACIERROR"){
						$('#results').html("Problem on getting distance");
					}else{
						$('#results').html("Authentication failed for getting distance");
					}
				}else{
					var tripAmount="";
					var disCalculated=false;
					if(dis1!="0" && dis1!=""){
						amount=disVenki*rate1;
						disCalculated=true;
					}
					if(disVenki-dis2>0 && dis2!="0" && dis2!=""){
						amount=amount-((disVenki-dis2)*rate2);
						disCalculated=true;
					}
					if(disVenki-dis3>0 && dis3!="0" && dis3!=""){
						amount=amount-((disVenki-dis3)*rate3);
						disCalculated=true;
					}
					if(disCalculated){
						tripAmount=(amount)+Number(startAmt);
						tripAmount = Math.floor((Number(tripAmount)*100)/25);
						tripAmount = ((Number(tripAmount)+1)*25)/100;
						tripAmount = Math.round(tripAmount*100)/100;
					} else {
						tripAmount=(disVenki*amount)+Number(startAmt);
						tripAmount = Math.floor((Number(tripAmount)*100)/25);
						tripAmount = ((Number(tripAmount)+1)*25)/100;
						tripAmount = Math.round(tripAmount*100)/100;
					}
					if(document.getElementById("startAmtVoucher").value!=""){
						extraData=" (Start Amount "+currencyPrefix+""+document.getElementById("startAmtVoucher").value+")";
					} if(document.getElementById("rpmVoucher").value!=""){
						amount=Number(document.getElementById("rpmVoucher").value);
					}
					var result=disVenki+" Miles & Approximate Amount "+currencyPrefix+""+tripAmount+" ("+currencyPrefix+""+amount+"/Mile)"+extraData;
					$('#results').html(result);
					if(document.getElementById("fareByDistance").value=="1"){
						document.getElementById('amt').value=tripAmount;
						changeChargesOR();
					}
				}
			},400);
		} else {
			$('#results').html("Both Pickup and Dropoff Address Are Mandatory");
		}
	}
  	function callback(response, status) {
  		if (status == google.maps.DistanceMatrixStatus.OK) {
  			var origins = response.originAddresses;
  			for (var i = 0; i < origins.length; i++) {
  				var results = response.rows[i].elements;
  				for (var j = 0; j < results.length; j++) {
  					var result = results[j].distance.text.split(" ");
  					disVenki = Math.round((Number(result[0])/1.60934)*100)/100;
  				}
  			}
  		}
  	}
  	
  	 function changeFares(){
  		if(document.getElementById("meter").value!=""){
  			var j=Number(document.getElementById("meter").value)-1;
  			document.getElementById("startAmtVoucher").value=jsonObjects.meterDetails[j].SA;
  			document.getElementById("tripAmount").value=jsonObjects.meterDetails[j].RMl;
  			document.getElementById("disN1").value=jsonObjects.meterDetails[j].D1;
  			document.getElementById("disN2").value=jsonObjects.meterDetails[j].D2;
  			document.getElementById("disN3").value=jsonObjects.meterDetails[j].D3;
  			document.getElementById("rateN1").value=jsonObjects.meterDetails[j].R1;
  			document.getElementById("rateN2").value=jsonObjects.meterDetails[j].R2;
  			document.getElementById("rateN3").value=jsonObjects.meterDetails[j].R3;
  		 }
  	 }

  	
  	function showSearchOption(type){
  		if(type=="1"){
  			document.getElementById("jobSear").value="";
  			$("#searchForJob" ).dialog({
  				modal: true,
  				width: 1030,
  			});
  		} else {
  			if(document.getElementById("jobSear").value!=""){
  				var curdate = new Date();
  				var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();		
  				var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
  				var cYear = curdate.getFullYear();
  				
  				var prevDate = new Date();
  				prevDate.setDate(prevDate.getDate() - 7);
  				var fDate = prevDate.getDate() >= 10 ? prevDate.getDate() : "0"+prevDate.getDate();		
  				var fMonth = prevDate.getMonth()+1 >=10 ? prevDate.getMonth()+1 : "0"+(prevDate.getMonth()+1);
  				var fYear = prevDate.getFullYear();

  				var fromDate =fMonth+"/"+fDate+"/"+fYear;
  				var toDate =cMonth+"/"+cDate+"/"+cYear;

  				var xmlhttp = null;
  				if (window.XMLHttpRequest)
  				{
  					xmlhttp = new XMLHttpRequest();
  				} else {
  					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  				}
  				var url = 'RegistrationAjax?event=searchJobs&value='+document.getElementById("jobSear").value+'&fD='+fromDate+'&tD='+toDate;
  				xmlhttp.open("GET", url, false);
  				xmlhttp.send(null);
  				var text = xmlhttp.responseText;
  				var jsonObj = "{\"address\":"+text+"}" ;
  				var obj = JSON.parse(jsonObj.toString());
  				var tbRows = "";
  				var tableAppend=  document.getElementById("myJobsToSearch");
  				document.getElementById("myJobsToSearch").innerHTML="";
  				if(obj.address[0].summ.length>0){
  					tbRows = tbRows + '<thead><tr><th colspan="7"><center>Current & Future Jobs</center></th><tr><th class="showDShTripId">TripId</th><th class="showDSHName">Name</th><th class="showDSHDate">Date</th><th class="showDSHAddress">St&nbsp;Address</th><th class="showDSHEDAddress">Ed&nbsp;Address</th><th class="showDSHRouteNo">R&nbsp;No.</th><th class="showDSHRouteNo">Driver</th><th>Status</th></tr></thead><tbody>';
  					for(var i=0;i<obj.address[0].summ.length;i++){
  						tbRows=tbRows+'<tr>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].TI+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].N+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].D+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].SA+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].EA+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].RN+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].summ[i].DID+'-'+obj.address[0].summ[i].VNO+'</td></center>';
  						var status="";
  						if(obj.address[0].summ[i].S=="30"){status="Broadcast";
  						}else if(obj.address[0].summ[i].S=="4"){status="Dispatching";
  						}else if(obj.address[0].summ[i].S=="8"){status="Dispatch Not Started";
  						}else if(obj.address[0].summ[i].S=="40"){status="Allocated";
  						}else if(obj.address[0].summ[i].S=="43"){status="On Rotue To Pickup";
  						}else if(obj.address[0].summ[i].S=="50"){status="Customer Cancelled Request";
  						}else if(obj.address[0].summ[i].S=="51"){status="No Show";
  						}else if(obj.address[0].summ[i].S=="49"){status="Soon To Clear";
  						}else if(obj.address[0].summ[i].S=="4"){status="Dispatching";
  						}else if(obj.address[0].summ[i].S=="0"){status="Unknown";
  						}else if(obj.address[0].summ[i].S=="47"){status="Trip Started";
  						}else if(obj.address[0].summ[i].S=="99"){status="Trip On Hold";
  						}else if(obj.address[0].summ[i].S=="3"){status="Couldnt Find Drivers";
  						}else if(obj.address[0].summ[i].S=="70"){status="Payment Received";
  						}else if(obj.address[0].summ[i].S=="90"){status="SrHold Job";
  						}else if(obj.address[0].summ[i].S=="48"){status="Driver Reporting NoShow";}

  						tbRows=tbRows+'<td><center>'+status+'</td></center>';
  						tbRows=tbRows+'</tr>';
  					}
  				}
  				if(obj.address[0].histy.length>0){
  					tbRows = tbRows + '<thead><tr><td colspan="7"><center>History Jobs</center></td><tr><th class="showDShTripId">TripId</th><th class="showDSHName">Name</th><th class="showDSHDate">Date</th><th class="showDSHAddress">St&nbsp;Address</th><th class="showDSHEDAddress">Ed&nbsp;Address</th><th class="showDSHRouteNo">R&nbsp;No.</th><th class="showDSHRouteNo">Driver</th><th>Status</th></tr></thead><tbody>';
  					for(var i=0;i<obj.address[0].histy.length;i++){
  						tbRows=tbRows+'<tr>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].TI+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].N+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].D+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].SA+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].EA+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].RN+'</td></center>';
  						tbRows=tbRows+'<td><center>'+obj.address[0].histy[i].DID+'-'+obj.address[0].histy[i].VNO+'</td></center>';
  						var status="";
  						if(obj.address[0].histy[i].S=="30"){status="Broadcast";
  						}else if(obj.address[0].histy[i].S=="40"){status="Allocated";
  						}else if(obj.address[0].histy[i].S=="43"){status="On Rotue To Pickup";
  						}else if(obj.address[0].histy[i].S=="50"){status="Customer Cancelled Request";
  						}else if(obj.address[0].histy[i].S=="51"){status="No Show";
  						}else if(obj.address[0].histy[i].S=="49"){status="Soon To Clear";
  						}else if(obj.address[0].histy[i].S=="4"){status="Dispatching";
  						}else if(obj.address[0].histy[i].S=="0"){status="Unknown";
  						}else if(obj.address[0].histy[i].S=="47"){status="Trip Started";
  						}else if(obj.address[0].histy[i].S=="99"){status="Trip On Hold";
  						}else if(obj.address[0].histy[i].S=="3"){status="Couldnt Find Drivers";
  						}else if(obj.address[0].histy[i].S=="70"){status="Payment Received";
  						}else if(obj.address[0].histy[i].S=="90"){status="SrHold Job";
  						}else if(obj.address[0].histy[i].S=="61"){status="Trip Completed";
  						}else if(obj.address[0].histy[i].S=="48"){status="Driver Reporting NoShow";}

  						tbRows=tbRows+'<td><center>'+status+'</td></center>';
  						tbRows=tbRows+'</tr>';
  					}
  				}
  				tableAppend.innerHTML=tbRows;
  			} else {
  	  			alert("Please give some value & search");
  	  		}
  		} 
  	}
  	function removeSearchScreen(){
		document.getElementById("myJobsToSearch").innerHTML="";
  		$("#searchForJob" ).dialog( "close" );
  		$("#searchForJob").hide();
  	}