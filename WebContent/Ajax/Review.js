
$.ctrl = function(key, callback, args) {
	var isCtrl = false;
	$(document).keydown(function(e) {
		if(!args) args=[]; // IE barks when args is null

		if(e.ctrlKey) isCtrl = true;
		if(e.keyCode == key.charCodeAt(0) && isCtrl) {
			callback.apply(this, args);
			return false;
		}
	}).keyup(function(e) {
		if(e.ctrlKey) isCtrl = false;
	});
};

$.ctrl('S', function() {
});

$.ctrl('D', function(s) {
});


var map = null;
var geocoder = null;
var latsgn = 1;
var lgsgn = 1;
var zm = 0;
var marker = null;
var posset = 0;
var clickCount = 0;
var temp = 1;
var insidePoint = true;
var markerJobArray=[];
var infoArray=[];
var tempArray=[];
var tripIdArray=[];
var worldCoordinate="";
var worldCoordinateNW="";

function initializeReview() {
	var defaultLati=document.getElementById("defaultLati").value;
	var defaultLongi=document.getElementById("defaultLongi").value;
	var latlng = new google.maps.LatLng(defaultLati,defaultLongi);
	var options = {
			zoom: 14,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			draggableCursor: 'crosshair'
	};
	map = new google.maps.Map(document.getElementById("mapReview"), options);
	geocoder = new google.maps.Geocoder();
	 getJobs('');driverDetails();getDate();
}
$(document).ready(function() {
	initializeReview();
	$(".driverDetailsReview").contextMenu({
		menu:'myMenuReview'}, function(action,el,pos){contextMenuReview(action,el,pos);
	});
});
function getDate(){
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();

	document.getElementById("fromDate").value = cMonth+"/"+cDate+"/"+cYear;
	document.getElementById("toDate").value = cMonth+"/"+cDate+"/"+cYear;

}
function contextMenuReview(action,el,pos){
	switch(action){
	case 'clearDriver':
	{
		var image="images/Dashboard/job.png";
		var alloCatedImage="images/Dashboard/jobAllocated.png";
		var jobSize=document.getElementById("totalJobs").value;
		var driverSize=document.getElementById("totalDrivers").value;
		var text= document.getElementById("manifestClick").value;
		for (i in markerJobArray) {
			markerJobArray[i].setMap(null);
			infoArray[i].setMap(null);
		}
		markerJobArray=[];
		infoArray=[];
		for(var i=0;i<jobSize;i++){
			if(document.getElementById("manifestJob_"+i).value==text){
				document.getElementById("manifestJob_"+i).value="";
			} 
			if(document.getElementById("manifestJob_"+i).value==""){
				point = new google.maps.LatLng(document.getElementById("latitude_"+i).value,document.getElementById("longitude_"+i).value);
				var markerIcon="";
				if(document.getElementById("routeNumber_"+i).value==""){
					markerIcon=image;
				} else {
					markerIcon=alloCatedImage;
				}
				var markerJob = new google.maps.Marker({position: point,
			        map: map, icon:markerIcon,  draggable: true });
				 	markerJob.setMap(map);
					 markerJobArray.push(markerJob);
					   var infoWindow = new google.maps.InfoWindow({
					        content: "<div>No.Of Passengers: "+document.getElementById("numOfPass_"+i).value+"</div><div>Drop Time: "+document.getElementById("dropTime_"+i).value+"</div>"+'<a id="menu0" onclick="bounce(0)"><div class="context">Manifest 1<\/div><\/a>'
							  +'<a id="menu1" onclick="bounce(1)"><div class="context">Manifest 2<\/div><\/a>'
		                      + '<a id="menu2" onclick="bounce(2)"><div class="context">Manifest 3<\/div><\/a>'
		                      + '<a id="menu3" onclick="bounce(3)"><div class="context">Manifest 4<\/div><\/a>'
		                      + '<a id="menu4" onclick="bounce(4)"><div class="context">Manifest 5<\/div><\/a>'
		                      +'<a id="menu5" onclick="bounce(5)"><div class="context">Manifest 6<\/div><\/a>'
		                      + '<a id="menu6" onclick="bounce(6)"><div class="context">Manifest 7<\/div><\/a>'
		                      +'<a id="menu7" onclick="bounce(7)"><div class="context">Manifest 8<\/div><\/a>'
		                      + '<a id="menu8" onclick="bounce(8)"><div class="context">Manifest 9<\/div><\/a>'
		                      +'<a id="menu9" onclick="bounce(9)"><div class="context">Manifest 10<\/div><\/a>'
		                      + '<a id="menu10" onclick="bounce(10)"><div class="context">Manifest 11<\/div><\/a>'
					    });
					 infoArray.push(infoWindow);
					 google.maps.event.addListener(markerJob, 'click',infoCallBack(infoWindow,markerJob,document.getElementById("tripId_"+i).value,i));
					 google.maps.event.addListener(markerJob, 'drag',dragAndAllocate(document.getElementById("tripId_"+i).value,i));
			}
		}
		for(var i=0;i<driverSize;i++){
			if(document.getElementById("manifestDriver_"+i).value==text){
				document.getElementById("driver_"+i).style.display="block";
			}
		}
		break;
	}
	case 'finalizeJobs':
	{
		var jobSize=document.getElementById("totalJobs").value;
		var driverSize=document.getElementById("totalDrivers").value;
		var text= document.getElementById("manifestClick").value;
		var tripId="";
		var driverId="";
		for(var i=0;i<jobSize;i++){
			if(document.getElementById("manifestJob_"+i).value==text){
				tripId=tripId+document.getElementById("tripId_"+i).value+";";
			}
		}
		for(var i=0;i<driverSize;i++){
			if(document.getElementById("manifestDriver_"+i).value==text){
				driverId=document.getElementById("driverId_"+i).value;
			}
		}
		insertJobs(tripId, driverId);
		break;
	}
	}
}

function addColumn(){
	var i=document.getElementById("counter").value;
	var j=parseInt(i);
	document.getElementById("Manifest_"+j).style.display="block";
	document.getElementById("counter").value=j+1;
}
 function deleteColumn(){
		var i=document.getElementById("counter").value;
		var j=parseInt(i);
		if(j==0){
			alert("No Manifest Added");
			return;
		} else {
			var k=j-1;
			document.getElementById("Manifest_"+k).style.display="none";
			document.getElementById("counter").value=k;
		}
}


function driverDetails(){
/*	document.getElementById('driverDetails').innerHTML="";
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='OpenRequestAjax?event=driverDetails';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var arrDriverId=[];
	var arrStatus=[];
	var arrResponse=text.split("^");
	var arrResponseSize = arrResponse.length;
	arrDriverId=[];
	arrStatus=[];
	arrCount=[];
	for(var i=0;i<arrResponseSize-1;i++){
		var driverId="";
		var status="";
		var drivers = arrResponse[i].split(";");
		var driversSize = drivers.length;
		for(var j=0;j<driversSize;j++){
			var nameValuePair=drivers[j].split("=");
			if(nameValuePair[0]=="D"){
				driverId=nameValuePair[1];
				arrDriverId.push(nameValuePair[1]);
			} else if(nameValuePair[0]=="S"){
				status=nameValuePair[1];
				arrStatus.push(nameValuePair[1]);
			}
		}
		if(status=="Y"){
			$('#driverDetails').append("<table id='availableDriver_"+driverId+"' class='driverDetailsReview' onclick=getTotalJobs("+driverId+")  onmouseup=bounce("+driverId+") onmousedown=clearTripId()> <a id=driverTemp>"+driverId+"</a></table>");
		}
	}
*/	
	
	$(".driverDetailsReview").contextMenu({
		menu:'myMenuReview'}, function(action,el,pos){contextMenuReview(action,el,pos);
	});
}	
function bounce(row){
	if(document.getElementById("dragedJob").value!=""){
		var tripIdPosition=document.getElementById("markerPosition").value;
		$("#Manifest_"+row).fadeOut();
		$("#Manifest_"+row).fadeIn();
		$("#Manifest_"+row).fadeOut();
		$("#Manifest_"+row).fadeIn();
		$("#Manifest_"+row).fadeOut();
		$("#Manifest_"+row).fadeIn();
		$("#Manifest_"+row).fadeOut();
		$("#Manifest_"+row).fadeIn();
		document.getElementById("manifestJob_"+tripIdPosition).value=row;
		markerJobArray[tripIdPosition].setMap(null);
	} else if(document.getElementById("dragedDriver").value!=""){
		document.getElementById("manifestDriver_"+document.getElementById("driverPosition").value).value=row;
		$("#Manifest_"+row).fadeOut();
		$("#Manifest_"+row).fadeIn();
		$("#Manifest_"+row).fadeOut();
		$("#Manifest_"+row).fadeIn();
		$("#Manifest_"+row).fadeOut();
		$("#Manifest_"+row).fadeIn();
		$("#Manifest_"+row).fadeOut();
		$("#Manifest_"+row).fadeIn();
		document.getElementById("driver_"+document.getElementById("driverPosition").value).style.display="none";
	}
	$(".contextmenu").hide();
	document.getElementById("dragedJob").value="";
	document.getElementById("markerPosition").value="";
	document.getElementById("dragedDriver").value="";
}	

function getJobs(tripId){
	var image="images/Dashboard/job.png";
	var alloCatedImage="images/Dashboard/jobAllocated.png";
	var point;
	var jobSize=document.getElementById("totalJobs").value;
	for(var i=0;i<jobSize;i++){
		point = new google.maps.LatLng(document.getElementById("latitude_"+i).value,document.getElementById("longitude_"+i).value);
		var markerIcon="";
		if(document.getElementById("routeNumber_"+i).value ==""){
			markerIcon=image;
		} else {
			markerIcon=alloCatedImage;
		}
		var markerJob = new google.maps.Marker({position: point,
	        map: map, icon:markerIcon,  draggable: true });
		 	markerJob.setMap(map);
			markerJobArray.push(markerJob);
			   var infoWindow = new google.maps.InfoWindow({
			        content: "<div>No.Of Passengers: "+document.getElementById("numOfPass_"+i).value+"</div><div>Drop Time: "+document.getElementById("dropTime_"+i).value+"</div>"+'<a id="menu0" onclick="bounce(0)"><div class="context">Manifest 1<\/div><\/a>'
					  +'<a id="menu1" onclick="bounce(1)"><div class="context">Manifest 2<\/div><\/a>'
                      + '<a id="menu2" onclick="bounce(2)"><div class="context">Manifest 3<\/div><\/a>'
                      + '<a id="menu3" onclick="bounce(3)"><div class="context">Manifest 4<\/div><\/a>'
                      + '<a id="menu4" onclick="bounce(4)"><div class="context">Manifest 5<\/div><\/a>'
                      +'<a id="menu5" onclick="bounce(5)"><div class="context">Manifest 6<\/div><\/a>'
                      + '<a id="menu6" onclick="bounce(6)"><div class="context">Manifest 7<\/div><\/a>'
                      +'<a id="menu7" onclick="bounce(7)"><div class="context">Manifest 8<\/div><\/a>'
                      + '<a id="menu8" onclick="bounce(8)"><div class="context">Manifest 9<\/div><\/a>'
                      +'<a id="menu9" onclick="bounce(9)"><div class="context">Manifest 10<\/div><\/a>'
                      + '<a id="menu10" onclick="bounce(10)"><div class="context">Manifest 11<\/div><\/a>'
			    });
			    infoArray.push(infoWindow);
			 google.maps.event.addListener(markerJob, 'click',infoCallBack(infoWindow,markerJob,document.getElementById("tripId_"+i).value,i));
			 google.maps.event.addListener(markerJob, 'drag',dragAndAllocate(document.getElementById("tripId_"+i).value,i));
//			 google.maps.event.addListener(markerJob, 'rightclick',showContextMenu(i));
	}
}

/*function showContextMenu(row) {
	var caurrentLatLng=new google.maps.LatLng(document.getElementById("latitude_"+row).value,document.getElementById("longitude_"+row).value);
	var WC="";var WCNW="";
    var contextmenuDir;
    projection = map.getProjection() ;
    $('.contextmenu').remove();
     contextmenuDir = document.createElement("div");
      contextmenuDir.className  = 'contextmenu';
      contextmenuDir.innerHTML = '<a id="menu0" onclick="bounce(0)"><div class="context">Manifest 1<\/div><\/a>'
    	  					  +'<a id="menu1" onclick="bounce(1)"><div class="context">Manifest 2<\/div><\/a>'
                              + '<a id="menu2" onclick="bounce(2)"><div class="context">Manifest 3<\/div><\/a>'
                              + '<a id="menu3" onclick="bounce(3)"><div class="context">Manifest 4<\/div><\/a>'
                              + '<a id="menu4" onclick="bounce(4)"><div class="context">Manifest 5<\/div><\/a>'
                              +'<a id="menu5" onclick="bounce(5)"><div class="context">Manifest 6<\/div><\/a>'
                              + '<a id="menu6" onclick="bounce(6)"><div class="context">Manifest 7<\/div><\/a>'
                              +'<a id="menu7" onclick="bounce(7)"><div class="context">Manifest 8<\/div><\/a>'
                              + '<a id="menu8" onclick="bounce(8)"><div class="context">Manifest 9<\/div><\/a>'
                              +'<a id="menu9" onclick="bounce(9)"><div class="context">Manifest 10<\/div><\/a>'
                              + '<a id="menu10" onclick="bounce(10)"><div class="context">Manifest 11<\/div><\/a>';

    $(map.getDiv()).append(contextmenuDir);

    google.maps.event.addListener(map, 'idle', function(event) {
        var bounds = map.getBounds();
        var nw = new google.maps.LatLng(
    		bounds.getNorthEast().lat(),
    		bounds.getSouthWest().lng()
        );
          WC = map.getProjection().fromLatLngToPoint(caurrentLatLng);
          WCNW = map.getProjection().fromLatLngToPoint(nw);
	});
	worldCoordinate=WC;
	worldCoordinateNW=WCNW;
 
    setMenuXY(caurrentLatLng);

    contextmenuDir.style.visibility = "visible";
}
function getCanvasXY(caurrentLatLng){
    var scale = Math.pow(2, map.getZoom());
   var caurrentLatLngOffset = new google.maps.Point(
       Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
       Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
   );
   return caurrentLatLngOffset;
}
function setMenuXY(caurrentLatLng){
    var mapWidth = $('#mapReview').width();
    var mapHeight = $('#mapReview').height();
    var menuWidth = $('.contextmenu').width();
    var menuHeight = $('.contextmenu').height();
    var clickedPosition = getCanvasXY(caurrentLatLng);
    var x = clickedPosition.x ;
    var y = clickedPosition.y ;
     if((mapWidth - x ) < menuWidth)//if to close to the map border, decrease x position
         x = x - menuWidth;
    if((mapHeight - y ) < menuHeight)//if to close to the map border, decrease y position
        y = y - menuHeight;

    $('.contextmenu').css('left',x);
    $('.contextmenu').css('top',y);
    };
*/

function insertJobs(tripId,driverId){
	var jobSize=document.getElementById("totalJobs").value;
	var manifestValue=document.getElementById("manifestClick").value;
	var j=parseInt(manifestValue);
	var k=j+1;
	if(tripId!=""){
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'OpenRequestAjax?event=insertSharedRide&tripId='+tripId+'&driverId='+driverId+'&routeNumber='+k;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text.indexOf("001;")>=0){
			document.getElementById("Manifest_"+manifestValue).style.display="none";
			for(var i=0;i<jobSize;i++){
				if(document.getElementById("manifestJob_"+i).value==manifestValue){
					document.getElementById("manifestJob_"+i).value=manifestValue+"000";
				}
			}
		} 
	} else {
		alert("No jobs allocated for this manifest");
	}
}

function dragAndAllocate(tripId,row){
	return function() {
		document.getElementById("dragedJob").value=tripId;
		document.getElementById("markerPosition").value=row;
		return;
	};		
}
function dragAndAllocateDriver(driverId,row){
		document.getElementById("dragedDriver").value=driverId;
		document.getElementById("driverPosition").value=row;
		return;
}
	
 function infoCallBack(infoJob,marker,tripId,row){
		return function() {
			infoJob.open(map,marker);
			document.getElementById("dragedJob").value=tripId;
			document.getElementById("markerPosition").value=row;
		};
	}

 function enterManifestClick(row){
	 document.getElementById("dragedJob").value="";
	 document.getElementById("markerPosition").value="";
	 document.getElementById("manifestClick").value=row;
 }

 
 
 
 
 
 
 
 