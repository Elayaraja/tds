function AJAXInteraction(url, callback) {
    var req = init();
    req.onreadystatechange = processRequest;
    function init() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    } 
    
    function processRequest () {
      // readyState of 4 signifies request is complete
      if (req.readyState == 4) {
        // status of 200 signifies sucessful HTTP call
        if (req.status == 200) {
          if (callback) callback(req.responseText);
        }
      }
    }

    this.doGet = function() {
      req.open("GET", url, true);
      req.send(null);
    }
}



//------ Ajax InterAction  Over    -------------------------------

function loadCompany(){
	
	var vcode = document.getElementById('vcode');
	var assoccode = document.getElementById("assoccode");
	//alert("Please wait collecting address Information ");
	var url = '/TDS/AjaxClass?event=loadCompany&vcode='+vcode.value+'&assoccode='+assoccode.value;
	var ajax = new AJAXInteraction(url,loadCompanyValues);
	ajax.doGet();

}

function loadCompanyValues(responseText)
{	 		 
		document.getElementById('vcostcenter').value = responseText;
}
function getChargesVoucher(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var voucherForCharge=document.getElementById("vno").value;
	var url = 'OpenRequestAjax?event=getChargesForVoucher&voucherNo='+voucherForCharge;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	for(var j=0;j<obj.address.length;j++){
		calVoucherCharges();
		var type=obj.address[j].T;
		var amount=obj.address[j].A;
		var i=j+1;
		document.getElementById("chargeKey"+i).value=type;
		document.getElementById("chargeAmount"+i).value=amount;
	}
} 
