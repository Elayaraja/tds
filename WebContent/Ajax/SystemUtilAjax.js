function AjaxInteraction(url, callback) {

	var req=init();
	req.onreadystatechange = processRequest;
alert("Ajax");
	function init() {
		if(window.XMLHttpRequest) {
			return new XMLHttpRequest();
		}
		else if(window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
	}

	function processRequest() {
		if(req.readyState == 4) {
			if(req.status==200) {
				if (callback)  callback(req.responseText);
			}
		}

	}

	this.doGet=function() {
		req.open("GET" ,url ,true);
		req.send(null);		
	}

}
//Ajax Interaction done

function startOrStopTimerForDashboard(){
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=StartOrStopTimer&module=systemsetupView&btnTimer=YES';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="Timer Started Successfully"){
		document.getElementById("StartOrStopTimer").value=text;
		document.getElementById("btnStartOrStopTimer").value="Stop Dispatch";
	}else if(text=="Timer Stopped Successfully"){
		document.getElementById("StartOrStopTimer").value=text;
		document.getElementById("btnStartOrStopTimer").value="Start Dispatch";
	}else {
		document.getElementById("StartOrStopTimer").value="";
	}
}

function startOrStopTimer(){
	var result= confirm("Do you want to change the status of the Automatic Dispatch process?");
	if(result==true){
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=StartOrStopTimer&module=systemsetupView&btnTimer=YES';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;

	document.getElementById("StartOrStopTimer").style.display = "block";
	if(text=="Timer Started Successfully"){
		document.getElementById("StartOrStopTimer").innerHTML=text;
		document.getElementById("reloadZones").innerHTML="";
		document.getElementById("reloadCabs").innerHTML="";
		document.getElementById("btnStartOrStopTimer").value="Stop Dispatch";
	}else if(text=="Timer Stopped Successfully"){
		document.getElementById("StartOrStopTimer").innerHTML=text;
		document.getElementById("reloadZones").innerHTML="";
		document.getElementById("reloadCabs").innerHTML="";
		document.getElementById("btnStartOrStopTimer").value="Start Dispatch";
	}else {
		document.getElementById("StartOrStopTimer").innerHTML=text;
	}
	}
}

function reloadZone(){
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=reloadZones&module=systemsetupView&btnreloadZones=YES';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!=null && text!=""){
		document.getElementById("reloadCabs").innerHTML="";
		document.getElementById("StartOrStopTimer").innerHTML="";
		document.getElementById("reloadZones").innerHTML=text;
	} else {
		document.getElementById("reloadZones").innerHTML="";
	}
}

function reloadCabs(){
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=reloadCab&module=systemsetupView&btnreloadCab=YES';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!=null && text!=""){
		document.getElementById("StartOrStopTimer").innerHTML="";
		document.getElementById("reloadZones").innerHTML="";
		document.getElementById("reloadCabs").innerHTML=text;
	} else {
		document.getElementById("reloadCabs").innerHTML="";
	}
}

