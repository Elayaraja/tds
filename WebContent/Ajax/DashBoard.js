function AJAXInteraction(url, callback) {
    var req = init();
    req.onreadystatechange = processRequest;
    function init() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    } 
    
    function processRequest () {
      // readyState of 4 signifies request is complete
      if (req.readyState == 4) {
        // status of 200 signifies sucessful HTTP call
        if (req.status == 200) {
          if (callback) callback(req.responseText);
        }
      }
    }

    this.doGet = function() {
      req.open("GET", url, true);
      req.send(null);
    }
}



//------ Ajax InterAction  Over    -------------------------------

function dashBoardAjax1(){
	//alert("Assocode DA1:" + document.getElementById('assoccode').value);
	var url = '/TDS/AjaxClass?event=getDashBoard&assoccode='+document.getElementById('assoccode').value+'&stat=4';
	var ajax = new AJAXInteraction(url,getDashBoardValues);
	ajax.doGet();

} 
//function dashBoardAjax(n){
//	document.getElementById("refreshZone").src="images/Dashboard/loading_icon.gif";
//	url = '/TDS/AjaxClass?event=getDashBoard&assoccode='+document.getElementById('assoccode').value+'&stat='+n;	
//	ajax = new AJAXInteraction(url,getDashBoardValues);
//	ajax.doGet();
//}
/*function dashBoardAjax3(){
	 url = '/TDS/AjaxClass?event=getDashBoard&assoccode='+document.getElementById('assoccode').value+'&stat=2';
	 ajax = new AJAXInteraction(url,getDashBoardValues);
	 ajax.doGet();
}
function dashBoardAjax4(){
	 url = '/TDS/AjaxClass?event=getDashBoard&assoccode='+document.getElementById('assoccode').value+'&stat=3';
	 ajax = new AJAXInteraction(url,getDashBoardValues);
	 ajax.doGet();
}
function dashBoardAjax5(){
	 url = '/TDS/AjaxClass?event=getDashBoard&assoccode='+document.getElementById('assoccode').value+'&stat=4';
	 ajax = new AJAXInteraction(url,getDashBoardValues);
	 ajax.doGet();
}*/
//function getDashBoardValues(responseText)
//{
//	document.getElementById("refreshZone").src="images/Dashboard/refresh1.png";	
//		document.getElementById("queueid").innerHTML = responseText;
//		 $(".label").contextMenu({ 
//		       	menu: 'myMenu2' }, function(action, el, pos) {contextMenuWork2(action, el, pos); });
//		 document.getElementById("changeHappeningZone").value="false";
//	}
		/*document.getElementById("openid").innerHTML = resp[1];
		document.getElementById('openBid').innerHTML = resp[2];
		if(resp[3]!='')
		document.getElementById('openoid').innerHTML = resp[3];
		
		*/
function openCC()
{
	window.open('control?action=manualccp&event=manualCCProcess&module=financeView','newCC');
}
function openSMS()
{
	window.open('control?action=openrequest&event=sendSMS&module=dispatchView','newSMS');
}
function removeFromQ(driver_id)
{
	var answer = prompt("Reason for delete ?","");
	if(answer!=null)
	{
		var url = '/TDS/AjaxClass?event=removeDriverinQ&driver_id='+driver_id+'&answer='+answer;
		var ajax = new AJAXInteraction(url,removeFromQValues);
		ajax.doGet();
	}
}
function removeFromQValues(responseText)
{
	var resp = responseText.split('###');
	document.getElementById("queueid").innerHTML = resp[0];
	
}
function changeFromQ(driver_id)
{
	var answer = prompt("Reason for move ?","");
	if(answer!=null)
	{
		var url = '/TDS/AjaxClass?event=moveDriverinQ&driver_id='+driver_id+'&answer='+answer;
	 	var ajax = new AJAXInteraction(url,changeFromQValues);
	 	ajax.doGet();
	}
	
}
function changeFromQValues(responseText)
{
	var resp = responseText.split('###');
	
	document.getElementById("queueid").innerHTML = resp[0];
}
function openDD()
{
	window.open('control?action=admin&event=driverTempDeActive&module=operationView','newDD');
}
function openOR()
{
	window.open('control?action=openrequest&event=saveRequest',"_self");
}
function openOR1(tripId)
{
	$('#ORDash').jqm();
	$("#ORDash").jqmShow();
	document.getElementById('nameDash').focus();
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getOpenRequest&tripId='+tripId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	//alert(jsonObj);
	var obj = JSON.parse(jsonObj.toString());
	for(var j=0;j<obj.address.length;j++){
		document.getElementById('phoneDash').value=obj.address[j].Phone;
		document.getElementById('nameDash').value=obj.address[j].Name;
		document.getElementById('sadd1Dash').value=obj.address[j].Add1;
		document.getElementById('sadd2Dash').value=obj.address[j].Add2;
		document.getElementById('scityDash').value=obj.address[j].City;
		document.getElementById('sstateDash').value=obj.address[j].State;
		document.getElementById('szipDash').value=obj.address[j].Zip;
		document.getElementById('sLatitudeDash').value=obj.address[j].Lat;
		document.getElementById('sLongitudeDash').value=obj.address[j].Lon;
		document.getElementById('sdate').value=obj.address[j].Date;
		document.getElementById("timeTemporary").value=obj.address[j].Time;
		$("#shrs option[value=NCH]").show();
		if(document.getElementById("timeType").value=="1"){
			document.getElementById("shrs").value = obj.address[j].Time;
		} else {
			document.getElementById("shrs").value = "NCH";
		}
//		document.getElementById("shrs").value="NCH";
		document.getElementById('CommentsDash1').value=obj.address[j].Comments;
		document.getElementById('specialInsDash1').value=obj.address[j].SplIns;
		document.getElementById('tripId').value=obj.address[j].tripId;
		document.getElementById('eadd1Dash').value=obj.address[j].EAdd1;
		document.getElementById('eadd2Dash').value=obj.address[j].EAdd2;
		document.getElementById('ecityDash').value=obj.address[j].ECity;
		document.getElementById('estateDash').value=obj.address[j].EState;
		document.getElementById('ezipDash').value=obj.address[j].EZip;
		document.getElementById('eLatitude').value=obj.address[j].ELat;
		document.getElementById('eLongitude').value=obj.address[j].ELon;
		document.getElementById('payTypeDash').value=obj.address[j].PayType;
		document.getElementById('acctDash').value=obj.address[j].Acct;
	    document.getElementById("queuenoDash").value=obj.address[j].Zone;
		document.getElementById('landMarkDash').value=obj.address[j].LandMark;
		document.getElementById('toLandMarkDash').value=obj.address[j].ELandMark;
		document.getElementById('numberOfPassengerDash').value=obj.address[j].numberOfPassengers;
		document.getElementById('meter').value=obj.address[j].MTy;
		document.getElementById('eMailDash').value=obj.address[j].EM;
		document.getElementById('tripStatus').value=obj.address[j].TS;
		document.getElementById('jobRating').value = obj.address[j].JR;
		document.getElementById("fleetForOR").value = obj.address[j].FL;
		document.getElementById('callerPhone').value=obj.address[j].CPh;
		document.getElementById('callerName').value=obj.address[j].CNa;
		document.getElementById('airName').value=obj.address[j].ANAM;
		document.getElementById('airFrom').value=obj.address[j].AFROM;
		document.getElementById('airTo').value=obj.address[j].ATO;
		document.getElementById('airNo').value=obj.address[j].AN;
		
		document.getElementById('refNum').value=obj.address[j].Rf;
		document.getElementById('refNum1').value=obj.address[j].Rf1;
		document.getElementById('refNum2').value=obj.address[j].Rf2;
		document.getElementById('refNum3').value=obj.address[j].Rf3;
		
		document.getElementById('premiumNo').value=obj.address[j].premiumCustomer;
		if(document.getElementById('callerPhone').value!="" || document.getElementById('callerName').value!="")
		{
			document.getElementById("hideTableCalledBy").style.display="block";
		}
		if(obj.address[j].AT!="-1"){
			document.getElementById('advanceTime').value=obj.address[j].AT;
		}
		if(document.getElementById("driverORcabDash").value=="Driver"){
			document.getElementById("driverDashOR").value = obj.address[j].Driver;
			document.getElementById('DriverCabSwitch').value=obj.address[j].Vehicle;
		} else {
			document.getElementById("driverDashOR").value = obj.address[j].Vehicle;
			document.getElementById('DriverCabSwitch').value=obj.address[j].Driver;
		}
		document.getElementById("repeatGroupDash").value=obj.address[j].multiJobTag;
		if(document.getElementById('repeatGroupDash').value!="" && document.getElementById('repeatGroupDash').value!="0"){
			document.getElementById("Error").innerHTML="This is a multi job.You cannot update date/time for all trips together.";
			document.getElementById("Error").style.display="block";
		}
		checkPaysTrip(tripId);
		var driverList=document.getElementById("drprofileSizeDash").value;
		var vehicleList=document.getElementById("vprofileSizeDash").value;
		var drFlag=obj.address[j].Flag;
		var sharedRide=obj.address[j].sharedRide;
		var dontDispatch=obj.address[j].dontDispatch;
		var premiumCustomer=obj.address[j].premiumCustomer;
		if(sharedRide==1){
			document.getElementById("sharedRideDash").checked=true;
		} if(dontDispatch==1){
			document.getElementById("dispatchStatusDash").checked=true;
		} if(premiumCustomer==1){
			document.getElementById("premiumCustomerDash").checked=true;
		}
		if(drFlag!=""){
			for (var i=0; i<driverList;i++){
				if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
					document.getElementById('dchk'+i).checked=true;
				}
			}
			for (var i=0;i<vehicleList;i++){
				if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
					document.getElementById('vchk'+i).checked=true;
				}
			}
			openSplReqDash();
		}	
	}
	if(document.getElementById('CommentsDash1').value!=""){
		openDispatchCommentsDash();
		document.getElementById('CommentsDash1').style.color='#000000';
	}
	if(	document.getElementById('specialInsDash1').value!=""){
		openDriverCommentsDash();
		document.getElementById('specialInsDash1').style.color='#000000';
	}
	if(document.getElementById('airName').value!="" && document.getElementById('airNo').value!="" && document.getElementById('airFrom').value!="" && document.getElementById('airTo').value!="" ){
		//openFlightInformationDash();
        var ele = document.getElementById("hideFlightInformation");
		document.getElementById("airName").readOnly = false;
		document.getElementById("airNo").readOnly = false;
		document.getElementById("airFrom").readOnly = false;
		document.getElementById("airTo").readOnly = false;
		if(ele.style.display == "block") {
			ele.style.display = "none";
		}
		else {
			ele.style.display = "block";
		}

      }
}

function openOR(nextScreen)
{
	window.open('control?action=openrequest&event=saveRequest&nextScreen='+nextScreen);
}
/*function openORFromDasBoard(nextScreen)
{
	var masterKey=document.getElementById("masterAddress").value;
	var phone=document.getElementById("phoneDash").value;
	var name=document.getElementById("nameDash").value;
	var date=document.getElementById("sdate").value;
	var time=document.getElementById("shrs").value;
	var add1=document.getElementById("sadd1Dash").value;
	var add2=document.getElementById("sadd2Dash").value;
	var city=document.getElementById("scityDash").value;
	var state=document.getElementById("sstateDash").value;
	var zip=document.getElementById("szipDash").value;
	var eadd1 = document.getElementById('eadd1Dash').value;
	var eadd2 = document.getElementById('eadd2Dash').value;
	var ecity = document.getElementById('ecityDash').value;
	var estate = document.getElementById('estateDash').value;
	var ezip = document.getElementById('ezipDash').value;
	var elati = document.getElementById('eLatitude').value;
	var elongi = document.getElementById('eLongitude').value;
	window.open('control?action=openrequest&event=saveRequest&nextScreen='+nextScreen+'&masterKey='+masterKey+'&phone='+phone+'&name='+name+'&add1='+add1+'&add2='+add2+'&city='+city+'&state='+state+'&zip='+zip+'&date='+date+'&time='+time+'&eAdd1='+eadd1+'&eAdd2='+eadd2+'&eCity='+ecity+'&eState='+estate+'&eZip='+ezip+'&eLongi='+elongi+'&eLati='+elati);
}
*/
function openReport()
{
	window.open('control?action=openrequest&event=Report','newReport');
}
function openDriverLocation()
{
	window.open('control?action=admin&event=showDriver&driver_id=','','top=0,left=0,resizable=no,toolbar=no,addressbar=no,menubar=­no,statu=no,location=yes,fullscreen=yes,scrollbars=yes');
}
function openDriverPathConfirmation()
{ 
	window.open('control?action=admin&event=confirmShortPath','newShortPath1','width=350,height=350,resizable=no,toolbar=no,addressbar=no,menubar=­no,statu=no,location=no,fullscreen=no,scrollbars=no');
}
function openDriverPath()
{
	window.open('control?action=admin&event=dashBoard&event2=shortPath&driver_id=','newShortPath2','top=0,left=0,resizable=no,toolbar=no,addressbar=no,menubar=­no,statu=no,location=yes,fullscreen=yes,scrollbars=yes');
}
function deleteReq(tripid,assoccode)
{
	 var flg = confirm("Do you really want to delete this trip"); 
	 if(flg) {
		var url = '/TDS/AjaxClass?event=deleteOpen&assoccode='+assoccode+'&tripid='+tripid;
		var ajax = new AJAXInteraction(url,getDashBoardDelete);
		ajax.doGet();
	 }
}
function deleteAllReq(assoccode)
{
	 var flg = confirm("Do you really want to delete all the trip"); 
	 if(flg) {
		var url = '/TDS/AjaxClass?event=deleteAll&assoccode='+assoccode;
		var ajax = new AJAXInteraction(url,getDashBoardDelete);
		ajax.doGet();
	 }
}
function getDashBoardDelete(responseText)
{
		var resp = responseText.split('###');	 
		document.getElementById('openoid').innerHTML = resp[0];
		
}

function doUpdateOQ(i,trip_id,assocode)
{
	var answer = prompt("Reason for Update ?","");
	if(answer!=null)
	{	
		clearTimeout(TimerID);
		 
		var url = '/TDS/AjaxClass?event=doQupdate&type=1&driver_id='+document.getElementById('driver_id'+i).value+'&queue_no='+document.getElementById('queue_no'+i).value+'&pickup='+document.getElementById('pickup'+i).value+'&trip_id='+trip_id+'&assocode='+assocode+'&answer='+answer;
		var ajax = new AJAXInteraction(url,doUpdateOQValues);
		ajax.doGet();
	}
}
function doUpdateOQValues(responseText)
{
	 
	var resp = responseText.split('###');
	if(resp[0] == "1")
		document.getElementById("openid").innerHTML = resp[1];
	else 
		alert("Check You Data/Trip");
	
	
	
	StartTimer();
}
/*function allocateOQ(i,trip_id,assocode)
{
	var answer = prompt("Reason for Allocate ?","");
	if(answer!=null)
	{
		clearTimeout(TimerID);
		var url = '/TDS/AjaxClass?event=allocateOQ&type=1&driver_id='+document.getElementById('driver_id'+i).value+'&queue_no='+document.getElementById('queue_no'+i).value+'&pickup='+document.getElementById('pickup'+i).value+'&trip_id='+trip_id+'&assocode='+assocode+'&answer='+answer;
		var ajax = new AJAXInteraction(url,allocateOQValues);
		ajax.doGet();
	}
}
function allocateOQValues(responseText)
{
	var resp = responseText.split('###');
	if(resp[0] == "0")
	{
		alert("Driver Successfully Allocated");
	} else if(resp[0] == "1")
	{
		alert("Fail to Allocate Driver");
	} else {
		alert("This Request Accept by Some one/Driver Already get Allocated");
	}
	
	document.getElementById("openid").innerHTML = resp[1];
	 	
	StartTimer();
} */

function allocateOQ()
{
	 
	var flg = true;
	if(!opener.closed){
		var answer = prompt("Reason for Allocate ?","");
		if(answer!=null)
		{
			var n = document.getElementById('size').value;
			var triparray = new Array ();
			for(var i=0;i<Number(n);i++)
			{
				if(document.getElementById('chck'+i).checked)
				{
					triparray.push(document.getElementById('chck'+i).value+";"+document.getElementById('driver_id'+i).value+";"+document.getElementById('queue_no'+i).value);
					//alert(triparray.toString());
					for(var j=i+1;j<Number(n);j++)
					{ 
						if(document.getElementById('chck'+j).checked)
						{
							if(document.getElementById('driver_id'+i).value== document.getElementById('driver_id'+j).value)
							{
								flg = false;
							}
						}
					}
				}
			}
			
			if(flg == true)
			{
				//alert(triparray.join(","));
				var url = '/TDS/AjaxClass?event=allocateOQ&type=1&triparray='+triparray.toString()+'&answer='+answer;
				var ajax = new AJAXInteraction(url,allocateOQValues);
				ajax.doGet();
			}else {
				alert("Pls Check you Selection");
			}
		} 
	} else {
		alert("Your Dashboard closed");
	}
}
function allocateOQValues(responseText)
{
	//alert(responseText) ;
	
	var resp = responseText.split('###');
	if(resp[0] == "1")
	{
		 
	} else{
		alert("Some of the driver that you selected are not allocated");
	}
	opener.document.getElementById("openid").innerHTML = resp[1];
	//opener.window.location.reload(true);
	window.self.close();
}
function allocateBR()
{
	 
	var flg = true;
	if(!opener.closed){
		var answer = prompt("Reason for Allocate ?","");
		if(answer!=null)
		{
			var n = document.getElementById('size').value;
			var triparray = new Array ();
			for(var i=0;i<Number(n);i++)
			{
				if(document.getElementById('chckB'+i).checked)
				{
					triparray.push(document.getElementById('chckB'+i).value+";"+document.getElementById('driver_idB'+i).value+";"+document.getElementById('queue_noB'+i).value);
					//alert(triparray.toString());
					for(var j=i+1;j<Number(n);j++)
					{ 
						if(document.getElementById('chckB'+j).checked)
						{
							if(document.getElementById('driver_idB'+i).value== document.getElementById('driver_idB'+j).value)
							{
								flg = false;
							}
						}
					}
				}
			}
			
			if(flg == true)
			{
				//alert(triparray.join(","));
				var url = '/TDS/AjaxClass?event=allocateOQ&type=2&triparray='+triparray.toString()+'&answer='+answer;
				var ajax = new AJAXInteraction(url,allocateBRValues);
				ajax.doGet();
			}else {
				alert("Pls Check you Selection");
			}
		} 
	} else {
		alert("Your Dashboard closed");
	}
}
function allocateBRValues(responseText)
{
	 
	var resp = responseText.split('###');
	if(resp[0] == "1")
	{
		 
	} else{
		alert("Some of the driver that you selected are not allocated");
	}
	opener.document.getElementById("openBid").innerHTML = resp[1];
	//opener.window.location.reload(true);
	window.self.close();
}


 function deleteOpenReq(tripid,assoccode)
 {
	var answer = prompt("Reason for Delete ?","");
	if(answer!=null)
	{
	 	var url = '/TDS/AjaxClass?event=deleteOpenReq&type=1&assoccode='+assoccode+'&tripid='+tripid+'&answer='+answer;
		var ajax = new AJAXInteraction(url,deleteOpenReqValues);
		ajax.doGet(); 
	}
 }
 function deleteOpenReqValues(responseText)
 {
	 var resp = responseText.split('###');	 
	 if(resp[0] == "1")
		 document.getElementById('openid').innerHTML = resp[1];
	else
		 alert('Request could not be closed');	 
 }
 function doUpdateBQ(i,tripid,assoccode)
 {
	 var answer = prompt("Reason for Update ?","");
	 if(answer!=null)
	 {
		clearTimeout(TimerID);
		 
		var url = '/TDS/AjaxClass?event=doQupdate&type=2&driver_id='+document.getElementById('driver_idB'+i).value+'&queue_no='+document.getElementById('queue_noB'+i).value+'&pickup='+document.getElementById('pickupB'+i).value+'&trip_id='+tripid+'&assocode='+assoccode+'&answer='+answer;
		var ajax = new AJAXInteraction(url,doUpdateBOQValues);
		ajax.doGet();
	 }
 }
 function doUpdateBOQValues(responseText)
 {
	  
	 var resp = responseText.split('###');
		if(resp[0] == "1"){
			 
			document.getElementById("openBid").innerHTML = resp[1];
			document.getElementById('openid').innerHTML = resp[2];
		}else {
			alert("Check You Data/Trip");
		}
		StartTimer();
 }
 function deleteOpenBReq(tripid,assoccode)
 {
	var answer = prompt("Reason for Delete ?","");
	if(answer!=null)
	{
	 	var url = '/TDS/AjaxClass?event=deleteOpenReq&type=2&assoccode='+assoccode+'&tripid='+tripid+'&answer='+answer;
		var ajax = new AJAXInteraction(url,deleteOpenBReqValues);
		ajax.doGet(); 
	}
 }
 function deleteOpenBReqValues(responseText)
 {
	 
	 var resp = responseText.split('###');	 
	 if(resp[0] == "1")
		 document.getElementById('openBid').innerHTML = resp[1];
	 else 
		 alert('Record Not Deleted');
	 
 }
 /*
 function allocateBQ(i,trip_id,assocode)
 {
 	var answer = prompt("Reason for Allocate ?","");
 	if(answer!=null)
 	{
 		clearTimeout(TimerID);
 		var url = '/TDS/AjaxClass?event=allocateOQ&type=2&driver_id='+document.getElementById('driver_idB'+i).value+'&queue_no='+document.getElementById('queue_noB'+i).value+'&pickup='+document.getElementById('pickupB'+i).value+'&trip_id='+trip_id+'&assocode='+assocode+'&answer='+answer;
 		var ajax = new AJAXInteraction(url,allocateBQValues);
 		ajax.doGet();
 	}
 }
 function allocateBQValues(responseText)
 {
 	var resp = responseText.split('###');
 	if(resp[0] == "0")
 	{
 		alert("Driver Successfully Allocated");
 	} else if(resp[0] == "1")
 	{
 		alert("Fail to Allocate Driver");
 	} else {
 		alert("This Request Allocte to Some one/Driver Already get Allocated");
 	}
 	
 	document.getElementById("openBid").innerHTML = resp[1];
 	document.getElementById('openid').innerHTML = resp[2];	
 	StartTimer();
 }*/
 
 function openDLocation()
 {
 	var all =0;
 	var driver_id="";
 	if(document.getElementById("all").checked)
 		all = 1;
 	else
 		driver_id = document.getElementById('driver_id').value;
 	
 	var assoccode  = document.getElementById('assoccode').value;
 	
 	var url = '/TDS/AjaxClass?event=showDriver&assoccode='+assoccode+'&driver_id='+driver_id;
	var ajax = new AJAXInteraction(url,openDLocationValues);
	ajax.doGet(); 
	 

 }
 function openDLocationValues(responseText)
 {
	 alert(responseText);
	 document.getElementById("mapdiv").innerHTML =responseText; 
 }
/* function updateDriver(i,trip_id,assocode){
		var answer = "";
		var url = '/TDS/AjaxClass?event=doQupdate&type=1&driver_id='+document.getElementById('driver_id'+i).value+'&queue_no='+document.getElementById('queue_no'+i).value+'&pickup='+document.getElementById('pickup'+i).value+'&trip_id='+trip_id+'&assocode='+assocode+'&answer='+answer;
		var ajax = new AJAXInteraction(url,doUpdateOQValues);
		ajax.doGet();
	}*/
 

 /*function map(lat,lang,driver){
		alert("lat"+lat+""+lang+"driver"+driver);
		var ajax = new AjaxInteraction(url,mapid);
		ajax.doGet();
		xz1(lat,lang,driver);
		
	}
	function mapid(responseText)
	{
			var resp = responseText.split('###');
			document.getElementById("mapid").innerHTML = resp[0];
	}*/
 /*function drivers(responseText)
 {
 		var resp = responseText.split('###');
 		document.getElementById("drivers").innerHTML = resp[0];
 }
 function dispatch(){
 	alert("dashboardAjax calling");
 	url = '/TDS/DashBoard?event=driverDetails';
 	var ajax = new AjaxInteraction(url,drivers);
 	ajax.doGet();
 }*/
 function openORDetails(tripId){
	$('#ORDash').jqm();
	$("#ORDash").jqmShow();
	$("input[type=text]").attr('readonly','readonly');
	$("#hideFlightInformation").attr('readonly',false);
	$("#displayFlightInformation").attr('readonly',false);
    $("input[type=checkbox]").attr('disabled','disabled');
	$(".buttonORDashboard").hide();
	$("select").attr('disabled','disabled');
	$("textarea").attr('readonly','readonly');
	$("#reDispatchInORDashboard").show();
	document.getElementById('nameDash').focus();
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getOpenRequest&tripId='+tripId+'&fromHistory=YES';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	//alert(jsonObj);
	var obj = JSON.parse(jsonObj.toString());
	
	for(var j=0;j<obj.address.length;j++){
		//alert(obj.address[j].ANAM);
		//alert(obj.address[j].ANO);
		document.getElementById('phoneDash').value=obj.address[j].Phone;
		document.getElementById('nameDash').value=obj.address[j].Name;
		document.getElementById('sadd1Dash').value=obj.address[j].Add1;
		document.getElementById('sadd2Dash').value=obj.address[j].Add2;
		document.getElementById('scityDash').value=obj.address[j].City;
		document.getElementById('sstateDash').value=obj.address[j].State;
		document.getElementById('szipDash').value=obj.address[j].Zip;
		document.getElementById('sLatitudeDash').value=obj.address[j].Lat;
		document.getElementById('sLongitudeDash').value=obj.address[j].Lon;
		document.getElementById('sdate').value=obj.address[j].Date;
		document.getElementById("timeTemporary").value=obj.address[j].Time;
		document.getElementById("shrs").value="NCH";
		document.getElementById('CommentsDash1').value=obj.address[j].Comments;
		document.getElementById('specialInsDash1').value=obj.address[j].SplIns;
		document.getElementById('tripId').value=obj.address[j].tripId;
		document.getElementById('eadd1Dash').value=obj.address[j].EAdd1;
		document.getElementById('eadd2Dash').value=obj.address[j].EAdd2;
		document.getElementById('ecityDash').value=obj.address[j].ECity;
		document.getElementById('estateDash').value=obj.address[j].EState;
		document.getElementById('ezipDash').value=obj.address[j].EZip;
		document.getElementById('eLatitude').value=obj.address[j].ELat;
		document.getElementById('eLongitude').value=obj.address[j].ELon;
		document.getElementById('payTypeDash').value=obj.address[j].PayType;
		document.getElementById('acctDash').value=obj.address[j].Acct;
	    document.getElementById("queuenoDash").value=obj.address[j].Zone;
		document.getElementById('landMarkDash').value=obj.address[j].LandMark;
		document.getElementById('toLandMarkDash').value=obj.address[j].ELandMark;
		document.getElementById('numberOfPassengerDash').value=obj.address[j].numberOfPassengers;
		document.getElementById('airName').value=obj.address[j].ANAM;
		document.getElementById('airFrom').value=obj.address[j].AFROM;
		document.getElementById('airTo').value=obj.address[j].ATO;
		document.getElementById('airNo').value=obj.address[j].AN;
		
		document.getElementById('refNum').value=obj.address[j].Rf;
		document.getElementById('refNum1').value=obj.address[j].Rf1;
		document.getElementById('refNum2').value=obj.address[j].Rf2;
		document.getElementById('refNum3').value=obj.address[j].Rf3;
		
    	//document.getElementById('dropTimeDash').value=obj.address[j].dropTime;
		document.getElementById('eMailDash').value=obj.address[j].EM;
		document.getElementById('tripStatus').value=obj.address[j].TS;
		
		if(obj.address[j].AT!="-1"){
			document.getElementById('advanceTime').value=obj.address[j].AT;
		}
		if(document.getElementById("driverORcabDash").value=="Driver"){
			document.getElementById("driverDashOR").value=obj.address[j].Driver;
		} else {
			cab=document.getElementById("driverDashOR").value=obj.address[j].Vehicle;
		}
		document.getElementById("repeatGroupDash").value=obj.address[j].multiJobTag;
		var driverList=document.getElementById("drprofileSizeDash").value;
		var vehicleList=document.getElementById("vprofileSizeDash").value;
		var drFlag=obj.address[j].Flag;
		var sharedRide=obj.address[j].sharedRide;
		var dontDispatch=obj.address[j].dontDispatch;
		var premiumCustomer=obj.address[j].premiumCustomer;
		if(sharedRide==1){
			document.getElementById("sharedRideDash").checked=true;
		} if(dontDispatch==1){
			document.getElementById("dispatchStatusDash").checked=true;
		} if(premiumCustomer==1){
			document.getElementById("premiumCustomerDash").checked=true;
		}
		if(drFlag!=""){
			for (var i=0; i<driverList;i++){
				if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
					document.getElementById('dchk'+i).checked=true;
				}
			}
			for (var i=0;i<vehicleList;i++){
				if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
					document.getElementById('vchk'+i).checked=true;
				}
			}
			openSplReqDash();
		}	
	}
	if(document.getElementById('CommentsDash1').value!=""){
		openDispatchCommentsDash();
		document.getElementById('CommentsDash1').style.color='#000000';
	}
	if(	document.getElementById('specialInsDash1').value!=""){
		openDriverCommentsDash();
		document.getElementById('specialInsDash1').style.color='#000000';
	}
	if(document.getElementById('airName').value!="" && document.getElementById('airNo').value!="" && document.getElementById('airFrom').value!="" && document.getElementById('airTo').value!="" ){
		//openFlightInformationDash();
		document.getElementById("airName").readOnly = false;
		document.getElementById("airNo").readOnly = false;
		document.getElementById("airFrom").readOnly = false;
		document.getElementById("airTo").readOnly = false;
	//	$("input[type=text]").attr('readonly',false);
		//$("#hideFlightInformation").attr('readonly',false);

		var ele = document.getElementById("hideFlightInformation");
		if(ele.style.display == "block") {
			
			ele.style.display = "none";
		}
		else {
			ele.style.display = "block";
			
		}

		
      }
	
}

