<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
	var isIE;
	var completeTable;
	var completeField;
	var autorow;
	var popup;
	var items = new Array();
	 var current = 0;
	 
	function AJAXInteraction(url, callback) {
		  
	    var req = init();
	    req.onreadystatechange = processRequest;
	    function init() {
	      if (window.XMLHttpRequest) {
	        return new XMLHttpRequest();
	      } else if (window.ActiveXObject) {
	        return new ActiveXObject("Microsoft.XMLHTTP");
	      }
	    } 
	    
	    function processRequest () {
	      // readyState of 4 signifies request is complete
	      if (req.readyState == 4) {
	        // status of 200 signifies sucessful HTTP call
	        if (req.status == 200) {
	          if (callback) callback(req.responseXML);
	        }
	      }
	    }

	    this.doGet = function() {
	      req.open("GET", url, true);
	      req.send(null);
	    }
	}
 
	function initRequest() {

		 
		 completeField = document.getElementById("complete-field");
		 var menu = document.getElementById("auto-row");
		 autorow = document.getElementById("menu-popup");
	 
	 	autorow.style.overflowY = 'auto';
 		autorow.scrollTop = 0;
	
		//autorow.style.maxHeight =  maxHeight + 'px';
		autorow.style.top = (getElementY(completeField)+completeField.offsetHeight+2) + "px";
		autorow.style.left = getElementX(completeField) + "px";
		autorow.style.width = completeField.offsetWidth + "px";
		
		completeTable = document.getElementById("completeTable");
		completeTable.setAttribute("bordercolor", "white");
		 
		
	}
	  
	
	function doCompletion() {
		if (completeField.value == "") {
			clearTable();
			 
		} else {
			var url = "/TDS/autocomplete1?id=" + escape(document.getElementById("complete-field").value);
			var ajax = new AJAXInteraction(url,postProcess);
			ajax.doGet();
	 	} 
	}
	 
	function postProcess(responseXML) {
	 	
		clearTable();
		var employees = responseXML.getElementsByTagName("drivers")[0];
		if (employees.childNodes.length > 0) {
			completeTable.setAttribute("bordercolor", "black");
			completeTable.setAttribute("border", "0");
		} else {
			clearTable();
		}
		for (loop = 0; loop < employees.childNodes.length; loop++) {
			var employee = employees.childNodes[loop];
			var id = employee.getElementsByTagName("id")[0];
			var name = employee.getElementsByTagName("name")[0];
			 
			appendEmployee(id.childNodes[0].nodeValue,name.childNodes[0].nodeValue);
		
		}
	}
	
	function clearTable() {
		if (completeTable){
			completeTable.setAttribute("bordercolor", "white");
			completeTable.setAttribute("border", "0");
			completeTable.style.visible = false;
		for (loop = completeTable.childNodes.length -1; loop >= 0 ; loop--) {
			completeTable.removeChild(completeTable.childNodes[ loop]);
			}
		}
	}


 
	
	function getElementY(element){ 
		var targetTop = 0;
		if (element.offsetParent) {
			while (element.offsetParent) {
				targetTop += element.offsetTop;
				element = element.offsetParent;
			}
		} else if (element.y) {
			targetTop += element.y;
		}
		return targetTop;
	}
	function getElementX(element){
		  var targetLeft = 0;
		  if (element.offsetParent) {
		    while (element.offsetParent) {
		      targetLeft += element.offsetLeft;
		      element = element.offsetParent;
		    }
		  } else if (element.x) {
		    targetLeft += element.yx;
		  }
		  return targetLeft;
		}
	
	function enter(name){
		
		document.getElementById("autofillform").id.value=name;
		
		clearTable();
	}
	
	function appendEmployee(id,name) {
		
		var items = new Array();
		var row;
		var nameCell;
		if (isIE) {
			row = completeTable.insertRow(completeTable.rows.length);
			nameCell = row.insertCell(0);
		}
		else {
			row = document.createElement("tr");
			nameCell = document.createElement("td");
			row.appendChild(nameCell);
			completeTable.appendChild(row);
		}
			row.className = "popupRow";
			nameCell.setAttribute("bgcolor", "#FFFAFA");
			nameCell.setAttribute("width", "60%");
			
			var linkElement = document.createElement("a");
			addOptionHandlers(nameCell);
			linkElement.className = "popupItem";
			linkElement.setAttribute("href","javascript:enter('"+id+"')");
			linkElement.appendChild(document.createTextNode(id +'('+name+')'));
			nameCell.appendChild(linkElement);
			items.push(nameCell);
			addKeyListener(autorow, start);
			//addListener(autorow, 'mouseover', handlePopupOver);
			//addListener(autorow, 'mouseout', handlePopupOut);

			
			
	}

	function start(e) {
		
		if (e.keyCode == 38) {
		      if (current > 0) {
		        items[current].className = '';
		        current--;
		        items[current].className = 'selected';
		        items[current].scrollIntoView(false);
		      } else {
		    	  items[current].className = 'selected';
		      }

		    //down arrow
		    }else if (e.keyCode == 40) {
	        if(current < items.length - 1) {
	            items[current].className = '';
	            current++;
	            items[current].className = 'selected';
	            items[current].scrollIntoView(false);
	          }
		    }else if (e.keyCode == 27) {
		    	autorow.style.visibility = 'hidden';
		        
		        if (isIE) {
		          event.returnValue = false;
		        } else {
		          e.preventDefault();
		        }
		    }
	}
	function addKeyListener(element, listener) {
		 
		  if (isIE) {
			  element.attachEvent("onkeydown",listener);
		  } else {
		    element.addEventListener("keypress",listener,false);
		  }
		}
	
	function addOptionHandlers(option) {
	     addListener(option, "mouseover", handleOver);
	  }

	function addListener(element, type, listener) {
		  if (element.addEventListener) {
		    element.addEventListener(type, listener, false);
		  } else {
		    element.attachEvent('on' + type, listener);
		  }
		}

	function handleOver(e) {
	   
	  }
	
</script>
<style type="text/css">
.popupItem {
	TEXT-DECORATION			: none;
	color					: #111111;
	font-family				: Arial;
	font-size				: 11px;
    font-weight			    : bold;
}

.autocomplete {
	position: absolute;
	color: #333;
	background-color: #fff; 
	border: 1px solid #666;
	visibility: visible;
	font-family: Arial;
	overflow: hidden;
}

.autocomplete tr {
	padding: 0;
	margin: 0;
	list-style: none;
}

.autocomplete td {
	display: block;
	white-space: nowrap;
	cursor: pointer;
	margin: 0px;
	padding-left: 5px;
	padding-right: 5px;
	border: 1px solid #fff;
}

.autocomplete td.selected {
	background-color: #cef;
	border-top: 1px solid #9bc;
	border-bottom: 1px solid #9bc;
}
 
 
</style>
<title>AJAX</title>
</head>
<body onload="initRequest()">
<h2>AJAX</h2>
<form id="autofillform"  method="get">
<table border="0" cellpadding="5" cellspacing="0">
<tr>
<td><b>Name:</b></td>
<td>
	<input type="text" size="10" autocomplete="off" id="complete-field" name="id" onkeyup="doCompletion();">
	<div style="position: absolute; top:165px;left:70px;" id="menu-popup"  class="autocomplete">
	<table id="completeTable" border="0" bordercolor="black" cellpadding="0" cellspacing="0" >
	</table>
</div>	
</td>
</tr>
<tr>
<td id="auto-row" colspan="2">

 <td/>
 </tr> 
</table>
</form>

</body>
</html>