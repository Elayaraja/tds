<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">

  function waitcursor()
       {
       
            document.body.style.cursor="wait";
            //document.getElementById('Button').value="Wait Processing may take up to 30 Seconds";
            //document.getElementById('Button').disabled=true;
            document.getElementById('but').innerHTML= "Wait Processing may take up to 30 Seconds";
            document.getElementById('Button').style.display='none';
            //Disables all the INPUT Controls
          

            //Disables all the HyperLink Controls
            for(i=0;i<document.all.tags('a').length;i++)
            {          

                 var io = document.all.tags('a')[i]; 
                 //Make sure you are not working with hidden Links

                 if(io.type != 'hidden')
                 {
                       io.disabled=true;
                       io.style.cursor ="wait";
                 }

              }

		return true;
  }


</script>
</head>
<body>
<form method="get" action="control" name="masterForm" onsubmit="return waitcursor()">
<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.getMCCProcess %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="voidPayment">
<table>
<tr>
<td>
<table id="pagebox">
	<tr> 
		<td>
			<table id="bodypage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Voiding&nbsp;Transaction</b></td>
			</tr>
			<tr>
				<td>Description</td>
				<td>
					<input type="hidden" name="transid" value="<%=request.getParameter("transid")==null?"0":request.getParameter("transid") %>">
					<textarea cols="15" rows="5" name="desc"><%=request.getParameter("desc")==null?"":request.getParameter("") %></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><div id="but"></div>
				<input type="submit" name="Button" id="Button" value="Void"></td>
			</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>			
</form>
</body>
</html>