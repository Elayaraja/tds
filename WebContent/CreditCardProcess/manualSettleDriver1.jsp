<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.ManualSettleBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>


<html>
<head>

<script type="text/javascript">

function selAll()
{
	if(document.getElementById('all').checked)
	{
		document.getElementById('CreditCard').checked = true;
		document.getElementById('Penalty').checked = true;
		document.getElementById('Voucher').checked = true;
		
	} else {
		document.getElementById('CreditCard').checked = false;
		document.getElementById('Penalty').checked = false;
		document.getElementById('Voucher').checked = false;
		
	}
}

function chckAll(size)
{

  if(document.getElementById('chck_all').checked)
  {	
	for(var i=0;i<Number(size);i++)
	{
  	 document.getElementById('Settle'+i).checked = true;
 	}
  } else {
	  for(var i=0;i<Number(size);i++)
		{
	  	  document.getElementById('Settle'+i).checked = false;
	 	}
  }
}

</script>

<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
	

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manual Settle</title>
</head>
<body>
<form method="post" action="control" name="masterForm" onsubmit="return caldatevalidation();">

<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="manualccp">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="manualSettle">
<table>
<tr>
<td>
<table id="pagebox">
	<tr> 
		<td>
			<table id="headerpage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Manual&nbsp;Settle</b></td>
			</tr>
			<tr>
			
				
				<td>Driver ID</td>
				<td>&nbsp;</td>
				 <td>&nbsp;</td>
				<td>
				 
				<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)"  autocomplete="off" class="form-autocomplete" onblur="caldid(id,'dirname','dName')" size="12" name="driver" id="driver"  value=<%=request.getParameter("driver")==null?"":request.getParameter("driver")%>  >&nbsp;&nbsp;&nbsp;
				<ajax:autocomplete
		  					fieldId="driver"
		  					popupId="model-popup1"
		  					targetId="driver"
		  					baseUrl="autocomplete.view"
		  					paramName="DRIVERID"
		  					className="autocomplete"
		  					progressStyle="throbbing" />
		  		<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
				<div id="dirname"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
												
				</td>
			</tr>
			<tr>
				<td> <input type="checkbox" name="CreditCard" id="CreditCard" <%=request.getParameter("CreditCard")==null?"":"selected" %>> Include CreditCard
				</td>
				<td> <input type="checkbox" name="Penalty" id="Penalty" <%=request.getParameter("Penalty")==null?"":"selected" %>> Include Misc Charges
				</td>
				<td> <input type="checkbox" name="Voucher" id="Voucher" <%=request.getParameter("Voucher")==null?"":"selected" %>> Include Vouchers
				</td>
				<td> <input type="checkbox" name="all" id="all" <%=request.getParameter("all")==null?"":"selected" %> onclick="selAll()"> Select All
				</td>
			
			
						<td align="center" colspan="7"><input type="submit" name="GetDetailsButton" value="Retrieve"></td>
				
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="pagebox">
	 
	<%if(session.getAttribute("manualSettle")!=null){ 
		ArrayList al_list=(ArrayList)session.getAttribute("manualSettle");
	%>
	<tr>
		<td>
			<table id="bodypage" border="1" align="center" cellpadding="0" cellspacing="0" width="600">
				<tr align="center">
					<td>S.No</td>
					<td>Description</td>
				 	<td>Date</td>
					<td>Amount</td>
					<td>Settle
					    <input type="checkbox" name="chck_all" id="chck_all" onclick="chckAll(<%=al_list.size() %>)">
					</td>
				</tr>
				<%for(int count=0;count<al_list.size();count++){
					ManualSettleBean settleBean = (ManualSettleBean)al_list.get(count); %>
				<tr align="center" id="trids<%=count %>">
					<td><%=count+1%></td>
					<td><%=settleBean.getDescription() %></td>
					<td><%=settleBean.getDate() %></td>
					<td><%=settleBean.getamt() %></td>
					<td><input type="checkbox" name="<%="Settle" + count%>"  id="<%="Settle" + count%>"></td>
				</tr>
				<%} %>
					<tr align="center">
					<td><input type="submit" name="ProcessButton" value="Settle"></td>
				</tr>
				
			</table>
			</td>			 
		</tr>
	<%} %>
</table>
</td>
</tr>
</table>
<form></form>
</body>
</html>