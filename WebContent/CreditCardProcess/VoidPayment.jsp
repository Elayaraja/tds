<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript">

  function waitcursor()
       {
       
            document.body.style.cursor="wait";
            //document.getElementById('Button').value="Wait Processing may take up to 30 Seconds";
            //document.getElementById('Button').disabled=true;
            document.getElementById('but').innerHTML= "Wait Processing may take up to 30 Seconds";
            document.getElementById('Button').style.display='none';
            //Disables all the INPUT Controls
          

            //Disables all the HyperLink Controls
            for(i=0;i<document.all.tags('a').length;i++)
            {          

                 var io = document.all.tags('a')[i]; 
                 //Make sure you are not working with hidden Links

                 if(io.type != 'hidden')
                 {
                       io.disabled=true;
                       io.style.cursor ="wait";
                 }

              }

		return true;
  }
</script>
</head>
<body>
<form method="get" action="control" name="masterForm" onsubmit="return waitcursor()">
<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.getMCCProcess %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="voidPayment">
<input type="hidden" name="systemsetup" value="6">
<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
        	 
            <div class="contentDV">
            <div class="contentDVInWrap">
            <div class="contentDVIn">
            	<div class="leftCol">
                	<div class="innerPage">
                    </div>
                </div>
                
                <div class="rightCol">
				<div class="rightColIn">
                	<div class="loginBX">
                	<div class="grayBX">
                    	<div class="topLCur"><div class="topRCur"><div class="topMCur"><div class="topMCur1"></div></div></div></div>
                        <div class="mid"><div class="mid1"><div class="mid2"><div class="mid3">
                        	<div class="formDV">
                        		<h2>Voiding Transaction</h2>
                                	<div class="fieldDv padB3">
                                    <fieldset>
                                    <label for="Description">Description</label>
                                    <div class="div4"><div class="div4In"><div class="inputBXWrap">
                                    <input type="hidden" name="transid" value="<%=request.getParameter("transid")==null?"0":request.getParameter("transid") %>">
									<textarea cols="15" rows="5" name="desc" class="inputBX"><%=request.getParameter("desc")==null?"":request.getParameter("") %></textarea>
                                    </div></div></div>
									</fieldset>
                                    
                                	                                   
                                    <fieldset>
                                    <div class="wid60 marAuto padT10">
                        			<div class="btnBlue">
                        			<div class="rht">
                        	 			<input type="submit" name="Button" id="Button" value="Void" class="lft"> 
                        	 		</div>
                            		<div class="clrBth"></div>
                        			</div>
                        			<div class="clrBth"></div>
                    				</div>			
									</fieldset>
									<fieldset>
									<div id="but"></div>
									</fieldset>
                                    </div>
                        	</div>
                        </div></div></div></div>
                        <div class="btmLCur"><div class="btmRCur"><div class="btmMCur"><div class="btmMCur1"><div class="btmMCur2"><div class="btmMCur3"><div class="btmMCur4"><div class="btmMCur5"></div></div></div></div></div></div></div></div>
                    </div>
                    </div>
                </div>    
              </div>
                <div class="clrBth"></div>
            </div>
            </div>
            </div>
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
		</div>
	</div>
</div>
</form>
</body>
</html>
