<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.cmp.bean.PenalityBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<%@page import="java.util.ArrayList"%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Insert title here</title>
	<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
	<script src="js/CalendarControl.js" language="javascript"></script>
	
	<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
	<%Calendar cal=Calendar.getInstance();%>
	
	<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
	<script type="text/javascript" src="js/ajaxcore.js"></script>
	<script type="text/javascript">
	
		/* This script and many more are available free online at
		The JavaScript Source!! http://javascript.internet.com
		Created by: David Leppek :: https://www.azcode.com/Mod10
		
		Basically, the alorithum takes each digit, from right to left and muliplies each second
		digit by two. If the multiple is two-digits long (i.e.: 6 * 2 = 12) the two digits of
		the multiple are then added together for a new number (1 + 2 = 3). You then add up the 
		string of numbers, both unaltered and new values and get a total sum. This sum is then
		divided by 10 and the remainder should be zero if it is a valid credit card. Hense the
		name Mod 10 or Modulus 10. */
		function Mod10(ccNumb) {  // v2.0
			var valid = "0123456789"  // Valid digits in a credit card number
			var len = ccNumb.length;  // The length of the submitted cc number
			var iCCN = parseInt(ccNumb);  // integer of ccNumb
			var sCCN = ccNumb.toString();  // string of ccNumb
			sCCN = sCCN.replace (/^\s+|\s+$/g,'');  // strip spaces
			var iTotal = 0;  // integer total set at zero
			var bNum = true;  // by default assume it is a number
			var bResult = false;  // by default assume it is NOT a valid cc
			var temp;  // temp variable for parsing string
			var calc;  // used for calculation of each digit
			
			// Determine if the ccNumb is in fact all numbers
			for (var j=0; j<len; j++) {
			  temp = "" + sCCN.substring(j, j+1);
			  if (valid.indexOf(temp) == "-1"){bNum = false;}
			}
			
			// if it is NOT a number, you can either alert to the fact, or just pass a failure
			if(!bNum){
			  /*alert("Not a Number");*/bResult = false;
			}
			
			// Determine if it is the proper length 
			if((len == 0)&&(bResult)){  // nothing, field is blank AND passed above # check
			  bResult = false;
			} else{  // ccNumb is a number and the proper length - let's see if it is a valid card number
			  if(len >= 15){  // 15 or 16 for Amex or V/MC
				   	 for(var i=len;i>0;i--){  // LOOP throught the digits of the card
				      calc = parseInt(iCCN) % 10;  // right most digit
				      calc = parseInt(calc);  // assure it is an integer
				      iTotal += calc;  // running total of the card number as we loop - Do Nothing to first digit
				      i--;  // decrement the count - move to the next digit in the card
				      iCCN = iCCN / 10;                               // subtracts right most digit from ccNumb
				      calc = parseInt(iCCN) % 10 ;    // NEXT right most digit
				      calc = calc *2;                                 // multiply the digit by two
				      // Instead of some screwy method of converting 16 to a string and then parsing 1 and 6 and then adding them to make 7,
				      // I use a simple switch statement to change the value of calc2 to 7 if 16 is the multiple.
				      switch(calc){
				        case 10: calc = 1; break;       //5*2=10 & 1+0 = 1
				        case 12: calc = 3; break;       //6*2=12 & 1+2 = 3
				        case 14: calc = 5; break;       //7*2=14 & 1+4 = 5
				        case 16: calc = 7; break;       //8*2=16 & 1+6 = 7
				        case 18: calc = 9; break;       //9*2=18 & 1+8 = 9
				        default: calc = calc;           //4*2= 8 &   8 = 8  -same for all lower numbers
				      }                                               
				    iCCN = iCCN / 10;  // subtracts right most digit from ccNum
				    iTotal += calc;  // running total of the card number as we loop
				  }  // END OF LOOP
				  if ((iTotal%10)==0){  // check to see if the sum Mod 10 is zero
				    bResult = true;  // This IS (or could be) a valid credit card number.
				  } else {
				    bResult = false;  // This could NOT be a valid credit card number
				  }
				}
			}
			// change alert to on-page display or other indication as needed.
			if(bResult) {
			 // alert("This IS a valid Credit Card Number!");
			}
			if(!bResult){
			  alert("This is NOT a valid Credit Card Number!");
			}
		  return bResult; // Return the results
		}

	
		
		function checkAmountTip()
		{
		     var flg = true,flg1=true;
			 if(!(chckAMT('amt')))
	        {
	            var answer = confirm('Amount is Greater than $200 Do you want to proceed');
	           // alert(answer);
	            if(answer)
	            {
	              //	alert("going to return"+answer);
	            	flg=  true;
	            } else {
	           		//alert(answer+":2");
	           		flg =  false;
	        		document.getElementById("amt").value="";
	        		//alert("");
	         		
	         	}
	        }
	        if(!(chckAMT('tip')))
	        {
	        	var answer = confirm('Tip Amount is Greater than $200 Do you want to proceed');
	        	//alert(answer);
	       	 	if(answer)
	            { 
	              	flg1=true; 
	            } else {
	        		document.getElementById("tip").value="";
	         		flg1= false
	         	}
	        }
	        
	        if(flg==true && flg1==true)
	         	return true;
	        else
	            return false;
		}	 
		
	   function waitcursor()
       {
       
        //alert("From Function:" + checkAmountTip() )
        if(!Mod10(document.masterForm.ccno.value))
          return false;
        if(!checkAmountTip())
           return false;
        if(document.getElementById("ccno").value == ""){
			document.getElementById("errorpage").innerHTML="Debit/Credit Card Number Not Valid";
		return false;
		}else if((document.getElementById("amt").value).trim() == "$" || document.getElementById("amt").value == "" ){
			document.getElementById("errorpage").innerHTML="Amt Not Valid";
		return false;
		}else if((document.getElementById("tip").value).trim() == "$" || document.getElementById("tip").value == ""){
			document.getElementById("errorpage").innerHTML="Tip Not Valid";
		return false;
		} else {
			document.getElementById("errorpage").innerHTML="";
		}
            document.body.style.cursor="wait";
            document.getElementById('Button').value="Wait Processing may take up to 30 Seconds";
            document.getElementById('Button').disabled=true;
            //Disables all the INPUT Controls
          

            //Disables all the HyperLink Controls
            for(i=0;i<document.all.tags('a').length;i++)
            {          

                 var io = document.all.tags('a')[i]; 
                 //Make sure you are not working with hidden Links

                 if(io.type != 'hidden')
                 {
                       io.disabled=true;
                       io.style.cursor ="wait";
                 }

              }

		return true;
  }
  
function focusOnCC()
{
	document.getElementById('ccno').focus();
}
function eventaction(){
document.body.style.cursor="auto";
            //Disables all the INPUT Controls
          
           //Disables all the HyperLink Controls
            for(i=0;i<document.all.tags('a').length;i++)
            {          

                 var io = document.all.tags('a')[i]; 
                 //Make sure you are not working with hidden Links

                 if(io.type != 'hidden')
                 {
                       io.disabled=false;
                       io.style.cursor ="auto";
                 }

              }

}  

function clearmsgid()
{
	document.getElementById('msgid').innerHTML = "";
}

function chckAMT(id)
{
	if(Number((document.getElementById(id).value).split(" ")[1]) > 200){
		//alert("Your Amount is Greater than $ 200");	
		return false;
	} else {
		return true;
	}
}

 </script>
	<%--
	<script type="text/javascript">
function cal(){
var al = window.open('', 'processing');  
window.setTimeout(function() {al.close()}, 5000);
}</script> --%>
</head>

<body onload="eventaction();focusOnCC();">
<form name="masterForm" action="control" method="post" onsubmit="return waitcursor();">
<input type="hidden" name="action" value="manualccp">
<input type="hidden" name="event" value="chckMaualUserCardInfo">
<input type="hidden" name="track1" value="">
 <input type="hidden" name="track2" value="">
 <%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
 %>
 <table id="pagebox">
 <tr>
 <td>
	<table > 
		<tr>
			<td>
				<table id="bodypage">	
					<tr>
						<td colspan="7" align="center">		
							<div id="title">
								<h2>Manual Credit/Debit Card Processing </h2>
							</div>
						</td>		
					</tr>
					<%
										
						String error = (String) request.getAttribute("error");
						
						
					%>
					<tr>
						<td colspan="7">
							<div id="errorpage">
								<%= (error !=null && error.length()>0)?""+error :"" %>
							</div>
							<div id="msgid">
							   	<font color=<%=request.getParameter("msgtype")!=null && request.getParameter("msgtype").equals("1")?"green":"red" %>>
									<%= request.getParameter("pageMsg")==null?"":request.getParameter("pageMsg") %>
								</font>
							</div>
						</td>
					</tr>
					<tr>
					<td valign="top">
							<table border=1>	
								<tr>
									<td>Card No <font color=red>*</font></td>
									<td><input type="text" name="ccno" id="ccno" size="18" tabindex="1"  value="<%=request.getParameter("ccno")==null?"":request.getParameter("ccno") %>" onkeyup="clearmsgid()"></td>
								</tr>
								<tr>
								 	<td>Exp Date  <font color=red>*</font></td>
								 	<td>
								 		<input type="text" name="expdate" tabindex="2" id="expdate" value="<%=request.getParameter("expdate")==null?"":request.getParameter("expdate") %>" size="4">
								 	</td>
								 	<%--  
									<td>
									
										<select name="expmonth" >
											<option value="01" <%=(request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 1)?"Selected":(request.getParameter("expmonth") !=null && request.getParameter("expmonth").equals("01"))?"selected":"" %> >Jan</option>
											<option value="02" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 2)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("02"))?"selected":"" %>>Feb</option>
											<option value="03" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 3)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("03"))?"selected":"" %>>Mar</option>
											<option value="04" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 4)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("04"))?"selected":"" %>>Apr</option>
											<option value="05" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 5)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("05"))?"selected":"" %>>May</option>
											<option value="06" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 6)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("06"))?"selected":"" %>>Jun</option>
											<option value="07" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 7)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("07"))?"selected":"" %>>Jul</option>
											<option value="08" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 8)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("08"))?"selected":"" %>>Aug</option>
											<option value="09" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 9)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("09"))?"selected":"" %>>Sep</option>
											<option value="10" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 10)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("10"))?"selected":"" %>>Oct</option>
											<option value="11" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 11)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("11"))?"selected":"" %>>Nov</option>
											<option value="12" <%= (request.getParameter("expmonth")==null && (cal.get(Calendar.MONTH) + 1) == 12)?"Selected":(request.getParameter("expmonth") != null && request.getParameter("expmonth").equals("12"))?"selected":"" %>>Dec</option>
								 		</select>
								 		<select name="expyear" >
								 		<%!int year = 2000; %>
								 		<%for(int i=0;i<100;i++){ %>
								 			<option value="<%=(""+(year+i)).substring(2) %>"  <%=(request.getParameter("expyear")==null  && (cal.get(Calendar.YEAR))==(year+i))?"selected":(request.getParameter("expyear") != null && request.getParameter("expyear").equals((""+(year+i)).substring(2)))?"selected":"" %> ><%=(""+(year+i)) %></option>
								 		<%} %>
								 		</select>
									</td>
									--%>
								</tr>
							 	<tr>
									<td>Amount<font color=red>*</font></td>
									<td><input type="text" name="amt" style=text-align:right tabindex="3" id="amt" size="8"  onblur="insert(id)"  value="<%=request.getParameter("amt")==null?"":request.getParameter("amt") %>"></td>
								</tr>
								<tr>
									<td>Tip<font color=red>*</font></td>
										<td><input type="text" name="tip" id="tip" style=text-align:right size="8" tabindex="4" onblur="insert(id)" value="<%=request.getParameter("tip")==null?"":request.getParameter("tip") %>"></td>
								</tr>
							 	<tr>
									<td>Driver ID <font color=red>*</font></td>
									<td>
									<input type="text" tabindex="5"  autocomplete="off" class="form-autocomplete"  onblur="caldid(id,'driname','dName')" size="12" name="driver_id" id="driver_id"  value=<%=request.getParameter("driver_id")==null?"":request.getParameter("driver_id")%>   >&nbsp;&nbsp;&nbsp;
									<ajax:autocomplete
					  					fieldId="driver_id"
					  					popupId="model-popup1"
					  					targetId="driver_id"
					  					baseUrl="autocomplete.view"
					  					paramName="DRIVERID"
					  					className="autocomplete"
					  					progressStyle="throbbing" />
					  						
					  				<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
									<div id="driname"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
									</td>
									<!--<td>
										<select name="driver_id" id="driver_id">
											<%// for(int j=0 ;j<al_driver.size();j=j+2){ %>
												<option value="<%//=al_driver.get(j).toString() %>" <%//=(al_driver.get(j).toString().equalsIgnoreCase(request.getParameter("driver_id")==null?"":request.getParameter("driver_id")))?"selected":"" %> ><%//=al_driver.get(j+1) %></option>
											<%//} %>
										</select>	
										
									</td>
								--></tr>
								<%-- 
								<tr>
									<td>Remark</td>
									<td>	
										<textarea name="bDesc"  cols="20" rows="5"><%=request.getParameter("bDesc")==null?"":request.getParameter("bDesc") %></textarea>
									</td>
								</tr>
								--%>
								</table>
							</td>
							<td valign="top">	
								<table border="1">
									<tr>
										<td colspan="4" align="center">Additional Info </td>
									</tr>
									<tr>
										<td colspan="2">CVV</td>
										<td colspan="2"><input type="text" name="cvv" tabindex="7" value="<%=request.getParameter("cvv")==null?"":request.getParameter("cvv") %>" size=4></td>
									 </tr>
							 		 	<tr>
											<td colspan="2">Zip Code</td>
											<td colspan="2"><input type="text" maxlength="5" tabindex="8"  name="zip" value="<%=request.getParameter("zip")==null?"":request.getParameter("zip") %>"></td>
										</tr>
								  	<tr>
										<td colspan="2" align="left"> Address1</td>
										<td colspan="2"><input type="text" name="add1" tabindex="9" value="<%=request.getParameter("add1")==null?"":request.getParameter("add1") %>"></td>  
									</tr> 
									<tr>
										<td colspan="2" align="left"> Address2</td>
										<td colspan="2"><input type="text" name="add2" tabindex="10" value="<%=request.getParameter("add2")==null?"":request.getParameter("add2") %>"></td>  
									</tr>
									<tr>
										<td colspan="2" align="left"> City</td>
										<td colspan="2"><input type="text" name="city" tabindex="11" value="<%=request.getParameter("city")==null?"":request.getParameter("city") %>"></td>  
									</tr>
									<tr>
										<td colspan="2" align="left"> State</td>
										<td colspan="2">
										<select name="state"  tabindex="12" onblur="javascript:document.getElementById('Button').focus();">
										  	<option value="AK" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >Alaska</option>
										 	<option value="AL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AL")?"selected":"" %> >Alabama</option>
										 	<option value="AR" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AR")?"selected":"" %> >Arkansas</option>
										 	<option value="AS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AS")?"selected":"" %> >American Samoa</option>
										 	<option value="AZ" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AZ")?"selected":"" %> >Arizona</option>
										 	<option value="CA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CA")?"selected":"" %> >California</option>
										 	<option value="CO" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CO")?"selected":"" %> >Colorado</option>
										 	<option value="CT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CT")?"selected":"" %> >Connecticut</option>
										 	<option value="DC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("DC")?"selected":"" %> >District of Columbia</option>
										 	<option value="DE" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("DE")?"selected":"" %> >Delaware</option>
										 	<option value="FL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("FL")?"selected":"" %> >Florida</option>
										 	<option value="GA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("GA")?"selected":"" %> >Georgia</option>
										 	<option value="HI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("HI")?"selected":"" %> >Hawaii</option>
										 	<option value="IA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("IA")?"selected":"" %> >Iowa</option>
										 	<option value="ID" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ID")?"selected":"" %> >Idaho</option>
										 	<option value="IL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >Illinois</option>
										 	<option value="IN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("IN")?"selected":"" %> >Indiana</option>
										 	<option value="KS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("KS")?"selected":"" %> >Kansas</option>
										 	<option value="KY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("KY")?"selected":"" %> >Kentucky</option>
										 	<option value="LA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("LA")?"selected":"" %> >Louisiana</option>
										 	<option value="MA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MA")?"selected":"" %> >Massachusetts</option>
										 	<option value="MD" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MD")?"selected":"" %> >Maryland</option>
										 	<option value="ME" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ME")?"selected":"" %> >Maine</option>
										 	<option value="MI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MI")?"selected":"" %> >Michigan</option>
										 	<option value="MN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MN")?"selected":"" %> >Minnesota</option>
										 	<option value="MO" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MO")?"selected":"" %> >Missouri</option>
										 	<option value="MP" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MP")?"selected":"" %> >Northern Mariana Islands</option>
										 	<option value="MS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MS")?"selected":"" %> >Mississippi</option>
										 	<option value="MT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MT")?"selected":"" %> >Montana</option>
										 	<option value="NC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NC")?"selected":"" %> >North Carolina</option>
										 	<option value="ND" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ND")?"selected":"" %> >North Dakota</option>
										 	<option value="NE" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NE")?"selected":"" %> >Nebraska</option>
										 	<option value="NH" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NH")?"selected":"" %> >New Hampshire</option>
										 	<option value="NJ" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NJ")?"selected":"" %> >New Jersey</option>
										 	<option value="NM" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >New Mexico</option>
										 	<option value="NV" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NV")?"selected":"" %> >Nevada</option>
										 	<option value="NY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NY")?"selected":"" %> >New York</option>
										 	<option value="OH" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OH")?"selected":"" %> >Ohio</option>
										 	<option value="OK" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OK")?"selected":"" %> >Oklahoma</option>
										 	<option value="OR" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OR")?"selected":"" %> >Oregon</option>
										 	<option value="PA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("PA")?"selected":"" %> >Pennsylvania</option>
										 	<option value="RI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("RI")?"selected":"" %> >Rhode Island</option>
										 	<option value="SC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("SC")?"selected":"" %> >South Carolina</option>
										 	<option value="SD" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("SD")?"selected":"" %> >South Dakota</option>
										 	<option value="TN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("TN")?"selected":"" %> >Tennessee</option>
										 	<option value="TX" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("TX")?"selected":"" %> >Texas</option>
										 	<option value="UT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("UT")?"selected":"" %> >Utah</option>
										 	<option value="VA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("VA")?"selected":"" %> >Virginia</option>
										 	<option value="VT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("VT")?"selected":"" %> >Vermont</option>
										 	<option value="WA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WA")?"selected":"" %> >Washington</option>
										 	<option value="WI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WI")?"selected":"" %> >Wisconsin</option>
										 	<option value="WV" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WV")?"selected":"" %> >West Virginia</option>
										 	<option value="WY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WY")?"selected":"" %> >Wyoming</option>
	 									</select>
									</td>  
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</td>
				</tr>
				<tr>
					<TD colspan="2" align="center">
						<input type="submit" name="Button" id="Button" tabindex="6"  value="Submit"  >&nbsp;
						<input type="reset" name="Button" value="Reset">
					</TD>
				</tr>					
			</table>
		</td>
	</tr>
	
</table>
</form>				
</body>
</html>