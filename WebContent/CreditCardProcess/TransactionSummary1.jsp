<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.TransactionSummaryBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<html>
<head>
<%! int startValue =1 ;double limit=0;int pstart=1;int pend=0;int pageNo=1;int plength=0;%>
<%if(request.getAttribute("startValue") != null){
	startValue = Integer.parseInt((String)request.getAttribute("startValue"));
	startValue=startValue+1;
}	
if(request.getAttribute("plength")!=null){
	plength=Integer.parseInt((String)request.getAttribute("plength"));
	}
%>


<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
 <script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
 
 <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
  
<script type="text/javascript">
function makeCapture(transid)
{
	window.location.href = "/TDS/control?action=manualccp&event=capturePayment&transid="+transid;	
}

function loadDiv(source,tranid,target)
{

	document.getElementById(target).style.background="orange";	
	document.getElementById(source).innerHTML =  "Really Return? <a href='#' onclick=\"makeVoid('"+tranid+"')\">Yes</a> / <a href='#' onclick=makeRedo('"+source+"','"+tranid+"','"+target+"')>No</a> ";
}
function makeRedo(source,tranid,target)
{
	document.getElementById(source).innerHTML = "<input type='button' value='Return' onclick=\"loadDiv('"+source+"','"+tranid+"','"+target+"') \">";
	document.getElementById(target).style.background="none";
}
function makeVoid(transid)
{
	 //var flg = confirm("Do you really want to void this transaction"); 
	// if(flg)
	 	window.location.href = "/TDS/control?action=manualccp&event=voidPayment&transid="+transid;
}
function makeAlter(total,transid)
{
	 
	 var flg = confirm("Do you really want to alter this transaction"); 
	 if(flg)
	 	window.location.href = "/TDS/control?action=manualccp&event=alterPayment&transid="+transid+"&amount="+total;
}
function caldatevalidation(){
	var fromdate=document.getElementById("from_date").value;
	var todate=document.getElementById("to_date").value
	fromdate = fromdate.substring(6,10)+fromdate.substring(0, 2)+fromdate.substring(3, 5);
	todate = todate.substring(6,10)+todate.substring(0, 2)+todate.substring(3, 5);
		if(fromdate>todate){
		alert("Check Fromdate And Todate");
       	  return false;
		}else {
			return true;
		}
}
</script>
</head>
<body>
<form method="post" action="control" name="masterForm" onsubmit="return caldatevalidation();">
<%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
	String type = request.getAttribute("type")==null?"0":request.getAttribute("type").toString();
%>

<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.getMCCProcess %>">
<%if(type.equals("0")) {%>
	<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="getSummarytoAlter">
<%} else { %>
	<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="getSummarytoAlterlimit">
<%} %>
<table>
<tr>
<td>
<table id="pagebox">
	<tr> 
		<td>
			<table id="bodypage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Transaction&nbsp;Summary</b></td>
			</tr>
			<tr>
			
				
				<td>Driver ID</td>
				<td>&nbsp;</td>
				 
				<td>
				 
				<input type="text"   autocomplete="off" class="form-autocomplete" onblur="caldid(id,'dirname','dName')" size="12" name="driver" id="driver"  value=<%=request.getParameter("driver")==null?"":request.getParameter("driver")%>  >&nbsp;&nbsp;&nbsp;
				<ajax:autocomplete
		  					fieldId="driver"
		  					popupId="model-popup1"
		  					targetId="driver"
		  					baseUrl="autocomplete.view"
		  					paramName="DRIVERID"
		  					className="autocomplete"
		  					progressStyle="throbbing" />
		  		<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">		
				<div id="dirname"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
				</td>
				
				<%--
				<td>
					<select name="driver">
						<option value="">Select</option>
						<% for(int j=0 ;j<al_driver.size();j=j+2){  
							<option value="<%=al_driver.get(j).toString() %>" <%=(al_driver.get(j).toString().equalsIgnoreCase(request.getParameter("driver")==null?"":request.getParameter("driver")))?"selected":"" %> ><%=al_driver.get(j+1) %></option>
						<%} %>
					</select>	
				</td>
				--%>
				<td>&nbsp;</td> 
				<td>Amount Greater Than</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  name="amount" value="<%= request.getParameter("amount")==null?"":request.getParameter("amount")%>">
				</td>
			</tr>
	 
			<tr>
				<td>From Date</td>
				<td>&nbsp;</td>
				<td>
				 	<input type="text" name="from_date" size="10" value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly" onfocus="showCalendarControl(this);">
					
				</td>	
				<td>&nbsp;</td>
				<td>To Date</td>
				<td>&nbsp;</td>
				<td>
				 	<input type="text" name="to_date" size="10" value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly" onfocus="showCalendarControl(this);">
					
				</td>
			 </tr>
			 <tr>
			 	<td>Approval Code</td>
			 	<td>&nbsp;</td>
				<td>
				 	<input type="text" name="approval_code" size="10" value="<%=request.getParameter("approval_code")==null?"":request.getParameter("approval_code") %>">
					
				</td>	
				<td>&nbsp;</td>
				<td>Card No(Last 4 Digit)</td>
				<td>&nbsp;</td>
				<td>
					<input type="text" name="card_no" size="5" value="<%=request.getParameter("card_no")==null?"":request.getParameter("card_no") %>">
				</td>
			 </tr>
			 <tr>
			 	<td>
			 		<input type="checkbox" name="approval" <%=request.getParameter("approval")==null?"":"selected" %>> Include Declined
			 	</td>
			 </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="7"><input type="submit" name="Button" value="Search"></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="pagebox">
	 
	<%if(request.getAttribute("al_list")!=null){ 
		ArrayList al_list=(ArrayList)request.getAttribute("al_list");
	%>
	<tr>
		<td>
			<table id="bodypage" border="1" align="center" cellpadding="0" cellspacing="0" width="600">
				<tr align="center">
					<td>S.No</td>
					<td>Driver Name</td>
				 	<td>Card&nbsp;No</td>
				 	<td>Approval Code</td>
					<td>Amount</td>
					<td>Tip Amount</td>
					<td>Total Amount</td>
					<td>Process&nbsp;Date</td>
					<td>Approval Status</td>
					<%if(type.equals("0")) {%>
						<td></td>
						<td></td>
					<%} %>
				</tr>
				<%for(int count=0;count<al_list.size();count++){
					TransactionSummaryBean summaryBean = (TransactionSummaryBean)al_list.get(count); %>
				<tr align="center" id="trids<%=count %>">
					<td><%=count+1%></td>
					<td><%=summaryBean.getT_driver() %></td>
					<td><%=summaryBean.getT_card_no() %></td>
					<td><%=summaryBean.getT_approval_code() %></td>
					<td align="right"><%=summaryBean.getT_amout() %></td>
					<td align="right"><%=summaryBean.getT_tip() %></td>
					<td align="right"><%=summaryBean.getT_total()%></td>
					<td><%=summaryBean.getT_processDate()%></td>
					<td><%=summaryBean.getT_approval_status().equals("1")?"Approved":"Declined"%></td>
					<%if(type.equals("0")) {%>
						<td>
						<%if(!summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg() && summaryBean.getT_approval_status().equals("1")){ %>
						<div id="id<%=count %>"> 
							<input type="button" value="Return" onclick="loadDiv('id<%=count %>','<%=summaryBean.getT_trans_id() %>','trids<%=count %>')">
						</div>	
						<%} else if(summaryBean.getT_approval_status().equals("1")) { %>
							<%=summaryBean.isT_voidflg()?"Txn Returned on " +summaryBean.getT_voiddate():summaryBean.isT_voidedflg()?"Genarate From Voiding":"" %>
						<%} %>
							<%--<input type="button" value="Void" onclick="makeVoid('<%=summaryBean.getT_trans_id() %>')"> 
							<input type="button" value="Alter" onclick="makeAlter('<%=summaryBean.getT_total()%>','<%=summaryBean.getT_trans_id() %>')"> --%>
						</td>
						<td>
							<%if(!summaryBean.getT_authcapflg().equals("C") && !summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg() && summaryBean.getT_approval_status().equals("1")) {%>
								<input type="button" value="Capture" onclick="makeCapture('<%=summaryBean.getT_trans_id() %>')">
							<%} %>
						</td>
					<%} %>
				</tr>
				<%} %>
			</table>
			</td>			 
		</tr>
		<tr>
			<td>
			<table align="center">
					<tr align="center">
						<%  
							if(request.getAttribute("pstart")!=null){
								pstart= Integer.parseInt((String)request.getAttribute("pstart"));
								}
							if(request.getAttribute("pend")!=null){
								pend=Integer.parseInt((String)request.getAttribute("pend"));
								}
							if(request.getAttribute("limit")!=null){
								limit = Double.parseDouble((String)request.getAttribute("limit"));
								}
								int start=0;
								int end =0;
							if(request.getAttribute("start")!=null){
								start=Integer.parseInt(request.getAttribute("start").toString())+1;
								end=Integer.parseInt(request.getAttribute("end").toString())+1;
							}
				 			
							System.out.println("limit:"+limit);
							System.out.println("PLength :"+plength);
							 
							 if(limit<=plength)
				 				{ 	 		
			 	 					for(int i=1;i<=limit;i++){ %>
			 	 					<%if(type.equals("0")) {%>
			 	 						<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlter&Button=Search&subevent=access&page<%=i %>=<%=start%>"><%=i %></a></td>
			 	 					<%} else { %>
			 	 						<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlterlimit&Button=Search&subevent=access&page<%=i %>=<%=start%>"><%=i %></a></td>
			 	 					<%} %>	
			 	 					<%} 
			 	 				}				 
							if(limit>=plength)
								{ 
									if(pstart!=1){
									%>
										<%if(type.equals("0")) {%>
			 	 							<td><a href="/TDS/control?action=userrolerequest&event=getUserList&Button=Search&subevent=access&previous=<%=""+pstart%>&first=no">&lt;</a></td>
			 	 						<%} else { %>
			 	 							<td><a href="/TDS/control?action=userrolerequest&event=getUserListlimit&Button=Search&subevent=access&previous=<%=""+pstart%>&first=no">&lt;</a></td>
			 	 						<%} %>	
									<%}
									for(int i=pstart;i<pend;i++){%>
										<%if(type.equals("0")) {%>				
											<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlter&Button=Search&subevent=access&pageNo=<%=i%>&pstart=<%=""+pstart%>&pend=<%=""+pend %>&first=no"><%=i %></a></td>
										<%} else { %>
											<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlterlimit&Button=Search&subevent=access&pageNo=<%=i%>&pstart=<%=""+pstart%>&pend=<%=""+pend %>&first=no"><%=i %></a></td>
										<%} %>	
									<%}
									if(pend<=limit){	%>
										<%if(type.equals("0")) {%>
											<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlter&Button=Search&subevent=access&next=<%=pend+""%>&first=no">&gt;</a></td>
										<%} else { %>
											<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlterlimit&Button=Search&subevent=access&next=<%=pend+""%>&first=no">&gt;</a></td>
										<%} %>	
									<%} 
								}%>
					</tr>
				</table>
			</td></tr>	
	<%} %>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>