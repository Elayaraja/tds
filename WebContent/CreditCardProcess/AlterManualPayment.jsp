<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.common.util.TDSConstants"%><html>
<head>
<%
	String error = request.getAttribute("error")==null?"":request.getAttribute("error").toString();
%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form method="post" action="control" name="masterForm">
<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.getMCCProcess %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="alterPayment">
<table>
<tr>
<td>
<table id="pagebox"> 
	<tr> 
		<td>
			<table id="bodypage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Alter&nbsp;Transaction</b></td>
			</tr>
			<tr>
				<td><font color="red"> <%= error == null?"":error.toString() %> </font></td>
			</tr>
			<tr>
				<td>Entered Amount</td>
				<td>
					<input type="text" readonly="readonly" name="amount" value="<%=request.getParameter("amount")==null?"0":request.getParameter("amount") %>">
				</td>
			</tr>
			<tr>
				<td>Alter Amount</td>
				<td>
					<input type="hidden" name="trans_id" value="<%=request.getParameter("transid")==null?"0":request.getParameter("transid") %>">
					<input type="text" name="alter_amount" value="<%=request.getParameter("alter_amount")==null?"0":request.getParameter("alter_amount") %>">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="Button" value="Alter"></td>
			</tr>
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
</table>			
</form>
</body>
</html>