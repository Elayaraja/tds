<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.cmp.bean.PenalityBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Calendar"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
	<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
	<script src="js/CalendarControl.js" language="javascript"></script>
	
	<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
	<%Calendar cal=Calendar.getInstance();%>
	
	<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
	<script type="text/javascript" src="js/ajaxcore.js"></script>
	<script type="text/javascript">
	
		/* This script and many more are available free online at
		The JavaScript Source!! http://javascript.internet.com
		Created by: David Leppek :: https://www.azcode.com/Mod10
		
		Basically, the alorithum takes each digit, from right to left and muliplies each second
		digit by two. If the multiple is two-digits long (i.e.: 6 * 2 = 12) the two digits of
		the multiple are then added together for a new number (1 + 2 = 3). You then add up the 
		string of numbers, both unaltered and new values and get a total sum. This sum is then
		divided by 10 and the remainder should be zero if it is a valid credit card. Hense the
		name Mod 10 or Modulus 10. */
		function Mod10(ccNumb) {  // v2.0
			var valid = "0123456789"  // Valid digits in a credit card number
			var len = ccNumb.length;  // The length of the submitted cc number
			var iCCN = parseInt(ccNumb);  // integer of ccNumb
			var sCCN = ccNumb.toString();  // string of ccNumb
			sCCN = sCCN.replace (/^\s+|\s+$/g,'');  // strip spaces
			var iTotal = 0;  // integer total set at zero
			var bNum = true;  // by default assume it is a number
			var bResult = false;  // by default assume it is NOT a valid cc
			var temp;  // temp variable for parsing string
			var calc;  // used for calculation of each digit
			
			// Determine if the ccNumb is in fact all numbers
			for (var j=0; j<len; j++) {
			  temp = "" + sCCN.substring(j, j+1);
			  if (valid.indexOf(temp) == "-1"){bNum = false;}
			}
			
			// if it is NOT a number, you can either alert to the fact, or just pass a failure
			if(!bNum){
			  /*alert("Not a Number");*/bResult = false;
			}
			
			// Determine if it is the proper length 
			if((len == 0)&&(bResult)){  // nothing, field is blank AND passed above # check
			  bResult = false;
			} else{  // ccNumb is a number and the proper length - let's see if it is a valid card number
			  if(len >= 15){  // 15 or 16 for Amex or V/MC
				   	 for(var i=len;i>0;i--){  // LOOP throught the digits of the card
				      calc = parseInt(iCCN) % 10;  // right most digit
				      calc = parseInt(calc);  // assure it is an integer
				      iTotal += calc;  // running total of the card number as we loop - Do Nothing to first digit
				      i--;  // decrement the count - move to the next digit in the card
				      iCCN = iCCN / 10;                               // subtracts right most digit from ccNumb
				      calc = parseInt(iCCN) % 10 ;    // NEXT right most digit
				      calc = calc *2;                                 // multiply the digit by two
				      // Instead of some screwy method of converting 16 to a string and then parsing 1 and 6 and then adding them to make 7,
				      // I use a simple switch statement to change the value of calc2 to 7 if 16 is the multiple.
				      switch(calc){
				        case 10: calc = 1; break;       //5*2=10 & 1+0 = 1
				        case 12: calc = 3; break;       //6*2=12 & 1+2 = 3
				        case 14: calc = 5; break;       //7*2=14 & 1+4 = 5
				        case 16: calc = 7; break;       //8*2=16 & 1+6 = 7
				        case 18: calc = 9; break;       //9*2=18 & 1+8 = 9
				        default: calc = calc;           //4*2= 8 &   8 = 8  -same for all lower numbers
				      }                                               
				    iCCN = iCCN / 10;  // subtracts right most digit from ccNum
				    iTotal += calc;  // running total of the card number as we loop
				  }  // END OF LOOP
				  if ((iTotal%10)==0){  // check to see if the sum Mod 10 is zero
				    bResult = true;  // This IS (or could be) a valid credit card number.
				  } else {
				    bResult = false;  // This could NOT be a valid credit card number
				  }
				}
			}
			// change alert to on-page display or other indication as needed.
			if(bResult) {
			 // alert("This IS a valid Credit Card Number!");
			}
			if(!bResult){
			  alert("This is NOT a valid Credit Card Number!");
			}
		  return bResult; // Return the results
		}

	
		
		function checkAmountTip()
		{
		     var flg = true,flg1=true;
			 if(!(chckAMT('amt')))
	        {
	            var answer = confirm('Amount is Greater than $200 Do you want to proceed');
	           // alert(answer);
	            if(answer)
	            {
	              //	alert("going to return"+answer);
	            	flg=  true;
	            } else {
	           		//alert(answer+":2");
	           		flg =  false;
	        		document.getElementById("amt").value="";
	        		//alert("");
	         		
	         	}
	        }
	        if(!(chckAMT('tip')))
	        {
	        	var answer = confirm('Tip Amount is Greater than $200 Do you want to proceed');
	        	//alert(answer);
	       	 	if(answer)
	            { 
	              	flg1=true; 
	            } else {
	        		document.getElementById("tip").value="";
	         		flg1= false
	         	}
	        }
	        
	        if(flg==true && flg1==true)
	         	return true;
	        else
	            return false;
		}	 
		
	   function waitcursor()
       {
       
        //alert("From Function:" + checkAmountTip() )
        if(!Mod10(document.masterForm.ccno.value))
          return false;
        if(!checkAmountTip())
           return false;
        if(document.getElementById("ccno").value == ""){
			document.getElementById("errorpage").innerHTML="Debit/Credit Card Number Not Valid";
		return false;
		}else if((document.getElementById("amt").value).trim() == "$" || document.getElementById("amt").value == "" ){
			document.getElementById("errorpage").innerHTML="Amt Not Valid";
		return false;
		}else if((document.getElementById("tip").value).trim() == "$" || document.getElementById("tip").value == ""){
			document.getElementById("errorpage").innerHTML="Tip Not Valid";
		return false;
		} else {
			document.getElementById("errorpage").innerHTML="";
		}
            document.body.style.cursor="wait";
            document.getElementById('Button').value="Wait Processing may take up to 30 Seconds";
            document.getElementById('Button').disabled=true;
            //Disables all the INPUT Controls
          

            //Disables all the HyperLink Controls
            for(i=0;i<document.all.tags('a').length;i++)
            {          

                 var io = document.all.tags('a')[i]; 
                 //Make sure you are not working with hidden Links

                 if(io.type != 'hidden')
                 {
                       io.disabled=true;
                       io.style.cursor ="wait";
                 }

              }

		return true;
  }
  
function focusOnCC()
{
	document.getElementById('ccno').focus();
}
function eventaction(){
document.body.style.cursor="auto";
            //Disables all the INPUT Controls
          
           //Disables all the HyperLink Controls
            for(i=0;i<document.all.tags('a').length;i++)
            {          

                 var io = document.all.tags('a')[i]; 
                 //Make sure you are not working with hidden Links

                 if(io.type != 'hidden')
                 {
                       io.disabled=false;
                       io.style.cursor ="auto";
                 }

              }

}  

function clearmsgid()
{
	document.getElementById('msgid').innerHTML = "";
}

function chckAMT(id)
{
	if(Number((document.getElementById(id).value).split(" ")[1]) > 200){
		//alert("Your Amount is Greater than $ 200");	
		return false;
	} else {
		return true;
	}
}

 </script>
</head>

<body onload="eventaction();focusOnCC();">
<input type="hidden" name="financial" value="3"/>
<form name="masterForm" action="control" method="post" onsubmit="return waitcursor();"/>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="action" value="manualccp"/>
<input type="hidden" name="event" value="manualCCProcess"/>
<input type="hidden" name="track1" value=""/>
 <input type="hidden" name="track2" value=""/>
 <%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
 %>

 
            	<div class="leftCol">
                	<div class="clrBth">
                    </div>
                </div>
                <div class="rightCol">
<div class="rightColIn">
               					 <h2><center>Manual Credit/Debit Card Processing</center> </h2>
                         			<%
                        			String error = (String) request.getAttribute("error");
									%>
									<div id="errorpage">
									<%= (error !=null && error.length()>0)?""+error :"" %></div>
									<div id="msgid">
									<font color=<%=request.getParameter("msgtype")!=null && request.getParameter("msgtype").equals("1")?"green":"red" %>>
									<%= request.getParameter("pageMsg")==null?"":request.getParameter("pageMsg") %>
								   	</font>
								   	</div>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                              <tr>
                              <td>
                              	<table id="bodypage" width="540" >
                              		<tr>
                              			<td  class="firstCol">Card No * </td>
                              			<td><input type="text" name="ccno"       tabindex="1"    id="ccno" size="18"   value="<%=request.getParameter("ccno")==null?"":request.getParameter("ccno") %>" onkeyup="clearmsgid()"/></td>
                              			<td class="firstCol">CVV</td>
                              			<td><input type="text" name="cvv"   tabindex="8"  value="<%=request.getParameter("cvv")==null?"":request.getParameter("cvv") %>" size=4/></td>
                              		</tr>
                              		<tr>
                              		<td class="firstCol">Exp Date *</td>
                              			<td><input type="text" name="expdate"   tabindex="2"  id="expdate" value="<%=request.getParameter("expdate")==null?"":request.getParameter("expdate") %>" size="10"/></td>
                              			
                              			<td class="firstCol">Zip Code</td>
                              			<td><input type="text" " maxlength="5"   tabindex="9"   name="zip"  value="<%=request.getParameter("zip")==null?"":request.getParameter("zip") %>"/>
                              			</td>
                              		
                              			
                              			
                              		</tr>
                              		<tr>
                              		<td  class="firstCol">Amount * </td>
                              			<td><input type="text" name="amt"   tabindex="3"  style=text-align:right   id="amt" size="8"  onblur="insert(id)"  value="<%=request.getParameter("amt")==null?"":request.getParameter("amt") %>"/></td>
                              		
                              		<td class="firstCol">Address1</td>
                              		<td><input type="text" name="add1"    tabindex="10"     value="<%=request.getParameter("add1")==null?"":request.getParameter("add1") %>"/></td>
                              		
                              		
                              			
                              		</tr>
                              		<tr>
                              		<td class="firstCol">Tip *</td>
                              			<td><input type="text" name="tip"     tabindex="4" id="tip" style=text-align:right size="8"   onblur="insert(id)" value="<%=request.getParameter("tip")==null?"":request.getParameter("tip") %>"/></td>
                              			<td class="firstCol">Address2</td>
                              			<td><input type="text" name="add2"      tabindex="11"   value="<%=request.getParameter("add2")==null?"":request.getParameter("add2") %>"/>
                              			</td>
                              			
                              			
                              		
                              			
                              			</tr>
                              		<tr>
                              		
                              			<td class="firstCol">Driver ID</td>
                              			<td><input type="text"  tabindex="5"         onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername','dName')" id="dd_driver_id" name="driver_id" size="15"    autocomplete="off" value="<%=request.getParameter("driver_id")==null?"":request.getParameter("driver_id")%>"/>
                              			 <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 			
							
								  <input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>"/>		
							 	  <div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
                              			</td>
                              			
                              		
                              			<td class="firstCol">City</td>
                              			<td><input type="text" name="city"     tabindex="12"  value="<%=request.getParameter("city")==null?"":request.getParameter("city") %>"/></td>
                              			
                              			</tr>
                              		<tr>
                              		<td class="firstCol">Category</td>
                              		<td class="firstCol"><select name="category" id="category" tabindex="6">
											<option value="Auth" selected="selected">Authorize</option>
											<option value="Sale">Sale</option></select>
                              		</td>
                              		<td class="firstCol">State</td>
                              			<td> <div class="div2"><div class="div2In">
                                    	
                                        	<select name="state"  tabindex="13" onblur="javascript:document.getElementById('Button').focus();">
										  	<option value="AK" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >Alaska</option>
										 	<option value="AL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AL")?"selected":"" %> >Alabama</option>
										 	<option value="AR" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AR")?"selected":"" %> >Arkansas</option>
										 	<option value="AS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AS")?"selected":"" %> >American Samoa</option>
										 	<option value="AZ" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AZ")?"selected":"" %> >Arizona</option>
										 	<option value="CA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CA")?"selected":"" %> >California</option>
										 	<option value="CO" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CO")?"selected":"" %> >Colorado</option>
										 	<option value="CT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("CT")?"selected":"" %> >Connecticut</option>
										 	<option value="DC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("DC")?"selected":"" %> >District of Columbia</option>
										 	<option value="DE" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("DE")?"selected":"" %> >Delaware</option>
										 	<option value="FL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("FL")?"selected":"" %> >Florida</option>
										 	<option value="GA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("GA")?"selected":"" %> >Georgia</option>
										 	<option value="HI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("HI")?"selected":"" %> >Hawaii</option>
										 	<option value="IA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("IA")?"selected":"" %> >Iowa</option>
										 	<option value="ID" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ID")?"selected":"" %> >Idaho</option>
										 	<option value="IL" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >Illinois</option>
										 	<option value="IN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("IN")?"selected":"" %> >Indiana</option>
										 	<option value="KS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("KS")?"selected":"" %> >Kansas</option>
										 	<option value="KY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("KY")?"selected":"" %> >Kentucky</option>
										 	<option value="LA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("LA")?"selected":"" %> >Louisiana</option>
										 	<option value="MA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MA")?"selected":"" %> >Massachusetts</option>
										 	<option value="MD" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MD")?"selected":"" %> >Maryland</option>
										 	<option value="ME" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ME")?"selected":"" %> >Maine</option>
										 	<option value="MI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MI")?"selected":"" %> >Michigan</option>
										 	<option value="MN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MN")?"selected":"" %> >Minnesota</option>
										 	<option value="MO" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MO")?"selected":"" %> >Missouri</option>
										 	<option value="MP" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MP")?"selected":"" %> >Northern Mariana Islands</option>
										 	<option value="MS" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MS")?"selected":"" %> >Mississippi</option>
										 	<option value="MT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("MT")?"selected":"" %> >Montana</option>
										 	<option value="NC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NC")?"selected":"" %> >North Carolina</option>
										 	<option value="ND" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("ND")?"selected":"" %> >North Dakota</option>
										 	<option value="NE" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NE")?"selected":"" %> >Nebraska</option>
										 	<option value="NH" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NH")?"selected":"" %> >New Hampshire</option>
										 	<option value="NJ" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NJ")?"selected":"" %> >New Jersey</option>
										 	<option value="NM" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("AK")?"selected":"" %> >New Mexico</option>
										 	<option value="NV" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NV")?"selected":"" %> >Nevada</option>
										 	<option value="NY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("NY")?"selected":"" %> >New York</option>
										 	<option value="OH" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OH")?"selected":"" %> >Ohio</option>
										 	<option value="OK" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OK")?"selected":"" %> >Oklahoma</option>
										 	<option value="OR" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("OR")?"selected":"" %> >Oregon</option>
										 	<option value="PA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("PA")?"selected":"" %> >Pennsylvania</option>
										 	<option value="RI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("RI")?"selected":"" %> >Rhode Island</option>
										 	<option value="SC" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("SC")?"selected":"" %> >South Carolina</option>
										 	<option value="SD" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("SD")?"selected":"" %> >South Dakota</option>
										 	<option value="TN" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("TN")?"selected":"" %> >Tennessee</option>
										 	<option value="TX" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("TX")?"selected":"" %> >Texas</option>
										 	<option value="UT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("UT")?"selected":"" %> >Utah</option>
										 	<option value="VA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("VA")?"selected":"" %> >Virginia</option>
										 	<option value="VT" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("VT")?"selected":"" %> >Vermont</option>
										 	<option value="WA" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WA")?"selected":"" %> >Washington</option>
										 	<option value="WI" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WI")?"selected":"" %> >Wisconsin</option>
										 	<option value="WV" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WV")?"selected":"" %> >West Virginia</option>
										 	<option value="WY" <%= request.getParameter("state")==null?"":request.getParameter("state").equals("WY")?"selected":"" %> >Wyoming</option>
                                        	
                                        </select>
                                    </div></div>
                                    
                              			</td>
                              			
                              		</tr>
                              		<tr><td class="firstCol">TripId</td><td class="firstCol" ><input type="text" name="tripid"      tabindex="7"   value=""/></td></tr>
                       	<tr   align="center"   >
                              		  
                              		<td     align="center"   colspan="4"   > 
                              			<input type="submit" name="Button"   tabindex="14"   id="Button"   value="Submit"  /> 
                              			<input type="reset" name="Button"   tabindex="15"  value="Reset"/>
                              			</td>
                               			 
                              		 	 
                              		</tr>
                              		
                              	</table> 
                              	 
                              </td>
                              </tr>
                              </table>
                            
                               
                                </div>  
                                    </div>
                                    
                             
              
            	
                <div class="clrBth"></div>
           
            
            <footer>Copyright &copy; 2010 Get A Cab</footer>
 
</body>
</html>
