<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.TransactionSummaryBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<html>
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
 <script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
 
 <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
  
<script type="text/javascript">
function loadDiv(source,tranid,target)
{

	document.getElementById(target).style.background="orange";	
	document.getElementById(source).innerHTML =  "Really Void? <a href='#' onclick=\"makeVoid('"+tranid+"')\">Yes</a> / <a href='#' onclick=makeRedo('"+source+"','"+tranid+"','"+target+"')>No</a> ";
}
function makeRedo(source,tranid,target)
{
	document.getElementById(source).innerHTML = "<input type='button' value='Void' onclick=\"loadDiv('"+source+"','"+tranid+"','"+target+"') \">";
	document.getElementById(target).style.background="none";
}
function makeVoid(transid)
{
	 //var flg = confirm("Do you really want to void this transaction"); 
	// if(flg)
	 	window.location.href = "/TDS/control?action=manualccp&event=voidPayment&transid="+transid;
}

function makeCapture(transid)
{
	window.location.href = "/TDS/control?action=manualccp&event=capturePayment&transid="+transid;	
}

function makeAlter(total,transid)
{
	 
	 var flg = confirm("Do you really want to alter this transaction"); 
	 if(flg)
	 	window.location.href = "/TDS/control?action=manualccp&event=alterPayment&transid="+transid+"&amount="+total;
}
</script>
</head>
<body>
<form method="post" action="control" name="masterForm">
<%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
%>

<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.getMCCProcess %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="getUntxnSummary">
<table>
<tr>
<td>
<table id="pagebox">
	<tr> 
		<td>
			<table id="bodypage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Transaction&nbsp;Summary</b></td>
			</tr>
			<tr>
			
				
				<td>Driver ID</td>
				<td>&nbsp;</td>
				 
				<td>				 
				<input type="text"  autocomplete="off" class="form-autocomplete" onblur="caldid(id,'dirname','dName')" size="6" name="driver" id="driver"  value=<%=request.getParameter("driver")==null?"":request.getParameter("driver")%>  >&nbsp;&nbsp;&nbsp;
				<ajax:autocomplete
		  					fieldId="driver"
		  					popupId="model-popup1"
		  					targetId="driver"
		  					baseUrl="autocomplete.view"
		  					paramName="DRIVERID"
		  					className="autocomplete"
		  					progressStyle="throbbing" />
		  		<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
				<div id="dirname"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
				</td>
				
				<%--
				<td>
					<select name="driver">
						<option value="">Select</option>
						<% for(int j=0 ;j<al_driver.size();j=j+2){  
							<option value="<%=al_driver.get(j).toString() %>" <%=(al_driver.get(j).toString().equalsIgnoreCase(request.getParameter("driver")==null?"":request.getParameter("driver")))?"selected":"" %> ><%=al_driver.get(j+1) %></option>
						<%} %>
					</select>	
				</td>
				--%>
				<td>&nbsp;</td> 
				<td>Amount Greater Than</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  name="amount" value="<%= request.getParameter("amount")==null?"":request.getParameter("amount")%>">
				</td>
			</tr>
	 
			<tr>
				<td>From Date</td>
				<td>&nbsp;</td>
				<td>
				 	<input type="text" name="from_date" size="10" value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly" onfocus="showCalendarControl(this);">
					
				</td>	
				<td>&nbsp;</td>
				<td>To Date</td>
				<td>&nbsp;</td>
				<td>
				 	<input type="text" name="to_date" size="10" value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly" onfocus="showCalendarControl(this);">
					
				</td>
			 </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="7"><input type="submit" name="Button" value="Search"></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="pagebox">
	 
	<%if(request.getAttribute("al_list")!=null){ 
		ArrayList al_list=(ArrayList)request.getAttribute("al_list");
	%>
	<tr>
		<td>
			<table id="bodypage" border="1" align="center" cellpadding="0" cellspacing="0" width="600">
				<tr align="center">
					<td>S.No</td>
					<TD>Driver</TD>
					<td>Card&nbsp;No</td>
					<td>Amount</td>
					<td>Tip Amount</td>
					<td>Total Amount</td>
					<td>Process&nbsp;Date</td>
					<td></td>
					<td></td>
				</tr>
				<%for(int count=0;count<al_list.size();count++){
					TransactionSummaryBean summaryBean = (TransactionSummaryBean)al_list.get(count); %>
				<tr align="center" id="trids<%=count %>">
					<td><%=count+1%></td>
					<td><%=summaryBean.getT_driver() %></td>
					<td><%=summaryBean.getT_card_no() %></td>
					<td align="right"><%=summaryBean.getT_amout() %></td>
					<td align="right"><%=summaryBean.getT_tip() %></td>
					<td align="right"><%=summaryBean.getT_total()%></td>
					<td><%=summaryBean.getT_processDate()%></td>
					<td>
					<%if(!summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg()){ %>
					<div id="id<%=count %>"> 
						<input type="button" value="Void" onclick="loadDiv('id<%=count %>','<%=summaryBean.getT_trans_id() %>','trids<%=count %>')">
					</div>	
					<%} else { %>
						<%=summaryBean.isT_voidflg()?"Txn Voided on " +summaryBean.getT_voiddate():summaryBean.isT_voidedflg()?"Genarate From Voiding":"" %>
					<%} %>
						<%--<input type="button" value="Void" onclick="makeVoid('<%=summaryBean.getT_trans_id() %>')"> 
						<input type="button" value="Alter" onclick="makeAlter('<%=summaryBean.getT_total()%>','<%=summaryBean.getT_trans_id() %>')"> --%>
					</td>
					<td>
						<%if(!summaryBean.getT_authcapflg().equals("C")) {%>
							<input type="button" value="Capture" onclick="makeCapture('<%=summaryBean.getT_trans_id() %>')">
						<%} %>
				</tr>
				<%} %>
			</table>
			</td>			 
		</tr>
	<%} %>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>