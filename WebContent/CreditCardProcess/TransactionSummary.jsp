<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.TransactionSummaryBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.FleetBO" %>


<%@page import="com.charges.constant.ICCConstant"%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%! int startValue =1 ;double limit=0;int pstart=1;int pend=0;int pageNo=1;int plength=0;%>
<%if(request.getAttribute("startValue") != null){
	startValue = Integer.parseInt((String)request.getAttribute("startValue"));
	startValue=startValue+1;
}	
if(request.getAttribute("plength")!=null){
	plength=Integer.parseInt((String)request.getAttribute("plength"));
	}
%>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		fleetList=(ArrayList)session.getAttribute("fleetList");
	}else{
		fleetList=null;
	} %>


<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
 <script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
 
 <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Insert title here</title>
  
<script type="text/javascript">
function makeCapture(transid)
{
	$("#addTipForApprove").jqm();
	$("#addTipForApprove").jqmShow();
	document.getElementById("transTemp").value=transid;
}
function submitNewSettle(){
	window.location.href = "/TDS/control?action=manualccp&event=capturePayment&module=financeView&tip_amount="+document.getElementById("tipForApprove").value+"&transid="+document.getElementById("transTemp").value;	
	$("#addTipForApprove").jqmHide();
}

function loadDiv(source,tranid,target,approvalcode)
{
	
	document.getElementById(target).style.background="orange";
	document.getElementById(source).innerHTML =  "Really Return? <a href='#' onclick=\"makeVoid('"+tranid+"','"+approvalcode+"')\">Yes</a> / <a href='#' onclick=makeRedo('"+source+"','"+tranid+"','"+target+"','"+approvalcode+"')>No</a> ";
}
function makeRedo(source,tranid,target,approvalcode)
{
	document.getElementById(source).innerHTML = "<input type='button' value='Return' onclick=\"loadDiv('"+source+"','"+tranid+"','"+target+"','"+approvalcode+"') \">";
	document.getElementById(target).style.background="none";
	
}
function makeVoid(transid,approvalcode)
{
	 //var flg = confirm("Do you really want to void this transaction"); 
	// if(flg)
	 	window.location.href = "/TDS/control?action=manualccp&event=voidPayment&module=financeView&transid="+transid+"&approvalcode="+approvalcode;
}
function makeAlter(total,transid)
{
	 
	 var flg = confirm("Do you really want to alter this transaction"); 
	 if(flg)
	 	window.location.href = "/TDS/control?action=manualccp&event=alterPayment&module=financeView&transid="+transid+"&amount="+total;
}
function caldatevalidation(){
	var fromdate=document.getElementById("from_date").value;
	var todate=document.getElementById("to_date").value
	fromdate = fromdate.substring(6,10)+fromdate.substring(0, 2)+fromdate.substring(3, 5);
	todate = todate.substring(6,10)+todate.substring(0, 2)+todate.substring(3, 5);
		if(fromdate>todate){
		alert("Check Fromdate And Todate");
       	  return false;
		}else {
			return true;
		}
}
</script>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var transactionsummary = document.getElementById("transactionsummary");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>Transaction Summary  Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(transactionsummary.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
</head>

<body>
<input type="hidden" name="finanical" value="3">
<form method="post" action="control" name="masterForm" onsubmit="return caldatevalidation();">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"financeView":request.getParameter("module") %>"     />
<input type="hidden" name="fleetSize" id="fleetSize" value="<%=fleetList!=null?fleetList.size():0%>">
<%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
	String type = request.getAttribute("type")==null?"0":request.getAttribute("type").toString();
%>

<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.getMCCProcess %>">
<%if(type.equals("0")) {%>
	<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="getSummarytoAlter">
<%} else { %>
	<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="getSummarytoAlterlimit">
<%} %>

                	<h2><center>Transaction Summary</center></h2>
                          			<%
                        			String error = (String) request.getAttribute("error");
									%>
									<div id="errorpage">
									<%= (error !=null && error.length()>0)?""+error :"" %></div>
									
                    <table style="width:100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<td>
					<table id="bodypage" style="width:100%;" >
					<tr>
					<td>Driver&nbsp;Id</td>
			        <td>
                     <input type="text"  autocomplete="off" size="15"class="form-autocomplete" onblur="caldid(id,'dirname','dName')"  name="driver" id="driver"  value=<%=request.getParameter("driver")==null?"":request.getParameter("driver")%>  >
					 <ajax:autocomplete
		  					fieldId="driver"
		  					popupId="model-popup1"
		  					targetId="driver"
		  					baseUrl="autocomplete.view"
		  					paramName="DRIVERID"
		  					className="autocomplete"
		  					progressStyle="throbbing" />
		  		    <input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
				   <div id="dirname"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
				  </td>
				  <td class="firstCol">Amount&nbsp;Greater&nbsp;Than</td>
				  <td>
				  <input type="text"  name="amount" size="15" value="<%= request.getParameter("amount")==null?"":request.getParameter("amount")%>">
				  </td>
				  </tr>
				  
				  <tr>
				  <td class="firstCol">From&nbsp;Date</td>
				  <td>
				  <input type="text" name="from_date" size="15"  value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly" onfocus="showCalendarControl(this);">
				  </td>
				  <td class="firstCol">To&nbsp;Date</td>
				  <td>
				  <input type="text" name="to_date" size="15"  value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly" onfocus="showCalendarControl(this);">
				  </td>	
				  </tr>
				  
				  <tr>
				  <td class="firstCol">Approval&nbsp;Code</td>
				  <td>
				  <input type="text" name="approval_code" size="15"  value="<%=request.getParameter("approval_code")==null?"":request.getParameter("approval_code") %>">
				  </td>
				  <td class="firstCol">Card&nbsp;No (Last 4 Digit)</td>
				  <td>
				  <input type="text" name="card_no"  size="15" value="<%=request.getParameter("card_no")==null?"":request.getParameter("card_no") %>">
				  </td>
				  </tr>	  

				<%if(fleetList!=null && fleetList.size()>0){%>
				  <tr>
				  <td class="firstCol">Select&nbsp;Fleet</td>
               	  <td>
                <select name="fleetV" id="fleetV" >
                <option value="0" >All</option>
       				<%for(int i=0;i<fleetList.size();i++){ %>
       					<option value="<%=fleetList.get(i).getFleetNumber()%>" ><%=fleetList.get(i).getFleetName() %></option>
				<% }%>
				</select>
				</td>
				</tr>
       			<%}%>
				  
				  <tr>
			 		<td align="left" colspan="7">
			 			<input type="checkbox"      style="vertical-align: middle"   name="approval" <%=request.getParameter("approval")==null?"":"selected" %>/> Include Declined
			 		</td>
			 	 </tr>
			 	 
			 	 <tr>
				<td colspan="7" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="Button" value="Search" class="lft"/> 
                        	   	  <input type="button" value="Save/Print" id="btnPrint" />
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
				</td>
			</tr>    
					
                      </table>
                    </td></tr>
                    </table>
		              
                </div>    
              </div>
              <div class="rightCol padT10">
			<div class="rightColIn padT10">
			<%if(request.getAttribute("al_list")!=null){ 
				ArrayList al_list=(ArrayList)request.getAttribute("al_list");
			%>
			<table style="width: 100%;" border="1" cellspacing="0" cellpadding="0" class="driverChargTab" id="transactionsummary">     
			<tr style="background-color:rgb(212, 219, 248)">
					<th style="width: 10%">Driver Name</th>
					<th style="width: 10%">Card No</th>
					<th style="width: 10%">Approval Code</th>
					<th style="width: 10%">Trip Id</th>
					<th style="width: 7%">Amount</th>
					<th style="width: 7%">Tip</th>
					<th style="width: 10%">Total</th>
					<th style="width: 10%">Process Date</th>
					<th style="width: 10%">Approval Status</th>
					<%if(type.equals("0")) {%>
						<th style="width: 8%">Details</th>
						<th style="width: 7%"></th>
					<%} %>
					<%if(fleetList!=null && fleetList.size()>0){%>
						<th style="width: 15%">Fleet Name</th>
					<%}%>
			</tr>
			<%for(int count=0;count<al_list.size();count++){
					TransactionSummaryBean summaryBean = (TransactionSummaryBean)al_list.get(count);
					if(count%2==0){
					%>
				<tr align="center" id="trids<%=count %>">
					<td style="background-color:white"><%=summaryBean.getT_driver() %></td>
					<td style="background-color:white"><%=summaryBean.getT_card_no() %></td>
					<td style="background-color:white"><%=summaryBean.getT_approval_code() %></td>
					<td style="background-color:white"><%=summaryBean.getT_trip() %></td>
					<td style="background-color:white"><%=summaryBean.getT_amout() %></td>
					<td style="background-color:white"><%=summaryBean.getT_tip() %></td>
					<td style="background-color:white"><%=summaryBean.getT_total()%></td>
					<td style="background-color:white"><%=summaryBean.getT_processDate()%></td>
					<td style="background-color:white"><%=summaryBean.getT_approval_status().equals("1")?"Authorized":"Declined"%></td>
					<%if(type.equals("0")) {%>
						<td>
						<%if(!summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg() && summaryBean.getT_approval_status().equals("1")){ %>
						 <div id="id<%=count %>">						
                        <input type="button" value="Return" class="lft" onclick="loadDiv('id<%=count %>','<%=summaryBean.getT_trans_id() %>','trids<%=count %>','<%=summaryBean.getT_approval_code() %>')">
                        </div>		
						
						<%} else if(summaryBean.getT_approval_status().equals("1")) { %>
							<%=summaryBean.isT_voidflg()?"Txn Returned on " +summaryBean.getT_voiddate():summaryBean.isT_voidedflg()?"Genarate From Voiding":"" %>
						<%} %>
							<%--<input type="button" value="Void" onclick="makeVoid('<%=summaryBean.getT_trans_id() %>')"> 
							<input type="button" value="Alter" onclick="makeAlter('<%=summaryBean.getT_total()%>','<%=summaryBean.getT_trans_id() %>')"> --%>
						</td>
						<td>
							<%if(!summaryBean.getT_authcapflg().equals(""+ICCConstant.SETTLED) && !summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg() && summaryBean.getT_approval_status().equals("1")) {%>
								<input type="button" value="Capture" onclick="makeCapture('<%=summaryBean.getT_trans_id() %>')">
							<%} %>
						</td>
					<%} %>
					<%if(fleetList!=null && fleetList.size()>0){%>
						<td>
						<%for(int j=0;j<fleetList.size();j++){ %>
						<%if(summaryBean.getT_fleetno().equalsIgnoreCase(fleetList.get(j).getFleetNumber())){ %>
							<%=fleetList.get(j).getFleetName() %>
							<%j=fleetList.size();
						} }%>
						</td>
					<%}%>
				</tr>
				<%}else{ %>
				<tr align="center" id="trids<%=count %>">
					<td style="background-color:lightblue"><%=summaryBean.getT_driver() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_card_no() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_approval_code() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_trip() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_amout() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_tip() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_total()%></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_processDate()%></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_approval_status().equals("1")?"Approved":"Declined"%></td>
					<%if(type.equals("0")) {%>
						<td style="background-color:lightblue">
						<%if(!summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg() && summaryBean.getT_approval_status().equals("1")){ %>
						 <div id="id<%=count %>">						
                        <input type="button" value="Return" class="lft" onclick="loadDiv('id<%=count %>','<%=summaryBean.getT_trans_id() %>','trids<%=count %>','<%=summaryBean.getT_approval_code() %>')">
                        </div>		
						
						<%} else if(summaryBean.getT_approval_status().equals("1")) { %>
							<%=summaryBean.isT_voidflg()?"Txn Returned on " +summaryBean.getT_voiddate():summaryBean.isT_voidedflg()?"Genarate From Voiding":"" %>
						<%} %>
							<%--<input type="button" value="Void" onclick="makeVoid('<%=summaryBean.getT_trans_id() %>')"> 
							<input type="button" value="Alter" onclick="makeAlter('<%=summaryBean.getT_total()%>','<%=summaryBean.getT_trans_id() %>')"> --%>
						</td>
						<td style="background-color:lightblue">
							<%if(!summaryBean.getT_authcapflg().equals(""+ICCConstant.SETTLED) && !summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg() && summaryBean.getT_approval_status().equals("1")) {%>
								<input type="button" value="Capture" onclick="makeCapture('<%=summaryBean.getT_trans_id() %>')">
							<%} %>
						</td>
					<%} %>
					<%if(fleetList!=null && fleetList.size()>0){%>
						<td style="background-color:lightblue">
						<%for(int j=0;j<fleetList.size();j++){ %>
						<%if(summaryBean.getT_fleetno().equalsIgnoreCase(fleetList.get(j).getFleetNumber())){ %>
							<%=fleetList.get(j).getFleetName() %>
							<%j=fleetList.size();
						} }%>
						</td>
					<%}%>
				</tr>
				<%} }%>
			</table>  
			<table align="center">
					<tr align="center">
						<%  
							if(request.getAttribute("pstart")!=null){
								pstart= Integer.parseInt((String)request.getAttribute("pstart"));
								}
							if(request.getAttribute("pend")!=null){
								pend=Integer.parseInt((String)request.getAttribute("pend"));
								}
							if(request.getAttribute("limit")!=null){
								limit = Double.parseDouble((String)request.getAttribute("limit"));
								}
								int start=0;
								int end =0;
							if(request.getAttribute("start")!=null){
								start=Integer.parseInt(request.getAttribute("start").toString())+1;
								end=Integer.parseInt(request.getAttribute("end").toString())+1;
							}
				 			
							System.out.println("limit:"+limit);
							System.out.println("PLength :"+plength);
							 
							 if(limit<=plength)
				 				{ 	 		
			 	 					for(int i=1;i<=limit;i++){ %>
			 	 					<%if(type.equals("0")) {%>
			 	 						<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlter&module=financeView&Button=Search&subevent=access&page<%=i %>=<%=start%>"><%=i %></a></td>
			 	 					<%} else { %>
			 	 						<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlterlimit&module=financeView&Button=Search&subevent=access&page<%=i %>=<%=start%>"><%=i %></a></td>
			 	 					<%} %>	
			 	 					<%} 
			 	 				}				 
							if(limit>=plength)
								{ 
									if(pstart!=1){
									%>
										<%if(type.equals("0")) {%>
			 	 							<td><a href="/TDS/control?action=userrolerequest&event=getUserList&module=financeView&Button=Search&subevent=access&previous=<%=""+pstart%>&first=no">&lt;</a></td>
			 	 						<%} else { %>
			 	 							<td><a href="/TDS/control?action=userrolerequest&event=getUserListlimit&Button=Search&module=financeView&subevent=access&previous=<%=""+pstart%>&first=no">&lt;</a></td>
			 	 						<%} %>	
									<%}
									for(int i=pstart;i<pend;i++){%>
										<%if(type.equals("0")) {%>				
											<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlter&Button=Search&module=financeView&subevent=access&pageNo=<%=i%>&pstart=<%=""+pstart%>&pend=<%=""+pend %>&first=no"><%=i %></a></td>
										<%} else { %>
											<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlterlimit&module=financeView&Button=Search&subevent=access&pageNo=<%=i%>&pstart=<%=""+pstart%>&pend=<%=""+pend %>&first=no"><%=i %></a></td>
										<%} %>	
									<%}
									if(pend<=limit){	%>
										<%if(type.equals("0")) {%>
											<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlter&module=financeView&Button=Search&subevent=access&next=<%=pend+""%>&first=no">&gt;</a></td>
										<%} else { %>
											<td><a href="/TDS/control?action=manualccp&event=getSummarytoAlterlimit&module=financeView&Button=Search&subevent=access&next=<%=pend+""%>&first=no">&gt;</a></td>
										<%} %>	
									<%} 
								}%>
					</tr>
				</table>
				<%} %>      
                </div>    
              </div>
            	
      
           	<div class="jqmWindow" id="addTipForApprove" style="width: 50%;">
           		<table>
           		<tr>
           		<td>Tip:</td><td><input type="text" id="tipForApprove" name="tipForApprove" value="0.00"/>
           		<input type="hidden" id="transTemp" name="transTemp" value=""/>
           		</td>
           		</tr>
           		<tr>
           		<td colspan="2"><input type="button" name="Capture" value="Capture" onclick="submitNewSettle()"/></td>
           		</tr>
           		</table>
            </div>
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
