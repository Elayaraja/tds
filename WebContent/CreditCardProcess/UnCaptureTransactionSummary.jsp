<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.TransactionSummaryBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="com.charges.constant.ICCConstant"%><html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" 	language="javascript"></script>
 <script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
 
 <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
  
<script type="text/javascript">
function loadDiv(source,tranid,target,approvalcode)
{

	document.getElementById(target).style.background="orange";	
	document.getElementById(source).innerHTML =  "Really Void? <a href='#' onclick=\"makeVoid('"+tranid+"','"+approvalcode+"')\">Yes</a> / <a href='#' onclick=makeRedo('"+source+"','"+tranid+"','"+target+"','"+approvalcode+"')>No</a> ";
}
function makeRedo(source,tranid,target,approvalcode)
{
	document.getElementById(source).innerHTML = "<input type='button' value='Void' onclick=\"loadDiv('"+source+"','"+tranid+"','"+target+"','"+approvalcode+"') \">";
	document.getElementById(target).style.background="none";
}
function makeVoid(transid,approvalcode)
{
	 //var flg = confirm("Do you really want to void this transaction"); 
	// if(flg)
	 	//window.location.href = "/TDS/control?action=manualccp&event=voidPayment&module=financeView&transid="+transid;
		window.location.href = "/TDS/control?action=manualccp&event=voidPayment&module=financeView&transid="+transid+"&approvalcode="+approvalcode;
}

function makeCapture(transid)
{
	window.location.href = "/TDS/control?action=manualccp&event=capturePayment&module=financeView&transid="+transid;	
}

function makeAlter(total,transid)
{
	 
	 var flg = confirm("Do you really want to alter this transaction"); 
	 if(flg)
	 	window.location.href = "/TDS/control?action=manualccp&event=alterPayment&module=financeView&transid="+transid+"&amount="+total;
}
</script>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var uncapturetransaction = document.getElementById("uncapturetransaction");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>Uncapture TransactionSummary  Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(uncapturetransaction.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
</head>
<body>
<form method="post" action="control" name="masterForm">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
%>

<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.getMCCProcess %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="getUntxnSummary">
<input type="hidden" name="financial" value="3">
 
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<h2><center>Transaction Summary</></h2>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<td>
					<table id="bodypage" width="540" >
                     <tr> 
                     <td class="firstCol">Driver&nbsp;Id</td>
                     <td>
                     <input type="text"  autocomplete="off" class="form-autocomplete" onblur="caldid(id,'dirname','dName')"  name="driver" id="driver"  value=<%=request.getParameter("driver")==null?"":request.getParameter("driver")%>  >&nbsp;&nbsp;&nbsp;
					 <ajax:autocomplete
		  					fieldId="driver"
		  					popupId="model-popup1"
		  					targetId="driver"
		  					baseUrl="autocomplete.view"
		  					paramName="DRIVERID"
		  					className="autocomplete"
		  					progressStyle="throbbing" />
		  		    <input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
				   <div id="dirname"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
                    </td>
                    
                    <td class="firstCol">Amount&nbsp;Greater&nbsp;Than</td> 
                    <td>
                    <input type="text"  name="amount" value="<%= request.getParameter("amount")==null?"":request.getParameter("amount")%>">
                    </td> 
                    </tr>
                    
                    <tr>
                     	<td class="firstCol">From&nbsp;Date</td> 
                     	<td>
                     		<input type="text" name="from_date"  value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly" onfocus="showCalendarControl(this);">
                     	</td>
                        <td class="firstCol">To&nbsp;Date</td>
                        <td>
                        <input type="text" name="to_date"  value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly" onfocus="showCalendarControl(this);">
                        </td> 
                    </tr>
                    
                    <tr>
				<td colspan="7" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="submit" name="Button" value="Search" class="lft">
                        	  	   	  <input type="button" value="Save/Print" id="btnPrint" />
                      
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
			</td>
			</tr>  
			</table>
                    </td></tr>
                    </table>
		              
                </div>    
              </div>
              <div class="rightCol padT10">
			  <div class="rightColIn padT10">      
               <%if(request.getAttribute("al_list")!=null){ 
				ArrayList al_list=(ArrayList)request.getAttribute("al_list");
				%>   
				<table width="100%" border="1" cellspacing="0" cellpadding="0" class="driverChargTab" id="uncapturetransaction">    
                      <tr style="background-color:rgb(212, 219, 248)">
                        <th width="15%" class="firstCol">S.No</th>
                        <th width="15%">Driver</th>
                        <th width="15%">Card No</th>
                        <th width="15%">Amount</th>
                        <th width="15%">Trip&nbsp;Amount</th>
                        <th width="20%">Total&nbsp;Amount</th>
                        <th width="30%">Process&nbsp;Date</th>
                      </tr>
                      
                      <%for(int count=0;count<al_list.size();count++){
						TransactionSummaryBean summaryBean = (TransactionSummaryBean)al_list.get(count); 
						if(count%2==0){
						%>
				    <tr align="center" id='trids<%=count %>'>
				    <td style="background-color:white"><%=count+1%></td>
					<td style="background-color:white"><%=summaryBean.getT_driver() %></td>
					<td style="background-color:white"><%=summaryBean.getT_card_no() %></td>
					<td  style="background-color:white"><%=summaryBean.getT_amout() %></td>
					<td  style="background-color:white"><%=summaryBean.getT_tip() %></td>
					<td  style="background-color:white"><%=summaryBean.getT_total()%></td>
					<td style="background-color:white"><%=summaryBean.getT_processDate()%></td>
					<td style="background-color:white">
					<%if(!summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg()){ %>
					<div id="id<%=count %>"> 
						<input type="button" value="Void" onclick="loadDiv('id<%=count %>','<%=summaryBean.getT_trans_id() %>','trids<%=count %>','<%=summaryBean.getT_approval_code() %>')">
					</div>	
					<%} else { %>
						<%=summaryBean.isT_voidflg()?"Txn Voided on " +summaryBean.getT_voiddate():summaryBean.isT_voidedflg()?"Genarate From Voiding":"" %>
					<%} %>
						<%--<input type="button" value="Void" onclick="makeVoid('<%=summaryBean.getT_trans_id() %>')"> 
						<input type="button" value="Alter" onclick="makeAlter('<%=summaryBean.getT_total()%>','<%=summaryBean.getT_trans_id() %>')"> --%>
					</td>
					<td style="background-color:white">
						<%if(!summaryBean.getT_authcapflg().equals(ICCConstant.SETTLED)) {%>
							<input type="button" value="Capture" onclick="makeCapture('<%=summaryBean.getT_trans_id() %>')">
						<%} %>
				</tr>
				<%}else{ %>
				<tr align="center">
				    <td style="background-color:lightblue"><%=count+1%></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_driver() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_card_no() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_amout() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_tip() %></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_total()%></td>
					<td style="background-color:lightblue"><%=summaryBean.getT_processDate()%></td>
					<td style="background-color:lightblue">
					<%if(!summaryBean.isT_voidflg() && !summaryBean.isT_voidedflg()){ %>
					<div id="id<%=count %>"> 
						<input type="button" value="Void" onclick="loadDiv('id<%=count %>','<%=summaryBean.getT_trans_id() %>','trids<%=count %>','<%=summaryBean.getT_approval_code() %>')">
					</div>	
					<%} else { %>
						<%=summaryBean.isT_voidflg()?"Txn Voided on " +summaryBean.getT_voiddate():summaryBean.isT_voidedflg()?"Genarate From Voiding":"" %>
					<%} %>
						<%--<input type="button" value="Void" onclick="makeVoid('<%=summaryBean.getT_trans_id() %>')"> 
						<input type="button" value="Alter" onclick="makeAlter('<%=summaryBean.getT_total()%>','<%=summaryBean.getT_trans_id() %>')"> --%>
					</td>
					<td style="background-color:lightblue">
						<%if(!summaryBean.getT_authcapflg().equals("C")) {%>
							<input type="button" value="Capture" onclick="makeCapture('<%=summaryBean.getT_trans_id() %>')">
						<%} %>
						</td>
				</tr>
				<%} }%>
				</table>
             <%} %>         
                                  
                </div>    
              </div>
            	
                <div class="clrBth"></div>
           
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
