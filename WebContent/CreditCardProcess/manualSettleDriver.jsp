<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.ManualSettleBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">

function selAll()
{
	if(document.getElementById('all').checked)
	{
		document.getElementById('CreditCard').checked = true;
		document.getElementById('Penalty').checked = true;
		document.getElementById('Voucher').checked = true;
		
	} else {
		document.getElementById('CreditCard').checked = false;
		document.getElementById('Penalty').checked = false;
		document.getElementById('Voucher').checked = false;
		
	}
}

function chckAll(size)
{

  if(document.getElementById('chck_all').checked)
  {	
	for(var i=0;i<Number(size);i++)
	{
  	 document.getElementById('Settle'+i).checked = true;
 	}
  } else {
	  for(var i=0;i<Number(size);i++)
		{
	  	  document.getElementById('Settle'+i).checked = false;
	 	}
  }
}

</script>

<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manual Settle</title>
</head>

<body>
<form method="post" action="control" name="masterForm" onsubmit="return caldatevalidation();">
<input  type="hidden"   name="module" id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="manualccp">
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="manualSettle">
<input type="hidden" name="financial" value="3">
 
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<h2><center>Manual Settlement</center></h2>
                	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                	<tr>
                	<td>
                	<table border="2" width="540"  align="center">
                	
                	<tr>
                	<td class="firstCol" >Driver&nbsp;Id</td>
                	<td colspan="6">
               		<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)"  autocomplete="off" class="form-autocomplete" onblur="caldid(id,'dirname','dName')" size="12" name="driver" id="driver"  value=<%=request.getParameter("driver")==null?"":request.getParameter("driver")%>  >
					<ajax:autocomplete
		  					fieldId="driver"
		  					popupId="model-popup1"
		  					targetId="driver"
		  					baseUrl="autocomplete.view"
		  					paramName="DRIVERID"
		  					className="autocomplete"
		  					progressStyle="throbbing" />
		  		  <input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
				  <div id="dirname"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
                 </td>
                 </tr>
                 
                 <tr>
                 <td><input type="checkbox" name="CreditCard"       style="vertical-align: middle"     id="CreditCard" <%=request.getParameter("CreditCard")==null?"":"selected" %>/> Include CreditCard</td>
                 <td>
                 <input type="checkbox" name="Penalty"       style="vertical-align: middle"   id="Penalty"  <%=request.getParameter("Penalty")==null?"":"selected" %>/> Include Misc Charges
                 </td>
                 <td>
                 <input type="checkbox" name="Voucher"   style="vertical-align: middle"   id="Voucher" <%=request.getParameter("Voucher")==null?"":"selected" %>/> Include Vouchers
                 </td>
                 <td>
                 <input type="checkbox" name="all"   style="vertical-align: middle"  id="all" <%=request.getParameter("all")==null?"":"selected" %> onclick="selAll()"/> Select All
                 </td>
                 <td>
                 <div class="wid70 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        <input type="submit" name="GetDetailsButton" value="Retrieve" class="lft">
                        	</div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                </div>
                </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
               </div>
               </div>
               
               <div class="rightCol padT10">
               <div class="rightColIn padT10">
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
               <%if(session.getAttribute("manualSettle")!=null){ 
				ArrayList al_list=(ArrayList)session.getAttribute("manualSettle");
			   %>
			   
			   <tr>
			   <th class="firstCol" width="15">S.No</th>
			   <th>Description</th>
			   <th>Date</th>
			   <th>Amount</th>
			   <th>Settle
			   <input type="checkbox" name="chck_all" id="chck_all" onclick="chckAll(<%=al_list.size() %>)">
			   </th>
               </tr>
               
               <%for(int count=0;count<al_list.size();count++){
					ManualSettleBean settleBean = (ManualSettleBean)al_list.get(count); 
					if(count%2==0){%>
					
					<tr align="center" id="trids<%=count %>">
					<td><%=count+1%></td>
					<td><%=settleBean.getDescription() %></td>
					<td><%=settleBean.getDate() %></td>
					<td><%=settleBean.getamt() %></td>
					<td><input type="checkbox" name="<%="Settle" + count%>"  id="<%="Settle" + count%>"></td>
					</tr>
					<%}else{ %>
					<tr align="center" id="trids<%=count %>">
					<td style="Background-color:lightgreen"><%=count+1%></td>
					<td style="Background-color:lightgreen"><%=settleBean.getDescription() %></td>
					<td style="Background-color:lightgreen"><%=settleBean.getDate() %></td>
					<td style="Background-color:lightgreen"><%=settleBean.getamt() %></td>
					<td style="Background-color:lightgreen"><input type="checkbox" name="<%="Settle" + count%>"  id="<%="Settle" + count%>"></td>
					</tr>
					
				<%} }%>  
				<tr align="center">
					<td colspan="5" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        <input type="submit" name="ProcessButton" value="Settle" class="lft">
                        	</div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>
					</td>
				</tr>
				<%} %>
				</table>
				
                             
                    
                </div>    
              </div>
            	
            <footer>Copyright &copy; 2010 Get A Cab</footer>
</body>
</html>
