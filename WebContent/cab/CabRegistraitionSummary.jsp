<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.CabRegistrationBean"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
        <%@page import="com.tds.tdsBO.FleetBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
/* function checkCab(){
	alert("i am coming cab mumber is "+document.getElementById("cab_cabno").value);
	var cabNum=document.getElementById("cab_cabno").value;
	  var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='RegistrationAjax?event=cabNumVerify&module=operationView&cabNum='+cabNum;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="Yes"){
		
	}else{
		alert("I am here");
		cabNum=="";
		document.getElementById("cab_cabno").focus();
	}
} */

function changeFleetForCab(cabNo,row){
	var r=confirm("Do you want to Change the fleet");
	if(r==true){
	 var fleet=document.getElementById("fleetCab_"+row).value;
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		<%	AdminRegistrationBO adminBO=(AdminRegistrationBO)session.getAttribute("user");%>
	url='SystemSetupAjax?event=changeCabFleet&cabNumber='+cabNo+'&fleet='+fleet;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text==1){
		alert("changed sucessfully");
	}else{
		alert("Couldn't change");
	}
}
}
function showFleet(row){
	$("#fleetForCab_"+row).show('slow');
}
function deleteCab(cabNumber,x){
	  var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='RegistrationAjax?event=deleteCab&module=operationView&cabNum='+cabNumber;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="1"){
		var table=document.getElementById("cabSummary");
		table.deleteRow(x.parentNode.parentNode.rowIndex);
	}
} 


function getCabDetails(){
    $("#download").live("click", function () {
        var driverSummary = document.getElementById("driverSummary");
        var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
        printWindow.document.write('<html><head><title>Cab Details</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(driverSummary.outerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    });

}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></meta>
	<title>Insert title here</title>
</head>
<body>
<%
	ArrayList al_summ = (ArrayList)request.getAttribute("al_summary");
%>
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
%>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	<%if(session.getAttribute("fleetList")!=null) {
	fleetList=(ArrayList)session.getAttribute("fleetList");
	} %>
	<form name="masterForm" action="control" method="post">
		<input type="hidden" name="action" value="registration"> <input
			type="hidden" name="event" value="cabRegistrationSummary"> <input
				type="hidden" name="systemsetup" value="6"> <input
					type="hidden" name="module" id="module"
					value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
							<c class="nav-header"><center>Cab Registration Summary</center></c>

					<div>
						<div>
							<table width="100%" border="0" cellspacing="0" cellpadding="0"
								>
								<tr>
									<td>
										<table id="bodypage" width="100%" align="center">
											

											<input type="hidden" name="make" tabindex="2"
													id="cab_cabmake" size="8"
													value="<%=request.getParameter("cab_cabmake")==null?"":request.getParameter("cab_cabmake") %>"></input>
												<input type="hidden" name="year" id="cab_cabyear"
													size="8" tabindex="3"
													value="<%=request.getParameter("cab_cabyear")==null?"":request.getParameter("cab_cabyear") %>"></input>
												
												</tr><tr>
												<td >Cab No</td>
												<td><input type="text" name="cab_cabno" id="cab_cabno" autocomplete="off"
													size="8" tabindex="1"
													value="<%=request.getParameter("cab_cabno")==null?"":request.getParameter("cab_cabno") %>">
														<ajax:autocomplete fieldId="cab_cabno"
															popupId="model-popup1" targetId="cab_cabno"
															baseUrl="autocomplete.view" paramName="cabNum"
															className="autocomplete" progressStyle="throbbing" />
												</input></td>
												<td >Registration</td>
												<td><input type="text" name="cab_rcno" tabindex="4"
													id="cab_rcno" size="8"
													value="<%=request.getParameter("cab_rcno")==null?"":request.getParameter("cab_rcno")%>">
												</input></td>
												</tr>
										<tr>
												<td >Cab Year</td>
												<td><input type="text" name="cab_cabyear"
													id="cab_cabyear" size="8" tabindex="3"
													value="<%=request.getParameter("cab_cabyear")==null?"":request.getParameter("cab_cabyear") %>">
												</input></td>
					<td >Status</td>
					<td>
					<select name="status">
							<option value="2">All</option>
                 		 	<option value="1">Active</option>
                 		 	<option value="0">InActive</option>
                 		 	</select>
					</td>
					</tr>
				<%if( fleetList!=null && fleetList.size()>0) {%>
					<tr><td>Company Name</td>
					<td> <select name="fleetSearch" id="fleetSearch" >
                      <%if(fleetList!=null){
					for(int j=0;j<fleetList.size();j++){ %>
						<option value="<%=fleetList.get(j).getFleetNumber()%>" <%=(fleetList.get(j).getFleetNumber().equals(adminBo.getAssociateCode())?"selected=selected":"")%> ><%=fleetList.get(j).getFleetName() %></option>
					<%=fleetList.get(j).getFleetNumber()%>
					<% }}%> 
                       </select></td></tr>
				<%} else {%>
					<input type="hidden" name="fleetSearch" value="<%=adminBo.getAssociateCode()%>"></input>
				<%}%>
											<tr>
											</tr>
											<tr>
												<td colspan="8" align="center">
												<input type="submit" name="Button" value="Search"/>
												<input  type="button" name="download" id ="download" value="Save/Print"  onclick="getCabDetails()" disable /></td>
			
												</td>
											</tr>
										</table></td>
								</tr>
							</table>
						</div>
					</div>
					<div>
						<div>
							<%if(al_summ!=null && al_summ.size()>0) {%>
							<table width="80%" border="1" height="auto" id="driverSummary" class="table-striped" align="center">
								<tr style="background-color: rgb(255, 102, 255)">
									<th width="16%">Cab No</th>
									<th width="16%">Cab Make</th>
									<th width="17%">Cab Year</th>
									<th width="17%">Registration</th>
									<th width="17%">Model</th>
									<th width="17%">VIN No</th>
									<th width="17%">Status</th>
								</tr>
								<% 
								boolean colorLightGreen = true;
								String colorPattern;
								%>
								<%for(int i=0;i<al_summ.size();i++){
								CabRegistrationBean registrationBean = (CabRegistrationBean)al_summ.get(i);
								colorLightGreen = !colorLightGreen;
								if(colorLightGreen){
									colorPattern="style=\"background-color:white\""; 
									}
								else{
									colorPattern="style=\"background-color:rgb(102, 255, 204)\"";
								}
								%>
								<tr align="center">
									<td <%=colorPattern%>><a
										href="/TDS/control?action=registration&module=systemsetupView&event=cabRegistration&Button=Edit&key=<%=registrationBean.getCab_cabkey() %>"><%=registrationBean.getCab_cabno() %>
									</td>
									<td <%=colorPattern%>><%=registrationBean.getCab_cabmake() %></td>
									<td <%=colorPattern%>><%=registrationBean.getCab_cabYear() %></td>
									<td <%=colorPattern%>><%=registrationBean.getCab_rcno() %></td>
									<td <%=colorPattern%>><%=registrationBean.getCab_model() %></td>
									<td <%=colorPattern%>><%=registrationBean.getCab_vinno() %></td>
									<td <%=colorPattern%>><%=registrationBean.getStatus().equals("1")?"Active":"Inactive"%></td>
									<%if( fleetList!=null && fleetList.size()>0) {%>
										<td> <select name="fleetCab_<%=i %>" id="fleetCab_<%=i %>" onchange="changeFleetForCab(<%=registrationBean.getCab_cabkey() %>,<%=i %>)" >
				                       <%if(fleetList!=null){
										for(int j=0;j<fleetList.size();j++){ %>
											<option value="<%=fleetList.get(j).getFleetNumber()%>" <%=(fleetList.get(j).getFleetNumber().equals(registrationBean.getCab_assocode())?"selected=selected":"")%> ><%=fleetList.get(j).getFleetName() %></option>
										<%=fleetList.get(j).getFleetNumber()%>
										<% }}%> 
				                        </select></td>
									<%} %>
									<td <%=colorPattern%>><input type="button"
										name="imgDeletecab" id="imgDeleteCab" value="Delete"
										onclick="deleteCab(<%=registrationBean.getCab_cabkey() %>,this)"></input>
									</td>
								</tr>
								<%} %>
							</table>
							<%} else if( request.getParameter("Button")!=null){ %>
							<table>
								<tr>
									<td><font color='red'>No Records Found</font>
									</td>
								</tr>
							</table>
							<%} %>
						</div>
					</div>
	<footer>Copyright &copy; 2010 Get A Cab</footer>
	</form>	<div id="Jobs" style="display:visible;position:absolute;width: 52%;background-color: rgb(219, 173, 173);">
			 		<table  class="footable"   id="jobHistory1" > 
		       		</table>
       				</div>
</body>
</html>
