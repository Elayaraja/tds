<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.cmp.bean.CabRegistrationBean"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form name="masterForm" action="control" method="post" onsubmit="return waitcursor()">
<input type="hidden" name="action" value="registration">
<input type="hidden" name="event" value="cabRegistration">
  <%
	 CabRegistrationBean registrationBean = (CabRegistrationBean)request.getAttribute("registrationBean");
  	String status = request.getAttribute("status").toString();
  
  %>
 <table id="pagebox">
 <tr>
 <td>
	<table > 
		<tr>
			<td>
				<table id="bodypage">	
					<tr>
						<td colspan="7" align="center">		
							<div id="title">
								<h2>Cab Registration </h2>
							</div>
						</td>		
					</tr>
					<%
										
						StringBuffer error = (StringBuffer) request.getAttribute("error");
						
						
					%>
					<tr>
						<td colspan="7">
							<div id="errorpage">
								<%= (error !=null && error.length()>0)?""+error.toString() :"" %>
							</div>
							<div id="msgid">
							   	<font color=<%=request.getParameter("msgtype")!=null && request.getParameter("msgtype").equals("1")?"green":"red" %>>
									<%= request.getParameter("pageMsg")==null?"":request.getParameter("pageMsg") %>
								</font>
							</div>
						</td>
					</tr>
					<tr>
					<td valign="top">
							<table border=1>	
								<tr>
									<td>Cab No</td>
									<td>
										<input type="text" name="cab_cabno" id="cab_cabno" size="8" tabindex="1"  value="<%=registrationBean.getCab_cabno()%>" >
										<input type="hidden" name="cab_cabkey" id="cab_cabkey" value="<%=registrationBean.getCab_cabkey() %>">
									</td>
								 	<td>Cab Make</td>
									<td><input type="text" name="cab_cabmake"  tabindex="2" id="cab_cabmake" size="8" value="<%=registrationBean.getCab_cabmake() %>"></td>
								</tr>
								<tr>
									<td>Cab Year</td>
									<td><input type="text" name="cab_cabyear" id="cab_cabyear" size="8" tabindex="3" value="<%=registrationBean.getCab_cabYear() %>"></td>
								 	<td>Certification of Registration:</td>
								 	<td>
								 		<input type="text" name="cab_rcno" tabindex="4" id="cab_rcno" value="<%=registrationBean.getCab_rcno() %>" size="8">
								 	</td>
							 	</tr>
								</table>
							</td>
				 	</tr>
				</table>
				</td>
				</tr>
				<tr>
					<TD colspan="2" align="center">
						<%if(status.equals("Insert")){ %>
							<input type="submit" name="Button" id="Button" tabindex="6"  value="Submit"  >&nbsp;
						<%} else { %>
							<input type="submit" name="Button" id="Button" tabindex="6"  value="Update"  >&nbsp;
						<%} %>
					</TD>
				</tr>					
			</table>
		</td>
	</tr>
	
</table>
</form>				
</body>
</html>