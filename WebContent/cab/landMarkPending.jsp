<%@page import="com.tds.tdsBO.LandMarkBO"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
#errorpage{
font-size:bold;
color:lightblue;
}
h2{
font-weight:bold; 
font-style:italic;
color:grey;
}
</style>
<script type="text/javascript">
function addToLandmark(SerialNo){
	window.open("control?action=registration&event=landmark&module=operationView&approve=yes&nxtScreen=landPending&sNo="+SerialNo,"_self");
}
function addToSelect(SerialNo){
	document.getElementById("serials").value=document.getElementById("serials").value+SerialNo+";";
}
function deleteLandmarkPending(serialNo, x){
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='RegistrationAjax?event=deleteLandmarkPending&module=operationView&delete=YES&mass=true&SerialNumber='+serialNo+';';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="Deleted Successfully"){
	var table = document.getElementById("bodypage");
	 table.deleteRow(x.parentNode.parentNode.rowIndex);
	}
}
function makeAction(type){
	var xmlhttp=null;
	var url;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	} if(type==1){
		url='RegistrationAjax?event=deleteLandmarkPending&module=operationView&delete=YES&mass=true&SerialNumber='+document.getElementById("serials").value;
	} else {
		url='control?action=registration&event=landmark&module=operationView&approveMass=yes&nxtScreen=landPending&sNo='+document.getElementById("serials").value;
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	document.getElementById("bodypage").innerHTML="";
}

</script>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var landmarkpending = document.getElementById("landmarkpending");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>Landmark Pending Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(landmarkpending.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>  
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
 <script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>  
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS(Taxi Dispatch System)</title>
</head>

<%ArrayList<OpenRequestBO> ar_list =(ArrayList<OpenRequestBO>)request.getAttribute("ar_list");
%>
<body>
<form name="landSearch"  method="post" action="control" >

<input type="hidden" name="action" value="systemsetup"/>
		<input type="hidden" name="event" value="landMarkPending"/>
		<input type="hidden" name"operation" value="2"/>
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />

	<div > 
                    <div></div>
                    
                </div> 
                <div>
<div >	
<h2><center>Approve Landmark</center></h2>

               	   
                	<table width="100%" border="0" cellspacing="2" cellpadding="0" >
                		<tr>
                			<td>LandMark:</td> 
                    	<td> <input type="text" name="landmark"   size="10"  id="landmark"  value=""    /> 
							 </td>
							 <td>Phone:</td> 
							 <td><input type="text" name="phone" size="10" id="phone" value="" /></td>

                        <td colspan="2" align="center">
			            <div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input  type="submit" name="button" value="Search" /> 
                        	  	  <input type="button" value="Save/Print" id="btnPrint" /></div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>
                    </td>  
                    </tr>
                		
                	</table> 
<table   width="100%" border="1" cellspacing="2" cellpadding="0" id="landmarkpending">
<tr>
	<td>
	<table id="bodypage" width="640" border="1">
	<%if(ar_list!=null&&ar_list.size()>0){%>
	<tr>
		<td style="background-color:lightgrey"><b>LANDMARK</b></td>
		<td style="background-color:lightgrey"> PHONE NUMBER</td>
		<td style="background-color:lightgrey">ADD1</td>
		<td style="background-color:lightgrey">CITY</td>
		<td style="background-color:lightgrey">STATE</td>
		<td style="background-color:lightgrey">ZIP</td>
		<td style="background-color:lightgrey">Approve</td>
	</tr>
	<%
	boolean colorlightblue=true;
	String color;%>
	<%for(int i=0;i<ar_list.size();i++) { 
		colorlightblue=!colorlightblue;
		if(colorlightblue){
			color="style=\"background-color:lightblue\"";
		}
		else
			color="style=\"background-color:white\"";
	%>
	<tr align="center">
		<td <%=color%> ><%=ar_list.get(i).getSlandmark() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getPhone() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getSadd1() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getScity() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getSstate() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getSzip() %>  </td> 
		<input type="hidden"name="sNo"id="sno" value="<%=ar_list.get(i).getSerialNo() %>"/>
		<input type="hidden"name="serials"id="serials" value=""/>
		<input type="hidden" name="row" id="row" value=<%=i %>></input>
		<td <%=color %>><input type="checkbox" name="massApprove" id="massApprove" onclick="addToSelect('<%=ar_list.get(i).getSerialNo() %>')" value="massSelect"/></td>
		<td <%=color %>><input type="button" name="add" id="add" onclick="addToLandmark('<%=ar_list.get(i).getSerialNo() %>')" value="Approve"/></td>
		<td <%=color %>><input type="button" name="delete" id="delete" onclick="deleteLandmarkPending('<%=ar_list.get(i).getSerialNo() %>',this)" value="Delete"/></td>
	
	</tr>
	<%} %>
	<tr>
	<td>
	<input type="button" value="Approve" onclick="makeAction(2)"/></td>
	<td>
	<input type="button" value="Delete" onclick="makeAction(1)"/>
	</td></tr>
</table> 

	<%}else if(request.getParameter("button")!=null){%>
	<td><div id="errorpage" align="center">No Records Found </div></td>
	<%} %>
	</table> 
	</td>
</tr>
</table>
</div>
</div> 
</form> 
</body>
</html>