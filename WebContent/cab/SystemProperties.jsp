<%@page import="java.io.FileNotFoundException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import=" java.util.Timer"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.CompanySystemProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<script type="text/javascript" src="Ajax/SystemUtilAjax.js"></script> 
<script type="text/javascript" src="js/jquery.js"></script>


<script type="text/javascript">
function showORFormat(){
	$('.ORFormatShow').show('slow');
}
</script> 

<%
CompanySystemProperties  cspBO = (CompanySystemProperties)request.getAttribute("cspBO");   
%>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript">
function checkPhoneValidation(){
	if(document.getElementById("dontCheckPh").checked){
		document.getElementById("noOfDigitsPh").value ="";
		document.getElementById("noOfDigitsPh").readOnly=true;
	}else{
		document.getElementById("noOfDigitsPh").readOnly=false;
	}
}
</script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Company System Properties</title>
</head>
<body>
	<form name="masterForm" action="control" method="post" />

	<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %>
	<input type="hidden" name="action" value="systemsetup" />
	<input type="hidden" name="event" value="systemProperties" />
	<input type="hidden" name="module" id="module"
		value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
	<input type="hidden" name="key"
		value="  <%=request.getAttribute("key") %>" />

	<div class="rightCol" style="width: 100%; margin-left: -8px;">
		<div class="rightColIn" style="width: 100%; margin-left: -1px;">
			<h1>Company System Properties</h1>

			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="driverChargTab">
				<tr>
					<td>
						<table id="bodypage" width="750">
							<tr>
								<td class="firstCol">Pre line 1</td>
								<td><input type="text" name="Preline1" id="Preline1"
									value="<%=cspBO.getPreline1() %>" /></td>
								<td class="firstCol">Post line 1</td>
								<td><input type="text" name="Postline1" id="Postline1"
									value="<%=cspBO.getPostline1() %>" /></td>
							</tr>
							<tr>
								<td class="firstCol">Pre line 2</td>
								<td><input type="text" name="Preline2" id="Preline2"
									value="<%=cspBO.getPreline2() %>" /></td>
								<td class="firstCol">Post line 2</td>
								<td><input type="text" name="Postline2" id="Postline2"
									value="<%=cspBO.getPostline2() %>" /></td>
							</tr>
							<tr>
								<td class="firstCol">Pre line 3</td>
								<td><input type="text" name="Preline3" id="Preline3"
									value="<%=cspBO.getPreline3() %>" /></td>
								<td class="firstCol">Post line 3</td>
								<td><input type="text" name="Postline3" id="Postline3"
									value="<%=cspBO.getPostline3() %>" /></td>
							</tr>
							<tr>
								<td class="firstCol">Pre line 4</td>
								<td><input type="text" name="Preline4" id="Preline4"
									value="<%=cspBO.getPreline4() %>" /></td>
								<td class="firstCol">Post line 4</td>
								<td><input type="text" name="Postline4" id="Postline4"
									value="<%=cspBO.getPostline4() %>" /></td>
							</tr>
							<tr>
								<td class="firstCol">Pre line 5</td>
								<td><input type="text" name="Preline5" id="Preline5"
									value="<%=cspBO.getPreline5() %>" /></td>
								<td class="firstCol">Post line 5</td>
								<td><input type="text" name="Postline5" id="Postline5"
									value="<%=cspBO.getPostline5() %>" /></td>
							</tr>
							<tr>
								<td class="firstCol">Post line 6</td>
								<td><input type="text" name="Postline6" id="Preline6"
									value="<%=cspBO.getPostline6() %>" /></td>
								<td class="firstCol">Post line 7</td>
								<td><input type="text" name="Postline7" id="Postline7"
									value="<%=cspBO.getPostline7() %>" /></td>
							</tr>
							<tr>
								<td class="firstCol">TimeOut for Operator</td>
								<td><input type="text" name="TimeOutOperator" id="TimeOutO"
									value="<%=cspBO.getTimeOutOperator() %>" /></td>
								<td class="firstCol">TimeOut for Driver</td>
								<td><input type= kicked baskar.at"text" name="TimeOutDriver" id="TimeOutD"
									value="<%=cspBO.getTimeOutDriver() %>" /></td>
							</tr>
							<tr>
								<td class="firstCol">Check Cab No</td>
								<td align="center"><select name="CheckCabNo">
										<option value="Yes"
											<%=cspBO.getCheckCabNoOnLogin().equalsIgnoreCase("Yes")?"selected":"" %>>Yes</option>
										<option value="No"
											<%=cspBO.getCheckCabNoOnLogin().equalsIgnoreCase("No")?"selected":"" %>>No</option>
								</select>
								</td>
								<td class="firstCol">Enter CVV Code</td>
								<td align="center"><select name="EnterCVVcode">
										<option value="Yes"
											<%=cspBO.getCvvCodeManadatory().equals("Yes")?"selected":"" %>>Yes</option>
										<option value="No"
											<%=cspBO.getCvvCodeManadatory().equals("No")?"selected":"" %>>No</option>
								</select>
								</td>
							</tr>

							<tr>
								<td class="firstCol">Allow Credit card Return</td>
								<td align="center"><select name="AllowCreditCardreturn">
										<option value="Yes"
											<%=cspBO.getAllowCreditCardReturns().equals("Yes")?"selected":"" %>>Yes</option>
										<option value="No"
											<%=cspBO.getAllowCreditCardReturns().equals("No")?"selected":"" %>>No</option>
								</select>
								</td>
							<td class="firstCol"> Dispatch Shared Ride</td>
							<%-- <td align="center"> <select name="DSR">
									<option value="yes">
							          <%=cspBO.getDSR().equals("Yes")?"selected":"" %>Yes</option>
							          <option value="No">
							           <%=cspBO.getDSR().equals("Yes")?"selected":"" %>No</option>
							           </select>
							           </td> --%>
								<td class="firstCol">Time Zone</td>
								<td align="center"><select class="firstCol" name="TimeZone"
									id="TimeZone">
										<option value="00">Select</option>
										<option value="12"ORFormat
											<%= cspBO.getTimezone()==12 ? "selected":"0" %>>+12</option>
										<option value="11"
											<%=cspBO.getTimezone()==11 ? "selected":"0"  %>>+11</option>
										<option value="10"
											<%=cspBO.getTimezone()==10 ? "selected":"0" %>>+10</option>
										<option value="09"
											<%=cspBO.getTimezone()==9 ? "selected":"0"  %>>+09</option>
										<option value="09"
											<%=cspBO.getTimezone()==8 ? "selected":"0" %>>+08</option>
										<option value="07"
											<%=cspBO.getTimezone()==7 ? "selected":"0"  %>>+07</option>
										<option value="06"
											<%=cspBO.getTimezone()==6 ? "selected":"0" %>>+06</option>
										<option value="05"
											<%=cspBO.getTimezone()==5 ? "selected":"0" %>>+05</option>
										<option value="04"
											<%=cspBO.getTimezone()==4 ? "selected":"0" %>>+04</option>
										<option value="03"
											<%=cspBO.getTimezone()==3 ? "selected":"0" %>>+03</option>
										<option value="02"
											<%=cspBO.getTimezone()==2 ? "selected":"0" %>>+02</option>
										<option value="01"
											<%=cspBO.getTimezone()==1 ? "selected":"0" %>>+01</option>
										<option value="00"
											<%=cspBO.getTimezone()==0 ? "selected":"0"  %>>00</option>
										<option value="-01"
											<%=cspBO.getTimezone()==-1 ? "selected":"0"  %>>-01</option>
										<option value="-02"
											<%=cspBO.getTimezone()==-2 ? "selected":"0"  %>>-02</option>
										<option value="-03"
											<%=cspBO.getTimezone()==-3 ? "selected":"0"  %>>-03</option>
										<option value="-04"
											<%=cspBO.getTimezone()==-4 ? "selected":"0"  %>>-04</option>
										<option value="-05"
											<%=cspBO.getTimezone()==-5 ? "selected":"0"  %>>-05</option>
										<option value="-06"
											<%=cspBO.getTimezone()==-6 ? "selected":"0" %>>-06</option>
										<option value="-07"
											<%=cspBO.getTimezone()==-7 ? "selected":"0"%>>-07</option>
										<option value="-08"
											<%=cspBO.getTimezone()==-8 ? "selected":"0"%>>-08</option>
										<option value="-09"
											<%=cspBO.getTimezone()==-9 ? "selected":"0" %>>-09</option>
										<option value="-10"
											<%=cspBO.getTimezone()==-10 ? "selected":"0" %>>-10</option>
										<option value="-11"
											<%=cspBO.getTimezone()==-11 ? "selected":"0"%>>-11</option>
										<option value="-12"
											<%=cspBO.getTimezone()==-12 ? "selected":"0" %>>-12</option>

								</select>&nbsp; <select class="firstCol" name="TimeZoneMin"
									id="TimeZoneMin">
										<option value="00">Select</option>
										<option value="00"
											<%=cspBO.getTimezoneMin()==00 ? "selected":"0"%>>00</option>

										<option value="05"
											<%=cspBO.getTimezoneMin()==05 ? "selected":"0" %>>05</option>

										<option value="10"
											<%=cspBO.getTimezoneMin()==10 ? "selected":"0" %>>10</option>

										<option value="15"
											<%=cspBO.getTimezoneMin()==15 ? "selected":"0"%>>15</option>

										<option value="20"
											<%=cspBO.getTimezoneMin()==20 ? "selected":"0" %>>20</option>


										<option value="25"
											<%=cspBO.getTimezoneMin()==25 ? "selected":"0" %>>25</option>

										<option value="30"
											<%=cspBO.getTimezoneMin()==30 ? "selected":"0" %>>30</option>

										<option value="35"
											<%=cspBO.getTimezoneMin()==35 ? "selected":"0"%>>35</option>

										<option value="40"
											<%=cspBO.getTimezoneMin()==40 ? "selected":"0" %>>40</option>

										<option value="45"
											<%=cspBO.getTimezoneMin()==45 ? "selected":"0" %>>45</option>


										<option value="50"
											<%=cspBO.getTimezoneMin()==50 ? "selected":"0" %>>50</option>

										<option value="55"
											<%=cspBO.getTimezoneMin()==55 ? "selected":"0"%>>55</option>


								</select>
								</td>

							</tr>
							<tr>
								<td align="center" class="firstCol">Default State</td>
								<td align="center"><input type="text" name="State" size="1"
									value="<%=cspBO.getState() %>" /></td>
								</select>
								</td>
								<td align="center" class="firstCol">Default Country</td>
								<td align="center"><input type="text" name="country" size="1"
									value="<%=cspBO.getCountry() %>"></input>
								</td>
							</tr>
							<tr>
								<td class="firstCol">Provide Phone Number</td>
								<td align="center"><select name="providePhoneNum">
										<option value="1"
											<%=cspBO.getProvidePhoneNum()==1?"selected":"" %>>Yes</option>
										<option value="2"
											<%=cspBO.getProvidePhoneNum()==2?"selected":"" %>>No</option>
								</select>
								</td>
								<td class="firstCol">Caller Id</td>
								<td align="center"><select name="CallerId">
										<option value="1" <%=cspBO.getCallerId()==1?"selected":"" %>>On</option>
										<option value="2" <%=cspBO.getCallerId()==2?"selected":"" %>>Off</option>
								</select>
								</td>
							</tr>
							 <tr>
									<td class="firstCol">OR Format</td>
								<td align="center"><input type="button" name="ORFormat" id="ORFormat"
									value="ORFormat" onclick="showORFormat()"/></td>
							<td class="firstCol">Check Mandatory Documents</td>
							<td align="center"><input type="checkbox" name="checkDocuments" id="checkDocuments"
									value="1" <%=cspBO.getCheckDocuments()==1?"checked":"" %>/></td>
							</tr>
							<tr>
							<td colspan="4">
							<div class="ORFormatShow" id="ORFormatShow" style= "display:none;">
							<input type="radio" name="orFormat" value="1" <%=cspBO.getORFormat()==1?"checked":"" %> ></input>
							<img alt="" src="images/ORFormat01.png" style="height: 100px;width:165px;">
							<input type="radio" name="orFormat" value="2" <%=cspBO.getORFormat()==2?"checked":"" %> ></input>
							<img alt="" src="images/ORFormat02.png" style="height: 100px;width:165px;">
							<input type="radio" name="orFormat" value="3" <%=cspBO.getORFormat()==3?"checked":"" %> ></input>
							<img alt="" src="images/ORFormat03.png" style="height: 100px;width:165px;">
							</div>
							</td></tr>
											<tr >
											<td colspan="4" align="center">
											<font size="3" style="font-weight: bold;"> Dispatch Properties</font>
											</td></tr><tr>
								<td class="firstCol" colspan="1">Calculate Zones</td>
								<td align="center"><select name="calculateZones">
										<option value="<%=true %>"
											<%= cspBO.getCalculateZones()==true?"selected":"" %>>Yes</option>
										<option value="<%=false %>"
											<%=cspBO.getCalculateZones()==false?"selected":"" %>>No</option>
								</select>
								</td>
								<td class="firstCol">Dispatch based on</td>
								<td align="center"><select name="Dispatch">
										<option value="1"
											<%=cspBO.getDispatchBasedOnDriverOrVehicle()==1?"selected":"" %>>Based
											on Driver</option>
										<option value="2"
											<%=cspBO.getDispatchBasedOnDriverOrVehicle()==2?"selected":"" %>>Based
											on Cab No</option>
								</select>
								</td>
							</tr>
							<tr>
								<td class="firstCol">Provide End Address</td>
								<td align="center"><select name="endAddress">
										<option value="1"
											<%=cspBO.getProvideEndAddress()==1?"selected":"" %>>Yes</option>
										<option value="2"
											<%=cspBO.getProvideEndAddress()==2?"selected":"" %>>No</option>
								</select>
								</td>
							 <td class="firstCol">ORtime&nbsp;Based&nbsp;on</td>
							 <td align="center"><select class="firstCol" name="ORTime" id="ORTime">
							 	 	 <option value="textBox"<%=cspBO.getORTime().equals("textBox")?"selected":""%>>Text Box</option>
							 	 	<option value="dropDownBox" <%=cspBO.getORTime().equals("dropDownBox")?"selected":""%>>Drop Down Box</option>
								 </select>
							 </td>
							</tr>
							<tr>
								<td class="firstCol">Driver alarm in minutes</td>
								<td align="center"><input type="text" name="driverAlarm" size="5" id="driverAlarm"
									value="<%=cspBO.getDriveralarm() %>" /></td>
								<td class="firstCol">Rate Per Mile</td>
								<td align="center">$<input type="text" size="5" name="ratePerKM" id="RatePerKM"
									value="<%=cspBO.getRatePerMile() %>" /></td>
							</tr>
							<tr>
							<td class="firstCol">Minimum Speed</td>
							<td align="center"><input type="text" size="5" name="minimumSpeed" id="minimumSpeed" value="<%=cspBO.getMinimumSpeed()==null?"":cspBO.getMinimumSpeed() %>"/> MpH</td>
								<td class="firstCol">Rate per Minute</td>
							<td align="center">$<input type="text" size="5" name="ratePerMinute" id="ratePerMinute" value="<%=cspBO.getRatePerMinute() %>"/></td>
							
							</tr>
							<tr>
							<td class="firstCol">Start Amount</td>
							<td align="center">$<input type="text" size="5" name="startAmount" id="startAmount" value="<%=cspBO.getStartAmount()==null?"":cspBO.getStartAmount() %>"/></td>
							
							<td class="firstCol">No.of Digits For Ph.</td>
							<td align="center"><input type="text" size="5" name="noOfDigitsPh" id="noOfDigitsPh" value="<%=cspBO.getTotalDigitsForPh()!=null?cspBO.getTotalDigitsForPh():""%>" <%=cspBO.getTotalDigitsForPh()==null||cspBO.getTotalDigitsForPh().equalsIgnoreCase("")?"readonly=readonly":"" %> /> D.C&nbsp;<input type="checkbox"  name="dontCheckPh" onchange="checkPhoneValidation()" id="dontCheckPh" <%=cspBO.getTotalDigitsForPh()==null||cspBO.getTotalDigitsForPh().equalsIgnoreCase("")?"checked=checked":"" %> /></td>
							
							</tr>
						<tr><td  class="firstCol">Simultaneous Login</td><td align="center"><select class="firstCol" name="simultaneousLogin" id="simultaneousLogin">
							<option value="1"  <%=cspBO.getSimultaneousLogin()==1?"Selected":"" %>>Allow</option>
							<option value="0" <%=cspBO.getSimultaneousLogin()==0?"Selected":"" %>>Deny</option>
							</select></td>
						<td  class="firstCol">Currency Prefix</td><td align="center"><select class="firstCol" name="currencyPrefix" id="currencyPrefix">
							<option value="$"  <%=cspBO.getCurrencyPrefix()==1?"Selected":"" %>>$</option>
							<option value="&euro;" <%=cspBO.getCurrencyPrefix()==2?"Selected":"" %>>&euro;</option>
							<option value="&pound;" <%=cspBO.getCurrencyPrefix()==3?"Selected":"" %>>&pound;</option>
							<option value="&#x20B9;" <%=cspBO.getCurrencyPrefix()==4?"Selected":"" %>>&#x20B9;</option>
							</select></td></tr>
							<tr >
							<tr>
								<td class="firstCol">Address AutoComplete OR</td>
								<td align="center"><select name ="addressCheck" id="addressCheck">
								<option value="1" <%=cspBO.getAddressFill()==1?"Selected":"" %>>By Geocode</option>
								<option value="2" <%=cspBO.getAddressFill()==2?"Selected":"" %>>By Places</option>
								</select>
								</td>
								<td class="firstCol">Mobile Setup Password</td>
								<td align="center"><input type="text" name ="mobilePassword" id="mobilePassword" value="<%=cspBO.getMobilePassword()%>"/>
								</td>
							</tr>
							<tr>
								<td class="firstCol">See Driver in Mobile</td>
								<td align="center"><select name ="mobileDriver" id="mobileDriver">
								<option value="1" <%=cspBO.getDriverListMobile()==1?"Selected":"" %>>Individual</option>
								<option value="2" <%=cspBO.getDriverListMobile()==2?"Selected":"" %>>Summary</option>
								</select>
								</td>
								<td class="firstCol">Distance based on</td>
								<td align="center"><select name ="distanceBasedOn" id="distanceBasedOn">
								<option value="0" <%=cspBO.getDistanceBasedOn()==0?"Selected":"" %>>Miles</option>
								<option value="1" <%=cspBO.getDistanceBasedOn()==1?"Selected":"" %>>KM</option>
								</select>
								</td>
							</tr>
							<td colspan="4" align="center">
								<input name="Button" type="submit" class="lft"
								style="background-color:#82CAFF;border-radius:25px;-moz-border-radius:25px; color: blue;" value="Submit" />
							</td></tr>
							<tr style="bottom: -80%">
								<%
									AdminRegistrationBO adminBO = (AdminRegistrationBO) request
											.getSession().getAttribute("user");
									String stopAssoccode = adminBO.getAssociateCode();
									Timer stopTimer = (Timer) getServletConfig().getServletContext()
											.getAttribute(stopAssoccode + "Timer");

								if(stopTimer==null){
								%>
								<td align="center" colspan="4">
								<input name="btnStartOrStopTimer" id="btnStartOrStopTimer"
									type="button" class="lft" style="background-color:#82CAFF;border-radius:25px;-moz-border-radius:25px;color: blue;" onclick="startOrStopTimer()"
									value="Start Dispatch"/>
								<%}else{ %>
								<input name="btnStartOrStopTimer" id="btnStartOrStopTimer"
									type="button" class="lft" style="background-color:#82CAFF;border-radius:25px;-moz-border-radius:25px; color: blue;"  onclick="startOrStopTimer()"
									value="Stop Dispatch"/>
									<%} %>
								<input name="reloadZones" type="button" class="lft"
									onclick="reloadZone()" style="background-color:#82CAFF;border-radius:25px;-moz-border-radius:25px; color: blue;" value="Reload Zones" />
								<input name="reloadZones" type="button" class="lft"
									onclick="reloadCabs()" style="background-color:#82CAFF;border-radius:25px;-moz-border-radius:25px; color: blue;" value="Reload Driver/cab Properties" />
													
								</td>
							</tr>
							<tr>
							<td colspan="4" align="center">
							 <input type="image" name="StartOrStopTimer"
									id="StartOrStopTimer" style="text-decoration: blink; text-shadow: gray; color: black;font-weight: bolder;" value="" />
							<input type="image" name="reloadZones" id="reloadZones" style="text-decoration: blink; text-shadow: gray; color: black;" value="" />
							<input type="image" name="reloadCabs" id="reloadCabs" style="text-decoration: blink; text-shadow: gray; color: black;"
													value="" /></td>
							</tr>
						</table></td>
				</tr>
			</table>
		</div>
	</div>
	<div class="footerDV" style="width: 93%; margin-left: -6px;">Copyright
		&copy; 2010 Get A Cab</div>
</body>
</html>
