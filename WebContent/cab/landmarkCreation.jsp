<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.dao.RegistrationDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.dao.RequestDAO"%>
<%@page import="com.tds.controller.TDSAjaxClass "%>
<!DOCTYPE html >
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<jsp:useBean id="openRequestBean" class="com.tds.tdsBO.OpenRequestBO" />
<html xmlns="http://www.w3.org/1999/xhtml">
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO) session
			.getAttribute("user");
%>
<% String landmarkPendingSNo=(String)request.getAttribute("sNo"); %>
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link rel="stylesheet" type="text/css" href="jqueryUI/layout-default-latest.css" />
	<link rel="stylesheet" type="text/css" href="jqueryUI/jquery.ui.base.css" />
	<link rel="stylesheet" type="text/css" href="jqueryUI/jquery.ui.theme.css" />
	<style type="text/css">

	.ui-layout-center ,	/* has content-div */
	.ui-layout-west ,	/* has Accordion */
	.ui-layout-east ,	/* has content-div ... */
	.ui-layout-east .ui-layout-content { /* content-div has Accordion */
		padding: 0;
		overflow: hidden;
	}
	.ui-layout-center P.ui-layout-content {
		line-height:	1.4em;
		margin:			0; /* remove top/bottom margins from <P> used as content-div */
	}
	h3, h4 { /* Headers & Footer in Center & East panes */
		font-size:		1.1em;
		background:		#EEF;
		border:			1px solid #BBB;
		border-width:	0 0 1px;
		padding:		7px 10px;
		margin:			0;
	}
	.ui-layout-east h4 { /* Footer in East-pane */
		font-size:		0.9em;
		font-weight:	normal;
		border-width:	1px 0 0;
	}
	</style>
	<!-- REQUIRED scripts for layout widget -->
	<script type="text/javascript" src="jqueryUI/jquery-latest.js"></script>

	<script type="text/javascript" src="jqueryUI/jquery-ui-latest.js"></script>
	<script type="text/javascript" src="jqueryUI/jquery.layout-latest.js"></script>
	<script type="text/javascript" src="jqueryUI/jquery.layout.resizePaneAccordions-latest.js"></script>
	<!-- compressed: /lib/js/jquery.layout.resizePaneAccordions-latest.min.js -->

    <script type="text/javascript" src="jqueryUI/themeswitchertool.js"></script> 
	<script type="text/javascript" src="jqueryUI/debug.js"></script>
	
	<!--  -->

<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
<!-- <script type="text/javascript" src="js/jquery.js"></script>
</script> -->
<script type="text/javascript" src="js/main3.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geocode&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>

<script type="text/javascript">

	var myLayout; // a var is required because this page utilizes: myLayout.allowOverflow() method

	$(document).ready(function () {
		myLayout = $('body').layout({
			// enable showOverflow on west-pane so popups will overlap north pane
			west__showOverflowOnHover: true,
			west__size:300,
			east__size:600  
		//,	west__fxSettings_open: { easing: "easeOutBounce", duration: 750 }
		});
        // THEME SWITCHER
        addThemeSwitcher('#themeSwitch', {
        	top: '-3px', 
        	right: '5px' 
        });
        // if a new theme is applied, it could change the height of some content,
        // so call resizeAll to 'correct' any header/footer heights affected
        // NOTE: this is only necessary because we are changing CSS *AFTER LOADING* using themeSwitcher
        setTimeout(myLayout.resizeAll, 1000); /* allow time for browser to re-render with new theme */


 	});

	</script>
	

	<style type="text/css">
	/**
	 *	Basic Layout Theme
	 * 
	 *	This theme uses the default layout class-names for all classes
	 *	Add any 'custom class-names', from options: paneClass, resizerClass, togglerClass
	 */

	.ui-layout-pane { /* all 'panes' */ 
		background: #FFF; 
		border: 1px solid #BBB; 
		padding: 10px; 
		overflow: auto;
	} 

	.ui-layout-resizer { /* all 'resizer-bars' */ 
		background: #DDD; 
	} 

	.ui-layout-toggler { /* all 'toggler-buttons' */ 
		background: #AAA; 
	} 


	</style>


	<style type="text/css">
	/**
	 *	ALL CSS below is only for cosmetic and demo purposes
	 *	Nothing here affects the appearance of the layout
	 */

	body {
		font-family: Arial, sans-serif;
		font-size: 0.85em;
	}
	p {
		margin: 1em 0;
	}

	/*
	 *	Rules below are for simulated drop-down/pop-up lists
	 */

	ul {
		/* rules common to BOTH inner and outer UL */
		z-index:	100000;
		margin:		1ex 0;
		padding:	0;
		list-style:	none;
		cursor:		pointer;
		border:		1px solid Black;
		/* rules for outer UL only */
		width:		15ex;
		position:	relative;
	}
	ul li {
		background-color: #EEE;
		padding: 0.15em 1em 0.3em 5px;
	}
	ul ul {
		display:	none;
		position:	absolute;
		width:		100%;
		left:		-1px;
		/* Pop-Up */
		bottom:		0;
		margin:		0;
		margin-bottom: 1.55em;
	}
	.ui-layout-north ul ul {
		/* Drop-Down */
		bottom:		auto;
		margin:		0;
		margin-top:	1.45em;
	}
	ul ul li		{ padding: 3px 1em 3px 5px; }
	ul ul li:hover	{ background-color: #FF9; }
	ul li:hover ul	{ display:	block; background-color: #EEE; }
	.ui-helper-hidden-accessible {
	display: none;
	}
		
		</style>
<style type="text/css">
.ui-autocomplete {
	display: block;
	margin: 0;
	padding: 2px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
	background-color: #EEEEEE;
	border: 1px solid #DDDDDD;
	max-height: 200px;
	overflow-y: scroll;
	text-align: left;
	z-index: 9999;
	width: 458px;
}
.ui-state-hover,.ui-widget-content .ui-state-hover,.ui-state-focus,.ui-widget-content .ui-state-focus {
	border: 1px solid #C77405;
	background-color: #FFFFFF;
	color: #C77405;
	font-weight: bold;
	outline: medium none;
}
</style>

<script type="text/javascript">

<%OpenRequestBO openRequestBo = (OpenRequestBO) request
					.getAttribute("openRequestBean");
			if (openRequestBo == null) {
				openRequestBo = new OpenRequestBO();
			}%> 
document.onkeydown = function(e){
    if (e == null) { // ie
        keycode = event.keyCode;
      } else { 
        keycode = e.which;
      } 
    if(keycode==13 && document.getElementById("tableLandmark").value==1){
	   	selectEnterLandValues();
	   	document.getElementById("tableLandmark").value=="";
	   	document.getElementById('AddDetails').style.display="none";
    }else if(keycode==13 && document.getElementById("tableLandmark").value=="") {
    	<%if ((request.getAttribute("status") != null) 	&& (request.getAttribute("status").equals("Submit"))) { %>
			document.getElementById('Button').value="Submit";
		<%} else {%>
			document.getElementById('Button').value="Update";
		<%}%>
    	document.masterForm.submit();
        document.getElementById("tableLandmark").value=="";
	    document.getElementById('AddDetails').style.display="none";
    }
    }
function openAdddetails(AddDetails)
{
	
  
  var add1 = document.getElementById('sadd1').value;
  var city = document.getElementById('scity').value;
  var zip = document.getElementById('szip').value;
  var state = document.getElementById('sstate').value;
  var latitude=document.getElementById('sLatitude').value
  var longitude=document.getElementById('sLongitude').value
	
  var mydivmap = document.getElementById('AddDetails');

  var xmlhttp=null;
  
  if (window.XMLHttpRequest)
  {
	xmlhttp=new XMLHttpRequest();
	
  }
 else
 {
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
 }	
 
	var url = "RegistrationAjax?event=getmapdetails&address='"+add1+"'&latitude='"+latitude+"'&longitude='"+longitude+"'";
	xmlhttp.open("GET",url,false);
	xmlhttp.send(null);
	var text=xmlhttp.responseText;
	var resp=text.split("###");
	if(resp[0].indexOf("002;")>=0){
		document.getElementById('AddDetails').innerHTML="No closeby addresses"; 
		document.getElementById('tableLandmark').value=1;
	}
	else if(resp[0]!=""){
		document.getElementById('AddDetails').innerHTML=resp[1]; 
		document.getElementById('tableLandmark').value=1;
	}
  return true;
}
function clearLatNLongRam(){
	document.getElementById('sLongitude').value  = "";
	document.getElementById('sLatitude').value  = "";
}
function closeLM()
{
	window.location.href="/TDS/control";
}
function validatePhone(){
	var phone=document.getElementById('phone').value;
	if(phone==""||phone.length==10){
		return true;
	} else {
		alert("Check Your Phone Number");
		document.getElementById('phone').value="";
		return false;
	}
}
function submitLM(type){
	if(type===1){
		document.getElementById('Button').value="Submit";
	} else if(type==2){
		document.getElementById('Button').value="Update";
	}
	document.masterForm.submit();
}
$(document).ready ( function(){
	 if(document.getElementById('sLongitude').value != ""){
		 initializeMap(1);
	 }
	 	 $( "button,input[type=button],a" ).button().click(function( event ) {
			event.preventDefault();
		});
});
</script>

<title>Landmark Creation</title>
</head>
<body onload="start()">
<div class="ui-layout-north ui-widget-content"  align="right">
	<a id="themeSwitch"></a>
</div>
	<form name="masterForm" id="masterForm" action="control" method="post">
	<div class="ui-layout-center">
	<h3 class="ui-widget-header"><center>Create Landmark</center></h3>
	<%if(request.getAttribute("sNo") !=null ){%> 
	<input type="hidden" name="snoLandmarkPending" id="snoLandmarkPending" value=<%=landmarkPendingSNo%> />
		<%} %>
		<input type="hidden" name="fadd1temp" id="fadd1temp" value="" /> <input
			type="hidden" name="fadd2temp" id="fadd2temp" value="" /> <input
			type="hidden" name="fcitytemp" id="fcitytemp" value="" /> <input
			type="hidden" name="fstatetemp" id="fstatetemp" value="" /> <input
			type="hidden" name="fziptemp" id="fziptemp" value="" /> <input
			type="hidden" name="landmarktemp" id="landmarkTemp" value="" /> <input
			type="hidden" name="landMarkSelect" id="landMarkSelect" value="1" />
			<input type="hidden" name="totalAddress" id="totalAddress" value="0"/>
			<input type="hidden" name="Button" id="Button" value="" />
		<input type="hidden" name="latitemp" id="latitemp" value="" /> <input
			type="hidden" name="longitemp" id="longitemp" value="" /> <input
			type="hidden" name="defaultLati" id="defaultLati"
			value="<%=adminBo.getDefaultLati()%>" /> <input type="hidden"
			name="defaultLongi" id="defaultLongi"
			value="<%=adminBo.getDefaultLogi()%>" /> <input type="hidden"
			name="eLatitude" id="eLatitude" value=""></input> <input
			type="hidden" name="eLongitude" id="eLongitude" value=""></input> <input
			type="hidden" name="defaultCountry" id="defaultCountry"
			value="<%=adminBo.getCountry()%>" />
		<form name="masterForm" action="control" method="post" />
		<input type="hidden" name="action" value="registration" /> <input
			type="hidden" name="event" value="landmark" /> <input type="hidden"
			name="operation" value="2" /> <input type="hidden"
			name="tableLandmark" id="tableLandmark" value="" /> <input
			type="hidden" name="module" id="module"
			value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />
		<input type="hidden" name="key" value="  <%=openRequestBo.getLandMarkKey()==null?"":openRequestBo.getLandMarkKey()%>" />
		<input type="hidden" id="addressCheck" value="<%=adminBo.getAddressFill()%>"/>
		<input type="hidden" id="userAddress" value=""/>
			
		<br />
		<br />
		<%
			String error = (String) request.getAttribute("error");
			if (request.getParameter("error") != null) {
				error = request.getParameter("error");

			}
		%>
		<div id="errorpage">
			<%=(error != null && error.length() > 0) ? "" + error : ""%>


		</div>
		<div id="landmarkAddress" >
			<center>
				Address:<input name="address" id="address" type="text" size="35" placeholder="Enter the Address !"  onblur="initializeMap(1)"></input>
			</center>
		</div>

		<div id="landmark" 	style="width: 100%; height: 100%; border-color: gray; border: 1px">
			<div id="landmarkTable" >
				<table >
					<tr>
						<td colspan="2"><font size="3">Landmark</font><input
							type="text" name="Landmark" id="Landmark"
							value="<%=openRequestBo.getSlandmark()%>" size="15"
							style="margin-left: 14px" placeholder="Enter the Landmark!"/></td>
						<td><font size="3">Ph. No.</font>
						</td>
						<td ><input type="text" name="phone" id="phone"
							size="15"
							value="<%=openRequestBo.getPhone() != null ? openRequestBo
					.getPhone() : ""%>"
							 onblur="validatePhone()" placeholder="Enter the Phone NO!"/></td>
					</tr>
					<tr>
						<td colspan="2"><font size="3">Address1</font> <input
							type="text" name="sadd1" id="sadd1" size="15"
							value="<%=openRequestBo.getSadd1()%>"
							onkeypress="clearLatNLongRam()" placeholder="Enter the Address 1 !"/></td>
						<td><font size="3" >Address2 </font>
						</td>
						<td ><input type="text" name="sadd2" id="sadd2"
							size="15" value="<%=openRequestBo.getSadd2()%> " placeholder="Enter the Address 2 !" /></td>
					</tr>
					<tr>
						<td colspan="2"><font size="3">City</font><input type="text"
							name="scity" id="scity" size="15"
							value="<%=openRequestBo.getScity()%>"
							onkeypress="clearLatNLongRam()" placeholder="Enter the City Name !"/></td>
						<td><font size="3">State</font>
						</td>
						<td><input type="text" name="sstate" size="3"
							id="sstate" placeholder="Enter the State !" value="<%=openRequestBo.getSstate().equals("") ? adminBo.getState()
					: openRequestBo.getSstate()%>"
							/> <font size="3">Zip</font>
						
						<input type="text" name="szip" size="5"
							id="szip" value="<%=openRequestBo.getSzip()%>" placeholder="Enter the Zip !"
							></input>
						</td>
					</tr>

					<tr>
						<td colspan="2"><font size="3">Latitude</font> <input
							type="text" name="sLatitude" id="sLatitude" size="15"
							value="<%=openRequestBo.getSlat() == null ? "" : openRequestBo
					.getSlat()%>" placeholder="Enter the Latitude"
							 /></td>
						<td ><font size="3">Longitude</font>
						</td>
						<td ><input type="text" name="sLongitude"
							id="sLongitude" size="15"
							value="<%=openRequestBo.getSlong() == null ? "" : openRequestBo
					.getSlong()%>" placeholder="Enter the Longitude !"
							 /></td>
					</tr>
					<tr>
						<td colspan="2"><font size="3">Key Name</font><input
							type="text" name="keyName" id="keyName" value="<%=openRequestBo.getLandmarkKeyName() %>"
							style="margin-left: 10px" /></td>
							
						 <td><font size="3">Cab Comments</font> </td><td>
						 <textarea rows="3" cols="15" name="comments" maxlength="250"><%=openRequestBo.getComments()==null?"":openRequestBo.getComments() %></textarea></td>
					</tr>
					<tr>
						<td colspan="2"><font size="3">Don't Dispatch</font><input
							type="checkbox" name="dontdis" id="dontdis" value="1"
							 /></td>
						<td colspan="2"><font size="3">Address Verified</font> <input
							type="checkbox" name="addverified" id="addverified"
							onclick="return addVerified()" value="1"  /></td>
					</tr>

					<tr>
						<td><input type="button" class="styled-button"
							value="Check Closeby LandMarks"
							onclick="openAdddetails('AddDetails')" /></td>
						<%
							if ((request.getAttribute("status") != null)
									&& (request.getAttribute("status").equals("Submit"))) {
						%>
						<td><input class="styled-button" name="Button" id="Button"
							type="button" value="Submit" align="right" onclick="submitLM(1)" />
						</td>
						<%
							} else {
						%>
						<td ><input class="styled-button" name="Button"
							id="Button" type="button" value="Update" align="right"
							onclick="submitLM(2)" /></td>
						<%
							}
						%>

						<td><input class="styled-button" name="Button" id="Button"
							type="button" value="close" align="right" onclick="closeLM()" />
						</td>
					</tr>
				</table>
				<div id="AddDetails2" >
					<table>
						<tr>
							<td>
								<div id="AddDetails" ></div></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		</div>
	</form>
	<div class="ui-layout-east">
		<div id="map_canvas" style="width: 100%; height: 100%;">
			</div></div>
</body>
</html>
