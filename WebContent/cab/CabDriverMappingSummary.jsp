
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>



<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.Address" %>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.CabDriverMappingBean"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<% ArrayList<CabDriverMappingBean> al_list = new ArrayList<CabDriverMappingBean>();
if(request.getAttribute("al_list")!=null){
al_list=(ArrayList<CabDriverMappingBean>)request.getAttribute("al_list");
}
System.out.print("JSP--->"+al_list.size());
CabDriverMappingBean MapBean = (CabDriverMappingBean)request.getAttribute("driverMappingBean"); 

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript"src="Ajax/Ajaxverification.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var CabDriverMapping = document.getElementById("tableid");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>CabDriverMapping Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(CabDriverMapping.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
<title>TDS (Taxi Dispatch System)</title>
</head>

<body>

<form action="control"  method="post"   name="summary" onsubmit="return showProcess()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
 <input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="CabDriverMappingSummary"/>
 
<div class="rightCol">
<div class="rightColIn">
<br />
                	<c class="nav-header"><center>Driver Cab Mapping List</center></c> 
                	<br />
                	<br />               	                	 
                	<table width="70%" border="0" cellspacing="0" cellpadding="0" class="dTableheader" align="center">
                		<tr>
                			<td>Driver ID</td>                					                       	
                    		<td>  
                    	    <input type="text"  name="driver_id"  id="driver_id"  value=""/>
                    	  	  <ajax:autocomplete
				  					fieldId="driver_id"
				  					popupId="model-popup1"
				  					targetId="driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" />  
                    		</td>                	 
                    		<td>Cab No</td>
                			<td><input type="text"    name="cabno" id="cabno"  value=""/> 
                		 		<ajax:autocomplete
				  					fieldId="cabno"
				  					popupId="model-popup1"
				  					targetId="cabno"
				  					baseUrl="autocomplete.view"
				  					paramName="cabNum"
				  					className="autocomplete"
				  					progressStyle="throbbing" />  	   
                			</td>
                		</tr>
                		<tr>
                			<td align="center"   colspan="4" > <input  type="submit" name="button" value="Search"    /> </td>
                		</tr>
                	</table>
                	<br />
                	<br />
                    <table width="70%" border="1" style="background-color:white;" cellspacing="0" cellpadding="0" align="center"  id="tableid" >
                      <tr><td><input type="button" value="Save/Print" id="btnPrint" />
                      </td></tr>
                       <tr align="center">
                        <th>Driver Id</th>
                        <th>Cab No</th>
                        <th>From Date</th>
                        <th>To Date</th>
                    </tr>
                    <%
                    if(!al_list.isEmpty()){							
                	boolean colorLightGreen = true;
                	String colorPattern;
					for(int i=0;i<al_list.size();i++){ 
		                    colorLightGreen = !colorLightGreen;
		            		if(colorLightGreen){
		            			colorPattern="style=\"background-color:lightblue\""; 
		            		}
		            		else{
		            			colorPattern="";
		            		}
						CabDriverMappingBean mappingBean = (CabDriverMappingBean)al_list.get(i);
						String userkey= mappingBean.getUserKey();						
					%>		
                      <tr  <%=colorPattern%> align="center">
                        	<td>
					 			<%=mappingBean.getDriver_id()  %>					  
							</td>
							
							<td>
								<%=mappingBean.getCab_no() %>
							</td>
							
							<td>
								<%=mappingBean.getFrom_date() %> 									
							</td>
							
							<td>
								<%=mappingBean.getTo_date() %>									
						   </td>
							<td <%=colorPattern%>><input  type="button"  name="deletebutton" value="Delete"  onclick="ajaxdeletecabmapdetails(<%=userkey%>,this);" ></input></td>						   
					</tr>
					<%}}
					else
					{					
					%>
					<tr>
					<td colspan="4" align="center"> No Records Found 
					</td>
					</tr>
					<%}%>		
				</table>

                    
                </div>    
              </div>
            	
            <footer>Copyright &copy; 2010 Get A Cab</footer>
            </form>
 
</body>
</html>
