<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">

function LogoutWindow(row_id,driverid) { 
	//alert(row_id);
	//	alert(driverid); 
	window.location.href ="/TDS/control?action=registration&event=driverlogout&module=operationView&Logintime="+row_id+"&logoutDriverId="+driverid; 
}
function deleteWindow(row_id) {
	//alert(row_id); 
	window.location.href ="/TDS/control?action=registration&event=driverlogout&module=operationView&deletekey="+row_id; 
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}



</script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/label.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var driverlogout = document.getElementById("driverlogout");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>Logout Driver/Operator Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(driverlogout.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
</head>

<%ArrayList ar_list = new ArrayList();

if(request.getAttribute("ar_list")!=null) {
	ar_list = (ArrayList)request.getAttribute("ar_list");
}

%>
<%ArrayList ar_hislist = new ArrayList();

if(request.getAttribute("ar_hislist")!=null) {
	ar_hislist = (ArrayList)request.getAttribute("ar_hislist");
}

%>

<body>
<form name="masterForm" action="control" method="get"  onsubmit="return showProcess()"/>
<input type="hidden" name="action" value="registration"/>
		<input type="hidden" name="event" value="driverlogout"/>
		<input type="hidden" name="operation" value="2"/>
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
		<div class="rightCol">
		<div class="rightColIn">	
		<c class="nav-header"><center>Logout Driver/Operator </center></c>
		<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
	<tr>
	<td>
			<table id="bodypage" width="100%" align="center" >
                          <%if(((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equals("Driver")) {%>
                    <tr>
                     	<td>Driver</td>
                    	<td>    
                           <input type="text" size="8"   value="<%=((AdminRegistrationBO)session.getAttribute("user")).getUid() %>" readonly="readonly"  name="driverid"  id="driverid"/>                                    
                          </td>
                          </tr>
                        <%} else { %>
                          <tr> 
                     	<td>Driver</td>
                    	<td>
                    	<input type="text" size="8"    id="driverid" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" name="driverid"  autocomplete="off"  onblur="caldid(id,'drivername','dName')"  id="driverid" value="<%=request.getAttribute("driver") == null ? "" : request.getAttribute("driver")  %>" class="form-autocomplete" />
                                 
                                    <ajax:autocomplete
				  					fieldId="driverid"
				  					popupId="model-popup1"
				  					targetId="driverid"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" />  	   
                    	</td>
                    	<td>Cab No</td>
                    	<td><input  type="text"   name="cabNo"  id="cabNo"   size="8" /> 
                    	 <ajax:autocomplete
				  					fieldId="cabNo"
				  					popupId="model-popup1"
				  					targetId="cabNo"
				  					baseUrl="autocomplete.view"
				  					paramName="cabNum"
				  					className="autocomplete"
				  					progressStyle="throbbing" />  	   
                    	</td>
                    	<td>Include Operators</td>
                    	<td><input  type="checkbox" name="includeOpr"  id="includeOpr" value="1" /> 
                    	</td>                    	
                    	</tr>
                    	<tr><td>Logout Driver</td>
                    	<td><input  type="checkbox" name="loutDriver"  id="loutDriver" value="1" /> 
                    	</td></tr>
                    	<tr>   
                    	<td>From</td>
					 	<td><input type="text" name="fromDate" id="fromDate" value="" onclick="showCalendarControl(this)" autocomplete="off"/></td>
				     	 </tr><tr><td>To</td>
					 	<td><input type="text" name="toDate" id="toDate" value="" onclick="showCalendarControl(this)" autocomplete="off"/></td>
				        </tr>         	
                        
                    	 <tr>
                    	 	<td align="center"  colspan="4" >
                    	 	<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>"/> 
							 	  <div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
                    	 	</td>
                    	 </tr>
                    	<%} %>                   
                    <tr align="center" >                     	                        
 					<td colspan="4" align="center">
					<div class="wid60 marAuto padT10">
                        
                        	   <input name="Button" type="submit"  class="lft"     value="Search"   />
                        	   <input type="button" value="Save/Print" id="btnPrint" />
		
  
                        	 </div>                                            	
					</td>                     	
                    </tr>
                    </table> 
	</td>
</tr>
</table>
<br />
<%if(request.getAttribute("ar_list")!=null) { %>
<table width="50%"  border="1"  align="center" class="thumbnail"  id="driverlogout">
      
	<tr align="center" style="background-color: lightblue">
		<th width="15%">LOGIN ID</th>
		<th width="15%">LOGINTIME</th>
		<th width="20%">PROFILE</th>
		<th width="15%">DRIVERNAME</th>
	    <th width="15%">CABNO</th>
		<th width="20%" >DETAILS</th>
	</tr>
	<%
	boolean colorlightGreen=true;
	String color;%>
	<%for(int i=0;i<ar_list.size();i=i+5) { 
		colorlightGreen=!colorlightGreen;
		if(colorlightGreen){
			color = "style=\"background-color:lightblue\""; 
		}
		else{
			color="";
		}
	%>	
	<tr align="center" <%=color%>> 
	    <td width="15%" > <%=ar_list.get(i) %>    </td>
		<td width="20%"> <%=ar_list.get(i+1) %>  </td>
		<td width="15%" > <%=ar_list.get(i+2) %>  </td> 
		<td width="15%" > <%=ar_list.get(i+3) %> </td>
		<td width="15%" > <%=ar_list.get(i+4) %> </td>
       	   
 	<td width="20%"> <input  type="button"   value="Logout"   onclick="showProcess();LogoutWindow('<%=ar_list.get(i+1) %>','<%=ar_list.get(i) %>')"   /> </td>
	</tr>	 
	<% } %>
</table>
<%}%>
 <%if(request.getAttribute("ar_hislist")!=null) { %>
<table width="50%"  border="1"  align="center" class="thumbnail" id="driverlogout">
        <tr align="center" style="background-color: lightblue">
		<th width="15%">LOGIN ID</th>
		<th width="15%">LASTLOGINTIME</th>
		<th width="20%">PROFILE</th>
		<th width="15%">DRIVERNAME</th>
		<th width="15%">CABNO</th>
		<th width="15%">LOGOUTTIME</th>
	</tr>
	<%
	boolean colorlightGreen=true;
	String color;%>
	<%for(int i=0;i<ar_hislist.size();i=i+6) { 
		colorlightGreen=!colorlightGreen;
		if(colorlightGreen){
			color = "style=\"background-color:lightblue\""; 
		}
		else{
			color="";
		}
	%>	
	<tr align="center" <%=color%>> 
	    <td width="15%" > <%=ar_hislist.get(i) %>    </td>
		<td width="40%" > <%=ar_hislist.get(i+1) %>  </td>
		<td width="20%"> <%=ar_hislist.get(i+2) %>  </td>
		<td width="15%" > <%=ar_hislist.get(i+3) %>  </td> 
 		<td width="15%" > <%=ar_hislist.get(i+4) %> </td>
        <td width="15%" > <%=ar_hislist.get(i+5) %> </td>
        	   
	</tr>	 
	<% } %>
</table>
<%}%>
 
</div>
</div>

</body>
</html>