<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
 
</script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

   
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
		<form name="masterForm" action="control" method="get"  />
		
		<input type="hidden" name="action" value="registration"/>
		<input type="hidden" name="event" value="disburshmentmessage"/>
		<input type="hidden" name="operation" value="2"/>
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
 
 
 


 	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div class="rightCol">
<div class="rightColIn">
<h1>Driver Disbursement Message</h1>
<%
									
									String error = (String) request.getAttribute("error");
			 						if(request.getParameter("error")!=null)
									{
										error = request.getParameter("error");
										
									}
								%>
								<div id="errorpage">
								<%= (error !=null && error.length()>0)?""+error :"" %>
								
 
								</div>
                        		 
                        		 
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
                    <td>
                    <table id="bodypage" width="440"    >
                    <tr>
                    	<td  class="firstCol">Start Date</td>
                    	<td>  
                    	    <input type="text"  name="startDate"  id="startDate"   onfocus="showCalendarControl(this);" size="10"    value="<%=(request.getAttribute("stdate")==null)?"":request.getAttribute("stdate") %>"      readonly="readonly"   /> 
                    	</td>
                    </tr>
                    <tr>
                    	<td class="firstCol" >End Date</td>
                    	<td> 
                    	    <input type="text" name="endDate"   id="endDate" onfocus="showCalendarControl(this);" size="10" value="<%=(request.getAttribute("eddate"))==null?"":request.getAttribute("eddate") %>"     readonly="readonly" /> 
                     	</td>
                    </tr> 
                      <tr>
                    	<td class="firstCol" >Short Desc</td>
                    	<td>
                    	<div class="div2"><div class="div2In">
                                    <textarea name="shortDesc"  cols="15" rows="2"<%=(request.getAttribute("shortdesc"))==null?"":request.getAttribute("shortdesc") %>></textarea>
                                    </div></div>
                    	</td> 
                    </tr>
                    <tr>
                    	<td class="firstCol" >Long Desc</td>
                    	<td>
                    	<div class="div2"><div class="div2In">
                                <textarea name="longDesc"  cols="15" rows="5"<%=(request.getAttribute("longdesc"))==null?"":request.getAttribute("longdesc") %>></textarea>
                                    </div></div>
                    	</td>
                    </tr>
                    
                    			
 							<tr>
				<td colspan="2" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	   <input name="Button" type="submit"  class="lft"     value="Submit"   />
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
				</td>
			</tr> 
                   
                    </table>
                   
                    </td>
                    </tr>
                    </table>
                 </div>
                 </div>
 
 
  
            	
                <div class="clrBth"></div>
          
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</body>
</html>
