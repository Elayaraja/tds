<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.CabRegistrationBean"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
<%
	ArrayList al_summ = (ArrayList)request.getAttribute("al_summary");
%>
<form name="masterForm" action="control" method="post">
<input type="hidden" name="action" value="registration">
<input type="hidden" name="event" value="cabRegistrationSummary">
<table id="pagebox">
 <tr>
 <td>
	<table id="bodypage" > 
		<tr>
			<td>
				<table >	
					<tr>
						<td colspan="7" align="center">		
							<div id="title">
								<h2>Cab Registration Summary </h2>
							</div>
						</td>		
					</tr>
					<tr>
							<td valign="top">
								<table border=1>	
									<tr>
								 	 	<td>Cab Make</td>
										<td><input type="text" name="make"  tabindex="2" id="cab_cabmake" size="8" value="<%=request.getParameter("cab_cabmake")==null?"":request.getParameter("cab_cabmake") %>"></td>
								 		<td>Cab Year</td>
										<td><input type="text" name="year" id="cab_cabyear" size="8" tabindex="3" value="<%=request.getParameter("cab_cabyear")==null?"":request.getParameter("cab_cabyear") %>"></td>
									</tr>
			 					</table>
			 			</td>
			 		</tr>
			 		<tr>
			 			<td colspan="2" align="center"><input type="submit" name="Button" value="Search"></td>
			 		</tr>
					</table>
				</td>
			</tr>
			<%if(al_summ!=null && al_summ.size()>0) {%>
			<tr>
				<td>
					<table border=1>
						<tr background="grey">
							<td>Cab No</td><td>Cab Make</td><td>Cab Year</td><td>Certification of Registration</td>
						</tr>
						<%
							for(int i=0;i<al_summ.size();i++){
								CabRegistrationBean registrationBean = (CabRegistrationBean)al_summ.get(i);
						
						%>
							<tr>
								<td><a href="/TDS/control?action=registration&event=cabRegistration&Button=Edit&key=<%=registrationBean.getCab_cabkey() %>"><%=registrationBean.getCab_cabno() %></td>
								<td><%=registrationBean.getCab_cabmake() %></td>
								<td><%=registrationBean.getCab_cabYear() %></td>
							</tr>
						<%} %>
					</table>
				</td>
			</tr>		
			<%} %>
 </table>
 </td></tr>
 </table>
</form>
</body>
</html>