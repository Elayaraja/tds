<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.CabDriverMappingBean"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

<script type="text/javascript">

function clearErrorInfo(){
	document.getElementById("errorpage").innerHTML ="";
}
 function autocom(i)
 {
	 autocomplete("driver_id"+i, "model-popup2", "driver_id"+i, "autocomplete.view", "DRIVERID", null, "throbbing",null);
 }
 function cabAutocom(i)
 {
	 autocomplete("cab_no"+i, "model-popup2", "cab_no"+i, "autocomplete.view", "cabNum", null, "throbbing",null);
 }
 
 function addRow(){
	var i=Number(document.getElementById("size").value);
	var table = document.getElementById("tableid");
	  var rowCount = table.rows.length;
	  var row = table.insertRow(rowCount);
	  var cell = row.insertCell(0);
	  var cell1 = row.insertCell(1);
	  var cell2 = row.insertCell(2);
	  var cell3 = row.insertCell(3);
	  //var cell4 = row.insertCell(4);
	  
	  var element = document.createElement("input");
	  element.type = "text";
	  element.name="driver_id"+i;
	  element.id="driver_id"+i;
	  element.size = '12';
	  element.className="form-autocomplete";
	  element.onkeypress = function(){
		  	autocom(i);
	  }
	  element.onfocus=function(){
		  var assocode= <%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>;
		  appendAssocode(assocode,"driver_id"+i);		
	  }
	  element.onblur = function(){
		  	caldid("driver_id"+i,'drivername'+i+'','dName'+i+'');
	  }
	   cell.appendChild(element);
	  element = document.createElement("div");
	  element.className="autocomplete";
	  element.id ="model-popup2"; 
	  cell.appendChild(element);
	  element = document.createElement("div");
	  element.id ="drivername"+i; 
	  cell.appendChild(element);
	  element = document.createElement("input");
	  element.id="dName"+i;
	  element.name="dName"+i;
	  element.type = "hidden";
	  cell.appendChild(element);
	  
	  
		var element1 = document.createElement("input");
		element1.type = "text";
		element1.name="cab_no"+i;
		element1.id="cab_no"+i;
		element1.size = '12';
		 element1.className="form-autocomplete";
		  element1.onkeypress = function(){
			  cabAutocom(i);
		  }
		cell1.appendChild(element1);
	 	 
		var element3 = document.createElement("input");
	 	element3.type = "text";
		 element3.name="from_date"+i;
		 element3.readOnly=true;
		element3.size = '10';
		element3.onfocus=new Function("showCalendarControl(this)");
		cell2.appendChild(element3);
		
		element3 = document.createElement("input");
	 	element3.type = "text";
		 element3.name="to_date"+i; 
		 element3.readOnly=true;
		element3.size = '10';
		element3.onfocus=new Function("showCalendarControl(this)");
		cell3.appendChild(element3);
		 
		document.getElementById("size").value=i+1;
}
function delRow(){
 	//alert("Remove");
 	var table = document.getElementById("tableid");
 	var i=Number(document.getElementById("size").value);
	var lastRow = table.rows.length; 
		if (lastRow > 2) 
		{
			table.deleteRow(lastRow - 1);
			document.getElementById("size").value = i-1;
		}  
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
<% 
	ArrayList<CabDriverMappingBean> al_list = (ArrayList<CabDriverMappingBean>)request.getAttribute("al_list"); 
%>
<form name="masterForm" action="control" method="post" onsubmit="return showProcess()">
<input type="hidden" name="action" value="registration" />
<input type="hidden" name="event" value="CabDriverMapping" />
<input type="hidden" name="size" id="size" value="<%=al_list.size() %>" />
<input type="hidden" name="systemsetup" value="6" />
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<br />
<c class="nav-header"><center>
                	Cab Driver Mapping</center></c>
                	<br />
                    <table width="80%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab" id="tableid" align="center">
	                    <%
							StringBuffer error = (StringBuffer) request.getAttribute("error");						
						%>
					<tr>
					<td colspan="7" style="color: red">
							<font color="red"><%=request.getParameter("page")==null?"":request.getParameter("page") %></font>
							<div id="errorpage">
								<%= (error !=null && error.length()>0)?""+error.toString() :"" %>
							</div>
							<div id="msgid">
							   	<font color="red" <%=request.getParameter("msgtype")!=null && request.getParameter("msgtype").equals("1")?"lightblue":"red" %>>
									<%= request.getParameter("pageMsg")==null?"":request.getParameter("pageMsg") %>
								</font>
							</div>
							</td>
							</tr>							
					<tr align="center">
                        <th width="15%">Driver Id</th>
                        <th width="15%">Cab No</th>
                        <th width="15%">From Date</th>
                        <th width="15%">To Date</th>
                   </tr>
                   <%
						for(int i=0;i<al_list.size();i++){ 
						CabDriverMappingBean mappingBean = al_list.get(i);
				   %>
				  <tr>
						<td>
							<input type="text"  name="driver_id<%=i%>" size="12" id="driver_id<%=i%>" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername<%=i %>','dName<%=i %>');clearErrorInfo()"  value="<%=mappingBean.getDriver_id() %>" class="form-autocomplete" onkeypress="autocom(<%=i %>)" autocomplete="off" />
							<input type="hidden" name="dName<%=i %>"  id="dName<%=i %>" value="<%=request.getParameter("dName"+i)==null?"":request.getParameter("dName"+i) %>" />
							<div id="drivername<%=i %>"><%=request.getParameter("dName"+i)==null?"":request.getParameter("dName"+i) %></div>
							<div id="model-popup2" class="autocomplete"></div>										  
					   	</td>					
						<td>
							<input type="text" name="cab_no<%=i %>" id="cab_no<%=i %>" value="<%=mappingBean.getCab_no() %>" size="12" class="form-autocomplete" onkeypress="cabAutocom(<%=i %>)" autocomplete="off" />						
					   	</td>									
					  	<td>
							<input type="text"  size="10" name="from_date<%=i %>" size="10" readonly="readonly" value="<%=mappingBean.getFrom_date() %>"  onfocus="showCalendarControl(this);" />									
					  	</td>						
					 	 <td>
							<input type="text"  size="10" name="to_date<%=i %>" size="10" readonly="readonly" value="<%=mappingBean.getTo_date() %>"  onfocus="showCalendarControl(this);" />									
					 	</td>
				 </tr>			
						<%} %>	
				 <tr></tr>
				 	</table>
				 	<br />
				 	<table width="80%" align="center">
					<tr align="center">
							<td colspan="4" align="center">
                                <input type="button" name="Button" value="Add" onclick="addRow()" ></input>
	                            <input type="button" name="removeButton" value="Remove" onclick="delRow()"></input>
	                            <input type="submit" name="Button" value="Submit" />
	                          </td>                                                                    
                      </tr>
                     </table>                       
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
