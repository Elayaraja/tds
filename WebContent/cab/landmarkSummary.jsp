<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style>
h2{
font-weight:bold; 
font-style:italic;
color:grey;
}
#errorpage{
color:red;
font-weight:bold;
}

</style>
<script type="text/javascript">
 
</script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 

<script type="text/javascript">
function deleteLandmark(serialNo, x){
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='RegistrationAjax?event=deleteLandmark&module=operationView&delete=YES&LMKey='+serialNo;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="Deleted Successfully"){
	var table = document.getElementById("bodypage");
	 table.deleteRow(x.parentNode.parentNode.rowIndex);
	}
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
 <script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var landmarksummary = document.getElementById("landmarksummary");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>Landmarksummary Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(landmarksummary.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>  
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>LandmarkCreation</title>
</head>

<%ArrayList<OpenRequestBO> ar_list =(ArrayList<OpenRequestBO>)request.getAttribute("ar_list");
OpenRequestBO openRequestBean = new OpenRequestBO();%>
<body>
<form name="landSearch"  method="post" action="control" onsubmit="return showProcess()">

<input type="hidden" name="action" value="registration"/>
		<input type="hidden" name="event" value="landmarkSummary"/>
		<input type="hidden" name="operation" value="2"/>
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />

	
	
<h2><center>Landmark  Summary</center></h2>

               	   
                	<table width="100%" border="0" cellspacing="2" cellpadding="0" >
                		<tr>
                	    <td>LandMark:</td> 
                    	<td><input type="text" name="slandmark" size="10"  id="slandmark" autocomplete="off"   /> 
                                    <ajax:autocomplete
  									fieldId="slandmark"
  									popupId="model-popup1"
  									targetId="slandmark"
  									baseUrl="autocomplete.view"
  									paramName="landmark"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
  									<input 	type="hidden" id="newLandMark1" value=""></input></td>		
					    
                		<td>
			<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input  type="submit" name="button" value="Search" />
                        	  <input type="button" value="Save/Print" id="btnPrint" /> </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>
                    </td>  
                		</tr>
               	</table> 
 
<table  border="1"  width="100%" cellspacing="2" cellpadding="0" id="landmarksummary">
<tr>
	<td>
	<table id="bodypage" width="100%" cellspacing="3"  align="center"  border="1" id="landmarksummary">
	<%if(ar_list!=null&&ar_list.size()>0){%>
	<tr>
		<td style="background-color:lightgrey" align="center">LANDMARK</td>
		<td style="background-color:lightgrey" align="center">PHONE NUMBER</td>
		<td style="background-color:lightgrey" align="center">ADD1</td>
		<td style="background-color:lightgrey" align="center">CITY</td>
		<td style="background-color:lightgrey" align="center">STATE</td>
		<td style="background-color:lightgrey" align="center">ZIP</td>
		<td style="background-color:lightgrey" align="center">DISPATCH STATUS</td>
		<td style="background-color:lightgrey"></td>
	</tr>
	<%
	boolean colorlightblue=true;
	String color;%>
	<%for(int i=0;i<ar_list.size();i++) { 
		colorlightblue=!colorlightblue;
		if(colorlightblue){
			color="style=\"background-color:lightblue\"";
		}
		else
			color="style=\"background-color:white\"";
	%>
	<tr align="center">
		<td <%=color%> ><a href="control?action=registration&event=landmark&Button=Edit&module=operationView&key=<%=ar_list.get(i).getLandMarkKey() %>"  >   <%=ar_list.get(i).getSlandmark() %> </a> </td>
		<td <%=color%>><%=ar_list.get(i).getPhone()==null?"":ar_list.get(i).getPhone() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getSadd1()==null?"":ar_list.get(i).getSadd1() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getScity()==null?"":ar_list.get(i).getScity() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getSstate()==null?"":ar_list.get(i).getSstate() %>  </td>
		<td <%=color%>><%=ar_list.get(i).getSzip()==null?"":ar_list.get(i).getSzip() %>  </td> 
		<td <%=color%>><%=ar_list.get(i).getDontDispatch()%>  </td>
		 
				<td <%=color%>><input type="button" name="delete" id="delete" value="Delete"  onclick="showProcess();deleteLandmark(<%=ar_list.get(i).getLandMarkKey() %>,this)"/></td>
		
	</tr>
	<%} %>
	
</table>
</div> 

	<% }else if(request.getParameter("button")!=null){%>
	
	<div id="errorpage" align="center">No Records Found </div>
	<%} %>
	</td></tr>
	</table> 
</div>
</div> 
</form> 
</body>
</html>