<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.cmp.bean.CabRegistrationBean"%>
<%@page import="com.tds.cmp.bean.MeterType"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.common.util.TDSProperties"%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.dao.RegistrationDAO" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<%  
String wasl_ccode = TDSProperties.getValue("WASL_Company");
	ArrayList al_list = new ArrayList();
	if(request.getAttribute("cabFlag")!=null){
		al_list = (ArrayList)request.getAttribute("cabFlag"); 
	}
%>

<%AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");%>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

 <script type="text/javascript">

function cabYearNumCheck()
{
	var NumCab = document.getElementById('cab_cabyear');
	
	NumCabvalue = NumCab.value;
	 if(NumCabvalue.length>4)
		 alert("Please enter only  4 numbers."); 	    
	      {
	      if(isNaN(NumCabvalue))
	       {
		    alert("Please enter only numbers.");
		    NumCab.value="";
		    return false;
	       }	
		return true;
	  }
	    NumCabvalue.length==0;
	    return false;
}



function fixDigit()
{
	alert(" fixDigit() function getting called");
	var fixDigit = document.getElementById('cab_cabyear');
	
	 fixDigitValue = fixDigit.value;
	 alert(fixDigitValue);
	  if(fixDigitValue.length>4)
	  {
		alert("Please enter only  4 numbers.");
		
		return false;	
		
	 }	
		return true;
		
}

function loadFile(docId){
	document.getElementById("docLoaderImage").src='searchDocs?do=viewDoc&docId='+docId;
}

$(document).ready ( function(){
	 <%if(al_list==null || al_list.isEmpty()){%>
		loadcabFlags();
	 <%}%>
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=getDocumentId&module=systemsetupView&cabNumber='+document.getElementById("cab_cabno").value;
	//var url='SystemSetupAjax?event=getDocumentId&module=systemsetupView&driverId=103001';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	document.getElementById("docLoader").innerHTML=text;
});
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}

function loadcabFlags()
{
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var vehicleCounter=0;
	var url='SystemSetupAjax?event=getFlagsForCompany';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"flagDetails\":"+text+"}";
	var obj = JSON.parse(jsonObj.toString());
	document.getElementById("vehicleProfile").innerHTML="";
	var $tbl = $('<table style="width:77%">').attr('id', 'flagValues');
	for(var j=0;j<obj.flagDetails.length;j++){
		if(obj.flagDetails[j].SW=="2"){
			$tbl.append(
				$('<td>').text(obj.flagDetails[j].LD).append($('<input/>').attr('type', 'checkbox').attr('name', 'chk'+vehicleCounter).attr('id', 'chk'+vehicleCounter).val(obj.flagDetails[j].K).attr('onclick',"checkGroup('"+vehicleCounter+"','"+obj.flagDetails[j].GI+"','"+obj.flagDetails[j].LD+"')")),
				$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vgroup'+vehicleCounter).attr('id','vgroup'+vehicleCounter).val(obj.flagDetails[j].GI)),
				$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vLD'+vehicleCounter).attr('id','vLD'+vehicleCounter).val(obj.flagDetails[j].LD))
			);
			vehicleCounter=Number(vehicleCounter)+1;
			$("#vehicleProfile").append($tbl);
		}
	}
	document.getElementById("profileSizeAjax").value=vehicleCounter;
}

function checkGroup(row,groupId,longDesc){
	var veProf="";
	if(document.getElementById("profileSizeAjax").value!=""){
		veProf=document.getElementById("profileSizeAjax").value;
	} else {
		veProf=document.getElementById("profileSize").value;
	}
	if(document.getElementById("chk"+row).checked==true){
		for(var i=0;i<veProf;i++){
			if(document.getElementById("chk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
				document.getElementById("chk"+row).checked=false;
				 alert("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
				 break;
			}
		}
	}
}

function showLongDesc(value){
	document.getElementById("longDescription").innerHTML=value;
	$("#longDescription").fadeIn().fadeOut().fadeIn().fadeOut();
}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
<title>TDS (Taxi Dispatch System)</title>
<!-- <script type="text/javascript" src="Ajax/OpenRequest.js"></script> -->

</head>

<body onload="loader()">

<form name="cabform" action="control" method="post" onsubmit="return showProcess()">
<input type="hidden" name="action" value="registration"> </input>
<input type="hidden" name="event" value="cabRegistration"></input>
<input type="hidden" name="systemsetup" value="6"></input>
<input type="hidden" id="profileSizeAjax" name="profileSizeAjax" value=""/>
<input type="hidden" name="numberOfDocumentId" id="numberOfDocumentId" value=""></input>
<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"/>
  <%
	CabRegistrationBean registrationBean = (CabRegistrationBean)request.getAttribute("registrationBean");
	
  	if(registrationBean==null){
  		registrationBean =  new CabRegistrationBean();
  	}
  	String status = "";
  	if(request.getAttribute("status")==null){
	  	status = request.getParameter("status");
  	}else {
  		status = request.getAttribute("status").toString();
  	}

  	boolean isEdit = status.equalsIgnoreCase("Update")?true:false;
  	System.out.println("status:"+status+"---isedit:"+isEdit);
  %>
		 <c class="nav-header"> <center>
                	Cab Registration</center></c><br/>
         <table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
                    <%
						StringBuffer error = (StringBuffer) request.getAttribute("error");
				    %>
				    <tr>
				    	<td>
				   		<div id="errorpage">
								<%= (error !=null && error.length()>0)?""+error.toString() :"" %>
				    	 </div>
				    	 <div id="msgid">
						<font color=<%=request.getParameter("msgtype")!=null && request.getParameter("msgtype").equals("1")?"green":"red" %>>
						<%= request.getParameter("pageMsg")==null?"":request.getParameter("pageMsg") %>
						</font>
					</div>
					
					<c class="nav-header">
					Vehicle Details
					</c>
				    <table id="bodypage" width="540">
					<tr>
					<td>Cab&nbsp;No</td>
					<td> 
						<input type="text" name="cab_cabno" id="cab_cabno" size="15"   value="<%=registrationBean.getCab_cabno()%>"  >
						<input type="hidden" name="cab_cabkey" id="cab_cabkey" value="<%=registrationBean.getCab_cabkey() %>">
					</td>
					<td>Cab&nbsp;Year</td>
					<td>
						<input type="text" name="cab_cabyear" id="cab_cabyear" size="15"  value="<%=registrationBean.getCab_cabYear() %>"  onkeyup="cabYearNumCheck()"/>
					</td>
					</tr>
					<tr>
					<td>Cab&nbsp;Make</td>
					<td>
						<input type="text" name="cab_cabmake"   id="cab_cabmake" size="15" value="<%=registrationBean.getCab_cabmake() %>">
					</td>
					<td>Model</td>
                    <td>
						<input type="text" name="cab_model"   id="cab_model" size="15" value="<%=registrationBean.getCab_model() %>">
		    	    </td>
					</tr>	
				</table><br /><br />
				<c class="nav-header">
				Vehicle Registration Details
				</c>
				<table id="bodypage" width="540">
				<tr>
				<td>Vehicle&nbsp;Identification&nbsp;No</td>				
				<td>
					<input type="text" name="cab_vin"  id="cab_vin" size="15" value="<%=registrationBean.getCab_vinno() %>"/>
		    	</td>
		    	<td>Registration&nbsp;No / Sequence No</td>
					<%if(isEdit){ %>
						<td style="color: red;"><%=registrationBean.getCab_rcno() %>
						<input type="hidden" name="cab_rcno" id="cab_rcno" value="<%=registrationBean.getCab_rcno() %>" size="15"/>
						</td>
					<%}else{ %>
						<td><input type="text" name="cab_rcno" id="cab_rcno" value="<%=registrationBean.getCab_rcno() %>" size="15"/> </td>
					<%} %>
				</tr>
				<tr>
				<td>Vehicle&nbsp;Plate&nbsp;No</td>
				<%if(isEdit){ %>
					<td style="color: red;"> <%=registrationBean.getCab_plate_number() %> <input type="hidden" name="cab_plate_No"  id="cab_plate_No" size="15" value="<%=registrationBean.getCab_plate_number() %>"/></td>
				<%}else{%>
					<td><input type="text" name="cab_plate_No"  id="cab_plate_No" size="15" value="<%=registrationBean.getCab_plate_number() %>"/></td>
				<%} %>
				
				<%if(adminBo.getAssociateCode().equals(wasl_ccode)){ %>
				<%if(isEdit){ %>
		    	<td>Vehicle&nbsp;Plate&nbsp;Name</td>
				<td>
					<table>
					<thead><tr><td align="center">Left</td><td align="center">Middle</td><td align="center">Right</td></tr></thead>
					<tbody>
					<tr>
					<td align="center" style="color: red; width: 30%;"> <%=registrationBean.getCab_plate_name_left() %> <input type="hidden" name="cab_p_name_L" id="cab_p_name_L" value="<%=registrationBean.getCab_plate_name_left() %>" /></td>
					<td align="center" style="color: red; width: 30%;"> <%=registrationBean.getCab_plate_name_middle() %> <input type="hidden" name="cab_p_name_M" id="cab_p_name_M" value="<%=registrationBean.getCab_plate_name_middle() %>" /></td>
					<td align="center" style="color: red; width: 30%;"> <%=registrationBean.getCab_plate_name_right() %> <input type="hidden" name="cab_p_name_R" id="cab_p_name_R" value="<%=registrationBean.getCab_plate_name_right() %>" /></td>
					</tr>
					</tbody>
					</table>
					
			    </td>
				<%}else{ %>
		    	<td>Vehicle&nbsp;Plate&nbsp;Name</td>
				<td>
					<table>
					<thead><tr><td align="center">Left</td><td align="center">Middle</td><td align="center">Right</td></tr></thead>
					<tbody>
					<tr>
					<td><input type="text" name="cab_p_name_L" id="cab_p_name_L" value="<%=registrationBean.getCab_plate_name_left() %>" size="15"/></td>
					<td><input type="text" name="cab_p_name_M" id="cab_p_name_M" value="<%=registrationBean.getCab_plate_name_middle() %>" size="15"/></td>
					<td><input type="text" name="cab_p_name_R" id="cab_p_name_R" value="<%=registrationBean.getCab_plate_name_right() %>" size="15"/></td>
					</tr>
					</tbody>
					</table>
					
			    </td>
				<%} %>
				<%} %>
				</tr>
				
				<tr>
				<td>Registration&nbsp;Date</td>				
				<td>
					<input type="text" name="txt_regDate"  id="txt_regDate" size="15" value="<%=registrationBean.getCab_registrationDate() %>" onfocus="showCalendarControl(this);" />
		    	</td>
		    	<td>Expiry&nbsp;Date</td>
					<td >
					<input type="text" name="txt_expDate" id="txt_expDate" value="<%=registrationBean.getCab_expiryDate() %>" size="15" onfocus="showCalendarControl(this);" />
			  		</td>
				</tr>
				</table> <br /><br />
				
				<c class="nav-header">
				Vehicle Status
				</c>
				<table id="bodypage" width="540">
				<tr>
				 <td>Status</td>
                 <td align="center">
                 		<div><div>
						 <div>
                	<select name="status">
                	<% System.out.println("cab STATUS::"+registrationBean.getStatus()); 
                	if((registrationBean.getStatus()!=null) && (registrationBean.getStatus().equalsIgnoreCase("1"))) {
                	%>
						<option value="0" <%=registrationBean.getStatus().equalsIgnoreCase("0")?"selected":"" %>  ><%="In Active" %></option>
						<option value="1"  <%=registrationBean.getStatus().equalsIgnoreCase("1")?"selected":"" %> ><%="Active"%></option> 
				 		
						 <%}else { %>
						
						<option value="1"  <%=registrationBean.getStatus().equalsIgnoreCase("1")?"selected":"" %>  ><%="Active" %></option>
						<option value="0"  <%=registrationBean.getStatus().equalsIgnoreCase("0")?"selected":"" %> ><%="In Active" %></option>
					
						<%} %>
					</select>
					</div></div>
                </td>
			<%-- 	
				<td class="firstCol">Vehicle Type</td>
				<td align="center">
                 	 <div class="div2"><div class="div2In"> <div class="inputBXWrap wid130">
                	 <select name="vtype" id="vtype" class="inputBX" >
                	    <option value="1"  <%=registrationBean.getCab_vtype().equalsIgnoreCase("1")?"selected":"" %>  ><%="Mini Van" %></option>
						<option value="2"   <%=registrationBean.getCab_vtype().equalsIgnoreCase("2")?"selected":"" %>  ><%="2 Door" %></option>
						<option value="3"  <%=registrationBean.getCab_vtype().equalsIgnoreCase("3")?"selected":"" %>  ><%="4 Door" %></option>
						<option value="4"  <%=registrationBean.getCab_vtype().equalsIgnoreCase("4")?"selected":"" %>  ><%="Fuel Efficient" %></option>
                	
                	</select>
					</div></div></div>
				</td> --%>
				
					<td style="white-space:nowrap">OdoMeter&nbsp;Value</td>
					<td><input type="number" name="odoMetervalue" id="odoMetervalue" value="<%=registrationBean.getOdoMeterValue()%>"></input>
					</td>
				</tr>
				<tr>
				<td style="white-space:nowrap">Default&nbsp;Meter&nbsp;Rate</td>
				<td>
					<select name="defaultMeterRate">
					<%
					try{
						ArrayList<MeterType> meters =registrationBean.getCab_meterTypes();
						int key=Integer.parseInt(registrationBean.getCab_meterRate()!=null&&registrationBean.getCab_meterRate()!=""?registrationBean.getCab_meterRate():"0");
						for(int i=0;i<meters.size();i++){
							if(key==meters.get(i).getKey()){
								%>
								<option value=<%=meters.get(i).getKey() %> selected> <%=meters.get(i).getMeterName()%></option>
								<%
							}else{
								%>
								<option value=<%=meters.get(i).getKey() %> ><%=meters.get(i).getMeterName()%></option>
								<%
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					%>
					</select>
				</td>
				</tr>
				</table> <br /> <br /> 
				<c class="nav-header">
				Payment Password
				</c>
				<table id="bodypage" width="540">
				<tr>
				 <td>Pay&nbsp;Id</td>
                 <td align="center">
                 <input type="text" name="vehPayId" id="vehPayId" value="<%=registrationBean.getPayId()==null?"":registrationBean.getPayId()%>"/>
				</td>
				 <td>Pay&nbsp;Password</td>
                 <td align="center">
                 <input type="password" name="vehPayPass" id="vehPayPass" value="<%=registrationBean.getPayPass()==null?"":registrationBean.getPayPass()%>"/>
				</td>
				</tr>
				</table>
				<br/><br />
				<c class="nav-header">
				Vehicle&nbsp;Profile</c>
				<table id="bodypage">
				<tr>
				<%if(al_list!=null && !al_list.isEmpty()) {%>
				<td> 
				<input type="hidden" name="profileSize" id="profileSize" value="<%=al_list.size()/3 %>"></input>
				</td>
				<td colspan="8">
				<%for(int i=0,j=0;i<al_list.size();i=i+3,j++){ %>
						<input type="checkbox" onclick="checkGroup('<%=j%>','<%=al_list.get(i+2)%>','<%=al_list.get(i)%>')"
						<%
										if(request.getParameter("chk"+j)!=null)
											 {
												 out.print("checked");
											 } else {
												 if(registrationBean.getAl_list()!=null)
												 {
													 for(int k=0;k<registrationBean.getAl_list().size();k++)
													 {
														 if(registrationBean.getAl_list().get(k).toString().equals(al_list.get(i+1)))
														 {
															 out.print("checked");
															 break;
														 }
													 }
												 }
											 }
										
										%>
									 name="chk<%=j %>" id="chk<%=j %>" value="<%=al_list.get(i+1) %>">
									 <%=al_list.get(i) %>
									 </input>
									 <input type="hidden" name="vgroup<%=j %>" id="vgroup<%=j %>" value="<%=al_list.get(i+2)%>"/>
									 <input type="hidden" name="vLD<%=j %>" id="vLD<%=j %>" value="<%=al_list.get(i)%>"/>
								<%} %>
								</td>
					<%} %>	
					</tr>
				</table>
 						
 			<div id="vehicleProfile" <%=(al_list!=null && !al_list.isEmpty())?"style=display:none":""%>></div>	
				
			<%-- <tr>
			
			 <td colspan="4 " align="center"> <table>
					<%if(al_list!=null && !al_list.isEmpty()) {%>
						<th><td>Vehicle Profile <input type="hidden" name="profileSize" value="<%=al_list.size()/2 %>"></td> </th>
						<%for(int i=0,j=0;i<al_list.size();i=i+2,j++){ %>
							<tr>
								<td>
									<input type="checkbox"
										<%
											 if(request.getParameter("chk"+j)!=null)
											 {
												 out.print("checked");
											 } else {
												 if(registrationBean.getAl_list()!=null)
												 {
													 for(int k=0;k<registrationBean.getAl_list().size();k++)
													 {
														 if(registrationBean.getAl_list().get(k).toString().equals(al_list.get(i+1)))
														 {
															 out.print("checked");
															 break;
														 }
													 }
												 }
											 }
										
										%>
									 name="chk<%=j %>" value="<%=al_list.get(i+1) %>"><%=al_list.get(i) %>
								</td>							
							</tr>
						<%} %>
					<%} %>
					</table>
					</td> 
					
					
					</tr> --%>
					
					<br /> <br />
					<center>
					<table>			 
					<tr>
					<td colspan="7" align="center">
					<%if(status.equals("Insert")){ %>
                        	 <input type="submit" name="Button" id="Button"   value="Submit"/>
                    <%}else{ %>
                        	 <input type="submit" name="Button" id="Button" value="Update"/>
                   <div>
	
	 
            <table id="documentImages">
            	<tr>
            	</tr>
            
            	</table>	</center>
	</div>
                    <%} %>	
					</td>
				</tr>
				
			</table>
			</td>
			</tr>
			</table>                     
                </div>    
              </div>
            		<div id="docLoader"></div>
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
  </form>
</body>
</html>