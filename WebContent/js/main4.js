var geocoder;
var map;
var marker;
var poly;
var latLng;
function initialize(){
//	MAP
	var latitude;
	var longitude;
	latitude=document.getElementById("sLatitude").value;
	longitude=document.getElementById("sLongitude").value;
	var latlng = new google.maps.LatLng(latitude,longitude);
	var options = {
			zoom: 12,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	map = new google.maps.Map(document.getElementById("map_canvas"), options);

	//GEOCODER
	geocoder = new google.maps.Geocoder();

	var polyOptions = {
			strokeColor: '#F52887',
			strokeOpacity: 0.9,
			strokeWeight: 4
	}
	poly = new google.maps.Polyline(polyOptions);
	poly.setMap(map);


	showLatLong1();
}
function showLatLong1() {
	var imageBlue="images/blueCar.png";
	var imageGreen="images/greenCar.png";
	var imageRed="images/redCar.png";
	var imageBlack="images/blackCar.png";
	var count=1;
	var oRows1=document.getElementById("driverDetailDisplay").getElementsByTagName("td");
	var iRowCount1= oRows1.length;
	var currentLat;
	var previousLat=0.00;
	var distance;
	var fuel;
	var carbon;
	var sameSpot = false;
	
	for(var i=0;i<iRowCount1;i++){
		var leftTime;
		lat = oRows1[i].getElementsByTagName("input")[0].value;
		longi = oRows1[i].getElementsByTagName("input")[1].value;
		status=oRows1[i].getElementsByTagName("input")[2].value;
		lastUpdate=oRows1[i].getElementsByTagName("input")[3].value;
		driverId=oRows1[i].getElementsByTagName("input")[4].value;
		distance=oRows1[i].getElementsByTagName("input")[5].value;
		fuel=oRows1[i].getElementsByTagName("input")[6].value;
		carbon=oRows1[i].getElementsByTagName("input")[7].value;
		var markerDriver;
		latLng = new google.maps.LatLng(lat,longi);
		function infoCallback(infowindow, marker) { 
			return function() { 
				infowindow.open(map, marker);
			}; 
		}
		if(true){
			currentLat= oRows1[i].getElementsByTagName("input")[0].value;
			//previousLat=oRows1[i-1].getElementsByTagName("input")[0].value;
//			if(lat==0.00000 || longi==0.00000){
//				if(status=="L") {
//					lat = oRows1[i-1].getElementsByTagName("input")[0].value;
//					longi = oRows1[i-1].getElementsByTagName("input")[1].value;
//					latLng = new google.maps.LatLng(lat,longi);
//					sameSpot=false;
//					markerDriver = new google.maps.Marker({
//						position : latLng,
//						map : map,
//						icon : imageRed
//					});
//					var path = poly.getPath();
//					path.push(latLng);
//				}
//			}else{
				if(currentLat==previousLat){
					var enteringTime;
					if(count==1){
						enteringTime=oRows1[i-1].getElementsByTagName("input")[3].value;
						count=count+1;
					}else {
						leftTime=oRows1[i].getElementsByTagName("input")[3].value;

						var infoWindow = new google.maps.InfoWindow({
							content : "This driver "+driverId+" last updates here at "+lastUpdate
						});
						infoWindow.set('position',new google.maps.LatLng(lat,longi));
						if(currentLat!=oRows1[i+1].getElementsByTagName("input")[0].value)
							google.maps.event.addListener(markerDriver, 'click',infoCallback(infoWindow,markerDriver));
					}
					if(sameSpot){
						continue;
					} else{
						sameSpot = true;
					}
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlack
					});
				} else if(status=="L") {
					previousLat=currentLat;
					count=1;
					sameSpot=false;
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageRed
					});
					var infoWindow = new google.maps.InfoWindow({
						content : "This driver "+driverId+" last updates here at "+lastUpdate
					});
					infoWindow.set('position',new google.maps.LatLng(lat,longi));
					google.maps.event.addListener(markerDriver, 'click',infoCallback(infoWindow,markerDriver));
				} else if(i==0){
					previousLat=currentLat;
					sameSpot=false;
					count=1;
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageGreen
					});
					var infoWindow = new google.maps.InfoWindow({
						content : "This driver "+driverId+" last updates here at "+lastUpdate
					});
					infoWindow.set('position',new google.maps.LatLng(lat,longi));
					google.maps.event.addListener(markerDriver, 'click',infoCallback(infoWindow,markerDriver));
				} else if(status=="Y"){
					previousLat=currentLat;
					sameSpot=false;
					count=1;
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlue
					});
					var infoWindow = new google.maps.InfoWindow({
						content : "This driver "+driverId+" last updates here at "+lastUpdate
					});
					infoWindow.set('position',new google.maps.LatLng(lat,longi));
					google.maps.event.addListener(markerDriver, 'click',infoCallback(infoWindow,markerDriver));
				} else {
					previousLat=currentLat;
					sameSpot=false;
					count=1;
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlue
					});
					var infoWindow = new google.maps.InfoWindow({
						content : "This driver "+driverId+" last updates here at "+lastUpdate
					});
					infoWindow.set('position',new google.maps.LatLng(lat,longi));
					google.maps.event.addListener(markerDriver, 'click',infoCallback(infoWindow,markerDriver));
				}
				markerDriver.setMap(map);
				var path = poly.getPath();
				path.push(latLng);
//			};
		}
		  var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
		  jobLabel.set('position', new google.maps.LatLng(lat,longi));
		  jobLabel.set('text',i); 
	}
}

$(document).ready(function() {
	initialize();
});