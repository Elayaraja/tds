function showLogs(){
	var screenValue=document.getElementById("screenValue").value;
	var trip=document.getElementById("tripId").value;
	var xmlhttp = null;
	if (window.XMLHttpRequest){
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=logsDetail&tripId='+trip;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text =xmlhttp.responseText;
	if($('#popUpOR').is(':visible') || $('#ORDash').is(':visible')){
		if(screenValue=="10"){	
			$("#popUpOR" ).dialog( "close" );
			$("#popUpOR").hide();
		} else {
			$("#ORDash").jqmHide();
		}
	}
	if($('#paymentDetailsDash').is(':visible')){
		if(screenValue=="10"){	
			 $("#paymentDetailsDash" ).dialog( "close" );
			 $("#paymentDetailsDash").hide();
		}else{
			 $("#paymentDetailsDash").jqmHide();
		}
	}
	if($('#jobDetailsMapDash').is(':visible')){
		if(screenValue=="10"){	
			$("#jobDetailsMapDash" ).dialog( "close" );
			$("#jobDetailsMapDash").hide();
		} else {
			$('#jobDetailsMapDash').jqmHide();
		}
	}
	document.getElementById("jobDetailsLogsDiv").style.zIndex =3000;
	document.getElementById("jobDetailsLogsPopUp").innerHTML="";
	$("#jobDetailsLogsPopUp").append("               ");
	$("#jobDetailsLogsPopUp").append("Trip ID:"+trip);
	$("#jobDetailsLogsPopUp").append(text);
	if(screenValue=="10"){	
		$( "#jobDetailsLogsDiv" ).dialog({
		     modal: true,
	     	width:1030,
	     	height:700
		 });
	} else {
		$("#jobDetailsLogsDiv").jqm();
		$("#jobDetailsLogsDiv").jqmShow();
	}
	$("#detailsOR").attr('disabled',false);
	$("#detailsLogs").attr('disabled','disabled');
	document.getElementById("detailsLogs").style.background='#ffffff';
	document.getElementById("detailsOR").style.background='#000000';
	document.getElementById("detailsLogsFont").style.color='#000000';
	document.getElementById("detailsORFont").style.color='#ffffff';
	document.getElementById("detailsPayment").style.background='#000000';
	document.getElementById("detailsPaymentFont").style.color='#ffffff';
	document.getElementById("mapForJobDetails").style.background='#000000';
	document.getElementById("mapForJobDetailsFont").style.color='#ffffff';
	$("input[type=text]").attr('readonly',false);
	$("input[type=checkbox]").attr('disabled',false);
	$(".dontEdit").attr('readonly', 'readonly');
	$(".buttonORDashboard").hide();
	$("select").attr('disabled',false);
	$("textarea").attr('readonly',false);
}
function showPayment(){
	var screenValue=document.getElementById("screenValue").value;
	document.getElementById("detailsLogs").style.background='#000000';
	document.getElementById("detailsLogsFont").style.color='#ffffff';
	document.getElementById("detailsOR").style.background='#000000';
	document.getElementById("detailsORFont").style.color='#ffffff';
	document.getElementById("detailsPayment").style.background='#ffffff';
	document.getElementById("detailsPaymentFont").style.color='#000000';
	document.getElementById("mapForJobDetails").style.background='#000000';
	document.getElementById("mapForJobDetailsFont").style.color='#ffffff';
	$("#detailsOR").attr('disabled',false);
	$("#detailsLogs").attr('disabled',false);
	$("#detailsPayment").attr('disabled','disabled');
	if($('#popUpOR').is(':visible') || $('#ORDash').is(':visible')){
		if(screenValue=="10"){	
			 $("#popUpOR" ).dialog( "close" );
			 $("#popUpOR").hide();
		} else {
			 $("#ORDash").jqmHide();
		}
	}
	if($('#jobDetailsLogsDiv').is(':visible')){
		if(screenValue=="10"){	
			$("#jobDetailsLogsDiv" ).dialog( "close" );
			$("#jobDetailsLogsDiv").hide();
		} else {
			$("#jobDetailsLogsDiv").jqmHide();
		}
	}
	if($('#jobDetailsMapDash').is(':visible')){
		if(screenValue=="10"){	
			$("#jobDetailsMapDash" ).dialog( "close" );
			$("#jobDetailsMapDash").hide();
		} else {
			$('#jobDetailsMapDash').jqmHide();
		}
	}
	var xmlhttp=null;
	var	tripId=document.getElementById('tripId').value;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url='RegistrationAjax?event=getPaymentDetails&tripId='+tripId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	for(var j=0;j<obj.address.length;j++){
		var description=obj.address[j].D;
		var number=obj.address[j].N;
		var type=obj.address[j].S;
		var amount=obj.address[j].A;
		var payType=obj.address[j].PV;
		if(description=="Failed")
		{
			document.getElementById('paymentDetailsDashDiv').innerHTML = "<b><center>Payment Details</center> <img alt='' style='z-index: 4000;' src='images/Dashboard/close.png' onclick='removePayment()'></b>No Records Found";
		} else {
			if(j==0){
				document.getElementById('paymentDetailsDashDiv').innerHTML = "<b><center>Payment Details</center><img alt='' style='z-index: 4000;' src='images/Dashboard/close.png' onclick='removePayment()'></br><br><b>Pay Type	:</b>	"+type+" <br/><b>Number	:</b>	"+number+" <br/><b>Name	:</b>"+description+" <br/><b><center>Charge Details</center></br>";
			}
			$('#paymentDetailsDashDiv').append(payType+"	<b>-</b>	"+amount+"</br>");
		}
		if(screenValue=="10"){	
			$( "#paymentDetailsDash" ).dialog({
			     modal: true,
		      	width:1030,
		     	height:700
			 });
		} else {
			$('#paymentDetailsDash').jqm();
			$('#paymentDetailsDash').jqmShow();
		}
		$("input[type=text]").attr('readonly',false);
		$("input[type=checkbox]").attr('disabled',false);
		$(".dontEdit").attr('readonly', 'readonly');
		$(".buttonORDashboard").hide();
		$("select").attr('disabled',false);
		$("textarea").attr('readonly',false);
	}
}
function openORDetails(tripId){
	var screenValue=document.getElementById("screenValue").value;
	/*document.getElementById("tabForDetails").style.display="visible";
	document.getElementById("tabForDetails").style.zindex="3000";*/
//	alert("i am in");
	$("#tabForDetails").show();
	if(tripId==null || tripId==''){
		tripId=document.getElementById("tripId").value;
		$("#tabForDetails").show();
		/*$('#ORDash').jqm();
		$('#ORDash').jqmShow();*/
		document.getElementById("detailsLogsFont").style.color='#ffffff';
		document.getElementById("detailsLogs").style.background='#000000';
		document.getElementById("detailsPayment").style.background='#000000';
		document.getElementById("detailsPaymentFont").style.color='#ffffff';
		document.getElementById("mapForJobDetails").style.background='#000000';
		document.getElementById("mapForJobDetailsFont").style.color='#ffffff';
		$("#detailsPayment").attr('disabled',false);
		$("#detailsLogs").attr('disabled',false);
	}else{
		document.getElementById("detailsLogsFont").style.color='#ffffff';
		document.getElementById("detailsLogs").style.background='#000000';
		document.getElementById("detailsPayment").style.background='#000000';
		document.getElementById("detailsPaymentFont").style.color='#ffffff';
		document.getElementById("mapForJobDetails").style.background='#000000';
		document.getElementById("mapForJobDetailsFont").style.color='#ffffff';
	}
	$("#reDispatchInORDashboard").show();
	if(screenValue=="10"){	
		$("#popUpOR" ).dialog({
		     modal: true,
	     	width:1030,
	     	height:700
		 });
	} else {
		$('#ORDash').jqm({modal:true});
		$("#ORDash").jqmShow();
	}
	if($('#paymentDetailsDash').is(':visible')){
		if(screenValue=="10"){	
			 $("#paymentDetailsDash" ).dialog( "close" );
			 $("#paymentDetailsDash").hide();
		} else {
			$('#paymentDetailsDash').jqmHide();
		}
	}
	if($('#jobDetailsLogsDiv').is(':visible')){
		if(screenValue=="10"){	
			$("#jobDetailsLogsDiv" ).dialog( "close" );
			$("#jobDetailsLogsDiv").hide();
		} else {
			$("#jobDetailsLogsDiv").jqmHide();
		}
	}
	if($('#jobDetailsMapDash').is(':visible')){
		if(screenValue=="10"){	
			$("#jobDetailsMapDash" ).dialog( "close" );
			$("#jobDetailsMapDash").hide();
		} else {
			$('#jobDetailsMapDash').jqmHide();
		}
	}
	$("#detailsOR").attr('disabled','disabled');
	document.getElementById("detailsOR").style.background='#ffffff';
	document.getElementById("detailsORFont").style.color='#000000';
	$("input[type=text]").attr('readonly','readonly');
	$("input[type=checkbox]").attr('disabled','disabled');
	$(".buttonORDashboard").hide();
	$("select").attr('disabled','disabled');
	$("textarea").attr('readonly','readonly');
	document.getElementById('nameDash').focus();
	/*$("#paymentDetails").show();
	$("#jobLogsDiv").show("slow");
	document.getElementById("jobLogsDiv").style.zIndex =3000;*/
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'OpenRequestAjax?event=getOpenRequest&tripId='+tripId+'&fromHistory=YES';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"address\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	for(var j=0;j<obj.address.length;j++){
		document.getElementById('phoneDash').value=obj.address[j].Phone;
		document.getElementById('nameDash').value=obj.address[j].Name;
		document.getElementById('sadd1Dash').value=obj.address[j].Add1;
		document.getElementById('sadd2Dash').value=obj.address[j].Add2;
		document.getElementById('scityDash').value=obj.address[j].City;
		document.getElementById('sstateDash').value=obj.address[j].State;
		document.getElementById('szipDash').value=obj.address[j].Zip;
		document.getElementById('sLatitudeDash').value=obj.address[j].Lat;
		document.getElementById('sLongitudeDash').value=obj.address[j].Lon;
		document.getElementById('sdate').value=obj.address[j].Date;
		document.getElementById("timeTemporary").value=obj.address[j].Time;
		document.getElementById("shrs").value="NCH";
		document.getElementById('CommentsDash1').value=obj.address[j].Comments;
		document.getElementById('specialInsDash1').value=obj.address[j].SplIns;
		document.getElementById('tripId').value=obj.address[j].tripId;
		document.getElementById('eadd1Dash').value=obj.address[j].EAdd1;
		document.getElementById('eadd2Dash').value=obj.address[j].EAdd2;
		document.getElementById('ecityDash').value=obj.address[j].ECity;
		document.getElementById('estateDash').value=obj.address[j].EState;
		document.getElementById('ezipDash').value=obj.address[j].EZip;
		document.getElementById('eLatitude').value=obj.address[j].ELat;
		document.getElementById('eLongitude').value=obj.address[j].ELon;
		document.getElementById('payTypeDash').value=obj.address[j].PayType;
		document.getElementById('acctDash').value=obj.address[j].Acct;
		document.getElementById("queuenoDash").value=obj.address[j].Zone;
		document.getElementById('landMarkDash').value=obj.address[j].LandMark;
		document.getElementById('toLandMarkDash').value=obj.address[j].ELandMark;
		document.getElementById('numberOfPassengerDash').value=obj.address[j].numberOfPassengers;
		//document.getElementById('dropTimeDash').value=obj.address[j].dropTime;
		document.getElementById('eMailDash').value=obj.address[j].EM;
		document.getElementById('tripStatus').value=obj.address[j].TS;
		document.getElementById('airName').value=obj.address[j].ANAM;
		document.getElementById('airFrom').value=obj.address[j].AFROM;
		document.getElementById('airTo').value=obj.address[j].ATO;
		document.getElementById('airNo').value=obj.address[j].AN;
	
		if(obj.address[j].AT!="-1"){
			document.getElementById('advanceTime').value=obj.address[j].AT;
		}
		/*	if(document.getElementById("driverORcabDash").value=="Driver"){
			document.getElementById("driverDashOR").value=obj.address[j].Driver;
		} else {
			cab=document.getElementById("driverDashOR").value=obj.address[j].Vehicle;
		}*/
		//document.getElementById("repeatGroupDash").value=obj.address[j].multiJobTag;
		/*var driverList=document.getElementById("drprofileSizeDash").value;
		var vehicleList=document.getElementById("vprofileSizeDash").value;
		var drFlag=obj.address[j].Flag;*/
		var sharedRide=obj.address[j].sharedRide;
		var dontDispatch=obj.address[j].dontDispatch;
		var premiumCustomer=obj.address[j].premiumCustomer;
		if(sharedRide==1){
			document.getElementById("sharedRideDash").checked=true;
		} if(dontDispatch==1){
			document.getElementById("dispatchStatusDash").checked=true;
		} if(premiumCustomer==1){
			document.getElementById("premiumCustomerDash").checked=true;
		}
		/*	if(drFlag!=""){
			for (var i=0; i<driverList;i++){
				if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
					document.getElementById('dchk'+i).checked=true;
				}
			}
			for (var i=0;i<vehicleList;i++){
				if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
					document.getElementById('vchk'+i).checked=true;
				}
			}
			openSplReqDash();
		}	*/
	}
	if(document.getElementById('CommentsDash1').value!=""){
		openDispatchCommentsDash();
		document.getElementById('CommentsDash1').style.color='#000000';
	}
	if(	document.getElementById('specialInsDash1').value!=""){
		openDriverCommentsDash();
		document.getElementById('specialInsDash1').style.color='#000000';
		
	}
	 document.getElementById('airName').removeAttribute('readonly');
	 document.getElementById('airNo').removeAttribute('readonly');
	 document.getElementById('airFrom').removeAttribute('readonly');
	 document.getElementById('airTo').removeAttribute('readonly');

	if(document.getElementById('airName').value!="" && document.getElementById('airNo').value!="" && document.getElementById('airFrom').value!="" && document.getElementById('airTo').value!="" ){
		//openFlightInformationDash();
		var ele = document.getElementById("hideFlightInformation");
			if(ele.style.display == "block") {
             ele.style.display = "none";
    		}
		else {
			ele.style.display = "block";
			      
		}

		
      }

}
function jobDetailsMap(){
	var screenValue=document.getElementById("screenValue").value;
	var tripId=document.getElementById("tripId").value;
	document.getElementById("detailsLogsFont").style.color='#ffffff';
	document.getElementById("detailsLogs").style.background='#000000';
	document.getElementById("detailsPayment").style.background='#000000';
	document.getElementById("detailsPaymentFont").style.color='#ffffff';
	document.getElementById("mapForJobDetails").style.background='#ffffff';
	document.getElementById("mapForJobDetailsFont").style.color='#000000';
	document.getElementById("detailsOR").style.background='#000000';
	document.getElementById("detailsORFont").style.color='#ffffff';
	$("#mapForJobDetails").attr('disabled','disabled');
	if($('#popUpOR').is(':visible') || $('#ORDash').is(':visible')){
		if(screenValue=="10"){	
			 $("#popUpOR" ).dialog( "close" );
			 $("#popUpOR").hide();
		} else {
			$('#ORDash').jqmHide();
		}
	}
	if($('#paymentDetailsDash').is(':visible')){
		if(screenValue=="10"){	
			$("#paymentDetailsDash" ).dialog( "close" );
			$("#paymentDetailsDash").hide();
		} else {
			$("#paymentDetailsDash").jqmHide();
		}
	}
	if($('#jobDetailsLogsDiv').is(':visible')){
		if(screenValue=="10"){	
			 $("#jobDetailsLogsDiv" ).dialog( "close" );
			 $("#jobDetailsLogsDiv").hide();
		} else {
			$("#jobDetailsLogsDiv").jqmHide();
		}
	}
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url='OpenRequestAjax?event=getLatLongi&tripId='+tripId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	document.getElementById("jobDetailsMapDashDiv").innerHTML=text;
	if(screenValue=="10"){	
		$( "#jobDetailsMapDash" ).dialog({
		     modal: true,
	    	width:1030,
	     	height:700
		 });
	} else {
		$('#jobDetailsMapDash').jqm();
		$('#jobDetailsMapDash').jqmShow();
	}
	initializeForMapDetails();
	$("input[type=text]").attr('readonly',false);
	$("input[type=checkbox]").attr('disabled',false);
	$(".dontEdit").attr('readonly', 'readonly');
	$(".buttonORDashboard").hide();
	$("select").attr('disabled',false);
	$("textarea").attr('readonly',false);
	/*$("#jobDetailsLogsDiv").jqmHide();
    $('#paymentDetailsDash').jqmHide();*/

}
function removeORDetails(){
	var screenValue=document.getElementById("screenValue").value;
	if(screenValue=="10"){	
		 $("#popUpOR" ).dialog( "close" );
		 $("#popUpOR").hide();
	} else {
		$("#ORDash").jqmHide();
	}
	$("input[type=text]").attr('readonly',false);
	$("input[type=checkbox]").attr('disabled',false);
	$(".dontEdit").attr('readonly', 'readonly');
	$(".buttonORDashboard").hide();
	$("select").attr('disabled',false);
	$("textarea").attr('readonly',false);
	$("#tabForDetails").hide();
}
function removeJobsDetailsLogsPopUp(){
	var screenValue=document.getElementById("screenValue").value;
	if(screenValue=="10"){	
		 $("#jobDetailsLogsDiv" ).dialog( "close" );
		 $("#jobDetailsLogsDiv").hide();
	} else {
		$("#jobDetailsLogsDiv").jqmHide();
	}
	$("#tabForDetails").hide();
}

function removePayment(){
	var screenValue=document.getElementById("screenValue").value;
	if(screenValue=="10"){	
		 $("#paymentDetailsDash" ).dialog( "close" );
		 $("#paymentDetailsDash").hide();
	} else {
		$("#paymentDetailsDash").jqmHide();
	}
	$("#tabForDetails").hide();
}