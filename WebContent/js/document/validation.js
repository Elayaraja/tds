//*****************************************************************
// Check whether string s is empty.
function isEmpty(s)
{
	if((fnTrimSpaces(s) == null) || (fnTrimSpaces(s).length == 0))
	{
		gErrFlag = "T";
		return true;
	}
	else
	{
		gErrFlag = "F";
		return false;
	}
}



//***********************************************************************************
//fnTrimSpaces trims leading and trailing spaces in 'strToTrim'
//***********************************************************************************

function fnTrimSpaces(strToTrim)
{
	strTemp = strToTrim;
	len=strTemp.length;
	i=0;
	while (i < len && strTemp.charAt(i)==" ")
		i=i+1;
	strTemp = strTemp.substring(i);
	len=strTemp.length;
	j = len-1;
	while (j>0 && strTemp.charAt(j)==" ")
		j = j-1;
	strTemp = strTemp.substring(0,j+1);
	return strTemp;
}

function fnGetFullDate()
{
	var strFinalDate;
	var d = new Date();

	//Get all the attributes
	var strMonth = d.getMonth() +1;

	if( strMonth < 10 )
		strMonth = "0" + strMonth ;

	var strDay   = d.getDate();
	if( strDay < 10 )
		strDay = "0" + strDay;

	var strYear  = d.getFullYear(); //This will return full year

	var strFinalDate = strMonth + "/" + strDay + "/" + strYear;
	return strFinalDate;
}



//***********************************************************************************
//fnCompareDate compares two Dates.
//It returns 0  -  if Dates are equal
//	     -1  -  if FirstDate < SecondDate
//	      1  -  if FirstDate > SecondDate
//***********************************************************************************
function fnCompareDate(FirstDateObj, SecondDateObj)
{
	var fdateValue =FirstDateObj.value ;
	var sdateValue= SecondDateObj.value;
	if(fdateValue.length>10){
		fdateValue = fdateValue.substring(0,10);
	}
	if(sdateValue.length>10){
		sdateValue = sdateValue.substring(0,10);
	}

	var FirstDate = new Date(fdateValue);
	var SecondDate = new Date(sdateValue);
	if (FirstDate < SecondDate)
	{
		 return -1;
	}
	else if (FirstDate > SecondDate)
	{
		 return 1;
	}
	else
		return 0;
}
