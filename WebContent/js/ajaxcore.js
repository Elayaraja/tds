
var isSafari = false;
var isMoz = false;
var isIE = false;

if (navigator.userAgent.indexOf("Safari") > 0) {
	isSafari = true;
	isMoz = false;
	isIE = false;
}
else if (navigator.product == "Gecko") {

	isSafari = false;
	isMoz = true;
	isIE = false;
} else {
	isSafari = false;
	isMoz = false;
	isIE = true;
}



//GLOBAL FUNCTIONS


/* Functions to handle browser incompatibilites */
function eventElement(event) {
	if (isMoz) {
		return event.currentTarget;
	} else {
		return event.srcElement;
	}
}

function addKeyListener(element, listener) {
	if (isSafari)
		element.addEventListener("keydown",listener,false);
	else if (isMoz) {
		element.addEventListener("keypress",listener,false);
	} else {
		element.attachEvent("onkeydown",listener);
	}
}

function addListener(element, type, listener) {
	if (element.addEventListener) {
		element.addEventListener(type, listener, false);
	} else {
		element.attachEvent('on' + type, listener);
	}
}

function removeListener(element, type, listener) {
	if (element.removeEventListener) {
		element.removeEventListener(type, listener, false);
	} else {
		element.detachEvent('on' + type, listener);
	}
}

/* XML Helper functions */
function flatten(node) {
	if (node.nodeType == 1) {
		return '<' + node.nodeName + flattenAttributes(node) + '>' +
		flattenChildren(node.childNodes) + '</' + node.nodeName + '>';
	} else if(node.nodeType == 3) {
		return node.nodeValue;
	}
}

function flattenAttributes(node) {
	var buffer = '';
	for (var i=0;i<node.attributes.length;i++) {
		var attribute = node.attributes[i];
		buffer += ' '+ attribute.name + '="' + attribute.value + '"';
	}
	return buffer;
}

function flattenChildren(nodes) {
	var buffer = '';
	if (nodes.length > 0) {
		for (var i=0;i<nodes.length;i++) {
			buffer += flatten(nodes[i]);
		}
	}
	return buffer;
}

function copyAttributes(source, destination) {
	for (var i=0;i<source.attributes.length;i++) {
		var attribute = source.attributes[i];
		destination.setAttribute(attribute.name, attribute.value);
	}
	destination.className = source.getAttribute('class');
}

function getElementY(element){
	var targetTop = 0;
	if (element.offsetParent) {
		while (element.offsetParent) {
			targetTop += element.offsetTop;
			element = element.offsetParent;
		}
	} else if (element.y) {
		targetTop += element.y;
	}
	return targetTop;
}

function getElementX(element){
	var targetLeft = 0;
	if (element.offsetParent) {
		while (element.offsetParent) {
			targetLeft += element.offsetLeft;
			element = element.offsetParent;
		}
	} else if (element.x) {
		targetLeft += element.yx;
	}
	if(targetLeft>0){
		targetLeft = targetLeft-250;
	}
	return targetLeft;
}



/**
 * Returns true if an element has a specified class name
 */
function hasClass(node, className) {
	if (node.className == className) {
		return true;
	}
	var reg = new RegExp('(^| )'+ className +'($| )')
	if (reg.test(node.className)) {
		return true;
	}
	return false;
}

/**
 * Adds a class name to an element
 */
function addClass(node, className) {
	if (hasClass(node, className)) {
		return false;
	}
	node.className += ' '+ className;
	return true;
}

/**
 * Removes a class name from an element
 */
function removeClass(node, className) {
	if (!hasClass(node, className)) {
		return false;
	}
	node.className = eregReplace('(^| )'+ className +'($| )', '', node.className);
	return true;
}

/**
 * Emulate PHP's ereg_replace function in javascript
 */
function eregReplace(search, replace, subject) {
	return subject.replace(new RegExp(search,'g'), replace);
}




//LIVE UPDATE CORE

/*
  liveUpdater returns the live update function to use
  uriFunc: The function to generate the uri
  postFunc: <optional> Function to run after processing is complete
  preFunc: <optional> Function to run before processing starts
 */
function liveUpdater(uriFunc, handlerFunc, postFunc, preFunc, emptyFunc, errorFunc) {
	if (!handlerFunc) handlerFunc = function() {};
	if (!postFunc) postFunc = function () {};
	if (!preFunc) preFunc = function () {};
	if (!emptyFunc) emptyFunc = function () {};
	if (!errorFunc) errorFunc = function () {};

	return createLiveUpdaterFunction(uriFunc, handlerFunc, postFunc, preFunc, emptyFunc, errorFunc);
}

function createLiveUpdaterFunction(uriFunc, handlerFunc, postFunc, preFunc, emptyFunc, errorFunc) {
	var request = false;
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	}

	function update() {
		if(request && request.readyState < 4) 
			request.abort();

		if(!window.XMLHttpRequest)
			request = new ActiveXObject("Microsoft.XMLHTTP");

		preFunc();

		request.onreadystatechange = processRequestChange;

		request.open("GET", uriFunc());
		request.send(null);
		//alert(navigator.appName);
		if(navigator.appName=="Netscape") {
			setTimeout(request.onreadystatechange,200);
		} else {
			setTimeout(request.onreadystatechange,800);
		}
		return true;
	}

	function processRequestChange() {
		if(request.readyState == 4) {
			if (request.status == 200) {
				var xmlDoc = request.responseXML;

				if (xmlDoc.documentElement == null
						|| xmlDoc.firstChild.nodeName == "parsererror") {
					emptyFunc();


				} else {
					handlerFunc(xmlDoc);
					postFunc();
				}
			} else {
				errorFunc();
			}
		}
	}

	return update;
}



//AUTOCOMPLETE

function autocomplete(id, popupId, targetId, uri, paramName, postFunc, progressStyle, minimumCharacters) {
	//alert(popupId); 
	var inputField = document.getElementById(id);
	var popup = document.getElementById(popupId);
	var targetField = document.getElementById(targetId); 
	if (!postFunc) postFunc = function () {};
	if (!minimumCharacters) minimumCharacters = 1;
	var items = new Array();
	var current = 0;
	var originalPopupTop = popup.offsetTop;

	//alert(targetField+"     "+targetId);

	function constructUri() {
		if(inputField.value!="" ){
			if(id=="slandmark" ){
				if(document.getElementById("newLandMark1").checked==true){
					hidePopup();
					return;
				}
			}else if(id=="elandmark" ){
				if(document.getElementById("newLandMark2").checked==true){
					hidePopup();
					return;
				}
			}else if(id=="landMarkDash"){
				if(document.getElementById("newLandMark").checked==true){
					hidePopup();
					return;
				}
			}else if(id=="toLandMarkDash"){
				if(document.getElementById("newToLandMark").checked==true){
					hidePopup();
					return;
				}
			}
			var separator = "?";
			//alert(uri);
			if (uri.indexOf("?") >= 0)
				separator = "&";
			//alert(paramName.substring(0,5));   
			var urlback =  "";
			if(paramName.substring(0,5) === "phone" && document.getElementById(paramName.substring(0,5)).value.length > 0) {
				urlback = uri + separator + paramName + "=" + escape(inputField.value) + "&" + paramName.substring(0,5) + "=" + document.getElementById(paramName.substring(0,5)).value;
				//alert(urlback);
			}else {
				urlback = uri + separator + paramName + "=" + escape(inputField.value);
				//alert("in else"+urlback);
			}
			//alert(urlback);
			return urlback;
		}
		else{
			hidePopup();
		}
	}


//	--------- Start of Select Popup Function--------------------
	function hidePopup() {
		popup.style.visibility = 'hidden';

	}

	function handlePopupOver() {
		removeListener(inputField, 'blur', hidePopup);
	}

	function handlePopupOut() {
		if (popup.style.visibility == 'visible') {
			addListener(inputField, 'blur', hidePopup);
		}


	}
//	Mouse Click

	function handleClick(e) {
		if(id=="slandmark" ){
			if(document.getElementById("newLandMark1").checked==true){
				hidePopup();
				return;
			}
		}else if(id=="elandmark" ){
			if(document.getElementById("newLandMark2").checked==true){
				hidePopup();
				return;
			}
		}else if(id=="landMarkDash"){
			if(document.getElementById("newLandMark").checked==true){
				hidePopup();
				return;
			}
		}else if(id=="toLandMarkDash"){
			if(document.getElementById("newToLandMark").checked==true){
				hidePopup();
				return;
			}
		}
		items = popup.getElementsByTagName("li");
		if(items.value<1 ){
			addListener(inputField, "blur", clearInput);
			popup.style.visibility = 'hidden';
		}else if(inputField.value.length!=0 && inputField.value!=""){
			var tempstr="";
			var b = inputField.value;
			var temp = new Array();
			temp = b.split(',');
			var len=temp.length;


			for (var i=0;i<len;i++)
			{
				if (i!=len-1)
				{
					tempstr=tempstr+temp[i];
					tempstr=tempstr+",";
				}

			}
			if(targetField.id === inputField.id) {
				inputField.value= items[current].id; 
			} else {
				targetField.value = items[current].getAttribute("id");
				inputField.value= items[current].innerHTML; 
			}
			popup.style.visibility = 'hidden';
			//inputField.focus();
		}
		if(id=="landMarkDash"){
			getLandMark(1);
		}else if(id=="toLandMarkDash"){
			getLandMark(2);
		}else if(id=="slandmark"){
			landDetails();
			openLanddetails(1);
		}else if(id=="elandmark"){
			landDetails();
			openLanddetails(2);
		}else if(id=="slandmarkSmall"){
			landDetails();
			openLanddetails(3);
		}else if(id=="elandmarkSmall"){
			landDetails();
			openLanddetails(4);
		}else if(id=="acctDash"){
			voucherDetails(1);
		} else if(id=="acct"){
			commentsDetail();showComments(1);
		}
	}

	function handleOver(e) {
		items[current].className = '';
		current = eventElement(e).index;
		items[current].className = 'selected';

	}

	function handlerFunc(xmlDoc) {
		var root = xmlDoc.documentElement;
		if (root != null) {
			var items = root.childNodes;
			if(items[0].getAttribute("value")!=null){
				// Transform item tags to LI tags
				var ul = xmlDoc.createElement("ul");
				for (var i=0; i<items.length; i++) {
					var li = xmlDoc.createElement("li");
					var liIdAttr = xmlDoc.createAttribute("id");
					var liText = xmlDoc.createTextNode(items[i].firstChild.nodeValue);
					li.setAttribute("id", items[i].getAttribute("value"));
					li.appendChild(liText);
					ul.appendChild(li);
				}
			}
			// Remove item tags
			for (var j=items.length-1; j>=0; j--) {
				root.removeChild(root.childNodes[j]);
			}

			// Add UL tag
			root.appendChild(ul);

			// Set innerHTML for popup
			if(xmlDoc.documentElement.hasChildNodes()==true){
				document.getElementById(popupId).innerHTML = flattenChildren(root.childNodes);
			}else if(xmlDoc.documentElement.hasChildNodes()==false){
				hidePopup();


			}

		}

	}

	function post() {
		current = 0;
		items = popup.getElementsByTagName("li");
		if ((items.length > 1) || (items.length == 1 && items[0].innerHTML != inputField.value)) {
			setPopupStyles();
			for (var i = 0; i < items.length; i++) {
				items[i].index = i;
				addOptionHandlers(items[i]);
			}
			items[0].className = 'selected';
		} else {
			/*	addListener(inputField, "blur", clearInput);*/
			popup.style.visibility = 'hidden';



		}



		if (progressStyle != null) {
			removeClass(inputField, progressStyle);
		}

		postFunc();
		return null;

	}

	function clearInput(){
		if(popup.style.visibility == 'hidden' && ((items.length < 1 ))){
			inputField.value="";
			//inputField.focus();
		}
	}


	function pre() {
		if (progressStyle != null) {
			addClass(inputField, progressStyle);
		}
	}

	function setPopupStyles() {
		var maxHeight;
		if (isIE) {
			maxHeight = 200;
		} else {
			maxHeight = window.outerHeight/3;
		}
		if (popup.offsetHeight < maxHeight) {
			popup.style.overflow = 'hidden';
		} else if (isMoz) {
			popup.style.maxHeight = maxHeight + 'px';
			popup.style.overflow = '-moz-scrollbars-vertical';
		} else {
			popup.style.height = maxHeight + 'px';
			popup.style.overflowY = 'auto';
		}


		popup.scrollTop = 0;
		popup.style.visibility = 'visible';

		// Start playing
		if(id=="landMarkDash"){
			popup.style.top = "50"+"%";
			popup.style.left = "50"+"%";
			popup.style.width = document.getElementById(id).offsetWidth + "px";
		}else if(id=="slandmark"){
			popup.style.left = "362"+"px";
			popup.style.width = document.getElementById(id).offsetWidth + "px";
		}else if(id=="elandmark"){
			popup.style.top = "320"+"px";
			popup.style.left = "362"+"px";
			popup.style.width = document.getElementById(id).offsetWidth + "px";
		}else if(id=="acctDash" ){
			popup.style.top = "117"+"px";
			popup.style.left = "173"+"px";
			popup.style.width = document.getElementById(id).offsetWidth + "px";
		} else if(id=="acct"){
			popup.style.top = "180"+"px";
			popup.style.left = "253"+"px";
			popup.style.width = document.getElementById(id).offsetWidth + "px";
		}else if(id=="toLandMarkDash"){
			popup.style.top = "72"+"%";
			popup.style.left = "50"+"%";
			popup.style.width = document.getElementById(id).offsetWidth + "px";
		}else{
//			popup.style.top = (getElementY(document.getElementById(id))+document.getElementById(id).offsetHeight+2) + "px";
			popup.style.left = getElementX(document.getElementById(id)) + "px";
			popup.style.width = document.getElementById(id).offsetWidth + "px";
		}
	}
	function checkInputField(){
		if(inputField=="" || inputField.value.length==0){
			hidePopup();
			inputField.value="";
		}
		if(inputField!="" && inputField.value.length!=0){
			addListener(inputField, "blur", handleClick);
		}
	}

	function addOptionHandlers(option) {
		if(inputField.value=="" || inputField.value.length==0){
			hidePopup();
			inputField.value="";
		}else if(inputField.value!="" && inputField.value.length!=0){

			addListener(inputField, "blur", handleClick);
		}

		addListener(option, "click", handleClick);
		addListener(option, "mouseover", handleOver);

	}
	var updater = liveUpdater(constructUri, handlerFunc, post, pre);
	var timeout = false;

	function start(e) {
		if (timeout)
			window.clearTimeout(timeout);

		//up arrow
		if (e.keyCode == 38) {
			if (current > 0) {
				items[current].className = '';
				current--;
				items[current].className = 'selected';
				items[current].scrollIntoView(false);
			}

			//down arrow
		} else if (e.keyCode == 40) {
			if(current < items.length - 1) {
				items[current].className = '';
				current++;
				items[current].className = 'selected';
				items[current].scrollIntoView(false);
			}

			//enter or tab
		} else if (e.keyCode == 13  && popup.style.visibility == 'visible') {      

			var tempstr="";
			var b = inputField.value;
			var temp = new Array();
			temp = b.split(',');

			var len=temp.length;
			for (var i=0;i<len;i++)
			{
				if (i!=len-1)
				{
					tempstr=tempstr+temp[i];
					tempstr=tempstr+",";
				}

			}  
			//tempstr=tempstr+items[current].id;
			//inputField.value=tempstr;
			//inputField.value = items[current].id;
			//targetField.value = items[current].getAttribute("id");


			if(targetField.id === inputField.id) {
				inputField.value= items[current].id; 
			} else {
				targetField.value = items[current].getAttribute("id");
				inputField.value= items[current].innerHTML; 
			}
			popup.style.visibility = 'hidden';
			//inputField.focus();
			if (isIE) {
				event.returnValue = false;
			} else {
				e.preventDefault();
			}
			hidePopup();
			//escape
		} else if(e.keyCode == 9  )
		{
			hidePopup();
		}else if (e.keyCode == 27) {
			hidePopup();

			if (isIE) {
				event.returnValue = false;
			} else {
				e.preventDefault();
			}

			// Check for empty input field or not enough characters
		} else if (inputField.value.length < minimumCharacters) {
			// hide popup and return
			hidePopup();
		} else {
			timeout = window.setTimeout(updater, 300);
		}


	}
	addKeyListener(inputField, start);
	addListener(popup, 'mouseover', handlePopupOver);
	addListener(popup, 'mouseout', handlePopupOut);


}


