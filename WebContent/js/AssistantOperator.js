
function createAccountFn() {
	if(document.getElementById("userId").value!=""&&document.getElementById("password").value!=""){
	$('#createAccount').submit();
	$( ".loading" ).dialog({
	      resizable: false,
	      modal: true
	    });
	}
}
$(document).ready(function() {

	myLayout = $('body').layout({
		west__size : 400,
		east__size : 0
		// RESIZE Accordion widget when panes resize
		,
		west__onresize : $.layout.callbacks.resizePaneAccordions,
		east__onresize : $.layout.callbacks.resizePaneAccordions
	});

	// ACCORDION - in the West pane
	$("#accordion1").accordion({
		heightStyle : "fill"
	});

	// ACCORDION - in the East pane - in a 'content-div'
	$("#accordion2").accordion({
		heightStyle : "fill",
		active : 1
	});

	// THEME SWITCHER
	addThemeSwitcher('#themeSwitch', {
		top : '12px',
		right : '5px'
	});
	// if a new theme is applied, it could change the height of some content,
	// so call resizeAll to 'correct' any header/footer heights affected
	// NOTE: this is only necessary because we are changing CSS *AFTER LOADING* using themeSwitcher
	setTimeout(myLayout.resizeAll, 1000); /* allow time for browser to re-render with new theme */

});

$(function() {
	$("#tabs").tabs();
	// fix the classes

});
function verifySelectedVouchers(){
	$( ".loading" ).dialog({
		resizable: false,
		modal: true
	});
	var oRows1=document.getElementById("voucherDetailTb").getElementsByTagName("tr");
	var iRowCount1= oRows1.length;
	var transId="";
	for(var i=1;i<iRowCount1;i++){
		if(oRows1[i].getElementsByTagName("input")[0].checked){
			transId= transId + oRows1[i].getElementsByTagName("input")[0].value +";";
		}
	}
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = 'AssistantOperator?event=verifyVoucher&transId='+transId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	$( ".loading" ).dialog( "close" );
	document.getElementById("errorValue").innerHTML ="Voucher Verified !!";
}

function submitSearchForVouchers(){
	  $( ".loading" ).dialog({
	      resizable: false,
	      modal: true
	    });
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url='AssistantOperator?event=getVoucherDetails&fDateForVoucher='+document.getElementById("fDateForVoucher").value+'&tDateForVoucher='+document.getElementById("tDateForVoucher").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var table = document.getElementById("voucherDetailTb");
	table.innerHTML = "";
	var jobsJsonObj = "{\"vouchers\":"+text+"}" ;
	var obj = JSON.parse(jobsJsonObj.toString());
	var tbAppend = "<tr><th>TransId</th><th>Cost Center</th><th>Rider Name</th><th>VoucherNo</th><th>Amount</th><th>TripId</th><th>Driver ID</th><th>PR Date</th><th>Verified</th><th>DP Date</th></tr>";
	for(var i=0;i<obj.vouchers.length;i++){
		tbAppend +="<tr><td align='center'>"+obj.vouchers[i].TX+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].CC+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].RN+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].VNO+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].T+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].TI+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].DID+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].PRS+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].VER+"</td>";
		tbAppend +="<td align='center'>"+obj.vouchers[i].DPS+"</td>" ;
		var selected ="";
		if(parseInt(obj.vouchers[i].VER)>0){
			selected ="checked='checked'";
		}
		tbAppend +="<td align='center'><input type='checkbox' name='verify' id ='verify"+i+"' value='"+obj.vouchers[i].TX+"' "+selected+"></td></tr>";
	}
	table.innerHTML = tbAppend;
	$( ".loading" ).dialog( "close" );
}
$(function() {
	$( "button,input[type=button]" ).button().click(function( event ) {
		event.preventDefault();
	});
	$( "input[type=submit]" ).button().click(function( event ) {
		event.preventDefault();
		$('#masterForm').submit();
		$( ".loading" ).dialog({
			modal: true
		});
	});
	var $loading = $(".loading");
	var windowH = $(window).height();
	var windowW = $(window).width();

	$loading.css({
		position:"fixed",
		left: ((windowW - $loading.outerWidth())/2 + $(document).scrollLeft()),
		top: ((windowH - $loading.outerHeight())/2 + $(document).scrollTop())
	});
	$( "#menu" ).menu();
	var $menuID = $("#menu");
	$menuID.css({
		position:"fixed",
		left: ( (windowW-$menuID.outerWidth()) + $(document).scrollLeft()),
		top: ($menuID.outerHeight()/3+ $(document).scrollTop())
	});
});



$(function() {
	$( ".datePicker" ).datepicker();
}); 
function showMenu(){
	if( $('#menu').is(':visible') ) {
		$( "#menu" ).hide();
	}else{
		$( "#menu" ).show();
	}
}
function showLogs(row,tripId){
	  $( ".loading" ).dialog({
	      resizable: false,
	      modal: true
	    });
	var img=document.getElementById("imgReplace1_"+row).src;
	if(img.indexOf("plus_icons.png")!= -1){
		document.getElementById("imgReplace1_"+row).src="images/minus_icons.png";
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url='AssistantOperator?event=jobLogsFOrAssistant&tripId='+tripId;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text!=null ){
			$("#logs_"+row).show("slow");
			document.getElementById("logs_"+row).innerHTML=text;
		}
	}else{
		document.getElementById("imgReplace1_"+row).src="images/plus_icons.png";
		$("#logs_"+row).hide("slow");
	}
		 $( ".loading" ).dialog( "close" );
}
function jobHistory(){
	  $( ".loading" ).dialog({
	      resizable: false,
	      modal: true
	    });
	var xmlhttp = null;
	if (window.XMLHttpRequest){
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'AssistantOperator?event=jobHistoryFOrAssistant&startDate='+document.getElementById("startDate").value+"&endDate="+document.getElementById("endDate").value+'&value = '+document.getElementById("searchForHistory").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var responseText = xmlhttp.responseText;
	if(responseText=="lo"){
		window.open('control','_self');
	}
	var jobsJsonObj = "{\"address\":"+responseText+"}" ;
	var obj = JSON.parse(jobsJsonObj.toString());
	var driverOrCab;
	var dispatchBasedOn="2";
	if(dispatchBasedOn==2){
		driverOrCab = "Cab";	
	}else{
		driverOrCab="Driver";
	}
	var tableAppend=  document.getElementById("historyTable");
	//var divPlain = document.getElementById("plainJobs");
	var tableRows='<tr id="row" class="customerRow"> ';
	tableRows=tableRows+'<th class="showJobNo">#</th><th width="20px"class="showAddress">Address</th><th class="showAge">Age</th><th class="showDriverAllocate">'+driverOrCab+'</th><th></th><th class="showPhone">Ph</th><th class="showName">Name</th><th class="showStatus">St</th><th class="showStZone">S Z</th><th class="showEdZone">E Z</th><th class="showPtime">Time</th>';
	for(var i=0;i<obj.address.length;i++){
		var driverCab="";
		var addressToShow=obj.address[i].A.substring(0,25);
		if(dispatchBasedOn==2){
			driverCab= obj.address[i].V;
		}else{
			driverCab= obj.address[i].D;
		}
		var rowColor = "";

		var colorForVip ="";
		var j=Number(i)+1;
		tableRows=tableRows+' <tr id="row"   bgcolor="'+colorForVip+'"  class="customerRow">'; 
		if(obj.address[i].SR=="1"){
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+'<font color="red">S</font>)</td>';
		}else{
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+')</td>';
		} if(obj.address[i].LMN!=""){
			addressToShow=obj.address[i].LMN;
		} 
		tableRows=tableRows+'<td class="showAddress" width="180px"id="customerid" onclick="currentDetails(\''+obj.address[i].D+'\','+obj.address[i].T+','+obj.address[i].LA+','+obj.address[i].LO+','+obj.address[i].S+');mapInitJobs('+0+');mapPlotDriver(0)">'+addressToShow+'</td>';
		tableRows=tableRows+'<td class="showAge"><font color="green"><center>'+obj.address[i].PT+'m</font></td>';
		tableRows=tableRows+'<input type="hidden" name="queue_no" id="queue_no'+i+'" value='+obj.address[i].Q+'>';
		if(obj.address[i].N=="Flag Trip"){
			tableRows=tableRows+'<td class="showDriverAllocate" id="contactname"><input type="hidden" readonly="readonly"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[i].T+',\''+driverCab+'\',\''+obj.address[i].RT+'\')"  onkeypress="changeHappeningJob()" /></td>';		tableRows=tableRows+'<td><input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/></td>';
		}else{
			tableRows=tableRows+'<td  class="showDriverAllocate" id="contactname"><input type="hidden"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[i].T+',\''+driverCab+'\',\''+obj.address[i].RT+'\')"  onkeypress="changeHappeningJob('+i+')" /></td>';		tableRows=tableRows+'<td><input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/></td>';
		}
		tableRows=tableRows+'<input type="hidden"id="tripid'+i+'" value="'+obj.address[i].T+'"/>';
		tableRows=tableRows+'<td class="showTripId"  style="display:none"id="tripid">'+obj.address[i].T+'</td>';
		tableRows=tableRows+'<td class="showDriverId" style="display:none"id="driverId">'+obj.address[i].D+'</td>';
		tableRows=tableRows+'<td class="showPhone" style="whitespace:nowrap" id="phoneNoInJobs">'+obj.address[i].P+'</td>';
		tableRows=tableRows+'<td class="showName"style=""id="riderName">'+obj.address[i].N+'</td>';
		tableRows=tableRows+'<input type="hidden" id="lan'+i+'" value="'+obj.address[i].LA+'"/>';
		tableRows=tableRows+'<input type="hidden" id="lon'+i+'" value="'+obj.address[i].LO+'"/>';
		tableRows=tableRows+'<input type="hidden" id="city'+i+'" value="'+obj.address[i].A+'">';
		tableRows=tableRows+'<input type="hidden" name="serviceDate'+i+'" id="serviceDate'+i+'"value="'+obj.address[i].SD+'">';


		var status="";
		if(obj.address[i].S=="30"){status="Broadcast";
		}else if(obj.address[i].S=="40"){status="Allocated";
		}else if(obj.address[i].S=="43"){status="On Rotue To Pickup";
		}else if(obj.address[i].S=="50"){status="Customer Cancelled Request";
		}else if(obj.address[i].S=="51"){status="No Show";
		}else if(obj.address[i].S=="4"){status="Dispatching";
		}else if(obj.address[i].S=="0"){status="Unknown";
		}else if(obj.address[i].S=="47"){status="Trip Started";
		}else if(obj.address[i].S=="99"){status="Trip On Hold";
		}else if(obj.address[i].S=="3"){status="Couldnt Find Drivers";
		}else if(obj.address[i].S=="48"){status="Driver Reporting NoShow";
		}else if(obj.address[i].S=="49"){status="Soon To Clear";}
		
		tableRows=tableRows+'<input type="hidden" name="status'+i+'" id="status'+i+'" value="'+status+'">';
		tableRows=tableRows+'<input type="hidden" name="phone'+i+'" id="phone'+i+'" value='+obj.address[i].P+'>';
		tableRows=tableRows+'<input type="hidden" name="passname'+i+'" id="passname'+i+'" value='+obj.address[i].N+'>';
		tableRows=tableRows+'<input type="hidden" name="driver" id="driver" value='+obj.address[i].D+'>';
		tableRows=tableRows+'<input type="hidden" name="statusvalue" id="statusvalue" value='+obj.address[i].S+'>';
		tableRows=tableRows+'<input type="hidden"id="driverIdForDetails'+i+'" name="driverIdForDetails'+i+'"value='+obj.address[i].D+'>';
		tableRows=tableRows+'<input type="hidden"id="stZone'+i+'" name="stZone'+i+'"value='+obj.address[i].Q+'>';
		tableRows=tableRows+'<input type="hidden"id="edZone'+i+'" name="edZone'+i+'"value='+obj.address[i].EDQ+'>';
		tableRows=tableRows+'<input type="hidden"id="OrComments'+i+'" name="OrComments'+i+'"value='+obj.address[i].OC+'>';
		tableRows=tableRows+'<input type="hidden"id="OrSplIns'+i+'" name="OrSplIns'+i+'"value='+obj.address[i].SPL+'>';
		tableRows=tableRows+'<input type="hidden"id="ORPty'+i+'" name="ORPty'+i+'"value='+obj.address[i].PTY+'>';
		tableRows=tableRows+'<input type="hidden"id="OrAmt'+i+'" name="OrAmt'+i+'"value='+obj.address[i].AMT+'>';
		tableRows=tableRows+'<input type="hidden"id="vehiNo'+i+'" name="vehiNo'+i+'"value='+obj.address[i].V+'>';

		tableRows=tableRows+'<td class="showStatus">';
		if(obj.address[i].SR=="1"){
			if(obj.address[i].S== "40"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorGreen.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="4"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorYellow.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="3"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "30"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else{
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}
		}else{
			if(obj.address[i].S== "30"){
				tableRows=tableRows+'<img src="images/Dashboard/megaphone_red.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 25px;height: 21px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "40"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "43"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="50"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="51"){
				tableRows=tableRows+'<input type="button" class="jqModal"onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="4"){
				tableRows=tableRows+'<input type="button"class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="background-color: yellow;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="0"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="47"){
				tableRows=tableRows+'<img class="jqModal"src="images/Dashboard/greencar1.png" onclick="showDialog();showFullDetails('+i+')"style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="99"){
				tableRows=tableRows+'&nbsp;<img  class="jqModal" src="images/Dashboard/redphone.gif" onclick="showDialog();showFullDetails('+i+')"style="width: 25px;height: 16px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="3"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="48"){
				tableRows=tableRows+'&nbsp;<img class="jqModal" src="images/Dashboard/redphone.gif"onclick="showDialog();showFullDetails('+i+')" style="width: 21px;height: 16px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "45"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else{
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}
		}
		tableRows=tableRows+'</td>';
		tableRows=tableRows+'<td class="showStZone"style=""id="stZone">'+obj.address[i].Q+'</td>';
		tableRows=tableRows+'<td class="showEdZone"style=""id="edZone">'+obj.address[i].EDQ+'</td>';
		tableRows=tableRows+'<td class="showPtime"style=""id="pickupTime">'+obj.address[i].ST+'</td>';
//		tableRows=tableRows+'<td class="showSplFlags"style=""id="sSplFlags">'+obj.address[i].DP+'</td>';
		tableRows=tableRows+'<td><img src="images/plus_icons.png" id="imgReplace1_'+i+'" onclick="showLogs('+i+','+obj.address[i].T+')"</td>';

//		tableRows=tableRows+'<td class="showLandmark"style=""id="ssLandmark">'+obj.address[i].LMN+'</td>'	
		tableRows= tableRows+'<tr><td colspan="12" id="logs_'+i+'" style="width:100%;height:100%"></td></tr>';

	}
	tableAppend.innerHTML=tableRows;
	//divPlain.innerHTML =tableRows;
 $( ".loading" ).dialog( "close" );
}	



function jobDetails1() {
	 $( ".loading" ).dialog({
	      resizable: false,
	      modal: true
	    });
	var xmlhttp = null;
	if (window.XMLHttpRequest){
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'AssistantOperator?event=jobDetailsForAssistant';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var responseText = xmlhttp.responseText;
	if(responseText=="lo"){
		window.open('control','_self');
	}
	var jobsJsonObj = "{\"address\":"+responseText+"}" ;
	var obj = JSON.parse(jobsJsonObj.toString());
	var driverOrCab;
	var dispatchBasedOn="2";
	if(dispatchBasedOn==2){
		driverOrCab = "Cab";	
	}else{
		driverOrCab="Driver";
	}
	var tableAppend=  document.getElementById("customerTable");
	//var divPlain = document.getElementById("plainJobs");
	var tableRows='<tr id="row" class="customerRow"> ';
	tableRows=tableRows+'<th class="showJobNo">#</th><th width="20px"class="showAddress">Address</th><th class="showAge">Age</th><th class="showDriverAllocate">'+driverOrCab+'</th><th></th><th class="showPhone">Ph</th><th class="showName">Name</th><th class="showStatus">St</th><th class="showStZone">S Z</th><th class="showEdZone">E Z</th><th class="showPtime">Time</th>';
	for(var i=0;i<obj.address.length;i++){
		var driverCab="";
		var addressToShow=obj.address[i].A.substring(0,25);
		if(dispatchBasedOn==2){
			driverCab= obj.address[i].V;
		}else{
			driverCab= obj.address[i].D;
		}
		var rowColor = "";

		var colorForVip ="";
		var j=Number(i)+1;
		tableRows=tableRows+' <tr id="row"   bgcolor="'+colorForVip+'"  class="customerRow">'; 
		if(obj.address[i].SR=="1"){
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+'<font color="red">S</font>)</td>';
		}else{
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+')</td>';
		} if(obj.address[i].LMN!=""){
			addressToShow=obj.address[i].LMN;
		} 
		tableRows=tableRows+'<td class="showAddress" width="180px"id="customerid" onclick="currentDetails(\''+obj.address[i].D+'\','+obj.address[i].T+','+obj.address[i].LA+','+obj.address[i].LO+','+obj.address[i].S+');mapInitJobs('+0+');mapPlotDriver(0)">'+addressToShow+'</td>';
		tableRows=tableRows+'<td class="showAge"><font color="green"><center>'+obj.address[i].PT+'m</font></td>';
		tableRows=tableRows+'<input type="hidden" name="queue_no" id="queue_no'+i+'" value='+obj.address[i].Q+'>';
		if(obj.address[i].N=="Flag Trip"){
			tableRows=tableRows+'<td class="showDriverAllocate" id="contactname"><input type="hidden" readonly="readonly"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[i].T+',\''+driverCab+'\',\''+obj.address[i].RT+'\')"  onkeypress="changeHappeningJob()" /></td>';		tableRows=tableRows+'<td><input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/></td>';
		}else{
			tableRows=tableRows+'<td  class="showDriverAllocate" id="contactname"><input type="hidden"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[i].T+',\''+driverCab+'\',\''+obj.address[i].RT+'\')"  onkeypress="changeHappeningJob('+i+')" /></td>';		tableRows=tableRows+'<td><input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/></td>';
		}
		tableRows=tableRows+'<input type="hidden"id="tripid'+i+'" value="'+obj.address[i].T+'"/>';
		tableRows=tableRows+'<td class="showTripId"  style="display:none"id="tripid">'+obj.address[i].T+'</td>';
		tableRows=tableRows+'<td class="showDriverId" style="display:none"id="driverId">'+obj.address[i].D+'</td>';
		tableRows=tableRows+'<td class="showPhone" style="whitespace:nowrap" id="phoneNoInJobs">'+obj.address[i].P+'</td>';
		tableRows=tableRows+'<td class="showName"style=""id="riderName">'+obj.address[i].N+'</td>';
		tableRows=tableRows+'<input type="hidden" id="lan'+i+'" value="'+obj.address[i].LA+'"/>';
		tableRows=tableRows+'<input type="hidden" id="lon'+i+'" value="'+obj.address[i].LO+'"/>';
		tableRows=tableRows+'<input type="hidden" id="city'+i+'" value="'+obj.address[i].A+'">';
		tableRows=tableRows+'<input type="hidden" name="serviceDate'+i+'" id="serviceDate'+i+'"value="'+obj.address[i].SD+'">';


		var status="";
		if(obj.address[i].S=="30"){status="Broadcast";
		}else if(obj.address[i].S=="40"){status="Allocated";
		}else if(obj.address[i].S=="43"){status="On Rotue To Pickup";
		}else if(obj.address[i].S=="50"){status="Customer Cancelled Request";
		}else if(obj.address[i].S=="51"){status="No Show";
		}else if(obj.address[i].S=="4"){status="Dispatching";
		}else if(obj.address[i].S=="0"){status="Unknown";
		}else if(obj.address[i].S=="47"){status="Trip Started";
		}else if(obj.address[i].S=="99"){status="Trip On Hold";
		}else if(obj.address[i].S=="3"){status="Couldnt Find Drivers";
		}else if(obj.address[i].S=="49"){status="Soon To Clear";
		}else if(obj.address[i].S=="48"){status="Driver Reporting NoShow";}
		tableRows=tableRows+'<input type="hidden" name="status'+i+'" id="status'+i+'" value="'+status+'">';
		tableRows=tableRows+'<input type="hidden" name="phone'+i+'" id="phone'+i+'" value='+obj.address[i].P+'>';
		tableRows=tableRows+'<input type="hidden" name="passname'+i+'" id="passname'+i+'" value='+obj.address[i].N+'>';
		tableRows=tableRows+'<input type="hidden" name="driver" id="driver" value='+obj.address[i].D+'>';
		tableRows=tableRows+'<input type="hidden" name="statusvalue" id="statusvalue" value='+obj.address[i].S+'>';
		tableRows=tableRows+'<input type="hidden"id="driverIdForDetails'+i+'" name="driverIdForDetails'+i+'"value='+obj.address[i].D+'>';
		tableRows=tableRows+'<input type="hidden"id="stZone'+i+'" name="stZone'+i+'"value='+obj.address[i].Q+'>';
		tableRows=tableRows+'<input type="hidden"id="edZone'+i+'" name="edZone'+i+'"value='+obj.address[i].EDQ+'>';
		tableRows=tableRows+'<input type="hidden"id="OrComments'+i+'" name="OrComments'+i+'"value='+obj.address[i].OC+'>';
		tableRows=tableRows+'<input type="hidden"id="OrSplIns'+i+'" name="OrSplIns'+i+'"value='+obj.address[i].SPL+'>';
		tableRows=tableRows+'<input type="hidden"id="ORPty'+i+'" name="ORPty'+i+'"value='+obj.address[i].PTY+'>';
		tableRows=tableRows+'<input type="hidden"id="OrAmt'+i+'" name="OrAmt'+i+'"value='+obj.address[i].AMT+'>';
		tableRows=tableRows+'<input type="hidden"id="vehiNo'+i+'" name="vehiNo'+i+'"value='+obj.address[i].V+'>';

		tableRows=tableRows+'<td class="showStatus">';
		if(obj.address[i].SR=="1"){
			if(obj.address[i].S== "40"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorGreen.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="4"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorYellow.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="3"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "30"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else{
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}
		}else{
			if(obj.address[i].S== "30"){
				tableRows=tableRows+'<img src="images/Dashboard/megaphone_red.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 25px;height: 21px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "40"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "43"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="50"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="51"){
				tableRows=tableRows+'<input type="button" class="jqModal"onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="4"){
				tableRows=tableRows+'<input type="button"class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="background-color: yellow;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="0"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="47"){
				tableRows=tableRows+'<img class="jqModal"src="images/Dashboard/greencar1.png" onclick="showDialog();showFullDetails('+i+')"style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="99"){
				tableRows=tableRows+'&nbsp;<img  class="jqModal" src="images/Dashboard/redphone.gif" onclick="showDialog();showFullDetails('+i+')"style="width: 25px;height: 16px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="3"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="48"){
				tableRows=tableRows+'&nbsp;<img class="jqModal" src="images/Dashboard/redphone.gif"onclick="showDialog();showFullDetails('+i+')" style="width: 21px;height: 16px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "45"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else{
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}
		}
		tableRows=tableRows+'</td>';
		tableRows=tableRows+'<td class="showStZone"style=""id="stZone">'+obj.address[i].Q+'</td>';
		tableRows=tableRows+'<td class="showEdZone"style=""id="edZone">'+obj.address[i].EDQ+'</td>';
		tableRows=tableRows+'<td class="showPtime"style=""id="pickupTime">'+obj.address[i].ST+'</td>';
//		tableRows=tableRows+'<td class="showSplFlags"style=""id="sSplFlags">'+obj.address[i].DP+'</td>';
//		tableRows=tableRows+'<td class="showLandmark"style=""id="ssLandmark">'+obj.address[i].LMN+'</td>'	
	}
	tableAppend.innerHTML=tableRows;
	//divPlain.innerHTML =tableRows;
	 $( ".loading" ).dialog( "close" );

}


function jobReservations() {
	 $( ".loading" ).dialog({
	      resizable: false,
	      modal: true
	    });
	var xmlhttp = null;
	if (window.XMLHttpRequest){
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'AssistantOperator?event=jobReservations&fDate='+document.getElementById("fDate").value+"&tDate="+document.getElementById("tDate").value+"&value="+document.getElementById("searchForReserve").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var responseText = xmlhttp.responseText;
	if(responseText=="lo"){
		window.open('control','_self');
	}
	var jobsJsonObj = "{\"address\":"+responseText+"}" ;
	var obj = JSON.parse(jobsJsonObj.toString());
	var driverOrCab;
	var dispatchBasedOn="2";
	if(dispatchBasedOn==2){
		driverOrCab = "Cab";	
	}else{
		driverOrCab="Driver";
	}
	var tableAppend=  document.getElementById("reservationsTbl");
	//var divPlain = document.getElementById("plainJobs");
	var tableRows='<tr id="row" class="customerRow"> ';
	tableRows=tableRows+'<th class="showJobNo">#</th><th width="20px"class="showAddress">Address</th><th class="showAge">Age</th><th class="showDriverAllocate">'+driverOrCab+'</th><th></th><th class="showPhone">Ph</th><th class="showName">Name</th><th class="showStatus">St</th><th class="showStZone">S Z</th><th class="showEdZone">E Z</th><th class="showPtime">Time</th>';
	for(var i=0;i<obj.address.length;i++){
		var driverCab="";
		var addressToShow=obj.address[i].A.substring(0,25);
		if(dispatchBasedOn==2){
			driverCab= obj.address[i].V;
		}else{
			driverCab= obj.address[i].D;
		}
		var rowColor = "";

		var colorForVip ="";
		var j=Number(i)+1;
		tableRows=tableRows+' <tr id="row"   bgcolor="'+colorForVip+'"  class="customerRow">'; 
		if(obj.address[i].SR=="1"){
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+'<font color="red">S</font>)</td>';
		}else{
			tableRows=tableRows+'<td id="jobNo" class="showJobNo" style="color:'+rowColor+'" >'+j+')</td>';
		} if(obj.address[i].LMN!=""){
			addressToShow=obj.address[i].LMN;
		} 
		tableRows=tableRows+'<td class="showAddress" width="180px"id="customerid" onclick="currentDetails(\''+obj.address[i].D+'\','+obj.address[i].T+','+obj.address[i].LA+','+obj.address[i].LO+','+obj.address[i].S+');mapInitJobs('+0+');mapPlotDriver(0)">'+addressToShow+'</td>';
		tableRows=tableRows+'<td class="showAge"><font color="green"><center>'+obj.address[i].PT+'m</font></td>';
		tableRows=tableRows+'<input type="hidden" name="queue_no" id="queue_no'+i+'" value='+obj.address[i].Q+'>';
		if(obj.address[i].N=="Flag Trip"){
			tableRows=tableRows+'<td class="showDriverAllocate" id="contactname"><input type="hidden" readonly="readonly"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[i].T+',\''+driverCab+'\',\''+obj.address[i].RT+'\')"  onkeypress="changeHappeningJob()" /></td>';		tableRows=tableRows+'<td><input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/></td>';
		}else{
			tableRows=tableRows+'<td  class="showDriverAllocate" id="contactname"><input type="hidden"  width="5px" id="driver_id'+i+'"   value="'+driverCab+'" class="driver-txt"  onblur="updateDriver1('+i+','+obj.address[i].T+',\''+driverCab+'\',\''+obj.address[i].RT+'\')"  onkeypress="changeHappeningJob('+i+')" /></td>';		tableRows=tableRows+'<td><input type="hidden" id="pickup'+i+'" name="pickup'+i+'" width="5px" value="'+obj.address[i].ST+'" class="time-txt" onblur="updateTime('+i+','+obj.address[i].T+',123,'+driverCab+')"  onkeypress="changeHappeningJob()"/></td>';
		}
		tableRows=tableRows+'<input type="hidden"id="tripid'+i+'" value="'+obj.address[i].T+'"/>';
		tableRows=tableRows+'<td class="showTripId"  style="display:none"id="tripid">'+obj.address[i].T+'</td>';
		tableRows=tableRows+'<td class="showDriverId" style="display:none"id="driverId">'+obj.address[i].D+'</td>';
		tableRows=tableRows+'<td class="showPhone" style="whitespace:nowrap" id="phoneNoInJobs">'+obj.address[i].P+'</td>';
		tableRows=tableRows+'<td class="showName"style=""id="riderName">'+obj.address[i].N+'</td>';
		tableRows=tableRows+'<input type="hidden" id="lan'+i+'" value="'+obj.address[i].LA+'"/>';
		tableRows=tableRows+'<input type="hidden" id="lon'+i+'" value="'+obj.address[i].LO+'"/>';
		tableRows=tableRows+'<input type="hidden" id="city'+i+'" value="'+obj.address[i].A+'">';
		tableRows=tableRows+'<input type="hidden" name="serviceDate'+i+'" id="serviceDate'+i+'"value="'+obj.address[i].SD+'">';


		var status="";
		if(obj.address[i].S=="30"){status="Broadcast";
		}else if(obj.address[i].S=="40"){status="Allocated";
		}else if(obj.address[i].S=="43"){status="On Rotue To Pickup";
		}else if(obj.address[i].S=="50"){status="Customer Cancelled Request";
		}else if(obj.address[i].S=="51"){status="No Show";
		}else if(obj.address[i].S=="4"){status="Dispatching";
		}else if(obj.address[i].S=="0"){status="Unknown";
		}else if(obj.address[i].S=="47"){status="Trip Started";
		}else if(obj.address[i].S=="99"){status="Trip On Hold";
		}else if(obj.address[i].S=="3"){status="Couldnt Find Drivers";
		}else if(obj.address[i].S=="49"){status="Soon To Clear";
		}else if(obj.address[i].S=="48"){status="Driver Reporting NoShow";}
		tableRows=tableRows+'<input type="hidden" name="status'+i+'" id="status'+i+'" value="'+status+'">';
		tableRows=tableRows+'<input type="hidden" name="phone'+i+'" id="phone'+i+'" value='+obj.address[i].P+'>';
		tableRows=tableRows+'<input type="hidden" name="passname'+i+'" id="passname'+i+'" value='+obj.address[i].N+'>';
		tableRows=tableRows+'<input type="hidden" name="driver" id="driver" value='+obj.address[i].D+'>';
		tableRows=tableRows+'<input type="hidden" name="statusvalue" id="statusvalue" value='+obj.address[i].S+'>';
		tableRows=tableRows+'<input type="hidden"id="driverIdForDetails'+i+'" name="driverIdForDetails'+i+'"value='+obj.address[i].D+'>';
		tableRows=tableRows+'<input type="hidden"id="stZone'+i+'" name="stZone'+i+'"value='+obj.address[i].Q+'>';
		tableRows=tableRows+'<input type="hidden"id="edZone'+i+'" name="edZone'+i+'"value='+obj.address[i].EDQ+'>';
		tableRows=tableRows+'<input type="hidden"id="OrComments'+i+'" name="OrComments'+i+'"value='+obj.address[i].OC+'>';
		tableRows=tableRows+'<input type="hidden"id="OrSplIns'+i+'" name="OrSplIns'+i+'"value='+obj.address[i].SPL+'>';
		tableRows=tableRows+'<input type="hidden"id="ORPty'+i+'" name="ORPty'+i+'"value='+obj.address[i].PTY+'>';
		tableRows=tableRows+'<input type="hidden"id="OrAmt'+i+'" name="OrAmt'+i+'"value='+obj.address[i].AMT+'>';
		tableRows=tableRows+'<input type="hidden"id="vehiNo'+i+'" name="vehiNo'+i+'"value='+obj.address[i].V+'>';

		tableRows=tableRows+'<td class="showStatus">';
		if(obj.address[i].SR=="1"){
			if(obj.address[i].S== "40"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorGreen.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="4"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorYellow.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="3"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "30"){
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}else{
				tableRows=tableRows+'<img src="images/Dashboard/operatorRed.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 28px;height: 26px;cursor: pointer;"/><br/>';
			}
		}else{
			if(obj.address[i].S== "30"){
				tableRows=tableRows+'<img src="images/Dashboard/megaphone_red.png"  class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="width: 25px;height: 21px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "40"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "43"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="50"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="51"){
				tableRows=tableRows+'<input type="button" class="jqModal"onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="4"){
				tableRows=tableRows+'<input type="button"class="jqModal" onclick="showDialog();showFullDetails('+i+')"style="background-color: yellow;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="0"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="47"){
				tableRows=tableRows+'<img class="jqModal"src="images/Dashboard/greencar1.png" onclick="showDialog();showFullDetails('+i+')"style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="99"){
				tableRows=tableRows+'&nbsp;<img  class="jqModal" src="images/Dashboard/redphone.gif" onclick="showDialog();showFullDetails('+i+')"style="width: 25px;height: 16px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="3"){
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S=="48"){
				tableRows=tableRows+'&nbsp;<img class="jqModal" src="images/Dashboard/redphone.gif"onclick="showDialog();showFullDetails('+i+')" style="width: 21px;height: 16px;cursor: pointer;"/><br/>';
			}else if(obj.address[i].S== "45"){
				tableRows=tableRows+'<img src="images/Dashboard/greencar1.png" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="width: 30px;height: 20px;cursor: pointer;"/><br/>';
			}else{
				tableRows=tableRows+'<input type="button" class="jqModal" onclick="showDialog();showFullDetails('+i+')" style="background-color: #FF0000;width:30px;cursor: pointer;"/><br/>';
			}
		}
		tableRows=tableRows+'</td>';
		tableRows=tableRows+'<td class="showStZone"style=""id="stZone">'+obj.address[i].Q+'</td>';
		tableRows=tableRows+'<td class="showEdZone"style=""id="edZone">'+obj.address[i].EDQ+'</td>';
		tableRows=tableRows+'<td class="showPtime"style=""id="pickupTime">'+obj.address[i].ST+'</td>';
//		tableRows=tableRows+'<td class="showSplFlags"style=""id="sSplFlags">'+obj.address[i].DP+'</td>';
//		tableRows=tableRows+'<td class="showLandmark"style=""id="ssLandmark">'+obj.address[i].LMN+'</td>'	
	}
	tableAppend.innerHTML=tableRows;
	//divPlain.innerHTML =tableRows;
	 $( ".loading" ).dialog( "close" );

}

$(document).ready ( function(){
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
	var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
	var hrsmin = hors+""+min;  

	document.masterForm.sdate.value = cMonth+"/"+cDate+"/"+cYear;
	document.masterForm.dateTemp.value = cMonth+"/"+cDate+"/"+cYear;
	document.masterForm.shrs.value  = ""; 
	document.masterForm.timeTemp.value=hrsmin;
	initializeMapHTML('1');
}); 



 function closeFunc(){
	window.location.href="/TDS/control";
 }

 function validationFields(){
 	if(document.getElementById("phone").value!=""&&document.getElementById("name").value!=""&&document.getElementById("shrs").value!=""&&document.getElementById("eMail").value!=""&&document.getElementById("sadd1").value!=""){
 		if(document.getElementById("compCode").value!=""){
 			if(document.getElementById("").value==""){
 				document.getElementById("payType").value="Cash";
 			}
	 		$('#masterForm').submit();
	 		$( ".loading" ).dialog({
				      modal: true
				    });
 		} else {
 			alert("You dont have access to create job for any company");
 		}
 	}else{
 		alert("Please fill All mandatory Fields !!!");
 	}
 }
 $(function() {
	    $( ".loading" ).dialog({
	      resizable: false,
	      modal: true
	    });
	  });
 $(window).bind("load", function() {
	   // code here
	 $( ".loading" ).dialog( "close" );
	});
 function registerUser(){
		$("#tabs").tabs("select", "#tabs-7"); 
	}
	function createUser(){
		$( ".loading" ).dialog({
			modal: true
		});
		var Name = document.getElementById("regName").value;
		var uId =  document.getElementById("regUserId").value;	
		var pass =  document.getElementById("regPass").value;
		var email =  document.getElementById("regEmail").value;
		var addr =  document.getElementById("regAddress").value;
		var city =  document.getElementById("regCity").value;
		var zip =  document.getElementById("regZip").value;
		var phNo =  document.getElementById("regPhNo").value;
		var timeZone = document.getElementById("timeZone").value;

		var responseText =0;
		var xmlhttp = null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		if(Name!=""&&uId!=""&&pass!=""&&email!=""&&addr!=""&&city!=""&&zip!=""&&phNo!=""){
			var url = "AssistantOperator?event=registerUser&name="+Name+"&uId="+uId+"&pass="+pass+"&email="+email+"&addr="+addr+"&city="+city+"&zip="+zip+"&phno="+phNo+"&timeZone="+timeZone;			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			 responseText = xmlhttp.responseText;
		}else{
			alert("Please Fill All the Values !!");
		}
		if(parseInt(responseText)>0){
			document.getElementById("errorValue").innerHTML ="Registered Successfully,Please Login to Continue !!";
			$("#tabs").tabs("select", "#tabs-1"); 
			document.getElementById("regName").value="";
			document.getElementById("regUserId").value="";	
			document.getElementById("regPass").value="";
			document.getElementById("regEmail").value="";
			document.getElementById("regAddress").value="";
			document.getElementById("regCity").value="";
			document.getElementById("regZip").value="";
			document.getElementById("regPhNo").value="";
		}
		$( ".loading" ).dialog( "close" );
	}
	function checkSubmit(e)
	{
	   if(e && e.keyCode == 13)
	   {
	     // document.forms[0].submit();
		   createAccountFn();
	   }
	}
	function clearOpenRequest(){
		document.getElementById("phone").value="";
		document.getElementById("name").value="";
		document.getElementById("shrs").value="Now";
		document.getElementById("eMail").value="";
		document.getElementById("sadd1").value="";
		document.getElementById("sadd2").value="";
		document.getElementById("scity").value="";
		document.getElementById("sstate").value="";
		document.getElementById("sLatitude").value="";
		document.getElementById("sLongitude").value="";
		document.getElementById("eLatitude").value="";
		document.getElementById("eLongitude").value="";
		document.getElementById("eadd1").value="";
		document.getElementById("eadd2").value="";
		document.getElementById("ecity").value="";
		document.getElementById("estate").value="";
		document.getElementById("ezip").value="";
		document.getElementById("szip").value="";
		document.getElementById("Comments1").value="";
		document.getElementById("specialIns1").value="";
		document.getElementById("flightInfo").value="";
		document.getElementById("payType").value="Cash";
		document.getElementById("voucherNum").value="";
		document.getElementById("ccNum").value="";
		document.getElementById("ccName").value="";
		document.getElementById("expDate").value="";
		document.getElementById("cvvNum").value="";
		document.getElementById("ccAmount").value="";
		document.getElementById("billAdd").value="";
	}
	$(document).ready(function() {
		var geocoder = new google.maps.Geocoder();
		$("#regAddress").autocomplete({
			//This bit uses the geocoder to fetch address values
			source: function(request, response) {
				var defaultState = document.getElementById("sstate").value;
				var defaultCountry=document.getElementById("defaultCountry").value;

				geocoder.geocode( {'address': request.term +', '+defaultState+', ' + defaultCountry  }, function(results, status) {
					response($.map(results, function(item) {
						return {
							label: item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							city: item.postal_code,
							addcomp: item.address_components
							//nhd: item.address_components_of_type("neighborhood")
						}
					}));
				});			

			},
			//This bit is executed upon selection of an address
			select: function(event, ui) {

				var arrAddress =ui.item.addcomp;
				var streetnum= "";
				var route = "";
				$("#regAddress").val("");
				// iterate through address_component array
				$.each(arrAddress, function (i, address_component) {
				
					if (address_component.types[0] == "street_number"){
						//$("#regAddress").val(address_component.long_name);
						streetnum = address_component.long_name;
					}

					if (address_component.types[0] == "locality"){
						$("#regCity").val(address_component.long_name);
					}

					if (address_component.types[0] == "route"){ 
						route = address_component.long_name;
					}
					if (address_component.types[0] == "country"){ 
						itemCountry = address_component.long_name;
					}


					if (address_component.types[0] == "postal_code"){ 
						itemPc = address_component.long_name;
							$("#regZip").val(address_component.long_name);
					}
					//return false; // break the loop

					});
				//alert(streetnum+":"+ route);
				//document.getElementById("regAddress").value="";
				document.getElementById("tempAdd").value=streetnum+" "+route;

			}

		});

	});
	function appendAddress(){
		document.getElementById('regAddress').value=document.getElementById('tempAdd').value;
	}
