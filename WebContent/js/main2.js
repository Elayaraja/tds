/*

  This Javascript is developed by Amshuhu Team
  Author: A.Vimal Bharathi

*/




var activeSection="1";
var activeSearchPane="hig"; // variable for search pane
var activeHiGCommunity="results";  // variable for community pane

var roverStatus=1;
var findPeopleStatus=0;
var webStatus=0;
var symptomStatus=0;

var matched="";
var regExp = "";
var matched_results="";
var vector_type="";


var nVector1 = "", nVector2 = "", nVector3 = "";  // Not a vector values ie. not in taxonomy.xml
var value1="",value2="",value3="";
var value_1="",value_2="",value_3="";
var prevVal1="",prevVal2="",prevVal3="";
var prevect1 = "", prevect2 = "", prevect3 = "";  // Variable for previous vector value

var xmlDoc,xmlDoc_C;  // Variables for xml read data
var CWPulldownsLoaded = false;	
var addsMessageLoaded = false;
//Keyword variable

var keywords_list1=new Array();
var keywords_list2=new Array();
var keywords_list3=new Array();

// Vector Values

var valueArray;
var value1="",value2="",value3="";
var value_1="",value_2="",value_3="";
var prevVal1="",prevVal2="",prevVal3="";

var totalArray=new Array();
totalArray[0]=value1+","+value2+","+value3;
var forwardArray=new Array();
var tempKey="";
var key="";
var p="";
var q="";

//Start Variables for getting xml messages
var addsXml;
var messageid = new Array();
var messagevalue = new Array();
var rover_value="";

// End Variables for gettting xml messages.

var j=0;var k=0; var l=0;

var switchwindowpanevalue = "";

var v1="Causes & Conditions";
//var v1="cancer";  // 
var v2="Treatments & Tonics";
var v3="Plans & Providers";
var receivedValue="";

window.onbeforeunload = confirmExit;
var needToConfirm = true;

/*---------------------------------------------------------------------------------------------------------*/
function confirmExit() {
	if(needToConfirm){
		return "MAKE SURE YOU CLICK ON CANCEL TO STAY IN THE CURRENT PANE";
	}
}
/*---------------------------------------------------------------------------------------------------------*/
function openPopupWindow(url) {
	//alert(url);
	var mywin = window.open(url,"_blank",'width=1004,height=635,menubar=0,status=no,location=no,toolbar=0,scrollbar=0,resizable=0');
	//mywin.moveTo(120,120);
}
/*---------------------------------------------------------------------------------------------------------*/
function closepop() {
	document.getElementById('divpopup').style.display = "none";
} 
/*---------------------------------------------------------------------------------------------------------*/
function openpop() {
	
	document.getElementById('divpopup').style.display = "block";							
}

/*---------------------------------------------------------------------------------------------------------*/
function openframepop(url) {
	document.getElementById('fpagepopup').src=url;
	document.getElementById('divpagepopup').style.display = "block";
}

function closeframepop() {
	document.getElementById('fpagepopup').src="";
	document.getElementById('divpagepopup').style.display = "none";
}
/*---------------------------------------------------------------------------------------------------------*/
function openhelpframepop(url) {
	document.getElementById('fhelppopup').src=url;
	document.getElementById('divhelppopup').style.display = "block";
}

function closehelpframepop() {
	document.getElementById('fhelppopup').src="";
	document.getElementById('divhelppopup').style.display = "none";
}

/*---------------------------------------------------------------------------------------------------------*/
function deactivateAllPanes() {
	
	document.getElementById("Image1").style.display="none";
	document.getElementById("Image2").style.display="none";
	document.getElementById("Image3").style.display="none";
	document.getElementById("findpeople").style.display="block";
	document.getElementById("web").style.display="block";
	document.getElementById("symptoms").style.display="block";
}

/*---------------------------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------------------------*/
function openInSameWindow_BS() {
	needToConfirm = false;
//	window.location.href='http://hig.sharewik.com/SharewikLite/index.jsp?q='+document.getElementById('w_search1').value;
	window.location.href='http://hig.sharewik.com/SharewikLite';
}
/*---------------------------------------------------------------------------------------------------------*/


function startupLoad() {
	
	
	var os = navigator.platform;
	
	if(!CWPulldownsLoaded) {
		loadCWPulldowns();
	}
	if(!addsMessageLoaded)  {
		loadmessages();
	}
	MM_preloadImages('images/find-people-on.jpg','images/web-search-on.jpg','images/symptoms-on.jpg');
	setTimeout("getMessages()",500);
	
	//Added for default value setting for google adsense approval
	//document.getElementById('w_search1').value="Endocrine / Diabetes";
	
	//if(document.getElementById('search_term').value!="")
	//{
		
		activateWidePulldowns();
		changeForm();
				
		/* Added on May 27 2010 
		 * If the URL comes up with a query string "q".then we should 
		 * change the search term with the information comes with the 
		 * query string "q".Here we store the query string value in a hidden
		 * input box with the id "search_term".
		 * */
		//value_1="Cancer";
		//value1="Cancer";
		//v1="Cancer";
		
		receivedValue = document.getElementById('search_term').value;
		
	
		//Replace mismatch between taxonomy mismatch between Basic Search and 3DSearch
		receivedValue = receivedValue.replace(/Endocrine::/g,"Endocrine / Diabetes::");
	
			
		//value_1 = document.getElementById('search_term').value;
		//value1= document.getElementById('search_term').value;
		//v1= document.getElementById('search_term').value;
		
		//alert("Inputvalue:"+receivedValue);
		/*This function is to set the default src of community.jsp to "/RoverDataService/getUrls.do?v1"
		 This will enable the Search Engine Results from RoverDataService as default When we click "Find People" Tab. */
		 
		//alert("Size of List 1 Array is "+keywords_list1.length);
			for(var inc=0;inc<keywords_list1.length;inc++)
			{
				matched = new String(keywords_list1[inc]).toLowerCase();
				regExp = new String(receivedValue).toLowerCase();
				if(matched==regExp)
				{
					//alert("Inputvalue1:"+receivedValue);
					
					vector_type=1;
					value_1=receivedValue;
					value1=receivedValue;
					v1=receivedValue;
					store(1,receivedValue);
					break;
				}
			}
		
		
		if(vector_type=="")
		{
			for(var inc=0;inc<keywords_list2.length;inc++)
			{
				matched = new String(keywords_list2[inc]).toLowerCase();
				regExp = new String(receivedValue).toLowerCase();
				if(matched==regExp)
				{
					vector_type=2;
					value_2=receivedValue;
					value2=receivedValue;
					v2=receivedValue;
					store(2,receivedValue);
					break;
				}
			}
		}
		if(vector_type=="")
		{
			for(var inc=0;inc<keywords_list3.length;inc++)
			{
				matched = new String(keywords_list3[inc]).toLowerCase();
				regExp = new String(receivedValue).toLowerCase();
				if(matched==regExp)
				{
					vector_type=3;
					value_3=receivedValue;
					value3=receivedValue;
					v3=receivedValue;
					store(3,receivedValue);
					break;
				}
			}
		}
		if(vector_type=="")
		{
			vector_type="";
		}
		//Here we find the vector under which the query string is matched
			if(vector_type==1)
			{
				rover_value = "Causes & Conditions::"+receivedValue;
				document.getElementById('c_search1').value=receivedValue;
			}
			else if(vector_type==2)
			{
				rover_value = "Treatments & Tonics::"+receivedValue;
				document.getElementById('c_search2').value=receivedValue;
				document.getElementById('c_search1').value="CAUSES AND CONDITIONS";
			}
			else if(vector_type==3)
			{
				rover_value = "Plans & Providers::"+receivedValue;
				document.getElementById('c_search3').value=receivedValue;
				document.getElementById('c_search1').value="CAUSES AND CONDITIONS";
			}
			else
			{
				//If the search term is not found under any of the vector,then make Causes and Conditions as the default one
				//rover_value = "Causes & Conditions::"+value_1;
				//alert("Inside Null ");
				document.getElementById('c_search1').value=receivedValue;
				if(receivedValue=="")
				{
					document.getElementById('c_search1').value="CAUSES AND CONDITIONS";
					rover_value ="";
				}else
				{
					value_1=receivedValue;
					value1=receivedValue;
					v1=receivedValue;
					nVector1=receivedValue;
				}
			}
		
		
		searchWeb(value_1,value_2,value_3,'hig');
		higCommunity(value_1,value_2,value_3,'results');
		searchSymptoms(value_1,value_2,value_3,'healthline');
		
		//document.getElementById('c_search1').value=value_1;
		/*We must concate the query string with "Casuses & Conditions::".Then only the rover can 
		get to know about from which vector the query string has come*/
		//value_1 = "Causes & Conditions::"+value_1;
		//rover_value = "Causes & Conditions::"+value_1;
		//alert(rover_value);
		
		if(rover_value!=""){
		//Methrd to call Rover.js and pass the query string values to display in flex
		//setTimeout("parent.frames['frmRover'].passToRover(rover_value,'','')",15000);
			setTimeout("parent.frames['frmRover'].passToRover(value1,value2,value3)",5000);
		}
		
		if(receivedValue=="")
		{
			deactivateAllPanes();
		}

		
		/*document.getElementById("Image1").style.display="none";
		document.getElementById("findpeople").style.display="block";
		
		document.getElementById('results').className="";
		document.getElementById('sharewik_res').className="selected";
		
		//vectorLogging(value_1,value_2,value_3,"HealthiCommunities");
		
		document.getElementById('communitytab').style.display="block";
		document.getElementById('webtab').style.display="none";
		document.getElementById('symptomstab').style.display="none";
		activeSection = 1;
		roverStatus=0;
		findPeopleStatus=1;
		webStatus=0;
		symptomStatus=0;
		getMessages();
		
		reloadRover();
		//changePane("Image1");
		//higCommunity(value_1,value_2,value_3,'results');
		//switchwindow("0", "SR");
		higCommunity(value_1,value_2,value_3,'sharewik_res');
		switchwindow("0", "SE");
		//End of Defaultsetting for GoogleAdsense*/
	//}
	
}

/*---------------------------------------------------------------------------------------------------------*/
function confirmfun() {
	needToConfirm = true;
}	
/*---------------------------------------------------------------------------------------------------------*/
/*function allowPopup() {
	needToConfirm = false;
	setTimeout("confirmfun()",10);
}
*//*---------------------------------------------------------------------------------------------------------*/
function changeForm() {
	//document.getElementById('w_search1').value != "CAUSES AND CONDITIONS" && 
	if(document.getElementById('navBox01WideContainer').style.display != "none" ) {
		document.getElementById('navBox01Container').style.display="block";
		document.getElementById('navBox02Container').style.display="block";
		document.getElementById('navBox01WideContainer').style.display="none";
		activateAllPanes();
	} 
}
/*---------------------------------------------------------------------------------------------------------*/
function activateHome() {
	if(document.getElementById('navBox01WideContainer').style.display == "none" ) {
		document.getElementById('navBox01Container').style.display="none";
		document.getElementById('navBox02Container').style.display="none";
		document.getElementById('navBox01WideContainer').style.display="block";
		deactivateAllPanes();
	} 
}

function activateHomeWith3TxtBox() {
	//if(document.getElementById('navBox01WideContainer').style.display == "none" ) {
		document.getElementById('navBox01Container').style.display="block";
		document.getElementById('navBox02Container').style.display="block";
	//	document.getElementById('navBox01WideContainer').style.display="block";
		deactivateAllPanes();
	//} 
}

/*---------------------------------------------------------------------------------------------------------*/
function activateAllPanes() {
	document.getElementById("findpeople").style.display="none";
	document.getElementById("web").style.display="none";
	document.getElementById("symptoms").style.display="none";
	document.getElementById("Image1").style.display="block";
	document.getElementById("Image2").style.display="block";
	document.getElementById("Image3").style.display="block";
}
/*---------------------------------------------------------------------------------------------------------*/
function changePane(paneId) {
	activateAllPanes();
	switch(paneId) {
	case "Image1": 
		bb.callFunction('swapSections',['2']);
		document.getElementById("Image1").style.display="none";
		document.getElementById("findpeople").style.display="block";
		
		vectorLogging(value_1,value_2,value_3,"HealthiCommunities");
		
		document.getElementById('communitytab').style.display="block";
		document.getElementById('webtab').style.display="none";
		document.getElementById('symptomstab').style.display="none";
		activeSection = 2;
		roverStatus=0;
		findPeopleStatus=1;
		webStatus=0;
		symptomStatus=0;
		getMessages();
		break;
	case "Image2":
		bb.callFunction('swapSections',['3']);
		document.getElementById("Image2").style.display="none";
		document.getElementById("web").style.display="block";
		
		vectorLogging(value_1,value_2,value_3,"HealthiWeb");
		
		document.getElementById('communitytab').style.display="none";
		document.getElementById('webtab').style.display="block";
		document.getElementById('symptomstab').style.display="none";
		activeSection = 3;
		roverStatus=0;
		findPeopleStatus=0;
		webStatus=1;
		symptomStatus=0;
		getMessages();
		break;
	case "Image3":
		bb.callFunction('swapSections',['4']);
		document.getElementById("Image3").style.display="none";
		document.getElementById("symptoms").style.display="block";
		
		vectorLogging(value_1,value_2,value_3,"HealthiSymptoms");
		
		document.getElementById('communitytab').style.display="none";
		document.getElementById('webtab').style.display="none";
		document.getElementById('symptomstab').style.display="block";
		activeSection = 4;
		roverStatus=0;
		findPeopleStatus=0;
		webStatus=0;
		symptomStatus=1;
		getMessages();
		break;
	case "":
		bb.callFunction('swapSections',['1']);
		activateAllPanes();
		document.getElementById('communitytab').style.display="none";
		document.getElementById('webtab').style.display="none";
		document.getElementById('symptomstab').style.display="none";
		if(value_1 == "" && value_2 == "" && value_3 == "") {
			//activateHome();
			activateHomeWith3TxtBox();
		}
		reloadRover();
		activeSection = 1;
		roverStatus=1;
		findPeopleStatus=0;
		webStatus=0;
		symptomStatus=0;
		getMessages();
		//setTimeout("parent.frames['frmRover'].passToRover(rover_value,'','')",15000);
		//setTimeout("parent.frames['frmRover'].passToRover(value1,value2,value3)",15000);
		
		break;
	}
	
	
	
}
/*---------------------------------------------------------------------------------------------------------*/
function changeTabState(stateId) {
	switch(stateId) {
	case "results": 
		
		if(document.getElementById('results').className=="") {
			document.getElementById('results').className="selected";
			document.getElementById('sharewik_res').className="";
			document.getElementById('wiki').className="";
			document.getElementById('patients').className="";
			document.getElementById('organised').className="";
		}
		higCommunity(value_1,value_2,value_3,'results');
		switchwindow("0", "SR");
		break;
	case "sharewik_res":
		if(document.getElementById('sharewik_res').className=="")
		{
			document.getElementById('results').className="";
			document.getElementById('sharewik_res').className="selected";
			document.getElementById('wiki').className="";
			document.getElementById('patients').className="";
			document.getElementById('organised').className="";
		}
		higCommunity(value_1,value_2,value_3,'sharewik_res');
		switchwindow("0", "SE");
		break;
	case "wiki": 
		if(document.getElementById('wiki').className=="") {
			document.getElementById('results').className="";
			document.getElementById('sharewik_res').className="";
			document.getElementById('wiki').className="selected";
			document.getElementById('patients').className="";
			document.getElementById('organised').className="";
		}
		higCommunity(value_1,value_2,value_3,'wiki');
		switchwindow("0", "CW");
		break;
		//Newly Added Tabs on May 10 2010
		/*---------- Starts Here --------*/
	case "patients":
		if(document.getElementById('patients').className=="")
		{
			document.getElementById('results').className="";
			document.getElementById('sharewik_res').className="";
			document.getElementById('wiki').className="";
			document.getElementById('patients').className="selected";
			document.getElementById('organised').className="";
		}
		higCommunity(value_1,value_2,value_3,'patients');
		switchwindow("0", "PL");
		break;
	case "organised":
		if(document.getElementById('organised').className=="")
		{
			document.getElementById('results').className="";
			document.getElementById('sharewik_res').className="";
			document.getElementById('wiki').className="";
			document.getElementById('patients').className="";
			document.getElementById('organised').className="selected";
		}
		higCommunity(value_1,value_2,value_3,'organised');
		switchwindow("0", "OW");
		break;
		/*----- Ends Here --------*/
	case "hig":	
		//alert("Entered into HiG");
		if(document.getElementById('hig').className=="") 
		{
			
			document.getElementById('hig').className="selected";
			document.getElementById('sharewikweb').className="";
			document.getElementById('google').className="";
			document.getElementById('yahoo').className="";	
			document.getElementById('healia').className="";
			document.getElementById('bing').className="";
		}
		searchWeb(value_1,value_2,value_3,stateId);
		switchwindow("1", "WR");
		break;
	case "sharewikweb": 
		if(document.getElementById('google').className=="") {
			document.getElementById('hig').className="";
			document.getElementById('sharewikweb').className="selected";
			document.getElementById('google').className="";
			document.getElementById('yahoo').className="";	
			document.getElementById('healia').className="";
			document.getElementById('bing').className="";
		}
		searchWeb(value_1,value_2,value_3,stateId);
		switchwindow("1", "GS");
		break;
	case "google": 
		if(document.getElementById('google').className=="") {
			document.getElementById('hig').className="";
			document.getElementById('sharewikweb').className="";
			document.getElementById('google').className="selected";
			document.getElementById('yahoo').className="";	
			document.getElementById('healia').className="";
			document.getElementById('bing').className="";
		}
		searchWeb(value_1,value_2,value_3,stateId);
		switchwindow("1", "GS");
		break;
	case "yahoo": 
		if(document.getElementById('yahoo').className=="") {
			
			document.getElementById('hig').className="";
			document.getElementById('sharewikweb').className="";
			document.getElementById('google').className="";
			document.getElementById('yahoo').className="selected";	
			document.getElementById('healia').className="";
			document.getElementById('bing').className="";
		}
		searchWeb(value_1,value_2,value_3,stateId);
		switchwindow("1", "YS");
		break;
	case "healia": 
		if(document.getElementById('healia').className=="") {
			document.getElementById('hig').className="";
			document.getElementById('sharewikweb').className="";
			document.getElementById('google').className="";
			document.getElementById('yahoo').className="";	
			document.getElementById('healia').className="selected";
			document.getElementById('bing').className="";
		}
		searchWeb(value_1,value_2,value_3,stateId);
		switchwindow("1", "HS");
		break;
	case "bing": 
		if(document.getElementById('bing').className=="") {
			document.getElementById('hig').className="";
			document.getElementById('sharewikweb').className="";
			document.getElementById('google').className="";
			document.getElementById('yahoo').className="";	
			document.getElementById('healia').className="";
			document.getElementById('bing').className="selected";
		}
		searchWeb(value_1,value_2,value_3,stateId);
		switchwindow("1", "BS");
		break;
	case "healthline": 
		if(document.getElementById('healthline').className=="") {
			document.getElementById('healthline').className="selected";
			document.getElementById('everydayhealth').className="";	
			document.getElementById('mayoclinic').className="";
		}
		searchSymptoms(value_1,value_2,value_3,stateId);
		switchwindow("2", "HL");
		break;
	case "everydayhealth": 
		if(document.getElementById('everydayhealth').className=="") {
			document.getElementById('healthline').className="";
			document.getElementById('everydayhealth').className="selected";	
			document.getElementById('mayoclinic').className="";
		}
		searchSymptoms(value_1,value_2,value_3,stateId);
		switchwindow("2", "EH");
		break;
	case "mayoclinic": 
		if(document.getElementById('mayoclinic').className=="") {
			document.getElementById('healthline').className="";
			document.getElementById('everydayhealth').className="";	
			document.getElementById('mayoclinic').className="selected";
		}
		searchSymptoms(value_1,value_2,value_3,stateId);
		switchwindow("2", "MC");
		break;
	}
	
	
	
	}
/*---------------------------------------------------------------------------------------------------------*/

function searchWeb(reg1,reg2,reg3,engine) 	{
	var target = document.getElementById('frmWeb');
	var searchEngine,criteria;
	var searchString;
	var actual_search_term;
	var splitted_string_w="";
	var reverse_array_w="";
	var new_serach_string_w ="";
// if the target iframe is  found
	if(target != null)
	{
		actual_search_term = reg1;
		
		reg1 = reg1.replace(/&/g,"and");
		reg2 = reg2.replace(/&/g,"and");
		reg3 = reg3.replace(/&/g,"and");
		
		new_serach_string_w = reg1+" "+reg2+" "+reg3;
		
		splitted_string_w= new_serach_string_w.split("::");
		reverse_array_w = splitted_string_w.reverse();
		new_serach_string_w = reverse_array_w[0];
	
		reg1 = reg1.replace(/::/g," ");
		reg2 = reg2.replace(/::/g," ");
		reg3 = reg3.replace(/::/g," ");
		//following is for the neela story only..
		if(reg1.indexOf('Metabolic') != -1){
			reg1 = reg1.replace("Metabolic and Endo","");
		}
	
		criteria = reg1+" "+reg2+" "+reg3;
		criteria = criteria.replace("$|$"," ");
		//criteria = "Cancer Digestive";
		//if search engine is  specified.
		
		/* New Function Added on May 11 2010 
		 * This Function is to split the Whole string into array of Strings.Here 
		 * we need to pass only the end of the String to the Search engine of "Patients Like me",
		 * "Organized Wisdom","HelathLine","EveryDay Health","Mayo Clinic".

		 
			For example if the String is "Cancer::Digestive::Oral  / Mouth" ,we should pass only "Oral".
			*/
			//alert(criteria);
			//Here we split the strings with the format of :: and save it in an array
			//var splitted_string_w= criteria.split("::");

			//Here we reverse the array to fetch the elements that are splitted at the end of the string, easily 
			//var reverse_array_w = splitted_string_w.reverse();
			
			//var new_serach_string_w = reverse_array_w[0];
			//alert(new_serach_string_w);
		/*===================== Ends Here ==================== */	

		
		if(engine != ''){
			searchEngine = engine;
		}else // find the current search Engine loaded in the iframe
		{
		//searchEngine="hig";
		if(target.src.indexOf('RoverWebDataService')!= -1){
			searchEngine = "hig";
		} else if(target.src.indexOf('google.com')!= -1){
			searchEngine = "google";
		}else if(target.src.indexOf('yahoo.com')!= -1){
			searchEngine = "yahoo";
		}else if(target.src.indexOf('healia.com')!= -1){
			searchEngine = "healia";
		}else if(target.src.indexOf('bing.com')!= -1){
			searchEngine = "bing";
		}else if(target.src.indexOf('sharewik.com')!= -1){
			searchEngine = "sharewikweb";
			
		}else{
			//if the web frame is loaded with other url which might be opened from favorites or recent visits.
			//searchEngine = "google";
			//searchEngine = "hig";
		}
		
	}
	switch(searchEngine){
		case "google":	searchString = "http://www.google.com/search?q=";
					searchString = searchString + criteria;
					activeSearchPane = "google";
					break;
		case "yahoo" : 	searchString = "http://search.yahoo.com/search?va=";
					searchString = searchString + criteria;
					activeSearchPane = "yahoo";
					break;
		case "healia"  :	searchString = "http://www.healia.com/healia/search.do?query=";
					searchString = searchString + criteria;
					activeSearchPane = "healia";
					break;
		case "bing"  :	searchString = "http://www.bing.com/search?q=";
					searchString = searchString + criteria;
					activeSearchPane = "bing";
					break;
		case "sharewikweb" : searchString = "http://www.google.com/cse?cx=018194584924815650089%3Atusscgrff38&ie=UTF-8&q=";
							searchString	= searchString+new_serach_string_w+"&sa=Search&siteurl=%2Fhome%2Fvimal%2FDesktop%2Ftest.html";
							searchString = "/SharewikSearch/getUrls.do?callingApp=3d&vector="+new_serach_string_w;
							//alert(searchString);
							//wikiString ="Cancer";
		//communityString = communityString+wikiString;
		//communityString = communityString+new_serach_string;
		activeHiGCommunity ="sharewikweb";
					break;
		
		case "hig":	
			var vector1="";	var vector2="";	var vector3="";
			if(document.getElementById('hig').className=="") {
				document.getElementById('hig').className="selected";
				//document.getElementById('wiki').className="";
			}
			if(value1!=""){
				vector1 = value1;
				v1=value1;
			}
			if(value2!=""){
				vector2 = value2;
				v2=value2;
			}
			if(value3!=""){
				vector3 = value3;
				v3=value3;
			}
			vector1=vector1.replace("Causes & Conditions::","");
			vector2=vector2.replace("Treatments & Tonics::","");
			vector3=vector3.replace("Plans & Providers::","");

			
			//activeHiGCommunity = "results";
			activeSearchPane = "hig";
			
			//vector1 = vector1.replace(/ - /g,"|**|");
			vector1 = vector1.replace(/::/g,"|*|");
			vector1 = vector1.replace(/&/g,"|@|");
			vector1 = vector1.replace(/ /g,"|$|");
			
			if(nVector1 != "") {
				nVector1 = nVector1.replace(/::/g,"|*|");
				nVector1 = nVector1.replace(/&/g,"|@|");
				nVector1 = nVector1.replace(/ /g,"|$|");
			}
			
			data = vector1.split("|*|");
			/*if(data.length == 4) {
				vector1 = data[0]+"|*|"+data[1]+"|*|"+data[2]+"-"+data[3]
			}*/
			//vector1.
			
			vector2 = vector2.replace(/::/g,"|*|");
			vector2 = vector2.replace(/&/g,"|@|");
			vector2 = vector2.replace(/ /g,"|$|");
			if(nVector2 != "") {
				nVector2 = nVector2.replace(/::/g,"|*|");
				nVector2 = nVector2.replace(/&/g,"|@|");
				nVector2 = nVector2.replace(/ /g,"|$|");
			}
			
			data = vector2.split("|*|");
			/*if(data.length == 3 ) {
				vector2 = data[0]+"|*|"+data[1]+"-"+data[2];
			} else if(data.length == 4) {
				vector2 = data[0]+"|*|"+data[1]+"-"+data[2]+"-"+data[3];
			}*/
			
			vector3 = vector3.replace(/::/g,"|*|");
			vector3 = vector3.replace(/&/g,"|@|");
			vector3 = vector3.replace(/ /g,"|$|");
			
			if(nVector3 != "") {
				nVector3 = nVector3.replace(/::/g,"|*|");
				nVector3 = nVector3.replace(/&/g,"|@|");
				nVector3 = nVector3.replace(/ /g,"|$|");
			}
			
			
			data = vector3.split("|*|");
			/*if(data.length == 3 ) {
				vector3 = data[0]+"|*|"+data[1]+"-"+data[2];
			}*/
			
			//alert(nVector1+"  Next Vector "+ vector1);
			if(nVector1 != vector1) {
				nVector1 = "";
			} 
			
			if(nVector2 != vector2) {
				nVector2 = "";
			}
			
			if(nVector3 != vector3) {
				nVector3 = "";
			}
			
			if(nVector1 == "" && nVector2 == "" && nVector3 == "") {
				searchString = "/RoverWebDataService/getUrls.do?v1="+vector1+"&v2="+vector2+"&v3="+vector3;
			} else if(nVector1 == "" && vector1 != ""){
				searchString = "/RoverWebDataService/getOtherUrls.do?v1="+vector1+"&v2="+vector2+"&v3="+vector3+"&nv1="+nVector1+"&nv2="+nVector2+"&nv3="+nVector3;										
				
			} else {
				//var vector = nVector1.replace("/::/", replaceValue)
				if(vector2 != "" && vector2 != "TREATMENTS AND TONICS" ) {
					nVector2 = vector2;
				}
				if(nVector3 != "" && vector3 != "PLANS AND PROVIDERS") {
					nVector3 = vector3;
				}
				searchString = "/ExternalUrlService/getUrls.do?vector="+nVector1+" "+nVector2+" "+nVector3;
				//openframepop('welcome/popup/extendTaxPopupFinal.html');
			}
			
		break;
			
	}
	
	target.src = searchString;
	}
}
/*---------------------------------------------------------------------------------------------------------*/
function higCommunity(value1,value2,value3,engine)
{
	
	var data;
	var target = document.getElementById('frmHig');
	// if the target iframe is  found
	var communityString;
	//alert(value1);
	
	var wikiString = '';
	if(value1 != '') {
		wikiString = value1;
	} else if(value1 == '' && value2 != '') {
		wikiString = value2;
	} else if(value1 == '' && value2 == '' && value3 != '') {
		wikiString = value3;
	}
	wikiString = wikiString.replace(/&/g,"and");
/* New Function Added on May 11 2010 
 * This Function is to split the Whole string into array of Strings.Here 
 * we need to pass only the end of the String to the Search engine of "Patients Like me",
 * "Organized Wisdom","HelathLine","EveryDay Health","Mayo Clinic".

 
	For example if the String is "Cancer::Digestive::Oral  / Mouth" ,we should pass only "Oral".
	*/
	
	//Here we split the strings with the format of :: and save it in an array
	var splitted_string= wikiString.split("::");

	//Here we reverse the array to fetch the elements that are splitted at the end of the string, easily 
	var reverse_array = splitted_string.reverse();
	
	var new_serach_string = reverse_array[0];
/*===================== Ends Here ==================== */	
		
	wikiString = wikiString.replace(/::/g," ");
	
	var communityView = "";
	
	if(target != null){
		if(engine != ''){
			communityView = engine;
			
		}else // find the current search Engine loaded in the iframe
		{
			target = document.getElementById('frmHig');
			if(target.src.indexOf('RoverDataService')!= -1){
				communityView = "results";
			} else if(target.src.indexOf('en.wikipedia.org')!= -1){
				communityView = "wiki";
			} else{
			//if the web frame is loaded with other url which might be opened from favorites or recent visits.
				communityView = "results";
			}
		}

		
		switch(communityView){
			case "results":	
							var vector1="";	var vector2="";	var vector3="";
							if(document.getElementById('results').className=="") {
								document.getElementById('results').className="selected";
								document.getElementById('sharewik_res').className="";
								document.getElementById('wiki').className="";
								document.getElementById('patients').className="";
								document.getElementById('organised').className="";
							}
							if(value1!=""){
								vector1 = value1;
								v1=value1;
							}
							if(value2!=""){
								vector2 = value2;
								v2=value2;
							}
							if(value3!=""){
								vector3 = value3;
								v3=value3;
							}
							vector1=vector1.replace("Causes & Conditions::","");
							vector2=vector2.replace("Treatments & Tonics::","");
							vector3=vector3.replace("Plans & Providers::","");
			
							
							activeHiGCommunity = "results";
							
							//vector1 = vector1.replace(/ - /g,"|**|");
							vector1 = vector1.replace(/::/g,"|*|");
							vector1 = vector1.replace(/&/g,"|@|");
							vector1 = vector1.replace(/ /g,"|$|");
							
							if(nVector1 != "") {
								nVector1 = nVector1.replace(/::/g,"|*|");
								nVector1 = nVector1.replace(/&/g,"|@|");
								nVector1 = nVector1.replace(/ /g,"|$|");
							}
							
							data = vector1.split("|*|");
							/*if(data.length == 4) {
								vector1 = data[0]+"|*|"+data[1]+"|*|"+data[2]+"-"+data[3]
							}*/
							//vector1.
							
							vector2 = vector2.replace(/::/g,"|*|");
							vector2 = vector2.replace(/&/g,"|@|");
							vector2 = vector2.replace(/ /g,"|$|");
							if(nVector2 != "") {
								nVector2 = nVector2.replace(/::/g,"|*|");
								nVector2 = nVector2.replace(/&/g,"|@|");
								nVector2 = nVector2.replace(/ /g,"|$|");
							}
							
							data = vector2.split("|*|");
							/*if(data.length == 3 ) {
								vector2 = data[0]+"|*|"+data[1]+"-"+data[2];
							} else if(data.length == 4) {
								vector2 = data[0]+"|*|"+data[1]+"-"+data[2]+"-"+data[3];
							}*/
							
							vector3 = vector3.replace(/::/g,"|*|");
							vector3 = vector3.replace(/&/g,"|@|");
							vector3 = vector3.replace(/ /g,"|$|");
							
							if(nVector3 != "") {
								nVector3 = nVector3.replace(/::/g,"|*|");
								nVector3 = nVector3.replace(/&/g,"|@|");
								nVector3 = nVector3.replace(/ /g,"|$|");
							}
							
							
							data = vector3.split("|*|");
							/*if(data.length == 3 ) {
								vector3 = data[0]+"|*|"+data[1]+"-"+data[2];
							}*/
							
							//alert(nVector1+"  Next Vector "+ vector1);
							if(nVector1 != vector1) {
								nVector1 = "";
							} 
							
							if(nVector2 != vector2) {
								nVector2 = "";
							}
							
							if(nVector3 != vector3) {
								nVector3 = "";
							}
							
							if(nVector1 == "" && nVector2 == "" && nVector3 == "") {
								communityString = "/RoverDataService/getUrls.do?v1="+vector1+"&v2="+vector2+"&v3="+vector3;
							} else if(nVector1 == "" && vector1 != ""){
								communityString = "/RoverDataService/getOtherUrls.do?v1="+vector1+"&v2="+vector2+"&v3="+vector3+"&nv1="+nVector1+"&nv2="+nVector2+"&nv3="+nVector3;										
								
							} else {
								//var vector = nVector1.replace("/::/", replaceValue)
								if(vector2 != "" && vector2 != "TREATMENTS AND TONICS" ) {
									nVector2 = vector2;
								}
								if(nVector3 != "" && vector3 != "PLANS AND PROVIDERS") {
									nVector3 = vector3;
								}
								communityString = "/ExternalUrlService/getUrls.do?vector="+nVector1+" "+nVector2+" "+nVector3;
								//openframepop('welcome/popup/extendTaxPopupFinal.html');
							}
							
						break;
			case "wiki" : 	communityString = "http://en.wikipedia.org/wiki/Special:Search?search=";
							//wikiString = "Cancer Digestive";
							communityString = communityString + wikiString + '&go=GO';
							activeHiGCommunity = "wiki";
						break;
			case "patients" : communityString = "http://www.patientslikeme.com/search?q=";
			//wikiString ="Cancer";
			//communityString = communityString+wikiString;
			communityString = communityString+new_serach_string;
			activeHiGVommunity ="patients";
						break;
			case "sharewik_res" : communityString = "http://www.google.com/cse?cx=018194584924815650089%3Atusscgrff38&ie=UTF-8&q=";
			communityString= communityString+new_serach_string+"&sa=Search&siteurl=%2Fhome%2Fvimal%2FDesktop%2Ftest.html";
			communityString = "/SharewikSearch/getUrls.do?callingApp=3d&vector="+new_serach_string+" "+nVector2+" "+nVector3;
			//wikiString ="Cancer";
			//communityString = communityString+wikiString;
			//communityString = communityString+new_serach_string;
			activeHiGCommunity ="sharewik_res";
			//alert(communityString);
						break;
			case "organised" : communityString = "http://www.dailystrength.org/search?q=";
			//wikiString ="Cancer";
			//communityString = communityString+wikiString;
			communityString = communityString+new_serach_string;
			activeHiGVommunity ="organised";
						break;			
			
		}
		target.src = communityString;
		
	}else // if the target iframe is  not found
	{
		setTimeout("higCommunity(value1,value2,value3,'results')",500);
	}
}
//allowPopup();
/*---------------------------------------------------------------------------------------------------------*/
function searchSymptoms(reg1,reg2,reg3,engine) {
	var searchString;
var target = document.getElementById('frmSymptoms');
// if the target iframe is  found
if(target != null){
	reg1 = reg1.replace(/&/g,"and");
	reg2 = reg2.replace(/&/g,"and");
	reg3 = reg3.replace(/&/g,"and");
	
	//Added on May 11 2010 
	 /* This Function is to split the Whole string into array of Strings.Here 
	 * we need to pass only the end of the String to the Search engine of "HelathLine","EveryDay Health","Mayo Clinic".

	 
		For example if the String is "Cancer::Digestive::Oral  / Mouth" ,we should pass only "Oral".
		*/
	/* ========== Starts Here ============*/
	var rearranged_string1 =  reg1.split("::");
	var rearranged_string2 =  reg1.split("::");
	var rearranged_string3 =  reg1.split("::");
	
	rearranged_string1 = rearranged_string1.reverse();
	rearranged_string2 = rearranged_string2.reverse();
	rearranged_string3 = rearranged_string3.reverse();
	
	var reg1_modified_string = rearranged_string1[0];
	var reg2_modified_string = rearranged_string2[0];
	var reg3_modified_string = rearranged_string3[0];
	
	
	/*================ Ends Here ==================*/
	
	reg1 = reg1.replace(/::/g," ");
	reg2 = reg2.replace(/::/g," ");
	reg3 = reg3.replace(/::/g," ");
	//following is for the neela story only..
	if(reg1.indexOf('Metabolic') != -1){
		reg1 = reg1.replace("Metabolic and Endo","");
	}
	var searchEngine,criteria;
	//criteria = reg1+" "+reg2+" "+reg3;
	criteria = reg1;
	criteria = criteria.replace("$|$"," ");
	
	//if search engine is  specified.
	if(engine != ''){
		searchEngine = engine;
	}else // find the current search Engine loaded in the iframe
	{
		target = document.getElementById('frmSymptoms');
		if(target.src.indexOf('healthline.com')!= -1){
			searchEngine = "healthline";
		}else if(target.src.indexOf('everydayhealth.com')!= -1){
			searchEngine = "everydayhealth";
		}else if(target.src.indexOf('mayoclinic.com')!= -1){
			searchEngine = "mayoclinic";
		}else{
		//if the web frame is loaded with other url which might be opened from favorites or recent visits.
			searchEngine = "healthline";
		}
	}
	//criteria = "Cancer Digestive";
	switch(searchEngine){
		/*
		case "healthline":	searchString = "http://www.healthline.com/search?q1=";
					searchString = searchString + criteria;
					activeSymptomPane = "healthline";
					break;
		case "everydayhealth" : 	searchString = "http://www.everydayhealth.com/PublicSite/searchresultsdynamic.aspx?q=";
					searchString = searchString + criteria;
					activeSymptomPane = "everydayhealth";
					break;
		case "mayoclinic"  :	searchString = "http://www.mayoclinic.com/health/search/search/SEARCHTYPE=site&KEYWORDS=";
					searchString = searchString + criteria;
					activeSearchPane = "mayoclinic";
					break;
		*/
	case "healthline":	searchString = "http://www.healthline.com/symptomsearch?addterm=";
		var replace_criteria=criteria;
		//alert("Before:"+replace_criteria);
		//alert(replace_criteria.indexOf(" "));
		//alert(replace.substring())
		replace_criteria = replace_criteria.replace(" " , "+");
		//alert("After:" + replace_criteria);
		//searchString = searchString + replace_criteria;
		searchString = searchString + reg1_modified_string;
		//alert(searchString);
		activeSymptomPane = "healthline";
		break;
	case "everydayhealth" : 	
		//searchString = "http://www.everydayhealth.com/PublicSite/searchresultsdynamic.aspx?q=";
		searchString = "http://www.everydayhealth.com/symptom-checker/";
		//searchString = searchString + criteria;
		activeSymptomPane = "everydayhealth";
		break;
	case "mayoclinic"  :	
		//searchString = "http://www.mayoclinic.com/health/search/search/SEARCHTYPE=site&KEYWORDS=";
		searchString = "http://mayoclinic.com/health/symptom-checker/DS00671";
		//searchString = searchString + criteria;
		activeSearchPane = "mayoclinic";
		break;
	}
	target.src = searchString;
}
}

/*
function searchSymptoms(reg1,reg2,reg3,engine) {
	var searchString;
var target = document.getElementById('frmSymptoms');
// if the target iframe is  found
if(target != null){
	reg1 = reg1.replace(/&/g,"and");
	reg2 = reg2.replace(/&/g,"and");
	reg3 = reg3.replace(/&/g,"and");
	
	reg1 = reg1.replace(/::/g," ");
	reg2 = reg2.replace(/::/g," ");
	reg3 = reg3.replace(/::/g," ");
	//following is for the neela story only..
	if(reg1.indexOf('Metabolic') != -1){
		reg1 = reg1.replace("Metabolic and Endo","");
	}
	var searchEngine,criteria;
	criteria = reg1+" "+reg2+" "+reg3;
	criteria = criteria.replace("$|$"," ");
	//if search engine is  specified.
	if(engine != ''){
		searchEngine = engine;
	}else // find the current search Engine loaded in the ifrafunction AJAXInteraction(url, callback) {
		 //alert(url);
	    var req = init();
	      req.onreadystatechange = processRequest;
	        
	    function init() {
	      if (window.XMLHttpRequest) {
	      
	        return new XMLHttpRequest();
	      } else if (window.ActiveXObject) {
	        return new ActiveXObject("Microsoft.XMLHTTP");
	      }
	    }
	    function processRequest () {
	      // readyState of 4 signifies request is complete
	      //alert(req.readyState);
	       if (req.readyState == 4) {
	         // status of 200 signifies sucessful HTTP call
	          if (req.status == 200) {
	       //   alert("STATUS"+req.status);
	          if (callback) callback(req.responseText);
	        }
	      }
	    }

	    this.doGet = function() {
	     
	    req.open("GET", url, true);
	    req.send(null);
	    }
	}me
	{
		target = document.getElementById('frmSymptoms');
		if(target.src.indexOf('healthline.com')!= -1){
			searchEngine = "healthline";
		}else if(target.src.indexOf('everydayhealth.com')!= -1){
			searchEngine = "everydayhealth";
		}else if(target.src.indexOf('mayoclinic.com')!= -1){
			searchEngine = "mayoclinic";
		}else{
		//if the web frame is loaded with other url which might be opened from favorites or recent visits.
			searchEngine = "healthline";
		}
	}
	//criteria = "Cancer Digestive";
	switch(searchEngine){
		/*
		 
		 case "healthline":	searchString = "http://www.healthline.com/search?q1=";
					searchString = searchString + criteria;
					activeSymptomPane = "healthline";
					break;
		case "everydayhealth" : 	searchString = "http://www.everydayhealth.com/PublicSite/searchresultsdynamic.aspx?q=";
					searchString = searchString + criteria;
					activeSymptomPane = "everydayhealth";
					break;
		case "mayoclinic"  :	searchString = "http://www.mayoclinic.com/health/search/search/SEARCHTYPE=site&KEYWORDS=";
					searchString = searchString + criteria;
					activeSearchPane = "mayoclinic";
					break;
		
	
		case "healthline":	searchString = "http://www.healthline.com/symptomsearch?addterm=";
					var replace_criteria=criteria;
					//alert("Before:"+replace_criteria);
					//alert(replace_criteria.indexOf(" "));
					//alert(replace.substring())
					replace_criteria = replace_criteria.replace(" " , "+");
					//alert("After:" + replace_criteria);
					searchString = searchString + replace_criteria;
					activeSymptomPane = "healthline";
					break;
		case "everydayhealth" : 	
					//searchString = "http://www.everydayhealth.com/PublicSite/searchresultsdynamic.aspx?q=";
					searchString = "http://www.everydayhealth.com/symptom-checker/";
					//searchString = searchString + criteria;
					activeSymptomPane = "everydayhealth";
					break;
		case "mayoclinic"  :	
					//searchString = "http://www.mayoclinic.com/health/search/search/SEARCHTYPE=site&KEYWORDS=";
					searchString = "http://mayoclinic.com/health/symptom-checker/DS00671";
					//searchString = searchString + criteria;
					activeSearchPane = "mayoclinic";
					break;
	
	}
	target.src = searchString;
}
}
*/
/*---------------------------------------------------------------------------------------------------------*/
//Method is calling taxonomy.xml
function loadCWPulldowns(){
	loadCWXML();
}
/*---------------------------------------------------------------------------------------------------------*/
//Method for loading taxonomy.xml files
function loadCWXML() {
	 if (window.ActiveXObject){
		xmlDoc_C=new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc_C.async=false;
		xmlDoc_C.load("welcome/myrover/taxonomy.xml");	
		processCWXML();
	}
	// code for Mozilla, Firefox, Opera, etc.
	else if (document.implementation && document.implementation.createDocument)	{
		xmlDoc_C=loadXMLDoc("welcome/myrover/taxonomy.xml");
		processCWXML();
	}else{
		alert('Your browser cannot handle this script');
	}
	// alert("Hi" + xmlDoc_C);
 }

/*---------------------------------------------------------------------------------------------------------*/
//Method for Parsing the taxonomy.xml file.
	function processCWXML() {
		//alert("Hi");
		var x=xmlDoc_C.documentElement.childNodes;
		var causes,treats,plans;
		for(var i=0; i<x.length;i++){
			if(x[i].nodeType==1){
				if(x[i].attributes.getNamedItem("value").nodeValue == "Causes & Conditions"){
					causes=x[i].childNodes;
					parseData(causes,"",1);
				}
				if(x[i].attributes.getNamedItem("value").nodeValue == "Treatments & Tonics"){
					treats=x[i].childNodes;
					parseData(treats,"",2);
				}
				if(x[i].attributes.getNamedItem("value").nodeValue == "Plans & Providers"){
					plans=x[i].childNodes;
					parseData(plans,"",3);
				}
			}
		}
function parseData(arr,prnt,section){
	 for(var i=0;i<arr.length;i++){	
		if(arr[i].nodeType==1){
			tempKey=arr[i].attributes.getNamedItem("value").nodeValue;
			if(prnt==""){
				p=tempKey;
		}
			if(arr[i].childNodes.length > 0){
				if(prnt!=""){
				p=prnt+"::"+tempKey;
			}    					
   			populate(p,section);	
				parseData(arr[i].childNodes,p,section);
			}else{
				p=prnt;
				if(p!=""){
   				populate(p+"::"+tempKey,section);
   			}else{
   				populate(tempKey,section);
   			}
			}
		}
	 }
}

function populate(key,section)	{
		if(section==1){
			keywords_list1[j]= key;
			j++;
		}
		if(section==2){
			keywords_list2[k]= key;
			k++;
		}
		if(section==3){
			keywords_list3[l]= key;
			l++;
		}	
}

CWPulldownsLoaded = true;
activateCommonPulldowns(); 	activateWidePulldowns();
}
	
/*---------------------------------------------------------------------------------------------------------*/
	//This method is wide pulldown  auto complete.
 function activateWidePulldowns(){
 	if(document.getElementById('w_search1')){
	 	$("#w_search1").autocomplete(keywords_list1,
			{
				delay:10,
				minChars:1,
				width:450,
				max:100,
				resultsClass: "autosuggest1",
				matchContains: true
			}
		);
		$("#w_search1").result(function(event, data, formatted) {
			if (data)  valueSelected(1,formatted);
			$("#w_search1").focus();	
		});
 	}else{
		setTimeout("activateWidePulldowns()",1000);
	}
 }
/*---------------------------------------------------------------------------------------------------------*/
 function clearValue(elm){
	 	if(elm.id == "w_search1" || elm.id == "c_search1")
	 		if(trim(elm.value)== "CAUSES AND CONDITIONS")
	 			elm.value="";
	 	if(elm.id == "c_search2")
	 		if(trim(elm.value)== "TREATMENTS AND TONICS")
	 			elm.value="";
	 	if(elm.id == "c_search3")
	 		if(trim(elm.value)== "PLANS AND PROVIDERS")
	 			elm.value="";
	} 
/*---------------------------------------------------------------------------------------------------------*/
	// Method for valueSelected in Dropdown
	function valueSelected(number,val){
		//alert("valueSelected"+val);
		if(number==1&&nVector1!="")
			openframepop('welcome/popup/extendTaxPopupFinal.html');
		else if(number==2&&nVector2!="")
			openframepop('welcome/popup/extendTaxPopupFinal.html');
		else if(number==3&&nVector3!="")
			openframepop('welcome/popup/extendTaxPopupFinal.html');
		
		store(number,val);
		if(val !="" && activeSection == 1) 
			setTimeout("parent.frames['frmRover'].passToRover(value1,value2,value3)",1000);
	}
/*---------------------------------------------------------------------------------------------------------*/
	// Method for assigning vector values for rover service
	function store(number,val)
	{
		if(number == 1){
			if(val != ""  && val.indexOf("Causes & Conditions")< 0){
				val="Causes & Conditions::"+val;
			}
			value1 = val;
		}else if(number == 2){
			if(val != ""  && val.indexOf("Treatments & Tonics")<0){
				val="Treatments & Tonics::"+val;
			}
			value2 = val;
		}else if(number == 3){
			if(val != ""  && val.indexOf("Plans & Providers")<0){
				val="Plans & Providers::"+val;
			}
			value3 = val;
		}
		
		if(value1 != prevVal1 || value2 != prevVal2 || value3 != prevVal3){
			//alert('yes');
			prevVal1 = value1;prevVal2 = value2;prevVal3 = value3;
			if(forwardArray.length > 0){
				for(var i=forwardArray.length-1;i>=0;i--){
					totalArray = totalArray.concat(forwardArray[i]);
				}
				forwardArray=new Array();
			}
			totalArray.push(value1+","+value2+","+value3);
		}
		setValues();
	}
/*---------------------------------------------------------------------------------------------------------*/
	//This method is used to format the vector values.
	function formatValues()
	{
		if(value1.indexOf("Causes & Conditions::")>=0){
			value_1=value1.replace("Causes & Conditions::","");
		}
		if(value2.indexOf("Treatments & Tonics::")>=0){
			value_2=value2.replace("Treatments & Tonics::","");
		}
		if(value3.indexOf("Plans & Providers::")>=0)	{
			value_3=value3.replace("Plans & Providers::","");
		}
		if(value1=="Causes & Conditions" || value1==""){
			value_1="";
		}
		if(value2=="Treatments & Tonics" || value2==""){
			value_2="";
		}
		if(value3=="Plans & Providers" || value3==""){
			value_3="";
		}
	}
/* -------------------------------------------------------------------------------------------------------------------- */
// This method is used to set the values into corresponding text field or drop down field
function setValues() {
		
		//alert("value_1--"+value_1+" ---- "+"value_2-- "+value_2+"value_3 -- "+value_3);
		formatValues();
		
		if(document.getElementById('c_search1')){
			if(value_1 !=""){
				document.getElementById('c_search1').value=value_1;
			}else{
				document.getElementById('c_search1').value="CAUSES AND CONDITIONS";
			}
			if(value_2 !=""){
				document.getElementById('c_search2').value=value_2;
			}else{
				document.getElementById('c_search2').value="TREATMENTS AND TONICS";
			}
			if(value_3 !=""){
				document.getElementById('c_search3').value=value_3;
			}else{
				document.getElementById('c_search3').value="PLANS AND PROVIDERS";
			}
		}
		if(document.getElementById('w_search1')){
			//alert(value_1);
			if(value_1 != ""){
				document.getElementById('w_search1').value=value_1;
				changeForm();
			} else if(value_2 != "" || value_3 != "") {
				changeForm();
			}else{
				document.getElementById('w_search1').value="CAUSES AND CONDITIONS";
			}
		}
		
		if(value_1 != "" ||  value_2 != "" || value_3 != "") {
			
			activateAllPanes();
			searchWeb(value_1,value_2,value_3,'');
			higCommunity(value_1,value_2,value_3,'results');
			searchSymptoms(value_1,value_2,value_3,'');
			setTimeout("getMessages()",1100);
		} else if(value_1 == "" && value_2 == "" && value_3 == ""){
			deactivateAllPanes();
			setTimeout("getMessages()",1100);
			//deactivateFind();
		}
}
/*---------------------------------------------------------------------------------------------------------*/
// Method for loading xml content using HttpRequest mainly for safari and Chrome.
function loadXMLDoc(docName) {
	var xhttp;
	if(window.XMLHttpRequest) {
		xhttp = new XMLHttpRequest();
	}
	xhttp.open("GET", docName, false);
	xhttp.send(null);
	return xhttp.responseXML;
}
/*---------------------------------------------------------------------------------------------------------*/
//This method is used in Rover pane auto complete.
function activateCommonPulldowns(){
 	if(document.getElementById('c_search1')){
	 	$("#c_search1").autocomplete(keywords_list1,
			{
				delay:10,
				//allowPopup();
				minChars:1,
				width:450,
				max:100,
				resultsClass: "autosuggest1",
				matchContains: true
			}
		);
		$("#c_search1").result(function(event, data, formatted) {
			if (data)  valueSelected(1,formatted);
			$("#c_search1").focus();	
		});
		$("#c_search2").autocomplete(keywords_list2,
			{
				delay:10,
				minChars:1,
				width:450,
				max:50,
				resultsClass: "autosuggest2",
				matchContains: true
			}
		);
	
		$("#c_search2").result(function(event, data, formatted) {
			if (data) valueSelected(2,formatted);
			$("#c_search2").focus();	
		});
		$("#c_search3").autocomplete(keywords_list3,
			{
				delay:10,
				minChars:1,
				width:450,
				max:50,
				resultsClass: "autosuggest3",
				matchContains: true
			}
		);
		$("#c_search3").result(function(event, data, formatted) {
			if (data) valueSelected(3,formatted);
			$("#c_search3").focus();		
		});
	}else{
		setTimeout("activateCommonPulldowns()",1000);
	}
 }
/*---------------------------------------------------------------------------------------------------------*/
//This method is used to check the given vector value is present in taxonomy.xml
function contains(str){
	var flag = false;
	for(var i=0;i < keywords_list1.length;i++){
		if(keywords_list1[i]== str)flag = true;
	}
	if(!flag){
		for(var i=0;i < keywords_list2.length;i++){
 			if(keywords_list2[i]== str)flag = true;
		}	
	}
	if(!flag){
		for(var i=0;i < keywords_list3.length;i++){
			if(keywords_list3[i]== str)flag = true;
		}
	}
	return flag;
}
/*---------------------------------------------------------------------------------------------------------*/
//firefox fix. Method for reloading the Rover
function reloadRover(){
	if(value_1 == "" && value_2 == "" && value_3 == "") {
 		 //deactivateFind();
 	} else if(activeSection == 3){
 		//activateFind();
 		//vectorLogging(value_1,value_2, value_3, "Rover");
 	}
	//alert(value1);
	//alert(value2);
	//alert(value3);
 	setTimeout("parent.frames['frmRover'].passToRover(value1,value2,value3)",1000);	
}
 	 
/*---------------------------------------------------------------------------------------------------------*/
 function activatePlayerMenu() {
	 document.getElementById('player').style.visibility="visible";
 }
/*---------------------------------------------------------------------------------------------------------*/
 function deActivatePlayerMenu() {
	 document.getElementById('player').style.visibility="hidden";
 }
/*---------------------------------------------------------------------------------------------------------*/
//This method is used to reset, particular vector.
function resetRegister(register){
	var val;
	var tempArray = new Array();
	if(totalArray.length > 1){
		switch(register){
			case "r1": 	//update totalArray
						for(var i=0;i<totalArray.length;i++){
							val=totalArray[i].split(',');
							tempArray[i]=""+","+val[1]+","+val[2];
						}
						totalArray = new Array();
						totalArray.push(tempArray[0]);
						
						for(var i=1;i<tempArray.length;i++){
							if(tempArray[i]!=tempArray[i-1]){
								totalArray.push(tempArray[i]);	
							}
						}
						if(forwardArray.length > 0){
							tempArray = new Array();
							for(var i=0;i<forwardArray.length;i++){
								val=forwardArray[i].split(',');
								tempArray[i]=""+","+val[1]+","+val[2];
							}
							forwardArray = new Array();
							forwardArray.push(tempArray[0]);
							for(var i=1;i<tempArray.length;i++){
								if(tempArray[i]!=tempArray[i-1]){
									forwardArray.push(tempArray[i]);	
								}
							}	
						}
							
						valueArray=totalArray[totalArray.length-1].split(',');
						value1=valueArray[0];value2=valueArray[1];value3=valueArray[2];
						value_1 = "";
						setValues();
							
							
						if(activeSection == 1) {
							setTimeout("parent.frames['frmRover'].resetRover()",2000);
							setTimeout("parent.frames['frmRover'].passToRover(value1,value2,value3)",2000);
						}
						break;
			case "r2": 	//update totalArray
						for(var i=0;i<totalArray.length;i++){
							val=totalArray[i].split(',');
							tempArray[i]=val[0]+","+""+","+val[2];
						}
						totalArray = new Array();
						totalArray.push(tempArray[0]);
						
						for(var i=1;i<tempArray.length;i++){
							if(tempArray[i]!=tempArray[i-1]){
								totalArray.push(tempArray[i]);	
							}
								//allowPopup();
						}
						
						if(forwardArray.length > 0){
							tempArray = new Array();
							for(var i=0;i<forwardArray.length;i++){
								val=forwardArray[i].split(',');
								tempArray[i]=""+","+val[1]+","+val[2];
							}
							forwardArray = new Array();
							forwardArray.push(tempArray[0]);
							for(var i=1;i<tempArray.length;i++){
								if(tempArray[i]!=tempArray[i-1]){
									forwardArray.push(tempArray[i]);	
								}
							}	
						}
						valueArray=totalArray[totalArray.length-1].split(',');
						value1=valueArray[0];value2=valueArray[1];value3=valueArray[2];
						value_2 = "";
						setValues();
						if(activeSection == 1) {
							setTimeout("parent.frames['frmRover'].resetRover()",2000);
							setTimeout("parent.frames['frmRover'].passToRover(value1,value2,value3)",2000);
						}
						break;
			case "r3": 	//update totalArray
						for(var i=0;i<totalArray.length;i++){
							val=totalArray[i].split(',');
							tempArray[i]=val[0]+","+val[1]+","+"";
						}
						totalArray = new Array();
						totalArray.push(tempArray[0]);
				
						for(var i=1;i<tempArray.length;i++){
							if(tempArray[i]!=tempArray[i-1]){
								totalArray.push(tempArray[i]);	
							}
						}
						if(forwardArray.length > 0){
							tempArray = new Array();
							for(var i=0;i<forwardArray.length;i++){
								val=forwardArray[i].split(',');
								tempArray[i]=""+","+val[1]+","+val[2];
							}
							forwardArray = new Array();
							forwardArray.push(tempArray[0]);
							for(var i=1;i<tempArray.length;i++){
								if(tempArray[i]!=tempArray[i-1]){
									forwardArray.push(tempArray[i]);	
								}
							}	
						}
						valueArray=totalArray[totalArray.length-1].split(',');
						value1=valueArray[0];value2=valueArray[1];value3=valueArray[2];
						value_3 = "";
						setValues();
						if(activeSection == 1) {
							setTimeout("parent.frames['frmRover'].resetRover()",2000);
							setTimeout("parent.frames['frmRover'].passToRover(value1,value2,value3)",2000);
						}
						break;
		}
		if(totalArray.length == 1){
				
		}
			
		//alert(totalArray.length);
	}
}	 	 
/*---------------------------------------------------------------------------------------------------------*/
// This method is used to reset all the 3 vectors.
function resetRegisters(){
	//alert("resetRegister");
	totalArray = new Array();
	forwardArray = new Array();
	value1="";value2="";value3="";
	totalArray[0]=value1+","+value2+","+value3;
	setValues();
	parent.frames['frmRover'].resetRover();
}  	 
/*---------------------------------------------------------------------------------------------------------*/
//This method is used to move backward in to the rover graph
function moveBackward1()	{	
	//alert("MoveBackward");
	if(totalArray.length > 1){
		forwardArray =forwardArray.concat(totalArray.splice(totalArray.length-1,1));
		valueArray=totalArray[totalArray.length-1].split(',');
		value1=valueArray[0];value2=valueArray[1];value3=valueArray[2];
		parent.frames['frmRover'].moveBackward(value1,value2,value3);
		setValues();
		//callfindResults();
		//activateButton('btnForward');
			
		if(totalArray.length == 1){
			//deactivateButton('btnBackward');
			//deactivateFind();
			//deactivateTopFind('find_on');
			//deactivateButton('btnFind');
		}
	}
}
/* -------------------------------------------------------------------------------------------------------------------- */
//This method is used to move forward into the rover graph
function moveForward1()	{
	//alert("MoveForward");
	if(forwardArray.length > 0){
		totalArray=totalArray.concat(forwardArray.splice(forwardArray.length-1,1));
		valueArray=totalArray[totalArray.length-1].split(',');
		value1=valueArray[0];value2=valueArray[1];value3=valueArray[2];
		parent.frames['frmRover'].moveBackward(value1,value2,value3);
		setValues();
		
		//activateButton('btnBackw
		//allowPopup();ard');
		if(forwardArray.length == 0){
			//deactivateButton('btnForward');
		}
	}
}
	
/*---------------------------------------------------------------------------------------------------------*/
//This method is used for loading messages.
function loadmessages()  {	
	if(window.ActiveXObject) {
		addsXml = new ActiveXObject("Microsoft.XMLDOM");
		addsXml.async = false;
		addsXml.load("advertisement.xml");
		processMessage();
	} else if(document.implementation && document.implementation.createDocument){
		/*addsXml = document.implementation.createDocument("","",null);
		addsXml.load("advertisement.xml");
		addsXml.onload=processMessage;*/
		addsXml = loadXMLDoc("advertisement.xml");
		processMessage();
	} else{
		alert('Your browser cannot handle this script');
	}
	
}
/*---------------------------------------------------------------------------------------------------------*/
//This method is used for processing the messages.
function processMessage() {
	var counter = 0;
	var xmlObj = addsXml.documentElement.childNodes;
	var nodeSize=xmlObj.length;
	///alert(nodeSize);
	for(i=0;i<nodeSize;i++) {
		if(xmlObj[i].nodeType == 1) {
			messageid[counter] = xmlObj[i].attributes.getNamedItem("id").nodeValue;
			messagevalue[counter++] = xmlObj[i].attributes.getNamedItem("value").nodeValue;
		}
	}
	//alert("data "+messageid.length);
	addsMessageLoaded = true;
}
/*---------------------------------------------------------------------------------------------------------*/
//This method is used for getting corresponding messages.
function getMessages() {
	//alert(div_id);
	var message = "";
	var message_id = "LP000";
		
	var vect1 = "";
	var vect2 ="";
	var vect3 ="";
	if((value_1 != "" || value_2 != "" || value_3 != "") ) {
		vect1 = value1.replace("Causes & Conditions::","");
		vect2 = value2.replace("Treatments & Tonics::","");
		vect3 = value3.replace("Plans & Providers::","");
	}
	var messageidcriteria = 0;
	var messageIdBinary;
	if(vect3 != "" ) {
		
		//alert("in v3 "+messageidcriteria);
		messageidcriteria = 1 + messageidcriteria; 
	} 
	if(vect2 != "") {
		
		//alert("in v2 "+messageidcriteria);
		messageidcriteria = 2 + messageidcriteria;
		//alert("in v2 "+messageidcriteria);
	} 
	if(vect1 != "") {
			
		//alert("in v1 "+messageidcriteria);
		messageidcriteria = 4 + messageidcriteria;
		//alert("in v1 "+messageidcriteria);
	}
	
	messageIdBinary = Number(messageidcriteria).toString(2).toUpperCase();
	if(messageIdBinary.length == 1 ) {
		messageIdBinary = "00"+ messageIdBinary;
	} else if(messageIdBinary.length == 2) {
		messageIdBinary = "0"+ messageIdBinary;
	}
	//alert(activeSection)
	if(activeSection == 1 && document.getElementById('navBox01WideContainer').style.display=="none") {
		if((vect1.split("::").length == 3  ) || (vect2.split("::").length == 2  ) || (vect3.split("::").length == 2  )) {
			//if((vect1.split("::").length == 3  && vect1 != prevect1 ) || (vect2.split("::").length == 1  && vect2 != prevect2) || (vect3.split("::").length == 1 && vect3 != prevect3 )) {
			message_id = "FRV"+messageIdBinary;
		} else {
			message_id = "SV"+messageIdBinary;
		}
			
	} else if(activeSection == 2) {
		message_id = "CR"+messageIdBinary;
	} else if(activeSection == 3 ) {
		message_id = "WR"+messageIdBinary;
	} else if(activeSection == 4 ) {
		message_id = "WR"+messageIdBinary;
	}
	//alert(message_id);
	prevect1 = vect1;
	prevect2 = vect2;
	prevect3 = vect3;
	//alert(value1+" 2"+value2+" 3"+value3 );
	//alert(activeSection);
	if(messageid != "") {
		//alert(messageid);
		for(i =0; i<messageid.length;i++)  {
			if(messageid[i] == message_id) {
				message = messagevalue[i];
				
			}
		}
	} 
	//alert(message)
	if(document.getElementById("advDiv1")) {
		document.getElementById("advDiv1").innerHTML = message;
	}
		
}
/*---------------------------------------------------------------------------------------------------------*/
	
function switchwindow(zone, currentValue) {
	
	var url = "/ActivityLogging/windowLog.do?&zone="+zone+"&prefocus="+switchwindowpanevalue+"&curfocus="+currentValue;
	
	var ajax = new PrepareAjaxCall(url, vectorLoggedSuccess);
	ajax.makeCall();
	switchwindowpanevalue = currentValue;
}

/*---------------------------------------------------------------------------------------------------------*/
function vectorLogging (v1, v2, v3, paneName) {
	var url = "/ActivityLogging/searchLog.do?&v1="+v1+"&v2="+v2+"&v3="+v3+"&searchloc="+paneName;
	var ajax = new PrepareAjaxCall(url, vectorLoggedSuccess);
	ajax.makeCall();
}
/*---------------------------------------------------------------------------------------------------------*/
var roverAnalysisValue = "";
function roverAnalysisLog(roverId, roverValue, isKeyPresent, popup) {

	var type = "";
	var keyPresent = isKeyPresent == true ? 0 : 1;
	//alert(roverId+" val "+roverValue+" Flag Status "+isKeyPresent)
	if(roverId == "c_search1" || roverId == 1 || roverId == "w_search1")
		type= "CandC";
	else if(roverId == "c_search2" || roverId == 2)
		type= "TandT";
	else if(roverId == "c_search3" || roverId == 3)
		type= "PandP";
	
	if(roverValue != "" && roverValue.indexOf("Causes & Conditions") >= 0) {
		roverValue = roverValue.replace("Causes & Conditions::","");
		roverValue = roverValue.replace("Causes & Conditions","");
	} else if(roverId == 2) {
		roverValue = roverValue.replace("Treatments & Tonics::","");
		roverValue = roverValue.replace("Treatments & Tonics","");
    } else if(roverId == 3) {
		roverValue = roverValue.replace("Plans & Providers::","");
    	roverValue = roverValue.replace("Plans & Providers","");
    }
	
	if( roverValue != "" && ( roverAnalysisValue != roverValue || roverAnalysisValue == "") && roverValue.indexOf("Enter search term here") ) {
		roverAnalysisValue = roverValue;
		var url = "/ActivityLogging/roverAnalysis.do?&vtype="+type+"&vvalue="+roverValue+"&isKey="+keyPresent+"&status="+popup;
		//alert(url);
		var ajax = new PrepareAjaxCall(url, roverAnalysisReturn);
		ajax.makeCall();
	}
}
/*---------------------------------------------------------------------------------------------------------*/
function roverAnalysisReturn(returnText ) {
	
}

/*---------------------------------------------------------------------------------------------------------*/
function vectorLoggedSuccess(responseText) {
		
}
/*---------------------------------------------------------------------------------------------------------*/
var request = GetXmlHttpObject();

function GetXmlHttpObject()
{
if (window.XMLHttpRequest)
  {
  // code for IE7+, Firefox, Chrome, Opera, Safari
  return new XMLHttpRequest();
  }
if (window.ActiveXObject)
  {
  // code for IE6, IE5
  return new ActiveXObject("Microsoft.XMLHTTP");
  }
return null;
}




/*function receiveResponse()
{
if (request.readyState==4)
  {
  document.getElementById("displayVideo").innerHTML=request.responseText;
  }
}*/
/*---------------------------------------------------------------------------------------------------------*/