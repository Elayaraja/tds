var geocoder;
var map;
var marker;
var poly;
var latLng;
var markers = [];
var labels = [];
function initialize(){
//	MAP
	var latitude;
	var longitude;
	latitude=document.getElementById("sLatitude").value;
	longitude=document.getElementById("sLongitude").value;
	var latlng = new google.maps.LatLng(latitude,longitude);
	var options = {
			zoom: 12,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	map = new google.maps.Map(document.getElementById("map_canvas"), options);

	//GEOCODER
	geocoder = new google.maps.Geocoder();
	//showLatLong1();
}

function clearMarkers(){
	while(markers.length){
        markers.pop().setMap(null);
    }
	
	if(poly===undefined || poly===null){
		
	}else{
		poly.setMap(null);
	}
	
	/*while(polylines.length){
		polylines.pop().setMap(null);
	}*/
	if (labels === undefined || labels.length == 0) {
		
	}else{
		while(labels.length){
			labels.pop().setMap(null);
		}
	}
	
	markers = [];
	labels = [];
}
var color = '#EBEEF0';
function showLatLong1() {
	var polyOptions = {
			strokeColor: '#0C7499',
			strokeOpacity: 0.9,
			strokeWeight: 4
	}
	poly = new google.maps.Polyline(polyOptions);
	poly.setMap(map);
	
	var imageBlue="images/blueCar.png";
	var imageGreen="images/greenCar.png";
	var imageRed="images/redCar.png";
	var imageBlack="images/blackCar.png";
	var count=1;
	var oRows1=document.getElementById("driverDetailDisplay").getElementsByTagName("td");
	var iRowCount1= oRows1.length;
	if(iRowCount1<1){
		return;
	}
	var time = "";
	var tripid="";
	var currentLat;
	var previousLat=0.00;
	var distance;
	var fuel;
	var carbon;
	var sameSpot = false;
	var isPrevZDone = true;
	var distance = 0.0;
	
	var centerlat=oRows1[iRowCount1-1].getElementsByTagName("input")[0].value;
	var centerlon=oRows1[iRowCount1-1].getElementsByTagName("input")[1].value;
	var checkLat = 0.0;
	var checkLong = 0.0;
	var count = 1;
	for(var i=0;i<iRowCount1;i++){
		var leftTime;
		lat = oRows1[i].getElementsByTagName("input")[0].value;
		longi = oRows1[i].getElementsByTagName("input")[1].value;
		status=oRows1[i].getElementsByTagName("input")[2].value;
		lastUpdate=oRows1[i].getElementsByTagName("input")[3].value;
		driverId=oRows1[i].getElementsByTagName("input")[4].value;
		time=oRows1[i].getElementsByTagName("input")[5].value;
		tripid=oRows1[i].getElementsByTagName("input")[6].value;
		distance = oRows1[i].getElementsByTagName("input")[7].value;
		//carbon=oRows1[i].getElementsByTagName("input")[7].value;
		var markerDriver;
		latLng = new google.maps.LatLng(lat,longi);
		function infoCallback(infowindow, marker) { 
			return function() { 
				infowindow.open(map, marker);
			}; 
		}
		if(count == 1 && lat > checkLat && longi > checkLong){
//			alert("start Point "+lat+", "+longi);
			stpoint = new google.maps.Marker({
	 				position:new google.maps.LatLng(lat, longi),
	 				map: map,
	 				animation: google.maps.Animation.DROP,
	 				icon:'images/greenMarker1.png',
	 		    });
	 			stpoint.setMap(map);
	 			count++;
		}else if(i == iRowCount1 - 1){
			endpoint = new google.maps.Marker({
 				position:new google.maps.LatLng(lat, longi),
 				map: map,
 				animation: google.maps.Animation.DROP,
 				icon:'images/pinkMarker1.png',
 		    });
			endpoint.setMap(map);
		}
			if(status=="0"){
				isPrevZDone = false;
				continue;
			}
			
			if(lat=="0.000000"){
				continue;
			}
			
			if(!isPrevZDone){
				markerDriver = new google.maps.Marker({
					position : latLng,
					map : map,
					icon : imageRed
				});
				var infoWindow = new google.maps.InfoWindow({
					content : "Driver "+driverId+" logged In "+time
				});
				infoWindow.set('position',new google.maps.LatLng(lat,longi));
				google.maps.event.addListener(markerDriver, 'click',infoCallback(infoWindow,markerDriver));
				
				var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
				  jobLabel.set('position', new google.maps.LatLng(lat,longi));
				  jobLabel.set('text',"LogIn");
				  
				  labels.push(jobLabel);
			}else{
				
				var infoWindow = null;
				
				if(status=="1"){
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageRed
					});
					
					infoWindow = new google.maps.InfoWindow({
						content : "Driver "+driverId+" logged Out on "+time
					});
					
					var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
					  jobLabel.set('position', new google.maps.LatLng(lat,longi));
					  jobLabel.set('text',"logout");
					  
					  labels.push(jobLabel);
					  
				}else if(status=="2"){
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlue
					});
					infoWindow = new google.maps.InfoWindow({
						content : "Driver "+driverId+" accept the trip:"+tripid+" on "+time
					});
					
					var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
					  jobLabel.set('position', new google.maps.LatLng(lat,longi));
					  jobLabel.set('text',tripid+" Accept");
					  
					  labels.push(jobLabel);
				}else if(status=="3"){
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlue
					});
					infoWindow = new google.maps.InfoWindow({
						content : "Driver "+driverId+" OnRoute the trip:"+tripid+"  on "+time
					});
					
					var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
					  jobLabel.set('position', new google.maps.LatLng(lat,longi));
					  jobLabel.set('text',tripid+ " OnRoute");
					  
					  labels.push(jobLabel);
					  
				}else if(status=="4"){
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlue
					});
					infoWindow = new google.maps.InfoWindow({
						content : "Driver "+driverId+" Onsite the trip:"+tripid+"  on "+time
					});
					
					var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
					  jobLabel.set('position', new google.maps.LatLng(lat,longi));
					  jobLabel.set('text',tripid+" Onsite");
					  
					  labels.push(jobLabel);
					  
				}else if(status=="5"){
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlue
					});
					infoWindow = new google.maps.InfoWindow({
						content : "Driver "+driverId+" Start the trip:"+tripid+"  on "+time
					});
					
					var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
					  jobLabel.set('position', new google.maps.LatLng(lat,longi));
					  jobLabel.set('text',tripid+" Started");
					  
					  labels.push(jobLabel);
					  
				}else if(status=="6"){
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlue
					});
					infoWindow = new google.maps.InfoWindow({
						content : "Driver "+driverId+" STC the trip:"+tripid+"  on "+time
					});
					
					var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
					  jobLabel.set('position', new google.maps.LatLng(lat,longi));
					  jobLabel.set('text',tripid+" STC");
					  
					  labels.push(jobLabel);
					  
				}else if(status=="7"){
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon : imageBlue
					});
					infoWindow = new google.maps.InfoWindow({
						content : "Driver "+driverId+" End the trip:"+tripid+"  on "+time
					});
					
					var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
					  jobLabel.set('position', new google.maps.LatLng(lat,longi));
					  jobLabel.set('text',tripid+" Ended");
					  
					  labels.push(jobLabel);
					  
				}else{
					markerDriver = new google.maps.Marker({
						position : latLng,
						map : map,
						icon: {
	 						    path: google.maps.SymbolPath.CIRCLE,
	 						    fillOpacity: 0.5,
	 						    fillColor: "#000000",
	 						    strokeOpacity: 1.0,
	 						    strokeColor: "#0C7499",
	 						    strokeWeight: 5.0
	 						  }
					});
					infoWindow = new google.maps.InfoWindow({
						content : "Updated time : "+time+". Dist. travelled : "+distance +" Km"
					});
					
					if(i==0 || i==(iRowCount1-1)){
						var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.3,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
						  jobLabel.set('position', new google.maps.LatLng(lat,longi));
						  
						if(i==0){
							jobLabel.set('text',"Start on "+time);
						}else{
							jobLabel.set('text',"End on "+time+" with DT:"+distance +" Km");
						}
						
						labels.push(jobLabel);
					}
				}
				
				infoWindow.set('position',new google.maps.LatLng(lat,longi));
				google.maps.event.addListener(markerDriver, 'click',infoCallback(infoWindow,markerDriver));
			}
			
			isPrevZDone = true;
			
			currentLat= oRows1[i].getElementsByTagName("input")[0].value;
				
		markerDriver.setMap(map);
		var path = poly.getPath();
		path.push(latLng);
		
		markers.push(markerDriver);
	}
	
	if(centerlat!=null && centerlat!=0.000000){
		var centerCoord = new google.maps.LatLng(centerlat,centerlon);
		map.setZoom(15);
		map.setCenter(centerCoord);
	}
	
}

$(document).ready(function() {
	initialize();
});