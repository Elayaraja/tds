var geocoder;
var map;
var marker;
var image;
var directionsDisplay;
var zoomLevel;
function initializeMapHTML(type){
//	MAP
	var latitude="";
	var longitude="";
	if(type==1){
		latitude=document.getElementById("sLatitude").value;
		longitude=document.getElementById("sLongitude").value;
	  	image="http://maps.google.com/mapfiles/ms/icons/green-dot.png";
		zoomLevel=16;
	} else if(type==3) {
		latitude=document.getElementById("defaultLati").value;
		longitude=document.getElementById("defaultLongi").value;
		image="http://itouchmap.com/i/blue-dot.png";
		zoomLevel=12;
	}
	else if(type==2){
		latitude=document.getElementById("eLatitude").value;
		longitude=document.getElementById("eLongitude").value;
	  	image="http://maps.google.com/mapfiles/ms/icons/red-dot.png";
		zoomLevel=16;
	}

	var latlng = new google.maps.LatLng(latitude,longitude);

	directionsDisplay = new google.maps.DirectionsRenderer();
	var options = {
			zoom: zoomLevel,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), options);
   directionsDisplay.setMap(map);
	//GEOCODER
	geocoder = new google.maps.Geocoder();

	marker = new google.maps.Marker({position:latlng,
		map: map,icon:image,draggable: true});
	marker.setMap(map);

}
	geocoder = new google.maps.Geocoder();
	$(function() {
		$("#sadd1").autocomplete({
			//This bit uses the geocoder to fetch address values
			source: function(request, response) {
				var defaultState = document.getElementById("sstate").value
				var defaultCountry=document.getElementById("defaultCountry").value;

				geocoder.geocode( {'address': request.term +', '+defaultState+', ' + defaultCountry  }, function(results, status) {
					response($.map(results, function(item) {
						return {
							label: item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							city: item.postal_code,
							addcomp: item.address_components
							//nhd: item.address_components_of_type("neighborhood")
						}
					}));
				})
			},
			//This bit is executed upon selection of an address
			select: function(event, ui) {

				var arrAddress =ui.item.addcomp;
				var streetnum= "";
				var route = "";


				// iterate through address_component array
				$.each(arrAddress, function (i, address_component) {
					if (address_component.types[0] == "street_number"){
						$("#sadd1").val(address_component.long_name);
						streetnum = address_component.long_name;
					}

					if (address_component.types[0] == "locality"){
						$("#scity").val(address_component.long_name);
					}

					if (address_component.types[0] == "route"){ 
						route = address_component.long_name;
					}
					if (address_component.types[0] == "country"){ 
						itemCountry = address_component.long_name;
					}


					if (address_component.types[0] == "postal_code"){ 
						itemPc = address_component.long_name;
							$("#szip").val(address_component.long_name);
					}
					//return false; // break the loop

					});
				$("#addTemp").val(streetnum + " " + route);
				$("#sLatitude").val(ui.item.latitude);
				$("#sLongitude").val(ui.item.longitude);

				var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
				marker.setPosition(location);
				map.setCenter(location);
				initializeMapHTML(1);
				$("#address").autocomplete("close");
			}
		});
	});

	$(function() {
		$("#eadd1").autocomplete({
			//This bit uses the geocoder to fetch address values
			source: function(request, response) {
				var defaultState = document.getElementById("sstate").value
				var defaultCountry=document.getElementById("defaultCountry").value;
				geocoder.geocode( {'address': request.term +', '+defaultState+', ' + defaultCountry  }, function(results, status) {
					response($.map(results, function(item) {
						return {
							label: item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							city: item.postal_code,
							addcomp: item.address_components
							//nhd: item.address_components_of_type("neighborhood")
						}
					}));
				})
			},
			//This bit is executed upon selection of an address
			select: function(event, ui) {

				var arrAddress =ui.item.addcomp;
				var streetnum= "";
				var route = "";


				// iterate through address_component array
				$.each(arrAddress, function (i, address_component) {

					if (address_component.types[0] == "street_number"){
						$("#eadd1").val(address_component.long_name);
						streetnum = address_component.long_name;
					}

					if (address_component.types[0] == "locality"){
						$("#ecity").val(address_component.long_name);
					}

					if (address_component.types[0] == "route"){ 
						route = address_component.long_name;
					}
					if (address_component.types[0] == "country"){ 
						itemCountry = address_component.long_name;
					}


					if (address_component.types[0] == "postal_code"){ 
						itemPc = address_component.long_name;
						$("#ezip").val(address_component.long_name);
					}

					//return false; // break the loop

				});

				$("#eAddTemp").val(streetnum + " " + route);
				$("#eLatitude").val(ui.item.latitude);
				$("#eLongitude").val(ui.item.longitude);


				var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
				marker.setPosition(location);
				map.setCenter(location);
				initializeMapHTML(2);
				$("#endAddress").autocomplete("close");

			}
		});
});
function markerDrag(){
	google.maps.event.addListener(marker, 'dragend', function() {
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var arrAddress=  results[0].address_components;
					//var arrAddress1=  results[1].address_components
					var streetnum;
					var route;
					$('#address').val(results[0].formatted_address);
					$('#sLatitude').val(marker.getPosition().lat());
					$('#sLongitude').val(marker.getPosition().lng());
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "street_number"){
							$("#sadd1").val(address_component.long_name);
							$("#sadd1Small").val(address_component.long_name);
							streetnum = address_component.long_name;
						}
						if (address_component.types[0] == "locality"){
							$("#scity").val(address_component.long_name);
							$("#scitySmall").val(address_component.long_name);
						}
						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}
						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#szip").val(address_component.long_name);
							$("#szipSmall").val(address_component.long_name);
						}
					});
					$("#addTemp").val(streetnum + " " + route);
					$("#sadd1").val(streetnum + " " + route);
					$("#sadd1Small").val(streetnum + " " + route);
				}
			}
		}); 
	});
}
function markerDragTo(){
	google.maps.event.addListener(marker, 'dragend', function() {
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var arrAddress=  results[0].address_components;
					//var arrAddress1=  results[1].address_components
					var streetnum;
					var route;
					$('#endAddress').val(results[0].formatted_address);
					$('#eLatitude').val(marker.getPosition().lat());
					$('#eLongitude').val(marker.getPosition().lng());
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "street_number"){
							$("#eadd1").val(address_component.long_name);
							$("#eadd1Small").val(streetnum + " " + route);
							streetnum = address_component.long_name;
						}
						if (address_component.types[0] == "locality"){
							$("#ecity").val(address_component.long_name);
							$("#ecitySmall").val(address_component.long_name);
						}
						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}
						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#ezip").val(address_component.long_name);
							$("#ezipSmall").val(address_component.long_name);
						}
					});
					$("#eadd1").val(streetnum + " " + route);
					$("#eAddTemp").val(streetnum + " " + route);
					$("#eadd1Small").val(streetnum + " " + route);
				}
			}
		}); 
	});
}

