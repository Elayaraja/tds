function showReports(){
	document.getElementById("fromDate").value="";
	document.getElementById("toDate").value="";
	document.getElementById("Date").value="";
	document.getElementById("tripId").value="";
	$("#oneDate").hide("slow");
	$("#basedTrip").hide("slow");
	$("#fromAndToDate").show("slow");
	$("#flagDetails").hide("slow");
}
function showSplFlags(){
	document.getElementById("flagDetailsTb").innerHTML ="";
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/ReportController?event=getSplFlags';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	$("#flagDetails").show();
	var obj = "{\"flags\":"+text+"}";
	jsonObj = JSON.parse(obj.toString());
	var container = $('#flagDetailsTb');
	for(var i=0;i<jsonObj.flags.length;i++){
		  var inputs = container.find('input');
		   var id = inputs.length+1;
		   $('<td><input type="checkbox" name="fl'+id+'" id="fl'+id+'" value="'+jsonObj.flags[i].FV+'" onclick="checkFlags('+id+')">').appendTo(container);
		   $('<label>'+jsonObj.flags[i].FD+'</label></td>' ).appendTo(container);
	}
	
}
function showVoucher(){
	$("#showVoucher").show("slow");
}
function checkFlags(id){
	var oRows1=document.getElementById("flagDetails").getElementsByTagName("td");
	var iRowCount1= oRows1.length;
	var count = 0;
	if(iRowCount1>5){
	for(var i=0;i<iRowCount1;i++){
		 fl = oRows1[i].getElementsByTagName("input")[0].value;
		   if(oRows1[i].getElementsByTagName("input")[0].checked){
			   count ++;
		   }
	}
	if(count>5){
		alert("please select maximum 5 values !!");
		document.getElementById("fl"+id).checked=false;
	}
}
}
function showOneDate(){
	document.getElementById("fromDate").value="";
	document.getElementById("toDate").value="";
	document.getElementById("Date").value="";
	document.getElementById("tripId").value="";
	$("#fromAndToDate").hide("slow");
	$("#basedTrip").hide("slow");
	$("#oneDate").show("slow");
	$("#flagDetails").hide("slow");
	$("#showVoucher").hide("slow");
}
function showTrip(){
	document.getElementById("fromDate").value="";
	document.getElementById("toDate").value="";
	document.getElementById("Date").value="";
	document.getElementById("tripId").value="";
	$("#fromAndToDate").hide("slow");
	$("#oneDate").hide("slow");
	$("#basedTrip").show("slow");
	$("#flagDetails").hide("slow");
	$("#showVoucher").hide("slow");

}
function goToController(){

	var stdate= dojo.date.locale.format(dijit.byId("fromDate").value,{datePattern: "yyyy-MM-dd", selector: "date"});
	var edDate= dojo.date.locale.format(dijit.byId("toDate").value,{datePattern: "yyyy-MM-dd", selector: "date"});
	var cDate=dojo.date.locale.format(dijit.byId("Date").value,{datePattern: "yyyy-MM-dd", selector: "date"});
	var reportNo = document.getElementById("reportNo").value;
	var tripId = document.getElementById("tripId").value;
	var voucherNo = document.getElementById("voucherNo").value;
	var oRows1=document.getElementById("flagDetails").getElementsByTagName("td");
	var output = document.getElementById("selectOutPut").value;
	var iRowCount1= oRows1.length;
	var flagsSelected ="";
	var fl ="";
	for(var i=0;i<iRowCount1;i++){
		 fl = oRows1[i].getElementsByTagName("input")[0].value;
		   if(oRows1[i].getElementsByTagName("input")[0].checked){
			   flagsSelected += fl +",";
		   }
	}
	flagsSelected = flagsSelected.substring(0,flagsSelected.length-1);
	
	if(reportNo=='1'){
		window.open("ReportController?event=reportEvent&ReportName=dailyDispatchPerformance&fromDate="+stdate+"&toDate="+edDate+"&output="+output+"&type=fromAndToDate","","locattion=no,status=no,menubar=no");
	}else if(reportNo==2){
		window.open("ReportController?event=reportEvent&ReportName=dailyJobCount&output="+output+"&type=oneDate&date="+cDate+"","","locattion=no,status=no,menubar=no");
	}else if(reportNo==3){
		window.open("ReportController?event=reportEvent&ReportName=operatorPerformance&output="+output+"&type=oneDate&date="+cDate+"","","locattion=no,status=no,menubar=no");
	}else if(reportNo==4){
		window.open("ReportController?event=reportEvent&ReportName=monthlyConsolidatedStatisticsGraph&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDate","","locattion=no,status=no,menubar=no");
	}else if(reportNo==5){
		window.open("ReportController?event=reportEvent&ReportName=monthlyJobCountGraph&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDate","","locattion=no,status=no,menubar=no");
	}else if(reportNo==6){
		window.open("ReportController?event=reportEvent&ReportName=jobStatistics&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDate","","locattion=no,status=no,menubar=no");
	}else if(reportNo==7){
		window.open("ReportController?event=reportEvent&ReportName=tripDetail&output="+output+"&tripId="+tripId+"&type=basedTrip","","locattion=no,status=no,menubar=no");
	}else if(reportNo==8){
		window.open("ReportController?event=reportEvent&ReportName=specailFlags&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&flags="+flagsSelected+"&type=flags","","locattion=no,status=no,menubar=no");
	}else if(reportNo==9){
		window.open("ReportController?event=reportEvent&ReportName=driverTripSheet&output="+output+"&type=oneDate&date="+cDate+"","","locattion=no,status=no,menubar=no");
	}else if(reportNo==10){
		window.open("ReportController?event=reportEvent&ReportName=ReservedJobs&output="+output+"&voucherNo="+voucherNo+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDateVoucher","","locattion=no,status=no,menubar=no");
	}else if(reportNo==11){
		window.open("ReportController?event=reportEvent&ReportName=invoiceByDriver&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDate","","locattion=no,status=no,menubar=no");
	}else if(reportNo==12){
		window.open("ReportController?event=reportEvent&ReportName=orangeTaxi&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDate","","locattion=no,status=no,menubar=no");
	}else if(reportNo==13){
		window.open("ReportController?event=reportEvent&ReportName=driverDisbursement&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDate","","locattion=no,status=no,menubar=no");
	}else if(reportNo==14){
		window.open("ReportController?event=reportEvent&ReportName=dispatcherShiftDetails&output="+output+"&type=oneDate&date="+cDate+"","","locattion=no,status=no,menubar=no");
	}else if(reportNo==15){
		window.open("ReportController?event=reportEvent&ReportName=responseOfACallByCity&output="+output+"&type=oneDate&date="+cDate+"","","locattion=no,status=no,menubar=no");
	}else if(reportNo==16){
		window.open("ReportController?event=reportEvent&ReportName=DriverDeactivationHistory&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDate","","locattion=no,status=no,menubar=no");
	}else if(reportNo==17){
		window.open("ReportController?event=reportEvent&ReportName=OperatorTODriverJobCount&output="+output+"&type=oneDate&date="+cDate+"","","locattion=no,status=no,menubar=no");
	}else if(reportNo==18){
		window.open("ReportController?event=reportEvent&ReportName=manuallyChangedTrip&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDateNew","","locattion=no,status=no,menubar=no");
	}else if(reportNo==19){
		window.open("ReportController?event=reportEvent&ReportName=driverShiftDetail&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDateNew","","locattion=no,status=no,menubar=no");
	}else if(reportNo==20){
		window.open("ReportController?event=reportEvent&ReportName=DriverJobDetails&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDateNew","","locattion=no,status=no,menubar=no");
	}else if(reportNo==21){
		window.open("ReportController?event=reportEvent&ReportName=driverLogouts&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDateNew","","locattion=no,status=no,menubar=no");
	}else if(reportNo==22){
		window.open("ReportController?event=reportEvent&ReportName=tripLogs&output="+output+"&fromDate="+stdate+"&toDate="+edDate+"&type=fromAndToDateNew","","locattion=no,status=no,menubar=no");
	}

}
function reportNo(reportNo){
	document.getElementById("reportNo").value = reportNo;
}
function category(categoryNo){
	$("#fromAndToDate").hide("slow");
	$("#oneDate").hide("slow");
	$("#basedTrip").hide("slow");
	if(categoryNo=='1'){
		$("#ReportTypes").hide("slow");
		$("#reportTwo").hide("slow");
		$("#reportThree").hide("slow");
		$("#reportFour").hide("slow");
		$("#reportFive").hide("slow");
		$("#reportOne").show("slow");
	}else if(categoryNo=='all'){
		$("#reportOne").hide("slow");
		$("#reportTwo").hide("slow");
		$("#reportThree").hide("slow");
		$("#reportFour").hide("slow");
		$("#reportFive").hide("slow");
		$("#ReportTypes").show("slow");
	}else if(categoryNo=='2'){
		$("#ReportTypes").hide("slow");
		$("#reportThree").hide("slow");
		$("#reportOne").hide("slow");
		$("#reportFour").hide("slow");
		$("#reportFive").hide("slow");
		$("#reportTwo").show("slow");
	}else if(categoryNo=='3'){
		$("#ReportTypes").hide("slow");
		$("#reportTwo").hide("slow");
		$("#reportOne").hide("slow");
		$("#reportFour").hide("slow");
		$("#reportFive").hide("slow");
		$("#reportThree").show("slow");
	}else if(categoryNo=='4'){
		$("#reportOne").hide("slow");
		$("#ReportTypes").hide("slow");
		$("#reportTwo").hide("slow");
		$("#reportThree").hide("slow");
		$("#reportFive").hide("slow");
		$("#reportFour").show("slow");
	}if(categoryNo=='5'){
		$("#ReportTypes").hide("slow");
		$("#reportTwo").hide("slow");
		$("#reportThree").hide("slow");
		$("#reportFour").hide("slow");
		$("#reportOne").hide("slow");
		$("#reportFive").show("slow");
	}
}