var geocoder;
var mapDetails;
var markerDriverDetails;
var poly;
var latLng;

function initializeForMapDetails(){
//	MAP
	var latitude;
	var longitude;
	var imageGreen="images/greenCar.png";
	latitude=document.getElementById("sLatitude").value==null?"0.00000":document.getElementById("sLatitude").value;
	longitude=document.getElementById("sLongitude").value==null?"0.00000":document.getElementById("sLongitude").value;
	var latlng="";
	if(latitude!=0.00000 && longitude!=0.000000){
		latlng = new google.maps.LatLng(latitude,longitude);
	}else{
		latitude=document.getElementById("defaultLatitude").value;
		longitude=document.getElementById("defaultLongitude").value;
		latlng = new google.maps.LatLng(latitude,longitude);
	}
	var options="";
	/*if((latitude!=0.00000 && longitude!=0.00000) && (latitude!="" && longitude!="")){*/
	if(latitude==document.getElementById("defaultLatitude").value && longitude==document.getElementById("defaultLongitude").value){

		options = {
				zoom: 6,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	}else{
		options = {
				zoom: 12,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
		}; 
	}
	/*}else{
		latitude=document.getElementById("defaultLatitude").value;
		longitude=document.getElementById("defaultLongitude").value;
		var latlng1 = new google.maps.LatLng(latitude,longitude);
		 options = {
				zoom: 12,
				center: latlng1,
				mapTypeId: google.maps.MapTypeId.ROADMAP
		};
	}*/
	mapDetails = new google.maps.Map(document.getElementById("map_canvas_location"), options);

	//GEOCODER
	/*geocoder = new google.maps.Geocoder();
	markerDriverDetails = new google.maps.Marker({
		position : latLng,
		map : mapDetails,
		icon : imageGreen
	});
	//markerDriverDetails.setMap(mapDetails);
	alert("marker set");*/
	var polyOptions = {
			strokeColor: '#F52887',
			strokeOpacity: 0.9,
			strokeWeight: 4
	};
	poly = new google.maps.Polyline(polyOptions);
	poly.setMap(mapDetails);
	showLatLong1();
}
function showLatLong1() {

	var oRows1=document.getElementById("driverDetailDisplayHistory").getElementsByTagName("td");
	var iRowCount1= oRows1.length;
	for(var i=0;i<iRowCount1;i++){
		lat = oRows1[i].getElementsByTagName("input")[0].value;
		longi = oRows1[i].getElementsByTagName("input")[1].value;
		status=oRows1[i].getElementsByTagName("input")[2].value;
		lastUpdate=oRows1[i].getElementsByTagName("input")[3].value;
		driverId=oRows1[i].getElementsByTagName("input")[4].value;
		if((lat!=0.00000 && longi!=0.00000 ) && (lat!="" && longi!="")){
			latLng = new google.maps.LatLng(lat,longi);
		}

		if(true){
			if(lat==0.00000 || longi==0.00000){
			}else{
				markerDriverDetails = new google.maps.Marker({
					position : latLng,
					map : mapDetails,
					icon : "images/greenCar.png"
				});
				markerDriverDetails.setMap(mapDetails);

				var path = poly.getPath();
				path.push(latLng);
			}
		}
	}
}

$(document).ready(function() {
	/*initialize();*/
});