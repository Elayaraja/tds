var geocoder;
var map;
var marker;
var image;
//var directionsService = new google.maps.DirectionsService();

$(document).ready(function() {
	geocoder = new google.maps.Geocoder();
	$(function() {
		$("#userAddress").autocomplete({
			//This bit uses the geocoder to fetch address values
			source: function(request, response) {
				var defaultState = document.getElementById("defaultState").value;
				var defaultCountry=document.getElementById("defaultCountry").value;

				geocoder.geocode( {'address': request.term +', '+defaultState+', ' + defaultCountry  }, function(results, status) {
					response($.map(results, function(item) {
						return {
							label: item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							city: item.postal_code,
							addcomp: item.address_components
							//nhd: item.address_components_of_type("neighborhood")
						}
					}));
				})
			},
			//This bit is executed upon selection of an address
			select: function(event, ui) {

				var arrAddress =ui.item.addcomp;
				var streetnum= "";
				var route = "";


				// iterate through address_component array
				$.each(arrAddress, function (i, address_component) {
					if (address_component.types[0] == "street_number"){
						$("#address1").val(address_component.long_name);
						streetnum = address_component.long_name;
					}

					if (address_component.types[0] == "locality"){
						$("#city").val(address_component.long_name);
					}
					if (address_component.types[0] == "administrative_area_level_1"){ 
						$("#state").val(address_component.short_name);
					}
					if (address_component.types[0] == "route"){ 
						route = address_component.long_name;
					}
					if (address_component.types[0] == "country"){ 
						itemCountry = address_component.long_name;
					}
					if (address_component.types[0] == "postal_code"){ 
						itemPc = address_component.long_name;
							$("#zip").val(address_component.long_name);
					}
					//return false; // break the loop
					});

				$("#address1").val(streetnum + " " + route);
				$("#latitude").val(ui.item.latitude);
				$("#longitude").val(ui.item.longitude);

			}
		});
	});
});
