var map;

function initializeMap(type) {
//  map = new L.Map('map_canvas', {zoomControl: false});
//	var latitude=document.getElementById("defaultLati").value;
//	var longitude=document.getElementById("defaultLongi").value;

//  var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
//    osm = new L.TileLayer(osmUrl, {maxZoom: 18});
//
//  map.setView(new L.LatLng(latitude, longitude), 12).addLayer(osm);

	var latitude="";
	var longitude="";
	if(type==1){
		latitude=document.getElementById("sLatitude").value;
		longitude=document.getElementById("sLongitude").value;
	  	image="http://maps.google.com/mapfiles/ms/icons/green-dot.png";
	} else if(type==3) {
		latitude=document.getElementById("defaultLati").value;
		longitude=document.getElementById("defaultLongi").value;
		image="http://itouchmap.com/i/blue-dot.png";
	}
	else if(type==2){
		latitude=document.getElementById("eLatitude").value;
		longitude=document.getElementById("eLongitude").value;
	  	image="http://maps.google.com/mapfiles/ms/icons/red-dot.png";
	}
	var latlng = new google.maps.LatLng(latitude,longitude);

	var options = {
			zoom: 12,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), options);
	//GEOCODER
	geocoder = new google.maps.Geocoder();

	marker = new google.maps.Marker({position:latlng,
		map: map,icon:image,draggable: true});
	marker.setMap(map);

}
function searchAddress(type){
	if((type==1 && document.getElementById("address").value.length>2)||(type==2 && document.getElementById("endAddress").value.length>2)){
		addr_search(type);
	} else {
		alert("Sorry give atleast 3 characters");
	}
}
function closeResults(){
	document.getElementById('resultsOSM').innerHTML="";
	document.getElementById("address").value="";
	document.getElementById("addressRed").value="";
	document.getElementById("endAddress").value="";
	document.getElementById("endAddressRed").value="";
}

function addr_search(searchOption) {
	var inp="";
	if(searchOption==1){
		inp= document.getElementById("address");
	} else {
		inp= document.getElementById("endAddress");
	}
	var defaultState = document.getElementById("sstate").value;
    var defaultCountry=document.getElementById("defaultCountry").value;
  $.getJSON('http://nominatim.openstreetmap.org/search?format=json&limit=5&q=' + inp.value+"+"+defaultState+"+"+defaultCountry, function(data) {

var items = [];

$.each(data, function(key, val) {
//	alert(data);
//	var jsonObj = "{\"place_id\":"+data+"}" ;
//	alert(jsonObj);
//	var obj = JSON.parse(jsonObj.toString());
//	alert(obj);
//	for(var j=0;j<obj.place_id.length;j++){
//		var display_name=obj.address[j].display_name;
//		alert(display_name);
//	}
  items.push(
    "<li><a href='#' onclick='chooseAddr(" +
    val.lat + ", " + val.lon + ");return false;'>" + val.display_name +
    '</a></li>'
  );
	if(searchOption==1){
		document.getElementById("addressRed").value=val.display_name;
		document.getElementById("endAddressRed").value="";
	} else {
		document.getElementById("endAddressRed").value=val.display_name;
		document.getElementById("addressRed").value="";
	}
});
$('#resultsOSM').empty();
    if (items.length != 0) {
      $('<ul/>', {
        'class': 'my-new-list',
        html: items.join('')
      }).appendTo('#resultsOSM');
    } else {
      $('<p>', { html: "No results found" }).appendTo('#resultsOSM');
    }
  });
}
function chooseAddr(lat, lng, type) {
	var addressNew=document.getElementById("addressRed").value;
	if(addressNew!=""){
		var addressParsed=addressNew.split(",");
		$("#sadd1").val(addressParsed[0]);
		$("#sadd2").val(addressParsed[1]);
		$("#scity").val(addressParsed[2]);
		$("#szip").val(addressParsed[5]);
		document.getElementById("sLatitude").value=lat;
		document.getElementById("sLongitude").value=lng;
		initializeMap(1);
	} else {
		addressNew=document.getElementById("endAddressRed").value;
		var addressParsed=addressNew.split(",");
		$("#eadd1").val(addressParsed[0]);
		$("#eadd2").val(addressParsed[1]);
		$("#ecity").val(addressParsed[2]);
		$("#ezip").val(addressParsed[5]);
		document.getElementById("eLatitude").value=lat;
		document.getElementById("eLongitude").value=lng;
		initializeMap(2);
	}
	closeResults();
}
$(document).ready(function() {
	initializeMap(3);markerDrag();
});
function markerDragTo(){
	google.maps.event.addListener(marker, 'dragend', function() {
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var arrAddress=  results[0].address_components;
					//var arrAddress1=  results[1].address_components
					var streetnum;
					var route;
					$('#endAddress').val(results[0].formatted_address);
					$('#eLatitude').val(marker.getPosition().lat());
					$('#eLongitude').val(marker.getPosition().lng());
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "street_number"){
							$("#eadd1").val(address_component.long_name);
							$("#eadd1Small").val(streetnum + " " + route);
							streetnum = address_component.long_name;
						}
						if (address_component.types[0] == "locality"){
							$("#ecity").val(address_component.long_name);
							$("#ecitySmall").val(address_component.long_name);
						}
						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}
						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#ezip").val(address_component.long_name);
							$("#ezipSmall").val(address_component.long_name);
						}
					});
					$("#eadd1").val(streetnum + " " + route);
					$("#eadd1Small").val(streetnum + " " + route);
					checkOldAddress(2);
				}
			}
		}); 
	});
}
function markerDrag(){
	google.maps.event.addListener(marker, 'dragend', function() {
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var arrAddress=  results[0].address_components;
					//var arrAddress1=  results[1].address_components
					var streetnum;
					var route;
					$('#address').val(results[0].formatted_address);
					$('#sLatitude').val(marker.getPosition().lat());
					$('#sLongitude').val(marker.getPosition().lng());
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "street_number"){
							$("#sadd1").val(address_component.long_name);
							$("#sadd1Small").val(address_component.long_name);
							streetnum = address_component.long_name;
						}
						if (address_component.types[0] == "locality"){
							$("#scity").val(address_component.long_name);
							$("#scitySmall").val(address_component.long_name);
						}
						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}
						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#szip").val(address_component.long_name);
							$("#szipSmall").val(address_component.long_name);
						}
					});
					$("#sadd1").val(streetnum + " " + route);
					$("#sadd1Small").val(streetnum + " " + route);
					checkOldAddress(1);
				}
			}
		}); 
	});
}

