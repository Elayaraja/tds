(function($){
	var initLayout = function() {
		var hash = window.location.hash.replace('#', '');
		var currentTab = $('ul.navigationTabs a')
							.bind('click', showTab)
							.filter('a[rel=' + hash + ']');
		if (currentTab.size() == 0) {
			currentTab = $('ul.navigationTabs a:first');
		}
		showTab.apply(currentTab.get(0));
		$('#colorpickerHolder').ColorPicker({flat: true});
		$('#colorpickerHolder2').ColorPicker({
			flat: true,
			color: '#00ff00',
			onSubmit: function(hsb, hex, rgb) {
				$('#colorSelector2 div').css('backgroundColor', '#' + hex);
			}
		});
		$('#colorpickerHolder2>div').css('position', 'absolute');
		var widt = false;
		$('#colorSelector2').bind('click', function() {
			$('#colorpickerHolder2').stop().animate({height: widt ? 0 : 173}, 500);
			widt = !widt;
		});
		$('.fleetlist,#avColor, #navColor, #noUpdate, #vipColor, #timeColor, #voucherColor, #cashColor,#zoneColor1,#zoneColor2,#zoneColor3,#zoneColor4,#zoneColor5,#zoneColor6,#zoneColor7,#zoneColor8,#zoneColor9,#zoneColor10,#fcolor').ColorPicker({
			onChange: function (hsb, hex, rgb, el) {
				$(el).css('backgroundColor', '#' + hex);
				$(el).val(hex);
				//alert(el);
				/*if(el=="avColor"){
					ccAvColor();
				}else if(el=="navColor"){
					ccAvColor();
				}else if(el=="noUpdate"){
					ccNoUpdate();
				}else if(el=="vipColor"){
					ccForVip();
				}else if(el=="timeColor"){
					ccForTimeCall();
				}else if(el=="voucherColor"){
					ccForVoucher();
				}else if(el=="cashColor"){
					ccForCash();
				}*/
			},
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
				$(el).css('backgroundColor', '#' + hex);
				createCookieForColors();
				createCookieForZoneColors();
				createCookieForFleet();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
			
		})
		.bind('keyup', function(){
			$(this).ColorPickerSetColor(this.value);
		});
		$('#colorSelector').ColorPicker({
			color: '#24f511',
			onShow: function (colpkr) {
				$(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				$(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				$('#colorSelector div').css('backgroundColor', '#' + hex);
				document.getElementById("colorCode").value=hex;
			}
		});
	};
	
	
	var showTab = function(e) {
		var tabIndex = $('ul.navigationTabs a')
							.removeClass('active')
							.index(this);
		$(this)
			.addClass('active')
			.blur();
		$('div.tab')
			.hide()
				.eq(tabIndex)
				.show();
	};
	
	EYE.register(initLayout, 'init');
})(jQuery)