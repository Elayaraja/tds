var geocoder;
var map;
var mapRoute;
var marker;
var image;
var directionsDisplay;
//var directionsService = new google.maps.DirectionsService();

function extractAddressFromPlaceResult(place, fieldBackup) {
	var street_no = null, street_name = null, postal = null, locality = null;

	if(place == undefined || place == null || place.address_components == undefined || place.address_components == null)
		return {"valid":false};

		for(var i=0;place.address_components.length>i;i++)
			for(var j=0;place.address_components[i].types.length>j;j++)
			{
				var t = place.address_components[i].types[j];
				if(t == 'route')
					street_name = place.address_components[i].long_name;
				else if(t == 'street_number')
					street_no = place.address_components[i].long_name;
				else if(t == 'postal_code')
					postal = place.address_components[i].long_name;
				else if(t == 'locality')
					locality = place.address_components[i].long_name;
			}

		if(!(street_no != null || fieldBackup == null))
		{
			var m = jQuery(fieldBackup).val().match(/^([0-9][0-9]*)[^0-9]/);
			if(m == null || m.length != 2)
				; // do nothing
			else
				street_no = m[1];
		}

		return {
			"valid": street_name != null,
			"street_no": street_no,
			"street_name": street_name,
			"locality": locality,
			"postal": postal };
}
function myFunction(type)
{
	if(type==1){
		setTimeout(function(){initializeMap(type);markerDrag();},300);
	} else {
		setTimeout(function(){initializeMap(type);markerDragTo();},300);
	}
}
function initializeMap(type){
//	MAP
	var latitude="";
	var longitude="";
	if(type==1){
		latitude=document.getElementById("sLatitude").value;
		longitude=document.getElementById("sLongitude").value;
		image="http://maps.google.com/mapfiles/ms/icons/green-dot.png";
		checkZone(latitude,longitude,type);
	} else if(type==3) {
		latitude=document.getElementById("defaultLati").value;
		longitude=document.getElementById("defaultLongi").value;
		image="http://itouchmap.com/i/blue-dot.png";
		//	markerDrag();
	}
	else if(type==2){
		latitude=document.getElementById("eLatitude").value;
		longitude=document.getElementById("eLongitude").value;
		image="http://maps.google.com/mapfiles/ms/icons/red-dot.png";
		checkZone(latitude,longitude,type);
		//	markerDragTo();
	}
	var latlng = new google.maps.LatLng(latitude,longitude);
	/*
	if(document.getElementById("sLatitude").value !=""){
	  	var image="http://maps.google.com/mapfiles/ms/icons/green-dot.png";
	}if(document.getElementById("eLatitude").value !=""){
	  	var image="http://maps.google.com/mapfiles/ms/icons/red-dot.png";
	} if(document.getElementById("sLatitude").value =="" && document.getElementById("eLatitude").value =="") {
		var image="http://itouchmap.com/i/blue-dot.png";
	}*/
	directionsDisplay = new google.maps.DirectionsRenderer();
	var options = {
			zoom: 12,
			center: latlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	map = new google.maps.Map(document.getElementById("map_canvas"), options);
	directionsDisplay.setMap(map);
	//GEOCODER
	geocoder = new google.maps.Geocoder();

	marker = new google.maps.Marker({position:latlng,
		map: map,icon:image,draggable: true});
	marker.setMap(map);

}
$(document).ready(function() {
	initializeMap(3);
	$(function() {
		if(document.getElementById("addressCheck").value=="1"){
			$("#address").autocomplete({
				//This bit uses the geocoder to fetch address values
				source: function(request, response) {
					var defaultState = document.getElementById("sstate").value;
					var defaultCountry=document.getElementById("defaultCountry").value;

					geocoder.geocode( {'address': request.term +', '+defaultState+', ' + defaultCountry  }, function(results, status) {
						response($.map(results, function(item) {
							return {
								label: item.formatted_address,
								value: item.formatted_address,
								latitude: item.geometry.location.lat(),
								longitude: item.geometry.location.lng(),
								city: item.postal_code,
								addcomp: item.address_components
								//nhd: item.address_components_of_type("neighborhood")
							}
						}));
					})
				},
				//This bit is executed upon selection of an address
				select: function(event, ui) {

					var arrAddress =ui.item.addcomp;
					var streetnum= "";
					var route = "";


					// iterate through address_component array
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "subpremise"){
							$("#sadd2").val(address_component.long_name);
						}
						if (address_component.types[0] == "street_number"){
							$("#sadd1").val(address_component.long_name);
							streetnum = address_component.long_name;
						}

						if (address_component.types[0] == "locality"){
							$("#scity").val(address_component.long_name);
						}

						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}


						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#szip").val(address_component.long_name);
						}
						//return false; // break the loop

					});

					$("#sadd1").val(streetnum + " " + route);
					$("#sLatitude").val(ui.item.latitude);
					$("#sLongitude").val(ui.item.longitude);

					//$("#street").val(ui.item.street);
					//$("#state").val(ui.item.address_components_of_type("neighborhood"));
					var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
					marker.setPosition(location);
					map.setCenter(location);
					initializeMap(1);markerDrag();checkOldAddress(1);
					$("#address").autocomplete("close");
					document.getElementById("userAddress").value="";
				}
			});
		} else {
			var defaultState = document.getElementById("sstate").value;
			var input = document.getElementById('address');
			var autocomplete = new google.maps.places.Autocomplete(input);
//			var geolocation = new google.maps.LatLng(
//					document.getElementById("defaultLati").value, document.getElementById("defaultLongi").value);
//			var circle = new google.maps.Circle({
//				center: geolocation,
//				radius: 2
//			});
//			autocomplete.setBounds(circle.getBounds());
			autocomplete.bindTo('bounds', map);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				if (place.address_components) {
					var address_details = extractAddressFromPlaceResult(place, '#address');
					$('#sadd1').val(address_details.street_no+" "+address_details.street_name);
					$('#szip').val(address_details.postal);
					$('#scity').val(address_details.locality);
					var address = address_details.street_no + ' ' + address_details.street_name + ', ' + address_details.locality+ ', US';
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({'address': address}, function(results, status) {
						if(status == google.maps.GeocoderStatus.OK)
						{
							$("#sLatitude").val(results[0].geometry.location.lat());
							$("#sLongitude").val(results[0].geometry.location.lng());
						}
					});
					myFunction(1);	checkOldAddress(1);
				}
			});
		}
	});
	$(function() {
		if(document.getElementById("addressCheck").value=="1"){
			$("#addressRed").autocomplete({
				//This bit uses the geocoder to fetch address values
				source: function(request, response) {
					var defaultState = document.getElementById("sstate").value
					var defaultCountry=document.getElementById("defaultCountry").value;

					geocoder.geocode( {'address': request.term +', '+defaultState+', ' + defaultCountry  }, function(results, status) {
						response($.map(results, function(item) {
							return {
								label: item.formatted_address,
								value: item.formatted_address,
								latitude: item.geometry.location.lat(),
								longitude: item.geometry.location.lng(),
								city: item.postal_code,
								addcomp: item.address_components
							}
						}));
					})
				},
				select: function(event, ui) {

					var arrAddress =ui.item.addcomp;
					var streetnum= "";
					var route = "";
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "subpremise"){
							$("#sadd2Small").val(address_component.long_name);
						}
						if (address_component.types[0] == "street_number"){
							$("#sadd1Small").val(address_component.long_name);
							streetnum = address_component.long_name;
						}
						if (address_component.types[0] == "locality"){
							$("#scitySmall").val(address_component.long_name);
						}

						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}


						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#szipSmall").val(address_component.long_name);
						}
					});

					$("#sadd1Small").val(streetnum + " " + route);
					$("#sLatitude").val(ui.item.latitude);
					$("#sLongitude").val(ui.item.longitude);

					var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
					marker.setPosition(location);
					map.setCenter(location);
					initializeMap(1);markerDrag();checkOldAddress(1);
				}
			});
		} else {	
			var defaultState = document.getElementById("sstate").value;
			var input = document.getElementById('addressRed');
			var autocomplete = new google.maps.places.Autocomplete(input);
//			var geolocation = new google.maps.LatLng(
//					document.getElementById("defaultLati").value, document.getElementById("defaultLongi").value);
//			var circle = new google.maps.Circle({
//				center: geolocation,
//				radius: 2
//			});
//			autocomplete.setBounds(circle.getBounds());
			autocomplete.bindTo('bounds', map);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				if (place.address_components) {
					var address_details = extractAddressFromPlaceResult(place, '#addressRed');
					$('#sadd1Small').val(address_details.street_no+" "+address_details.street_name);
					$('#szipSmall').val(address_details.postal);
					$('#scitySmall').val(address_details.locality);
					var address = address_details.street_no + ' ' + address_details.street_name + ', ' + address_details.locality+ ', US';
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({'address': address}, function(results, status) {
						if(status == google.maps.GeocoderStatus.OK)
						{
							$("#sLatitude").val(results[0].geometry.location.lat());
							$("#sLongitude").val(results[0].geometry.location.lng());
						}
					});
					myFunction(1);	checkOldAddress(1);
				}
			});
		}
	});
	//Add listener to marker for reverse geocoding
	google.maps.event.addListener(marker, 'dragend', function() {
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var arrAddress=  results[0].address_components;
					//var arrAddress1=  results[1].address_components
					var streetnum;
					var route;
					$('#address').val(results[0].formatted_address);
					$('#sLatitude').val(marker.getPosition().lat());
					$('#sLongitude').val(marker.getPosition().lng());
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "subpremise"){
							$("#sadd2").val(address_component.long_name);
							$("#sadd2Small").val(address_component.long_name);
						}
						if (address_component.types[0] == "street_number"){
							$("#sadd1").val(address_component.long_name);
							$("#sadd1Small").val(address_component.long_name);
							streetnum = address_component.long_name;
						}

						if (address_component.types[0] == "locality"){
							$("#scity").val(address_component.long_name);
							$("#scitySmall").val(address_component.long_name);
						}

						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}


						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#szip").val(address_component.long_name);
							$("#szipSmall").val(address_component.long_name);
						}
					});
					$("#sadd1").val(streetnum + " " + route);
					$("#sadd1Small").val(streetnum + " " + route);
					checkOldAddress(1);
				}
			}
		}); 
	});

});
$(document).ready(function() {
	$(function() {
		if(document.getElementById("addressCheck").value=="1"){
			var defaultState = document.getElementById("sstate").value;
			var defaultCountry=document.getElementById("defaultCountry").value;
			geocoder = new google.maps.Geocoder();
			$("#endAddress").autocomplete({
				//This bit uses the geocoder to fetch address values
				source: function(request, response) {
					geocoder.geocode( {'address': request.term +', '+defaultState+', ' + defaultCountry  }, function(results, status) {
						response($.map(results, function(item) {
							return {
								label: item.formatted_address,
								value: item.formatted_address,
								latitude: item.geometry.location.lat(),
								longitude: item.geometry.location.lng(),
								city: item.postal_code,
								addcomp: item.address_components
								//nhd: item.address_components_of_type("neighborhood")
							}
						}));
					})
				},
				//This bit is executed upon selection of an address
				select: function(event, ui) {

					var arrAddress =ui.item.addcomp;
					var streetnum= "";
					var route = "";


					// iterate through address_component array
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "subpremise"){
							$("#eadd2").val(address_component.long_name);
						}
						if (address_component.types[0] == "street_number"){
							$("#eadd1").val(address_component.long_name);
							streetnum = address_component.long_name;
						}
						if (address_component.types[0] == "locality"){
							$("#ecity").val(address_component.long_name);
						}

						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}


						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#ezip").val(address_component.long_name);
						}

						//return false; // break the loop

					});

					$("#eadd1").val(streetnum + " " + route);
					$("#eLatitude").val(ui.item.latitude);
					$("#eLongitude").val(ui.item.longitude);

					//$("#street").val(ui.item.street);
					//$("#state").val(ui.item.address_components_of_type("neighborhood"));

					var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
					marker.setPosition(location);
					map.setCenter(location);
					initializeMap(2);markerDragTo();checkOldAddress(2);
				}
			});
		} else {
			var defaultState = document.getElementById("sstate").value;
			var input = document.getElementById('endAddress');
			var autocomplete = new google.maps.places.Autocomplete(input);
//			var geolocation = new google.maps.LatLng(
//					document.getElementById("defaultLati").value, document.getElementById("defaultLongi").value);
//			var circle = new google.maps.Circle({
//				center: geolocation,
//				radius: 2
//			});
//			autocomplete.setBounds(circle.getBounds());
			autocomplete.bindTo('bounds', map);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				if (place.address_components) {
					var address_details = extractAddressFromPlaceResult(place, '#endAddress');
					$('#eadd1').val(address_details.street_no+" "+address_details.street_name);
					$('#ezip').val(address_details.postal);
					$('#ecity').val(address_details.locality);
					var address = address_details.street_no + ' ' + address_details.street_name + ', ' + address_details.locality+ ', US';
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({'address': address}, function(results, status) {
						if(status == google.maps.GeocoderStatus.OK)
						{
							$("#eLatitude").val(results[0].geometry.location.lat());
							$("#eLongitude").val(results[0].geometry.location.lng());
						}
					});
					myFunction(2);markerDragTo();checkOldAddress(2);
				}
			});
		}
	});
});
$(document).ready(function() {
	$(function() {
		if(document.getElementById("addressCheck").value=="1"){
			var defaultState = document.getElementById("sstate").value;
			var defaultCountry=document.getElementById("defaultCountry").value;
			geocoder = new google.maps.Geocoder();
			$("#endAddressRed").autocomplete({
				//This bit uses the geocoder to fetch address values
				source: function(request, response) {
					geocoder.geocode( {'address': request.term +', '+defaultState+', ' + defaultCountry  }, function(results, status) {
						response($.map(results, function(item) {
							return {
								label: item.formatted_address,
								value: item.formatted_address,
								latitude: item.geometry.location.lat(),
								longitude: item.geometry.location.lng(),
								city: item.postal_code,
								addcomp: item.address_components
								//nhd: item.address_components_of_type("neighborhood")
							}
						}));
					})
				},
				select: function(event, ui) {
					var arrAddress =ui.item.addcomp;
					var streetnum= "";
					var route = "";

					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "subpremise"){
							$("#eadd2Small").val(address_component.long_name);
						}
						if (address_component.types[0] == "street_number"){
							$("#eadd1Small").val(address_component.long_name);
							streetnum = address_component.long_name;
						}

						if (address_component.types[0] == "locality"){
							$("#ecitySmall").val(address_component.long_name);
						}

						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}


						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#ezipSmall").val(address_component.long_name);
						}
					});

					$("#eadd1Small").val(streetnum + " " + route);
					$("#eLatitude").val(ui.item.latitude);
					$("#eLongitude").val(ui.item.longitude);

					var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
					marker.setPosition(location);
					map.setCenter(location);
					initializeMap(2);markerDragTo();checkOldAddress(2);
				}
			});
		} else {	
			var defaultState = document.getElementById("sstate").value;
			var input = document.getElementById('endAddressRed');
			var autocomplete = new google.maps.places.Autocomplete(input);
//			var geolocation = new google.maps.LatLng(
//					document.getElementById("defaultLati").value, document.getElementById("defaultLongi").value);
//			var circle = new google.maps.Circle({
//				center: geolocation,
//				radius: 2
//			});
//			autocomplete.setBounds(circle.getBounds());
			autocomplete.bindTo('bounds', map);
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
				var place = autocomplete.getPlace();
				if (place.address_components) {
					var address_details = extractAddressFromPlaceResult(place, '#endAddressRed');
					$('#eadd1Small').val(address_details.street_no+" "+address_details.street_name);
					$('#ezipSmall').val(address_details.postal);
					$('#ecitySmall').val(address_details.locality);
					var address = address_details.street_no + ' ' + address_details.street_name + ', ' + address_details.locality+ ', US';
					geocoder = new google.maps.Geocoder();
					geocoder.geocode({'address': address}, function(results, status) {
						if(status == google.maps.GeocoderStatus.OK)
						{
							$("#eLatitude").val(results[0].geometry.location.lat());
							$("#eLongitude").val(results[0].geometry.location.lng());
						}
					});
					myFunction(2);markerDragTo();checkOldAddress(2);
				}
			});
		}
	});
});
function markerDragTo(){
	google.maps.event.addListener(marker, 'dragend', function() {
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var arrAddress=  results[0].address_components;
					//var arrAddress1=  results[1].address_components
					var streetnum;
					var route;
					$('#endAddress').val(results[0].formatted_address);
					$('#eLatitude').val(marker.getPosition().lat());
					$('#eLongitude').val(marker.getPosition().lng());
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "subpremise"){
							$("#eadd2").val(address_component.long_name);
							$("#eadd2Small").val(address_component.long_name);
						}
						if (address_component.types[0] == "street_number"){
							$("#eadd1").val(address_component.long_name);
							$("#eadd1Small").val(streetnum + " " + route);
							streetnum = address_component.long_name;
						}
						if (address_component.types[0] == "locality"){
							$("#ecity").val(address_component.long_name);
							$("#ecitySmall").val(address_component.long_name);
						}
						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}
						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#ezip").val(address_component.long_name);
							$("#ezipSmall").val(address_component.long_name);
						}
					});
					$("#eadd1").val(streetnum + " " + route);
					$("#eadd1Small").val(streetnum + " " + route);
					checkOldAddress(2);
				}
			}
		}); 
	});
}
function markerDrag(){
	google.maps.event.addListener(marker, 'dragend', function() {
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var arrAddress=  results[0].address_components;
					//var arrAddress1=  results[1].address_components
					var streetnum;
					var route;
					$('#address').val(results[0].formatted_address);
					$('#sLatitude').val(marker.getPosition().lat());
					$('#sLongitude').val(marker.getPosition().lng());
					$.each(arrAddress, function (i, address_component) {
						if (address_component.types[0] == "subpremise"){
							$("#sadd2").val(address_component.long_name);
							$("#sadd2Small").val(address_component.long_name);
						}
						if (address_component.types[0] == "street_number"){
							$("#sadd1").val(address_component.long_name);
							$("#sadd1Small").val(address_component.long_name);
							streetnum = address_component.long_name;
						}
						if (address_component.types[0] == "locality"){
							$("#scity").val(address_component.long_name);
							$("#scitySmall").val(address_component.long_name);
						}
						if (address_component.types[0] == "route"){ 
							route = address_component.long_name;
						}
						if (address_component.types[0] == "country"){ 
							itemCountry = address_component.long_name;
						}
						if (address_component.types[0] == "postal_code"){ 
							itemPc = address_component.long_name;
							$("#szip").val(address_component.long_name);
							$("#szipSmall").val(address_component.long_name);
						}
					});
					$("#sadd1").val(streetnum + " " + route);
					$("#sadd1Small").val(streetnum + " " + route);
					checkOldAddress(1);
				}
			}
		}); 
	});
}
function callDriverRoute() {
	if(document.getElementById("sLatitude").value!="" && document.getElementById("sLatitude").value!="0.000000" && document.getElementById("eLatitude").value!="" && document.getElementById("eLatitude").value!="0.000000"){
		document.getElementById('mapForDirection').innerHTML="";
		if(document.getElementById("ORFormat").value!="10"){
			$('#mapForDirection').jqm();
			$('#mapForDirection').jqmShow();
		} else {
			document.getElementById("finalMapScreen").style.display="block";
		}
		var options={
				elt:document.getElementById('mapForDirection'),        
				zoom:16,                                   
				latLng:{lat:document.getElementById("defaultLati").value, lng:document.getElementById("sLongitude").value},    
				mtype:'osm'                                
		};
		mapRoute = new MQA.TileMap(options);

		MQA.withModule('largezoom', function() {
			mapRoute.addControl(
					new MQA.LargeZoom(),
					new MQA.MapCornerPlacement(MQA.MapCorner.TOP_LEFT, new MQA.Size(5,5))
			);
		});
		MQA.withModule('mousewheel', function() {
			mapRoute.enableMouseWheelZoom();
		});
		MQA.withModule('directions', function() {
			mapRoute.addRoute([
			                   {latLng: {lat:document.getElementById("sLatitude").value, lng:document.getElementById("sLongitude").value}},
			                   {latLng: {lat:document.getElementById("eLatitude").value, lng:document.getElementById("eLongitude").value}}
			                   ]);
		});
	} else {
		alert("Both Pick Up & Drop Off Address Are Mandatory");
	}
}
var polygonZonesGlobal =[];
var labelGlobal =[];
var b2=new Boolean(1);	

function showAllZones(){
	if(b2){
		for(var i=0;i<polygonZonesGlobal.length;i++){
			polygonZonesGlobal[i].setMap(null);
			labelGlobal[i].setMap(null);
		}
		document.getElementById("zoneCheckImage").value="Show Zones";
		document.getElementById("zoneCheckImage").style.backgroundColor="#6CC417"; 
		polygonZonesGlobal =[];
		labelGlobal =[];
	} else {
		showLatLongNew();
	}
	b2=!b2;
}
function showLatLongNew() {
	var fillColors =  [];
	fillColors.push("#FFA500");
	fillColors.push("#FF00FF");
	fillColors.push("#FF0000");
	fillColors.push("#00FF00");
	fillColors.push("#808000");
	fillColors.push("#616D7E");
	fillColors.push("#306EFF");
	fillColors.push("#8D38C9");
	fillColors.push("#F52887");
	fillColors.push("#00FF00");
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = '/TDS/DashBoard?event=allZonesForDash';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var obj = "{\"latLong\":"+text+"}";
	var latLongList = JSON.parse(obj.toString());
	if(Number(latLongList.latLong.length)>1){
		for(var i=0;i<latLongList.latLong.length;i++){
			var latLng =[];
			for(var j=0;j<latLongList.latLong[i].ZC.length;j++){
				latLng.push(new google.maps.LatLng(latLongList.latLong[i].ZC[j].latitude ,latLongList.latLong[i].ZC[j].longitude));
			}
			if(latLng.length>2){
				var polygon = new google.maps.Polygon({paths: latLng,
					strokeColor: "#000000",
					strokeOpacity: 0.6,
					strokeWeight: 1.5,
					fillColor: fillColors[i%10],
					fillOpacity: 0.45
				});
				polygon.setMap(map);
				polygonZonesGlobal.push(polygon);
			} else {
				var image="http://itouchmap.com/i/blue-dot.png";
				var latlngSingle=new google.maps.LatLng(latLongList.latLong[i].ZC[0].latitude ,latLongList.latLong[i].ZC[0].longitude);
				marker = new google.maps.Marker({position:latlngSingle,
					map: map,icon:image,draggable: false});
				marker.setMap(map);
			}
			var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
			jobLabel.set('position', new google.maps.LatLng(latLongList.latLong[i].CLAT,latLongList.latLong[i].CLON));
			jobLabel.set('text',latLongList.latLong[i].ZD+'('+latLongList.latLong[i].ZK+')');
			jobLabel.setMap(map);
			labelGlobal.push(jobLabel);
		}
		document.getElementById("zoneCheckImage").value="Hide Zones";
		document.getElementById("zoneCheckImage").style.backgroundColor="#C11B17"; 
	}
}


