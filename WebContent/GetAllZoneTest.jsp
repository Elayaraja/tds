<%@page import="com.tds.cmp.bean.ZoneTableBeanSP"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%ArrayList<String> allZones=(ArrayList<String>) request.getAttribute("zones");%> 
<%ArrayList<Double> Latitudes= (ArrayList<Double>) request.getAttribute("latitude"); %>
<%ArrayList<Double> Longitudes= (ArrayList<Double>) request.getAttribute("longitude");%> 
 <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
<title>Review All Zones</title>
<meta name="description"
	content="Find the latitude and longitude of a point using Google Maps.">
<meta name="keywords"
	content="latitude, longitude, google maps, get latitude and longitude">
<script src='<%=TDSProperties.getValue("googleMapV3")%>'></script>
<SCRIPT type="text/javascript" src="/js/mapTypeControl.js"></SCRIPT>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/labelMap.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
	//         
	// Latitude and Longitude math routines are from: http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html

	var map = null;
	var geocoder = null;
	var marker = null;

	function initialLoad() {
		var defaultLati=<%=adminBo.getDefaultLati()%>
		var defaultLongi=<%=adminBo.getDefaultLogi()%>
		var latlng = new google.maps.LatLng(defaultLati,defaultLongi);
		var options = {
				zoom: 14,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				draggableCursor: 'crosshair'
		};

		map = new google.maps.Map(document.getElementById("map"), options);
		//GEOCODER
		geocoder = new google.maps.Geocoder();

		  marker = new google.maps.Marker({
              map: map,
              draggable: true
      });
		showLatLongNew();
	}

 function showLatLongNew() {
		var fillColors =  [];
		fillColors.push("#FFA500");
		fillColors.push("#FF00FF");
		fillColors.push("#FF0000");
		fillColors.push("#00FF00");
		fillColors.push("#808000");
		fillColors.push("#616D7E");
		fillColors.push("#306EFF");
		fillColors.push("#8D38C9");
		fillColors.push("#F52887");
		fillColors.push("#00FF00");

  		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=allZonesForDash';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"latLong\":"+text+"}";
		var latLongList = JSON.parse(obj.toString());
		for(var i=0;i<latLongList.latLong.length;i++){
			var latLng =[];
			for(var j=0;j<latLongList.latLong[i].ZC.length;j++){
				latLng.push(new google.maps.LatLng(latLongList.latLong[i].ZC[j].latitude ,latLongList.latLong[i].ZC[j].longitude));
			}
			var polygon = new google.maps.Polygon({paths: latLng,
				strokeColor: "#000000",
				strokeOpacity: 0.6,
				strokeWeight: 1.5,
			});
			polygon.setMap(map);
		}
 		callForDots();
 }
 function callForDots(){
	 <%for(int i=0;i<allZones.size();i++){%>
	  	var points= new google.maps.LatLng(<%=Latitudes.get(i)%>,<%=Longitudes.get(i)%>);
		var image = "https://maps.gstatic.com/intl/en_us/mapfiles/markers2/measle_blue.png";
		var marker = new google.maps.Marker({position: points,
	           map: map, icon:image});
		marker.setMap(map);
		<%}%>
 }
function infoCall(info,poly){
	return function() { 
		info.open(map, poly);
	}; 
} 	
</script>
</head>
<body  onload="initialLoad()">  
<form name="masterForm" action="systemsetup" method="post" >
<input type="hidden" id="size" name="size" value="">
<input type="hidden" name="action" value="systemsetup" />
<input type="hidden" name="event" value="createqueueCoordinate" />
<input type="hidden" id="outsidePoint" name="outsidePoint" value="">
<input  type="hidden"   name="module"    id="module"  value="systemsetupView">
<center>
<div id="h">
<div id="h0"></div>
<div id="o">
<div id="content">
<table cellpadding="4" cellspacing="0" width="100%">
	<tr valign="top">
		<h1>Review All Zones
		<input type="submit" value="Close" style="width: 170px; height: 40px; font-size: 15px; font-weight: bold; border: 2px solid red; background: lightgreen;" name="reset" "></h1>
		<div id="wrapper" style="margin: 5px">
		<div id="map" style="width: 1500px; height: 850px"></div>
		</div>
		<table cellpadding="2" cellspacing="0" border="1">
				<tr>
				</tr>
			
</table>
</table>
</div>
</div>
</div>
</center>
</form>
</body>
</html>

