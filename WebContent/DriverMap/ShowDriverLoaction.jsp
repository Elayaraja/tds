<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%><html>
<head>
<script type="text/javascript">

function disabledriver()
{
	if(document.getElementById('all').checked)
	{
		document.getElementById('driver_id').value="";
		 
		document.getElementById('driver_id').disabled = true;
	} else {
		 
		document.getElementById('driver_id').disabled = false;
	}
}    
</script>
<script type="text/javascript" src="Ajax/DashBoard.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<form name="masterForm" action="control" method="post">
<input type="hidden" name="action" value="admin">
<input type="hidden" name="event" value="showDriver">
<input type="hidden" name="assoccode" id="assoccode" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>">

<table width="100%" id='bodypage'>
	<tr>
		<td>
		<table width="100%">	
			<tr>
				<td colspan="7" align="center" style="width: 300px; color: #017D95;border: 1;border-style: solid" >		
						<h2>Driver Location Shows</h2>
				</td>
			</tr>
			<tr>	
				<td>Driver id</td>
				<td>
					 <input type="text" id="driver_id" name="driver_id" size="15"  value="" class="form-autocomplete">
					  <ajax:autocomplete
						  					fieldId="DP_DRIVER_ID0"
						  					popupId="model-popup1"
						  					targetId="DP_DRIVER_ID0"
						  					baseUrl="autocomplete.view"
						  					paramName="DRIVERID"
						  					className="autocomplete"
						  					progressStyle="throbbing" /> 
						  					
					 
					 <input type="checkbox" name="all" id="all" value="1" onclick="disabledriver()">All Driver 
				</td>	
			</tr>
			<tr>
				<!--  <td><input type="button" Value="Show" onclick="openDLocation()"></td>-->
				<td><input type="submit" name="Button" value="Show"></td>
			</tr>
		</table>
		</td>
		</tr>
		<tr>
			<td>
				<div id="mapdiv">
					<jsp:include flush="true" page="/DriverMap/DriverMapCompany.jsp"></jsp:include>
				</div>
			</td>	
		</tr>
</table>
</form>	
		
</body>
</html>