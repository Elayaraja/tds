<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" session="false"%>

<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.tds.dao.RequestDAO"%>
<%@page import="java.util.HashMap"%>

<%@page import="com.common.util.TDSProperties"%>

<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml"
	xmlns:v="urn:schemas-microsoft-com:vml">
<head>
<%! List m_driverList = null;Map m_driverDetailMap = null; %>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<script src='<%= TDSProperties.getValue("googleMap") %>'
	type="text/javascript"></script>
<script type="text/javascript">
	
    function initialize(latitude, longitude, driver, extension) {
  
      if (GBrowserIsCompatible() ) { 
        var map = new GMap2(document.getElementById("map_canvas"));
        map.setCenter(new GLatLng(39.238449, -96.329435), 4);
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());

        // Create a base icon for all of our markers that specifies the
        // shadow, icon dimensions, etc.
        var baseIcon = new GIcon(G_DEFAULT_ICON);
        baseIcon.iconSize = new GSize(20, 34);
        baseIcon.shadowSize = new GSize(37, 34);
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(9, 2);

        // Creates a marker whose info window displays the letter corresponding
        // to the given index.
        function createMarker(point, ext, driver) {

          var letteredIcon = new GIcon(baseIcon);

          // Set up our GMarkerOptions object
          markerOptions = { icon:letteredIcon };
          var marker = new GMarker(point, markerOptions);

          GEvent.addListener(marker, "click", function() {
            marker.openInfoWindowHtml("1-888-GET-A-CAB "+driver+" " + ext + "</b>");
          });
          return marker;
        }

        // Add 10 markers to the map at random locations
		
	  var latlng;
 	  for(var counter = 0; counter < latitude.length; counter++) {
          latlng = new GLatLng(latitude[counter], longitude[counter]);
          map.addOverlay(createMarker(latlng, extension[counter], driver[counter]));
	 }

	  //var latlng1 = new GLatLng(39.945603, -104.954240);
          //map.addOverlay(createMarker(latlng1, 2));

      }
    }

		
    function samp() {

    	var latitude =  new Array ();
		var longitude = new Array (); 
		var driver = new Array ();
		var extension = new Array ();
        <%	m_driverList = request.getAttribute("mapDetail")==null? RequestDAO.getDriverLocation():(ArrayList)request.getAttribute("mapDetail");
        	//System.out.println(m_driverList);
        	if(m_driverList != null) {
        		//m_driverDetailMap = new HashMap();
        		for(int m_driverCount = 0; m_driverCount < m_driverList.size(); m_driverCount++) {
        			m_driverDetailMap = (Map)m_driverList.get(m_driverCount);
        %>
					latitude[<%= m_driverCount %>] = <%= m_driverDetailMap.get("latitude") %>
					longitude[<%= m_driverCount %>] = <%= m_driverDetailMap.get("longitude") %>
					extension[<%= m_driverCount %>] = <%= m_driverDetailMap.get("driverext") %> 
					driver[<%= m_driverCount %>] = <%= m_driverDetailMap.get("driverid") %>;	
        	<%	} %>
		initialize(latitude, longitude, driver, extension);
		<% } %>
    }
    </script>
</head>
<body onload="samp()">
<%if(request.getAttribute("full") == null){ %>
	<div id="map_canvas" style="width: 500px; height: 300px"></div>
<%} else { %>
	<div id="map_canvas" style="width: 800px; height: 500px"></div>
<%} %>
</body>
</html>