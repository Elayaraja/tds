<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.HashMap"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%@page import="com.common.util.TDSConstants"%><html>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/cssverticalmenu.css" />
<link href="css/newtdsstyles.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function openDashBoard(){
	//alert(""); 
	//var q=(document.location.href);
	//open('/TDS/control?action=admin&event=dashBoard'+escape(q),'','top=0,left=0,resizable=no,toolbar=no,menubar=no,statu=no,location=yes,fullscreen=true,scrollbars=yes');


	var q=(document.location.href);
	q=q+'?action=admin&event=dashBoard';
	window.open('DashBoard?event=dispatchValues',target="_blank");

	//window.open('DashBoard?event=dispatchValues','','top=0,left=0,resizable=no,toolbar=no,addressbar=no,menubar=no,statu=no,location=yes,fullscreen=yes,scrollbars=yes',target="_blank");
	
	//var q1 = open(q,'','top=0,left=0,resizable=no,toolbar=no,menubar=no,statu=no,location=yes,fullscreen=true,scrollbars=yes');
		
}  
</script>

<script type="text/javascript" src="js/cssverticalmenu.js"></script>
<style type="text/css">
.glossymenu, .glossymenu li ul{
list-style-type: none;
margin: 0;
padding: 0; 
width: 250px; /*WIDTH OF MAIN MENU ITEMS*/
border: 1px solid black;
}

.glossymenu li{
position: relative;
}

.glossymenu li a{
background: #FDD374;
font: bold 12px Verdana, Helvetica, sans-serif;
color: black;
display: block;
width: auto;
padding: 5px 0;
padding-left: 10px;
text-decoration: none;
}

.glossymenu li ul{ /*SUB MENU STYLE*/
position: absolute;
width: 240px; /*WIDTH OF SUB MENU ITEMS*/
left: 0;
top: 0;
display: none;
}

.glossymenu li ul li{
float: left;
}

.glossymenu li ul a{
width: 240px; /*WIDTH OF SUB MENU ITEMS - 10px padding-left for A elements */
}

.glossymenu .arrowdiv{
position: absolute;
right: 2px;
background: transparent url(images/arrow.gif) no-repeat center right;
}

.glossymenu li a:visited, .glossymenu li a:active{
color: ButtonText;
}

.glossymenu li a:hover{
background-image: url(images/glossyback2.gif);
font: bold 12px Verdana, Helvetica, sans-serif;
color: white;
}

/* Holly Hack for IE \*/
* html .glossymenu li { float: left; height: 1%; }
* html .glossymenu li a { height: 1%; }
/* End */
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body> 
<table   border="0">
 <Tr> 
 <td> 
 
 
<div   id="menudetail" >
<% 	AdminRegistrationBO adminBO = null;
			if(session.getAttribute("user") != null) { 
				adminBO= (AdminRegistrationBO)session.getAttribute("user");
%>
<%if(adminBO.getRoles().contains("1000")) { %>
 <div    class="menudetail"    id="dispatchView"  <%if(request.getParameter("module")!=null && request.getParameter("module").equals("dispatchView")){ %> style="display:block"<% } else { %> style="display:none" <% }%>>
 		
	   <ul id="verticalmenu" class="glossymenu">
	   <li      style="background-color: #FDD374"  >     <font  size="3"  color="blue"      ><b>DISPATCH</b></font> 
	    </li>
		<%if(adminBO.getRoles().contains("1001")) { %>
	    	<li> 
	    		<a href="DashBoard?event=dispatchValues" >Operator&nbsp;Dash&nbsp;Board</a>
	    	</li>
	    <%} %>
	    <%if(adminBO.getRoles().contains("1003")) { %>
	    <li>
	    	<a href="control?action=openrequest&event=saveRequest&module=dispatchView" >Create&nbsp;A&nbsp;Job</a>
	    </li>
		<%}if(adminBO.getRoles().contains("1004")) { %>	
	    <li> 
	      	<a href="control?action=openrequest&event=getopenCriteria&module=dispatchView" >Change&nbsp;Existing&nbsp;Job</a>
	    </li>
	    <%}if(adminBO.getRoles().contains("1005")) { %>
		<li>
			<a href="control?action=openrequest&event=getAcceptCriteria&module=dispatchView" >Change&nbsp;Accepted&nbsp;Job</a>
		</li>
		<%}if(adminBO.getRoles().contains("1006")) { %>
		<li>
			<a href="control?action=openrequest&event=sendSMS&module=dispatchView" >Send&nbsp;Message&nbsp;to&nbsp;Drivers</a>
		</li>
		<%} if(adminBO.getRoles().contains("1013")) { %>
		<li>
			<a href="control?action=openrequest&event=operatorShift&module=dispatchView" >Operator&nbsp;Shift</a>
		</li>
		<%}%>
		<%if(adminBO.getRoles().contains("1020")) { %>
		<li>
			<a href="control?action=openrequest&event=openRequestReview&module=dispatchView" >OR Review</a>
		</li>
		<%}%>
	    </ul>
 
 </div>
<%} %>
 
<%if(adminBO.getRoles().contains("2000")) { %>
<% 
%>

 <div    class="menudetail"   id="operationView"  <%if(request.getParameter("module")!=null && request.getParameter("module").equals("operationView")){ %> style="display:block"<% } else { %> style="display:none" <% }%>>
 
    <ul id="verticalmenu" class="glossymenu">
     <li      style="background-color: #FDD374"  >     <font  size="3"  color="blue"      ><b>OPERATIONS</b></font> 
	    </li>
     <%if(adminBO.getRoles().contains("2016"))  {%> 
    <li><a href="control?action=registration&event=driverMapping&subevent=home&module=operationView">Driver&nbsp;Zone&nbsp;Mapping</a></li> 
     
      <%}if(adminBO.getRoles().contains("2018"))  {%> 
    <li><a href="control?action=registration&event=disbursementmessage&module=operationView">Disbursement&nbsp;Message</a></li>
    <%}if(adminBO.getRoles().contains("2030"))  {%> 
    <li><a href="control?action=registration&event=disbursementMessageSummary&module=operationView">Disbursement&nbsp;Message&nbsp;Summary</a></li>
    <%}if(adminBO.getRoles().contains("2019"))  {%> 
    
    <li><a href="control?action=registration&event=landmark&module=operationView">Create&nbsp;Landmark</a></li>
    <%}if(adminBO.getRoles().contains("2020")) { %>	
     	<li><a href="control?action=registration&event=landmarkSummary&module=operationView" >Search&nbsp;Edit&nbsp;LandMark</a></li>
     <%}if(adminBO.getRoles().contains("2021")) { %>	
     	<li><a href="control?action=registration&event=driverlogout&module=operationView" >Log&nbsp;Out&nbsp;Driver</a></li>	 
   	<%}if(adminBO.getRoles().contains("5308")) { %>	
		<li><a href="control?action=systemsetup&event=landMarkPending&module=operationView" >Landmark Approve Pending </a></li>
   	<%}if(adminBO.getRoles().contains("2032"))  {%> 
    	<li><a href="control?action=registration&event=disbursementMessageSummary&module=operationView">Disbursement&nbsp;Message&nbsp;Summary</a></li>
	 <%}if(adminBO.getRoles().contains("2022")) { %>	
		<li><a href="control?action=registration&event=CabDriverMapping&module=operationView" >Map&nbsp;Driver&nbsp;To&nbsp;Cab </a></li>
	 <%}if(adminBO.getRoles().contains("2023")) { %>	
		<li><a href="control?action=registration&event=CabDriverMappingSummary&module=operationView" >Review&nbsp;Driver&nbsp;Cab&nbsp;Mapping</a></li>
   
    <%} if(adminBO.getRoles().contains("2015"))  {%> 
    <li><a href="control?action=registration&event=driverSave&subevent=driver&module=operationView">Driver/Operator&nbsp;Registration</a></li> 
    <%} if(adminBO.getRoles().contains("2001")) { %>
    	 <li><a href="control?action=admin&event=driverTempDeActive&module=operationView" >Deactivate&nbsp;Driver</a></li>
       <%}if(adminBO.getRoles().contains("2002")) { %>	
    	<li><a href="control?action=admin&event=getDeActiveSummary&module=operationView" >Activate&nbsp;Driver</a></li>
    <%}if(adminBO.getRoles().contains("2007")) { %>	
     	<li><a href="control?action=registration&event=createLostandFound&module=operationView" >Lost&nbsp;and&nbsp;Found</a></li>
    <%}if(adminBO.getRoles().contains("2003")) { %>	
     	<li><a href="control?action=registration&event=lostandFoundSummary&module=operationView" >Search&nbsp;Lost&nbsp;and&nbsp;Found</a></li> 
    <%}if(adminBO.getRoles().contains("2005")) { %>	
    	<li><a href="control?action=registration&event=createMaintence&module=operationView" >Create&nbsp;Cab&nbsp;Maintenance</a></li>
    <%}if(adminBO.getRoles().contains("2006")) { %>	
    	<li><a href="control?action=registration&event=maintenanceSummary&module=operationView" >Review&nbsp;Cab&nbsp;Maintenance</a></li>
    <%}if(adminBO.getRoles().contains("2008")) { %>	
    	<li><a href="control?action=registration&event=ZoneEntry&module=operationView" >Zone&nbsp;Login</a></li>
    <%}if(adminBO.getRoles().contains("2013")) { %>	
	<li><a href="control?action=registration&event=custInfo&module=operationView" >Client Account</a></li>
	<%}if(adminBO.getRoles().contains("2014")) { %>	
	<li><a href="control?action=openrequest&event=ServiceSummary&module=operationView" >Service&nbsp;Summary</a></li>
	<%}if(adminBO.getRoles().contains("2025")) { %>	
	<li><a href="control?action=CustomerService&event=createComplaints&module=operationView" >Create&nbsp;Complaints</a></li>
	<%}if(adminBO.getRoles().contains("2024")) { %>	
	<li><a href="control?action=CustomerService&event=reviewComplaints&module=operationView" >Review&nbsp;Complaints</a></li>
	<%}if(adminBO.getRoles().contains("2026")) { %>	
	<li><a href="control?action=registration&event=driverLocation&module=operationView" >Driver Search</a></li>
	<%}%>
     </ul>
 
 
</div>

 

<%} %>
<%if(adminBO.getRoles().contains("3000")) { %>
<div     class="menudetail"   id="financeView"   <%if(request.getParameter("module")!=null && request.getParameter("module").equals("financeView")) { %> style="display:block" <%}else { %>  style="display:none" <%} %>>
 
 
    <ul   id="verticalmenu" class="glossymenu">
     <li      style="background-color: #FDD374"  >     <font  size="3"  color="blue"      ><b>FINANCE</b></font> 
	    </li>
    <%if(adminBO.getRoles().contains("3001")) { %>
    	<li><a href="control?action=manualccp&event=manualCCProcess&module=financeView" >Process&nbsp;Credit&nbsp;Card</a></li>
    <%}if(adminBO.getRoles().contains("3002")) { %>
    	<li><a href="control?action=manualccp&event=getSummarytoAlter&module=financeView" >Reverse&nbsp;Credit&nbsp;Card&nbsp;Transactions</a></li>
     <%}if(adminBO.getRoles().contains("3021")) { %>
    	<li><a href="control?action=manualccp&event=getSummarytoAlterlimit&module=financeView" >Review Credit Card Transaction</a></li>
     <%}if(adminBO.getRoles().contains("3003")) { %>	
    	<li><a href="control?action=registration&event=createVoucher&module=financeView" >Create&nbsp;Voucher</a></li>
    <%}if(adminBO.getRoles().contains("3004")) { %>	
    	<li><a href="control?action=registration&event=getVoucherEditPage&module=financeView" >Change&nbsp;Voucher</a></li>
    	<%}if(adminBO.getRoles().contains("3005")) { %>	    		
    	<li><a href="control?action=manualccp&event=getUntxnSummary&module=financeView" >Reverse&nbsp;UnCapture&nbsp;CC&nbsp;Transactions</a></li>
    <%}if(adminBO.getRoles().contains("3006")) { %>	    		
    	<li><a href="control?action=registration&event=uncapturedamtdetails&module=financeView" >Tag&nbsp;Voucher&nbsp;Payment</a></li>
    <%}if(adminBO.getRoles().contains("3007")) { %>	
    	<li><a href="control?action=registration&event=driverDisbursement&module=financeView" >Driver&nbsp;Disbursement</a></li>
    <%}if(adminBO.getRoles().contains("3008")) { %>	
    	<li><a href="control?action=registration&event=driverDisbursementSummary&module=financeView" >Review&nbsp;Driver&nbsp;Disbursement</a></li> 
    <%}if(adminBO.getRoles().contains("2004")) { %>	
    	<li><a href="control?action=registration&event=driverPenalityMaster&module=financeView" >Misc&nbsp;Driver&nbsp;Credit/Charge</a></li>
    <%}if(adminBO.getRoles().contains("3025")) { %>	
		<li><a href="control?action=finance&event=reversepaymentEvent&module=financeView" >Reverse&nbsp;Disbursement</a></li>
    <%}if(adminBO.getRoles().contains("6000")) { %>	
		<li  style="background-color: #FDD374 "><font color="blue" ><B>CSV Upload</B></font></li>
			<%if(adminBO.getRoles().contains("6000")) { %>
				<li><a href="control?action=CSV&event=csvUploadStart&module=financeView" >Upload Misc Charges/Credits</a></li>
			 <%} %>
    <%}if(adminBO.getRoles().contains("3022")) { %>
		<li><a href="control?action=manualccp&event=manualSettle&module=financeView" >Manual Settlement</a></li>	 
	<%}if(adminBO.getRoles().contains("3100")) { %>
		<li><a href="control?action=manualccp&event=multiCharges&module=financeView" >Create&nbsp;Multipe&nbsp;Charges</a></li>	 
	<%}if(adminBO.getRoles().contains("3024")) { %>
		<li><a href="control?action=finance&event=chequeDetails&module=financeView" >Mark&nbsp;Paid&nbsp;Checks</a></li>	 
    <%}if(adminBO.getRoles().contains("3023")) { %>
		<li><a href="control?action=finance&event=cashSettlement&module=financeView" >Cash&nbsp;Register</a></li>	 
 	<%}if(adminBO.getRoles().contains("3061")) { %>
		<li><a href="control?action=finance&event=generateInvoice&module=financeView" >Generate&nbsp;Invoice</a></li>	 
    <%}if(adminBO.getRoles().contains("3062")) { %>
		<li><a href="control?action=finance&event=invoicePayment&module=financeView" >Mark&nbsp;For&nbsp;Invoice&nbsp;Payment</a></li>	 
 
    <%} %>
   
    </ul>
 
</div>
<%} %>
<%if(adminBO.getRoles().contains("4000")) { %>
 <div id="reportView"  style="display:none"  >
  <ul id="verticalmenu" class="glossymenu">
	<li><a href="control?action=openrequest&event=Report"  >Reports</a></li>
	</ul>
	</div>
<%} %>
<div  id="logoutView"  style="display:none" >
  <ul id="verticalmenu" class="glossymenu">
<li><a href="control?action=registration&event=logout" >Logout</a></li>
</ul>
</div>
<%if(adminBO.getRoles().contains("5000")) { %>
<div id="systemsetupView"  class="menudetail"     <%if(request.getParameter("module")!=null && request.getParameter("module").equals("systemsetupView")) { %> style="display:block" <%}else { %>  style="display:none" <%} %>>
  <ul id="verticalmenu" class="glossymenu">
   <li      style="background-color: #FDD374"  >     <font  size="3"  color="blue"      ><b>SYSTEM SETUP</b></font> 
	    </li>
	 
		<%if(adminBO.getRoles().contains("5100")) { %>
		<li  style="background-color: #FDD374 "><font color="blue" >  <B>User Admin</B></font></li>
			

			<%if(adminBO.getRoles().contains("5102")) { %>
		    	<li><a href="control?action=registration&event=summaryDR&module=systemsetupView" >Update&nbsp;Driver</a></li>
		     <%}if(adminBO.getRoles().contains("5106")) { %>
		    	<li><a href="control?action=registration&event=Adminview&module=systemsetupView" >Update&nbsp;Operators</a></li>
		    <%}if(adminBO.getRoles().contains("5103")) { %>
		    	<li><a href="control?action=userrolerequest&event=getUserList&subevent=access&module=systemsetupView" >View/Change User&nbsp;Permissions</a></li>
		    <%}if(adminBO.getRoles().contains("5104")) { %><!--
		    	<li><a href="control?action=registration&event=companyentry" >Company&nbsp;Registration</a></li>
		     --><%}if(adminBO.getRoles().contains("5105")) { %>
		    	<li><a href="control?action=registration&event=savecompany&submit=View&module=systemsetupView" >Edit Company</a></li>
		    <%} if(adminBO.getRoles().contains("5207")) { %>
				<li><a href="control?action=Security&event=callerIdGen&module=systemsetupView" >Caller&nbsp;ID&nbsp;Password</a></li>
			 <%}if(adminBO.getRoles().contains("5121")) { %>	
		    	<li><a href="control?action=registration&event=cmpyFlagSetup&module=systemsetupView" >Setup Driver Cab Properties</a></li> 		   
			<%}%>
			<%if(adminBO.getRoles().contains("5320")) { %>	
				<li><a href="control?action=Security&event=passKeyGen&module=systemsetupView" >Authorize Computers</a></li>
			<%}%> 
			<%if(adminBO.getRoles().contains("5321")) { %>   
    			<li><a href="control?action=TDS_DOCUMENT_UPLOAD&event=passKeyGen&module=systemsetupView&do=disp" >Upload Documents</a></li>
    	<%}%> 
    	 <%if(adminBO.getRoles().contains("5325")) { %>   
    			<li><a href="control?action=TDS_DOCUMENT_SEARCH&event=passKeyGen&module=systemsetupView&do=searchOption" >Search Documents</a></li>
    	<%}%> 
    	<%if(adminBO.getRoles().contains("5324")) { %>
    					<li><a href="control?action=TDS_DOCUMENT_APPROVAL&event=passKeyGen&module=systemsetupView&do=dispAdmin" >Approve Documents</a></li>
    	<%}%> 
		<%} %>	
		<%if(adminBO.getRoles().contains("5200")) { %>
				<li style="background-color: #FDD374" ><font color="blue" ><B>Zone</B> </font></li>
			<%if(adminBO.getRoles().contains("5201")) { %>
				<li><a href="control?action=systemsetup&event=createqueueCoordinate&module=systemsetupView" >Create&nbsp;Zones</a></li>
			<%}if(adminBO.getRoles().contains("5202")) { %>	
			    <li><a href="control?action=systemsetup&event=queuesummary&module=systemsetupView" >Review Zones</a></li>
			<%}if(adminBO.getRoles().contains("5205")) { %>	
			    <li><a href="control?action=systemsetup&event=adjacentZones&module=systemsetupView" >Insert Adjacent Zones</a></li>
			<%}if(adminBO.getRoles().contains("5206")) { %>	
			    <li><a href="control?action=systemsetup&event=reviewAdjacentZones&module=systemsetupView" >Review Adjacent Zones</a></li> 
			<li style="background-color: #FDD374" ><font color="blue" ><B>Cab</B> </font></li>
		     <%}if(adminBO.getRoles().contains("5117")) { %>	
		    	<li><a href="control?action=registration&event=cabRegistration&module=systemsetupView" >Register A Cab</a></li>
		    <%}if(adminBO.getRoles().contains("5118")) { %>	
		    	<li><a href="control?action=registration&event=cabRegistrationSummary&module=systemsetupView" >Review Cabs</a></li>
		    <%} %>	
		<%} %>
		<%if(adminBO.getRoles().contains("5300")) { %>
			<%if(adminBO.getRoles().contains("5301")) { %>
			<li  style="background-color: #FDD374 "><font color="blue" ><B>Credit Card</B></font></li>
			<%if(adminBO.getRoles().contains("5302")) { %>
				<li><a href="control?action=registration&event=companyccsetup&module=systemsetupView" >Credit Card Charges</a></li>
			<%} %>	
		<%} %>
		<%if(adminBO.getRoles().contains("5400")) { %>	
		<li  style="background-color: #FDD374 " ><font color="blue" ><B>Financial</B></font></li>
			<%if(adminBO.getRoles().contains("5401")) { %>
				<li><a href="control?action=registration&event=cmpPaymentSettelmentSch&module=systemsetupView" >Payment Settlement Schedule</a></li>
			<%}if(adminBO.getRoles().contains("5402")) { %>	
			    <li><a href="control?action=registration&event=driverChargesMaster&module=systemsetupView" >Recurring Driver Charges</a>	</li>
			<%} %>
			<%if(adminBO.getRoles().contains("5303")) { %>	
				<li><a href="control?action=systemsetup&event=systemProperties&module=systemsetupView" >System &nbsp;Properties</a></li>
		<%} %> 
		<%} %>
		<%if(adminBO.getRoles().contains("5403")) { %>	
			    <li><a href="control?action=systemsetup&event=insertWrapper&module=systemsetupView" >Create SMS User</a>	</li>
			<%}if(adminBO.getRoles().contains("5404")) { %>	
			    <li><a href="control?action=systemsetup&event=updateWrapper&module=systemsetupView" >Update SMS User</a>	</li>
			<%} %>
		 
	 
	<%} %>
	</ul>
</div>
<%} %>

</div>

<%}else{ %>
<div   id="driverView"  style="display:none" >
  <ul id="verticalmenu" class="glossymenu">
 	 <li>
		<a href="control?action=registration&event=saveDriver&subevent=load">Driver&nbsp;Registration</a>
	</li>
	<li>
		<a href="control?action=registration&event=savecompany" >Company&nbsp;Registration</a>
	</li>

	<li>
		<a href="control?action=openrequest&event=saveRequest" >Open&nbsp;Request&nbsp;Entry</a>
	</li>
 
	<li>
		<a href="control?action=openrequest&event=getreceipt" >Get&nbsp;Receipt</a>			
	</li>

	<li>
		<a href="control?action=registration&event=showsActiveUser" >Active User</a>			
	</li>
 
 	<li>
		<a href="control?action=registration&event=checkUser" >Login</a>
	</li>
		</ul>
</div>
 <%} %>
 <div class="menudetail"       id="passengerView"  <%if(request.getParameter("module")!=null && request.getParameter("module").equals("passengerView")){ %> style="display:block"<% } else { %> style="display:none" <% }%>>
	 <ul   id="verticalmenu" class="glossymenu">
	 <li><a href="control?action=registration&event=passengerEntry&subEvent=psrEntry&module=passengerView" class="systemSetup" title="Active User"   >Passenger</a></li>
	 <li><a href="control?action=registration&event=passengerEntry&subEvent=passengerSummary&module=passengerView" class="systemSetup" title="Active User">PassengerSummary</a></li> 
	 </ul>
</div>
 
</td>
 </Tr>

</table>
</body>
</html>