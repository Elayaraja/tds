<%@page import="com.tds.tdsBO.CreditCardBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverVehicleBean"%>
<%@page import="java.util.Iterator"%>
<%@page import= "java.util.Map"%>
<%@page import= "java.util.Set"%>
<%@page import="java.util.HashMap"%>
<html>
<head>
<script type="text/javascript" src="js/main3Small.js?v=<%=TDSConstants.versionNo%>"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geocode&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/thickbox.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
function submitUser(){
	if(document.getElementById("customerName").value!=""&&document.getElementById("phoneNum").value!=""&&document.getElementById("latitude").value!=""&&document.getElementById("longitude").value!=""){
		document.addClient.submit();
	} else {
		document.getElementById("errorpage").innerHTML="Please Enter All Fields";
		return false;		
	}
}
</script>
<style type="text/css">
.ui-autocomplete {
	border: 1px solid #C77405;
	background-color: #FFFFFF;
	color: #C77405;
	font-weight: bold;
	outline: medium;
	margin-left: 1100px;
}
</style>
</head>
<body>
<form action="control" method="post" name="addClient">
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");

	ArrayList<DriverVehicleBean> dvList=new ArrayList<DriverVehicleBean>();
	HashMap<String,DriverVehicleBean> companyFlags=(HashMap<String,DriverVehicleBean>) request.getAttribute("companyFlags");
	Set hashMapEntries = companyFlags.entrySet();
	Iterator it = hashMapEntries.iterator();
	while(it.hasNext()){
		Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
		dvList.add(companyFlag.getValue());
	}

%>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="custInfo"/>
<input type="hidden" name="event2" value="addNew" />
<input type="hidden" name="defaultState" id="defaultState" value="<%=adminBo.getState()%>"/>
<input type="hidden" name="defaultCountry" id="defaultCountry" value="<%=adminBo.getCountry()%>"/>
<input type="hidden" name="latitude" id="latitude" value=""/>
<input type="hidden" name="longitude" id="longitude" value=""/>

<div>
<div>
 <c class="nav-header"><center>Add New Client</center></c>
 <br />  
 		
 <div id="errorpage" style="color: red">			
			</div> 
       
       <br />
<table style="width: 70%;" border="0" cellspacing="0" cellpadding="0" align="center">

<tr><td width="40%" align="center" style="font-weight: bold; font-size:medium; ">Client Personal Data</td></tr>
<tr>
	<td width="20%">Name</td>
	<td><input type="text" name="customerName" id="customerName" value="" autocomplete="off"></input></td>
	</tr>
	<tr>
	<td width="20%">Phone Number</td>
	<td><input type="text" name="phoneNum" id="phoneNum" value="" autocomplete="off"></input></td>
</tr>
<tr>
	<td width="20%">Email Id</td>
	<td><input type="text" name="mail" id="mail" value="" autocomplete="off"></input></td>
	</tr>
	<tr>
<%if(dvList!=null &&  dvList.size()>0) {%>
	<td width="20%">Special Request</td>
	<%int driverCounter=0;
	  int vehicleCounter=0;%>
	<td>
	<%for(int i=0;i<dvList.size();i++){ %>
	<%if(dvList.get(i).getDriverVehicleSW()==1){ %>
		<input type="checkbox"
					name="dchk<%=driverCounter%>" id="dchk<%=driverCounter%>" value="<%=dvList.get(i).getKey()==null?"":dvList.get(i).getKey()%>" /><%=dvList.get(i).getShortDesc() %>
			<%driverCounter++; %>
		<%}else if(dvList.get(i).getDriverVehicleSW()==2) { %>
		<input type="checkbox"
					name="vchk<%=vehicleCounter %>" id="vchk<%=vehicleCounter%>" value="<%=dvList.get(i).getKey()==null?"":dvList.get(i).getKey() %>" /><%=dvList.get(i).getShortDesc()%>
		
		<%vehicleCounter++; %>
					<%} %>
				<%} %>
		<input type="hidden" name="drprofileSize" id="drprofileSize" value="<%=driverCounter%>" />
		<input type="hidden" name="vprofileSize" id="vprofileSize" value="<%=vehicleCounter%>" />			
	</td>
<%} %>
</tr>
<tr><td> &nbsp; </td></tr>
<tr><td width="40%" align="center" style="font-weight: bold; font-size:medium; ">Client Address Details</td></tr>
<tr>
	<td width="20%"><b><font size="2">Address Search:</font>
		<input class="ui-autocomplete-input" name="userAddress" id="userAddress" type="text" size="45"></input></b></td>
</tr>
<tr>
	<td width="20%">Address Line1</td>
	<td><input type="text" name="address1" id="address1" value="" readonly="readonly"></input></td>
	</tr>
	<tr>
	<td width="20%">Address Line2</td>
	<td><input type="text" name="address2" id="address2" value=""></input></td>
</tr>
<tr>
	<td width="20%">City</td>
	<td><input type="text" name="city" id="city" value=""></input></td>
	</tr>
	<tr>
	<td width="20%">State</td>
	<td><input type="text" name="state" id="state" size="3" value=""></input>
	</td></tr>
	<tr><td width="20%">
		Zip</td><td><input type="text" name="zip" size="5" id="zip" value=""/></td>
</tr>
<tr><td> &nbsp; </td></tr>
<tr><td width="40%" align="center" style="font-weight: bold; font-size:medium; ">Comments</td></tr>
<tr>
	<td width="20%">Operator Comments</td>
	<td><textarea rows="3" cols="30" name="operatorComm" id="operatorComm"></textarea></td>
	</tr>
	<tr>
	<td width="20%">Driver Comments</td>
	<td><textarea rows="3" cols="30" name="driverComm" id="driverComm"></textarea></td>
</tr>
<tr></tr>
<tr>
  <td colspan="1" align="right" style="height: 20px">
  	<input type="button" name="addNewUser" value="Add User" onclick="return submitUser()"/>	                 		
  </td>
</tr>
</table>   
</div></div>   
 </form>
 </body>
</html> 