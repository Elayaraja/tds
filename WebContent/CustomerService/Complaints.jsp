<%@page import="com.tds.cmp.bean.ComplaintsBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
function validatePhone(){
	var phone=document.getElementById('passengerPhone').value;
	if(phone==""|| phone.length==10){
		return true;
	} else {
		alert("Check Your Phone Number");
		document.getElementById('passengerPhone').value="";
		return false;
	}
}
function validation()
{
	var t1 = document.getElementById('complaintNo');
	var t2 =document.getElementById('passengerPhone');
	var t3 =document.getElementById('Subject');
	var t4 =document.getElementById('status');
	if(t1.value==""&& t2.value=="" && t3.value=="" && t4.value=="")
    {
    alert("Please enter atleast one value");
	  return false;
    }}
</script>
<style>
h2{
font-family:italic;
color:grey;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>


<%
	ArrayList<ComplaintsBean> Complaintlist=(ArrayList<ComplaintsBean>)request.getAttribute("getComplaintList");
%>
<%
	ComplaintsBean complaintsBean =new ComplaintsBean();
%>
<title>TDS(Taxi Dispatch System)</title>
</head>
<body>
<form name="ComplaintSummary"  method="post" action="control" >
<input type="hidden" name="action" value="CustomerService"/>
		<input type="hidden" name="event" value="reviewComplaints"/>
		<input type="hidden" name="operation" value="2"/>
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<%AdminRegistrationBO adminBO=(AdminRegistrationBO)session.getAttribute("user"); %>
	<div> 
                    <div></div>
                </div> 
                <div>
<div align="center" >	
<h2>Complaint Details</h2>
<table width="100%" border="0" cellspacing="2" cellpadding="" >
					<tr>
							<td  >Complaint no:</td>
                			<td><input type="text" name="complaintNo"     id="complaintNo"   value=""  /></td>
                			<td >Trip ID:</td> 
                			<td> <input type="text" name="driverId"    id="driverId"   value="" /></td>
                			<td >Driver ID:</td> 
                			<td><input type="text" name="driverId"    id="driverId"   value=""  />
                			<ajax:autocomplete
				  					fieldId="driverId"
				  					popupId="model-popup1"
				  					targetId="driverId"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 	
				  					</td>
                			</tr>
					  		<tr>
                			<td >Rider Phone:</td>
                			<td><input type="text" name="passengerPhone"  id="passengerPhone"  value="" onblur="validatePhone()" /></td>
                    		<td > Subject:</td>
                 			<td><input type="text" name="Subject" id="Subject" value=""/></td>
                 			</tr>
                 			<tr>
                 			<td >Status:</td>
                 		 	<td><select name="status">
                 		 	<option value="opened">Opened</option>
                 		 	<option value="inprogress">InProgress</option>
                 		 	<option value="closed">Closed</option></select></td>
                 		 	</tr>
                 			<tr >
                 			<td  align="center" colspan="9">
                 			<input type="submit"   name="button"  value="Search" onclick = "return validation()" /></td>
                 		 	</tr>
                           </table>
<%if (request.getParameter("button")!=null && Complaintlist.size()==0){%>
<tr>
<td>
<div id="errorpage">
No Records Found
</div></td>

</tr>



<%  } else if(Complaintlist!=null&&Complaintlist.size()>0){%>  
 
               
<table   width="100%" border="0" cellspacing="" cellpadding="0">
<tr>
<td>
	<table  id="bodypage"width="750"  border="1" align="center" cellspacing="3" cellpadding="0">
 	<tr  style="font-weight:bold">
		<td style="background-color:lightgrey">CNO</td>
		<td style="background-color:lightgrey">Trip ID</td>
		<td style="background-color:lightgrey">DriverID</td>
		<td style="background-color:lightgrey">Rider Name</td>
		<td style="background-color:lightgrey">Rider_PH</td>
		<td style="background-color:lightgrey">Subject</td>
		<td style="background-color:lightgrey">Created By</td>
		<td style="background-color:lightgrey">Date_AND_Time</td>
		<td style="background-color:lightgrey">Status</td>
		<td style="background-color:lightgrey">Edit</td>
	</tr>
<% 
	boolean colorLightblue = true;
	String colorPattern;%>
	<%for(int i=0;i<Complaintlist.size();i++) { 
		colorLightblue = !colorLightblue;
		if(colorLightblue){
			colorPattern="style=\"background-color:lightblue\""; 
			}
		else{
			colorPattern="";
		}
	%>
	<tr>
		<td align="center" <%=colorPattern%>><%=Complaintlist.get(i).getComplaintNo() %>  </td>
		<td align="center"<%=colorPattern %>><%=Complaintlist.get(i).getTripId() %></td>
		<td align="center" <%=colorPattern%>><%=Complaintlist.get(i).getDriverId() %>  </td>
		<td align="center" <%=colorPattern%>><%=Complaintlist.get(i).getPassengerName() %>  </td>
		<td align="center" <%=colorPattern%>><%=Complaintlist.get(i).getPassengerPhone() %>  </td> 
		<td align="center" <%=colorPattern%>><%=Complaintlist.get(i).getSubject() %>  </td>
		<td align="center"<%=colorPattern %>><%=Complaintlist.get(i).getCreatedBy() %></td>
		<td align="center"<%=colorPattern %>><%=Complaintlist.get(i).getDateTime() %></td>
		<td align="center"<%=colorPattern %>><%=Complaintlist.get(i).getStatus() %></td>
		<td <%=colorPattern %>> <a href="control?action=CustomerService&event=createComplaints&module=operationView&Edit=Yes&complaintNo=<%=Complaintlist.get(i).getComplaintNo()%>">Edit</a></td> 
	</tr>
	<% } }%>
</table> 
	</td>
</tr>
				<tr> 
                		<td >
			<div >
                        <div >
                        	<div >
                        	
                        	 </div>
                            <div></div>
                        </div>
                        <div ></div>
                    </div>
                    </td>  
                		</tr></table>
                		<%String error="";
                        if(request.getAttribute("error")!=null){
                        error=(String) request.getAttribute("error");
                        } else if(request.getParameter("error")!=null){
                        error=(String)request.getParameter("error");
                        }
                        %>
<td colspan="8">
<div id="errorpage" style="">
<%=error.length()>0?""+error:""%>
</div>
</div>
</div> 
</form> 
</body>
</html>