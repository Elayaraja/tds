<%@page import="com.tds.cmp.bean.ComplaintDetailBean"%>
<%@page import="com.tds.cmp.bean.ComplaintsBean"%>
<%@page import="com.tds.dao.RegistrationDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.OpenRequestBO" %>
<%@page import="com.tds.dao.RequestDAO" %>
<%@page import="com.tds.controller.TDSAjaxClass"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="complaintsBean"   class="com.tds.cmp.bean.ComplaintsBean" />
<jsp:useBean id="complaintDetailBean"   class="com.tds.cmp.bean.ComplaintDetailBean" />
<%
	String s1= (String)request.getAttribute("s2");
%>
<%
	ComplaintsBean complaints=(ComplaintsBean)request.getAttribute("complaints"); 

	if (complaints == null){
		complaints = new ComplaintsBean();
	}
	String readonlyBecauseUpdate="";
	String select="";
	String pass="";
	boolean colorChange=false;
%>
<html >
<head>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
    
    //called when key is pressed in textbox
	$("#passengerPhone").keypress(function (e)  
	{ 
	  //if the letter is not digit then display error and don't type anything
	  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
	  {
		//display error message
		$("#errmsg").html("Rider Phone Number should contain Digits Only").show().fadeOut("slow").fadeIn("slow").fadeOut("slow");  
	    return false;
      }	
	});

  });
function removeError(){
	  document.getElementById("errorpage").innerHTML ="";
}
  </script>
<script type="text/javascript">
function validatePhone(){
	var phone=document.getElementById('passengerPhone').value;
	if(phone=="" || phone.length==10 ){
		return true;
	} else {
		alert("Check Your Phone Number");
		document.getElementById('passengerPhone').value="";
		return false;
	}
}


function validation()
{
	var t1 =document.getElementById('passengerPhone');
	var t2 =document.getElementById('Subject');
	var t3 =document.getElementById('status');
	var t4 = document.getElementById('description');
	if(t1.value==""||t2.value==""||t3.value=="" || t4.value=="")
    {
    alert("Please enter all the values");
	  return false;
	  
    }}
</script>
<style>
h2{
font-family:italic;
color:grey;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>

<%String error="";
String col="color:#00FF00";
                        if(request.getAttribute("error")!=null){
                        error=(String) request.getAttribute("error");
                        } else if(request.getParameter("error")!=null){
                        error=(String)request.getParameter("error");
                        }
                        %>
                        
                       <%--  <%
                        String color="";
                        if(error.length()!=36){
                        	color="";
                        }else{
                        	color="color:#00FF00";
                        	}%> --%>
<td colspan="8">
<div id="errorpage" style="color:#00FF00">
<%=error.length()>0?""+error:""%>
</div>
<%-- <td colspan="8" >
<div id="success page" style="color:#00FF00">
<%=error.length()==0?""+error:""%>
</div --%>


</head>
<title>TDS(Taxi Dispatch System)</title>

<%
	String s3=request.getParameter("complaintNo");
%>
<body>
		<form name="CustomerService" action="control" method="post"  />
		<input type="hidden" name="action" value="CustomerService"/>
		<input type="hidden" name="event" value="createComplaints"/>
		<input type="hidden" name="operation" value="2"/>
	    <input type="hidden"  name="Cno"  id="Cno"  value="<%=s3%>" /> 
		<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>"     />
		<input type="hidden"  name="key"  value="  <%=request.getAttribute("key")%>" />
                
<div style= width:100%;margin-left:-8px;>
<div style= width:100%;margin-left:-1px;>
<%
	if(s1!=null){
readonlyBecauseUpdate ="readonly";
%>
<h3>Update Complaints</h3>
<%
	}else{
readonlyBecauseUpdate =" ";
%>
<h2><center>Insert Complaints</center></h2>
<%
	}
%>
</div>
</div>
 

 <table width="100%" border="0" cellspacing="2" cellpadding="0" >
                   
                    <tr>
                    <td>
                    <table  width="100%" cellspacing="2"align="center">
                        <tr>
                        <td >Rider Name :</td>
                        <td><input type="text" autocomplete="off" name="passengerName"  id="passengerName" <%=readonlyBecauseUpdate%> value="<%=complaints.getPassengerName()%>" onkeypress="removeError()"/></td>
                    	<td>Rider Phone :</td>
                     	<td> <input type="text" autocomplete="off" name="passengerPhone"  id="passengerPhone"   <%=readonlyBecauseUpdate%>    value="<%=complaints.getPassengerPhone()%>" onclick="Checkchar()" onblur="validatePhone()" />
                     	  
                     	 </td></tr>
                     	 
         
                        <tr>
                        <td>Driver:</td>
                        <td> <input autocomplete="off" type="text"  name="DriverId"  id="DriverId" <%=readonlyBecauseUpdate%> value="<%=complaints.getDriverId()%>" /><ajax:autocomplete
				  					fieldId="DriverId"
				  					popupId="model-popup1"
				  					targetId="DriverId"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> </td>
                    	<td>Subject:</td>
                        <td> <input type="text"  name="Subject"  id="Subject"  <%=readonlyBecauseUpdate%>     value="<%=complaints.getSubject()%>" /> </td>
                        </tr>
                        <tr>
                     	<td>Description:</td>
                    	<td colspan="0"> <textarea rows="5" cols="50" name="description"  id="description" value=""></textarea> </td>
						</tr>
                        <%
                        	ArrayList<ComplaintsBean> complaint = (ArrayList<ComplaintsBean>)request.getAttribute("a_listDS");
                        %>
                        <%
                        	ComplaintsBean dsBean =new ComplaintsBean();
                        %>    
                    <%if(s1!=null) {%> 
                    <tr>
                    <td>Status:</td>
                        		<td>
                        		<select name="status"  >
                        		<option value="opened" <%=complaints.getStatus().equals("opened")?"selected":"" %>>Opened</option>
								<option value="inprogress" <%=complaints.getStatus().equals("inprogress")?"selected":"" %>>InProgress</option>
								<option value="closed" <%=complaints.getStatus().equals("closed")?"selected":"" %> >Closed By</option>                        	
                        	</select>
                       </td>                 
                               
                     </tr>
 				      <tr> 
                		<td colspan="2" align="center">
			               <div class="wid60 marAuto padT10">
                              <div class="btnBlue">
                        	    <div class="rht">
                        	     <input   type="submit"   name="Update" id = "Update" value="Update"  /> </div>
                             <div class="clrBth"></div>
                          </div>
                         <div class="clrBth"></div>
                        </div>
                      </td>  
                   </tr>
                    <%}else{ %>
                    <tr>
                    <td>Status</td>
                        		<td>
                        		<select name="status"  >
                        		<option value="opened" >Opened</option>
								<option value="inprogress" DISABLED >InProgress</option>
								<option value="closed" DISABLED >Closed By</option>                        	
                        	    </select>
                                </td>
                                <td>Trip ID</td>  
                                 <td><input type="text"id="tripId" name="tripId" value="" autocomplete="off"/></td>                        
                                 </tr>
                		<td colspan="9" align="center">
			               <div class="wid60 marAuto padT10">
                              <div class="btnBlue">
                        	    <div class="rht">
                        	     <input   type="submit"   name="submit" id = "submit" value="Submit" onclick="return validation()"/> </div>
                             <div class="clrBth"></div>
                          </div>
                         <div class="clrBth"></div>
                        </div>
                      </td>  
                   </tr></table></td>
                  
                    </tr></table> <td>
                    <div  id="errmsg" style="position: absolute;center:750px;top:320px;"  ></div>
                    </td></div></div>
                  <%}boolean colorLightblue = true;
              		 String color;  
                       if(complaint!=null && !complaint.isEmpty()) {%> 
						<table width=100%>
                        <tr style="background-color:lightgrey">
                        <td  >Previous Descriptions </td>
                        <td  >Created By </td>
                        <td  >Date And Time</td>
                        </tr>
                        <tr>
                            <%for(int i=0;i<complaint.size();i++) { 
                            	colorLightblue = !colorLightblue;
                        		if(colorLightblue){
                        			color="style=\"background-color:lightblue\""; 
                        			}
                        		else{
                        			color="";
                        		}
 	                        %> 
 	                      <tr align="center"><td <%=color%> ><%=complaint.get(i).getDesc() %>   </td>
 	                      	  <td <%=color%> ><%=complaint.get(i).getCreatedBy() %>   </td>
 	                      	  <td <%=color %>><%=complaint.get(i).getDateTime() %></td>
 	                      </tr> 
                        <%} %>
                        <%} %> 
                         
                    </table>
               
                             
</body>

</html>
                  
