<%@page import="com.tds.tdsBO.WrapperBO"%>
<%@page import="java.io.FileNotFoundException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Timer"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.CompanySystemProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<script type="text/javascript" src="Ajax/SystemUtilAjax.js"></script>




<html xmlns="http://www.w3.org/1999/xhtml">
<head>


<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

<%
	String error = "";
	String col = "color:#00FF00";
	if (request.getAttribute("error") != null) {
		error = (String) request.getAttribute("error");
	} else if (request.getParameter("error") != null) {
		error = (String) request.getParameter("error");
	}
%>


<td colspan="8">
	<div id="errorpage" style="color: #00FF00">
		<%=error.length() > 0 ? "" + error : ""%>
	</div>

	<title>Insert Wrapper</title>
</head>
<body>
	<form name="masterForm" action="control" method="post" />

	<%
		AdminRegistrationBO adminBo = (AdminRegistrationBO) session
				.getAttribute("user");
		WrapperBO wrapper = (WrapperBO) request.getAttribute("wrapper"); 
	%>
	<input type="hidden" name="action" value="systemsetup" />
	<input type="hidden" name="event" value="insertWrapper" />
	<input type="hidden" name="module" id="module"
		value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />


	<div class="rightCol" style="width: 100%; margin-left: -8px;">
		<div class="rightColIn" style="width: 100%; margin-left: -1px;">
			<h1>Create a Wrapper Account</h1>

			<table width="100%" border="0" cellspacing="0" cellpadding="0"
				class="driverChargTab">
				<tr>
					<td>
						<table id="bodypage" width="750">

							<tr>
								<td align="center" class="firstCol">Driver Id</td>
<td><input type="text" name="txtDriverId" size="25" id="txtDriverId"
									value="<%=wrapper.getDriverId() == null ? "" : wrapper.getDriverId()%>" />
									 <ajax:autocomplete
				  					fieldId="txtDriverId"
				  					popupId="model-popup1"
				  					targetId="txtDriverId"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" />  
					  </td>
								</td>
								<input type="hidden" name="hdnDriverId"
									value="<%=wrapper.getDriverId() == null ? "" : wrapper.getDriverId()%>" />
								<td align="center" class="firstCol">Cab Number</td>
								<td><input type="text" name="txtCabNumber" size="25" id="txtCabNumber"
									value="<%=wrapper.getCabNumber() == null ? "" : wrapper.getCabNumber()%>" />
									 <ajax:autocomplete
				  					fieldId="hdnDriverId"
				  					popupId="model-popup1"
				  					targetId="hdnDriverId"
				  					baseUrl="autocomplete.view"
				  					paramName="cabNum"
				  					className="autocomplete"
				  					progressStyle="throbbing" />  
								</td>
								</td>

							</tr>
							<tr>
								<td align="center" class="firstCol">Password</td>
								<td><input type="password" name="pwdWrapper" size="25"
									value="<%=wrapper.getPassword() == null ? "" : wrapper
					.getPassword()%>" />
								</td>

								<td align="center" class="firstCol">Phone Number</td>
								<td><input type="text" name="txtPhoneNumber" size="25"
									value="<%=wrapper.getPhoneNumber() == null ? "" : wrapper
					.getPhoneNumber()%>" />
								</td>


							</tr>



							<%
								if (wrapper.getDriverId() == null || wrapper.getDriverId() == "") {
							%>
							<tr>
								<td colspan="2" align="center">
									<div class="wid60 marAuto padT10">
										<div class="btnBlue">
											<div class="rht">
												<input name="btnWrapperInsert" type="submit" class="lft"
													value="Create" />
											</div>
											<div class="clrBth"></div>
										</div>
										<div class="clrBth"></div>
									</div>
								</td>
							</tr>
							<%
								} else {
							%>
							<tr>
								<td colspan="2" align="center">
									<div class="wid60 marAuto padT10">
										<div class="btnBlue">
											<div class="rht">
												<input name="btnWrapperUpdate" type="submit" class="lft"
													value="Update" />
											</div>
											<div class="clrBth"></div>
										</div>
										<div class="clrBth"></div>
									</div>
								</td>
							</tr>
						</table> <%
 	}
 %>
					</td>

				</tr>
			</table>
		</div>
	</div>
	<div class="footerDV" style="width: 93%; margin-left: -6px;">Copyright
		&copy; 2010 Get A Cab</div>
</body>
</html>
