<%@page import="com.tds.cmp.bean.ZoneTableBeanSP"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%ArrayList<ZoneTableBeanSP> allZones=(ArrayList<ZoneTableBeanSP>) request.getAttribute("zones");%> 
<% double[] Latitudes = (double[])request.getAttribute("latitude"); %>
<% double[] Longitudes = (double[])request.getAttribute("longitude"); %>
<% ArrayList<Double> externalLatAndLon = (ArrayList<Double>)request.getAttribute("externalLatitudeAndLongitude"); %>
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
<title>Review Zone</title>
<meta name="description"
	content="Find the latitude and longitude of a point using Google Maps.">
<meta name="keywords"
	content="latitude, longitude, google maps, get latitude and longitude">
 <script type="text/javascript" src="<%=TDSProperties.getValue("googleMapV3")%>"></script>
 <link rel="stylesheet" type="text/css" href="http://code.google.com/css/codesite.css"></link>
<SCRIPT type="text/javascript" src="/js/mapTypeControl.js"></SCRIPT>
<script type="text/javascript" src="js/label.js"></script>
<script type="text/javascript" src="js/polygonEdit.js"></script>
<script type="text/javascript" src="http://code.google.com/js/prettify.js"></script>
<style type="text/css">
.styled-button {
	-webkit-box-shadow: rgba(0, 0, 0, 0.0976562) 0px 1px 0px 0px;
	background-color: #EEE;
	border: 1px solid #999;
	color: #666;
	font-family: 'Lucida Grande', Tahoma, Verdana, Arial, Sans-serif;
	font-weight: bold;
	padding: 2px 6px;
	height: 28px;
}
</style>
<script type="text/javascript">
	//         
	// Latitude and Longitude math routines are from: http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html

	var map = null;
	var geocoder = null;
	var latsgn = 1;
	var lgsgn = 1;
	var zm = 0;
	var marker = null;
	var posset = 0;
	var clickCount = 0;
	var temp = 1;
	var insidePoint = true;

	function initialize() {
		var temp=0;
		var defaultLati=<%=adminBo.getDefaultLati()%>
		var defaultLongi=<%=adminBo.getDefaultLogi()%>
		var latlng = new google.maps.LatLng(defaultLati,defaultLongi);
		var options = {
				zoom: 16,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				draggableCursor: 'crosshair'
		};

		map = new google.maps.Map(document.getElementById("map"), options);
		//GEOCODER
		geocoder = new google.maps.Geocoder();
		var bounds = new google.maps.LatLngBounds();

		  marker = new google.maps.Marker({
              map: map,
              draggable: true
      });
		showLatLong1();dragMarker();
	}


	function showLatLong1() {
		var bounds = new google.maps.LatLngBounds();
		if(<%=Latitudes[1]%>!=0.000 && <%=Latitudes.length > 2%>){
		<% 
	String point = "";%>
	<%for(int i=0;i<Latitudes.length-1;i++){%>
	<%point = point + "new google.maps.LatLng(" + Latitudes[i] + "," + Longitudes[i] + "), ";%>
	
	<% } %>
	<%point = point + "new google.maps.LatLng(" + Latitudes[Latitudes.length-1] + "," + Longitudes[Latitudes.length -1] + ") ";%>
	var path=[<%=point%>];
	polygon = new google.maps.Polygon({map:map,
			paths: path,
		    strokeColor: "#0000FF",
		    strokeOpacity: 0.8,
		    strokeWeight: 3,
		    fillColor: "#15317E",
		    fillOpacity: 0.35
		  });
  	 polygon.runEdit(true);
  	var externalPoint = new google.maps.LatLng(<%=externalLatAndLon.get(0)%>,<%=externalLatAndLon.get(1)%>);
  	var externalImage="http://maps.google.com/mapfiles/ms/icons/green-dot.png";
	marker = new google.maps.Marker({position: externalPoint,
        map: map,icon:externalImage,draggable: true});
	marker.setMap(map);
	map.setCenter(new google.maps.LatLng(<%=Latitudes[0]%>,<%=Longitudes[0]%>));
	map.setZoom(14);
	posset = 1;
	} else {
	  	var points= new google.maps.LatLng(<%=Latitudes[0]%>,<%=Longitudes[0]%>);
		var image="http://itouchmap.com/i/blue-dot.png";
		marker = new google.maps.Marker({position: points,
            map: map, icon:image});
		marker.setMap(map);
		map.setCenter(points);
		map.setZoom(14);
	}
	}
	function dragMarker(){
		google.maps.event.addListener(marker, 'dragend', function() {
			geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						document.getElementById('externalLatitude_').value=marker.getPosition().lat();
						document.getElementById('externalLongitude_').value=marker.getPosition().lng();
					}
				}
			}); 
		});
	}
	function updateDispatchType(){
		var zoneNumber=document.getElementById("zoneNumber").value;
		var dispatchType=document.getElementById("shortestPath").value;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'OpenRequestAjax?event=updateDispatchType&zoneNumber='+zoneNumber+'&dispatchType='+dispatchType;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		alert(text);
	}
	var polygonZonesGlobal =[];
	var labelGlobal =[];
	var b2=new Boolean(1);	
	
	function showAllZones(){
		if(b2){
			showLatLongNew();
		} else {
			for(var i=0;i<polygonZonesGlobal.length;i++){
				polygonZonesGlobal[i].setMap(null);
				labelGlobal[i].setMap(null);
			}
			polygonZonesGlobal =[];
			labelGlobal =[];
		}
		b2=!b2;
	}
	 function showLatLongNew() {
		var fillColors =  [];
		fillColors.push("#FFA500");
		fillColors.push("#FF00FF");
		fillColors.push("#FF0000");
		fillColors.push("#00FF00");
		fillColors.push("#808000");
		fillColors.push("#616D7E");
		fillColors.push("#306EFF");
		fillColors.push("#8D38C9");
		fillColors.push("#F52887");
		fillColors.push("#00FF00");
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=allZonesForDash';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"latLong\":"+text+"}";
		var latLongList = JSON.parse(obj.toString());
		if(Number(latLongList.latLong.length)>1){
			for(var i=0;i<latLongList.latLong.length;i++){
				var latLng =[];
				for(var j=0;j<latLongList.latLong[i].ZC.length;j++){
					latLng.push(new google.maps.LatLng(latLongList.latLong[i].ZC[j].latitude ,latLongList.latLong[i].ZC[j].longitude));
				}
				if(latLng.length>2){
					var polygon = new google.maps.Polygon({paths: latLng,
						strokeColor: "#000000",
						strokeOpacity: 0.6,
						strokeWeight: 1.5,
						fillColor: fillColors[i%10],
						fillOpacity: 0.45
					});
					polygon.setMap(map);
					polygonZonesGlobal.push(polygon);
				} else {
					var image="http://itouchmap.com/i/blue-dot.png";
					var latlngSingle=new google.maps.LatLng(latLongList.latLong[i].ZC[0].latitude ,latLongList.latLong[i].ZC[0].longitude);
					marker = new google.maps.Marker({position:latlngSingle,
						map: map,icon:image,draggable: false});
					marker.setMap(map);
				}
				var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
				jobLabel.set('position', new google.maps.LatLng(latLongList.latLong[i].CLAT,latLongList.latLong[i].CLON));
				jobLabel.set('text',latLongList.latLong[i].ZD+'('+latLongList.latLong[i].ZK+')');
				jobLabel.setMap(map);
				labelGlobal.push(jobLabel);
			}
		}
	 }

</script>

<link href="/d.css" type="text/css" rel="stylesheet">
</head>
<body onload="initialize()">
<form name="masterForm" action="systemsetup" method="post" />
<input type="hidden" id="size" name="size" value="">
<input type="hidden" name="action" value="systemsetup" />
<input type="hidden" name="event" value="createqueueCoordinate" />
<input type="hidden" id="outsidePoint" name="outsidePoint" value="">
<input type="hidden" id="rowSize" name="rowSize" value="">
<input  type="hidden"   name="module"    id="module"  value="systemsetupView">
<center>
<div id="h">
<div id="h0"></div>
<div id="o">
<div id="content">
<table  width="100%">
	<tr valign="top">
		<h1>Review Zone (<%=allZones.get(0).getZoneDesc() %>)<img src="images/Dashboard/zoneLogin.gif" onclick="showAllZones()"/>
		</h1>
						<%
							String error ="";
						if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
					%>
						<h2>
							<%= error.length()>0?""+error :"" %>
					   		</h2>
				   		<%} %>
		<div id="wrapper" style="margin: 5px">
		<div id="map" style="width:1250px; height: 465px;"></div>
		<div id="info" style="display: none" ></div>
		</div>
		<table>
				<tr>
				<td><font style="font-size: 15px; font-weight: bold;  background: EFEFFB;">Zone Number</font>
				<input type="text" name="zone" size="5" id="zoneNumber" style="font-weight: bold; font-size:20; background-color:#EFEFFB; color: red;" align="center" value="<%=request.getParameter("queueid")==null?allZones.get(0).getZoneKey():request.getParameter("queueid")%>" readonly>
			 	<input type="submit" value="Close" style="width: 105px; height: 25px; font-size: 10px; font-weight: bold;  background: lightgreen;" name="reset" >
			 	<input type="submit" value="Delete" style="width: 105px; height: 25px; font-size: 10px; font-weight: bold;  background: lightgreen;" name="delete" >
			 	<input type="button" value="Complete Design" style="width: 105px; height: 25px; font-size: 10px; font-weight: bold;  background: lightgreen;" onclick="getValue()">
			 	<input type="submit" disabled="disabled" value="Update" style="width: 105px; height: 25px; font-size: 10px; font-weight: bold;  background: lightgreen;" name="updateZone" id="updateZone"></td>
				</tr>
		</table>
		<% if(Latitudes.length >0  ) {  %>
		<table id="latlongShow" style="display: none" width="15%" border="3" cellspacing="3" cellpadding="3" class="driverChargTab" align="center">
			 <th width="15%">Latitude</th>
			<th width="15%">Longitude</th>
			<%for(int i=0;i<Latitudes.length;i++){ %>
			<tr>
				<td><input name="latbox_"<%=i%> id="latitude_"<%=i%> value="<%=Latitudes[i] %>"></td>
				<td><input name="lonbox_"<%=i%> id="longitude_"<%=i%> value="<%=Longitudes[i] %>"></td>
			</tr>
			<%}%>
			<%} %> 
			</table>
			<table id="extLatLongShow" style="display: none" width="15%" border="3"  class="driverChargTab" align="center">
			<%if(externalLatAndLon.size() > 0){ %>
			<th width="15%">External&nbsp;Latitude</th>
			<th width="15%">External&nbsp;Longitude</th>
			<tr>
				<td><input name="externalLatitude_"<%=0%> id="externalLatitude_"<%=0%> value="<%=externalLatAndLon.get(0) %>"></td>
				<td><input name="externalLongitude_"<%=1%> id="externalLongitude_"<%=1%> value="<%=externalLatAndLon.get(1) %>"></td>
			</tr>
			<%}%>
			</table>
			
</table>
</div>
</div>
</center>
</body>
</html>
