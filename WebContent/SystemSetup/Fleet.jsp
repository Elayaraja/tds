
<%@page import="com.tds.tdsBO.DocumentBO"%>
<%@page import="com.tds.security.bean.TagSystemBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	    <%@page import="com.tds.tdsBO.FleetBO"%>
	
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %> 
	<%if(session.getAttribute("fleetList")!=null) {
		fleetList=(ArrayList)session.getAttribute("fleetList");
		int listsize=fleetList.size();
	} %>
		<%ArrayList<FleetBO> fleetListForACompany= (ArrayList<FleetBO>)(request.getAttribute("getFleet")==null?new ArrayList<FleetBO>():request.getAttribute("getFleet"));%>
	
	<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
	<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
	<script src="js/CalendarControl.js" language="javascript"></script>
	
	
	<script>

	
		function delrow(){
			try {
				if(document.getElementById("size").value>0){
			 var table = document.getElementById("fleetMapping");
			 var rowCount = table.rows.length;
			 var temp=(Number(document.getElementById("size").value)-1);
			document.getElementById('size').value=temp;
			rowCount--;
				
			table.deleteRow(rowCount);
				}
			}catch(e){alert(e.message);}
			
			
		}

		function cal(){
			var i=Number(document.getElementById("size").value)+1;
			document.getElementById('size').value=i;
			var table = document.getElementById("fleetMapping");
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			  var cell1 = row.insertCell(0);
			  var cell2 = row.insertCell(1);
			  var element1 = document.createElement("input");
			  element1.type = "text";
			  element1.name="fleetName"+i;
			  element1.id="fleetName"+i;
			  element1.align='right'; 
			  element1.size = '10';
			  element1.value="";
			  cell1.appendChild(element1);
			  	var element2 = document.createElement("select");
			 	 element2.name="fleet"+i;
				 element2.id="fleet"+i;
				  <%if(fleetListForACompany!=null && fleetListForACompany.size()>0){
						for(int i=0;i<fleetList.size();i++){ %>
						  var theOption = document.createElement("option");
						  theOption.text="<%=fleetListForACompany.get(i).getFleetName() %>";
							theOption.value="<%=fleetListForACompany.get(i).getFleetNumber()%>";
							element2.options.add(theOption);
							theOption = document.createElement("option");
					<% }}%> 
					cell2.appendChild(element2);
			}
	 	function deleteFleet(assoccode,x){
			  var xmlhttp=null;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
			url='SystemSetupAjax?event=deleteFleet&module=systemsetupView&delete=YES&assoccode='+assoccode;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if(text=="1"){
				var table=document.getElementById("fleetMapping");
				table.deleteRow(x.parentNode.parentNode.rowIndex);
			}
		}  
	 	function showProcess(){
	 		if(document.getElementById("TDSAppLoading")!=null){
	 			$("#TDSAppLoading").show();
	 		}
	 		return true;
	 	}
	</script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>TDS (Taxi Dispatch System)</title>
</head>
<body>
<%ArrayList<DocumentBO> getDocuments= (ArrayList<DocumentBO>)(request.getAttribute("getDocuments")==null?new ArrayList<TagSystemBean>():request.getAttribute("getDocuments"));%>
	<% String pass=(String)request.getAttribute("password"); %>
	 <%String result="";
			if(request.getAttribute("page")!=null) {
			result = (String)request.getAttribute("page");
			}
			%>
			<div id="errorpage" style="">
			<%=result.length()>0? result:"" %>
			</div> 
	<form name="fleetMasterForm" action="control" method="post"  onsubmit="return showProcess()"/>
	<input type="hidden" name="action" value="systemsetup" />
	<input type="hidden" name="event" value="fleetMapping" />
	<input type="hidden" name="button" id="button" value="" />
	<input type="hidden" name="size" id="size" value=""/>
	<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
	<c class="nav-header"> <center>Fleet Mapping</center></c>
	<%-- <input type="text" value="<%=fleetList.size() %>" /> --%>
	<table width="90%" align="center">
	<tr> <td >
	  	<table width="70%" border="0" align="center"  cellspacing="0" cellpadding="0" id="fleetMapping">
       	<tr align="left" style="background-color: grey; color: silver; font-size: large; font-weight: bold;">
       	 <th>Fleet Name</th>       	 
       	 <th>FleetColor</th>
       	 <th>Fleet Action </th>       	
     	</tr>       			
						<%if(fleetList!=null && 	fleetList.size()>0){ %>
							<%for(int i=0;i<fleetList.size();i++){ %>
							<tr align="left">
								<td ><input type="text" size="10"  readonly="readonly" id="doc_<%=i%>" value="<%=fleetList.get(i).getFleetName() %>"/></td>																								
								<td><input type="text" readonly="readonly" id="fleetcolor<%=i%>" style="background-color:#<%=fleetList.get(i).getFcolor() %>" value="#<%=fleetList.get(i).getFcolor() %>"></input></td> 
								<td>
								<img alt="" src="jsp/images/deletebutton.jpg"  height="15%" width="15%" onclick="deleteFleet('<%=fleetList.get(i).getFleetNumber() %>',this)" />
								</td>
							</tr>
								
								<%} %>		
		       					</table> 
		       					</td> 
		       					</tr>
								<tr>
							<td align="center">   
	                                 <input type="button" name="Button" value="Edit Fleet Name" onclick="cal()"></input>
	                                 &nbsp;    <input type="button" name="Button" value="Cancel"  onclick="delrow()"></input>
	                                 &nbsp;    <input type="submit" name="submit" value="Submit"/>                                 
	   					  </td>
   					  <%}else{ %>
   					  <td colspan="3" align="center" style="font-size: large;">No Fleets Found   					  
   					  </td>  
   					  <%} %>
   					  </tr>                                       
      </table> 
	
</body>
</html>