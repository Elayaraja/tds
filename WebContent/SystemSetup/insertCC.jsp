<%@page import="com.tds.tdsBO.CompanyMasterBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.tdsBO.CompanyMasterBO,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@page import="com.common.util.TDSProperties"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.tds.dao.RequestDAO"%>
<%@page import="java.util.HashMap"%>

<head>
<jsp:useBean id="cmpBean" class="com.tds.tdsBO.CompanyMasterBO"
	scope="request" />
<%
	if (request.getAttribute("cmpBean") != null)
		cmpBean = (CompanyMasterBO) request.getAttribute("cmpBean");
%>
<style type="text/css">
#color {
	font-size: 100%;
	color: white;
	text-align: center;
	background-color: rgb(46, 100, 121);
	height: 40%;
	width: 100%;
}
</style>
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}

$(document).ready ( function(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'SystemSetupAjax?event=getCCDetails';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"ccDetails\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	for(var j=0;j<obj.ccDetails.length;j++){
		document.getElementById("userName").value=obj.ccDetails[j].UN;
		document.getElementById("password").value=obj.ccDetails[j].PW;
		document.getElementById("customerName").value=obj.ccDetails[j].CN;
		document.getElementById("provider").value=obj.ccDetails[j].PR;
		document.getElementById("Address").value=obj.ccDetails[j].EM;
		if(document.getElementById("provider").value=="2"){
			document.getElementById("slimCdOptions").style.display="block";
			document.getElementById("siteId").value=obj.ccDetails[j].SI;
			document.getElementById("priceId").value=obj.ccDetails[j].PI;
			document.getElementById("key").value=obj.ccDetails[j].K;
		}
	}
});
function showExtraOptions(){
	if(document.getElementById("provider").value=="2"){
		document.getElementById("slimCdOptions").style.display="block";
	} else {
		document.getElementById("slimCdOptions").style.display="none";
	}
}
</script>
</head>
<body>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<form method="post" action="control" name="queue" onsubmit="return showProcess()"/>
	<input type="hidden" name="action" value="systemsetup" />
	<input type="hidden" name="event" value="ccUsername" />
	<input type="hidden" name="module" id="module"
		value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />
	<c class="nav-header"><center>CreditCard Details</center></c>
	<div>
		<div>

			<table width="60%" border="0" align="center"
				style="background-color: rgb(239, 245, 245); text-align: center;"
				cellspacing="0" cellpadding="0" class="driverChargTab">
				<tr>
					<%
						String error = "";
						if (request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
						} else if (request.getParameter("errors") != null) {
							error = (String) request.getParameter("errors");
						}
					%>
					<td colspan="7">
						<div id="errorpage">
							<%=error.length() > 0 ? "" + error : ""%>
						</div>
					</td>
				</tr>
				<tr>
					<td>User Name</td>
					<td><input type="text" name="userName" id="userName"
						value="<%=cmpBean.getCcUsername() != null ? cmpBean.getCcUsername()
					: ""%>" />
					</td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="text" name="password" id="password"
						value="<%=cmpBean.getCcPassword() != null ? cmpBean.getCcPassword()
					: ""%>" />
					</td>
				</tr>
				<tr>
					<td>Customer Name</td>
					<td><input type="text" name="customerName" id="customerName"
						value="<%=cmpBean.getCname() == null ? "" : cmpBean.getCname()%>" /></td>
					<tr>
						<td>Provider</td>
						<td><select name="provider" id="provider" onchange="showExtraOptions()">
								<option value="0" selected>Select</option>
								<option value="1" <%=cmpBean.getCcProvider() == 1 ? "selected" : ""%>>Direct Payment</option>
								<option value="2" <%=cmpBean.getCcProvider() == 2 ? "selected" : ""%>>Slim CD</option>
						</select></td>
					</tr>
					<tr>
						<td>Vendor Type</td>
						<td><select name="vendor" id="vendor">
								<option value="0" selected>Select</option>
								<option value="6273"
									<%=cmpBean.getCcVendor() == 1 ? "selected" : ""%>>1</option>
								<option value="2" <%=cmpBean.getCcVendor() == 2 ? "selected" : ""%>>2</option>
								<option value="3" <%=cmpBean.getCcVendor() == 3 ? "selected" : ""%>>3</option>
						</select></td>
					</tr>
					<tr>
						<td>URL</td>
						<td><input type="text" name="Address" id="Address"
							value="<%=cmpBean.getEmail() == null ? "" : cmpBean.getEmail()%>" /></td>
					</tr>
					<tr>
						<td colspan="6">
							<div align="center">
								<input type="submit" name="Submit" value="Submit" class="lft" />
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="6">
							<div
								style="text-align: center; background-color: rgb(211, 224, 236); height: 40%;">Copyright
								&copy; 2010 Get A Cab</div>
						</td>
					</tr>
			</table>
		</div>
	</div>
				<div id="slimCdOptions" style="display: none;">
					<table>
						<tr><td>Site Id:</td><td>
						<input type="text" name="siteId" id="siteId" value="<%=cmpBean.getSiteId()==null?"":cmpBean.getSiteId()%>"/>
						</td></tr>
						<tr><td>Price Id:</td><td>
						<input type="text" name="priceId" id="priceId" value="<%=cmpBean.getPriceId()==null?"":cmpBean.getPriceId()%>"/>
						</td></tr>
						<tr><td>Key</td><td>
						<input type="text" name="key" id="key" value="<%=cmpBean.getKey()==null?"":cmpBean.getKey()%>"/>
						</td></tr>
					</table>
				</div>




</body>
</html>
