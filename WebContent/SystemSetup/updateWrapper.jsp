<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.WrapperBO"%>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
function deleteWrapper(driverId,x){
	
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=deleteWrapper&module=systemsetupView&delete=YES&driverId='+driverId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="Deleted Successfully"){
	var table = document.getElementById("updateWrapper");
	 table.deleteRow(x.parentNode.parentNode.rowIndex);
	}
	

}

</script>
<body>
	<form name="masterForm" action="control" method="post">
		<%
			ArrayList<WrapperBO> wrapperList = (ArrayList<WrapperBO>) request
					.getAttribute("wrapperList");
		%>
		<%
			AdminRegistrationBO adminBo = (AdminRegistrationBO) session
					.getAttribute("user");
		%>
		<input type="hidden" name="action" value="systemsetup" /> <input
			type="hidden" name="event" value="updateWrapper" /> <input
			type="hidden" name="module" id="module"
			value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />

		<%
			String error = "";
			String col = "color:red";
			if (request.getAttribute("error") != null) {
				error = (String) request.getAttribute("error");
			} else if (request.getParameter("error") != null) {
				error = (String) request.getParameter("error");
			}
		%>


		<td colspan="8">
			<div id="errorpage" style="color: red;">
				<%=error.length() > 0 ? "" + error : ""%>
			</div>

			<div class="leftCol">
				<div class="rightCol">
					<div class="rightColIn">
						<table width="100%" style="width: 750px" border="1"
							cellspacing="1" cellpadding="0" class="driverChargTab">
							<td colspan='7' align='center'><h1>
									<center>Update Wrapper</center>
								</h1></td>
							<tr>

								<tr>
									<td class="firstCol">Driver Id:</td>
									<td><input type="text" name="txtDriverId" id="txtDriverId"
										value="" /></td>
									<td class="firstCol">Phone Number:</td>
									<td><input type="text" name="txtPhoneNumber"
										id="txtPhoneNumber" value="" /></td>
								</tr>
								<tr>
									<td class="firstCol">Cab Number:</td>
									<td><input type="text" name='txtCabNumber'
										id="txtCabNumber" value="" />
									</td>
								</tr>
								<tr>

									<td colspan="4" align="center">
										<div class="wid60 marAuto padT10">
											<div class="btnBlue">
												<div class="rht">
													<input type="submit" name="btnSearch" id="btnSearch"
														class="lft" value="Submit" />
												</div>
												<div class="clrBth"></div>
											</div>
											<div class="clrBth"></div>
										</div>
									</td>
								</tr>
						</table>
						<%
							if (wrapperList != null && wrapperList.size() > 0) {
						%>

						<table width="100%" style="width: 750px" border="0"
							cellspacing="0" cellpadding="0" class="driverChargTab">
							<tr>
								<td>
									<table id="updateWrapper" style="width: 750px" id="bodypage" width="540">
										<tr>

											<td class="firstCol">DriverId</td>
											<td class="firstCol">Password</td>
											<td class="firstCol">Phone</td>
											<td class="firstCol">CabNumber</td>
										</tr>
										<%
											boolean colorLightGreen = true;
												String colorPattern;
										%>
										<%
											for (int i = 0; i < wrapperList.size(); i++) {
													colorLightGreen = !colorLightGreen;
													if (colorLightGreen) {
														colorPattern = "style=\"background-color:lightgreen\"";
													} else {
														colorPattern = "";
													}
										%>
										<tr>

											<td align="center" <%=colorPattern%>><font color="black"><%=wrapperList.get(i).getDriverId()%></font>
											</td>
											<td align="center" <%=colorPattern%>><font color="black"><%=wrapperList.get(i).getPassword()%></font>
											</td>

											<td align="center" <%=colorPattern%>><font color="black"><%=wrapperList.get(i).getPhoneNumber()%></font>
											</td>
											<td align="center" <%=colorPattern%>><font color="black"><%=wrapperList.get(i).getCabNumber()%></font>
											</td>
											<td><a style="text-decoration: none;"
												href="control?action=systemsetup&event=updateWrapper&update=YES&module=systemsetupView&driverId=<%=wrapperList.get(i).getDriverId()%>"><font
													color="black">Update</font>
											</a></td>
											<%-- <td><a style="text-decoration: none;"
												href="control?action=systemsetup&event=updateWrapper&delete=YES&module=systemsetupView&driverId=<%=wrapperList.get(i).getDriverId()%>"><font
													color="black">Delete</font>
											</a></td> --%>
											<td><input type="image" name="delete" id="delete" value="delete" onclick="deleteWrapper(<%=wrapperList.get(i).getDriverId()%>,this)"/> </td>
										</tr>
										<%
											}
											}
										%>

									</table>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
	</form>
</body>
</html>
