<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.AdjacentZonesBO"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS(Taxi Dispatching System)</title>
<%ArrayList<AdjacentZonesBO> al_adjZones = new ArrayList<AdjacentZonesBO>();
if(request.getAttribute("zones")!=null){
	al_adjZones = (ArrayList<AdjacentZonesBO>)request.getAttribute("zones");
}%>

</head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
function delrow(){
	try {
	 var table = document.getElementById("zones");
	 var rowCount = table.rows.length;
	 var temp=(Number(document.getElementById("size").value)-1);
	document.getElementById('size').value=temp;
	rowCount--;

	table.deleteRow(rowCount);
	}catch(e){alert(e.message);}
}

function cal(){
	var i=Number(document.getElementById("size").value)+1;
	document.getElementById('size').value=i;
	var table = document.getElementById("zones");
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	 var cell = row.insertCell(0);
	  //var cell1 = row.insertCell(1);
	  var cell1 = row.insertCell(1);
	  //var cell3= row.insertCell(3);
	  var cell2= row.insertCell(2);
 
	  
	  var element = document.createElement("input");
	  element.type = "text";
	  element.className="form-autocomplete";
	  element.name="zoneNumber"+i;
	  element.id="zoneNumber"+i;
	  element.align='right'; 
	  element.onkeypress=function(){autocomplete("zoneNumber"+i, "model-popup2", "zoneNumber"+i, "autocomplete.view", "ZONES", null, "throbbing",null);};
	  //element.onblur= function(){zoneName(element.value,1,i);};
	  element.size = '10';
	  cell.appendChild(element);
	  element = document.createElement("div");
	  element.className="autocomplete";
	  element.id ="model-popup2"; 
	  cell.appendChild(element);
	  
	  
	 /*  var element1 = document.createElement("input");
	  element1.type = "image";
	  element1.name="zoneName"+i;
	  element1.id="zoneName"+i;
	  element1.align='right'; 
	  element1.size = '10';
	  element1.value="";
	  cell1.appendChild(element1); */
	  
		
		
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name="AdjacentZones"+i;
		element2.id="AdjacentZones"+i;
		//element2.onblur=function() {zoneName(element2.value,2,i);};
		element2.size = '10';
		element2.className= 'ss';
		element2.onkeypress=function(){autocomplete("AdjacentZones"+i, "model-popup2", "AdjacentZones"+i, "autocomplete.view", "ZONES", null, "throbbing",null);};
		cell1.appendChild(element2);
		
		
	/* 	var element3 = document.createElement("input");
		  element3.type = "image";
		  element3.name="AdjzoneName"+i;
		  element3.id="AdjzoneName"+i;
		  element3.align='right'; 
		  element3.size = '10';
		  element3.value="";
		  cell3.appendChild(element3); */
		
		var element4 = document.createElement("select");
	 	 element4.name="orderNumber"+i;
		 element4.id="orderNumber"+i;
		 element4.id="orderNumber"+i;
		
		  var theOption = document.createElement("option");
		  theOption.text="1";
			theOption.value="1";
			element4.options.add(theOption);
			theOption = document.createElement("option");
			
			 theOption.text="2"; 
			theOption.value="2";
			element4.options.add(theOption);
			theOption = document.createElement("option");
			theOption.text="3";
			theOption.value="3";
			element4.options.add(theOption);
			theOption = document.createElement("option");
			theOption.text="4";
			theOption.value="4";
			element4.options.add(theOption);
			theOption = document.createElement("option");
			theOption.text="5";
			theOption.value="5";
			element4.options.add(theOption);
			theOption.text="6";
			theOption.value="6";
			element4.options.add(theOption);
			theOption = document.createElement("option");
			 theOption.text="7"; 
			theOption.value="7";
			element4.options.add(theOption);
			theOption = document.createElement("option");
			theOption.text="8";
			theOption.value="8";
			element4.options.add(theOption);
			theOption = document.createElement("option");
			theOption.text="9";
			theOption.value="9";
			element4.options.add(theOption);
			theOption = document.createElement("option");
			theOption.text="10";
			theOption.value="10";
			element4.options.add(theOption); 
			//cell1.appendChild(element); */
			cell2.appendChild(element4);
			
		 <%-- <%for(int i=0;i<=10;i++){%>
			var theOption = document.createElement("option");
			theOption.text=i;
			theOption.value=i;
			element.options.add(theOption);
			<%}%> --%>
	}
	
function emptyZoneName(value,row){
	if(value==1){
		document.getElementById("zoneName"+row).value="";
	}else{
		document.getElementById("AdjzoneName"+row).value="";
	}
	
}

function checkval(){
			document.masterForm.submit();
}
$(document).ready ( function(){
	 <%if(request.getAttribute("zones")!=null){%>
	 		loadAdjZones();
	 <%}%>
});
function loadAdjZones(){
	<%for(int i=0;i<al_adjZones.size();i++){%>
		cal();
		var j=<%=i+1%>;
		document.getElementById("zoneNumber"+j).value="<%=al_adjZones.get(i).getZoneNumber()%>";
		document.getElementById("AdjacentZones"+j).value="<%=al_adjZones.get(i).getAdjacentZones()%>";
		document.getElementById("orderNumber"+j).value=<%=al_adjZones.get(i).getOrderNumber()%>;
	<%}%>
}
function zoneName(zoneNum,switchKey,row){
	if(zoneNum!=""){
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url='AjaxClass?event=getZoneName&zoneNum='+zoneNum;
		xmlhttp.open("GET",url,false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;
		var resp=text.split("###");
		if(resp[0]!=null && resp[0]!=""){
			
			if(switchKey==1){
				document.getElementById("zoneName"+row).value=resp[0];
			}else{
			document.getElementById("AdjzoneName"+row).value=resp[0];
			}
		} else {
			alert("No zones found");
			document.getElementById("zoneNumber"+row).value="";
			document.getElementById("AdjzoneName"+row).value="";

		}
	}
	
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
<body>
<form method="post" action="control" name="queue" onsubmit="return showProcess()">
 <input type="hidden" name="action" value="systemsetup">
<input type="hidden" name="event" value="adjacentZones">
<input type="hidden" name="module" value="systemsetupView">
<input type="hidden" id="size" name="size" value="0">
<c class="nav-header"><center>Insert Adjacent Zones</center></c>
						<%
							String error ="";
							if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
							} 
					%>
							<div id="errorpage" class="alert-error">
							<%= error.length()>0?""+error :"" %>
					   		</div>
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top" id="zones">
                      <tr>
                        <th width="15%" class="firstCol">Zone Number</th>
                        <th width="15%">Adjacent zone</th>
                        <th width="15%">Adj Order Number</th>
                      </tr>
                    </table>
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
					  <tr>
					  	<td colspan="4" align="center">
						  <input type="button" name="Button" value="ADD" onclick="cal()" class="lft">
	                     <input type="button" name="Button" value="Remove"  onclick="delrow()" class="lft" >
	                     <input type="submit" name="button" class="lft" value="Submit"/>
	                     </td>
					</tr>
</table>	
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
</form> 
</body>
</html>