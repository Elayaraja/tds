<%@page import="com.tds.tdsBO.DocumentBO"%>
<%@page import="com.tds.security.bean.TagSystemBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
	<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"></link>
	<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript">
function checkPassword(){
	var password=document.getElementById("password").value;
	var rePassword=document.getElementById("rePassword").value;
	if(password!="" && rePassword!=""){
		if(password==rePassword){
			document.getElementById("changePassword").value="Submit";
			document.masterForm.submit();
		}else{
			alert("Your password & re-password doesn't match");
			document.getElementById("password").value="";
			document.getElementById("rePassword").value="";
			document.getElementById("password").focus();
		}
	}
}
function submitFunction(){
	showProcess();
	document.getElementById("submitData").value="Submit";
	document.masterForm.submit();
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Tag Computers</title>
</head>
<body>
<form name="masterForm" id="masterForm" action="control" method="post" >
<% String value="";
if(request.getAttribute("value") != null)
	value = (String)request.getAttribute("value");
%>
	<input type="hidden" name="action" value="systemsetup" />
	<input type="hidden" name="event" value="userProfile" />
	<input type="hidden" name="button" id="button" value="" />
	<input type="hidden" name="size" id="size" value=""/>
	<input type="hidden" name="changePassword" id="changePassword" value=""/>
	<input type="hidden" name="submitData" id="submitData" value=""/>
	<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
	<div class="leftCol"></div>
	<div class="clrBth"></div>
                <div class="rightCol">
				<div class="rightColIn">
	<h1>User Profile</h1>
	  	<table width="750" border="0" cellspacing="0" cellpadding="0" style="left: 80px;" class="driverChargTab" id="documentTypes">
        <tr>
						<%
							String error ="";
							if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
							} else if(request.getParameter("errors")!=null)
							{
							error = (String)request.getParameter("errors");
							}
					%>
						<td colspan="7">
							<div id="errorpage">
							<%= error.length()>0?""+error :"" %>
					   		</div>
				   		</td>
				 </tr>
      <tr><td>
      OpenRequest Screen Format:
      </td>
      <td>
      <select name="openrequestFormat" id="openRequestformat">
    <option value="1" <%=value==null?"":value.equals("1")?"selected":"" %>>Format1</option>
    <option value="2" <%=value==null?"":value.equals("2")?"selected":"" %>>Format2</option>
    <option value="3" <%=value==null?"":value.equals("3")?"selected":"" %>>Format3</option>
      </select>
      </td>
      </tr>
      <tr>
      <td>
      <input type="button" name="finish" id="finish" value="Submit" onclick="submitFunction()"/>
      </td>
      </tr>
      <tr style="background-color: gray;">
      <td colspan="2" align="center"><b>Change Login Password:</b></td>
      </tr>
      <tr>
      <td>
      Current Password:
      </td>
      <td>
            <input type="password" name="currentPassword" id="currentPassword" value="" />
      </td>
      </tr>
      <tr>
      <td>New Password:</td>
      <td>
      <input type="password" name="password" id="password" value=""/>
      </td>
    </tr>
      <tr>
     <td>Retype Password:</td>
      <td>
      <input type="password" name="rePassword" id="rePassword" value=""/>
      </td>
      </tr>
        <tr><td>
        <input type="button" name="changePassword" id="changePassword" value="Change Password" onclick="checkPassword()"/>
        </td></tr>                              
      </table> 
	</div></div>
</form>
</body>
</html>