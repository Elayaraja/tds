<%@page import="com.tds.tdsBO.OpenRequestFieldOrderBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>


<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.Address" %>
<%@page import="com.tds.cmp.bean.CustomerProfile" %>

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%
ArrayList<OpenRequestFieldOrderBO> fieldList = new ArrayList<OpenRequestFieldOrderBO>();
if(request.getAttribute("fieldsList")!=null){
	fieldList =  (ArrayList<OpenRequestFieldOrderBO>) request.getAttribute("fieldsList");
	System.out.println(fieldList.get(2).getVendorName().toString());

}

%> 
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>User Address Details</title>
<style type="text/css">
	#colr
	{
		border:0px solid blue;
		color:'#000000';
		background-color:lightblue;
		font-family:Times New Roman;
		font-size:14px;
	}
	#val
	{
		background-color:lightgray;
		font-size:14px;
		color:'#000000';
		font-family:Times New Roman;
		border:0px solid blue;
		width:40%;
	}
	tr
	{
		width:40%;
	}
	td
	{
		width:20%;
	}
</style>
<script type="text/javascript">
var nocount;
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}

$(document).ready ( function(){
	//alert("")
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=reviewuploadfilemapping';
	//alert(url);
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text=xmlhttp.responseText;
	var jsonObj = "{\"flagDetails\":"+text+"}";
	var obj = JSON.parse(jsonObj.toString());
	$("#val").append("<table>"); 

	for(var i=0;i<obj.flagDetails.length;i++)
		{
			//$("#val").append("<tr align=center style='width:100%; background-color:lightgreen'><td style='width:10%;'><input type='checkbox' name=check"+i+" id=check"+i+" style='visibility:hidden' value="+obj.flagDetails[i].vendor+"></input>"+obj.flagDetails[i].vname+"</td><td style='width:10%;'>"+obj.flagDetails[i].name+"</td><td style='width:10%;'>"+obj.flagDetails[i].ph+"</td><td style='width:10%;'>"+obj.flagDetails[i].gsa1+"</td><td style='width:10%;'>"+obj.flagDetails[i].gsa2+"</td><td style='width:10%;'>"+obj.flagDetails[i].gea1+"</td><td style='width:10%;'>"+obj.flagDetails[i].gea2+"</td><td style='width:10%;'>"+obj.flagDetails[i].gsc+"</td><td style='width:10%;'>"+obj.flagDetails[i].gec+"</td><td style='width:10%;'>"+obj.flagDetails[i].gsl+"</td></tr>");
			//$("#val").append("<tr align=center style='width:100%; background-color:lightgreen'><td>"+obj.flagDetails[i].gslo+"</td><td>"+obj.flagDetails[i].gedl+"</td><td>"+obj.flagDetails[i].gedlo+"</td><td>"+obj.flagDetails[i].gs+"</td><td>"+obj.flagDetails[i].gsd+"</td><td>"+obj.flagDetails[i].gpt+"</td><td>"+obj.flagDetails[i].gac+"</td><td>"+obj.flagDetails[i].ga+"</td><td>"+obj.flagDetails[i].goc+"</td><td>"+obj.flagDetails[i].goc2+"</td></tr>");
			//$("#val").append("<tr style=\"visibility: hidden\"><td>Start Longitude</td><td>End Latitude</td><td> End Longitude</td><td>Service Time</td><td>Service Date</td><td>Pay Type</td><td>Account</td><td>Amount</td><td>Operator Comments</td><td>Dispatch Comments</td></tr>");
			$("#val").append("<tr align=center style='width:100%;background-color:lightgreen'><td style='width:35%'>"+(i+1)+"</td><td style='width:30%;'>"+obj.flagDetails[i].vname+"</td><td style='width:40%;'><input type=button id=edit"+i+" value=edit onclick='edit("+obj.flagDetails[i].vendor+")'/></td><td style='width:35%;'><input type='button' id='delete"+i+"' value='delete' onclick='deleted("+obj.flagDetails[i].vendor+")'/></td></tr>");
			$("#val").append("<tr align=center style='width:100%;background-color:lightgreen;visibility:hidden'><td style='width:25%'>S.No</td><td style='width:25%'>Vendor Name</td><td style='width:25%'>Edit</td><td style='width:25%'>Delete</td>");
			nocount=i;
		}
	$("#val").append("</table>");
	});	
	/* function deletevalue2()
	{
		alert("delete function calling");
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{ 
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		//alert(url);
		for(var i=0;i<=nocount;i++)
		{
			if(document.getElementById('check'+i).checked)
			{
				//alert(document.getElementById("check"+i).value);
				var vendor=document.getElementById("check"+i).value;
				var url='SystemSetupAjax?event=deletereviewuploadfilemapping&vendor='+vendor;
				xmlhttp.open("GET", url, false);
				xmlhttp.send(null);
				var text=xmlhttp.responseText;
				//alert(text);
			}
		}
		window.location.href = "mainNew.jsp?screen=/SystemSetup/reviewUploadFileMapping.jsp&module=systemsetupView";
	} */
	function deleted(dl)
	{
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url='SystemSetupAjax?event=deletereviewuploadfilemapping&vendorid='+dl;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;
		alert(text);
		window.location.href = "mainNew.jsp?screen=/SystemSetup/reviewUploadFileMapping.jsp&module=systemsetupView";
	}
	
	function edit(ed)
	{
		document.getElementById("colr").style.display="none";
		document.getElementById("val").style.display="none";
		//document.getElementById("viewbutton").style.display="none";
		//document.getElementById("deletebutton").style.display="none";
		document.getElementById("backbutton").style.display="block";
		
		var val = ed;
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url='SystemSetupAjax?event=reviewuploadfilemapping&vendor='+ed;
		//alert(url);
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;
		var jsonObj = "{\"flagDetails\":"+text+"}";
		var obj = JSON.parse(jsonObj.toString());
		for(var i=0;i<obj.flagDetails.length;i++)
		{
			if(val==obj.flagDetails[i].vendor)
				{
					//alert(obj.flagDetails[i].vname);
					var c=0;
					$("#uploadformate").append("<center>Vendor Name : "+obj.flagDetails[i].vname+"<br>");
									//alert(obj.flagDetails[i].vname);
					$("#uploadformate").append("<table align=center>");
					$("#uploadformate").append("<tr><td>S.No</td><td>Value</td><td>Position</td></tr>");
					if(obj.flagDetails[i].name!=null  && obj.flagDetails[i].name!='')
						{
							c++;
					$("#uploadformate").append("<tr><td>"+c+"</td><td>Name</td><td>"+obj.flagDetails[i].name+"</td></tr>");
						}
					if(obj.flagDetails[i].ph!=null && obj.flagDetails[i].ph!='')
					{
						c++;
					$("#uploadformate").append("<tr><td>"+c+"</td><td>Phone</td><td>"+obj.flagDetails[i].ph+"</td></tr>");
					}
					if(obj.flagDetails[i].gsa1!=null && obj.flagDetails[i].gsa1!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Starting Address 1</td><td>"+obj.flagDetails[i].gsa1+"</td></tr>");
					}
					if(obj.flagDetails[i].gsa2!=null && obj.flagDetails[i].gsa2!='')
					{
						c++;
						//alert(obj.flagDetails[i].gsa2);
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Starting Address 2</td><td>"+obj.flagDetails[i].gsa2+"</td></tr>");
					}
					if(obj.flagDetails[i].gea1!=null && obj.flagDetails[i].gea1!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>End Address 1</td><td>"+obj.flagDetails[i].gea1+"</td></tr>");
					}
					if(obj.flagDetails[i].gea2!=null &&  obj.flagDetails[i].gea2!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>End Address 2</td><td>"+obj.flagDetails[i].gea2+"</td></tr>");
					}
					if(obj.flagDetails[i].gsc!=null && obj.flagDetails[i].gsc!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Start City</td><td>"+obj.flagDetails[i].gsc+"</td></tr>");
					}
					if(obj.flagDetails[i].gec!=null && obj.flagDetails[i].gec!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>End City</td><td>"+obj.flagDetails[i].gec+"</td></tr>");
					}
					if(obj.flagDetails[i].gsl!=null && obj.flagDetails[i].gsl!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Start Latitude</td><td>"+obj.flagDetails[i].gsl+"</td></tr>");
					}
					if(obj.flagDetails[i].gslo!=null && obj.flagDetails[i].gslo!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Start Longtitude</td><td>"+obj.flagDetails[i].gslo+"</td></tr>");
					}
					
					//want
					if(obj.flagDetails[i].gedl!=null && obj.flagDetails[i].gedl!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>End Latitude</td><td>"+obj.flagDetails[i].gedl+"</td></tr>");
					}
					if(obj.flagDetails[i].gedlo!=null && obj.flagDetails[i].gedlo!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>End Longtitude</td><td>"+obj.flagDetails[i].gedlo+"</td></tr>");
					}
					if(obj.flagDetails[i].gs!=null && obj.flagDetails[i].gs!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Service Time</td><td>"+obj.flagDetails[i].gs+"</td></tr>");
					}
					if(obj.flagDetails[i].gsd!=null && obj.flagDetails[i].gsd!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Service Date</td><td>"+obj.flagDetails[i].gsd+"</td></tr>");
					}
					if(obj.flagDetails[i].gpt!=null && obj.flagDetails[i].gpt!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Pay Type</td><td>"+obj.flagDetails[i].gpt+"</td></tr>");
					}
					
					if(obj.flagDetails[i].gac!=null && obj.flagDetails[i].gac!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Account</td><td>"+obj.flagDetails[i].gac+"</td></tr>");
					}
					if(obj.flagDetails[i].ga!=null && obj.flagDetails[i].ga!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Amount</td><td>"+obj.flagDetails[i].ga+"</td></tr>");
					}
					if(obj.flagDetails[i].goc!=null && obj.flagDetails[i].goc!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Operator Comments</td><td>"+obj.flagDetails[i].goc+"</td></tr>");
					}
					if(obj.flagDetails[i].god2!=null && obj.flagDetails[i].god2!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Dispature Comments</td><td>"+obj.flagDetails[i].god2+"</td></tr>");
					}
					if(obj.flagDetails[i].getrout!=null && obj.flagDetails[i].getrout!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Route Number</td><td>"+obj.flagDetails[i].getrout+"</td></tr>");
					}
					if(obj.flagDetails[i].getshare!=null && obj.flagDetails[i].getshare!='')
					{
						c++;
						$("#uploadformate").append("<tr><td>"+c+"</td><td>Share Ride</td><td>"+obj.flagDetails[i].getshare+"</td></tr>");
					}
					$("#uploadformate").append("</table></center>");
					document.getElementById("uploadformate").style.display="block";
					break;
				}
		}
		
	}
</script>
</head>
<body>
   
       <div class="leftCol"> 
                    <div class="clrBth"></div>
       </div> 
       <div class="rightCol">
       <div class="rightColIn">
   

<form  name="masterForm" style="margin-right:180px;width:970px;"  action="control" method="post" onsubmit="retrun showProcess()">

   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %><input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <input type="hidden" name="action" value="systemsetup"/>
		<input type="hidden" name="event" value="reviewOpenRequestFieldOrder"/>
		
		<input type="hidden" name="operation" value="2"/>
        
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
   
       <div class="leftCol"> 
                    <div class="clrBth"></div>
       </div> 
       <div class="rightCol">
       <div class="rightColIn">
      <%String result="";
			if(request.getAttribute("page")!=null) {
			result = (String)request.getAttribute("page");
			}
			%>
			<div id="errorpage" style="">
			<%=result.length()>0? result:"" %>
			</div> 
       
			<c class="nav-header"><center>Review Upload Mapping</center></c><br></br>
   
       			<table bgcolor="lightblue" id="colr" style="margin-left: 35%;">
       			<tr align=center>
       			<td>S.No</td><td>Vendor Name</td><td>&nbsp;&nbsp;&nbsp;&nbsp;Edit</td><td>&nbsp;&nbsp;Delete&nbsp;&nbsp;</td></tr>
       			 </table>  	 
                <div id="val" style="margin-left: 35%;"></div>  
                <div id="uploadformate" style="Display:none">
                </div>
                <!-- <center><input type="button" value="View Upload File Formate" onclick="viewfileformate()" id="viewbutton"></input>
                <input type="button" value="Delete" onclick="deletevalue()" id="deletebutton"></input>
                <input type="button" value="Delete" onclick="deletevalue2()" id="deletebutton2" style="display:none"></input>
                <a href="mainNew.jsp?screen=/SystemSetup/reviewUploadFileMapping.jsp&module=systemsetupView"></a><button id="backbutton" style="display:none">Back</button></a>
                </center> -->
                <center>
                <a href="mainNew.jsp?screen=/SystemSetup/reviewUploadFileMapping.jsp&module=systemsetupView"><input type="button" id="backbutton" value="Back" style="display:none"></input></a> 
                </center>
              <div class="clrBth"></div>   
              <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 </div>
 </div>
 </form>
 </body>
</html> 