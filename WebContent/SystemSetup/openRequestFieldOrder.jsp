<%@page import="com.tds.tdsBO.OpenRequestFieldOrderBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.WrapperBO"%>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<script type="text/javascript">

function delrow(){
	try {
		if(document.getElementById("size").value>0){
	 var table = document.getElementById("orFields");
	 var rowCount = table.rows.length;
	 var temp=(Number(document.getElementById("size").value)-1);
	document.getElementById('size').value=temp;
	rowCount--;

	table.deleteRow(rowCount);
		}
	}catch(e){alert(e.message);}
	
	
}
	function cal(z) {
		if(z==1){
		<%if (request.getAttribute("arrayList") != null) {
			ArrayList fieldArray=(ArrayList) request.getAttribute("arrayList");
			int arraySize=fieldArray.size();
	%>		
	<%	for(int i=0;i<arraySize;i++){%>
	var k = Number(document.getElementById("size").value) + 1;
	document.getElementById('size').value = k;
	<%}%>
	<%}%>
	}
	
	}
	 function checkFields(mode,dropdownPosition){
		var size=document.getElementById('size').value;
		if(mode==1){
			var id2=document.getElementById("orFieldPosition"+dropdownPosition).value;
			for(var i=1;i<=size;i++){
				var id1=document.getElementById("orFieldPosition"+i).value;
					if(id1==id2){
						if(i!=dropdownPosition){
						var r=confirm("You have entered same values in field "+i+" and field "+dropdownPosition+" do you want to merge these 2 fields");
						if(r==true){
							
						}else{
						document.getElementById("orFieldPosition"+dropdownPosition).value='Dont Upload this field';
						}
						return;
					}
				}
			}
		}else{
		/* 	for(var j=1;j<=size;j++){
				var id2=document.getElementById("orFieldPosition"+j).value;
				for(var i=1;i<=size;i++){
				var id1=document.getElementById("orFieldPosition"+i).value;
					if(id1==id2 && id1!="Dont Upload this field"){
						if(i!=j){
						alert("don't enter 2 same values ..the field "+i+" and field "+j+" having the same value");
						return false;
						}
					}
				
					
				}
			} */
			return true;
	
		}

	} 
	function showNewVendor(){
		$('#vendorName').show('slow');
		$('#vendor').hide('slow');
		$('#newVendor').hide('slow');
		$('#oldVendor').show('slow');
		$('#vendorTD').show('slow');
	}
	function showOldVendor(){
		$('#vendorName').hide('slow');
		$('#vendor').show('slow');
		$('#newVendor').show('slow');
		$('#oldVendor').hide('slow');
		$('#vendorTD').hide('slow');
		document.getElementById("vendorName").value="";
	}
	
	$(document).ready ( function(){
		
		cal(1);
	});
</script>
<%
	OpenRequestFieldOrderBO orpBO = new OpenRequestFieldOrderBO();
	ArrayList<String> fields= new ArrayList<String>();
	OpenRequestFieldOrderBO orfBO = new OpenRequestFieldOrderBO();
	ArrayList<OpenRequestFieldOrderBO> vendorList = new ArrayList<OpenRequestFieldOrderBO>();
String separatedBy="";
String dateFormat="";
%>
<%
	if (request.getAttribute("orpBO") != null) {
		orpBO = (OpenRequestFieldOrderBO) request.getAttribute("orpBO");
	}
if (request.getAttribute("arrayList") != null) {
	fields=(ArrayList) request.getAttribute("arrayList");
	
}
if (request.getAttribute("vendorList") != null) {
	vendorList=(ArrayList<OpenRequestFieldOrderBO>) request.getAttribute("vendorList");
	
}
if (request.getAttribute("separatedBy") != null) {
	separatedBy=request.getAttribute("separatedBy").toString();
	
}
if (request.getAttribute("dateFormat") != null) {
	dateFormat=request.getAttribute("dateFormat").toString();
	
}
/* if(request.getParameter("orFieldOrderBean")!=null){
	orfBO=(OpenRequestFieldOrderBO) request.getAttribute("orFieldOrderBean");
} */
%>
<body >
	<form name="masterForm" action="control" method="post" onsubmit="return checkFields(2,0)">

		<input type="hidden" name="action" value="systemsetup" /> <input
			type="hidden" name="event" value="openRequestFieldOrder" /> <input
			type="hidden" name="module" id="module"
			value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />
		<input type="hidden" id="size" name="size" value="<%=request.getParameter("arrayList")==null?"":request.getParameter("arrayList").length()-1 %>" />

		<%
			String error = "";
			String col = "color:red";
			if (request.getAttribute("error") != null) {
				error = (String) request.getAttribute("error");
			} else if (request.getParameter("error") != null) {
				error = (String) request.getParameter("error");
			}
		%>
     <c class="nav-header"><center>Field Order in File</center></c>
 <table bgcolor="lightgray" border=2 style="width:60%;margin-left:1%;margin-top:5%; position:absolute;">
     <tr>
      <input type="hidden" name="separatedBy" id="separatedBy" value="<%=separatedBy%>"></input>
    <input type="hidden" name="dateFormat" id="dateFormat" value="<%=dateFormat%>"></input>
     <%for(int i=0;i<fields.size();i++){ %>
     <tr><td height="10px;">
     <%=i+1%>.
     </td>
     
     <td >
      <%=fields.get(i) %>
     </td>
     <td  >
    
     <select name="orFieldPosition<%=i%>" id="orFieldPosition<%=i%>" onchange="checkFields(1,<%=i%>)" >
    <option value="Dont Upload this field" >Don't Upload this field</option>
	<option value="TripId">TripId</option>
	<option value="Name">Name</option>    
	<option value="Phone" >Phone</option>
	<option value="StartAddress1">StartAddress1</option>
	<option value="StartAddress2">StartAddress2</option>
	<option value="StartCity">StartCity</option>  
	<option value="StartLatitude" >StartLatitude</option>
	<option value="StartLongitude">StartLongitude</option>
	<option value="EndAddress1">EndAddress1</option>   
	<option value="EndAddress2" >EndAddress2</option>
	<option value="EndCity">EndCity</option>
	<option value="EndLatitude">EndLatitude</option>   
	<option value="EndLongitude" >EndLongitude</option>
	<option value="ServiceDate">ServiceDate</option>
	<option value="ServiceTime">ServiceTime</option> 
	<option value="payType">payType</option>
	<option value="payAccount">payAccount</option>   
	<option value="amount" >amount</option>
	<option value="sharedRide">sharedRide</option>
	<option value="numberOfPassengers">numberOfPassengers</option>  
	<option value="OperatorComments">Operator Comments</option>  
	<option value="DispatchComments">Driver Comments</option>
	<option value="RouteNumber">Route Number</option>
	<option value="pickUpOrder">PickUp Order</option>
	<option value="dropOffOrder">DropOff Order</option>
	<option value="CallerPhone">CallerPhone</option>
	<option value="ClientRefNum">ClientRefNum</option>
	<option value="ClientRefNum1">ClientRefNum1</option>
	<option value="ClientRefNum2">ClientRefNum2</option>
	<option value="ClientRefNum3">ClientRefNum3</option>
	<option value="CustomField">CustomField</option>
     </select>
     </td>
     </tr>
     <%} %>
     <tr><td>
     <select name="vendor" id="vendor" >
     <%if(vendorList!=null){ 
     for(int i=0;i<vendorList.size();i++){
     %>
          <option value="<%=vendorList.get(i).getVendor() %>" ><%=vendorList.get(i).getVendorName() %></option>
     <%}
     } %>
	         
     </select>
     </td>
     <td id="vendorTD" style="display:none;">
     Vendor Name:
     <input type="text" name="vendorName" id="vendorName" style="display: none;" value=""/>
     </td>
     
     <td>
     <input type="button" id="newVendor" value="New Vendor" onclick="showNewVendor()"></input>
     <input type="button" id="oldVendor" value="Show Old Vendor" style="display:none;" onclick="showOldVendor()"></input>
     </td></tr>
     
     <tr><td><center>
     <div class="btnBlue padT10 padR5 fltLft ">
                                                   <div class="rht">
                                                     <input type="submit" name="submit" class="lft" value="Submit"/>
                                                   </div>
                                              </div>
                    
                        <div class="clrBth"></div></center>
                        </td></tr>
     </table>
		<table id="orFields">
			<tr>
					<td colspan="8">
					<div id="errorpage" style="color: red;">
						<%=error.length() > 0 ? "" + error : ""%>
					</div>

					<div class="leftCol">
						<div class="rightCol">
							<div class="rightColIn">
								<table width="100%" style="width: 750px" border="1"
									cellspacing="1" cellpadding="0" class="driverChargTab">
									<tr>
										<td colspan='7' align='center'><h1>
												<center>Field Order for OR Upload Files</center>
											</h1></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
