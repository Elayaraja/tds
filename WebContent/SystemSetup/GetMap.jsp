<%@page import="com.lowagie.text.Document"%>
<%@page import="com.sun.xml.bind.v2.schemagen.xmlschema.Import"%>
<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO) session
			.getAttribute("user");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
<title>Latitude and Longitude of a Point</title>
<meta name="description"
	content="Find the latitude and longitude of a point using Google Maps.">
<meta name="keywords"
	content="latitude, longitude, google maps, get latitude and longitude">
<script src='<%=TDSProperties.getValue("googleMapV3")%>'></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geocode&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>
<SCRIPT type="text/javascript" src="/js/mapTypeControl.js"></SCRIPT>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/labelMap.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<style type="text/css">
 .ui-autocomplete {
	display: block;
	margin: 0;
	padding: 2px;
	-moz-border-radius: 4px;
	-webkit-border-radius: 4px;
	border-radius: 4px;
	background-color: #EEEEEE;
	border: 1px solid #DDDDDD;
	max-height: 200px;
	overflow-y: scroll;
	text-align: left;
	z-index: 9999;
	width: 458px;
} 
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus {
     border: 1px solid #C77405;
     background-color: #FFFFFF;
     color: #C77405;
     font-weight: bold;
     outline: medium none;
 }
</style>

<script type="text/javascript">
	var map = null;
	var geocoder = null;
	var latsgn = 1;
	var lgsgn = 1;
	var zm = 0;
	var marker = null;
	var posset = 0;
	var clickCount = 0;
	var temp = 1;
	var insidePoint = true;
	var eventPoints = 0;
	var poly;
	var pathArray=[];

	function setOutsidePoint() {
		if (document.getElementById("outsidePoint").value <= "0"
				&& document.getElementById("size").value >= 2) {
			insidePoint = false;
			document.getElementById("Status").value = "Outside Zone";
			document.getElementById("outsidePoint").value = "1";
		} else {
			alert("You Need Min.3 Points For Zone & Can Give Only One OutSide Zone Point");
		}
	}
	function setBorderPoint() {
		insidePoint = true;
		document.getElementById("outsidePoint").value ="";
		document.getElementById("Status").value = "Inside Zone";
	}
	function validation() {
		if (document.getElementById("ZoneType").value != "E"
				&& document.getElementById("ZoneType").value != "D") {
			var t1 = document.getElementById("size");
			if (t1.value <= 2) {
				alert("Please Plot Atleast Three Points ");
				return false;
			}
			var outValue = document.getElementById("outsidePoint").value;
			if (outValue == "") {
				alert("Please Give A Outside Zone Point");
				return false;
			}
		}
	}
	function checkZone() {
		if (document.getElementById("ZoneType").value == "E"
				|| document.getElementById("ZoneType").value == "D") {
			if (eventPoints <= 0) {
				eventPoints = 1;
			} else {
				alert("Sorry In Event/Default Queue You Can Give Only One Zone Point");
				var table = document.getElementById("add123");
				var rowCount = table.rows.length;
				map.clearOverlays();
				for (deleteRow = 0; deleteRow <= rowCount - 2; deleteRow++) {
					table.deleteRow(deleteRow);
				}
			}
		}
		if (document.getElementById("outsideLat").value != '') {
			alert("You Can Give Only One OutSide Zone Point");
			map.clearOverlays();
		}
	}

	function delRow() {
		var table = document.getElementById("add123");
		var rowCount = table.rows.length;
		document.getElementById("outsideLat").value = '';
		document.getElementById("outsideLon").value = '';
		document.getElementById("latitudeTemp").value='';
		document.getElementById("longitudeTemp").value='';
		for (deleteRow = rowCount - 1; deleteRow > 0; deleteRow--) {
			table.deleteRow(deleteRow);
		}
		document.getElementById("size").value = "";
		marker.clearMap(map);
		initialize();
	}

	function initialize() {
		var table = document.getElementById("add123");
		var defaultLati=<%=adminBo.getDefaultLati()%>
		var defaultLongi=<%=adminBo.getDefaultLogi()%>
		var zoomLevel;
		var latlng;
		pathArray=[];
		var externalImage = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
		var image = "http://itouchmap.com/i/blue-dot.png";
		/* document.getElementById("enterAddress").value='';
		if(document.getElementById("latitudeTemp").value!=""){
			latlng= new google.maps.LatLng(document.getElementById("latitudeTemp").value, document.getElementById("longitudeTemp").value);
			zoomLevel=16;
		} else { */
			latlng= new google.maps.LatLng(defaultLati,defaultLongi);
			zoomLevel=12;
		//}
		var options = {
				zoom: 12,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				draggableCursor: 'crosshair'
		};

		map = new google.maps.Map(document.getElementById("map"), options);
		//GEOCODER
		geocoder = new google.maps.Geocoder();
		marker = new google.maps.Marker({
			map : map,
			draggable : true
		});
		var path;
		poly = new google.maps.Polyline({
		      strokeColor: '#000000',
		      strokeOpacity: 1.0,
		      strokeWeight: 3
		    });
	    poly.setMap(map);
		google.maps.event.addListener(map, 'click', function(event) {
			checkZone();
			if (document.getElementById("outsidePoint").value == 0 && document.getElementById("ZoneType").value != "E"
				&& document.getElementById("ZoneType").value != "D") {
				if( document.getElementById("size").value==""){
				markerVenki = new google.maps.Marker({
						position : event.latLng,
						map : map,
						icon : image
					});
				} 
				path = poly.getPath();
				path.push(event.latLng);
				pathArray.push(event.latLng);
				if(document.getElementById("size").value >=2){
					lastPoint();
				}
			} else {
				marker = new google.maps.Marker({
					position : event.latLng,
					map : map,
					icon : externalImage
				});
			}
			var pointLat = event.latLng.lat();
			var pointLon = event.latLng.lng();
			computepos(pointLat, pointLon);
		});
		showLatLongNew();
	}
	var polygonZonesGlobal =[];
	var labelGlobal =[];
	var b2=new Boolean(1);	
	
	function showAllZones(){
		if(b2){
			for(var i=0;i<polygonZonesGlobal.length;i++){
				polygonZonesGlobal[i].setMap(null);
				labelGlobal[i].setMap(null);
			}
			polygonZonesGlobal =[];
			labelGlobal =[];
		} else {
			showLatLongNew();
		}
		b2=!b2;
	}
	 function showLatLongNew() {
		var fillColors =  [];
		fillColors.push("#FFA500");
		fillColors.push("#FF00FF");
		fillColors.push("#FF0000");
		fillColors.push("#00FF00");
		fillColors.push("#808000");
		fillColors.push("#616D7E");
		fillColors.push("#306EFF");
		fillColors.push("#8D38C9");
		fillColors.push("#F52887");
		fillColors.push("#00FF00");
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = '/TDS/DashBoard?event=allZonesForDash';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"latLong\":"+text+"}";
		var latLongList = JSON.parse(obj.toString());
		if(Number(latLongList.latLong.length)>1){
			for(var i=0;i<latLongList.latLong.length;i++){
				var latLng =[];
				for(var j=0;j<latLongList.latLong[i].ZC.length;j++){
					latLng.push(new google.maps.LatLng(latLongList.latLong[i].ZC[j].latitude ,latLongList.latLong[i].ZC[j].longitude));
				}
				if(latLng.length>2){
					var polygon = new google.maps.Polygon({paths: latLng,
						strokeColor: "#000000",
						strokeOpacity: 0.6,
						strokeWeight: 1.5,
						fillColor: fillColors[i%10],
						fillOpacity: 0.45
					});
					polygon.setMap(map);
					polygonZonesGlobal.push(polygon);
				} else {
					var image="http://itouchmap.com/i/blue-dot.png";
					var latlngSingle=new google.maps.LatLng(latLongList.latLong[i].ZC[0].latitude ,latLongList.latLong[i].ZC[0].longitude);
					marker = new google.maps.Marker({position:latlngSingle,
						map: map,icon:image,draggable: false});
					marker.setMap(map);
				}
				var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
				jobLabel.set('position', new google.maps.LatLng(latLongList.latLong[i].CLAT,latLongList.latLong[i].CLON));
				jobLabel.set('text',latLongList.latLong[i].ZD+'('+latLongList.latLong[i].ZK+')');
				jobLabel.setMap(map);
				labelGlobal.push(jobLabel);
			}
		}
	 }
	
	function lastPoint(){
	var clickCount = pathArray.length;
	poly.setMap(null);
	poly = new google.maps.Polyline({
	      strokeColor: '#000000',
	      strokeOpacity: 1.0,
	      strokeWeight: 3
	    });
	poly.setMap(map);
	var path=poly.getPath();
	for(i=0;i<clickCount;i++){
		path.push(pathArray[i]);
	}
	path.push(pathArray[0]);
	}
	
	$(document).ready(function() {
		initialize();
		$(function() {
		$("#enterAddress").autocomplete({
			source: function(request, response) {
				var defaultState=document.getElementById("defaultState").value;
				var defaultCountry=document.getElementById("defaultCountry").value;
				geocoder.geocode( {'address': request.term +','+defaultState+','+defaultCountry }, function(results, status) {
					response($.map(results, function(item) {
						return {
							label: item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							city: item.postal_code,
							addcomp: item.address_components
						}
					}));
				})
			},
			select: function(event, ui) {

				var arrAddress =ui.item.addcomp;
				var streetnum= "";
				var route = "";
				$("#latitudeTemp").val(ui.item.latitude);
				$("#longitudeTemp").val(ui.item.longitude);
				$("#enterAddress").autocomplete("close");
			}
		});
	});
	});
	function computepos(pointLat, pointLon) {
		if (insidePoint) {
			if(clickCount==0){
				document.getElementById("firstLatitude").value = pointLat;
				document.getElementById("firstLongitude").value = pointLon;
			}
			var clickCount = Number(document.getElementById("size").value);
			var table = document.getElementById("add123");
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var cell = row.insertCell(0);
			var cell1 = row.insertCell(1);

			var element = document.createElement("input");
			element.type = "text";
			element.name = "latbox" + clickCount;
			element.id = "latbox" + clickCount;
			element.size = '20';
			element.value = pointLat;
			cell.appendChild(element);

			var element1 = document.createElement("input");
			element1.type = "text";
			element1.name = "lonbox" + clickCount;
			element1.id = "lonbox" + clickCount;
			element1.size = '20';
			element1.value = pointLon;
			cell1.appendChild(element1);

			document.getElementById("size").value = clickCount + 1;
		} else {
			if (document.getElementById("outsideLat").value == '') {
				document.getElementById("outsideLat").value = pointLat;
				document.getElementById("outsideLon").value = pointLon;
			}
		}
	}
	function Undo(){
		var clickCount = Number(document.getElementById("size").value);
		if(clickCount==0 || clickCount==""){
			document.getElementById("size").value="";
			return;
		}
		var table = document.getElementById("add123");
		var image = "http://itouchmap.com/i/blue-dot.png";
		var rowCount = table.rows.length;
		table.deleteRow(clickCount);
		poly.setMap(null);
		poly = new google.maps.Polyline({
		      strokeColor: '#000000',
		      strokeOpacity: 1.0,
		      strokeWeight: 3
		    });
	    poly.setMap(map);
		var path=poly.getPath();
		for(i=0;i<clickCount-1;i++){
			path.push(pathArray[i]);
		}
		path.push(pathArray[0]);
		pathArray.splice(pathArray.length-1,1);
		if(clickCount==1){
			markerVenki.setMap(null);
		}
		document.getElementById("size").value = clickCount-1;
		if(document.getElementById("size").value==0){
			document.getElementById("size").value="";
		}		
	}
	function addressValue(){
		latlng= new google.maps.LatLng(document.getElementById("latitudeTemp").value, document.getElementById("longitudeTemp").value);
		zoomLevel=16;
		map.setCenter(latlng);
		map.setzoom(zoomLevel);
	}
</script>

<link href="/d.css" type="text/css" rel="stylesheet">
</head>
<body>
	<form name="masterForm" action="systemsetup" method="post" />
	<input type="hidden" id="size" name="size" value="">
	<input type="hidden" name="action" value="systemsetup" />
	<input type="hidden" name="event" value="createqueueCoordinate" />
	<input type="hidden" id="outsidePoint" name="outsidePoint" value="">
	<input type="hidden" id="queueid" name="queueid" value="<%=request.getParameter("queueid")%>">
	<input type="hidden" id="qdesc" name="qdesc" value="<%=request.getParameter("qdesc")%>">
	<input type="hidden" id="qdelay" name="qdelay" value="<%=request.getParameter("qdelay")%>">
	<input type="hidden" id="qresponse" name="qresponse" value="<%=request.getParameter("qresponse")%>">
	<input type="hidden" id="qloginswitch" name="qloginswitch" value="<%=request.getParameter("qloginswitch")%>">
	<input type="hidden" id="qdispatchtype" name="qdispatchtype" value="<%=request.getParameter("qdispatchtype")%>">
	<input type="hidden" id="qqueuetype" name="qqueuetype" value="<%=request.getParameter("qqueuetype")%>">
	<input type="hidden" id="qbroad" name="qbroad" value="<%=request.getParameter("qbroad")%>">
	<input type="hidden" id="qrequestbef" name="qrequestbef" value="<%=request.getParameter("qrequestbef")%>">
	<input type="hidden" id="qdispatchdistance" name="qdispatchdistance" value="<%=request.getParameter("qdispatchdistance")%>">
	<input type="hidden" id="phoneCallDelayTime" name="phoneCallDelayTime" value="<%=request.getParameter("phoneCallDelayTime")%>">
	<input type="hidden" id="adjacentZonesCriteria" name="adjacentZonesCriteria" value="<%=request.getParameter("adjacentZonesCriteria")%>">
	<input type="hidden" id="noPhoneCall" name="noPhoneCall" value="<%=request.getParameter("noPhoneCall")%>">
	<input type="hidden" id="staleCallTime" name="staleCallTime" value="<%=request.getParameter("staleCallTime")%>" />
	<input type="hidden" id="startTime" name="startTime" value="">
	<input type="hidden" id="endTime" name="endTime" value="">
	<input type="hidden" id="dayOfWeek" name="dayOfWeek" value="">
	<input type="hidden" id="ZoneType" name="ZoneType" value="<%=request.getParameter("qqueuetype")%>">
	<input type="hidden" name="module" id="module" value="systemsetupView">
	<input type="hidden" name="latitudeTemp" id="latitudeTemp" value=""/>
	<input type="hidden" name="longitudeTemp" id="longitudeTemp" value=""/>
	<input type="hidden" name="latitudeTemp" id="firstLatitude" value=""/>
	<input type="hidden" name="longitudeTemp" id="firstLongitude" value=""/>
	<input type="hidden" name="defaultState" id="defaultState" value="<%=adminBo.getState()%>"/>
	<input type="hidden" name="defaultCountry" id="defaultCountry" value="<%=adminBo.getCountry()%>"/>
	<input type="hidden" id="staleCallBasedOn" name="staleCallBasedOn" value="<%=request.getParameter("staleCallBasedOn")%>" />

	<center>
		<div id="h">
			<div id="h0"></div>
			<div id="o">
				<div id="content" style="margin-top: -20px;">
					<table width="100%">
						<tr valign="top">
							<h1>Create Zone</h1>
							<tr><td><font size="3" color="black" style="font-weight: bold;">Location:&nbsp;</font></td>
						<td><input type="text" id="enterAddress" name="enterAddress" size="20" value="" class="ui-autocomplete" style="background-color:#BDEDFF;font-size: small;color: black;font-weight: bold;" onblur="addressValue()"></td>
						<td><font size="3" color="black" style="font-weight: bold;">Show Zones</font></td><td><img src="images/Dashboard/zoneLogin.gif" onclick="showAllZones()"/></td></tr>
						<div id="map" style="width: 1250px; height: 475px;margin-top: -20px;"></div>
						</tr>
						<tr>
							<td><input type="button"
								style="width: 150px; height: 40px; font-size: 15px; font-weight: bold; border: 2px solid red; background: lightgreen;"
								name="Inside" value="Inside Zone" onclick="setBorderPoint()">
							</td>
							<td><input type="button"
								style="width: 150px; height: 40px; font-size: 15px; font-weight: bold; border: 2px solid red; background: lightgreen;"
								name="Outside" value="Outside Zone" onclick="setOutsidePoint()">
							</td>
							<td><input type="button"
								style="width: 170px; height: 40px; font-size: 15px; font-weight: bold; border: 2px solid red; background: lightgreen;"
								name="Clear" value="Undo" "id="undo"
								onclick="Undo()">
							</td>
							<td><input type="button"
								style="width: 170px; height: 40px; font-size: 15px; font-weight: bold; border: 2px solid red; background: lightgreen;"
								name="Clear" value="Clear All Markers" "id="reset"
								onclick="initialize();delRow()">
							</td>
							<td><input type="submit"
								style="width: 150px; height: 40px; font-size: 15px; font-weight: bold; border: 2px solid red; background: lightgreen;"
								name="makeZone" value="Finalize" onclick="return validation()">
							</td>
						</tr>
						<tr>
							<td><input type="text" size="25%" id="Status"
								style="font-weight: bold; font-size: 20; background-color: lightgreen; color: red;"
								value="Inside Zone" readonly>
							</td>
						</tr>
						<table cellpadding="5" cellspacing="5" width="50%" border="1%">
							<tr>
								<td class="firstcol">Zone Number</td>
								<td><input type="text" style="" name="zoneName"
									value="<%=request.getParameter("queueid")%>" readonly>
								</td>
							</tr>
							<tr>
								<td class="firstcol">Outside Latitude</td>
								<td><input type="text" style="" name="outsideLat"
									id="outsideLat" value="">
								</td>
							</tr>
							<tr>
								<td class="firstcol">Outside Longitude</td>
								<td><input type="text" style="" name="outsideLon"
									id="outsideLon" value="">
								</td>
							</tr>
						</table>
						<table width="50%" cellpadding="5" cellspacing="5" id="add123" 	border="1%">
							<tr>
								<th width="30%" align="left" style="" class="firstCol">Latitude</th>
								<th width="30%" align="justify" style="" class="firstCol">Longitude</th>
							</tr>
						</table>
					</table>
				</div>
			</div>
	</center>
</body>
</html>
