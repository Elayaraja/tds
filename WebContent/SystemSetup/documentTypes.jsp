
<%@page import="com.tds.tdsBO.DocumentBO"%>
<%@page import="com.tds.security.bean.TagSystemBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<script type="text/javascript" src="js/jquery.js"></script>

<head>
	<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
	<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
	<script src="js/CalendarControl.js" language="javascript"></script>
	<script>  

		function delrow(){
			try {
				if(document.getElementById("size").value>0){
			 var table = document.getElementById("documentTypes");
			 var rowCount = table.rows.length;
			 var temp=(Number(document.getElementById("size").value)-1);
			document.getElementById('size').value=temp;
			rowCount--;
				
			table.deleteRow(rowCount);
				}
			}catch(e){alert(e.message);}
			
			
		}
		function changeExpiry(row){
			var severityValue=document.getElementById("serverity"+row).value;
			if(severityValue=="Critical"){
				document.getElementById("checkForExpiry"+row).checked=true;
				document.getElementById("checkForExpiry"+row).disabled=true;
			} else {
				document.getElementById("checkForExpiry"+row).disabled=false;
				document.getElementById("checkForExpiry"+row).checked=false;
			}
		}

		function cal(){
			var i=Number(document.getElementById("size").value)+1;
			document.getElementById('size').value=i;
			var table = document.getElementById("documentTypes");
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var cell = row.insertCell(0);
			var cell1 = row.insertCell(1);
			var cell2 = row.insertCell(2);
			var cell3 = row.insertCell(3);
			var cell4 = row.insertCell(4);

			  
			  var element = document.createElement("input");
			  element.type = "text";
			  element.name="documentType"+i;
			  element.id="documentType"+i;
			  element.align='right'; 
			  element.size = '10';
			  cell.appendChild(element);
			  
			  
			  var element1 = document.createElement("input");
			  element1.type = "text";
			  element1.name="shortName"+i;
			  element1.id="shortName"+i;
			  element1.align='right'; 
			  element1.size = '10';
			  element1.value="";
			  cell1.appendChild(element1);
			  
				var element2 = document.createElement("select");
			 	 element2.name="serverity"+i;
				 element2.id="serverity"+i;
				
				  var theOption = document.createElement("option");
				  theOption.text="Non-Critical";
					theOption.value="Non-Critical";
					element2.options.add(theOption);
					theOption = document.createElement("option");
					 theOption.text="Critical"; 
					theOption.value="Critical";
					element2.options.add(theOption);
					element2.onchange=function(){changeExpiry(i);};
					cell2.appendChild(element2);
					
					var element3 = document.createElement("select");
				 	 element3.name="driverOrVehicle"+i;
					 element3.id="driverOrVehicle"+i;
					
					  var theOption = document.createElement("option");
					  theOption.text="Driver/Operator";
						theOption.value="Driver/Operator";
						element3.options.add(theOption);
						theOption = document.createElement("option");
						 theOption.text="Vehicle"; 
						theOption.value="Vehicle";
						element3.options.add(theOption);
						
						cell3.appendChild(element3);
						
						  var element4 = document.createElement("input");
						  element4.type = "checkbox";
						  element4.name="checkForExpiry"+i;
						  element4.id="checkForExpiry"+i;
						  element4.align='right'; 
						  element4.size = '10';
						  element4.value="1";
						  cell4.appendChild(element4);
			  
			}
		function showEditFields(row){
			$("#docType_"+row).attr('readonly',false);
			$("#docSName_"+row).attr('readonly',false);
			$("#docSType_"+row).attr('disabled',false);
			$("#docDBelongs_"+row).attr('readonly',false);
			$("#docCheckExpiry_"+row).attr('disabled',false);
			$("#edit_"+row).hide();
			$("#done_"+row).show();
		}
		function expiryDate(row){
			if(document.getElementById("docSType_"+row).value=="Non-Critical"){
				document.getElementById("docCheckExpiry_"+row).checked=false;
			}else{
				document.getElementById("docCheckExpiry_"+row).checked=true;
			}
		}
		function EditDocType(serialNumber,row){
			var docType=$("#docType_"+row).val();
			var shortName=$("#docSName_"+row).val();
			var severityType=$("#docSType_"+row).val();
			var docBelongs=$("#docDBelongs_"+row).val();
			var expiry=0;
			if(document.getElementById("docCheckExpiry_"+row).checked==true){
				expiry=1;
			}
			var xmlhttp=null;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
			url='SystemSetupAjax?event=editDocumentType&module=systemsetupView&serialNumber='+serialNumber+'&docType='+docType+'&shortName='+shortName+'&severity='+severityType+'&belongs='+docBelongs+'&checkForExpiry='+expiry;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if(text==1){
				$("#docType_"+row).attr('readonly',true);
				$("#docSName_"+row).attr('readonly',true);
				$("#docSType_"+row).attr('disabled',true);
				$("#docDBelongs_"+row).attr('readonly',true);
				$("#docCheckExpiry_"+row).attr('disabled',true);
				$("#edit_"+row).show();
				$("#done_"+row).hide();
			}else{
				alert("Update unsuccessful");
			}
		}
	 	function deleteDocType(serialNumber,x){
			  var xmlhttp=null;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
			url='SystemSetupAjax?event=deleteDocumentType&module=systemsetupView&delete=YES&serialNumber='+serialNumber;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if(text=="Deleted Successfully"){
				var table=document.getElementById("documentTypes");
				table.deleteRow(x.parentNode.parentNode.rowIndex);
			}
		}  
	</script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tag Computers</title>
</head>
<body>
<%ArrayList<DocumentBO> getDocuments= (ArrayList<DocumentBO>)(request.getAttribute("getDocuments")==null?new ArrayList<TagSystemBean>():request.getAttribute("getDocuments"));%>
	<% String pass=(String)request.getAttribute("password"); %>
	 <%String result="";
			if(request.getAttribute("page")!=null) {
			result = (String)request.getAttribute("page");
			}
			%>
			<div id="errorpage" style="">
			<%=result.length()>0? result:"" %>
			</div> 
	<form name="masterForm" action="control" method="post"  />
	<input type="hidden" name="action" value="systemsetup" />
	<input type="hidden" name="event" value="documentTypes" />
	<input type="hidden" name="button" id="button" value="" />
	<input type="hidden" name="size" id="size" value=""/>
	<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
	<c class="nav-header"><center>Document Types</center></c>
	<div class="leftCol"></div>
	<div class="clrBth"></div>
	<table>
	<tr> <td >
	  	<table width="100%" border="0" cellspacing="4" cellpadding="2" style="left: 80px;" class="driverChargTab" id="documentTypes">
       	<tr >
       	 <th width="20%" class="firstCol">Document Name</th>
       	 <th width="20%" class="firstCol">Short name</th>
          <th width="20%" class="firstCol">Document Type</th>
          <th width="20%" class="firstCol">Driver/Vehicle</th>
         <th width="20%" class="firstCol">Check For Expiry</th>
       	</tr>
       	<% 
	boolean colorLightGreen = true;
	String colorPattern;%>
								<%
								for(int i=0;i<getDocuments.size();i++){
								colorLightGreen = !colorLightGreen;
								if(colorLightGreen){
									colorPattern="style=\"background-color:lightgreen\""; 
									}
								else{
									colorPattern="";
								}
							%>
							<tr>

									<td ><input type="text" size="10" readonly="readonly" id="docType_<%=i%>" value="<%=getDocuments.get(i).getDocumentType() %>"/></td>
									<td ><input type="text" size="10" readonly="readonly" id="docSName_<%=i%>" value="<%=getDocuments.get(i).getShortName() %>"/></td>
									<td ><select size="1" disabled="disabled" id="docSType_<%=i%>" onblur="expiryDate(<%=i%>)">
									<option  value="Critical" <%=getDocuments.get(i).getServerityType().equals("Critical")?"selected":"" %> >Critical</option>
									<option  value="Non-Critical" <%=getDocuments.get(i).getServerityType().equals("Non-Critical")?"selected":"" %>>Non_Critical</option>
									</select>
									</td>
									<td ><input type="text" size="10" readonly="readonly" id="docDBelongs_<%=i%>" value="<%=getDocuments.get(i).getDocumentBelongsTo()==null?"": getDocuments.get(i).getDocumentBelongsTo()%>"/></td>
									<td ><input type="checkbox" disabled="disabled" id="docCheckExpiry_<%=i%>" value="1" <%=getDocuments.get(i).getCheckForExpiry()==0?"": "checked"%> /></td>
									<td id="edit_<%=i%>">
									<input type="button"
									name="imgEdit" id="imgEdit" value="Edit"
									onclick="showEditFields(<%=i%>)"></input>
									</td> 
									<td id="done_<%=i%>" style="display:none;">
									<input type="button"
									name="imgEdit" id="imgEdit" value="Done"
									onclick="EditDocType('<%=getDocuments.get(i).getSerialNumber() %>',<%=i%>)"></input>
									</td> 
									 <td>
									<input type="button"
									name="imgDelete" id="imgDelete" value="Delete"
									onclick="deleteDocType('<%=getDocuments.get(i).getSerialNumber() %>',this)"></input>
									</td> 
								</tr>
								
								<%}%>
       						 </td> 
                           </tr>           
                         </table>
                         <table>
                         <tr>
                                <td><input type="button" name="Button" value="ADD" onclick="cal()" class="lft"></input></td>
                                <td><input type="button" align="middle" name="Button" value="Remove"  onclick="delrow()" class="lft" ></input></td>   
                                <td><input type="submit" name="submit"  class="lft" value="Submit"></input></td>
                         </tr>
                         </table>
</body>
</html>