<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<%@page import="com.tds.tdsBO.QueueCoordinatesBO,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.common.util.TDSProperties"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.tds.dao.RequestDAO"%>
<%@page import="java.util.HashMap"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<head>
<title>TDS(Taxi Dispatching System)</title>
<jsp:useBean id="queueCoOrdinateBO" class="com.tds.tdsBO.QueueCoordinatesBO" scope="request"/>
<% if(request.getAttribute("queueCoordinate") != null)
	queueCoOrdinateBO = (QueueCoordinatesBO)request.getAttribute("queueCoordinate");

%>
<script type="text/javascript" src="js/jquery.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="Ajax/Voucher.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<style>
h2{
	font-family: oblique;
	color: grey;
}

#foo {
	font-weight: bold;
	color: blue;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
    
    //called when key is pressed in textbox
	$("#qdelay").keypress(function (e)  
	{ 
	  //if the letter is not digit then display error and don't type anything
	  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
	  {
		//display error message
		$("#errmsg").html("Dispatch Lead Time should contain Numbers Only").show();  
	    return false;
      }	
	});
	$("#qresponse").keypress(function (e)  
			{ 
			  //if the letter is not digit then display error and don't type anything
			  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
			  {
				//display error message
				$("#errmsg").html("Driver Acceptance Time should contain Numbers Only").show(); 
			    return false;
		      }	
			});
	$("#qbroad").keypress(function (e)  
			{ 
			  //if the letter is not digit then display error and don't type anything
			  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
			  {
				//display error message
				$("#errmsg").html("BroadCast Delay should contain Numbers Only").show();
			    return false;
		      }	
			});
	$("#phoneCallDelayTime").keypress(function (e)  
			{ 
			  //if the letter is not digit then display error and don't type anything
			  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
			  {
				//display error message
				$("#errmsg").html("PhoneCall Delay should contain Numbers Only").show();
			    return false;
		      }	
			});
	$("#qdispatchdistance").keypress(function (e)  
			{ 
			  //if the letter is not digit then display error and don't type anything
			  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
			  {
				//display error message
				$("#errmsg").html("Dispatch Distance should contain Numbers Only").show();
			    return false;
		      }	
			});
	$("#queueid").keypress(function (e)  
			{ 
			  //if the letter is not digit then display error and don't type anything
			  if( (e.which > 31 && e.which < 48) || (e.which > 57 && e.which < 65) || (e.which > 90 && e.which < 97) || (e.which > 122))
			  {
				//display error message
				$("#errmsg").html("QueueId Cannot Contain Spaces/Spl. Characters").show();  
			    return false;
		      }	
			});
	phoneDelayTime();

  }); 
  </script>
  <script type="text/javascript">
  function loadSpecialFlags(){
	var xmlhttp=null;
	  document.getElementById("driverProfileByAjax").style.display="block";
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var driverCounter=0;
//	var vehicleCounter=0;
	var url='SystemSetupAjax?event=getFlagsForZone';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	
	var text = xmlhttp.responseText;
	var jsonObj = "{\"flagDetails\":"+text+"}";
	var obj = JSON.parse(jsonObj.toString());
	document.getElementById("driverProfileByAjax").innerHTML="";
	var $tbl = $('<table style="width:100%;" class="thumbnail" >').attr('id', 'flagValues');
	$tbl.append($('<td style="font-weight: bolder" align="center">').text('Special Flags'));	
	for(var j=0;j<obj.flagDetails.length;j++){
			$tbl.append($('<tr>').append(
				$('<td>').text(obj.flagDetails[j].LD).append($('<input/>').attr('type', 'checkbox').attr('name', 'dchk'+driverCounter).attr('id', 'dchk'+driverCounter).val(obj.flagDetails[j].FL).attr('onclick',"checkGroup('"+driverCounter+"','"+obj.flagDetails[j].SD+"','"+obj.flagDetails[j].LD+"')")),
				 $('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vgroup'+driverCounter).attr('id','vgroup'+driverCounter).val(obj.flagDetails[j].SD)),
				$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vLD'+driverCounter).attr('id','vLD'+driverCounter).val(obj.flagDetails[j].LD))
		 	));
			driverCounter=Number(driverCounter)+1;
	}
	document.getElementById("drProfile").value=driverCounter;
	$("#driverProfileByAjax").append($tbl);
	<%if(request.getAttribute("queueCoordinate") != null){%>
		checkFlagsInUpdate();
	<%}%>
}
function checkFlagsInUpdate(){
	var ZoneFlagList=document.getElementById("drProfile").value;
	var ZnFlag = "<%=queueCoOrdinateBO.getZnprofile()%>";
	for (var i=0; i<ZoneFlagList;i++){
		if(ZnFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
			document.getElementById('dchk'+i).checked=true;
		}
	}
}
function checkGroup(row,groupId,longDesc){
	var drProf=document.getElementById("drProfile").value;
	if(document.getElementById("dchk"+row).checked==true){
		for(var i=0;i<drProf;i++){
			if(document.getElementById("dchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
				document.getElementById("dchk"+row).checked=false;
				 alert("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
				 break;
			}
		}
	}
}
  
</script>
<script type="text/javascript">
function phoneDelayTime(){
	if(document.getElementById("phoneCallDelayTime").value==-1){
		 document.getElementById("noCall").checked = true;
		 document.getElementById("phoneCallDelayTime").value="";
		 document.getElementById("phoneCallDelayTime").readOnly=true;
	}
}
	function selectDistance() {
		if (document.getElementById("shortestPath").value == "M"
				|| document.getElementById("shortestPath").value == "B") {
			document.getElementById("qdispatchdistance").style.display = "none";
			document.getElementById("name").style.display = "none";
			document.getElementById("qdispatchdistance").value = "0";
		} else {
			document.getElementById("qdispatchdistance").style.display = "";
			document.getElementById("name").style.display = "";
		}
	}
	function selectDispatch() {
		if (document.getElementById("qqueuetype").value == "D" && document.getElementById("shortestPath").value == "Q"){
			$("#errmsg").html("For Default Queue You Cannot Select QueueBased Dispatch").show();
			return false;
		}
		}
	
	function check() {
		var id = document.getElementById('queueid').value;
		var desc = document.getElementById('qdesc').value;
		var t1 = document.getElementById('qdelay');
		var t2 = document.getElementById('qresponse');
		var t4 = document.getElementById('qdispatchdistance');
		if (t1.value == "" || t2.value == "" || t4.value == "" ||id==""||desc=="" || (document.getElementById("qqueuetype").value == "D" && document.getElementById("shortestPath").value == "Q")) {
			alert("Please enter some values in all the columns & For Default Zone You Cannot Select QueueBased Dispatch");
			return false;
		}
	}
	function broadcastCheck() {
		if (document.getElementById("dontBroadcast").checked == true) {
			document.getElementById("qbroad").value = "";
			document.getElementById("qbroad").readOnly = true;
		} else {
			document.getElementById("qbroad").readOnly = false;
		}
	}
	function noPhoneCalls() {
		if(document.getElementById("noCall").checked==true){
			 document.getElementById("phoneCallDelayTime").readOnly=true;
			 document.getElementById("phoneCallDelayTime").value="";
			 document.getElementById("noPhoneCall").value=1;
		}else{
			document.getElementById("phoneCallDelayTime").readOnly=false;
			document.getElementById("phoneCallDelayTime").value="";
			document.getElementById("noPhoneCall").value=0; 
		}
		/* if(checked){
			 document.getElementById("phoneCallDelayTime").readOnly=true;
			 document.getElementById("phoneCallDelayTime").value=-1;
		}else{
			 document.getElementById("phoneCallDelayTime").readOnly=false;
			 document.getElementById("phoneCallDelayTime").value="";
		} */
	}
	function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
	
</script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready ( function(){
	selectDistance();
	loadSpecialFlags();
});
</script>
</head>
<body>
<form method="post" action="control" name="queue" onsubmit="showProcess()">
 <input type="hidden" name="action" value="systemsetup">
 <input type="hidden" name="drProfile" id="drProfile" value="" />
 <input type="hidden" name="dchk" id="dchk" value="" />
 <input type="hidden" name="startTime" value="">
 <input type="hidden" name="event" value="createqueueCoordinate">
<input type="hidden" name="systemsetup" value="6">
<input type="hidden" name="startTime" value="">
<input type="hidden" name="endTime" value="">
<input type="hidden" name="dayOfWeek" value="">
<input  type="hidden"  name="module"   id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
<input type="hidden"  name="zoneNo" id="zoneNo" value="<%=queueCoOrdinateBO.getQueueId() %>"/>
<div  id="errmsg" class="alert-error"></div>
			<% if( queueCoOrdinateBO.getQDescription() != null && queueCoOrdinateBO.isUpdateStaus()) { %>
			<c class="nav-header"><center>Update Zone</center></c>
			<% } else { %>
			<c class="nav-header"><center>Create Zone</center></c>
			<% } %>
			  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
			  <tr>
			<%
			String error ="";
			if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
							} else if(request.getParameter("errors")!=null)
							{
							error = (String)request.getParameter("errors");
							}
					%>
						<td colspan="7">
							<div id="errorpage" class="alert-error">
							<%= error.length()>0?""+error :"" %>
					   		</div>
				   		</td>
				 </tr>
				 
				 <tr>
				 	<td >Zone&nbsp;Id</td>
				 	<td>
				 		<input type="text" name="queueid" id="queueid"  size="10" value="<%= queueCoOrdinateBO.getQueueId() != null ? queueCoOrdinateBO.getQueueId() : "" %>"/>
				 	</td>
				 				 
				 	<td >Zone&nbsp;Description</td>
				 	<td>
				 		<input type="text" name="qdesc" id="qdesc" size="15" value="<%= queueCoOrdinateBO.getQDescription() != null ? queueCoOrdinateBO.getQDescription() : "" %>"/>
				 	</td>
				 </tr>	
				 
				 <tr>
				 <td >Dispatch&nbsp;Lead&nbsp;Time</td>
				 <td>
				 <input type="text"  size="10" name="qdelay" id="qdelay" value="<%= queueCoOrdinateBO.getQDelayTime() != null ? queueCoOrdinateBO.getQDelayTime() : "0"  %>" onblur= "validation()"/>
				 </td>
				
				 <td >Driver&nbsp;Acceptance&nbsp;Time<br>(in Seconds)</td>
				 <td>
				 <input type="text"  size="10" name="qresponse" id="qresponse" value="<%= queueCoOrdinateBO.getSmsResponseTime() != null ? queueCoOrdinateBO.getSmsResponseTime() : "10" %>" onblur="validation()"/> 
				 </td>
				 </tr>
				 <tr>
				 <td >Phone Call Delay Time</td>
				  <td>
				  <input type="text" name="phoneCallDelayTime" id="phoneCallDelayTime" size="2" value="<%=queueCoOrdinateBO.getPhoneCallTime()==null?"":queueCoOrdinateBO.getPhoneCallTime()%>"/>
				  <input type="hidden" name="noPhoneCall" id="noPhoneCall"  value="0"/>
				No Phone Call
				<input type="checkbox" name="noCall" id="noCall" value="" onclick="noPhoneCalls()"></input>
				  </td>
				  <td >Broadcast&nbsp;Delay</td>
				<td>
				<input type="text"  size="3" name="qbroad" id="qbroad" <%=queueCoOrdinateBO.getQBroadcastDelay() == null?"readonly" : ""%> value="<%= queueCoOrdinateBO.getQBroadcastDelay() != null ? queueCoOrdinateBO.getQBroadcastDelay() : ""  %>" onblur="return validation()"/>
				<font size="0.6px">Don't Broadcast</font>
				<input type="checkbox" name="dontBroadcast" id="dontBroadcast"  <%=queueCoOrdinateBO.getQBroadcastDelay() == null?"checked" : ""%> onclick="broadcastCheck()"/>
				</td>
				 </tr>
				 <tr><td >Stale Call Time</td>
				  <td>
				  <input type="text" name="staleCallTime" id="staleCallTime" size="2" value="<%=queueCoOrdinateBO.getStaleCallTime()==0?"":queueCoOrdinateBO.getStaleCallTime()%>"/>
                       </td> 
                       <td >Stale Call Based On</td>
                       <td>
				<select name="staleCallBasedOn" id="staleCallBasedOn"  >
                        		<option value="1" <%=queueCoOrdinateBO.getStaleCallbasedOn()==0?"":queueCoOrdinateBO.getStaleCallbasedOn()==1?"selected":""%> >Lead Time</option>
								<option value="2" <%=queueCoOrdinateBO.getStaleCallbasedOn()==0?"":queueCoOrdinateBO.getStaleCallbasedOn()==2?"selected":""%> >Pickup Time</option>
                 	</select>
				</td>  
                     </tr>
				 <tr>
				 <td >Login&nbsp;Type</td>
				<td><select name="qloginswitch">
				<option value="R" <%=queueCoOrdinateBO.getQLoginSwitch()==null?"":queueCoOrdinateBO.getQLoginSwitch().equals("R")?"selected":"" %>>Restricted</option>
 				<option value="A" <%=queueCoOrdinateBO.getQLoginSwitch()==null?"":queueCoOrdinateBO.getQLoginSwitch().equals("A")?"selected":"" %>>Unrestricted</option>
 				<%= queueCoOrdinateBO.getQLoginSwitch() != null ? queueCoOrdinateBO.getQLoginSwitch():""%>
 				</select>
 				</td>
				
				<td >Dispatch&nbsp;Type</td>
				<td><select name="qdispatchtype" id="shortestPath" onchange="selectDistance()">
				<option value="Q"<%=queueCoOrdinateBO.getQDispatchType()==null?"":queueCoOrdinateBO.getQDispatchType().equals("Q")?"selected":""%>>Queue Based</option>
				<option value="SD"<%=queueCoOrdinateBO.getQDispatchType()==null?"":queueCoOrdinateBO.getQDispatchType().equals("SD")?"selected":""%>>Shortest Distance</option>
<%-- 				<option value="S"<%=queueCoOrdinateBO.getQDispatchType()==null?"":queueCoOrdinateBO.getQDispatchType().equals("S")?"selected":""%>>Shortest Distance Everyone</option>
 --%>				<option value="M"<%=queueCoOrdinateBO.getQDispatchType()==null?"":queueCoOrdinateBO.getQDispatchType().equals("M")?"selected":""%>>Do not Dispatch</option>
				<option value="B"<%=queueCoOrdinateBO.getQDispatchType()==null?"":queueCoOrdinateBO.getQDispatchType().equals("B")?"selected":""%>>Broadcast</option>
 				<%=queueCoOrdinateBO.getQDispatchType() !=null ? queueCoOrdinateBO.getQDispatchType():"" %>
			</select>				
 				</td>
				</tr>
				<tr>
				<td >Zone&nbsp;Type</td>
				<td><select name="qqueuetype" id="qqueuetype" onchange="return selectDispatch()">
				<option value="A" <%=queueCoOrdinateBO.getQTypeofQueue()==null?"":queueCoOrdinateBO.getQTypeofQueue().equals("A")?"selected":""%>>Actual</option>
				<option value="E" <%=queueCoOrdinateBO.getQTypeofQueue()==null?"":queueCoOrdinateBO.getQTypeofQueue().equals("E")?"selected":""%>>Event</option>
				<option value="V" <%=queueCoOrdinateBO.getQTypeofQueue()==null?"":queueCoOrdinateBO.getQTypeofQueue().equals("V")?"selected":""%>>Virtual</option>
				<option value="D" <%=queueCoOrdinateBO.getQTypeofQueue()==null?"":queueCoOrdinateBO.getQTypeofQueue().equals("D")?"selected":""%>>Default</option>
				<option value="DO" <%=queueCoOrdinateBO.getQTypeofQueue()==null?"":queueCoOrdinateBO.getQTypeofQueue().equals("DO")?"selected":""%>>Dispaly Only</option>
 				</select>
 				</td>
 				<td >Adjacent Zones Driver Pickup Criteria</td>
                        		<td><select name="adjacentZonesCriteria" id="adjacentZonesCriteria"  >
                        		<option value="1" <%=queueCoOrdinateBO.getAdjacentZoneProperties()==null?"":queueCoOrdinateBO.getAdjacentZoneProperties().equals("1")?"selected":""%> >Closest Zone</option>
								<option value="2" <%=queueCoOrdinateBO.getAdjacentZoneProperties()==null?"":queueCoOrdinateBO.getAdjacentZoneProperties().equals("2")?"selected":""%> >Earliest Driver</option>
                        		<option value="3" <%=queueCoOrdinateBO.getAdjacentZoneProperties()==null?"":queueCoOrdinateBO.getAdjacentZoneProperties().equals("3")?"selected":""%> >Closest Driver</option>
                        	
                        	</select>
                       </td>
                     </tr>
                     <tr>
				<td  id="name" style="display: none;">Dispatch&nbsp;Distance<br>(in miles)</br></td>
				<td>
				<input type="text"  size="10" name="qdispatchdistance" id="qdispatchdistance" value="<%= queueCoOrdinateBO.getQDispatchDistance() != null ? queueCoOrdinateBO.getQDispatchDistance():"0"  %>" onblur="validation()"/>
				</td>
				<td  id="name">Max&nbsp;Acceptance&nbsp;Dispatch<br>(in miles)</br></td>
				<td>
				<input type="text"  size="10" name="maxAcceptDistance" id="maxAcceptDistance" value="<%= queueCoOrdinateBO.getMaxDistance() != null ? queueCoOrdinateBO.getMaxDistance():"10"  %>" />
				</td>
				</tr>
				<tr>
				<td  id="name">Hide&nbsp;For&nbsp;Shortest&nbsp;Dispatch</td>
				<td><input type="checkbox" name="bdSwitch" id="bdSwitch" value="1" <%=queueCoOrdinateBO.getQBdSwitch() == 1?"checked" : ""%> /></td>
				</tr>
				<tr><td><div id="driverProfileByAjax" style="margin-left: 75%;width: 120%;margin-top:17%;display:block;"></div>
			</td></tr>
				<tr>
				<td colspan="7" align="center">
                        	<% if( queueCoOrdinateBO.getQDescription() != null && queueCoOrdinateBO.isUpdateStaus()) { %>
									<input type="submit" name="update" value="Update" class="lft" onclick="return check()"/>
							<% } else { %>
								<input type="submit" name="save" value="Submit" class="lft" onclick="return check()"/>
							<% } %>
				</td>
			</tr>    
			</table>             
					
			</form>   
			       
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</body>
</html>
