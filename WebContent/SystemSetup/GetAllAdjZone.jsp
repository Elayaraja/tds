<%@page import="com.tds.tdsBO.AdjacentZonesBO"%>
<%@page import="com.tds.cmp.bean.ZoneTableBeanSP"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%ArrayList<AdjacentZonesBO> allZones=(ArrayList<AdjacentZonesBO>) request.getAttribute("zones");%> 
<%ArrayList Latitudes= (ArrayList) request.getAttribute("latitude"); %>
<%ArrayList Longitudes= (ArrayList) request.getAttribute("longitude");%> 
<%String allZones1=(String) request.getAttribute("zoneParent");%> 
<%ArrayList Latitudes1= (ArrayList) request.getAttribute("latitude1"); %>
<%ArrayList Longitudes1= (ArrayList) request.getAttribute("longitude1");%> 
<%ArrayList centerLat= (ArrayList) request.getAttribute("centreLatitude");%> 
<%ArrayList centerLon= (ArrayList) request.getAttribute("centreLongitude");%> 
<%ArrayList centerLat1= (ArrayList) request.getAttribute("centreLatitude1");%> 
<%ArrayList centerLon1= (ArrayList) request.getAttribute("centreLongitude1");%> 

 <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
<title>Latitude and Longitude of a Point</title>
<meta name="description"
	content="Find the latitude and longitude of a point using Google Maps.">
<meta name="keywords"
	content="latitude, longitude, google maps, get latitude and longitude">
<script src='<%=TDSProperties.getValue("googleMapV3")%>'></script>
<SCRIPT type="text/javascript" src="/js/mapTypeControl.js"></SCRIPT>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/labelMap.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
	//         
	// Latitude and Longitude math routines are from: http://www.fcc.gov/mb/audio/bickel/DDDMMSS-decimal.html

	var map = null;
	var geocoder = null;
	var latsgn = 1;
	var lgsgn = 1;
	var zm = 0;
	var marker = null;
	var posset = 0;
	var clickCount = 0;
	var temp = 1;
	var insidePoint = true;

	function xz() {
		var temp=0;
		var defaultLati=<%=adminBo.getDefaultLati()%>
		var defaultLongi=<%=adminBo.getDefaultLogi()%>
		var latlng = new google.maps.LatLng(defaultLati,defaultLongi);
		var options = {
				zoom: 14,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				draggableCursor: 'crosshair'
		};

		map = new google.maps.Map(document.getElementById("map"), options);
		//GEOCODER
		geocoder = new google.maps.Geocoder();

		  marker = new google.maps.Marker({
              map: map,
              draggable: true
      });
		showLatLong1();
	}

 	function showLatLong1() {
	<% 
	String point = "";
	String[] strokeColors = {"#616D7E","#306EFF","#8D38C9","#F52887","#00FF00","#FFA500","#FF00FF","#FF0000","#00FF00","#808000"};
	String[] fillColors =   {"#FFA500","#FF00FF","#FF0000","#00FF00","#808000","#616D7E","#306EFF","#8D38C9","#F52887","#00FF00"};
	%>
	<%for(int i=0;i<allZones.size();i++){%>
	<%ArrayList<Double> latArr = (ArrayList<Double>) Latitudes.get(i);
	  ArrayList<Double> lonArr = (ArrayList<Double>) Longitudes.get(i);%>
	  if(<%=latArr.size()>2%>){
	<% point = ""; 
	for (int k =0;k<latArr.size()-1;k++){%>
 	<%point = point + "new google.maps.LatLng(" +latArr.get(k)+ "," +lonArr.get(k)+ "), ";%>
	<%}%>
 	<%point = point + "new google.maps.LatLng(" +latArr.get(0) + "," +lonArr.get(0) + ") ";%>
	var polyCoords=[<%=point%>];
	
var polygon = new google.maps.Polygon({paths: polyCoords,
		    strokeColor: "<%=strokeColors[i%10]%>",
		    strokeOpacity: 0.9,
		    strokeWeight: 3.5,
		    fillColor: "<%=fillColors[i%10]%>",
		    fillOpacity: 0.45
		  });
 	polygon.setMap(map); 
 	 } else {
	  	var points= new google.maps.LatLng(<%=latArr.get(0)%>,<%=lonArr.get(0)%>);
	  	var image="http://itouchmap.com/i/blue-dot.png";
		var marker = new google.maps.Marker({position: points,
            map: map, icon:image});
		marker.setMap(map);
 	}
	  var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
	  jobLabel.set('position', new google.maps.LatLng(<%=centerLat.get(i)%>,<%=centerLon.get(i)%>));
	  jobLabel.set('text','<%=allZones.get(i).getAdjacentZones()%> (Adj Zone <%=i+1%>)'); 
 	<%}%>
	<%ArrayList<Double> latArr1 = (ArrayList<Double>) Latitudes1.get(0);
	  ArrayList<Double> lonArr1= (ArrayList<Double>) Longitudes1.get(0);%>
	  if(<%=latArr1.size()>2%>){
	<%String point1 = ""; 
	for (int k =0;k<latArr1.size()-1;k++){%>
	<%point1 = point1 + "new google.maps.LatLng(" +latArr1.get(k)+ "," +lonArr1.get(k)+ "), ";%>
	<%}%>
	<%point1 = point1 + "new google.maps.LatLng(" +latArr1.get(0) + "," +lonArr1.get(0) + ") ";%>
	var polyCoords=[<%=point1%>];
	
var polygon = new google.maps.Polygon({paths: polyCoords,
		    strokeColor: "#808000",
		    strokeOpacity: 0.9,
		    strokeWeight: 3.5,
		    fillColor: "#00FF00",
		    fillOpacity: 0.45
		  });
	polygon.setMap(map); 
	 } else {
	  	var points= new google.maps.LatLng(<%=latArr1.get(0)%>,<%=lonArr1.get(0)%>);
	  	var image="http://itouchmap.com/i/blue-dot.png";
		var marker = new google.maps.Marker({position: points,
      map: map, icon:image});
		marker.setMap(map);
	}
	  var jobLabel = new Label({map : map,strokeColor: "#ffffff",strokeOpacity: 0.6,strokeWeight: 1.5,fillColor: "#000000",color:"#ffffff"});
	  jobLabel.set('position', new google.maps.LatLng(<%=centerLat1.get(0)%>,<%=centerLon1.get(0)%>));
	  jobLabel.set('text','<%=allZones1%> (Parent Zone)'); 
 }
 
 	
$(document).ready( function() {
	xz();
});
</script>
<link href="/d.css" type="text/css" rel="stylesheet">
</head>
<body>  
<form name="masterForm" action="systemsetup" method="post" />
<input type="hidden" id="size" name="size" value="">
<input type="hidden" name="action" value="systemsetup" />
<input type="hidden" name="event" value="reviewAdjacentZones" />
<input type="hidden" id="outsidePoint" name="outsidePoint" value="">
<input  type="hidden"   name="module"    id="module"  value="systemsetupView">
<center>
<div id="h">
<div id="h0"></div>
<div id="o">
<div id="content">
<table cellpadding="4" cellspacing="0" width="100%">
	<tr valign="top">
		<h1>Review All Adjacent Zones</h1>
		<div id="wrapper" style="margin: 5px">
		<div id="map" style="width: 1150px; height: 450px"></div>
		</div>
		<table cellpadding="2" cellspacing="0" border="1">
				<tr>
			 	<td><input type="submit" value="Close" style="width: 170px; height: 40px; font-size: 15px; font-weight: bold; border: 2px solid red; background: lightgreen;" name="reset" "></td>
				</tr>
			
</table>
</div>
</div>
</center>
</tr>
</table>
</div>
</div>
</div>
</center>
</body>
</html>

