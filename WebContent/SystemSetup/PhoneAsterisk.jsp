<%@page import="com.tds.cmp.bean.AsteriskBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.tdsBO.QueueCoordinatesBO,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.SMSBean" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@page import="com.common.util.TDSProperties"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="com.tds.dao.RequestDAO"%>
<%@page import="java.util.HashMap"%>

<head>
<jsp:useBean id="queueCoOrdinateBO" class="com.tds.tdsBO.QueueCoordinatesBO" scope="request"/>
<% if(request.getAttribute("queueCoordinate") != null)
	queueCoOrdinateBO = (QueueCoordinatesBO)request.getAttribute("queueCoordinate");

%>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
</script>
 <div  id="errmsg" style="position: absolute;left:950px;top:320px;font-size: 20px;color: red;font-weight: bold;"  ></div>
</head>
<body> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<form method="post" action="control" name="IVR">
 <input type="hidden" name="action" value="phoneIVR"/>
<input type="hidden" name="event" value="insertAsterisk"/>
<%AsteriskBean asBean=(AsteriskBean)request.getAttribute("asteriskValues");%>
<%SMSBean smsBean=(SMSBean)request.getAttribute("smsValues");%>
           	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">

                	<c class="nav-header"><center>Create Phone Asterisk</center></c>
                    <table width="50%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
						<%
							String error ="";
							if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
							} else if(request.getParameter("errors")!=null)
							{
							error = (String)request.getParameter("errors");
							}
					%>
						<td colspan="7">
							<div id="errorpage" style="color: red">
							<%= error.length()>0?""+error :"" %>
					   		</div>
				   		</td>
				 </tr>
				 <tr>
				 <td class="firstCol"  style="width: 25%;">Name</td>
				 <td style="width: 25%;"><input type="text" name="name" id="name" value="<%=asBean.getName()==null?"":asBean.getName()%>"/></td>
				 <td class="firstCol"  style="width: 25%;">IP Address</td>
				 <td style="width: 25%;"><input type="text" name="IPAddress" id="IPAddress" value="<%=asBean.getIpAddress()==null?"":asBean.getIpAddress()%>"/></td>
				 </tr>
				 <tr>
				 <td class="firstCol"  style="width: 25%;">User Name</td>
				 <td style="width: 25%;"><input type="text" name="userName" id="userName" value="<%=asBean.getUserName()==null?"":asBean.getUserName()%>"/></td>
				 <td class="firstCol"  style="width: 25%;">Password</td>
				 <td style="width: 25%;"><input type="text" name="password" id="password" value="<%=asBean.getPassword()==null?"":asBean.getPassword()%>"/></td>
				 </tr>
				 <tr>
				 <td class="firstCol"  style="width: 25%;">Location</td>
				 <td style="width: 25%;"><input type="text" name="location" id="location" value="<%=asBean.getLocation()==null?"":asBean.getLocation()%>"/></td>
				 <td class="firstCol"  style="width: 25%;">Phone</td>
				 <td style="width: 25%;"><input type="text" name="phone" id="phone" value="<%=asBean.getPhoneNumber()==null?"":asBean.getPhoneNumber()%>"/></td>
				 </tr>
				 <tr><td style="width: 100%;margin-left: 45%;"><input type="submit" value="Submit" name="submit" align="middle"/></td>
				 </tr>
				 </table><table width="50%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
				 <tr>
				 <td   style="width: 25%;">UserId</td>
				 <td style="width: 25%;"><input type="text" name="userId" id="userId" value="<%=smsBean.getUserName()==null?"":smsBean.getUserName()%>"/></td>
				 <td   style="width: 25%;">Password</td>
				 <td style="width: 25%;"><input type="text" name="passwordSMS" id="password" value="<%=smsBean.getPassword()==null?"":smsBean.getPassword()%>"/></td>
				 </tr>
				 <tr>
				 <td   style="width: 25%;">Credentials</td>
				<td><select name="credentials" id="credentials">
				<option value="1" selected="selected">Nexmo</option>
				<option value="2" <%=smsBean.getType()==2?"selected=selected":"" %>>Twilio</option>
				</select>
				</td>
				 <td   style="width: 25%;">Phone</td>
				 <td style="width: 25%;"><input type="text" name="phoneSMS" id="phoneSMS" value="<%=smsBean.getPhone()==null?"":smsBean.getPhone()%>"/></td>
				</tr>
				<tr>
				<td style="width: 100%;margin-left: 45%;"><input type="submit" value="Submit" name="submitSMS" align="middle"/></td>
				 </tr>
				 </table>
				 </div>
</form>
</body>
</html>
