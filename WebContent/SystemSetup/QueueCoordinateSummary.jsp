<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="java.util.ArrayList,com.tds.tdsBO.QueueCoordinatesBO,java.util.List,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%! QueueCoordinatesBO m_qCoOrdinateBO = null;List m_queueList = null; %>
<%
	if(request.getAttribute("queue") != null) {
		m_queueList = (ArrayList) request.getAttribute("queue");
	}
	else 
		m_queueList = new ArrayList();
%>
<head>
<title>TDS(Taxi Dispatching System)</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}

function getZoneDetails(){
    $("#download").live("click", function () {
        var zoneDetails = document.getElementById("zoneDetails");
        var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
        printWindow.document.write('<html><head><title>zone Details</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(zoneDetails.outerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    });

}

</script>
</head>
<body>
<form name="queueSummary" action="control" method="post" onsubmit="return showProcess()">
<input type="hidden" name="action" value="systemsetup"/>
<input type="hidden" name="event" value="queuesummary">
<input type="hidden" name="systemsetup" value="6"/> 
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
			<c class="nav-header"><center>Zone Summary</center></c>
						<%
							String error ="";
						if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
					%>
							<div id="errorpage" class="alert-error">
							<%= error.length()>0?""+error :"" %>
					   		</div>
				   		<%} %>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
                    <tr>
                    <td >Zone&nbsp;Id</td>
                    <td>
                    	<input type="text"  name="queueid"  value=""> 
                    </td>
                    <td >Zone&nbsp;Description</td>
                    <td>
                    	<input type="text"  name="qdesc"  value=""> 
                    </td>
                    </tr>
                    <tr>
				<td colspan="4" align="center">
                 <input type="submit" name="queuedetails" value="Get Details" class="lft">
               	 <input type="submit" name="getAllZones" value="View All Zones" class="lft">
			  </td>
			</tr>         
            </table>
			<% if(m_queueList != null && m_queueList.size() >0  ) {  %>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" id="zoneDetails" class="thumbnail">                    
            <tr><td><input  type="button" name="download" id ="download" value="Save/Print"  onclick="getZoneDetails()" disable /></td>
			 </tr>
			<tr>
				<th style="width: 15%;" >Zone Id</th>
				<th style="width: 15%;">Description</th>
				<th style="width: 15%;">Login&nbsp;Type</th>
				<th style="width: 15%;">Dispatch&nbsp;Type</th>
				<th style="width: 15%;">Zone&nbsp;Type</th>
				<th style="width: 5%;">Broadcast&nbsp;Delay</th>
				<th style="width: 5%;">Dispatch&nbsp;Distance</th>
				<th style="width: 5%;">Dispatch&nbsp;Lead&nbsp;Time</th>
				<th style="width: 5%;">Driver&nbsp;Acceptance&nbsp;Time(Secs)</th>
				
			</tr> 
			
			<% 
	boolean colorLightGreen = true;
	String colorPattern;%>
	<%for(int count = 0; count < m_queueList.size(); count++) { 

		colorLightGreen = !colorLightGreen;
		if(colorLightGreen){
			colorPattern="style=\"background-color:lightgreen\""; 
			}
		else{
			colorPattern="";
		}
		m_qCoOrdinateBO = (QueueCoordinatesBO) m_queueList.get(count);
	%>
			
			<tr>
				<td <%=colorPattern%>>
					<a href = "control?action=systemsetup&event=createqueueCoordinate&module=systemsetupView&queuestatus=edit&queueid=<%=m_qCoOrdinateBO.getQueueId()==null?"":m_qCoOrdinateBO.getQueueId()%>">
					<%= m_qCoOrdinateBO.getQueueId()==null?"": m_qCoOrdinateBO.getQueueId() %>
					</a>
				</td>
				<td <%=colorPattern%>><%= m_qCoOrdinateBO.getQDescription() %></td>
				<%if(m_qCoOrdinateBO.getQLoginSwitch().equalsIgnoreCase("R")) {%>
				<td <%=colorPattern%>>Restricted</td>
				<%} else { %>
				<td <%=colorPattern%>>UnRestricted</td>
				<%} %>
 				<%if(m_qCoOrdinateBO.getQDispatchType().equalsIgnoreCase("Q")){ %>
				<td <%=colorPattern%>>Queue Based</td>
				<%} else if(m_qCoOrdinateBO.getQDispatchType().equalsIgnoreCase("SD")){ %>
				<td <%=colorPattern%>>Shortest Distance One at a time</td>
				<%}else if(m_qCoOrdinateBO.getQDispatchType().equalsIgnoreCase("S")){ %>
				<td <%=colorPattern%>>Shortest Distance Everyone</td>
				<%}else if(m_qCoOrdinateBO.getQDispatchType().equalsIgnoreCase("M")){ %>
				<td <%=colorPattern%>>Do not Dispatch</td>
				<%}else { %>
				<td <%=colorPattern%>>Broadcast</td>
				<%} %>
  				<%if(m_qCoOrdinateBO.getQTypeofQueue().equalsIgnoreCase("A")){ %>
				<td <%=colorPattern%>>Actual</td>
				<%} else if(m_qCoOrdinateBO.getQTypeofQueue().equalsIgnoreCase("V")){ %>
				<td <%=colorPattern%>>Virtual</td>
				<%}else if(m_qCoOrdinateBO.getQTypeofQueue().equalsIgnoreCase("E")){ %>
				<td <%=colorPattern%>>Event</td>
				<%}else if(m_qCoOrdinateBO.getQTypeofQueue().equalsIgnoreCase("DO")){ %>
				<td <%=colorPattern%>>Display Only</td>
				<%}else{ %>
				<td <%=colorPattern%>>Default</td>
				<%} %>
				<td <%=colorPattern%>><%= m_qCoOrdinateBO.getQBroadcastDelay()==null?"":m_qCoOrdinateBO.getQBroadcastDelay() %></td>
				<td <%=colorPattern%>><%= m_qCoOrdinateBO.getQDispatchDistance()==null?"":m_qCoOrdinateBO.getQDispatchDistance() %></td>
				<td <%=colorPattern%>><%=m_qCoOrdinateBO.getQDelayTime()==null?"":m_qCoOrdinateBO.getQDelayTime() %></td>
				<td <%=colorPattern%>><%=m_qCoOrdinateBO.getSmsResponseTime()==null?"":m_qCoOrdinateBO.getSmsResponseTime() %></td>
			 	<td <%=colorPattern%>>		
 			 	<a style="text-decoration: none;" href = "control?action=systemsetup&event=queuesummary&module=systemsetupView&delete=YES&zone=<%=m_qCoOrdinateBO.getQueueId()%>" >Delete</a>
 		 	 </td>
			  </tr>
			  <%} %>
			  </table>
			
		<%} %>
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
		
