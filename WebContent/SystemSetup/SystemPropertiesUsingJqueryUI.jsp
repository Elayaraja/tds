<!DOCTYPE html >
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.util.Timer"%>
<%@page import="com.tds.cmp.bean.CompanySystemProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import=" java.util.Timer"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.CompanySystemProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<script type="text/javascript">
	function showORFormat() {
		$('.ORFormatShow').show('slow');
	}
</script>
<title>System Properties</title>

<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO) session
			.getAttribute("user");
	CompanySystemProperties cspBO = (CompanySystemProperties) request
			.getAttribute("cspBO");
%>
<link rel="stylesheet" type="text/css"
	href="jqueryUI/layout-default-latest.css" />
<link rel="stylesheet" type="text/css"
	href="jqueryUI/jquery.ui.base.css" />
<link rel="stylesheet" type="text/css"
	href="jqueryUI/jquery.ui.theme.css" />
<!-- CUSTOMIZE/OVERRIDE THE DEFAULT CSS -->
<style type="text/css">

/* remove padding and scrolling from elements that contain an Accordion OR a content-div */
.ui-layout-center, /* has content-div */ .ui-layout-west,
	/* has Accordion */ .ui-layout-east, /* has content-div ... */
	.ui-layout-east .ui-layout-content { /* content-div has Accordion */
	padding: 0;
	overflow: hidden;
}

.ui-layout-center P.ui-layout-content {
	line-height: 1.4em;
	margin: 0; /* remove top/bottom margins from <P> used as content-div */
}

h3,h4 { /* Headers & Footer in Center & East panes */
	font-size: 1.1em;
	background: #EEF;
	border: 1px solid #BBB;
	border-width: 0 0 1px;
	padding: 7px 10px;
	margin: 0;
}

.ui-layout-east h4 { /* Footer in East-pane */
	font-size: 0.9em;
	font-weight: normal;
	border-width: 1px 0 0;
}
</style>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="Ajax/SystemUtilAjax.js"></script>
<!-- REQUIRED scripts for layout widget -->
<script type="text/javascript" src="jqueryUI/jquery-latest.js"></script>

<script type="text/javascript" src="jqueryUI/jquery-ui-latest.js"></script>
<script type="text/javascript" src="jqueryUI/jquery.layout-latest.js"></script>
<script type="text/javascript"
	src="jqueryUI/jquery.layout.resizePaneAccordions-latest.js"></script>
<!-- compressed: /lib/js/jquery.layout.resizePaneAccordions-latest.min.js -->

<script type="text/javascript" src="jqueryUI/themeswitchertool.js"></script>
<script type="text/javascript" src="jqueryUI/debug.js"></script>

<!--  -->

<script type="text/javascript">
	$(document).ready(function() {
		checkMeterTypes();
		myLayout = $('body').layout({
			west__size : 600,
			east__size : 0
			// RESIZE Accordion widget when panes resize
			,
			west__onresize : $.layout.callbacks.resizePaneAccordions,
			east__onresize : $.layout.callbacks.resizePaneAccordions
		});

		// ACCORDION - in the West pane
		$("#accordion1").accordion({
			heightStyle : "fill"
		});

		// ACCORDION - in the East pane - in a 'content-div'
		$("#accordion2").accordion({
			heightStyle : "fill",
			active : 1
		});

		// THEME SWITCHER
		addThemeSwitcher('.ui-layout-north', {
			top : '12px',
			right : '5px'
		});
		// if a new theme is applied, it could change the height of some content,
		// so call resizeAll to 'correct' any header/footer heights affected
		// NOTE: this is only necessary because we are changing CSS *AFTER LOADING* using themeSwitcher
		setTimeout(myLayout.resizeAll, 1000); /* allow time for browser to re-render with new theme */

	});

	$(function() {
		$("button,input[type=button]").button().click(function(event) {
			event.preventDefault();
		});
		$("input[type=submit]").button().click(function(event) {
			event.preventDefault();
			//$("#loading").jqm();
			//$("#loading").show();
			$("#loading").dialog({
				modal : true
			});
			$('#masterForm').submit();
		});
		var $loading = $("#loading");
		var windowH = $(window).height();
		var windowW = $(window).width();

		$loading.css({
			position : "fixed",
			left : ((windowW - $loading.outerWidth()) / 2 + $(document)
					.scrollLeft()),
			top : ((windowH - $loading.outerHeight()) / 2 + $(document)
					.scrollTop())
		});
	});

	function checkPhoneValidation() {
		if (document.getElementById("dontCheckPh").checked) {
			document.getElementById("noOfDigitsPh").value = "";
			document.getElementById("noOfDigitsPh").readOnly = true;
		} else {
			document.getElementById("noOfDigitsPh").readOnly = false;
		}
	}
	function goToDashboard() {
		window.open('DashBoard?event=dispatchValues', '_self');
	}
	function goToHome() {
		window.open('control', '_self');
	}

	function delLastMeter() {
		try {
			var table = document.getElementById("extraMeters");
			var rowCount = table.rows.length;
			var size = document.getElementById('fieldSize').value;
			if (Number(size) > 0) {
				var temp = (Number(size) - 1);
				document.getElementById('fieldSize').value = temp;
				rowCount--;
				table.deleteRow(rowCount);
			}
		} catch (e) {
			alert(e.message);
		}
	}
	function giveNewMeter() {
		var i = document.getElementById("fieldSize").value;

		var table = document.getElementById("extraMeters");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell0 = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);
		var cell3 = row.insertCell(3);
		var cell4 = row.insertCell(4);
		var cell5 = row.insertCell(5);
		var cell6 = row.insertCell(6);
		var cell7 = row.insertCell(7);
		var cell8 = row.insertCell(8);
		var cell9 = row.insertCell(9);
		var cell10= row.insertCell(10);
		var cell11 = row.insertCell(11);

		var element0 = document.createElement("input");
		element0.type = "radio";
		element0.name = "default";
		element0.id = "default"+i;
		element0.value = i;
		element0.onclick=function(){makeDefault(i);};

		cell0.appendChild(element0);

		var element1 = document.createElement("input");
		element1.type = "text";
		element1.name = "meterName" + i;
		element1.id = "meterName" + i;
		element1.size = "10";
		cell1.appendChild(element1);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "ratePerMile" + i;
		element2.id = "ratePerMile" + i;
		element2.size = "3";
		cell2.appendChild(element2);

		var element3 = document.createElement("input");
		element3.type = "text";
		element3.name = "ratePerMin" + i;
		element3.id = "ratePerMin" + i;
		element3.size = "3";
		cell3.appendChild(element3);

		var element4 = document.createElement("input");
		element4.type = "text";
		element4.name = "startAmt" + i;
		element4.id = "startAmt" + i;
		element4.size = "3";
		cell4.appendChild(element4);

		var element5 = document.createElement("input");
		element5.type = "text";
		element5.name = "minSpeed" + i;
		element5.id = "minSpeed" + i;
		element5.size = "3";
		cell5.appendChild(element5);

		var element6 = document.createElement("input");
		element6.type = "text";
		element6.name = "dist1" + i;
		element6.id = "dist1" + i;
		element6.size = "3";
		cell6.appendChild(element6);

		var element7 = document.createElement("input");
		element7.type = "text";
		element7.name = "rate1" + i;
		element7.id = "rate1" + i;
		element7.size = "3";
		cell7.appendChild(element7);

		var element8 = document.createElement("input");
		element8.type = "text";
		element8.name = "dist2" + i;
		element8.id = "dist2" + i;
		element8.size = "3";
		cell8.appendChild(element8);

		var element9 = document.createElement("input");
		element9.type = "text";
		element9.name = "rate2" + i;
		element9.id = "rate2" + i;
		element9.size = "3";
		cell9.appendChild(element9);

		var element10 = document.createElement("input");
		element10.type = "text";
		element10.name = "dist3" + i;
		element10.id = "dist3" + i;
		element10.size = "3";
		cell10.appendChild(element10);

		var element11 = document.createElement("input");
		element11.type = "text";
		element11.name = "rate3" + i;
		element11.id = "rate3" + i;
		element11.size = "3";
		cell11.appendChild(element11);

		document.getElementById('fieldSize').value = Number(i) + 1;
	}
	function makeDefault(defaultValue){
		document.getElementById("defaultMeter").value=Number(defaultValue)+1;
	}
	function checkMeterTypes() {
		if (Number(document.getElementById("fieldSize").value) > 0) {
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'SystemSetupAjax?event=getMeterTypes';
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if (text != "") {
				var jsonObj = "{\"meterDetails\":" + text + "}";
				var obj = JSON.parse(jsonObj.toString());
				document.getElementById('fieldSize').value = 0;
				for (var j = 0; j < obj.meterDetails.length; j++) {
					giveNewMeter();
					document.getElementById('meterName' + j).value = obj.meterDetails[j].MN;
					document.getElementById('ratePerMile' + j).value = obj.meterDetails[j].RMl;
					document.getElementById('ratePerMin' + j).value = obj.meterDetails[j].RMn;
					document.getElementById('startAmt' + j).value = obj.meterDetails[j].SA;
					document.getElementById('minSpeed' + j).value = obj.meterDetails[j].MS;
					document.getElementById('dist1' + j).value = obj.meterDetails[j].D1;
					document.getElementById('dist2' + j).value = obj.meterDetails[j].D2;
					document.getElementById('dist3' + j).value = obj.meterDetails[j].D3;
					document.getElementById('rate1' + j).value = obj.meterDetails[j].R1;
					document.getElementById('rate2' + j).value = obj.meterDetails[j].R2;
					document.getElementById('rate3' + j).value = obj.meterDetails[j].R3;
					if(obj.meterDetails[j].DF==1){
						document.getElementById('default'+j).checked=true;
						document.getElementById('defaultMeter').value=Number(j)+1;
					}
				}
			}
		}
	}
</script>
</head>
<body>
	<form id="masterForm"
		action="control?action=systemsetup&event=systemProperties&Button=yes"
		method="post">
		<div>
			<input type="hidden" name="action" value="systemsetup" /> <input
				type="hidden" name="event" value="systemProperties" /> <input
				type="hidden" name="key" value="  <%=request.getAttribute("key")%>" />
		</div>
		<input type="hidden" name="defaultMeter" id="defaultMeter" value="">
		<input type="hidden" name="fieldSize" id="fieldSize"
			value="<%=cspBO.getMeterTypes()%>" />
		<div class="ui-layout-north ui-widget-content" style="display: none;">
			<div style="float: right; margin-right: 160px;">

				<button onClick="removeUITheme(); myLayout.resizeAll()">Remove
					Theme</button>
			</div>
			<input name="Button" type="submit" value="Submit"></input>
			<%
				AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
				String stopAssoccode = adminBO.getAssociateCode();
				Timer stopTimer =  (Timer) getServletConfig().getServletContext().getAttribute(stopAssoccode+"Timer");
				if (stopTimer == null) {
					System.out.println("stop timer is null");
			%>
			<input name="btnStartOrStopTimer" id="btnStartOrStopTimer"
				type="button" onclick="startOrStopTimer()" value="Start Dispatch" />
			<%
				} else {
					System.out.println("stop timer is Not null");
			%>
			<input name="btnStartOrStopTimer" id="btnStartOrStopTimer"
				type="button" onclick="startOrStopTimer()" value="Stop Dispatch" />
			<%
				}
			%>
			<input name="reloadZones" type="button" onclick="reloadZone()"
				value="Reload Zones" /> <input name="reloadZones" type="button"
				onclick="reloadCabs()" value="Reload Driver/cab Properties" />
			<button onclick="goToHome()">Home</button>
			<!-- <button onclick="goToDashboard()">Dashboard</button> -->
		<div id="StartOrStopTimer" style="text-decoration: blink; font-weight: bolder; height: 10px"></div>
		</div>
		<center>
		<div class="ui-layout-south ui-widget-content ui-state-error" style="display:none; height: 10px">
			<div id="reloadZones" style="text-decoration: blink; text-shadow: gray; color: black;"></div>
			<div id="reloadCabs" style="text-decoration: blink; text-shadow: gray; color: black;"></div>
		</div>
		</center>

		<div class="ui-layout-center" style="display: none;">
			<h3 class="ui-widget-header">
				<center>Company System Properties</center>
			</h3>
			<div class="ui-layout-content ui-widget-content">
				<div id="loading" class="jqmWindow" style="display: none">
					<br /> <br /> <br /> <img src="images/loading.gif"></img>
				</div>
				<table align="center" style="width: 100% height:100%">
					<tr>
						<td><label>Preline1</label></td>
						<td><input type="text" name="Preline1" id="Preline1"
							value="<%=cspBO.getPreline1()%>" /></td>
						<td><label>Post Line1</label></td>
						<td><input type="text" name="Postline1" id="Postline1"
							value="<%=cspBO.getPostline1()%>" /></td>
					</tr>
					<tr>
						<td><label>Preline2</label></td>
						<td><input type="text" name="Preline2" id="Preline2"
							value="<%=cspBO.getPreline2()%>" /></td>
						<td><label>Post Line2</label></td>
						<td><input type="text" name="Postline2" id="Postline2"
							value="<%=cspBO.getPostline2()%>" /></td>
					</tr>
					<tr>
						<td><label>Preline3</label></td>
						<td><input type="text" name="Preline3" id="Preline3"
							value="<%=cspBO.getPreline3()%>" /></td>
						<td><label>Post Line3</label></td>
						<td><input type="text" name="Postline3" id="Postline3"
							value="<%=cspBO.getPostline3()%>" /></td>
					</tr>
					<tr>
						<td><label>Preline4</label></td>
						<td><input type="text" name="Preline4" id="Preline4"
							value="<%=cspBO.getPreline4()%>" /></td>
						<td><label>Post Line4</label></td>
						<td><input type="text" name="Postline4" id="Postline4"
							value="<%=cspBO.getPostline4()%>" /></td>
					</tr>
					<tr>
						<td><label>Preline5</label></td>
						<td><input type="text" name="Preline5" id="Preline5"
							value="<%=cspBO.getPreline5()%>" /></td>
						<td><label>Post Line5</label></td>
						<td><input type="text" name="Postline5" id="Postline5"
							value="<%=cspBO.getPostline5()%>" /></td>
					</tr>

					<tr>
						<td></td>
						<td></td>
						<td><label>Post Line6</label></td>
						<td><input type="text" name="Postline6" id="Postline6"
							value="<%=cspBO.getPostline6()%>" /></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td><label>Post Line7</label></td>
						<td><input type="text" name="Postline7" id="Postline7"
							value="<%=cspBO.getPostline7()%>" /></td>
					</tr>
					<tr>
						<td><label>Time Out For Operator</label></td>
						<td><input type="text" name="TimeOutOperator" id="TimeOutO"
							value="<%=cspBO.getTimeOutOperator()%>" /></td>
						<td><label>Timeout For Driver</label></td>
						<td><input type="text" name="TimeOutDriver" id="TimeOutD"
							value="<%=cspBO.getTimeOutDriver()%>" /></td>
					</tr>

					<tr>
						<td><label>Check CabNo</label></td>
						<td><select name="CheckCabNo">
								<option
									<%=cspBO.getCheckCabNoOnLogin().equalsIgnoreCase("Yes") ? "selected"
					: ""%>
									value="Yes">Yes</option>
								<option
									<%=cspBO.getCheckCabNoOnLogin().equalsIgnoreCase("No") ? "selected"
					: ""%>
									value="No">No</option>
						</select></td>
						<td><label>Enter CVV Code</label></td>
						<td><select name="EnterCVVcode">
								<option value="Yes"
									<%=cspBO.getCvvCodeManadatory().equals("Yes") ? "selected"
					: ""%>>Yes</option>
								<option value="No"
									<%=cspBO.getCvvCodeManadatory().equals("No") ? "selected"
					: ""%>>No</option>
						</select></td>
					</tr>

					<tr>
						<td><label>Time Zone</label></td>
						<td><select name="timeZoneArea">
								<option value="US/Eastern" selected="selected">Eastern</option>
								<option value="US/Central"
									<%=cspBO.getTimeZoneArea().equals("US/Central") ? "selected"
					: ""%>>Central</option>
								<option value="US/Mountain"
									<%=cspBO.getTimeZoneArea().equals("US/Mountain") ? "selected"
					: ""%>>Mountain</option>
								<option value="US/Pacific"
									<%=cspBO.getTimeZoneArea().equals("US/Pacific") ? "selected"
					: ""%>>Pacific</option>
								<option value="Asia/Kolkata"
									<%=cspBO.getTimeZoneArea().equals("Asia/Kolkata") ? "selected"
					: ""%>>IST</option>
								<option value="Asia/Qatar"
									<%=cspBO.getTimeZoneArea().equals("Asia/Qatar") ? "selected"
					: ""%>>Saudi/Qatar</option>
								<option value="America/Bogota"
									<%=cspBO.getTimeZoneArea().equals("America/Bogota") ? "selected"
					: ""%>>Columbia</option>
								<option value="Asia/Bangkok"
									<%=cspBO.getTimeZoneArea().equals("Asia/Bangkok") ? "selected"
					: ""%>>Thailand</option>
						</select></td>
						<td><label>Time Zone</label></td>
						<td><select class="firstCol" name="TimeZone" id="TimeZone">
								<option value="00">Select</option>
								<option value="12" ORFormat
									<%=cspBO.getTimezone() == 12 ? "selected" : "0"%>>+12</option>
								<option value="11"
									<%=cspBO.getTimezone() == 11 ? "selected" : "0"%>>+11</option>
								<option value="10"
									<%=cspBO.getTimezone() == 10 ? "selected" : "0"%>>+10</option>
								<option value="09"
									<%=cspBO.getTimezone() == 9 ? "selected" : "0"%>>+09</option>
								<option value="09"
									<%=cspBO.getTimezone() == 8 ? "selected" : "0"%>>+08</option>
								<option value="07"
									<%=cspBO.getTimezone() == 7 ? "selected" : "0"%>>+07</option>
								<option value="06"
									<%=cspBO.getTimezone() == 6 ? "selected" : "0"%>>+06</option>
								<option value="05"
									<%=cspBO.getTimezone() == 5 ? "selected" : "0"%>>+05</option>
								<option value="04"
									<%=cspBO.getTimezone() == 4 ? "selected" : "0"%>>+04</option>
								<option value="03"
									<%=cspBO.getTimezone() == 3 ? "selected" : "0"%>>+03</option>
								<option value="02"
									<%=cspBO.getTimezone() == 2 ? "selected" : "0"%>>+02</option>
								<option value="01"
									<%=cspBO.getTimezone() == 1 ? "selected" : "0"%>>+01</option>
								<option value="00"
									<%=cspBO.getTimezone() == 0 ? "selected" : "0"%>>00</option>
								<option value="-01"
									<%=cspBO.getTimezone() == -1 ? "selected" : "0"%>>-01</option>
								<option value="-02"
									<%=cspBO.getTimezone() == -2 ? "selected" : "0"%>>-02</option>
								<option value="-03"
									<%=cspBO.getTimezone() == -3 ? "selected" : "0"%>>-03</option>
								<option value="-04"
									<%=cspBO.getTimezone() == -4 ? "selected" : "0"%>>-04</option>
								<option value="-05"
									<%=cspBO.getTimezone() == -5 ? "selected" : "0"%>>-05</option>
								<option value="-06"
									<%=cspBO.getTimezone() == -6 ? "selected" : "0"%>>-06</option>
								<option value="-07"
									<%=cspBO.getTimezone() == -7 ? "selected" : "0"%>>-07</option>
								<option value="-08"
									<%=cspBO.getTimezone() == -8 ? "selected" : "0"%>>-08</option>
								<option value="-09"
									<%=cspBO.getTimezone() == -9 ? "selected" : "0"%>>-09</option>
								<option value="-10"
									<%=cspBO.getTimezone() == -10 ? "selected" : "0"%>>-10</option>
								<option value="-11"
									<%=cspBO.getTimezone() == -11 ? "selected" : "0"%>>-11</option>
								<option value="-12"
									<%=cspBO.getTimezone() == -12 ? "selected" : "0"%>>-12</option>

						</select>&nbsp; <select class="firstCol" name="TimeZoneMin"
							id="TimeZoneMin">
								<option value="00">Select</option>
								<option value="00"
									<%=cspBO.getTimezoneMin() == 00 ? "selected" : "0"%>>00</option>

								<option value="05"
									<%=cspBO.getTimezoneMin() == 05 ? "selected" : "0"%>>05</option>

								<option value="10"
									<%=cspBO.getTimezoneMin() == 10 ? "selected" : "0"%>>10</option>

								<option value="15"
									<%=cspBO.getTimezoneMin() == 15 ? "selected" : "0"%>>15</option>

								<option value="20"
									<%=cspBO.getTimezoneMin() == 20 ? "selected" : "0"%>>20</option>


								<option value="25"
									<%=cspBO.getTimezoneMin() == 25 ? "selected" : "0"%>>25</option>

								<option value="30"
									<%=cspBO.getTimezoneMin() == 30 ? "selected" : "0"%>>30</option>

								<option value="35"
									<%=cspBO.getTimezoneMin() == 35 ? "selected" : "0"%>>35</option>

								<option value="40"
									<%=cspBO.getTimezoneMin() == 40 ? "selected" : "0"%>>40</option>

								<option value="45"
									<%=cspBO.getTimezoneMin() == 45 ? "selected" : "0"%>>45</option>


								<option value="50"
									<%=cspBO.getTimezoneMin() == 50 ? "selected" : "0"%>>50</option>

								<option value="55"
									<%=cspBO.getTimezoneMin() == 55 ? "selected" : "0"%>>55</option>


						</select></td>
					</tr>
					
					<tr>
						<td><label>Default State</label></td>
						<td><input type="text" name="State" size="1"
							value="<%=cspBO.getState()%>" /></td>
						<td><label>Default Country</label></td>
						<td><input type="text" name="country" size="1"
							value="<%=cspBO.getCountry()%>"></input></td>
					</tr>
					
					<tr>
						<td><label>Default Language</label></td>
						<td><select name="DefaultLanguage">
							<option value="ar" <%=cspBO.getDefaultLanguage().equals("ar") ? "selected" : ""%>>Arabic</option>
							<option value="bg" <%=cspBO.getDefaultLanguage().equals("bg") ? "selected" : ""%>>Bulgarian</option>
							<option value="bn" <%=cspBO.getDefaultLanguage().equals("bn") ? "selected" : ""%>>Bengali</option>
							<option value="ca" <%=cspBO.getDefaultLanguage().equals("ca") ? "selected" : ""%>>Catalan</option>
							<option value="cs" <%=cspBO.getDefaultLanguage().equals("cs") ? "selected" : ""%>>Czech</option>
							<option value="ds" <%=cspBO.getDefaultLanguage().equals("da") ? "selected" : ""%>>Danish</option>
							<option value="de" <%=cspBO.getDefaultLanguage().equals("de") ? "selected" : ""%>>German</option>
							<option value="el" <%=cspBO.getDefaultLanguage().equals("el") ? "selected" : ""%>>Greek</option>
							<option value="en" <%=cspBO.getDefaultLanguage().equals("en") ? "selected" : ""%>>English</option>
							<option value="en-AU" <%=cspBO.getDefaultLanguage().equals("en-AU") ? "selected" : ""%>>English (Australian)</option>
							<option value="en-GB" <%=cspBO.getDefaultLanguage().equals("en-GB") ? "selected" : ""%>>English (Great Britain)</option>
							<option value="es" <%=cspBO.getDefaultLanguage().equals("es") ? "selected" : ""%>>Spanish</option>
							<option value="eu" <%=cspBO.getDefaultLanguage().equals("eu") ? "selected" : ""%>>Basque</option>
							<option value="fa" <%=cspBO.getDefaultLanguage().equals("fa") ? "selected" : ""%>>Farsi</option>
							<option value="fi" <%=cspBO.getDefaultLanguage().equals("fi") ? "selected" : ""%>>Finnish</option>
							<option value="fil" <%=cspBO.getDefaultLanguage().equals("fil") ? "selected" : ""%>>Filipino</option>
							<option value="fr" <%=cspBO.getDefaultLanguage().equals("fr") ? "selected" : ""%>>French</option>
							<option value="gl" <%=cspBO.getDefaultLanguage().equals("gl") ? "selected" : ""%>>Galician</option>
							<option value="gu" <%=cspBO.getDefaultLanguage().equals("gu") ? "selected" : ""%>>Gujarati</option>
							<option value="hi" <%=cspBO.getDefaultLanguage().equals("hi") ? "selected" : ""%>>Hindi</option>
							<option value="hr" <%=cspBO.getDefaultLanguage().equals("hr") ? "selected" : ""%>>Croatian</option>
							<option value="hu" <%=cspBO.getDefaultLanguage().equals("hu") ? "selected" : ""%>>Hungarian</option>
							<option value="id" <%=cspBO.getDefaultLanguage().equals("id") ? "selected" : ""%>>Indonesian</option>
							<option value="it" <%=cspBO.getDefaultLanguage().equals("it") ? "selected" : ""%>>Italian</option>
							<option value="iw" <%=cspBO.getDefaultLanguage().equals("iw") ? "selected" : ""%>>Hebrew</option>
							<option value="ja" <%=cspBO.getDefaultLanguage().equals("ja") ? "selected" : ""%>>Japanese</option>
							<option value="kn" <%=cspBO.getDefaultLanguage().equals("kn") ? "selected" : ""%>>Kannada</option>
							<option value="ko" <%=cspBO.getDefaultLanguage().equals("ko") ? "selected" : ""%>>Korean</option>
							<option value="lt" <%=cspBO.getDefaultLanguage().equals("lt") ? "selected" : ""%>>Lithuanian</option>
							<option value="lv" <%=cspBO.getDefaultLanguage().equals("lv") ? "selected" : ""%>>Latvian</option>
							<option value="ml" <%=cspBO.getDefaultLanguage().equals("ml") ? "selected" : ""%>>Malayalam</option>
							<option value="mr" <%=cspBO.getDefaultLanguage().equals("mr") ? "selected" : ""%>>Marathi</option>
							<option value="nl" <%=cspBO.getDefaultLanguage().equals("nl") ? "selected" : ""%>>Dutch</option>
							<option value="no" <%=cspBO.getDefaultLanguage().equals("no") ? "selected" : ""%>>Norwegian</option>
							<option value="pt" <%=cspBO.getDefaultLanguage().equals("pt") ? "selected" : ""%>>Portuguese</option>
							<option value="pt-BR" <%=cspBO.getDefaultLanguage().equals("pt-BR") ? "selected" : ""%>>Portuguese (Brazil)</option>
							<option value="pt-PT" <%=cspBO.getDefaultLanguage().equals("pt-PT") ? "selected" : ""%>>Portuguese (Portugal)</option>
							<option value="ro" <%=cspBO.getDefaultLanguage().equals("ro") ? "selected" : ""%>>Romanian</option>
							<option value="ru" <%=cspBO.getDefaultLanguage().equals("ru") ? "selected" : ""%>>Russian</option>
							<option value="sk" <%=cspBO.getDefaultLanguage().equals("sk") ? "selected" : ""%>>Slovak</option>
							<option value="sl" <%=cspBO.getDefaultLanguage().equals("sl") ? "selected" : ""%>>Slovenian</option>
							<option value="sr" <%=cspBO.getDefaultLanguage().equals("sr") ? "selected" : ""%>>Serbian</option>
							<option value="sv" <%=cspBO.getDefaultLanguage().equals("sv") ? "selected" : ""%>>Swedish</option>
							<option value="ta" <%=cspBO.getDefaultLanguage().equals("ta") ? "selected" : ""%>>Tamil</option>
							<option value="te" <%=cspBO.getDefaultLanguage().equals("te") ? "selected" : ""%>>Telugu</option>
							<option value="th" <%=cspBO.getDefaultLanguage().equals("th") ? "selected" : ""%>>Thai</option>
							<option value="tl" <%=cspBO.getDefaultLanguage().equals("tl") ? "selected" : ""%>>Tagalog</option>
							<option value="tr" <%=cspBO.getDefaultLanguage().equals("tr") ? "selected" : ""%>>Turkish</option>
							<option value="uk" <%=cspBO.getDefaultLanguage().equals("uk") ? "selected" : ""%>>Ukrainian</option>
							<option value="vi" <%=cspBO.getDefaultLanguage().equals("vi") ? "selected" : ""%>>Vietnamese</option>
							<option value="zh-CN" <%=cspBO.getDefaultLanguage().equals("zh-CN") ? "selected" : ""%>>Chinese (Simplified)</option>
							<option value="zh-TW" <%=cspBO.getDefaultLanguage().equals("zh-TW") ? "selected" : ""%>>Chinese (Traditional)</option>
						</select></td>
					</tr>
					
					<tr>
						<td><label>Provide Ph No.</label></td>
						<td><select name="providePhoneNum">
								<option value="1"
									<%=cspBO.getProvidePhoneNum() == 1 ? "selected" : ""%>>Yes</option>
								<option value="2"
									<%=cspBO.getProvidePhoneNum() == 2 ? "selected" : ""%>>No</option>
						</select></td>
						<td><label>Caller ID</label></td>
						<td><select name="CallerId">
								<option value="1"
									<%=cspBO.getCallerId() == 1 ? "selected" : ""%>>On</option>
								<option value="2"
									<%=cspBO.getCallerId() == 2 ? "selected" : ""%>>Off</option>
						</select>
							<select name="CIType">
								<option value="0"
									<%=cspBO.getCallerIdType() == 0 ? "selected" : ""%>>Line Number</option>
								<option value="1"
									<%=cspBO.getCallerIdType() == 1 ? "selected" : ""%>>Phone Number</option>
							</select>
						</td>
					</tr>
					<tr>
						<td><label>OR Format</label></td>
						<td><input type="button" name="ORFormat" id="ORFormat"
							value="ORFormat" onclick="showORFormat()" /></td>
						<td><label>Check Mandatory Documents</label></td>
						<td><input type="checkbox" name="checkDocuments"
							id="checkDocuments" value="1"
							<%=cspBO.getCheckDocuments() == 1 ? "checked" : ""%> /></td>
					</tr>
					<tr>
						<td><div class="ORFormatShow" id="ORFormatShow"
								style="display: none;">
								<input type="radio" name="orFormat" value="1"
									<%=cspBO.getORFormat() == 1 ? "checked" : ""%>></input> <img
									alt="" src="images/ORFormat01.png"
									style="height: 100px; width: 165px;"> 
<%-- 								<input type="radio" name="orFormat" value="2"
									<%=cspBO.getORFormat() == 2 ? "checked" : ""%>></input> <img
									alt="" src="images/ORFormat03.png"
									style="height: 100px; width: 165px;"> 
								<input type="radio" name="orFormat" value="3"
									<%=cspBO.getORFormat() == 3 ? "checked" : ""%>></input> <img
									alt="" src="images/ORFormat02.png"
									style="height: 100px; width: 165px;"> 
 --%>							<input type="radio" name="orFormat" value="4"
									<%=cspBO.getORFormat() == 4 ? "checked" : ""%>></input> <img
									alt="" src="images/ORFormat04.png"
									style="height: 100px; width: 165px;">
							</div></td>
					</tr>
					<tr>
						<td><label>Window Time</label></td>
						<td><input type="number" name="windowTime" id="windowTime"
							value="<%=cspBO.getWindowTime()%>"></input></td>
						<td><label>Login Time Limit</label></td>
						<td><input type="number" name="loginTImeLimit"
							id="loginTImeLimit" value="<%=cspBO.getLoginLimitTime()%>"></input></td>
					</tr>
					<tr>
						<td><label>Allow credit Card Return</label></td>
						<td><select name="AllowCreditCardreturn">
								<option value="Yes"
									<%=cspBO.getAllowCreditCardReturns().equals("Yes") ? "selected"
					: ""%>>Yes</option>
								<option value="No"
									<%=cspBO.getAllowCreditCardReturns().equals("No") ? "selected"
					: ""%>>No</option>
						</select></td>
						<td><label>Tag Device To Cab</label></td>
						<td><input type="checkbox" name="tagDeviceCab" value="1"
							<%=cspBO.getTagDeviceCab() == 1 ? "checked=checked" : ""%> /></td>
					</tr>
					<tr>
						<td><label>Phone & SMS Prefix</label></td>
						<td><input type="text" name="prefixNum" id="prefixNum" size="4" value="<%=cspBO.getMobilePrefix()==null?"":cspBO.getMobilePrefix()%>"/>
							<input type="text" name="prefixSMS" id="prefixSMS" size="4" value="<%=cspBO.getSmsPrefix()==null?"":cspBO.getSmsPrefix()%>"/>
						</td>
						<td><label>Show Driving Directions</label></td>
						<td><input type="checkbox" name="drDirection" value="1" <%=cspBO.getDriverDirection() == 1 ? "checked=checked" : ""%> />
						</td>
					</tr>
					<tr></tr>
					<tr><td colspan="4" ><h3 class="ui-widget-header">
					<center>Customer App. System Properties</center>
					</h3></td></tr>
					<tr>
						<td><label>Credit card for Customer</label></td>
						<td><select name="CustomerCCProcess" id="CustomerCCProcess">
							<option value="1" <%=cspBO.getCcForCustomerApp()==1 ? "selected":"" %>>Mandatory</option>
							<option value="0" <%=cspBO.getCcForCustomerApp()==0 ? "selected":"" %>>Optional</option>
						</select>
						<label>Amt</label>
						$<input type="text" name="ccDefaultAmt" value="<%=cspBO.getCcDefaultAmt()==null?"":cspBO.getCcDefaultAmt()%>" size="4"/>
						</td>
					</tr>
					<tr>
						<td><label>Payment for Customer</label></td>
						<td><select name="Customerpayment" id="Customerpayment">
							<option value="1" <%=cspBO.getPaymentForCustomerApp()==1 ? "selected":"" %>>Mandatory</option>
							<option value="0" <%=cspBO.getPaymentForCustomerApp()==0 ? "selected":"" %>>Optional</option>
						</select>
						</td>
						<td align="center"><label>ETA Calculate</label></td>
						<td><select name="ETA_Calculate" id="ETA_Calculate">
							<option value="1" <%=cspBO.getETA_Calculate()==1 ? "selected":"" %>>Always Off</option>
							<option value="0" <%=cspBO.getETA_Calculate()==0 ? "selected":"" %>>Always On</option>
							<option value="2" <%=cspBO.getETA_Calculate()==2 ? "selected":"" %>>On after Allocate to driver</option>
						</select>
						</td>
					</tr>
					<tr>
						<td><label>Send Push Message to Customer after Accept : </label></td>
						<td><select name="sendPushToCustomer" id="sendPushToCustomer">
							<option value="1" <%=cspBO.getSendPushToCustomer()==1 ? "selected":"" %>>YES</option>
							<option value="0" <%=cspBO.getSendPushToCustomer()==0 ? "selected":"" %>>NO</option>
						</select>
						</td>
						
					</tr>
					<tr></tr>
					
					<tr><td colspan="4" align="center"><h3 class="ui-widget-header">Only for Fleet Customers</h3></td></tr>
					<tr><td>Do you want to dispatch all the fleets jobs in a Master fleet itself?</td>
					<td><select name="fleetDispatch" id="fleetDispatch">
							<option value="1" <%=cspBO.getFleetDispatchSwitch()==1 ? "selected":"" %>>Yes</option>
							<option value="0" <%=cspBO.getFleetDispatchSwitch()==0 ? "selected":"" %>>No</option>
						</select></td>
					</tr>
				</table>
			</div>
		</div>

		<div class="ui-layout-west" style="display: none;">
			<div id="accordion1" class="basic">

				<h3>
					<a href="#">Meter Details</a>
				</h3>
				<div>
					<table style="height: 50px">
						<tr>
							<td><label>Meter</label></td>
							<td>&nbsp;&nbsp;<select name="meterMandatory"
								id="meterMandatory">
									<option value="false"
										<%=!cspBO.isMeterMandatory() ? "Selected" : ""%>>Mandatory
										Off</option>
									<option value="true"
										<%=cspBO.isMeterMandatory() ? "Selected" : ""%>>Mandatory
										On</option>
							</select></td>
							<td>&nbsp;&nbsp;<select name="autoStartMeter" id="autoStartMeter">
									<option value="false" <%=!cspBO.isStartMeterAutomatically() ? "Selected" : ""%>>Ask Driver</option>
									<option value="true" <%=cspBO.isStartMeterAutomatically() ? "Selected" : ""%>>Automatically Start</option>
							</select></td>
							<td>&nbsp;&nbsp;<select name="startMeterHidden" id="startMeterHidden">
									<option value="false" <%=!cspBO.isStartmeterHidden() ? "Selected" : ""%>>Hide Meter Off</option>
									<option value="true" <%=cspBO.isStartmeterHidden() ? "Selected" : ""%>>Hide Meter On</option>
							</select></td>
						</tr>
						<tr>
							<td><label>Distance Based On</label></td>
							<td>&nbsp;&nbsp;<select name="distanceBasedOn"
								id="distanceBasedOn">
									<option value="0"
										<%=cspBO.getDistanceBasedOn() == 0 ? "Selected" : ""%>>Miles</option>
									<option value="1"
										<%=cspBO.getDistanceBasedOn() == 1 ? "Selected" : ""%>>KM</option>
							</select></td>
<%-- 							<td><label>CC Extra Charge</label></td>
							<td>&nbsp;&nbsp;<input type="text" name="ccExtra" id="ccExtra" value="<%=cspBO.getCreditCharge()%>">
							</td>
 --%>						</tr>
						<tr>
							<td><label>Rate Per mile</label></td>
							<td>$<input type="text" size="5" name="ratePerKM"
								id="RatePerKM" value="<%=cspBO.getRatePerMile()%>" /></td>
							<td><label>Rate Per minute</label></td>
							<td>$<input type="text" size="5" name="ratePerMinute"
								id="ratePerMinute" value="<%=cspBO.getRatePerMinute()%>" /></td>
						</tr>

						<tr>
							<td><label>Start Amount</label></td>
							<td>$<input type="text" size="5" name="startAmount"
								id="startAmount"
								value="<%=cspBO.getStartAmount() == null ? "" : cspBO.getStartAmount()%>" /></td>
							<td><label>Minimum Speed</label></td>
							<td>&nbsp;&nbsp;<input type="text" size="5"
								name="minimumSpeed" id="minimumSpeed"
								value="<%=cspBO.getMinimumSpeed() == null ? "" : cspBO
					.getMinimumSpeed()%>" />
								MpH
							</td>
						</tr>
					</table>
					<label>Additional Meter Types</label>
					<table style="height: 50px;width: 30%;" id="extraMeters">
						<tr>
							<th style="width: 2%; background-color: #d3d3d3;">Default</th>
							<th style="width: 20%; background-color: #d3d3d3;">Meter Name</th>
							<th style="width: 10%; background-color: #d3d3d3;">Rate/Mile</th>
							<th style="width: 10%; background-color: #d3d3d3;">Rate/Min</th>
							<th style="width: 10%; background-color: #d3d3d3;">Start Amt</th>
							<th style="width: 10%; background-color: #d3d3d3;">Min Speed</th>
							<th style="width: 10%; background-color: #d3d3d3;">Distance 1</th>
							<th style="width: 10%; background-color: #d3d3d3;">Discount 1</th>
							<th style="width: 10%; background-color: #d3d3d3;">Distance 2</th>
							<th style="width: 10%; background-color: #d3d3d3;">Discount 2</th>
							<th style="width: 10%; background-color: #d3d3d3;">Distance 3</th>
							<th style="width: 10%; background-color: #d3d3d3;">Discount 3</th>
						</tr>
					</table>
					<table style="height: 50px">
						<tr>
							<td><input type="button" name="addMeter" id="addMeter"
								value="Add Meter" onclick="giveNewMeter()"></td>
							<td><input type="button" name="removeMeter" id="removeMeter"
								value="Remove Meter" onclick="delLastMeter()"></td>
						</tr>
					</table>
				</div>
				<h3>
					<a href="#">Job details</a>
				</h3>
				<div>
					<table>
						<tr>
							<td><label>Dispatch Based On</label></td>
							<td><select name="Dispatch">
									<option value="1"
										<%=cspBO.getDispatchBasedOnDriverOrVehicle() == 1 ? "selected"
					: ""%>>Based
										on Driver</option>
									<option value="2"
										<%=cspBO.getDispatchBasedOnDriverOrVehicle() == 2 ? "selected"
					: ""%>>Based
										on Cab No</option>
							</select></td>
						</tr>
						<tr>
							<td><label>Calculate Zones</label></td>
							<td><select name="calculateZones">
									<option value="<%=true%>"
										<%=cspBO.getCalculateZones() == true ? "selected" : ""%>>Yes</option>
									<option value="<%=false%>"
										<%=cspBO.getCalculateZones() == false ? "selected" : ""%>>No</option>
							</select></td>
							<td><label>Book To Zone Automatically</label></td>
							<td><select name="bookAutomatically">
									<option value="<%=true%>"
										<%=cspBO.isBookAutomatically()? "selected" : ""%>>Yes</option>
									<option value="<%=false%>"
										<%=!cspBO.isBookAutomatically() ? "selected" : ""%>>No</option>
							</select></td>
						</tr>
						<tr>
							<td><label>Provide End Address</label></td>
							<td><select name="endAddress">
									<option value="1"
										<%=cspBO.getProvideEndAddress() == 1 ? "selected" : ""%>>Yes</option>
									<option value="2"
										<%=cspBO.getProvideEndAddress() == 2 ? "selected" : ""%>>No</option>
							</select></td>
							<td><label>Dispatch SharedRide Jobs</label></td>
							<td><select name="srBatchJob">
									<option value="0"
										<%=cspBO.getBatchForSR() == 0 ? "selected" : ""%>>Yes</option>
									<option value="1"
										<%=cspBO.getBatchForSR() == 1 ? "selected" : ""%>>No</option>
							</select></td>
						</tr>

						<tr>
							<td><label>OR Time Based On</label></td>
							<td><select class="firstCol" name="ORTime" id="ORTime">
									<option value="textBox"
										<%=cspBO.getORTime().equals("textBox") ? "selected" : ""%>>Text
										Box</option>
									<option value="dropDownBox"
										<%=cspBO.getORTime().equals("dropDownBox") ? "selected" : ""%>>Drop
										Down Box</option>
							</select></td>
							<td><label>Send Broadcast Job</label></td>
							<td><select name="bcJobs">
									<option value="0"
										<%=cspBO.getSendBCJ() == 0 ? "selected" : ""%>>No</option>
									<option value="1"
										<%=cspBO.getSendBCJ() == 1 ? "selected" : ""%>>Yes</option>
							</select></td>
							
						</tr>
						<tr>
							<td><label>Driver alarm in minutes</label></td>
							<td><input type="text" name="driverAlarm" size="5"
								id="driverAlarm" value="<%=cspBO.getDriveralarm()%>" /></td>
							  	<td><label>Zone Step away time</label></td>
							<td><input type="text" name="zonesStep" size="3"
								id="zonesStep" value="<%=cspBO.getZonesStepaway()%>" />Mins</td>
						</tr>
						<tr>
							<td><label>No of Digits For Ph</label></td>
							<td><input type="text" size="5" name="noOfDigitsPh"
								id="noOfDigitsPh"
								value="<%=cspBO.getTotalDigitsForPh() != null ? cspBO
					.getTotalDigitsForPh() : ""%>"
								<%=cspBO.getTotalDigitsForPh() == null
					|| cspBO.getTotalDigitsForPh().equalsIgnoreCase("") ? "readonly=readonly"
					: ""%> />
								D.C&nbsp;<input type="checkbox" name="dontCheckPh"
								onchange="checkPhoneValidation()" id="dontCheckPh"
								<%=cspBO.getTotalDigitsForPh() == null
					|| cspBO.getTotalDigitsForPh().equalsIgnoreCase("") ? "checked=checked"
					: ""%> /></td>
						<td><label>See Zone Status</label></td>
							<td><select name="seeZoneStatus">
									<option value="1"
										<%=cspBO.getSeeZoneStatus()== 1 ? "selected" : ""%>>Yes</option>
									<option value="0"
										<%=cspBO.getSeeZoneStatus()== 0 ? "selected" : ""%>>No</option>
							</select></td>
						
						</tr>
						<tr>
							<td><label>Simultoneous login</label></td>
							<td><select class="firstCol" name="simultaneousLogin"
								id="simultaneousLogin">
									<option value="1"
										<%=cspBO.getSimultaneousLogin() == 1 ? "Selected" : ""%>>Allow</option>
									<option value="0"
										<%=cspBO.getSimultaneousLogin() == 0 ? "Selected" : ""%>>Deny</option>
							</select></td>
						</tr>
						<tr>
							<td><label>Check balance</label></td>
							<td><select class="firstCol" name="driverBalance" id="driverBalance">
									<option value="true"
										<%=cspBO.isCheckBalance() ? "Selected" : ""%>>Yes</option>
									<option value="false"
										<%=!cspBO.isCheckBalance() ? "Selected" : ""%>>No</option>
							</select></td>
							<td><label>Balance Amount</label></td>
							<td><input type="text" name="driverBalanceAmount" size="5" id="driverBalanceAmount" 
							value="<%=cspBO.getDriverBalanceAmount()%>" /></td>
						</tr>
						<tr>
							<td><label>Currency prefix</label></td>
							<td><select class="firstCol" name="currencyPrefix"
								id="currencyPrefix">
									<option value="1"
										<%=cspBO.getCurrencyPrefix() == 1 ? "Selected" : ""%>>$</option>
									<option value="2"
										<%=cspBO.getCurrencyPrefix() == 2 ? "Selected" : ""%>>&euro;</option>
									<option value="3"
										<%=cspBO.getCurrencyPrefix() == 3 ? "Selected" : ""%>>&pound;</option>
									<option value="4"
										<%=cspBO.getCurrencyPrefix() == 4 ? "Selected" : ""%>>&#x20B9;</option>
							</select></td>
							<td><label>Job Offer-No Response</label></td>
							<td align="center"><select name="jobAction"
								id="jobAction">
									<option value="0" <%=cspBO.getJobAction() == 0 ? "Selected" : ""%>>No Action</option>
									<option value="1" <%=cspBO.getJobAction() == 1 ? "Selected" : ""%>>LogOut</option>
							</select></td>
							
						</tr>
						<tr>
							<td><label>Address Auto-Complete</label></td>
							<td><select name="addressCheck" id="addressCheck">
									<option value="1"
										<%=cspBO.getAddressFill() == 1 ? "Selected" : ""%>>By
										Geocode</option>
									<option value="2"
										<%=cspBO.getAddressFill() == 2 ? "Selected" : ""%>>By
										Places</option>
									<option value="3"
										<%=cspBO.getAddressFill() == 3 ? "Selected" : ""%>>By
										OSM</option>
							</select></td>
						</tr>

						<tr>
							<td><label>See Driver in Mobile</label></td>
							<td align="center"><select name="mobileDriver"
								id="mobileDriver">
									<option value="2"
										<%=cspBO.getDriverListMobile() == 2 ? "Selected" : ""%>>Summary</option>
									<option value="1"
										<%=cspBO.getDriverListMobile() == 1 ? "Selected" : ""%>>Individual</option>
							</select></td>
						</tr>
						<tr>
							<td><label>Enter Distance Amount In Fare</label></td>
							<td align="center"><select name="distanceToFare"
								id="distanceToFare">
									<option value="0"
										<%=cspBO.getDistanceValueToFare() == 0 ? "Selected" : ""%>>No</option>
									<option value="1"
										<%=cspBO.getDistanceValueToFare() == 1 ? "Selected" : ""%>>Yes</option>
							</select></td>
						</tr>
						<tr>
							<td><label>Zone Logout Time</label></td>
							<td align="center"><input type="text" size="3"
								name="zoneLogout" id="zoneLogout"
								value="<%=cspBO.getZoneLogoutTime() == 0 ? "" : cspBO
					.getZoneLogoutTime()%>">
							</td>
						</tr>
						<tr>
							<td><label>Show Future Job Counts</label></td>
							<td align="center"><input type="checkbox"
								name="futurecounts" id="futurecounts" value="1"
								<%=cspBO.getFutureCounts() == 1 ? "checked=checked" : ""%>></input>
							</td>
						</tr>
						
						<tr>
							<td><label>Payment for Jobs</label></td>
							<td align="center"><select name="payment_mandatory" id="payment_mandatory">
									<option value="1"
										<%=cspBO.getPaymentMandatory().equalsIgnoreCase("1") ? "Selected" : ""%>>Mandatory</option>
									<option value="0"
										<%=cspBO.getPaymentMandatory().equalsIgnoreCase("0") ? "Selected" : ""%>>Optional</option>
							</select></td>
						</tr>
						
						<tr>
						<td><label>No Show</label></td>
						<td><select name="no_show" id="no_show">
							<option value="0" <%=cspBO.getNo_show()==0 ? "Selected" : ""%>>Wait for Operator to Approve</option>
							<option value="1" <%=cspBO.getNo_show()==1 ? "Selected" : ""%>>Auto Approve</option>
						</select></td>
						</tr>
						<tr>
						<td><label>On No Show - Driver ReLogin to Zone (If already logged in any zone):</label></td>
						<td><select name="ns_Login" id="ns_Login">
							<option value="0" <%=cspBO.getZone_login()==0 ? "Selected" : ""%>>No</option>
							<option value="1" <%=cspBO.getZone_login()==1 ? "Selected" : ""%>>Yes</option>
						</select></td>
						</tr>
						
						<tr>
						<td><label>Location History for Driver Enabled:</label></td>
						<td><select name="storeHistory" id="storeHistory">
							<option value="0" <%=cspBO.getStoreGPSHistory()==0 ? "Selected" : ""%>>No</option>
							<option value="1" <%=cspBO.getStoreGPSHistory()==1 ? "Selected" : ""%>>Yes</option>
						</select></td>
						</tr>
						
						<tr>
						<td><label>Check Driver Mobile (While Login):</label></td>
						<td><select name="checkDriverMobile" id="checkDriverMobile">
							<option value="0" <%=cspBO.getCheckDriverMobile()==0 ? "Selected" : ""%>>No</option>
							<option value="1" <%=cspBO.getCheckDriverMobile()==1 ? "Selected" : ""%>>Yes</option>
						</select></td>
						</tr>
						
						<tr>
						<td><label>If Yes, Want to check Driver app Version?</label></td>
						<td><select name="checkDrMobVersion" id="checkDrMobVersion">
							<option value="0" <%=cspBO.getCheckDr_Mob_Version()==0 ? "Selected" : ""%>>No</option>
							<option value="1" <%=cspBO.getCheckDr_Mob_Version()==1 ? "Selected" : ""%>>Yes</option>
						</select></td>
						</tr>
						
						<tr>
						<td><label>Enable start address on Flag trip?</label></td>
						<td><select name="staddress_flagtrip" id="staddress_flagtrip">
							<option value="0" <%=cspBO.getEnable_stAdd_flagTrip()==0 ? "Selected" : ""%>>No</option>
							<option value="1" <%=cspBO.getEnable_stAdd_flagTrip()==1 ? "Selected" : ""%>>Yes</option>
						</select></td>
						</tr>
						<tr>
						<td><label>Have to send message to Driver, if job is not accepted or not received?</label></td>
						<td><select name="power_mode" id="power_mode">
							<option value="0" <%=cspBO.getPower_mode()==0 ? "Selected" : ""%>>No</option>
							<option value="1" <%=cspBO.getPower_mode()==1 ? "Selected" : ""%>>Yes</option>
						</select></td>
						</tr>
						<tr>
						<td><label>Enable power save mode in Driver app?</label></td>
						<td><select name="enable_power_save" id="enable_power_save">
							<option value="0" <%=cspBO.getEnable_power_save()==0 ? "Selected" : ""%>>No</option>
							<option value="1" <%=cspBO.getEnable_power_save()==1 ? "Selected" : ""%>>Yes</option>
						</select></td>
						</tr>
						
					</table>
				</div>
				<h3>
					<a href="#">Service Alerts</a>
				</h3>
				<div>
					<table>
						<tr>
							<td><label>No Progress</label></td>
							<td><input type="checkbox" name="noProgress" id="noProgress"
								value="1"
								<%=cspBO.getNoProgress() == 1 ? "checked=checked" : ""%>></input>
								<input type="text" name="distanceNoProgress"
								id="distanceNoProgress"
								value="<%=cspBO.getDistanceNoProgress()%>"
								placeholder="Enter the Distance"></input>Miles</td>
						</tr>
						<tr>
							<td><label>Meter On</label></td>
							<td><input type="checkbox" name="meterOn" id="meterOn"
								value="1" <%=cspBO.getMeterOn() == 1 ? "checked=checked" : ""%>></input>
								<input type="text" name="distanceMeterOn" id="distanceMeterOn"
								value="<%=cspBO.getDistanceMeterOn()%>"
								placeholder="Enter the Distance"></input>Miles</td>
						</tr>
						<tr>
							<td><label>Remote No trip</label></td>
							<td><input type="checkbox" name="remoteNoTrip"
								id="remoteNoTrip" value="1"
								<%=cspBO.getRemoteNoTrip() == 1 ? "checked=checked" : ""%>></input>
								<input type="text" name="distanceRemoteNOTrip"
								id="distanceRemoteNOTrip"
								value="<%=cspBO.getDistanceRemoteNoTrip()%>"
								placeholder="Enter the Distance"></input>Miles</td>
						</tr>
						<tr>
							<td><label>Onsite Distance</label></td>
							<td></td>
							<td>
								<input type="text" name="onSiteDist" id="onSiteDist"
								value="<%=cspBO.getOnsiteDist()%>"
								placeholder="Enter the Distance"></input>Miles</td>
						</tr>
						<tr>
							<td><label>Posting Frequncy</label></td>
							<td></td>
							<td>
								<input type="text" name="postFrequency" id="postFrequency"
								value="<%=cspBO.getFrequencyOfPosting()%>">Mins</input></td>
						</tr>
						<tr>
							<td><label>No Trip too Soon</label></td>
							<td><input type="checkbox" name="noTripTooSoon"
								id="noTripTooSoon" value="1"
								<%=cspBO.getNoTripTooSoon() == 1 ? "checked=checked" : ""%>></input>
								<input type="text" name="distanceNoTripTooSoon"
								id="distanceNoTripTooSoon"
								value="<%=cspBO.getDistanceNoTripTooSoon()%>"
								placeholder="Enter the Distance"></input>Miles</td>
						</tr>
					</table>
				</div>

				<h3>
					<a href="#">App. Rate Calculation Settings</a>
				</h3>
				<div>
					<table>
						<tr>
							<td><label>Rate Per Mile</label></td>
							<td><input type="text" name="appRatePerMile"
								id="appRatePerMile" value="<%=cspBO.getAppRatePerMile()%>"
								placeholder="Enter the Rate"></input>$</td>
						</tr>
						<tr>
							<td><label>Rate Per Minute</label></td>
							<td><input type="text" name="appRatePerMinute"
								id="appRatePerMinute" value="<%=cspBO.getAppRatePerMinute()%>"
								placeholder="Enter the Rate"></input>$</td>
						</tr>
						<tr>
							<td><label>Start Rate</label></td>
							<td><input type="text" name="appStartRate" id="appStartRate"
								value="<%=cspBO.getAppStartRate()%>"
								placeholder="Enter the Rate"></input>$</td>
						</tr>

					</table>
				</div>
			</div>
		</div>
	</form>
</body>
</html>
