<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.tds.tdsBO.DispatchPropertiesBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
    <!DOCTYPE html >

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Disptach Properties</title>
<link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
$(document).ready ( function(){
$("#onAcceptMessage").keypress(function (e) { 
	var	messageLength=document.getElementById("onAcceptMessage").value.length;
	if(messageLength>139 && e.which!=8 && e.which!=46){
		alert("Sorry OnAccept Message Cannot Exceed 140 Characters");
		return false;
	}
});
$("#onRouteMessage").keypress(function (e) { 
	var	messageLength=document.getElementById("onRouteMessage").value.length;
	if(messageLength>139 && e.which!=8 && e.which!=46){
		alert("Sorry OnRoute Message Cannot Exceed 140 Characters");
		return false;
	}
});
$("#onSiteMessage").keypress(function (e) { 
	var	messageLength=document.getElementById("onSiteMessage").value.length;
	if(messageLength>139 && e.which!=8 && e.which!=46){
		alert("Sorry OnSite Message Cannot Exceed 140 Characters");
		return false;
	}
});
});
</script>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
<body>
<form method="post" action="control" name="masterForm" id="masterForm" onsubmit="return showProcess()">
<input type="hidden" name="action" value="systemsetup"/>
<input type="hidden" name="event" value="dispatchProperties"/>
<input type="hidden" name="module" value="systemsetupView"/>
<input type="hidden" id="size" name="size" value=""/>
	<c class="nav-header"><center>Dispatch Properties</center></c>
                		 
 <table style="width: 100%;" class="navbar-static-top" border="1">
 <tr>
						<%
						DispatchPropertiesBO dispatchBO = new DispatchPropertiesBO();
						if(request.getAttribute("dispatchBO")!=null){
							dispatchBO=(DispatchPropertiesBO) request.getAttribute("dispatchBO");
						}
						DispatchPropertiesBO dispatchCallBO = new DispatchPropertiesBO();
						if(request.getAttribute("dispatchCallBO")!=null){
							dispatchCallBO=(DispatchPropertiesBO) request.getAttribute("dispatchCallBO");
						}
						
							String error ="";
							if(request.getAttribute("errors") != null) {
							 error = (String) request.getAttribute("errors");
							} 
					%>
						<td colspan="7">
							<div id="errorpage">
							<%= error.length()>0?""+error :"" %>
					   		</div>
				   		</td>
				 </tr>
                      <tr>
                        <th style="width: 40%;">Fields needed</th>
                        <th style="width: 20%;">On Offer</th>
                        <th style="width: 20%;">On Accept</th>
                        <th style="width: 20%;">On Site</th>
                      
                      </tr>
                  <tr>
                  <td align="justify">Passenger Name</td>
                  <td align="center"><input type="checkbox" name="name" value="1" <%=dispatchBO.getName_onOffer()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="name_OnAccept" value="1" <%=dispatchBO.getName_onAccept()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="name_OnSite" value="1" <%=dispatchBO.getName_onSite()==1?"checked":"" %> /></td>
                  </tr>
                  
                   <tr>
                  <td align="justify">Passenger PhoneNumber</td>
                  <td align="center"><input type="checkbox" name="phoneNumber" value="1" <%=dispatchBO.getPhoneNumber_onOffer()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="phoneNumber_OnAccept" value="1"  <%=dispatchBO.getPhoneNumber_onAccept()==1?"checked":"" %>  /></td>
                  <td align="center"><input type="checkbox" name="phoneNumber_OnSite" value="1"  <%=dispatchBO.getPhoneNumber_onSite()==1?"checked":"" %>  /></td>
                  </tr>
                  
                  <tr>
                  <td align="justify">Start Address1</td>
                  <td align="center"><input type="checkbox" name="sAdd1" value="1" <%=dispatchBO.getsAddress1_onOffer()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="sAdd1_OnAccept" value="1" <%=dispatchBO.getsAddress1_onAccept()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="sAdd1_OnSite" value="1" <%=dispatchBO.getsAddress1_onSite()==1?"checked":"" %>/></td>
                  </tr>
                  <tr>
                  <td align="justify">Start Address2</td>
                  <td align="center"><input type="checkbox" name="sAdd2" value="1" <%=dispatchBO.getsAddress2_onOffer()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="sAdd2_OnAccept" value="1" <%=dispatchBO.getsAddress2_onAccept()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="sAdd2_OnSite" value="1" <%=dispatchBO.getsAddress2_onSite()==1?"checked":"" %> /></td>
                  </tr>
                  
                   <tr>
                  <td align="justify">Start City</td>
                  <td align="center"><input type="checkbox" name="sCity" value="1"  <%=dispatchBO.getsCity_onOffer()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="sCity_OnAccept" value="1"  <%=dispatchBO.getsCity_onAccept()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="sCity_OnSite" value="1"  <%=dispatchBO.getsCity_onSite()==1?"checked":"" %> /></td>
                  </tr>
                  
                    <tr>
                  <td align="justify">End Address1</td>
                  <td align="center"><input type="checkbox" name="eAdd1" value="1" <%=dispatchBO.geteAddress1_onOffer()==1?"checked":"" %>  /></td>
                  <td align="center"><input type="checkbox" name="eAdd1_OnAccept" value="1" <%=dispatchBO.geteAddress1_onAccept()==1?"checked":"" %>  /></td>
                  <td align="center"><input type="checkbox" name="eAdd1_OnSite" value="1" <%=dispatchBO.geteAddress1_onSite()==1?"checked":"" %>  /></td>
                  </tr>
                  
                    <tr>
                  <td align="justify">End Address2</td>
                  <td align="center"><input type="checkbox" name="eAdd2" value="1" <%=dispatchBO.geteAddress2_onOffer()==1?"checked":"" %>  /></td>
                  <td align="center"><input type="checkbox" name="eAdd2_OnAccept" value="1" <%=dispatchBO.geteAddress2_onAccept()==1?"checked":"" %>  /></td>
                  <td align="center"><input type="checkbox" name="eAdd2_OnSite" value="1" <%=dispatchBO.geteAddress2_onSite()==1?"checked":"" %>  /></td>
                  </tr>
                  
                    <tr>
                  <td align="justify">End City</td>
                  <td align="center"><input type="checkbox" name="eCity" value="1" <%=dispatchBO.geteCity_onOffer()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="eCity_OnAccept" value="1" <%=dispatchBO.geteCity_onAccept()==1?"checked":"" %> /></td>
                  <td align="center"><input type="checkbox" name="eCity_OnSite" value="1" <%=dispatchBO.geteCity_onSite()==1?"checked":"" %> /></td>
                  </tr>
                  
                    <tr>
                  <td align="justify">Payment Type</td>
                  <td align="center"><input type="checkbox" name="paymentType" value="1" <%=dispatchBO.getPaymentType_onOffer()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="paymentType_OnAccept" value="1" <%=dispatchBO.getPaymentType_onAccept()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="paymentType_OnSite" value="1" <%=dispatchBO.getPaymentType_onSite()==1?"checked":"" %>/></td>
                  </tr>
                   <tr>
                  <td align="justify">Fare Amount</td>
                  <td align="center"><input type="checkbox" name="fareAmt" value="1" <%=dispatchBO.getFareAmt_onOffer()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="fareAmt_OnAccept" value="1" <%=dispatchBO.getFareAmt_onAccept()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="fareAmt_OnSite" value="1" <%=dispatchBO.getFareAmt_onSite()==1?"checked":"" %>/></td>
                  </tr>
                   <tr>
                  <tr>
                  <td align="justify">Start Zones</td>
                  <td align="center"><input type="checkbox" name="zones" value="1" <%=dispatchBO.getZone_onOffer()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="zones_OnAccept" value="1" <%=dispatchBO.getZone_onAccept()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="zones_OnSite" value="1" <%=dispatchBO.getZone_onSite()==1?"checked":"" %>/></td>
                  </tr>
                  
                   <tr>
                  <td align="justify">End Zones</td>
                  <td align="center"><input type="checkbox" name="endZones" value="1" <%=dispatchBO.getEndZone_onOffer()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="endZones_OnAccept" value="1" <%=dispatchBO.getEndZone_onAccept()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="endZones_OnSite" value="1" <%=dispatchBO.getEndZone_onSite()==1?"checked":"" %>/></td>
                  </tr>
                   <tr>
                  <td align="justify">Job Attributes(Spl Req)</td>
                  <td align="center"><input type="checkbox" name="jobAttributes" value="1" <%=dispatchBO.getJobAttributeOnOffer()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="jobAttributes_OnAccept" value="1" <%=dispatchBO.getJobAttributeOnAccept()==1?"checked":"" %>/></td>
                  <td align="center"><input type="checkbox" name="jobAttributes_OnSite" value="1" <%=dispatchBO.getJobAttributeOnSite()==1?"checked":"" %>/></td>
                  </tr>
               </table>  
                <h2><center>Call/Message Passenger Properties</center></h2>
 				<table style="width: 100%;" class="navbar-static-top" border="1">
 				<tr>
 				        <th style="width: 40%;">Fields needed</th>
                        <th style="width: 20%;">On Accept</th>
                        <th style="width: 20%;">On Route</th>
                        <th style="width: 20%;">On Site</th>
                 </tr>
                  <tr>
	                  <td align="justify">Call Passenger</td>
	                  <td align="center"><input type="checkbox" name="callPass_OnAccept" value="1" <%=dispatchCallBO.getCall_onAccept()==1?"checked":"" %> /></td>
	                  <td align="center"><input type="checkbox" name="callPass_OnRoute" value="1" <%=dispatchCallBO.getCall_onRoute()==1?"checked":"" %> /></td>
	                  <td align="center"><input type="checkbox" name="callPass_OnSite" value="1" <%=dispatchCallBO.getCall_onSite()==1?"checked":"" %> /></td>
                  </tr>
                   <tr>
	                  <td align="justify">SMS Passenger</td>
	                  <td align="center"><input type="checkbox" name="msgPass_OnAccept" value="1" <%=dispatchCallBO.getMessage_onAccept()==1?"checked":"" %> /></td>
	                  <td align="center"><input type="checkbox" name="msgPass_OnRoute" value="1" <%=dispatchCallBO.getMessage_onRoute()==1?"checked":"" %> /></td>
	                  <td align="center"><input type="checkbox" name="msgPass_OnSite" value="1" <%=dispatchCallBO.getMessage_onSite()==1?"checked":"" %> /></td>
                  </tr>
                   <tr>
	                  <td align="justify">SMS Message <br/><font color="red">(Ex:Your Job Allocated To Driver [Name] Id [Driver] With Cab [CabNo]. To Speak With Driver [Driver],Call [PhNum] **Fields Inside [] Are Case Sensitive**)</font></td>
	                  <td align="center"><textarea rows="5" cols="1" name="onAcceptMessage" id="onAcceptMessage" placeholder="To Use DriverId Type [Driver],Cab Num [CabNo],Driver Ph [PhNum] & Name [Name]"><%=dispatchCallBO.getMessageAccept()==null?"":dispatchCallBO.getMessageAccept() %></textarea>
	                  </td>
	                  <td align="center"><textarea rows="5" cols="1" name="onRouteMessage" id="onRouteMessage" placeholder="To Use DriverId Type [Driver],Cab Num [CabNo],Driver Ph [PhNum] & Name [Name]"><%=dispatchCallBO.getMessageRoute()==null?"":dispatchCallBO.getMessageRoute() %></textarea>
	                  </td>
	                  <td align="center"><textarea rows="5" cols="1" name="onSiteMessage" id="onSiteMessage" placeholder="To Use DriverId Type [Driver],Cab Num [CabNo],Driver Ph [PhNum] & Name [Name]"><%=dispatchCallBO.getMessageSite()==null?"":dispatchCallBO.getMessageSite() %></textarea>
	                  </td>
                  </tr>
                  <tr> 
                  	<td colspan="4" align="center"><input type="submit" class="styled-button" name="submit" value="Submit" onclick="submitProperties()"/>
					</td>
				</tr>
                 </table>
 </form>
</body>
</html>