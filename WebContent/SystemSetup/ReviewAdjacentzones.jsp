<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdjacentZonesBO"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
		

<% ArrayList<AdjacentZonesBO> zone_list=(ArrayList<AdjacentZonesBO>)request.getAttribute("zone_list");

 %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>TDS(Taxi Dispatching System)</title>
</head>
  <script>
  $(document).ready(function(){
	  getZones();  
	 
  //$("#zoneNumber").autocomplete(data);
  //$("#AdjacentZones").autocomplete(data);
  });
  
  var data;
  function getZones(){
	 
	  var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='SystemSetupAjax?event=zoneDetails&module=systemsetupView';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	data=text.split("^^^");
  	
  }
  
  </script>
<script type="text/javascript">
function deleteAdjZones(serialNo, x){
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=deleteAdjacentZones&module=systemsetupView&delete=YES&serialNum='+serialNo;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="Deleted Successfully"){
	var table = document.getElementById("adjacentZones");
	 table.deleteRow(x.parentNode.parentNode.rowIndex);
	}
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
<body>
<form method="post" action="control" name="queue" onsubmit="return showProcess()">
 <input type="hidden" name="action" value="systemsetup">
<input type="hidden" name="event" value="ReviewAdjacentZones">
<input type="hidden" name="module" value="systemsetupView">
<c class="nav-header"><center>Review Adjacent Zones</center></c>
						<%
							String error ="";
							if(request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
							} 
					%>
							<div id="errorpage" class="alert-error">
							<%= error.length()>0?""+error :"" %>
					   		</div>
 <table style="width:100%;" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
                    <tr>
                    	<td >Zone Number</td>
                    	<td><input type="text" name="zoneNumber" id="zoneNumber" size="7"  value=""/></td>
                    	<td >Adjacent Zones</td>
                    	<td><input type="text"  name="AdjacentZones" id="AdjacentZones" size="7"  value=""/></td>
                    </tr>
                    <tr align="center" >
                    			<td colspan="4" align="center">
                        	   <input type="submit" name="button"  value="Search"/>
				</td>
				
                    </tr>
                    </table>
			 <%if(zone_list!=null){ %>
	<table id ="adjacentZones" style="width:80%;" align="center" border="1" cellspacing="0" cellpadding="0" class="thumbnail">
			<tr>
				<td style="width:20%;">Zone&nbsp;Number</td>
				<td style="width:30%;">Adjacent&nbsp;Zone</td>
				<td style="width:25%;">Order&nbsp;Number</td>
				<td style="width:25%;">Actions</td>
			</tr>
			<% 
	boolean colorLightGreen = true;
	String colorPattern;%>
			<% for(int i= 0;i< zone_list.size();i++) {  
			colorLightGreen = !colorLightGreen;
		if(colorLightGreen){
			colorPattern="style=\"background-color:lightgreen\""; 
			}
		else{
			colorPattern="";
		}
	%>
                   
                    <tr>
                    <td align="center" <%=colorPattern %>><%=zone_list.get(i).getZoneNumber() %>
                    <input type="hidden" name="zoneNum" id="zoneNum" value="<%=zone_list.get(i).getZoneNumber() %>"/>
                    </td>
                    <td align="center" <%=colorPattern %>><%=zone_list.get(i).getAdjacentZones() %>
                    <input type="hidden" name="adjZone" id="adjZone" value="<%=zone_list.get(i).getAdjacentZones() %>"/>
                    </td>
                    <td align="center" <%=colorPattern %>><%=zone_list.get(i).getOrderNumber() %> 
                   <input type="hidden" name="orderNum" id="orderNum" value="<%=zone_list.get(i).getOrderNumber() %>"/>
                   </td>
                   <td align="center" <%=colorPattern %>>
                   <input type="button" name="btndelete" id="btndelete" onclick="showProcess();deleteAdjZones(<%=zone_list.get(i).getSerialNo() %>, this)"   value="delete"/>
                   	<a style="text-decoration: none;" href="control?action=systemsetup&event=reviewAdjacentZones&module=systemsetupView&edit=YES&zoneNum=<%=zone_list.get(i).getZoneNumber() %>&button=YES" onclick="showProcess();">Edit</a>	               
                   	<a style="text-decoration: none;" href="control?action=systemsetup&event=reviewAdjacentZones&module=systemsetupView&review=YES&zoneNum=<%=zone_list.get(i).getZoneNumber() %>&button=YES" onclick="showProcess();">Review</a>	               
                    </td>
                    </tr>
                     <%} %>
                    </table> 
            <% } %>
</form>
</body>
</html>