<%@ page import="com.tds.constants.docLib.*" %>
<%
	String ctxPath = request.getContextPath();
	
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>


<title>Document Library</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<link rel="stylesheet" type="text/css" href="<%=ctxPath %>/css/document.css" media="all">
<link rel="stylesheet" href="<%=ctxPath %>/css/document/docStyle.css" type="text/css" />
<link href="<%=ctxPath %>/js/uploadify/uploadify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<%=ctxPath %>/js/uploadify/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="<%=ctxPath %>/js/uploadify/swfobject.js"></script>
<script type="text/javascript" src="<%=ctxPath %>/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>
	<%-- 
	<link type="text/css" rel="stylesheet" href="jqueryNew/bootstrap.min.css" media="screen"></link>

<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 
<script type="text/javascript" 	src="jqueryNew/graphDemo.js"></script> 

<script type="text/javascript" src="<%=ctxPath %>/js/document/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<%=ctxPath %>/js/document/jquery.tablesorter.pager.js"></script>
 --%>

<script type="text/javascript">

function cancelUpload(){

	var compId = document.getElementById("compId").value;
	var userId = document.getElementById("userId").value;
	document.getElementById("upload").action="<%=ctxPath %>/control?action=TDS_DOCUMENT_UPLOAD&event=passKeyGen&module=operationView&do=disp&userId="+userId;
	document.getElementById("upload").submit();
}

	$(document).ready(function() {
		var cmpId= "<%=request.getAttribute("compId") %>";
		var userId= "<%=request.getAttribute("userId") %>";
		var maxSize = "<%=DocumentConstant.MAX_SIZE %>";
		var fileType = "<%=DocumentConstant.FILETYPE %>";
		
		$('#file_upload').uploadify( {
			'uploader' : '<%=ctxPath %>/js/uploadify/uploadify.swf',
			'script' : '<%=ctxPath %>/uploadDocs?do=upload',
			'cancelImg' : '<%=ctxPath %>/js/uploadify/cancel.png',
			'folder' : '/usr/local',
			'multi' : true,
			'type' : 'POST',
			'fileExt'     : fileType,
			'fileDesc'    : 'TDS Files',
			'sizeLimit'   : maxSize,
			'simUploadLimit': 1,
			'scriptData' : {'compId': cmpId , 'userId':userId,'docId':'', },
			'removeComplted'	:	true,
			onAllComplete : function(evt,queueId,fileObj,response,data){
					alert("Document uploaded successfully.");
			}
		});

	});
</script>



</head>
<body>

<!-- <form name="upload" action="uploadDocs?compCode=12&do=disp&role=USER" method="post"
	enctype="multipart/form-data">  -->

	<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	
<form name="upload" action="uploadDocs" method="post"
	enctype="multipart/form-data" id="upload">
	
<input type="hidden" name="compId" id="compId"  value='<%=request.getAttribute("compId") %>'/>
<input type="hidden" name="userId" id="userId"  value='<%=request.getAttribute("userId") %>'/>	
<table width="75%" >
<tr>
<td>


<table id="documentListasdf" class="tablesorter"   width="75%" > 
<thead>
	<tr>
		<t2><b><center>UPLOAD DOCUMENT</center></b></t2>
		
	</tr>
	
</thead>	



<tr>
	<td width="50%">
		<div style="overflow: auto;height=400px;vertical-align: bottom;">
		
		<input id="file_upload" name="file_upload" type="file" />
		
		</div>
	</td>
	<td >
			<B>Instructions</B><br>
			1.	Can able to upload max of 10 documents. </BR> 
			2.	Each document can be maximum <%=DocumentConstant.MAX_SIZE_DESC %>.</BR>
			3.	Allowed File extension : <%=DocumentConstant.FILETYPE %> </BR>
			4.	UPloaded document requires 2 to 3 
				business day to approve by portal admin.</BR>
		</BR>	
			</BR>
				<div align="center">
		<input type="button" name="Upload" value="Upload"
			onclick="javascript:$('#file_upload').uploadifyUpload()" />
		<input type="button" name="Cancel" value="Cancel" onclick="javascript:cancelUpload()"/>			
		</div>
	</td>
</tr>

</table>
</td>
</tr> 
</table>
 </div>    
              </div>

</form>

            	
    
          
            
            <footer>Copyright &copy; 2010 Get A Cab</footer>>
</body>
</html>

