<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<link type="text/css" href="css/main.css" rel="stylesheet" />
<script type="text/javascript" src="<%=TDSProperties.getValue("googleMapV3")%>"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geocode&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>
<link href="css/Popup.css" rel="stylesheet" type="text/css" />

<%AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user"); %>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/thickbox.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></link>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 
	
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="js/gmap3.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<%String voucherNo=""; %>
<title>TDS (Taxi Dispatch System)</title>

<script type="text/javascript">

var geocoder;
var map;

$(document).ready( function() {
	currentDateHistory();
	//initializeGeocode();
	
	var latitude=document.getElementById("defaultLatitude").value;
	var longitude=document.getElementById("defaultLongitude").value;
	var centerCoord = new google.maps.LatLng(latitude,longitude);
	//map = new google.maps.Map(document.getElementById("map_canvas"), null);
	
	map = new google.maps.Map(document.getElementById('map_canvas'), {
	    center: centerCoord,
	    zoom: 13,
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  });
	
	//google.maps.event.addDomListener(window, "load", initialize);
});

function initializeGeocode(i){
		
	var defaultState=document.getElementById("defaultState").value;
	var defaultCountry=document.getElementById("defaultCountry").value;
	var inputP = document.getElementById('PEA_'+i);
	
	if(document.getElementById("addressCheck").value=="2"){
		var input = document.getElementById('EA_'+i);
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map);
		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			
			if (!place.geometry) {
				$("#EA_"+i).val(inputP.value);
			      window.alert("Autocomplete's returned place contains no geometry");
			      return;
			    }
			
			if (place.address_components) {
				var address_details = extractAddressFromPlaceResult(place, '#EA_'+i);
				$('#EA_add1_'+i).val(address_details.street_no+" "+address_details.street_name);
				$('#EA_Post_'+i).val(address_details.postal);
				$('#EA_City_'+i).val(address_details.locality);
				$('#EA_add2_'+i).val(address_details.areaLevel);
			}
			
			var latitude = place.geometry.location.lat();
			var longitude = place.geometry.location.lng();  
			
			$("#EA_Lat_"+i).val(latitude);
			$("#EA_Lon_"+i).val(longitude);
			
			//$("#EA_"+i).val(autocomplete);
		});
	} else {
		geocoder = new google.maps.Geocoder();
		/* var input = document.getElementById('EA_'+i);
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map); */
		$("#EA_"+i).autocomplete({
			//This bit uses the geocoder to fetch address values
			source: function(request, response) {
				geocoder.geocode( {'address': request.term +','+defaultState+','+defaultCountry }, function(results, status) {
					response($.map(results, function(item) {
						return {
							label: item.formatted_address,
							value: item.formatted_address,
							latitude: item.geometry.location.lat(),
							longitude: item.geometry.location.lng(),
							city: item.postal_code,
							addcomp: item.address_components
							//nhd: item.address_components_of_type("neighborhood")
						}
					}));
				})
			},
			//This bit is executed upon selection of an address
			select: function(event, ui) {
				var arrAddress =ui.item.addcomp;
				var streetnum= "";
				var route = "";
				var add2 = "";
				var post = ""
				var city ="";
				var itemCountry="";
				
				$.each(arrAddress, function (i, address_component) {
					if (address_component.types[0] == "subpremise"){
						add2 = address_component.long_name;
					}
					
					if (address_component.types[0] == "street_number"){
						streetnum = address_component.long_name;
					}

					if (address_component.types[0] == "locality"){
						city = address_component.long_name;
					}

					if (address_component.types[0] == "route"){ 
						route = address_component.long_name;
					}
					if (address_component.types[0] == "country"){ 
						itemCountry = address_component.long_name;
					}
					if (address_component.types[0] == "postal_code"){ 
						post = address_component.long_name;
					}
				});
				
				$("#EA_add1_"+i).val(streetnum+" "+route);
				$("#EA_add2_"+i).val(add2);
				$("#EA_City_"+i).val(city);
				$("#EA_Post_"+i).val(post);
				
				$("#EA_"+i).val(arrAddress);
				$("#EA_Lat_"+i).val(ui.item.latitude);
				$("#EA_Lon_"+i).val(ui.item.longitude);

				//$("#EA_"+i).autocomplete("close");
			}
		});
	}
}

function extractAddressFromPlaceResult(place, fieldBackup) {
	var street_no = null, street_name = null, postal = null, locality = null, country = null, areaLevel=null;

	if(place == undefined || place == null || place.address_components == undefined || place.address_components == null)
		return {"valid":false};

		for(var i=0;place.address_components.length>i;i++)
			for(var j=0;place.address_components[i].types.length>j;j++)
			{
				var t = place.address_components[i].types[j];
				//alert(t+"---"+place.address_components[i].long_name);
				if(t == 'route')
					street_name = place.address_components[i].long_name;
				else if(t == 'street_number')
					street_no = place.address_components[i].long_name;
				else if(t == 'postal_code')
					postal = place.address_components[i].long_name;
				else if(t == 'locality')
					locality = place.address_components[i].long_name;
				else if(t == 'country')
					country = place.address_components[i].long_name;
				else if(t == 'sublocality_level_1')
					areaLevel = place.address_components[i].long_name;
			}

		if(!(street_no != null || fieldBackup == null))
		{
			var m = jQuery(fieldBackup).val().match(/^([0-9][0-9]*)[^0-9]/);
			if(m == null || m.length != 2)
				; // do nothing
			else
				street_no = m[1];
		}

		return {
			"valid": street_name != null,
			"street_no": street_no,
			"street_name": street_name,
			"locality": locality,
			"country": country,
			"areaLevel": areaLevel,
			"postal": postal };
}

function currentDateHistory() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	document.getElementById("from_date").value =cMonth+"/"+cDate+"/"+cYear;
	document.getElementById("toDateHis").value =cMonth+"/"+cDate+"/"+cYear;
}

function getHistoryTrips(){
	
	document.getElementById("ORHTableEdit").innerHTML ="";
	document.getElementById("errorMessage").innerHTML ="";
	
	var number = document.getElementById("voucherNo1").value;
	var fdate = document.getElementById("from_date").value;
	var tdate = document.getElementById("toDateHis").value;
	
	if(fdate=="" || tdate==""){
		alert("Date is mandatory");
		return;
	}
	try{
	var xmlhttp;
	 if(window.XMLHttpRequest) 
		 {
		 xmlhttp = new XMLHttpRequest();
		 }
	 else
		 {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		 }
	 var url='EditJobs?event=getDetails&vNo='+number+'&fdate='+fdate+'&tdate='+tdate;
	 //alert("url is"  +url);
	 xmlhttp.open("GET",url,false);
	 xmlhttp.send(null);
	 var text = xmlhttp.responseText;
	 //alert("text is" +text);
	 if(text=="Credentials not Found, Please login and try again.." || text=="Sorry, You dont have access to view this page.."){
		document.getElementById("errorMessage").innerHTML =text;
		document.getElementById("errorMessage").style.display="block";
	 	alert(text);
	 }else if(text == "DateProblem"){
		 document.getElementById("errorMessage").innerHTML ="All Fields are Mandatory";
		 document.getElementById("errorMessage").style.display="block";
		 alert("All fields are mandatory");
	 }else if(text == "NoData"){
		 document.getElementById("errorMessage").innerHTML ="No data found";
		 document.getElementById("errorMessage").style.display="block";
		 alert("No data found");
	 }else{
		 var jsonobj = "{\"details\":"+text+"}" ;
		 var obj = JSON.parse(jsonobj.toString());
		 var tbl;
		 	     $(document).ready(function(){
		         $("#ORHTableEdit").html(" ");
				 $("#ORHTableEdit").append('<table border="1" cellspacing="1" cellpadding="1" align="center"  bgcolor="#FFFFFF" id="CustomerDetails">');
				 $("#CustomerDetails").append('<thead bgcolor="#00FFFF"><tr>'+
				 '<th width="10%">TripId</th>'+
				 '<th width="20%">Start Add.</th>'+
				 '<th width="30%">End Add.</th>'+
				 '<th width="13%">Start Time</th>'+
				 '<th width="13%">End Time</th>'+
				 '<th width="8%">OPTION</th></tr></thead>');
		         });
		        for(var i=0;i <obj.details.length;i++)
				{
			 	 tbl=$('<tr align="center"/>');
				 tbl.append('<td align="center" bgcolor="#FFFFFF"><input type="text"   align="center" id="tripId_'+i+'"   value="'+obj.details[i].TI +'" readonly></td>');				
				 tbl.append('<td align="center" bgcolor="#FFFFFF"><textarea rows="2" cols="20" wrap="soft" align="center" id="SA_'+i+'"    style="width:90%"  readonly>'+obj.details[i].SA + '</textarea></td>');
				 tbl.append('<td align="center" bgcolor="#FFFFFF"><textarea rows="2" cols="20" wrap="soft" align="left" id="EA_'+i+'" class="ui-autocomplete-input" style="width:68%; height:90%" readonly> '+obj.details[i].EA +' </textarea> <input type="button" align="right" id="EditEA_'+i+'"  style="width:20%" value="Edit" onclick="editDrpoOff('+i+')">'
				 +'<input type="hidden" name="EA_Lat_'+i+'" id="EA_Lat_'+i+'" value="" />'
				 +'<input type="hidden" name="EA_Lon_'+i+'" id="EA_Lon_'+i+'" value="" />'
					+'<input type="hidden" name="EA_add1_'+i+'" id="EA_add1_'+i+'" value="" />'
					+'<input type="hidden" name="EA_add2_'+i+'" id="EA_add2_'+i+'" value="" />'
					+'<input type="hidden" name="EA_Post_'+i+'" id="EA_Post_'+i+'" value="" />'
					+'<input type="hidden" name="EA_city_'+i+'" id="EA_City_'+i+'" value="" />'
				 +'<input type="hidden" name="PEA_'+i+'" id="PEA_'+i+'" value="'+obj.details[i].EA +'" />'
				 +'</td>');
				 //class="ui-autocomplete-input"
				 //tbl.append('<td align="center" bgcolor="#FFFFFF"><input type="button" align="center" id="EditEA_'+i+'"  style="width:50%" value="Edit" onclick="editDrpoOff('+i+')"></td>');
	 			 tbl.append('<td align="center" bgcolor="#FFFFFF"><input type="text"   align="center" id="stTime_'+i+'" style="width:90%" value="'+obj.details[i].CRTIME +'"readonly></td>');
				 tbl.append('<td align="center" bgcolor="#FFFFFF"><input type="text"   align="center" id="edTime_'+i+'" style="width:90%" value="' +obj.details[i].COTIME +'"readonly></td>');
				 tbl.append('<td align="center" bgcolor="#FFFFFF"><input type="button" align="center" id="Edit_'+i+'"  style="width:90%" value="Edit" onclick="editData('+i+')"></td>');
				 $('#CustomerDetails').append(tbl);
				}
      }
	}catch(err){
		document.getElementById("errorMessage").innerHTML ="Server not connected";
		document.getElementById("errorMessage").style.display="block";
		alert("Server not connected");
	}
}

function editData(i){
	document.getElementById("errorMessage").innerHTML ="";
	   var xmlhttp;
	     if(window.XMLHttpRequest) 
		    {
		      xmlhttp = new XMLHttpRequest();
		    }
	     else
		    {
		      xmlhttp = new regexActiveXObject("Microsoft.XMLHTTP");
		    }
	    var editSave = document.getElementById('Edit_'+i);
	    var tripId = document.getElementById('tripId_'+i);
	    var stTime = document.getElementById('stTime_'+i);
	    var edTime = document.getElementById('edTime_'+i);
	    if(editSave.value == "Edit"){
	    	editSave.value="Update";
	        stTime.removeAttribute('readonly',false);
	        edTime.removeAttribute('readonly',false);
	    } else{
	    	try{
		    	var url='EditJobs?event=updateDetails&tripId='+tripId.value+'&stdate='+stTime.value+'&eddate='+edTime.value;
		   	 	//alert("url is"  +url);
		   	 	xmlhttp.open("GET",url,false);
		   	 	xmlhttp.send(null);
		   	 	var text = xmlhttp.responseText;
		   	 	if(text=="Credentials not Found, Please login and try again.." || text=="Sorry, You dont have access to view this page.."){
		   	 		document.getElementById("ORHTableEdit").innerHTML ="";
		   	 		document.getElementById("errorMessage").innerHTML =text;
					document.getElementById("errorMessage").style.display="block";
		   	 		alert(text);
		   	 	}else if(text=="Done"){
		   	 		editSave.value="Edit";
		   	 		stTime.readOnly= true;
		    		edTime.readOnly= true;
		    		document.getElementById("errorMessage").innerHTML ="Time Successfully updated";
				 	document.getElementById("errorMessage").style.display="block";
		   	 	}else if(text=="Failed"){
		   	 		document.getElementById("errorMessage").innerHTML ="Failed to update. Please check the date format YYYY-mm-dd HH:mm:ss";
				 	document.getElementById("errorMessage").style.display="block";
				 	alert("Failed to update. Please check the date format YYYY-mm-dd HH:mm:ss");
		   	 	}else{
		   	 		document.getElementById("errorMessage").innerHTML ="All Fields are mandatory";
			 		document.getElementById("errorMessage").style.display="block";
			 		alert("All fields are mandatory");
		   	 	}
	    	}catch(err){
	    		document.getElementById("errorMessage").innerHTML ="Server not connected";
	    		document.getElementById("errorMessage").style.display="block";
	    		alert("Server not connected");
	    	}
	    }
}

function editDrpoOff(i){
	document.getElementById("errorMessage").innerHTML ="";
	   var xmlhttp;
	     if(window.XMLHttpRequest) 
		    {
		      xmlhttp = new XMLHttpRequest();
		    }
	     else
		    {
		      xmlhttp = new regexActiveXObject("Microsoft.XMLHTTP");
		    }
	    var editSave = document.getElementById('EditEA_'+i);
	    var tripId = document.getElementById('tripId_'+i);
	    //alert(tripId.value);
	    
	    var dropAdd = document.getElementById('EA_'+i);
	    
	    if(editSave.value == "Edit"){
	    	editSave.value="Update";
	    	dropAdd.removeAttribute('readonly',false);
	    	initializeGeocode(i);
	    } else{
	    	
	    	dropAdd.readOnly= true;
    		editSave.value="Edit";
    		
	    	var elat = document.getElementById('EA_Lat_'+i);
	    	var elon = document.getElementById('EA_Lon_'+i);
	    	var pdropAdd = document.getElementById('PEA_'+i);
	    	
	    	var eadd1 = document.getElementById('EA_add1_'+i);
	    	var eadd2 = document.getElementById('EA_add2_'+i);
	    	var epost = document.getElementById('EA_Post_'+i);
	    	var ecity = document.getElementById('EA_City_'+i);
	    	
	    	if(dropAdd.value == "" || elat.value == "" || elon.value == ""){
	    		alert("Autocomplete's returned place contains no geometry");
	    		eadd1.value="";
	    		eadd2.value="";
	    		ecity.value="";
	    		epost.value="";
	    		elat.value="";
	    		elon.value="";
	    		dropAdd.value=pdropAdd.value;
	    		return;
	    	}
	    	try{
	    		//alert(dropAdd.value);
		    	var url='EditJobs?event=updateEA&tripId='+tripId.value+'&eadd1='+eadd1.value+'&eadd2='+eadd2.value+'&ecity='+ecity.value+'&epost='+epost.value+'&elati='+elat.value+'&elong='+elon.value;
		   	 	//alert("url is"  +url);
		   	 	xmlhttp.open("GET",url,false);
		   	 	xmlhttp.send(null);
		   	 	var text = xmlhttp.responseText;
		   	 	if(text=="Credentials not Found, Please login and try again.." || text=="Sorry, You dont have access to view this page.."){
		   	 		document.getElementById("ORHTableEdit").innerHTML ="";
		   	 		document.getElementById("errorMessage").innerHTML =text;
					document.getElementById("errorMessage").style.display="block";
		   	 		alert(text);
		   	 	}else if(text=="Done"){
		   	 		editSave.value="Edit";
		    		document.getElementById("errorMessage").innerHTML ="End Address Successfully updated";
				 	document.getElementById("errorMessage").style.display="block";
		   	 	}else if(text=="Failed"){
		   	 		document.getElementById("errorMessage").innerHTML ="Failed to update";
				 	document.getElementById("errorMessage").style.display="block";
				 	alert("Failed to update.");
		   	 	}else{
		   	 		document.getElementById("errorMessage").innerHTML ="All Fields are mandatory";
			 		document.getElementById("errorMessage").style.display="block";
			 		alert("All fields are mandatory");
		   	 	}
	    	}catch(err){
	    		document.getElementById("errorMessage").innerHTML ="Server not connected";
	    		document.getElementById("errorMessage").style.display="block";
	    		alert("Server not connected");
	    	}
	    }
}

</script>

<style type="text/css">
#LongDesc em{
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
	}
	
	#LongDesc1 em{
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
	}
	
	.hover em{
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
	}
	.pac-container{
	border: 1px solid #C77405;
	background-color: #FFFFFF;
	color: #C77405;
	font-weight: bold;
	outline: medium none;
	width: 500px !important;
	}	
	.ui-state-hover,.ui-widget-content .ui-state-hover,.ui-state-focus,.ui-widget-content .ui-state-focus
	{
	border: 1px solid #C77405;
	background-color: #FFFFFF;
	color: #C77405;
	font-weight: bold;
	outline: medium none;
	}	
	
      .gmap3{
        margin: 20px auto;
        border: 1px dashed #C0C0C0;
        width: 1024px;
        height: 768px;
      }
      
 #confirmBox
{
    display: none;
    background-color: #eee;
    border-radius: 5px;
    border: 1px solid #aaa;
    position: fixed;
    width: 600px;
    left: 50%;
    margin-left: -150px;
    margin-top: -500px;
    padding: 6px 8px 8px;
    box-sizing: border-box;
    text-align: center;
}
.amountForCharge{
	text-align: right;
	font-weight: bold;
}
.ccButtons{
	-moz-border-radius:8px 8px 8px 8px;
	background-color: #FDD017;
	color: #342826;
	width: 40px;
	height: 40px;
	font-size: large;
}
.ccButtonsAct{
	-moz-border-radius:8px 8px 8px 8px;
	background-color: #008000;
	color: #FFFFFF;	
	width: 40px;
	height: 40px;
	font-size: large;
}
.ccButtonsPkd{
	-moz-border-radius:8px 8px 8px 8px;
	background-color: #0000FF;
	color: #FFFFFF;
	width: 40px;
	height: 40px;
	font-size: large;
}
#confirmBox .button {
    background-color: #ccc;
    display: inline-block;
    border-radius: 3px;
    border: 1px solid #aaa;
    padding: 2px;
    text-align: center;
    width: 160px;
    cursor: pointer;
}
#confirmBox .button:hover
{
    background-color: #ddd;
}
#confirmBox .message
{
    text-align: left;
    margin-bottom: 8px;
}
#confirmBoxJobs
{
    display: none;
    background-color: #eee;
    border-radius: 5px;
    border: 1px solid #aaa;
    position: fixed;
    width: 600px;
    left: 50%;
    margin-left: -150px;
    margin-top: -500px;
    padding: 6px 8px 8px;
    box-sizing: border-box;
    text-align: center;
}
#confirmBoxJobs .button {
    background-color: #ccc;
    display: inline-block;
    border-radius: 3px;
    border: 1px solid #aaa;
    padding: 2px;
    text-align: center;
    width: 160px;
    cursor: pointer;
}
#confirmBoxJobs .button:hover
{
    background-color: #ddd;
}
#confirmBoxJobs .message
{
    text-align: left;
    margin-bottom: 8px;
}
label {
	font-size: medium;
	color: maroon;
}

#page-header{
	background-color: aqua;
	}

 </style>

</head>
<body>
<input type="hidden" name="addressCheck" id="addressCheck" value="<%=adminBO.getAddressFill() %>" />
<!-- <input type="hidden" name="addressCheck" id="addressCheck" value="2" /> -->
<input type="hidden" name="defaultState" id="defaultState" value="<%=adminBO.getState() %>">
<input type="hidden" name="defaultCountry" id="defaultCountry" value="<%=adminBO.getCountry() %>">
<input type="hidden" name="defaultLatitude" id="defaultLatitude" value="<%=adminBO.getDefaultLati() %>">
<input type="hidden" name="defaultLongitude" id="defaultLongitude" value="<%=adminBO.getDefaultLogi() %>">
<div>
<h2><center>Edit History Jobs</center> </h2>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
<tr>
<td>
<table style="width: 80%;" align="center" id="bodypage" >
<tr align="center">
<td>Voucher No:&nbsp;&nbsp;
	<input type="text" autocomplete="off" name="voucherNo1" id="voucherNo1" value="<%=voucherNo %>" /> 
          <ajax:autocomplete 
  					fieldId="voucherNo1"
  					popupId="model-popup1"
  					targetId="voucherNo1"
  					baseUrl="autocomplete.view"
  					paramName="getAccountList"
  					className="autocomplete"
  					progressStyle="throbbing" />
  	<%-- <input type="hidden" name="vno" id="vno" value="<%=voucherNo%>"/> --%>
</td>
<td><font color="black" style="">Select Date:&nbsp;&nbsp;</font>
	<input type="text" name="from_date" id="from_date" value="" onfocus="showCalendarControl(this);" />
</td>
<td><font color="black">To Date:&nbsp;&nbsp;</font>
	<input type="text" name="toDateHis" id="toDateHis" value="" onfocus="showCalendarControl(this);" /></td>
<td><input type="button" id="Search" value="Search" onclick="getHistoryTrips()"/> </td>
</tr>
</table>
</td>
</tr>
</table>
</div>
<br/>
<br/>
<div id ="errorMessage" style="display: none; color:#E41B17;size: 20px;font-weight: bold; width: 80%;"></div>
<br/>
<br/>

<div id ="userMessage" style="color:#E41B17;size: 20px;font-weight: bold; width: 80%;"></div>
<table style="width: 80%;" align="center" id="MessageTable" >
<tr><td>** Date Format Should be as YYYY-MM-DD HH:MM:SS </td></tr>
</table>
<br/>
<br/>

<div id = "ORHTableEdit" style="width: 100%" align="center">
<table id="CustomerDetails" style="background-color:white;" align="center"></table>
</div>
<div id="map_canvas" style="display: none; width:50%; height:600px; margin-left: 47%;margin-top:-600px;overflow: inherit;z-index: 100">
    </div>
</body>
</html>