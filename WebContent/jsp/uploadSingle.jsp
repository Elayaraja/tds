<%@ page import="com.tds.constants.docLib.*" %>
<%
	String ctxPath = request.getContextPath();
	
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<link rel="stylesheet" type="text/css" href="<%=ctxPath %>/css/document.css" media="all">


<title>Document Library</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->

<link rel="stylesheet" href="<%=ctxPath %>/css/document/docStyle.css" type="text/css" />

<link href="<%=ctxPath %>/js/uploadify/uploadify.css" type="text/css"
	rel="stylesheet" />

<script type="text/javascript"
	src="<%=ctxPath %>/js/uploadify/jquery-1.4.2.min.js"></script>

<script type="text/javascript"
	src="<%=ctxPath %>/js/uploadify/swfobject.js"></script>

<script type="text/javascript"
	src="<%=ctxPath %>/js/uploadify/jquery.uploadify.v2.1.4.min.js"></script>

<script type="text/javascript">


function cancelUpload(){

	var compId = document.getElementById("compId").value;
	var userId = document.getElementById("userId").value;
	document.getElementById("upload").action="<%=ctxPath %>/control?action=TDS_DOCUMENT_UPLOAD&event=passKeyGen&module=operationView&do=disp&compId="+compId+"&userId="+userId;
	document.getElementById("upload").submit();
}

	$(document).ready(function() {
	var cmpId= "<%=request.getAttribute("compId") %>";
	var docId= "<%=request.getAttribute("docId") %>";
	var userId= "<%=request.getAttribute("userId") %>";
	var maxSize = "<%=DocumentConstant.MAX_SIZE %>";
	var fileType = "<%=DocumentConstant.FILETYPE %>";
	
		$('#file_upload').uploadify( {

			'uploader' : '<%=ctxPath %>/js/uploadify/uploadify.swf',
			'script' : '<%=ctxPath %>/uploadDocs?do=uploadAlternate',
			'cancelImg' : '<%=ctxPath %>/js/uploadify/cancel.png',
			'folder' : '<%=ctxPath %>/tmp',
			'multi' : false,
			'type' : 'POST',
			'fileExt'     : fileType,
			'fileDesc'    : 'TDS Files',
			'sizeLimit'   : maxSize,
			'scriptData' : {'compId': cmpId ,'docId': docId,'userId':userId},
			'removeComplted'	:	true,
			onAllComplete : function(evt,queueId,fileObj,response,data){
						alert("Document uploaded successfully.");
						cancelUpload();
				}
	
		

		});

	});
</script>



</head>
<body>

<form name="upload" id="upload" action="uploadDocs?compCode=12&do=disp&role=USER" method="post"
	enctype="multipart/form-data">

	
<input type="hidden" name="compId" id="compId"  value='<%=request.getAttribute("compId") %>'/>
<input type="hidden" name="userId" id="userId"  value='<%=request.getAttribute("userId") %>'/>	
	
<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<h1>Upload Alternative Document for Expired</h1>
<table  width="75%" >
<tr>
<td>


<table id="documentListasdf" class="tablesorter"   width="75%" > 
<thead>
	<TR>
		<th colspan="2" >Upload Documents</th>
		
	</TR>
	
</thead>	



<tr>
	<td width="50%">
		<div style="overflow: auto;height=400px;vertical-align: bottom;">
		
		<input id="file_upload" name="file_upload" type="file" />
		
		</div>
	</td>
	<td >
			<B>Instructions</B><br>
			1.	Upload alternative document for <%=request.getAttribute("docType") %> - <%=request.getAttribute("docNum") %></BR> 
			2.	Maximum <%=DocumentConstant.MAX_SIZE_DESC %> can upload.</BR>
			3.	Allowed File extension -= <%=DocumentConstant.FILETYPE %></BR>
			4.	UPloaded document requires 2 to 3 
				business day to approve by portal admin.</BR>
		</BR>	
			</BR>
				<div align="center">
		<input type="hidden" value="<%=request.getAttribute("docId") %>" name="hDocId" id="hDocId" />	
		<input type="button" name="Upload" value="Upload"
			onclick="javascript:$('#file_upload').uploadifyUpload()" />
		<input type="button" name="Cancel" value="Cancel" onclick="javascript:cancelUpload()"/>			
		</div>
	</td>
</tr>

</table>
</td>
</tr> 
</table>
 </div>    
              </div>

</form>

            	
                <div class="clrBth"></div>
          
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
</body>
</html>

