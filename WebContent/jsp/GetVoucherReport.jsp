<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.tdsBO.AdminRegistrationBO;"%>
<html>
<head>
<link href="css/newtdsstyles.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function callReport(action){
//alert("callReport");
var acode= document.getElementById("acode").value;
//alert(acode);
var fromValue ="";
var toValue = "";
var fromdate=document.getElementById("from").value;
var todate=document.getElementById("to").value;
if(fromdate != ""){
fromValue = fromdate.substring(6,10)+fromdate.substring(0,2)+fromdate.substring(3,5);
}

if(todate != ""){
toValue =  todate.substring(6,10)+todate.substring(0,2)+todate.substring(3,5);
}
 
 
var stime = document.getElementById("shh").value+document.getElementById("smm").value;
var etime = document.getElementById("ehh").value+document.getElementById("emm").value;
var query="";
if(fromValue!="" && toValue !=""){
query+=" AND CONCAT(SUBSTRING(CR_STTIME,1,4),SUBSTRING(CR_STTIME,6,2),SUBSTRING(CR_STTIME,9,2))>='"+fromValue+"' AND CONCAT(SUBSTRING(CR_STTIME,1,4),SUBSTRING(CR_STTIME,6,2),SUBSTRING(CR_STTIME,9,2))<='"+toValue+"'";
//alert(query);
}
if(stime!="" && etime!=""){
query+=" AND CONCAT(SUBSTRING(CR_STTIME,12,2),SUBSTRING(CR_STTIME,15,2))>='"+stime+"' AND CONCAT(SUBSTRING(CR_EDTIME,12,2),SUBSTRING(CR_EDTIME,15,2)) <='"+etime+"'";
//alert(query);
}
if(fromValue=="" && toValue != ""){
query+="AND CONCAT(SUBSTRING(CR_STTIME,1,4),SUBSTRING(CR_STTIME,6,2),SUBSTRING(CR_STTIME,9,2))='"+fromValue+"'";
//alert(query);
}
if(fromValue!="" && toValue == ""){
query+=" AND CONCAT(SUBSTRING(CR_STTIME,1,4),SUBSTRING(CR_STTIME,6,2),SUBSTRING(CR_STTIME,9,2))='"+toValue+"'";
//alert(query);
}
if(stime=="" && etime!=""){
query+=" AND CONCAT(SUBSTRING(CR_EDTIME,12,2),SUBSTRING(CR_EDTIME,15,2)) ='"+etime+"'";
//alert(query);
}
if(stime!="" && etime==""){
query+=" AND CONCAT(SUBSTRING(CR_STTIME,12,2),SUBSTRING(CR_STTIME,15,2)) ='"+stime+"'";
//alert(query);
}
if(acode !=""){
query+=" AND CR_ASSOCCODE ='"+acode+"'";
//alert(query);
}
//alert(query);
var hid=document.getElementById("context");
var stlink=hid.value+'/frameset?__report=BIRTReport/Voucher_report.rptdesign&__format=pdf&query='+query;
//alert(fromdate+""+todate);
//alert(stlink);
//action.href = stlink;
document.masterForm.action = stlink
document.masterForm.submit();
}

function calltime(id){
//alert("calltime");
if(id.value < 10 || id.value == 0){
	if(id.value.length < 2){
		id.value = "0"+id.value;
		}
}
}
</script>
</head>
<body>
<form name="masterForm" action="control" method="post">
<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<table id="pagebox">
	<tr>
		<td>
			<table id="bodypage">
			<tr>
				<td colspan="4" align="center">
					<div id="title" >
						<h2>Get&nbsp;Voucher&nbsp;Report</h2>
					</div>
				</td>
			</tr>
			<tr>
				
				<td>Start&nbsp;Date</td>
				<td><input type="text" name="fdate" id="from" size="10"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.masterForm.fdate);return false;" HIDEFOCUS>
				<img name="popcal" align="absmiddle" src="images/calendar.gif" width="25" height="16" border="0" alt=""></a>		
				<iframe width="20"  height="178"  name="gToday:normal:agenda.jsfdate" id="gToday:normal:agenda.jsfdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
				</iframe></td>
				<td>End&nbsp;Date</td>
				<td><input type="text" name="tdate" id="to"  size="10" />
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.masterForm.tdate);return false;" HIDEFOCUS>
				<img name="popcal" align="absmiddle" src="images/calendar.gif" width="25" height="16" border="0" alt=""></a>		
				<iframe width=20  height=178  name="gToday:normal:agenda.jstdate" id="gToday:normal:agenda.jstdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
				</iframe></td>
			</tr>
			<tr>
				<td>Start&nbsp;Time&nbsp;</td>
				<td>HH<input type="text" name="shh" id="shh" size="2" onblur="calltime(this)"/>&nbsp;MM<input type="text" name="smm" id="smm" size="2" onblur="calltime(this)"/></td>
				<td>End&nbsp;Time&nbsp;</td>
				<td>HH<input type="text" name="ehh" id="ehh" size="2" onblur="calltime(this)"/>&nbsp;MM<input type="text" name="emm" id="emm" size="2" onblur="calltime(this)"/></td>
			</tr>	
			<%
			String acode = "";
			if(session.getAttribute("user")!=null){
				AdminRegistrationBO adminBo= (AdminRegistrationBO)session.getAttribute("user");
				acode=adminBo.getAssociateCode();
				System.out.println("From Jsp acode ="+acode);
			}
			
			%>
			<tr>
				<td colspan="4" align="center">
				<input type="hidden" name="acode" id="acode" value="<%=acode%>">
				<input type="submit" name="submit" value="Search" onclick="callReport(this)"/></td>
				
			</tr>			
			</table>
		</td>
	</tr>
</table>
</form>
</body>
</html>