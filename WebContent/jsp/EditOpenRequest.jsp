<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>
 
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%><html>
<head>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
 <link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS (Taxi Dispatch System)</title>
<script language="javascript">


function setPhone(p_phone) {
	if(p_phone.value.length == 3) {
		p_phone.value += "-";
	} else if(p_phone.value.length == 7) {
		p_phone.value += "-";
	}
}
</script>
</head>
<%
String status[] = {"Request","In Process","Completed"};
%>
<body>
<form name="masterForm" action="control" method="post">
<jsp:useBean id="openBO"   class="com.tds.tdsBO.OpenRequestBO"  />
<% if(request.getAttribute("openrequest") != null) {
		openBO = (OpenRequestBO)request.getAttribute("openrequest");
	%>
		<script type="text/javascript">
			document.masterForm.sdate.value = "";
			document.masterForm.shrs.value = "";
			document.masterForm.smin.value = "";
			alert("Hello");
		</script>
	<% } %>
<jsp:setProperty name="openBO"  property="*"/>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveOpenRequest %>">
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");

%>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="stlatitude" value="<%= openBO.getSlat() %>"> 
<input type="hidden" name="stlongitude" value="<%= openBO.getSlong() %>">
<input type="hidden" name="edlatitude" value="<%= openBO.getEdlatitude() %>">
<input type="hidden" name="edlongitude" value="<%= openBO.getEdlongitude() %>">
<input type="hidden" name="tripid" value="<%= openBO.getTripid() %>">
 
<input type="hidden" name="sesexit" id="sesexit" value="<%=adminBo==null?"0":"1"%>">

 
            <div class="leftCol">
                <div class="clrBth"></div>
                </div>  
            	              
                <div class="rightCal">
                <div class="rightColIn">
                            		 
                        		<%
									String error ="";
									if(request.getAttribute("errors") != null) {
									error = (String) request.getAttribute("errors");
									}
								%>
								<div id="errorpage">
								<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %></div>
								
								<%String result="";
								if(request.getAttribute("page")!=null) {
									result = (String)request.getAttribute("page");
								}
								%>
								<div id="errorpage">
								<%=result.length()>0?"Sucessfully"+result:"" %>
								</div>
								<h1>Open Request</h1>
                                		
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<td>
					<table id="bodypage" width="100%" >
						<tr>
							<td class="firstCol" >Phone No</td>
							<td><input type="text"   name="phone"  id="phone" value="<%= openBO.getPhone()  %>" onkeyup="setPhone(this)"/>
							 <input type="button" name="btnphone" class="lft" value="CkPhNo" tabindex="2" onclick="loadFromToAddr()"/>
							</td>
							<td class="firstCol" >Name</td>
							<td><input type="text"   name="name"  value="<%= openBO.getName() %>" /></td> 
						</tr>
						<tr>
							
							<td class="firstCol" >Service Date  </td>
							<td><input readonly="readonly" type="text"   name="sdate"   size="10"  value="<%= openBO.getSdate()  %>" onfocus="showCalendarControl(this);"/></td>
							<td class="firstCol" >Service   Time</td>
							<td><input type="text"   name="shrs"   maxlength="4"  size="4" value="<%= openBO.getShrs()  %>" />Hrs</td>
						</tr> 
 					</table  >
 					</td>
 					</tr> 
 					</table>
 					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
 					<tr>
 					<td>
 					<table  id="bodypage" width="800" align="left" >
 						
 						<tr       >
 						<td colspan="3"    style="background-color:#FCD374" ><b>From Address</b></td>
 						<td colspan="3"  style="background-color:#FCD374"  ><b>To Address</b></td>
 						
 						 
 						</tr>
 						<tr>
 							<td  class="firstCol">Intersection</td>
 							<td><input type="text" name="sintersection"   id="sintersection"   value="<%= openBO.getSintersection()  %>"/></td>
 							<td></td>
 							<td></td>
 							<td></td>
 							<td></td>
 						</tr>
 						<tr>
 							<td  class="firstCol">Land Mark</td>
 							<td><input type="text" name="slandmark"   id="slandmark"   value="<%= openBO.getSlandmark()  %>"/></td>
 							<td></td>
 							<td></td>
 							<td></td>
 							<td></td>
 						</tr>
 					<%-- 	<tr>
 							<td   class="firstCol">Air Port Code</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                 	<input type="text" tabindex="9" id="saircodeid" name="saircodeid"   value="<%= openBO.getSaircodeid() %>" autocomplete="off"   class="form-autocomplete"/>
                                    <input type="hidden" id ="saircode" name = "saircode" value="<%= openBO.getSaircode() %>"/>
                                    <ajax:autocomplete
  									fieldId="saircodeid"
  									popupId="model-popup1"
  									targetId="saircode"
  									baseUrl="autocomplete.view"
  									paramName="aircode"
  									className="autocomplete"
  									progressStyle="throbbing"/>
  							 		</div></div></div></td>
 							<td> <div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btnsaircode" class="lft"   value="Check Source Aircode"   />
                                         </div>
                                        </div>
                                    </div></td>
 							<td class="firstCol" >To Air Port Code</td>
 							<td>
 								 <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" tabindex="16" id="eaircodeid" name="eaircodeid" class="inputBX" value="<%= openBO.getEaircodeid() %>" autocomplete="off"   class="form-autocomplete"/>
									<input type="hidden" id ="eaircode" name = "eaircode" value="<%= openBO.getEaircode() %>"/>
									<ajax:autocomplete
  									fieldId="eaircodeid"
  									popupId="model-popup2"
  									targetId="eaircode"
  									baseUrl="autocomplete.view"
  									paramName="aircode"
  									className="autocomplete"
  									progressStyle="throbbing" />
									</div></div></div>
 							</td>
 							<td> <div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btneaircode" class="lft" value="Check Destination Aircode" tabindex="17" />
                                        </div>
                                        </div>
                                    </div></td>
 						</tr> --%>
 						<tr>
 							<td  class="firstCol">Address 1</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text"   id="sadd1" name="sadd1"   value="<%=openBO.getSadd1()  %>" class="form-autocomplete" onblur="loadOtherData()" />
                                    <ajax:autocomplete
				  					fieldId="sadd1"
  									popupId="model-popupsadd1"
  									targetId="sadd1"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress1"
  									className="autocomplete"
  									progressStyle="throbbing" /> 
  			                         </div></div></div></td>
  			                         <td></td>
 							<td class="firstCol">Address 1</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text"   name="eadd1"   id="eadd1" value="<%= openBO.getEadd1()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"/>
									<ajax:autocomplete
  									fieldId="eadd1"
  									popupId="model-popupeadd1"
  									targetId="eadd1"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress1"
  									className="autocomplete"
  									progressStyle="throbbing"/>
                                    </div></div></div></td>
 							<td></td>
 							
 						</tr>
 						<tr>
 							<td   class="firstCol"> Address 2</td>
 							<td> <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="sadd2"   id="sadd2"    value="<%= openBO.getSadd2()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()"/>
									<ajax:autocomplete
  									fieldId="sadd2"
  									popupId="model-popupsadd2"
  									targetId="sadd2"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress2"
  									className="autocomplete"
  									progressStyle="throbbing"/>
		                            </div></div></div></td>
		                            <td></td>
 							<td class="firstCol">Address 2</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="eadd2"   id="eadd2"    value="<%= openBO.getEadd2()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"/>
									<ajax:autocomplete
  									fieldId="eadd2"
  									popupId="model-popupeadd2"
  									targetId="eadd2"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress2"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div></td>
 							<td></td>
 							 
 						</tr>
 						<tr>
 							<td  class="firstCol">City</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="scity"   id="scity"   value="<%= openBO.getScity()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()"/>
									<ajax:autocomplete
  									fieldId="scity"
  									popupId="model-popupscity"
  									targetId="scity"
  									baseUrl="autocomplete.view"
  									paramName="phonecity"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
  									</div></div></div></td>
  									<td></td>
 							<td class="firstCol">City</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="ecity"   id="ecity"   value="<%= openBO.getEcity()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"/>
									<ajax:autocomplete
  									fieldId="ecity"
  									popupId="model-popupecity"
  									targetId="ecity"
  									baseUrl="autocomplete.view"
  									paramName="phonecity"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div></td>
 							<td></td>
 							 
 						</tr> 
 						<tr>
 							<td  class="firstCol" >State</td>
 							<td> <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="sstate"   id="sstate"   value="<%= openBO.getSstate()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()"/>
									<ajax:autocomplete
  									fieldId="sstate"
  									popupId="model-popupsstate"
  									targetId="sstate"
  									baseUrl="autocomplete.view"
  									paramName="phonestate"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
                                   	 </div></div></div></td>
                                   	 <td></td>
 							<td class="firstCol">State</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="estate"    id="estate"   value="<%= openBO.getEstate()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"/>
									<ajax:autocomplete
  									fieldId="estate"
  									popupId="model-popupestate"
  									targetId="estate"
  									baseUrl="autocomplete.view"
  									paramName="phonestate"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div></td>
 							<td></td>
 							 
 						</tr>
 						<tr>
 							<td  class="firstCol">Zip</td>
 							<td> <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="szip"   id="szip"   value="<%= openBO.getSzip()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()"/>
									<ajax:autocomplete
  									fieldId="szip"
  									popupId="model-popupszip"
  									targetId="szip"
  									baseUrl="autocomplete.view"
  									paramName="phonezip"
  									className="autocomplete"
  									progressStyle="throbbing"/>
  									</div></div></div>
 									<% if(session.getAttribute("user") != null) {  %>
									<%
										ArrayList al_q = (ArrayList)request.getAttribute("al_q");
									%></td>
									
									<td><div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btnfromaddress" class="lft" value="Check From Address"   />
                                         </div>
                                        </div>
                                    </div></td>  
 							<td class="firstCol">Zip</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="ezip" id="ezip" tabindex="22" class="inputBX" value="<%=openBO.getEzip()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"/>
									<ajax:autocomplete
  									fieldId="ezip"
  									popupId="model-popupezip"
  									targetId="ezip"
  									baseUrl="autocomplete.view"
  									paramName="phonezip"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div></td>
 							
 							
 						</tr>
 						<tr>
 							<td  class="firstCol">Zone</td>
 							<td><div class="div5"><div class="div5In">
	 								<select name='queueno' id='queueno'  >
	 								<option value=''>Select</option>
									<%
										if(al_q!=null){
										for(int i=0;i<al_q.size();i=i+2){ 
									%>
										<option value='<%=al_q.get(i).toString() %>' <%=openBO.getQueueno().equals(al_q.get(i).toString())?"selected":"" %>><%=al_q.get(i+1).toString() %></option>
									<%
									}
									} %>
									</select>
									</div></div> 
									<% } else{  %> 
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap">
                                    <input type="hidden" name="queueno" id="queueno" value="<%=openBO.getQueueno() %>"/></div></div></div>
                                    <%} %></td>
 							<td><div class="div3">
									<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="button" name="getzone" class="lft" value="Get Zone" tabindex="" onclick='loadQueue()' /></div>
                                        </div>
                                    </div></td>
 							<td></td>
 							<td></td>
 							<td><div class="div3">
                                    	<div class="btnBlueNew1 padT10 ">
                                         <div class="rht">
                                         <input type="submit" name="btntoaddress" class="lft" value="Check To Address"  />
                                         </div>
                                    </div></div></td>
 						</tr>
 						 
 							<tr>
 							<td></td>
 							<td></td>
							<td  align="right" ><input type="submit" name="submit" value="Update" > </td>
							<td><input type="submit" name="delete" value="Delete Record"></td>
							<td></td>
							<td></td>  
			</tr>   
 						 
 					</table> 
    
					</td>
					</tr>
					</table>
    
                                    
                                    </div>    
                                
                        	</div>
                      
                </div>    
              </div>
            	
                <div class="clrBth"></div>
         
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
           
 
</form>
</body>
</html>
