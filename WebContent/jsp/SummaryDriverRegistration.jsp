<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="java.util.ArrayList,com.tds.tdsBO.DriverRegistrationBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%
	String ctxPath = request.getContextPath();
	
%>
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

<style type="text/css" title="currentStyle">
.txtarea{
    max-width:100%;
    min-height:200px;    
    display:block;
    width:100%;
}

.mydiv{
    padding:10px;
}

.gen_btn{
    padding:5px;
    background-color:#743ED9;
    color:white;
    font-family:arial;
    font-size:13px;
    border:2px solid black;
}

.gen_btn:hover{
    background-color:#9a64ff;
}

	@import "dataTable/css/demo_page.css";
	@import "dataTable/css/demo_table_jui.css";
	@import "dataTable/css/jquery-ui-1.8.4.custom.css";
	@import "dataTable/css/TableTools_JUI.css";
</style>
  
  <script type="text/javascript" charset="utf-8"src="dataTable/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8"src="dataTable/js/ZeroClipboard.js"></script>
	<script type="text/javascript" charset="utf-8"src="dataTable/js/TableTools.js"></script>
	<script type="text/javascript" src="js/markerwithlabel.js"></script>
	<script type="text/javascript" src="js/Util.js"></script>
  

<script type="text/javascript" src="js/jqModal.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>
<script type="text/javascript" src="<%=ctxPath %>/js/document/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<%=ctxPath %>/js/document/jquery.tablesorter.pager.js"></script>

<%! int startValue =1 ;double limit=0;int pstart=1;int pend=0;int pageNo=1;int plength=0; 
String first="";
String last="";
String driver="";
String cab="";

%>
<%if(request.getAttribute("startValue") != null){
	startValue = Integer.parseInt((String)request.getAttribute("startValue"));
	startValue=startValue+1;
}	
if(request.getAttribute("plength")!=null){
	plength=Integer.parseInt((String)request.getAttribute("plength"));
	}

if(request.getAttribute("f_name")!=null){
	first= (String)request.getAttribute("f_name");
	}
if(request.getAttribute("l_name")!=null){
	last= (String)request.getAttribute("l_name");
	}
if(request.getAttribute("driver_id")!=null){
	driver= (String)request.getAttribute("driver_id");
	}
if(request.getAttribute("cab_no")!=null){
	cab= (String)request.getAttribute("cab_no");
	}
String status="";
if(request.getAttribute("status")!=null){
	status= (String)request.getAttribute("status");
	}

%>
<script type="text/javascript">
function showFleetDiv(row){
	$("#multiFleet_"+row).show('slow');
}
function changeFleetForDriver(driverId,row){
	var r =confirm("Do you want to Change Fleet");
	if(r==true){
	 var fleet=document.getElementById("fleetDriver_"+row).value;
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		<%	AdminRegistrationBO adminBO=(AdminRegistrationBO)session.getAttribute("user");%>
	url='registration?event=driverRatingDetails&driverId='+driverId+'&fleet='+fleet;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text==1){
		alert("changed sucessfully");
	}else if(text==-1){
		alert("Driver is LoggedIn");
	}else{
		alert("Couldn't change");
	}
	}
}
function showProcess(){
	/* if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true; */
}
function deleteDriver(driverNumber,x){
	var confirmation=confirm("Records on driver "+driverNumber+" going to delete");
	if (confirmation==true)
	  {
	  var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='RegistrationAjax?event=deleteDriver&module=operationView&driverNum='+driverNumber+'&driverOrOperator='+0;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="1"){
		var table=document.getElementById("driverSummary");
		table.deleteRow(x.parentNode.parentNode.rowIndex);
	}
	  }
	else
	 alert("Driver"+driverNumber+"not deleted because you pressed cancel");
} 

function resetDriver(driverNumber){
	var confirmation=confirm("Are you sure want to Reset "+driverNumber+"'s Android-Id");
	if (confirmation==true)
	{
	  var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url='RegistrationAjax?event=resetDriver&module=operationView&driverId='+driverNumber;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text=="1"){
			alert(driverNumber+" Resets Successfully");
		}else{
			alert("Failed to Reset "+driverNumber);
		}
	}
} 
 

</script>
<script type="text/javascript">
function getDriverDetails(){
	alert("hi");
    $("#downloads").live("click", function () {
        var driverSummary = document.getElementById("driverSummary");
        var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
        printWindow.document.write('<html><head><title>Cab Details</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(driverSummary.outerHTML);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    });

}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS (Taxi Dispatch System)</title>

</head>

<body>
<form name="driverSummary"  action="control"   method="post" onsubmit="return showProcess()">
<input type="hidden"  name="action"  value="registration"  />
<input  type="hidden" name="event"  value="SummaryDR"/>

<input  type="hidden"  name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
   <c class="nav-header"><center>Driver Registration Summary</center></c>
                	<%
						String error ="";
						if(request.getAttribute("update") != null) {
						error = (String) request.getAttribute("update");
						
						}
					%>
					<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
					ArrayList al_driverData=new ArrayList();
					al_driverData=(ArrayList)request.getAttribute("driversumdata");
					DriverRegistrationBO driverBO = new DriverRegistrationBO();
					String isreadOnly = adminBO.getCheckDriverMobile()==0?"hidden":"button";
%>


<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>					
					<div id="error"></div>
					<div>
					<table width="100%"  border="0" cellpadding="0"  class="navbar-static-top">
					<tr>
					 <td><b>First Name</b></td>
					 <td> <input  type="text"  autocomplete="off"   id="first_name"  name="first_name"  value="<%=first %>" />  </td>
					 <td><b>Last Name</b></td>
					 <td> <input  type="text"   autocomplete="off"  name="last_name" id="last_name"  value="<%=last %>"  />  </td> 
					</tr>
					<tr>
					<td><b>Driver ID</b></td>
					 <td> <input  type="text"  autocomplete="off"   name="driver_id"  id="driver_id"  value="<%=driver %>" /> 
					  <ajax:autocomplete
				  					fieldId="driver_id"
				  					popupId="model-popup1"
				  					targetId="driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" />  
					  </td>
					 <td><b>cab No</b></td>
					 <td> <input  type="text"  autocomplete="off"   name="cab_no" id="cab_no"   value="<%=cab %>" /> 
					  <ajax:autocomplete
				  					fieldId="cab_no"
				  					popupId="model-popup1"
				  					targetId="cab_no"
				  					baseUrl="autocomplete.view"
				  					paramName="cabNum"
				  					className="autocomplete"
				  					progressStyle="throbbing" />  	 
					  </td>
					</tr>
					<tr>
					<td><b>Status</b></td>
					<td>
					<select name="status" id="status">
					       	<option value="2" <%=status=="2"?"selected":""%>>All</option>
                 		 	<option value="1" <%=status=="1"?"selected":""%>>Active</option>
                 		 	<option value="0" <%=status=="0"?"selected":""%>>InActive</option>
                 		 	</select>
					</td>					</tr>
					<tr   align="center"  >
					<td colspan="4" >
					<input  type="submit" name="Button" value="Search" onclick="button()"/> 
					<input  type="button" name="download" id ="downloads" value="Download"  onclick="getDriverDetails()" disable /></td>
				</tr>
					</table>
					<br>
					</div>
					<div style="display: block;">
					
                    <table width="80%" border="1" height="auto" id="driverSummary" class="table-striped" align="center">
                    
                    <thead> 
                     <tr style="background-color: rgb(255, 102, 255)">	
                        <th width="10%">S.No</th>
                        <th width="20%">First Name</th>
                        <th width="20%">Last Name</th>
                        <th width="10%">User No</th>
                        <th width="5%">Cab No</th>
                        <th width="10%">Status</th>
                        <th width="20%">Action</th>
                      </tr>
                      </thead>
                      <tbody>		
                      <%if(request.getAttribute("driversumdata")!=null){ 												
						boolean colorLightGreen = true;
						String colorPattern;
						for(int r_count=0;r_count<al_driverData.size();r_count++){
							colorLightGreen = !colorLightGreen;
							if(colorLightGreen){
								colorPattern="style=\"background-color:white\""; 
								}
							else{
								colorPattern="style=\"background-color:rgb(102, 255, 204)\"";
							}
				    %>
				    		 
                      <tr>
                        <td align="center" <%=colorPattern %>><%=r_count+1 %></td>
                       	<td align="center" <%=colorPattern %>><%=((DriverRegistrationBO)al_driverData.get(r_count)).getFname()%></td>
                        <td align="center" <%=colorPattern %>>
                          <%=((DriverRegistrationBO)al_driverData.get(r_count)).getLname()%>
	                    </td>
                        <td align="center" <%=colorPattern %>>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getUid()%>&nbsp;</td>
                        <td align="center" <%=colorPattern %>>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getCabno()%>&nbsp;</td>
                        <td align="center" <%=colorPattern %>>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getStatus().equals("1")?"Active":"Inactive"%>&nbsp;</td>
                        <td align="center" <%=colorPattern %>>&nbsp;<a href="/TDS/control?action=registration&module=systemsetupView&event=editDR&seq=<%=((DriverRegistrationBO)al_driverData.get(r_count)).getSno()%>" onclick="showProcess()">Edit</a>&nbsp;
                        <input type="button" name="imgDeleteDriver" id="imgDeleteDriver" value="Delete" onclick="deleteDriver('<%=((DriverRegistrationBO)al_driverData.get(r_count)).getSno()%>',this);showProcess()"></input>
                        <input type="<%=isreadOnly%>" name="resetDriverAid" id="resetDriverAid" value="Reset" onclick="resetDriver('<%=((DriverRegistrationBO)al_driverData.get(r_count)).getSno()%>')" ></input>
                        </td>
                        <%if(fleetList!=null && fleetList.size()>0){%>
	                        <td align="center" <%=colorPattern %>>
		                         <select name="fleetDriver_<%=r_count %>" id="fleetDriver_<%=r_count %>" onchange="changeFleetForDriver(<%=((DriverRegistrationBO)al_driverData.get(r_count)).getSno()%>,<%=r_count%>)" >
		                         <%if(fleetList!=null){
									for(int i=0;i<fleetList.size();i++){ %>
										<option value="<%=fleetList.get(i).getFleetNumber()%>" <%=(fleetList.get(i).getFleetNumber().equals(adminBo.getAssociateCode())?"selected=selected":"")%> ><%=fleetList.get(i).getFleetName() %></option>
										<%=fleetList.get(i).getFleetNumber()%>
									<% }
								  }%> 
	                        	</select>
	                        </td>
	                     <%} %>
                       	
                      </tr>
                  <%} %>
                  <tr>
                  </tr>                  
				<%}else{ %>
				 <tr><td colspan="7" style="background-color:rgb(114, 236, 236); font-weight: bold; " align="center">
				 No Records Found</td></tr>
				 <%} %> 
                  </tbody>
               
                 
				</table>
                  </div>
                   <div id="pager" class="pager" align="center" style="position:absolute; ">
						<img src="<%=ctxPath %>/images/first.png" class="first"/>
						<img src="<%=ctxPath %>/images/prev.png" class="prev"/>
						<input type="text" class="pagedisplay" disabled="disabled"/>	
						<img src="<%=ctxPath %>/images/next.png" class="next"/>
						<img src="<%=ctxPath %>/images/last.png" class="last"/>
						<input type="hidden" value="15" class="pagesize" id="pagesize" />		
					</div>
					
			
	            
</form>
 	<script defer="defer">
	$(document).ready(function() 
    { 
        $("#driverSummary")
		.tablesorter({widthFixed: false, widgets: ['zebra']})
		.tablesorterPager({container: $("#pager")}); 
    } 
	); 
</script>
    	
               
 	<div id="Jobs" style="display:visible;position:absolute;width: 52%;background-color: rgb(219, 173, 173);">
			 		<table  class="footable"   id="jobHistory1" > 
		       		</table>
       				</div>
 </body>

</html>
