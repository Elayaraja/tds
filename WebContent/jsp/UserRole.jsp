<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript">
function checkAllRegistrationEntry(rpId){ 
		document.UserRole.driverreg.checked = rpId.checked;
		document.UserRole.userreg.checked = rpId.checked;
		document.UserRole.company.checked = rpId.checked;
		document.UserRole.queue.checked = rpId.checked;
	}
function checkAllRequestEntry(rpId){
		document.UserRole.acceptopenreq.checked = rpId.checked;
		document.UserRole.openreqentry.checked = rpId.checked;
		document.UserRole.lostandfound.checked = rpId.checked;
		document.UserRole.maintenance.checked = rpId.checked;
		document.UserRole.behavior.checked = rpId.checked;
}

function checkAllVoucherEntry(rpId){
		document.UserRole.createvoucher.checked = rpId.checked;
		document.UserRole.changevoucher.checked = rpId.checked;
}

function checkAllSummaryEntry(rpId){
	document.UserRole.changeopenreq.checked = rpId.checked;
	document.UserRole.driversummary.checked = rpId.checked;
	document.UserRole.maintenancesummary.checked = rpId.checked;
	document.UserRole.lostandfoundsummary.checked = rpId.checked;
	document.UserRole.behaviorsummary.checked = rpId.checked;
	document.UserRole.queuesummary.checked = rpId.checked;
	document.UserRole.driveravailsummary.checked = rpId.checked;
}
function checkAllUserList(rpId){
	document.UserRole.userlist.checked = rpId.checked;
}
function checnkAllXmlData(rpId){
	document.UserRole.openreqxml.checked = rpId.checked;
	//document.UserRole.roleNo10.checked = rpId.checked;
	document.UserRole.acceptreqxml.checked = rpId.checked;
}
function checkAllReportData(rpId){
	document.UserRole.tripsummaryrep.checked = rpId.checked;
	document.UserRole.voucherreport.checked = rpId.checked;
	document.UserRole.driverpaymentreport.checked = rpId.checked;
	document.UserRole.companypaymentreport.checked = rpId.checked;
}
function checkinActive(rpId){
	document.UserRole.active.checked="";
}
function checkActive(rpId){
	document.UserRole.inactive.checked="";
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS (Taxi Dispatch System)</title>
</head>
<body>
<input type="hidden" name="systemsetup" value="6">
<form name="UserRole" action="control" method="post"><input
	type="hidden" name="<%= TDSConstants.actionParam %>"
	value="<%= TDSConstants.userRoleAction %>"> <input
	type="hidden" name="<%= TDSConstants.eventParam %>"
	value="<%= TDSConstants.updateUserRole%>"> <jsp:useBean
	id="adminBO" class="com.tds.tdsBO.AdminRegistrationBO" scope="request" />
<jsp:setProperty name="adminBO" property="*" />


<table id="pagebox" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<table id="bodypage" width="800" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td colspan="8" align="center">
				<div id="title">
				<h2>User&nbsp;Privilages</h2>
				</div>
				</td>
			</tr>
			<%
				
				if (request.getAttribute("UserRole") != null) {
					adminBO = (AdminRegistrationBO) request.getAttribute("UserRole");
					System.out.println("open req from jsp = = "+adminBO.getOpenreqentry());
				}
				
			%>

			<tr>
				<td align="left" colspan="4">User&nbsp;ID&nbsp;:&nbsp;<%=adminBO.getUname()%></td>
				<td align="right" colspan="4">User&nbsp;Type&nbsp;:&nbsp;<%=adminBO.getUsertypeDesc()%></td>

			</tr>
			<tr align="center">
				<%
				if ((adminBO.getActive()).trim().equalsIgnoreCase("1")) {
				%>
				<td colspan="8">&nbsp;Status<input type="radio" name="active"
					checked="checked" onclick="checkActive(active);"/>&nbsp;Active&nbsp;<input type="radio"
					name="inactive" onclick="checkinActive(inactive);"/>&nbsp;In&nbsp;Active</td>
				<%
				}
				%>
				<%
				if ((adminBO.getActive()).trim().equalsIgnoreCase("0")) {
				%>
				<td colspan="8">&nbsp;Status<input type="radio" name="active" onclick="checkActive(active);"/>&nbsp;Active&nbsp;<input
					type="radio" name="inactive" checked="checked" onclick="checkinActive(inactive);"/>&nbsp;In&nbsp;Active</td>
				<%
				}
				%>

			</tr>
			<tr>
				<td colspan="8">
				<table>
					<tr>
						<td align="left">
						<table>
							<tr>
								<td bgcolor="#d3d3d3">Registration&nbsp;Entry&nbsp; <input
									type="checkbox" name="check1" value="1"
									onclick="checkAllRegistrationEntry(check1);" /></td>
							</tr>
							<%
									if ((adminBO.getDriverreg()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=driverreg checked>Driver&nbsp;Registration&nbsp;-1</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=driverreg>Driver&nbsp;Registration&nbsp;-1</TD></TR>");
								if ((adminBO.getUserreg()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=userreg checked>User&nbsp;Registration 2</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=userreg>User&nbsp;Registration 2</TD></TR>");
								
								if ((adminBO.getCompanymaster()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=company checked>Company&nbsp;Registration 2</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=company>Company&nbsp;Registration 2</TD></TR>");
								
								if ((adminBO.getQueuecoordinate()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=queue checked>Zone&nbsp;Co-Ordinate</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=queue>Zone&nbsp;Co-Ordinate</TD></TR>");
							%>
						</table>
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>
						<table>
							<tr>
								<td bgcolor="#d3d3d3">Request&nbsp;Entry&nbsp;<input
									type="checkbox" name="check2" value="2"
									onclick="checkAllRequestEntry(check2);" /></td>
							</tr>
							<%
								if ((adminBO.getAcceptopenreq()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=acceptopenreq checked>Accept&nbsp;Open&nbsp;Request&nbsp;-3</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=acceptopenreq>Accept&nbsp;Open&nbsp;Request&nbsp;-3</TD></TR>");
								if ((adminBO.getOpenreqentry()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=openreqentry checked>Open&nbsp;Request&nbsp;Entry&nbsp; 4</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=openreqentry>Open&nbsp;Request&nbsp;Entry&nbsp; 4</TD></TR>");
								if ((adminBO.getLostandfound()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=lostandfound checked>Lost&nbsp;Found</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=lostandfound>Lost&nbsp;Found</TD></TR>");
								if ((adminBO.getMaintenance()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=maintenance checked>Cab&nbsp;Maintenance</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=maintenance>Cab&nbsp;Maintenance</TD></TR>");
								if ((adminBO.getBehavior()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=behavior checked>Driver&nbsp;Behavior</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=behavior>Driver&nbsp;Behavior</TD></TR>");
								
							%>
						</table>
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>
							<table>
							<tr>
								<td bgcolor="#d3d3d3">Voucher&nbsp;Entry&nbsp;<input
									type="checkbox" name="check3" value="3"
									onclick="checkAllVoucherEntry(check3);" /></td>
							</tr>
							<%
									if ((adminBO.getCreatevoucher()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=createvoucher checked>Create&nbsp;Voucher-5</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=createvoucher>Create&nbsp;Voucher-5</TD></TR>");
							if ((adminBO.getChangeVoucher()).trim().equalsIgnoreCase("1"))
								out
								.println("<TR><TD><Input type=CheckBox name=changevoucher checked>Change&nbsp;Voucher</TD></TR>");
							else
								out
								.println("<TR><TD><Input type=CheckBox name=changevoucher>Change&nbsp;Voucher</TD></TR>");
							%>
						</table>
						</td>
					</tr>
					<tr>
						<td>
						<table>
							<tr>
								<td bgcolor="#d3d3d3">Summary&nbsp;Entry&nbsp;<input
									type="checkbox" name="check4" value="4"
									onclick="checkAllSummaryEntry(check4);" /></td>
							</tr>
							<%
									if ((adminBO.getChangeopenreq()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=changeopenreq checked>Change&nbsp;Open&nbsp;Request&nbsp;Summary -6</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=changeopenreq>Change&nbsp;Open&nbsp;Request&nbsp;Summary -6</TD></TR>");

								if ((adminBO.getDriversummary()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=driversummary checked>Driver&nbsp;Summary -7</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=driversummary>Driver&nbsp;Summary -7</TD></TR>");
								if ((adminBO.getMaintenanceSummary()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=maintenancesummary checked>Maintenance&nbsp;Summary</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=maintenancesummary>Maintenance&nbsp;Summary</TD></TR>");
								if ((adminBO.getLostandfoundSummary()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=lostandfoundsummary checked>Lost&nbsp;Found&nbsp;Summary</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=lostandfoundsummary>Lost&nbsp;Found&nbsp;Summary</TD></TR>");
								if ((adminBO.getBehaviorSummary()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=behaviorsummary checked>Driver&nbsp;Behavior&nbsp;Summary</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=behaviorsummary>Driver&nbsp;Behavior&nbsp;Summary</TD></TR>");
								if ((adminBO.getQueuecoordinateSummary()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=queuesummary checked>Queue&nbsp;Co-Ordinate&nbsp;Summary</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=queuesummary>Queue&nbsp;Co-Ordinate&nbsp;Summary</TD></TR>");
								
								if ((adminBO.getDriverAvailability()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=driveravailsummary checked>Driver&nbsp;Availability&nbsp;Summary</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=driveravailsummary>Driver&nbsp;Availability&nbsp;Summary</TD></TR>");
								
							%>
						</table>
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>
							<table>
							<tr>
								<td bgcolor="#d3d3d3">User&nbsp;List&nbsp;<input
									type="checkbox" name="check5" value="5"
									onclick="checkAllUserList(check5);" /></td>
							</tr>
							<%
									if ((adminBO.getUserlist()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=userlist checked>User&nbsp;List -8</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=userlist>User&nbsp;List -8</TD></TR>");
							%>
						</table>
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>
							<table>
							<tr>
								<td bgcolor="#d3d3d3">XML&nbsp;Data's&nbsp;<input
									type="checkbox" name="check6" value="6"
									onclick="checnkAllXmlData(check6);" /></td>
							</tr>
							<%
									if ((adminBO.getOpenreqxml()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=openreqxml checked>Open&nbsp;Request&nbsp;XML -9</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=openreqxml>Open&nbsp;Request&nbsp;XML -9</TD></TR>");

								/*if (hmp_roleList.containsKey("10"))
									out
									.println("<TR><TD><Input type=CheckBox name=roleNo10 checked>In&nbsp;Progress&nbsp;Request&nbsp;XML-10</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=roleNo10>In&nbsp;Progress&nbsp;Request&nbsp;XML-10</TD></TR>");
								*/
								if ((adminBO.getAcceptreqxml()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=acceptreqxml checked>Accepted&nbsp;Request&nbsp;XML-11</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=acceptreqxml>Accepted&nbsp;Request&nbsp;XML-11</TD></TR>");
							%>
						</table>
						</td>
					</tr>
					<tr>
						<td>
							<table>
							<tr>
								<td bgcolor="#d3d3d3">Reports&nbsp;Data's&nbsp;<input
									type="checkbox" name="check7" value="7"
									onclick="checkAllReportData(check7);" /></td>
							</tr>
							<%
									if ((adminBO.getDriverPaymentReport()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=driverpaymentreport checked>Driver&nbsp;Payment;&nbsp;Report -13</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=driverpaymentreport>Driver&nbsp;Payment;&nbsp;Report -13</TD></TR>");
							%>
							
							<%
									if ((adminBO.getCompanyPaymentReport()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=companypaymentreport checked>Company&nbsp;Payment&nbsp;Report -14</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=companypaymentreport>Company&nbsp;Payment&nbsp;Report -14</TD></TR>");
							%>						
							<%
									if ((adminBO.getTripsummaryrep()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=tripsummaryrep checked>Trip&nbsp;Summary&nbsp;Report -12</TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=tripsummaryrep>Trip&nbsp;Summary&nbsp;Report -12</TD></TR>");
							%>
							<%
									if ((adminBO.getVoucherReport()).trim().equalsIgnoreCase("1"))
									out
									.println("<TR><TD><Input type=CheckBox name=voucherreport checked>Voucher&nbsp;Report </TD></TR>");
								else
									out
									.println("<TR><TD><Input type=CheckBox name=voucherreport>Voucher&nbsp;Report</TD></TR>");
							%>
						</table>
						</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td>
						
						</td>
						
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						
						<td>
						
						</td>
					
						</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>

		<td align="center"><input type="hidden" name="uname"
			value="<%=adminBO.getUname()%>" /> <input type="hidden"
			name="status" value="<%=adminBO.getActive()%>" /> <input
			type="hidden" name="utype" value="<%=adminBO.getUsertype()%>" />
		<input type="submit" name="update" value="Update" /></td>
	</tr>
</table>
</form>
</body>
</html>
