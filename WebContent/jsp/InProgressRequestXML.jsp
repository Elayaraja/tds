<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO;"%>
<%
	ArrayList al_inProgressRequest = new ArrayList();	
	OpenRequestBO openRequestBO;
	if(request.getAttribute("inprogressdata") != null) {
		al_inProgressRequest = (ArrayList) request.getAttribute("inprogressdata");
	}
%>
<%= "<xml version=\"1.0\" >"+
	"<server>"+
	"<Messages>"+
	"<Message>	"+
	"<MessageCode>"+
	"</MessageCode>"+
	"<MessageDesc>"+
	"</MessageDesc>"+
	"<MessageVariable>"+
	"</MessageVariable>"+
	"</Message>	"+
	"<Message>	"+
	"<MessageCode>"+
	"</MessageCode>"+
	"<MessageDesc>"+
	"</MessageDesc>"+
	"<MessageVariable>"+
	"</MessageVariable>"+
	"</Message>	"+
	"</Messages>"+
	"<ProgressRequests>"
%>
	<%
		for(int counter=0;counter<al_inProgressRequest.size();counter++){
			openRequestBO = (OpenRequestBO)al_inProgressRequest.get(counter);
			out.println("<ProgressRequest>");
			out.println("<TripID>"+openRequestBO.getTripid()+"</TripID>");
			out.println("<Phone>"+openRequestBO.getPhone()+"</Phone>");
			out.println("<Saddress1>"+openRequestBO.getSadd1()+"</Saddress1>");
			out.println("<Saddress2>"+openRequestBO.getSadd2()+"</Saddress2>");
			out.println("<Scity>"+openRequestBO.getScity()+"</Scity>");
			out.println("<Sstate>"+openRequestBO.getSstate()+"</Sstate>");
			out.println("<Szip>"+openRequestBO.getSzip()+"</Szip>");
			out.println("<Eaddress1>"+openRequestBO.getEadd1()+"</Eaddress1>");
			out.println("<Eaddress2>"+openRequestBO.getEadd2()+"</Eaddress2>");			
			out.println("<Ecity>"+openRequestBO.getEcity()+"</Ecity>");
			out.println("<Estate>"+openRequestBO.getEstate()+"</Estate>");
			out.println("<Ezip>"+openRequestBO.getEzip()+"</Ezip>");
			out.println("<SLatitude>"+openRequestBO.getSlat()+"</SLatitude>");
			out.println("<SLongitude>"+openRequestBO.getSlong()+"</SLongitude>");
			out.println("<STime>"+openRequestBO.getSttime() +"</STime>");
			out.println("<Driverid>"+openRequestBO.getDriverid()+"</Driverid>");
			out.println("</ProgressRequest>");
		}		
	%>
<%= "</ProgressRequests></server>" %>