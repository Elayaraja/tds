<%@page import="com.tds.tdsBO.CreditCardBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.Address"%>
<%@page import="com.tds.cmp.bean.CustomerProfile"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%
Address  add = (Address)request.getAttribute("add");   
CustomerProfile  cprofile = (CustomerProfile)request.getAttribute("cprofile"); 
ArrayList<Address> addHist =  (ArrayList<Address>) request.getAttribute("AddressHistory");
ArrayList<CustomerProfile> profHist =  (ArrayList<CustomerProfile>) request.getAttribute("ProfileHistory");
CreditCardBO cardDetails =  (CreditCardBO) request.getAttribute("cardDetails");


%>
<script type="text/javascript" src="js/jquery.js"></script>
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<script type="text/javascript">


function addCreditCard(){
	var customerName=document.getElementById("userName").value;
	var cardNumber=document.getElementById("creditCardNumber").value;
	var nameOnCard=document.getElementById("nameOnCard").value;
	var expiryDate=document.getElementById("expiryDate").value;
	var masterKey=document.getElementById("masterKey").value;
	if(cardNumber!="" && expiryDate!="" && nameOnCard!=""){
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'CustomerServiceAjax?event=checkCustomerKey&masterKey='+masterKey;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text==""){
			var url = 'CustomerServiceAjax?event=creditCardRegistration&customerName='+customerName+'&masterKey='+masterKey;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			alert("Click add once again");
		}else if(text!=null && text!=""){
			var url = 'CustomerServiceAjax?event=creditCardDetails&customerName='+customerName+'&customerKey='+text+'&cardNumber='+cardNumber+'&nameOnCard='+nameOnCard+'&expiryDate='+expiryDate;	
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			text = xmlhttp.responseText;
			if(text!="" && text!="0"){
				alert("Your Creditcard is added Successfully");
			}else{
				alert("Your Creditcard is not added Successfully");
	
			}
			$('#creditCardRegistrationjqm').jqmHide();
			document.getElementById("userName").value="";
			document.getElementById("creditCardNumber").value="";
			document.getElementById("nameOnCard").value="";
			document.getElementById("expiryDate").value="";
			location.reload();
		}
	}else{
		alert("Enter Card Details");
	}
}
function payAmount(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'CustomerServiceAjax?event=paymentFromCreditCard&amount='+document.getElementById("amount").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	
}
function showPhones(){
	$('#userPhoneNumbers').jqm();
	$('#userPhoneNumbers').jqmShow();
	getExistingPhones();
}
function getExistingPhones(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'RegistrationAjax?event=getExtraPhones&primaryNumber='+document.getElementById("masterKey").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	var jsonObj = "{\"extraNumbers\":"+text+"}" ;
	var obj = JSON.parse(jsonObj.toString());
	if(obj.extraNumbers.length>0 && document.getElementById('numberOfPhones').value=="0"){
		for(var j=0;j<obj.extraNumbers.length;j++){
			addPhones();
			document.getElementById("phNum"+j).value=obj.extraNumbers[j].Ph;
		}
	}
}
function addPhones() {
	var i = Number(document.getElementById("numberOfPhones").value);
	document.getElementById('numberOfPhones').value = i+1;
	var table = document.getElementById("phoneNumbers");
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	var cell1 = row.insertCell(0);

	var element2 = document.createElement("input");
	element2.type = "text";
	element2.name = "phNum" + i;
	element2.id = "phNum" + i;
	element2.size = '10';
	cell1.appendChild(element2);
}
function removePhones() {
	try {
		var table = document.getElementById("phoneNumbers");
		var rowCount = table.rows.length;
		var size=document.getElementById('numberOfPhones').value;
		if(Number(size)>0){
			var temp = (Number(size) - 1);
			document.getElementById('numberOfPhones').value = temp;
			rowCount--;
			table.deleteRow(rowCount);
		}
	} catch (e) {
		alert(e.message);
	}
}
function submitExtraNumbers(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var extraData="&totalNum="+document.getElementById('numberOfPhones').value;
	
	for(var i=0;i<document.getElementById('numberOfPhones').value;i++){
		extraData=extraData+"&number"+i+"="+document.getElementById("phNum"+i).value;
	}
	var url = 'RegistrationAjax?event=enterExtraPhones&primaryNumber='+document.getElementById("masterKey").value+extraData;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	$('#userPhoneNumbers').jqmHide();
}

function showPopup(){
	$('#creditCardRegistrationjqm').jqm();
	$('#creditCardRegistrationjqm').jqmShow();
}
function showUpdatePopup(userName,customerKey,cardNumber,nameOnCard,expiryDate,ccInfoKey){
	$("#add").hide();
	$("#update").show();
	document.getElementById("updateUserNameShow").value=userName;
	document.getElementById("updateCreditCardNumberShow").value=cardNumber;
	document.getElementById("updateNameOnCardShow").value=nameOnCard;
	document.getElementById("updateExpiryDateShow").value=expiryDate;
	document.getElementById("updateccInfoKey").value=ccInfoKey;
	document.getElementById("updatecustomerKey").value=customerKey;
	$('#creditCardUpdatejqm').jqm();
	$('#creditCardUpdatejqm').jqmShow();
}
function updateCreditCard(){
	
	var customerName=document.getElementById("updateUserName").value;
	var cardNumber=document.getElementById("updateCreditCardNumber").value;
	var nameOnCard=document.getElementById("updateNameOnCard").value;
	var expiryDate=document.getElementById("updateExpiryDate").value;
	var ccInfoKey=document.getElementById("updateccInfoKey").value;
	var customerKey=document.getElementById("updatecustomerKey").value;
	if(cardNumber!="" && cardNumber.length==16 && expiryDate!="" && nameOnCard!=""){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}

	var url = 'CustomerServiceAjax?event=creditCardUpdate&cardNumber='+cardNumber+'&nameOnCard='+nameOnCard+'&expiryDate='+expiryDate+'&customerName='+customerName+'&ccInfoKey='+ccInfoKey+'&customerKey='+customerKey;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!=""){
		alert("Your Creditcard is Updated Successfully");
	}else{
		alert("Your Creditcard is not Updated Successfully");
	}
	
	}else{
		alert("Enter Card Details Correctly");
	}
	$('#creditCardUpdatejqm').jqmHide();
	document.getElementById("updateUserName").value="";
	document.getElementById("updateCreditCardNumber").value="";
	document.getElementById("updateNameOnCard").value="";
	document.getElementById("updateExpiryDate").value="";
	document.getElementById("updateccInfoKey").value="";
	document.getElementById("updatecustomerKey").value="";
	location.reload();
}
function deleteCreditCard(userName,customerKey,cardNumber,nameOnCard,expiryDate,ccInfoKey){
	if(cardNumber!="" && expiryDate!="" && nameOnCard!=""){
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		var url = 'CustomerServiceAjax?event=creditCardDelete&cardNumber='+cardNumber+'&nameOnCard='+nameOnCard+'&expiryDate='+expiryDate+'&customerName='+userName+'&ccInfoKey='+ccInfoKey+'&customerKey='+customerKey;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		location.reload();
		}
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}

function closejqm() {
	$("#creditCardRegistrationjqm").hide("slow");
	$("#creditCardUpdatejqm").hide("slow");
	$(".jqmWindow").jqmHide();
}


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>User Address Details</title>

</head>
<body>

	<form name="masterForm" action="control" method="post"
		onsubmit="return showProcess()">

		<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %><input type="hidden" name="module" id="module"
			value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
		<input type="hidden" name="action" value="registration" /> <input
			type="hidden" name="event" value="custInfo" /> <input type="hidden"
			name="operation" value="2" /> <input type="hidden" name="module"
			id="module"
			value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
		<input type="hidden" name="key"
			value="  <%=request.getAttribute("key") %>" />
		<input type="hidden" name="numberOfPhones" id="numberOfPhones" value="0"/>
		<div class="rightCol">
			<div class="rightColIn">
				<%String result="";
			if(request.getAttribute("page")!=null) {
			result = (String)request.getAttribute("page");
			}
			%>
				<div id="errorpage" style="">
					<%=result.length()>0? result:"" %>
				</div>

				<c class="nav-header">
				<center>User Address Details</center>
				</c>
				<%if(profHist!=null && profHist.size()>0) {%>
				<table width="80%" border="0" cellspacing="0" cellpadding="0"
					align="center">
					<tr>
						<td>Phone No:</td>
						<td><input type="text" name="Number" id="Number"
							value="<%=cprofile.getNumber()==null?"":cprofile.getNumber() %>" />
						</td>
						<td>Name</td>
						<td><input type="text" name='Name' id="Name"
							value="<%=cprofile.getName()==null?"":cprofile.getName() %>" /></td>
					</tr>
					<tr>
						<td colspan="4" align="center"><input type="submit"
							name="button" id="button" value="Submit" /></td>
					</tr>
				</table>
				<br />
				<center>
					<h4>Client Details</h4>
				</center>
				<table width="100%" border="1" cellspacing="0" cellpadding="0"
					align="center">
					<tr style="background-color: rgb(46, 100, 121); color: white">
						<th width="9%">Phone NO:</th>
						<th width="4%">Master Key</th>
						<th width="8%">Name</th>
						<th width="14%">Address1</th>
						<th width="8%%">Address2</th>
						<th width="7%">City</th>
						<th width="3%">State</th>
						<th width="4%">Zip</th>
						<th width="6%">Payment Type</th>
						<th width="4%">Account</th>
						<th width="5%">Last Four Digit</th>
						<th width="7%">Description</th>
						<th width="4%">Premium Customer</th>
						<th width="4%">Corporate Code</th>
						<th width="10%">Operation</th>
					</tr>
					<%
					boolean colorLightGreen = true;
					String colorPattern;
					for(int i=0;i<profHist.size();i++) {  
						colorLightGreen = !colorLightGreen;
						if(colorLightGreen){
							colorPattern="style=\"background-color:rgb(242, 250, 250)\""; 
						}
						else{
							colorPattern="style=\"background-color:white\"";
						}%>

					<tr>
						<td align="center" <%=colorPattern%>><%=profHist.get(i).getNumber() %>	</td>
						<td align="center" <%=colorPattern%>><%=profHist.get(i).getMasterKey() %>
							<input type='hidden' name='masterKey' id='masterKey' value="<%=profHist.get(i).getMasterKey() %>" /></td>
						<td align="center" <%=colorPattern%>><%=profHist.get(i).getName() %>
						<input	type='hidden' name='Name<%=(i)%>' id='Name<%=(i)%>' value="<%=profHist.get(i).getName() %>" /></td>
						<td align="center" <%=colorPattern%>> <%=profHist.get(i).getAdd1() %> </td>
						<td align="center" <%=colorPattern%>> <%=profHist.get(i).getAdd2() %> </td>
						<td align="center" <%=colorPattern%>> <%=profHist.get(i).getCity() %> </td>
						<td align="center" <%=colorPattern%>> <%=profHist.get(i).getState() %> </td>
						<td align="center" <%=colorPattern%> > <%=profHist.get(i).getZip() %> </td>
						<td align="center" <%=colorPattern%> > <%=profHist.get(i).getPaymentType() %></td>
						<td align="center" <%=colorPattern%> > <%=profHist.get(i).getAccount() %> </td>
						<input type="hidden" name="account" id="account" value="<%=profHist.get(i).getAccount() %>" />
						<td align="center" <%=colorPattern%> > <%=profHist.get(i).getCClastfourdigits() %> </td>
						<td align="center" <%=colorPattern%> > <%=profHist.get(i).getDescription() %> </td>
						<td align="center" <%=colorPattern%> > <%=profHist.get(i).getPremiumcustomer() %></td>
						<td align="center" <%=colorPattern%> > <%=profHist.get(i).getCorporateCode() %> </td>
						<td colspan="4" align="center" style="height: 20px;background-color:white;" <%=colorPattern%>><a
							style="text-decoration: none;"
							href="control?action=registration&event=custInfo&event2=updateMaster&module=operationView&masterKey=<%=profHist.get(i).getMasterKey() %>">
								Update</a> / <a style="text-decoration: none;"
							href="control?action=registration&event=custInfo&event2=deleteMaster&module=operationView&masterKey=<%=profHist.get(i).getMasterKey() %>"
							onclick="deleting()">Delete</a></td>
					</tr>
					<%}%>
				</table>
				<br />
				<center>
				
					<h4>CreditCard Details</h4>
					<h4>Add Extra Phone Numbers</h4>
				</center>
				<%if(cardDetails!=null) {%>
				<table width="100%" id="cardDetails" border="1" cellspacing="0"	cellpadding="0" align="center">					
					<tr style="background-color: white;">
						<th>UserName</th>
						<th>CustomerKey</th>
						<th>CardNumber</th>
						<th>Name On Card</th>
						<th>Exp.Date</th>
					</tr>
						<%boolean colorLightGreen1 = true;
						String colorPattern1;
						for(int i=0;i<profHist.size();i++) {  
							colorLightGreen1 = !colorLightGreen1;
							if(colorLightGreen1){
								colorPattern1="style=\"background-color:rgb(242, 250, 250)\""; 
								}
							else{
								colorPattern1="style=\"background-color:white\"";
							}						
                         %>
					<tr <%=colorPattern1 %>>
						<td align="center"><%=cardDetails.getUserName() %></td>
						<td align="center"><%=cardDetails.getCustomerKey() %>
						</td>
						<td align="center"><%=cardDetails.getCardNumber() %>
						</td>
						<td align="center"><%=cardDetails.getNameOnCard()%></td>
						<td align="center"><%=cardDetails.getExpiryDate()%></td>
						<td colspan="4" align="center"><a
							style="text-decoration: none;"
							href="javascript:showUpdatePopup('<%=cardDetails.getUserName()%>','<%=cardDetails.getCustomerKey() %>','<%=cardDetails.getCardNumber() %>','<%=cardDetails.getNameOnCard()%>', '<%=cardDetails.getExpiryDate()%>','<%=cardDetails.getCcInfoKey()%>')">Update</a>
						</td>
						<td colspan="4" align="center"><a
							style="text-decoration: none;"
							href="javascript:deleteCreditCard('<%=cardDetails.getUserName()%>','<%=cardDetails.getCustomerKey() %>','<%=cardDetails.getCardNumber() %>','<%=cardDetails.getNameOnCard()%>', '<%=cardDetails.getExpiryDate()%>','<%=cardDetails.getCcInfoKey()%>')">Delete</a>
						</td>
					</tr>
					<% }}%>
					</table>
					<center>
					<input type="button" value="Add New Card" onclick="showPopup()"></input>
					<input type="button" value="Add Extra Phones" onclick="showPhones()"></input>
					</center>								
				<br />
				<center>
					<h4>Address History</h4>
				</center>
				<table width="80%" id="addressHistory" border="1" cellspacing="0" cellpadding="0" align="center">
					<tr style="background-color: rgb(46, 100, 121); color: white">
						<th style="display: none">Phone NO:</th>
						<th>LandMark</th>
						<th>Address1</th>
						<th>Address2</th>
						<th>City</th>
						<th>State</th>
						<th>Zip</th>
						<th>Operation</th>
						<th style="display: none">Latitude</th>
						<th style="display: none">Longitude</th>
					</tr>
					<%if(addHist!=null && addHist.size()>0) {
						boolean colorLightGreen2 = true;
						String colorPattern2;									
					for(int i=0;i<addHist.size();i++) {  
						colorLightGreen2 = !colorLightGreen2;
						if(colorLightGreen2){
							colorPattern2="style=\"background-color:rgb(242, 250, 250)\""; 
							}
						else{
							colorPattern2="style=\"background-color:white\"";
						}			
                       %>
					<tr <%=colorPattern2 %>>
						<%if(addHist.get(i).getLandmarkKey()!=null && !addHist.get(i).getLandmarkKey().equals("")){ %>
						<td align="center"><%=addHist.get(i).getLandmarkName() %></td>
						<%}else{ %>
						<td align="center">No LM Used</td>
						<%} %>
						<td align="center" style="display: none;"><%=addHist.get(i).getNumber() %>
						</td>
						<td align="center"><%=addHist.get(i).getAdd1() %></td>
						<td align="center"><%=addHist.get(i).getAdd2() %></td>
						<td align="center"><%=addHist.get(i).getCity()%></td>
						<td align="center"><%=addHist.get(i).getState()%></td>
						<td align="center"><%=addHist.get(i).getZip()%></td>
						<td colspan="4" align="center">
						<%if(addHist.get(i).getLandmarkKey()!=null && !addHist.get(i).getLandmarkKey().equals("")){  %>
						<a
							style="text-decoration: none;"
							href="control?action=registration&event=custInfo&event2=deleteAddress&module=operationView&masterKey=<%=profHist.get(0).getMasterKey() %>&landmarkKey=<%=addHist.get(i).getLandmarkKey()%>">Delete</a>
							<!--  deletion of customer profile not yet completed -->
						<%}else{ %>
						<a
							style="text-decoration: none;"
							href="control?action=registration&event=custInfo&event2=updateAddress&module=operationView&masterKey=<%=profHist.get(0).getMasterKey() %>&addressKey=<%=addHist.get(i).getAddressKey()%>">Update</a>						
						/ <a
							style="text-decoration: none;"
							href="control?action=registration&event=custInfo&event2=deleteAddress&module=operationView&masterKey=<%=profHist.get(0).getMasterKey() %>&addressKey=<%=addHist.get(i).getAddressKey()%>">Delete</a>						
						<%} %>
						</td>
						<%}}%>
					</tr>
				</table>
				<%} else { %>
				<div id="emptyPage" style="font-size: medium; color: red;">
					<%request.getAttribute("emptyPage"); %>
				</div>
				<%} %>
				<div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
			</div>
		</div>
		<div class="jqmWindow" id="userPhoneNumbers">
			<table id="phoneNumbers">
				<tr style="width: 30%;">
					<th style="width: 25%;background-color: #d3d3d3; ">Phones</th>
				</tr>
			</table>
			<table>
			<tr style="width: 30%;">
				<td><input type="button" name="add" value="Add" onclick="addPhones()"/></td>
				<td><input type="button" name="delete" value="Remove" onclick="removePhones()"/></td>
				<td><input type="button" name="enterPh" value="Submit" onclick="submitExtraNumbers()"/></td>
			</tr>
			</table>		
		</div>
		<div class="jqmWindow" id="creditCardRegistrationjqm">
		<table width="100%">
		<tr align="center" style="background-color: #00FFFF; width: 100%">
		<td colspan="4"> Credit Card Registration &nbsp; 		
		<a style="margin-left: 60px" href="#" >
		<img alt="" src="images/Dashboard/close.png" onclick="closejqm()"/>	</a>
		</td>						
		</tr>
				<tr>
					<td>UserName: </td>
					<td><input type="text" name="userName" id="userName" value=""></input>
					</td>
					<td>Card Number: </td>
					<td><input type="text" name="creditCardNumber" id="creditCardNumber" value=""></input>
					</td>
				</tr>
				<tr>
					<td>Name On Card: </td>
					<td><input type="text" name="nameOnCard" id="nameOnCard" value=""></input>
					</td>
					<td>Expiry Date: </td>
					<td><input type="text" name="expiryDate" id="expiryDate" value=""></input>
					</td>
				</tr>
				<tr>
					<td colspan="4" align="center"><input type="button" value="Add" onclick="addCreditCard()"></input>
					</td>
				</tr>
			</table>
		</div>
		<div class="jqmWindow" id="creditCardUpdatejqm">
		<table width="100%">		
			<tr align="center" style="background-color: #00FFFF; width: 100%">
				<td colspan="4"> Data in our Database &nbsp; 		
				<a style="margin-left: 60px" href="#" >
				<img alt="" src="images/Dashboard/close.png" onclick="closejqm()"/>	</a>
				</td>						
			</tr>
				<tr>
					<td>Name:</td>
					<td><input type="text" name="updateUserNameShow"
						id="updateUserNameShow" readonly="readonly" value=""></input></td>
					<td>CardNumber:</td>
					<td><input type="text" name="updateCreditCardNumberShow"
						id="updateCreditCardNumberShow" readonly="readonly" value=""></input>
					</td>
				</tr>

				<tr>
					<td>Name On Card:</td>
					<td><input type="text" name="updateNameOnCardShow"
						readonly="readonly" id="updateNameOnCardShow" value=""></input></td>
					<td>Expiry Date:</td>
					<td><input type="text" name="updateExpiryDateShow"
						readonly="readonly" id="updateExpiryDateShow" value=""></input></td>
				</tr>
				<tr>
					<td>
						<h3>Data to be Updated</h3>
					</td>
				</tr>
				<tr>
					<td>UserName:</td>
					<td><input type="text" name="updateUserName"
						id="updateUserName" value=""></input></td>
					<td>Card Number:</td>
					<td><input type="text" name="updateCreditCardNumber"
						id="updateCreditCardNumber" value=""></input></td>
				</tr>
				<tr>
					<td>Name On Card:</td>
					<td><input type="text" name="updateNameOnCard"
						id="updateNameOnCard" value=""></input></td>
					<td>Expiry Date:</td>
					<td><input type="text" name="updateExpiryDate"
						id="updateExpiryDate" value=""></input> <input type="hidden"
						name="ccInfoKey" id="updateccInfoKey" value=""></input> <input
						type="hidden" name="updateccInfoKey" id="updateccInfoKey" value=""></input>
						<input type="hidden" name="updatecustomerKey"
						id="updatecustomerKey" value=""></input></td>
				</tr>
				<tr>
					<td><input id="add" type="button" value="Add"
						onclick="addCreditCard()"></input> <input id="update"
						type="button" value="Update" onclick="updateCreditCard()"
						style="display: none;"></input></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>
