<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Report Frame</title>
<%	String fDate="";
	String tDate="";
	String cDate="";
	String tripId="";
	String reportName = (String)request.getAttribute("name");
	reportName="BIRTReport/"+reportName;
if(request.getAttribute("fdate")!=null){
	fDate = (String)request.getAttribute("fdate");
	tDate = (String)request.getAttribute("tdate");
}
if(request.getParameter("date")!=null){
	cDate = (String)request.getParameter("date");
}
if(request.getParameter("tripId")!=null){
	tripId = (String)request.getParameter("tripId");
}
AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
String invoiceNo="";
if(request.getAttribute("invoiceNo")!=null){
	invoiceNo=(String)request.getAttribute("invoiceNo");}%>
	
</head>
<body>

<birt:viewer id="birtViewer" reportDesign="<%=reportName%>"
pattern="frameset"
height="600"
width="1200"
format="pdf"
 >
 <birt:param name="stdate" value="<%=fDate %>"></birt:param>
 <birt:param name="enddate" value="<%=tDate%>"></birt:param>
  <birt:param name="tripId" value="<%=tripId%>"></birt:param>
 <birt:param name="date" value="<%=cDate%>"></birt:param>
 <birt:param name="asscode" value="<%=adminBO.getAssociateCode()%>"></birt:param>
 <birt:param name="invoiceNumber" value="<%=invoiceNo%>"></birt:param>
 <birt:param name="timeOffset" value="<%=adminBO.getTimeZoneOffet()%>"></birt:param>
</birt:viewer>
 
</body>
</html>