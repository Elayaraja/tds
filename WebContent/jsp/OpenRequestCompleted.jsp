<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.OpenRequestBO" %>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="openBO"   class="com.tds.tdsBO.OpenRequestBO" />
<script type="text/javascript" src="Ajax/Common.js"></script>
 
<%
	ArrayList al_prevOpenRequests = (ArrayList)request.getAttribute("prevopenrequests");
%>
 	<%if(al_prevOpenRequests!=null && al_prevOpenRequests.size()>0) {%>
	    <NumRows>NumRows=2</NumRows>###
    <%} %>
    <%if(al_prevOpenRequests!=null && !al_prevOpenRequests.isEmpty()) {%>
    
 	<h3><center style="background-color:#FCD374">Completed Requests</center></h3>
 	 <div style="overflow-y:scroll;max-height: 150px;overflow-x:hide; "> 
 	<table border=2 BORDERCOLOR=green valign=top ><tr style='color: blue'><td>Driver</td><td width=5%>PickupTime</td><td width=75%>PickupAddress</td><td width="20%">Name</td>
 	</tr>

	<%for(int i=0;i<al_prevOpenRequests.size();i++){ %>
													
	<%
	OpenRequestBO ORforDisplay = (OpenRequestBO) al_prevOpenRequests.get(i);;
	%>
	<tr> 
	 <td><%=ORforDisplay.getDriverid().toString()%><textarea style='display: none;' name='DriverId_<%=i%>' id='DriverId_<%=i%>'><%=ORforDisplay.getDriverid() %></textarea></td>																
	 <td><%=ORforDisplay.getSdate()%>&nbsp;<%=ORforDisplay.getSttime()%><textarea style='display: none;' name='startTime_<%=i%>' id='startTime_<%=i%>'><%=ORforDisplay.getSttime() %></textarea></td>
	 <td><%=ORforDisplay.getSadd1()%>,<%=ORforDisplay.getSadd2()%>,<%=ORforDisplay.getScity()%>,<%=ORforDisplay.getSstate()%>- <%=ORforDisplay.getSzip()%>										
	  <td><%=ORforDisplay.getName() %><textarea style='display: none;' name='Name_<%=i%>' id='Name_<%=i%>'><%=ORforDisplay.getName() %></textarea></td>
 	 <td><B></B><input type='button' id='completedAddress<%=i%>' name='completedAddress' value='Restart' onClick='appendCompletedAddress(<%=ORforDisplay.getTripid()%>)'></input></td> 
 	 <td><B></B><input type='button' id='completedAddress<%=i%>' name='completedAddress' value='Complaint' onClick='enterComplaint(<%=ORforDisplay.getPhone()%>)'></input></td> 
	<td><input type="hidden" id="completedAdd1<%=i%>" value="<%=ORforDisplay.getSadd1()==null?"":ORforDisplay.getSadd1()%>"></input>
	 <input type="hidden" id="completedAdd2<%=i%>" value="<%=ORforDisplay.getSadd2()==null?"":ORforDisplay.getSadd2()%>"></input>
	 <input type="hidden" id="completedCity<%=i%>" value="<%=ORforDisplay.getScity()==null?"":ORforDisplay.getScity()%>"></input>
	 <input type="hidden" id="completedState<%=i%>" value="<%=ORforDisplay.getSstate()==null?"":ORforDisplay.getSstate()%>"></input>
	 <input type="hidden" id="completedZip<%=i%>" value="<%=ORforDisplay.getSzip()==null?"":ORforDisplay.getSzip()%>"></input>
	 <input type="hidden" id="completedDate<%=i%>" value="<%=ORforDisplay.getSdate()==null?"":ORforDisplay.getSdate()%>"></input>
	 <input type="hidden"  name='completedLatitude<%=i%>' id='completedLatitude<%=i%>' value="<%=ORforDisplay.getSlat()==null?"":ORforDisplay.getSlat() %>"></input>
     <input type="hidden"  name='completedLongitude<%=i%>' id='completedLongitude<%=i%>' value="<%=ORforDisplay.getSlong()==null?"":ORforDisplay.getSlong() %>"></input>
     <input type="hidden"  name='tripId<%=i%>' id='tripId<%=i%>' value="<%=ORforDisplay.getTripid()==null?"":ORforDisplay.getTripid() %>"></input>
	 <input type="hidden"  name='comments<%=i%>' id='comments<%=i%>' value="<%=ORforDisplay.getComments()==null?"":ORforDisplay.getComments()+"<esc>" %>"></input>
	 <input type="hidden" id="completedEAdd1<%=i%>" value="<%=ORforDisplay.getEadd1()==null?"":ORforDisplay.getEadd1()%>"></input>
	 <input type="hidden" id="completedEAdd2<%=i%>" value="<%=ORforDisplay.getEadd2()==null?"":ORforDisplay.getEadd2()%>"></input>
	 <input type="hidden" id="completedECity<%=i%>" value="<%=ORforDisplay.getEcity()==null?"":ORforDisplay.getEcity()%>"></input>
	 <input type="hidden" id="completedEState<%=i%>" value="<%=ORforDisplay.getEstate()==null?"":ORforDisplay.getEstate()%>"></input>
	 <input type="hidden" id="completedEZip<%=i%>" value="<%=ORforDisplay.getEzip()==null?"":ORforDisplay.getEzip()%>"></input>
	 <input type="hidden"  name='completedELatitude<%=i%>' id='completedELatitude<%=i%>' value="<%=ORforDisplay.getEdlatitude()==null?"":ORforDisplay.getEdlatitude() %>"></input>
     <input type="hidden"  name='completedELongitude<%=i%>' id='completedELongitude<%=i%>' value="<%=ORforDisplay.getEdlongitude()==null?"":ORforDisplay.getEdlongitude() %>"></input>
	 <input type="hidden" id="Acct<%=i%>" value="<%=ORforDisplay.getAcct()==null?"":ORforDisplay.getAcct()%>"></input>
	 <input type="hidden" id="PayType<%=i%>" value="<%=ORforDisplay.getPaytype()==null?"":ORforDisplay.getPaytype()%>"></input>
	 <input type="hidden" id="Zone<%=i%>" value="<%=ORforDisplay.getQueueno()==null?"":ORforDisplay.getQueueno()%>"></input>
	 <input type="hidden" id="LandMark<%=i%>" value="<%=ORforDisplay.getSlandmark()==null?"":ORforDisplay.getSlandmark()%>"></input>
	 <input type="hidden" id="ELandMark<%=i%>" value="<%=ORforDisplay.getElandmark()==null?"":ORforDisplay.getElandmark()%>"></input>
	 <input type="hidden" id="Flag<%=i%>" value="<%=ORforDisplay.getDrProfile()==null?"":ORforDisplay.getDrProfile()%>"></input>
	 <input type="hidden" id="Phone<%=i%>" value="<%=ORforDisplay.getPhone()==null?"":ORforDisplay.getPhone()%>"></input>
	 <input type="hidden" id="Name_<%=i%>" value="<%=ORforDisplay.getName()==null?"":ORforDisplay.getName()%>"></input>
	 <input type="hidden" id="SplIns<%=i%>" value="<%=ORforDisplay.getSpecialIns()==null?"":ORforDisplay.getSpecialIns()%>"></input>
 	 <input type="hidden" id="sharedRide<%=i%>" value="<%=ORforDisplay.getTypeOfRide()==null?"":ORforDisplay.getTypeOfRide()%>"></input>
 	 <input type="hidden" id="repeatGroup<%=i%>" value="<%=ORforDisplay.getRepeatGroup()==null?"":ORforDisplay.getRepeatGroup()%>"></input>
 	 <input type="hidden" id="roundTrip<%=i%>" value="<%=ORforDisplay.getRoundTrip()==null?"":ORforDisplay.getRoundTrip()%>"></input>
 	 <input type="hidden" id="numOfPass<%=i%>" value="<%=ORforDisplay.getNumOfPassengers()==0?"1":ORforDisplay.getNumOfPassengers()%>"></input>
 	 <input type="hidden" id="dropTime<%=i%>" value="<%=ORforDisplay.getDropTime()==null?"":ORforDisplay.getDropTime()%>"></input></td>
 	  <% if(ORforDisplay.getSpecialIns()==null)
	  { %>
		  <td align="center">-</td>
	  <% }
	  else{%> 
		  
		  <td><%=ORforDisplay.getSpecialIns()%></td>
	  <% } %> 
	 <%} %>
	</tr>
 	 </table> </div>
   	<%} %>
								
								
								
								
								