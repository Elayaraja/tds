<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList,java.util.List,java.util.Map,java.util.HashMap"%>
<html>
<head>
 <link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
	function callReport(action) {
		var fromdate = "";
		var todate = "";
		var driverid = "";
		var url = "";
		fromdate = document.getElementById("start").value;
		todate = document.getElementById("end").value;
		driverid = document.getElementById("getDriverId").options[document.getElementById("getDriverId").selectedIndex].value
		if(fromdate != "" && todate != "" && driverid != "") {
			url = document.getElementById("context").value+'/frameset?__report=BIRTReport/DriverPaymentReport.rptdesign&__format=pdf&driverID='+driverid+'&startDate='+fromdate+'&endingDate='+todate
			document.driverPaymentForm.action = url;
			document.driverPaymentForm.target = 'new';
			//alert(url);
			driverPaymentForm.submit();
		} else {
			alert("You must give all Values");
		}
	}

</script>
<%
	List al_driverlist = null;
	Map mp_driverMap = null;
	System.out.println("Before Request "+request.getAttribute("driverList"));
	if(request.getAttribute("driverList") != null) {
		System.out.println(" Request "+request.getAttribute("driverList"));
		al_driverlist = (ArrayList) request.getAttribute("driverList");
	}
%>
</head>
<body>
<form name="driverPaymentForm" action="control" method="post">
	<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
	<table id="pagebox">
		<tr>
			<td>
			<table id="bodypage">
			<tr>
				<td colspan="7" align="center">
					<div id="title" >
						<h2>Get&nbsp;Driver&nbsp;Payment&nbsp;Report</h2>
					</div>
				</td>
			</tr>
			<tr>
				<td>Select Driver Name</td>
				<td>&nbsp;</td>
				<td colspan="5">
					<select id="getDriverId">
						<option>Select</option>
					<%
						System.out.println("List "+ al_driverlist);
						for(int count = 0;al_driverlist != null && count < al_driverlist.size(); count ++) {
							mp_driverMap = (HashMap) al_driverlist.get(count);
					%>
							<option value="<%= mp_driverMap.get("driverid") %>"><%= mp_driverMap.get("driverName") %></option>
					<%
						}
					%>
					</select>
				</td>
			</tr>
			<tr>
				<td>Start&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
					<input type="text" name="sdate" id="start" size="10" onfocus="showCalendarControl(this);"/>
						
				<iframe width="20"  height="178"  name="gToday:normal:agenda.startdate" id="gToday:normal:agenda.startdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
				</iframe>
				</td>
				<td>&nbsp;</td>
				<td>End&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
				<input type="text" name="edate" id="end" size="10" onfocus="showCalendarControl(this);" />
				
				<iframe width="20"  height="178"  name="gToday:normal:agenda.enddate" id="gToday:normal:agenda.enddate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
				</iframe>
				</td>
			</tr>
			<tr>
				<td colspan="7"> 
					<input type="submit" name="submit" value="Search" onclick="callReport(this)"/>
				</td>
			</tr>
			</table>
			</td>
		</tr>
	</table>
</form>
</body>
</html>