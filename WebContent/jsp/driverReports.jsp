<%@taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.QueueBean"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@page import="com.tds.cmp.bean.ZoneTableBeanSP"%>
<%@page import="com.tds.cmp.bean.DriverCabQueueBean" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

<%
	String assocode = ((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode();
	ArrayList<QueueBean> driversReport = new ArrayList<QueueBean>();
	if(request.getAttribute("al_list")!=null){
		driversReport = (ArrayList<QueueBean>) request.getAttribute("al_list");
	}else{
		driversReport=null;
	}
	
	String driverId = request.getAttribute("driverId")!=null?(String)request.getAttribute("driverId"):"";
	String fDate = request.getAttribute("fDate")!=null?(String)request.getAttribute("fDate"):"";
	String tDate = request.getAttribute("tDate")!=null?(String)request.getAttribute("tDate"):"";
%>

<script type="text/javascript">
	$(document).ready(function(){
		setDate();
	});

 function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
 
 function setDate(){
	 var f="<%=fDate%>"; 
	 var t="<%=tDate%>"; 
	 if(f=="" || t==""){
	   	var curdate = new Date();
		var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();		
		var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
		var cYear = curdate.getFullYear();
	 	document.getElementById("stDate").value =cMonth+"/"+cDate+"/"+cYear;
		document.getElementById("edDate").value =cMonth+"/"+cDate+"/"+cYear;
	 }else{
		document.getElementById("stDate").value =f;
		document.getElementById("edDate").value =t;
	 }
 }
 
 function letsDoSearch(){
	document.getElementById("comments").innerHTML ="";
	var driverid=document.getElementById("driverId").value;
	if(driverid!=""){
  		document.getElementById("canSubmit").value="Yes";
 	}else{
  		$('#comments').append("Driver ID is mandatory");
  		document.getElementById("canSubmit").value="No";
 	}
 }
 
 function allSet(){
	var value = true;
	if(document.getElementById("canSubmit").value=="No"){
		value = false;
	}else{
		if(document.getElementById("stDate").value=="" || document.getElementById("edDate").value==""){
			value = false;
			$('#comments').append("From Date & To date is mandatory");
		}
	}
	return value;
 }
 
 function downloadReport(){
	    $("#download").live("click", function () {
	        var driverReportD = document.getElementById("driverReportD");
	        var printWindow = window.open('', '', 'height=500,width=1200,font-size=22');
	        printWindow.document.write('<html><head><title>DriverReport</title>');
	        printWindow.document.write('</head><body >');
	        printWindow.document.write(driverReportD.outerHTML);
	        printWindow.document.write('</body></html>');
	        printWindow.document.close();
	        printWindow.print();
	    });
	}
 
</script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
</head>

<body>
<form  name="masterFormfordriverReports"  action="control" method="post" onsubmit="return allSet()">
<input  type="hidden" name="module" id="module" value="operationView"/>
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="driverReport"/>
<input type="hidden" name="search" value="search"/>
<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<input type="hidden" name="canSubmit" id="canSubmit" value="No"/>

<c class="nav-header"> <center>Driver Report</center></c>
	<br/>
	<br/>
		<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
 		<tr align="left">
			<td>Driver&nbsp;ID:&nbsp;&nbsp;
				<input  type="text"  autocomplete="on" name="driverId"  id="driverId"  value="<%=driverId %>" />
					<ajax:autocomplete
						fieldId="driverId"
						popupId="model-popup1"
						targetId="driverId"
						baseUrl="autocomplete.view"
						paramName="DRIVERID"
						className="autocomplete"
			  			progressStyle="throbbing"/> 
			</td>
		</tr>
		
		<tr></tr>
		
		<tr align="left">
		 	<td><font color="black">From Date:</font> 
		 		<input type="text" name="stDate" id="stDate" size="10" value="" onfocus="hideCalendarControl();showCalendarControl(this);" />
		 	</td>
			<td><font color="black">To Date:</font> 
				<input type="text" name="edDate" id="edDate" size="10" value="" onfocus="hideCalendarControl();showCalendarControl(this);" />
			</td>
		</tr>
		
		<tr>
			<td>&nbsp;</td> 
		</tr>
		
    	<tr align="center">
			<td colspan="1" align="center">
			  	<input type="submit" name="search" id="search" value="Search" onclick="letsDoSearch()"/>
			</td>
			<%if (driversReport != null && driversReport.size()>0){%>
				<td colspan="0.5" align="left">
			  		<input type="button" name="download" id="download" value="Download Report" onclick="downloadReport()"/>
				</td>
			<%}%>
			<td colspan="0.5">&nbsp;</td>
		</tr>
 		</table>
 		
 		<div name="comments" id="comments" style="color: red;" ></div>
			 </b>
			 <br/>
			 <br/>
 
		<%if (driversReport != null){%>
			<table id="driverReportD" width="80%" border="0" height="auto" align="center">
				<tr style="background-color: white;" align="center">
					<th width="15%">DriverId</th>
					<th width="15%">VehicleNo</th>
					<th width="20%">Login Time (Date Time)</th>
					<th width="20%">Logout Time (Date Time)</th>
					<th width="20%">Working Hours (HH:mm:ss)</th>
				</tr>
				
				<tr>
					<td>&nbsp;</td> 
				</tr>
				<%
				  //String dateColorPattern = "style=\"background-color:rgb(46, 100, 121);\" \"color: white;\"";
				  //style="color:aliceblue; background-color:rgb(102, 255, 204);"
				  String colorPattern = "style=\"background-color:white\"";
				  String color = "style=\"background-color:rgb(102, 255, 204)\"";
				  
				  if (driversReport.size() > 0) {
					  for (int i = 0; i < driversReport.size() ; i++) {
						  QueueBean dza = (QueueBean) driversReport.get(i);
						  	if(i==0){
								%>
								<tr align="center">
									<td colspan="5" style="background-color:rgb(102, 255, 204); font-size: medium;"><%=dza.getLastLoginTime()%></td>
								</tr>									
								<%
						  	}
							
						  	%> <tr align="center">
									<td <%=colorPattern%>><%=dza.getDriverid()%></td>
									<td <%=colorPattern%>><%=dza.getVehicleNo()%></td>
									<td <%=colorPattern%>><%=dza.getLO_Logintime()%></td>
									<td <%=colorPattern%>><%=dza.getLO_LogoutTime()%></td>
									<td <%=colorPattern%>><%=dza.getStartTimeStamp()%></td>
								</tr>
							<%
							
							if(dza.getFlg()==1){
								%>
								<tr style="border-bottom-color: white;" >
									<td colspan="4" style="color:rgb(46, 100, 121); background-color:white; font-style:italic; font-size: medium; border-right-color: white;" align="right">Total time period in a Day : <%=dza.getDq_Reason()%></td>
									<td colspan="1" style="color:white; background-color:white; border-left-color: white;" >&nbsp;</td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<%
								if(i<(driversReport.size()-1)){
									%>
									<tr align="center">
										<td colspan="5" style="background-color:rgb(102, 255, 204); font-size: medium;" ><%=driversReport.get(i+1).getLastLoginTime()%></td>
									</tr>
									<%
								}
							}
						}
					}else {%>
						<tr align="center">
							<td colspan="6" align="center" style="background-color: rgb(102, 255, 204)">No Records Found</td>
						</tr>
					<%}%>
					
					</table>
		<%}%>
		
		<br/><br/>
		<div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
		</form>
	</body>
</html>