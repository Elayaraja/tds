<%@page import="com.tds.tdsBO.QueueBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.ZoneTableBeanSP" %>
<%@page import="com.tds.cmp.bean.DriverCabQueueBean" %>

<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

<%
String assocode = ((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode();
ArrayList<ZoneTableBeanSP> zones =(ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute( assocode+ "Zones");
%>
<script type="text/javascript">
	$(document).ready(function() {
		setDate();
	});

 function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
 
 function setDate(){
		var curdate = new Date();
		var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0" + curdate.getDate();
		var cMonth = curdate.getMonth() + 1 >= 10 ? curdate.getMonth() + 1 : "0" + (curdate.getMonth() + 1);
		var cYear = curdate.getFullYear();
		document.getElementById("dzhDate").value = cMonth + "/" + cDate + "/" + cYear;
	}
	function serachDriverHistory() {
		var zoneId = document.getElementById("zoneIdDZA").value;
		var date = document.getElementById("dzhDate").value;
		var time = document.getElementById("dzhTime").value;
		if(zoneId!="" && date!="" && time !=""){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'SystemSetupAjax?event=driverZoneHistory&zoneId='+zoneId+'&hisDate='+date+'&hisTime='+time;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			var jsonObj = "{\"address\":"+text+"}" ;
			var obj = JSON.parse(jsonObj.toString());
			var tableAppend=document.getElementById("showAllDriverZoneHistory");
			var tableRows="<table><tr style='background-color:gray'><td style='width:20%' align='center'>Zone Name</td><td align='center' style='width:15%'>Driver Id</td><td align='center' style='width:15%'>Cab Num</td><td align='center' style='width:25%'>Logged In Time</td><td align='center' style='width:10%'>Position</td></tr>";
			tableAppend.innerHTML="";
			for(var j=0;j<obj.address.length;j++){
				tableRows=tableRows+'<tr>';
				tableRows=tableRows+'<td align="center">'+obj.address[j].ZI+'</td>';
				tableRows=tableRows+'<td align="center">'+obj.address[j].DId+'</td>';
				tableRows=tableRows+'<td align="center">'+obj.address[j].CNo+'</td>';
				tableRows=tableRows+'<td align="center">'+obj.address[j].T+'</td>';
				tableRows=tableRows+'<td align="center">'+obj.address[j].P+'</td>';
				tableRows=tableRows+'</tr>';
			}
			tableAppend.innerHTML = tableRows+"</table>";
			$("#driversInZone").show();
		} else {
			alert("All Fields Are Mandatory");
		}
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
</head>

<body>
<form  name="masterFormfordriverZoneActivity"  action="control" method="post" onsubmit="return allSet()">
<input  type="hidden"   name="module"  id="module"  value="operationView"   />
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="driverZoneActivityHistory"/>
<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<input type="hidden" name="canDZASubmit" id="canDZASubmit" value="No"/>
<input type="hidden" name="nDZA" id="nDZA" value="1"/>

<c class="nav-header"> <center>Driver Zone History</center></c>
<br />
<br />
 <b>
	<table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
 	<tr align="left">
 		<td>ZoneId:<select name="zoneIdDZA" id="zoneIdDZA">
		<%if(zones!=null){
			for(int i=0; i<zones.size() ;i++){ %>
			<option	<%=zones.get(i).getZoneKey()%> 	value="<%=zones.get(i).getZoneKey()%>"> <%=zones.get(i).getZoneKey()+" ("+zones.get(i).getZoneDesc()+" )"%></option>
			<%}
		}else{%>
			<option	value="No" >No zones found</option>
		<%} %>	
		</select></td>
 		<td><font color="black">Date:</font> 
 		<input type="text" name="dzhDate" id="dzhDate" size="10" value="" onfocus="hideCalendarControl();showCalendarControl(this);" />
 		</td>
	
		<td><font color="black">Time:&nbsp;&nbsp;&nbsp;&nbsp;</font> 
		 <select name="dzhTime" id="dzhTime" style="border: 1px solid #C0C0C0; font-size:14px; height:25px; text-align:left; width:74px; margin:0; padding:0" >
		     <option value="0000">12:00 A</option>
              <option value="0015">12:15 A</option>
              <option value="0030">12:30 A</option>
              <option value="0045">12:45 A</option>
              <option value="0100">01:00 A</option>
              <option value="0115">01:15 A</option>
              <option value="0130">01:30 A</option>
              <option value="0145">01:45 A</option>
              <option value="0200">02:00 A</option>
              <option value="0215">02:15 A</option>
              <option value="0230">02:30 A</option>
              <option value="0245">02:45 A</option>
              <option value="0300">03:00 A</option>
              <option value="0315">03:15 A</option>
              <option value="0330">03:30 A</option>
              <option value="0345">03:45 A</option>
              <option value="0400">04:00 A</option>
              <option value="0415">04:15 A</option>
              <option value="0430">04:30 A</option>
              <option value="0445">04:45 A</option>
              <option value="0500">05:00 A</option>
              <option value="0515">05:15 A</option>
              <option value="0530">05:30 A</option>
              <option value="0545">05:45 A</option>
              <option value="0600">06:00 A</option>
              <option value="0615">06:15 A</option>
              <option value="0630">06:30 A</option>
              <option value="0645">06:45 A</option>
              <option value="0700">07:00 A</option>
              <option value="0715">07:15 A</option>
              <option value="0730">07:30 A</option>
              <option value="0745">07:45 A</option>
              <option value="0800">08:00 A</option>
              <option value="0815">08:15 A</option>
              <option value="0830">08:30 A</option>
              <option value="0845">08:45 A</option>
              <option value="0900">09:00 A</option>
              <option value="0915">09:15 A</option>
              <option value="0930">09:30 A</option>
              <option value="0945">09:45 A</option>
              <option value="1000">10:00 A</option>
              <option value="1015">10:15 A</option>
              <option value="1030">10:30 A</option>
              <option value="1045">10:45 A</option>
              <option value="1100">11:00 A</option>
              <option value="1115">11:15 A</option>
              <option value="1130">11:30 A</option>
              <option value="1145">11:45 A</option>
              <option value="1200">12:00 P</option>
              <option value="1215">12:15 P</option>
              <option value="1230">12:30 P</option>
              <option value="1245">12:45 P</option>
              <option value="1300">01:00 P</option>
              <option value="1315">01:15 P</option>
              <option value="1330">01:30 P</option>
              <option value="1345">01:45 P</option>
              <option value="1400">02:00 P</option>
              <option value="1415">02:15 P</option>
              <option value="1430">02:30 P</option>
              <option value="1445">02:45 P</option>
              <option value="1500">03:00 P</option>
              <option value="1515">03:15 P</option>
              <option value="1530">03:30 P</option>
              <option value="1545">03:45 P</option>
              <option value="1600">04:00 P</option>
              <option value="1615">04:15 P</option>
              <option value="1630">04:30 P</option>
              <option value="1645">04:45 P</option>
              <option value="1700">05:00 P</option>
              <option value="1715">05:15 P</option>
              <option value="1730">05:30 P</option>
              <option value="1745">05:45 P</option>
              <option value="1800">06:00 P</option>
              <option value="1815">06:15 P</option>
              <option value="1830">06:30 P</option>
              <option value="1845">06:45 P</option>
              <option value="1900">07:00 P</option>
              <option value="1915">07:15 P</option>
              <option value="1930">07:30 P</option>
              <option value="1945">07:45 P</option>
              <option value="2000">08:00 P</option>
              <option value="2015">08:15 P</option>
              <option value="2030">08:30 P</option>
              <option value="2045">08:45 P</option>
              <option value="2100">09:00 P</option>
              <option value="2115">09:15 P</option>
              <option value="2130">09:30 P</option>
              <option value="2145">09:45 P</option>
              <option value="2200">10:00 P</option>
              <option value="2215">10:15 P</option>
              <option value="2230">10:30 P</option>
              <option value="2245">10:45 P</option>
              <option value="2300">11:00 P</option>
              <option value="2315">11:15 P</option>
              <option value="2330">11:30 P</option>
              <option value="2345">11:45 P</option>
            </select>
		</td>
	</tr>
    <tr align="center">
	    <td colspan="3" align="center"><input type="button" name="searchHistory" id="searchHistory" value="Search" onclick="serachDriverHistory()"/>
	    </td> 
	</tr>
 </table>
 </b>
 <br />
 <br />
 <div id="driversInZone" align="center" style="display: block;">
		<table style="width: 100%;"  id="showAllDriverZoneHistory"></table>
</div>
 <br />
 <br />
 <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
