<%@taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.QueueBean"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@page import="com.tds.cmp.bean.ZoneTableBeanSP"%>
<%@page import="com.tds.cmp.bean.DriverCabQueueBean" %>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

<%
String assocode = ((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode();
ArrayList<ZoneTableBeanSP> zones =(ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute( assocode+ "Zones");
ArrayList<QueueBean> driversZone = new ArrayList<QueueBean>();
if(request.getAttribute("al_list")!=null){
	driversZone = (ArrayList<QueueBean>) request.getAttribute("al_list");
}else{
	driversZone=null;
}
int flag=0;
if(request.getAttribute("ndza")!=null){
	flag = Integer.parseInt(request.getAttribute("ndza").toString());
}
String zoneId = request.getAttribute("zoneId")!=null?(String)request.getAttribute("zoneId"):"";
String driverId = request.getAttribute("driverId")!=null?(String)request.getAttribute("driverId"):"";
String fDate = request.getAttribute("fDate")!=null?(String)request.getAttribute("fDate"):"";
String tDate = request.getAttribute("tDate")!=null?(String)request.getAttribute("tDate"):"";
%>

<script type="text/javascript">
	$(document).ready(function(){
		setDate();
	});

 function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
 
 function setDate(){
	 var f="<%=fDate%>"; 
	 var t="<%=tDate%>"; 
	 if(f=="" || t==""){
	   var curdate = new Date();
		var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();		
		var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
		var cYear = curdate.getFullYear();
		
	 	document.getElementById("dzaFromDate").value =cMonth+"/"+cDate+"/"+cYear;
		document.getElementById("dzaToDate").value =cMonth+"/"+cDate+"/"+cYear;
	 	document.getElementById("dzaFromDate1").value =cMonth+"/"+cDate+"/"+cYear;

	 }else{
		 document.getElementById("dzaFromDate").value =f;
		document.getElementById("dzaToDate").value =t;
	 }
 }
 
 function searchByBoth(n){
	   document.getElementById("showByBoth").innerHTML ="";
	   var zoneid=document.getElementById("zoneIdDZA").value;
	   var driverid=document.getElementById("driverIdDZA").value;
	   //var n = document.getElementById("radioDZA").value;
	   //alert("Search By Both click:"+n);
	   // by zone & driverid
	   if(n=="3"){
		   if(zoneid!="" && driverid!=""){
			   document.getElementById("canDZASubmit").value="Yes";
		   }else{
			   $('#showByBoth').append("ZoneId & DriverID is mandatory");
			   document.getElementById("canDZASubmit").value="No";
		   }
		   //by zone
	   }else if(n=="1"){
		   driverid="";
		   document.getElementById("driverIdDZA").value="";
		   if(zoneid!="" && zoneid!="No"){
			   document.getElementById("canDZASubmit").value="Yes";
		   }else{
			   $('#showByBoth').append("ZoneId is mandatory");
			   document.getElementById("canDZASubmit").value="No";
		   }
	   }
	   // find driver activity 
	  	 else if(n=="4" || n==""){
	  		// alert("its 4");
		   zoneid="";
		   document.getElementById("zoneIdDZA").value="";
		   if(driverid!=""){
			   document.getElementById("canDZASubmit").value="Yes";
		   }else{
			   $('#showByBoth').append("Driver Id is Mandatory");
			   document.getElementById("canDZASubmit").value="No";
		   }
		   }
	   // find driver activity 
	  	 else if(n=="7"){
	  		// alert("its 5");
		   zoneid="";
		   document.getElementById("zoneIdDZA").value="";
		   if(driverid!=""){
			   document.getElementById("canDZASubmit").value="Yes";
		   }else{
			   $('#showByBoth').append("Driver Id is Mandatory");
			   document.getElementById("canDZASubmit").value="No";
		   }
		   }
	   //by driverid
	  	 else{
		   zoneid="";
		   document.getElementById("zoneIdDZA").value="";
		   if(driverid!=""){
			   document.getElementById("canDZASubmit").value="Yes";
		   }else{
			   $('#showByBoth').append("DrievrID is mandatory");
			   document.getElementById("canDZASubmit").value="No";
		   }
	   }
	   document.getElementById("nDZA").value=n;
 }
 
 function allSet(){
	var value = true;
	if(document.getElementById("canDZASubmit").value=="No"){
		value = false;
	}else{
	//alert(value+"   "+document.getElementById("canDZASubmit").value);
		if(document.getElementById("dzaFromDate").value=="" || document.getElementById("dzaToDate").value==""){
			value = false;
			$('#showByBoth').append("From Date & To date is mandatory");
		}
	}
	return value;
 }
 
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
</head>

<body>
<form  name="masterFormfordriverZoneActivity"  action="control" method="post" onsubmit="return allSet()">
<input  type="hidden"   name="module"  id="module"  value="operationView"/>
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="driverZoneActivity"/>
<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<input type="hidden" name="canDZASubmit" id="canDZASubmit" value="No"/>
<input type="hidden" name="nDZA" id="nDZA" value="1"/>

<c class="nav-header"> <center>Driver Zone Activity</center></c>
<br />
<br />
                        		 
 <%-- <table width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
 	<tr>
 		<td align="center"><label>Select QueueId</label></td>
		<td><select name="zoneId" >
		<!-- <option	value="0" >Drivers not logged into zone</option> -->
		<%if(zones!=null){
		for(int i=0; i<zones.size() ;i++){ %>
			<option	<%=zones.get(i).getZoneKey()%> 	value="<%=zones.get(i).getZoneKey()%>" >  <%=zones.get(i).getZoneKey()+" ("+zones.get(i).getZoneDesc()+" )"%></option>
		<%}}%>	
		</select></td>
	</tr>
    <tr>                                        	                 	                                
    	<td colspan="1" align="right">				
        	<input type="submit" name="button" id="button" value="Search" />                      
		</td>								
    </tr>
 </table>     --%>
 
 <b>
	<table  width="75%" border="0" cellspacing="0" cellpadding="0" align="center">
 	<tr align="left">
 		<td>ZoneId:
 		<select name="zoneIdDZA" id="zoneIdDZA">
		<%if(zones!=null)
			{
						 for(int i=0; i<zones.size() ;i++)
						 { %>
						<option	<%=zones.get(i).getZoneKey()%> 	value="<%=zones.get(i).getZoneKey()%>" <%=zoneId.equals(zones.get(i).getZoneKey())?"Selected":"" %>>  <%=zones.get(i).getZoneKey()+" ("+zones.get(i).getZoneDesc()+" )"%></option>
					   <%}
			}				
			else
			{%>
						<option	value="No" >No zones found</option>
						<%
						
			}%>	
		</select></td>
						
		<td>DriverID:
					    <input  type="text"  autocomplete="on" name="driverIdDZA"  id="driverIdDZA"  value="<%=driverId %>" />
					    <ajax:autocomplete
						fieldId="driverIdDZA"
						popupId="model-popup1"
						targetId="driverIdDZA"
						baseUrl="autocomplete.view"
						paramName="DRIVERID"
						className="autocomplete"
			  			progressStyle="throbbing"/> 
					    </td>
				        </tr>
	
		<tr align="left">
		 		<td><font color="black">From Date:</font> 
		 		<input type="text" name="dzaFromDate" id="dzaFromDate" size="10" value="" onfocus="hideCalendarControl();showCalendarControl(this);" />
		 		</td>
			
				<td><font color="black">To Date:</font> 
				<input type="text" name="dzaToDate" id="dzaToDate" size="10" value="" onfocus="hideCalendarControl();showCalendarControl(this);" />
				</td>
		</tr>
    	<tr align="center">
			    <td colspan="1" align="center">Search :
			  	<input type="submit" name="radioDZA1" id="radioDZA1" value="By Zone " onclick="searchByBoth('1')"/> 
			  	<input type="submit" name="radioDZA2" id="radioDZA2" value="By DriverID" onclick="searchByBoth('2')"/> 
			  	<input type="submit" name="radioDZA3" id="radioDZA3" value="By Zone & DriverID" onclick="searchByBoth('3')"/>
			    <input type="submit" name="radioDZA4" id="radioDZA4" value="Find Driver Activity" onclick="searchByBoth('4')"/>
			  	<input type="submit" name="radioDZA5" id="radioDZA5" value="Find Driver Break Hours" onclick="searchByBoth('7')"/>  
		 	<!--<input type="text" name="dzaFromDate1" id="FromDate1" size="10" value="" onfocus="hideCalendarControl();showCalendarControl(this);" />	--> 
	</tr>
 		</table>	
 		<div name="showByBoth" id="showByBoth" style="color: red;" ></div>
			 </b>
			 <br/>
			 <br/>
 
		<%
			if (flag != 4 && flag !=5 && flag !=7 && flag !=8) 
				{
							if (driversZone != null)
							{
												%>
												<table width="80%" border="1" height="auto" id="driversInZone"
												align="center">
												<tr style="background-color: white;" align="center">
												<th width="12%">ZoneId</th>
												<th width="12%">DriverId</th>
												<th width="6%">VehicleNo</th>
												<th width="15%">Zone Login Time</th>
												<th width="15%">Zone Logout Time</th>
												<th width="40%">Reason</th>
												</tr><%
												boolean colorLightGreen = true;
												String colorPattern;
												if (driversZone.size() > 0) 
												{%>
											<!--	<input type="submit" name="downloadDriverActivity" id="downloadDriverActivity" value="Download Driver Activity" onclick="searchByBoth('6')"/>--><%
													for (int i = 0; i < driversZone.size() ; i++) 
													{
														QueueBean dza = (QueueBean) driversZone.get(i);
														colorLightGreen = !colorLightGreen;
															if (colorLightGreen) {
															colorPattern = "style=\"background-color:white\"";
															} else {
															colorPattern = "style=\"background-color:rgb(102, 255, 204)\"";
															}%>
															<tr align="center">
															<td <%=colorPattern%>><%=dza.getQU_NAME()%></td>
															<td <%=colorPattern%>><%=dza.getDriverId()%></td>
															<td <%=colorPattern%>><%=dza.getVehicleNo()%></td>
															<td <%=colorPattern%>><%=dza.getDq_LoginTime()%></td>
															<%
															if (dza.getStatus().equals("2")){%>
															<td <%=colorPattern%>><%=dza.getDq_LogoutTime()%></td>
															<%} else {%>
															<td <%=colorPattern%>>Present</td><%
															}%>
															<td <%=colorPattern%>><%=dza.getDq_Reason()%></td>
															</tr><%
													}
											}
												
												
									else 
										{
										%>
										<tr align="center">
										<td colspan="6" align="center"
										style="background-color: rgb(102, 255, 204)">No Records Found</td>
										</tr>
										<%
										}
										%>
										</table>
													<%
				
													%>
													<%
							}
					
				}
			
			else if(flag == 4 || flag == 5){
			%>
			<%
					if (driversZone != null)
						{
						%>
						
						<table width="80%" border="1" height="auto" id="driversInZone"
						align="center">
						<tr align="center">
						
						<td colspan="3" align="center" style="background-color:white"><b>DRIVER ACTIVITY TABLE</b></td>
											</tr>	<tr align="center">
						<td colspan="3" align="center" style="background-color: #FFE5B4"><b>Rows In This Color Shows "DRIVER JOB DETAILS" </b></td>
											</tr>	<tr align="center">
						<td colspan="3" align="center" style="background-color: #FFFF99"><b>Rows In This Color Shows "DRIVER LOGIN DETAILS" </b></td>
											</tr>	<tr align="center">
						<td colspan="3" align="center" style="background-color: #C3FDB8"><b>Rows In This Color Shows "DRIVER ZONE LOGIN DETAILS"</b></td>
						
						</tr>
				
						<tr style="background-color: white;" align="center">
							<th width="30%">START TIME</th>
							<th width="30%">END TIME</th>
							<th width="40%">REASON/JOB ID/CAB NO</th>
						</tr>
						
						<%
							boolean colorLightGreen = true;
							String colorPattern;
							if (driversZone.size() > 0) 
							{
								%> 
						 		<input type="submit" name="downloadDriverActivity" id="downloadDriverActivity" value="Download Driver Activity" onclick="searchByBoth('5')"/>
								<%

									for (int i = 0; i < driversZone.size(); i++) 
									{
										QueueBean dza =  driversZone.get(i);
										colorLightGreen = !colorLightGreen;
												if (dza.getQuerystatus().equalsIgnoreCase("1")) {
												colorPattern = "style=\"background-color:#FFE5B4\"";
												} else if (dza.getQuerystatus().equalsIgnoreCase("2")){
												colorPattern = "style=\"background-color:#FFFF99\"";
												}
												else 
												{
												colorPattern = "style=\"background-color:#C3FDB8\"";
												}
												%>
										<tr align="center">
										<td colspan="3" align="center" style="background-color: white"><b></b></td>
										</tr>	
										<% if (dza.getQuerystatus().equalsIgnoreCase("1"))
										{%>
										
										 <tr align="center">
											<td <%=colorPattern%>><%=dza.getORH_JOB_ACCEPT_TIME()%></td>
											<td <%=colorPattern%>><%=dza.getORH_JOB_COMPLETE_TIME()%></td> 
											<td <%=colorPattern%>><%=dza.getORH_REASON()%></td>
											</tr>
										
										<% } 
										else if (dza.getQuerystatus().equalsIgnoreCase("2"))
										{%>
										
										
										<tr align="center">
											<td <%=colorPattern%>><%=dza.getLO_Logintime()%></td>
											<td <%=colorPattern%>> <%=dza.getLO_LogoutTime()%> </td>
											<td <%=colorPattern%>><%=dza.getLO_REASON()%></td>
										</tr>
										
										
										<%	}
										else 
											{%>
											 
											<tr align="center">
											<td <%=colorPattern%>><%=dza.getDq_LoginTime()%></td>
											<td <%=colorPattern%>> <%=dza.getDq_LogoutTime()%> </td>
											<td <%=colorPattern%>><%=dza.getDq_Reason()%></td>
											</tr>	 
										<%}	%>
									    </tr>
							<%}//for
								
						}
						else {
								%><tr align="center"><td colspan="3" align="center" style="background-color:red">NO RECORDS FOUND</td>
								</tr><%
							}
						%></table><%
						}
			}
		else if(flag == 7){
			if (driversZone != null)
			{
								%>
							<table width="80%" border="1" height="auto" id="driversInZone"
						align="center">
						<tr align="center">
						
						<td colspan="4" align="center" style="background-color:rgb(102, 255, 204)"><b>DRIVER WORKING TIME TABLE</b></td>
											</tr>	
								<tr style="background-color: white;" align="center">
								<th width="20%">TRIP ID</th>
								<th width="20%">JOB START TIME</th>
								<th width="20%">JOB END TIME</th>
								<th width="20%">JOB COMPLETITION TIME</th>
								
								</tr><%
								boolean colorLightGreen = true;
								String colorPattern;
								if (driversZone.size() > 0) 
								{
									for (int i = 0; i < driversZone.size() ; i++) 
									{
										QueueBean dza = (QueueBean) driversZone.get(i);
										colorLightGreen = !colorLightGreen;
											if (colorLightGreen) {
											colorPattern = "style=\"background-color:white\"";
											} else {
											colorPattern = "style=\"background-color:rgb(102, 255, 204)\"";
											}%>
											<tr align="center">
											<td <%=colorPattern%>><%=dza.getORH_TRIP_ID()%></td>
											<td <%=colorPattern%>><%=dza.getORH_JOB_PICKUP_TIME()%></td>
											<td <%=colorPattern%>><%=dza.getORH_JOB_COMPLETE_TIME()%></td>
											<td <%=colorPattern%>><%=dza.getORH_TIME_DIFF()%></td>
											</tr><%
									}
									
							}
					
								
					else 
						{
						%>
						<tr align="center">
						<td colspan="4" align="center"
						style="background-color: rgb(102, 255, 204)">No Records Found</td>
						</tr>
						<%
						}
						%>
						</table>
									<%
			}
		}

		
		%>
		<br /><br/>
		<div class="footerDV">Copyright &copy; 2010 Get A Cab</div>

	</form>
</body>
</html>