<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page
	import="java.util.ArrayList,com.tds.tdsBO.LostandFoundBO,java.util.List,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%! LostandFoundBO m_lostandFoundBO = null;List m_lostandFoundList = null; %>
<% 
	if(request.getAttribute("lostAndFound") != null) {
		m_lostandFoundList = (ArrayList) request.getAttribute("lostAndFound");
	}
%>
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
<title>TDS (Taxi Dispatch System)</title>
</head>

<body>
	<input type="hidden" name="operation" value="2" />
	<div>
		<c class="nav-header">
		<center>Lost Found Criteria</center>
		</c>
		<%			String error ="";
			if(request.getAttribute("errors") != null) {
			error = (String) request.getAttribute("errors");
			}		
	    %>
		<div id="errorpage">
			<%= error.length()>0?""+error :"" %>
		</div>
		<div id="errmsg" style="position: absolute; left: 750px; top: 320px;"></div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<form name="behaviorSummary" action="control" method="post" onsubmit="return showProcess()">
						<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
						<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>" /> 
						<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.lostandFoundList %>" />
								<table id="bodypage" width="80%" align="center">
									<tr>
										<td>Item&nbsp;Name &nbsp;</td>
										<td><input type="text" name="itemName" value="" /></td>
										<td></td>
										<td>Lost&nbsp;Date &nbsp;</td>
										<td><input type="text" size="15" name="tripDate" value="" onfocus="showCalendarControl(this);" readonly="readonly" /></td>
									</tr>

									<tr>
										<td colspan="7" align="center">
											<input type="submit" name="lostandFound" value="Get Details" />
										</td>
									</tr>
								</table>
					</form>
				</td>
			</tr>
			<% if(m_lostandFoundList != null && m_lostandFoundList.size() >0  ) {  %>
			<tr>
				<td>
					<table align="center" id="bodypage" width="70%" border="1" style="background-color:rgb(46, 100, 121); ">
						<tr  style=" color: #FFFF00">
							<th>Sno</th>
							<th>Trip&nbsp;Id</th>
							<th>Item&nbsp;Name</th>
							<th>Date</th>
							<th>Time</th>
						</tr>
						<% 
	boolean colorLightGreen = true;
	String color;%>
				<% for(int count = 0; count < m_lostandFoundList.size(); count++) {  
				m_lostandFoundBO = (LostandFoundBO) m_lostandFoundList.get(count);
			colorLightGreen = !colorLightGreen;
		if(colorLightGreen){
			color="style=\"background-color:rgb(242, 250, 250);text-align:center;\"";
		}
		else{
	color="style=\"background-color:white;text-align:center;\"";
		}
	%>					<tr <%=color %>>
							<td><%= count+1 %></td>
							<td><a	href="control?action=registration&event=lostandFoundSummary&detailsForLAF=YES&module=operationView&lostandFoundID=<%= m_lostandFoundBO.getLfKey() %>" onclick="showProcess()">
							<%= m_lostandFoundBO.getTripId() %></a></td>
							<td><%= m_lostandFoundBO.getItemName() %></td>							
							<td><%= m_lostandFoundBO.getLfdate() %></td>
							<td><%= m_lostandFoundBO.getLftime() %></td>
							<%} %>
						</tr>
						<% } %>
					</table>
				</td>
			</tr>

		</table>

	<div class="footerDV">Copyright &copy; 2010 Get A Cab</div>

</body>
</html>
