<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>DriverRating</title>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript">
	function getDriverRating() {
		var dId = document.getElementById("driverId").value;
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'RegistrationAjax?event=driverRatingDetails&dId=' + dId;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var response = xmlhttp.responseText;
		var jObj = "{\"listArray\":" + response + "}";
		var list = JSON.parse(jObj.toString());
		document.getElementById("nameTable").style.display = "block";
		document.getElementById("driverName").style.display = "block";
		var table = $('#driverRatingTbl');
		document.getElementById("driverName").innerHTML = "";
		document.getElementById("driverRatingTbl").innerHTML = "";
		$('#driverName').append('<tr><td>Driver Name :</td><td>'+list.listArray[0].NM+'</td></tr>');
		table.append('<tr style="background-color:lime"><td>Trip Id</td><td>Customer Name</td><td>Date</td><td>Rating</td><td>Comments</td></tr>');
		for (var i = 0; i < list.listArray.length; i++) {
			if (i % 2 == 0) {
				table.append('<tr style="background-color:white"><td>' + list.listArray[i].TI + '</td><td>' + list.listArray[i].CN + '</td><td>' + list.listArray[i].DT + '</td><td>' + list.listArray[i].RT + '</td><td>' + list.listArray[i].CMT + '</td></tr>');
			} else {
				table.append('<tr style="background-color:lightblue"><td>' + list.listArray[i].TI + '</td><td>' + list.listArray[i].CN + '</td><td>' + list.listArray[i].DT + '</td><td>' + list.listArray[i].RT + '</td><td>' + list.listArray[i].CMT + '</td></tr>');
			}
		}
	}
</script>
</head>
<body>
	<form name="driverRating" action="control">
		<div align="center">
			<h2>Driver Rating</h2>
			<table width="100%" border="0" cellspacing="1" cellpadding="">
				<tr align="center">
					<td><h4>Driver Id :</h4></td>
					<td><input type="text" name="driverId" id="driverId" value=""
						autocomplete="off" /> <ajax:autocomplete fieldId="driverId"
							popupId="model-popup1" targetId="driverId"
							baseUrl="autocomplete.view" paramName="DRIVERID"
							className="autocomplete" progressStyle="throbbing" /></td>
				<td><input type="Button" name="submitDriverId" value="Search"
						onclick="getDriverRating()" /></td>
				</tr>
			</table>
			<div id="driverName" style="display: none;font-weight: bold;font-style: italic;"></div>
			<table width="100%" border="0" cellspacing="2" cellpadding="" style="display: none" id="nameTable">
			</table>
			<div id="driverRatingTbl123">
				<table width="600" cellspacing="2" border="1" align="center" id="driverRatingTbl">
				</table>
			</div>
		</div>
	</form>
</body>
</html>