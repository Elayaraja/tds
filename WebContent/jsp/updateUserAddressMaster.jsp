<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.Address" %>
<%@page import="com.tds.cmp.bean.CustomerProfile" %>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverVehicleBean"%>
<%@page import="java.util.Iterator"%>
<%@page import= "java.util.Map"%>
<%@page import= "java.util.Set"%>


<% CustomerProfile  UserAddressMaster = (CustomerProfile)request.getAttribute("UserAddressMaster");   %>



<html>
<head>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update User  Address</title>
<script type="text/javascript">
function formatAccountNumber(){
	var accountValue=document.getElementById("txtaccountNo").value;
	var split=accountValue.split("(");
	account=split[0];
	document.getElementById("accountNo").value=account;
}
$(document).ready ( function(){
	var drFlag=document.getElementById('FlagCustomer').value;
	if(drFlag!=null && drFlag!=""){
		var driverList=document.getElementById("drprofileSize").value;
		var vehicleList=document.getElementById("vprofileSize").value;
		if(drFlag!=""){
			for (var i=0; i<driverList;i++){
				if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
					document.getElementById('dchk'+i).checked=true;
				}
			}
			for (var i=0;i<vehicleList;i++){
				if(drFlag.indexOf(document.getElementById('vchk'+i).value) >= 0){
					document.getElementById('vchk'+i).checked=true;
				}
			}
		}
	}
});
</script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
</head>
<body>
<form  name="masterForm"  action="control" method="post" onsubmit="formatAccountNumber()">

   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   
   ArrayList<DriverVehicleBean> dvList=new ArrayList<DriverVehicleBean>();
   HashMap<String,DriverVehicleBean> companyFlags=(HashMap<String,DriverVehicleBean>) request.getAttribute("companyFlags");
   Set hashMapEntries = companyFlags.entrySet();
   Iterator it = hashMapEntries.iterator();
   while(it.hasNext()){
   	Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
   	dvList.add(companyFlag.getValue());
   }

   %>
   
   <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
   <input type="hidden" name="action" value="registration"/>
   <input type="hidden" name="event" value="custInfo"/>
   <input type="hidden" id="FlagCustomer" value="<%=UserAddressMaster.getDrProfile() %>"/>

   <c class="nav-header"><center>Update Master Address</center> </c>
 
                       <table width="60%" border="0" cellspacing="0" cellpadding="0" align="center">
                         <tr>
                    	  <td width="25%" >Phone No:</td>
                    	  <td>  
                    	    <input type="text"  name="Number"  id="Number" value="<%=UserAddressMaster.getNumber() %>" />
                    	  </td></tr>
                    	  <tr>
                    	  <td>Name</td>
                    	  <td>  
                    	    <input type="text"  name='Name'  id="Name" value="<%=UserAddressMaster.getName() %>"/>
                    	 </td>
                       	</tr>
                     	<tr>
                      	<td>Address1:</td>
                    	  <td>  
                    	    <input type="text"  name="add1"  id="add1" value="<%=UserAddressMaster.getAdd1() %>" />
                    	  </td></tr>
                    	  <tr>
                    	  <td>Address2:</td>
                    	  <td>  
                    	    <input type="text"  name='add2'  id="add2" value="<%=UserAddressMaster.getAdd2() %>" />
                    	 </td> 
                     </tr>       
                      <tr>
                      <td>City</td>
                    	  <td>  
                    	    <input type="text"  name="city"  id="city" value="<%=UserAddressMaster.getCity() %>" />
                    	  </td></tr>
                    	  <tr><td width="20%">State</td>
                    	  <td>  
                    	    <input type="text"  name='state'  id="state" value="<%=UserAddressMaster.getState() %>" />
                    	 </td> 
                     </tr>       
                    <tr>                    
                    <tr>
                      <td>Zip</td>
                    	  <td>  
                    	    <input type="text"  name="zip"  id="zip" value="<%=UserAddressMaster.getZip() %>" />
                    	  </td>
                    	 <input type="hidden" name="masterKey" id="masterKey" value="<%=UserAddressMaster.getMasterKey() %>"/>
                       </tr><tr>   
                      <td>Account&nbsp;No:</td>
                      <td>
                      	<input type="text"  name="account"  id="account" value="<%=UserAddressMaster.getAccount() %>"  />
                     	<ajax:autocomplete 
  					fieldId="account"
  					popupId="model-popup1"
  					targetId="account"
  					baseUrl="autocomplete.view"
  					paramName="getAccountList"
  					className="autocomplete"
  					progressStyle="throbbing"/>
                      	 <!-- -<input type="hidden"  name="account"  id="account" value="" /> -->
                    	  </td>
                    	  </tr>  
                    	  <tr>
                    	  <td>Payment type:</td>
                    	  <td>
                    	  <input type="text" name="paymentType" id="paymentType" value="<%=UserAddressMaster.getPaymentType() %>"/>
                    	  </td></tr><tr>
                    	  <td>Premium Customer</td>
                    	  <td>
                    	  <input type="checkbox" name="premiumCust" id="premiumCust" value="<%=UserAddressMaster.getPremiumcustomer()%>" <%=UserAddressMaster.getPremiumcustomer().equals("1")?"checked":"" %> />
                    	  </td>
                    	  </tr>
                    	  <tr>
                    	   <td>Driver Comments:</td>
                    	  <td>
                    	  <textarea rows="2" cols ="30" name="comments" id="comments"><%=UserAddressMaster.getOperComments() %></textarea>
                    	  </td></tr>
                    	  <tr>
                    	   <td>Operator Comments:</td>
                    	  <td>
                    	  <textarea rows="2" cols ="30" name="dispcomments" id="dispcomments"><%=UserAddressMaster.getComments() %></textarea>
                    	  </td></tr>
                    	  <tr>
                    	<%if(dvList!=null &&  dvList.size()>0) {%>
						<td width="20%">Special Request</td>
								<%int driverCounter=0;
								  int vehicleCounter=0;%>
								<td>
								<%for(int i=0;i<dvList.size();i++){ %>
								<%if(dvList.get(i).getDriverVehicleSW()==1){ %>
									<input type="checkbox"
												name="dchk<%=driverCounter%>" id="dchk<%=driverCounter%>" value="<%=dvList.get(i).getKey()==null?"":dvList.get(i).getKey()%>" /><%=dvList.get(i).getShortDesc() %>
 									<%driverCounter++; %>
 								<%}else if(dvList.get(i).getDriverVehicleSW()==2) { %>
									<input type="checkbox"
												name="vchk<%=vehicleCounter %>" id="vchk<%=vehicleCounter%>" value="<%=dvList.get(i).getKey()==null?"":dvList.get(i).getKey() %>" /><%=dvList.get(i).getShortDesc()%>
									
									<%vehicleCounter++; %>
 											<%} %>
											<%} %>
 											</td>
									<input type="hidden" name="drprofileSize" id="drprofileSize"
												value="<%=driverCounter%>" />
									<input type="hidden" name="vprofileSize" id="vprofileSize"
												value="<%=vehicleCounter%>" />			
							
					<%} %>
                    </tr>
                    <tr>
                          <td>Mail Id</td>
                    	  <td>
                    	  <input type="text" name="mailId" id="mailId" value="<%=UserAddressMaster.geteMail()==null?"":UserAddressMaster.geteMail() %>" />
                    	  </td>
                    </tr>
                    <tr>
                    <td colspan="2" align="center">
						 	  <input type="submit"  name="UpdateAddMaster" id="UpdateAddMaster"   value="Update" />
                         	  <input type="submit"  name="UserAddMasterPage" id="UserAddMasterPage"  value="Create User" />
                         	  <input type="submit"  name="cancel" id="cancel"  value="Cancel" />
                    </td>
				    </tr> 
                </table>
</form>
</body>
</html>
