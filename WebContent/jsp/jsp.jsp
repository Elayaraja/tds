<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.OpenRequestBO" %>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<jsp:useBean id="openBO"   class="com.tds.tdsBO.OpenRequestBO"  />
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 
<%
	ArrayList al_veList = (ArrayList)request.getAttribute("vecFlag");
	ArrayList al_drList = (ArrayList)request.getAttribute("drFlag");;

%>
<script type="text/javascript">

//document.onkeyup = KeyCheck;  



function popUp(row_id) { 
//	var url = '/TDS/AjaxClass?event=viewOpenRequestDetails&tripid='+row_id;
	var urll ='/TDS/AjaxClass?event=openrequestDetails&module=dispatchView&acceptrequest=Change Open Request&tripid='+row_id; 
	window.open(urll, "",'screenX=0,screenY=0,toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=1100,height=800'); 
}


function reqDetails(id) {
	alert("this"+id+"end");
}

function KeyCheck(e,previous,next)  {  

   var KeyID = (window.event) ? event.keyCode : e.keyCode; 
   if((keyID = 38) || (keyID = 40)) { 
   	switch(KeyID) 
   	{  
      case 38:  
      document.getElementById(previous).focus(); 
      break; 
      
      case 40: 
      document.getElementById(next).focus(); 
      break;
   } 
}
}
</script>
 

<% if(request.getAttribute("openrequest") != null) {
		openBO = (OpenRequestBO)request.getAttribute("openrequest");
	%>
<script type="text/javascript">
			document.masterForm.sdate.value = "";
			document.masterForm.shrs.value = "";
			document.masterForm.smin.value = "";
			
		</script>
	<% } %>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
<script language="javascript">
function loadDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	//alert(cMonth+"/"+cDate+"/"+cYear);
	<%
	if(openBO.getSdate().length() <= 0) {
	%>
		document.masterForm.sdate.value = cMonth+"/"+cDate+"/"+cYear;
	<% 
	}
	  %>
 }


function openDrop() { 
	if(document.getElementById("smsStatus").checked) {  
		document.getElementById("smsList").style.display='block'; 
	}else {
		document.getElementById("smsList").style.display='none';
		document.getElementById("toSms").value="0";
	}
}
function closePopup() {
	alert("close window");
	window.close();
	
}
	

</script>
</head>
<%
String status[] = {"Request","In Process","Completed"};
%>
<body onload="loadDate()">
<form name="masterForm" action="control" method="post">

<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	
%>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<jsp:setProperty name="openBO"  property="*"/>

<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>"/>
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveOpenRequest %>"/>
<input type="hidden" name="stlatitude" id="stlatitude"  value="<%= openBO.getSlat() %>"/> 
<input type="hidden" name="stlongitude" id="stlongitude" value="<%= openBO.getSlong() %>"/>
<input type="hidden" name="edlatitude" id="edlatitude" value="<%= openBO.getEdlatitude() %>"/>
<input type="hidden" name="edlongitude" id="edlongitude" value="<%= openBO.getEdlongitude() %>"/>

<input type="hidden" name="sesexit" id="sesexit" value="<%=adminBo==null?"0":"1"%>"/>
<input type="hidden" name="dispatch" value="1"/>

<% 	HashMap hmp_address;
	if(request.getAttribute("address") != null) {
		hmp_address = (HashMap)request.getAttribute("address");
		
  %>
 		<input type="hidden" name="stadd1" value="<%= hmp_address.get("stadd1").toString() %>"/>
 		<input type="hidden" name="stadd2" value="<%= hmp_address.get("stadd2").toString() %>"/>
 		<input type="hidden" name="stcity" value="<%= hmp_address.get("stcity").toString() %>"/>
 		<input type="hidden" name="ststate" value="<%= hmp_address.get("ststate").toString() %>"/>
 		<input type="hidden" name="stzip" value="<%= hmp_address.get("stzip").toString() %>"/>
 		<input type="hidden" name="edadd1" value="<%= hmp_address.get("edadd1").toString() %>"/>
 		<input type="hidden" name="edadd2" value="<%= hmp_address.get("edadd2").toString() %>"/>
 		<input type="hidden" name="edcity" value="<%= hmp_address.get("edcity").toString() %>"/>
 		<input type="hidden" name="edstate" value="<%= hmp_address.get("edstate").toString() %>"/>
 		<input type="hidden" name="edzip" value="<%= hmp_address.get("edzip").toString() %>"/>
<%
	}
%>

 
            <div class="leftCol">
                <div class="clrBth"></div>
                </div>  
            	              
                <div class="rightCal">
                <div class="rightColIn">
                         		<%
									String error ="";
									if(request.getAttribute("errors") != null) {
									error = (String) request.getAttribute("errors");
									}
								%>
								<div id="errorpage">
								<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %></div>
								
								
								<%String result="";
								if(request.getAttribute("page")!=null) {
									result = (String)request.getAttribute("page");
								}
								%>
								<div id="errorpage">
								<%=result.length()>0?"Sucessfully"+result:"" %>
								</div>
								
								
								
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<td>
					
					<table id="bodypage" width="100%" >
						<tr  >
							<td colspan="4"  align="center" ><font  size="4" > Open Request</font></td>
						</tr>
						<tr>
							<td class="firstCol" >Phone No</td>
							<td><input type="text"   name="phone"  id="phone" value="<%= openBO.getPhone()  %>" onkeyup="KeyCheck(event,'phone','nameId')" onblur="openPopup(this),openDetails(phone,'details')"   /></td>
							<td>
							 <div class="div3">
                                    	  <div class="btnBlue">
                                         <div class="rht">
                                         <input type="button" name="btnphone" class="lft" value="Check PhoneNo" tabindex="2" onclick="loadFromToAddr()"/>
                                         </div>
                                        </div> 
                                    </div>
                                   
							</td>
						</tr>
						<tr>
							<td class="firstCol" >Name</td>
							<td><input type="text"    id="nameId"    name="name"  value="<%= openBO.getName() %>"    onkeyup="KeyCheck(event,'phone','shrs')" /></td>
							<td>Don't Dispatch<input  type="checkbox" name="dispatchStatus"  style="vertical-align: middle"  />  </td>
						</tr>
						<tr>
							<td class="firstCol" >Service Date</td>
							<td><input readonly="readonly" type="text"   name="sdate"   id="sdate"  size="10"  value="<%= openBO.getSdate()  %>" onfocus="showCalendarControl(this);"/></td>
							<td>Sent Sms<input type="checkbox"  name="smsStatus"  id="smsStatus"  onclick="openDrop()" style="vertical-align: middle"   />   </td>
						</tr>
						<tr>
							<td class="firstCol" >Service Time</td>
							<td><input type="text"   id="shrs"   name="shrs"   maxlength="4"  size="4" value="<%= openBO.getShrs()   %>"   onkeyup="KeyCheck(event,'nameId','sintersection')"   /></td>
							<td>
							<div id="smsList"  style="display: none"  >
							   <select  name="toSms"  id="toSms"  >
							     <option value="0" >Select</option>
							     <option value="tm">Tmobile</option>
							     <option value="ve">Verizon</option>
							     <option value="sp">Sprint</option>
							     <option value="cr">Cricket</option>
							   </select>
							</div>
							</td>
						</tr>
 					</table  >
 					</td>
 					</tr> 
 					</table>
 					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
 					<tr>
 					<td>
 					<table  id="bodypage" width="700" align="left"  border="0" >
 						
 						<tr       >
 						<td colspan="2"    style="background-color:#FCD374" ><b>From Address</b></td>
 						<td colspan="2"  style="background-color:#FCD374"  ><b>To Address</b></td>
 						
 						 
 						</tr>
 						<tr   >
 							<td  class="firstCol">Intersection</td>
 							<td><input type="text" name="sintersection"     size="10"  id="sintersection"   value="<%= openBO.getSintersection()  %>"   onkeyup="KeyCheck(event,'shrs','slandmark')"  /></td>
 							 
 							
 							 
 						</tr>
 						<tr>
 							<td  class="firstCol">Land Mark</td>
 							<td><input type="text" name="slandmark"   size="10"  id="slandmark"   value="<%= openBO.getSlandmark()  %>"  onkeyup="KeyCheck(event,'sintersection','saircodeid')"/></td>
 							 
 						</tr>
<%--  						<tr>
 							<td   class="firstCol">Air Port Code</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                 	<input type="text" tabindex="9"  size="10" id="saircodeid" name="saircodeid"   value="<%= openBO.getSaircodeid() %>" autocomplete="off"   class="form-autocomplete"  onkeyup="KeyCheck(event,'slandmark','chekcsource')"/>
                                    <input type="hidden" id ="saircode" name = "saircode" value="<%= openBO.getSaircode() %>"/>
                                    <ajax:autocomplete
  									fieldId="saircodeid"
  									popupId="model-popup1"
  									targetId="saircode"
  									baseUrl="autocomplete.view"
  									paramName="aircode"
  									className="autocomplete"
  									progressStyle="throbbing"/>
  							 		</div></div></div><br>
  							 		<div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btnsaircode" class="lft"   id="chekcsource"  value="ChkSrceArcde"   onkeyup="KeyCheck(event,'saircodeid','sadd1')"   />
                                         </div>
                                        </div>
                                    </div>
  							 		
  							 		</td>
 							 
 							<td class="firstCol" >To Air Port Code</td>
 							<td>
 								 <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" tabindex="16"   size="10" id="eaircodeid" name="eaircodeid" class="inputBX" value="<%= openBO.getEaircodeid() %>" autocomplete="off"   class="form-autocomplete"     onkeyup="KeyCheck(event,'szip','destination')"/>
									<input type="hidden" id ="eaircode" name = "eaircode" value="<%= openBO.getEaircode() %>"/>
									<ajax:autocomplete
  									fieldId="eaircodeid"
  									popupId="model-popup2"
  									targetId="eaircode"
  									baseUrl="autocomplete.view"
  									paramName="aircode"
  									className="autocomplete"
  									progressStyle="throbbing" />
									</div></div></div>
									<br>
									<div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btneaircode"   id="destination" class="lft" value="ChkDstinArcde" tabindex="17"  onkeyup="KeyCheck(event,'eaircodeid','eadd1')"  />
                                        </div>
                                        </div>
                                    </div>
 							</td>
 						 
 						</tr>
 --%> 						<tr>
 							<td  class="firstCol">Address 1</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text"   id="sadd1" name="sadd1"   size="10"   value="<%=openBO.getSadd1()  %>" class="form-autocomplete" onblur="loadOtherData()"  onkeyup="KeyCheck(event,'chekcsource','sadd2')" />
                                    <ajax:autocomplete
				  					fieldId="sadd1"
  									popupId="model-popupsadd1"
  									targetId="sadd1"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress1"
  									className="autocomplete"
  									progressStyle="throbbing" /> 
  			                         </div></div></div></td>
  			                          
 							<td class="firstCol">Address 1</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text"   size="10"  name="eadd1"   id="eadd1" value="<%= openBO.getEadd1()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"  onkeyup="KeyCheck(event,'destination','eadd2')"/>
									<ajax:autocomplete
  									fieldId="eadd1"
  									popupId="model-popupeadd1"
  									targetId="eadd1"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress1"
  									className="autocomplete"
  									progressStyle="throbbing"/>
                                    </div></div></div></td>
 						 
 							
 						</tr>
 						<tr>
 							<td   class="firstCol"> Address 2</td>
 							<td> <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="sadd2"   size="10"  id="sadd2"    value="<%= openBO.getSadd2()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()"  onkeyup="KeyCheck(event,'sadd1','scity')"/>
									<ajax:autocomplete
  									fieldId="sadd2"
  									popupId="model-popupsadd2"
  									targetId="sadd2"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress2"
  									className="autocomplete"
  									progressStyle="throbbing"/>
		                            </div></div></div></td>
		                             
 							<td class="firstCol">Address 2</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="eadd2"   size="10"  id="eadd2"    value="<%= openBO.getEadd2()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"  onkeyup="KeyCheck(event,'eadd1','ecity')"/>
									<ajax:autocomplete
  									fieldId="eadd2"
  									popupId="model-popupeadd2"
  									targetId="eadd2"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress2"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div></td>
 						 
 						</tr>
 						<tr>
 							<td  class="firstCol">City</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="scity"   id="scity"   size="10"   value="<%= openBO.getScity()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()"   onkeyup="KeyCheck(event,'sadd2','sstate')"/>
									<ajax:autocomplete
  									fieldId="scity"
  									popupId="model-popupscity"
  									targetId="scity"
  									baseUrl="autocomplete.view"
  									paramName="phonecity"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
  									</div></div></div></td>
  								 
 							<td class="firstCol">City</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="ecity"    size="10"  id="ecity"   value="<%= openBO.getEcity()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()" onkeyup="KeyCheck(event,'eadd2','estate')" />
									<ajax:autocomplete
  									fieldId="ecity"
  									popupId="model-popupecity"
  									targetId="ecity"
  									baseUrl="autocomplete.view"
  									paramName="phonecity"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div></td>
 				 
 							 
 						</tr> 
 						<tr>
 							<td  class="firstCol" >State</td>
 							<td> <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="sstate"   size="10"  id="sstate"   value="<%= openBO.getSstate()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()"  onkeyup="KeyCheck(event,'scity','szip')" />
									<ajax:autocomplete
  									fieldId="sstate"
  									popupId="model-popupsstate"
  									targetId="sstate"
  									baseUrl="autocomplete.view"
  									paramName="phonestate"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
                                   	 </div></div></div></td>
                            
 							<td class="firstCol">State</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="estate"   size="10"   id="estate"   value="<%= openBO.getEstate()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"  onkeyup="KeyCheck(event,'ecity','ezip')" />
									<ajax:autocomplete
  									fieldId="estate"
  									popupId="model-popupestate"
  									targetId="estate"
  									baseUrl="autocomplete.view"
  									paramName="phonestate"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div></td>
 					 
 						</tr>
 						<tr>
 							<td  class="firstCol">Zip</td>
 							<td> <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="szip"   size="10"  id="szip"   value="<%= openBO.getSzip()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()" onkeyup="KeyCheck(event,'sstate','eaircodeid')" />
									<ajax:autocomplete
  									fieldId="szip"
  									popupId="model-popupszip"
  									targetId="szip"
  									baseUrl="autocomplete.view"
  									paramName="phonezip"
  									className="autocomplete"
  									progressStyle="throbbing"/>
  									</div></div></div>
 									<% if(session.getAttribute("user") != null) {  %>
									<%
										ArrayList al_q = (ArrayList)request.getAttribute("al_q");
									%></td>
							 
 							<td class="firstCol">Zip</td>
 							<td><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="ezip"  size="10" id="ezip" tabindex="22" class="inputBX" value="<%=openBO.getEzip()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()"  onkeyup="KeyCheck(event,'estate','btnfromaddress')" />
									<ajax:autocomplete
  									fieldId="ezip"
  									popupId="model-popupezip"
  									targetId="ezip"
  									baseUrl="autocomplete.view"
  									paramName="phonezip"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div>
                                    <br>
                                    <div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btnfromaddress"  id="btnfromaddress"   class="lft" value="ChkFromAddress"   onkeyup="KeyCheck(event,'ezip','checktoadd')"  />
                                         </div>
                                        </div>
                                    </div>
                                    </td> 
 						</tr>
 						<tr>
 							<td  class="firstCol">Zone</td>
 							<td><div class="div5"><div class="div5In">
	 								<select name='queueno' id='queueno'     onkeyup="KeyCheck(event,'szip','eaircodeid')"     >
	 								<option value=''>Select</option>
									<%
										if(al_q!=null){
										for(int i=0;i<al_q.size();i=i+2){ 
									%>
										<option value='<%=al_q.get(i).toString() %>' <%=openBO.getQueueno().equals(al_q.get(i).toString())?"selected":"" %>><%=al_q.get(i+1).toString() %></option>
									<%
									}
									} %>
									</select>
									</div></div> 
									<% } else{  %> 
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap">
                                    <input type="hidden" name="queueno" id="queueno" value="<%=openBO.getQueueno() %>"/></div></div></div>
                                    <%} %>
                                    <br>
                                    <div class="div3">
									<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="button"   id="getzone" name="getzone" class="lft" value="Get Zone" tabindex="" onclick='loadQueue()' /></div>
                                        </div>
                                    </div>
                                    </td>
 							 <td></td>
 						 
 							<td><div class="div3">
                                    	<div class="btnBlueNew1 padT10 ">
                                         <div class="rht">
                                         <input type="submit" name="btntoaddress"   id="checktoadd"  class="lft" value="ChkToAddress"      onkeyup="KeyCheck(event,'btnfromaddress','submit')" />
                                         </div>
                                    </div></div></td>
 						</tr>
 						<tr >
 							<td>Special Instruction</td>
 							<td  colspan="2" ><textarea rows="3"    name="specialIns"   id="specialIns"  cols="50"></textarea> 
 							</td>
 						</tr>
 						<tr>
 							<td>Special Request</td>
 							<td>
 								<table>
 									<TR><TD>Driver Profile</TD><TD>Vehicle Profile</TD></TR>
 									<TR>
 										<TD>
 											<table>
 											<%if(al_drList!=null && !al_drList.isEmpty()) {%>
												<th><td><input type="hidden" name="drprofileSize" value="<%=al_drList.size()/2 %>"/></td> </th>
												<%for(int i=0,j=0;i<al_drList.size();i=i+2,j++){ %>
													<tr>
														<td>
															<input type="checkbox"
																<%
																	 if(request.getParameter("dchk"+j)!=null)
																	 {
																		 out.print("checked");
																	 } else {
																		 if(openBO.getAl_drList()!=null)
																		 {
																			 for(int k=0;k<openBO.getAl_drList().size();k++)
																			 {
																				 if(openBO.getAl_drList().get(k).toString().equals(al_drList.get(i+1)))
																				 {
																					 out.print("checked");
																					 break;
																				 }
																			 }
																		 }
																	 }
																
																%>
															 name="dchk<%=j %>" value="<%=al_drList.get(i+1) %>"/><%=al_drList.get(i) %>
														</td>							
													</tr>
												<%} %>
											<%} %>
											</table>
 										</TD>
 										<TD>
 											<table>
 											<%if(al_veList!=null && !al_veList.isEmpty()) {%>
												<th><td><input type="hidden" name="vprofileSize" value="<%=al_veList.size()/2 %>"/></td> </th>
												<%for(int i=0,j=0;i<al_veList.size();i=i+2,j++){ %>
													<tr>
														<td>
															<input type="checkbox"
																<%
																	 if(request.getParameter("vchk"+j)!=null)
																	 {
																		 out.print("checked");
																	 } else {
																		 if(openBO.getAl_vecList()!=null)
																		 {
																			 for(int k=0;k<openBO.getAl_vecList().size();k++)
																			 {
																				 if(openBO.getAl_vecList().get(k).toString().equals(al_veList.get(i+1)))
																				 {
																					 out.print("checked");
																					 break;
																				 }
																			 }
																		 }
																	 }
																
																%>
															 name="vchk<%=j %>" value="<%=al_veList.get(i+1) %>"/><%=al_veList.get(i) %>
														</td>							
													</tr>
												<%} %>
											<%} %>
											</table>
 										</TD>
 									</TR>
 								</table>
 							</td>
 						</tr>
 						
 							<tr>
				<td colspan="6" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	   <input type="submit" name="submit" class="lft" value="Submit"   id="submit"     onkeyup="KeyCheck(event,'checktoadd','submit')" />
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
				</td>
			</tr> 
 						 
 					</table>
 					<table  id="bodypage" width="300" align="right"  border="0" >
 						<tr>
 							<td   height="155px"      valign="top" >
 								<div   id="details"      style='position:absolute; width:450px; height: 150px; overflow:auto' > </div>
 							</td>
 						</tr>
 					</table>
					</td>
					</tr>
					</table>
								 
                </div>    
              </div>
            	
                <div class="clrBth"></div>
       
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
