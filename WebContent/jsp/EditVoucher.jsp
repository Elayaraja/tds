<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<%@page import="com.tds.tdsBO.VoucherBO,java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.common.util.TDSConstants;"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script src="js/CalendarControl.js" type="javascript"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 
<script>
function grantTheAcessToAssistant(){
	//var allInputs = $("[type=checkbox]");
	var oRows=document.getElementById("vouchersTable").getElementsByTagName("tr");
	var iRowCount= oRows.length;
	var z1 ="";
	for(var i=1;i<iRowCount;i++){
		if(oRows[i].getElementsByTagName("input")[0].checked){
		 z1 = z1 + oRows[i].getElementsByTagName("input")[0].value +";";
		}
	}
	var email = document.getElementById("grantUser").value;
	if(z1 !=null&&z1!=""&&email!=null&&email!=""){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ="finance?event=grantAccessToAssistant&emailId="+email+"&accountNo="+z1;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="true"){
		alert("permission granted for the userID");
		document.getElementById("grantUser").value="";
	}
	}else{
		alert("Oops,Check Your AccountNo and EmailID !!!!");
	}
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}

</script>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var voucherprocess = document.getElementById("voucherprocess");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>Voucher process Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(voucherprocess.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
    
    
<style>
h2{
font-family:italic;
color:grey;
}
#foo{
font-weight:bold;
color:blue;
}
</style>
</head>

<body>
<form method="post" action="control" name="masterForm" style="margin-right: 170px;" onsubmit="return showProcess()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<jsp:useBean id="voucherBo" class="com.tds.tdsBO.VoucherBO" scope="request"/>
<% if(request.getAttribute("voucherentry") != null)
	voucherBo = (VoucherBO)request.getAttribute("voucherentry");
	%>
<jsp:setProperty name="voucherBo" property="*" />
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.getAllVoucher %>">
<input type="hidden" name="operation" value="3">
 
            	<div>
                	
                    <div></div>
                </div>
                
                <div>
<div>
                	<h2><center>Change Account Entry</center></h2>
                	<%
						String error ="";
						if(request.getAttribute("errors") != null) {
						error = (String) request.getAttribute("errors");
						}		
				  %>
				  <%= error.length()>0?"You must fulfilled the following error<br>"+error :"" %>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                    <tr>
                    <td>
                    <table id="bodypage" width="840" >
                    <tr>
                    <td >Voucher&nbsp;No:</td>
                    <td><input type="text" autocomplete="off" name="vno" id="vno" value="<%= voucherBo.getVno()%>" /> 
                            <ajax:autocomplete 
  					fieldId="vno"
  					popupId="model-popup1"
  					targetId="vno"
  					baseUrl="autocomplete.view"
  					paramName="getAccountList"
  					className="autocomplete"
  					progressStyle="throbbing" />
  							<input type="hidden" name="vno" id="vno" value="<%=voucherBo==null?"":voucherBo%>"/>
                    </td>
                    <td >Rider&nbsp;Name:</td>
                    <td>
                    <input type="text"  name="vname" value="<%= voucherBo.getVname() %>"></input>
                    </td>
                    </tr>
                    
                    <tr>
                    <td >From&nbsp;Date:</td>
                    <td>
                    <input type="text" name="from_date"  value="<%=voucherBo.getFrom_date() %>" onfocus="showCalendarControl(this);"  readonly="readonly">
                    </td>
                    <td >To&nbsp;Date:</td>
                    <td>
                    <input type="text" name="to_date"  value="<%=voucherBo.getTo_date() %>" onfocus="showCalendarControl(this);"  readonly="readonly">
                    </td> 
                    </tr>
                    
                    <tr>
                    <td  >Cost&nbsp;Center:</td>
                    <td><input type="text"  name="vcostcenter" value="<%= voucherBo.getVcostcenter()  %>" />
                    </td> 
                    </tr>
                    
                    <tr>
                    <td colspan="7" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="submit" name="submit" value="Show Vouchers" class="lft"/>
                        	 </div>
                            <div ></div>
                        </div>
                        <div ></div>
                    </div>	
                    </td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    </table>
                    </div>
                    </div>
                    
                     <div>
			<div>
            
             <%if(request.getAttribute("summary")!=null){ 
			ArrayList voucher_data=(ArrayList)request.getAttribute("summary");
			%>      
			<table width="100%" id="voucherprocess" border="1"  cellpadding="2"  style="background-color:white"> 
                      <tr>
                        <th  width="10%" style="background-color:rgb(217, 220, 243)" >S.No</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)">Voucher No</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)">Company Name</th>
                        <th width="10%"style="background-color:rgb(217, 220, 243)" >Voucher Name</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)">Cost Center</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)">Status</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)"> Contact Info</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)"> Contact No.</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)"> Use</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)"> Exp.Date</th>
                        <th width="10%" style="background-color:rgb(217, 220, 243)"> Delay Days</th>
                        <th width="10%" style="background-color:lightgrey"></th>
                      
                      </tr>
                      <%for(int count=0;count<voucher_data.size();count++){
						voucherBo = (VoucherBO)voucher_data.get(count); %>
						
					<tr >
						<td align="center"><%=count+1%></td>
						<td align="center"><%-- <%if(voucherBo.getVstatus().equalsIgnoreCase("P")){ %> --%><a href="control?action=registration&event=getAllVoucher&event1=editVoucher&module=financeView&vno=<%=voucherBo.getVno()%>" style="text-decoration: none"> <%-- <%}%> --%> <%=voucherBo.getVno()%><%-- <%if(voucherBo.getVstatus().equalsIgnoreCase("P")){ %> --%></a><%-- <%} %> --%></td>
						<td align="center"><%=voucherBo.getCompanyName()==null?"":voucherBo.getCompanyName()%></td>
						<td align="center"><%=voucherBo.getVname() %></td>
						<td align="center"><%=voucherBo.getVcostcenter() %></td>
						<td align="center"><%=voucherBo.getVstatus().equals("1")?"Active":"InActive"%></td>
						<td align="center"><%=voucherBo.getVcontact() %></td>
						<td align="center"><%=voucherBo.getTelephoneNumber() %></td>
						<td align="center"><%=voucherBo.getVfreq().equals("1")?"Single Use":"Multiple Uses" %></td>
						<td align="center"><%=voucherBo.getVexpdate().equals("12/12/9999")?"Never":voucherBo.getVexpdate().equals("12/31/9999")?"Never": voucherBo.getVexpdate()%></td>
						<td align="center"><%=voucherBo.getVdelay().equals("-1")?"On Receipt Of Payment":voucherBo.getVdelay() %></td>
						<td align="center"><input type="checkbox" name="voucherSelected" id="voucherSelected<%=count%>" value="<%=voucherBo.getVno()%>"></input></td>
				
				 </tr>
				<%} %>		
                      </table>
                      <%} %>                    
                </div>    
              </div>
        </form>    	
           <input type="button" value="Save/Print" id="btnPrint" />
             
        <div id="grantAssistant">
               <form id="assistantAccess">   
              <input type="email" name="grantUser" id="grantUser" value="" placeholder="Enter the Assistants Email ID to Grant the Access!" title="Enter Email Id Of Asssistant Operator to Grant the Access of Selected Vouchers"  required="required" style="width:30%" />
              <input type="button" value="Grant Access To Selected Vouchers" onclick="grantTheAcessToAssistant()">
              </form>
              </div>
        
            
            <footer>Copyright &copy; 2010 Get A Cab</footer>
 

</body>
</html>
