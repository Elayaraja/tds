<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.tdsBO.DriverBehaviorBO,com.common.util.TDSConstants"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%! 
	String m_behaviorName[] =  {"Penalty", "Bonus" };
	String m_behaviorType[] = {"BP","BB"};
	int typeCount = 0;
%>
<jsp:useBean id="behaviorBo" class="com.tds.tdsBO.DriverBehaviorBO" scope="request"/>
<% if(request.getAttribute("driverBehavior") != null)
	behaviorBo = (DriverBehaviorBO)request.getAttribute("driverBehavior");

%>
<html>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<head>
<script type="text/javascript">
function loadDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	//alert(document.behavior.bdate.value);
	if(document.behavior.bdate.value == "" || document.behavior.bdate.value ==  "null") {
		document.behavior.bdate.value = cMonth+'/'+cDate+'/'+cYear
	}
	
}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body onload="loadDate()">
	<form method="post" action="control" name="behavior">


<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.createDrvierBehavior %>">
<input type="hidden" name="behaviorNo" value="<%= behaviorBo.getBehaviorKey() %>">
<table id="pagebox">
	<tr>
		<td>
			<table id="bodypage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Driver&nbsp;Behavior</b></td>
			</tr>
			<tr>
			<%
			String error ="";
			if(request.getAttribute("errors") != null) {
				error = (String) request.getAttribute("errors");
			}		
			%>
			<td colspan="7">
				<div id="errorpage">
					<%= error.length()>0?"You must fulfilled the following error<br>"+error :"" %>
				</div>
			</td>
			</tr>
			<tr>
				<td>Behavior&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  size="10" name="bdate" value="<%= behaviorBo.getBehaviorDate()  %>"  >
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.behavior.bdate);return false;" HIDEFOCUS>
					<img name="popcal" align="absmiddle" src="images/calendar.gif" width="25" height="16" border="0" alt=""></a>		
					<iframe width=20  height=178  name="gToday:normal:agenda.jssdate" id="gToday:normal:agenda.jssdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
					</iframe>
				</td>
				<td>&nbsp;</td>
				<td>Behavior&nbsp;Type</td>
				<td>&nbsp;</td>
				<td>
					<select name="btype" >
						<%
							for(typeCount = 0; typeCount < m_behaviorType.length; typeCount ++ ) {
								if(m_behaviorType[typeCount].equalsIgnoreCase(behaviorBo.getBehaviorType())) {
						 %>
									<option value="<%= m_behaviorType[typeCount] %>" selected="selected"> <%= m_behaviorName[typeCount] %> </option>
						<%
								} else {
						%>
									<option value="<%= m_behaviorType[typeCount] %>"> <%= m_behaviorName[typeCount] %> </option>
						<%
							} }
						%>
					</select>
				</td>
				
			</tr>
			<TR>
				<td>Driver&nbsp;Name</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  id="driver" name="driver" value="<%= behaviorBo.getDriverName() != null ? behaviorBo.getDriverName() : "" %>" autocomplete="off"   class="form-autocomplete" />
					<ajax:autocomplete 
  					fieldId="driver"
  					popupId="model-popup1"
  					targetId="driver"
  					baseUrl="autocomplete.view"
  					paramName="driver"
  					className="autocomplete"
  					progressStyle="throbbing" /> 
				</td>
				<td>&nbsp;</td>
				<td>Amount</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  name="bamount" value="<%= behaviorBo.getAmount() != null ? behaviorBo.getAmount() : "" %>">
				</td>

			</TR>
			<tr>
				<td>Reason</td>
				<td>&nbsp;</td>
				<td colspan = "4">
					<textarea name="bcomment" rows="5"  cols="25"><%= behaviorBo.getComment() != null ? behaviorBo.getComment() : ""   %></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="7" align="center">
					<%
						System.out.println(behaviorBo.getBehaviorKey());
					
					%>	
					<% if(behaviorBo.getBehaviorKey() != null && !behaviorBo.getBehaviorKey().equalsIgnoreCase("null")) { %>
						<input type="submit" name="update"  value="Update">
					<% } else { %>
						<input type="submit" name="save" value="Save" >
					<% } %>
				</td>
			</tr>
			</table>
		</td>
	</tr>

</table>
</form>
</body>
</html>