<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList,com.tds.tdsBO.DriverRegistrationBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.passengerBO" %>
<%! int startValue =1 ;double limit=0;int pstart=1;int pend=0;int pageNo=1;int plength=0; 
String first="";
String last="";
String passengerId="";
String city="";
%>
<%if(request.getAttribute("startValue") != null){
	startValue = Integer.parseInt((String)request.getAttribute("startValue"));
	startValue=startValue+1;
}	
if(request.getAttribute("plength")!=null){
	plength=Integer.parseInt((String)request.getAttribute("plength"));
	}

if(request.getAttribute("f_name")!=null){
	first= (String)request.getAttribute("f_name");
	}
if(request.getAttribute("l_name")!=null){
	last= (String)request.getAttribute("l_name");
	}
if(request.getAttribute("pasrId")!=null){
	passengerId= (String)request.getAttribute("pasrId");
	}
if(request.getAttribute("pasrCity")!=null){
	city= (String)request.getAttribute("pasrCity");
	}


%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS (Taxi Dispatch System)</title>
</head>
<body>
<form name="driverSummary"  action="control"   method="post">
<input type="hidden"  name="action"  value="registration"  />
<input  type="hidden" name="event"  value="passengerSummary"/>

<input  type="hidden"  name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
 
                
                <div class="rightCol">
<div class="rightColIn">
                	<h1>Passenger  Summary</h1>
                	<%
						String error ="";
						if(request.getAttribute("errors") != null) {
						error = (String) request.getAttribute("errors");
						}
					%>
					<% %>
					<div id="errorpage">
						<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %>
					</div>
					<table width="100%"  border="0" cellpadding="0"  class="driverChargTab" >
					<tr>
					 <td><b>First Name</b></td>
					 <td> <input  type="text"     name="first_name"  value="<%=first %>" />  </td>
					 <td><b>Last Name</b></td>
					 <td> <input  type="text"     name="last_name"  value="<%=last %>"  />  </td> 
					</tr>
					<tr>
					<td><b>Passenger Id</b></td>
					 <td> <input  type="text"     name="passID"    value="<%=passengerId %>" />  </td>
					 <td><b>city</b></td>
					 <td> <input  type="text"     name="passCity"    value="<%=city %>" />  </td>
					</tr>
					<tr   align="center"  >
					
					<td colspan="4"  align="center" >
					
					<input  type="submit"      name="Button"   value="Search"      />   </td>
					</tr>
					</table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                     <tr>
                        <th width="15%" class="firstCol">S.No</th>
                        <th width="30%">First Name</th>
                        <th width="20%">Last Name</th>
                        <th width="20%">Passenger Id</th>
                        <th width="20%">City</th>
                        <th width="20%">State</th>
                        <th width="20%">Zip</th>  
                        <th width="20%">Details</th>
                      </tr>
                      
                      <%
                      passengerBO passenger = new passengerBO();
						ArrayList al_driverData=new ArrayList();
						if(request.getAttribute("passengerdata")!=null){
						al_driverData=(ArrayList)request.getAttribute("passengerdata");
						for(int r_count=0;r_count<al_driverData.size();r_count++){
							 
							 
							 
				    %>
				   <%if(r_count%2==0){ %>
                      <tr>
	                        <td ><%=r_count+startValue %></td>
	                        
                        	<td ><%=((passengerBO)al_driverData.get(r_count)).getFname()  %></td>
                         <td>
                           <%=((passengerBO)al_driverData.get(r_count)).getLname()%>
                       </td>
                        <td>&nbsp;<%=((passengerBO)al_driverData.get(r_count)).getUid()%>&nbsp;</td>
                          <td>&nbsp;<%=((passengerBO)al_driverData.get(r_count)).getCity()  %>&nbsp;</td>
                          <td>&nbsp;<%=((passengerBO)al_driverData.get(r_count)).getState()  %>&nbsp;</td>
                          <td>&nbsp;<%=((passengerBO)al_driverData.get(r_count)).getZip()  %>&nbsp;</td>
                         <td>&nbsp;<a href="/TDS/control?action=registration&event=passengerEntry&subEvent=psrEditEntry&flag=editPage&module=passengerView&key=<%=((passengerBO)al_driverData.get(r_count)).getPass_key()  %>">Edit</a>&nbsp;</td> 
                      </tr>
                 <%}else{ %>     
                    <tr>
	                        <td style="background-color:lightgreen"><%=r_count+startValue %></td>
                        	<td style="background-color:lightgreen"><%=((passengerBO)al_driverData.get(r_count)).getFname()%></td>
                        <td style="background-color:lightgreen">
                          <%=((passengerBO)al_driverData.get(r_count)).getLname()%>
                       </td>
                        <td style="background-color:lightgreen">&nbsp;<%=((passengerBO)al_driverData.get(r_count)).getUid()%>&nbsp;</td>
                         <td style="background-color:lightgreen" >&nbsp;<%=((passengerBO)al_driverData.get(r_count)).getCity()  %>&nbsp;</td>
                         <td style="background-color:lightgreen" >&nbsp;<%=((passengerBO)al_driverData.get(r_count)).getState()  %>&nbsp;</td>
                         <td style="background-color:lightgreen" >&nbsp;<%=((passengerBO)al_driverData.get(r_count)).getZip()  %>&nbsp;</td>
                         <td style="background-color:lightgreen">&nbsp;<a href="/TDS/control?action=registration&event=passengerEntry&subEvent=psrEditEntry&flag=editPage&module=passengerView&key=<%=((passengerBO)al_driverData.get(r_count)).getPass_key()  %>">Edit</a>&nbsp;</td>
                      </tr>
                      <%} %>
                  <%} %>
                      
                     <tr>
                     <td colspan="8">
                      
                      <table align="center"   style=<%=al_driverData.size()==10?"display:block":"display:none" %> >
					<tr align="center">
						<%  
							if(request.getAttribute("pstart")!=null){
								pstart= Integer.parseInt((String)request.getAttribute("pstart"));
								}
							if(request.getAttribute("pend")!=null){
								pend=Integer.parseInt((String)request.getAttribute("pend"));
								}
							if(request.getAttribute("limit")!=null){
								limit = Double.parseDouble((String)request.getAttribute("limit"));
								}
								int start=0;
								int end =0;
							if(request.getAttribute("DRBO")!=null){
								passenger = (passengerBO)request.getAttribute("DRBO");
								}
				 				start=Integer.parseInt(passenger.getStart())+1;
								end =Integer.parseInt(passenger.getEnd());
				 
							 if(limit<=plength)
				 				{ 	 		
			 	 					for(int i=1;i<=limit;i++){ %>
			 	 					<td><a href="/TDS/control?action=registration&event=passengerSummary&page<%=i %>=<%=start%>"><%=i %></a></td>
			 	 					<%} 
			 	 				}				 
							if(limit>=plength)
								{ 
									if(pstart!=1){
									%>
									<td><a href="/TDS/control?action=registration&event=passengerSummary&previous=<%=""+pstart%>&first=no&first_name=<%=first %>&last_name=<%=last %>&passID=<%=passengerId %>&passCity=<%=city %>&module=<%=request.getParameter("module")==null?"":request.getParameter("module") %>"></a></td>
									<%}
									for(int i=pstart;i<pend;i++){%>				
									<td><a href="/TDS/control?action=registration&event=passengerSummary&&module=<%=request.getParameter("module")==null?"":request.getParameter("module") %>&first_name=<%=first %>&last_name=<%=last %>&passID=<%=passengerId %>&passCity=<%=city %>&pageNo=<%=i%>&pstart=<%=""+pstart%>&pend=<%=""+pend %>&first=no"><%=i %></a></td>
									<%}
									if(pend<=limit){	%>
									<td><a href="/TDS/control?action=registration&event=passengerSummary&&module=<%=request.getParameter("module")==null?"":request.getParameter("module") %>&first_name=<%=first %>&last_name=<%=last %>&passID=<%=passengerId %>&passCity=<%=city %>&next=<%=pend+""%>&first=no">&gt;</a></td>
									<%} 
								}%>
					</tr>
					</table>
				</td>
				</tr>
			<%}else{%>
			No&nbsp;Record&nbsp;Found&nbsp;
			<%} %>
				 	</table> 
                </div>    
              </div>
            	
                <div class="clrBth"></div>
          
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
	 
</form>
</body>
</html>
