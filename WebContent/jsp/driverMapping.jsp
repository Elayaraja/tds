<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment" %>
<%
   String wizard = request.getParameter("wizard");
%>

<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

<%DriverDisbursment driverid=(DriverDisbursment)request.getAttribute("disbo") == null ? new DriverDisbursment():(DriverDisbursment)request.getAttribute("disbo") ;
%>
<script type="text/javascript">
 function calculate(target,row_id) {
 	 if(document.getElementById('loginflag'+row_id).checked) {
 		 document.getElementById('queue_name'+row_id).value="true";
 		 document.getElementById("nologinflag"+row_id).checked=false;
	 } else {
		 document.getElementById('queue_name'+row_id).value="false";
 		 document.getElementById("nologinflag"+row_id).checked=true;
	 }
 }
 function noCalculate(row_id){
		if(document.getElementById("nologinflag"+row_id).checked==true){	 
	 		 document.getElementById('queue_name'+row_id).value="noAccess";
			document.getElementById("loginflag"+row_id).checked=false;
		}else {
			 document.getElementById('queue_name'+row_id).value="false";
			document.getElementById("loginflag"+row_id).checked=true;
		 }
 }
 function hideTable(){
	 var ele = document.getElementById("driverZoneMapping");
	 document.getElementById("button").value="Search";
	 ele.style.display = "none";

 }
 function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
 function callDriverName(){
	 setTimeout(function(){
		 caldid('driverid','drivername','dName')
	 },50);
 }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
</head>

<body>
<form  name="masterFormfordrivermapping"  action="control" method="post" onsubmit="return showProcess()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="driverMapping"/>
<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<input type="hidden" name="operation" value="2"/>

<c class="nav-header"> <center>Driver Mapping</center></c>
<br />
<br />
                        		 
 <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                       <%if(((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equals("Driver")) {%>
                    	<tr align="center">
                     	<td  align="center">Driver</td>
                    	<td>    
                         <input type="text" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getUid() %>" readonly="readonly"  name="driverid"  id="driverid"/>                                 
                          </td>
                      	</tr>
                      	<tr>                                        	                 	                                
                    		<td colspan="2" align="center">				
                        	  <input type="submit"  name="button" id="button"  onclick="Calgetdetails()" value="Search" />                      
							</td>								
                    </tr>
                        <%} else { %>
                        <tr align="center"> 
                     	<td  align="center">Driver</td>
                    	<td>
                    	<input type="text" id="driverid" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" name="driverid"  autocomplete="off"  onblur="callDriverName()" onkeypress="hideTable()" id="driverid" value="<%=request.getAttribute("driver") == null ? "" : request.getAttribute("driver")  %>" class="form-autocomplete" />
                                    <ajax:autocomplete
				  					fieldId="driverid"
				  					popupId="model-popup1"
				  					targetId="driverid"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 			
							<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>"/> 
							<div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>                 	  
                    	</td>
                    	 </tr>                    	                  	 
                 
                    <tr >                                        	                 	                                   
                    		<td colspan="2" align="center">				
                        	  <input type="submit"  name="button" id="button" onclick="Calgetdetails()" value="Search" />                      
							</td>			
					   	<%} %>                   	
                    </tr>
              </table>    
                    <br />
                    <br />                
       <table id="driverZoneMapping" width="70%"  border="0" cellspacing="0" cellpadding="0" align="center">
       <tr>
       <td>
       <%                   DriverDisbursment driver = new DriverDisbursment();
   							ArrayList<DriverDisbursment> al_driverData=new ArrayList<DriverDisbursment>();
   							if(request.getAttribute("switchlist")!=null){
	   							al_driverData=(ArrayList)request.getAttribute("switchlist");
   							//Following fields are used to flip the color of the row based on the
   							// colorLightGreen Flag. The color is flipped for every time in the for loop
   							boolean colorLightGreen = true;
   							String colorPattern = "";
   					%>
   					 <table width="100%" border="1" cellspacing="0" cellpadding="0" >
                       	<thead style="background-color: gray;">
                       	<tr >
                       	<th>Queue Name</th>
                   	 	<th>Zone</th>
                       	<th>Allow</th> 
                       	<th>Deny</th> 
                       	</tr>
                       	</thead>                      	
                       	<%if(al_driverData.size()>0) { %>
                       
                        	<%for(int r_count=0;r_count<al_driverData.size();r_count++){ 
                         		colorLightGreen = !colorLightGreen;
                       		if(colorLightGreen){
                       			colorPattern = "style=\"background-color:lightblue\""; 
                       		} else{
                       			colorPattern = "";
                       		}%>
                              
                       	<tbody>                       	
 					 <tr align="center" >  
                       	<td  <%=colorPattern%>> <%= ((DriverDisbursment)al_driverData.get(r_count)).getQueue_name() %> </td>
                       	<td  <%=colorPattern%> > <%=((DriverDisbursment)al_driverData.get(r_count)).getQueue_desc()  %> </td>
                       	<input   type="hidden"  name="desc"  id="desc<%=r_count %>"   value="<%=((DriverDisbursment)al_driverData.get(r_count)).getQueue_name()%>"   ></input>
                       	<input   type="hidden"  name="queue_name"   id="queue_name<%=r_count %>"  value="<%=((DriverDisbursment)al_driverData.get(r_count)).getQueue_login_flag()%>"  /> 
                       	<td <%=colorPattern%> >
                       	<%if(al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("true") || al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("trues")){ %>
                       		 <input type="checkbox" name="loginflag" id="loginflag<%=r_count %>" <%=al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("trues")?"disabled=disabled":"" %> checked="checked" onclick="calculate('queue_name',<%=r_count %>)"  />  
                       	<%} else { %>
							<input type="checkbox" name="loginflag" id="loginflag<%=r_count %>"  <%=al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("trues")||al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("noAccess")?"disabled=disabled":"" %> onclick="calculate('queue_name',<%=r_count %>)"/>  </td>
                       	<%} %>
                    
                       	<td <%=colorPattern%> >
                       	<%if((al_driverData.get(r_count)).getQueue_login_flag().equalsIgnoreCase("noAccess") || al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("false")) { %>
                       		<input   type="checkbox" name="nologinflag"    id="nologinflag<%=r_count %>" <%=al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("false")?"disabled=disabled":"" %> checked="checked" onclick="noCalculate(<%=r_count%>)"/>    
                       	<%} else { %>
							<input  type="checkbox" name="nologinflag" id="nologinflag<%=r_count %>" <%=al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("false")||al_driverData.get(r_count).getQueue_login_flag().equalsIgnoreCase("true")?"disabled=disabled":"" %> onclick="noCalculate(<%=r_count%>)"/>  </td>
                       	<%} %>
                       	
                       	</tr>
                       	<%} %>
                       	<%} %>
                       	</tbody>
                     </table>
                       <%} %>
       </td>
       </tr>
       <tr>
      	<td colspan="2" align="center">
       	<% if(request.getAttribute("buttonflag")=="Update") { %>					
           	   <input type="submit"  name="button" id="button"  onclick="Calgetdetails()" value="Update" />                        	 
       	   <%} %>
   <%if(wizard!=null){%>  		
           	   <input type="submit"  name="button"  value="Next" />                        	
   <%}%>	   
       	
       </td>
       </tr>
       </table>                               
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>