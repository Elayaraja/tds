<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
 
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList,com.tds.tdsBO.CabMaintenanceBO,java.util.List,com.common.util.TDSConstants"%>
<%! CabMaintenanceBO m_maintenanceBO = null;List m_maintenanceList = null; %>
<%
	if(request.getAttribute("maintenance") != null) {
		m_maintenanceList = (ArrayList) request.getAttribute("maintenance");
	}
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link type="text/css" rel="stylesheet" href="css/CalendarControl.css" ></link>
<script src="js/CalendarControl.js" language="javascript"></script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
h2{
font-family:italic;
color:grey;
}
#foo{
font-weight:bold;
color:blue;
}
</style>
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
<script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var maintenanceSummary = document.getElementById("maintenanceSummary");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title>Cab Maintenance Summary Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(maintenanceSummary.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>  
</head>
<body>
<div>

<input type="hidden" name="operation" value="2">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
 
                	   
                </div>
                <div>
                    <div>
                	<h2><center>Cab Maintenance Summary</center></h2>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab" >
                    <tr>
					<td>
					<form name="maintenanceSummary" action="control" method="post" onsubmit="return showProcess()">
					<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
					<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.maintenanceList %>">
					<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />

		<table id="bodypage" width="600"  cellspacing="3">	
		<tr>
		<td >Service&nbsp;Code :&nbsp;</td>
		<td >Service&nbsp;Date :&nbsp;</td> 
		</tr>
		<tr>
		<td><input type="text"  name="scode"  value=""></input> </td>
		<td><input type="text"  size="10" name="sdate" id="sdate" value="" 	onfocus="showCalendarControl(this);" /></td>
		</tr>
	<tr>
		<td colspan="7" align="center">
			<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="Button" value="Get Details" class="lft"></input>
                        	   	  <input type="button" value="Save/Print" id="btnPrint" /></div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>
						
		</td>
	</tr>
</table>
</form>
</td>
	</tr>
	<% if(m_maintenanceList != null && m_maintenanceList.size()>0  ) {  %>
	<tr>
	<td>
		<table  width="600" cellspacing="2" align="center" border="1" id="maintenanceSummary">
			<tr style="background-color:lightblue">
				<th >S no</th>
				<th >Driver</th>
				<th >Service&nbsp;Cost</th>
				<th >Service&nbsp;Date</th>
				<th >Service&nbsp;Code</th>
			</tr>
		 <%
                	boolean colorLightblue = true;
                	String colorPattern;
                	for(int count = 0; count < m_maintenanceList.size(); count++) {  
				 colorLightblue = !colorLightblue;
         		if(colorLightblue){
         			colorPattern="style=\"background-color:lightblue\""; 
         		}
         		else{
         			colorPattern="style=\"background-color:white\"";
         		}
				m_maintenanceBO = (CabMaintenanceBO) m_maintenanceList.get(count);
         		%>
			<tr align="center">
				<td <%=colorPattern %>><%= count+1 %> </td>
				<td <%=colorPattern %>><%= m_maintenanceBO.getDriverName() %></td>
				<td <%=colorPattern %>><%= m_maintenanceBO.getAmount() %></td>
				<td <%=colorPattern %>><%= m_maintenanceBO.getServiceDate() %></td>
				<td <%=colorPattern %>>
					<a href = "control?action=registration&event=maintenanceById&module=operationView&maintenanceID=<%= m_maintenanceBO.getMaintenanceKey() %>">
					<%= m_maintenanceBO.getServiceCode() %>
					</a>
				</td>
				
			</tr>
			<% } %>
		</table>
	</td>
</tr>
<% } else if(request.getParameter("Button")!=null){ %>
	<tr>
		<td>No records available</td>
	</tr>
<% } %>
</table>
</div>    
</div>
                      	
            <div class="clrBth"></div>
           
            
            <div id="foo"><b>Copyright &copy; 2010 Get A Cab</b></div>
 
</body>
</html>
