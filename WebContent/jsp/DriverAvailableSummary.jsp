<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,java.util.List,java.util.Map,java.util.HashMap"%>
<html>

<%
	List m_driverAvailList = null;Map m_driverAvailableMap = null;
	if(request.getAttribute("driverAvailList") != null) {
		m_driverAvailList = (ArrayList) request.getAttribute("driverAvailList");
	}
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>

<table id="pagebox" cellspacing="0" cellpadding="0" >
<tr>
	<td colspan="7" align="center">		
			<div id="title">
				<h2>Driver&nbsp;Available</h2>
			</div>
	</td>		
</tr>
	
<% if(m_driverAvailList != null && m_driverAvailList.size() > 0  ) {  %>
<tr>
	<td>
		<table id="bodypage" width="900">
			<tr>
				<td>Sno</td>
				<td>Driver&nbsp;Id</td>
				<td>Latitude</td>
				<td>Longitude</td>
				<td>Phone&nbsp;No</td>
			</tr>
			<% for(int count = 0; count < m_driverAvailList.size(); count++) {  
				m_driverAvailableMap = (HashMap) m_driverAvailList.get(count);
			%>
			<tr>
				<td><%= count+1 %> </td>
				<td><%= m_driverAvailableMap.get("driverid") %></td>
				<td><%= m_driverAvailableMap.get("driverid") %></td>
				<td><%= m_driverAvailableMap.get("longitude") %></td>
				<td><%= m_driverAvailableMap.get("phoneno") %></td>
				
			</tr>
			<% } %>
		</table>
	</td>
</tr>
<% } else { %>
	<tr>
		<td>No records available</td>
	</tr>
<% } %>
</table>

</body>
</html>