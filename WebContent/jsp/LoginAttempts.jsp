<%@page import="com.tds.cmp.bean.LoginAttempts"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@page import="com.tds.tdsBO.CabMaintenanceBO,com.common.util.TDSConstants"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList,java.util.List,java.util.Map,java.util.HashMap"%>

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%><html>
<head>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

 <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
  <link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 
<style>
h2{
font-family:italic;
color:grey;
}
</style> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<%
	ArrayList<LoginAttempts> loginAttempts= new ArrayList<LoginAttempts>();
	if(request.getAttribute("loginAttempts") != null) {
			loginAttempts = (ArrayList<LoginAttempts>)request.getAttribute("loginAttempts");
	}
%>
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
</head>
<body>
<form name="masterForm" action="control" method="post" onsubmit="retrun showProcess()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"/>
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="loginAttempts"/>

	<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
	
	 
	<table class="nav-static-top" id="pagebox" width="100%" align="left" style="">
		<tr>
			<td>
			<table class="nav-static-top" id="bodypage" border="0" cellspacing="3" cellpadding="3"  width="100%">
			<tr>
				<td colspan="9" align="center">
					<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div>
<div   >
					<h2><center>Login&nbsp;Attempts</center></h2>
					</div></div>
				</td>
			</tr>
			 
			<tr>
			<td >User ID</td>
			<td>
		     <input type="text" name="driverId" id="driverId" size="10"/>
			
			</td>
				<td >Start&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
					<input type="text" name="fromDate" id="fromDate" size="10" onfocus="showCalendarControl(this);"/>
				</td>
				
				<td  >End&nbsp;Date</td>
				<td>&nbsp;</td>
				<td >
				<input type="text" name="toDate" id="toDate" size="10" onfocus="showCalendarControl(this);" />
				 
				</td>
			</tr>
			<tr>	
				<td   colspan="9" align="center"> 
					<input type="submit" name="submit"   value="Submit" />
				</td>
			</tr>
			</table>
			</td>
		</tr>
		<%if(loginAttempts.size()>0){ %>
		<tr>
		<td>
		<table width="100%" border="0" cellspacing="1" cellpadding="0">
						
						<tr>
							<td   >
								<table  id="bodypage"  border="1" cellspacing="1" cellpadding="" align="center">
									
										<tr>
												<td  align="center" style="background-color:lightgrey"><b>UserId
												</b></td>
												<td align="center" style="background-color:lightgrey" ><b>IP Address</b>
												</td>
												<td align="center" style="background-color: lightgrey"><b>Reason</b>
												</td>
												<td align="center" style="background-color: lightgrey"><b>Time</b>
												</td>
												
											</tr>

											<%
											boolean colorLightblue = true;
												String colorPattern;
										%>
											<%
											for (int i = 0; i < loginAttempts.size(); i++) {
													colorLightblue = !colorLightblue;
													if (colorLightblue) {
														colorPattern = "style=\"background-color:#EFEFFB;white-space: nowrap;\"";
													} else {
														colorPattern = "style=\"background-color:lightblue;white-space: nowrap;\"";
													}
										%>
											<tr>
											 <td style="display:none"id="userID"><%=loginAttempts.get(i).getUserId()%></td>
												<td align="center" <%=colorPattern%>><%=loginAttempts.get(i).getUserId()==null?"":loginAttempts.get(i).getUserId()%> 
													</td>
												<td align="center" <%=colorPattern%>><%=loginAttempts.get(i).getIpAddress()==null?"":loginAttempts.get(i).getIpAddress()%></td>
												<td align="center" <%=colorPattern%>><%=loginAttempts.get(i).getReason()==null?"":loginAttempts.get(i).getReason()%>
												</td>
												<td align="center" <%=colorPattern%>><%=loginAttempts.get(i).getTime()==null?"":loginAttempts.get(i).getTime()%>
												</td>
																								

											</tr>
											<%
											}
											}
										%>
										</table>
							</tr>
						</table>
		</td>
		</tr>	
	</table>
</form>

</body>
</html>