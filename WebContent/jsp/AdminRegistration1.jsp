<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html>
<head>
<script type="text/javascript">
	function changename(type){
	document.getElementById("changename").innerHTML=document.getElementById(type).value+" ID";
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%
	String userType[] = {"Admin","Driver","Manager","Operator","Corp Admin","Corp User","PUC","Government"};
%>
<body>
<form method="post" action="control" name="admin">
<jsp:useBean id="adminBo" class="com.tds.tdsBO.AdminRegistrationBO" scope="request"/>
<% if(request.getAttribute("adminBO") != null) 
		adminBo = (AdminRegistrationBO) request.getAttribute("adminBO");
%>
<jsp:setProperty name="adminBo" property="*" />
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveAdminRegistraion %>">

<table id="pagebox">
	<tr>
		<td>
<table id="bodypage">	
	<tr>
		<td colspan="7" align="center">		
			<div id="title">
				<h2>Admin Registration</h2>
			</div>
		</td>		
	</tr>
	<%
		String error ="";
		if(request.getAttribute("errors") != null) {
			error = (String) request.getAttribute("errors");
		}
		
	%>
	<tr>
		<td colspan="7">
			<div id="errorpage">
				<%= error.length()>0?"You must fulfilled the following error<br>"+error :"" %>
			</div>
		</td>
	</tr>
	<tr>
		<td>User&nbsp;Name</td>
		<td>&nbsp;</td>
		<td>
			<input type="text"  name="uname" value="<%= adminBo.getUname() %>">
		</td>
		<td>&nbsp;</td>
		 <td>User&nbsp;Type</td>
		<td>&nbsp;</td>
		<td>
			<select name="usertype" id="usertype" onchange="changename(id);" >
				<%
					for(int count=0;count<userType.length;count++) {
						if(adminBo.getUsertype().equalsIgnoreCase(userType[count]))  {
				%>
				<option value="<%= userType[count] %>" selected="selected"><%= userType[count] %></option>
				<% } else { %>
				<option value="<%= userType[count] %>" ><%= userType[count] %></option>
				<%
				} }
				%>
			</select>
		</td>
		<!--<td>Parent&nbsp;Company</td>
		<td>&nbsp;</td>
		<td>
			<input type="text"  name="pcompany" value="<%= adminBo.getPcompany()  %>" >
		</td>
	--></tr>
	<tr>
		<td>First&nbsp;Name</td>
		<td>&nbsp;
		<td>
			<input type="text"  name="Fname" value="<%= adminBo.getFname() %>"> 
		</td>
		<td>&nbsp;</td>
		<td>Last&nbsp;Name</td>
		<td>&nbsp;</td>
		<td><input type="hidden" name="Uid" value="<%= adminBo.getUid()== null?"":adminBo.getUid() %>">
			<input type="text"  name="Lname" value="<%= adminBo.getLname() %>"> 
		</td>
		
	</tr>
	<tr>
		<td>Password</td>
		<td>&nbsp;</td>
		<td>
		<%if(request.getAttribute("pagefor") != null && (request.getAttribute("pagefor").equals("createAdmin"))){ %>
			<input type="password"  name="password" value="<%= adminBo.getPassword() %>">
			<%}else{ %>
			<input type="password"  name="password"   value="<%= adminBo.getPassword() %>">
			<%} %>
		</td>
		<td>&nbsp;</td>
		<td>Re-Type&nbsp;Password</td>
		<td>&nbsp;</td>
		<td>
		<%if(request.getAttribute("pagefor") != null && (request.getAttribute("pagefor").equals("createAdmin"))){ %>
			<input type="password"  name="repassword" value="<%= adminBo.getRepassword() %>">
			<%}else{ %>
			<input type="password"  name="repassword"   value="<%= adminBo.getPassword() %>">
			<%} %>
		</td>
	</tr>
	<tr>
		<%--<td>Association&nbsp;Code</td>
		<td>&nbsp;</td>
		<%
		if(request.getAttribute("pagefor") != null)
		if(request.getAttribute("pagefor").equals("createAdmin")){ %>
		<td>
			<input type="text"  name="associateCode" value="<%= adminBo.getAssociateCode() %>">
		</td>
		<%} else{%>
		<td>
			<input type="text"  name="associateCode"  readonly="readonly" value="<%= adminBo.getAssociateCode() %>">
		</td>
		<%} %>--%>
		<td>EMail</td>
		<td>&nbsp;</td>
		<td>
		 <input type="text"  name="Email"  value="<%= adminBo.getEmail() == null ?"":adminBo.getEmail() %>">
		</td>
		<td>&nbsp;</td>
		<%if(request.getAttribute("pagefor") != null && (request.getAttribute("pagefor").equals("createAdmin"))){ %>
		<td id="changename">Admin</td>
		<td>&nbsp;</td>
		<td><input type="text"  name="Drid"   value="<%= adminBo.getDrid()== null ? request.getAttribute("drkey") == null ?"" :request.getAttribute("drkey"):adminBo.getDrid() %>"></td>
		<%}else{ %>
		<td>
			<input type="hidden"  name="Drid"   value="<%= adminBo.getDrid()== null ? request.getAttribute("drkey") == null ?"" :request.getAttribute("drkey"):adminBo.getDrid() %>">
		</td>	
		<%} %>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Active
		<td><input type="radio"  name="Active" value="1"  <%= adminBo.getActive().equals("1")?"checked":"" %>>
		InActive<input type="radio" name="Active" value="0" <%= adminBo.getActive().equals("0")?"checked":"" %>></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>
			 
		</td>
		
	</tr>
	
	<%if(request.getAttribute("pagefor") != null && (request.getAttribute("pagefor").equals("createAdmin"))){ %>
		<tr>
		<td colspan="7" align="center">
			<input type="submit" name="submit" value="Create Admin" >
		</td>
	</tr>
	<%} else { %>
	<tr>
		<td colspan="7" align="center">
			<input type="submit" name="submit" value="Update Admin" >
		</td>
	</tr>
	
	<%} %>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>