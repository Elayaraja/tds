<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>

<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html> --%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.DriverRegistrationBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.charges.bean.ChargesBO"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.common.util.TDSProperties"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<%
String wasl_ccode = TDSProperties.getValue("WASL_Company");
ArrayList al_list = new ArrayList();
if(request.getAttribute("drFlag")!=null){ 
	al_list= (ArrayList)request.getAttribute("drFlag");
}%>

<%AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");%>

<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>
<!-- <style type="text/css">

.ui-datepicker-calendar{
width: 274px;
height: 172px;
}
.ui-datepicker-header{
width: 274px;
height: 38px;
}
</style>
 --><script type="text/javascript">


		function closeDriverProfile() {
			var driver = document.getElementById('registration');
			if (driver.value == "D") {
				var drivertable = document.getElementById('Driver');
				drivertable.style.display = '';
				var drivertable1 = document.getElementById('driverProfile');
				drivertable1.style.display = '';
				var buttonvalue = document.getElementById('NextScreen');
				masterregistration();
				buttonvalue.value = "NextScreen";
				document.getElementById('top3').style.display="block";
			} else {
				var drivertable = document.getElementById('Driver');
				drivertable.style.display = 'none';
				var drivertable1 = document.getElementById('driverProfile');
				drivertable1.style.display = '';
				document.getElementById("driverProfileByAjax").style.display = "none";
				document.getElementById('top3').style.display="none";
			}
			if (driver.value == "O") {
				var buttonvalue = document.getElementById('NextScreen');
				buttonvalue.value = "Provide Access";
				document.getElementById("driverProfileByAjax").style.display = "none";
				document.getElementById('top3').style.display="none";
			}
		}
		function getUserAccess() {
			var userid = document.getElementById('uid');
			var userdesc = document.getElementById('registration');
			var status = document.getElementById('status');

			if (userdesc.value != "D") {
				window.location.href = "control?action=userrolerequest&module=systemsetupView&event=getUserList&userid="
						+ userid.value
						+ "&uname="
						+ userid.value
						+ "&utype="
						+ userdesc.value
						+ "&userDesc="
						+ userdesc.value
						+ "&status=" + status.value;
			} else {
				window.location.href = "control?action=registration&event=driverMapping&subevent=home&module=operationView";
			}
		}
		function submitInfo() {
			document.getElementById('Information').value = "1";
		}
		function getLast3IDs() {
			var xmlhttp = null;
			if (window.XMLHttpRequest) {
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'RegistrationAjax?event=getLast3Ids&module=systemsetupView';
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			document.getElementById("driverIDReference").innerHTML = text;
		}
	</script>
<script type="text/javascript">
function clearErrorInfo(){
	document.getElementById("errorpage").innerHTML ="";
}
function loadFile(docId){
	document.getElementById("docLoaderImage").src='searchDocs?do=viewDoc&docId='+docId;
}

$(document).ready ( function(){	
	closeDriverProfile();
	var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url='SystemSetupAjax?event=getDocumentId&module=systemsetupView&driverId='+document.getElementById("uid").value;
		//var url='SystemSetupAjax?event=getDocumentId&module=systemsetupView&driverId=103001';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("docLoader").innerHTML=text;
		});

function masterregistration(){
	<%if(al_list==null || al_list.isEmpty()){%>
		var xmlhttp=null;
		document.getElementById("driverProfileByAjax").style.display="block";
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var driverCounter=0;
		var url='SystemSetupAjax?event=getFlagsForCompany';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		
		var text = xmlhttp.responseText;
		var jsonObj = "{\"flagDetails\":"+text+"}";
		var obj = JSON.parse(jsonObj.toString());
		document.getElementById("driverProfileByAjax").innerHTML="";
		var $tbl = $('<table style="width:100%" class="thumbnail">').attr('id', 'flagValues');
		//var tbl;
		$tbl.append($('<td>').text('Driver Profile'));	
		for(var j=0;j<obj.flagDetails.length;j++){
			if(obj.flagDetails[j].SW=="1"){
				$tbl.append(
					$('<td>').text(obj.flagDetails[j].LD).append($('<input/>').attr('type', 'checkbox').attr('name', 'chk'+driverCounter).attr('id', 'chk'+driverCounter).val(obj.flagDetails[j].K).attr('onclick',"checkGroup('"+driverCounter+"','"+obj.flagDetails[j].GI+"','"+obj.flagDetails[j].LD+"')")),
					$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vgroup'+driverCounter).attr('id','vgroup'+driverCounter).val(obj.flagDetails[j].GI)),
					$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vLD'+driverCounter).attr('id','vLD'+driverCounter).val(obj.flagDetails[j].LD))
				);
				driverCounter=Number(driverCounter)+1;
				$("#driverProfileByAjax").append($tbl);
			}
		}
		document.getElementById("profileSizeAjax").value=driverCounter;
	<%}%>
}
		
function checkGroup(row,groupId,longDesc){
	var drProf="";
	if(document.getElementById("profileSizeAjax").value!=""){
		drProf=document.getElementById("profileSizeAjax").value;
	} else {
		drProf=document.getElementById("profileSize").value;
	}
	if(document.getElementById("chk"+row).checked==true){
		for(var i=0;i<drProf;i++){
			if(document.getElementById("chk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
				document.getElementById("chk"+row).checked=false;
				 alert("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
				 break;
			}
		}
	}
}
		
</script>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>

<title>TDS (Taxi Dispatch System)</title>
</head>
<%
	String frequen[] = {"No","Yes"};
	ArrayList<ChargesBO> charges=new ArrayList<ChargesBO>();
%>
<body  >

	<form name="masterForm" action="control" method="post">
		<jsp:useBean id="driverBO" class="com.tds.tdsBO.DriverRegistrationBO" scope="request" />
		<jsp:useBean id="openBO" class="com.tds.tdsBO.OpenRequestBO" scope="request" />
		<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
		<input type="hidden" id="profileSizeAjax" name="profileSizeAjax" value=""/>
		<%
final char regkey[] = {'S','D','O'};
final String registrationValue[] = {"Select","Driver","Operator"};
%>
		<%
if(request.getAttribute("driverDetails")!=null){
	driverBO = (DriverRegistrationBO)request.getAttribute("driverDetails");
}if(request.getAttribute("defaultZone")!=null){
	openBO = (OpenRequestBO)request.getAttribute("defaultZone");
}if(request.getAttribute("template")!=null){
	charges =(ArrayList<ChargesBO>)request.getAttribute("template");
}
boolean isEdit = request.getAttribute("edit")!=null?true:false;
%>

		<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"></input>
			 <%if(request.getAttribute("edit")!=null){ %>
				<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.editUpdateDriverDetail %>"> </input>
				<input type="hidden" name="seq" value="<%=driverBO.getSno()%>"></input> 
				<input type="hidden" name="Information" id="Information" value="" /> 
				
			 <%}else{ %>
					<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.driversavedetails %>"> </input>
					<input type="hidden" name="wizard" value="yes"> </input>
			<%} %>
			<c class="nav-header"><center>Driver/Operator Registration</center></c>
					<%
						String error ="";
						if(request.getAttribute("errors") != null) {
						error = (String) request.getAttribute("errors");
						}
 					%>
									<div id="errorpage" class="alert-error">
										<%=request.getAttribute("page")!=null?(String)request.getAttribute("page"):"" %>
										<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %>
									</div>

					<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="navbar-static-top">
										<tr>
											<td>
												<table id="bodypage" width="740" >
													<tr>
														<td >Registration:</td>
														<td><select name="registration" id="registration" onchange="closeDriverProfile()">
															<%for(int i=0;i<regkey.length;i++) {
			                	     							if(driverBO.getRegistration() == regkey[i]) {%>
																<option value="<%=regkey[i] %>" selected="selected" name="driver" id="driver"
																   onmouseup="closeDriverProfile()"><%= registrationValue[i]%></option>
																<%}else { %>
																<option value="<%=regkey[i] %>" name="driver" id="driver"
																	onclick="closeDriverProfile()"><%= registrationValue[i]%></option>
																<%} %>
															<%} %>
														</select></td>

														<td>User&nbsp;Id</td>
														<td>
															<%if(request.getAttribute("edit")!=null){ %> <input
															type="text" name="uid" id="uid" readonly
															value="<%= driverBO.getUid()  %>" /> <%}else{ %> <input
															type="text" name="uid" id="uid"
															value="<%= driverBO.getUid()  %>" onblur="clearErrorInfo()"/> <%} %>
														</td>

													</tr>

													<tr>
														<td>First&nbsp;Name</td>
														<td><input type="text" name="fname"
															value="<%= driverBO.getFname()  %>" /></td>
														<td>Last&nbsp;Name</td>
														<td><input type="text" name="lname"
															value="<%= driverBO.getLname()  %>" /></td>
													</tr>

													<tr>
														<td>Address1</td>
														<td><input type="text" name="add1"
															value="<%= driverBO.getAdd1()  %>" /></td>
														<td>Address2</td>
														<td><input type="text" name="add2"
															value="<%= driverBO.getAdd2()  %>" /></td>
													</tr>

													<tr>
														<td>City</td>
														<td><input type="text" name="city"
															value="<%= driverBO.getCity() %>" /></td>
														<td>State</td>
														<td><input type="text" name="state"
															value="<%= driverBO.getState()  %>" /></td>
													</tr>

													<tr>
														<td>Zip&nbsp;Code</td>
														<td><input type="text" name="zip"
															value="<%= driverBO.getZip()%>" /></td>
														<td>BankAct</td>
														<td><input type="text" name="acct"
															value="<%= driverBO.getAcct()%>" /></td>
													</tr>

													<tr>
														<td>Phone&nbsp;No</td>
														<td><input type="text" name="phone"
															value="<%= driverBO.getPhone()%>" /></td>
														
														<td>Email&nbsp;Id</td>
														<td><input type="text" name="mailId" id="mailId"
															value="<%= driverBO.getEmailId()%>" /></td>
													</tr>
														
<%-- 															<td>Driver&nbsp;Extension</td>
														<td><input type="text" name="drExt" class="datePicker"
															value="<%= driverBO.getDriExten()  %>" /></td>
 --%>							
 													<tr>
														<td>Social Security Number</td>
														<td><input type="text" name="soSecurityNo" id="soSecurityNo" 
															value="<%=driverBO.getSocialSecurityNo()%>"/></td>
														
														<td style="display: none">Car&nbsp;Make</td>
														<td style="display: none"><input type="text"
															name="cmake" value="<%= driverBO.getCmake()  %>" /></td>
													</tr>

													<tr>
														<td style="display: none">Car&nbsp;Year</td>
														<td style="display: none"><input type="text"
															name="cyear" value="<%= driverBO.getCyear()  %>" /></td>
														<td style="display: none">Car&nbsp;Model</td>
														<td style="display: none"><input type="text"
															name="cmodel" value="<%= driverBO.getCmodel()  %>" /></td>
													</tr>

													<tr>
														<td style="display: none">Cab&nbsp;No</td>
														<td style="display: none"><input type="text"
															name="cabno" value="<%= driverBO.getCabno()  %>" /></td>
               
													</tr>

													<tr style="display: none;">
																				<td style="display: none">Pass&nbsp;No</td>
														<td style="display: none"><input type="text" name="passno"
															value="<%= driverBO.getPassno()  %>" /></td>
													
														<td style="display: none">Frequency&nbsp;Of&nbsp;Deposit</td>
														<td align="center" style="display: none">
															<div>
																<div>
																	<div>
																		<select name="fdeposit">
																			<%
							for(int count = 0;count<frequen.length;count++) {
							if(driverBO.getFdeposit() != "" && Integer.parseInt(driverBO.getFdeposit()) == count) {
					%>
																			<option value="<%= count  %>" selected="selected"><%= frequen[count] %></option>
																			<% 		} else {
					%>
																			<option value="<%= count  %>"><%= frequen[count] %></option>
																			<%
						}
					}
				%>
																		</select>
																	</div>
																</div>
															</div></td>
													</tr>

													<tr>
														<td>Status</td>
														<td align="center">
															<div>
																<div >
																	<div >
																		<select name="status" id="status">
																			<% System.out.println("DRIVER STATUS::"+driverBO.getStatus()); 
                	if(driverBO.getStatus().equalsIgnoreCase("1")) {
                	%>
														<option value="1" <%=driverBO.getStatus().equalsIgnoreCase("1")?"selected":"" %>><%="Active" %></option>
																			<option value="0" <%=driverBO.getStatus().equalsIgnoreCase("0")?"selected":"" %>><%="In Active" %></option>
																			<%}else { %>
																			<option value="1" <%=driverBO.getStatus().equalsIgnoreCase("1")?"selected":"" %>><%="Active" %></option>
																			<option value="0" <%=driverBO.getStatus().equalsIgnoreCase("0")?"selected":"" %>><%="In Active" %></option>
																			<%} %>
																		</select>
																	</div>
																</div>
															</div></td>
														<td>E-Mail&nbsp;Host&nbsp;Name</td>
														<td><input type="text" name="ehost"
															value="<%= driverBO.getEhostid()  %>" /></td>
												</tr>
												</table></td>
										</tr>
									</table>
										<div id="driverIDReference" class="pull-left"></div>
									<%if(al_list!=null && !al_list.isEmpty()) {%>
									<table width="100%" id="driverProfile" class="thumbnail">														
									<tr>
														<td>Driver Profile 
														<input type="hidden" name="profileSize" id="profileSize" value="<%=al_list.size()/3 %>"></input>
														</td>
														<%for(int i=0,j=0;i<al_list.size();i=i+3,j++){ %>
														<td>
														<input type="checkbox" onclick="checkGroup('<%=j%>','<%=al_list.get(i+2)%>','<%=al_list.get(i)%>')"
															<%
											 if(request.getParameter("chk"+j)!=null)
											 {
												 out.print("checked");
											 } else {
												 if(driverBO.getAl_list()!=null)
												 {
													 for(int k=0;k<driverBO.getAl_list().size();k++)
													 {
														 if(driverBO.getAl_list().get(k).toString().equals(al_list.get(i+1)))
														 {
															 out.print("checked");
															 break;
														 }
													 }
												 }
											 }
										
										%>	name="chk<%=j %>" id="chk<%=j %>" value="<%=al_list.get(i+1) %>" /><%=al_list.get(i) %></td>
									 <input type="hidden" name="vgroup<%=j %>" id="vgroup<%=j %>" value="<%=al_list.get(i+2)%>"/>
									 <input type="hidden" name="vLD<%=j %>" id="vLD<%=j %>" value="<%=al_list.get(i)%>"/>
										
														<%}%>
													</tr>	
									</table>	<%} else {%>
																		<div id="driverProfileByAjax" <%= (al_list!=null && !al_list.isEmpty())?"style=display:none":""%>></div>
									
										<table width="0%" id="driverProfile" class="thumbnail" style="display: none;">														
									</table><%} %>
									<table width="100%" border="0" cellspacing="0" id="Driver" cellpadding="0" class="thumbnail">
										<tr>
											<td>
												<tr>
											<td>
												<tr><td>Driver License</td>
												<%if(adminBo.getAssociateCode().equals(wasl_ccode) && isEdit) {%>
													<td style="color: red;"><%= driverBO.getDrLicense() %>
													<input type="hidden" name="drLicense" id="drLicense" value="<%= driverBO.getDrLicense() %>" />
													</td>
												<%} else{%>
													<td ><input type="text" name="drLicense" id="drLicense" value="<%= driverBO.getDrLicense() %>" /></td>
												<%} %>
													<td>Expiry date</td>
													<td ><input  type="text" name="drExpiryDt" id="drExpiryDt" onfocus="showCalendarControl(this);" readonly="readonly"value="<%= driverBO.getDrLicenseExpiry()  %>"/></td></tr>
												<%if(adminBo.getAssociateCode().equals(wasl_ccode)) {%>
												<tr><td>Driver - Date of Birth (As in License)</td>
														<td>
														<table>
														<thead><tr><td align="center">dd-MM-YYYY</td></tr></thead>
														<%if(isEdit) {%>
															<tbody><tr><td style="color: red;"><%=driverBO.getDr_dob()%>
															<input type="hidden" name="drDOB" id="drDOB" value="<%= driverBO.getDr_dob() %>" />
															<input type="hidden" name="drJson" id="drJson" value="<%= driverBO.getDr_details_json() %>" />
															</td></tr></tbody>
														<%}else{ %>
														<tbody><tr><td><input  type="text" name="drDOB" id="drDOB" value=""/></td></tr></tbody>
														<%} %>
														
														</table>
														</td></tr>
												<%} %>
												<tr><td>Hack License</td>
														<td ><input type="text" name="hkLicense" id="hkLicense" value="<%= driverBO.getHkLicense()  %>"/></td>
														<td>Expiry date</td>
														<td ><input  type="text" name="hkExpiryDt" id="hkExpiryDt" onfocus="showCalendarControl(this);" readonly="readonly" value="<%= driverBO.getHkLicenseExpiry()  %>"/></td>
														</tr>
																<%-- 		<%if(request.getAttribute("edit")!=null){ %> --%>
														<tr><td>Driver Rating</td>
														<td> <select name="driverRatings" id="driverRatings" >
                    
                      <option value="1" <%=driverBO.getDriverRatings()==1?"selected=selected":"" %> >1</option>
                      <option value="2" <%=driverBO.getDriverRatings()==2?"selected=selected":"" %> >2</option>
                      <option value="3" <%=driverBO.getDriverRatings()==3?"selected=selected":"" %> >3</option>
                      <option value="4" <%=driverBO.getDriverRatings()==4?"selected=selected":"" %> >4</option>
                      <option value="5" <%=driverBO.getDriverRatings()==5?"selected=selected":"" %> >5</option>
               
                        </select></td>
                        <td>Settings Access</td>
                        <td><input type="checkbox" name="drAccess" id="drAccess" value="1" <%=driverBO.getSettingsAccess()==1?"checked":"" %>/></td>
														
														</tr>
											<%-- 	<%} %> --%>
														<td>PaymentUserId</td>
														<td>
															<%if(request.getAttribute("edit")!=null){ %> <input
															type="text" name="payUserId" id="payUserId"
															value="<%=driverBO.getPayUserId() %>"></input> <%}else { %>
															<input type="text" name="payUserId" id="payUserId" value=""></input> <%} %>
														</td>
														<td>PaymentMerchant</td>
														<td>
															<%if(request.getAttribute("edit")!=null){ %> <input
															type="text" name="payMerchand" id="payMerchand"
															value="<%=driverBO.getPayMerchant() %>"></input> <%}else { %>
															<input type="text" name="payMerchand"id="payMerchand" value=""></input>
															<%} %>
														</td>
													<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>
	<div id="multiFleet">
	
                       <%if(fleetList!=null && fleetList.size()>0){%>
                         <select name="fleet" id="fleet" >
                       <%if(fleetList!=null){
	for(int i=0;i<fleetList.size();i++){ %>
	<option value="<%=fleetList.get(i).getFleetNumber()%>" <%=(fleetList.get(i).getFleetNumber().equals(adminBo.getAssociateCode())?"selected=selected":"")%> ><%=fleetList.get(i).getFleetName() %></option>
<% }}%> 
                        </select>
                        <input type="button" name="fleet" id="fleet" value="Change Fleet" onclick="changeFleet()"/>
                       <%} %>
                       </div>
													</td>
													<tr>
														<td>PaymentPassword</td>
														<td><input type="password" name="payPassWord" id="payPassWord"
															value="<%=driverBO.getPayPassword()%>" /></td>
														<td>PaymentRe-Type&nbsp;Password</td>
														<td><input type="password" name="payrePassWord" id="payrePassWord"
															value="<%=driverBO.getPayRePassword() %>"></input></td>
													</tr>
													
							<tr>							
							<% if(session.getAttribute("user") != null) {  %>
							<%ArrayList al_q = (ArrayList)request.getAttribute("al_q");%>
								<td style="width: 10%;">Zone</td><td style="width: 35%;"><select name='queueno' id='queueno'>
										<option value=''>Select</option>
										<%
										if(al_q!=null){
										for(int i=0;i<al_q.size();i=i+2){ 
									%>
										<option value='<%=al_q.get(i).toString() %>'
											<%=openBO.getQueueno().equals(al_q.get(i).toString())?"selected":"" %>><%=al_q.get(i+1).toString() %></option>
										<%
									}
									} %>
								</select> <% } else{  %>

									<div>
										<div>
											<div>
												<input type="hidden" name="queueno" id="queueno"
													value="<%=openBO.getQueueno() %>" />
											</div>
										</div>
									</div> <%} %>
									<%if(charges.size()>0){ %>
									<td style="width: 10%;">Template</td><td><Select name="templateType">
	
		<%for(int i=0;i<charges.size();i++){%>
		
		<option value="<%=charges.get(i).getPayTemplateKey()%>" <%=charges.get(i).getPayTemplateKey()==driverBO.getTemplate()?"selected":"" %>  ><%=charges.get(i).getPayTemplateDesc() %></option>	
	<%} %></Select>
	</td><%} %></tr>
												</table>
			<table width="100%" class="badge">
								<tr>
									<td>Password</td>
									<td><input type="password" name="password" id="password" value="" /></td>
									<td>RE-Type&nbsp;Password</td>
									<td><input type="password" name="repassword" id="repassword" value="" /></td>
								</tr>
								<tr>		
									<td style="width: 30%;" align="left">
									<input type="button" name="top3" id="top3" style="display: none;" value="Largest DriverIDs " onclick="getLast3IDs()"></input></td>
									<%if(request.getAttribute("edit")!=null){ %>
									<td align="center">
									<input type="button" name="Button" value="Update" onclick="submitInfo();javascript:submit()"></input></td>
									<%}   else if(request.getAttribute("edit")==null){    %>
									<td align="center">
									<input type="button" name="button" value="Submit" onclick="javascript:submit()"></input></td>
									<%} %>
									<td align="right">
									<input type="submit" name="NextScreen" id="NextScreen" value="NextScreen"></input></td>
								</tr>
								<tr>
									<td colspan="8" align="center">
											<%if(request.getAttribute("edit")!=null){ %>
											<input type="button" align="middle" name="changePassword" id="changePassword" value="Change Password"
												onclick="javascript:submit()"></input> 
											<input type="hidden" name="changing" id="changing" value="changePassword" />
											<%} %>
									</td>
								</tr>
				</table>
					<div id="docLoader" class="carousel-caption">
					</div>
	            <table id="documentImages" class="carousel-caption">
            	<tr>
            	</tr>
            	</table>
						
	</form>
</body>
</html>


