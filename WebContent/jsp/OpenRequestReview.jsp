<%@page import="com.tds.tdsBO.DriverLocationBO"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7">
<title>Shared Ride</title>
<meta name="description"
	content="Find the latitude and longitude of a point using Google Maps.">
<meta name="keywords"
	content="latitude, longitude, google maps, get latitude and longitude">
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<SCRIPT type="text/javascript" src="/js/mapTypeControl.js"></SCRIPT>
<script type="text/javascript" src="js/jquery.js"></script> 
<script type="text/javascript" src="Ajax/Review.js"></script>
<script type="text/javascript" src="js/jquery.contextMenu.js"></script>
<link rel="stylesheet" href="css/jquery.contextMenu.css"></link>
 <link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
 <link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" 	media="screen"></link>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 
<script src="js/code.js"></script>
<link href="/d.css" type="text/css" rel="stylesheet">
<style type="text/css">
.contextmenu{
  visibility:hidden;
  background:#ffffff;
  border:1px solid #8888FF;
  z-index: 10;
  position: relative;
  width: 140px;
}
.contextmenu div{
padding-left: 5px
}
</style>
<script type="text/javascript">	
function functionClose()
{
	window.location.href="/TDS/control";
}
</script>
</head>
<body>
<%ArrayList<OpenRequestBO> Jobs=(ArrayList<OpenRequestBO>)request.getAttribute("trips"); 
ArrayList<DriverLocationBO> Drivers=(ArrayList<DriverLocationBO>)request.getAttribute("drivers"); 
OpenRequestBO routeNumber=(OpenRequestBO)request.getAttribute("route");%>
<form name="masterForm" action="control" method="post" >
<input type="hidden" id="size" name="size" value="">
<input type="hidden" name="action" value="openrequest" />
<input type="hidden" name="event" value="openRequestReview" />
<input  type="hidden"   name="module"    id="module"  value="dispatchView">
<input type="hidden" name="dragedJob" id="dragedJob" value=""/>
<input type="hidden" name="dragedDriver" id="dragedDriver" value=""/>
<input type="hidden" name="defaultLati" id="defaultLati" value="<%=adminBo.getDefaultLati()%>"/>
<input type="hidden" name="defaultLongi" id="defaultLongi" value="<%=adminBo.getDefaultLogi()%>"/>
<input type="hidden" name="markerPosition" id="markerPosition" value=""/>
<input type="hidden" name="driverPosition" id="driverPosition" value=""/>
<input type="hidden" name="totalJobs" id="totalJobs" value="<%=Jobs.size()%>"/>
<input type="hidden" name="totalDrivers" id="totalDrivers" value="<%=Drivers.size()%>"/>
<input type="hidden" name="counter" id="counter" value="0"/>
<input type="hidden" name="manifestClick" id="manifestClick" value=""/>
<%if(Jobs!=null) {%>
<table id="jobs">
<%for(int i=0;i<Jobs.size();i++) {%>
<tr>
<td id="job_<%=i%>">
<input type="hidden" name="tripId" id="tripId_<%=i %>" value="<%=Jobs.get(i).getTripid()==null?"":Jobs.get(i).getTripid()%>"/>
<input type="hidden" name="latitude" id="latitude_<%=i %>" value="<%=Jobs.get(i).getSlat()==null?"":Jobs.get(i).getSlat()%>"/>
<input type="hidden" name="longitude" id="longitude_<%=i %>" value="<%=Jobs.get(i).getSlong()==null?"":Jobs.get(i).getSlong()%>"/>
<input type="hidden" name="numOfPass" id="numOfPass_<%=i %>" value="<%=Jobs.get(i).getNumOfPassengers()%>"/>
<input type="hidden" name="startTime" id="startTime_<%=i %>" value="<%=Jobs.get(i).getSttime()==null?"":Jobs.get(i).getSttime()%>"/>
<input type="hidden" name="dropTime" id="dropTime_<%=i %>" value="<%=Jobs.get(i).getDropTime()==null?"":Jobs.get(i).getDropTime()%>"/>
<input type="hidden" name="routeNumber" id="routeNumber_<%=i %>" value="<%=Jobs.get(i).getRouteNumber().equals("0")?"":Jobs.get(i).getRouteNumber()%>"/>
<input type="hidden" name="manifest" id="manifestJob_<%=i %>" value=""/>
</td>
<%} %>
</tr></table>
<%}%><tr><td style="position: absolute;">
<select name="routeNumber" id="routeNumber" style="border: 1px solid #C0C0C0; font-size:10px; height:20px; text-align:left; width:120px; margin:0; padding:0" >
<option value="0" selected="selected">Get All</option>
<option value="1" >1</option>
<option value="2" >2</option>
<option value="3" >3</option>
<option value="4" >4</option>
<option value="5" >5</option>
<option value="6" >6</option>
<option value="7" >7</option>
<option value="8" >8</option>
<option value="9" >9</option>
<option value="10" >10</option>
<option value="11" >11</option>
</select></td>
<td><input type="text" name="driverId" id="driverId" value="" size="10"/></td>
<td class="firstCol" colspan="4"><font size="2">From Date</font><input style="width: 80px; height: 15px;" type="text" name="fromDate" id="fromDate" value="" onclick="displayCalendar(document.masterForm.sdate,'mm/dd/yyyy',this)"/></td>
<td class="firstCol" colspan="4"><font size="2">To Date</font><input style="width: 80px; height: 15px;" type="text" name="toDate" id="toDate" value="" onclick="displayCalendar(document.masterForm.sdate,'mm/dd/yyyy',this)"/></td>											
<td>Allocated<input  type="checkbox" name="allocated" style="vertical-align: middle" id="allocated"/></td>
<td>Un Allocated<input  type="checkbox" name="unAllocated" style="vertical-align: middle" id="unAllocated"/></td>
<td><input type="submit" value="Get Jobs" name="getJobs" id="getJobs"/></td>
</tr>

<div id=ORReview style="height:600px;background-color: lightgrey;">
<table cellpadding="4" cellspacing="0" width="100%">
	<tr valign="top">
		<div id="mapReview" style="margin-left:10px;width: 147%; height: 500px"></div>
<tr>		
<td><input type="submit" align="middle" value="Reset" style="font-size: 100%;"/></td>
<td><input type="button" align="middle" value="Close" style="font-size: 100%;" onclick="functionClose()"/></td>
<tr>
<td style="margin-left: 30%; margin-top:-8%; absolute;"><input type="button" id="addManifest" value="Add" style="font-size: 100%;" onclick="addColumn()"><input type="button" id="removeManifest" value="Remove" style="font-size: 100%;" onclick="deleteColumn()"></td>
<%if(Drivers!=null) {%>
<table id="drivers" style="margin-left: 15%;margin-top: -2%;">
<%for(int i=0;i<Drivers.size();i++) {%>
<td id="driver_<%=i%>">
<input type="button" name="driverId" id="driverId_<%=i%>" value="<%=Drivers.get(i).getDriverid()==null?"":Drivers.get(i).getDriverid()%>" onclick="dragAndAllocateDriver(<%=Drivers.get(i).getDriverid() %>,<%=i%>)"/>
<input type="hidden" name="manifest" id="manifestDriver_<%=i %>" value=""/>
</td>
<%} %>
</table></tr>
<%}%>
	<div style="width: 100%;" id="driverDetails" class="driverDetailsReview">
		<table width="100%">
<tr>
			<td><div id=Manifest_0 style="display: none;position: absolute;margin-top: -14%;margin-left: 2%;" ><img src="images/driverBucket.png" onmouseup="bounce(0)" onmousedown="enterManifestClick(0)" class="driverDetailsReview" >Manifest 1</div></td>
			<td><div id=Manifest_1 style="display: none;position: absolute;margin-top: -14%;margin-left: 3%;" ><img src="images/driverBucket.png" onmouseup="bounce(1)" onmousedown="enterManifestClick(1)" class="driverDetailsReview">Manifest 2</div></td>
			<td><div id=Manifest_2 style="display: none;position: absolute;margin-top: -14%;margin-left: 4%;" ><img src="images/driverBucket.png" onmouseup="bounce(2)" onmousedown="enterManifestClick(2)" class="driverDetailsReview">Manifest 3</div></td>
			<td><div id=Manifest_3 style="display: none;position: absolute;margin-top: -14%;margin-left: 5%;" ><img src="images/driverBucket.png" onmouseup="bounce(3)" onmousedown="enterManifestClick(3)" class="driverDetailsReview">Manifest 4</div></td>
			<td><div id=Manifest_4 style="display: none;position: absolute;margin-top: -14%;margin-left: 6%;" ><img src="images/driverBucket.png" onmouseup="bounce(4)" onmousedown="enterManifestClick(4)" class="driverDetailsReview">Manifest 5</div></td>
			<td><div id=Manifest_5 style="display: none;position: absolute;margin-top: -14%;margin-left: 7%;" ><img src="images/driverBucket.png" onmouseup="bounce(5)" onmousedown="enterManifestClick(5)" class="driverDetailsReview">Manifest 6</div></td>
			<td><div id=Manifest_6 style="display: none;position: absolute;margin-top: -14%;margin-left: 8%;" ><img src="images/driverBucket.png" onmouseup="bounce(6)" onmousedown="enterManifestClick(6)" class="driverDetailsReview">Manifest 7</div></td>
			<td><div id=Manifest_7 style="display: none;position: absolute;margin-top: -14%;margin-left: 9%;" ><img src="images/driverBucket.png" onmouseup="bounce(7)" onmousedown="enterManifestClick(7)" class="driverDetailsReview">Manifest 8</div></td>
			<td><div id=Manifest_8 style="display: none;position: absolute;margin-top: -14%;margin-left: 10%;" ><img src="images/driverBucket.png" onmouseup="bounce(8)" onmousedown="enterManifestClick(8)" class="driverDetailsReview">Manifest 9</div></td>
			<td><div id=Manifest_9 style="display: none;position: absolute;margin-top: -14%;margin-left: 11%;" ><img src="images/driverBucket.png" onmouseup="bounce(9)" onmousedown="enterManifestClick(9)" class="driverDetailsReview">Manifest 10</div></td>
			<td><div id=Manifest_10 style="display: none;position: absolute;margin-top: -14%;margin-left: 12%;" ><img src="images/driverBucket.png" onmouseup="bounce(10)" onmousedown="enterManifestClick(10)" class="driverDetailsReview">Manifest 11</div></td>
			</tr>
</table>
	</div>
	<tr></tr>
	<ul id="myMenuReview" class="contextMenu">
	<li class="clearDriver"><a href="#clearDriver">Clear</a></li>
	<li class="finalizeJobs"><a href="#finalizeJobs">Finalize</a></li>
	</ul>
</table>
</div>
</center></form>
</body>
</html>
