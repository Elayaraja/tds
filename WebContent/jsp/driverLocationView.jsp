<%@page import="com.tds.tdsBO.DriverLocationHistoryBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> --%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment" %>
<%@page import="com.common.util.TDSProperties"%>

<head>
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	ArrayList<DriverLocationHistoryBO> driverLocation=new ArrayList<DriverLocationHistoryBO>();
	if(request.getParameter("driverLocation")!=null || request.getAttribute("driverLocation")!=null){
		driverLocation=(ArrayList<DriverLocationHistoryBO>)request.getAttribute("driverLocation");
	}
	DriverLocationHistoryBO driverLocationBO=new DriverLocationHistoryBO(); 
	String driverId = (String)request.getAttribute("driverId");
%>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script src='<%=TDSProperties.getValue("googleMapV3")%>'></script>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/label.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<%-- <%if(driverLocation!=null && driverLocation.size()>0){%> --%>
	<script type="text/javascript" src="js/main5gps.js?vNo=8"></script>
<%-- <%} %> --%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
 function pagerefresh(par){
	
	document.getElementById("data").innerHTML="";
	clearMarkers();
	
	//alert("refresh starts");
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var dec = par;
	var driverId = document.getElementById("driverId").value;
	var Date = document.getElementById("Date").value;
	var stHr = document.getElementById("stHr").value;
	var etHr = document.getElementById("etHr").value
	
	var url = '/TDS/registration?event=driverLocationGPS&driverId=' + driverId + '&Date=' + Date+ '&stHr=' + stHr+ '&etHr=' + etHr+ '&showmap='+dec;
	if(par == "no"){
		var win = window.open(url, '_blank');
		if(win){
		    //Browser has allowed it to be opened
		    win.focus();
		}else{
		    //Broswer has blocked it
		    alert('Please allow popups for this site');
		}
		//alert("Saved as file");
		return;
	}
	xmlhttp.open("GET", url, false);
	
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == "Nothing") {
		alert("No data found");
	}else{
		//alert("text:"+text);
		var jsonObj = "{\"jobj\":"+text+"}";
		var object = JSON.parse(jsonObj.toString());
		//document.getElementById("data").innerHTML="";
		//alert(object.jobj.length);
		if(object.jobj.length>0){
			//var $tbl = $('<table style="width:100%">').attr('id', 'driverDetailDisplay');
			var tableRows = '<table style="width:100%" id ="driverDetailDisplay"> <tr>';
			for(var i = 0; i < object.jobj.length; i++){
				
				tableRows=tableRows+'<td>';
				tableRows=tableRows+'<input type="hidden" name="driverLati" id="driverLati" value="'+object.jobj[i].A+'"/>';
				tableRows=tableRows+'<input type="hidden" name="driverLongi" id="driverLongi" value="'+object.jobj[i].B+'"/>';
				tableRows=tableRows+'<input type="hidden" name="status"     id="status" value="'+object.jobj[i].C+'"/>';
				tableRows=tableRows+'<input type="hidden" name="lastUpdate" id="lastUpdate" value="'+object.jobj[i].D+'"/>';
				tableRows=tableRows+'<input type="hidden" name="driverId"   id="driverId" value="'+object.jobj[i].E+'"/>';
				tableRows=tableRows+'<input type="hidden" name="time"       id="time" value="'+object.jobj[i].F+'"/>';
				tableRows=tableRows+'<input type="hidden" name="tripId"     id="tripId" value="'+object.jobj[i].G+'"/>';
				tableRows=tableRows+'<input type="hidden" name="dist"     id="dist" value="'+object.jobj[i].H+'"/>';
				tableRows=tableRows+'</td><';
				
			}
			tableRows=tableRows+'/tr></table>';
			//alert(tableRows);
			//document.getElementById("data").innerHTML=tableRows;
			 $('#data').append(tableRows);
			showLatLong1();
		}
	}
	
}

</script>

</head>

<body>	

<form  name="driverlocation"  action="control" method="post" onsubmit="retrun showProcess()">
<%-- <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="driverLocationGPS"/> --%>
<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<input type="hidden" name="operation" value="2"/>

 <input type="hidden" name="sLatitude" id="sLatitude" value="<%=adminBo.getDefaultLati()%>"/>
  <input type="hidden" name="sLongitude" id="sLongitude" value="<%=adminBo.getDefaultLogi()%>"/>
<div>
	<c class="nav-header"><center>
		Driver Location Search</center></c>
		<br />
 <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                    <td>
                    <table id="bodypage" width="80%" align="center">
                    <tr>
                    	<td>Driver Id</td>
                    	<td>
							 <input type="text" name="driverId" id="driverId" value="" autocomplete="off"/>
							 <ajax:autocomplete
				  					fieldId="driverId"
				  					popupId="model-popup1"
				  					targetId="driverId"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 	
				  					</td>
 <!-- <td class="firstCol" >Last Updated Date</td>
                    	<td>
 <input type="text" name="lastUpdatedDate" id="lastUpdatedDate" value=""/>
</td>

					 <tr> -->
<!-- 					 <tr>
					 <td>TripId</td>
					 	<td><input type="text" name="tripId" id="tripId" value=""  autocomplete="off"/></td>
					 </tr>
 -->				
 
 					<td>Select Date</td>
					 	<td><input type="text" name="Date" id="Date" value="" onclick="showCalendarControl(this)" autocomplete="off"/></td>
					</tr>
 					<tr>
 					<td>Start time</td>
 					<td>
 					<select name="stHr" id="stHr" style="border: 1px solid #C0C0C0; font-size:14px; height:25px; text-align:left; width:74px; margin:0; padding:0" >
											     <option value="0000" >12:00 A</option>
                                                 <option value="0015" >12:15 A</option>
                                                 <option value="0030" >12:30 A</option>
                                                 <option value="0045" >12:45 A</option>
                                                 <option value="0100" >01:00 A</option>
                                                 <option value="0115" >01:15 A</option>
                                                 <option value="0130" >01:30 A</option>
                                                 <option value="0145" >01:45 A</option>
                                                 <option value="0200" >02:00 A</option>
                                                 <option value="0215" >02:15 A</option>
                                                 <option value="0230" >02:30 A</option>
                                                 <option value="0245" >02:45 A</option>
                                                 <option value="0300" >03:00 A</option>
                                                 <option value="0315" >03:15 A</option>
                                                 <option value="0330" >03:30 A</option>
                                                 <option value="0345" >03:45 A</option>
                                                 <option value="0400" >04:00 A</option>
                                                 <option value="0415" >04:15 A</option>
                                                 <option value="0430" >04:30 A</option>
                                                 <option value="0445" >04:45 A</option>
                                                 <option value="0500" >05:00 A</option>
                                                 <option value="0515" >05:15 A</option>
                                                 <option value="0530" >05:30 A</option>
                                                 <option value="0545" >05:45 A</option>
                                                 <option value="0600" >06:00 A</option>
                                                 <option value="0615" >06:15 A</option>
                                                 <option value="0630" >06:30 A</option>
                                                 <option value="0645" >06:45 A</option>
                                                 <option value="0700" >07:00 A</option>
                                                 <option value="0715" >07:15 A</option>
                                                 <option value="0730" >07:30 A</option>
                                                 <option value="0745" >07:45 A</option>
                                                 <option value="0800" >08:00 A</option>
                                                 <option value="0815" >08:15 A</option>
                                                 <option value="0830" >08:30 A</option>
                                                 <option value="0845" >08:45 A</option>
                                                 <option value="0900" >09:00 A</option>
                                                 <option value="0915" >09:15 A</option>
                                                 <option value="0930" >09:30 A</option>
                                                 <option value="0945" >09:45 A</option>
                                                 <option value="1000" >10:00 A</option>
                                                 <option value="1015" >10:15 A</option>
                                                 <option value="1030" >10:30 A</option>
                                                 <option value="1045" >10:45 A</option>
                                                 <option value="1100" >11:00 A</option>
                                                 <option value="1115" >11:15 A</option>
                                                 <option value="1130" >11:30 A</option>
                                                 <option value="1145" >11:45 A</option>
                                                 <option value="1200" >12:00 P</option>
                                                 <option value="1215" >12:15 P</option>
                                                 <option value="1230" >12:30 P</option>
                                                 <option value="1245" >12:45 P</option>
                                                 <option value="1300" >01:00 P</option>
                                                 <option value="1315" >01:15 P</option>
                                                 <option value="1330" >01:30 P</option>
                                                 <option value="1345" >01:45 P</option>
                                                 <option value="1400" >02:00 P</option>
                                                 <option value="1415" >02:15 P</option>
                                                 <option value="1430" >02:30 P</option>
                                                 <option value="1445" >02:45 P</option>
                                                 <option value="1500" >03:00 P</option>
                                                 <option value="1515" >03:15 P</option>
                                                 <option value="1530" >03:30 P</option>
                                                 <option value="1545" >03:45 P</option>
                                                 <option value="1600" >04:00 P</option>
                                                 <option value="1615" >04:15 P</option>
                                                 <option value="1630" >04:30 P</option>
                                                 <option value="1645" >04:45 P</option>
                                                 <option value="1700" >05:00 P</option>
                                                 <option value="1715" >05:15 P</option>
                                                 <option value="1730" >05:30 P</option>
                                                 <option value="1745" >05:45 P</option>
                                                 <option value="1800" >06:00 P</option>
                                                 <option value="1815" >06:15 P</option>
                                                 <option value="1830" >06:30 P</option>
                                                 <option value="1845" >06:45 P</option>
                                                 <option value="1900" >07:00 P</option>
                                                 <option value="1915" >07:15 P</option>
                                                 <option value="1930" >07:30 P</option>
                                                 <option value="1945" >07:45 P</option>
                                                 <option value="2000" >08:00 P</option>
                                                 <option value="2015" >08:15 P</option>
                                                 <option value="2030" >08:30 P</option>
                                                 <option value="2045" >08:45 P</option>
                                                 <option value="2100" >09:00 P</option>
                                                 <option value="2115" >09:15 P</option>
                                                 <option value="2130" >09:30 P</option>
                                                 <option value="2145" >09:45 P</option>
                                                 <option value="2200" >10:00 P</option>
                                                 <option value="2215" >10:15 P</option>
                                                 <option value="2230" >10:30 P</option>
                                                 <option value="2245" >10:45 P</option>
                                                 <option value="2300" >11:00 P</option>
                                                 <option value="2315" >11:15 P</option>
                                                 <option value="2330" >11:30 P</option>
                                                 <option value="2345" >11:45 P</option>
                                                 </select>
	 					</td>
 					
 					<td>End time</td>
 					<td>
 					<select name="etHr" id="etHr" style="border: 1px solid #C0C0C0; font-size:14px; height:25px; text-align:left; width:74px; margin:0; padding:0" >
                                                 <option value="0015" >12:15 A</option>
                                                 <option value="0030" >12:30 A</option>
                                                 <option value="0045" >12:45 A</option>
                                                 <option value="0100" >01:00 A</option>
                                                 <option value="0115" >01:15 A</option>
                                                 <option value="0130" >01:30 A</option>
                                                 <option value="0145" >01:45 A</option>
                                                 <option value="0200" >02:00 A</option>
                                                 <option value="0215" >02:15 A</option>
                                                 <option value="0230" >02:30 A</option>
                                                 <option value="0245" >02:45 A</option>
                                                 <option value="0300" >03:00 A</option>
                                                 <option value="0315" >03:15 A</option>
                                                 <option value="0330" >03:30 A</option>
                                                 <option value="0345" >03:45 A</option>
                                                 <option value="0400" >04:00 A</option>
                                                 <option value="0415" >04:15 A</option>
                                                 <option value="0430" >04:30 A</option>
                                                 <option value="0445" >04:45 A</option>
                                                 <option value="0500" >05:00 A</option>
                                                 <option value="0515" >05:15 A</option>
                                                 <option value="0530" >05:30 A</option>
                                                 <option value="0545" >05:45 A</option>
                                                 <option value="0600" >06:00 A</option>
                                                 <option value="0615" >06:15 A</option>
                                                 <option value="0630" >06:30 A</option>
                                                 <option value="0645" >06:45 A</option>
                                                 <option value="0700" >07:00 A</option>
                                                 <option value="0715" >07:15 A</option>
                                                 <option value="0730" >07:30 A</option>
                                                 <option value="0745" >07:45 A</option>
                                                 <option value="0800" >08:00 A</option>
                                                 <option value="0815" >08:15 A</option>
                                                 <option value="0830" >08:30 A</option>
                                                 <option value="0845" >08:45 A</option>
                                                 <option value="0900" >09:00 A</option>
                                                 <option value="0915" >09:15 A</option>
                                                 <option value="0930" >09:30 A</option>
                                                 <option value="0945" >09:45 A</option>
                                                 <option value="1000" >10:00 A</option>
                                                 <option value="1015" >10:15 A</option>
                                                 <option value="1030" >10:30 A</option>
                                                 <option value="1045" >10:45 A</option>
                                                 <option value="1100" >11:00 A</option>
                                                 <option value="1115" >11:15 A</option>
                                                 <option value="1130" >11:30 A</option>
                                                 <option value="1145" >11:45 A</option>
                                                 <option value="1200" >12:00 P</option>
                                                 <option value="1215" >12:15 P</option>
                                                 <option value="1230" >12:30 P</option>
                                                 <option value="1245" >12:45 P</option>
                                                 <option value="1300" >01:00 P</option>
                                                 <option value="1315" >01:15 P</option>
                                                 <option value="1330" >01:30 P</option>
                                                 <option value="1345" >01:45 P</option>
                                                 <option value="1400" >02:00 P</option>
                                                 <option value="1415" >02:15 P</option>
                                                 <option value="1430" >02:30 P</option>
                                                 <option value="1445" >02:45 P</option>
                                                 <option value="1500" >03:00 P</option>
                                                 <option value="1515" >03:15 P</option>
                                                 <option value="1530" >03:30 P</option>
                                                 <option value="1545" >03:45 P</option>
                                                 <option value="1600" >04:00 P</option>
                                                 <option value="1615" >04:15 P</option>
                                                 <option value="1630" >04:30 P</option>
                                                 <option value="1645" >04:45 P</option>
                                                 <option value="1700" >05:00 P</option>
                                                 <option value="1715" >05:15 P</option>
                                                 <option value="1730" >05:30 P</option>
                                                 <option value="1745" >05:45 P</option>
                                                 <option value="1800" >06:00 P</option>
                                                 <option value="1815" >06:15 P</option>
                                                 <option value="1830" >06:30 P</option>
                                                 <option value="1845" >06:45 P</option>
                                                 <option value="1900" >07:00 P</option>
                                                 <option value="1915" >07:15 P</option>
                                                 <option value="1930" >07:30 P</option>
                                                 <option value="1945" >07:45 P</option>
                                                 <option value="2000" >08:00 P</option>
                                                 <option value="2015" >08:15 P</option>
                                                 <option value="2030" >08:30 P</option>
                                                 <option value="2045" >08:45 P</option>
                                                 <option value="2100" >09:00 P</option>
                                                 <option value="2115" >09:15 P</option>
                                                 <option value="2130" >09:30 P</option>
                                                 <option value="2145" >09:45 P</option>
                                                 <option value="2200" >10:00 P</option>
                                                 <option value="2215" >10:15 P</option>
                                                 <option value="2230" >10:30 P</option>
                                                 <option value="2245" >10:45 P</option>
                                                 <option value="2300" >11:00 P</option>
                                                 <option value="2315" >11:15 P</option>
                                                 <option value="2330" >11:30 P</option>
                                                 <option value="2345" >11:45 P</option>
                                                 <option value="2359" >12:00 P</option>
                                                 </select>
	 				</td>
 					</tr>
 					<tr>
					<td colspan="2" align="center">
										<div class="wid60 marAuto padT10">					                     
					<input type="button" name="showmap" id="showmap" value="Show The Driver" onclick="pagerefresh('yes')"/>
					</div>					                            					                    		
					</td>
					</tr>
					<tr>
					<td colspan="2" align="center">
										<div class="wid60 marAuto padT10">					                     
					<input type="button" name="Download Journey Details" id="" value="Download Journey Details" onclick="pagerefresh('no')"/>
					</div>					                            					                    		
					</td>
					</tr>
										 </table></td></tr></table>					    
 </div>
 <%-- <%if(driverLocation!=null && driverLocation.size()>0){%> --%>
  
  <div id="map_canvas" align="center" style="position: absolute; width: 90%; height: 75%; margin-top: 2%;" ></div>
<div id="data" name="data" style="display: none;"></div>
   <%--  <table id="driverDetailDisplay" align="center">
 <tr>
 <%for(int i=0;i<driverLocation.size();i++){ %>
 <td>
  <input type="hidden" name="driverLati" id="driverLati" value="<%=driverLocation.get(i).getLatitude()%>"/>
  <input type="hidden" name="driverLongi" id="driverLongi" value="<%=driverLocation.get(i).getLongitude()%>"/>
  <input type="hidden" name="status" id="status" value="<%=driverLocation.get(i).getStatus()%>"/>
  <input type="hidden" name="lastUpdate" id="lastUpdate" value="<%=driverLocation.get(i).getUpdatedTime()%>"/>
   <input type="hidden" name="driverId" id="driverId" value="<%=driverLocation.get(i).getDriverid()%>"/>
   <input type="hidden" name="time" id="time" value="<%=driverLocation.get(i).getTimeDifference()%>"/>
   <input type="hidden" name="tripId" id="tripId" value="<%=driverLocation.get(i).getsNo()%>"/>
</td>
<%} %>
</tr>
</table> --%>
<%-- <%} %> --%>
<%String error="";
                        if(request.getAttribute("error")!=null){
                        error=(String) request.getAttribute("error");
                        } else if(request.getParameter("error")!=null){
                        error=(String)request.getParameter("error");
                        } %>
<table>                       
<tr><td colspan="8">
<div id="errorpage" style=" color: red">
<%=error.length()>0?""+error:""%>
</div></td></tr></table>  
</form>
</body>
</html>