<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.CompanyMasterBO"%>

<%@page import="java.util.ArrayList"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form name="companyForm" action="control" method="post">
<%ArrayList a_list = (ArrayList)request.getAttribute("companyBO");
	%>

<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveCompany %>">

<table id="pagebox" cellspacing="0" cellpadding="0" >
	<tr>
		<td>
<table id="bodypage" border="1">	
	<tr>
		<td colspan="4" align="center">		
			<div id="title">
				<h2>View/Edit Company</h2>
			</div>
		</td>		
	</tr>
	<%
		final char m_dispatchStatus[] = {'a','x','q','m'};
		final String m_dispatchValue[] = {"Automatic Allocation", "Manual Allocation", "Queue Order","Computer Aided"};
         if(a_list !=null)
        	 for(int i=0;i<a_list.size();i++) {
        	 CompanyMasterBO companyBO=(CompanyMasterBO) a_list.get(i);
		%>
	<tr>
		<td>Company&nbsp;Name : </td>
		<td><font color="blue"><%=companyBO.getCname() %></font></td>
		<td>Company Description</td>
		<td><font color="blue"><%= companyBO.getCdesc() %></font> </td>
	</tr>
	<tr>
		<td>Address1 : </td>
		<td><font color="blue"> <%= companyBO.getAdd1() %></font></td>
		<td>Address2 : </td>
		<td><font color="blue"><%= companyBO.getAdd2() %></font></td>
	</tr>
	<tr>
		<td>City : </td>
		<td><font color="blue"> <%= companyBO.getCity() %></font></td>
		<td>State : </td>
		<td><font color="blue"><%= companyBO.getState() %></font></td>
	</tr>
	<tr>
		<td>Zip&nbsp;code : </td>
		<td><font color="blue"><%= companyBO.getZip() %></font></td>
		<td>Phone&nbsp;No : </td>
		<td><font color="blue"><%= companyBO.getPhoneno() %></font></td>
		
	</tr>
	<tr>
		<td>Bank&nbsp;Account&nbsp;No : </td>
		<td><font color="blue"><%= companyBO.getBankacno() %></font></td>
		<td>Bank&nbsp;Name : </td>
		<td><font color="blue"><%= companyBO.getBankname() %></font></td>
	</tr>
	<tr>
		<td>Bank&nbsp;Routing&nbsp;No : </td>
		<td><font color="blue"><%= companyBO.getBankroutingno() %></font></td>
		<td>Dispatch&nbsp;Status : </td>
		<td>
			<input type="hidden" name="assocode" value="<%=companyBO.getAssccode() %>">
			<font color="blue">
			<%
				for(int counter = 0; counter < m_dispatchStatus.length; counter ++) {
					if(companyBO.getDipatchStatus() == m_dispatchStatus[counter]) {
					%>
						<%= m_dispatchValue[counter] %>
					<%
					 
					 }
				}
			%>
			</font>
		</td>
	</tr>
	<tr>
		<TD> Logo</TD>
		<%if(!companyBO.getFileid().equals("0")){ %>
			<TD>
			 <%if(companyBO.getFilename()!=null) {%>
				<a href='/TDS/control?action=file&event=fileDownload&fileid=<%=companyBO.getFileid() %>&filename=<%=companyBO.getFilename() %>'><%=companyBO.getFilename()%></a>
			<% } else { %>
				No Logo
			<%} %>	
			</TD>
		<%} %>
		<td>
		Email
		</td>
		<td>
		<%=companyBO.getEmail() %>
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center">
			<a href = '/TDS/control?action=admin&event=getMerchant&assocode=<%=companyBO.getAssccode() %>'>Set Merchant Profile</a>
		</td>
	</tr>
	<tr>
		<td colspan="4" align="center">
			<input type="submit" value="Edit" name="submit" >
		</td>
	</tr>
</table>
<%} %>
</td>
</tr>
</table>
</form>
</body>
</html>