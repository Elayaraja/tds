<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.tdsBO.LostandFoundBO,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<jsp:useBean id="lostandfoundBo" class="com.tds.tdsBO.LostandFoundBO"
	scope="request" />
<% if(request.getAttribute("lostandfound") != null)
	lostandfoundBo = (LostandFoundBO)request.getAttribute("lostandfound");
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>



<input type="hidden" name="modefromSummary" id="modefromSummary"
	value="<%=lostandfoundBo.getMode() %>"></input>
<script type="text/javascript">

$(document).ready(function(){
    
    //called when key is pressed in textbox
	$("#time").keypress(function (e)  
	{ 
	  //if the letter is not digit then display error and don't type anything
	  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
	  {
		//display error message
		$("#errmsg").html("Time should contain Digits Only").show().fadeOut("slow").fadeIn("slow").fadeOut("slow");  
	    return false;
      }	
	});
	fixMode();
  });
function fixMode(){
	var type=document.getElementById("modefromSummary").value;
	if(type==0){
		$("#miscellaneous").hide('slow');
		$("#driver").hide('slow');
		$("#passenger").show('slow');
		document.getElementById("mode").value=0;
	}else if(type==1){
		$("#miscellaneous").hide('slow');
		$("#passenger").hide('slow');
		$("#driver").show('slow');
		document.getElementById("mode").value=1;
	}else if(type==2){
		$("#passenger").hide('slow');
		$("#driver").hide('slow');
		$("#miscellaneous").show('slow');
		document.getElementById("mode").value=2;

	}
}
function DateCompare(mode){
	var objfromDt="";
	var objtoDt="";
	if(mode==1){
		objfromDt=document.getElementById("fromDate");
		objtoDt=document.getElementById("toDate");
	}else if(mode==2){
		objfromDt=document.getElementById("fromDate_DLF");
		objtoDt=document.getElementById("toDate_DLF");
	}


if(objfromDt!="" && objtoDt!=""){
/* objfromDt="12/01/2008";
objtoDt="11/01/2008"; */
var arrFrom=objfromDt.value.split('/');
var arrTo=objtoDt.value.split('/');
var strFromMonth=arrFrom[0];
var strFromDate=arrFrom[1];
var strToMonth=arrTo[0];
var strToDt=arrTo[1];
if(parseInt(arrFrom[0])<10)
{
strFromMonth=arrFrom[0].replace('0','');
}
if(parseInt(arrFrom[1])<10)
{
strFromDate=arrFrom[1].replace('0','');
}
if(parseInt(arrTo[0])<10)
{
strToMonth=arrTo[0].replace('0','');
}
if(parseInt(arrTo[1])<10)
{
strToDt=arrTo[1].replace('0','');
}
var mon1 = parseInt(strFromMonth);
var dt1 = parseInt(strFromDate);
var yr1 = parseInt(arrFrom[2]);

var mon2 = parseInt(strToMonth);
var dt2 = parseInt(strToDt);
var yr2 = parseInt(arrTo[2]);

var date1 = new Date(yr1, mon1, dt1);
var date2 = new Date(yr2, mon2, dt2);
if(date2 < date1)
{
alert("From date always less than to date");
return false;
}
}
} 
function showLostAndFound(type){
	if(type==0){
		$("#miscellaneous").hide('slow');
		$("#driver").hide('slow');
		$("#passenger").show('slow');
		document.getElementById("mode").value=0;
	}else if(type==1){
		$("#miscellaneous").hide('slow');
		$("#passenger").hide('slow');
		$("#driver").show('slow');
		document.getElementById("mode").value=1;
	}else if(type==2){
		$("#passenger").hide('slow');
		$("#driver").hide('slow');
		$("#miscellaneous").show('slow');
		document.getElementById("mode").value=2;

	}
}
  function detailsForThisNumber(type){
	  if(type==0){
		var phoneNumber= document.getElementById("phoneNumber").value;
		var fromDate=document.getElementById("fromDate").value;
		var toDate=document.getElementById("toDate").value;
		 var xmlhttp=null;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			url='RegistrationAjax?event=tripDetails&phoneNumber='+phoneNumber+'&fromDate='+fromDate+'&toDate='+toDate+'&mode=1';
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if(text!=null ){
				$("#details").show("slow");
				document.getElementById("details").innerHTML=text;
			}
	  }else if(type==1){
			var fromDate=document.getElementById("fromDate_DLF").value;
			var toDate=document.getElementById("toDate_DLF").value;
			var driverId=document.getElementById("driverId_DLF").value;
			 var xmlhttp=null;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				url='RegistrationAjax?event=tripDetails&driverId='+driverId+'&fromDate='+fromDate+'&toDate='+toDate+'&mode=2';
				xmlhttp.open("GET", url, false);
				xmlhttp.send(null);
				var text = xmlhttp.responseText;
				if(text!=null ){
					$("#details").show("slow");
					document.getElementById("details").innerHTML=text;
				}
	  }
  }
  function fixValues(tripId,driverID,cabNo,time,date,mode){
	  if(mode==1){
	 document.getElementById("tripid").value=tripId;
	 document.getElementById("driverId").value= driverID;
	 document.getElementById("cabNo").value= cabNo;
	 document.getElementById("time").value= time;
	 document.getElementById("tripDate").value=date;
	  $("#details").hide("slow");
	  }else if(mode==2){
	 document.getElementById("tripId_DLF").value=tripId;
	 document.getElementById("riderName").value=driverID;
	 document.getElementById("riderPhone").value=cabNo;
	document.getElementById("time_DLF").value= time;
	document.getElementById("tripDate_DLF").value=date;
	$("#details").hide("slow");
	  }
  }
  function showSearch(mode){
	  if(mode==0){
	  if((document.getElementById("phoneNumber").value!="" && document.getElementById("phoneNumber").value!=null) && (document.getElementById("fromDate").value!=null && document.getElementById("fromDate").value!="") && (document.getElementById("toDate").value!=null && document.getElementById("toDate").value!="")){
		$("#search").show("slow");  
	  }else{
			$("#search").hide("slow");  
	  }
	  }else if(mode==1){
		  if((document.getElementById("driverId_DLF").value!="" && document.getElementById("driverId_DLF").value!=null) && (document.getElementById("fromDate_DLF").value!=null && document.getElementById("fromDate_DLF").value!="") && (document.getElementById("toDate_DLF").value!=null && document.getElementById("toDate_DLF").value!="")){
				$("#search_DLF").show("slow");  
			  }else{
					$("#search_DLF").hide("slow");  
			  }  
		  
	  }
  }
  function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
  </script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
</head>

<body>
	<form method="post" action="control" name="lostandfound" onsubmit="return showProcess()">
		<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
		<input type="hidden" name="<%= TDSConstants.actionParam %>"	value="<%= TDSConstants.registrationAction %>" /> 
		<input type="hidden" name="<%= TDSConstants.eventParam %>"	value="<%= TDSConstants.createLostorFoundDetail %>" /> 
		<input type="hidden" name="lostandfoundNo" value="<%= lostandfoundBo.getLfKey()  %>" /> 
		<input type="hidden" name="operation" value="2" />
		<c class="nav-header">
		<center>Lost and Found</center></c>
		<%			String error ="";
			if(request.getAttribute("errors") != null) {
			error = (String) request.getAttribute("errors");
			}		
	    %>
		<div id="errorpage">
			<%= error.length()>0?""+error :"" %>
		</div>
		<div id="errmsg" style="position: absolute; left: 750px; top: 320px;"></div>
		
		
		<%if( lostandfoundBo.getLfKey() != null &&  !lostandfoundBo.getLfKey().equalsIgnoreCase("null")) { 			
		}else{ %>
		<% if(lostandfoundBo.getMode() ==null){ %>
		<select name="passengerOrDriver">
			<option>Select</option>
			<option value="0" onclick="showLostAndFound(0)">Reported By
				Rider</option>
			<option value="1" onclick="showLostAndFound(1)">Reported By
				Driver</option>
			<option value="2" onclick="showLostAndFound(2)">Reported By
				Miscellaneous</option>
		</select>
		<%} }%> 
		<br />
		<br />
		
		<input type="hidden" name="mode" id="mode" value="<%=lostandfoundBo.getMode()%>" />
		
		<div id="passenger" style="display: none; width: 90%" align="center">
		<center> <b>Given by Rider</b></center>
		<br />
			<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td>
						<table id="bodypage" width="100%" align="center">
							<tr>
								<td>From Date</td>
								<td><input type="text" size="10" name="fromDate" id="fromDate" onblur="showSearch(0)" onkeypress="showSearch()"	value="" 
								onfocus="showCalendarControl(this);" readonly="readonly" onchange="DateCompare(1)" /></td>
								<td></td>
								<td>To Date</td>
								<td><input type="text" size="10" name="toDate" onblur="showSearch()" onkeypress="showSearch(0)" onclick="showSearch()" id="toDate" value=""
									onfocus="showCalendarControl(this);" onchange="DateCompare(1)"	readonly="readonly" /></td>
							</tr>
							<tr>
								<td>Phone Number</td>
								<td><input type="text" name="phoneNumber" id="phoneNumber" onblur="showSearch(0)" onkeypress="showSearch(0)"
									value="<%= lostandfoundBo.getPhoneNumber() != null ? lostandfoundBo.getPhoneNumber() : "" %>" /></td>
								<td>
									<div id="search" style="display: none;">
										<input type="button" name="search" id="search" value="search" onclick="detailsForThisNumber(0);DateCompare(1)"></input>
									</div>
								</td>
							</tr>
							<tr>
								<td>DriverID</td>
								<td><input type="text" name="driverId" id="driverId" value="<%= lostandfoundBo.getDriverid() != null ? lostandfoundBo.getDriverid() : "" %>" />
									<ajax:autocomplete fieldId="driverId" popupId="model-popup1" targetId="driverId" baseUrl="autocomplete.view"
										paramName="DRIVERID" className="autocomplete" progressStyle="throbbing" /></td>
								<td></td>
								<td>CabNo</td>
								<td><input type="text" name="cabNo" id="cabNo" value="" />
									<ajax:autocomplete fieldId="cabNo" popupId="model-popup1" targetId="cabNo" baseUrl="autocomplete.view"
										paramName="cabNum" className="autocomplete"	progressStyle="throbbing" /></td>
							</tr>
							<tr>
								<td>Trip ID</td>
								<td><input type="text" name="tripid" id="tripid" value="<%= lostandfoundBo.getTripId() != null ? lostandfoundBo.getTripId() : "" %>" /></td>
								<td></td>
								<td>Item Name</td>
								<td><input type="text" name="itemName" value="<%= lostandfoundBo.getItemName() != null ? lostandfoundBo.getItemName() : "" %>" /></td>
							</tr>
							<tr>
								<td>Item Description</td>
								<td colspan="6"><textarea name="itemDesc" rows="4" cols="40"><%= lostandfoundBo.getItemDesc() != null ? lostandfoundBo.getItemDesc() : "" %>
								</textarea></td>
							</tr>
							<tr>
								<td>Trip Date</td>
								<td><input type="text" size="10" name="tripDate" id="tripDate" value="<%= lostandfoundBo.getLfdate() != null ? lostandfoundBo.getLfdate() : ""  %>"
									onfocus="showCalendarControl(this);" readonly="readonly" /></td>
								<td></td>
								<td>Time</td>
								<td><input type="text" name="time" id="time" value="<%= lostandfoundBo.getLftime() != null ? lostandfoundBo.getLftime() : "" %>" /></td>
							</tr>
							<tr></tr>
							<tr></tr>
								<% if( lostandfoundBo.getLfKey() != null &&  !lostandfoundBo.getLfKey().equalsIgnoreCase("null")) { %>
								<tr align="center">
								<td colspan="2" align="right">Status</td>
								<td colspan="3" align="left">
									<% if(lostandfoundBo.getStatus()==null || lostandfoundBo.getStatus().equals("null")) {%>
									<select name="status">
										<option value="opened">Opened</option>
										<option value="inprogress">InProgress</option>
										<option value="closed">Closed</option>
								</select> <%}else{ %> <select name="status">
										<option value="opened"
											<%=lostandfoundBo.getStatus().equals("opened")?"selected":"" %>>Opened</option>
										<option value="inprogress"
											<%=lostandfoundBo.getStatus().equals("inprogress")?"selected":"" %>>InProgress</option>
										<option value="closed"
											<%=lostandfoundBo.getStatus().equals("closed")?"selected":"" %>>Closed</option>
								</select> <%} %>
								</td>
								</tr>
								<tr align="center">
								<td colspan="5">
									<input type="submit" name="update" value="Update" />											
								</td>
								</tr>
								<% } else { %>
								<tr align="center">
								<td colspan="2" align="center">Status</td>
								<td colspan="3"><select name="status">
										<option value="opened">Opened</option>
										<option value="inprogress" DISABLED>InProgress</option>
										<option value="closed" DISABLED>Closed By</option>
								</select></td>
								</tr>
								<tr align="center">
								<td colspan="5">								
									<input type="submit" name="save" value="Save" />									
								</td>
								</tr>
								<% } %>															
						</table>
					</td>
				</tr>
			</table>
		</div>
		
		
		<div id="driver" style="display: none; width: 90%" align="center">
		<center> <b>Given by Driver </b></center>
		<br />
			<table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td>
						<table id="bodypage" width="100%" align="center">
							<tr>
								<td>From Date</td>
								<td><input type="text" size="10" name="fromDate_DLF" id="fromDate_DLF" onblur="showSearch(1)" onkeypress="showSearch(1)" value=""
									onfocus="showCalendarControl(this);" readonly="readonly" onchange="DateCompare(2)" /></td>
								<td></td>
								<td>To Date</td>
								<td><input type="text" size="10" name="toDate_DLF" onblur="showSearch(1)" onkeypress="showSearch(1)" onclick="showSearch(1)" id="toDate_DLF" value=""
									onfocus="showCalendarControl(this);" onchange="DateCompare(2)" readonly="readonly" /></td>
							</tr>
							<tr>
								<td>Driver ID</td>
								<td><input type="text" name="driverId_DLF" id="driverId_DLF" onblur="showSearch(1)" onkeypress="showSearch(1)"
									value="<%=lostandfoundBo.getDriverid()==null?"":lostandfoundBo.getDriverid()%>" /></td>
								<td>
									<div id="search_DLF" style="display: none;">
										<input type="button" name="search_DLF" id="search_DLF" value="search" onclick="detailsForThisNumber(1);DateCompare(1)"></input>
									</div>
								</td>
							</tr>
							<tr>
								<td>Rider Name</td>
								<td><input type="text" name="riderName" id="riderName" value="<%=lostandfoundBo.getRiderName()==null?"":lostandfoundBo.getRiderName()%>" />
								</td>
								<td></td>
								<td>Rider PhoneNumber</td>
								<td><input type="text" name="riderPhone" id="riderPhone" value="<%=lostandfoundBo.getPhoneNumber()==null?"":lostandfoundBo.getPhoneNumber()%>" />
								</td>
							</tr>
							<tr>
								<td>Trip ID</td>
								<td><input type="text" name="tripId_DLF" id="tripId_DLF" value="<%= lostandfoundBo.getTripId() != null ? lostandfoundBo.getTripId() : "" %>" /></td>
								<td></td>
								<td>Item Name</td>
								<td><input type="text" name="itemName_DLF" value="<%= lostandfoundBo.getItemName() != null ? lostandfoundBo.getItemName() : "" %>" /></td>
							</tr>
							<tr>
								<td>Item Description</td>
								<td colspan="6"><textarea name="itemDesc_DLF" rows="4" cols="25"><%= lostandfoundBo.getItemDesc() != null ? lostandfoundBo.getItemDesc() : ""  %>
								</textarea></td>
							</tr>
							<tr>
								<td>Trip Date</td>
								<td><input type="text" size="10" name="tripDate_DLF" id="tripDate_DLF" value="<%= lostandfoundBo.getLfdate() != null ? lostandfoundBo.getLfdate() : "" %>"
									onfocus="showCalendarControl(this);" readonly="readonly" /></td>
								<td></td>
								<td>Time</td>
								<td><input type="text" name="time_DLF" id="time_DLF" value="<%= lostandfoundBo.getLftime() != null ? lostandfoundBo.getLftime() : "" %>" /></td>
							</tr>
							<tr></tr>
							<br />
								<% if( lostandfoundBo.getLfKey() != null &&  !lostandfoundBo.getLfKey().equalsIgnoreCase("null")) { %>
								<tr align="center">
								<td colspan="2" align="center">Status</td>
								<td colspan="3" align="left">
									<% if(lostandfoundBo.getStatus()==null || lostandfoundBo.getStatus().equals("null")) {%>
									<select name="status_DLF">
										<option value="opened">Opened</option>
										<option value="inprogress">InProgress</option>
										<option value="closed">Closed By</option>
								</select> <%}else{ %> <select name="status_DLF">
										<option value="opened"
											<%=lostandfoundBo.getStatus().equals("opened")?"selected":"" %>>Opened</option>
										<option value="inprogress"
											<%=lostandfoundBo.getStatus().equals("inprogress")?"selected":"" %>>InProgress</option>
										<option value="closed"
											<%=lostandfoundBo.getStatus().equals("closed")?"selected":"" %>>Closed</option>
								</select> <%} %>
								</td>
								</tr>
								<tr align="center">
								<td colspan="5">
										<input type="submit" name="update" value="Update" />											
								</td>
								</tr>
								<% } else { %>
								<tr align="center">
								<td colspan="2" align="right">Status</td>
								<td colspan="3" align="left">
								<select name="status_DLF">
										<option value="opened">Opened</option>
										<option value="inprogress" DISABLED>InProgress</option>
										<option value="closed" DISABLED>Closed</option>
								</select></td>
								</tr>
								<tr align="center">
								<td colspan="5">
										<input type="submit" name="save" value="Save" />							
								</td>		
								</tr>						
									<% } %>							
						</table>
					</td>
				</tr>
			</table>
		</div>
		
		<div id="miscellaneous" style="display: none; width:90%" align="center" >
		<center> <b>Given by Miscellaneous </b></center>
			<br /><table width="70%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr align="center">
					<td align="center">
						<table id="bodypage" width="60%" align="center">
							<tr>
								<td>Trip Id</td>
								<td><input type="text" name="tripid_MLF" value="<%= lostandfoundBo.getTripId() != null ? lostandfoundBo.getTripId() : "" %>" /></td>
							</tr>
							<tr>
								<td>Item Name</td>
								<td><input type="text" name="itemName_MLF" value="<%= lostandfoundBo.getItemName() != null ? lostandfoundBo.getItemName() : "" %>" /></td>
							</tr>
							<tr>
								<td>Item Description</td>
								<td><textarea name="itemDesc_MLF" rows="4" cols="25"><%= lostandfoundBo.getItemDesc() != null ? lostandfoundBo.getItemDesc() : ""   %></textarea></td>
							</tr>
							<tr>
								<td>Trip Date</td>
								<td><input type="text" size="10" name="tripDate_MLF" value="<%= lostandfoundBo.getLfdate() != null ? lostandfoundBo.getLfdate() : ""  %>"
									onfocus="showCalendarControl(this);" readonly="readonly" /></td>
							</tr>
							<tr>
								<td>Time</td>
								<td><input type="text" name="time_MLF" id="time" value="<%= lostandfoundBo.getLftime() != null ? lostandfoundBo.getLftime() : "" %>" /></td>
							</tr>
							<tr></tr>
								<tr></tr>
 							<% if( lostandfoundBo.getLfKey() != null &&  !lostandfoundBo.getLfKey().equalsIgnoreCase("null")) { %>
							<tr align="center">
								<td colspan="1">Status</td>
								<td align="left" colspan="1">
								<% if(lostandfoundBo.getStatus()==null || lostandfoundBo.getStatus().equals("null")) {%>
								<select name="status_MLF">
										<option value="opened">Opened</option>
										<option value="inprogress">InProgress</option>
										<option value="closed">Closed</option>
								</select> 
								<%}else{ %> 
								<select name="status_MLF">
										<option value="opened"
											<%=lostandfoundBo.getStatus().equals("opened")?"selected":"" %>>Opened</option>
										<option value="inprogress"
											<%=lostandfoundBo.getStatus().equals("inprogress")?"selected":"" %>>InProgress</option>
										<option value="closed"
											<%=lostandfoundBo.getStatus().equals("closed")?"selected":"" %>>Closed</option>
								</select> 													
								<%}%>
								</td>	
								</tr> 
								<tr align="center">
								<td align="center">
										<input type="submit" name="update" value="Update"></input>											
								</td>
							</tr>
								<%} else { %> 
								<tr align="center">
								<td colspan="1">Status</td>
								<td align="left" colspan="1">
								<select name="status_MLF">
										<option value="opened">Opened</option>
										<option value="inprogress" DISABLED>InProgress</option>
										<option value="closed" DISABLED>Closed</option>
								</select>
								</td>
							</tr>
							<tr align="center">
								<td colspan="2">
								<input type="submit" name="save" value="Save" />
								</td>
							</tr>
							<% } %>
						</table>
					</td>
				</tr>
			</table>
		</div>
		<div id="details" style="display: none; margin-top: -400px; margin-right: -450px;" align="right"> </div>
</body>
</html>



