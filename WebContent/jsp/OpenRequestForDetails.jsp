<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<jsp:useBean id="openBO" class="com.tds.tdsBO.OpenRequestBO" />

<%AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user"); %>

		<form name="masterForm" id="masterForm" action="control"  method="post">
				<input type="hidden" name="<%= TDSConstants.actionParam %>"
			value="<%= TDSConstants.requestAction %>" /> <input type="hidden"
			name="<%= TDSConstants.eventParam %>"
			value="<%= TDSConstants.saveOpenRequest %>" /> 
				<%String driverOrCab;
if(adminBO.getDispatchBasedOnVehicleOrDriver()==2){
	driverOrCab = "Cab";
}else
	driverOrCab= "Driver";%>	

		<input type="hidden" name="sLatitude" id="sLatitudeDash" value=""/>
		<input type="hidden" name="sLongitude" id="sLongitudeDash" value=""/>
		<input type="hidden" name="eLatitude" id="eLatitude" value=""/>
		<input type="hidden" name="eLongitude" id="eLongitude" value=""/>
		<input type="hidden" name="nextScreen" id="nextScreen" value="dashBoard"/>
		<input type="hidden" id="masterAddress" name="masterAddress" size="20"	value="" ></input>
		<input type="hidden" id="addressKey" name="addressKey" size="20"	value="" ></input>
		<input type="hidden" id="addressKeyTo" name="addressKeyTo" size="20"	value="" ></input>
		<input type="hidden" id="LMKeyDash" name="LMKeyDash" size="20"	value="" ></input>
		<input type="hidden" id="saveOpenReq" name="saveOpenReq" size="20"	value="saveOpenReq" ></input>
		<input type="hidden" id="tripId" name="tripId" size="20"	value="" ></input>
		<input type="hidden" id="lineNumber" name="lineNumber" size="20"	value="" ></input>
		<input type="hidden" id="accept" name="accept" size="20"	value="<%=adminBO.getUid()%>" ></input>
		<input type="hidden" id="tripId" name="tripId" size="20"	value="" ></input>
		<input type="hidden" name="sstate" size="2" id="sstateDash" value="<%=adminBO.getState()%>"/>
		<input type="hidden" name="szip" size="5" id="szipDash" value=""></input>
		<input type="hidden" name="estate" size="2" id="estateDash" value="<%=adminBO.getState()%>"/>
		<input type="hidden" name="ezip" size="5" id="ezipDash" value=""></input>	
		<input type="hidden" name="origin" id="originDash" value=""></input>
		<input type="hidden" name="destination" id="destinationDash" value=""></input>
		<input type="hidden" name="phoneTemp" id="phoneTemp" value=""></input>
		<input type="hidden" name="tripAmount" id="tripAmount" value="<%=adminBO.getRatePerMile()%>"></input>
		<input type="hidden" name="repeatGroup" id="repeatGroupDash" value=""/>
		<input type="hidden" name="multiJobUpdate" id="multiJobUpdateDash" value=""/>
		<input type="hidden" name="driverORcabDash" id="driverORcabDash" value="<%=driverOrCab%>"/>
		<input type="hidden" name="phoneVerificationDash" id="phoneVerificationDash" value=""/>
		<input type="hidden" name="CommentsTemporaryDash" id="CommentsTemporaryDash" value=""/>
		<input type="hidden" name="addressEnter" id="addressEnter" value=""/>
		<input type="hidden" name="timeTemporary" id="timeTemporary" value=""/>
		<input type="hidden" id="tripStatus" value=""/>
		<input type="hidden" id="updateAll" value=""/>
		<input type="hidden" id="DriverCabSwitch" value=""/>
		<input type="hidden" name="fieldSize" id="fieldSize" value=""/> 
		<%if((adminBO.getORTime()).equals("textBox")){ %>
			<input type="hidden" id="timeType" value="1"/>
		<%} else { %>
			<input type="hidden" id="timeType" value="2"/>
		<%} %>
		
			<table style="width: 600px;height: 330px;border: medium solid grey; width: 550px; height: 330px; -moz-border-radius: 8px 8px 8px 8px;" border="0" cellspacing="0" cellpadding="0"class="driverChargTab" bgcolor="#f8f8ff">

					<tr>
						<td  colspan='3' align='center' style="background-color:#d3d3d3;size: 500px"><h5>
								<center><font size="3" style="margin-left: 200px;text-overflow: ellipsis;overflow:hidden; white-space:nowrap;">Create A Job</font></center></td><td align="right" style="background-color:#d3d3d3;">
							</h5>
							<a style="margin-left:60px;background-color:#d3d3d3" href="#"><img alt="" style="z-index: 4000;" src="images/Dashboard/close.png" onclick="removeORDetails()"></a>
					</td>
					</tr> 
									<tr>  
										<td class="firstCol" colspan="4" width="20%"><font size="2">&nbsp;Phone No&nbsp;</font><input type="text" size="10" name="phone" id="phoneDash"
											value="" autocomplete="off" />
									  <font size="2">No Ph</font><input type="checkbox" name="noShow" id="noShow"></input>
										<font size="2">&nbsp;&nbsp;&nbsp;Name&nbsp;</font><input type="text" size="10" id="nameDash" name="name"
											value="" /><font size="2">&nbsp;&nbsp;DD:</font><input type="checkbox" name="dispatchStatus" id="dispatchStatusDash"></input>
										  <font size="2">SR</font><input type="checkbox" name="sharedRide" id="sharedRideDash"></input>
										  <font size="2">RT</font><input  type="checkbox" name="roundTrip" id="roundTrip"/>
										  <font size="2">VIP</font><input type="checkbox" name="premiumCustomerDash" id="premiumCustomerDash" <%= (openBO.getPremiumCustomer()==1)?"checked":"" %>
											style="vertical-align: middle" /></td></tr>
											<tr>  
										<td class="firstCol" colspan="4" width="20%"><font size="2">&nbsp;Email&nbsp;</font><input type="text" size="15" name="eMail" id="eMailDash"
											value="" autocomplete="off" />
										<input type="button" value="Translate" onclick="showProvider()"/>	
									  <font size="2">No.Of.Passenger</font><input type="text" size="5" name="numberOfPassengerDash" id="numberOfPassengerDash" value=""/></input>
				Cabs
				<select name="numOfCabs" id="numOfCabs"  style="border: 1px solid #C0C0C0; font-size:14px; height:20px; text-align:left; width:75px; margin:0; padding:0" >
                	 				 <option value="1" selected>1</option>
                                     <option value="2" <%=openBO.getNumOfCabs()==2?"selected":"" %>>2</option>
                                     <option value="3" <%=openBO.getNumOfCabs()==3?"selected":"" %>>3</option>
                                     <option value="4" <%=openBO.getNumOfCabs()==4?"selected":"" %>>4</option>
                                     <option value="5" <%=openBO.getNumOfCabs()==5?"selected":"" %>>5</option>
                                     <option value="6" <%=openBO.getNumOfCabs()==6?"selected":"" %>>6</option>
                                     <option value="7" <%=openBO.getNumOfCabs()==7?"selected":"" %>>7</option>
                                     <option value="8" <%=openBO.getNumOfCabs()==8?"selected":"" %>>8</option>
                                     <option value="9" <%=openBO.getNumOfCabs()==9?"selected":"" %>>9</option>
                                     <option value="10" <%=openBO.getNumOfCabs()==10?"selected":"" %>>10</option>

                </select></td></tr>
										<tr><td class="firstCol" colspan="4"><font size="2">Date</font><input style="width: 80px; height: 15px;" type="text" name="sdate" id="sdate"
											value="<%= openBO.getSdate()!=null?openBO.getSdate():""  %>" readonly="readonly"  />
											<%if((adminBO.getORTime()).equals("textBox")){ %>
											<font size="2">Time</font><input type="text" id="shrs" name="shrs" maxlength="4" style="width: 40px; height: 15px;font-size: 14px"
											value="<%= openBO.getShrs()!=null?openBO.getShrs():""%>"
											/> 
											<%}else{ %>
											<font size="2">Time</font>
											 <select name="shrs" id="shrs" style="border: 1px solid #C0C0C0; font-size:13px; height:20px; text-align:left; width:70px; margin:0; padding:0" >
											     <option value="Now" selected >Now</option>
											     <option value="NCH" selected >NoChange</option>
											     <option value="0000" <%=openBO.getShrs().equals("2400")?"selected":"" %>>12:00 A</option>
                                                 <option value="0015" <%=openBO.getShrs().equals("2415")?"selected":"" %>>12:15 A</option>
                                                 <option value="0030" <%=openBO.getShrs().equals("2430")?"selected":"" %>>12:30 A</option>
                                                 <option value="0045" <%=openBO.getShrs().equals("2445")?"selected":"" %>>12:45 A</option>
                                                 <option value="0100" <%=openBO.getShrs().equals("0100")?"selected":"" %>>01:00 A</option>
                                                 <option value="0115" <%=openBO.getShrs().equals("0115")?"selected":"" %>>01:15 A</option>
                                                 <option value="0130" <%=openBO.getShrs().equals("0130")?"selected":"" %>>01:30 A</option>
                                                 <option value="0145" <%=openBO.getShrs().equals("0145")?"selected":"" %>>01:45 A</option>
                                                 <option value="0200" <%=openBO.getShrs().equals("0200")?"selected":"" %>>02:00 A</option>
                                                 <option value="0215" <%=openBO.getShrs().equals("0215")?"selected":"" %>>02:15 A</option>
                                                 <option value="0230" <%=openBO.getShrs().equals("0230")?"selected":"" %>>02:30 A</option>
                                                 <option value="0245" <%=openBO.getShrs().equals("0245")?"selected":"" %>>02:45 A</option>
                                                 <option value="0300" <%=openBO.getShrs().equals("0300")?"selected":"" %>>03:00 A</option>
                                                 <option value="0315" <%=openBO.getShrs().equals("0315")?"selected":"" %>>03:15 A</option>
                                                 <option value="0330" <%=openBO.getShrs().equals("0330")?"selected":"" %>>03:30 A</option>
                                                 <option value="0345" <%=openBO.getShrs().equals("0345")?"selected":"" %>>03:45 A</option>
                                                 <option value="0400" <%=openBO.getShrs().equals("0400")?"selected":"" %>>04:00 A</option>
                                                 <option value="0415" <%=openBO.getShrs().equals("0415")?"selected":"" %>>04:15 A</option>
                                                 <option value="0430" <%=openBO.getShrs().equals("0430")?"selected":"" %>>04:30 A</option>
                                                 <option value="0445" <%=openBO.getShrs().equals("0445")?"selected":"" %>>04:45 A</option>
                                                 <option value="0500" <%=openBO.getShrs().equals("0500")?"selected":"" %>>05:00 A</option>
                                                 <option value="0515" <%=openBO.getShrs().equals("0515")?"selected":"" %>>05:15 A</option>
                                                 <option value="0530" <%=openBO.getShrs().equals("0530")?"selected":"" %>>05:30 A</option>
                                                 <option value="0545" <%=openBO.getShrs().equals("0545")?"selected":"" %>>05:45 A</option>
                                                 <option value="0600" <%=openBO.getShrs().equals("0600")?"selected":"" %>>06:00 A</option>
                                                 <option value="0615" <%=openBO.getShrs().equals("0615")?"selected":"" %>>06:15 A</option>
                                                 <option value="0630" <%=openBO.getShrs().equals("0630")?"selected":"" %>>06:30 A</option>
                                                 <option value="0645" <%=openBO.getShrs().equals("0645")?"selected":"" %>>06:45 A</option>
                                                 <option value="0700" <%=openBO.getShrs().equals("0700")?"selected":"" %>>07:00 A</option>
                                                 <option value="0715" <%=openBO.getShrs().equals("0715")?"selected":"" %>>07:15 A</option>
                                                 <option value="0730" <%=openBO.getShrs().equals("0730")?"selected":"" %>>07:30 A</option>
                                                 <option value="0745" <%=openBO.getShrs().equals("0745")?"selected":"" %>>07:45 A</option>
                                                 <option value="0800" <%=openBO.getShrs().equals("0800")?"selected":"" %>>08:00 A</option>
                                                 <option value="0815" <%=openBO.getShrs().equals("0815")?"selected":"" %>>08:15 A</option>
                                                 <option value="0830" <%=openBO.getShrs().equals("0830")?"selected":"" %>>08:30 A</option>
                                                 <option value="0845" <%=openBO.getShrs().equals("0845")?"selected":"" %>>08:45 A</option>
                                                 <option value="0900" <%=openBO.getShrs().equals("0900")?"selected":"" %>>09:00 A</option>
                                                 <option value="0915" <%=openBO.getShrs().equals("0915")?"selected":"" %>>09:15 A</option>
                                                 <option value="0930" <%=openBO.getShrs().equals("0930")?"selected":"" %>>09:30 A</option>
                                                 <option value="0945" <%=openBO.getShrs().equals("0945")?"selected":"" %>>09:45 A</option>
                                                 <option value="1000" <%=openBO.getShrs().equals("1000")?"selected":"" %>>10:00 A</option>
                                                 <option value="1015" <%=openBO.getShrs().equals("1015")?"selected":"" %>>10:15 A</option>
                                                 <option value="1030" <%=openBO.getShrs().equals("1030")?"selected":"" %>>10:30 A</option>
                                                 <option value="1045" <%=openBO.getShrs().equals("1045")?"selected":"" %>>10:45 A</option>
                                                 <option value="1100" <%=openBO.getShrs().equals("1100")?"selected":"" %>>11:00 A</option>
                                                 <option value="1115" <%=openBO.getShrs().equals("1115")?"selected":"" %>>11:15 A</option>
                                                 <option value="1130" <%=openBO.getShrs().equals("1130")?"selected":"" %>>11:30 A</option>
                                                 <option value="1145" <%=openBO.getShrs().equals("1145")?"selected":"" %>>11:45 A</option>
                                                 <option value="1200" <%=openBO.getShrs().equals("1200")?"selected":"" %>>12:00 P</option>
                                                 <option value="1215" <%=openBO.getShrs().equals("1215")?"selected":"" %>>12:15 P</option>
                                                 <option value="1230" <%=openBO.getShrs().equals("1230")?"selected":"" %>>12:30 P</option>
                                                 <option value="1245" <%=openBO.getShrs().equals("1245")?"selected":"" %>>12:45 P</option>
                                                 <option value="1300" <%=openBO.getShrs().equals("1300")?"selected":"" %>>01:00 P</option>
                                                 <option value="1315" <%=openBO.getShrs().equals("1315")?"selected":"" %>>01:15 P</option>
                                                 <option value="1330" <%=openBO.getShrs().equals("1330")?"selected":"" %>>01:30 P</option>
                                                 <option value="1345" <%=openBO.getShrs().equals("1345")?"selected":"" %>>01:45 P</option>
                                                 <option value="1400" <%=openBO.getShrs().equals("1400")?"selected":"" %>>02:00 P</option>
                                                 <option value="1415" <%=openBO.getShrs().equals("1415")?"selected":"" %>>02:15 P</option>
                                                 <option value="1430" <%=openBO.getShrs().equals("1430")?"selected":"" %>>02:30 P</option>
                                                 <option value="1445" <%=openBO.getShrs().equals("1445")?"selected":"" %>>02:45 P</option>
                                                 <option value="1500" <%=openBO.getShrs().equals("1500")?"selected":"" %>>03:00 P</option>
                                                 <option value="1515" <%=openBO.getShrs().equals("1515")?"selected":"" %>>03:15 P</option>
                                                 <option value="1530" <%=openBO.getShrs().equals("1530")?"selected":"" %>>03:30 P</option>
                                                 <option value="1545" <%=openBO.getShrs().equals("1545")?"selected":"" %>>03:45 P</option>
                                                 <option value="1600" <%=openBO.getShrs().equals("1600")?"selected":"" %>>04:00 P</option>
                                                 <option value="1615" <%=openBO.getShrs().equals("1615")?"selected":"" %>>04:15 P</option>
                                                 <option value="1630" <%=openBO.getShrs().equals("1630")?"selected":"" %>>04:30 P</option>
                                                 <option value="1645" <%=openBO.getShrs().equals("1645")?"selected":"" %>>04:45 P</option>
                                                 <option value="1700" <%=openBO.getShrs().equals("1700")?"selected":"" %>>05:00 P</option>
                                                 <option value="1715" <%=openBO.getShrs().equals("1715")?"selected":"" %>>05:15 P</option>
                                                 <option value="1730" <%=openBO.getShrs().equals("1730")?"selected":"" %>>05:30 P</option>
												 <option value="1745" <%=openBO.getShrs().equals("1745")?"selected":"" %>>05:45 P</option>
                                                 <option value="1800" <%=openBO.getShrs().equals("1800")?"selected":"" %>>06:00 P</option>
                                                 <option value="1815" <%=openBO.getShrs().equals("1815")?"selected":"" %>>06:15 P</option>
                                                 <option value="1830" <%=openBO.getShrs().equals("1830")?"selected":"" %>>06:30 P</option>
                                                 <option value="1845" <%=openBO.getShrs().equals("1845")?"selected":"" %>>06:45 P</option>
                                                 <option value="1900" <%=openBO.getShrs().equals("1900")?"selected":"" %>>07:00 P</option>
                                                 <option value="1915" <%=openBO.getShrs().equals("1915")?"selected":"" %>>07:15 P</option>
                                                 <option value="1930" <%=openBO.getShrs().equals("1930")?"selected":"" %>>07:30 P</option>
                                                 <option value="1945" <%=openBO.getShrs().equals("1945")?"selected":"" %>>07:45 P</option>
                                                 <option value="2000" <%=openBO.getShrs().equals("2000")?"selected":"" %>>08:00 P</option>
                                                 <option value="2015" <%=openBO.getShrs().equals("2015")?"selected":"" %>>08:15 P</option>
                                                 <option value="2030" <%=openBO.getShrs().equals("2030")?"selected":"" %>>08:30 P</option>
                                                 <option value="2045" <%=openBO.getShrs().equals("2045")?"selected":"" %>>08:45 P</option>
                                                 <option value="2100" <%=openBO.getShrs().equals("2100")?"selected":"" %>>09:00 P</option>
                                                 <option value="2115" <%=openBO.getShrs().equals("2115")?"selected":"" %>>09:15 P</option>
                                                 <option value="2130" <%=openBO.getShrs().equals("2130")?"selected":"" %>>09:30 P</option>
                                                 <option value="2145" <%=openBO.getShrs().equals("2145")?"selected":"" %>>09:45 P</option>
                                                 <option value="2200" <%=openBO.getShrs().equals("2200")?"selected":"" %>>10:00 P</option>
                                                 <option value="2215" <%=openBO.getShrs().equals("2215")?"selected":"" %>>10:15 P</option>
                                                 <option value="2230" <%=openBO.getShrs().equals("2230")?"selected":"" %>>10:30 P</option>
                                                 <option value="2245" <%=openBO.getShrs().equals("2245")?"selected":"" %>>10:45 P</option>
                                                 <option value="2300" <%=openBO.getShrs().equals("2300")?"selected":"" %>>11:00 P</option>
                                                 <option value="2315" <%=openBO.getShrs().equals("2315")?"selected":"" %>>11:15 P</option>
                                                 <option value="2330" <%=openBO.getShrs().equals("2330")?"selected":"" %>>11:30 P</option>
                                                 <option value="2345" <%=openBO.getShrs().equals("2345")?"selected":"" %>>11:45 P</option>
                                                 </select>
											<%} %>
											<font size="2">Adv.Time</font>
											 <select name="advanceTime" id="advanceTime" style="border: 1px solid #C0C0C0; font-size:14px; height:20px; text-align:left; width:44px; margin:0; padding:0" >
											     <option value="-1" selected >Dflt</option>
                                               	<option value="15">15</option>
                                                 <option value="30">30</option>
                                                 <option value="45">45</option>
                                                 <option value="60">60</option>
                                                 <option value="75">75</option>
                                                 <option value="90">90</option>
                                                 <option value="105">105</option>
                                                 <option value="120">120</option>
                                                 
                                                 </select><!-- <input type="text" size="1"  value="Dflt" name="advanceTime" id="advanceTime"></input> --> 
									<c id="dateTime" class="firstCol"   style="size: 10px;display:none;">
									<font size='2'>Date</font>
												 <input style="width: 70px; height: 15px;font-size: 14px" type="text" name="sdateTrip" id="sdateTrip"
											value="<%= openBO.getSdate()!=null?openBO.getSdate():""  %>" />
											<select name="shrsTrip" id="shrsTrip" onkeyup="KeyCheck(event,'nameId','sintersection')" style="border: 1px solid #C0C0C0; font-size:13px; height:20px; text-align:left; width:70px; margin:0; padding:0" >
                                                 <option value="0000" <%=openBO.getShrs().equals("2400")?"selected":"" %>>12:00 A</option>
                                                 <option value="0015" <%=openBO.getShrs().equals("2415")?"selected":"" %>>12:15 A</option>
                                                 <option value="0030" <%=openBO.getShrs().equals("2430")?"selected":"" %>>12:30 A</option>
                                                 <option value="0045" <%=openBO.getShrs().equals("2445")?"selected":"" %>>12:45 A</option>
                                                 <option value="0100" <%=openBO.getShrs().equals("0100")?"selected":"" %>>01:00 A</option>
                                                 <option value="0115" <%=openBO.getShrs().equals("0115")?"selected":"" %>>01:15 A</option>
                                                 <option value="0130" <%=openBO.getShrs().equals("0130")?"selected":"" %>>01:30 A</option>
                                                 <option value="0145" <%=openBO.getShrs().equals("0145")?"selected":"" %>>01:45 A</option>
                                                 <option value="0200" <%=openBO.getShrs().equals("0200")?"selected":"" %>>02:00 A</option>
                                                 <option value="0215" <%=openBO.getShrs().equals("0215")?"selected":"" %>>02:15 A</option>
                                                 <option value="0230" <%=openBO.getShrs().equals("0230")?"selected":"" %>>02:30 A</option>
                                                 <option value="0245" <%=openBO.getShrs().equals("0245")?"selected":"" %>>02:45 A</option>
                                                 <option value="0300" <%=openBO.getShrs().equals("0300")?"selected":"" %>>03:00 A</option>
                                                 <option value="0315" <%=openBO.getShrs().equals("0315")?"selected":"" %>>03:15 A</option>
                                                 <option value="0330" <%=openBO.getShrs().equals("0330")?"selected":"" %>>03:30 A</option>
                                                 <option value="0345" <%=openBO.getShrs().equals("0345")?"selected":"" %>>03:45 A</option>
                                                 <option value="0400" <%=openBO.getShrs().equals("0400")?"selected":"" %>>04:00 A</option>
                                                 <option value="0415" <%=openBO.getShrs().equals("0415")?"selected":"" %>>04:15 A</option>
                                                 <option value="0430" <%=openBO.getShrs().equals("0430")?"selected":"" %>>04:30 A</option>
                                                 <option value="0445" <%=openBO.getShrs().equals("0445")?"selected":"" %>>04:45 A</option>
                                                 <option value="0500" <%=openBO.getShrs().equals("0500")?"selected":"" %>>05:00 A</option>
                                                 <option value="0515" <%=openBO.getShrs().equals("0515")?"selected":"" %>>05:15 A</option>
                                                 <option value="0530" <%=openBO.getShrs().equals("0530")?"selected":"" %>>05:30 A</option>
                                                 <option value="0545" <%=openBO.getShrs().equals("0545")?"selected":"" %>>05:45 A</option>
                                                 <option value="0600" <%=openBO.getShrs().equals("0600")?"selected":"" %>>06:00 A</option>
                                                 <option value="0615" <%=openBO.getShrs().equals("0615")?"selected":"" %>>06:15 A</option>
                                                 <option value="0630" <%=openBO.getShrs().equals("0630")?"selected":"" %>>06:30 A</option>
                                                 <option value="0645" <%=openBO.getShrs().equals("0645")?"selected":"" %>>06:45 A</option>
                                                 <option value="0700" <%=openBO.getShrs().equals("0700")?"selected":"" %>>07:00 A</option>
                                                 <option value="0715" <%=openBO.getShrs().equals("0715")?"selected":"" %>>07:15 A</option>
                                                 <option value="0730" <%=openBO.getShrs().equals("0730")?"selected":"" %>>07:30 A</option>
                                                 <option value="0745" <%=openBO.getShrs().equals("0745")?"selected":"" %>>07:45 A</option>
                                                 <option value="0800" <%=openBO.getShrs().equals("0800")?"selected":"" %>>08:00 A</option>
                                                 <option value="0815" <%=openBO.getShrs().equals("0815")?"selected":"" %>>08:15 A</option>
                                                 <option value="0830" <%=openBO.getShrs().equals("0830")?"selected":"" %>>08:30 A</option>
                                                 <option value="0845" <%=openBO.getShrs().equals("0845")?"selected":"" %>>08:45 A</option>
                                                 <option value="0900" <%=openBO.getShrs().equals("0900")?"selected":"" %>>09:00 A</option>
                                                 <option value="0915" <%=openBO.getShrs().equals("0915")?"selected":"" %>>09:15 A</option>
                                                 <option value="0930" <%=openBO.getShrs().equals("0930")?"selected":"" %>>09:30 A</option>
                                                 <option value="0945" <%=openBO.getShrs().equals("0945")?"selected":"" %>>09:45 A</option>
                                                 <option value="1000" <%=openBO.getShrs().equals("1000")?"selected":"" %>>10:00 A</option>
                                                 <option value="1015" <%=openBO.getShrs().equals("1015")?"selected":"" %>>10:15 A</option>
                                                 <option value="1030" <%=openBO.getShrs().equals("1030")?"selected":"" %>>10:30 A</option>
                                                 <option value="1045" <%=openBO.getShrs().equals("1045")?"selected":"" %>>10:45 A</option>
                                                 <option value="1100" <%=openBO.getShrs().equals("1100")?"selected":"" %>>11:00 A</option>
                                                 <option value="1115" <%=openBO.getShrs().equals("1115")?"selected":"" %>>11:15 A</option>
                                                 <option value="1130" <%=openBO.getShrs().equals("1130")?"selected":"" %>>11:30 A</option>
                                                 <option value="1145" <%=openBO.getShrs().equals("1145")?"selected":"" %>>11:45 A</option>
                                                 <option value="1200" <%=openBO.getShrs().equals("1200")?"selected":"" %>>12:00 P</option>
                                                 <option value="1215" <%=openBO.getShrs().equals("1215")?"selected":"" %>>12:15 P</option>
                                                 <option value="1230" <%=openBO.getShrs().equals("1230")?"selected":"" %>>12:30 P</option>
                                                 <option value="1245" <%=openBO.getShrs().equals("1245")?"selected":"" %>>12:45 P</option>
                                                 <option value="1300" <%=openBO.getShrs().equals("1300")?"selected":"" %>>01:00 P</option>
                                                 <option value="1315" <%=openBO.getShrs().equals("1315")?"selected":"" %>>01:15 P</option>
                                                 <option value="1330" <%=openBO.getShrs().equals("1330")?"selected":"" %>>01:30 P</option>
                                                 <option value="1345" <%=openBO.getShrs().equals("1345")?"selected":"" %>>01:45 P</option>
                                                 <option value="1400" <%=openBO.getShrs().equals("1400")?"selected":"" %>>02:00 P</option>
                                                 <option value="1415" <%=openBO.getShrs().equals("1415")?"selected":"" %>>02:15 P</option>
                                                 <option value="1430" <%=openBO.getShrs().equals("1430")?"selected":"" %>>02:30 P</option>
                                                 <option value="1445" <%=openBO.getShrs().equals("1445")?"selected":"" %>>02:45 P</option>
                                                 <option value="1500" <%=openBO.getShrs().equals("1500")?"selected":"" %>>03:00 P</option>
                                                 <option value="1515" <%=openBO.getShrs().equals("1515")?"selected":"" %>>03:15 P</option>
                                                 <option value="1530" <%=openBO.getShrs().equals("1530")?"selected":"" %>>03:30 P</option>
                                                 <option value="1545" <%=openBO.getShrs().equals("1545")?"selected":"" %>>03:45 P</option>
                                                 <option value="1600" <%=openBO.getShrs().equals("1600")?"selected":"" %>>04:00 P</option>
                                                 <option value="1615" <%=openBO.getShrs().equals("1615")?"selected":"" %>>04:15 P</option>
                                                 <option value="1630" <%=openBO.getShrs().equals("1630")?"selected":"" %>>04:30 P</option>
                                                 <option value="1645" <%=openBO.getShrs().equals("1645")?"selected":"" %>>04:45 P</option>
                                                 <option value="1700" <%=openBO.getShrs().equals("1700")?"selected":"" %>>05:00 P</option>
                                                 <option value="1715" <%=openBO.getShrs().equals("1715")?"selected":"" %>>05:15 P</option>
                                                 <option value="1730" <%=openBO.getShrs().equals("1730")?"selected":"" %>>05:30 P</option>
                                                 <option value="1745" <%=openBO.getShrs().equals("1745")?"selected":"" %>>05:45 P</option>
                                                 <option value="1800" <%=openBO.getShrs().equals("1800")?"selected":"" %>>06:00 P</option>
                                                 <option value="1815" <%=openBO.getShrs().equals("1815")?"selected":"" %>>06:15 P</option>
                                                 <option value="1830" <%=openBO.getShrs().equals("1830")?"selected":"" %>>06:30 P</option>
                                                 <option value="1845" <%=openBO.getShrs().equals("1845")?"selected":"" %>>06:45 P</option>
                                                 <option value="1900" <%=openBO.getShrs().equals("1900")?"selected":"" %>>07:00 P</option>
                                                 <option value="1915" <%=openBO.getShrs().equals("1915")?"selected":"" %>>07:15 P</option>
                                                 <option value="1930" <%=openBO.getShrs().equals("1930")?"selected":"" %>>07:30 P</option>
                                                 <option value="1945" <%=openBO.getShrs().equals("1945")?"selected":"" %>>07:45 P</option>
                                                 <option value="2000" <%=openBO.getShrs().equals("2000")?"selected":"" %>>08:00 P</option>
                                                 <option value="2015" <%=openBO.getShrs().equals("2015")?"selected":"" %>>08:15 P</option>
                                                 <option value="2030" <%=openBO.getShrs().equals("2030")?"selected":"" %>>08:30 P</option>
                                                 <option value="2045" <%=openBO.getShrs().equals("2045")?"selected":"" %>>08:45 P</option>
                                                 <option value="2100" <%=openBO.getShrs().equals("2100")?"selected":"" %>>09:00 P</option>
                                                 <option value="2115" <%=openBO.getShrs().equals("2115")?"selected":"" %>>09:15 P</option>
                                                 <option value="2130" <%=openBO.getShrs().equals("2130")?"selected":"" %>>09:30 P</option>
                                                 <option value="2145" <%=openBO.getShrs().equals("2145")?"selected":"" %>>09:45 P</option>
                                                 <option value="2200" <%=openBO.getShrs().equals("2200")?"selected":"" %>>10:00 P</option>
                                                 <option value="2215" <%=openBO.getShrs().equals("2215")?"selected":"" %>>10:15 P</option>
                                                 <option value="2230" <%=openBO.getShrs().equals("2230")?"selected":"" %>>10:30 P</option>
                                                 <option value="2245" <%=openBO.getShrs().equals("2245")?"selected":"" %>>10:45 P</option>
                                                 <option value="2300" <%=openBO.getShrs().equals("2300")?"selected":"" %>>11:00 P</option>
                                                 <option value="2315" <%=openBO.getShrs().equals("2315")?"selected":"" %>>11:15 P</option>
                                                 <option value="2330" <%=openBO.getShrs().equals("2330")?"selected":"" %>>11:30 P</option>
  											     <option value="2345" <%=openBO.getShrs().equals("2345")?"selected":"" %>>11:45 P</option>
                                                 </select>
											</c></font></td>
											</tr>
											<!-- <tr>
						<td colspan='4' align='center' style="background-color:#d3d3d3;size: 500px"><h5>
								<center><font size="3">Payment</font></center>
							</h5>
					</td>		
					</tr> -->
				
					<tr>
						<td colspan="4" align="center"><font size="2">Pmt:</font>
						<select name="paytype" id="payTypeDash" style="width:88px;" >
								<option value="0">Select</option>
								<option value="Cash" selected>Cash</option>
								<option value="CC">CC</option>
								<option value="VC">Voucher</option>
						</select>
						<font size="2">Acct:</font>
						<input type="text" size="12" name="acct" id="acctDash" autocomplete="off"/>
						<font size="2">Amt:</font>
						<input type="text" size="3" name="amt" id="amtDash"
							onkeyup="amtNumCheck()" onchange="changeCharges()"/>
						<%ArrayList al_q = (ArrayList)request.getAttribute("al_q");%>
										<font size="2">Zone:</font><select name='queueno' id='queuenoDash'
									onkeyup="KeyCheck(event,'szip','eaircodeid')" style="width:120px;">
										<option value=''>Select</option>
										<%
										if(al_q!=null){
										for(int i=0;i<al_q.size();i=i+2){ 
									%>
										<option value='<%=al_q.get(i).toString() %>'
											<%=openBO.getQueueno().equals(al_q.get(i).toString())?"selected":"" %>><%=al_q.get(i+1).toString() %></option>
										<%
									} %>
								</select> <% } else{  %>


									<div class="div2">
										<div class="div2In">
											<div class="inputBXWrap">
												<input type="hidden" name="queueno" id="queuenoDash"
													value="<%=openBO.getQueueno() %>" />
											</div>
										</div>
									</div> <%} %>	
									
								
						</td>
					</tr>
											
				<tr>
						<td width="100%" colspan="4" style="background-color: lightgreen"><font size="2">P/U A</font><font size="1">ddress</font><input name="address" id="addressDash"  class="ui-autocomplete-input" type="text" size="18"></input>
					<font size="2">L</font><font size="1">and</font><font size="2">M</font><font size="1">ark</font><input name="landMark" id="landMarkDash"  type="text" size="23" autocomplete="off"></input>
					<ajax:autocomplete
  									fieldId="landMarkDash"
  									popupId="model-popup1"
  									targetId="landMarkDash"
  									baseUrl="autocomplete.view"
  									paramName="landmark"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
					<font size="2">N</font><font size="1">ew</font><input type="checkbox" name="newLandMark" id="newLandMark" />
					</td> </tr> 
					<tr>
						<td class="firstCol" colspan="1" width="20%" align="center"><font size="2">Add1</font></td><td colspan="1" width="30%"><input type="text" name="sadd1" size="15" id="sadd1Dash"
											value="" style="color: green" readonly="readonly" autocomplete="off" />
						</td>
						<td class="firstCol" colspan="2"  align="center"><font size="2">Add2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><input type="text" name="sadd2" size="15" id="sadd2Dash"
											value="" />
						</td>
					</tr>
					<tr>
						<td class="firstCol" colspan="1" width="20%" align="center"><font size="2">City</font></td><td colspan="1" width="30%"><input type="text" name="scity" size="15" id="scityDash"
											value="" readonly="readonly"/>
						</td>
						<td colspan="2" align="center" width="30%"><input style="background-color: #f8f8ff;size: 3;color: red;" type="button" name="clearPU" size="15" id="clearPU"
											value="Clear P/U Add."/>
						</td>
					</tr>
					<tr>
						<td width="100%" colspan="4" style="background-color: #fa8072"><font size="2">D/O A</font><font size="1">ddress</font><input name="DropAddressDash" id="DropAddressDash"  class="ui-autocomplete-input" type="text" size="18" ></input>
					<font size="2">L</font><font size="1">and</font><font size="2">M</font><font size="1">ark</font><input name="landMark" id="toLandMarkDash"  type="text" size="23" autocomplete="off" ></input>
					<ajax:autocomplete
  									fieldId="toLandMarkDash"
  									popupId="model-popup1"
  									targetId="toLandMarkDash"
  									baseUrl="autocomplete.view"
  									paramName="landmark"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
					<font size="2">N</font><font size="1">ew</font><input type="checkbox" name="newLandMark" id="newToLandMark" />
					</td> </tr>
					<tr>
						<td class="firstCol" colspan="1" width="20%" align="center"><font size="2">Add1</font></td><td colspan="1" width="30%"><input type="text" name="eadd1" size="15" id="eadd1Dash"
											value="" style="color: green" readonly="readonly" autocomplete="off" />
						</td>
						<td class="firstCol" colspan="2" align="center"><font size="2">Add2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font><input type="text" name="eadd2" size="15" id="eadd2Dash" value="" />
						</td>
					</tr>
					<tr>
						<td class="firstCol" colspan="1" width="20%" align="center"><font size="2">City</font></td><td colspan="1" width="30%"><input type="text" name="ecity" size="15" id="ecityDash"
											value="" readonly="readonly" />
						</td>
						<td colspan="2" align="center" width="30%"><input type="button" style="background-color: #f8f8ff;size: 3;color: red;" name="clearDO" size="15" id="clearDO"
											value="Clear D/O Add."/>
						</td>
					</tr>

				</table>
					<table id="hideTableDriverComments"  width="100%" style="background-color:#f8f8ff;width:550px;-moz-border-radius: 8px 8px 8px 8px;border: medium solid grey;">
						<tr><td width="20" style=""><%=driverOrCab %>Comments</td>
						<td><textarea rows="2" cols="24" name="specialIns1" id="specialInsDash1" style="color: gray;" onfocus="clearSplInsDash(2);this.style.color='#000000'" onkeypress="return checkSplInsLength()">Temporary Comments</textarea></td><td width="25" style=""><textarea rows="2" cols="24" name="specialIns" id="specialInsDash" style="color:gray;" onfocus="clearSplInsDash(1);this.style.color='#000000'" onkeypress="return checkSplInsLength()">Permanent Comments</textarea></td></tr>
					</table>
					<table id="hideFlightInformation" width="100%" style="display: none;background-color:#f8f8ff;width:550px;-moz-border-radius: 8px 8px 8px 8px;border: medium solid grey;" >
									<td colspan="2">Flight Information</td>
									<tr><td>AirlineName:</td><td><input type="text" name="airName" id="airName" value="<%= openBO.getAirName()==null?"":openBO.getAirName()%>" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td><td>AirlineNo:<input type="text" name="airNo" id="airNo" value="<%= openBO.getAirNo()==null?"":openBO.getAirNo()%>" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td></tr><tr><td>From:</td><td><input type="text" name="airFrom" id="airFrom" value="<%= openBO.getAirFrom()==null?"":openBO.getAirFrom()%>"style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td><td>To:<input type="text" name="airTo" id="airTo" value="<%= openBO.getAirTo()==null?"":openBO.getAirTo()%>" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td></tr>
							</table>	
				  	<table id="hideTableComments"  width="100%" style="background-color:#f8f8ff;width:550px;-moz-border-radius: 8px 8px 8px 8px;border: medium solid grey;">
						<tr><td width="20" style="">Dispatch Comments</td>
						<td><textarea rows="2" cols="24" name="CommentsDash1" id="CommentsDash1" style="color: gray;" onfocus="clearCommentsDash(2);this.style.color='#000000'" onkeypress="return checkCommentsLength()">Temporary Comments</textarea></td><td width="25" style=""><textarea rows="2" cols="24" name="CommentsDash" id="CommentsDash" style="color:gray;" onfocus="clearCommentsDash(1);this.style.color='#000000'" onkeypress="return checkCommentsLength()">Permanent Comments</textarea></td></tr>
					<tr>
					<td colspan='7' align='center' style="background-color:#f8f8ff;size: 500px">
					<input type="button" name="reDispatchInORDashboard" id="reDispatchInORDashboard" value="Redispatch"  style="background-color: #f8f8ff;size: 3;color: red;position: absolute;margin-left: 200px;" onclick="reDispatchFromOR()"></input>
				</td></tr>
</form>
