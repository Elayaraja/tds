<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList,java.util.List,java.util.Map,java.util.HashMap"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
	function callReport(action) {
		
		var fromdate = "";
		var todate = "";
		var driverid = "";
		var url = "";
		var code= "";
		fromdate = document.getElementById("start").value;
		todate = document.getElementById("end").value;
		code = document.getElementById("code").value;
		//fromdate.replace('-','/');
		
		//todate.replace('/','-');
		//driverid = document.getElementById("getDriverId").options[document.getElementById("getDriverId").selectedIndex].value
		if(fromdate != "" && todate != "") {
			window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/DriverSettledDetails_Each.rptdesign&__format=html&stdate='+fromdate+'&enddate='+todate+'&driverid='+code);
		//	url =document.getElementById("context").value+'/frameset?__report=BIRTReport/DriverSetteledSummary.rptdesign&__format=pdf&st_date='+fromdate+'&end_date='+todate;
			//document.driverPaymentForm.action = url;
			//document.driverPaymentForm.target = 'new';
			//driverPaymentForm.submit();
		} else {
			alert("You must give all Values");
		}
	}

</script>
<%
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
	 
%>
</head>
<body>
<form name="driverPaymentForm" action="control" method="post">
	<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
	<table id="pagebox">
		<tr>
			<td>
			<table id="bodypage">
			<tr>
				<td colspan="7" align="center">
					<div id="title" >
						<h2>Get&nbsp;Driver&nbsp;Payment&nbsp;Setteled For Driveronly</h2>
					</div>
				</td>
			</tr>
			 
			<tr>
				<td>Start&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
					<input type="text" name="sdate" id="start" size="10"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.driverPaymentForm.sdate);return false;" HIDEFOCUS>
				<img name="popcal" align="absmiddle" src="images/calendar.gif" width="25" height="16" border="0" alt=""></a>		
				<iframe width="20"  height="178"  name="gToday:normal:agenda.startdate" id="gToday:normal:agenda.startdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
				</iframe>
				</td>
				<td>&nbsp;</td>
				<td>End&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
				<input type="text" name="edate" id="end" size="10"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.driverPaymentForm.edate);return false;" HIDEFOCUS>
				<img name="popcal" align="absmiddle" src="images/calendar.gif" width="25" height="16" border="0" alt=""></a>		
				<iframe width="20"  height="178"  name="gToday:normal:agenda.enddate" id="gToday:normal:agenda.enddate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
				</iframe>
				</td>
			</tr>
			<tr><td></td><td></td><td>Driver&nbsp;ID</td><td>
				 
				 <select name="code"  id="code">
					<% for(int j=0 ;j<al_driver.size();j=j+2){ %>
						<option value="<%=al_driver.get(j).toString() %>" ><%=al_driver.get(j+1) %></option>
					<%} %>
				</select>
					</td>		
				<td colspan="7" align="center"> 
					<input type="button"   value="Search" onclick="callReport(this)"/>
				</td>
			</tr>
			</table>
			</td>
		</tr>
	</table>
</form>
</body>
</html>