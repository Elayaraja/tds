<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>


<jsp:useBean id="openBO" class="com.tds.tdsBO.OpenRequestBO" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<script type="text/javascript" src="js/swfobject.js"></script>
		<script type="text/javascript">
			swfobject.registerObject("voiceMessageID", "9.0.0");
		</script>
<script src="js/CalendarControl.js" language="javascript"></script>
<script language="javascript"> 

 function openPayment()
 {
	
	var ele = document.getElementById("HideText");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "Payment";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "Payment";
	} 
 } 
 
 function openSplReq()
 {
	 var ele = document.getElementById("hideTable");
		var table = document.getElementById("displayTable");
		if(ele.style.display == "block") {
	    		ele.style.display = "none";
			text.innerHTML = "Special Request";
	  	}
		else {
			ele.style.display = "block";
			text.innerHTML = "Special Reques";
		}
	 } 
 function openOperatorComments()
 {
	 var ele = document.getElementById("hideTableComments");
		var table = document.getElementById("displayTableComments");
		if(ele.style.display == "block") {
	    		ele.style.display = "none";
			text.innerHTML = "Operator Comments";
	  	}
		else {
			ele.style.display = "block";
			text.innerHTML = "Operator Comments";
		}
	 } 
	
</script>

<!--  <script src="js/shortcut.js" language="javascript"></script>-->

<%
	ArrayList al_veList = (ArrayList)request.getAttribute("vecFlag");
	ArrayList al_drList = (ArrayList)request.getAttribute("drFlag");
	
	ArrayList al_prevOpenRequests = (ArrayList)request.getAttribute("prevopenrequests");
	ArrayList al_prevAddress = (ArrayList)request.getAttribute("prevaddresses");
%>

<script type="text/javascript">
	document.onkeydown = function(e){
    if (e == null) { // ie
        keycode = event.keyCode;
      } else { 
        keycode = e.which;
      } 
	  if((keycode == 27) && document.getElementById('tableCount').value =="1"){ 
	  		$("#detailsAutoPopulated1").jqmHide();
	  		document.getElementById("sadd1").value='';
		  	document.getElementById("sadd2").value='';
		  	document.getElementById("scity").value='';
		  	document.getElementById("szip").value='';
		  	document.getElementById("sLatitude").value='';
		  	document.getElementById("sLongitude").value='';
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("eadd1").value='';
		  	document.getElementById("eadd2").value='';
		  	document.getElementById("ecity").value='';
		  	document.getElementById('userAddress').value='';
		  	document.getElementById('masterAddress').value='';
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("name").focus();
	  } else if((keycode == 27) && document.getElementById('tableCount').value =="2"){
		  	$("#MapDetails").jqmHide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("sadd1").focus();
	  } else if ((keycode == 27) && document.getElementById('tableCount').value =="3"){
	  		$("#LandDetails").jqmHide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById('landKey').value="";
	  		document.getElementById("slandmark").focus();
	  } else if ((keycode == 27) && document.getElementById('tableCount').value ==""){
	  		document.getElementById("sadd1").value='';
		  	document.getElementById("sadd2").value='';
		  	document.getElementById("scity").value='';
		  	document.getElementById("szip").value='';
		  	document.getElementById("sLatitude").value='';
		  	document.getElementById("sLongitude").value='';
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("eadd1").value='';
		  	document.getElementById("eadd2").value='';
		  	document.getElementById("ecity").value='';
		  	document.getElementById('userAddress').value='';
		  	document.getElementById('masterAddress').value='';
	  	 	document.getElementById('userFrom').value='';
	  } else if((keycode==13) && document.getElementById('tableCount').value =="1") { 
			$("#detailsAutoPopulated1").jqmHide();
	  		if(document.getElementById('nameTemp').value!=""&&document.getElementById('startTimeTemp').value!=""&&document.getElementById('dateTemp').value!=""){
				document.getElementById('name').value=document.getElementById('nameTemp').value;
				document.getElementById('shrs').value=document.getElementById('startTimeTemp').value;
				document.getElementById('sdate').value=document.getElementById('dateTemp').value;
			}
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("name").focus();
	  } else if ((keycode==13) && document.getElementById('tableCount').value =="2"){
	  $("#MapDetails").jqmHide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("sadd1").focus();
	  		selectEnterMapDetails();
	  } else if ((keycode==13) && document.getElementById('tableCount').value =="3"){
	  		$("#LandDetails").jqmHide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("sadd1").focus();
	  		selectEnterLandValues();
	  }	else if ((keycode==13) && document.getElementById('tableCount').value ==""){
		    document.getElementById("saveOpenReq").value = "saveOpenReq";
		  	document.getElementById("Done").click();
	  		document.getElementById("phone").value='';
	  		document.getElementById("name").value='';
	  		document.getElementById("sadd1").value='';
		  	document.getElementById("sadd2").value='';
		  	document.getElementById("scity").value='';
		  	document.getElementById("szip").value='';
		  	document.getElementById("sLatitude").value='';
		  	document.getElementById("sLongitude").value='';
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("eadd1").value='';
		  	document.getElementById("eadd2").value='';
		  	document.getElementById("ecity").value='';

	  }	

	  }
function submitFunctionForForm() {
	if(document.getElementById('tableCount').value ==""){
		return true;
	} else
		return false;
}

function focus(row_id) {
		 var field = row_id; 
		 document.getElementById(field).focus(); 
	 }

function popUp(row_id) { 	  
		 
		 
			var id = document.getElementById(row_id).value; 
		
			if(id.length>0) {  
				
				var url ='/TDS/AjaxClass?event=getlandmardDetails&module=dispatchView&landMark='+id; 
			
				document.getElementById('slandmark').href=url; 
				var a = document.getElementById('slandmark');
				popup_window = window.open (url,"mywindow");
			
				var t = a.title || a.name || null;
				var l = a.href || a.alt;
				var g = a.rel || false;
				
				 
			}  
		}
	 
	
	function reqDetails(id) {
		alert("this"+id+"end");
	}
	function change(){
	  	document.getElementById('masterAddress').value='';
  	 	document.getElementById('userFrom').value='';
	  	document.getElementById("sLatitude").value='';
	  	document.getElementById("sLongitude").value='';
	} 
	function KeyCheck(e,previous,next)  {  
		
	   var KeyID = (window.event) ? event.keyCode : e.keyCode; 
	   if((keyID = 38) || (keyID = 40)||(keyID = 13) || (keyID = 27) || (keyID=37)|| (keyID=39)) { 
	   	switch(KeyID) 
	   	{  
	   	case 37:
	   		document.getElementsByName(previous).focus();
	   		break;
	   		
	   	case 39:
	   		document.getElementsByName(next).focus();
	   		break;
	   		
	   	  case 38: 
	      document.getElementById(previous).focus(); 
	      break; 
	      
	      case 40: 
	      document.getElementById(next).focus(); 
	      break;
	      
	      case 27:
	      document.getElementById("details").innerHTML="";
	      document.getElementById("LandDetails").innerHTML="";
	      document.getElementById("MapDetails").innerHTML="";
	      break;
	   } 
	}
	}
 	function clearLankMarkKey()
	{
		document.getElementById('landmarkKey').value  = "";
	}
	</script>
<% if(request.getAttribute("openrequest") != null) {
		openBO = (OpenRequestBO)request.getAttribute("openrequest");
		
	%>
<script type="text/javascript">
			//document.getElementById("sdate").value = "";
			//document.getElementById("shrs").value = "";
			//document.getElementById("smin").value = "";
			
	</script>

<% } %>

<link href="css/Popup.css" rel="stylesheet" type="text/css" />
<link type="text/css" href="js/jqModal.css" rel="stylesheet"></link>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" /> -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/thickbox.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>

 <link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
 <link type="text/css" rel="stylesheet"
	href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css"
	media="screen"></LINK>
<SCRIPT type="text/javascript"
	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>TDS (Taxi Dispatch System)</title> <script language="javascript">



function loadDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
	var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
	var hrsmin = hors+""+min;  
	
	
	<%
	if(openBO.getSdate().length() <= 0) {
	%>
		document.masterForm.sdate.value = cMonth+"/"+cDate+"/"+cYear;
		document.masterForm.shrs.value  = hrsmin; 
		
	<% 
	}
	%>
 }
function openDrop() { 
	if(document.getElementById("smsStatus").checked) {  
		document.getElementById("smsList").style.display='block'; 
	}else {
		document.getElementById("smsList").style.display='none';
		document.getElementById("toSms").value="0";
	}
}
function closePopup() { 
	
	window.close();
	
}	

function clearLatNLong(){
	document.getElementById('sLongitude').value  = "";
	document.getElementById('sLatitude').value  = "";
	document.getElementById('addressverify').style.background="yellow";
	
}
 function previousAddress(){
	 $('#detailsAutoPopulated1').jqm(); 
 }
 function MapDetails(){
	 $('#MapDetails').jqm();
 }
 
 function landDetails(){
	 $('#LandDetails').jqm();
 }
</script>
</head>

<%
String status[] = {"Request","In Process","Completed"};
%>
<body onload="loadDate()">
	<form name="masterForm" id="masterForm" action="control" method="post">
		<div>
			<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="700" height="300" id="voiceMessageID">
				
				<object type="application/x-shockwave-flash" data="js/f211.swf" width="700" height="300">
				</object>
				
			</object>
		</div>
		

   </form>
</body>
</html>


