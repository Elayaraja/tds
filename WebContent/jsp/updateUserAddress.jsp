<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.Address" %>
<%@page import="com.tds.cmp.bean.CustomerProfile" %>

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>

<% Address  UserAddress = (Address)request.getAttribute("UserAddress"); 
String masterKey=(String)request.getAttribute("masterKey");
ArrayList addressHistoryDemo=(ArrayList<Address>)request.getAttribute("addressHistoryDemo");
%>

<head>
<script type="text/javascript">
document.onkeydown = function(e){
    if (e == null) { // ie
        keycode = event.keyCode;
      } else { 
        keycode = e.which;
      } 
    if(keycode==13 && document.getElementById("tableLandmark").value==1){
	   	selectEnterLandValues();
	   	document.getElementById("tableLandmark").value=="";
	   	document.getElementById('AddDetails').style.display="none";
    }else if(keycode==13 && document.getElementById("tableLandmark").value=="") {
		document.getElementById('UpdateUserAddress').value="Update";
    	document.masterForm.submit();
        document.getElementById("tableLandmark").value=="";
        if(document.getElementById("addverified").checked==true){
			document.getElementById("addverified").value="1";
		} else {
			document.getElementById("addverified").value="0";
		}
	    document.getElementById('AddDetails').style.display="none";
    }
    }

	function openAdddetails(AddDetails) {
		var add1 = document.getElementById('sadd1').value;
		var city = document.getElementById('scity').value;
		var zip = document.getElementById('szip').value;
		// var mydivmap = document.getElementById('AddDetails').value;
		//mydivmap.style.display = 'none';
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = "RegistrationAjax?event=getmapdetails&address='" + add1
				+ "'&latitude=''&longitude=''";
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById('AddDetails').innerHTML = text;
		document.getElementById('tableLandmark').value=1;
		return true;
	}

	function submitAddressFN() {
		document.getElementById("UpdateUserAddress").value = "Update";
		if(document.getElementById("addverified").checked==true){
			document.getElementById("addverified").value="1";
		} else {
			document.getElementById("addverified").value="0";
		}
		document.masterForm.submit();
	}
</script>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
   



<title>Update User  Address</title>
</head>
<body>
<form  name="masterForm"  action="control" method="post">

   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %>
   <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
   <input type="hidden" name="action" value="registration"/>
   <input type="hidden" name="event" value="custInfo"/>
  <input type="hidden" name="UpdateUserAddress"  id="UpdateUserAddress" value=""/>
  <input type="hidden" 	name="tableLandmark" id="tableLandmark" value="" /> 
  <input type="hidden" name="fadd1temp" id="fadd1temp" value="" /> 
  <input type="hidden" name="fadd2temp" id="fadd2temp" value="" /> 
  <input type="hidden" name="fcitytemp" id="fcitytemp" value="" /> 
  <input type="hidden" name="fstatetemp" id="fstatetemp" value="" /> 
  <input type="hidden" name="fziptemp" id="fziptemp" value="" /> 
  <input type="hidden" name="latitemp" id="latitemp" value="" /> 
  <input type="hidden" name="longitemp" id="longitemp" value="" />
   <input type="hidden" name="sLatitude" id="sLatitude" value="" /> 
  <input type="hidden" name="sLongitude" id="sLongitude" value="" />
  <input type="hidden" name="landMarkSelect" id="landMarkSelect" value="1" />
  <input type="hidden" name="landmarktemp" id="landmarkTemp" value="" />
  <input type="hidden" name="slandmark" id="slandmark" value="" />
   <input type="hidden" name="addverified" id="addverified" value="" />
  
   <c class="nav-header"><center>Update User Address</center></c>
                 <table width="50%" border="0" cellspacing="0" cellpadding="0" align="center">
                         <tr>
                    	  <td width="20%">Phone No:</td>
                    	  <td>  
                    	    <input type="text"  name="Number" readonly="readonly"   id="Number" value="<%=UserAddress.getNumber() %>" />
                    	  </td>
                    	  </tr><tr>
                    	  <td>Name</td>
                    	  <td>  
                    	    <input type="text"  name='Name'  id="Name" value="<%=UserAddress.getName() %>"/>
                    	 </td>
                       </tr>
                     <tr>
                      <td>Address1:</td>
                    	  <td>  
                    	    <input type="text"  name="sadd1"  id="sadd1" value="<%=UserAddress.getAdd1() %>" />
                    	  </td>
                    	  </tr><tr>
                    	  <td>Address2:</td>
                    	  <td>  
                    	    <input type="text"  name='sadd2'  id="sadd2" value="<%=UserAddress.getAdd2() %>" />
                    	 </td> 
                     </tr>       
                    <tr>
                    <tr>
                      <td>City</td>
                    	  <td>  
                    	    <input type="text"  name="scity"  id="scity" value="<%=UserAddress.getCity() %>" />
                    	  </td></tr><tr>
                    	  <td>State</td>
                    	  <td>  
                    	    <input type="text"  name='sstate'  id="sstate" value="<%=UserAddress.getState() %>" />
                    	 </td> 
                     </tr>       
                    <tr>                    
                    <tr>
                      <td>Zip</td>
                    	  <td>  
                    	    <input type="text"  name="szip"  id="szip" value="<%=UserAddress.getZip() %>" />
                    	    <input type="hidden" name="masterKey" id="masterKey" value="<%=masterKey %>"/>
                    	    <input type="hidden" name="addressKey" id="addressKey" value="<%=UserAddress.getAddressKey() %>"/>
                    	  </td></tr><tr> 
                    	   <td>AddressVerified</td>                     	   
                           <td> <input type="checkbox" name="addverified"  id="addverified" value=""/>                    	 
                     		</tr>                       
                    <tr>
                    <td colspan="2" align="center">
                         	  <input type="button"  name="button4verify" id="button4verify"  value="Verify Address" onclick="openAdddetails('AddDetails')" />
                         	  <input type="button"  name="UpdateUserAddress" id="UpdateUserAddress"  value="Update" onclick="submitAddressFN()"/>
					</td>
				    </tr> 
                </table>
        <div id="AddDetails2" style="height:90%; width:100%; margin-top: 4px; margin-left:-6px;border: 1px solid green; ">  
                <table width='92%' id="AddDetails1" >
		 		<tr>
 			         <td valign="top" >
 				     <div   id="AddDetails" overflow:auto'  style="border: 1px solid red;width:108%;"> </div>
 				   	 </td>
 				     </tr>
                 </table>
         </div>  
  
   

</form>
</body>
</html>