<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,com.tds.tdsBO.DriverRegistrationBO;"%>
<%! int startValue =1 ;double limit=0;int pstart=1;int pend=0;int pageNo=1;int plength=0;%>
<%if(request.getAttribute("startValue") != null){
	startValue = Integer.parseInt((String)request.getAttribute("startValue"));
	startValue=startValue+1;
}	
if(request.getAttribute("plength")!=null){
	plength=Integer.parseInt((String)request.getAttribute("plength"));
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS (Taxi Dispatch System)</title>
</head>
<body>
<table id="pagebox">
	<tr>
		<td>
<table id="bodypage" border="1" cellpadding="0" cellspacing="0">	
	<tr>
		<td colspan="12" align="center">
			<div id="title">
				<h2>Driver&nbsp;Registration&nbsp;Summary</h2>
			</div>
		</td>
	</tr>
	<tr>
	<%
		String error ="";
		if(request.getAttribute("errors") != null) {
			error = (String) request.getAttribute("errors");
		}
		
	%>
	<tr>
		<td colspan="8">
			<div id="errorpage">
				<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %>
			</div>
		</td>
	</tr>
	<tr align="center" bgcolor="#d3d3d3">
			<td>&nbsp;S.No&nbsp;</td>
		<!--  	<td>&nbsp;PhoneNo&nbsp;</td> -->
			<td>&nbsp;First&nbsp;Name&nbsp;</td>
			<td>&nbsp;Last&nbsp;Name&nbsp;</td>
		<!--	<td>&nbsp;Address&nbsp;</td>
			<td>&nbsp;Car&nbsp;Make&nbsp;</td>
			<td>&nbsp;Car&nbsp;Year&nbsp;</td>
			<td>&nbsp;Car&nbsp;Model&nbsp;</td> -->
			<td>&nbsp;User&nbsp;No&nbsp;</td> 
 			<td>&nbsp;Cab&nbsp;No&nbsp;</td>
	<!--		<td>&nbsp;Pass&nbsp;No&nbsp;</td> -->
			<td>&nbsp;Details&nbsp;</td>					
	</tr>
		<%
		DriverRegistrationBO driverBO = new DriverRegistrationBO();
		ArrayList al_driverData=new ArrayList();
		if(request.getAttribute("driversumdata")!=null){
		al_driverData=(ArrayList)request.getAttribute("driversumdata");
		for(int r_count=0;r_count<al_driverData.size();r_count++){
		%>
		<tr align="center" valign="top">
			<td><%=r_count+startValue %></td>
	<%-- 		<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getPhone()%>&nbsp;</td>
	--%>		<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getFname()%>&nbsp;</td>
			<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getLname()%>&nbsp;</td>
	<%--		
			<td align="left">&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getAdd1()%><%if((((DriverRegistrationBO)al_driverData.get(r_count)).getAdd1())!=null){%>,<%}%><br>
				&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getAdd2()%><%if((((DriverRegistrationBO)al_driverData.get(r_count)).getAdd2())!=null){%>,<%}%><br>
				&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getCity()%><%if((((DriverRegistrationBO)al_driverData.get(r_count)).getCity())!=null){%>,<%}%><br>
				&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getState()%><%if((((DriverRegistrationBO)al_driverData.get(r_count)).getState())!=null){%>,<%}%><br>
				&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getZip()%><%if((((DriverRegistrationBO)al_driverData.get(r_count)).getZip())!=null){%>.<%}%><br>
			</td>
			<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getCmake()%>&nbsp;</td>
			<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getCyear()%>&nbsp;</td>
			<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getCmodel()%>&nbsp;</td> --%>
			<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getUid()%>&nbsp;</td>
			<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getCabno()%>&nbsp;</td>
	<%--		<td>&nbsp;<%=((DriverRegistrationBO)al_driverData.get(r_count)).getPassno()%>&nbsp;</td>  --%>
			<td>&nbsp;<a href="/TDS/control?action=registration&event=editDR&seq=<%=((DriverRegistrationBO)al_driverData.get(r_count)).getSno()%>">Edit</a>&nbsp;</td>
		</tr>		
		<%}%>
		</table>
		<table align="center">
					<tr align="center">
						<%  
							if(request.getAttribute("pstart")!=null){
								pstart= Integer.parseInt((String)request.getAttribute("pstart"));
								}
							if(request.getAttribute("pend")!=null){
								pend=Integer.parseInt((String)request.getAttribute("pend"));
								}
							if(request.getAttribute("limit")!=null){
								limit = Double.parseDouble((String)request.getAttribute("limit"));
								}
								int start=0;
								int end =0;
							if(request.getAttribute("DRBO")!=null){
								driverBO = (DriverRegistrationBO)request.getAttribute("DRBO");
								}
				 				start=Integer.parseInt(driverBO.getStart())+1;
								end =Integer.parseInt(driverBO.getEnd());
				 
							 if(limit<=plength)
				 				{ 	 		
			 	 					for(int i=1;i<=limit;i++){ %>
			 	 					<td><a href="/TDS/control?action=registration&event=SummaryDR&page<%=i %>=<%=start%>"><%=i %></a></td>
			 	 					<%} 
			 	 				}				 
							if(limit>=plength)
								{ 
									if(pstart!=1){
									%>
									<td><a href="/TDS/control?action=registration&event=SummaryDR&previous=<%=""+pstart%>&first=no">&lt;</a></td>
									<%}
									for(int i=pstart;i<pend;i++){%>				
									<td><a href="/TDS/control?action=registration&event=SummaryDR&pageNo=<%=i%>&pstart=<%=""+pstart%>&pend=<%=""+pend %>&first=no"><%=i %></a></td>
									<%}
									if(pend<=limit){	%>
									<td><a href="/TDS/control?action=registration&event=SummaryDR&next=<%=pend+""%>&first=no">&gt;</a></td>
									<%} 
								}%>
					</tr>
				</table>
			<%}
		else{%>
		<table>
		<tr>
			<td>No&nbsp;Record&nbsp;Found&nbsp;</td>
		</tr>
		</table>
		<%} %>
	
	</td>
	</tr>
	</table>	
</body>
</html>