<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="js/jquery.min.js"></script> 
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/Report.js"></script>
 <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Simonetta">
<link href='http://fonts.googleapis.com/css?family=Emilys+Candy' rel='stylesheet' type='text/css'>
<style type="text/css">
    .total {
      font-family: 'Simonetta', serif;
      font-size: 18px;
    }
.niceButton {
    -webkit-box-shadow: rgba(0, 0, 0, 0.0976562) 0px 1px 0px 0px;
    background-color: #EEE;
    border: 1px solid #999;
    color: #666;
    font-family: 'Lucida Grande', Tahoma, Verdana, Arial, Sans-serif;
    font-weight: bold;
    padding: 2px 6px;
    height: 28px;
}
</style>
</head>
<body>
 <table class="total"bgcolor="#EFEFFB" width="1000px" height="280px" style="margin-right:180px;border-style:solid;overflow:scroll;border:15px solid #d4dae8;-moz-border-radius: 8px 8px 8px 8px;opacity: 0.9;padding: 3px">
 	<tr align="center" bgcolor="#d4dae8" style="text-shadow: 4px 4px 4px #aaa;" ><th style="-moz-border-radius: 8px 8px 8px 8px; opacity: 0.9; padding: 3px;"> Category </th><th>Reports </th><th>Input Parameters</th></tr>
 	<tr>
 	<td bgcolor="#f7f7f7" width="280px" style="overflow: auto">
 	<a onclick="category('all')"style="cursor: pointer;">All Reports</a><br/>
 	<a onclick="category('1')"style="cursor: pointer;">1.Dispatch</a><br/>
 	<a onclick="category('2')"style="cursor: pointer;">2.Operations</a><br/>
 	<a onclick="category('3')"style="cursor: pointer;">3.Finance</a><br/>
 	<a onclick="category('4')"style="cursor: pointer;">4.System Setup</a></td>
 	<td align="left" valign="top" height="260px" width="380px"><div id="ReportTypes" style=" height: 260px;overflow: auto; width: 380px;">
    <a onclick="showReports();reportNo('1')" style="cursor: pointer;">1.Job Statistics</a><br>
<!--<a onclick="showReports();reportNo('2')" style="cursor: pointer;">2.Average Time For dispatch</a><br>
<!--<a onclick="showOneDate();reportNo('3')" style="cursor: pointer;">2.Operator Performance per Day</a><br>
	<a onclick="showOneDate();reportNo('4')" style="cursor: pointer;">3.Job Statistics Details</a><br>
 --><a onclick="showOneDate();reportNo('5')" style="cursor: pointer;">2.Job Summary</a><br>
 	<a onclick="showReports();reportNo('6')" style="cursor: pointer;">3.Time Statistics</a><br>
 	<a onclick="showTrip();reportNo('7')" style="cursor: pointer;">4.Shared Ride Summary</a><br>
 	<a onclick="showOneDate();reportNo('8')" style="cursor: pointer;">5.Operator Daily Summaries</a><br>
 </div>
 <div id="reportOne" style="display:none">
 	<a onclick="showTrip();reportNo('6')" style="cursor: pointer;">1.Job Summary</a><br>
	<a onclick="showReports();reportNo('1')" style="cursor: pointer;">2.Job Statistics</a><br>
<!--<a onclick="showReports();reportNo('2')" style="cursor: pointer;">3.Average Time For dispatch</a><br>
 --><a onclick="showTrip();reportNo('7')" style="cursor: pointer;">3.Shared Ride Summary</a><br>
  </div>
 <div id="reportTwo" style="display:none">
 	<a onclick="showOneDate();reportNo('8')" style="cursor: pointer;">1.Operator Daily Summaries</a><br>
<!--<a onclick="showOneDate();reportNo('3')" style="cursor: pointer;">2.Operator Performance per Day</a><br>
 --><a onclick="showTrip();reportNo('6')" style="cursor: pointer;">2.Time Statistics</a><br>
 </div>
 <div id="reportThree" style="display:none"> 
 </div>	
 <div id="reportFour" style="display:none"> 
 </div>
 </td>
 <td bgcolor="#f7f7f7" valign="middle" >
<!--  <img src="images/Dashboard/GetAcab2.png" width="230px" height="60px" >
 -->
 		<div id="fromAndToDate"  style="display:none;">
 			<table bordercolor="black"border="0"bgcolor="#E0E0E0" style="-moz-border-radius: 8px 8px 8px 8px;opacity: 0.9;padding: 3px">
 				<tr><td>From Date:<input type="text" id="fromDate"name="fromDate"size="8" value="" onfocus="showCalendarControl(this)"><br/></td>
 					<td>To Date:<input type="text" id="toDate"name="toDate" value=""size="8" onfocus="showCalendarControl(this)"><br/></td></tr>
 				<tr><td align="center" colspan="2"><input type="submit" class="niceButton" name="Submit" id="Submit" value="Submit"onclick="goToController()"/></td></tr></table>
 		</div>
 		<div id="oneDate"  style="display:none;">
 			 <table bordercolor="black"border="0"bgcolor="#E0E0E0" style="-moz-border-radius: 8px 8px 8px 8px;opacity: 0.9;padding: 3px">
 			<tr><td> Date: <input type="text" name="Date"id="Date"size="8" onfocus="showCalendarControl(this)"/></td>
 			<td> <input type="submit" class="niceButton" name="submit"id="submit" value="submit" onclick="goToController()"></tr>
 			</table>
 		</div>
 		<div id="basedTrip"  style="display:none;">
 			 <table bordercolor="black"border="0"bgcolor="#E0E0E0" style="-moz-border-radius: 8px 8px 8px 8px;opacity: 0.9;padding: 3px">
 			<tr><td> Trip ID: <input type="text" name="tripId"id="tripId"size="8" /></td>
 			<td> <input type="submit" class="niceButton" name="submit"id="submit" value="submit" onclick="goToController()"></tr>
 			</table>
 		</div>
 		</td>
 	</tr>
 </table>
 <input type="hidden" name="reportNo" id="reportNo" value="">
<!--  <img src="images/Dashboard/Getacab2.png"> -->
</body>
</html>