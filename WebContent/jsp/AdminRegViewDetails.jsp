<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css" title="currentStyle">
.txtarea{
    max-width:100%;
    min-height:200px;    
    display:block;
    width:100%;
}

.mydiv{
    padding:10px;
}

.gen_btn{
    padding:5px;
    background-color:#743ED9;
    color:white;
    font-family:arial;
    font-size:13px;
    border:2px solid black;
}

.gen_btn:hover{
    background-color:#9a64ff;
}
	@import "dataTable/css/demo_page.css";
	@import "dataTable/css/demo_table_jui.css";
	@import "dataTable/css/jquery-ui-1.8.4.custom.css";
	@import "dataTable/css/TableTools_JUI.css";
</style>


<script type="text/javascript" charset="utf-8"src="dataTable/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf-8"src="dataTable/js/ZeroClipboard.js"></script>
	<script type="text/javascript" charset="utf-8"src="dataTable/js/TableTools.js"></script>
	<script type="text/javascript" src="js/markerwithlabel.js"></script>
	<script type="text/javascript" src="js/Util.js"></script>
 <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="https://github.com/douglascrockford/JSON-js/raw/master/json2.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<title>TDS(Taxi Dispatching System)</title>
<script type="text/javascript">
	function cal(ivalue) {
	document.getElementById("ivalue").value=document.getElementById(ivalue).value;
	}
	function submitDetailsforEdit(){
		var ivalue=document.getElementById("ivalue").value;
		window.open('control?action=registration&event=saveadminRegistration&module=systemsetupView&ivalue='+ivalue+'&submit=Edit',"_self");
	}
	function deleteOperator(driverNumber,x){
		var driver=driverNumber;
		var confirmation=confirm("Records on  "+driver+" going to delete");
		if (confirmation==true){
		  var xmlhttp=null;
			if (window.XMLHttpRequest){
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		url='RegistrationAjax?event=deleteDriver&module=operationView&driverNum='+driver+'&driverOrOperator='+1;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text=="1"){
			var table=document.getElementById("operatorSummary");
			table.deleteRow(x.parentNode.parentNode.rowIndex);
		}
	 }
	}
	/* function show(){
		alert("hello");
	} */
	function showFleetTable(i){
		$("#fleetAccess_"+i).show("slow");
		$("#fleetAccessButton_"+i).hide("slow");
	}
	 function provideAccess(driverId,size,row){
		var fleet="";
		var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		for(var i=0;i<size;i++){
			if(document.getElementById("fleet_"+row+"_"+i).checked==true){
				fleet=fleet+"&fleetNumber"+i+"="+document.getElementById("fleet_"+row+"_"+i).value+"&fleetName"+i+"="+document.getElementById("fleetName_"+row+"_"+i).value;
			}
		}
		url='SystemSetupAjax?event=giveFleetAccess&driverId='+driverId+'&'+fleet;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text==1){
			$("#fleetAccess_"+row).hide("slow");
			$("#fleetAccessButton_"+row).show("slow");
		}
	} 
 function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
 
	function getoperaDetails(){
		  $("#download").live("click", function () {
		        var operatorSummary = document.getElementById("operatorSummary");
		        var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
		        printWindow.document.write('<html><head><title>operatorSummary Details</title>');
		        printWindow.document.write('</head><body >');
		        printWindow.document.write(operatorSummary.outerHTML);
		        printWindow.document.write('</body></html>');
		        printWindow.document.close();
		        printWindow.print();
		    });
		
			}
 
</script>
</head>
<body>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); 
	ArrayList<FleetBO> fleetForOperators=new ArrayList<FleetBO>();
	 
	if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	} %>
<form method="post" action="control" name="admin" onsubmit="return showProcess();">
<input type="hidden" name="<%=TDSConstants.actionParam%>" value="<%=TDSConstants.registrationAction%>"/> 
<input	type="hidden" name="event" value = "Adminview"></input>
<input type="hidden" name="systemsetup" value="6"></input>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<c class="nav-header"><center>Operators Summary</center></c>
                     <table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
                    <tr>
                    	<td >First&nbsp;Name</td>
                    	<td> <input type="text"  name="firstName"  id="firstName"  value=""/> </td>
                    	<td >Last&nbsp;Name</td>
                    	<td> <input type="text"  name="lastName"  id="lastName" value=""/> </td>                    	
                    </tr>   	
                    <tr>
		                <td >User&nbsp;Name</td>
		                <td> <input type="text" name="userName" id="userName" value=""></input></td>
		            </tr>
                <tr>
				<td colspan="7" align="center">
                     <input type="submit" name="GetDetails" value="Get Details" id="GetDetails"  class="lft"></input>
                     <input type="button" name="download" id="download" value="Save/Print"	onclick="getoperaDetails()">                             
			
				</td>
				</tr>
				</table>
              <%ArrayList a_list=(ArrayList)request.getAttribute("adminBO");
					if(a_list != null){
					boolean colorLightGreen = true;
					String colorPattern;
					%>
                    <table width="100%" align="center" id="operatorSummary" border="0" cellspacing="0" cellpadding="0" class="thumbnail">
                      <tr>
                        <th width="15%">User Name</th>
                        <th width="15%">UserType</th>
                        <th width="15%">Name</th>
                        <th width="5%">Edit</th>
                        <%if(fleetList!=null && fleetList.size()>0) {%>
                        <th width="10%">Fleet</th>
                        <%} %>
                        <th width="5%">Delete</th>
                      </tr>
                      	<%for(int i=0;i<a_list.size();i++) {
   						colorLightGreen = !colorLightGreen;
   						if(colorLightGreen){
   							colorPattern="style=\"background-color:lightgreen\""; 
   							}
   						else{
   							colorPattern="";
   						}
						AdminRegistrationBO adminBO=(AdminRegistrationBO) a_list.get(i);%>
						<tr>
							<td <%=colorPattern %> align="center"><%=adminBO.getUname() %></td>
							<td <%=colorPattern %> align="center"><%=adminBO.getUsertype() %></td>
							<td <%=colorPattern %> align="center"><%=adminBO.getFname()%>&nbsp;<%= adminBO.getLname()%></td>
						    <td <%=colorPattern %> align="center">
                       	 		<input type="hidden" name="uname<%=i %>" id="uname<%=i %>" value="<%=adminBO.getUid() %>">
								<input type="hidden" name="ivalue" id="ivalue" value="">
								<input type="button" name="submit" value="Edit" class="lft" onclick="showProcess();cal('uname<%=i %>');submitDetailsforEdit();"></input>
                    		</td>
                        <%if(fleetList!=null && fleetList.size()>0) {%>
                    		<td <%=colorPattern %> align="center">
                       			<input type="button" name="fleetAccess" id="fleetAccessButton_<%=i %>" value="Fleet Access" class="lft" onclick="showFleetTable(<%=i%>)"></input>
                    		</td>	
                        <%} %>
                    		<td <%=colorPattern %> align="center">&nbsp;
                    			<input type="button" name="imgDeleteOperafleet_tor" id="imgDeleteOperator" value="Delete" onclick="showProcess();deleteOperator('<%=adminBO.getUid() %>',this)"></input>
                    		</td>
						</tr>
						<tr id="fleetAccess_<%=i %>" style="display: none;">
						<%if(fleetList!=null && fleetList.size()>0) {
						fleetForOperators=(ArrayList)request.getAttribute("fleetListForOperators");
						} %>
						<%for(int fleet=0;fleet<fleetList.size();fleet++){ %>
						<td <%=colorPattern %> align="center">
						<%String checkedOrNot=""; %>
						<%=fleetList.get(fleet).getFleetName() %>:
						<%for (int fleetAccess=0;fleetAccess<fleetForOperators.size();fleetAccess++){ %>
						<%FleetBO fleetBO=fleetForOperators.get(fleetAccess); %> 
						<%if(fleetBO.getUserId().equals(adminBO.getUid())){%>
 						
							<%if(fleetBO.getFleetNumber().equals(fleetList.get(fleet).getFleetNumber())){
								checkedOrNot="checked=checked";
								fleetAccess=fleetForOperators.size();
							}else{
								checkedOrNot="";
							}
						}else{ 
							checkedOrNot="";
						}%>
						<%} %>
						<input type="checkbox" name="fleetChk_<%=i %>" id="fleet_<%=i %>_<%=fleet%>" <%=checkedOrNot %>  value="<%=fleetList.get(fleet).getFleetNumber()%>"/>
						<input type="hidden" name="fleetName_<%=fleet %>" id="fleetName_<%=i %>_<%=fleet%>" value="<%=fleetList.get(fleet).getFleetName()%>"/>
						</td>
						<%} %>
						<td <%=colorPattern %> align="center">
						<input type="button" name="giveAccess" value="ProvideAccess" onclick="showProcess();provideAccess('<%=adminBO.getUid() %>',<%=fleetList.size()%>,<%=i%>)"/>
						</tr>
						<%}%>
						</table>
					<%} %>
		
		            <footer>Copyright &copy; 2010 Get A Cab</footer>

     	
    </form>		
</body>
</html>
