<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.tdsBO.CabMaintenanceBO,com.common.util.TDSConstants"%>

<jsp:useBean id="cabMaintenanceBo" class="com.tds.tdsBO.CabMaintenanceBO" scope="request"/>
<% if(request.getAttribute("cabMaintenance") != null)
	cabMaintenanceBo = (CabMaintenanceBO)request.getAttribute("cabMaintenance");

%>
<html>
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
	<form method="post" action="control" name="maintenance">
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.createMaintenanceDetail %>">
<input type="hidden" name="maintenceNo" value="<%= cabMaintenanceBo.getMaintenanceKey() %>">
<table id="pagebox">
	<tr>
		<td>
			<table id="bodypage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Cab&nbsp;Maintenance</b></td>
			</tr>
			<tr>
			<%
			String error ="";
			if(request.getAttribute("errors") != null) {
				error = (String) request.getAttribute("errors");
			}		
			%>
			<td colspan="7">
				<div id="errorpage">
					<%= error.length()>0?"You must fulfilled the following error<br>"+error :"" %>
				</div>
			</td>
			</tr>
			<tr>
				<td>Service&nbsp;Code</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  size="8" name="mcode" value="<%= cabMaintenanceBo.getServiceCode() == null ? "" : cabMaintenanceBo.getServiceCode() %>">
				</td>
				<td>&nbsp;</td>
				<td>Service&nbsp;Date</td>
				<td>&nbsp;</td>
				
				<td>
					<input type="text"  size="8" name="sdate" value="<%= cabMaintenanceBo.getServiceDate() == null ? "" : cabMaintenanceBo.getServiceDate()  %>" onfocus="showCalendarControl(this);"  readonly="readonly" >
				</td>
			</tr>
			<tr>
				<td>Amount</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  name="samount" value="<%= cabMaintenanceBo.getAmount() == null ? "" : cabMaintenanceBo.getAmount()  %>">
				</td>
				<td>&nbsp;</td>
				<td>Description</td>
				<td>&nbsp;</td>
				<td>
					<textarea name="scomment" rows="5" cols="25"><%=  cabMaintenanceBo.getServiceDesc() == null ? "" : cabMaintenanceBo.getServiceDesc()   %></textarea>
				</td>
			</tr>
			<tr>
				<td colspan="7" align="center">
					
					<% if(cabMaintenanceBo.getMaintenanceKey() != null && !cabMaintenanceBo.getMaintenanceKey().equalsIgnoreCase("null") ) { %>
						<input type="submit" name="update" value="Update">
					<% } else { %>
						<input type="submit" name="save" value="Save">						
					<% } %>
				</td>
			</tr>
			</table>
		</td>
	</tr>

</table>
</form>
</body>
</html>