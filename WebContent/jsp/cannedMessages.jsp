<%@page import="com.tds.tdsBO.MessageBO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html >
<head>
<%ArrayList<MessageBO> messageslist = (ArrayList<MessageBO>)request.getAttribute("messages"); %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/cannedCss.css" type="text/css" media="all" />
<link href='http://fonts.googleapis.com/css?family=Questrial|Droid+Sans|Alice' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="js/jquery.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<script type="text/javascript" src="js/jqModal.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<title>TDS Canned Messages</title>
<style type="text/css">
 #dvTemplate td:hover{
	opacity: 1.2;
	filter: Alpha(opacity =   130); /* IE8 and earlier */
	} 
 #dvTemplate td{
	opacity: 0.7;
	filter: Alpha(opacity =   70); /* IE8 and earlier */
	} 	
#dvTemplate img{
	opacity: 0.9;
	filter: Alpha(opacity =   90); /* IE8 and earlier */
	cursor: pointer;
}
table-layout: fixed;	
</style>
<script type="text/javascript">
function createMsg(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'DashBoard?event=createMsg&msg='+document.getElementById("msgTemp").value+'&category='+document.getElementById("category").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!="0"){
		document.getElementById("msgTemp").value ="";
		document.getElementById("error").innerHTML ="Message has been saved";
	}
	location.reload();
}
 function removeError(){
	document.getElementById("error").innerHTML ="";
}
 $(document).ready(function() {
	 $("#dvTemplate td").mouseover(function () {
		 $(this).find("img").show();
		 });
	 $("#dvTemplate td").mouseleave(function () {
		 $(this).find("img").hide();
		 });
	 
	var  obj = document.getElementById("dvTemplate");
	 obj.scrollTop = obj.scrollHeight;
	});
 function showFullDetails(msg,category){
	 document.getElementById("msgToSend").value=msg;
	 document.getElementById("categorytoSend").value = category;
	 if(category=="0"){
		 $("#driverID").show();
		 $("#operatorID").hide();
		 $("#driverOrOperator").hide();
		 
	 }else if(category=="1"){
		 $("#driverID").hide();
		 $("#operatorID").show();
		 $("#driverOrOperator").hide();
	 }else{
		 $("#driverID").hide();
		 $("#operatorID").hide();
		 $("#driverOrOperator").show();
	 }
 }
 function sendMessage(){
	 if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	 var dId=document.getElementById("driverOrOperator").value;
	 if(dId==""){
		 dId=document.getElementById("driverID").value;
	 }
	 if(dId==""){
		 dId=document.getElementById("operatorID").value;
	 }
	 var url = 'control?action=openrequest&event=sendSMS&driver_id='+dId+'&message='+document.getElementById("msgToSend").value;
	 xmlhttp.open("GET", url, false);
	 xmlhttp.send(null);
	 //var text = xmlhttp.responseText; 
 }
 </script>
 <script language="javascript" type="text/javascript">
function limitText() {
	var limitField = document.getElementById("msgTemp").value;
	if (limitField.length > 100) {
		document.getElementById("msgTemp").value= limitField.substring(0, 100);
	} else {
		document.getElementById("countdown").innerHTML = 100 - limitField.length;
	}
}
function removeMsg(msgkey){
	var makeSure = confirm("Do you want to delete it?");
	if (makeSure == true) {
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'DashBoard?event=removeMsg&msgKey='+msgkey;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text!=0){
			location.reload();
		}
	} 
}
function closePage(){
	window.location.href="/TDS/control";
}

</script>
</head>
<body style="height: 100%; width: 100%; margin: 0; padding: 0; background-color: lightgrey">
	<table style="height: 100%; width: 100%;" border="1">
		<tr style="height: 5%" align="center">
			<td colspan="2"><h1>Canned Messages</h1><img alt='' src='images/home.png' style="margin-left: 80%;" onclick="closePage()"/></td>
		</tr>
		<tr>
			<td align="left" style="widTh: 35%" valign="top">
				<div id="info" style="color: green">
					<h6></h6>
				</div> <textArea placeholder="Click the template!" id="msgToSend" name="msgToSend" onfocus="removeError()" readonly="readonly" rows="2"></textArea> 
				<select id="categorytoSend" disabled="disabled"><option value="0">Driver</option>
					<option value="1">Operator</option>
					<option value="2" selected="selected">All</option>
			</select>
			 <br /> <input type="text" name="driverID" id="driverID" value="" placeholder="Driver ID" style="display: none; height: 30px; width: 190px;"> 
			 <ajax:autocomplete fieldId="driverID" popupId="model-popup1" targetId="driverID" baseUrl="autocomplete.view" paramName="DRIVERID" className="autocomplete" progressStyle="throbbing" /> <br /> 
			 <input type="text" name="operatorID" id="operatorID" value="" placeholder="Operator ID" style="display: none; height: 30px; width: 190px;"> 
			 <ajax:autocomplete fieldId="operatorID" popupId="model-popup1" targetId="operatorID" baseUrl="autocomplete.view" paramName="operatorId" className="autocomplete" progressStyle="throbbing" />
			 <input type="text" name="driverOrOperator" id="driverOrOperator" value="" placeholder="Driver or Operator" style="display: none; height: 30px; width: 190px;"> 
			 <ajax:autocomplete fieldId="driverOrOperator" popupId="model-popup1" targetId="driverOrOperator" baseUrl="autocomplete.view" paramName="employeeNames" className="autocomplete" progressStyle="throbbing" /> 
			 <input type="button" name="sendMsg" id="sendMsg" value="★ Send Message" onclick="sendMessage()">
			<br />
			<br />
			<br />
			<div id="error" style="color: green">
			<h6></h6>
				</div> <font size="1">(Maximum characters: 100)<br>You have <b id="countdown">100</b> characters left.</font><br />
				 <textArea placeholder="Type your message here!" id="msgTemp" name="msgTemp" onfocus="removeError()" rows="2" onKeyDown="limitText();" onKeyUp="limitText()"></textArea> 
				 <select id="category"><option value="0">Driver</option>
									    <option value="1">Operator</option>
										<option value="2" selected="selected">All</option>
			</select><br /> <input type="button" name="submitMsg" id="submitMsg" value="★ Save Message" onclick="createMsg()"> <br /></td>
			<td valign="top">
				<div id="dvTemplate" style="max-height: 450px; overflow-y: scroll; overflow-x: hidden; width: 800px; height: 450px">
					<table style="width: 100%; height: 100%">
						<tr>
							<%for (int i = 0; i < messageslist.size(); i++) { %>
							<%if (i % 4 == 0) {	%>
						</tr><tr>
							<%}	%>
							<td id="tdMsg" style="background: rgba(255, 255, 255, 0.5); width: 100px; height: 100px" valign="top" title="MSG:<%=messageslist.get(i).getMessage() %> :Category:<%=messageslist.get(i).getCategory()%>"><%=messageslist.get(i).getMessage() %>
							<img src="images/Dashboard/trash.png" width="18px" onclick="removeMsg(<%=messageslist.get(i).getMsgKey() %>)" height="18px" align="left" style="display: none" class="imgClose">
							<img src="images/Dashboard/copyMsg.gif" style="display: none" align="left" 	onclick="showFullDetails('<%=messageslist.get(i).getMessage() %>','<%=messageslist.get(i).getCategory()%>')">
							<input type="hidden" name="categoryValue" id="categoryValue" value="<%=messageslist.get(i).getCategory()%>">
							</td>
							<%} %>
						</tr>
					</table>
				</div></td>
		</tr>
	</table>
	<div id="showDetails" class="jqmWindow"></div>
</body>
</html>
