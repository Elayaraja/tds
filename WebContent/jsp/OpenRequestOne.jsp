<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.OpenRequestBO" %>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="openBO"   class="com.tds.tdsBO.OpenRequestBO" />
<script type="text/javascript" src="Ajax/Common.js"></script>
 
<%
	ArrayList al_prevOpenRequests = (ArrayList)request.getAttribute("prevopenrequests");
	ArrayList al_prevAddress = (ArrayList)request.getAttribute("prevaddresses");
	ArrayList al_landMark=(ArrayList)request.getAttribute("landMark");
	ArrayList<OpenRequestBO> al_prevLandMark=(ArrayList<OpenRequestBO>)request.getAttribute("prevLM");
%>
 	<%if(al_prevOpenRequests!=null && al_prevOpenRequests.size()>0) {%>
	    <NumRows>NumRows=2</NumRows>###
	<%}else if(al_prevAddress!=null && al_prevAddress.size()>1) {%>
   	 	<NumRows>NumRows=2</NumRows>###
   	 <%}else if(al_landMark!=null && al_landMark.size()>1) {%>
   	 	<NumRows>NumRows=2</NumRows>###
    <%} else if(al_prevAddress!=null && al_prevAddress.size()==1){%>
    	<NumRows>NumRows=1</NumRows>###
    <%} else if(al_landMark!=null && al_landMark.size()==1){%>
    	<NumRows>NumRows=3</NumRows>###
    <%} else if(al_prevLandMark!=null && al_prevLandMark.size()>0){%>
    	<NumRows>NumRows=2</NumRows>###
    <%} %>
    <%if(al_prevOpenRequests!=null && !al_prevOpenRequests.isEmpty()) {%>
    
 	<h3><center style="background-color:#FCD374">Open Jobs</center></h3>
	 <div style="overflow-y:scroll;max-height: 160px;overflow-x:hide; "> 
 	<table border=2 BORDERCOLOR=green valign=top style="overflow: scroll;"><tr style='color: blue'><td>Edit</td><td>Driver</td><td width=5%>PickupTime</td><td width=45%>PickupAddress</td><td width=25%>Spl Ins</td><td width="25%">Name</td><td>Status</td>
 	</tr>

	<%for(int i=0;i<al_prevOpenRequests.size() && i<3;i++){ %>
													
	<%
	OpenRequestBO ORforDisplay = (OpenRequestBO) al_prevOpenRequests.get(i);;
	%>
	<tr> 
 	 <td><B></B><input type='radio' id='PrevAddress<%=i%>' name='PrevAddress' value='PrevAddress' onClick='appendPreviousAddress(<%=i%>,this)'></input></td> 
	 <td><%=ORforDisplay.getDriverid().toString()%><textarea style='display: none;' name='DriverId_<%=i%>' id='DriverId_<%=i%>'><%=ORforDisplay.getDriverid() %></textarea></td>																
	 <td><%=ORforDisplay.getSdate()%>&nbsp;<%=ORforDisplay.getSttime()%><textarea style='display: none;' name='startTime_<%=i%>' id='startTime_<%=i%>'><%=ORforDisplay.getSttime() %></textarea></td>
	 <td><%=ORforDisplay.getSadd1()%>,<%=ORforDisplay.getSadd2()%>,<%=ORforDisplay.getScity()%>,<%=ORforDisplay.getSstate()%>- <%=ORforDisplay.getSzip()%>										
	 <input type="hidden" id="prevAdd1<%=i%>" value="<%=ORforDisplay.getSadd1()==null?"":ORforDisplay.getSadd1()%>"></input>
	 <input type="hidden" id="prevAdd2<%=i%>" value="<%=ORforDisplay.getSadd2()==null?"":ORforDisplay.getSadd2()%>"></input>
	 <input type="hidden" id="prevCity<%=i%>" value="<%=ORforDisplay.getScity()==null?"":ORforDisplay.getScity()%>"></input>
	 <input type="hidden" id="prevState<%=i%>" value="<%=ORforDisplay.getSstate()==null?"":ORforDisplay.getSstate()%>"></input>
	 <input type="hidden" id="prevZip<%=i%>" value="<%=ORforDisplay.getSzip()==null?"":ORforDisplay.getSzip()%>"></input>
	 <input type="hidden" id="prevDate<%=i%>" value="<%=ORforDisplay.getSdate()==null?"":ORforDisplay.getSdate()%>"></input>
	 <input type="hidden"  name='prevLatitude<%=i%>' id='prevLatitude<%=i%>' value="<%=ORforDisplay.getSlat()==null?"":ORforDisplay.getSlat() %>"></input>
     <input type="hidden"  name='prevLongitude<%=i%>' id='prevLongitude<%=i%>' value="<%=ORforDisplay.getSlong()==null?"":ORforDisplay.getSlong() %>"></input>
     <input type="hidden"  name='tripId<%=i%>' id='tripId<%=i%>' value="<%=ORforDisplay.getTripid()==null?"":ORforDisplay.getTripid() %>"></input>
	 <input type="hidden"  name='comments<%=i%>' id='comments<%=i%>' value="<%=ORforDisplay.getComments()==null?"":ORforDisplay.getComments() %>"></input>
	 <input type="hidden" id="prevEAdd1<%=i%>" value="<%=ORforDisplay.getEadd1()==null?"":ORforDisplay.getEadd1()%>"></input>
	 <input type="hidden" id="prevEAdd2<%=i%>" value="<%=ORforDisplay.getEadd2()==null?"":ORforDisplay.getEadd2()%>"></input>
	 <input type="hidden" id="prevECity<%=i%>" value="<%=ORforDisplay.getEcity()==null?"":ORforDisplay.getEcity()%>"></input>
	 <input type="hidden" id="prevEState<%=i%>" value="<%=ORforDisplay.getEstate()==null?"":ORforDisplay.getEstate()%>"></input>
	 <input type="hidden" id="prevEZip<%=i%>" value="<%=ORforDisplay.getEzip()==null?"":ORforDisplay.getEzip()%>"></input>
	 <input type="hidden"  name='prevELatitude<%=i%>' id='prevELatitude<%=i%>' value="<%=ORforDisplay.getEdlatitude()==null?"":ORforDisplay.getEdlatitude() %>"></input>
     <input type="hidden"  name='prevELongitude<%=i%>' id='prevELongitude<%=i%>' value="<%=ORforDisplay.getEdlongitude()==null?"":ORforDisplay.getEdlongitude() %>"></input>
	 <input type="hidden" id="Acct<%=i%>" value="<%=ORforDisplay.getAcct()==null?"":ORforDisplay.getAcct()%>"></input>
	 <input type="hidden" id="VehicleId<%=i%>" value="<%=ORforDisplay.getVehicleNo()==null?"":ORforDisplay.getVehicleNo()%>"></input>
	 <input type="hidden" id="PayType<%=i%>" value="<%=ORforDisplay.getPaytype()==null?"":ORforDisplay.getPaytype()%>"></input>
	 <input type="hidden" id="Zone<%=i%>" value="<%=ORforDisplay.getQueueno()==null?"":ORforDisplay.getQueueno()%>"></input>
	 <input type="hidden" id="LandMark<%=i%>" value="<%=ORforDisplay.getSlandmark()==null?"":ORforDisplay.getSlandmark()%>"></input>
	 <input type="hidden" id="ELandMark<%=i%>" value="<%=ORforDisplay.getElandmark()==null?"":ORforDisplay.getElandmark()%>"></input>
	 <input type="hidden" id="Flag<%=i%>" value="<%=ORforDisplay.getDrProfile()==null?"":ORforDisplay.getDrProfile()%>"></input>
	 <input type="hidden" id="Phone<%=i%>" value="<%=ORforDisplay.getPhone()==null?"":ORforDisplay.getPhone()%>"></input>
	 <input type="hidden" id="Name_<%=i%>" value="<%=ORforDisplay.getName()==null?"":ORforDisplay.getName()%>"></input>
	 <input type="hidden" id="SplIns<%=i%>" value="<%=ORforDisplay.getSpecialIns()==null?"":ORforDisplay.getSpecialIns()%>"></input>
 	 <input type="hidden" id="sharedRide<%=i%>" value="<%=ORforDisplay.getTypeOfRide()==null?"":ORforDisplay.getTypeOfRide()%>"></input>
 	 <input type="hidden" id="repeatGroup<%=i%>" value="<%=ORforDisplay.getRepeatGroup()==null?"":ORforDisplay.getRepeatGroup()%>"></input>
 	 <input type="hidden" id="roundTrip<%=i%>" value="<%=ORforDisplay.getRoundTrip()==null?"":ORforDisplay.getRoundTrip()%>"></input>
 	 <input type="hidden" id="numOfPass<%=i%>" value="<%=ORforDisplay.getNumOfPassengers()==0?"1":ORforDisplay.getNumOfPassengers()%>"></input>
 	 <input type="hidden" id="dropTime<%=i%>" value="<%=ORforDisplay.getDropTime()==null?"":ORforDisplay.getDropTime()%>"></input>
 	 <input type="hidden" id="premiumCustomerReq<%=i%>" value="<%=ORforDisplay.getPremiumCustomer()%>"></input>
 	 <input type="hidden" id="tripStatusReq<%=i%>" value="<%=ORforDisplay.getChckTripStatus()%>"></input>
	 <input type="hidden" id="advTime<%=i%>" value="<%=ORforDisplay.getAdvanceTime()==null?"":ORforDisplay.getAdvanceTime()%>"></input>
	 <input type="hidden" id="fleetCode<%=i%>" value="<%=ORforDisplay.getAssociateCode()==null?"":ORforDisplay.getAssociateCode()%>"></input>
	 <input type="hidden" id="jobRate<%=i%>" value="<%=ORforDisplay.getJobRating()%>"></input>
	 <input type="hidden" id="callerPhone<%=i%>" value="<%=ORforDisplay.getCallerPhone()==null?"":ORforDisplay.getCallerPhone()%>"></input>
	 <input type="hidden" id="callerName<%=i%>" value="<%=ORforDisplay.getCallerName()==null?"":ORforDisplay.getCallerName()%>"></input>
	 <input type="hidden" id="meterType<%=i%>" value="<%=ORforDisplay.getPaymentMeter()%>"></input>
	 <input type="hidden" id="eMailReq_<%=i%>" value="<%=ORforDisplay.getEmail()==null?"":ORforDisplay.getEmail()%>"></input>
     <input type="hidden" id="airNameReq_<%=i%>" value="<%=ORforDisplay.getAirName()==null?"":ORforDisplay.getAirName()%>"></input>
  	 <input type="hidden" id="airNoReq_<%=i%>" value="<%=ORforDisplay.getAirNo()==null?"":ORforDisplay.getAirNo()%>"></input>
  	 <input type="hidden" id="airFromReq_<%=i%>" value="<%=ORforDisplay.getAirFrom()==null?"":ORforDisplay.getAirFrom()%>"></input>
  	 <input type="hidden" id="airToReq_<%=i%>" value="<%=ORforDisplay.getAirTo()==null?"":ORforDisplay.getAirTo()%>"></input>
  	 
  	 <input type="hidden" id="refnum_<%=i%>" value="<%=ORforDisplay.getRefNumber()==null?"":ORforDisplay.getRefNumber()%>"></input>
  	 <input type="hidden" id="refnum1_<%=i%>" value="<%=ORforDisplay.getRefNumber1()==null?"":ORforDisplay.getRefNumber1()%>"></input>
  	 <input type="hidden" id="refnum2_<%=i%>" value="<%=ORforDisplay.getRefNumber2()==null?"":ORforDisplay.getRefNumber2()%>"></input>
  	 <input type="hidden" id="refnum3_<%=i%>" value="<%=ORforDisplay.getRefNumber3()==null?"":ORforDisplay.getRefNumber3()%>"></input>
  	 </td>
  	
  	  <% if(ORforDisplay.getSpecialIns()==null)
	  { %>
		  <td align="center">-</td>
	  <% }
	  else{%> 
		  
		  <td><%=ORforDisplay.getSpecialIns()%></td>
	  <% } %> 
	  
	  <td><%=ORforDisplay.getName() %><textarea style='display: none;' name='Name_<%=i%>' id='Name_<%=i%>'><%=ORforDisplay.getName() %></textarea></td>

	  <%if(ORforDisplay.getChckTripStatus()==40) {%>
	  <td>Job allocated</td>
	  <%} else if(ORforDisplay.getChckTripStatus()==43) {%>
	  <td>On route to pickup</td>
	  <%} else if(ORforDisplay.getChckTripStatus()==47) {%>
	  <td>Trip started</td>
	  <%} else if(ORforDisplay.getChckTripStatus()==48) {%>
	  <td>Driver reported No Show</td>
	  <%} else if(ORforDisplay.getChckTripStatus()==4) {%>
	  <td>Performing dispatch</td>
	  <%} else if(ORforDisplay.getChckTripStatus()==3) {%>
	  <td>Performing dispatch can't find drivers</td>
	  <%} else if(ORforDisplay.getChckTripStatus()==8) {%>
	  <td>Dispatch not started</td>
	  <%} else if(ORforDisplay.getChckTripStatus()==99) {%>
	  <td>Trip on hold</td>
	  <%} %>	
	  <td>
	  		<input type='Button' id='DeletePrevReq<%=i%>' name='DeletePrevReq' value='Cancel Job' onClick='deleteJobsPopUp(1,"",<%=i%>)'></input>
	  		<input type='Button' id='getStatusFromDriver<%=i%>' name='getStatusFromDriver' value='Get Status' onClick='callDriverForStatusOR(<%=ORforDisplay.getTripid()%>)'></input>
	  		<input type='Button' id='printThisJob<%=i%>' name='printThisJob' value='Print' onClick='printCurrentJob(<%=ORforDisplay.getTripid()%>)'></input>
	  </td>
	 <%} %>
	</tr>
 	 </table> </div>
   	<%} %>
  <%if(al_prevAddress!=null && !al_prevAddress.isEmpty()) {%>
 								<h3><center style="background-color:#FCD374">Previous Address</center></h3>
								 <div style="overflow-y:scroll;max-height: 190px;overflow-x:hide; "> 
								 <table border=2 BORDERCOLOR=green>
 								<tr style='color: blue'><td>S.No</td><td>Select</td><td width="25%">Add1</td><td width="15%">Add2</td><td width="20%">City</td><td  width="20%">State</td><td>Zip</td><td width="25%">Name</td>
 								</tr>
								
								<%for(int i=0;i<al_prevAddress.size() && i<3;i++){ %>
													
								<%
								OpenRequestBO ORforDisplayAdd = (OpenRequestBO) al_prevAddress.get(i);
								%> 
								<tr>
								<td><%=(i+1)%></td>
   								<td><B></B><input type='radio' id='PrevAddress<%=i%>' name='PrevAddress' value='Fcheck'  onkeyup="KeyCheck(event,'Fcheck<%=i %>','Fcheck<%=i%>')" onClick='appendParentFrom(<%=i%>,this)'></input></td> 
                                <td><%=ORforDisplayAdd.getSadd1().toString()%><textarea style='display: none;' name='sadd1_<%=i%>' id='sadd1_<%=i%>'><%=ORforDisplayAdd.getSadd1() %></textarea></td>
                                <td><%=ORforDisplayAdd.getSadd2().toString()%><textarea style='display: none;' name='sadd2_<%=i%>' id='sadd2_<%=i%>'><%=ORforDisplayAdd.getSadd2() %></textarea></td>
                                <td><%=ORforDisplayAdd.getScity().toString()%><textarea style='display: none;' name='scity<%=i%>' id='scity<%=i%>'><%=ORforDisplayAdd.getScity() %></textarea></td>
                                <td><%=ORforDisplayAdd.getSstate().toString()%><textarea style='display: none;' name='sstate<%=i%>' id='sstate<%=i%>'><%=ORforDisplayAdd.getSstate() %></textarea></td>
                                <td><%=ORforDisplayAdd.getSzip().toString()==null?"":ORforDisplayAdd.getSzip().toString()%><textarea style='display: none;' name='szip<%=i%>' id='szip<%=i%>'><%=ORforDisplayAdd.getSzip() %></textarea></td>
                                <td><%=ORforDisplayAdd.getName()%><textarea style='display: none;' name='name<%=i%>' id='name_<%=i%>'><%=ORforDisplayAdd.getName() %></textarea>
                 				<input type="hidden"  name='sLatitude<%=i%>' id='sLatitude<%=i%>' value="<%=ORforDisplayAdd.getSlat() %>"></input>
                                <input type="hidden"  name='sLongitude<%=i%>' id='sLongitude<%=i%>' value="<%=ORforDisplayAdd.getSlong() %>"></input>
                 				<input type="hidden" name='sAccount<%=i%>' id='sAccount<%=i%>' value="<%=ORforDisplayAdd.getAcct()!=null?ORforDisplayAdd.getAcct():""%>"></input>
                 				<input type="hidden"  name='addressKey_<%=i%>' id='addressKey_<%=i%>' value="<%=ORforDisplayAdd.getUserAddressKey() %>"></input>
                                <input type="hidden"  name='masterAddressKey_<%=i%>' id='masterAddressKey_<%=i%>' value="<%=ORforDisplayAdd.getMasterAddressKey() %>"></input>
                                <input type="hidden"  name='payType<%=i%>' id='payType<%=i%>' value="<%=ORforDisplayAdd.getPaytype() %>"></input>
                                <input type="hidden"  name='comments<%=i%>' id='commentsAdd<%=i%>' value="<%=ORforDisplayAdd.getComments()!=null?ORforDisplayAdd.getComments():"" %>"></input>
                                <input type="hidden"  name='commentsDriver<%=i%>' id='commentsDriver<%=i%>' value="<%=ORforDisplayAdd.getSpecialIns()==null?"":ORforDisplayAdd.getSpecialIns()%>"></input>
							 	 <input type="hidden" id="premiumCustomer<%=i%>" value="<%=ORforDisplayAdd.getPremiumCustomer()%>"></input>
							 	 <input type="hidden" id="advanceTime<%=i%>" value="<%=ORforDisplayAdd.getAdvanceTime()%>"></input>
                 				<input type="hidden"  name='totalAddressSize' id='totalAddressSize' value="<%=al_prevAddress.size() %>"></input>
							 	 <input type="hidden" id="eMail_<%=i%>" value="<%=ORforDisplayAdd.getEmail()==null?"":ORforDisplayAdd.getEmail()%>"></input>
							 	  <input type="hidden" id="FlagCustomer<%=i%>" value="<%=ORforDisplayAdd.getDrProfile()==null?"":ORforDisplayAdd.getDrProfile()%>"></input>
							 	 </td>
                 				</tr>
								<%} %>
						     	  </table></div>
								<%}  else {%>
                 				<input type="hidden"  name='totalAddressSize' id='totalAddressSize' value="0"></input>
								<%} %> 
						<%if(al_prevLandMark!=null && !al_prevLandMark.isEmpty()) {%>
 								<h3><center style="background-color:#FCD374">Previous LandMark</center></h3>
								 <div style="overflow-y:scroll;max-height: 110px;overflow-x:hide; "> 
								 <table border=2 BORDERCOLOR=green > 
 								<tr style='color: blue'><td>S.No</td><td width="25%">LandMark</td><td width="25%">Add1</td><td width="15%">Add2</td><td width="20%">City</td><td  width="20%">State</td><td>Zip</td>
 								</tr>
							
								<%for(int i=0;i<al_prevLandMark.size() && i<3;i++){ %>
								<tr>
   								<td><B></B><input type='radio' id='PrevAddress<%=i%>' name='PrevAddress' value='LMCheck'  onkeyup="KeyCheck(event,'LMCheck<%=i%>','LMCheck<%=i%>')" onClick='appendPrevLM(<%=i%>,this)'></input></td> 
                                <td><%=al_prevLandMark.get(i).getSlandmark()==null?"":al_prevLandMark.get(i).getSlandmark()%><textarea style='display: none;' name='sLM_<%=i%>' id='PLM<%=i%>'><%=al_prevLandMark.get(i).getSlandmark() %></textarea></td>
                                <td><%=al_prevLandMark.get(i).getSadd1().toString()%><textarea style='display: none;' name='sadd1_<%=i%>' id='sadd1PLM_<%=i%>'><%=al_prevLandMark.get(i).getSadd1() %></textarea></td>
                                <td><%=al_prevLandMark.get(i).getSadd2().toString()%><textarea style='display: none;' name='sadd2_<%=i%>' id='sadd2PLM_<%=i%>'><%=al_prevLandMark.get(i).getSadd2() %></textarea></td>
                                <td><%=al_prevLandMark.get(i).getScity().toString()%><textarea style='display: none;' name='scity<%=i%>' id='scityPLM<%=i%>'><%=al_prevLandMark.get(i).getScity() %></textarea></td>
                                <td><%=al_prevLandMark.get(i).getSstate().toString()%><textarea style='display: none;' name='sstate<%=i%>' id='sstatePLM<%=i%>'><%=al_prevLandMark.get(i).getSstate() %></textarea></td>
                                <td><%=al_prevLandMark.get(i).getComments()==null?"":al_prevLandMark.get(i).getComments()%><textarea style='display: none;' name='commentsPLM<%=i%>' id='commentsPLM<%=i%>'><%=al_prevLandMark.get(i).getComments()==null?"":al_prevLandMark.get(i).getComments() %></textarea></td>
                                <td><%=al_prevLandMark.get(i).getSzip()==null?"":al_prevLandMark.get(i).getSzip()%><textarea style='display: none;' name='szip<%=i%>' id='szipPLM<%=i%>'><%=al_prevLandMark.get(i).getSzip()!=null?al_prevLandMark.get(i).getSzip():"" %></textarea>
                 				<input type="hidden"  name='sLatitude<%=i%>' id='sLatitudePLM<%=i%>' value="<%=al_prevLandMark.get(i).getSlat()==null?"0.00":al_prevLandMark.get(i).getSlat() %>"></input>
                                <input type="hidden"  name='sLongitude<%=i%>' id='sLongitudePLM<%=i%>' value="<%=al_prevLandMark.get(i).getSlong()==null?"0.00":al_prevLandMark.get(i).getSlong() %>"></input>
                 				<input type="hidden"  name='LMKey<%=i%>' id='LMKey<%=i%>' value="<%=al_prevLandMark.get(i).getLandMarkKey()==null?"":al_prevLandMark.get(i).getLandMarkKey() %>"></input></td>
                 				</tr>
								<%} %>
						     	  </table></div>
								<%} %>

  <%if(al_landMark!=null && !al_landMark.isEmpty()) {%>
 								 
 								<h3><center style="background-color:#FCD374">LandMark</center></h3>
								 <div style="overflow-y:scroll;max-height: 110px;overflow-x:hide; "> 
								 <table border=2 BORDERCOLOR=green valign=top > 
 								<tr style='color: blue'><td>S.No</td><td>Edit</td><td>LandMark</td><td width="30%">Add1</td><td width="30%">Add2</td><td width="20%">City</td><td  width="20%">State</td><td>Zip</td>
 								</tr>
							
								<%for(int i=0;i<al_landMark.size() && i<3;i++){ %>
													
								<%
								OpenRequestBO ORforDisplayAdd = (OpenRequestBO) al_landMark.get(i);
								%> 
								<tr>
								<td><%=(i+1)%></td>
   								<td><B></B><input type='radio' id='PrevAddress<%=i%>' name='PrevAddress' value='landMark'  onkeyup="KeyCheck(event,'landMark<%=i %>','landMark<%=i%>')" onClick='appendLandMark(<%=i%>,this)'></input></td> 
                                <td><%=ORforDisplayAdd.getSlandmark().toString()%><textarea style='display: none;' name='landMark_<%=i%>' id='landMarkLM_<%=i%>'><%=ORforDisplayAdd.getSlandmark() %></textarea></td>
                                <td><%=ORforDisplayAdd.getSadd1().toString()%><textarea style='display: none;' name='sadd1_<%=i%>' id='sadd1LM_<%=i%>'><%=ORforDisplayAdd.getSadd1() %></textarea></td>
                                <td><%=ORforDisplayAdd.getSadd2().toString()%><textarea style='display: none;' name='sadd2_<%=i%>' id='sadd2LM_<%=i%>'><%=ORforDisplayAdd.getSadd2() %></textarea></td>
                                <td><%=ORforDisplayAdd.getScity().toString()%><textarea style='display: none;' name='scity<%=i%>' id='scityLM<%=i%>'><%=ORforDisplayAdd.getScity() %></textarea></td>
                                <td><%=ORforDisplayAdd.getSstate().toString()%><textarea style='display: none;' name='sstate<%=i%>' id='sstateLM<%=i%>'><%=ORforDisplayAdd.getSstate() %></textarea></td>
                                <td><%=ORforDisplayAdd.getComments()==null?"":ORforDisplayAdd.getComments()%><textarea style='display: none;' name='commentsLM<%=i%>' id='commentsLM<%=i%>'><%=ORforDisplayAdd.getComments()==null?"":ORforDisplayAdd.getComments() %></textarea></td>
                                <td><%=ORforDisplayAdd.getSzip()==null?"":ORforDisplayAdd.getSzip()%><textarea style='display: none;' name='szip<%=i%>' id='szipLM<%=i%>'><%=ORforDisplayAdd.getSzip() %></textarea>
                 				<input type="hidden"  name='sLatitude<%=i%>' id='sLatitudeLM<%=i%>' value="<%=ORforDisplayAdd.getSlat() %>"></input>
                                <input type="hidden"  name='sLongitude<%=i%>' id='sLongitudeLM<%=i%>' value="<%=ORforDisplayAdd.getSlong() %>"></input>
                 				<input type="hidden"  name='Key<%=i%>' id='Key<%=i%>' value="<%=ORforDisplayAdd.getLandMarkKey()==null?"":ORforDisplayAdd.getLandMarkKey() %>"></input></td>
                 				</tr>
								<%} %>
						     	  </table></div>
						<%} %>	