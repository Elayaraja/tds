<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>
 
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%><html>
<head>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
 <link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS (Taxi Dispatch System)</title>
<script language="javascript">

function popupClose() { 
	window.close(); 
}
function backButton() { 
	 
	var urll ='/TDS/control#dispatchView'; 
	window.open(urll, "",'screenX=0,screenY=0,toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=0,width=1100,height=800'); 
}
 


function setPhone(p_phone) {
	if(p_phone.value.length == 3) {
		p_phone.value += "-";
	} else if(p_phone.value.length == 7) {
		p_phone.value += "-";
	}
}
</script>
</head>
<%
String status[] = {"Request","In Process","Completed"};
%>
<body>
<form name="masterForm" action="control" method="post">
<jsp:useBean id="openBO"   class="com.tds.tdsBO.OpenRequestBO"  />
<% if(request.getAttribute("openrequest") != null) {
		openBO = (OpenRequestBO)request.getAttribute("openrequest");
	%>
		<script type="text/javascript">
			document.masterForm.sdate.value = "";
			document.masterForm.shrs.value = "";
			document.masterForm.smin.value = "";
			alert("Hello");
		</script>
	<% } %>
<jsp:setProperty name="openBO"  property="*"/>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveOpenRequest %>">
<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");

%>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="stlatitude" value="<%= openBO.getSlat() %>"> 
<input type="hidden" name="stlongitude" value="<%= openBO.getSlong() %>">
<input type="hidden" name="edlatitude" value="<%= openBO.getEdlatitude() %>">
<input type="hidden" name="edlongitude" value="<%= openBO.getEdlongitude() %>">
<input type="hidden" name="tripid" value="<%= openBO.getTripid() %>">
 
<input type="hidden" name="sesexit" id="sesexit" value="<%=adminBo==null?"0":"1"%>">

 
            <div class="leftCol">
                <div class="clrBth"></div>
                </div>  
            	              
                <div class="rightCal">
                <div class="rightColIn">
                            		 
                        		<%
									String error ="";
									if(request.getAttribute("errors") != null) {
									error = (String) request.getAttribute("errors");
									}
								%>
								<div id="errorpage">
								<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %></div>
								
								<%String result="";
								if(request.getAttribute("page")!=null) {
									result = (String)request.getAttribute("page");
								}
								%>
								<div id="errorpage">
								<%=result.length()>0?"Sucessfully"+result:"" %>
								</div>
								<h1>View Open Request</h1>
                                		
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<td>
					<table id="bodypage" width="100%" >
						<tr>
							<td class="firstCol" >Phone No</td>
							<td> <%= openBO.getPhone()  %></td>
							<td>
							  
                                   
							</td>
						</tr>
						<tr>
							<td class="firstCol" >Name</td>
							<td> <%= openBO.getName() %></td>
							<td></td>
						</tr>
						<tr>
							<td class="firstCol" >Service Date</td>
							<td> <%= openBO.getSdate()  %></td>
							<td></td>
						</tr>
						<tr>
							<td class="firstCol" >Service Time</td>
							<td> <%= openBO.getShrs()  %></td>
							<td></td>
						</tr>
 					</table  >
 					</td>
 					</tr> 
 					</table>
 					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
 					<tr>
 					<td>
 					<table  id="bodypage" width="800" align="left" >
 						
 						<tr       >
 						<td colspan="3"    style="background-color:#FCD374" ><b>From Address</b></td>
 						<td colspan="3"  style="background-color:#FCD374"  ><b>To Address</b></td>
 						
 						 
 						</tr>
 						<tr>
 							<td  class="firstCol">Intersection</td>
 							<td> <%= openBO.getSintersection()  %></td>
 							<td></td>
 							<td></td>
 							<td></td>
 							<td></td>
 						</tr>
 						<tr>
 							<td  class="firstCol">Land Mark</td>
 							<td><%= openBO.getSlandmark()  %></td>
 							<td></td>
 							<td></td>
 							<td></td>
 							<td></td>
 						</tr>
<%--  						<tr>
 							<td   class="firstCol">Air Port Code</td>
 							<td><%= openBO.getSaircode() %></td>
 							<td>  </td>
 							<td class="firstCol" >To Air Port Code</td>
 							<td><%= openBO.getEaircode() %></td>
 								  
 							 
 						</tr>
 --%> 						<tr>
 							<td  class="firstCol">Address 1</td>
 							<td><%=openBO.getSadd1()  %></td>
 							 
  			                         <td></td>
 							<td class="firstCol">Address 1</td>
 							<td> <%=openBO.getEadd1() %> </td> 
 							<td></td>
 							
 						</tr>
 						<tr>
 							<td   class="firstCol"> Address 2</td>
 							<td> <%=openBO.getSadd2() %> </td>
 							 
		                            <td></td>
 							<td class="firstCol">Address 2</td>
 							<td><%= openBO.getEadd2()  %></td> 
 							<td></td>
 							 
 						</tr>
 						<tr>
 							<td  class="firstCol">City</td>
 							<td><%= openBO.getScity()  %></td>
 							 		<td></td>
 							<td class="firstCol">City</td>
 							<td><%= openBO.getEcity()  %></td> 
 							<td></td>
 							 
 						</tr> 
 						<tr>
 							<td  class="firstCol" >State</td>
 							<td><%= openBO.getSstate()  %></td> 
                                   	 <td></td>
 							<td class="firstCol">State</td>
 							<td><%= openBO.getEstate()  %></td> 
 							<td></td>
 							 
 						</tr>
 						<tr>
 							<td  class="firstCol">Zip</td>
 							<td><%= openBO.getSzip()  %></td> 
									<td></td>
 							<td class="firstCol">Zip</td>
 							<td><%=openBO.getEzip()  %></td>
 							 
 							 
 							
 						</tr>
 						<tr>
 							<td  class="firstCol">Zone</td>
 							<td> <%=openBO.getQueueno() %>   </td>
 							 
 						 
 						</tr>
 						 
 							<tr>
 							<td></td>
 							<td></td>
							<td  align="right" ><input type="submit" name="submit" value="Close " onclick="popupClose()"  > </td>
						 
 							<td></td>  
			</tr>   
 						 
 					</table> 
    
					</td>
					</tr>
					</table>
    
                                    
                                    </div>    
                                
                        	</div>
                      
                </div>    
              </div>
            	
                <div class="clrBth"></div>
         
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
           
 
</form>
</body>
</html>
