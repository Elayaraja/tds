<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.OpenRequestBO" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>


<jsp:useBean id="openBO"   class="com.tds.tdsBO.OpenRequestBO" />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="javascript"> 

 function openPayment()
 {
	
	var ele = document.getElementById("HideText");
	var text = document.getElementById("displayText");
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "Payment";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "Payment";
	} 
 } 
 
 function openSplReq()
 {
	 var ele = document.getElementById("hideTable");
		var table = document.getElementById("displayTable");
		if(ele.style.display == "block") {
	    		ele.style.display = "none";
			text.innerHTML = "Special Request";
	  	}
		else {
			ele.style.display = "block";
			text.innerHTML = "Special Reques";
		}
	 } 
	 
</script>

<script src="js/shortcut.js" language="javascript"></script>

<%
	ArrayList al_veList = (ArrayList)request.getAttribute("vecFlag");
	ArrayList al_drList = (ArrayList)request.getAttribute("drFlag");
	
	ArrayList al_prevOpenRequests = (ArrayList)request.getAttribute("prevopenrequests");
	ArrayList al_prevAddress = (ArrayList)request.getAttribute("prevaddresses");
%>

<script type="text/javascript">

shortcut.add("F2",function() { 
	focus("sadd1");
});
shortcut.add("F3",function() { 
	focus("slandmark");
});
shortcut.add("F4",function() { 
	focus("sintersection");
});
shortcut.add("F5",function() { 
	focus("eadd1");
});
shortcut.add("Enter",function() { 
	submit(submit());
});
	
	function focus(row_id) {
		 var field = row_id; 
		 document.getElementById(field).focus(); 
	 }


	 function submit() { 
		 document.masterForm.submit(); 
	 }
	 
	 
	 function popUp(row_id) { 	  
		 
		 
			var id = document.getElementById(row_id).value; 
		
			if(id.length>0) {  
				
				//document.getElementById('landthick');
				
				
			
				var url ='/TDS/AjaxClass?event=getlandmardDetails&module=dispatchView&landMark='+id; 
			
				document.getElementById('slandmark').href=url; 
				var a = document.getElementById('slandmark');
				popup_window = window.open (url,"mywindow");
			
				var t = a.title || a.name || null;
				var l = a.href || a.alt;
				var g = a.rel || false;
				
				 
			}  
		}
	 
	 /* function popUp(row_id) { 	  
	 
	 	
		var id = document.getElementById(row_id).value; 
		if(id.length>0) {  
			document.getElementById('landthick').style.display="block";
			var urll ='/TDS/AjaxClass?event=getlandmardDetails&module=dispatchView&landMark='+id; 
			document.getElementById("landthick").href=urll; 
			var a = document.getElementById('landthick');
			var t = a.title || a.name || null;
			var l = a.href || a.alt;
			var g = a.rel || false;
			tb_show(t,l,g);
			 
		}  
	} */ 
	
	
	function reqDetails(id) {
		alert("this"+id+"end");
	}

	function KeyCheck(e,previous,next)  {  
		
	 
	   var KeyID = (window.event) ? event.keyCode : e.keyCode; 
	   if((keyID = 38) || (keyID = 40)||(keyID = 13)) { 
	   	switch(KeyID) 
	   	{  
	      case 38:  
	      document.getElementById(previous).focus(); 
	      break; 
	      
	      case 40: 
	      document.getElementById(next).focus(); 
	      break;
	   } 
	}
	}
	
	function clearLankMarkKey()
	{
		document.getElementById('landmarkKey').value  = "";
	}
	</script>
	<% if(request.getAttribute("openrequest") != null) {
		openBO = (OpenRequestBO)request.getAttribute("openrequest");
		
	%>
	 <script type="text/javascript">
			document.masterForm.sdate.value = "";
			document.masterForm.shrs.value = "";
			document.masterForm.smin.value = "";
			
	</script>
	
	<% } %>
	
<!-- <link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/> -->
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" /> -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/thickbox.js"></script>
<!-- <link rel="stylesheet" href="css/thickbox.css" media="screen" type="text/css"> -->

<link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></LINK>
<SCRIPT type="text/javascript" src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
<script language="javascript">
			
function loadDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
	var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
	var hrsmin = hors+""+min;  
	
	
	<%
	if(openBO.getSdate().length() <= 0) {
	%>
		document.masterForm.sdate.value = cMonth+"/"+cDate+"/"+cYear;
		document.masterForm.shrs.value  = hrsmin; 
		
	<% 
	}
	%>
 }
function openDrop() { 
	if(document.getElementById("smsStatus").checked) {  
		document.getElementById("smsList").style.display='block'; 
	}else {
		document.getElementById("smsList").style.display='none';
		document.getElementById("toSms").value="0";
	}
}
function closePopup() { 
	
	window.close();
	
}	

function clearLatNLong(){
	document.getElementById('sLongitude').value  = "";
	document.getElementById('sLatitude').value  = "";
	document.getElementById('addressverify').style.background="yellow";
}


</script>
</head>

<%
String status[] = {"Request","In Process","Completed"};
%>
<body   onload="loadDate()"  >
<form name="masterForm" action="control" method="post">

<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	
%>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<jsp:setProperty name="openBO"  property="*"/>

<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>"/>
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveOpenRequest %>"/>



<input type="hidden" name="stlatitude" id="stlatitude"  value="<%= openBO.getSlat() %>"/> 
<input type="hidden" name="stlongitude" id="stlongitude" value="<%= openBO.getSlong() %>"/>


 					
<input type="hidden"  id="landmarkKey"    name="landmarkKey"  value="<%= openBO.getLandMarkKey() %>"/>
<input type="hidden"  id="land_userZone"    name="land_userZone"  value="<%= openBO.getLandMarkZone() %>"/>
<input type="hidden"  id="userFrom" name="userFrom"  value="<%= openBO.getUserAddressKey() %>"/>
<input type="hidden"  id="userTo"     name="userTo"   value="<%= openBO.getUserAddressKey() %>"/>
 					
 					


<input type="hidden" name="edlatitude" id="edlatitude" value="<%= openBO.getEdlatitude() %>"/>
<input type="hidden" name="edlongitude" id="edlongitude" value="<%= openBO.getEdlongitude() %>"/>

<input type="hidden" name="sesexit" id="sesexit" value="<%=adminBo==null?"0":"1"%>"/>
<input type="hidden" name="dispatch" value="1"/>

<% 	HashMap hmp_address;
	if(request.getAttribute("address") != null) {
		hmp_address = (HashMap)request.getAttribute("address");
		
  %>
 		<input type="hidden" name="stadd1" value="<%= hmp_address.get("stadd1").toString() %>"/>
 		<input type="hidden" name="stadd2" value="<%= hmp_address.get("stadd2").toString() %>"/>
 		<input type="hidden" name="stcity" value="<%= hmp_address.get("stcity").toString() %>"/>
 		<input type="hidden" name="ststate" value="<%= hmp_address.get("ststate").toString() %>"/>
 		<input type="hidden" name="stzip" value="<%= hmp_address.get("stzip").toString() %>"/>
 		<input type="hidden" name="edadd1" value="<%= hmp_address.get("edadd1").toString() %>"/>
 		<input type="hidden" name="edadd2" value="<%= hmp_address.get("edadd2").toString() %>"/>
 		<input type="hidden" name="edcity" value="<%= hmp_address.get("edcity").toString() %>"/>
 		<input type="hidden" name="edstate" value="<%= hmp_address.get("edstate").toString() %>"/>
 		<input type="hidden" name="edzip" value="<%= hmp_address.get("edzip").toString() %>"/>
<%
	}
%>

<div class="rightCal" style="width:45%;color: black;">



<%  
    String error ="";
	if(request.getAttribute("errors") != null) 
	{
	error = (String) request.getAttribute("errors");
	}		
%>
            <div id="errorpage" style="">
              <%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %>
           </div>
								
			<%String result="";
			if(request.getAttribute("page")!=null) {
			result = (String)request.getAttribute("page");
			}
			%>
			<div id="errorpage" style="">
			<%=result.length()>0?"Sucessfully"+result:"" %>
			</div>
			
			
			<table width="100%" border="1" cellspacing="0" cellpadding="0" class="driverChargTab">
			
            
            <tr>
 			<td>
 					<table  id="bodypage"  border="1"  width="100%" style="background-color:#81F7F3;">
 					<table  id="bodypage"  border="1"  width="100%" >
 					<tr>
 					<td colspan="4"  align="center"  style="background-color:#81F7F3;"  ><font size="4" ><b>Open Request</b></font></td>
 					</tr>
 					<%-- <tr>
 					<td>LMKEY</td>
 					<td><input type="text"  id="landmarkKey"    name="landmarkKey"  value="<%= openBO.getLandMarkKey() %>"/></td>
 					<td>User/Lm Zone</td>
 					<td><input type="text"  id="land_userZone"    name="land_userZone"  value="<%= openBO.getLandMarkZone() %>"/></td>
 					</tr>
 					<tr>
 					<td>User From</td>
 					<td><input type="text"  id="userFrom" name="userFrom"  value="<%= openBO.getUserAddressKey() %>"/></td>
 					<td>User To</td>
 					<td><input type="text"  id="userTo"     name="userTo"   value="<%= openBO.getUserAddressKey() %>"/></td>
 					</tr> --%>
 					
 					
 					</table>
 					
 					<!-- <table  id="bodypage"  border="1" bordercolor="yellow" width="100%" > -->
 					<table  id="bodypage"  border="1"  width="100%" >
 					<tr>
 					     
 						<td  class="firstCol"  width="20" >Phone No</td>
 						<td><input type="text"   name="phone"  id="phone" value="<%= openBO.getPhone()  %>" onkeyup="KeyCheck(event,'phone','nameId')" onblur="openDetails(phone,'details')"/></td>
 						<td  width="9"><input type="button" name="btnphone" class="lft" value="CheckPhoneNo" tabindex="2" onclick="loadFromToAddr()"/></td>
 					</tr>
 					
 					<tr style="height:3px">
 						<td  width="20" >Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
 						
 						
 						<%-- <td  width="35"><input type="text" size="20" id="nameId"    name="name"  value="<%= openBO.getName() %>" onkeyup="KeyCheck(event,'phone','shrs')" /></td> --%>
 						
 						<td  width="35"><input type="text" size="20" id="name"    name="name"  value="<%= openBO.getName() %>"/></td>
 						
 						
 						<td  width="20">Don't Dispatch<input  type="checkbox" name="dispatchStatus"  id="dispatchStatus" <%= ((openBO.getDispatchStatus()!=null) && (openBO.getDispatchStatus().equals("true")))?"checked":"" %>   style="vertical-align: middle"  /></td>
 						<td  width="20"align="center">
 						<a id="displayText" style="color: red;" href="javascript:openPayment();">Payment</a>
 						</td>
 						
 					</tr>
 					<tr>
 						<td  class="firstCol"  width="20" colspan="1" style=" size: 10px" >Pickup Date&Time </td>
 						<td  width="25"><input readonly="readonly" type="text" style="width: 80px;height: 15px;"   name="sdate"   id="sdate" value="<%= openBO.getSdate()  %>" />
 						<img src="images/calendar.gif"  onclick="displayCalendar(document.masterForm.sdate,'mm/dd/yyyy',this)"  /> 
 						<input type="text"   id="shrs"   name="shrs"   maxlength="4" style="width: 32px;height: 15px;"  value="<%= openBO.getShrs()   %>"   onkeyup="KeyCheck(event,'nameId','sintersection')"/>
 						<font size="2">Hrs</font></td>						
 						
 						<td align="center">
 					          <select  name="toSms"  id="toSms"  >
							     <option value="no">No Sms</option>							     
							     <option value="tm">Tmobile</option>
							     <option value="ve">Verizon</option>
							     <option value="sp">Sprint</option>
							     <option value="cr">Cricket</option>
							   </select>
 					    
 					    </td>
 					
 					</tr>
 					
 					<!-- <tr>
 					<td></td>
 					<td></td> 
							<td>
							<div id="smsList"  style="display: none" >
							   <select  name="toSms"  id="toSms"  >
							     <option value="0" >Select</option>
							     <option value="tm">Tmobile</option>
							     <option value="ve">Verizon</option>
							     <option value="sp">Sprint</option>
							     <option value="cr">Cricket</option>
							   </select>
							</div>
							</td>							 
						</tr> -->
						</table>
						
						
						</table>
						
						</table>
						
						
						
						<table id="HideText"" border="1"  width="100%" style="display: none" >
						<tr>
						<td align="center">PaymentType</td>
						<td>  
                         <select  name="paytype"  id="paytype"  >
                                 <option value="0">Select</option>
							     <option value="Cash">Cash</option>
							     <option value="CC">CC</option>
							     <option value="VC">VC</option>
							     
					    </select>
                        </td> 		
						<td align="center" >Account :</td>
						<td><input type="text" size="4"  name="acct"  id="acct"/></td>
						<td align="center">Amount:</td>
						<td><input type="text" size="4"   name="amt"  id="amt" onkeyup="amtNumCheck()"/></td>
						</tr>
						
						</table>
						
						
						
						<table border="1"  width="100%" >
						<tr>
						<td width="100%" colspan="4" >
						<table border="0.5"  width="100%" >
						<tr>
 						<!-- <td colspan="4"   align="center"    style="background-color:#FCD374" width="90%" >	<b>From Address</b></td> -->
						<td  width="95%" align="center"    style="background-color:#FCD374" >	<b>From Address</b></td>
						
 						<td width="5%" bgcolor="yellow" id="addressverify"><font  style="font-size:x-small;">Address Verified</font></td>
 						</tr> 	
 					    </table>
 					    </td>
 					   
 					    </tr>
 					    
 						<tr> 						
 						<td  class="firstCol"  width="30">Add1(<font color="red" >F2</font>)</td>
 							<td  width="25">
 							<div class="div2">
 							<div class="div2In">
 							<div class="inputBXWrap wid130">
 							
                             <input type="text"   id="sadd1" name="sadd1"   size="20"   value="<%=openBO.getSadd1()  %>" class="form-autocomplete" onblur="Data()"  onkeyup="KeyCheck(event,'chekcsource','sadd2')" onkeypress="openMapdetails_test('MapDetails',event)" />
                                    
                                    <ajax:autocomplete
				  					fieldId="sadd1"
  									popupId="model-popupsadd1"
  									targetId="sadd1"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress1"
  									className="autocomplete"
  									progressStyle="throbbing" /> 
  			                 </div> 
  			                 </div>
  			                 </div>
  			                        </td>
  			                         
  			                        <td   class="firstCol"   width="20"> Add2</td>
 							        <td  width="20">
 							        <div class="div2">
 							        <div class="div2In">
 							        <div class="inputBXWrap wid130">
                                    <input type="text" name="sadd2"   size="10"  id="sadd2"    value="<%= openBO.getSadd2()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data()"  onkeyup="KeyCheck(event,'sadd1','scity')"/>
									<ajax:autocomplete
  									fieldId="sadd2"
  									popupId="model-popupsadd2"
  									targetId="sadd2"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress2"
  									className="autocomplete"
  									progressStyle="throbbing"/>
		                            </div>
		                            </div>
		                            </div>
		                            </td> 
		                                    
 						</tr>
 						<tr>
 						<td  class="firstCol "   width="20">City</td>
 						<td  width="25">
 						<div class="div2">
 						<div class="div2In">
 						<div class="inputBXWrap wid130">
                                    <input type="text" name="scity"   id="scity"   size="10"   value="<%= openBO.getScity()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data()"   onkeyup="KeyCheck(event,'sadd2','sstate')" onkeypress="openMapdetails_test('MapDetails',event)"/>
									<ajax:autocomplete
  									fieldId="scity"
  									popupId="model-popupscity"
  									targetId="scity"
  									baseUrl="autocomplete.view"
  									paramName="phonecity"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
  						</div> 
  						</div>
  						</div>
  						</td>
  						<td    class="firstCol" width="20">Land Mark (<font color="red" >F3</font>)</td>
  						<td  width="20"> 
 							<div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                 	<%-- <input type="text" name="slandmark"   size="10"  id="slandmark"   value="<%= openBO.getSlandmark()  %>" onkeyup="KeyCheck(event,'sintersection','saircodeid')" onblur="popUp('slandmark')"  /> --%>
                                    <input type="text" name="slandmark"   size="10"  id="slandmark"   value="<%= openBO.getSlandmark()  %>"  onblur="openLanddetails('LandDetails')"  />
                                    
<%--                                    <input type="hidden" id ="landmark" name = "landmark" value="<%= openBO.getSaircode() %>"/>
 --%>                                    <ajax:autocomplete
  									fieldId="slandmark"
  									popupId="model-popup1"
  									targetId="slandmark"
  									baseUrl="autocomplete.view"
  									paramName="landmark"
  									className="autocomplete"
  									progressStyle="throbbing"/>
  							 		</div></div></div>
 							
 							</td>
  						
 						</tr>
 						
 					<tr>
 						
 							<td  class="firstCol"  width="15" >State</td>
 							<td   width="20">
 							
 	                     <%-- <input type="text" name="sstate"   size="10"  id="sstate"   value="<%= openBO.getSstate()  %>"  onblur="Data()"  onkeyup="KeyCheck(event,'scity','szip')" onkeypress="openMapdetails_test('MapDetails',event)"/> --%>			
 							  
 						      <input type="text" name="sstate"   size="3"  id="sstate"   value="<%=openBO.getSstate() %>"  onblur="Data()"  onkeyup="KeyCheck(event,'scity','szip')" onkeypress="openMapdetails_test('MapDetails',event)"/> 
 							  
 							  <%-- <input type="text" name="sstate"   size="10"  id="sstate"   value="<%= openBO.getSstate()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data()"  onkeyup="KeyCheck(event,'scity','szip')" />
									<ajax:autocomplete
  									fieldId="sstate"
  									popupId="model-popupsstate"
  									targetId="sstate" 
  									baseUrl="autocomplete.view"
  									paramName="phonestate"
  									className="autocomplete"
  									progressStyle="throbbing"/> --%>
  									
  									<%-- Zip<input type="text" name="szip"   size="6"  id="szip"   value="<%= openBO.getSzip()  %>"  onblur="openMapdetails('MapDetails')" onkeyup="KeyCheck(event,'sstate','eaircodeid')"  /> --%>
  									
  									Zip(<font color="red" style="font-size:x-small;"  >F8</font>)<input type="text" name="szip"   size="6"  id="szip"   value="<%= openBO.getSzip()  %>"  onkeypress="openMapdetails_test('MapDetails',event)"  />
  									<%-- Zip<input type="text" name="szip"   size="6"  id="szip"   value="<%= openBO.getSzip()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data()" onkeyup="KeyCheck(event,'sstate','eaircodeid')" />
									<ajax:autocomplete
  									fieldId="szip"
  									popupId="model-popupszip"
  									targetId="szip"
  									baseUrl="autocomplete.view"
  									paramName="phonezip"
  									className="autocomplete"
  									progressStyle="throbbing"/> --%>
 							</td> 
 							
 							
 <td  class="firstCol"  width="20" >Intersection (<font color="red" >F4</font>)</td>
 <td  width="25" ><input type="text"    name="sintersection"     size="10"  id="sintersection"   value="<%= openBO.getSintersection()  %>" onkeyup="KeyCheck(event,'shrs','slandmark')"  /></td>
  						 								
 </tr>    
       <tr>
             <td>Arpt-Cde</td>
             <td >
      <!--   <div class="div2"><div class="div2In"><div class="inputBXWrap wid130"> -->
<%--              <input type="text" tabindex="9"  id="saircodeid" name="saircodeid"   value="<%= openBO.getSaircodeid() %>"  onkeyup="KeyCheck(event,'slandmark','chekcsource')"/>
             <input type="hidden" id ="saircode" name = "saircode" value="<%= openBO.getSaircode() %>"/>
 --%>                                    
      <%--  <input type="text" tabindex="9"  size="10" id="saircodeid" name="saircodeid"   value="<%= openBO.getSaircodeid() %>" autocomplete="off"   class="form-autocomplete"  onkeyup="KeyCheck(event,'slandmark','chekcsource')"/> --%>
                                    <%-- <ajax:autocomplete
  									fieldId="saircodeid"
  									popupId="model-popup1"
  									targetId="saircode"
  									baseUrl="autocomplete.view"
  									paramName="aircode"
  									className="autocomplete"
  									progressStyle="throbbing"/>
  							 		</div></div></div> --%>
  			</td> 
  							 		
  						   <td width="70%" align="center">
 						   <a id="displayTable"  style="border-width: 20px; color: red; text-align: center;" href="javascript:openSplReq();">Special Request</a>
 						   </td> 
 						   
 						   <td width="50%" align="center">
 						   <!-- <a id="displayTable"  style="border-width: 20px; color: green; text-align: center;" href="javascript:openSplReq();">Long/Lat</a> -->
 						   <table style=" height: 5px;">
 							       <tr> 
						           <td align="left"><font size="1" color="green">La:</font></td>
						           <%-- <td><%= openBO.getSlat() %></td> --%>
						           <%-- <td><input type="text" size="6" style="width: 100px;height: 10px" name="lat"  id="lat"  value="<%= openBO.getSlat() %>"/></td> --%>
						           <td><input type="text" style="width: 50px;height: 8px;font-size:xx-small; color:red; border-color: white;size: 1px; " name="sLatitude"  id="sLatitude"  value="<%=openBO.getSlat()==null?"":openBO.getSlat() %>"/></td>
						           <!-- </tr>
						           <tr>  -->
 							       <td align="left" ><font size="1" color="green">Lo:</font></td>
					              
					               <td><input type="text"  style="width: 50px;height: 8px; font-size:xx-small;color:red; border-color:white;size: 1px;" name="sLongitude"  id="sLongitude"  value="<%=openBO.getSlong()==null?"":openBO.getSlong() %>" />
 							       </td>
 							     
 							       </tr>
 						  </table>
 						   </td>		
                         </tr> 
 					</table>
 					
 					<table id="hideTable" border="1"  width="100%" style="display: none">
 				
 				        <%if(al_drList!=null && !al_drList.isEmpty()) {%>
 						
 						    <tr>  
 							<td>Special Request</td>
 							   <td>
 								<table width="250">
 								 <TR>
 									<TD>Driver</TD>
 										<TD>
 										<table> 
 											
												<th><td><input type="hidden" name="drprofileSize" value="<%=al_drList.size()/2 %>"/></td> </th>
												<%for(int i=0,j=0;i<al_drList.size();i=i+2,j++){ %>
													<!-- <tr> -->
														<td>
															<input type="checkbox"
																<%
																	 if(request.getParameter("dchk"+j)!=null)
																	 {
																		 out.print("checked");
																	 } else {
																		 if(openBO.getAl_drList()!=null)
																		 {
																			 for(int k=0;k<openBO.getAl_drList().size();k++)
																			 {
																				 if(openBO.getAl_drList().get(k).toString().equals(al_drList.get(i+1)))
																				 {
																					 out.print("checked");
																					 break;
																				 }
																			 }
																		 }
																	 }
																
																%>
															 name="dchk<%=j %>" value="<%=al_drList.get(i+1) %>"/><%=al_drList.get(i) %>
														</td>							
													<!-- </tr> -->
												<%} %>
											<%} %>
											</table>
 										</TD>
 										
 									  <!-- </TR> -->
 									
 									
 									</TR>
 									
 									
 									<TR>
 									<TD>Vehicle</TD>
 									<TD>
 											<table>
 											<%if(al_veList!=null && !al_veList.isEmpty()) {%>
												<th><td><input type="hidden" name="vprofileSize" value="<%=al_veList.size()/2 %>"/></td> </th>
												<%for(int i=0,j=0;i<al_veList.size();i=i+2,j++){ %>
													<tr>
														<td>
															<input type="checkbox"
																<%
																	 if(request.getParameter("vchk"+j)!=null)
																	 {
																		 out.print("checked");
																	 } else {
																		 if(openBO.getAl_vecList()!=null)
																		 {
																			 for(int k=0;k<openBO.getAl_vecList().size();k++)
																			 {
																				 if(openBO.getAl_vecList().get(k).toString().equals(al_veList.get(i+1)))
																				 {
																					 out.print("checked");
																					 break;
																				 }
																			 }
																		 }
																	 }
																
																%>
															 name="vchk<%=j %>" value="<%=al_veList.get(i+1) %>"/><%=al_veList.get(i) %>
														</td>							
													</tr>
												<%} %>
											<%} %>
											</table>
 										</TD>
 									
 									
 									</TR>
 									
 								 </table> 
 							</td>
 					   <!-- <td>
 							     <table>
 							      <tr> 
 							      <td align="center" >Longitude:</td>
						          <td><input type="text" size="6"  name="long"  id="long"/></td>
						           </tr>
						           <tr>
						           <td align="center">Latitude:</td>
						           <td><input type="text" size="6"   name="lat"  id="lat"/></td>
 							      </tr>
 							      </table>
 					   </td> -->
 							
 						</tr>
 					
 					</table>
 					
 		 <table border="1"  width="100%" >
 				
 				<tr>
 				
 				<td  width="20">Special Instruction</td>
 				<td  width="25" colspan="3" ><textarea rows="2" cols="45"   name="specialins"  id="specialins" ></textarea> 
 				</td>			
 				
                  
				<td align="center">
                <input  type="submit" name="submit" class="lft" value="Submit"   id="submit"  onkeyup="KeyCheck(event,'checktoadd','submit')" />
				</td>
			  </tr>
			    
 					<table>
 					
 					
 					
 					<table border="1"  width="100%" >
 					<tr>
 						<td colspan="4"    align="center"   style="background-color:#FCD374"  ><b>To Address</b></td>
 					</tr>
 					 <tr>
 						
 						<td class="firstCol"  width="20">Add 1</td>
 							<td  width="25"><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text"   size="10"  name="eadd1"   id="eadd1" value="<%= openBO.getEadd1()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data1()"  onkeyup="KeyCheck(event,'destination','eadd2')"/>
									<ajax:autocomplete
  									fieldId="eadd1"
  									popupId="model-popupeadd1"
  									targetId="eadd1"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress1"
  									className="autocomplete"
  									progressStyle="throbbing"/>
                                    </div></div></div></td>
                                    <td class="firstCol"  width="20">Add 2</td>
 							<td  width="25"><div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="eadd2"   size="10"  id="eadd2"    value="<%= openBO.getEadd2()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data1()"  onkeyup="KeyCheck(event,'eadd1','ecity')"/>
									<ajax:autocomplete
  									fieldId="eadd2"
  									popupId="model-popupeadd2"
  									targetId="eadd2"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress2"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div></td> 
 					</tr>
 					<tr>	
		                             
 							
                            <td  width="20">City</td>
 							<td  width="20" height="5px">
 							
 							       <!-- <div class="div2"><div class="div2In"><div class="inputBXWrap wid130"> -->
                                    <input type="text" name="ecity"    size="8"  id="ecity"   value="<%= openBO.getEcity()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data1()" onkeyup="KeyCheck(event,'eadd2','estate')" />
									<%-- <ajax:autocomplete
  									fieldId="ecity"
  									popupId="model-popupecity"
  									targetId="ecity"
  									baseUrl="autocomplete.view"
  									paramName="phonecity"
  									className="autocomplete"
  									progressStyle="throbbing" /> --%>
  									
                                    <!-- </div></div></div> -->
                                    
                                    </td>
                                    
                                    <td width="20" >Arpt-Cde</td>
 							<td  width="25" height="5px">
 								 <!-- <div class="div2"><div class="div2In"><div class="inputBXWrap wid130"> -->
 								 
<%--                                     <input type="text" tabindex="16"   size="10" id="eaircodeid" name="eaircodeid" class="inputBX" value="<%= openBO.getEaircodeid() %>" autocomplete="off"   class="form-autocomplete"     onkeyup="KeyCheck(event,'szip','destination')"/>
									<input type="hidden" id ="eaircode" name = "eaircode" value="<%= openBO.getEaircode() %>"/>
 --%>									<%-- <ajax:autocomplete
  									fieldId="eaircodeid"
  									popupId="model-popup2"
  									targetId="eaircode"
  									baseUrl="autocomplete.view"
  									paramName="aircode"     
  									className="autocomplete"
  									progressStyle="throbbing" />
									</div></div></div>
									<br></br> --%>
									<!-- <div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btneaircode"   id="destination" class="lft" value="ChkDstinArcde" tabindex="17"  onkeyup="KeyCheck(event,'eaircodeid','eadd1')"  />
                                        </div>
                                        </div>
                                    </div> -->
 							</td>
 						 
 						</tr>
 				 <tr> 
 				 <td class="firstCol"  width="20">State</td>
 				 <td  width="20"> 
 							<input type="text" name="estate"   size="3"   id="estate"   value="<%= openBO.getEstate()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data1()"  onkeyup="KeyCheck(event,'ecity','ezip')" />
									<ajax:autocomplete
  									fieldId="estate"
  									popupId="model-popupestate"
  									targetId="estate"
  									baseUrl="autocomplete.view"
  									paramName="phonestate"
  									className="autocomplete"
  									progressStyle="throbbing" />Zip
  									<input type="text" name="ezip"  size="6" id="ezip" tabindex="22" class="inputBX" value="<%=openBO.getEzip()  %>" autocomplete="off"   class="form-autocomplete" onblur="Data1()"  onkeyup="KeyCheck(event,'estate','btnfromaddress')" />
									<ajax:autocomplete
  									fieldId="ezip"
  									popupId="model-popupezip"
  									targetId="ezip"
  									baseUrl="autocomplete.view"
  									paramName="phonezip"
  									className="autocomplete"
  									progressStyle="throbbing" /> 
 							 </td> 
 							  <td  align="center" width="15">
 							  <input type="submit" name="btntoaddress"   id="checktoadd"  size="5" value="ChkToAddress"      onkeyup="KeyCheck(event,'btnfromaddress','submit')" />
 							 
 							 </td>
 							 			  
 				</tr> 
 				<!-- <tr>
 						
 						<td>
 						  <div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btnfromaddress"  id="btnfromaddress"   class="lft" value="ChkFromAddress"   onkeyup="KeyCheck(event,'ezip','checktoadd')"  />
                                         </div>
                                        </div>
                                    </div>
 						</td>
 						<td></td> 
                                    		<td><div class="div3">
                                    	<div class="btnBlueNew1 padT10 ">
                                         <div class="rht">
                                         <input type="submit" name="btntoaddress"   id="checktoadd"  class="lft" value="ChkToAddress"      onkeyup="KeyCheck(event,'btnfromaddress','submit')" />
                                         </div>
                                    </div></div></td>
 						
 					  </tr>	 -->
 					</table>
 					
 					
 					
			
			<% if(session.getAttribute("user") != null) {  %>
							
							<%
							ArrayList al_q = (ArrayList)request.getAttribute("al_q");
							%>
				<table border="1"  width="100%" >
				
				<tr>
                           <td  class="firstCol"   width="15%">Zone</td>
                           
                           <td width="70%" >
                           <!-- <div class="div5">
                           <div class="div5In">
 						   <div id="queueid"> -->
 							
	 					  <select name='queueno' id='queueno'     onkeyup="KeyCheck(event,'szip','eaircodeid')" >
	 								<option value=''>Select</option>
									<%
										if(al_q!=null){
										for(int i=0;i<al_q.size();i=i+2){ 
									%>
								    <option value='<%=al_q.get(i).toString() %>' <%=openBO.getQueueno().equals(al_q.get(i).toString())?"selected":"" %>><%=al_q.get(i+1).toString() %></option>
									<%
									}
									} %> 
									</select>
									<!-- </div>
									</div>									
									</div>  -->
									<% } else{  %> 
									
									
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap">
                                    <input type="hidden" name="queueno" id="queueno" value="<%=openBO.getQueueno() %>"/></div></div></div>
                                    <%} %>
                                    <br>
                                    <div class="div3">
									<div class="btnBlueNew1">
									<td>
                                    <div class="rht">
                                         <input type="button" id="getzone" name="getzone" class="lft" value="Get Zone" tabindex="" onclick="openMapdetails('MapDetails')" />
                                         
                                    </div>
                                   </td>
                                    </div>
                                    </div>
                                    </td>
			         </tr>
			       </table>
			<!-- </tr> -->
			
			<tr>
		<td>
		<div id='styled_popup' name='styled_popup' style='width: 380px; height: 300px;border : 1px solid red;display:none; position: absolute; top: 350px; left: 350px; zoom: 1'>
			<table cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td>
						 <img height='100' width='150' src='images/progress-bar.gif'/> 
					</td> 
				</tr>
		  </table>
		</div>
		</td>
	</tr> 
	
    </table>  
		</div>
		
		

          <div class="clrBth"></div>
                
                
            <div class="footerDV" style="height:10%;width:45%;">Copyright &copy; 2010 Get A Cab</div>
            
            <!-- <div class="footerTopDV" style="height:25%; width:40%; margin-top: -550px; margin-left:590px;"> -->
            
            <div class="footerTopDV" id="detailsAutoPopulated1" style="height:25%; width:40%; margin-top: -650px; margin-left:590px;">
            
                 <table width='50%'>
		 						<tr>
 							    <td valign="top" >
 								<div   id="details" overflow:auto' > </div>
 							     <div id="detailsAutoPopulated" overflow:auto'>
 							     <jsp:include page="/jsp/OpenRequestOne.jsp" />
 							     </div>
 							     
 								
 							</td>
 						</tr>
 						</br>
 						</br>
 						<tr>
 							<td valign="top" >
 
 							</td>
 						</tr>
		</table>
		</div>
		
		<!-- 13-05-11 Begin-->
		
		<div id="LandMarkDetails1" style="height:50%; width:45%; margin-top: 30px; margin-left:590px; ">
		<!-- <div class="TopDVforLand" style="height:50%; width:50%; margin-top: 30px; margin-left:590px; "> -->
            
            <table width='50%'>
		 		<tr>
 			         <td valign="top" >
 				     <div 	style=""  id="LandDetails" overflow:auto'> </div>	
 				     <div 	style=""id="LandMarkDetails" overflow:auto'>
 				     <jsp:include page="/jsp/landmark.jsp" />
 				     </div>
 				     	
 				     </td>
 				     </tr>
 				     </br>
 				     </br>
 				<tr>
 				     <td valign="top"> 
 				     </td>
 	     		</tr>
		   </table>
		</div>
		<!-- 13-05-11 End-->
		
		<!-- 20-05-11 begin -->
		<div class="LastDVforMap" id="MapDetails1" style="height:50%; width:50%; margin-top: 30px; margin-left:590px; ">
		
		<!-- <div class="TopDVforLand" style="height:50%; width:50%; margin-top: 30px; margin-left:590px; "> -->
            
            <table width='92%'>
		 		<tr>
 			         <td valign="top" >
 				     <div style="border:"";"  id="MapDetails" overflow:auto'> </div>
 				     	
 				     <%-- <div id="detailsAutoPopulated" overflow:auto'>
 							     <jsp:include page="/jsp/mapdetails.jsp" /> 
 					      </div>--%>
 				     
 				     </td>
 				    
 				     </tr>
 				     </br>
 				     </br>
 				<tr>
 				     <td valign="top"> 
 				     </td>
 	     		</tr>
		   </table>
		</div>
		
		
		
		     
		     
		<!-- 20-05-11 end -->
		
		
		
		
		
		
		
		</div>   
</form>
</body>
</html>



	