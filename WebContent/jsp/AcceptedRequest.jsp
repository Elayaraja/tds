<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap;"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS (Taxi Dispatch System)</title>
</head>
<body>
<form name="masterForm" action="control" method="post">
<jsp:useBean id="openRequestBO"   class="com.tds.tdsBO.OpenRequestBO" scope="request"  />
<jsp:setProperty name="openRequestBO"  property="*"/>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.getAcceptedandOpenRequest%>">

<table id="pagebox" cellspacing="0" cellpadding="0" >
	<tr>
		<td>
<table id="bodypage" width="900">	
	<tr>
		<td colspan="8" align="center">		
			<div id="title">
				<h2>Accepted&nbsp;Request</h2>
			</div>
		</td>		
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td colspan="8" align="center">Enter&nbsp;Phone&nbsp;No :&nbsp;<input type="text"  name="phone" value="<%= openRequestBO.getPhone() %>" />&nbsp;<input type="submit" name="getrecord" value="GetRecord"/></td>
		
	</tr>
	<tr><td>&nbsp;</td></tr>
	<%if(request.getAttribute("AcceptedData")!=null){
		
		ArrayList al_data1 =new ArrayList();
		HashMap hs_data=(HashMap)request.getAttribute("AcceptedData");
		int h_count=0;
		int flag=0;
		while(h_count < hs_data.size()){
			h_count++;
			ArrayList al_data =new ArrayList();
			if(flag==0){
				al_data=(ArrayList)hs_data.get("acceptedrequest");
				%>
				<tr bgcolor="#d3d3d3">
					<td colspan="8" align="center">Accepted&nbsp;Request<br></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<%
			}
			else{
				al_data=(ArrayList)hs_data.get("openrequest");
				%>
				<tr bgcolor="#d3d3d3">
					<td colspan="8" align="center">Open&nbsp;Request</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<%
			}
		
		for(int count=0;count< al_data.size();count++){
			flag=1;
			
			openRequestBO=(OpenRequestBO)al_data.get(count);
			%>	
	<tr>
		<td>
			
			<table border="1" bordercolor="#483D8B" cellpadding="0" cellspacing="0" width="600" align="center">
				<tr>
					<td colspan="2">Trip&nbsp;ID</td>
					<td colspan="2"><input type="text" name="tripid"  value="<%= openRequestBO.getTripid()  %>"/></td>
					<td colspan="2">Driver&nbsp;ID</td>
					<td colspan="2"><input type="text" name="driverid"  value="<%= openRequestBO.getDriverid() %>"/></td>
				</tr>
				<tr>
					<td colspan="2">Service&nbsp;Time</td>
					<td colspan="2"><input type="text" name="stime" readonly="readonly"  value="<%= openRequestBO.getSttime() %>"/></td>
					<td colspan="2">Service&nbsp;Date</td>
					<td colspan="2"><input type="text" name="sdate" readonly="readonly"   value="<%= openRequestBO.getSdate() %>"/></td>
				</tr>
				<tr bgcolor="#d3d3d3">
					<td colspan="4" align="center">From&nbsp;Address</td>
					<td colspan="4" align="center">To&nbsp;Address</td>
				</tr>
		<tr>
		<td colspan="2">Address&nbsp;1</td>
		<td colspan="2">
			<input type="text"   name="sadd1" value="<%= openRequestBO.getSadd1()  %>"  onblur="document.masterForm.sadd2.focus">
		</td>
		<td colspan="2">Address&nbsp;1</td>
		<td colspan="2">
			<input type="text"  name="eadd1" value="<%= openRequestBO.getEadd1()  %>" >
		</td>
	</tr>
	<tr>
		<td colspan="2">Address&nbsp;2</td>
		<td colspan="2">
			<input type="text" name="sadd2"   value="<%= openRequestBO.getSadd2()  %>">
		</td>
		<td colspan="2">Address&nbsp;2</td>
		<td colspan="2">
			<input type="text" name="eadd2"   value="<%= openRequestBO.getEadd2()  %>">
		</td>
	</tr>
	<tr>
		<td colspan="2">State</td>
		<td colspan="2">
			<input type="text" name="sstate"  value="<%= openRequestBO.getSstate()  %>">
		</td>
		<td colspan="2">State</td>
		<td colspan="2">
			<input type="text" name="estate"  value="<%= openRequestBO.getEstate()  %>">
		</td>
	</tr>
	<tr>
		<td colspan="2">City</td>
		<td colspan="2">
			<input type="text" name="scity"  value="<%= openRequestBO.getScity()  %>" >
		</td>
		<td colspan="2">City</td>
		<td colspan="2">
			<input type="text" name="ecity"  value="<%= openRequestBO.getEcity()  %>" >
		</td>
	</tr>
	<tr>
		<td colspan="2">Zip</td>
		<td colspan="2">
			<input type="text" name="szip"  value="<%= openRequestBO.getSzip()  %>" >
		</td>
		<td colspan="2">Zip</td>
		<td colspan="2">
			<input type="text" name="ezip"  value="<%= openRequestBO.getEzip()  %>" >
		</td>
	</tr>
	<% if(openRequestBO.getDriverid() != "" ) { %>
	<tr>
		<td colspan="2">Latitude</td>
		<td colspan="2">
			<input type="text" name="stlatitude"  value="<%= openRequestBO.getSlat()  %>" >
		</td>
		<td colspan="2">Latitude</td>
		<td colspan="2">
			<input type="text" name="edlatitude"  value="<%= openRequestBO.getEdlatitude()  %>" >
		</td>
	</tr>
	<tr>
		<td colspan="2">Longitude</td>
		<td colspan="2">
			<input type="text" name="stlongitude"  value="<%= openRequestBO.getSlong()  %>" >
		</td>
		<td colspan="2">Longitude</td>
		<td colspan="2">
			<input type="text" name="edlongitude"  value="<%= openRequestBO.getEdlongitude()  %>" >
		</td>
	</tr>
	<% } %>
<%-- 	<tr>
		<td colspan="2">Airport&nbsp;Code</td>
		<td colspan="2">
			<input type="text" name="saircode"  value="<%= openRequestBO.getSaircode()  %>" >
		</td>
		<td colspan="2">Airport&nbsp;Code</td>
		<td colspan="2">
			<input type="text" name="eaircode"  value="<%= openRequestBO.getEaircode()  %>" >
		</td>
	</tr>
 --%>	
	
	<tr align="center" >
		<td colspan="8">
			<input type="submit" value="Update"  name="update">
			<input type="submit" value="Delete"  name="delete">
		</td>
	</tr>		
	</table>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<%}}
		}	
	%>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>