<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html> --%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%!int startValue = 1;
	double limit = 0;
	int pstart = 1;
	int pend = 0;
	int pageNo = 1;
	int plength = 0;%>
<%
	if (request.getAttribute("startValue") != null) {
		startValue = Integer.parseInt((String) request
				.getAttribute("startValue"));
		startValue = startValue + 1;
	}
	if (request.getAttribute("plength") != null) {
		plength = Integer.parseInt((String) request
				.getAttribute("plength"));
	}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
function deleteOperator(driverNumber,x,userType){
 	var driver=driverNumber;
	var confirmation=confirm("Records on  "+driver+" going to delete");
	if (confirmation==true){
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	if(userType=="Operator")
	url='RegistrationAjax?event=deleteDriver&module=operationView&driverNum='+driver+'&driverOrOperator='+1;
	else
	url='RegistrationAjax?event=deleteDriver&module=operationView&driverNum='+driver+'&driverOrOperator='+0;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="1"){ 
		var table=document.getElementById("operatorSummary");
		table.deleteRow(x.parentNode.parentNode.rowIndex);
	 }
 } 
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
	<title>TDS (Taxi Dispatch System)</title>
</head>

<body>
	<form action="control" method="get" name="adminsummary" onsubmit="return showProcess()">
	<input type="hidden" name="action" value="userrolerequest" /> 
	<input type="hidden" name="event" value="getUserList" /> 
	<input type="hidden" name="subevent" value="access" />
	<input type="hidden" name="module" id="module" value="<%=request.getParameter("module") == null ? "" : request.getParameter("module")%>" />
	<c class="nav-header"><center>View/Change User Permission</center></c>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
		<tr>
			<td>User Name</td>
			<td><input type="text" name="user_id" size="12" id="user_id" onfocus="appendAssocode('<%=((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode()%>',id)"
					onblur="caldid(id,'drivername','dName')" class="form-autocomplete" onkeypress="autocom()" autocomplete="off" /> <input type="hidden" name="dName" id="dName" 
					value="<%=request.getParameter("dName") == null ? "" : request.getParameter("dName")%>" />
				<ajax:autocomplete fieldId="user_id" popupId="model-popup1" targetId="user_id" baseUrl="autocomplete.view" paramName="employeeNames" className="autocomplete" progressStyle="throbbing" />
			</td>
			<td>User Type</td>
			<td><input type="text" name="usertype" /></td>
			</tr>
			<tr>
			<td>Based On</td>
			<td>
			<input type="checkbox" name="drivers" value="1">Driver</input>
			<input type="checkbox" name="nonDrivers" value="2">Non-Drivers</input>
			</td>
			</tr>
			<tr>
				<td align="center" colspan="4"><input type="submit"
					name="button" value="search" /></td>
			</tr>
	</table>
					<%
						AdminRegistrationBO adminbo = new AdminRegistrationBO();
						ArrayList al_driverData = new ArrayList();
						if (request.getAttribute("Adminsummary") != null) {%>
					<table width="80%"  id="operatorSummary" border="0" cellspacing="0" cellpadding="0" class="thumbnail" align="center">
					<tr>
						<th width="10%" class="firstCol">User Name</th>
						<th width="10%" class="firstCol">Name</th>
						<th width="10%">User Type</th>
						<th width="10%">Status</th>
						<th width="10%">Delete</th>
					</tr>
					<%	
							al_driverData = (ArrayList) request.getAttribute("Adminsummary");
							for (int r_count = 0; r_count < al_driverData.size(); r_count++) {
					%>
					<%
						if (r_count % 2 == 0) {
					%>
					<tr align="center">
					<td>&nbsp;<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUname()%>&nbsp;</td>
					<td>&nbsp;<%=((AdminRegistrationBO) al_driverData.get(r_count)).getFname()%>&nbsp;<%=((AdminRegistrationBO) al_driverData.get(r_count)).getLname()%></td>
					<td>&nbsp;<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUsertypeDesc()%>&nbsp;</td>
					<td>
					<%
						if (Integer.parseInt((((AdminRegistrationBO) al_driverData.get(r_count)).getActive())) == 1) {
					%>
					<input type="button" value="Change Permission" class="lft" onclick='window.location.href="control?action=userrolerequest&module=systemsetupView&event=getUserList&userid=<%=((AdminRegistrationBO) al_driverData
						.get(r_count)).getUid()%>&uname=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUname()%>&utype=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUsertype()%>&userDesc=<%=((AdminRegistrationBO) al_driverData
						.get(r_count)).getUsertypeDesc()%>&status=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getActive()%>";'>
					<%
						} else {
					%> 
					<input type="button" value="In Active" class="lft" onclick='window.location.href="control?action=userrolerequest&module=systemsetupView&event=getUserList&userid=<%=((AdminRegistrationBO) al_driverData
						.get(r_count)).getUid()%>&uname=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUname()%>&utype=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUsertype()%>&userDesc=<%=((AdminRegistrationBO) al_driverData
						.get(r_count)).getUsertypeDesc()%>&status=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getActive()%>";'>
					<%
						}
					%>
					</td>
                       	<td >&nbsp;<input type="button" name="imgDeleteOperator" id="imgDeleteOperator" value="Delete" onclick="showProcess();deleteOperator('<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUid()%>',this,'<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUsertypeDesc()%>')"></input></td>
					</tr>
					<%
						} else {
					%>
					<tr align="center">
						<td style="background-color: lightgreen">&nbsp;<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUname()%>&nbsp;</td>
						<td  style="background-color:lightgreen">&nbsp;<%=((AdminRegistrationBO) al_driverData.get(r_count)).getFname()%>&nbsp;<%=((AdminRegistrationBO) al_driverData.get(r_count)).getLname()%></td>
						<td style="background-color: lightgreen">&nbsp;<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUsertypeDesc()%>&nbsp;</td>
						<td style="background-color: lightgreen" valign="top">
					<%
						if (Integer.parseInt((((AdminRegistrationBO) al_driverData.get(r_count)).getActive())) == 1) {
					%>
					<input type="button" value="Change Permission" class="lft" onclick='showProcess();window.location.href="control?action=userrolerequest&event=getUserList&module=systemsetupView&userid=<%=((AdminRegistrationBO) al_driverData
						.get(r_count)).getUid()%>&uname=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUname()%>&utype=<%=((AdminRegistrationBO) al_driverData
						.get(r_count)).getUsertype()%>&userDesc=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUsertypeDesc()%>&status=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getActive()%>";'>
					<%
						} else {
					%> 
					<input type="button" value="In Active" class="lft" onclick='showProcess();window.location.href="control?action=userrolerequest&event=getUserList&module=systemsetupView&userid=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUid()%>&uname=<%=((AdminRegistrationBO) al_driverData
						.get(r_count)).getUname()%>&utype=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUsertype()%>&userDesc=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUsertypeDesc()%>&status=<%=((AdminRegistrationBO) al_driverData.get(r_count)).getActive()%>";'>
					<%
						}
					%>
					</td>
	             	<td style="background-color:lightgreen">&nbsp;<input type="button" name="imgDeleteOperator" id="imgDeleteOperator" value="Delete" onclick="showProcess();deleteOperator('<%=((AdminRegistrationBO) al_driverData.get(r_count)).getUid()%>',this)"></input></td>
					</tr>
					<%
						}
					%>
					<%
						}
					%>
					<tr>
						<td colspan="3">
							<table align="center">
								<tr align="center">
									<%
										if (request.getAttribute("pstart") != null) {
												pstart = Integer.parseInt((String) request
														.getAttribute("pstart"));
											}
											if (request.getAttribute("pend") != null) {
												pend = Integer.parseInt((String) request
														.getAttribute("pend"));
											}
											if (request.getAttribute("limit") != null) {
												limit = Double.parseDouble((String) request
														.getAttribute("limit"));
											}
											int start = 0;
											int end = 0;
											if (request.getAttribute("DRBO") != null) {
												adminbo = (AdminRegistrationBO) request
														.getAttribute("DRBO");
											}
											start = Integer.parseInt(adminbo.getStart()) + 1;
											end = Integer.parseInt(adminbo.getEnd());

											if (limit <= plength) {
												for (int i = 1; i <=limit; i++) {
									%>
									<td><a
										href="/TDS/control?action=userrolerequest&event=getUserList&module=systemsetupView&subevent=access&pageNo=<%=i%>&pstart=<%="" + pstart%>&pend=<%="" + pend%>&first=no" onclick="showProcess();"><%=i%></a>
									</td>
									<%
										}
											}else if (limit >= plength) {
												if (pstart != 1) {
									%>
									<td><a
										href="/TDS/control?action=userrolerequest&event=getUserList&subevent=access&module=systemsetupView&previous=<%="" + pstart%>&first=no" onclick="showProcess();">&lt;</a>
									</td>
									<%
										}
												for (int i = pstart; i < pend; i++) {
									%>
									<td><a
										href="/TDS/control?action=userrolerequest&event=getUserList&module=systemsetupView&subevent=access&pageNo=<%=i%>&pstart=<%="" + pstart%>&pend=<%="" + pend%>&first=no" onclick="showProcess();"><%=i%></a>
									</td>
									<%
										}
												if (pend <= limit) {
									%>
									<td><a
										href="/TDS/control?action=userrolerequest&event=getUserList&module=systemsetupView&subevent=access&next=<%=pend + ""%>&first=no" onclick="showProcess();">&gt;</a>
									</td>
									<%
										}
											}
									%>
								</tr>
							</table></td>
					</tr>
					</table>
					<%
						}
					%>
				
		<div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
	</form>
</body>
</html>
