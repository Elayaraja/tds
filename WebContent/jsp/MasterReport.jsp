<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
    
<!DOCTYPE HTML>
<html>
<head>
 <link href='https://ajax.googleapis.com/ajax/libs/dojo/1.7.4/dijit/themes/claro/claro.css' rel='stylesheet' type='text/css'/>
<script>dojoConfig = {parseOnLoad: true};</script>
 <script src="https://ajax.googleapis.com/ajax/libs/dojo/1.8.2/dojo/dojo.js"></script> 
 <script>
 dojo.require("dijit.form.Button");
 dojo.require("dijit.Dialog");
 dojo.require("dijit.form.TextBox");
 dojo.require("dijit.form.DateTextBox");
 dojo.require("dijit.form.TimeTextBox");
 function checkData(){
     var data = formDlg.get('value');
     console.log(data);
     if(data.sdate > data.edate){
         alert("Start date must be before end date");
         return false;
     }else{
         return true;
     }
 }</script>
<style type="text/css">
.niceButton {
    -webkit-box-shadow: rgba(0, 0, 0, 0.0976562) 0px 1px 0px 0px;
    background-color: #EEE;
    border: 1px solid #999;
    color: #666;
    font-family: 'Lucida Grande', Tahoma, Verdana, Arial, Sans-serif;
    font-weight: bold;
    padding: 2px 6px;
    height: 28px;z
}
table {
border-collapse: collapse;
border-spacing: 0;
font-family: Futura, Arial, sans-serif;
background-color: #EFEFFB ;
}
caption { font-size: larger; margin: 1em auto; }
th, td { padding: .75em;
overflow: auto; }
th {
background: linear-gradient(#ccc,##777);
color: #fff;
}
th:first-child { border-radius: 9px 0 0 0; }
th:last-child { border-radius: 0 9px 0 0; }
tr:last-child td:first-child { border-radius: 0 0 0 9px; }
tr:last-child td:last-child { border-radius: 0 0 9px 0; }
tr:nth-child(odd) { background: #ccc; }
.total a:HOVER{
	background-color: white;
}
.total{
	width: 100%;
}
</Style>
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/Report.js?sv=3"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<title>TDS Reports</title></head>
<body class="claro"  >
<table class="total">
 	<tr align="left" ><th > Category </th><th>Reports </th><th align="right">Input Parameters</th></tr>
 	<tr>
 	<td  class="cat" style="overflow: auto" width="20%">
 	<a onclick="category('all')"style="cursor:pointer;">All Reports</a><br/>
 	<a onclick="category('1')"style="cursor: pointer;">1.Dispatch</a><br/>
 	<a onclick="category('2')"style="cursor: pointer;">2.Operations</a><br/>
 	<a onclick="category('3')"style="cursor: pointer;">3.Finance</a><br/>
 	<a onclick="category('4')"style="cursor: pointer;">4.System Setup</a><br>
 	<a onclick="category('5')"style="cursor: pointer;">5.Driver Details</a></td>
 	<td class="typ" width="30%">
 	<div id="ReportTypes">
 	 <a onclick="showReports();reportNo('1')" style="cursor: pointer;">1.Daily Dispatch Performance</a><br>
    <a onclick="showOneDate();reportNo('2')" style="cursor: pointer;">2.Daily Job Count</a><br>
    <a onclick="showOneDate();reportNo('3')" style="cursor: pointer;">3.Daily Job Count by Operator</a><br>
    <a onclick="showReports();reportNo('4')" style="cursor: pointer;">4.Monthly Consolidated Statistics Graph</a><br>
    <a onclick="showReports();reportNo('5')" style="cursor: pointer;">5.Monthly Job Count Graph</a><br>
    <a onclick="showReports();reportNo('6')" style="cursor: pointer;">6.Monthly Job Counts</a><br>
    <a onclick="showTrip();reportNo('7')" style="cursor: pointer;">7.Trip Details</a><br>
    <a onclick="showReports();showSplFlags();reportNo('8')" style="cursor: pointer;">8.Job statistics by special Flags</a><br>
    <a onclick="showOneDate();reportNo('9')" style="cursor: pointer;">9.Driver Trip & Shift Details</a><br>
    <a onclick="showReports();showVoucher();reportNo('10')" style="cursor: pointer;">10.Reserved Jobs by AccountNo</a><br>
    <a onclick="showReports();reportNo('11')" style="cursor: pointer;">11.Invoice Details</a><br>
    <a onclick="showReports();reportNo('12')" style="cursor: pointer;">12.Regulatory Trip Reports</a><br>
    <a onclick="showReports();reportNo('13')" style="cursor: pointer;">13.Driver Disbursement Details</a><br>
    <a onclick="showOneDate();reportNo('14')" style="cursor: pointer;">14.Dispatcher Shift Details and performance of the Day</a><br>
    <a onclick="showOneDate();reportNo('15')" style="cursor: pointer;">15.Dispatch Performance By City</a><br>
    <a onclick="showReports();reportNo('16')" style="cursor: pointer;">16.Driver Deactivation Summary</a><br>
    <a onclick="showOneDate();reportNo('17')" style="cursor: pointer;">17.Operator to Driver Total Jobs Per Day</a><br>
    <a onclick="showReports();reportNo('18')" style="cursor: pointer;">18.Manually Changed Trip</a><br>
    <a onclick="showReports();reportNo('19')" style="cursor: pointer;">19.Driver Shifts</a><br>
    <a onclick="showReports();reportNo('20')" style="cursor: pointer;">20.Driver Jobs</a><br>
    <a onclick="showReports();reportNo('21')" style="cursor: pointer;">21.Driver Logout Report</a><br>
    <a onclick="showReports();reportNo('22')" style="cursor: pointer;">22.Job Logs</a><br>
  
 
 </div>
 <div id="reportOne" style="display:none">
  	<a onclick="showReports();reportNo('1')" style="cursor: pointer;">1.Daily Dispatch Performance</a><br>
    <a onclick="showOneDate();reportNo('2')" style="cursor: pointer;">2.Daily Job Count</a><br>
    <a onclick="showTrip();reportNo('7')" style="cursor: pointer;">4.Trip Details</a><br>
    <a onclick="showReports();reportNo('18')" style="cursor: pointer;">18.Manually Changed Trip</a><br>
  </div>
 <div id="reportTwo" style="display:none" >
    <a onclick="showOneDate();reportNo('3')" style="cursor: pointer;">1.Daily Job Count by Operator</a><br>
    <a onclick="showReports();reportNo('4')" style="cursor: pointer;">2.Monthly Consolidated Statistics Graph</a><br>
    <a onclick="showReports();reportNo('5')" style="cursor: pointer;">3.Monthly Job Count Graph</a><br>
    <a onclick="showReports();reportNo('6')" style="cursor: pointer;">4.Monthly Job Counts</a><br>
    <a onclick="showReports();reportNo('19')" style="cursor: pointer;">19.Driver Shifts</a><br>
    <a onclick="showOneDate();reportNo('20')" style="cursor: pointer;">20.Driver Jobs By Shift</a><br>
 </div>
 <div id="reportFive" style="display:none"> 
 	<a onclick="showOneDate();reportNo('9')" style="cursor: pointer;">1.Driver Trip & Shift Details</a><br>
 </div></td>
 <td  class="inp" width="60%" align="right" >
 			<table id="fromAndToDate"  style="display:none;">
 				 <tr>
            <td><label for="date">Start date: </label></td>
            <td><input data-dojo-type="dijit.form.DateTextBox" type="text"id="fromDate"name="fromDate"></td>
        </tr>
        <tr>
            <td><label for="date">End date: </label></td>
            <td><input data-dojo-type="dijit.form.DateTextBox" type="text" id="toDate"name="toDate"></td>
        </tr><tr><td align="center" colspan="2"><input type="submit"  class="niceButton" name="Submit" id="Submit" value="Submit"onclick="goToController()"/></td></tr></table>
 			 
 			 <table id="oneDate"  style="display:none;">
 			<tr><td> Date: <input type="text" name="Date" data-dojo-type="dijit.form.DateTextBox"id="Date"size="8" onfocus="showCalendarControl(this)"/></td>
 			<td> <input type="submit"  class="niceButton" name="submit"id="submit" value="submit" onclick="goToController()"></tr>
 			</table>
 			
 			<table>
 			<tr><td>Output Format : </td><td>
 			<select id="selectOutPut">
 			<option value="pdf">PDF</option>
 			<option value="csv">CSV</option>
 			<option value="docx">DOCX</option>
 			<option value="xls">XLS</option>
 			<option value="rtf">RTF</option>
 			</select></td></tr>
 			</table>
 			
 			 <table id="basedTrip"  style="display:none;">
 			<tr><td> Trip ID: <input type="text" name="tripId"id="tripId"size="8" /></td>
 			<td> <input type="submit" class="niceButton" name="submit"id="submit" value="submit" onclick="goToController()"></tr>
 			</table>
 			
 			<div id="flagDetails" style="display:none"><table id="flagDetailsTb"><tr></tr></table></div>
 			<div id="showVoucher" style="display:none"><input type="text" name="voucherNo" id ="voucherNo" value ="" placeholder="voucherNo!!"><ajax:autocomplete 
  					fieldId="voucherNo"
  					popupId="model-popup1"
  					targetId="voucherNo"
  					baseUrl="autocomplete.view"
  					paramName="getAccountList"
  					className="autocomplete"
  					progressStyle="throbbing"/></div>
 		</td>
 	</tr>
 </table>
<a href="control" style="text-decoration:none" > <input type="button" class="niceButton" value="Home"> </a> 
 <input type="hidden" name="reportNo" id="reportNo" value=""/>
</body>
</html>