<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.CompanyMasterBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<title>TDS(Taxi Dispatching System)</title>
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
</head>
<body>
<form name="companyForm" action="control" method="post" onsubmit="return showProcess()">
<%ArrayList a_list = (ArrayList)request.getAttribute("companyBO");%>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"/>
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveCompany %>"/>
<input type="hidden" name="systemsetup" value="6"/>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
				<c class="nav-header"><center>View/Edit Company</center></c>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
                    <%
						final char m_dispatchStatus[] = {'a','x','q','m'};
						final String m_dispatchValue[] = {"Automatic Allocation", "Manual Allocation", "Queue Order","Computer Aided"};
         				if(a_list !=null)
        	 			for(int i=0;i<a_list.size();i++) {
        	 			CompanyMasterBO companyBO=(CompanyMasterBO) a_list.get(i);
					%>
					 <tr>
					 <td >Company&nbsp;Name</td>
					 <td>
					 <input type="text" name="cname"  value="<%= companyBO.getCname() %>" >
					 </td>
					 <td >Company Description</td>
					 <td>
					 <input type="text" name="cdesc"  value="<%= companyBO.getCdesc() %>">
					 </td>
					 </tr>	
					 <tr>
					 <td >Address1</td>
					 <td>
					 <input type="text" name="add1" value="<%= companyBO.getAdd1() %>" >
					 </td>
					 <td >Address2</td>
					 <td>
					 <input type="text" name="add2" value="<%= companyBO.getAdd2() %>" >
					 </td>
					 </tr>
					 <tr>
					 <td >City</td>
					 <td>
					 <input type="text" name="city" value="<%= companyBO.getCity() %>" > 
					 </td>
					 <td >State</td>
					 <td>
					 <input type="text" name="state" value="<%= companyBO.getState() %>" >
					 </td>
					 </tr>
					<tr>
					<td >Zip&nbsp;Code</td>
					<td>
					<input type="text" name="zip" value="<%= companyBO.getZip() %>">
					</td>
					<td >Phone&nbsp;No</td>
					<td>
					<input type="text" name="phoneno"  value="<%= companyBO.getPhoneno() %>">
					</td>
					</tr>
					<tr>
					<td >Bank&nbsp;Account&nbsp;No</td>			
					<td>
					<input type="text" name="bankacno" value="<%= companyBO.getBankacno() %>" >
					</td>
					<td >Bank&nbsp;Name</td>
					<td>
					<input type="text" name="bankname" value="<%= companyBO.getBankname() %>" >
					</td>
					</tr>
					<tr>
					<td >Bank&nbsp;Routing&nbsp;No</td>
					<td>
					<input type="text" name="bankroutingno" value="<%= companyBO.getBankroutingno() %>" >
					</td>
					<td >Dispatch&nbsp;Status</td>
					<td>
					<input type="hidden" name="assocode" value="<%=companyBO.getAssccode() %>">
					<%
						for(int counter = 0; counter < m_dispatchStatus.length; counter ++) {
						if(companyBO.getDipatchStatus() == m_dispatchStatus[counter]) {
					%>
						<%= m_dispatchValue[counter] %>
					<%
					 
					 }
					}
					%>
					</td>
					</tr>
				    <tr>
				    <td >Logo</td>
					<%if(!companyBO.getFileid().equals("0")){ %>
					<td>
			 		<%if(companyBO.getFilename()!=null) {%>
					<a href='/TDS/control?action=file&event=fileDownload&fileid=<%=companyBO.getFileid() %>&filename=<%=companyBO.getFilename() %>' onclick="showProcess()"><%=companyBO.getFilename()%></a>
					<% } else { %>
					No Logo
					<%} %>	
					</td>
					<%} %>
					<td >Email</td>
					<td>
					<%=companyBO.getEmail() %>
					</td>
					</tr>
					<tr>
					<td colspan="4" align="center">
					<a href = '/TDS/control?action=admin&event=getMerchant&assocode=<%=companyBO.getAssccode() %>' onclick="showProcess()">Set Merchant Profile</a>
					</td>
					</tr>
					<tr>
					<td colspan="6" align="center">
                        <input type="submit" value="Edit" name="submit">
                    </td>
                    </tr>
                    <%} %>
                    </table>
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
					
					
		
		
				
						                   
               
                     