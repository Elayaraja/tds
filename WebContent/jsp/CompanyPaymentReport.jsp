<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList,java.util.List,java.util.Map,java.util.HashMap"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
	function callReport(action) {
		var fromdate = "";
		var todate = "";
		var companyid = "";
		var url = "";
		fromdate = document.getElementById("start").value;
		todate = document.getElementById("end").value;
		companyid = document.getElementById("getCompanyId").options[document.getElementById("getCompanyId").selectedIndex].value
		if(fromdate != "" && todate != "" && companyid != "") {
			url = document.getElementById("context").value+'/frameset?__report=BIRTReport/CompanyPaymentReport.rptdesign&__format=pdf&companyCode='+companyid+'&startDate='+fromdate+'&endingDate='+todate
			document.companyPaymentForm.action = url;
			document.companyPaymentForm.target = 'new';
			companyPaymentForm.submit();
		} else {
			alert("You must give all Values");
		}
	}

</script>
<%
	List al_companylist = null;
	Map mp_companyMap = null;
	System.out.println("Before Request "+request.getAttribute("companyList"));
	if(request.getAttribute("companyList") != null) {
		System.out.println(" Request "+request.getAttribute("companyList"));
		al_companylist = (ArrayList) request.getAttribute("companyList");
	}
%>
</head>
<body>
<form name="companyPaymentForm"  action="control" method="post">
	<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
	<table id="pagebox">
		<tr>
			<td>
			<table id="bodypage">
			<tr>
				<td colspan="7" align="center">
					<div id="title" >
						<h2>Get&nbsp;Company&nbsp;Payment&nbsp;Report</h2>
					</div>
				</td>
			</tr>
			<tr>
				<td>Select Driver Name</td>
				<td>&nbsp;</td>
				<td colspan="5">
					<select id="getCompanyId">
						<option>Select</option>
					<%
						System.out.println("List "+ al_companylist);
						for(int count = 0;al_companylist != null && count < al_companylist.size(); count ++) {
							mp_companyMap = (HashMap) al_companylist.get(count);
					%>
							<option value="<%= mp_companyMap.get("companyid") %>"><%= mp_companyMap.get("companyName") %></option>
					<%
						}
					%>
					</select>
				</td>
			</tr>
			<tr>
				<td>Start&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
					<input type="text" name="sdate" id="start" size="10"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.companyPaymentForm.sdate);return false;" HIDEFOCUS>
				<img name="popcal" align="absmiddle" src="images/calendar.gif" width="25" height="16" border="0" alt=""></a>		
				<iframe width="20"  height="178"  name="gToday:normal:agenda.startdate" id="gToday:normal:agenda.startdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
				</iframe>
				</td>
				<td>&nbsp;</td>
				<td>End&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
				<input type="text" name="edate" id="end" size="10"/>
				<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.companyPaymentForm.edate);return false;" HIDEFOCUS>
				<img name="popcal" align="absmiddle" src="images/calendar.gif" width="25" height="16" border="0" alt=""></a>		
				<iframe width="20"  height="178"  name="gToday:normal:agenda.enddate" id="gToday:normal:agenda.enddate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
				</iframe>
				</td>
			</tr>
			<tr>
				<td colspan="7"> 
					<input type="submit" name="submit" value="Search" onclick="callReport(this)"/>
				</td>
			</tr>
			</table>
			</td>
		</tr>
	</table>
</form>
</body>
</html>