<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.tdsBO.CabMaintenanceBO,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<jsp:useBean id="cabMaintenanceBo" class="com.tds.tdsBO.CabMaintenanceBO" scope="request"/>
<% if(request.getAttribute("cabMaintenance") != null)
	cabMaintenanceBo = (CabMaintenanceBO)request.getAttribute("cabMaintenance");

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>



<script type="text/javascript">
$(document).ready(function(){
    
    //called when key is pressed in textbox
	$("#samount").keypress(function (e)  
	{ 
	  //if the letter is not digit then display error and don't type anything
	  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57))
	  {
		//display error message
		$("#errmsg").html("<font color=\"red\">*Amount should contain Numbers Only</font>");
	    return false;
      }	
	});

  });
function removeError(){
	  document.getElementById("errorpage").innerHTML ="";
	  document.getElementById("errmsg").innerHTML ="";
}

function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}


</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
</head>
<body>
<div  id="errmsg" style="position: absolute;left:380px;top:190px;"  ></div>
<form method="post" action="control" name="maintenance" onsubmit="return showProcess()"/>
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"/>
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.createMaintenanceDetail %>"/>
<input type="hidden" name="maintenceNo" value="<%= cabMaintenanceBo.getMaintenanceKey() %>"/>
<input type="hidden" name="operation" value="2"/>
                        		<c class="nav-header"><center>CAB MAINTENANCE</center></c><br></br>
                        		<%
								String error ="";
								if(request.getAttribute("errors") != null)
								{
									error = (String) request.getAttribute("errors");
								}		
								%>
								<div id="errorpage">
								<!--  <font color="red"><%= error.length()>0?""+error :"" %></font> -->
								<%if(error.equals("1")){
								%>
								<div  id="errmsg1" style="position: absolute;left:380px;top:100px;"><font color="red">*You must Enter Service Code</font></div>
								<%
								}else
								if(error.equals("2")){
								%>
								<div  id="errmsg2" style="position: absolute;left:380px;top:180px;"><font color="red">*You must Enter Service amount</font></div>
								<%
								}
								if(error.equals("Created Cab Maintenance"))
								{
									%>
									<div  id="success" style="position: absolute;left:420px;top:80px;"><font color="blue" >*Cab Maintenance created Successfully</font></div>
									<%
								}
								%>
								</div>
								 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
                    <td>
                    <table id="bodypage" width="440">
                    <tr>
	                    <td>Service Code</td>
	                    <td><input type="text"  size="8" id="scode"name="mcode"   value="<%= cabMaintenanceBo.getServiceCode() == null ? "" : cabMaintenanceBo.getServiceCode() %>"onkeypress="removeError()"/></td>
                    </tr>
                    <tr>
	                    <td>Service Date</td>
	                    <td><input type="text"  size="8" name="sdate"   value="<%= cabMaintenanceBo.getServiceDate() == null ? "" : cabMaintenanceBo.getServiceDate()  %>" onfocus="showCalendarControl(this);"  readonly="readonly" onkeypress="removeError()"/></td>
                    </tr>
                    <tr>
	                    <td>Amount</td>
	                    <td>$<input type="text"  name="samount" size="10" id="samount"   value="<%= cabMaintenanceBo.getAmount() == null ? "" : cabMaintenanceBo.getAmount()  %>"onkeypress="removeError()"/></td>
                    </tr>
                    <tr>
	                    <td>Description</td>
	                    <td><textarea name="scomment" rows="5" cols="25" ><%=  cabMaintenanceBo.getServiceDesc() == null ? "" : cabMaintenanceBo.getServiceDesc()   %> </textarea></td>
                    </tr>
                    	<tr>
                    		<td colspan="2" align="center">
                        	  
                    		             <% if(cabMaintenanceBo.getMaintenanceKey() != null && !cabMaintenanceBo.getMaintenanceKey().equalsIgnoreCase("null") ) { %>
										<input type="submit" name="update" value="Update" class="lft"/>
									<% } else { %>
									<input type="submit" name="save" value="Save" class="lft" onclick="nullvalue()"/>				
									<% } %>		
				</td>
                    		 
                    	</tr>
                    </table>
                    </td>
                    </tr>
                    </table>          
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</body>
</html>