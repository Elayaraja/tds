<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO;"%>
<%! int startValue =1 ;double limit=0;int pstart=1;int pend=0;int pageNo=1;int plength=0;%>
<%if(request.getAttribute("startValue") != null){
	startValue = Integer.parseInt((String)request.getAttribute("startValue"));
	startValue=startValue+1;
}	
if(request.getAttribute("plength")!=null){
	plength=Integer.parseInt((String)request.getAttribute("plength"));
	}
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TDS (Taxi Dispatch System)</title>
</head>
<body>
<table id="pagebox">
	<tr>
		<td>
<table id="bodypage" border="1" cellpadding="0" cellspacing="0">	
	<tr>
		<td colspan="12" align="center">
			<div id="title">
				<h2>View/change User Permissions</h2>
			</div>
		</td>
	</tr>
	<tr>
	<%
		String error ="";
		if(request.getAttribute("errors") != null) {
			error = (String) request.getAttribute("errors");
		}
		
	%>
	<tr>
		<td colspan="8">
			<div id="errorpage">
				<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %>
			</div>
		</td>
	</tr>
	<tr align="center" bgcolor="#d3d3d3">
		<td>UserName</td>
		<td>UserType</td>
		<td>Status</td>				
	</tr>
		<%
		AdminRegistrationBO adminbo = new AdminRegistrationBO();
		ArrayList al_driverData=new ArrayList();
		if(request.getAttribute("Adminsummary")!=null){
		al_driverData=(ArrayList)request.getAttribute("Adminsummary");
		for(int r_count=0;r_count<al_driverData.size();r_count++){
		%>
		<tr align="center" valign="top">
			<td>&nbsp;<%=((AdminRegistrationBO)al_driverData.get(r_count)).getUname()%>&nbsp;</td>
			<td>&nbsp;<%=((AdminRegistrationBO)al_driverData.get(r_count)).getUsertypeDesc()%>&nbsp;</td>
			<%if(Integer.parseInt((((AdminRegistrationBO)al_driverData.get(r_count)).getActive()))==1){ %>
					<td><input type="button" value="Active" onclick='window.location.href="control?action=userrolerequest&event=getUserList&userid=<%=((AdminRegistrationBO)al_driverData.get(r_count)).getUid() %>&uname=<%=((AdminRegistrationBO)al_driverData.get(r_count)).getUname() %>&utype=<%=((AdminRegistrationBO)al_driverData.get(r_count)).getUsertype() %>&userDesc=<%= ((AdminRegistrationBO)al_driverData.get(r_count)).getUsertypeDesc() %>&status=<%=((AdminRegistrationBO)al_driverData.get(r_count)).getActive()%>";'></td>
      		<% } else{%>
					<td><input type="button" value="In Active" onclick='window.location.href="control?action=userrolerequest&event=getUserList&userid=<%=((AdminRegistrationBO)al_driverData.get(r_count)).getUid() %>&uname=<%=((AdminRegistrationBO)al_driverData.get(r_count)).getUname() %>&utype=<%=((AdminRegistrationBO)al_driverData.get(r_count)).getUsertype() %>&userDesc=<%= ((AdminRegistrationBO)al_driverData.get(r_count)).getUsertypeDesc() %>&status=<%=((AdminRegistrationBO)al_driverData.get(r_count)).getActive()%>";'></td>
	         <%} %>
		<%}%>
		</table>
		<table align="center">
					<tr align="center">
						<%  
							if(request.getAttribute("pstart")!=null){
								pstart= Integer.parseInt((String)request.getAttribute("pstart"));
								}
							if(request.getAttribute("pend")!=null){
								pend=Integer.parseInt((String)request.getAttribute("pend"));
								}
							if(request.getAttribute("limit")!=null){
								limit = Double.parseDouble((String)request.getAttribute("limit"));
								}
								int start=0;
								int end =0;
							if(request.getAttribute("DRBO")!=null){
								adminbo = (AdminRegistrationBO)request.getAttribute("DRBO");
								}
				 				start=Integer.parseInt(adminbo.getStart())+1;
								end =Integer.parseInt(adminbo.getEnd());
				 
							 if(limit<=plength)
				 				{ 	 		
			 	 					for(int i=1;i<=limit;i++){ %>
			 	 					<td><a href="/TDS/control?action=userrolerequest&event=getUserList&subevent=access&page<%=i %>=<%=start%>"><%=i %></a></td>
			 	 					<%} 
			 	 				}				 
							if(limit>=plength)
								{ 
									if(pstart!=1){
									%>
									<td><a href="/TDS/control?action=userrolerequest&event=getUserList&subevent=access&previous=<%=""+pstart%>&first=no">&lt;</a></td>
									<%}
									for(int i=pstart;i<pend;i++){%>				
									<td><a href="/TDS/control?action=userrolerequest&event=getUserList&subevent=access&pageNo=<%=i%>&pstart=<%=""+pstart%>&pend=<%=""+pend %>&first=no"><%=i %></a></td>
									<%}
									if(pend<=limit){	%>
									<td><a href="/TDS/control?action=userrolerequest&event=getUserList&subevent=access&next=<%=pend+""%>&first=no">&gt;</a></td>
									<%} 
								}%>
					</tr>
				</table>
			<%}
		else{%>
		<table>
		<tr>
			<td>No&nbsp;Record&nbsp;Found&nbsp;</td>
		</tr>
		</table>
		<%} %>
	
	</td>
	</tr>
	</table>	
</body>
</html>
