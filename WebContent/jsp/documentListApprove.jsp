<%@ page language="java" import="java.util.*" %>
<%@ page import="com.tds.bean.docLib.*" %>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%
	String ctxPath = request.getContextPath();
	
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
 
 <script type="text/javascript" src="<%=ctxPath %>/js/document/validation.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link rel="stylesheet" type="text/css" href="<%=ctxPath %>/css/document.css" media="all">



<script type="text/javascript" src="js/jqModal.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>
<script type="text/javascript" src="<%=ctxPath %>/js/document/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<%=ctxPath %>/js/document/jquery.tablesorter.pager.js"></script>

<title>TDS(Taxi Dispatching System)</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">


<script type="text/javascript">

function uploadNew(){
	
	var compId = document.getElementById("compId").value;
	document.getElementById("uploadForm").action="<%=ctxPath %>/control?action=TDS_DOCUMENT_APPROVAL&event=passKeyGen&module=operationView&do=uploadOthers&compId="+compId;
	document.getElementById("uploadForm").submit();
}


function approveFile(){
	var txtComp = document.getElementById("txtComp").value;
	var numb = document.getElementById("docNumber").value;
	var docType = document.getElementById("docType").value;
	var expDate = document.getElementById("expDate").value;
	var sever = document.getElementById("Severity").value;
	var userId = document.getElementById("txtUserId").value;
	
	
	
	if(isEmpty(txtComp)){
		alert("Select the file to approve.");
		return false;
	}
	
	if(isEmpty(userId)){
		alert("Provide User Id");
		return false;
	}
	
	if(isEmpty(numb)){
		alert("Document Number cannot be empty");
		document.getElementById("docNumber").focus();
		return false;
	}

	
	if(docType==' ### ### '){
		alert("Document Type cannot be empty");
		document.getElementById("docType").focus();
		return false;
	}


	if(isEmpty(expDate)){
		if(sever!='N/A'){
			alert("Expiry Date cannot be empty");
			document.getElementById("expDate").focus();
			return false;
		}
	}
	
	document.getElementById("uploadForm").action='<%=ctxPath %>/control?action=TDS_DOCUMENT_APPROVAL&event=passKeyGen&module=operationView&do=approveFile';
	document.getElementById("uploadForm").submit();
}
	function rejectFile(){
	
		var docId= document.getElementById("hCompId").value;
		var userName = document.getElementById("hCompId").value; 'Admin';
		document.getElementById("uploadForm").action='<%=ctxPath %>/control?action=TDS_DOCUMENT_APPROVAL&event=passKeyGen&module=operationView&do=rejectFile&docId='+docId;
		document.getElementById("uploadForm").submit();
	}

	function loadFile(docId,docName,compCode,docUserId,docType){
		clearForm();
		document.getElementById("hCompId").value=docId;
		document.getElementById("txtComp").value=docName;
		

		if(docUserId!='null' || docUserId!=null){
		document.getElementById("txtUserId").value = docUserId;
		}else{
			document.getElementById("txtUserId").value = "";
		}
		if(docType!=null && docType!=""){
		var severity="";
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url='SystemSetupAjax?event=getseverity&module=systemsetupView&docType='+docType;
		xmlhttp.open("GET",url,false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;
		 
		var resp=text.split("###");
		if(resp[1]!=null && resp[1]!=""){
			severity=resp[1];
		} 
		if(resp[2]!=null && resp[2]!=""){
			module=resp[2];
		}
			 document.getElementById("docType").value =text;
			 document.getElementById("Severity").value=severity;
		}else{
			 document.getElementById("docType").value="";
			 document.getElementById("Severity").value="";
		}
		document.getElementById("docLoader").src='uploadDocs?do=viewDoc&docId='+docId;
	}
	function cancelDoc(){
		clearForm();
	}

	function setServerity(obj){
		var x = obj.value;
		var a = x.split('###');
		document.getElementById("hType").value=a[0];
		document.getElementById("hModule").value=a[2];
		document.getElementById("Severity").value=a[1];
		
	}		

	function clearForm(){
		
		document.getElementById("docType").value=" ### ### ";
		document.getElementById("txtComp").value="";
		document.getElementById("docLoader").src='';
		document.getElementById("Severity").value="";
		document.getElementById("hType").value="";
		document.getElementById("hModule").value="";
		document.getElementById("hCompId").value="";
		document.getElementById("docNumber").value="";
		document.getElementById("expDate").value="";
		document.getElementById("txtUserId").value="";
		
		 
	}
	function showOption(){
		var optionValue=document.getElementById("docType").value;
		document.getElementById("driverOrcab").value=optionValue;
		if(optionValue==1){
			$('#docTypeVehicle').hide();
			$('#docTypeDriver').show('slow');
		}else if(optionValue==2){
			$('#docTypeDriver').hide();
			$('#docTypeVehicle').show('slow');
		}
	}
</script>



</head>
<body>

<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<form id="uploadForm" name="uploadForm" action="uploadDocs?do=approveFile" method="post" >


<input type="hidden" name="hModule" id="hModule" />
<input type="hidden" name="hType" id="hType" />
<input type="hidden" name="hCompId" id="hCompId" />
<input type="hidden" name="compId" id="compId" />
<input type="hidden" name="userId" id="userId"  value='<%=request.getAttribute("userId") %>'/>	
<input type="hidden" name="userId1" id="userId1"  value='<%=request.getAttribute("reply") %>'/>
<input type="hidden" name="driverOrcab" id="driverOrcab" value=""/>

<c class="nav-header"><center> Approve Documents </center></c>
<br>
<%if(request.getAttribute("reply")=="ok"){%>
<font color="green">Documents Approved Successfully</font>
<%}
%>
<br>
<br>
<B>To Upload Documents for Others <a href="javascript:uploadNew()" > <img alt="" src="jsp/images/upload.png" width="2%" height="2%"> </a></B>
<br><br>
<div style="float: left; width: 65%;">
<div style="float: left; width: 40%;">

	<div style="display: block;">
	<center><b> List Of Documents</b></center>
	<table id="documentList" height="auto"  width="100%" class="table-striped">
	<thead> 
		<TR>
			<TH>S.&nbsp;No</TH>
			<TH>User&nbsp;Id</TH>
			<TH>Document&nbsp;Type</TH>
			<TH>View</TH>
		</TR>		
	</thead>
	<tbody align="center"> 	
		<%
			Collection collection = (Collection)request.getAttribute("docList");
	  		int i=0;
	  		for (Iterator iter = collection.iterator(); iter.hasNext();) {
	  			i = i+1;
	  			DocBean element = (DocBean) iter.next();
	  			
	  			if(i%2==0){
	  				out.println("<tr class=\"alternate\"><TD>");
	  			}else{
	  				out.println("<tr><TD>");
	  			}	 
	  			out.println(+i);
	  			out.println("</td><td>");
	  			if(element.getDocUploadedBy()!=null){
	  				out.println(element.getDocUploadedBy());
	  			}else{
	  				out.println("N/A");
	  			}	  				  			
	  			out.println("</td><td>");
	  			if(element.getDocType()==null || element.getDocType().equals("")){
	  				out.println("UnKnown");
	  			}else{
	  			out.println(element.getDocType()=="" ? "UnKnown" : element.getDocType());
	  			}
	  			out.println("</td><td>");
	  			out.println("<a href=\"javascript:loadFile('"+element.getDocId()+"','"+element.getDocName()+"','"+element.getTdsCompCode()+"','"+element.getDocUploadedBy()+"','"+element.getDocType()+"')\">");
	  			%> 
	  			<img alt="" src="jsp/images/view.jpg" height="30%" width="30%" >
	  			<% 
	  			out.println("</a>");
	  			out.println("</td></tr>");
	  		}
	  		
	  		if(i==0){
	  			out.println("<tr><TD colspan=3 align=\"center\" >NO RECORD FOUND</TD></TR>");
	  		}
		%>
	</tbody>
	</table>
	</div>
	
	<div id="pager" class="pager" align="center" style="position:absolute; ">
		<img src="<%=ctxPath %>/images/first.png" class="first"/>
		<img src="<%=ctxPath %>/images/prev.png" class="prev"/>
		<input type="text" class="pagedisplay" disabled="disabled"/>
		<img src="<%=ctxPath %>/images/next.png" class="next"/>
		<img src="<%=ctxPath %>/images/last.png" class="last"/>
		<input type="hidden" value="15" class="pagesize" id="pagesize" />		
	</div>
</div>
<div style="width: 1%"></div>
<div style="float: right; width: 40%">

		<fieldset><center></center><b> Document Information </b>
		<TABLE>
		<TR>
			<TD>User Id
			</TD><TD>
				<input type="text" id="txtUserId" name="txtUserId" />
			</TD>
		</TR>
		<TR>
			<TD>File Name
			</TD><TD>
				<input type="text" id="txtComp" name="txtComp" readonly />
			</TD>
		</TR>
		<TR>
			<TD>Number
			</TD><TD>
				<input type="text" name="docNumber" id="docNumber"/>
			</TD>
		</TR>
		<TR>
			<TD>Type
			</TD><TD>
					<Select name="docType" id="docType" onchange="showOption()"  >
					<option value="0">-Select-</option>
					<option value="1">Driver/Operator</option>
					<option value="2">Vehicle</option>
					</Select>
			
				<select name="docTypeDriver" onchange="javascript:setServerity(this);" id="docTypeDriver" style="display:none;" >
				<option  value=" ### ### " >-Choose-</option>
				<%
				Collection docTypeColl = (Collection)request.getAttribute("docTypeDataDriver");
		  		for (Iterator iter = docTypeColl.iterator(); iter.hasNext();) {
		  			NameValueTag element = (NameValueTag) iter.next();%>
		  			<option value="<%=element.getDocType()%>###<%=element.getSeverity()%>###<%=element.getModule()%>"><%=element.getDocType()%></option>
		  		<%}	%>
				</Select>
			<Select name="docTypeVehicle" onchange="javascript:setServerity(this);" id="docTypeVehicle" style="display:none;" >
				<option  value=" ### ### " >-Choose-</option>
				<%
				Collection docTypeCollVehicle = (Collection)request.getAttribute("docTypeDataVehicle");
		  		for (Iterator iter = docTypeCollVehicle.iterator(); iter.hasNext();) {
		  			NameValueTag element = (NameValueTag) iter.next();%>
		  			<option value="<%=element.getDocType()%>###<%=element.getSeverity()%>###<%=element.getModule()%>"><%=element.getDocType()%></option>
		  	<%}%>
			</Select>
			</TD>
			
		</TR>
		<TR>
			<TD>Exp. Date
			</TD><TD>
				<input type="text" name="expDate" id="expDate" readonly size="12" maxlength="10" onclick="showCalendarControl(this);"/>&nbsp;
			</TD>
		</TR>
		<TR>
			<TD>Severity
			</TD><TD>
				<input type="text" name="Severity" id="Severity" readonly />
			</TD>
		</TR>
		
		<TR>
			<TD colspan="2" align="center" >
				<input type="button" value="Approve" onclick="javascript:approveFile()">&nbsp;&nbsp;
				<input type="button" value="Reject" onclick="javascript:rejectFile()">&nbsp;&nbsp;
				<input type="button" value="Cancel" onclick="cancelDoc()"> 
			</TD> 
			
		</TR>
		</TABLE>
		 </fieldset>
</div>
	</div>
	<div style="width: 5%"></div>
	<div></div>
	<div style="float: right; width: 35%; height:60%; margin-right:-5% " >
			<center><B> Document View </B><br></center>
			<iframe src="" id="docLoader" name="docLoader" width="auto" height="60%" scrolling="auto" frameborder="0" align="left" >	</iframe>	
			 <br>
			 </div>
           

</form>

<script defer="defer">
	$(document).ready(function() 
    { 
        $("#documentList")
		.tablesorter({widthFixed: false, widgets: ['zebra']})
		.tablesorterPager({container: $("#pager")}); 
    } 
	); 
</script>

</body>
</html>


