<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="java.io.*" %>
<%@ page import="com.tds.dao.UserDetail" %> 
<%@ page import="org.codehaus.jettison.json.JSONArray"%>
<%@ page import="org.codehaus.jettison.json.JSONException" %>
<%@ page import="org.codehaus.jettison.json.JSONObject" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Customer Detail</title>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript">


function sendXmlData()
{
	 var xmlhttp;
	 if(window.XMLHttpRequest) 
		 {
		 xmlhttp = new XMLHttpRequest();
		 }
	 else
		 {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		 }
	 var url='control?action=openrequest&event=customerdetail&module=operationView&name='+document.getElementById("name").value+'&phonenumber='+document.getElementById("number").value;
	 //alert("url is"  +url);
	 xmlhttp.open("GET",url,false);
	 xmlhttp.send(null);
	 var text = xmlhttp.responseText;
	 //alert("text is" +text); 
	 if(text == "[]")
	 {

			alert("No details ");
	 }
	 else
	 {
	 var jsonobj = "{\"address\":"+text+"}" ;
	 var obj = JSON.parse(jsonobj.toString());
	 var tbl;
	 	     $(document).ready(function(){
			 $("#CustomerDetails").remove();
	         $("#myDiv").html(" ");
			 $("#myDiv").append('<table border="1" width="70%" cellspacing="1" cellpadding="1"  bgcolor="#FFFFFF" id="CustomerDetails">');
			 $("#CustomerDetails").append('<thead bgcolor="#00FFFF"><tr><th>SNO</th><th>ID</th><th>NAME</th><th>EMAIL</th><th>PHONENO</th><th>key</th><th>OPTION</th><th>OPTION1</th></tr></thead>');
	         });
	        for(var i=0;i <obj.address.length;i++)
			{
		 	 tbl=$('<tr/>');
			 tbl.append('<td bgcolor="#FFFFFF"><input type="text"   id="sno_'+i+'"   style="width:50px" value="'+obj.address[i].sno +'" readonly></td>');				
			 tbl.append('<td bgcolor="#FFFFFF"><input type="text"   id="id_'+i+'"    style="width:100px"  value="'+obj.address[i].id + '"readonly></td>');
			 tbl.append('<td bgcolor="#FFFFFF"><input type="text"   id="name_'+i+'"  style="width:175px" value="'+obj.address[i].name +'"readonly></td>');
 			 tbl.append('<td bgcolor="#FFFFFF"><input type="text"   id="email_'+i+'" style="width:175px" value="'+obj.address[i].email +'"readonly></td>');
			 tbl.append('<td bgcolor="#FFFFFF"><input type="text"   id="phone_'+i+'" style="width:175px" value="' +obj.address[i].phone +'"readonly></td>');
			 tbl.append('<td bgcolor="#FFFFFF"><input type="text"   id="key_'+i+'" style="width:175px" value="' +obj.address[i].key +'"readonly></td>');
			 tbl.append('<td bgcolor="#FFFFFF"><input type="button" id="Edit_'+i+'"  value="Edit" onclick="editData('+i+')"></td>');
			 if(obj.address[i].status == "0"){
			 tbl.append('<td bgcolor="#FFFFFF"><input type="button" id="Deactive_'+i+'"   value="DeActivate" onclick="DeActivate('+i+')"></td>');
			 }
			 else if(obj.address[i].status == "1"){
				 tbl.append('<td bgcolor="#FFFFFF"><input type="button" id="Deactive_'+i+'"   value="Activate" onclick="DeActivate('+i+')"></td>');	 
			 }
			 $('#CustomerDetails').append(tbl);
			}
	   }
       }
   </script> 
   <script type="text/javascript">
        $("#btnPrint").live("click", function () {
            var CustomerDetails = document.getElementById("CustomerDetails");
            var printWindow = window.open('', '', 'height=400,width=1300,font-size=20');
            printWindow.document.write('<html><head><title> Customer Details</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(CustomerDetails.outerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });
    </script>
   <script type="text/javascript">
   function editData(i){
	   var xmlhttp;
	     if(window.XMLHttpRequest) 
		    {
		      xmlhttp = new XMLHttpRequest();
		    }
	     else
		    {
		      xmlhttp = new regexActiveXObject("Microsoft.XMLHTTP");
		    }
	    var ele=document.getElementById('Edit_'+i);
	    var sno=document.getElementById('sno_'+i);
	    var id =document.getElementById('id_'+i);
	    var name=document.getElementById('name_'+i);
	    var email=document.getElementById('email_'+i);
	    var phone=document.getElementById('phone_'+i);
	    if(ele.value == "Edit"){
	         ele.value="Save";
	         id.removeAttribute('readonly',false);
	         name.removeAttribute('readonly',false);
	         email.removeAttribute('readonly',false);
	         phone.removeAttribute('readonly',false);
	        }
	    else{
	    	 ele.value="Edit";
	    	 var url='control?action=openrequest&event=customerdetail&module=operationView&sno='+sno.value+'&id='+id.value+'&name='+name.value +'&email='+email.value +'&phonenumber='+phone.value+'&reason=update';
	 		 var reg ='^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$';
	 		     var reg2=/^\d{10}$/;
	 		     var reg3 =/^(\d{3}-\d{3}-\d{4})$/;
                    if(email.value.match(reg)!= null)
	 				{
 					if(phone.value.match(reg2)|| phone.value.match(reg3))
 				    {
 					xmlhttp.open("GET",url,false);
	    	     	xmlhttp.send(null);		 	
			     	var response = xmlhttp.responseText;
			 		if(response == "success")
			 		     {
 						     alert("Customer Detail Updated");
 	     		         }
			    	else{
				    		 alert("Not Updated!Please Give Correct Information"); 
				    		 ele.value="Save";
 	 			          }
 	 			     id.setAttribute   ('readonly' , 'True');
	                 name.setAttribute ('readonly' , 'True');
	    	         email.setAttribute('readonly' , 'True');
	     	         phone.setAttribute('readonly' , 'True');
 				    }
 					else{
 						alert("In-Valid Phone number");
 						 ele.value="Save";
 				    	
 					}
	 		 }
	 		 else{
	 			alert("Incorrect Email");
	 			 ele.value="Save";
	 		 } 
	    }
   }
	    	
  </script>
  <script type="text/javascript">
	  function DeActivate(i){ 
  		var xmlhttp;
  		if(window.XMLHttpRequest) 
	         {
	     		 xmlhttp = new XMLHttpRequest();
	         }
  		else
	         {
	      		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	         }
  	    var DeId=document.getElementById('Deactive_'+i);
  		var sno=document.getElementById('sno_'+i);
	    var id =document.getElementById('id_'+i);
	    var name=document.getElementById('name_'+i);
	    var email=document.getElementById('email_'+i);
	    var phone=document.getElementById('phone_'+i);
		if(DeId.value == "DeActivate"){
		DeId.value="Activate";	
		var Act = 1;
  		var url='control?action=openrequest&event=customerdetail&module=operationView&sno='+sno.value+'&id='+id.value+'&name='+name.value +'&email='+email.value +'&phonenumber='+phone.value+'&reason=DeActivate'+'&value='+Act;
  		xmlhttp.open("GET",url,false);
  		xmlhttp.send(null);
  		var response=xmlhttp.responseText;
  		if(response=="success"){
  		   alert("De-Activated");
		}
  		else{
  		    alert("Not De-Activated");
  		}
		}
		else{
			DeId.value="DeActivate";
			Act=0;
			var url='control?action=openrequest&event=customerdetail&module=operationView&sno='+sno.value+'&id='+id.value+'&name='+name.value +'&email='+email.value +'&phonenumber='+phone.value+'&reason=DeActivate&value='+Act;
			xmlhttp.open("GET",url,false);
	  		xmlhttp.send(null);
	  		var response=xmlhttp.responseText;
	  		if(response == "success"){
	  			//$('#myDiv').html('<p>  <br> <b>Activated </b></p>');
	  			alert("Activated");
	  		}
	  		else{
	  			//$('#myDiv').html('<p>  <br> <b>Not-Activated </b></p>');
	  			alert("Not-Activated");
	  		}
		}
       }
           </script>
         </head>
      <body>
      	<c class="nav-header"><center> Customer Details</center></c>
		
        <form id="customer">
        <div align="center">
        <table><tr><td>
                Customer Name<input type="text" id="name" input autofocus="autofocus"> 
                Phone Number<input type ="text" id="number">
              </td></tr><tr><td>
                                 <input type="button"  value="Search" onclick="sendXmlData()">
                                   	<input type="button" value="Save/Print" id="btnPrint" /></td></tr></table>
         </form> 
                <div id="myDiv" >
                         <table id="CustomerDetails" style="background-color:white;"></table>
                </div>
      </body>
      </html>