<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@page import="java.util.ArrayList"%>
<html>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<%DriverDisbursment driverid=(DriverDisbursment)request.getAttribute("disbo") == null ? new DriverDisbursment():(DriverDisbursment)request.getAttribute("disbo") ;
		%>
<script type="text/javascript">
function Calgetdetails(){
	
}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body >
<form  name="masterForm"  action="control" method="post">
<input type="hidden" name="action" value="registration">
<input type="hidden" name="event" value="zoneentry">

 <input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<table id="pagebox">
	<tr>
		<td>
<table id="bodypage">	
	<tr>
		<td colspan="7" align="center">		
			<div id="title">
				<h2>Zone Login</h2>
			</div>
		</td>		
	</tr>
	<tr>
	<td colspan="7" align="left">
		<%StringBuffer error =(StringBuffer) request.getAttribute("error"); %>
		<%= (error !=null && error.length()>0)?""+error :"" %>
		</td>
	</tr>
	<%if(((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equals("Driver")) {%>
	<tr>
		<td>Driver</td>
		<td>&nbsp;</td>
		<td colspan="4" >
			<input type="text" size="5" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getUid() %>" readonly="readonly"  name="driverid"  id="driverid">
		</td>
	</tr>
	
	<%} else { %>
	<tr>
		<td>Driver</td>
		<td>&nbsp;</td>
		
		<td colspan="4" >
		
			<input type="text" size="8"  id="driverid" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" name="driverid"  autocomplete="off"  onblur="caldid(id,'drivername','dName')"  id="driverid" value="<%=driverid.getDriverid() == null ? "" :driverid.getDriverid() %>" class="form-autocomplete" >
			 <ajax:autocomplete
				  					fieldId="driverid"
				  					popupId="model-popup1"
				  					targetId="driverid"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 			
							
							 <input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">		
							 	  <div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
		</td>
	</tr>
	
	<%} %>
	<tr>
		<td>Zone</td>
		<td>&nbsp;</td>
		<td colspan="4" >
		<%
		
			ArrayList al_List = (ArrayList)request.getAttribute("zonelist")==null? new ArrayList() :(ArrayList)request.getAttribute("zonelist") ;
		
			DriverDisbursment driverDisbursment=null;%>
			<select name="zone">
			<%if(al_List != null)
		      		for(int i=0;i<al_List.size();i=i+2){  
		     %>
					<option value="<%=al_List.get(i)%>"  <%= al_List.get(i).toString().equals(request.getParameter("zone")==null?"":request.getParameter("zone")) ? "selected":"" %> ><%=al_List.get(i+1).toString() %></option>
			<%} %>
			</select>
				
		</td>
		
	</tr>
	
	<tr>
	<td colspan="7" align="center">
		<input type="submit"  name="button" onclick="Calgetdetails()" value="Submit" >
		</td>
	</tr>
</table>
</td>
</tr>
</table>
<table id="tableID"></table>
</form>
</body>
</html>