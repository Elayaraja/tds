<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.cmp.bean.PenalityBean"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>


<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<html>
<head>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>

<script type="text/javascript">

function AJAXInteraction(url, callback) {
    var req = init();
    req.onreadystatechange = processRequest;
    function init() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    } 
    
    function processRequest () {
      // readyState of 4 signifies request is complete
      if (req.readyState == 4) {
        // status of 200 signifies sucessful HTTP call
        if (req.status == 200) {
          if (callback) callback(req.responseText);
        }
      }
    }

    this.doGet = function() {
      req.open("GET", url, true);
      req.send(null);
    }
}


function doVoucherUpdate()
{

	 var url = '/TDS/AjaxClass?event=doUpdateVoucher&costc='+document.getElementById("costc").value;
	 var ajax = new AJAXInteraction(url,doVoucherUpdateReturn);
	 ajax.doGet();
	
}
function doVoucherUpdateReturn(responseText)
{
	alert(responseText);

	window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/Voucher.rptdesign&__format='+fmat+'&stdate='+fromdate+'&enddate='+todate+'&asscode='+code+'&payDate='+responseText+'&costc='+document.getElementById('costc').value);
	
}


</script>

<script type="text/javascript">
 function autocom()
 { 
	 autocomplete("Driverid", "model-popup2", "Driverid", "autocomplete.view", "DRIVERID", null, "throbbing",null);
 }
</script>
<% 	AdminRegistrationBO adminBO = null;
			if(session.getAttribute("user") != null) { 
				adminBO= (AdminRegistrationBO)session.getAttribute("user");
			}
%>
 
<script type="text/javascript">
var temp="";
var reportno="103";
function calreportno(){
	reportno = document.getElementById("moduleno").value;
}
function cal(){
	 document.getElementById("bb").innerHTML="";
	 document.getElementById("aa").innerHTML="";
	document.getElementById("butt").innerHTML="";
	 document.getElementById("report").innerHTML="";
	 document.getElementById("voucherid").innerHTML="";
	var type = document.getElementById("type").value;
	if(type=="2" ){
		if(document.getElementById("Dcode").value == "0"){
		 document.getElementById("bb").innerHTML="Driver ID  ";
		 document.getElementById("aa").innerHTML=" <input type='text' size='10' id='Driverid' name='Driverid' onkeyup = autocom(); onblur=caldid(id,'drivername','dName'); onfocus=appendAssocode('"+document.getElementById('code')+"',id) ><input type='hidden' name='dName' id='dName'> <div id='drivername'></div> <div id='model-popup2' class='autocomplete'></div>";
		
		}
	}
	else if(type=="3"){
	 document.getElementById("aa").innerHTML="Pdf <input type='radio'  name='pdf12' id='pdf12'>";
	 document.getElementById("butt").innerHTML="Html <input type='radio' name='pdf12' id='11Html'>";
	 document.getElementById("voucherid").innerHTML="Send For Payment <input type='radio' name='payment' id='pay'> Sent All Payement <input type='radio' name='payment' id='allpay'> All Record <input type='radio' name='payment' id='all'><br>Cost Center<input type='text' name='costc' id='costc' value=''>";
	 
} else if(type == "4"){
	 document.getElementById("report").innerHTML="<select name='moduleno' id='moduleno' onchange='calreportno();'>"
        +"<option value='103' selected='selected'>Driver Activation/Deactivate Driver</option>"
		+"<option value='204'>Driver Allocation Update From Broadcast</option>"
		+"<option value='203'>Driver Allocation Update From Dashboard</option>"
		+"<option value='202'>Driver Allocation From Broadcast</option>"
		+"<option value='201'>Driver Allocation From open Request</option>"
		+"<option value='206'>Driver Remove From Broadcast</option>"
		+"<option value='205'>Driver Remove From Non Allocated Open Request</option>"
		+"<option value='204'>Forget Password</option>"
		+"<option value='208'>Remove Driver from Zone</option>"
		+"<option value='207'>Move Driver From Zone</option>"
		+"<option value='102'>Payment Settlement Schedule Update</option>"
		+"</select>";
	  }else {
	 document.getElementById("report").innerHTML="";
	 document.getElementById("bb").innerHTML="";
	 document.getElementById("aa").innerHTML="";
	 document.getElementById("butt").innerHTML="";
}
}
function callReport(action){
	try{
		if(document.getElementById("Dcode").value == "0"){
		var temp  = document.reportview.Driverid.value;
		}else if(document.getElementById("Dcode").value == "1") {		
		var temp  = document.getElementById("Driverid1").value;
		}
		}catch(e){}
	
	var fromdate = "";
	var todate = "";
	var code = "";
	var Drivercode = "";
   fromdate = document.getElementById("start").value;
	todate = document.getElementById("end").value;
	var fmat="Html";
	try{
	if(document.getElementById("pdf12").checked == true){
		fmat="pdf";
	}else if(document.getElementById("11Html").checked == true){
		fmat="html";
	}
	}catch(e){}
	code =document.getElementById("code").value;
	Drivercode =document.getElementById("Dcode").value;
	var type = document.getElementById("type").value;
	if(fromdate != "" && todate != "" && type != "0") {
		fromdate = fromdate.substring(6,10)+fromdate.substring(0, 2)+fromdate.substring(3, 5);
		todate = todate.substring(6,10)+todate.substring(0, 2)+todate.substring(3, 5);
		if(Number(fromdate)>Number(todate)) {
			alert("Pls Check FromDate And Todate");
		}else {
					
					if(type=="1"){
						window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/DriverSetteledSummary.rptdesign&__format=html&st_date='+fromdate+'&end_date='+todate+'&asscode='+code);
					  } else if(type=="2") {
						if(temp == ""){
				 						alert("You Must Provide The Driver Id");
						} else if(document.getElementById("Dcode").value == "0") {
							window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/DriverSettledDetails_Each.rptdesign&__format=html&stdate='+fromdate+'&enddate='+todate+'&driverid='+temp+'&type= Driver Detailed Report');
						}else if(document.getElementById("Dcode").value == "1"){
							window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/DriverSettledDetails_Each.rptdesign&__format=html&stdate='+fromdate+'&enddate='+todate+'&driverid='+temp+'&type= Driver Detailed Report');
						}
					} else if(type=="3"){
						if(document.getElementById("all").checked){
							window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/Voucher.rptdesign&__format='+fmat+'&stdate='+fromdate+'&enddate='+todate+'&asscode='+code);
						} else if(document.getElementById("pay").checked)	{
							doVoucherUpdate();
						} else if(document.getElementById("allpay").checked)	{
							window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/Voucher.rptdesign&__format='+fmat+'&stdate='+fromdate+'&enddate='+todate+'&asscode='+code);
						}
					} else if(type=="4") {
						if(reportno == '103')
							window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/AuditReport.rptdesign&__format=pdf&stdate='+fromdate+'&enddate='+todate+'&asscode='+code+'&DRACDEAC= and ( MODULE = "103" or MODULE = "100" )');
						else
							window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/AuditReport.rptdesign&__format=pdf&stdate='+fromdate+'&enddate='+todate+'&asscode='+code+'&DRACDEAC= and MODULE ='+reportno+'');
					} else if(type=="5") {
						window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/ManualCreditcart Summaryrptdesign.rptdesign&__format=pdf&stdate='+fromdate+'&enddate='+todate+'&assocode='+code);
					}
					 else if(type=="6") {
						 window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/cronejobTEST.rptdesign&__format=pdf&stdate='+fromdate+'&enddate='+todate+'&asscode='+code);	
					}
					 else if(type=="7") {
						 window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/Cabmaintanance.rptdesign&__format=pdf&stdate='+fromdate+'&enddate='+todate+'&asscode='+code);	
					}
					 else if(type=="8") {
							window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/DLoginDetailsrptdesign.rptdesign&__format=pdf&stdate='+fromdate+'&enddate='+todate+'&asscode='+code);	
					}	
					 else if(type=="9") {
							window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/Uncapturedperday.rptdesign&__format=pdf&stdate='+fromdate+'&enddate='+todate+'&assocode='+code); 
					}	
					 
			}
		} else {
		alert("You must Provide all Values");
	}
 }
</script>


</head>
<body>
<form name="reportview" action="control" method="get">
    <input type="hidden" id="context" value="<%=request.getContextPath()%>"/>	
	<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
	<table id="pagebox">
		<tr>
			<td>
	<table id="bodypage">
			<tr>
				<td align="center">
					<div id="title" >
						<h2>Report</h2>
					</div>
				</td>
			</tr>
			<tr>
			<td>
			<table id="bodypage">
			<tr>
				<td align="center" colspan="7" >
					
				</td>
			</tr>
			 <tr>
				<td colspan="7" align="center" >
				<select name="type" id="type" onchange="cal();">
					<option value="0">Select</option>
				<%if(adminBO.getRoles().contains("4001")) { %>
					<option value="1">Driver Settled Summary</option>
					<%} %>
					<%if(adminBO.getRoles().contains("4002")) { %>
					<option value="2">Driver Individual Settled Summary</option>
					<%} %>
					<%if(adminBO.getRoles().contains("4003")) { %>
					<option value="3">Voucher Summary</option>
					<%} %>
					<%if(adminBO.getRoles().contains("4004")) { %>
					<option value="4">Audit Summary</option>
					<%} %>
					<%if(adminBO.getRoles().contains("4005")) { %>
					<option value="5">Manual Credit Card Summary</option>
					<%} %>
					<%if(adminBO.getRoles().contains("4006")) { %>
					<option value="6">schedule JOB Report</option>
					<%} %>
					<%if(adminBO.getRoles().contains("4007")) { %>
					<option value="7">CAB Maintenance Report</option>
					<%} %>
					<%if(adminBO.getRoles().contains("4008")) { %>
					<option value="8">Driver Login Report</option>
					<%} %>
					<%if(adminBO.getRoles().contains("4009")) { %>
					<option value="9">Uncaptured Credit card Details</option>
					<%} %>
					</select>
			</td>
			</tr>
			 <tr>
				<td colspan="7" id="report" align="center" >
				</td>
			</tr>
			
			<tr>
				  <td>		
					<input type="hidden" name="Dcode" id="Dcode" size="10" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equals("Driver") ? "1":"0" %>"/>
			 		<input type="hidden" name="code" id="code" size="10" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>"/>
				 </td>
				<td colspan="6" >
					 <input type="hidden" name="Driverid1" id="Driverid1" size="10" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getUid() == null ? "0":((AdminRegistrationBO)session.getAttribute("user")).getUid() %>"/>
				</td>
				
			</tr>
			<tr>
				 <td colspan="7"></td>
			</tr>
			
			<tr>
				<td>From&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  name="sdate" id="start" size="10" onfocus="showCalendarControl(this);"  readonly="readonly"/>
				
					<iframe width="20"  height="178"  name="gToday:normal:agenda.startdate" id="gToday:normal:agenda.startdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
					</iframe>
				</td>
				<td>&nbsp;</td>
				<td>To&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
					<input type="text" name="edate" id="end" size="10" onfocus="showCalendarControl(this);" readonly="readonly"/>
				
					<iframe width="20"  height="178"  name="gToday:normal:agenda.enddate" id="gToday:normal:agenda.enddate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
					</iframe>
				</td>
			</tr>
			<tr>
				<td>
					<div id="bb"></div>
				</td>
				<td>&nbsp;</td>
				<td>
					<div id="aa"></div>
				</td>
				<td colspan="3">
					<div id="butt"></div>
				</td>
			</tr>
			<tr>
			 	<td colspan="3">
			 		<div id='voucherid'>
			 		
			 		</div>
				</td>
			</tr>
			<tr>
				<td colspan="7" align="center" >
				<input type="button" name= "button" id= "button" value="Generate Report" onclick="callReport(this)"/>
				</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	</td>
	</tr>
	</table>
	</form>
</body>
</html>
