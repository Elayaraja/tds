<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.VoucherBO"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.charges.bean.ChargesBO"%>
<%@page import="java.util.ArrayList"%>

<%
	ArrayList<ChargesBO> chargeType =(ArrayList<ChargesBO>)request.getAttribute("chargeTypes");
%>
<jsp:useBean id="voucherBo" class="com.tds.tdsBO.VoucherBO"
	scope="request" />
<%
	if(request.getAttribute("voucherentry") != null){
		voucherBo = (VoucherBO)request.getAttribute("voucherentry");
	}
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
function grantTheAcessToAssistant(){
	var email = document.getElementById("grantUser").value;
	var accountNo = document.getElementById("vno").value;
	if(accountNo !=null&&accountNo!=""&&email!=null&&email!=""){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {

		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ="finance?event=grantAccessToAssistant&emailId="+email+"&accountNo="+accountNo;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="true"){
		alert("permission granted for the userID");
		document.getElementById("grantUser").value="";
		document.getElementById("vno").value="";
	}
	}else{
		alert("Oops,Check Your AccountNo and EmailID !!!!");
	}
}
function loadSpecialFlags(){
	var xmlhttp=null;
	  document.getElementById("driverProfileByAjax").style.display="block";
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var driverCounter=0;
//	var vehicleCounter=0;
	var url='SystemSetupAjax?event=getFlagsForCompany';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	
	var text = xmlhttp.responseText;
	var jsonObj = "{\"flagDetails\":"+text+"}";
	var obj = JSON.parse(jsonObj.toString());
	document.getElementById("driverProfileByAjax").innerHTML="";
	var $tbl = $('<table style="width:100%" >').attr('id', 'flagValues');
	$tbl.append($('<td style="font-weight: bolder" align="center">').text('Special Flags'));	
	for(var j=0;j<obj.flagDetails.length;j++){
			$tbl.append($('<td>').append(
				$('<td>').text(obj.flagDetails[j].LD).append($('<input/>').attr('type', 'checkbox').attr('name', 'dchk'+driverCounter).attr('id', 'dchk'+driverCounter).val(obj.flagDetails[j].K).attr('onclick',"checkGroup('"+driverCounter+"','"+obj.flagDetails[j].GI+"','"+obj.flagDetails[j].LD+"')")),
				$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vgroup'+driverCounter).attr('id','vgroup'+driverCounter).val(obj.flagDetails[j].GI)),
				$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vLD'+driverCounter).attr('id','vLD'+driverCounter).val(obj.flagDetails[j].LD))
			));
			driverCounter=Number(driverCounter)+1;
	}
	document.getElementById("drProfile").value=driverCounter;
	$("#driverProfileByAjax").append($tbl);
	<%if(request.getAttribute("voucherentry") != null){%>
		checkFlagsInUpdate();
	<%}%>
}
function checkFlagsInUpdate(){
	var driverList=document.getElementById("drProfile").value;
	var drFlag = "<%=voucherBo.getSpecialFlags()%>";
	for (var i=0; i<driverList;i++){
		if(drFlag.indexOf(document.getElementById('dchk'+i).value) >= 0){
			document.getElementById('dchk'+i).checked=true;
		}
	}
}
function checkGroup(row,groupId,longDesc){
	var drProf=document.getElementById("drProfile").value;
	if(document.getElementById("dchk"+row).checked==true){
		for(var i=0;i<drProf;i++){
			if(document.getElementById("dchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && groupId!="0" && groupId!="" && i!=row){
				document.getElementById("dchk"+row).checked=false;
				 alert("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
				 break;
			}
		}
	}
}
function calVoucherCharges() {
	var i = Number(document.getElementById("chargeSize").value)+1;
	document.getElementById('chargeSize').value = i;
	var table = document.getElementById("chargesTable");
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);

	var element1 = document.createElement("select");
	element1.name = "chargeKey" + i;
	element1.id = "chargeKey" + i;
	<%if(chargeType!=null && chargeType.size()>0){
		for(int i=0;i<chargeType.size();i++){%>
	var theOption = document.createElement("option");
	theOption.text = "<%=chargeType.get(i).getPayTypeDesc()%>";
	theOption.value = "<%=chargeType.get(i).getPayTypeKey()%>";
		element1.options.add(theOption);
<%}}%>
	cell1.appendChild(element1);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "chargeAmount" + i;
		element2.id = "chargeAmount" + i;
		element2.size = '7';
		element2.onchange = function() {
			callAmount();
		};
		cell2.appendChild(element2);
	}
	function delVoucher() {
		try {
			var table = document.getElementById("chargesTable");
			var rowCount = table.rows.length;
			var size = document.getElementById('chargeSize').value;
			if (Number(size) > 0) {
				var temp = (Number(size) - 1);
				document.getElementById('chargeSize').value = temp;
				rowCount--;
				table.deleteRow(rowCount);
			}
		} catch (e) {
			alert(e.message);
		}

	}
	function clearAll() {
		var amount = document.getElementById("fixedAmount").value;
		if (amount != "") {
			var table = document.getElementById("chargesTable");
			var rowCount = table.rows.length;
			var size = document.getElementById('chargeSize').value;
			for (var i = 0; i < Number(size); i++) {
				rowCount--;
				table.deleteRow(rowCount);
			}
			document.getElementById("chargeSize").value = "";
			calVoucherCharges();
			document.getElementById("chargeKey" + 1).value = "1";
			document.getElementById("chargeAmount" + 1).value = amount;
		}
	}
	
	
	
	function addcreditcard()
	{
		//alert("enter");
		var  CCNumber= document.getElementById("ccNumber").value;
		//alert(CCNumber);
		 var exdate = document.getElementById("expDate").value;
		// alert(exdate);
	     var cvcode = document.getElementById("cvvCode").value;
	     //alert(cvcode);
		 var nameC = document.getElementById("nameCC").value;
		 //alert(nameC);
		var zipCode = document.getElementById("zipCodeCC").value;
		 //alert(zipCode);
	    var transtype = document.getElementById("transtype").value;
	    
	    var customerKey=document.getElementById("vno").value;
	  //  alert(customerKey);
	    
	    var customerName=document.getElementById("cName").value;
	    //alert(customerName);
		 

    if(CCNumber !=null&&CCNumber!=""&&exdate!=null&&exdate!=""&&cvcode!=null&&cvcode!=""&&nameC!=null&&nameC!=""&&zipCode!=null&&zipCode!=""&&transtype!=null&&transtype!=""){
			 
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url ="CustomerServiceAjax?event=creditCardDetails&customerKey="+customerKey+"&customerName="+customerName+"&cardNumber="+CCNumber+"&&expiryDate="+exdate+"&cvvCode="+cvcode+"&nameOnCard="+nameC+"&zipcodeCC="+zipCode+"&transType="+transtype;
		alert(url);
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		alert("text"+text);
		document.getElementById("infokey").value=text;
		if(text!=null){
			document.getElementById("grantUser").value="";
			closecreditcardscreen();
			document.getElementById("btncreditcard").disabled=true;
				}
    }else{
    	alert("Oops,All are Mandatory Fields !!!!");

    }
	}

	if($('#preAuthCC').is(':visible')){
		if(screenValue=="10"){	
			$("#preAuthCC" ).dialog( "close" );
			$("#preAuthCC").hide();
		} else {
			$("#preAuthCC").jqmHide();
		}
	}
	
	  function closecreditcardscreen(){
		    $("#preAuthCC").jqmHide();
	  }
	
	function showCreditCardScreen(){
/* 	    if ($("#preAuthCC").css('display') == 'none'){
	        $("#preAuthCC").show();
	        $("#btnClick").val("Hide");
	    }
	    else{
	        $("#preAuthCC").hide();
	        $("#button").val("close");
	    }
 */
 	$("#preAuthCC").jqm();
 	$("#preAuthCC").jqmShow();
	}


	function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
</script>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="Ajax/Voucher.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<script src="js/CalendarControl.js" language="javascript"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="jqueryNew/bootstrap.js"></script>
<script type="text/javascript" src="jqueryNew/bootstrap.min.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<style>
h2{
	font-family: oblique;
	color: grey;
}

#foo {
	font-weight: bold;
	color: blue;
}
</style>
<script type="text/javascript">
	function showBillingAddress() {
		if (document.getElementById("differentAddress").checked == true) {
			$("#differentBillingAddress").show('slow');
		} else {
			$("#differentBillingAddress").hide('slow');
			document.getElementById(differentBillingAddress).value = "";
		}
	}
	function checkBillingAddress() {
		if (document.getElementById("billingAddress").value != "") {
			$("#differentBillingAddress").show('slow');
			document.getElementById("differentAddress").checked = true;
		}
	}
	function deleteVoucher() {
		var deleteVoucher = confirm("All client accoutns regarding this Voucher will be changed");
		if (deleteVoucher == true) {
			return true;
		} else {
			return false;
		}
	}
	function validateEmailId() {
		showProcess();
		var emailField = document.getElementById("emailId");
		if (emailField.value != "") {
			var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			var address = emailField.value;
			if (reg.test(address) == false) {
				alert('Invalid Email Address');
				/* emailField.focus();
				emailField.value = ""; */
				return false;
			}
		}
	}
	checked = false;
	function changeDelayDays() {
		var form = document.masterForm;
		if (checked == false) {
			checked = true;
		} else {
			checked = false;
		}
		if (checked) {
			document.getElementById("vdelay").readOnly = true;
			document.getElementById("vdelay").value = -1;
		} else {
			document.getElementById("vdelay").readOnly = false;
			document.getElementById("vdelay").value = "";
		}
	}
	function checkDelayDays() {
		if (document.getElementById("vdelay").value != null
				&& document.getElementById("vdelay").value != "") {
			if (document.getElementById("vdelay").value == -1) {
				document.getElementById("receiptPaid").checked = checked;
				document.getElementById("vdelay").readOnly = true;
			}
		}
	}

	$(document).ready(function() {
		loadSpecialFlags();
		if (
<%=voucherBo.isDontExpire()==true%>
	) {
			document.getElementById("dontExpire").checked = true;
		}
		if (
<%=voucherBo.isOnlyForSR()==true%>
	) {
			document.getElementById("onlyForSR").checked = true;
		}
		$("#telNumber").keypress(function(e) {
			//if the letter is not digit then display error and don't type anything
			if (e.which != 8 && e.which != 0 && (e.which<48 || e.which>57)) {
				//display error message
				return false;
			}
		});
		$("#faxNumber").keypress(function(e) {
			//if the letter is not digit then display error and don't type anything
			if (e.which != 8 && e.which != 0 && (e.which<48 || e.which>57)) {
				//display error message
				return false;
			}
		});
		$("#vdelay").keypress(function(e) {
			//if the letter is not digit then display error and don't type anything
			if (e.which != 8 && e.which != 0 && (e.which<48 || e.which>57)) {
				//display error message
				return false;
			}
		});
		getChargesVoucher();
		checkDelayDays();
		checkBillingAddress();
<%if(request.getAttribute("modify")==null){%>
	clearAll();
<%}%>
	});
</script>
</head>
<%!String status[] = { "OneTime", "Unlimited" };%>
<body>
	<form method="post" action="control" name="masterForm"
		<%if (request.getAttribute("modify") != null) {%>
		onsubmit="return deleteVoucher();return validateEmailId();"
		<%} else {%> onsubmit="return validateEmailId();" <%}%>>
		<input type="hidden" name="module" id="module"
			value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />
		<input type="hidden" name="<%=TDSConstants.actionParam%>"
			value="<%=TDSConstants.registrationAction%>" /> <input type="hidden"
			name="<%=TDSConstants.eventParam%>"
			value="<%=TDSConstants.saveVoucher%>" /> <input type="hidden"
			id="assoccode"
			value="<%=((AdminRegistrationBO) session.getAttribute("user"))
					.getAssociateCode()%>" />
		<input type="hidden" name="finanical" value="3" /> <input
			type="hidden" name="updatCharges" id="updatCharges"
			value="<%=request.getAttribute("availCharges") == null ? ""
					: request.getAttribute("availCharges")%>" />
		<input type="hidden" name="chargeSize" id="chargeSize" value="" />
		<input type="hidden" name="drProfile" id="drProfile" value="" />
         	<input type="hidden" name="infokey" id="infokey" value="" />
         

		<div>

			<div></div>
		</div>

		<div>
			<div>
				<%
					if (request.getAttribute("modify") == null) {
				%>
				<h2>
					<center>Account Creation</center>
				</h2>
				<%
					} else {
				%>
				<h2>
					<center>Update Account</center>
				</h2>
				<%
					}
				%>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<%
						String error = "";
						if (request.getAttribute("errors") != null) {
							error = (String) request.getAttribute("errors");
						}
					%>

					<tr>
						<td colspan="7">
							<div id="errorpage">
								<%=error.length() > 0 ? "You must fulfilled the following error<br>"
					+ error
					: ""%>
							</div>
							<table id="bodypage" width="540">
								<tr>
									<td>Account&nbsp;No:*</td>
									<td><input type="text" size="15" name="vno" id="vno"
										value="<%=voucherBo.getVno()%>" /> <input type="hidden"
										value="<%=voucherBo.getVno()%>" name="voucherTrue"
										id="voucherTrue" /></td>
									<td>Format:</td>
									<td><select name="voucherFormat">
											<option value="1"
												<%if (voucherBo.getVoucherFormat() != null) {%>
												<%=voucherBo.getVoucherFormat().equalsIgnoreCase("1") ? "Selected"
						: ""%>
												<%}%>>Format1</option>
											<option value="2"
												<%if (voucherBo.getVoucherFormat() != null) {%>
												<%=voucherBo.getVoucherFormat().equalsIgnoreCase("2") ? "Selected"
						: ""%>
												<%}%>>Format2</option>
									</select></td>
								</tr>

								<tr>
									<td>Frequency:</td>
									<td><select name="vfreq">
											<option value="1"
												<%=voucherBo.getVfreq().equalsIgnoreCase("1") ? "Selected"
					: ""%>><%="One Time"%></option>
											<option value="2"
												<%=voucherBo.getVfreq().equalsIgnoreCase("2") ? "Selected"
					: ""%>><%="Multiple Uses"%></option>
									</select></td>
									<td>Max&nbsp;Amount:*</td>
									<td><input type="text" size="15" name="vamount"
										id="vamount" value="<%=voucherBo.getVamount()%>"
										onblur="insert(id);" /></td>
								</tr>
								<tr>
									<td>Fixed&nbsp;Amount:</td>
									<td><input type="text" size="15" name="fixedAmount"
										id="fixedAmount" value="<%=voucherBo.getFixedAmount()%>"
										onblur="clearAll()" /></td>
									<td>No Tip:</td>
									<td><input type="checkbox" name="noTip" id="noTip"
										value="1" <%=voucherBo.getNoTip() == 1 ? "checked" : ""%> /></td>

								</tr>
								<tr>
									<td>Company&nbsp;Code:</td>
									<td><input type="text" size="15" id="vcode" name="vcode"
										value="<%=voucherBo.getVcode()%>" onblur="loadCompany()">
									</td>
									<td>Cost&nbsp;Center:</td>
									<td><input type="text" size="15" id="vcostcenter"
										name="vcostcenter" value="<%=voucherBo.getVcostcenter()%>"
										autocomplete="off" class="form-autocomplete"> <ajax:autocomplete
											fieldId="vcostcenter" popupId="model-popup1"
											targetId="vcostcenter" baseUrl="autocomplete.view"
											paramName="vcostcenter" className="autocomplete"
											progressStyle="throbbing" /></td>
								</tr>

								<tr>
									<td>Contact Information:</td>
									<td><input type="text" size="15" name="vcontact"
										value="<%=voucherBo.getVcontact()%>"></td>
									<td>Expire&nbsp;Date:</td>
									<td><input type="text" name="vexpdate" id="vexpDate"
										size="15" value="<%=voucherBo.getVexpdate()%>"
										onfocus="showCalendarControl(this);" readonly="readonly">
									</td>
								</tr>
								<tr>
									<td>Don't Expire:</td>
									<td><input type="checkbox" size="15" name="dontExpire"
										id="dontExpire" onclick="dontExpire();"></td>
									<td>Only for SR:</td>
									<td><input type="checkbox" name="onlyForSR" id="onlyForSR"
										size="15" /></td>
								</tr>
								<tr>
									<td>Description:</td>
									<td><textarea rows="3" cols="12" name="vdesc"><%=voucherBo.getVdesc()%></textarea>
									</td>
									<td>Payment Delay Date:*</td>
									<td><input type="text" name="vdelay" id="vdelay" size="15"
										value="<%=voucherBo.getVdelay()%>"></td>
			<%-- 			 		    <td  id="creditcard"><input  type="button" <%=(voucherBo.getInfokey()!=null && !voucherBo.getInfokey().equals(""))?"disabled=disabled":"" %> name="creditcard" id="btncreditcard" value="creditcard" size="15" onclick="showCreditCardScreen()"></td>
								<td>Process instantly</td>
									<td><input type="checkbox" size="15" name="processins"
										id="processins" onclick="processinstantly();"></td>
									<td>continue on decline</td>
									<td><input type="checkbox" name="decline" id="decline"
										size="15" /></td>
			
		             	            </td>
		                		<td>payment Type</td>
									<td><input type="checkbox" name="paytype" id="paytype"
										size="15" /></td>
			
		             	            </td>
		                                                								
			 --%>			
								
								
								
								
								</tr>

								<tr>
									<td>Diff. billing address:</td>
									<td><input type="checkbox" name="differentAddress"
										id="differentAddress" onclick="showBillingAddress()"></input>
										<c id="differentBillingAddress" style=" display: none"> <input
											type="text" name="billingAddress" id="billingAddress"
											size="7" value="<%=voucherBo.getBillingAddress()%>"></input>
										</c></td>
									<td>On rcpt of Payment:</td>
									<td colspan="2"><input type="checkbox" name="receiptPaid"
										id="receiptPaid" value="" onclick="changeDelayDays()"></input>
									</td>

								</tr>
								<tr>
									<td>Company Name:</td>
									<td><input type="text" name="cName" id="cName" size="15"
										value="<%=voucherBo.getCompanyName()%>"></input></td>
									<td>Company Address:</td>
									<td><input type="text" name="cAddress" size="15"
										value="<%=voucherBo.getCompanyAddress()%>"></input></td>
								</tr>

								<tr>
									<td>Contact Person:</td>
									<td><input type="text" name="contactPerson" size="15"
										value="<%=voucherBo.getContactName()%>"></input></td>
									<td>Contact Title:</td>
									<td><input type="text" name="contactTitle" size="15"
										value="<%=voucherBo.getContactTitle()%>"></input></td>
								</tr>
								<tr>
									<td>Telephone Number:</td>
									<td><input type="text" name="telNumber" id="telNumber"
										size="15" value="<%=voucherBo.getTelephoneNumber()%>"></input>
									</td>
									<td>Fax Number:</td>
									<td><input type="text" name="faxNumber" id="faxNumber"
										size="15" value="<%=voucherBo.getFaxNumber()%>"></input></td>
								</tr>
								<tr>
									<td>Email Id:</td>
									<td><input type="text" name="emailId" id="emailId"
										size="15" value="<%=voucherBo.getEmailAddress()%>"></input></td>
									<td>Email - Invoice Process:</td>
									<td>
									<%if (request.getAttribute("modify") == null) {%> 
									<select name="emailStatus">
											<option value="0">Manual</option>
											<option value="1">Automatic</option>
									</select> 
									<%} else {%> 
									<select name="emailStatus">
										<option value="1" <%=voucherBo.getEmailStatus() == 1 ? "selected":""%>>Automatic</option>
										<option value="0" <%=voucherBo.getEmailStatus() == 0 ? "selected": ""%>>Manual</option>
									</select> 
									<%}%>
									</td>
								</tr>
								<tr>
									<td>Start Amount:</td>
									<td><input type="text" name="sAmount" id="sAmount"
										size="15"
										value="<%=voucherBo.getStartAmount() == null ? "" : voucherBo
					.getStartAmount()%>"></input>
									</td>
									<td>Rate Per Mile:</td>
									<td><input type="text" name="ratePerMile" id="ratePerMile"
										size="15"
										value="<%=voucherBo.getRatePerMile() == null ? "" : voucherBo
					.getRatePerMile()%>"></input>
									</td>
								</tr>
								<tr>
								<td>Rider Name:</td>
									<td><input type="text" name="vname" id="vname" size="15"
										value="<%=voucherBo.getVname()%>"></input></td>
									<td>Status:</td>
									<td>
										<%
											if (request.getAttribute("modify") == null) {
										%> <select name="status">
											<option value="1">Active</option>
											<option value="0">InActive</option>
									</select> <%
 	} else {
 %> <select name="status">
											<option value="1"
												<%=voucherBo.getActiveOrInactive() == 1 ? "selected"
						: ""%>>Active</option>
											<option value="0"
												<%=voucherBo.getActiveOrInactive() == 0 ? "selected"
						: ""%>>InActive</option>
									</select> <%
 	}
 %>
									</td>
									
								</tr>
								<tr>
								<td>Allocate Only To</td>
								<td><input type="text" name="allocateOnlyTo" id="allocateOnlyTo" value="<%=voucherBo.getAllocateOnlyTo()==null?"":voucherBo.getAllocateOnlyTo()%>">
								<ajax:autocomplete
				  					fieldId="allocateOnlyTo"
				  					popupId="model-popup1"
				  					targetId="allocateOnlyTo"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 
								</td>
								<td>Not Allocate To</td>
								<td><input type="text" name="notAllocateTo" id="notAllocateTo" value="<%=voucherBo.getNotAllocateTo()==null?"":voucherBo.getNotAllocateTo()%>">
								</td>
								</tr>
								<tr>
									<td>Driver Comments:</td>
									<td><textarea rows="3" cols="12" name="driverComments"><%=voucherBo.getDriverComments() == null ? "" : voucherBo
					.getDriverComments()%></textarea></td>
									<td>OperatorComments:</td>
									<td><textarea rows="3" cols="12" name="operatorComments"><%=voucherBo.getOperatorComments() == null ? "" : voucherBo
					.getOperatorComments()%></textarea></td>
								</tr>
					
								<tr>
									<td>CreditCard processing(%)</td>
									<td><input type="text" name="ccProcessPercentage" id="ccProcessPercentage" value="<%=voucherBo.getCcProcessPercentage()==0?"0":voucherBo.getCcProcessPercentage()%>"></td>
								</tr>
					
							         <table>           
									<tr><div id="driverProfileByAjax"></div>
									</tr></table>
										<tr><td>			
	                       	<label>Grant Permission To Assistant Operator E-mail ID</label> 
	                       	<table><tr><td><input type="email" name="grantUser" id="grantUser" value=""	 placeholder="Enter the Assistants Email ID !"  />
		                     <input type="button" name="" id="" value="Grant Access" onclick="grantTheAcessToAssistant()" /></td></tr></table>
	                          </td> </tr>                                                                                                                                              
	
						        	<table ><tr><th><label>Creditcard Registration</label></th></tr></table>
								<tr><td>PROCESS INSTANTLY
									<input type="checkbox" size="15" name="processins" id="processins" onclick="processinstantly();"></td></tr>
									<td>CONTINUE ON DECLINE
									<input type="checkbox" name="decline" id="decline" size="15" /></td>
			
		             	            <td>PAYMENT TYPE
									<input type="checkbox" name="paytype" id="paytype" size="15" /></td>
						          <td  id="creditcard"><input  type="button" <%=(voucherBo.getInfokey()!=null && !voucherBo.getInfokey().equals(""))?"disabled=disabled":"" %> name="creditcard" id="btncreditcard" value="CREDITCARD" size="15" onclick="showCreditCardScreen()"></td></tr></table>
			
	        
							</table>
											</td></tr>
											<table style="width: 50%;" border="0" background-color="#f8f8ff" class="nav-static-top" id="chargesTable">
									<tr>
										<th style="width: 35%; background-color: #d3d3d3;">Charge Type</th>
										<th style="width: 15%; background-color: #d3d3d3;">Amount</th>
									</tr>
								        </table>
								<tr>
									<td><input type="button" name="add" value="Add Row" onclick="calVoucherCharges()" /></td>
									<td><input type="button" name="remove" value="Remove" style="margin-left: 5%;" onclick="delVoucher()" /></td>
								
								
									
							
						</td>
					</tr>
	              <%
										if (request.getAttribute("modify") == null) {
									%>
									<td>
													<input type="submit" name="submit" value="Create Voucher"
														onclick="cal()">
									</td>
									<%
										} else {
									%>
									<td colspan="4" align="center"><input type="submit"
										name="update" id="update" value="Update Voucher" class="lft" />
										<input type="submit" name="delete" id="delete"
										value="Delete Voucher" class="lft" /></td>
								</tr>
								<%
									}
								%>
								
										
				</table>
			</div></div></form>
	
	
	<div id="preAuthCC" class="jqmWindow" style="display:none;height:330px;width:750px">
     <center><h1>Credit Card Details</h1></center>
     <table style="width: 100%;">
<tr>
<td>CC Number</td>
<td><input type="text" id="ccNumber" value=""   maxlength="16" size="16" pattern="[0-9]{16}" required></td>
<td>Expiry Date</td>
<td><input type="text" size="5" name="expDate"   id="expDate" value="" maxlength="4" size="4" required></td>
</tr>
<tr>
<td>CVV Code</td>
<td><input type="text" size="5" id="cvvCode" value=""  maxlength="3" size="3" required></td>
<td>Name</td>
<td><input type="text" id="nameCC" value="" pattern="[A-Za-z]"></td>
</tr>
<tr>
<td>ZipCode</td>
<td><input type="text" size="5" id="zipCodeCC" value=""   maxlength="5" size="5"></td>
</tr>
<tr><td>Action:</td>
	<td><select name="Transtype" id="transtype">
    <option value="Authorize">Authorize</option>
    <option value="Sale">Sale</option>
    <option value="Settle">Force</option>
   </select></td><tr>
<tr><td align="center"> 
</td></tr>
</table><div align="center"><input type="button" id="ccDetail" value="Process" onclick="addcreditcard()"/>
<input type="button" id="close" value="close" onclick=" closecreditcardscreen()" />
</div>	
	
</body>

</html>