<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>


<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.Address" %>
<%@page import="com.tds.cmp.bean.CustomerProfile" %>

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%
Address  add = (Address)request.getAttribute("add");   
CustomerProfile  cprofile = (CustomerProfile)request.getAttribute("cprofile"); 
ArrayList<Address> addHist =  (ArrayList<Address>) request.getAttribute("AddressHistory");
ArrayList<CustomerProfile> profHist =  (ArrayList<CustomerProfile>) request.getAttribute("ProfileHistory");
%> 
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
function getUserAddress(name,phone,masterKey){
	document.getElementById('Name').value="";
	document.getElementById('Number').value="";
	
	document.getElementById('Name').value  = name;
	document.getElementById('Number').value  = phone;
	document.getElementById('masterKey').value = masterKey;
	document.masterForm.button.click();
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>

<title >User Address Details</title>
</head>
<body>

<form  name="masterForm" style="margin-right:100px"  action="control" method="post" onsubmit="return showProcess()">

   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %><input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <input type="hidden" name="action" value="registration"/>
		<input type="hidden" name="event" value="custInfo"/>
		
		<input type="hidden" name="operation" value="2"/>
        
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
		<input type="hidden"  name="key"  value="  <%=request.getAttribute("key") %>" /> 
   
       <div>
       <div>
      <%String result="";
			if(request.getAttribute("page")!=null) {
			result = (String)request.getAttribute("page");
			}
			%>
			<div id="errorpage" style="">
			<%=result.length()>0? result:"" %>
			</div> 
       
			<c class="nav-header"><center>User Address Details</center></c>

<!-- <table width="100%" border="2" cellspacing="1" cellpadding="0" class="driverChargTab">
                <tr>
						<td> -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				   <tr align="center">
				     <td>Phone No:</td>
				     <td>  
			         <input type="text"  name="Number"  id="Number"  value="<%=cprofile.getNumber() %>" autocomplete="off"/>
			         </td>
                 	         <td>Name</td>
                 	         <td>                   	    
                 	         <input type="text"  name='Name'  id="Name"  value="<%=cprofile.getName() %>" autocomplete="off"/>
                 	          <ajax:autocomplete
	  					fieldId="Name"
	  					popupId="model-popup1"
	  					targetId="Name"
	  					baseUrl="autocomplete.view"
	  					paramName="UserName"
	  					className="autocomplete"
	  					progressStyle="throbbing" />
                 	</td>
                    </tr>
              		<tr align="center">                    
                    <td colspan="4">
                         	 <input type="submit"  name="button" style="background-color:#95B9C7;" id="button" value="Submit" />
                         	 <input type="submit"  name="Add" id="Add" style="background-color:#95B9C7;" value="Add Client" />
					</td>
				    </tr> 
                </table>
              
             <!--    </td>
         </tr>
    </table>    --> 
       
   <!--  <table width="100%" border="1" cellspacing="1" cellpadding="0" class="driverChargTab">
                    <tr>
                      <td> -->
                	   <%if(profHist.size()>0) { %>
                	   <center>  <h5>Client Details </h5></center>
                            <table width="100%" border="1" cellspacing="0" cellpadding="0" class="driverChargTab">
                                        <tr>
                                    	  <td  class="firstCol">Phone NO:</td> 
                                    	  <td  class="firstCol">Master Key</td> 
                                    	  <td  class="firstCol">Name</td> 
                                    	  <td  class="firstCol">Address1</td>
                                    	  <td  class="firstCol">Address2</td>	
                                    	  <td  class="firstCol">City</td>	
                                    	  <td  class="firstCol">State</td>	
                                    	  <td  class="firstCol">Zip</td>	
                                    	  <td  class="firstCol">Payment Type</td> 
                                    	  <td  class="firstCol">Account</td> 
                                    	  <td  class="firstCol">Customer Profile</td> 
                                    	  <td  class="firstCol">Last Four Digit</td> 
                                    	  <td  class="firstCol">Latitude</td> 
                                    	  <td  class="firstCol">Longitude</td> 
                                    	  <td  class="firstCol">Description</td>
                                    	  <td  class="firstCol">Premium Customer</td> 
                                    	  <td  class="firstCol">Corporate Code</td> 
                                    	  <td  class="firstCol">Customer No.</td> 
                      
                 <!--                    	  <td style="display:none"class="firstCol">CCprofile</td>
                                    	  <td style="display:none"class="firstCol">CClastfourdigits</td>
                                    	  <td style="display:none" style="display:none" class="firstCol">CCtype</td>
                                    	  <td style="display:none" class="firstCol">Latitude</td>
                                    	  <td style="display:none" class="firstCol">Longitude</td>
                  -->                       </tr>
                                       <%for(int i=0;i<profHist.size();i++) {                                    	
                                    	%>
		     						  
		     						  <%if(i%2==0) { %>
		     						  <tr>
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getNumber() %> </td> 
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getMasterKey() %> 
		     						    		     						    <input type="hidden" name="masterKey" id="masterKey" value="<%=profHist.get(i).getMasterKey() %>"/> </td>
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getName() %><input type='hidden' name='Name<%=(i)%>' id='Name<%=(i)%>' value=<%=profHist.get(i).getName() %>/> </td>  
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getAdd1() %> </td>
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getAdd2() %> </td>
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getCity() %> </td>
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getState() %> </td>
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getZip() %> </td>
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getPaymentType() %> </td> 
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getAccount() %> </td>
		     						    <input type="hidden" name="account" id="account" value="<%=profHist.get(i).getAccount() %>"/> 
		     						   
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getCCprofile() %> </td> 
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getCClastfourdigits() %> </td> 
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getLatitude() %> </td> 
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getLongitude() %> </td> 
		     		     				<td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getDescription() %> </td> 
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getPremiumcustomer().equals("1")?"Yes":"No" %> </td> 
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getCorporateCode() %> </td> 
		     						    <td  align="center"style="background-color:lightgreen" > <%=profHist.get(i).getCustomerNo() %> </td> 
	     						  
	                                   	 <%if(profHist.size()!=1){ %>
	                                   	 <td colspan="4" align="center">
						                  <div class="wid60 marAuto padT10">
                                             <div class="btnBlue">
                                               <div class="rht">
<%-- 	                                        	<input type="button"  name="edit" id="edit"   class="lft" value="Edit" onclick=" getUserAddress('<%=profHist.get(i).getName() %>','<%=profHist.get(i).getNumber() %>','<%=profHist.get(i).getMasterKey()%>')" /> 
 --%>												<a href="control?action=registration&event=custInfo&event2=edit&module=operationView&Edit=Yes&masterKey=<%=profHist.get(i).getMasterKey()%>">Edit</a>                                               
  </div>
                                              <div class="clrBth"></div>
                                            </div>
                                          <div class="clrBth"></div>
                    	                 </div>		
                                        </td>
                                        
                                        <%} %>  
                                        <%if(profHist.size()==1){ %>
                                        <td colspan="4" align="center">
						                  <div class="wid60 marAuto padT10">
                                             <div class="btnBlue">
                                               <div class="rht">
<!--                                                  <input type="submit"  name="deleteUserMaster" id="deleteUserMaster"   class="lft" value="Delete" />
 --> 												<a href="control?action=registartion&event=custInfo&event2=deleteMaster&module=operationView&masterKey=<%=profHist.get(i).getMasterKey()%>">Delete</a>        
                                     		
                                         <div class="wid60 marAuto padT10">
                                             
                                          <div class="clrBth"></div>
                    	                 </div>		
                                                <!--  deletion of customer profile not yet completed -->
                                              </div>
                                            <div class="clrBth"></div>
                                           </div>
                                         <div class="clrBth"></div>
                    	                </div>			
                                        </td>
                                        <td colspan="4" align="center">
						                  <div class="wid60 marAuto padT10">
                                             <div class="btnBlue">
                                               <div class="rht">
<!--                                                 <input type="submit"  name="updateUserMaster" id="updateUserMaster"   class="lft"  value="Update" />
 -->             									<a href="control?action=registration&event=custInfo&event2=updateMaster&module=operationview&masterKey=<%=profHist.get(i).getMasterKey() %>">Update</a>	           
                         </div>
                                              <div class="clrBth"></div>
                                            </div>
                                          <div class="clrBth"></div>
                    	                 </div>			
                                         </td>
                                        
                                        <%} %>   
                                        </tr>
                                       <%} else { %>
		     						  <tr>
		     						    <td  align="center" > <%=profHist.get(i).getNumber() %> </td> 
		     						    <td  align="center"> <%=profHist.get(i).getMasterKey() %> <input type="hidden" name="masterKey" value="<%=profHist.get(i).getMasterKey() %>"/> </td> 
		     						    
		     						    <td  align="center" > <%=profHist.get(i).getName() %><input type='hidden' name='Name<%=(i)%>' id='Name<%=(i)%>' value=<%=profHist.get(i).getName() %>/> </td>  
		     						    <td  align="center" > <%=profHist.get(i).getAdd1() %> </td>
		     						    <td  align="center" > <%=profHist.get(i).getAdd2() %> </td>
		     						    <td  align="center" > <%=profHist.get(i).getCity() %> </td>
		     						    <td  align="center" > <%=profHist.get(i).getState() %> </td>
		     						    <td  align="center"> <%=profHist.get(i).getZip() %></td>
		     						    <td  align="center" > <%=profHist.get(i).getPaymentType() %> </td> 
		     						    <td  align="center" > <%=profHist.get(i).getAccount() %> </td> 
		     						    <td  align="center"> <%=profHist.get(i).getCCprofile() %> </td> 
		     						    <td  align="center"> <%=profHist.get(i).getCClastfourdigits() %> </td> 
		     						    <td  align="center"> <%=profHist.get(i).getLatitude() %> </td> 
		     						    <td  align="center" > <%=profHist.get(i).getLongitude() %> </td> 
		     		     				<td  align="center"> <%=profHist.get(i).getDescription() %> </td> 
		     						    <td  align="center"> <%=profHist.get(i).getPremiumcustomer() %> </td> 
		     						    <td  align="center"> <%=profHist.get(i).getCorporateCode() %> </td> 
		     						    <td  align="center"> <%=profHist.get(i).getCustomerNo() %> </td> 
		     						    
		     						    
	                                   	 <%if(profHist.size()!=1){ %>
	                                   	 <td colspan="4" align="center">
						                  <div class="wid60 marAuto padT10">
                                             <div class="btnBlue">
                                               <div class="rht">
												<a href="control?action=registration&event=custInfo&event2=edit&module=operationView&Edit=Yes&masterKey=<%=profHist.get(i).getMasterKey()%>">Edit</a>                                                     </div>
                                              <div class="clrBth"></div>
                                            </div>
                                          <div class="clrBth"></div>
                    	                 </div>		
                                        </td>
                                        
                                        <%} %>  
                                        <%if(profHist.size()==1){ %>
                                        <td colspan="4" align="center">
						                  <div class="wid60 marAuto padT10">
                                             <div class="btnBlue">
                                               <div class="rht">
                                                 <input type="submit"  name="deleteUserMaster" id="deleteUserMaster"   class="lft" value="Delete" />
                                                <!--  deletion of customer profile not yet completed -->
                                              </div>
                                            <div class="clrBth"></div>
                                           </div>
                                         <div class="clrBth"></div>
                    	                </div>			
                                        </td>
                                        <td colspan="4" align="center">
						                  <div class="wid60 marAuto padT10">
                                             <div class="btnBlue">
                                               <div class="rht">
                                                <input type="submit"  name="updateUserMaster" id="updateUserMaster"   class="lft" value="Update" />
                                                </div>
                                              <div class="clrBth"></div>
                                            </div>
                                          <div class="clrBth"></div>
                    	                 </div>			
                                         </td>
                                        
                                        <%} %>   
                                        </tr>
                                    	 
                                    	<% }}}%>
                    </table>
                  
                                    <!-- 	</td>
                                    	</tr> -->
                     <% if(profHist.size()==1){ %>
                    <center><h5>Address Details </h5></center>  
                    <!--   <tr><td> -->                            	
                                     <table width="100%" border="1" cellspacing="1" cellpadding="0" class="driverChargTab">
                                    	 
                                     
                                      <tr>
                                    	<td  class="firstCol" style="display:none">Phone NO:</td> 
                                    	<td  class="firstCol">Name</td> 
                                    	<td  class="firstCol">Address1</td>
                                    	<td  class="firstCol">Address2</td>	
                                    	<td  class="firstCol">CITY</td>
                                    	<td  class="firstCol">State</td>
                                    	<td  class="firstCol">Zip</td>
                                    	<td  class="firstCol" style="display:none">Latitude</td>
                                    	<td  class="firstCol" style="display:none">Longitude</td>
                                     </tr>
                                    	<%for(int i=0;i<addHist.size();i++) {                                    	
                                    	%>
		     						  
		     				<%-- 		  <%if(i%2==0) { %> --%>
		     						  <tr>
		     						    <td  align="center"style="background-color:pink;display:none;" > <%=addHist.get(i).getNumber() %> </td>    
		     						    <td  align="center"style="background-color:pink" > <%=addHist.get(i).getName() %><input type='hidden' name='Name<%=(i)%>' id='Name<%=(i)%>' value=<%=profHist.get(i).getName() %>/> </td>  
		     						    <td  align="center"style="background-color:pink" > <%=addHist.get(i).getAdd1() %> </td>
		     						    <td  align="center"style="background-color:pink" > <%=addHist.get(i).getAdd2() %> </td>
                                    	<td  align="center" style="background-color:pink" ><%=addHist.get(i).getCity()%> </td>
                                    	<td  align="center" style="background-color:pink"> <%=addHist.get(i).getState()%></td> 
                                    	<td  align="center" style="background-color:pink"> <%=addHist.get(i).getZip()%></td>
                                    	<td  align="center"style="background-color:pink ;display:none;" ><%=addHist.get(i).getLatitude()%> </td>
                                    	<td  align="center"style="background-color:pink ;display:none;" ><%=addHist.get(i).getLongitude()%> </td>
                                    	<td colspan="1" align="center">
						                  <div class="wid60 marAuto padT10">
                                             <div class="btnBlue">
                                               <div class="rht">
                         	                     <input type="submit" style=" width: 50px; font-size: 10px;"  name="delete" id="delete"   class="lft" value="Delete" />
                       	                       </div>
                                              <div class="clrBth"></div>
                                             </div>
                                         <div class="clrBth"></div>
                    	                </div>			
					                   </td>
					                   <td colspan="1" align="center">
						                  <div class="wid60 marAuto padT10">
                                             <div class="btnBlue">
                                               <div class="rht">
                         	                     <input type="submit"  name="updateUserAdd" id="updateUserAdd"  class="lft" value="Update" />
                       	                       </div>
                                              <!-- <div class="clrBth">
                                                                       	                     <input type="submit"  name="updateUserAdd" id="updateUserAdd"  class="lft" value="Update" />
                                              </div> -->
                                             </div>
                                         <div class="clrBth"></div>
                    	                </div>			
					                   </td>
					                   
                                    </tr>
                                    <%--    <%} %> --%>
                                    	<% }}else if((profHist.size()==0 && request.getParameter("button")!=null &&(request.getParameter("Number")!="" || request.getParameter("Name")!=""))){ %>
                                    	 <font style="color:red;size:10">No records found</font>
                                	 <%}%>
                     <%if(profHist.size()==0 && request.getParameter("button")!=null &&(request.getParameter("Number")=="" && request.getParameter("Name")=="") ){ %>
                                   	 <font style="color:red;size:10">Fill atleast one field</font>
                                	 <%}%>
                                    	
                            </table>
               <!--  </td>
                </tr>
                </table> -->
              <div class="clrBth"></div>   
 
              <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 </div>
 </div>
 </form>
 </body>
</html> 
 