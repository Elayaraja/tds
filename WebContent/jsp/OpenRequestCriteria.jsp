<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO;"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function setPhone(p_phone) {
	if(p_phone.value.length == 3) {
		p_phone.value += "-";
	} else if(p_phone.value.length == 7) {
		p_phone.value += "-";
	}
}
</script>
</head>
<body>
<form name="openForm" action="control" method="post">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<jsp:useBean id="openRequestBO"   class="com.tds.tdsBO.OpenRequestBO" scope="request"  />
<% if(request.getAttribute("openrequest") != null)
	openRequestBO = (OpenRequestBO)request.getAttribute("openrequest");
	ArrayList al_open = null;
	if(request.getAttribute("openSummary") != null) 
		al_open = (ArrayList)request.getAttribute("openSummary");
	%>
<jsp:setProperty name="openRequestBO"  property="*"/>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.getOpenRequestCriteria %>">
<input type="hidden" name="dispatch" value="1">
 
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<h1>Open Request Criteria</h1>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
					<td>
					<table id="bodypage" width="540" >
				<tr>
				<td class="firstCol">Trip&nbsp;Id</td>
				<td>
				<input type="text" name="tripid" value="<%= openRequestBO.getTripid() %>">
				</td>
				<td class="firstCol">Phone&nbsp;No</td>
				<td>
				<input type="text" name="phone" value="<%= openRequestBO.getPhone() %>" onkeyup="setPhone(this)">
				</td>
				</tr>
				<tr>
				<td class="firstCol">Service&nbsp;Date</td>
				<td>
				<input type="text" name="sdate" value="<%= openRequestBO.getSdate() %>" onfocus="showCalendarControl(this);"  readonly="readonly">
				</td>
				<td class="firstCol">Starting&nbsp;City</td>
				<td>
				<input type="text" name="scity" value="<%= openRequestBO.getScity() %>">
				</td>
				</tr>
				<tr>
				<td class="firstCol">Starting&nbsp;State</td>
				<td>
				<input type="text" name="sstate" value="<%= openRequestBO.getSstate() %>">
				</td>
				<td class="firstCol">Starting&nbsp;ZipCode</td>
				<td>
				<input type="text" name="szip" value="<%= openRequestBO.getSzip() %>">
				</td>
				</tr>
				<tr>
				<td colspan="7" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="openrequest" class="lft" value="Search"/></div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
				</td>
				</tr>   
				</table>
                    </td></tr>
                    </table>
                    </div>    
              </div>
              <div class="rightCol padT10">
			<div class="rightColIn padT10">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
			<% if(al_open != null && al_open.size() >0  ) {  %>
			
			<tr>
				<th width="15%" class="firstCol">S.NO</th>
				<th width="20%">Service Date</th>
				<th width="15%">Service Time</th>
				<th width="40%">From Address</th>
				<th width="40%">To Address</th>
				<th width="30%">Phone No</th>
			</tr> 
			<% for(int count = 0; count < al_open.size(); count++) {  
				openRequestBO = (OpenRequestBO) al_open.get(count);
			%>
			<%if(count%2==0) { %>
			
			
			<tr>
				<td   align="left"><%= count+1 %> </td>
				<td ><%= openRequestBO.getSdate() %></td>
				<td><%= openRequestBO.getSttime() %></td>
				<td style="text-align: left"  ><%= openRequestBO.getSadd1()+","+openRequestBO.getSadd2()+","+openRequestBO.getScity()+","+openRequestBO.getSstate()+","+openRequestBO.getSzip() %></td>
				<td style="text-align: left" ><%= openRequestBO.getEadd1()+","+openRequestBO.getEadd2()+","+openRequestBO.getEcity()+","+openRequestBO.getEstate()+","+openRequestBO.getEzip() %></td>
				<td>
					<% if(request.getAttribute("btn") != null && request.getAttribute("btn").equals("openrequest") ) {%>
						<a href="control?action=openrequest&event=getopenCriteria&module=dispatchView&openrequest=Change Open Request&tripid=<%= openRequestBO.getTripid() %>" ><%= openRequestBO.getPhone() %></a>
					<% } else { %>
						<a href="control?action=openrequest&event=getopenCriteria&module=dispatchView&acceptrequest=Change Open Request&tripid=<%= openRequestBO.getTripid() %>" ><%= openRequestBO.getPhone() %></a>
					<% } %>
				</td>
			</tr>
			<%} else { %>
			
			<tr     >
				<td style="background-color:lightgreen"><%= count+1 %> </td>
				<td style="background-color:lightgreen" ><%= openRequestBO.getSdate() %></td>
				<td style="background-color:lightgreen"><%= openRequestBO.getSttime() %></td>
				<td style="background-color:lightgreen;text-align:left" ><%= openRequestBO.getSadd1()+","+openRequestBO.getSadd2()+","+openRequestBO.getScity()+","+openRequestBO.getSstate()+","+openRequestBO.getSzip() %></td>
				<td style="background-color:lightgreen;text-align:left" ><%= openRequestBO.getEadd1()+","+openRequestBO.getEadd2()+","+openRequestBO.getEcity()+","+openRequestBO.getEstate()+","+openRequestBO.getEzip() %></td>
				<td style="background-color:lightgreen">
					<% if(request.getAttribute("btn") != null && request.getAttribute("btn").equals("openrequest") ) {%>
						<a href="control?action=openrequest&event=getopenCriteria&module=dispatchView&openrequest=Change Open Request&tripid=<%= openRequestBO.getTripid() %>" ><%= openRequestBO.getPhone() %></a>
					<% } else { %>
						<a href="control?action=openrequest&event=getopenCriteria&module=dispatchView&acceptrequest=Change Open Request&tripid=<%= openRequestBO.getTripid() %>" ><%= openRequestBO.getPhone() %></a>
					<% } %>
				</td>
			</tr>
			<%} %>
			<%}%>
			
			<% } else{%>
			
			No records available
			
			<% }%>
			</table>
                                  
                </div>    
              </div>
            	
                <div class="clrBth"></div>
            
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
