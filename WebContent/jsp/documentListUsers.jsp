<%@ page language="java" import="java.util.*"%>
<%@ page import="com.tds.bean.docLib.*"%>

<%
	String ctxPath = request.getContextPath();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" >
<html>
<head>

<link rel="stylesheet" type="text/css" href="<%=ctxPath%>/css/document.css" media="all">
<link rel="stylesheet" href="<%=ctxPath%>/css/document/docStyle.css" type="text/css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

<script type="text/javascript" src="<%=ctxPath%>/js/document/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<%=ctxPath%>/js/document/jquery.tablesorter.pager.js"></script>

<title>TDS(Taxi Dispatching System)</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">


<script type="text/javascript">

	function loadFile(docId){
		document.getElementById("docLoader").src='uploadDocs?do=viewDoc&docId='+docId;
	}



	function uploadNew(){
	
		document.getElementById("upload").action="<%=ctxPath%>/control?action=TDS_DOCUMENT_UPLOAD&event=passKeyGen&module=operationView&do=uploadNew";
		document.getElementById("upload").submit();
	}

	function uploadAlternate(docId,docNumber,docType){
		document.getElementById("hDocId").value=docId;
		document.getElementById("hDocNum").value=docNumber;
		document.getElementById("hDocType").value=docType;
		document.getElementById("upload").action="<%=ctxPath%>/control?action=TDS_DOCUMENT_UPLOAD&event=passKeyGen&module=operationView&do=uploadOne";
		document.getElementById("upload").submit();
	}
</script>



</head>
<body>

	<input type="hidden" name="module" id="module"
		value="<%=request.getParameter("module") == null ? "" : request
					.getParameter("module")%>" />
	<form name="upload" id="upload" action="uploadDocs" method="post">

		<input type="hidden" name="hDocId" id="hDocId" /> <input
			type="hidden" name="hDocType" id="hDocType" /> <input type="hidden"
			name="hDocNum" id="hDocNum" /> <input type="hidden" name="compId"
			id="compId" value='<%=request.getAttribute("compId")%>' /> <input
			type="hidden" name="userId" id="userId"
			value='<%=request.getAttribute("userId")%>' />
		<br>
		<c class="nav-header">
		<center>Documents </center>
		</c><br>
		<br>
		 <B> To Upload New Documents <a
			href="javascript:uploadNew()"> <img alt="" src="jsp/images/upload.png" width="2%" height="2%">
		</a></B>
		<br>
		<br>
		<div style="float: left; width: 60%">
			<!-- <div style="display: block;height: 190px;"> -->
			<table id="documentList" class="table-striped">
				<thead>
					<TR>
						<th>Type</th>
						<TH>Name</TH>
						<TH>Number</TH>
						<TH>Exp.Date</TH>
						<tH>Status</th>
						<tH>View</th>
					</TR>

				</thead>
				<tbody align="center">
					<%
						Collection collection = (Collection) request
								.getAttribute("docList");
						int i = 0;
						for (Iterator iter = collection.iterator(); iter.hasNext();) {
							i = i + 1;
							DocBean element = (DocBean) iter.next();
							if (i % 2 == 0) {
								out.println("<tr class=\"alternate\"><TD>");
							} else {
								out.println("<tr><TD>");
							}

							if (element.getDocType() != null) {
								out.println(element.getDocType());
							} else {
								out.println("N/A");
							}

							out.println("</td><td>");
							out.println(element.getDocName());
							out.println("</td><td>");
							if (element.getDocNumber() != null) {
								out.println(element.getDocNumber());
							} else {
								out.println("N/A");
							}
							out.println("</td><td>");
							if (element.getDocExpiry() != null) {
								out.println(element.getDocExpiry());
							} else {
								out.println("N/A");
							}
							out.println("</td><td>");
							out.println(element.getDocStatus());
							if (element.getDocStatus().equals("EXPIRED")) {
								out.println("&nbsp;|&nbsp;<a href=\"javascript:uploadAlternate('"
										+ element.getDocId()
										+ "','"
										+ element.getDocNumber()
										+ "','"
										+ element.getDocType() + "')\">Upload New</a>");
							}
							out.println("</td><td>");
							out.println("<a href=\"javascript:loadFile('"+ element.getDocId() + "')\">");
							%>
							<img alt="" src="jsp/images/view.jpg" height="30%" width="30%" >
							<%
						out.println("</a>");
							out.println("</td></tr>");
						}
						if (i == 0) {
							out.println("<tr><TD colspan=6 align=\"center\" >NO RECORD FOUND</TD></TR>");
						}
					%>
					</tbody>
			<c id="pager" class="pager" align="center" style="position:absolute; ">
				<img src="<%=ctxPath%>/images/first.png" class="first" /> <img
					src="<%=ctxPath%>/images/prev.png" class="prev" /> <input
					type="text" class="pagedisplay" disabled="disabled" /> <img
					src="<%=ctxPath%>/images/next.png" class="next" /> <img
					src="<%=ctxPath%>/images/last.png" class="last" /> <input
					type="hidden" value="15" class="pagesize" id="pagesize" />
					</c>
				
				</table>			
		</div>

		<div style="float: right; height:60%; width: 30% ">   
		<b> <c align="center"> Document View</c></b>
			<iframe src="" id="docLoader" name="docLoader" width="auto" height="60%" scrolling="auto" frameborder="0" >	</iframe>
			<c class="footerDV" align="right">Copyright&nbsp;2010 Get A Cab</c>
			<!-- <iframe src="" id="docLoader" name="docLoader" width="100%"
				height="500px"> </iframe> -->
		</div>

	</form>



	
	<script defer="defer">
		$(document).ready(function() {
			$("#documentList").tablesorter({
				widthFixed : false,
				widgets : [ 'zebra' ]
			}).tablesorterPager({
				container : $("#pager")
			});
		});
	</script>

</body>
</html>

