<%@page import="com.common.util.TDSProperties"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<%@page import="com.charges.bean.ChargesBO"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Set"%>
<%@page import="com.tds.cmp.bean.DriverVehicleBean"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.tds.dao.AdministrationDAO"%>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<style type="text/css" title="currentStyle">
@import "dataTable/css/demo_page.css";

@import "dataTable/css/demo_table_jui.css";

@import "dataTable/css/jquery-ui-1.8.4.custom.css";

@import "dataTable/css/TableTools_JUI.css";
</style>



<link rel="stylesheet" type="text/css"
	href="jqueryUI/layout-default-latest.css" />

<link rel="stylesheet" type="text/css"
	href="jqueryUI/jquery.ui.base.css" />
<link rel="stylesheet" type="text/css"
	href="jqueryUI/jquery.ui.theme.css" />
<link type="text/css" href="css/main.css" rel="stylesheet" />
<link href="css/Popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet"
	href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css"
	media="screen"></link>
<script src="jqueryUI/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="jqueryUI/jquery-latest.js"></script>
<script type="text/javascript" src="jqueryUI/jquery-ui-latest.js"></script>
<script type="text/javascript" src="jqueryUI/jquery.layout-latest.js"></script>
<script type="text/javascript"
	src="jqueryUI/jquery.layout.resizePaneAccordions-latest.js"></script>
<script type="text/javascript" src="jqueryUI/themeswitchertool.js"></script>
<script type="text/javascript" src="jqueryUI/debug.js"></script>
<script type="text/javascript"
	src=<%=TDSProperties.getValue("googleMapV3") %>></script>
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geocode&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>
<script type="text/javascript" src="js/MapForORDetails.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="js/OpenRequestDetails.js"></script>
<script type="text/javascript" src="js/Util.js"></script>
<script type="text/javascript" src="DashboardAjax/DispatchCommon.js?vNo=<%=TDSConstants.versionNo %>"></script>

<script type="text/javascript"
	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>

<script type="text/javascript" charset="utf-8"
	src="dataTable/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8"
	src="dataTable/js/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8"
	src="dataTable/js/TableTools.js"></script>
<script type="text/javascript" src="js/markerwithlabel.js"></script>
<script type="text/javascript" src="js/Util.js"></script>

<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="jqueryNew/bootstrap.js"></script>
<script type="text/javascript" src="jqueryNew/bootstrap.min.js"></script>




<%		 	ArrayList<ChargesBO> chargeType =(ArrayList<ChargesBO>)request.getAttribute("chargeTypes");
 %>
<%
			String search = (String) request.getAttribute("value");
			String fDate = (String) request.getAttribute("fDate");
			String tDate = (String) request.getAttribute("tDate");
		%>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
<%if(session.getAttribute("fleetList")!=null) {
	fleetList=(ArrayList)session.getAttribute("fleetList");
	} %>
<%
			AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
				ArrayList<DriverVehicleBean> dvList=new ArrayList<DriverVehicleBean>();
				HashMap<String,DriverVehicleBean> companyFlags=(HashMap<String,DriverVehicleBean>) request.getAttribute("companyFlags");
				Set hashMapEntries = companyFlags.entrySet();
				Iterator it = hashMapEntries.iterator();
				while(it.hasNext()){
					Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
					dvList.add(companyFlag.getValue());
				}

		%>

	
<style type="text/css">
.ui-helper-hidden-accessible {
	display: none;
}

.ui-datepicker-calendar {
	width: 274px;
	height: 172px;
}

.ui-datepicker-header {
	width: 274px;
	height: 38px;
}

#LongDesc em {
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
}

#LongDesc1 em {
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
}

.hover em {
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
}

.pac-container {
	border: 1px solid #C77405;
	background-color: #FFFFFF;
	color: #C77405;
	font-weight: bold;
	outline: medium none;
	width: 500px !important;
}

.ui-state-hover,.ui-widget-content .ui-state-hover,.ui-state-focus,.ui-widget-content .ui-state-focus
	{
	border: 1px solid #C77405;
	background-color: #FFFFFF;
	color: #C77405;
	font-weight: bold;
	outline: medium none;
}

.gmap3 {
	margin: 20px auto;
	border: 1px dashed #C0C0C0;
	width: 1024px;
}

/* #confirmBox
{
    display: none;
    background-color: #eee;
    border-radius: 5px;
    border: 1px solid #aaa;
    position: fixed;
    width: 600px;
    left: 50%;
    margin-left: -150px;
    margin-top: -500px;
    padding: 6px 8px 8px;
    box-sizing: border-box;
    text-align: center;
} */
#confirmBox .button {
	background-color: #ccc;
	display: inline-block;
	border-radius: 3px;
	border: 1px solid #aaa;
	padding: 2px;
	text-align: center;
	width: 160px;
	cursor: pointer;
}

#confirmBox .button:hover {
	background-color: #ddd;
}

#confirmBox .message {
	text-align: left;
	margin-bottom: 8px;
}
/* #confirmBoxJobs
{
    display: none;
    background-color: #eee;
    border-radius: 5px;
    border: 1px solid #aaa;
    position: fixed;
    width: 600px;
    left: 50%;
    margin-left: -150px;
    margin-top: -500px;
    padding: 6px 8px 8px;
    box-sizing: border-box;
    text-align: center;
} */
#confirmBoxJobs .button {
	background-color: #ccc;
	display: inline-block;
	border-radius: 3px;
	border: 1px solid #aaa;
	padding: 2px;
	text-align: center;
	width: 160px;
	cursor: pointer;
}

#confirmBoxJobs .button:hover {
	background-color: #ddd;
}

#confirmBoxJobs .message {
	text-align: left;
	margin-bottom: 8px;
}
/* remove padding and scrolling from elements that contain an Accordion OR a content-div */
.ui-layout-center, /* has content-div */ .ui-layout-west,
	/* has Accordion */ .ui-layout-east, /* has content-div ... */
	.ui-layout-east .ui-layout-content { /* content-div has Accordion */
	padding: 0;
	overflow: hidden;
}

.ui-layout-center P.ui-layout-content {
	line-height: 0.9em;
	margin: 0; /* remove top/bottom margins from <P> used as content-div */
}

h3,h4 { /* Headers & Footer in Center & East panes */
	font-size: 1.1em;
	background: #EEF;
	border: 1px solid #BBB;
	border-width: 0 0 1px;
	padding: 7px 10px;
	margin: 0;
}

.ui-layout-east h4 { /* Footer in East-pane */
	font-size: 0.9em;
	font-weight: normal;
	border-width: 1px 0 0;
}
</style>

<script type="text/javascript">
	function showCommentsForHistory(row){
		 var img=document.getElementById("commentButton_"+row).value;
			if(img.indexOf("Show Comments")!= -1){
				document.getElementById("commentButton_"+row).value="Hide Comments";
				$("#comments_"+row).show("slow");
			}else{
				document.getElementById("commentButton_"+row).value="Show Comments";
				$("#comments_"+row).hide("slow");
			}
	}

	function showMeterDataForHistory(row){
		var img=document.getElementById("meterData_"+row).value;
		if(img.indexOf("Show MeterData")!= -1){
			document.getElementById("meterData_"+row).value="Hide MeterData";
			$("#meterValues_"+row).show("slow");
		}else{
			document.getElementById("meterData_"+row).value="Show MeterData";
			$("#meterValues_"+row).hide("slow");
		}
	}
	 
	 $(document).ready( function() {
		 <%if(request.getAttribute("value")==null){%>
		 	currentDateHistory();
		 <%}%>
		 $(".datePicker").datepicker();

		 $( "button,input[type=button]" ).button().click(function( event ) {
				event.preventDefault();
			});
		 myLayout = $('body').layout({
				west__size:			0
			,	east__size:			570,
				south__size:		470
				// RESIZE Accordion widget when panes resize
			,	west__onresize:		$.layout.callbacks.resizePaneAccordions
			,	east__onresize:		$.layout.callbacks.resizePaneAccordions
			});

			// ACCORDION - in the West pane
			$("#accordion1").accordion({
				heightStyle:	"fill"
			});
			
			// ACCORDION - in the East pane - in a 'content-div'
			$("#accordion2").accordion({
				heightStyle:	"fill"
			,	active:			1
			});


			// THEME SWITCHER
			addThemeSwitcher('.ui-layout-north',{ top: '-3px', right: '5px' });
			// if a new theme is applied, it could change the height of some content,
			// so call resizeAll to 'correct' any header/footer heights affected
			// NOTE: this is only necessary because we are changing CSS *AFTER LOADING* using themeSwitcher
			setTimeout( myLayout.resizeAll, 1000 ); /* allow time for browser to re-render with new theme */

	});
	 function currentDateHistory() {
			var curdate = new Date();
			var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
			var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
			var cYear = curdate.getFullYear();
			document.getElementById("fromDateHis").value =cMonth+"/"+cDate+"/"+cYear;
			document.getElementById("toDateHis").value =cMonth+"/"+cDate+"/"+cYear;
	 }
/*			var cMonthFrom =cMonth;
			var cDateFrom = curdate.getDate()-7;
			var cYearFrom = cYear;
 			if(cDateFrom < 0){
				cDateFrom = 31 - (cDate);
				cMonthFrom = cMonth-1;
			} if(cMonthFrom <= 0){
				cMonthFrom = 12-(cMonthFrom);
				cYearFrom = cYear -1;
			}
			var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
			var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
			var hrsmin = hors+""+min;  	
			//document.masterForm.shrs.value  = hrsmin; */		 
	function calCharges() {
		var i = Number(document.getElementById("fieldSize").value) + 1;
		document.getElementById('fieldSize').value = i;
		var table = document.getElementById("chargesTable");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);

		var element1 = document.createElement("select");
		element1.name = "payType" + i;
		element1.id = "payType" + i;
		<%if(chargeType!=null && chargeType.size()>0){
			for(int i=0;i<chargeType.size();i++){%>
		var theOption = document.createElement("option");
		theOption.text = "<%=chargeType.get(i).getPayTypeDesc()%>";
		theOption.value = "<%=chargeType.get(i).getPayTypeKey()%>";
		element1.options.add(theOption);
		<%}}%>
		cell1.appendChild(element1);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "amountDetail" + i;
		element2.id = "amountDetail" + i;
		element2.size = '7';
		element2.onchange=function(){callAmountHistory();};
		cell2.appendChild(element2);
	} 
	function callAmountHistory(){
			var size=document.getElementById('fieldSize').value;
			document.getElementById('amount').value="";
			for(var j=0;j<size;j++){
				var i=j+1;
				var firstValue=document.getElementById('amount').value;
				var newValue=document.getElementById("amountDetail"+i).value;
				if(firstValue==""){
					firstValue="0";
				}
				document.getElementById('amount').value=parseFloat(firstValue)+parseFloat(newValue);
			}
		}
	
	
	
	function addVouchers(tripId,Driver){
		$( "#VoucherAdd" ).dialog({
		     modal: true,
	      	width:1030,
		 });
/* 		 $('#VoucherAdd').jqm();
		 $('#VoucherAdd').jqmShow();
 */		 clearChargesHis();
		 document.getElementById("tripIdVC").value=tripId;
		 document.getElementById("vcDriver").value=Driver;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'OpenRequestAjax?event=getCharges&tripId='+tripId;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			var jsonObj = "{\"address\":"+text+"}" ;
			var obj = JSON.parse(jsonObj.toString());
			for(var j=0;j<obj.address.length;j++){
				calCharges();
				var type=obj.address[j].T;
				var amount=obj.address[j].A;
				var i=j+1;
				document.getElementById("payType"+i).value=type;
				document.getElementById("amountDetail"+i).value=amount;
			}
			callAmountHistory();
	 }
	
	 function printHistoryJob(tripId){
		window.open("ReportController?event=reportEvent&ReportName=passengerPayment&output=pdf&tripId="+tripId+"&type=basedMail&assoccode="+document.getElementById("assoccodeHis").value+"&timeOffset="+document.getElementById("timeZoneHis").value);
	 }

	
	function submitVoucher(){
		var tripId=document.getElementById("tripIdVC").value;
		var vcNum=document.getElementById("vcNum").value;
		var driverId=document.getElementById("vcDriver").value;
		var payType=document.getElementById("payType").value;
		var amount=document.getElementById("amount").value;
		var cvv=document.getElementById("cvvNum").value;
		var expDate=document.getElementById("expDate").value;
		if(vcNum==""){
			vcNum=document.getElementById("ccNum").value;
		}
		if(document.getElementById("fieldSize").value!="" && document.getElementById("fieldSize").value>0){
			if(vcNum!=""){
				var xmlhttp=null;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				url='OpenRequestAjax?event=addVouchers&tripId='+tripId+'&vcNum='+vcNum+'&driverId='+driverId+'&payType='+payType+'&amount='+amount+'&expDate='+expDate+'&cvvCode='+cvv;
				xmlhttp.open("GET", url, false);
				xmlhttp.send(null);
				var text = xmlhttp.responseText;
				if(text.indexOf('001')>=0){
					 submitCharges(2);	
					 document.getElementById("tripIdVC").value="";
					 document.getElementById("vcDriver").value="";
					 document.getElementById("vcNum").value="";
					 document.getElementById("ccNum").value="";
					 document.getElementById("payType").value="cc";
					 document.getElementById("expDate").value="";
					 document.getElementById("cvvNum").value="";
					 document.getElementById("amount").value="";
					 $("#VoucherAdd" ).dialog( "close" );
					 $("#VoucherAdd").hide();
					 //$('#VoucherAdd').jqmHide();
				} else {
					alert("Voucher does not exist/expired");
				}
		 	} else {
				 alert("Please enter any voucher number");
		 	}
	 	} else {
		 	alert("Please Add Atleast One Charge");
	 	}
	 }
	
	function closeVoucherAdd(value){
		if(value==1){
			 $("#VoucherAdd" ).dialog( "close" );
			 $("#VoucherAdd").hide();
		} else {
			 $("#paymentDetails" ).dialog( "close" );
			 $("#paymentDetails").hide();
			//$('#paymentDetails').jqmHide();
		}
	}
	function onDownload() {
	    document.location = 'data:Application/octet-stream,' +
	                         encodeURIComponent(dataToDownload);
	}
	
	 function getPayment(tripId){
		 var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url='RegistrationAjax?event=getPaymentDetails&tripId='+tripId;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var jsonObj = "{\"address\":"+text+"}" ;
		var obj = JSON.parse(jsonObj.toString());
		for(var j=0;j<obj.address.length;j++){
			var description=obj.address[j].D;
			var number=obj.address[j].N;
			var type=obj.address[j].S;
			var amount=obj.address[j].A;
			var payType=obj.address[j].PV;
			var transId=obj.address[j].TXN;
			if(description=="Failed")
			{
				document.getElementById('paymentFromServer').innerHTML = "<b><center>Payment Details</center></b></br>No Records Found";
			} else {
				if(j==0){
					document.getElementById('paymentFromServer').innerHTML = "<b><center>Payment Details</center></br><br><b>Pay Type	:</b>	"+type+" <br/><b>Number	:</b>	"+number+" <br/><b>Name	:</b>"+description+" <br/><b><center>Charge Details</center></br>";
				}
				$('#paymentFromServer').append(payType+"<b>-</b>"+amount+"</br>");
			}
			if(transId!=""){
				document.getElementById("transId").value=transId;
			}
			$( "#paymentDetails" ).dialog({
			     modal: true,
		      	width:1030
			 });
//			$('#paymentDetails').jqm();
//			$('#paymentDetails').jqmShow();
		}
	 }
	 function reverseTransaction(){
		 if(document.getElementById("transId").value==""){
			 alert("This is not valid for return");
			 return;
		 } else {
			 var xmlhttp = null;
			 if (window.XMLHttpRequest)
			 {
				 xmlhttp = new XMLHttpRequest();
			 } else {
				 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			 }
			 var url = 'OpenRequestAjax';
			 var parameters='event=enterCCAuth&transId='+document.getElementById("transId").value+'&category=Void';
			 xmlhttp.open("POST",url, false);
			 xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			 xmlhttp.setRequestHeader("Content-length", parameters.length);
			 xmlhttp.setRequestHeader("Connection", "close");
			 xmlhttp.send(parameters);
			 var text = xmlhttp.responseText;
			 var resp=text.split("###");
			 var result=resp[0].split(";");
			 if(result[0].indexOf('Failed')>=0){
				alert("Failed to reverse Transaction");
			 } else {
				 $("#paymentDetails" ).dialog( "close" );
				 $("#paymentDetails").hide();
				 //$('#paymentDetails').jqmHide();
			 }
		 }
	 }
	 function clearChargesHis(){
		 var table = document.getElementById("chargesTable");
		 var rowCount = table.rows.length;
		 var size=document.getElementById('fieldSize').value;
		 for(var i=0;i<Number(size);i++){
			 rowCount--;
			 table.deleteRow(rowCount);
		 }
		 document.getElementById("fieldSize").value="";
	 }
	 
 function showOrHideLogs(row,tripId){
	 var img=document.getElementById("imgReplace1_"+row).src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgReplace1_"+row).src="images/minus_icons.png";
			 var xmlhttp=null;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				url='OpenRequestAjax?event=logsDetail&tripId='+tripId;
				xmlhttp.open("GET", url, false);
				xmlhttp.send(null);
				var text = xmlhttp.responseText;
				if(text!=null ){
					$("#logsRow_"+row).show("slow");
					$("#logs_"+row).show("slow");
					if(text!="No Records Found"){
					$("#buttonRow_"+row).show("slow");
					$("#buttonShowMap_"+row).show("slow");
					$("#addVoucher_"+row).show("slow");
					$("#showPayment_"+row).show("slow");
					}
					document.getElementById("logs_"+row).innerHTML=text;
				}
		}else{
			document.getElementById("imgReplace1_"+row).src="images/plus_icons.png";
			$("#buttonRow_"+row).hide("slow");
			$("#buttonShowMap_"+row).hide("slow");
			$("#addVoucher_"+row).hide("slow");
			$("#showPayment_"+row).hide("slow");
			$("#logsRow_"+row).hide("slow");
			$("#logs_"+row).hide("slow");
		}
 }

 function showMapforTrip(tripId){
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url='OpenRequestAjax?event=getLatLongi&tripId='+tripId;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("jobHistory").innerHTML=text;
		$( "#jobHistory" ).dialog({
		     modal: true,
	      	width:1030
		 });
	 	initialize();
 }
 function changeOption(){
	 if(document.getElementById("payType").value=="vc"){
		 document.getElementById("voucher").style.display="block";
		 document.getElementById("credit").style.display="none";
		 document.getElementById("ccNum").value="";
	 } else {
		 document.getElementById("credit").style.display="block";
		 document.getElementById("voucher").style.display="none";
		 document.getElementById("vcNum").value="";
	 }
 }
 
	function submitCharges(type){
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var repeatJob="";
		if(document.getElementById("fieldSize").value!=""){
			var chargeSize=document.getElementById("fieldSize").value;
			var repeatJob = repeatJob+'&chargesLength='+chargeSize;
		 	for (var i=1; i<=chargeSize;i++){
			 	repeatJob=repeatJob+'&chargeKey'+i+'='+document.getElementById('payType'+i).value;
			 	repeatJob=repeatJob+'&chargeAmount'+i+'='+document.getElementById('amountDetail'+i).value;
		 	}
			var url='OpenRequestAjax?event=chargesFromHistory&tripId='+document.getElementById("tripIdVC").value+repeatJob;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			if(type==1){
				 $("#VoucherAdd" ).dialog( "close" );
				 $("#VoucherAdd").hide();
				//$('#VoucherAdd').jqmHide();
			}
	 	} else {
		 	alert("Please Add Atleast One Charge");
	 	}
 	}
	function closePage(){
		window.location.href="/TDS/control";
	}
	var objectNew=[];
 
	function getHistoryValues(str){
		var fleetNo="";
		if(document.getElementById("fleetSizeHL")!=null){
			//alert("fleetsize:"+document.getElementById("fleetSizeHL").value);
			if(document.getElementById("fleetSizeHL").value!=0){
				//alert("fleetNo:"+document.getElementById("fleetNoHL").value);
				if(document.getElementById("fleetNoHL").value!="all"){
					fleetNo=document.getElementById("fleetNoHL").value;
				}
			}
		}
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var extraData="";
		if(document.getElementById("drprofileSize").value!=""){
			 extraData=extraData+'&splFlag='+document.getElementById("drprofileSize").value;	
			 for (var i=0;i<document.getElementById("drprofileSize").value;i++){
				 if(document.getElementById('dchk'+i).checked==true){
					 extraData=extraData+'&dchk'+i+'='+document.getElementById('dchk'+i).value;
				 }
			 }
		}
		var url = 'RegistrationAjax?event=getHistoryJobs&value='+document.getElementById("value").value+'&fromDate='+document.getElementById("fromDateHis").value+'&toDate='+document.getElementById("toDateHis").value+"&status="+document.getElementById("status").value+"&download="+str+extraData+'&fleetNo='+fleetNo+'&trip_Src='+document.getElementById("trip_Src").value;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(str=="1"){
			if(text.indexOf("TI")>=0){
				var jsonObj = "{\"address\":"+text+"}" ;
				objectNew = JSON.parse(jsonObj.toString());
		 			fillJsonValues();
		    		} else {
				document.getElementById("showHistory").innerHTML="";
			}
		} else 	{			
			//alert("You are going to download");			
				var jsonObj = "{\"address\":"+text+"}" ;
				objectNew = JSON.parse(jsonObj.toString());
				fillJsonValues1();
				if ($("#filter").is(':focus')) {
				return;
			}	
			//	window.open(url,'historyJobs');
		      }
	 }
 	 function fillJsonValues(){
		 var distance1="";
 		if(objectNew.address.length>0){
		document.getElementById("showHistory").innerHTML="";
		var $tbl1 = $('<table style="width:20%;">').attr('id', 'search');
			$tbl1.append($('<tr>').append(
					$('<td style="width:5%;font-weight:bolder;" align="center">').text("Search"),
					$('<td style="width:10%">').append($('<input/>').attr('type', 'text').attr('name', 'searchValues').attr('id','searchValues').val("").attr('onkeyup',"customSearchOfaTable('historyValues','searchValues','row')"))
			)
			);
			var $tbl = $('<table style="width:100%">').attr('id', 'historyValues');
			$tbl.append($('<tr>').append(
					$('<th>').text("TripId"),
					$('<th>').text("Trip Date"),
					$('<th>').text("Name"),
					$('<th>').text("Driver ID"),
					$('<th>').text("Vehicle No"),
					$('<th>').text("Phone"),
					$('<th>').text("Start Address"),
					$('<th>').text("End Address"),
					$('<th>').text("Trip Status"),
					$('<th>').text("Amount"),
					$('<th>').text("Pass#"),
					$('<th>').text("Ref No#"),
					$('<th>').text("Meter Data"),
					
					$('<th>').text("Distance"),
					<%if(fleetList!=null && fleetList.size()>0){%>
					$('<th>').text("Fleet"),
					<%} %>
					$('<th>').text("Comments")
			)
			);
			for(var i = 0; i < objectNew.address.length; i++){
				$tbl.append($('<input/>').attr('type', 'hidden').attr('name', 'tripId').attr('id','tripId'+i).val(objectNew.address[i].TI));
				
				$tbl.append($('<tr>').append(
						$('<td style="width:5%" align="center">').text(objectNew.address[i].TI),
						$('<td style="width:10%" align="center">').text(objectNew.address[i].TD),
						$('<td style="width:5%" align="center">').text(objectNew.address[i].NA),
						$('<td style="width:5%" align="center">').text(objectNew.address[i].DR),
						$('<td style="width:3%" align="center">').text(objectNew.address[i].VN),
						$('<td style="width:7%" align="center">').text(objectNew.address[i].PH),
						$('<td style="width:18%" align="center">').text(objectNew.address[i].SA+""+objectNew.address[i].SA2+""+objectNew.address[i].SC),
						$('<td style="width:18%" align="center">').text(objectNew.address[i].EA+""+objectNew.address[i].EA2+""+objectNew.address[i].EC),
						$('<td style="width:12%" align="center">').text(objectNew.address[i].TS),
						$('<td style="width:3%" align="center">').text(document.getElementById("currecyPrefix").value+""+objectNew.address[i].AM),
						$('<td style="width:4%" align="center">').text(objectNew.address[i].NP),
						$('<td style="width:7%" align="center">').text(objectNew.address[i].RF),
						<%-- <%if(adminBo.isMeterHidden()){%>
						$('<td style="width:3%" align="center">').text(document.getElementById("currecyPrefix").value+""+objectNew.address[i].MP),
						<%} %> --%>
						$('<td style="width:5%" align="center">').append( (objectNew.address[i].GPS=="Yes")?$('<input/>').attr('type', 'button').attr('name', 'meterData_'+i).attr('id','meterData_'+i).val("Show MeterData").attr('onclick','showMeterDataForHistory('+i+')'):"-"),
						$('<td style="width:3%" align="center">').append($('<input size="4"/>').attr('type', 'label').attr('name', 'distance_'+i).attr('id','distance_'+i).val(""+0.00)),
						
						<%if(fleetList!=null && fleetList.size()>0){%>
							$('<td style="width:5%" align="center">').text(objectNew.address[i].FL),
						<%} %>
						//if((objectNew.address[i].CM!="") || (objectNew.address[i].SI!="")){
						$('<td style="width:5%" align="center">').append($('<input/>').attr('type', 'button').attr('name', 'commentButton_'+i).attr('id','commentButton_'+i).val("Show Comments").attr('onclick','showCommentsForHistory('+i+')')),
						//} else {
						//}
						$('<td style="width:5%">').append($('<input/>').attr('type', 'button').attr('name', 'detailShow_'+i).attr('id','detailShow_'+i).val("Details").attr('onclick','openORDetails('+objectNew.address[i].TI+')')),
						$('<td style="width:5%">').append($('<input/>').attr('type', 'button').attr('name', 'printJob_'+i).attr('id','printJob_'+i).val("Print").attr('onclick','printHistoryJob('+objectNew.address[i].TI+')')),
						$('<td style="width:5%">').append($('<img/>').attr('alt', '').attr('name', 'imgReplace1_'+i).attr('id','imgReplace1_'+i).attr('src','images/plus_icons.png').attr('onclick','showOrHideLogs('+i+','+objectNew.address[i].TI+')'))
//						$('<td style="width:5%">').append($('<input/>').attr('type', 'hidden').attr('name', 'flags_'+i).attr('id','flags_'+i).val(objectNew.address[i].DRC))
				)
				);
				$tbl.append($('<tr id="comments_'+i+'" style="display:none;">').append(
						$('<td colspan="8" style="font-weight: bolder;">').text('DC:'+objectNew.address[i].CM+' CC:'+objectNew.address[i].SI)
				)
				);
				if(objectNew.address[i].GPS=="Yes"){
					$tbl.append($('<tr id="meterValues_'+i+'" style="display:none;">').append(
							$('<td colspan="14" style="font-weight: bolder;">').text('Meter Amount:'+objectNew.address[i].MP+' - Is Hidden:'+objectNew.address[i].GHidden+' - Time Accured (secs):'+objectNew.address[i].GTAIS+' - Distance Accured (miles):'+objectNew.address[i].GDAIM+' - Amount Calculated For Time:'+objectNew.address[i].GTC
									+' - Amount Calculated For Distance:'+objectNew.address[i].GDC+' - Start Amount:'+objectNew.address[i].GST+' - Minimum Speed:'+objectNew.address[i].GMS)
					)
					);
				}
				$tbl.append($('<tr  id="buttonRow_'+i+'" style="display:none;">').append(
						$('<td>').append($('<input/>').attr('type', 'button').attr('name', 'addVoucher_'+i).attr('id','addVoucher_'+i).val("AddPayment").attr('onclick','addVouchers('+objectNew.address[i].TI+','+objectNew.address[i].TI+')')),
						$('<td>').append($('<input/>').attr('type', 'button').attr('name', 'showPayment_'+i).attr('id','showPayment_'+i).val("ViewPayment").attr('onclick','getPayment('+objectNew.address[i].TI+')'))
				)
				);
				$tbl.append($('<tr id="logsRow_'+i+'" style="display:none;">').append(
						$('<td id="logs_'+i+'" style="display:none;" colspan="8">')
				)
				);
				}
			$("#showHistory").append($tbl1);
			$("#showHistory").append($tbl);
		
		}
 		var x;
 		var r=confirm("Do you want calculate Distance");
 		if (r==true){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}document.getElementById("button1").disabled=false;
            
			for(var j=0;j<objectNew.address.length;j++){
				if(objectNew.address[j].SLA!="" && objectNew.address[j].ELA!=""){
					if(objectNew.address[j].SLA!="0.000000" && objectNew.address[j].ELA!="0.000000"){
					     var origin=objectNew.address[j].SLA+","+objectNew.address[j].SLO;
						var destination=objectNew.address[j].ELA+","+objectNew.address[j].ELO;
						//var url = 'http://open.mapquestapi.com/directions/v1/route?key=Fmjtd%7Cluub2qub21%2C8l%3Do5-961gg4&outFormat=json&routeType=&timeType=1&enhancedNarrative=false&locale=en_US&unit=m&from='+origin+'&to='+destination+'&drivingStyle=2&highwayEfficiency=21.0';
					    //var url = 'https://open.mapquestapi.com/directions/v1/route?key=ZX3FD32ZdJ8LfqzB2cKPMD6WDiuSbwTE&outFormat=json&routeType=fastest&timeType=1&enhancedNarrative=false&locale=en_US&unit=m&from='+origin+'&to='+destination+'&drivingStyle=2&highwayEfficiency=21.0';
					    var url = 'OpenRequestAjax?event=getDistFMQA&from='+origin+'&to='+destination+'&routeType=fastest';
						xmlhttp.open("GET", url, false);
						xmlhttp.send(null);
						var text = xmlhttp.responseText;
						if(text=="" || text == "GRACIERROR" || text=="AUTH_FAILED"){
							document.getElementById("distance_"+j).value="0";
						}else{
							var condition=text.split('"distance":');
							var finalDistance=condition[1].split(',');
						    distance1=Math.round((finalDistance[0])*100)/100;
					      	document.getElementById("distance_"+j).value=distance1+0.00;
						}
					}
				}else{
					document.getElementById("distance_"+j).value="0";
				}
			}
		}else{
			document.getElementById("button1").disabled=false;
        }
 	}	 
 	 	 
 	 function fillJsonValues1(){
		 var distance1="";
 		if(objectNew.address.length>0){
		document.getElementById("jobHistory1").innerHTML="";
	    $("#jobHistory1").remove();
		$("#showHistory").hide();
		$("#advancedScreen").hide();
		
		$("#Jobs").html("");//
					
		$("#Jobs").append('<table  width="100%"  class="display" data-filter="#filter" data-filter-text-only="true" id="jobHistory1" style="margin-top: 121px;" >');
		
		$("#jobHistory1").append("<thead><tr><th><img alt='' src='images/home.png' onclick='closePage()'></th></tr><tr><th data-class='expand' class='showtripid'>TripID</th><th data-class='expand' class='showdriverid'>Driverid</th><th data-class='expand' class='showname'>Name</th><th data-class='expand' class='showphone'>Phone</th><th data-class='expand' class='showvehicleno'>VehicleNo</th><th data-class='expand' class='showsaddress'>StartAddress</th><th data-class='expand' class='showslat'>S_latitude</th><th data-class='expand' class='showslon'>S_longitude</th><th data-class='expand' class='showtripstatus'>TripStatus</th><th data-class='expand' class='showendaddress'>End_Address</th><th data-class='expand' class='showelat'>Endlatitude</th><th data-class='expand' class='showelong'>Endlongitude</th><th data-class='expand' class='showdate'>Date</th><th data-class='expand' class='showpaytype'>Paytype</th><th data-class='expand' class='showdistance'>Distance</th><th data-class='expand' class='showamt'>Amount</th><th data-class='expand' class='showtripsource'>TripSource</th><th data-class='expand' class='showcreatedby'> CreatedBy</th><th data-class='expand' class='showrouteno'>Route_Number</th><th data-class='expand' class='showonsitetime'>On_Site_Time</th><th data-class='expand' class='showtripstarttime'>Trip_Start_Time</th><th data-class='expand' class='showendtime'>Trip_End_Time</th><th data-class='expand' class='showfleet'> Fleet</th><th data-class='expand' class='showdriverprofile'>DriverProfile</th><th data-class='expand' class='showvehicleprofile'>VehicleProfile</th><th data-class='expand' class='showacct'>Acct</th><th data-class='expand' class='showcallername'>CallerName</th><th data-class='expand' class='showcallerpn'>CallerPhone</th><th data-class='expand' class='showrefno'>RefNumber</th><th data-class='expand' class='showrefnum1'>RefNumber1</th><th data-class='expand' class='showrefnum2'>RefNumber2</th><th data-class='expand' class='showrefnum3'>RefNumber3</th><th data-class='expand' class='showStOdo'>Start ODO</th><th data-class='expand' class='showEdOdo'>End ODO</th><th data-class='expand' class='showsplins'>SpecialIns</th><th data-class='expand' class='showcomm'>Comments</th></tr></thead><tbody>"); 
		tr = $('<tr/>');
		 for(var i = 0; i < objectNew.address.length; i++){
			 tr = $('<tr/>');
			 tr.append("<td class='showtripid'>" + objectNew.address[i].TI + "</td>");
			 tr.append("<td class='showdriverid'>" + objectNew.address[i].DR + "</td>");
			 tr.append("<td class='showname'>" + objectNew.address[i].NA + "</td>");
			 tr.append("<td class='showphone'>" + objectNew.address[i].PH + "</td>");
			 tr.append("<td class='show'>" + objectNew.address[i].VN + "</td>");
			 tr.append("<td class='showstartaddress'>" + objectNew.address[i].SA +" "+ objectNew.address[i].SC+ "</td>");
			 tr.append("<td class='showslat'>" + objectNew.address[i].SLA + "</td>");
			 tr.append("<td class='showSlong'>" + objectNew.address[i].SLO + "</td>");
			 tr.append("<td class='showtripsrc'>" + objectNew.address[i].TRS + "</td>");
			 tr.append("<td class='showendadd'>" + objectNew.address[i].EA + " "+ objectNew.address[i].EC+"</td>");
			 tr.append("<td class='showendlat'>" + objectNew.address[i].ELA + "</td>");
		     tr.append("<td class='showendlong'>" + objectNew.address[i].ELO + "</td>");
		     tr.append("<td class='showtd'>" + objectNew.address[i].TD + "</td>");
			 tr.append("<td class='showpaytype'>" + objectNew.address[i].PT + "</td>");
			 distance1="0.00";
			 
			 /* if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				if(objectNew.address[i].SLA!="" && objectNew.address[i].ELA!=""){
					if(objectNew.address[i].SLA!="0.000000" && objectNew.address[i].ELA!="0.000000"){
			     var origin=objectNew.address[i].SLA+","+objectNew.address[i].SLO;
				var destination=objectNew.address[i].ELA+","+objectNew.address[i].ELO;
				//var url = 'http://open.mapquestapi.com/directions/v1/route?key=Fmjtd%7Cluub2qub21%2C8l%3Do5-961gg4&outFormat=json&routeType=&timeType=1&enhancedNarrative=false&locale=en_US&unit=m&from='+origin+'&to='+destination+'&drivingStyle=2&highwayEfficiency=21.0';
			    var url = 'https://open.mapquestapi.com/directions/v1/route?key=ZX3FD32ZdJ8LfqzB2cKPMD6WDiuSbwTE&outFormat=json&routeType=fastest&timeType=1&enhancedNarrative=false&locale=en_US&unit=m&from='+origin+'&to='+destination+'&drivingStyle=2&highwayEfficiency=21.0';
				xmlhttp.open("GET", url, false);
				xmlhttp.send(null);
				var text = xmlhttp.responseText;
				var condition=text.split('"distance":');
				var finalDistance=condition[1].split(',');
			    distance1=""+Math.round((finalDistance[0])*100)/100;
		      	}else{
					distance1="";
				}
			} */
					
				tr.append("<td class='showShdistance'>" + distance1 + "</td>");
				    tr.append("<td class='showam'>" + objectNew.address[i].AM + "</td>");
				tr.append("<td class='showtripsource'>" + objectNew.address[i].TSO + "</td>");
			    tr.append("<td class='showcrb'>" + objectNew.address[i].CRB + "</td>");
			    tr.append("<td class='showRouteno'>" + objectNew.address[i].RNO + "</td>");
				tr.append("<td class='showSitime'>" + objectNew.address[i].SITIME + "</td>");
				tr.append("<td class='showstarttime'>" + objectNew.address[i].CRTIME + "</td>");
				tr.append("<td class='showcomptime'>" + objectNew.address[i].COTIME + "</td>");
			    tr.append("<td class='showfleet'>" + objectNew.address[i].FL + "</td>");
			    tr.append("<td class='showdrpn'>" + objectNew.address[i].DP + "</td>");
				tr.append("<td class='showvehpn'>" + objectNew.address[i].VP + "</td>");
				tr.append("<td class='showacct'>" + objectNew.address[i].ACCT + "</td>");
			    tr.append("<td class='showcn'>" + objectNew.address[i].CN + "</td>");
				tr.append("<td class='showCP'>" + objectNew.address[i].CP + "</td>");
				tr.append("<td class='showShRef'>" + objectNew.address[i].RF + "</td>");
				tr.append("<td class='showShRef1'>" + objectNew.address[i].RN+ "</td>");
			    tr.append("<td class='showShRef2'>" + objectNew.address[i].RF2 + "</td>");
			    tr.append("<td class='showShRef3'>" + objectNew.address[i].RF3 + "</td>");
				tr.append("<td class='showStOdo'>" + objectNew.address[i].SO + "</td>");
				tr.append("<td class='showEdOdo'>" + objectNew.address[i].EO + "</td>");
				tr.append("<td class='showins'>" + objectNew.address[i].SI + "</td>");
				tr.append("<td class='showShcomm'>" + objectNew.address[i].CM + "</td>");
						
		 //   tr.append("<td class='showShDrId'><input type='text' name='distance_"+i+"' id='distance_"+i+"' value='' ></td>"); 
			  
		     $('#jobHistory1').append(tr);
		}
		 	}
 		 	
	   $('#jobHistory1').append("<tfoot><tr><td colspan='9'><div class='pagination pagination-centered hide-if-no-paging' ></div></td></tr></tfoot>  ");
		$("#Jobs").append( $('#jobHistory1'));
		$('#jobHistory1')
		.dataTable(
				{
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"sDom": '<"H"Tfr>t<"F"ip>',
					"sDom" : 'T<"clear">lfrtip',
					"iDisplayLength": 20,
					"aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
					"oTableTools" : {
						"sSwfPath" : "dataTable/js/copy_csv_xls_pdf.swf","aButtons": [
						                                                              "copy", "csv", "xls", "pdf",
						                                                              {
						                                                            	  "sExtends":    "collection",
						                                                            	  "sButtonText": "Save",
						                                                            	  "aButtons":    [ "csv", "xls", "pdf" ]
						                                                              }
						                                                              ]}
				});
 		}

 		
 	
 	 
 
function hideAllTables(){
	if(document.getElementById("searchValues").value==""){
		document.getElementById("value").focus();
		setTimeout(function(){fillJsonValues();},50);
	}
}
function checkGroupHistory(row,groupId,longDesc){
	var drProf=document.getElementById("drprofileSize").value;
	if(document.getElementById("dchk"+row).checked==true){
		for(var i=0;i<drProf;i++){
			if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
				document.getElementById("dchk"+row).checked=false;
				 alert("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
				 break;
			}
		}
	}
}

function changeFleet(fleet,i) {
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = 'SystemSetupAjax?event=changeFleet&fleet=' + fleet;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);

	var text = xmlhttp.responseText;
	if(text.indexOf("Successfully")<0){
		alert(text);
	}
	readCookieForCenter();
	document.getElementById("changedFleet").value=fleet;
	//onloadDash();
	
	$(".fleetbutton").css('backgroundColor', '');
	$("#fleet_"+i).css('backgroundColor', 'green');
	document.getElementById("fleetForOR").value=document.getElementById("changedFleet").value;		
	//$("#color").val($("#fleetcolor_"+i).val());
	//document.getElementById().style.backgroundColor = color;
	document.getElementById("top-header").style.backgroundColor = "#"+$("#fleetcolor_"+i).val();			
	//changeCompanyLogo();
	//location.reload();
}

</script>

<title>TDS (Taxi Dispatch System)</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
	<form name="masterForm" id="masterForm" action="control" method="post">
		<input type="hidden" name="action" value="openrequest" /> <input
			type="hidden" name="event" value="openRequestHistoryWithLogs" /> <input
			type="hidden" name="module" id="module" value="dispatchView" /> <input
			type="hidden" name="transId" id="transId" value="" /> <input
			type="hidden" id="currecyPrefix"
			value="<%=adminBo.getCurrencyPrefix()==1?"$":adminBo.getCurrencyPrefix()==2?"&euro;":adminBo.getCurrencyPrefix()==3?"&pound;":"&#x20B9;"%>" />
		<input type="hidden" name="screenValue" id="screenValue" value="10" />
		<input type="hidden" name="fleetSizeHL" id="fleetSizeHL" value=<%=fleetList.size()%> />
		<input type="hidden" name="assoccode" id="assoccodeHis" value="<%=adminBo.getMasterAssociateCode()%>" />	
		<input type="hidden" name="timeZone" id="timeZoneHis" value="<%=adminBo.getTimeZone()%>" />	
		

		<div class="ui-layout-north ui-widget-content" style="display: none;"></div>
		<div class="ui-layout-center" style="display: none"
			id="advancedScreen">
			<h2 class="ui-widget-header">
			
	<%if(fleetList!=null&&fleetList.size()>0){%>
      <div id="fleetDiv" style="  -moz-border-radius: 8px 8px 8px 8px;align: right; width: auto; height: auto; position: absolute; top: 18px;max-height:62px;" align="center">
          <table ><tr>
	<%-- <%for(int i=0;i<fleetList.size();i++){ 
		if(i%4==0){%>
			<tr>
	    <%}%>
				<td>
					<input type="button" name="fleet_<%=i%>" class="fleetbutton"  value ="<%=fleetList.get(i).getFleetName()%>" id ="fleet_<%=i%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBo.getAssociateCode())?"style='background-color:green'":""%> onclick="changeFleet('<%=fleetList.get(i).getFleetNumber()%>','<%=i%>')"  />
					<input type="hidden" name="fleetNumberForCookie<%=i %>" id="fleetNumberForCookie<%=i %>" value="<%=fleetList.get(i).getFleetNumber()%>"/>
					<input type="hidden" name="changedFleet" id="changedFleet" value=""/>
	 			</td>
	<%} %> --%>
			<td>Select Fleet : 
					<select name="fleetNoHL" id="fleetNoHL" >
					<option value="all" >All</option>								
                      <%for(int j=0;j<fleetList.size();j++){ %>
								<option value="<%=fleetList.get(j).getFleetNumber()%>" ><%=fleetList.get(j).getFleetName() %></option>
								<%=fleetList.get(j).getFleetNumber()%>
							<%
						}%> 
                       </select>
                      </td>
          </tr>
         </table></div>
  <%} %>
		
				<center>Job History & Logs</center>
			</h2>
			
			<table style="width: 100%;">
				<tr align="center">
					<td><img alt="" src="images/home.png" onclick="closePage()">
					</td>
					<td><font color="black" style="">Value:&nbsp;&nbsp;</font> <input
						type="text" name="value" id="value" size="10"
						value="<%=search==null?"":search %>" /></td>
					<td><font color="black">From Date:&nbsp;&nbsp;</font> <input
						type="text" class="datePicker" name="fromDateHis" id="fromDateHis"
						size="10" value="<%=fDate==null?"":fDate %>" /></td>
					<td><font color="black">To Date:&nbsp;&nbsp;</font> <input
						type="text" class="datePicker" name="toDateHis" id="toDateHis"
						size="10" value="<%=tDate==null?"":tDate %>" /></td>
					<td><font color="black">Status:&nbsp;&nbsp;</font> <select
						name="status" id="status">
							<option value="0">All</option>
							<option value="61">Trip Completed</option>
							<option value="50">Customer Cancelled</option>
							<option value="52">Couldn't Service</option>
							<option value="55">Operator Cancelled</option>
							<option value="70">Payment Received</option>
					</select></td>
					<td><font color="black">Trip Src:&nbsp;</font> <select
						name="trip_Src" id="trip_Src">
							<option value="0">All</option>
							<option value="1">Operator Created</option>
							<option value="2">Flag Trip</option>
							<option value="3">Customer Booked</option>
							<option value="4">Others</option>
					</select></td>
					<td><input type="button" name="button" id="button"
						value="Search" onclick="getHistoryValues('1')" /> <input
						type="button" name="button" id="button1" value="Download"
						onclick="getHistoryValues('2')" disabled /></td>
				</tr>
				<tr>
					<%if(dvList!=null &&  dvList.size()>0) {
							int driverCounter=0;%>
					<%for(int i=0;i<dvList.size();i++){ %>
					<td><input type="checkbox" name="dchk<%=driverCounter%>"
						id="dchk<%=driverCounter%>"
						value="<%=dvList.get(i).getKey()==null?"":dvList.get(i).getKey()%>"
						onclick="checkGroupHistory('<%=driverCounter%>','<%=dvList.get(i).getGroupId()%>','<%=dvList.get(i).getLongDesc()%>')" /><%=dvList.get(i).getLongDesc() %>
						<input type="hidden" name="dgroup<%=driverCounter %>"
						id="dgroup<%=driverCounter%>"
						value="<%=dvList.get(i).getGroupId()%>" /> <input type="hidden"
						name="dLD<%=driverCounter %>" id="dLD<%=driverCounter%>"
						value="<%=dvList.get(i).getLongDesc()%>" /></td>
					<%driverCounter++;%>
					<%}%>
					<td><input type="hidden" name="drprofileSize"
						id="drprofileSize" value="<%=driverCounter%>" /></td>
					<%} else {%>
					<td><input type="hidden" name="drprofileSize"
						id="drprofileSize" value="" /></td>
					<%}%>
				</tr>
			</table>
		</div>
		<div class="ui-layout-south ui-widget-content ui-state-error"
			style="display: none; height: 10px" id="showHistory"></div>
		<div id="jobHistory" style="display: none;"></div>
		<div id="Jobs" style="display: visible; position: absolute;">
			<table class="footable" data-filter="#filter"
				data-filter-text-only="true" id="jobHistory1" data-page-size="5">
			</table>
		</div>
		<div id="paymentDetails" style="display: none;">
			<div id="paymentFromServer"
				style="font-weight: bolder; color: #7E2217; background-color: #BDEDFF; border-color: #FF3333; border-width: 3px; -moz-border-radius: 12px 12px 12px 12px;"></div>
			<table bgcolor="#BDEDFF" style="width: 100%;">
				<tr>
					<td><center>
							<input type="button" name="reverse" id="reverse" value="Reverse"
								onclick="reverseTransaction()"></input>
						</center></td>
				</tr>
			</table>
		</div>
		<div id="VoucherAdd" style="display: none;">
			<h4>
				<center style="color: #000000">Payment Details</center>
			</h4>
			<table style="width: 100%;">
				<tr>
					<td style="width: 25%;">Type</td>
					<td style="width: 25%;"><select name="payType" id="payType"
						onchange="changeOption()">
							<option value="cc">Credit Card</option>
							<option value="vc">Voucher</option>
							<option value="cash">Cash</option>
					</select></td>
					<td style="width: 25%;">TripId</td>
					<td style="width: 25%;"><input type="text" readonly="readonly"
						name="tripIdVC" id="tripIdVC" value="" /></td>
				</tr>
				<tr>
					</center>
					<td style="width: 25%;">Voucher/CC Num</td>
					<td id="voucher" style="width: 25%; display: none"><input
						type="text" name="vcNum" class="ui-autocomplete-input" id="vcNum"
						value="" autocomplete="off" /> <ajax:autocomplete fieldId="vcNum"
							popupId="model-popup2" targetId="vcNum"
							baseUrl="autocomplete.view" paramName="getAccountList"
							className="autocomplete" progressStyle="throbbing" /></td>
					<td id="credit" style="width: 25%;"><input type="text"
						name="ccNum" id="ccNum" value="" /></td>
					<td style="width: 25%;">Amount</td>
					<td style="width: 25%;"><input type="text" name="amount"
						id="amount" value="" /></td>
				</tr>
				<tr>
					<td style="width: 25%;">Cvv Number</td>
					<td style="width: 25%;"><input type="text" name="cvvNum"
						id="cvvNum" value="" /></td>
					<td style="width: 25%;">Exp. Date</td>
					<td style="width: 25%;"><input type="text" name="expDate"
						id="expDate" value="" /></td>
				</tr>
				<tr>
					<input type="hidden" name="vcDriver" id="vcDriver" value="" />
					<td align="center"><input type="button" value="Add"
						id="confirm" onclick="submitVoucher()" /></td>
				</tr>
			</table>
			<h4>
				<center style="color: #000000">Charge Details</center>
			</h4>
			<table id="chargesTable" style="width: 42%;" border="1"
				bgcolor="#f8f8ff">
				<tr>
					<th style="width: 23%; background-color: #d3d3d3;">Charge Type</th>
					<th style="width: 14%; background-color: #d3d3d3;">Amount</th>
				</tr>
			</table>
			<table id="chargesTable" style="width: 42%;" border="1"
				bgcolor="#f8f8ff">
				<tr>
					<td><input type="button" name="add" value="Add Row"
						onclick="calCharges()" /> <input type="button" name="clear"
						value="Clear All" onclick="clearChargesHis()" /> <input
						type="button" name="chargesAdd" value="Enter Charges"
						onclick="submitCharges(1)" /></td>
				</tr>
			</table>
		</div>
	</form>

	<div id="jobDetailsMapDash"
		style="display: none; height: 430px; width: 750px; margin-left: 10%; z-index: 3000;">
		<table id="tabForDetails"
			style="z-index: 3000; width: 750px; cursor: pointer;">
			<tr style="background-color: black;">
				<td id="detailsOR" onclick="openORDetails('')"><font
					id="detailsORFont" color="white">Details</font></td>
				<td id="detailsLogs" onclick="showLogs()"><font
					id="detailsLogsFont" color="white">Logs</font></td>
				<td id="detailsPayment" onclick="showPayment()"><font
					id="detailsPaymentFont" color="white">Payment Details</font></td>
				<td id="mapForJobDetails" onclick="jobDetailsMap()"><font
					id="mapForJobDetailsFont" color="white">Map</font></td>
			</tr>
		</table>
		<div id="jobDetailsMapDashDiv"
			style="height: 330px; width: 550px; margin-left: 10%;"></div>
	</div>

	<div id="paymentDetailsDash"
		style="display: none; height: 430px; width: 750px; margin-left: 10%; z-index: 3000;">
		<table id="tabForDetails"
			style="z-index: 3000; width: 750px; cursor: pointer;">
			<tr style="background-color: black;">
				<td id="detailsOR" onclick="openORDetails('')"><font
					id="detailsORFont" color="white">Details</font></td>
				<td id="detailsLogs" onclick="showLogs()"><font
					id="detailsLogsFont" color="white">Logs</font></td>
				<td id="detailsPayment" onclick="showPayment()"><font
					id="detailsPaymentFont" color="white">Payment Details</font></td>
				<td id="mapForJobDetails" onclick="jobDetailsMap()"><font
					id="mapForJobDetailsFont" color="white">Map</font></td>
			</tr>
		</table>
		<div id="paymentDetailsDashDiv"
			style="height: 330px; width: 550px; margin-left: 10%;"></div>
	</div>

	<div id="jobDetailsLogsDiv"
		style="display: none; height: 430px; width: 750px; margin-left: 10%; z-index: 3000;">
		<table id="tabForDetails"
			style="z-index: 3000; width: 750px; cursor: pointer;">
			<tr style="background-color: black;">
				<td id="detailsOR" onclick="openORDetails('')"><font
					id="detailsORFont" color="white">Details</font></td>
				<td id="detailsLogs" style="background-color: white;"
					onclick="showLogs()"><font id="detailsLogsFont" color="black">Logs</font>
				</td>
				<td id="detailsPayment" onclick="showPayment()"><font
					id="detailsPaymentFont" color="white">Payment Details</font></td>
				<td id="mapForJobDetails" onclick="jobDetailsMap()"><font
					id="mapForJobDetailsFont" color="white">Map</font></td>
			</tr>
		</table>
		<div id="jobDetailsLogsPopUp"
			style="height: 330px; width: 550px; margin-left: -2%;"></div>
	</div>

	<div id="popUpOR"
		style="display: none; height: 430px; width: 750px; margin-left: 10%; z-index: 3000;">
		<table id="tabForDetails"
			style="z-index: 3000; width: 750px; cursor: pointer;">
			<tr style="background-color: black;">
				<td id="detailsOR" style="background-color: white;"
					onclick="openORDetails('')"><font id="detailsORFont"
					color="black">Details</font></td>
				<td id="detailsLogs" onclick="showLogs()"><font
					id="detailsLogsFont" color="white">Logs</font></td>
				<td id="detailsPayment" onclick="showPayment()"><font
					id="detailsPaymentFont" color="white">Payment Details</font></td>
				<td id="mapForJobDetails" onclick="jobDetailsMap()"><font
					id="mapForJobDetailsFont" color="white">Map</font></td>
			</tr>
		</table>
		<div id="ORDash"
			style="height: 330px; width: 550px; margin-left: 10%;">
			<jsp:include page="/jsp/OpenRequestForDetails.jsp" />
		</div>
	</div>


</body>
</html>
