<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
	function changename(type){
	document.getElementById("changename").innerHTML=document.getElementById(type).value+" ID";
	}
 function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>TDS(Taxi Dispatching System)</title>
</head>
<%
	String userType[] = {"Admin","Manager","Operator","Corp Admin","Corp User","PUC","Government"};
%>
<body>
	<form method="post" action="control" name="admin"
		onsubmit="return showProcess()">
		<input type="hidden" name="module" id="module"
			value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
		<jsp:useBean id="adminBo" class="com.tds.tdsBO.AdminRegistrationBO"
			scope="request" />
		<% if(request.getAttribute("adminBO") != null) 
		adminBo = (AdminRegistrationBO) request.getAttribute("adminBO");
%>
		<jsp:setProperty name="adminBo" property="*" />
		<input type="hidden" name="<%= TDSConstants.actionParam %>"
			value="<%= TDSConstants.registrationAction %>"> <input
			type="hidden" name="<%= TDSConstants.eventParam %>"
			value="<%= TDSConstants.saveAdminRegistraion %>"> <input
			type="hidden" name="systemsetup" value="6">
		<%
						String error ="";
						if(request.getAttribute("errors") != null) {
						error = (String) request.getAttribute("errors");
						}
					%>
		<div id="errorpage" class="alert-error">
			<%= error.length()>0?""+error :"" %>
		</div>
		<c class="nav-header">
		<center>Admin Registration</center>
		</c>
		<table width="100%" border="0" cellspacing="0" cellpadding="0"
			class="navbar-static-top">
			<tr>
				<td>First&nbsp;Name</td>
				<td><input type="text" name="Fname"
					value="<%= adminBo.getFname() %>"></td>
				<td>Last&nbsp;Name</td>
				<td><input type="hidden" name="Uid"
					value="<%= adminBo.getUid()== null?"":adminBo.getUid() %>">
					<input type="text" name="Lname" value="<%= adminBo.getLname() %>">
				</td>
			</tr>
			<tr>
				<td>User&nbsp;Name</td>
				<td><input type="text" name="uname"
					value="<%= adminBo.getUname() %>"></td>
				<td>User&nbsp;Type</td>
				<td><select name="usertype" id="usertype"
					onchange="changename(id);">
						<%
								for(int count=0;count<userType.length;count++) {
								if(adminBo.getUsertype().equalsIgnoreCase(userType[count]))  {
							%>
						<option value="<%= userType[count] %>" selected="selected"><%= userType[count] %></option>
						<% } else { %>
						<option value="<%= userType[count] %>"><%= userType[count] %></option>
						<%
							} }
							%>
				</select></td>
			</tr>

			<tr>
				<td>Email</td>
				<td><input type="text" name="Email"
					value="<%= adminBo.getEmail() == null ?"":adminBo.getEmail() %>">
				</td>
				<%if(request.getAttribute("pagefor") != null && (request.getAttribute("pagefor").equals("createAdmin"))){ %>
				<td id="changename">Admin</td>
				<td><input type="text" name="Drid"
					value="<%= adminBo.getDrid()== null ? request.getAttribute("drkey") == null ?"" :request.getAttribute("drkey"):adminBo.getDrid() %>">
				</td>
				<%}else{ %>
				<input type="hidden" name="Drid"
					value="<%= adminBo.getDrid()== null ? request.getAttribute("drkey") == null ?"" :request.getAttribute("drkey"):adminBo.getDrid() %>">
				<%} %>
				<td>Status</td>
				<td>Active<input type="radio" style="vertical-align: middle"
					name="Active" value="1"
					<%= adminBo.getActive().equals("1")?"checked":"" %> /> InActive<input
					type="radio" style="vertical-align: middle" name="Active" value="0"
					<%= adminBo.getActive().equals("0")?"checked":"" %> />
				</td>
			</tr>
			<tr>
				<td>Password</td>
				<td>
					<%if(request.getAttribute("pagefor") != null && (request.getAttribute("pagefor").equals("createAdmin"))){ %>
					<input type="password" name="password" value=""> <%}else{ %>
					<input type="password" name="password" value=""> <%} %>
				</td>
				<td>Re-Type&nbsp;Password</td>
				<td>
					<%if(request.getAttribute("pagefor") != null && (request.getAttribute("pagefor").equals("createAdmin"))){ %>
					<input type="password" name="repassword" value=""> <%}else{ %>
					<input type="password" name="repassword" value=""> <%} %>
				</td>
			</tr>
			<%if(request.getAttribute("pagefor") != null && (request.getAttribute("pagefor").equals("createAdmin"))){ %>
			<tr>
				<td colspan="4" align="center"><input type="submit"
					name="submit" value="Create Admin"></td>
			</tr>
			<%}else{ %>
			<tr>
				<td colspan="4" align="center"><input type="submit"
					name="submit" value="UpdateAdmin"> <input type="submit"
					name="submit" value="UpdatePassword"></td>
			</tr>
			<%} %>
		</table>
		<div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
	</form>
</body>
</html>
