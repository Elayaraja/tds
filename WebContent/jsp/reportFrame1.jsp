 <html>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.common.util.TDSProperties"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%   AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
String reportName = (String)request.getAttribute("reportName");
String invoiceNo =(String)request.getAttribute("invoiceNo");
String url = TDSProperties.getValue("JasperServer")+reportName+"&invoiceNo="+invoiceNo+"&assocode="+adminBO.getAssociateCode();
 %>

 <head><style>
 html 
{
 overflow: auto;
}
 
html, body, div, iframe 
{
 margin: 0px; 
 padding: 0px; 
 height: 100%; 
 border: none;
}
iframe 
{
 display: block; 
 width: 100%; 
 border: none; 
 overflow-y: auto; 
 overflow-x: hidden;
}
 </style></head>
 <body>
 <iframe src="<%=url%>" width="200" height="200"></iframe>
</body>
</html>
