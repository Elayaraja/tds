<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%@page import="java.util.ArrayList"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Registration View</title>
<script type="text/javascript">
	function cal(ivalue) {
	document.getElementById("ivalue").value=document.getElementById(ivalue).value;
	}
</script>
</head>

<body>
<form method="post" action="control" name="admin">
  <input type="hidden" name="<%=TDSConstants.actionParam%>"	value="<%=TDSConstants.registrationAction%>"> 
	<input	type="hidden" name="<%=TDSConstants.eventParam%>"
	value="<%=TDSConstants.saveAdminRegistraion%>">

<table id="pagebox">
	<tr>
		<td>
		<table id="bodypage" border="1">
			<tr>
				<td colspan="7" align="center">
				<div id="title">
				<h2>Admin Registration View</h2>
				</div>
				</td>
			</tr>
			<%ArrayList a_list=(ArrayList)request.getAttribute("adminBO");
			
			if(a_list != null){%>
				<tr>
				<td>User Name</td>
				<td>UserType</td>
				<td>Edit</td>
			</tr>
				<%for(int i=0;i<a_list.size();i++) {
			AdminRegistrationBO adminBO=(AdminRegistrationBO) a_list.get(i);%>
			
			<tr>
				<td><%=adminBO.getUname() %></td>
				<td><%=adminBO.getUsertype() %></td>
				<td><input type="hidden" name="uname<%=i %>" id="uname<%=i %>" value="<%=adminBO.getUid() %>">
				<input type="hidden" name="ivalue" id="ivalue" value="">
				<input type="Submit" name="submit" value="Edit" onclick="cal('uname<%=i %>');">
				</td>
			</tr>
			<%}} %>
		 </table>
		</td>
	</tr>
</table>
</form>
</body>
</html>