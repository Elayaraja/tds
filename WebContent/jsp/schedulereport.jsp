<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList,java.util.List,java.util.Map,java.util.HashMap"%>

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%><html>
<head>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
 <link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
 <link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript">
	function callReport(action) {
		
		var fromdate = "";
		var todate = "";
		var driverid = "";
		var url = "";
		var code= "";
		fromdate = document.getElementById("start").value;
		todate = document.getElementById("end").value;
		code = document.getElementById("code").value;
		//fromdate.replace('-','/');
		
		//todate.replace('/','-');
		//driverid = document.getElementById("getDriverId").options[document.getElementById("getDriverId").selectedIndex].value
		if(fromdate != "" && todate != "") {
			window.open(document.getElementById("context").value+'/frameset?__report=BIRTReport/cronejobTEST.rptdesign&__format=html&stdate='+fromdate+'&enddate='+todate+'&asscode='+code);
		//	url =document.getElementById("context").value+'/frameset?__report=BIRTReport/DriverSetteledSummary.rptdesign&__format=pdf&st_date='+fromdate+'&end_date='+todate;
			//document.driverPaymentForm.action = url;
			//document.driverPaymentForm.target = 'new';
			//driverPaymentForm.submit();
		} else {
			alert("You must give all Values");
		}
	}

</script>
<%
	List al_driverlist = null;
	Map mp_driverMap = null;
	System.out.println("Before Request "+request.getAttribute("driverList"));
	if(request.getAttribute("driverList") != null) {
		System.out.println(" Request "+request.getAttribute("driverList"));
		al_driverlist = (ArrayList) request.getAttribute("driverList");
	}
%>
</head>
<body>
<form name="driverPaymentForm" action="control" method="post">
	<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
	<table id="pagebox">
		<tr>
			<td>
			<table id="bodypage">
			<tr>
				<td colspan="7" align="center">
					<div id="title" >
						<h2>Schedule&nbsp;Report</h2>
					</div>
				</td>
			</tr>
			 
			<tr>
				<td>Start&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
					<input type="text" name="sdate" id="start" size="10" onfocus="showCalendarControl(this);"/>
				 
				</td>
				<td>&nbsp;</td>
				<td>End&nbsp;Date</td>
				<td>&nbsp;</td>
				<td>
				<input type="text" name="edate" id="end" size="10" onfocus="showCalendarControl(this);"/>
				 
				</td>
			</tr>
			<tr><td>
				 <input type="hidden" name="code" id="code" size="10" value="<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>"/>
				</td>		
				<td colspan="7" align="center"> 
					<input type="button"   value="Search" onclick="callReport(this)"/>
				</td>
			</tr>
			</table>
			</td>
		</tr>
	</table>
</form>
</body>
</html>
