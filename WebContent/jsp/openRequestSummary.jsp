<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="java.util.HashMap"%>
<%@page import= "java.util.Set"%>
<%@page import="com.tds.cmp.bean.DriverVehicleBean"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	 <%if(request.getAttribute("value")==null){%>
	 currentDate();
	 <%}%>
	}); 

function currentDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
	var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
	var hrsmin = hors+""+min;  
	document.getElementById("fDate").value =cMonth+"/"+cDate+"/"+cYear;
	document.getElementById("tDate").value =cMonth+"/"+cDate+"/"+cYear;

	//document.masterForm.shrs.value  = hrsmin; 
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}


function openOR2(tripId)
{
	window.open('control?action=openrequest&event=saveRequest&tripId='+tripId,"_self");
}


</script>
</head>
	<body>
	<form name="masterForm" action="control" method="post" onsubmit="return showProcess">
		<%
		ArrayList<OpenRequestBO> openReqSummary = (ArrayList<OpenRequestBO>) request.getAttribute("openRequestSummary");
		HashMap<String,DriverVehicleBean> companyFlags=(HashMap<String,DriverVehicleBean>) request.getAttribute("allFlags");

		String search = (String) request.getAttribute("value");
		String fDate = (String) request.getAttribute("fDate");
		String tDate = (String) request.getAttribute("tDate");
		%>
		<%
			AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
		%>
		<input type="hidden" name="action" value="openrequest" /> 
		<input type="hidden" name="event" value="openRequestSummaryMain" /> 
		 <input	type="hidden" name="module" id="module" value="dispatchView" />
			<c class="nav-header"><center>Reservation Summary</center></c>
	<table width="100%" style="width:100%;" cellspacing="1" cellpadding="0" class="navbar-static-top">
			
			<tr>
				<td><font color="black">Search:</font> <input type="text"
					name="keyWord" id="keyWord" size="10" value="<%=search==null?"":search %>"  />
				</td>

				<td><font color="black">From Date:</font>
					<input type="text" name="fDate" id="fDate" size="10" value="<%=fDate==null?"":fDate %>" onfocus="showCalendarControl(this);" />
						</td>
						<td><font color="black">To Date:</font> <input type="text"
							name="tDate" id="tDate" size="10" value="<%=tDate==null?"":tDate %>" onfocus="showCalendarControl(this);"  />
						</td>

						<td><input type="submit" name="submit" id="submit"
							value="Search" /></td>

						</tr>

</table>




						<%
							if (openReqSummary != null && openReqSummary.size() > 0) {
						%>
					<table style="width: 100%" border="1" cellspacing="1" cellpadding="1" 	class="thumbnail">
										<tr>
												<td align="center" style="background-color: #EFEFFB;width: 10%;"><font
													color="black">TripId</font>
												</td>
												<td align="center" style="background-color: #EFEFFB;width: 10%;"><font
													color="black">Time</font>
												</td>
												<td align="center" style="background-color: #EFEFFB;width: 10%;"><font
													color="black">DriverId</font>
												</td>
												<td align="center" style="background-color: #EFEFFB;width: 10%;"><font
													color="black">Phone</font>
												</td>
												<td align="center"  style="background-color: #EFEFFB;width: 10%;"><font
													color="black">Name</font>
												</td>
												<td align="center"  style="background-color: #EFEFFB;width: 10%;"><font
													color="black">Special Flag</font>
												</td>
												<td align="center" style="background-color: #EFEFFB;width: 20%;"><font
													color="black">Pick up Address</font>
												</td>
												<td align="center" style="background-color: #EFEFFB;width: 20%;"><font
													color="black">Drop Address</font>
												</td>
												<td align="center" style="background-color: #EFEFFB; width: 5%;"><font size="2"
													color="black">Pass#</font></td>
												<td align="center" style="background-color: #EFEFFB; width: 10%;"><font size="2"
													color="black">Ref. No#</font></td>
												
											</tr>

											<%
											boolean colorLightGreen = true;
												String colorPattern;
										%>
											<%
											for (int i = 0; i < openReqSummary.size(); i++) {
												String specialFlags="";
													colorLightGreen = !colorLightGreen;
													if (colorLightGreen) {
														colorPattern = "style=\"background-color:#EFEFFB;white-space: nowrap;\"";
													} else {
														colorPattern = "style=\"background-color:lightgreen;white-space: nowrap;\"";
													}
													
													if(companyFlags!=null){
														for(int j=0;j<openReqSummary.get(i).getDrProfile().length();j++){
															specialFlags = specialFlags + companyFlags.get(openReqSummary.get(i).getDrProfile().substring(j,j+1)).getShortDesc() + ";";                     
															colorPattern = "style=\"background-color:#FF9933;white-space: nowrap;\"";
														}
													}
										%>
											<tr id="row" class='jobSummaryRow'>
											 <td style="display:none"id="jobSummaryTripid"><%=openReqSummary.get(i).getTripid()%></td>
												<td align="center" <%=colorPattern%>><font
													color="black"><%=openReqSummary.get(i).getTripid()==null?"":openReqSummary.get(i).getTripid()%> 
													<%
													long repeatJobs=Long.parseLong(openReqSummary.get(i).getRepeatGroup());
													if(repeatJobs==0){%>
											<%}else{ %>
											*
											<%} %></font>
													<input type="hidden" name="tripId" id="tripId<%=i%>"
													value=<%=openReqSummary.get(i).getTripid()%>></input></td>
												<td align="center" <%=colorPattern%>><font
													color="black"><%=openReqSummary.get(i).getStartTimeStamp()==null?"":openReqSummary.get(i).getStartTimeStamp()%></font></td>
												<td align="center" <%=colorPattern%>><font
													color="black"><%=openReqSummary.get(i).getDriverid()==null?"":openReqSummary.get(i).getDriverid()%></font>
												</td>
												<td align="center" <%=colorPattern%>><font
													color="black"><%=openReqSummary.get(i).getPhone()==null?"":openReqSummary.get(i).getPhone()%></font>
												</td>
												<td align="center" <%=colorPattern%>><font
													color="black"><%=openReqSummary.get(i).getName()==null?"":openReqSummary.get(i).getName()%></font>
												</td>
												<td align="center" <%=colorPattern%>><font
													color="black"><%=specialFlags%></font>
												</td>
												<td align="center" <%=colorPattern%>><font
													color="black"><%=openReqSummary.get(i).getSadd1()==null?"":openReqSummary.get(i).getSadd1()%> <%=openReqSummary.get(i).getSadd2()==null?"":openReqSummary.get(i).getSadd2()%> <%=openReqSummary.get(i).getScity()==null?"":openReqSummary.get(i).getScity()%> 
													</font>
												</td>
												<td align="center" <%=colorPattern%>><font
													color="black"><%=openReqSummary.get(i).getEadd1()==null?"":openReqSummary.get(i).getEadd1()%> <%=openReqSummary.get(i).getEadd2()==null?"":openReqSummary.get(i).getEadd2()%><%=openReqSummary.get(i).getEcity()==null?"":openReqSummary.get(i).getEcity()%>
													</font>
												</td>
												
												<td align="center" <%=colorPattern%>>
													<font size="2" color="black"><%=openReqSummary.get(i).getNumOfPassengers()<1?1:openReqSummary.get(i).getNumOfPassengers()%></font>
												</td>
												<td align="center" <%=colorPattern%>>
												<font size="2" color="black"><%=openReqSummary.get(i).getRefNumber()==null?"":openReqSummary.get(i).getRefNumber()%></font>
												</td>
												
												<td align="center" <%=colorPattern%>>
												<input type="button" value="Edit" onclick="openOR2(<%=openReqSummary.get(i).getTripid()%>)"/>
												</td>
												

											</tr>
											<%
											}
											}
										%>
										</table>
										</form>
</body></html>