<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.OpenRequestBO" %>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<jsp:useBean id="openBO"   class="com.tds.tdsBO.OpenRequestBO"  />
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<% if(request.getAttribute("openrequest") != null) {
		openBO = (OpenRequestBO)request.getAttribute("openrequest");
	%>
<script type="text/javascript">
			document.masterForm.sdate.value = "";
			document.masterForm.shrs.value = "";
			document.masterForm.smin.value = "";
			
		</script>
	<% } %>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="Ajax/OpenRequest.js"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TDS (Taxi Dispatch System)</title>
<script language="javascript">
function loadDate() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	//alert(cMonth+"/"+cDate+"/"+cYear);
	<%
	if(openBO.getSdate().length() <= 0) {
	%>
		document.masterForm.sdate.value = cMonth+"/"+cDate+"/"+cYear;
	<% 
	}%>

</script>
</head>
<%
String status[] = {"Request","In Process","Completed"};
%>
<body onload="loadDate()">
<form name="masterForm" action="control" method="post">

<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");

%>
<jsp:setProperty name="openBO"  property="*"/>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveOpenRequest %>">
<input type="hidden" name="stlatitude" id="stlatitude"  value="<%= openBO.getSlat() %>"> 
<input type="hidden" name="stlongitude" id="stlongitude" value="<%= openBO.getSlong() %>">
<input type="hidden" name="edlatitude" id="edlatitude" value="<%= openBO.getEdlatitude() %>">
<input type="hidden" name="edlongitude" id="edlongitude" value="<%= openBO.getEdlongitude() %>">

<input type="hidden" name="sesexit" id="sesexit" value="<%=adminBo==null?"0":"1"%>">

<% 	HashMap hmp_address;
	if(request.getAttribute("address") != null) {
		hmp_address = (HashMap)request.getAttribute("address");
		
  %>
 		<input type="hidden" name="stadd1" value="<%= hmp_address.get("stadd1").toString() %>">
 		<input type="hidden" name="stadd2" value="<%= hmp_address.get("stadd2").toString() %>">
 		<input type="hidden" name="stcity" value="<%= hmp_address.get("stcity").toString() %>">
 		<input type="hidden" name="ststate" value="<%= hmp_address.get("ststate").toString() %>">
 		<input type="hidden" name="stzip" value="<%= hmp_address.get("stzip").toString() %>">
 		<input type="hidden" name="edadd1" value="<%= hmp_address.get("edadd1").toString() %>">
 		<input type="hidden" name="edadd2" value="<%= hmp_address.get("edadd2").toString() %>">
 		<input type="hidden" name="edcity" value="<%= hmp_address.get("edcity").toString() %>">
 		<input type="hidden" name="edstate" value="<%= hmp_address.get("edstate").toString() %>">
 		<input type="hidden" name="edzip" value="<%= hmp_address.get("edzip").toString() %>">
<%
	}
%>

<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
        	<div class="headerDV">
            	<div class="headerDVInner">
                	<h1 class="logoLft"><a href="#" title="Get-A-Cab"></a></h1>
                    <div class="headerRhtDV">
                    <div class="headerRhtDVInner">	
                        <div class="loginName">
                        <div class="rhtCol">
                    	<h1 class="loginNameDV"><span></span></h1>
                        </div>
                         <div class="lftCol"></div>
                        <div class="clrBth"></div>
                        </div>
                        <div>
                        <div class="rhtCol">
                    		<div class="searchDV">
                                <span>Search:</span>
                                <div class="inputBXLC"><div class="inputBXRC"><input name="" type="text" class="inputBX" /></div></div>
                                <input class="searchBtn" name="" type="button" />
                            </div>
                        </div>
                        <div class="lftCol">
                        	<ul class="topNav">
                            	<li class="last"><a href="#" title="Contact Us">Contact Us</a></li>
                                <li><a href="#" title="About Us">About Us</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="clrBth"></div>
                           <div class="mainNav1">
                    	<div class="mainNav1L">
                        	<div class="mainNav1R">
                        	    <ul>
                                	<li><a href="control?action=registration&event=saveDriver&subevent=load" class="dispatch" title="Diver Reg">Diver Reg</a></li>
                                    <li><a href="control?action=registration&event=savecompany" class="operations" title="Company Reg">Company Reg</a></li>
                                    <li><a href="control?action=openrequest&event=saveRequest" class="finance" title="Open Request">Open Request</a></li>
                                    <li><a href="control?action=openrequest&event=getreceipt" class="reports" title="Get Receipt">Get Receipt</a></li>
                                    <li><a href="control?action=registration&event=checkUser" class="logout" title="Login">Login</a></li>
                                    <li><a href="control?action=registration&event=showsActiveUser" class="systemSetup" title="Active User">Active User</a></li>
                                </ul>
                                
                            </div>
                    	</div>
                    </div>
                                     
                    <div class="clrBth"></div>
                </div>
            </div>
                        <div class="contentDV">
            <div class="contentDVInWrap">
            <div class="contentDVIn">
            	<div class="leftCol">
                	<div class="innerPage">
                    </div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<div class="grayBX">
                    	<div class="topLCur"><div class="topRCur"><div class="topMCur"><div class="topMCur1"></div></div></div></div>
                        <div class="mid"><div class="mid1"><div class="mid2"><div class="mid3">
                        	<div class="formDV">
                        		<h2>Open Request</h2>
                        		<%
									String error ="";
									if(request.getAttribute("errors") != null) {
									error = (String) request.getAttribute("errors");
									}
								%>
								<div id="errorpage">
								<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %></div>
                                	<div class="fieldDv">
                                     <fieldset1>
                                    <div class="div3">
                                    	  <div class="btnBlue">
                                         <div class="rht">
                                         <input type="button" name="btnphone" class="lft" value="Check PhoneNo" tabindex="2" onclick="loadFromToAddr()">
                                         </div>
                                        </div>
                                        
                                    </div>
                                   
                                	<label1 for="Phone No">Phone No</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                   <input type="text" tabindex="1" name="phone" class="inputBX" id="phone" value="<%= openBO.getPhone()  %>" onkeyup="setPhone(this)">
                                    </div></div></div>
									</fieldset1>
                                    
                                	<fieldset>
                                    <label1 for="Name">Name</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" tabindex="3" name="name" class="inputBX" value="<%= openBO.getName() %>" >
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label1 for="Service Date">Service Date</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid100">
                                    <input readonly="readonly" type="text" tabindex="4" name="sdate" class="inputBX" size="10"  value="<%= openBO.getSdate()  %>" onfocus="showCalendarControl(this);">
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label1 for="Service Time">Service Time</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid70 fltBtm">
                                    <input type="text" tabindex="5" name="shrs" class="inputBX" maxlength="4"  size="4" value="<%= openBO.getShrs()  %>" >
                                    </div></div></div>
									</fieldset>
                                    </div>
                                    
                                <h3>From Address</h3>
                                    <div class="fieldDv">
                                    
                                    <fieldset>
                                    <label1 for="Intersection">Intersection</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="sintersection" class="inputBX" id="sintersection" tabindex="7" value="<%= openBO.getSintersection()  %>">
                                     </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label1 for="Land Mark">Land Mark</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="slandmark" class="inputBX" id="slandmark" tabindex="8" value="<%= openBO.getSlandmark()  %>">
                                    </div></div></div>
									</fieldset>
                                    
                                     <fieldset1>
                                    <div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btnsaircode" class="lft"   value="Check Source Aircode" tabindex="10" >
                                         </div>
                                        </div>
                                    </div>
                                   
<%--                                 	<label1 for="Air Port Code">Air Port Code</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                 	<input type="text" tabindex="9" id="saircodeid" name="saircodeid" class="inputBX" value="<%= openBO.getSaircodeid() %>" autocomplete="off"   class="form-autocomplete">
                                    <input type="hidden" id ="saircode" name = "saircode" value="<%= openBO.getSaircode() %>">
                                    <ajax:autocomplete
  									fieldId="saircodeid"
  									popupId="model-popup1"
  									targetId="saircode"
  									baseUrl="autocomplete.view"
  									paramName="aircode"
  									className="autocomplete"
  									progressStyle="throbbing"/>
  							 		</div></div></div>
									</fieldset1>
 --%>                                                                
                                                             	
                                    <fieldset>
                                    <label1 for="Address 1">Address 1</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" tabindex="11" id="sadd1" name="sadd1" class="inputBX" value="<%=openBO.getSadd1()  %>" class="form-autocomplete" onblur="loadOtherData()" >
                                    <ajax:autocomplete
				  					fieldId="sadd1"
  									popupId="model-popupsadd1"
  									targetId="sadd1"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress1"
  									className="autocomplete"
  									progressStyle="throbbing" /> 
  			                         </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label1 for="Address 2">Address 2</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="sadd2" class="inputBX" id="sadd2" tabindex="12"  value="<%= openBO.getSadd2()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()">
									<ajax:autocomplete
  									fieldId="sadd2"
  									popupId="model-popupsadd2"
  									targetId="sadd2"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress2"
  									className="autocomplete"
  									progressStyle="throbbing"/>
		                            </div></div></div>
									</fieldset>                                        
                                    
                                    
                                    <fieldset>
                                    <label1 for="City">City</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="scity" class="inputBX" id="scity" tabindex="13" value="<%= openBO.getScity()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()">
									<ajax:autocomplete
  									fieldId="scity"
  									popupId="model-popupscity"
  									targetId="scity"
  									baseUrl="autocomplete.view"
  									paramName="phonecity"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
  									</div></div></div>
									</fieldset>
                                    
                                    
                                    <fieldset>
                                    <label1 for="State">State</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="sstate" class="inputBX" id="sstate" tabindex="14" value="<%= openBO.getSstate()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()">
									<ajax:autocomplete
  									fieldId="sstate"
  									popupId="model-popupsstate"
  									targetId="sstate"
  									baseUrl="autocomplete.view"
  									paramName="phonestate"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
                                   	 </div></div></div>
									</fieldset> 
                                   
                                   
                                    
                                    <fieldset>
                                    <label1 for="Zip">Zip</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="szip" class="inputBX" id="szip" tabindex="15" value="<%= openBO.getSzip()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData()">
									<ajax:autocomplete
  									fieldId="szip"
  									popupId="model-popupszip"
  									targetId="szip"
  									baseUrl="autocomplete.view"
  									paramName="phonezip"
  									className="autocomplete"
  									progressStyle="throbbing"/>
  									</div></div></div>
									</fieldset>
									<% if(session.getAttribute("user") != null) {  %>
									<%
										ArrayList al_q = (ArrayList)request.getAttribute("al_q");
									%>
									<fieldset1>
									<div class="div3">
									<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="button" name="getzone" class="lft" value="Get Zone" tabindex="" onclick='loadQueue()' ></div>
                                        </div>
                                    </div>
                                    
                                    <label1 for="Zone">Zone</label1>
                                    <div class="div5"><div class="div5In">
	 								<select name='queueno' id='queueno' tabindex="">
	 								<option value=''>Select</option>
									<%
										if(al_q!=null){
										for(int i=0;i<al_q.size();i=i+2){ 
									%>
										<option value='<%=al_q.get(i).toString() %>' <%=openBO.getQueueno().equals(al_q.get(i).toString())?"selected":"" %>><%=al_q.get(i+1).toString() %></option>
									<%
									}
									} %>
									</select>
									</div></div>
									</fieldset1>
									<% } else{  %>
									 <fieldset1>
                                    <label1 for=""></label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap">
                                    <input type="hidden" name="queueno" id="queueno" value="<%=openBO.getQueueno() %>"></div></div></div>
                                    <%} %>
                                    </fieldset1>
                                    </div>                           
                                                                                                         
                                     <h3>To Address</h3>
                                    <div class="fieldDv">
                                    
                                    <fieldset1>
                                    <div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btneaircode" class="lft" value="Check Destination Aircode" tabindex="17" >
                                        </div>
                                        </div>
                                    </div>
<%--                                 	<label1 for="Air Port Code">Air Port Code </label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" tabindex="16" id="eaircodeid" name="eaircodeid" class="inputBX" value="<%= openBO.getEaircodeid() %>" autocomplete="off"   class="form-autocomplete">
									<input type="hidden" id ="eaircode" name = "eaircode" value="<%= openBO.getEaircode() %>">
									<ajax:autocomplete
  									fieldId="eaircodeid"
  									popupId="model-popup2"
  									targetId="eaircode"
  									baseUrl="autocomplete.view"
  									paramName="aircode"
  									className="autocomplete"
  									progressStyle="throbbing" />
									</div></div></div>
									</fieldset1>
 --%>                                                                  	
                                    <fieldset>
                                    <label1 for="Address 1">Address 1</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" tabindex="18" name="eadd1" class="inputBX" id="eadd1" value="<%= openBO.getEadd1()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()">
									<ajax:autocomplete
  									fieldId="eadd1"
  									popupId="model-popupeadd1"
  									targetId="eadd1"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress1"
  									className="autocomplete"
  									progressStyle="throbbing"/>
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label1 for="Address 2">Address 2</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="eadd2" class="inputBX" id="eadd2" tabindex="19"  value="<%= openBO.getEadd2()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()">
									<ajax:autocomplete
  									fieldId="eadd2"
  									popupId="model-popupeadd2"
  									targetId="eadd2"
  									baseUrl="autocomplete.view"
  									paramName="phoneaddress2"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label1 for="City">City</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="ecity" class="inputBX" id="ecity" tabindex="20" value="<%= openBO.getEcity()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()">
									<ajax:autocomplete
  									fieldId="ecity"
  									popupId="model-popupecity"
  									targetId="ecity"
  									baseUrl="autocomplete.view"
  									paramName="phonecity"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label1 for="State">State</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="estate" class="inputBX"  id="estate" tabindex="21" value="<%= openBO.getEstate()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()">
									<ajax:autocomplete
  									fieldId="estate"
  									popupId="model-popupestate"
  									targetId="estate"
  									baseUrl="autocomplete.view"
  									paramName="phonestate"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset1>
                                    <div class="div3">
                                    	<div class="btnBlueNew1">
                                         <div class="rht">
                                         <input type="submit" name="btnfromaddress" class="lft" value="Check From Address" tabindex="24" >
                                         </div>
                                        </div>
                                    </div>
                                    
                                    <label1 for="Zip">Zip</label1>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid130">
                                    <input type="text" name="ezip" id="ezip" tabindex="22" class="inputBX" value="<%=openBO.getEzip()  %>" autocomplete="off"   class="form-autocomplete" onblur="loadOtherData1()">
									<ajax:autocomplete
  									fieldId="ezip"
  									popupId="model-popupezip"
  									targetId="ezip"
  									baseUrl="autocomplete.view"
  									paramName="phonezip"
  									className="autocomplete"
  									progressStyle="throbbing" />
                                    </div></div></div>
									  </fieldset1>
                                    
                                     <fieldset1 >
                                    <div class="div3">
                                    	<div class="btnBlueNew1 padT10 ">
                                         <div class="rht">
                                         <input type="submit" name="btntoaddress" class="lft" value="Check To Address" tabindex="25">
                                         </div>
                                    </div></div>
                                      </fieldset1>
                                  
                                                                       
                                    <fieldset>
                                    <div class="div2"><div class="div2In">
                                    		<div class="wid85 padT10 padR5 ">
                                          <div class="btnBlue">
                                               <div class="rht">
                                                <input type="submit" name="submit" class="lft" value="Submit" tabindex="27" >
                                               </div>
                                               <div class="clrBth"></div>
                                          </div>
                                          <div class="clrBth"></div>
                                    </div>
                                    
                                    </div></div>
									</fieldset>
                                    
                                    
                                    
                                    </div>    
                                
                        	</div>
                        </div></div></div></div>
                        <div class="btmLCur"><div class="btmRCur"><div class="btmMCur"><div class="btmMCur1"><div class="btmMCur2"><div class="btmMCur3"><div class="btmMCur4"><div class="btmMCur5"></div></div></div></div></div></div></div></div>
                    </div>
                    
                </div>    
              </div>
            	
                <div class="clrBth"></div>
            </div>
            </div>
            </div>
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
		</div>
	</div>
</div>
</form>
</body>
</html>
