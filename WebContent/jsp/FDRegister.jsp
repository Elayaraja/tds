<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.tdsBO.LostandFoundBO,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<jsp:useBean id="lostandfoundBo" class="com.tds.tdsBO.LostandFoundBO" scope="request"/>
<% if(request.getAttribute("lostandfound") != null)
	lostandfoundBo = (LostandFoundBO)request.getAttribute("lostandfound");

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form method="post" action="control" name="lostandfound">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.createLostorFoundDetail %>">
<input type="hidden" name="lostandfoundNo" value="<%= lostandfoundBo.getLfKey()  %>">
<input type="hidden" name="operation" value="2">

  
  
 	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div class="rightCol">
<div class="rightColIn">
<h1>FD Register</h1>
	<%
									String error ="";
									if(request.getAttribute("errors") != null) {
									error = (String) request.getAttribute("errors");
									}		
							    %>
							    <div id="errorpage">
							    <%= error.length()>0?""+error :"" %>
							         </div>                 		 
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
                    <td>
                    <table id="bodypage" width="440"    >
                    <tr>
                    	<td class="firstCol" >MID</td>
                    	<td><input type="text" name="MID"    value="000199770003994"/></td>
                    </tr>
                       <tr>
                    	<td class="firstCol" >TID</td>
                    	<td><input type="text"  name="TID"    value="01099794"/></td>
                    </tr>
                    	<tr>
                    	<td class="firstCol" >SID</td>
                    	<td><input type="text"  name="SID"    value="115"/></td>
                    </tr>
                       <tr>
                    	<td class="firstCol" > XML-Text</td>
                    	<td><textarea name="xml_text"   rows="4" cols="25"><%= lostandfoundBo.getItemDesc() != null ? lostandfoundBo.getItemDesc() : ""   %></textarea></td>
                    </tr>
                    <tr align="center" >
                     
                    	
                    			<td colspan="2" align="center">
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	      <input type="submit" name="submit" class="lft" value="submit"/>
							      
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
				</td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    </table>
</div>
</div>
                        	 
																		
                                                     
                
            	
                <div class="clrBth"></div>
           
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</body>
</html>

									
									
