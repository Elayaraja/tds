<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
 <link href='http://ajax.googleapis.com/ajax/libs/dojo/1.7.4/dijit/themes/claro/claro.css' rel='stylesheet' type='text/css'/>
<script>dojoConfig = {parseOnLoad: true};</script>
 <script src="http://ajax.googleapis.com/ajax/libs/dojo/1.8.2/dojo/dojo.js"></script> 
 <script>
 dojo.require("dijit.form.Button");
 dojo.require("dijit.Dialog");
 dojo.require("dijit.form.TextBox");
 dojo.require("dijit.form.DateTextBox");
 dojo.require("dijit.form.TimeTextBox");
 function checkData(){
     var data = formDlg.get('value');
     console.log(data);
     if(data.sdate > data.edate){
         alert("Start date must be before end date");
         return false;
     }else{
         return true;
     }
 }
	function showProcess(){
		if(document.getElementById("TDSAppLoading")!=null){
			$("#TDSAppLoading").show();
		}
		return true;
	}

 </script>
<script type="text/javascript">
	 $(document).ready(function(){
		 <%if(request.getAttribute("value")==null){%>
		 currentDate();
		 <%}%>
		}); 
	function currentDate() {
		var curdate = new Date();
		var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
		var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
		var cYear = curdate.getFullYear();
		var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
		var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
		var hrsmin = hors+""+min;  
		document.getElementById("fromDate").value =cMonth+"/"+cDate+"/"+cYear;
		document.getElementById("toDate").value =cMonth+"/"+cDate+"/"+cYear;
	 }
	
	function showLogs(i,routeNo){
		var img=document.getElementById("showLog_"+i).src;
		if(img.indexOf("ffd40f_11x11_icon_plus.gif")!= -1){
			document.getElementById("showLog_"+i).src ="images/ffd40f_11x11_icon_minus.gif";
			 var xmlhttp = null;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			url ='DashBoard?event=ajaxSharedLogs&routeNo='+routeNo;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText; 
			if(text!=""){
				var jsonObj = "{\"sharedLogs\":"+text+"}" ;
				var objectNew = JSON.parse(jsonObj.toString());
				document.getElementById("sharedlogs_"+i).innerHTML ="";
				var $tbl = $('<table style="width:100%">').attr('id', 'sharedJobs');
				$tbl.append($('<tr>').append(
						$('<th>').text("TripId"),
						$('<th>').text("Status"),
						$('<th>').text("Created At"),
						$('<th>').text("Created By")
				)
				);
				for(var j = 0; j < objectNew.sharedLogs.length; j++){
					$tbl.append($('<tr>').append(
							$('<td style="width:5%" align="center">').text(objectNew.sharedLogs[j].TI),
							$('<td style="width:10%" align="center">').text(objectNew.sharedLogs[j].SA),
							$('<td style="width:5%" align="center">').text(objectNew.sharedLogs[j].CT),
							$('<td style="width:5%" align="center">').text(objectNew.sharedLogs[j].CB)
				)
				);
				}
				$("#sharedlogs_"+i).append($tbl);
				$("#sharedlogs_"+i).show('slow');
			}
		}else{
			document.getElementById("showLog_"+i).src="images/ffd40f_11x11_icon_plus.gif";
			document.getElementById("sharedlogs_"+i).innerHTML ="";
			$("#sharedlogs_"+i).hide('slow');
		}
	}
	function submitValues(){
		var stdate= dojo.date.locale.format(dijit.byId("fromDate").value,{datePattern: "yyyy-MM-dd", selector: "date"});
		var edDate= dojo.date.locale.format(dijit.byId("toDate").value,{datePattern: "yyyy-MM-dd", selector: "date"});
		document.getElementById("fromDate").value=stdate;
		document.getElementById("toDate").value=edDate;
		document.masterForm.submit();
	}
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body class="claro">
	<form name="masterForm" action="control" method="post" onsubmit="return showProcess()">
		<%
			ArrayList<OpenRequestBO> sharedRideLogs = (ArrayList<OpenRequestBO>) request.getAttribute("sharedJobLogs");
			String search = (String) request.getAttribute("routeNo");
			String fDate = (String) request.getAttribute("fDate");
			String tDate = (String) request.getAttribute("tDate");
		%>
		<%
			AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
		%>
		<input type="hidden" name="action" value="openrequest" /> <input
			type="hidden" name="event" value="sharedJobLogs" /> <input
			type="hidden" name="module" id="module" value="dispatchView" />
			<c class="nav-header"><center>Shared Ride Logs</center></c>
			<div id="jobHistory">
			<table width="100%" style="width:100%;"  cellspacing="1" cellpadding="0" class="navbar-static-top">
						<tr>
						<td  ><font color="black">RouteNo:</font></td>
							<td><input type="text" name="routeNo" id="routeNo" size="10" value="<%=search==null?"":search %>" />
							</td>
								<td  ><font color="black">From Date:</font>
							<input type="text" data-dojo-type="dijit.form.DateTextBox" name="fromDate" id="fromDate" size="10" value="<%=fDate==null?"":fDate %>" />
							
							</td>	
							
							
						<td  ><font color="black">To Date:&nbsp;&nbsp;&nbsp;&nbsp;</font>
							<input type="text" data-dojo-type="dijit.form.DateTextBox" name="toDate" id="toDate" size="10" value="<%=tDate==null?"":tDate %>" />
							
							</td>	
							
							</tr>
											<tr>

							<td colspan="4" align="center">
											<input type="button" name="button" id="button" value="Submit"  onclick="submitValues()"/>
							</td>
						</tr>
					</table>
					
					<%
						if (sharedRideLogs != null && sharedRideLogs.size() > 0) {
					%>
					<div style="height:400px;width:100%;overflow:auto;">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="thumbanil">
									<tr>
										<td style="width: 10%;" align="center">Route ID</td>
										<td style="width: 13%;" align="center">CreatedBy</td>
										<td style="width: 13%;" align="center">Time</td>
										<td style="width: 13%;" align="center">Date</td>
										<td style="width: 20%;" align="center">Reason</td>
										<td style="width: 20%;" align="center">Status</td>
										</tr>
										<%
											boolean colorLightGreen = true;
												String colorPattern;
												
										%>
										<%
											for (int i = 0; i < sharedRideLogs.size(); i++) {
													colorLightGreen = !colorLightGreen;
													if (colorLightGreen) {
														colorPattern = "style=\"background-color:lightgreen;white-space: nowrap;\"";
														
													} else {
														colorPattern = "white-space: nowrap;";
													}
										%>
										<tr >
											<td align="center" <%=colorPattern%>><font color="black"><%=sharedRideLogs.get(i).getRouteNumber()==null?"":sharedRideLogs.get(i).getRouteNumber()%></font>
											</td>
											<td align="center" <%=colorPattern%>><font color="black"><%=sharedRideLogs.get(i).getCreatedBy()==null?"":sharedRideLogs.get(i).getCreatedBy()%></font>
											</td>
											<td align="center" <%=colorPattern%>><font color="black"><%=sharedRideLogs.get(i).getCreatedTime()==null?"":sharedRideLogs.get(i).getCreatedTime()%></font>
											</td>
											<td align="center" <%=colorPattern%>><font color="black"><%=sharedRideLogs.get(i).getCreatedDate()==null?"":sharedRideLogs.get(i).getCreatedDate()%></font>
											</td>
											<td  <%=colorPattern %>"  align="center" ><font color="black"><%=sharedRideLogs.get(i).getSharedLogReason()==null?"":sharedRideLogs.get(i).getSharedLogReason()%></font>
											</td>
											<td align="center" <%=colorPattern%>><font color="black"><%=sharedRideLogs.get(i).getShRideStatus()==null?"":sharedRideLogs.get(i).getShRideStatus()%></font></td>
											<td align="center" <%=colorPattern%>><font color="black"><img src ="images/ffd40f_11x11_icon_plus.gif" name="showLog_<%=i%>" id="showLog_<%=i%>" onclick="showLogs('<%=i%>','<%=sharedRideLogs.get(i).getRouteNumber()%>')"></img></font></td>
										</tr>
										<tr><td colspan="8"><div id="sharedlogs_<%=i%>"></div></td></tr>
										<%
											}
											}
										%>
									
								</table>
					</div>
				</div>
	</form>
</body>
</html>
