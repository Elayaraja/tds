<%@page import="com.tds.tdsBO.DriverLocationHistoryBO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment" %>
<%@page import="com.common.util.TDSProperties"%>


<head>
<%	
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	ArrayList<DriverLocationHistoryBO> driverLocation=new ArrayList<DriverLocationHistoryBO>();
	if(request.getParameter("driverLocation")!=null || request.getAttribute("driverLocation")!=null){
		driverLocation=(ArrayList<DriverLocationHistoryBO>)request.getAttribute("driverLocation");
	}
	double distance=0;
	double fuelConsumption=0;
	
	if(request.getAttribute("distance")!=null){
		distance=(Double)request.getAttribute("distance");
	}
	if(request.getAttribute("fuelConsumption")!=null)
	{ 
		fuelConsumption=(Double)request.getAttribute("fuelConsumption");
	}
	double carbonEmission=0;
	if(request.getAttribute("carbonEmission")!=null){
		 carbonEmission=(Double)request.getAttribute("carbonEmission");
	}
	DriverLocationHistoryBO driverLocationBO=new DriverLocationHistoryBO(); 
%>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script src='<%=TDSProperties.getValue("googleMapV3")%>'></script>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/label.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<%if(driverLocation!=null && driverLocation.size()>0){%>
	<script type="text/javascript" src="js/main4.js"></script>
<%} %> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
</head>

<body>		
<form  name="driverlocation"  action="control" method="post" onsubmit="retrun showProcess()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="action" value="registration"/>
<input type="hidden" name="event" value="driverLocation"/>
<input type="hidden" id="context" value="<%=request.getContextPath()%>"/>
<input type="hidden" name="operation" value="2"/>

 <input type="hidden" name="sLatitude" id="sLatitude" value="<%=adminBo.getDefaultLati()%>"/>
  <input type="hidden" name="sLongitude" id="sLongitude" value="<%=adminBo.getDefaultLogi()%>"/>
<div>
	<c class="nav-header"><center>
		Driver Search</center></c>
		<br />
 <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                    <td>
                    <table id="bodypage" width="80%" align="center">
                    <tr>
                    	<td>Driver Id</td>
                    	<td>
							 <input type="text" name="driverId" id="driverId" value="" autocomplete="off"/>
							 <ajax:autocomplete
				  					fieldId="driverId"
				  					popupId="model-popup1"
				  					targetId="driverId"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 	
				  					</td>
				  		</tr>
 <!-- <td class="firstCol" >Last Updated Date</td>
                    	<td>
 <input type="text" name="lastUpdatedDate" id="lastUpdatedDate" value=""/>
</td>

					 <tr> -->
<!-- 					 <tr>
					 <td>TripId</td>
					 	<td><input type="text" name="tripId" id="tripId" value=""  autocomplete="off"/></td>
					 </tr>
 -->					 <tr>
					 <td>From</td>
					 	<td><input type="text" name="fromDate" id="fromDate" value="" onclick="showCalendarControl(this)" autocomplete="off"/></td>
					 </tr>
					 <tr>
					 <td>To</td>
					 	<td><input type="text" name="toDate" id="toDate" value="" onclick="showCalendarControl(this)" autocomplete="off"/></td>
					 </tr>
					 <tr>
					 <td colspan="2" align="center">
										<div class="wid60 marAuto padT10">					                     
					 <input type="submit" name="showmap" id="showmap" value="Show Map"/>
					 </div>					                            					                    		
					</td>
					</tr>
					 </table></td></tr></table>					    
 </div>
 <%if(driverLocation!=null && driverLocation.size()>0){%>
  
  <div id="map_canvas" align="center" style="position: absolute; width: 1000px; height: 500px; margin-top:110px;margin-top: 10%;" ></div>
 
    <table id="driverDetailDisplay" align="center">
 <tr>
 <%for(int i=0;i<driverLocation.size();i++){ %>
 <td>
  <input type="hidden" name="driverLati" id="driverLati" value="<%=driverLocation.get(i).getLatitude()%>"/>
  <input type="hidden" name="driverLongi" id="driverLongi" value="<%=driverLocation.get(i).getLongitude()%>"/>
  <input type="hidden" name="status" id="status" value="<%=driverLocation.get(i).getStatus()%>"/>
  <input type="hidden" name="lastUpdate" id="lastUpdate" value="<%=driverLocation.get(i).getUpdatedTime()%>"/>
   <input type="hidden" name="driverId" id="driverId" value="<%=driverLocation.get(i).getDriverid()%>"/>
   <input type="hidden" name="distance" id="distance" value="<%=distance%>"/>
   <input type="hidden" name="fuel" id="fuel" value="<%=fuelConsumption%>"/>
   <input type="hidden" name="carbon" id="carbon" value="<%=carbonEmission%>"/>
</td>
<%} %>
</tr>
</table>
<%} %>
<%String error="";
                        if(request.getAttribute("error")!=null){
                        error=(String) request.getAttribute("error");
                        } else if(request.getParameter("error")!=null){
                        error=(String)request.getParameter("error");
                        } %>
<table>                       
<tr><td colspan="8">
<div id="errorpage" style=" color: red">
<%=error.length()>0?""+error:""%>
</div></td></tr></table>  
</form>
</body>
</html>