<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>


<%@page import="java.io.FileNotFoundException"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.Address" %>
<%@page import="com.tds.cmp.bean.CustomerProfile" %>

<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%
Address  add = (Address)request.getAttribute("add");   
CustomerProfile  cprofile = (CustomerProfile)request.getAttribute("cprofile"); 
ArrayList<Address> addHist =  (ArrayList<Address>) request.getAttribute("AddressHistory");
ArrayList<CustomerProfile> profHist =  (ArrayList<CustomerProfile>) request.getAttribute("ProfileHistory");
%> 
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>User Address Details</title>
<script type="text/javascript">
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>
</head>
<body>
<form  name="masterForm"  action="control" method="post" onsubmit="return showProcess()">

   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %><input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <input type="hidden" name="action" value="registration"/>
		<input type="hidden" name="event" value="custInfo"/>
		
		<input type="hidden" name="operation" value="2"/>
        
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
		<input type="hidden"  name="key"  value="  <%=request.getAttribute("key") %>" /> 
         
      <%String result="";
			if(request.getAttribute("page")!=null) {
			result = (String)request.getAttribute("page");
			}
			%>
			<div id="errorpage" style="">
			<%=result.length()>0? result:"" %>
			</div>        
			<c class="nav-header"><center>User Address Details</center></c>
				<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
				   <tr>
				     <td>Phone No:</td>
				     <td>  
			         <input type="text"  name="Number"  id="Number"  value="<%=cprofile.getNumber() %>"/>
			         </td>
                 	 <td>Name</td>
                 	 <td>                      	    
                 	 <input type="text"  name='Name'  id="Name"  value="<%=cprofile.getName() %>"/>
                 	 </td>
                     </tr>
                	<tr align="center">                    
                    <td colspan="4" align="center">
					  	 <input type="submit"  name="button" id="button"   class="lft" value="Submit" />
                    </td>
				    </tr> 
                </table>
                <br />
               <center>  <h5>Client Details </h5></center>
               <table width="100%" border="1" cellspacing="0" cellpadding="0" align="center">
                <tr style="background-color: rgb(46, 100, 121); color: white">
            	  <th width="9%">Phone NO:</th> 
            	  <th width="4%">Master Key</th> 
            	  <th width="8%">Name</th> 
            	  <th width="14%">Address1</th>
            	  <th width="10%">Address2</th>	
            	  <th width="7%">City</th>	
            	  <th width="4%">State</th>	
            	  <th width="5%">Zip</th>	
            	  <th width="6%">Payment Type</th> 
            	  <th width="4%">Account</th> 
            	  <th width="5%">Last Four Digit</th> 
            	  <th width="7%">Description</th>
            	  <th width="4%">Premium Customer</th> 
            	  <th width="4%">Corporate Code</th>
            	  <th width="8%"></th> 
 				</tr>
                       <% 
						boolean colorLightGreen = true;
						String colorPattern;%>
						<%for(int i=0;i<profHist.size();i++) {  
							colorLightGreen = !colorLightGreen;
							if(colorLightGreen){
								colorPattern="style=\"background-color:rgb(242, 250, 250)\""; 
								}
							else{
								colorPattern="style=\"background-color:white\"";
							}
						%>
					                                      
					  <tr>
					    <td  align="center"<%=colorPattern%>> <%=profHist.get(i).getNumber() %> </td> 
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getMasterKey() %></td>					    
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getName() %>
					    <input type='hidden' name='Name<%=(i)%>' id='Name<%=(i)%>' value=<%=profHist.get(i).getName() %>/> </td>  
					    <td  align="center"<%=colorPattern%>> <%=profHist.get(i).getAdd1() %> </td>
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getAdd2() %> </td>
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getCity() %> </td>
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getState() %> </td>
					    <td  align="center"<%=colorPattern%>> <%=profHist.get(i).getZip() %> </td>
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getPaymentType() %> </td> 
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getAccount() %> </td>
					    <input type="hidden" name="account" id="account" value="<%=profHist.get(i).getAccount() %>"/> 					   
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getCClastfourdigits() %> </td> 
	     				<td  align="center"<%=colorPattern%>> <%=profHist.get(i).getDescription() %> </td> 
					    <td  align="center"<%=colorPattern%>> <%=profHist.get(i).getPremiumcustomer() %> </td> 
					    <td  align="center"<%=colorPattern%> > <%=profHist.get(i).getCorporateCode() %> </td> 				  
		 				<td colspan="4" align="center" <%=colorPattern%>>
                         <input type="submit"  name="deleteUserMaster" id="deleteUserMaster" value="Delete" />
	 			         <a style="text-decoration: none;"style="text-align: center;"  href ="control?action=registration&event=custInfo&event2=edit&module=operationView&masterKey=<%=profHist.get(i).getMasterKey() %>">Edit</a>	    
                       	</td>	     						  
	     				
	     				<%}if((profHist.size()==0 && request.getParameter("button")!=null &&(request.getParameter("Number")!="" || request.getParameter("Name")!=""))){ %>
                                    	 <font style="color:red;size:10">No records found</font>
                                	 <%}%>
                     	<%if(profHist.size()==0 && request.getParameter("button")!=null &&(request.getParameter("Number")=="" && request.getParameter("Name")=="") ){ %>
                                   	 <font style="color:red;size:10">Fill anyone of the fields to get details</font>
                                	 <%}%>
					  </tr>                                	 
                  	  </table>
              <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 </form>
 </body>
</html> 