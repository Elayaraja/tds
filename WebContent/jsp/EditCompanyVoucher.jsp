<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.tds.tdsBO.VoucherBO,java.util.ArrayList"%>
<%@page import="com.common.util.TDSConstants;"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
 <script type="text/javascript">
 function cal(){
pop=window.open("control?action=registration&event=CompanyDetailview","win2","width=400,height=500");
pop.focus();
}</script>
</head>
<body>
<form method="post" action="control" name="masterForm">
<jsp:useBean id="voucherBo" class="com.tds.tdsBO.VoucherBO" scope="request"/>
<% if(request.getAttribute("voucherentry") != null)
	voucherBo = (VoucherBO)request.getAttribute("voucherentry");
	%>
<jsp:setProperty name="voucherBo" property="*" />
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.getAllcompanyVoucher %>">
<table>
<tr>
<td>
<table id="pagebox">
	<tr>
		<td>
			<table id="bodypage">
			<tr>
				<td id="title" colspan="7" align="center"><b>Change&nbsp;Company&nbsp;Voucher&nbsp;Entry</b></td>
			</tr>
			<tr>
			<%
			String error ="";
			if(request.getAttribute("errors") != null) {
			error = (String) request.getAttribute("errors");
			}		
			%>
			<td colspan="7">
				<div id="errorpage">
					<%= error.length()>0?"You must fulfilled the following error<br>"+error :"" %>
				</div>
			</td>
			</tr>
			<tr>
				<td>Voucher&nbsp;No</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  name="vno" value="<%= voucherBo.getVno()  %>" >
				</td>
				<td>&nbsp;</td>
				<td>Rider&nbsp;Name</td>
				<td>&nbsp;</td>
				<td>
					<input type="text"  name="vname" value="<%= voucherBo.getVname() %>">
				</td>
			</tr>
			<tr>
				<td>Company&nbsp;ID</td>
				<td>&nbsp;</td>
				<td><input type="text"  id="vcode" name="vcode"  readonly="readonly" value="<%= voucherBo.getVcostcenter()  %>"  >
				</td>
				<td>	<img src="images/calendar.gif" onclick="cal()"></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>From Date</td>
				<td>&nbsp;</td>
				<td>
				<input type="text" name="from_date" size="10" value="<%=voucherBo.getFrom_date() %>" readonly="readonly" onfocus="showCalendarControl(this);">
									</td>	
				<td>&nbsp;</td>
				<td>To Date</td>
				<td>&nbsp;</td>
				<td>
				 	<input type="text" name="to_date" size="10" value="<%=voucherBo.getTo_date() %>" readonly="readonly" onfocus="showCalendarControl(this);">
									</td>
			 </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="7"><input type="submit" name="submit" value="Show Vouchers"></td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table id="pagebox">
	 
	<%if(request.getAttribute("summary")!=null){ 
		ArrayList voucher_data=(ArrayList)request.getAttribute("summary");
	%>
	<tr>
		<td>
			<table id="bodypage" border="1" align="center" cellpadding="0" cellspacing="0" width="600">
				<tr align="center">
					<td>S.No</td>
					<td>Voucher&nbsp;No</td>
					<td>Rider&nbsp;Name</td>
					<td>Company&nbsp;Name</td>
					<td>Amount</td>
					<td>Status</td>
				</tr>
				<%for(int count=0;count<voucher_data.size();count++){
					voucherBo = (VoucherBO)voucher_data.get(count); %>
				<tr align="center">
					<td><%=count+1%></td>
					<td><a  href="control?action=registration&event=CompanycreateVoucher&VNO=<%=voucherBo.getVno()%>" style="text-decoration: none; color: red"><%=voucherBo.getVno()%></a></td>
					<td><%=voucherBo.getVname() %></td>
					<td><%=voucherBo.getVcode()	 %></td>
					<td>$<%=voucherBo.getVamount()%></td>
					<td><%=voucherBo.getVstatus()%></td>
					
				</tr>
				<%} %>
			</table>
			</td>			
		</tr>
	<%} %>
</table>
</td>
</tr>
</table>
</form>
</body>
</html>