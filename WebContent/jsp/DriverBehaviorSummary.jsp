<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList,com.tds.tdsBO.DriverBehaviorBO,java.util.List,com.common.util.TDSConstants"%>
<html>
<%! DriverBehaviorBO m_behaviorBO = null;List m_behaviorList = null; String m_type = "";%>
<%! 
	String m_behaviorName[] =  {"Select","Penalty", "Bonus"  };
	String m_behaviorType[] = { "","BP","BB"};
	int typeCount = 0;
%>
<%
	System.out.println("In JSP "+request.getAttribute("behavior"));
	if(request.getAttribute("behavior") != null) {
		m_behaviorList = (ArrayList) request.getAttribute("behavior");
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
<form name="behaviorSummary" action="control" method="post">

<table id="pagebox" cellspacing="0" cellpadding="0" >
<tr>
		<td>

<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.behaviorList %>">
			<table id="bodypage" width="900">	
	<tr>
		<td colspan="7" align="center">		
			<div id="title">
				<h2>Lost&nbsp;Found&nbsp;Criteria</h2>
			</div>
		</td>		
	</tr>
	
	<tr>
		<td>Behavior&nbsp;Type</td>
		<td>&nbsp;</td>
		<td>
			<select name="btype">
						<%
							for(typeCount = 0; typeCount < m_behaviorType.length; typeCount ++ ) {
						 %>
									<option value="<%= m_behaviorType[typeCount] %>"> <%= m_behaviorName[typeCount] %> </option>
						<%
								} 
						%>
						
		</select>
		</td>
		<td>&nbsp;</td>
		<td>Behavior&nbsp;Date</td>
		<td>&nbsp;</td>
		<td>
			<input type="text"  size="10" name="bdate" value="" >
			<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.behaviorSummary.bdate);return false;" HIDEFOCUS>
			<img name="popcal" align="absmiddle" src="images/calendar.gif" width="25" height="16" border="0" alt=""></a>		
			<iframe width=20  height=178  name="gToday:normal:agenda.jssdate" id="gToday:normal:agenda.jssdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; top:-500px; left:-500px;">		
			</iframe>
		</td>
	</tr>
	
	<tr>
		<td colspan="7" align="center">
			<input type="submit" name="lostandFound" value="Get Details">			
		</td>
	</tr>
</table>

		</td>
	</tr>	
<% if(m_behaviorList != null && m_behaviorList.size() >0  ) {  %>
<tr>
	<td>
		<table id="bodypage" width="900">
			<tr>
				<td>Sno</td>
				<td>Penalty&nbsp;Date</td>
				<td>Penalty&nbsp;Type</td>
				<td>Comments</td>
				<td>Penalty&nbsp;Amount</td>
				<td>Driver&nbsp;Name</td>
			</tr>
			<% for(int count = 0; count < m_behaviorList.size(); count++) {  
				m_behaviorBO = (DriverBehaviorBO) m_behaviorList.get(count);
			%>
			<tr>
				<td><%= count+1 %> </td>
				<td><%= m_behaviorBO.getBehaviorDate() %></td>
				<td> 
					<% 
						if(m_behaviorBO.getBehaviorType().equalsIgnoreCase("BP")) {
							m_type = "Penalty";
						} else if(m_behaviorBO.getBehaviorType().equalsIgnoreCase("BB")) {
							m_type = "Bonus";
						}
					%>
					<%= m_type %>
				</td>
				<td><%= m_behaviorBO.getComment() %></td>
				<td>
					<a href = "control?action=registration&event=behaviorById&behaviorID=<%= m_behaviorBO.getBehaviorKey() %>">
					<%= m_behaviorBO.getAmount() %>
					</a>
				</td>
				<td><%= m_behaviorBO.getDriverId() %></td>
			</tr>
			<% } %>
		</table>
	</td>
</tr>
<% } else { %>
	<tr>
		<td>No records available</td>
	</tr>
<% } %>
</table>
</form>
</body>
</html>