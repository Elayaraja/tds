<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="java.util.ArrayList"%>
<html>
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
function setPhone(p_phone) {
	if(p_phone.value.length == 3) {
		p_phone.value += "-";
	} else if(p_phone.value.length == 7) {
		p_phone.value += "-";
	}
}
</script>
</head>
<body>
<form name="openForm" action="control" method="post">
<jsp:useBean id="openRequestBO"   class="com.tds.tdsBO.OpenRequestBO" scope="request"  />
<% if(request.getAttribute("openrequest") != null)
	openRequestBO = (OpenRequestBO)request.getAttribute("openrequest");
	ArrayList al_open = null;
	if(request.getAttribute("openSummary") != null) 
		al_open = (ArrayList)request.getAttribute("openSummary");
	%>
<jsp:setProperty name="openRequestBO"  property="*"/>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.getAcceptRequestCriteria %>">

<table id="pagebox" cellspacing="0" cellpadding="0" >
	<tr>
		<td>
<table id="bodypage" width="800" >	
	<tr>
		<td colspan="8" align="center">		
			<div id="title">
				<h2>Accepted&nbsp;Request&nbsp;Criteria</h2>
			</div>
		</td>		
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
	</tr>
	
	<tr>
		<td colspan="2">Trip&nbsp;ID</td>
		<td colspan="2"> 
			<input type="text" name="tripid" value="<%= openRequestBO.getTripid() %>"> 
		</td>
		<td colspan="2">Phone&nbsp;no</td>
		<td colspan="2">
			<input type="text" name="phone" value="<%= openRequestBO.getPhone() %>" onkeyup="setPhone(this)">
		</td>
	</tr>
	<tr>
		<td colspan="2">Service&nbsp;Date</td>
		<td colspan="2">
			<input type="text" name="sdate" value="<%= openRequestBO.getSdate() %>" onfocus="showCalendarControl(this);"  readonly="readonly">
		</td>
		<td colspan="2">Starting&nbsp;City</td>
		<td colspan="2">
			<input type="text" name="scity" value="<%= openRequestBO.getScity() %>">
		</td>
	</tr>
	<tr>
		<td colspan="2">Starting&nbsp;State</td>
		<td colspan="2">
			<input type="text" name="sstate" value="<%= openRequestBO.getSstate() %>">
		</td>
		<td colspan="2">Starting&nbsp;Zipcode</td>
		<td colspan="2">
			<input type="text" name="szip" value="<%= openRequestBO.getSzip() %>">
		</td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="8" align="center">
			<input type="submit" name="acceptrequest" value="Accept Request">			
		</td>
	</tr>
</table>

</td>
</tr> 
<% if(al_open != null && al_open.size() >0  ) {  %>

<tr>
	<td>
		<table id="bodypage" width="800" border="1" cellpadding="0" cellspacing="0" >
			<TR>
					<TD colspan="6">&nbsp;</TD>
			</TR>
			<tr align="center" bgcolor="#A5A5A5">
				<td >Sno</td>
				<td>Service&nbsp;Date</td>
				<td>Service&nbsp;Time</td>
				<td>From&nbsp;Address</td>
				<td>To&nbsp;Address</td>
				<td>Phone no</td>
			</tr>
			<% for(int count = 0; count < al_open.size(); count++) {  
				openRequestBO = (OpenRequestBO) al_open.get(count);
			%>
			<tr valign="top">
				<td><%= count+1 %> </td>
				<td><%= openRequestBO.getSdate() %></td>
				<td><%= openRequestBO.getSttime() %></td>
				<td><%= openRequestBO.getSadd1()+"<br>"+openRequestBO.getSadd2()+"<br>"+openRequestBO.getScity()+"<br>"+openRequestBO.getSstate()+"<br>"+openRequestBO.getSzip() %></td>
				<td><%= openRequestBO.getEadd1()+"<br>"+openRequestBO.getEadd2()+"<br>"+openRequestBO.getEcity()+"<br>"+openRequestBO.getEstate()+"<br>"+openRequestBO.getEzip() %></td>
				<td>
					<a href="control?action=openrequest&event=getAcceptCriteria&acceptrequest=Change Accepted Request&tripid=<%= openRequestBO.getTripid() %>" ><%= openRequestBO.getPhone() %></a>
				</td>
			</tr>
			<% } %>
		</table>
	</td>
</tr>
<% } else {%>
	<tr>
		<td>No records available</td>
	</tr>
<% } %>
</table>
</form>
</body>
</html>