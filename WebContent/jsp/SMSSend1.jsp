<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.common.util.TDSConstants"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
function disabledriver()
{
	if(document.getElementById('all').checked)
	{
		document.getElementById('driver_id').value="";
		 
		document.getElementById('driver_id').disabled = true;
	} else {
		 
		document.getElementById('driver_id').disabled = false;
	}
} 
function cal(){
	
	var a=document.getElementById('driver_id').value;
	var c=document.getElementById('msg').value;
	if(a == "" && document.getElementById('all').checked == false) { 
		document.getElementById('validation').innerHTML="<font color='red'>DriverId Invalid</font>";
		return false;
	}
	 
	 if(c==""){
			document.getElementById('validation').innerHTML="<font color='red'>Invalid Description</font>";
			return false;
			} 
	return true;
		
	} 
function formsubmit(){
	var result=cal();
	if(result==true)
	{
		document.getElementById('Button').value="Submit";
		
		document.masterForm.submit();
	}
}
</script> 
</head>
<body>
<form name="masterForm" id="masterForm" action="control" method="post" >
	<input type="hidden" name="action" value="openrequest">
	<input type="hidden" name="event" value="<%=TDSConstants.getSendSMS %>">
	<table id="pagebox">	
		<tr>
		<td>  
			<table id="bodypage">	
				<tr>
					<td colspan="7" align="center">		
						<div id="title">
							<h2>Short Messaging Service</h2>
						</div>
					</td>		
				</tr>
				<%
					StringBuffer error = (StringBuffer) request.getAttribute("error");
				%>
				<tr>
					<td colspan="7">
						<div id="errorpage">
							<%= (error !=null && error.length()>0)?"You must fullfilled the following error<br>"+error :"" %>
						</div>
					</td>
				</tr>
				<tr>
				<td colspan="4"><div id=validation></div></td></tr>
				<tr>
					<td>&nbsp;</td>
					<td>Driver id</td>
					<td>&nbsp;</td>
					<td>
					 
						<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername','dName')" name="driver_id" id="driver_id" value="<%=request.getParameter("driver_id")==null?"":request.getParameter("driver_id") %>">
						<input type="checkbox"  name="all" id="all" <%=(request.getParameter("all") != null && request.getParameter("all").equals("1"))?"checked":"" %> onclick="disabledriver()"> Select All
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>
					<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">		
							 	  <div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>Message Body</td>
					<td>&nbsp;</td>
					<td><textarea  name="msg" id="msg" cols="25" rows="10"></textarea></td>
				</tr>
				<tr>
					<TD colspan="4" align="center">
					  <input type="hidden" name="Button" id="Button" value="">
						<input type="Button" value="Submit"  onclick="formsubmit();">&nbsp;&nbsp;&nbsp;
					</TD>
				</tr>	
			</table>
		</td>
		</tr>
	</table>			
</form>	
</body>
</html>