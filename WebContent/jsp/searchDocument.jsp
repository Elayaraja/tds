<%@ page language="java" import="java.util.*" %>
<%@ page import="com.tds.bean.docLib.*" %>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%
	String ctxPath = request.getContextPath();
	
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<link type="text/javascript" href="js/Util.js"></link>
<script type="text/javascript" 	src="js/Util.js"></script>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link rel="stylesheet" type="text/css" href="<%=ctxPath %>/css/document.css" media="all">
<script type="text/javascript" src="<%=ctxPath %>/js/document/validation.js"></script>

<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<script type="text/javascript" src="js/jqModal.js"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

<script type="text/javascript" src="<%=ctxPath%>/js/document/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<%=ctxPath%>/js/document/jquery.tablesorter.pager.js"></script>


<title>TDS(Taxi Dispatching System)</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keywdocvieword3">
<meta http-equiv="description" content="This is my page">


<script type="text/javascript">

	function loadFile(docId){
		document.getElementById("docLoader").src='searchDocs?do=viewDoc&docId='+docId;
			$('#docview').jqm();
  			$('#docview').jqmShow();
	}


	function searchDoc(){
		var numb = document.getElementById("searchNumber").value;
		var cId = document.getElementById("searchCompId").value;
		var doType = document.getElementById("searchDocType").value;
		var drivertype = document.getElementById("driverOrCab").value;
		var userId = document.getElementById("userId").value;
		if(isEmpty(numb) && isEmpty(doType) && isEmpty(drivertype) && isEmpty(userId)){
			alert("Please provide search data");
			document.getElementById("searchNumber").focus();
			empty=1;
		}else{ 
			document.getElementById("docLoader").src='';
			document.getElementById("upload").action="<%=ctxPath %>/control?action=TDS_DOCUMENT_SEARCH&event=passKeyGen&module=operationView&do=searchFile";
			document.getElementById("upload").submit();
		}
	}
	function archiveFile(docId,i){
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='SystemSetupAjax?event=archiveDocument&module=operationView&docId='+docId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text==1){
		document.getElementById("archivetd_"+i).innerHTML='Archived';
	}
	
  	
	}
	
	function deleteFile(docId,i){
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='SystemSetupAjax?event=deleteDocument&module=operationView&docId='+docId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text==1){
		document.getElementById("deletetd_"+i).innerHTML='Deleted';
	}
	}
	
	function closeDoc() {
		$('#docview').jqmHide();
	}

	$('#documentList').on('click', 'tbody tr', function(event) {
	    $(this).addClass('highlight').siblings().removeClass('highlight');
	}); 
	
</script>



</head>
<body>

<form name="upload" id="upload" action="searchDocs" method="post">
<input type="hidden" name="searchCompId" id="searchCompId" value="<%=request.getAttribute("compId") %>">
<br>



<h2><center> SEARCH DOCUMENTS</center></h2>

<div>

<fieldset>
<table id="docSearch" align="left" width="70%" border="0" cellspacing="0" cellpadding="0" class="nav-static-header" >
	<tr>
		<td>Number</td>
		<td><input type="text" name="searchNumber" id="searchNumber" value=""></input></td>
		<td>Type</td>
		<td>
			<Select name="searchDocType" id="searchDocType" >
			<option value="" >-Choose-</option>
		<%
		Collection docTypeColl = (Collection)request.getAttribute("docTypeData");
  		for (Iterator iter = docTypeColl.iterator(); iter.hasNext();) {
  			NameValueTag element = (NameValueTag) iter.next();
  			out.println("<option value='"+element.getDocType()+"'>");
  			out.println(element.getDocType());
  			out.println("</option>");
  		}%>
		
		</Select></td>
		</tr>
		
		<tr>
		<td width="10%" >User</td>
		<td width="10%"><input type="text" name="userId" id="userId" value=""/></td>
		<td width="5%"> </td>	
		<td width="10%" >Driver/Cab</td>
		<td width="10%" >
		<select name="driverOrCab" id="driverOrCab">
		<!-- <option value="" >-Choose-</option> -->
		<option value="0">Driver</option>
		<option value="1">Cab</option>
		</select>
		</td>
		</tr><tr>
		<td colspan="8" align="center"> <input type="button" value="Search" onclick="javascript:searchDoc()" />
		</td>
		</tr> 
		</table>
</fieldset>

</div>
<br />
<div style="float: left; height:auto; width: 100%">

<!-- <div style="display: block;height: 190px;"> -->
	<table id="documentList" align="left" class="table-striped" >
	<input type="text" id="searchvalue" name="searchvalue" value="" placeholder="Search" onkeypress="customSearchOfaTable('documentList','searchvalue','row')" /> 
<thead> 
	<tr>
		<th>User Id</th>
		<th>Doc. Type</th>
		<th>Doc. Number</th>
		<th>Exp. Date</th>
		<th>Status</th>
		<th>View</th>
		<th>Archive</th>
		<th>Delete</th>
	</tr>
	
</thead>
<tbody align="center"> 	
	<%
		Collection collection = (Collection)request.getAttribute("docList");
  		int i=0;
  		for (Iterator iter = collection.iterator(); iter.hasNext();) {
  			i = i+1;
  			DocBean element = (DocBean) iter.next();
  			if(i%2==0){
  				out.println("<tr class=\"alternate\"><TD>");
  			}else{
  				out.println("<tr><TD>");
  			}
  			
  			if(element.getDocUploadedBy()!=null){
  				out.println(element.getDocUploadedBy());
  			}else{
  				out.println("N/A");
  			}
  			out.println("</td><td>");
  			if(element.getDocType()!=null){
  				out.println(element.getDocType());
  			}else{
  				out.println("N/A");
  			}
  			out.println("</td><td>");
  			/* out.println(element.getDocName());
  			out.println("</td><td>"); */
  			if(element.getDocNumber()!=null){
  				out.println(element.getDocNumber());
  			}else{
  				out.println("N/A");
  			}
  			out.println("</td><td>");

  			if(element.getDocExpiry()!=null){
  				out.println(element.getDocExpiry());
  			}else{
  				out.println("N/A");
  			}
  			
  			out.println("</td><td>");
  			out.println(element.getDocStatus());
  			out.println("</td><td>");
  			out.println("<a href=\"javascript:loadFile('"+element.getDocId()+"')\">");
  			%> 
  			<img alt="" src="jsp/images/view.jpg" height="20%" width="20%" >
  			<% 
  			out.println("</a>");
  			out.println("</td><td id=\"archivetd_"+i+"\">");
  			if(!element.getDocStatus().equals("ARCHIVED")){
  			out.println("<a href=\"javascript:archiveFile('"+element.getDocId()+"','"+i+"')\">");
  			%> <img alt="" src="jsp/images/archieve.png"  height="22%" width="22%"><% 
  		  	out.println("</a>");
  			}
  			out.println("</td><td id=\"deletetd_"+i+"\">");
  			out.println("<a href=\"javascript:deleteFile('"+element.getDocId()+"','"+i+"')\">");
  			%> <img alt="" src="jsp/images/deletebutton.jpg"  height="25%" width="25%"><% 
  		  	out.println("</a>");
  			out.println("</td></tr>");
  		}
  		
  		if(i==0) {
  			out.println("<tr><TD colspan=7 align=\"center\" >NO RECORD FOUND</TD></TR>");
  		}
	%>
</tbody>	
<c id="pager" class="pager" align="center" style="position:absolute; ">
		<img src="<%=ctxPath %>/images/first.png" class="first"/>
		<img src="<%=ctxPath %>/images/prev.png" class="prev"/>
		<input type="text" class="pagedisplay" disabled="disabled"/>
		<img src="<%=ctxPath %>/images/next.png" class="next"/>
		<img src="<%=ctxPath %>/images/last.png" class="last"/>
		<input type="hidden" value="15" class="pagesize" id="pagesize" />		
	</c>
	

</table>
</div>
<div style="float: right;height:60%; width: 30% ">   
<center> <b>Document View</b></center>
<!--  class="jqmWindow" id="docview" align="center" 
		<img alt="" src="jsp/images/closebutton.gif" align=right onclick="closeDoc()"> 
		 <div class="footerDV" align="right">Copyright &copy; 2010 Get A Cab</div>-->
		<iframe src="" id="docLoader" name="docLoader" width="auto" height="60%" scrolling="auto" frameborder="0" align="middle"  >	</iframe>		
		<br>
		
		</div>
	</form>

         

<script defer="defer">
	$(document).ready(function() 
    { 
        $("#documentList")
		.tablesorter({widthFixed: false, widgets: ['zebra']})
		.tablesorterPager({container: $("#pager")}); 
    } 
	); 
	
	
</script>
<footer>Copyright&nbsp;2010 Get A Cab</footer>
</body>
</html>

