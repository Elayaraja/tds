<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.tdsBO.LostandFoundBO,com.common.util.TDSConstants"%>
<jsp:useBean id="lostandfoundBo" class="com.tds.tdsBO.LostandFoundBO" scope="request"/>
<% if(request.getAttribute("lostandfound") != null)
	lostandfoundBo = (LostandFoundBO)request.getAttribute("lostandfound");

%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<form method="post" action="control" name="lostandfound">
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>">
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.createLostorFoundDetail %>">
<input type="hidden" name="lostandfoundNo" value="<%= lostandfoundBo.getLfKey()  %>">

<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
        	<div class="headerDV">
            	<div class="headerDVInner">
                	<h1 class="logoLft"><a href="#" title="Get-A-Cab"></a></h1>
                    <div class="headerRhtDV">
                    <div class="headerRhtDVInner">	
                        <div class="loginName">
                        <div class="rhtCol">
                    	<h1 class="loginNameDV"><span>Login Name:</span> abc xyz</h1>
                        </div>
                        <div class="lftCol"></div>
                        <div class="clrBth"></div>
                        </div>
                        <div>
                        <div class="rhtCol">
                    		<div class="searchDV">
                                <span>Search:</span>
                                <div class="inputBXLC"><div class="inputBXRC"><input name="" type="text" class="inputBX" /></div></div>
                                <input class="searchBtn" name="" type="button" />
                            </div>
                        </div>
                        <div class="lftCol">
                        	<ul class="topNav">
                            	<li class="last"><a href="#" title="Contact Us">Contact Us</a></li>
                                <li><a href="#" title="About Us">About Us</a></li>
                            </ul>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="clrBth"></div>
                    
                    <div class="mainNav">
                    	<div class="mainNavL">
                        	<div class="mainNavR">
                            	<ul>
                                	<li><a href="#" class="dispatch" title="Dispatch">Dispatch</a></li>
                                    <li><a href="#" class="operations" title="Operations">Operations</a></li>
                                    <li><a href="#" class="finance" title="Finance">Finance</a></li>
                                    <li><a href="#" class="reports" title="Reports">Reports</a></li>
                                    <li><a href="#" class="logout" title="Logout">Logout</a></li>
                                    <li><a href="#" class="systemSetup" title="System Setup">System Setup</a></li>
                                </ul>
                                
                            </div>
                    	</div>
                    </div>
                    <div class="clrBth"></div>
                </div>
            </div>
            
            <div class="contentDV">
            <div class="contentDVInWrap">
            <div class="contentDVIn">
            	<div class="leftCol">
                	<div class="innerPage">
                    </div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<div class="grayBX">
                    	<div class="topLCur"><div class="topRCur"><div class="topMCur"><div class="topMCur1"></div></div></div></div>
                        <div class="mid"><div class="mid1"><div class="mid2"><div class="mid3">
                        	<div class="formDV">
                        		<h2>Lost and Found Detail</h2>
                        		<%
									String error ="";
									if(request.getAttribute("errors") != null) {
									error = (String) request.getAttribute("errors");
									}		
							    %>
							    <%= error.length()>0?"You must fulfilled the following error<br>"+error :"" %>
                                	<div class="fieldDv">
                                    <fieldset>
                                     <label for="Trip Id">Trip Id</label>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap">
                                    <input type="text" name="tripid" class="inputBX"  value="<%= lostandfoundBo.getTripId() != null ? lostandfoundBo.getTripId() : "" %>">
                                    </div></div></div>
									</fieldset>
                                    
                                	<fieldset>
                                    <label for="Item Name">Item Name</label>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap">
                                    <input type="text"  name="itemName" class="inputBX"  value="<%= lostandfoundBo.getItemName() != null ? lostandfoundBo.getItemName() : "" %>">
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label for="Item Description">Item Description&nbsp;</label>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap ">
                                     <textarea name="itemDesc" class="inputBX"  rows="4" cols="25"><%= lostandfoundBo.getItemDesc() != null ? lostandfoundBo.getItemDesc() : ""   %></textarea>
                                    </div></div></div>
									</fieldset>
                                    
                                    <fieldset>
                                    <label for="Trip Date">Trip Date</label>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid85">
                                    <input type="text"  size="10" name="tripDate"  class="inputBX" value="<%= lostandfoundBo.getLfdate() != null ? lostandfoundBo.getLfdate() : ""  %>" onfocus="showCalendarControl(this);"  readonly="readonly" >
                                    </div></div></div>
									</fieldset>
									
									<fieldset>
                                    <label for="Time">Time</label>
                                    <div class="div2"><div class="div2In"><div class="inputBXWrap wid85">
                                    <input type="text"  name="time" class="inputBX" value="<%= lostandfoundBo.getLftime() != null ? lostandfoundBo.getLftime() : "" %>"> 
                                    </div></div></div>
									</fieldset>
									 <fieldset>
                                    <div class="div2">
                                    <div class="div2In">
                                    		  <div class="btnBlue padT10 padR5 fltLft">
                                                   <div class="rht">
                                                   <% if( lostandfoundBo.getLfKey() != null &&  !lostandfoundBo.getLfKey().equalsIgnoreCase("null")) { %>
                                                   <input type="submit" name="update" class="lft" value="Update">
                                                   <% } else { %>
												<input type="submit" name="save" class="lft" value="Save">
												<% } %>
                                                   </div>
                                              </div>			                                      
                                       </div>
                                    </div>
									</fieldset>
																		
                                     </div>
                           </div>			                                                                                
                                                                  
                       
                        	
                        </div></div></div></div>
                        <div class="btmLCur"><div class="btmRCur"><div class="btmMCur"><div class="btmMCur1"><div class="btmMCur2"><div class="btmMCur3"><div class="btmMCur4"><div class="btmMCur5"></div></div></div></div></div></div></div></div>
                    </div>
                    
                </div>    
              </div>
            	
                <div class="clrBth"></div>
            </div>
            </div>
            </div>
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
		</div>
	</div>
</div>
</body>
</html>

									
									
