<%@page import="com.common.util.TDSProperties"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.Iterator"%>
<%@page import= "java.util.Map"%>
<%@page import= "java.util.Set"%>
<%@page import="com.tds.cmp.bean.DriverVehicleBean"%>
<%@page import="com.charges.bean.ChargesBO"%> 
<%@page import="com.tds.tdsBO.FleetBO"%>
<jsp:useBean id="openBO" class="com.tds.tdsBO.OpenRequestBO" />
<%		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user"); %>
<html>
<head>
<%
ArrayList<DriverVehicleBean> dvList=new ArrayList<DriverVehicleBean>();
HashMap<String,DriverVehicleBean> companyFlags=(HashMap<String,DriverVehicleBean>) request.getAttribute("companyFlags");
Set hashMapEntries = companyFlags.entrySet();
Iterator it = hashMapEntries.iterator();
while(it.hasNext()){
	Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
	dvList.add(companyFlag.getValue());
}
ArrayList al_prevOpenRequests = (ArrayList)request.getAttribute("prevopenrequests");
ArrayList al_prevAddress = (ArrayList)request.getAttribute("prevaddresses");
ArrayList al_landMark=(ArrayList)request.getAttribute("landMark");
ArrayList<ChargesBO> chargeType =(ArrayList<ChargesBO>)request.getAttribute("chargeTypes");
ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); 
if(session.getAttribute("fleetList")!=null) {
fleetList=(ArrayList)session.getAttribute("fleetList");
} %>
<script type="text/javascript">

	document.onkeydown = function(e){
	    if (e == null) {
	        keycode = event.keyCode;
	      } else { 
	        keycode = e.which;
	      }
    	if(e.ctrlKey){
    		if(keycode == 69 || keycode==101 || keycode==70 ||keycode==102 ||keycode==71 ||keycode==103 ||keycode==72 ||keycode==104 ||keycode==74 ||keycode==106 ||keycode==75 ||keycode==107 ||keycode==76 ||keycode==108 ||keycode==79 ||keycode==111 || keycode == 80 || keycode==112 || keycode==116 ||keycode == 78 || keycode==110 ||keycode == 85 || keycode==117){
	    		//e.stopPropagation();
	    		return false;
    		}
    	}
    	if(e.altKey && keycode == 13 && document.getElementById('tableCount').value ==""){
    		driverAllocationCheck();
    	} else if(e.altKey && keycode == 88){
    		fnClose();
    	}else if(e.altKey && keycode == 80){
    		document.getElementById('phone').focus();
    	}else if(e.altKey && keycode == 89){
    		document.getElementById('address').focus();
    	}else if(e.altKey && keycode == 84){
    		document.getElementById('endAddress').focus();
    	}else if(e.altKey && keycode == 81){
    		clearPUAddressOR();
    	}else if(e.altKey && keycode == 87){
    		clearDOAddressOR();
    	}else if(e.altKey && keycode == 46){
			  document.getElementById("successPage").style.display="none";
              document.getElementById("errorPage").style.display="none";
              document.getElementById("successPage1").style.display="none";
              document.getElementById("errorPage1").style.display="none";
              document.getElementById("phoneError").style.display="none";	
			  clearOpenRequest();
    	}else if(e.altKey && keycode == 38){
    		openSplReq();
    	}else if(e.altKey && keycode == 40){
    		openDispatchComments();
			document.getElementById("specialIns1").focus();
    	}else if(e.altKey && keycode == 39){
    		openDriverComments();
			document.getElementById("Comments1").focus();
    	}else if(e.altKey && keycode == 68){
    		distance();
    	}else if((keycode == 27) && document.getElementById('tableCount').value =="1"){ 
		  	$("#detailsAutoPopulated1" ).dialog( "close" );
			$("#detailsAutoPopulated1").hide();
			  if( document.getElementById('acct').value=="undefined" ){
				  document.getElementById('acct').value="";
			  }
	  		if(document.getElementById("Comments").value!="" && document.getElementById("displayTableComments").style.display=="none" && document.getElementById('Comments').value!="null"){
	  			openDispatchComments();
	  		}
	  		if( document.getElementById('acct').value!="" && document.getElementById("HideText").style.display=="none" && document.getElementById('acct').value!="null"){
	  			openPayment();
  				document.getElementById('payType').value="VC";
	  		}
	  		document.getElementById('tableCount').value="";
	  		if(document.getElementById('acct').value!=""){
  				showComments(2);
  		 	}
	  		 document.getElementById("sdate").focus();
	  } else if((keycode == 27) && document.getElementById('tableCount').value =="2"){
		  	$("#MapDetails" ).dialog( "close" );
			$("#MapDetails").hide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("sadd1").focus();
	  } else if ((keycode == 27) && document.getElementById('tableCount').value =="3"){
		  $("#LandDetails" ).dialog( "close" );
		  $("#LandDetails").hide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById('LMKey').value="";
	  		document.getElementById("sadd1").focus();
	  } else if ((keycode == 27) && document.getElementById('tableCount').value =="4"){
		  $("#commentsByAcct" ).dialog( "close" );
		  $('#commentsByAcct').hide();
	  		document.getElementById('tableCount').value="";
	  }	else if ((keycode == 27) && document.getElementById('tableCount').value =="5"){
		  $("#completedJobs" ).dialog( "close" );
		  $("#completedJobs").hide();
	  		document.getElementById('tableCount').value="";
	  } else if ((keycode == 27) && document.getElementById('tableCount').value =="6"){
		  $("#phoneProvider" ).dialog( "close" );
		  $("#phoneProvider").hide();
	  		document.getElementById('tableCount').value="";
	  } else if ((keycode == 27) && document.getElementById('tableCount').value =="7"){
		  $("#tripsForFlags" ).dialog( "close" );
		  $("#tripsForFlags").hide();
             document.getElementById('tableCount').value="";
	  }else if((keycode==13) && document.getElementById('tableCount').value =="1") { 
		  	$("#detailsAutoPopulated1" ).dialog( "close" );
		  	$("#detailsAutoPopulated1").hide();
			  if( document.getElementById('acct').value=="undefined"){
				  document.getElementById('acct').value="";
			  }
				appendFromToEnterValues();
	  		if(document.getElementById('startTimeTemp').value!="" && document.getElementById('dateTemp').value!=""){
				$("#shrs option[value=NCH]").show();
				$("#shrsSmall option[value=NCH]").show();
				document.getElementById('shrs').value="NCH";
				document.getElementById('shrsSmall').value="NCH";
	  		    document.getElementById('sdate').value=document.getElementById('dateTemp').value;
	  			//document.getElementById('sdate').disabled=true;
	  		  	//document.getElementById('sdateSmall').disabled=true;
	  		} else {
	  			loadDate();
	  		  	document.getElementById('sdate').disabled=false;
	  		  	document.getElementById('sdateSmall').disabled=false;
	  		}
	  		if(document.getElementById("Comments").value!="" && document.getElementById("displayTableComments").style.display=="none" && document.getElementById('Comments').value!="null"){
	  			openDispatchComments();
	  		}
	  		if( document.getElementById('acct').value!="" && document.getElementById("HideText").style.display=="none" && document.getElementById('acct').value!="null"){
	  			openPayment();
  				document.getElementById('payType').value="VC";
	  		}
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("name").focus();
		} else if ((keycode==13) && document.getElementById('tableCount').value =="2"){
 		$("#MapDetails" ).dialog( "close" );
 		$("#MapDetails").hide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("sadd1").focus();
	  		selectEnterMapDetails();
	  		document.getElementById("name").focus();
	  } else if ((keycode==13) && document.getElementById('tableCount').value =="3"){
		  $("#LandDetails" ).dialog( "close" );
		  $("#LandDetails").hide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("sadd1").focus();
	  		selectEnterLandValues();
	  		document.getElementById("name").focus();
	  }	else if ((keycode == 13) && document.getElementById('tableCount').value =="4"){
		  $("#commentsByAcct" ).dialog( "close" );
		  $("#commentsByAcct").hide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("name").focus();
	  		document.getElementById("name").focus();
	  }else if ((keycode == 13) && document.getElementById('tableCount').value =="5"){
		  $("#completedJobs" ).dialog( "close" );
		  $("#completedJobs").hide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("name").focus();
	  } else if ((keycode == 13) && document.getElementById('tableCount').value =="6"){
		  $("#phoneProvider" ).dialog( "close" );
		  $("#phoneProvider").hide();
	  		document.getElementById('tableCount').value="";
	  		document.getElementById("name").focus();
	  } else if ((keycode == 13) && document.getElementById('tableCount').value =="7"){
		  $("#tripsForFlags" ).dialog( "close" );
		  $("#tripsForFlags").hide();
          document.getElementById('tableCount').value="";
	  		document.getElementById("name").focus();
	  }else if ((keycode == 13) && document.getElementById('tableCount').value =="8"){
		  $("#showShortcut" ).dialog( "close" );
		  $("#showShortcut").hide();
          document.getElementById('tableCount').value="";
	  		document.getElementById("name").focus();
	  }
	};
	function fnClose()
	{	
		if(document.getElementById("ccAuthCode").value!=""){
		var check=confirm("You have pending authorized CC. Want to void/return it?");
		if(check==true){
			 var xmlhttp = null;
			 if (window.XMLHttpRequest)
			 {
				 xmlhttp = new XMLHttpRequest();
			 } else {
				 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			 }
			 var url = 'OpenRequestAjax';
			 var category="";
			 if(document.getElementById("transType").value=="Auth"){
				 category="&category=Void";
			 } else {
				 category="&category=Return";
			 }
			 var parameters='event=enterCCAuth&transId='+document.getElementById("ccAuthCode").value+category;
			 xmlhttp.open("POST",url, false);
			 xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			 xmlhttp.setRequestHeader("Content-length", parameters.length);
			 xmlhttp.setRequestHeader("Connection", "close");
			 xmlhttp.send(parameters);
			 var text = xmlhttp.responseText;
			 var resp=text.split("###");
			 var result=resp[0].split(";");
			 if(result[0].indexOf('Failed')>=0){
				document.getElementById("errorPage").innerHTML="Failed to void transaction";
				document.getElementById("errorPage").style.display="block";
				document.getElementById("successPage").style.display="none";
				return false;
			 } else {
					window.location.href="/TDS/control";
			 }
		}else {
			window.location.href="/TDS/control";
		}
		} else {
			window.location.href="/TDS/control";
		}
	}
/* 	function showDate(){
		if(document.getElementById('roundTrip').checked==true){
			document.getElementById('sdateTrip').value=document.getElementById('sdate').value;
			$('#roundTripTable').show('slow');
		}else{
			$('#roundTripTable').hide('slow');
		}
	}
 */	function loadDate() {
		var curdate = new Date();
		var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
		var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
		var cYear = curdate.getFullYear();
		var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
		var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
		var hrsmin = hors+""+min;  

		document.masterForm.sdate.value = cMonth+"/"+cDate+"/"+cYear;
		document.masterForm.shrs.value  = 'Now'; 
		document.masterForm.sdateSmall.value = cMonth+"/"+cDate+"/"+cYear;
		document.masterForm.shrsSmall.value  = 'Now';
	}
	 
	function delrow() {
		try {
			var table = document.getElementById("chargesTable");
			var rowCount = table.rows.length;
			var size=document.getElementById('fieldSize').value;
			if(Number(size)>0){
				var temp = (Number(size) - 1);
				document.getElementById('fieldSize').value = temp;
				rowCount--;
				table.deleteRow(rowCount);
				callAmount();
			}
		} catch (e) {
			alert(e.message);
		}
	}
	function calCharges() {
		var i = Number(document.getElementById("fieldSize").value) + 1;
		document.getElementById('fieldSize').value = i;
		var table = document.getElementById("chargesTable");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);

		var element1 = document.createElement("select");
		element1.name = "payType" + i;
		element1.id = "payType" + i;
		var theOption = document.createElement("option");
		theOption.text = "Select";
		theOption.value = "-1";
		 element1.options.add(theOption);

		<%if(chargeType!=null && chargeType.size()>0){
			for(int i=0;i<chargeType.size();i++){%>
		var theOption = document.createElement("option");
		theOption.text = "<%=chargeType.get(i).getPayTypeDesc()%>";
		theOption.value = "<%=chargeType.get(i).getPayTypeKey()%>";
		 element1.options.add(theOption);
		 element1.onchange=function(){check(i);};
		 <%}}%>
	     cell1.appendChild(element1);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "amountDetail" + i;
		element2.id = "amountDetail" + i;
		element2.size = '7';
		element2.className='amountForCharge';
		element2.onchange=function(){callAmountDash();checkPercentagesDash(i);};
		cell2.appendChild(element2);
	} 

	function check(a){
		var keyToCheck = document.getElementById("payType"+a).value;
	   if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'RegistrationAjax?event=checkdynamiccharges&keyValue='+keyToCheck;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		 var jsonObj = "{\"address\":"+text+"}" ;
		 var obj = JSON.parse(jsonObj.toString());
		 for(var j=0;j<obj.address.length;j++){
			 var key=obj.address[j].K;
	         var amount=obj.address[j].A;
			 document.getElementById("payType"+a).value = key;
			 document.getElementById("amountDetail"+a).value=amount;
		 }
		 callAmountDash();checkPercentagesDash(a);
	}
	
	 function checkPercentages(rowValue){
			var keyToCheck = document.getElementById("payType"+rowValue).value;
			var newCall=0;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'RegistrationAjax?event=checkPayPercent&keyValue='+keyToCheck;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			 var jsonObj = "{\"address\":"+text+"}" ;
			 var obj = JSON.parse(jsonObj.toString());
			 var nextRow=rowValue;
			 for(var j=0;j<obj.address.length;j++){
				 nextRow = Number(nextRow)+1;		 
				 var key=obj.address[j].K;
				 var formula=obj.address[j].F;
				 var splitFirst=formula.split("pct");
				 for(var l=1;l<=document.getElementById("fieldSize").value;l++){
					 if(document.getElementById("payType"+l).value==key){
						 newCall=1;
						 nextRow=l;
						 break;
					 }
				 }
				 if(newCall==0){
				 	calCharges();
				 }
				 document.getElementById("payType"+nextRow).value = key;
				 document.getElementById("amountDetail"+nextRow).value = (Number(document.getElementById("amountDetail"+rowValue).value)*splitFirst[0])/100 ;
				 checkPercentages(nextRow);callAmount();
			 }
		 }
	  var jsonObjects=[];
	  function getMeterTypes(){
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			var url = 'SystemSetupAjax?event=getMeterTypes';
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if(text!=""){
				var jsonObj = "{\"meterDetails\":"+text+"}";
				var obj = JSON.parse(jsonObj.toString());
				jsonObjects=JSON.parse(jsonObj.toString());
				var table = document.getElementById("forMeter");
				var rowCount = table.rows.length;
				var row = table.insertRow(rowCount);
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);
		
				var element = document.createElement("label");
				element.name = "meterText";
				element.id = "meterText";
				element.appendChild(document.createTextNode("Meters:"));
				cell1.appendChild(element);
				
				var element1 = document.createElement("select");
				element1.name = "meter";
				element1.id = "meter";
				for(var j=0;j<obj.meterDetails.length;j++){
					var theOption = document.createElement("option");
					theOption.text = obj.meterDetails[j].MN;
					theOption.value =obj.meterDetails[j].MK;
					element1.options.add(theOption);
				}
				element1.onchange=function(){changeFares();};
				cell2.appendChild(element1);
			} else {
				var table = document.getElementById("forMeter");
				var rowCount = table.rows.length;
				var row = table.insertRow(rowCount);
				var cell1 = row.insertCell(0);
				var element = document.createElement("hidden");
				element.name = "meter";
				element.id = "meter";
				element.value="0";
				cell1.appendChild(element);
			}
	  }
	  
	  function changeFares(){
		var j=document.getElementById("meter").value;
		document.getElementById("startAmtVoucher").value=jsonObjects.meterDetails[j].SA;
		document.getElementById("tripAmount").value=jsonObjects.meterDetails[j].RMl;
	  }

	  function distance(){
		 	var origin= document.getElementById("sLatitude").value+","+document.getElementById("sLongitude").value;
			var destination= document.getElementById("eLatitude").value+","+document.getElementById("eLongitude").value;
			if(document.getElementById("sLatitude").value!="" && document.getElementById("eLatitude").value!=""){
				var amount=document.getElementById("tripAmount").value;
				var startAmt=document.getElementById("startAmtVoucher").value;
				var extraData="";
				var distanceBy=document.getElementById("distanceCalc").value;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				var url = 'OpenRequestAjax?event=getDistFMQA&from='+origin+'&to='+destination+'&routeType='+distanceBy;
				//var url = 'https://open.mapquestapi.com/directions/v1/route?key=ZX3FD32ZdJ8LfqzB2cKPMD6WDiuSbwTE&outFormat=json&routeType='+distanceBy+'&timeType=1&enhancedNarrative=false&locale=en_US&unit=m&from='+origin+'&to='+destination+'&drivingStyle=2&highwayEfficiency=21.0';
				
				xmlhttp.open("GET", url, false);
				xmlhttp.send(null);
				var text = xmlhttp.responseText;
				
				if(text=="" || text == "GRACIERROR"){
					$('#results').html( "Problem on getting distance" );
				}else if(text=="AUTH_FAILED"){
					$('#results').html( "Authentication failed for getting distance" );
				}else{
					var condition=text.split('"distance":');
					var finalDistance=condition[1].split(',');
				    var distance=Math.round((finalDistance[0])*100)/100;
				    if(document.getElementById("startAmtVoucher").value!=""){
						extraData=" (Start Amount $"+document.getElementById("startAmtVoucher").value+")";
				    } if(document.getElementById("rpmVoucher").value!=""){
				    	amount=Number(document.getElementById("rpmVoucher").value);
				    }
				  	var tripAmount=Math.round(((distance*amount)+Number(startAmt))*100)/100;
				    var result=distance+" Miles & Approximate Amount $"+tripAmount+" ($"+amount+"/Mile)"+extraData;
				    $('#results').html(result);
				    if(document.getElementById("fareByDistance").value=="1"){
					    document.getElementById('amt').value=tripAmount;
					    changeChargesOR();
				    }
				}
			} else {
			    $('#results').html("Both Pickup and Dropoff Address Are Mandatory");
			}
		}
	  
	  function closePage(){
			window.location.href="/TDS/control";
		}	  
</script>
<style type="text/css">

#page-header{
	background-color: aqua;
	}


.ui-helper-hidden-accessible {
display: none;
}
.ui-datepicker-calendar{
width: 274px;
height: 172px;
}
.ui-datepicker-header{
width: 274px;
height: 38px;
}
#LongDesc em{
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
	}
	
	#LongDesc1 em{
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
	}
	
	.hover em{
	background: url(images/yellow.png) no-repeat center;
	position: absolute;
	text-align: center;
	padding: 10px 12px 5px;
	color: black;
	font-style: normal;
	z-index: 3;
	display: none;
	}
	.pac-container{
	border: 1px solid #C77405;
	background-color: #FFFFFF;
	color: #C77405;
	font-weight: bold;
	outline: medium none;
	width: 500px !important;
	}	
	.ui-state-hover,.ui-widget-content .ui-state-hover,.ui-state-focus,.ui-widget-content .ui-state-focus
	{
	border: 1px solid #C77405;
	background-color: #FFFFFF;
	color: #C77405;
	font-weight: bold;
	outline: medium none;
	}	
	
      .gmap3{
        margin: 20px auto;
        border: 1px dashed #C0C0C0;
        width: 1024px;
      }
      
 #confirmBox
{
    display: none;
    background-color: #eee;
    border-radius: 5px;
    border: 1px solid #aaa;
    position: fixed;
    width: 600px;
    left: 50%;
    margin-left: -150px;
    margin-top: -500px;
    padding: 6px 8px 8px;
    box-sizing: border-box;
    text-align: center;
}
#confirmBox .button {
    background-color: #ccc;
    display: inline-block;
    border-radius: 3px;
    border: 1px solid #aaa;
    padding: 2px;
    text-align: center;
    width: 160px;
    cursor: pointer;
}
#confirmBox .button:hover
{
    background-color: #ddd;
}
#confirmBox .message
{
    text-align: left;
    margin-bottom: 8px;
}
/* #confirmBoxJobs
{
    display: none;
    background-color: #eee;
    border-radius: 5px;
    border: 1px solid #aaa;
    position: fixed;
    width: 600px;
    left: 50%;
    margin-left: -150px;
    margin-top: -500px;
    padding: 6px 8px 8px;
    box-sizing: border-box;
    text-align: center;
} */
.amountForCharge{
	text-align: right;
	font-weight: bold;
}

#confirmBoxJobs .button {
    background-color: #ccc;
    display: inline-block;
    border-radius: 3px;
    border: 1px solid #aaa;
    padding: 2px;
    text-align: center;
    width: 160px;
    cursor: pointer;
}
#confirmBoxJobs .button:hover
{
    background-color: #ddd;
}
#confirmBoxJobs .message
{
    text-align: left;
    margin-bottom: 8px;
}
	/* remove padding and scrolling from elements that contain an Accordion OR a content-div */
	.ui-layout-center ,	/* has content-div */
	.ui-layout-west ,	/* has Accordion */
	.ui-layout-east ,	/* has content-div ... */
	.ui-layout-east .ui-layout-content { /* content-div has Accordion */
		padding: 0;
		overflow: hidden;
	}
	.ui-layout-center P.ui-layout-content {
		line-height:	0.9em;
		margin:			0; /* remove top/bottom margins from <P> used as content-div */
	}
	h3, h4 { /* Headers & Footer in Center & East panes */
		font-size:		1.1em;
		background:		#EEF;
		border:			1px solid #BBB;
		border-width:	0 0 1px;
		padding:		7px 10px;
		margin:			0;
	}
	.ui-layout-east h4 { /* Footer in East-pane */
		font-size:		0.9em;
		font-weight:	normal;
		border-width:	1px 0 0;
	}

</style>
<% if(request.getAttribute("openrequest") != null) {
		openBO = (OpenRequestBO)request.getAttribute("openrequest");
	} 
%>
<link rel="stylesheet" type="text/css" href="jqueryUI/layout-default-latest.css" />
<link rel="stylesheet" type="text/css" href="jqueryUI/jquery.ui.base.css" />
<link rel="stylesheet" type="text/css" href="jqueryUI/jquery.ui.theme.css" />

<link type="text/css" href="css/main.css" rel="stylesheet" />
<link href="css/Popup.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></link>

<script type="text/javascript" src="jqueryUI/jquery-latest.js"></script>
<script type="text/javascript" src="jqueryUI/jquery-ui-latest.js"></script>
<script type="text/javascript" src="jqueryUI/jquery.layout-latest.js"></script>
<script type="text/javascript" src="jqueryUI/jquery.layout.resizePaneAccordions-latest.js"></script>
<script type="text/javascript" src="jqueryUI/themeswitchertool.js"></script> 
<script type="text/javascript" src="jqueryUI/debug.js"></script>

<script type="text/javascript" src="<%=TDSProperties.getValue("googleMapV3")%>"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geocode&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="js/gmap3.js"></script>
<script type="text/javascript" src="js/thickbox.js"></script>

<script type="text/javascript" src="Ajax/OpenRequest.js?v="<%=TDSConstants.versionNo %>></script>
<script type="text/javascript" src="js/main3.js?v="<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<link href='http://fonts.googleapis.com/css?family=Arbutus' rel='stylesheet' type='text/css'>

<script type="text/javascript" src="DashboardAjax/DispatchCommon.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="DashboardAjax/DispatchCommon1.js?vNo=<%=TDSConstants.versionNo %>"></script>
<script type="text/javascript" src="js/OpenRequestDetails.js?vNo=<%=TDSConstants.versionNo %>""></script>
<script type="text/javascript" src="Ajax/DashBoard.js?vNo=<%=TDSConstants.versionNo %>"></script>
<link rel="stylesheet" href="css/DashboardNew.css?vNo=<%=TDSConstants.versionNo %>">
<script type="text/javascript" src="Ajax/Common.js?vNo=<%=TDSConstants.versionNo %>"></script>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geocode&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>
<link href="css/Popup.css" rel="stylesheet" type="text/css" />


<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript">
$(document).ready ( function(){
	getMeterTypes();
	colorChange();
	 <%if(al_prevOpenRequests!=null){%>
	 	$( "#detailsAutoPopulated1" ).dialog({
	      modal: true,
	      width:630
	    });
		 document.getElementById('tableCount').value="1";
		 $( "#detailsAutoPopulated1" ).dialog({
		      modal: true,
	      	width:630
		 });
	 <%} else if(al_prevAddress!= null && al_prevAddress.size()>1){%>
		 $( "#detailsAutoPopulated1" ).dialog({
	      modal: true,
	      width:630
	    });
		 document.getElementById('tableCount').value="1";
	 <%} else if(al_landMark!= null && al_landMark.size()>1){%>
		 $( "#detailsAutoPopulated1" ).dialog({
	      modal: true,
	      width:630
	    });
		 document.getElementById('tableCount').value="1";
	 <%} else if(al_prevOpenRequests!=null &&  al_prevAddress.size()==1){%>
	 	 appendParentFrom(0,'Fcheck');
	 <%}%>
	 document.getElementById("payType").value="Cash";
	 <%if(adminBO.getCallerId()==1){%>
	 InitOR();
	 <%} else {%>
	 hideCallerId();
	 <%}%>
	 <%if(openBO.getTripid()!=null && openBO.getTripid()!=""){%>
	 	Driver();
	 <%} else {%>
	 	loadDate();
	 <%}%>
     $('input:text:first').focus();
     var $inp = $('input:text');
     $inp.bind('keydown', function(e) {
         //var key = (e.keyCode ? e.keyCode : e.charCode);
         var key = e.which;
         if (key == 13) {
             e.preventDefault();
             var nxtIdx = $inp.index(this) + 1;
             $(":input:text:eq(" + nxtIdx + ")").focus();
         }
     });
     $(".datePicker").datepicker();
	 $("#phone").focus();
	 $("#phone").keypress(function (e)  
	 { 
	  //if the letter is not digit then display error and don't type anything
	  if( e.which!=8 && e.which!=0 && (e.which<48 || e.which>57) && e.which!=13)
	  {
	  //display error message
	  $("#phoneError").html("Ph.No should not contain any spl.characters/spaces/alphabets").show().fadeIn("slow");  
	  return false;
	  } else {
			var phNo=document.getElementById("phone").value.length;
			if(phNo>9){
				  $("#phoneError").html("Ph.No should contain only 10 digits").show().fadeIn("slow");  
				  return false;
			}
	  }	
	  });
	 $("#specialIns").keypress(function (e)  
	 { 
	  //if the letter is not digit then display error and don't type anything
	  if(e.which!=8 )
	  {
		var spl1="";  
		var spl=document.getElementById("specialIns").value.length;
	    if(document.getElementById("specialIns1").value !="Temporary Comments"){
			spl1=document.getElementById("specialIns1").value.length;
		}
		if(spl+spl1 >249){
			alert("Sorry Comments Can't Exceed 250 Characters");
			return false;
		}
	  } if(e.which==59 || e.which==61){
		  alert("Semi colon and equal symbol are not allowed");
		  return false;
	  }	
	  });
	 $("#specialIns1").keypress(function (e)  
	 { 
	  //if the letter is not digit then display error and don't type anything
	  if(e.which!=8 )
	  {
		var spl="";  
		if(document.getElementById("specialIns").value !="Permanent Comments"){
			spl=document.getElementById("specialIns").value.length;
		}
		var spl1=document.getElementById("specialIns1").value.length;
		if(spl+spl1 >249){
			alert("Sorry Comments Can't Exceed 250 Characters");
			return false;
		}
	  }if(e.which==59 || e.which==61){
		  alert("Semi colon and equal symbol are not allowed");
		  return false;
	  }		
	 });
	 $("#Comments").keypress(function (e)  
	 { 
	  //if the letter is not digit then display error and don't type anything
	  if(e.which!=8)
	  {
		var spl1="";  
		var	spl=document.getElementById("Comments").value.length;
		if(document.getElementById("Comments1").value !="Temporary Comments"){
			spl1=document.getElementById("Comments1").value.length;
		}
		if(spl+spl1 >249){
			alert("Sorry Comments Can't Exceed 250 Characters");
			return false;
		}
	  }if(e.which==59 || e.which==61){
		  alert("Semi colon and equal symbol are not allowed");
		  return false;
	  }		
	  });
	 $("#Comments1").keypress(function (e)  
	 { 
	  //if the letter is not digit then display error and don't type anything
	  if(e.which!=8)
	  {
		var spl="";
		if(document.getElementById("Comments").value !="Permanent Comments"){
			spl=document.getElementById("Comments").value.length;
		}
		var spl1=document.getElementById("Comments1").value.length;
		if(spl+spl1 >249){
			alert("Sorry Comments Can't Exceed 250 Characters");
			return false;
		}
	  }if(e.which==59 || e.which==61){
		  alert("Semi colon and equal symbol are not allowed");
		  return false;
	  }		
	  });
	 $( "button,input[type=button]" ).button().click(function( event ) {
			event.preventDefault();
		});
	 myLayout = $('body').layout({
			west__size:			0
		,	east__size:			650
			// RESIZE Accordion widget when panes resize
		,	west__onresize:		$.layout.callbacks.resizePaneAccordions
		,	east__onresize:		$.layout.callbacks.resizePaneAccordions
		});

		// ACCORDION - in the West pane
		$("#accordion1").accordion({
			heightStyle:	"fill"
		});
		
		// ACCORDION - in the East pane - in a 'content-div'
		$("#accordion2").accordion({
			heightStyle:	"fill"
		,	active:			1
		});


		// THEME SWITCHER
		addThemeSwitcher('.ui-layout-north',{ top: '-3px', right: '5px' });
		// if a new theme is applied, it could change the height of some content,
		// so call resizeAll to 'correct' any header/footer heights affected
		// NOTE: this is only necessary because we are changing CSS *AFTER LOADING* using themeSwitcher
		setTimeout( myLayout.resizeAll, 1000 ); /* allow time for browser to re-render with new theme */
	 
		$("#shrs option[value=NCH]").hide();
		$("#shrsSmall option[value=NCH]").hide();
});

</script>
<title>TDS (Taxi Dispatch System)</title> 
</head>
<%
String status[] = {"Request","In Process","Completed"};
%>
<% String startupFunction = "";
if(al_prevOpenRequests!=null){
	//startupFunction = "previousAddress();";
} else if(al_prevAddress!= null && al_prevAddress.size()>1){
	//startupFunction = "previousAddress();";
} 
%>
<body> 
	<form name="masterForm" id="masterForm" action="control" method="post" onsubmit="return submitFunctionForForm()">
<%String driverOrCab;
if(adminBO.getDispatchBasedOnVehicleOrDriver()==2){
	driverOrCab = "Cab";
}else{
	driverOrCab= "Driver";%>
<%} %>
		<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
		<jsp:setProperty name="openBO" property="*" />
		<input type="hidden" name="phoneTemp" id="phoneTemp" value=""/>
		<input type="hidden" name="nameTemp" id="nameTemp" value=""/>
		<input type="hidden" name="nameIdTemp" id="nameIdTemp" value=""/>
		<input type="hidden" name="startTimeTemp" id="startTimeTemp" value=""/>
		<input type="hidden" name="dateTemp" id="dateTemp" value=""/>
		<input type="hidden" name="fadd1temp" id="fadd1temp" value=""/>
		<input type="hidden" name="fadd2temp" id="fadd2temp" value=""/>
		<input type="hidden" name="fcitytemp" id="fcitytemp" value=""/>
		<input type="hidden" name="fstatetemp" id="fstatetemp" value=""/>
		<input type="hidden" name="fziptemp" id="fziptemp" value=""/>
		<input type="hidden" name="landmarktemp" id="landmarkTemp" value=""/>
		<input type="hidden" name="latitemp" id="latitemp" value=""/>
		<input type="hidden" name="longitemp" id="longitemp" value=""/>
		<input type="hidden" name="eLatitemp" id="eLatitemp" value=""/>
		<input type="hidden" name="eLongitemp" id="eLongitemp" value=""/>
		<input type="hidden" name="tadd1temp" id="tadd1temp" value=""/>
		<input type="hidden" name="tadd2temp" id="tadd2temp" value=""/>
		<input type="hidden" name="tcitytemp" id="tcitytemp" value=""/>
		<input type="hidden" name="tstatetemp" id="tstatetemp" value=""/>
		<input type="hidden" name="tziptemp" id="tziptemp" value=""/>
		<input type="hidden" name="saveOpenReq" id="saveOpenReq" value=""/>
		<input type="hidden" name="defaultLati" id="defaultLati" value="<%=adminBO.getDefaultLati()%>"/>
		<input type="hidden" name="defaultLongi" id="defaultLongi" value="<%=adminBO.getDefaultLogi() %>"/>
		<input type="hidden" name="defaultState" id="defaultState" value="<%=openBO.getSstate()%>"/>
		<input type="hidden" name="defaultCountry" id="defaultCountry" value="<%=adminBO.getCountry()%>"/>
		<input type="hidden" name="fareByDistance" id="fareByDistance" value="<%=adminBO.getFareByDistance()%>"/>
 		<input type="hidden" name="tripId" id="tripId" value="<%=openBO.getTripid()%>"/>
 		<input type="hidden" name="userAddressTemp" id="userAddressTemp" value=""/>
		<input type="hidden" name="masterAddressTemp" id="masterAddressTemp" value=""/>
		<input type="hidden" name="secOR" id="secOR" value="6"></input>
		<input type="hidden" name="sLatitude" id="sLatitude" value="<%=openBO.getSlat()==null?"":openBO.getSlat()%>"/>
		<input type="hidden" name="sLongitude" id="sLongitude" value="<%=openBO.getSlong()==null?"":openBO.getSlong()%>"/>	
		<input type="hidden" name="edlatitude" id="eLatitude" value="<%=openBO.getEdlatitude()==null?"":openBO.getEdlatitude()%>"/>
		<input type="hidden" name="edlongitude" id="eLongitude" value="<%=openBO.getEdlongitude()==null?"":openBO.getEdlongitude()%>"/>	
		<input type="hidden" name="premiumNum" id="premiumNum" value=""/>
		<input type="hidden" name="phone0" id="phone0" value=""/>	
		<input type="hidden" name="phone1" id="phone1" value=""/>	
		<input type="hidden" name="phone2" id="phone2" value=""/>	
		<input type="hidden" name="phone3" id="phone3" value=""/>	
		<input type="hidden" name="phone4" id="phone4" value=""/>	
		<input type="hidden" name="phone5" id="phone5" value=""/>	
		<input type="hidden" name="phone6" id="phone6" value=""/>	
		<input type="hidden" name="phone7" id="phone7" value=""/>	
		<input type="hidden" name="phone8" id="phone8" value=""/>
		<input type="hidden" name="phone9" id="phone9" value=""/>
		<input type="hidden" name="name0" id="name0" value=""/>	
		<input type="hidden" name="name1" id="name1" value=""/>	
		<input type="hidden" name="name2" id="name2" value=""/>	
		<input type="hidden" name="name3" id="name3" value=""/>	
		<input type="hidden" name="name4" id="name4" value=""/>	
		<input type="hidden" name="name5" id="name5" value=""/>	
		<input type="hidden" name="name6" id="name6" value=""/>	
		<input type="hidden" name="name7" id="name7" value=""/>	
		<input type="hidden" name="name8" id="name8" value=""/>	
		<input type="hidden" name="name9" id="name9" value=""/>	
		<input type="hidden" name="LMKey" id="LMKey" value=""/>	
		<input type="hidden" name="keyPress" id="keyPress" value=""/>
		<input type="hidden" name="repeatGroup" id="repeatGroup" value="<%=openBO.getRepeatGroup()==null?"":openBO.getRepeatGroup()%>"/>
		<input type="hidden" name="multiJobUpdate" id="multiJobUpdate" value="<%=openBO.getRepeatGroup()==null?"":openBO.getRepeatGroup()%>"/>
		<input type="hidden" name="startAmtVoucher" id="startAmtVoucher" value="<%=adminBO.getStartAmount()%>"/>
		<input type="hidden" name="startAmtVoucher1" id="startAmtVoucher1" value="<%=adminBO.getStartAmount()%>"/>
		<input type="hidden" name="rpmVoucher" id="rpmVoucher" value=""/>
		<input type="hidden" name="nextScreen" id="nextScreen" value="<%=request.getParameter("nextScreen")==null?"":request.getParameter("nextScreen")%>"/>
		<input type="hidden" name="tripAmount" id="tripAmount" value="<%=adminBO.getRatePerMile()%>"/>
		<input type="hidden" id="driverORcab" value="<%=driverOrCab %>"/>
		<input type="hidden" id="phoneVerification" value=""/>
		<input type="hidden" id="CommentsTemporary" value=""/>
		<input type="hidden" id="switchFromToAddress" value=""/>
		<input type="hidden" id="popUpPickUp" value=""/>
		<input type="hidden" id="popUpDropOff" value=""/>
		<input type="hidden" id="tripStatus" value=""/>
		<input type="hidden" id="updateAll" value=""/>
		<input type="hidden" name="fieldSize" id="fieldSize" value=""/> 
		<input type="hidden" id="tripIdAuto" value=""/>
		<input type="hidden" id="ccAuthCode" value=""/>
		<input type="hidden" id="transType" value=""/>
		<input type="hidden" id="userAddress" name="userAddress" value="" />
		<input type="hidden" id="userAddressTo" name="userAddressTo" value="" />
		<input type="hidden" id="masterAddress" name="masterAddress" value="" />
		<input type="hidden" id="DriverCabSwitch" value=""/>
		<input type="hidden" id="addressCheck" value="<%=adminBO.getAddressFill()%>"/>
		<input type="hidden" id="initialFleet" value="<%=adminBO.getAssociateCode() %>"/> 
		<input type="hidden" id="ORFormat" value="10"/> 
		<input type="hidden" id="totalAddress" value=""/> 
		<input type="hidden" id="dontAddPickUp" value=""/> 
		<input type="hidden" id="dontAddDropOff" value=""/> 
		<input type="hidden" id="roundTripExist" value=""/> 
<input type="hidden" id="accountSuccess" value=""/>
		<%if((adminBO.getORTime()).equals("textBox")){ %>
			<input type="hidden" id="timeTypeOR" value="1"/>
		<%} else { %>
			<input type="hidden" id="timeTypeOR" value="2"/>
		<%} %>	
 		<%if(request.getParameter("nextScreen")!=null){ %>
 				<input type="hidden" name="nextScreen" id="response" value="<%=request.getParameter("nextScreen")%>"/>
 		<%} %>
 		
 		<input type="hidden" name="fleetSize1" id="fleetSize1" value="<%=fleetList.size()%>" />
		<%for(int i=0; i<fleetList.size();i++) {
			%><input type="hidden" name="fleetNumber_<%=i %>" id="fleetNumber_<%=i %>" value="<%=fleetList.get(i).getFleetNumber()%>" />
			<input type="hidden" name="fleetcolor_<%=i %>" id="fleetcolor_<%=i %>" value="<%=fleetList.get(i).getFcolor()%>" />
		<%}%>
 		
 		
 		<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.requestAction %>" /> 
 		<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveOpenRequest %>" /> 
		<input type="hidden" name="stlatitude" id="stlatitude" value="<%= openBO.getSlat() %>" />
		<input type="hidden" name="stlongitude" id="stlongitude" value="<%= openBO.getSlong() %>" /> 
		<input type="hidden" id="landmarkKey" name="landmarkKey" value="<%= openBO.getLandMarkKey() %>" /> 
		<input type="hidden" id="land_userZone" name="land_userZone" value="<%= openBO.getLandMarkZone() %>" /> 
		<input type="hidden" id="userFrom" name="userFrom" value="<%= openBO.getUserAddressKey() %>" /> 
		<input type="hidden" id="userTo" name="userTo" value="<%= openBO.getUserAddressKey() %>" />
		<input type="hidden" name="sesexit" id="sesexit" value="<%=adminBO==null?"0":"1"%>" /> 
		<input type="hidden" name="dispatch" value="1" /> 
		<input type="hidden" name="tableCount" id="tableCount" value=""/>
	

		<% 	HashMap hmp_address;
			if(request.getAttribute("address") != null) {
				hmp_address = (HashMap)request.getAttribute("address");
  		%>
		<input type="hidden" name="stadd1" value="<%= hmp_address.get("stadd1").toString() %>" /> 
		<input type="hidden" name="stadd2" value="<%= hmp_address.get("stadd2").toString() %>" /> 
		<input type="hidden" name="stcity" value="<%= hmp_address.get("stcity").toString() %>" /> 
		<input type="hidden" name="ststate" value="<%= hmp_address.get("ststate").toString() %>" /> 
		<input type="hidden" name="stzip" value="<%= hmp_address.get("stzip").toString() %>" /> 
		<input type="hidden" name="edadd1" value="<%= hmp_address.get("edadd1").toString() %>" /> 
		<input type="hidden" name="edadd2" value="<%= hmp_address.get("edadd2").toString() %>" /> 
		<input type="hidden" name="edcity" value="<%= hmp_address.get("edcity").toString() %>" /> 
		<input type="hidden" name="edstate" value="<%= hmp_address.get("edstate").toString() %>" /> 
		<input type="hidden" name="edzip" value="<%= hmp_address.get("edzip").toString() %>" />
		<%
	}
%>
<div class="ui-layout-north ui-widget-content" style="display: none;">
</div>
	<div class="ui-layout-center" style="display: none" id="advancedScreen">
		<div class="ui-layout-content ui-widget-content">
			<div id="successPage"  style="display: none;color:#52D017;size: 20px;font-weight: bold;">
			</div>
			<div id="errorPage"  style="display: none;color:#E41B17;size: 20px;font-weight: bold;">
			</div>
				<table border="1" style="width: 87%;">
					<tr id="page-header">
					     <td align="center"><img alt="" src="images/home.png" onclick="closePage()"></td>	
							<td colspan="4" align="center" >
							<font size="4"><b>Create A Job</b></font>
								<%if(fleetList!=null && fleetList.size()>0){%>
	                    			<select name="fleet" id="fleet" onclick="colorChange()" >
	                    				<%if(fleetList!=null){
											for(int i=0;i<fleetList.size();i++){ %>
												<option value="<%=fleetList.get(i).getFleetNumber()%>" <%=fleetList.get(i).getFleetNumber().equals(adminBO.getAssociateCode())?"selected=selected":""%>><%=fleetList.get(i).getFleetName() %></option>
											<% }
										}%> 
	                    			</select>
						        <%} else {%>
						           	<input type="hidden" name="fleet" id="fleet" value=""/>
						         <%} %>
						</td>
					</tr>
					<tr>
						<td style="width: 20%;">Phone No</td>
						<td style="width: 33%;"><input type="text"  name="phone" id="phone" tabindex="1" value="<%= openBO.getPhone()  %>" autocomplete="off"
								onblur="openDetails(phone,'details');" required="required" onkeypress="checkKeyPress()"/></td>
						<td style="width: 35%;"><font size="2">No Phone</font>
							<input  type="checkbox" name="noPhone" style="vertical-align: middle" id="noPhone" onclick='noShowPhone()'/>
							<font size="2">Don't Dispatch</font>
							<input type="checkbox" name="dispatchStatus" id="dispatchStatus" <%= ((openBO.getDispatchStatus()!=null) && (openBO.getDispatchStatus().equals("true")))?"checked":"" %> style="vertical-align: middle" />
							<font size="2">VIP</font>
							<input type="checkbox" name="premiumCustomer" id="premiumCustomer" <%= (openBO.getPremiumCustomer()==1)?"checked":"" %> style="vertical-align: middle" />
						</td>
						<td style="width: 33%;" align="center">
							<a id="displayJobOfDays" style="border-width: 20px; text-align: center;" href="javascript:displayJobOfDays();">Repeat Jobs</a></td>
					</tr>
					<tr style="height: 3px">
						<td style="width: 10%;">Name</td>
						<td style="width: 23%;">
							<input type="text" id="name" required="required" tabindex="1" name="name" value="<%= openBO.getName() %>"></input></td>
						<td style="width: 33%;">
							<font size="2">Shared Ride</font>
							<input type="checkbox" name="sharedRide" id="sharedRide" <%= ((openBO.getTypeOfRide()!=null) && (openBO.getTypeOfRide().equals("true")))?"checked":"" %> style="vertical-align: middle" onclick="openSplReq()"/>
							<font size="2">Round Trip</font>
							<input  type="checkbox" name="roundTrip" style="vertical-align: middle" id="roundTrip"/>
							<font size="2">Clone</font>
							<input  type="checkbox" name="cloneJob" style="vertical-align: middle" id="cloneJob"/>
						</td>
						<td style="width: 33%;" align="center">
							<a id="displayText"  href="javascript:openPayment();">Payment</a>
						</td>
					</tr>
					<tr style="height: 3px;">
						<td style="width: 10%;">Email</td>
						<td style="width: 23%;">
							<input type="text" id="eMail" name="eMail" value="<%= openBO.getEmail()==null?"":openBO.getEmail() %>" />
						</td>
						<td style="width: 33%;">
							<input type="button" value="Translate" id="translate" style="width: 40%;font-size: x-small;" onclick="showProviders()"/>
							<font size="2">#PAS</font><input type="text" size="2" name="numberOfPassenger" id="numberOfPassenger" value=""/>
						</td>
						<td style="width: 33%;" align="center">
							<font>#Cabs</font>
							<select name="numOfCabs" id="numOfCabs"  style="border: 1px solid #C0C0C0; font-size:14px; height:20px; text-align:left; width:45px; margin:0; padding:0" >
            	 				 <option value="1" selected="selected">1</option>
                                 <option value="2" <%=openBO.getNumOfCabs()==2?"selected":"" %>>2</option>
                                 <option value="3" <%=openBO.getNumOfCabs()==3?"selected":"" %>>3</option>
                                 <option value="4" <%=openBO.getNumOfCabs()==4?"selected":"" %>>4</option>
                                 <option value="5" <%=openBO.getNumOfCabs()==5?"selected":"" %>>5</option>
                                 <option value="6" <%=openBO.getNumOfCabs()==6?"selected":"" %>>6</option>
                                 <option value="7" <%=openBO.getNumOfCabs()==7?"selected":"" %>>7</option>
                                 <option value="8" <%=openBO.getNumOfCabs()==8?"selected":"" %>>8</option>
                                 <option value="9" <%=openBO.getNumOfCabs()==9?"selected":"" %>>9</option>
                                 <option value="10" <%=openBO.getNumOfCabs()==10?"selected":"" %>>10</option>
             				</select>
             			</td>
					</tr>
					<tr>
						<td style="width: 10%;">Date&Time</td>
						<td style="width: 23%;">
							<input type="text" class="datePicker" style="width: 90px; height: 15px;font-size: 14px;" name="sdate" id="sdate" readonly="readonly" value="<%= openBO.getSdate()!=null?openBO.getSdate():"" %>" onchange="changeTime();"/>
							<%if(adminBO.getORTime().equals("textBox")){ %>
								<input type="text" id="shrs" name="shrs" maxlength="4" style="width: 41px; height: 15px;font-size: 14px;" value="<%= openBO.getShrs()!=null?openBO.getShrs():""%>" /> 
							<%}else{ %>
								 <select name="shrs" id="shrs" style="border: 1px solid #C0C0C0; font-size:14px; height:25px; text-align:left; width:74px; margin:0; padding:0" >
								     <option value="Now" selected="selected" >Now</option>
								     <option value="NCH" >NoChange</option>
								     <option value="0000" <%=openBO.getShrs().equals("2400")?"selected":"" %>>12:00 A</option>
                                     <option value="0015" <%=openBO.getShrs().equals("2415")?"selected":"" %>>12:15 A</option>
                                     <option value="0030" <%=openBO.getShrs().equals("2430")?"selected":"" %>>12:30 A</option>
                                     <option value="0045" <%=openBO.getShrs().equals("2445")?"selected":"" %>>12:45 A</option>
                                     <option value="0100" <%=openBO.getShrs().equals("0100")?"selected":"" %>>01:00 A</option>
                                     <option value="0115" <%=openBO.getShrs().equals("0115")?"selected":"" %>>01:15 A</option>
                                     <option value="0130" <%=openBO.getShrs().equals("0130")?"selected":"" %>>01:30 A</option>
                                     <option value="0145" <%=openBO.getShrs().equals("0145")?"selected":"" %>>01:45 A</option>
                                     <option value="0200" <%=openBO.getShrs().equals("0200")?"selected":"" %>>02:00 A</option>
                                     <option value="0215" <%=openBO.getShrs().equals("0215")?"selected":"" %>>02:15 A</option>
                                     <option value="0230" <%=openBO.getShrs().equals("0230")?"selected":"" %>>02:30 A</option>
                                     <option value="0245" <%=openBO.getShrs().equals("0245")?"selected":"" %>>02:45 A</option>
                                     <option value="0300" <%=openBO.getShrs().equals("0300")?"selected":"" %>>03:00 A</option>
                                     <option value="0315" <%=openBO.getShrs().equals("0315")?"selected":"" %>>03:15 A</option>
                                     <option value="0330" <%=openBO.getShrs().equals("0330")?"selected":"" %>>03:30 A</option>
                                     <option value="0345" <%=openBO.getShrs().equals("0345")?"selected":"" %>>03:45 A</option>
                                     <option value="0400" <%=openBO.getShrs().equals("0400")?"selected":"" %>>04:00 A</option>
                                     <option value="0415" <%=openBO.getShrs().equals("0415")?"selected":"" %>>04:15 A</option>
                                     <option value="0430" <%=openBO.getShrs().equals("0430")?"selected":"" %>>04:30 A</option>
                                     <option value="0445" <%=openBO.getShrs().equals("0445")?"selected":"" %>>04:45 A</option>
                                     <option value="0500" <%=openBO.getShrs().equals("0500")?"selected":"" %>>05:00 A</option>
                                     <option value="0515" <%=openBO.getShrs().equals("0515")?"selected":"" %>>05:15 A</option>
                                     <option value="0530" <%=openBO.getShrs().equals("0530")?"selected":"" %>>05:30 A</option>
                                     <option value="0545" <%=openBO.getShrs().equals("0545")?"selected":"" %>>05:45 A</option>
                                     <option value="0600" <%=openBO.getShrs().equals("0600")?"selected":"" %>>06:00 A</option>
                                     <option value="0615" <%=openBO.getShrs().equals("0615")?"selected":"" %>>06:15 A</option>
                                     <option value="0630" <%=openBO.getShrs().equals("0630")?"selected":"" %>>06:30 A</option>
                                     <option value="0645" <%=openBO.getShrs().equals("0645")?"selected":"" %>>06:45 A</option>
                                     <option value="0700" <%=openBO.getShrs().equals("0700")?"selected":"" %>>07:00 A</option>
                                     <option value="0715" <%=openBO.getShrs().equals("0715")?"selected":"" %>>07:15 A</option>
                                     <option value="0730" <%=openBO.getShrs().equals("0730")?"selected":"" %>>07:30 A</option>
                                     <option value="0745" <%=openBO.getShrs().equals("0745")?"selected":"" %>>07:45 A</option>
                                     <option value="0800" <%=openBO.getShrs().equals("0800")?"selected":"" %>>08:00 A</option>
                                     <option value="0815" <%=openBO.getShrs().equals("0815")?"selected":"" %>>08:15 A</option>
                                     <option value="0830" <%=openBO.getShrs().equals("0830")?"selected":"" %>>08:30 A</option>
                                     <option value="0845" <%=openBO.getShrs().equals("0845")?"selected":"" %>>08:45 A</option>
                                     <option value="0900" <%=openBO.getShrs().equals("0900")?"selected":"" %>>09:00 A</option>
                                     <option value="0915" <%=openBO.getShrs().equals("0915")?"selected":"" %>>09:15 A</option>
                                     <option value="0930" <%=openBO.getShrs().equals("0930")?"selected":"" %>>09:30 A</option>
                                     <option value="0945" <%=openBO.getShrs().equals("0945")?"selected":"" %>>09:45 A</option>
                                     <option value="1000" <%=openBO.getShrs().equals("1000")?"selected":"" %>>10:00 A</option>
                                     <option value="1015" <%=openBO.getShrs().equals("1015")?"selected":"" %>>10:15 A</option>
                                     <option value="1030" <%=openBO.getShrs().equals("1030")?"selected":"" %>>10:30 A</option>
                                     <option value="1045" <%=openBO.getShrs().equals("1045")?"selected":"" %>>10:45 A</option>
                                     <option value="1100" <%=openBO.getShrs().equals("1100")?"selected":"" %>>11:00 A</option>
                                     <option value="1115" <%=openBO.getShrs().equals("1115")?"selected":"" %>>11:15 A</option>
                                     <option value="1130" <%=openBO.getShrs().equals("1130")?"selected":"" %>>11:30 A</option>
                                     <option value="1145" <%=openBO.getShrs().equals("1145")?"selected":"" %>>11:45 A</option>
                                     <option value="1200" <%=openBO.getShrs().equals("1200")?"selected":"" %>>12:00 P</option>
                                     <option value="1215" <%=openBO.getShrs().equals("1215")?"selected":"" %>>12:15 P</option>
                                     <option value="1230" <%=openBO.getShrs().equals("1230")?"selected":"" %>>12:30 P</option>
                                     <option value="1245" <%=openBO.getShrs().equals("1245")?"selected":"" %>>12:45 P</option>
                                     <option value="1300" <%=openBO.getShrs().equals("1300")?"selected":"" %>>01:00 P</option>
                                     <option value="1315" <%=openBO.getShrs().equals("1315")?"selected":"" %>>01:15 P</option>
                                     <option value="1330" <%=openBO.getShrs().equals("1330")?"selected":"" %>>01:30 P</option>
                                     <option value="1345" <%=openBO.getShrs().equals("1345")?"selected":"" %>>01:45 P</option>
                                     <option value="1400" <%=openBO.getShrs().equals("1400")?"selected":"" %>>02:00 P</option>
                                     <option value="1415" <%=openBO.getShrs().equals("1415")?"selected":"" %>>02:15 P</option>
                                     <option value="1430" <%=openBO.getShrs().equals("1430")?"selected":"" %>>02:30 P</option>
                                     <option value="1445" <%=openBO.getShrs().equals("1445")?"selected":"" %>>02:45 P</option>
                                     <option value="1500" <%=openBO.getShrs().equals("1500")?"selected":"" %>>03:00 P</option>
                                     <option value="1515" <%=openBO.getShrs().equals("1515")?"selected":"" %>>03:15 P</option>
                                     <option value="1530" <%=openBO.getShrs().equals("1530")?"selected":"" %>>03:30 P</option>
                                     <option value="1545" <%=openBO.getShrs().equals("1545")?"selected":"" %>>03:45 P</option>
                                     <option value="1600" <%=openBO.getShrs().equals("1600")?"selected":"" %>>04:00 P</option>
                                     <option value="1615" <%=openBO.getShrs().equals("1615")?"selected":"" %>>04:15 P</option>
                                     <option value="1630" <%=openBO.getShrs().equals("1630")?"selected":"" %>>04:30 P</option>
                                     <option value="1645" <%=openBO.getShrs().equals("1645")?"selected":"" %>>04:45 P</option>
                                     <option value="1700" <%=openBO.getShrs().equals("1700")?"selected":"" %>>05:00 P</option>
                                     <option value="1715" <%=openBO.getShrs().equals("1715")?"selected":"" %>>05:15 P</option>
                                     <option value="1730" <%=openBO.getShrs().equals("1730")?"selected":"" %>>05:30 P</option>
                                     <option value="1745" <%=openBO.getShrs().equals("1745")?"selected":"" %>>05:45 P</option>
                                     <option value="1800" <%=openBO.getShrs().equals("1800")?"selected":"" %>>06:00 P</option>
                                     <option value="1815" <%=openBO.getShrs().equals("1815")?"selected":"" %>>06:15 P</option>
                                     <option value="1830" <%=openBO.getShrs().equals("1830")?"selected":"" %>>06:30 P</option>
                                     <option value="1845" <%=openBO.getShrs().equals("1845")?"selected":"" %>>06:45 P</option>
                                     <option value="1900" <%=openBO.getShrs().equals("1900")?"selected":"" %>>07:00 P</option>
                                     <option value="1915" <%=openBO.getShrs().equals("1915")?"selected":"" %>>07:15 P</option>
                                     <option value="1930" <%=openBO.getShrs().equals("1930")?"selected":"" %>>07:30 P</option>
                                     <option value="1945" <%=openBO.getShrs().equals("1945")?"selected":"" %>>07:45 P</option>
                                     <option value="2000" <%=openBO.getShrs().equals("2000")?"selected":"" %>>08:00 P</option>
                                     <option value="2015" <%=openBO.getShrs().equals("2015")?"selected":"" %>>08:15 P</option>
                                     <option value="2030" <%=openBO.getShrs().equals("2030")?"selected":"" %>>08:30 P</option>
                                     <option value="2045" <%=openBO.getShrs().equals("2045")?"selected":"" %>>08:45 P</option>
                                     <option value="2100" <%=openBO.getShrs().equals("2100")?"selected":"" %>>09:00 P</option>
                                     <option value="2115" <%=openBO.getShrs().equals("2115")?"selected":"" %>>09:15 P</option>
                                     <option value="2130" <%=openBO.getShrs().equals("2130")?"selected":"" %>>09:30 P</option>
                                     <option value="2145" <%=openBO.getShrs().equals("2145")?"selected":"" %>>09:45 P</option>
                                     <option value="2200" <%=openBO.getShrs().equals("2200")?"selected":"" %>>10:00 P</option>
                                     <option value="2215" <%=openBO.getShrs().equals("2215")?"selected":"" %>>10:15 P</option>
                                     <option value="2230" <%=openBO.getShrs().equals("2230")?"selected":"" %>>10:30 P</option>
                                     <option value="2245" <%=openBO.getShrs().equals("2245")?"selected":"" %>>10:45 P</option>
                                     <option value="2300" <%=openBO.getShrs().equals("2300")?"selected":"" %>>11:00 P</option>
                                     <option value="2315" <%=openBO.getShrs().equals("2315")?"selected":"" %>>11:15 P</option>
                                     <option value="2330" <%=openBO.getShrs().equals("2330")?"selected":"" %>>11:30 P</option>
                                     <option value="2345" <%=openBO.getShrs().equals("2345")?"selected":"" %>>11:45 P</option>
	                             </select>
							<%} %></td>
						<td style="width: 33%;"><font size="2">Adv Time</font>
							 <select name="advanceTime" id="advanceTime" style="border: 1px solid #C0C0C0; font-size:14px; height:20px; text-align:left; width:50px; margin:0; padding:0" >
							     <option value="-1" selected="selected" >Dflt</option>
                               	 <option value="15">15</option>
                                 <option value="30">30</option>
                                 <option value="45">45</option>
                                 <option value="60">60</option>
                                 <option value="75">75</option>
                                 <option value="90">90</option>
                                 <option value="105">105</option>
                                 <option value="120">120</option>
                              </select>
							  <font	size="1">Min</font>
						</td>
						<td style="width: 33%;" align="center">
							<select name="toSms" id="toSms">
								<option value="No" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("No")?"selected":"" %>>No Sms</option>
								<option value="ATT" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("ATT")?"selected":"" %>>ATT</option>
								<option value="BoostMobile" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("BoostMobile")?"selected":"" %>>Boost Mobile</option>
								<option value="Cricket" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("Cricket")?"selected":"" %>>Cricket</option>
								<option value="MetroPCS" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("MetroPCS")?"selected":"" %>>MetroPCS</option>
								<option value="SimpleMobile" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("SimpleMobile")?"selected":"" %>>Simple Mobile</option>
								<option value="TMobile" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("TMobile")?"selected":"" %>>TMobile</option>
								<option value="Verizon" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("Verizon")?"selected":"" %>>Verizon</option>
								<option value="VirginMobile" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("VirginMobile")?"selected":"" %>>Virgin</option>
								<option value="Sprint" <%=openBO.getOR_SMS_SENT()==null?"":openBO.getOR_SMS_SENT().equals("Sprint")?"selected":"" %>>Sprint</option>
							</select>
						</td>
					</tr>
				</table>
				<table id="HideText" border="1" style="display: none;width: 87%;">
					<tr>
						<td align="center">PaymentType</td>
						<td><select name="paytype" id="payType" onchange="voucherChangeOR(1)">
								<option value="0">Select</option>
								<option value="Cash">Cash</option>
								<option value="CC">CC</option>
								<option value="VC">Voucher</option>

						</select></td>
						<td align="center">Account :</td>
						<td><input type="text" size="12" autocomplete="off" class="ui-autocomplete-input" name="acct" id="acct"  value="" onchange="voucherChangeOR(2)"/>
						<ajax:autocomplete 
  					fieldId="acct"
  					popupId="model-popup2"
  					targetId="acct"
  					baseUrl="autocomplete.view"
  					paramName="getAccountList"
  					className="autocomplete"
  					progressStyle="throbbing"/>
  					</td>
						<td align="center">Amount:</td>
						<td><input type="text" size="4" name="amt" id="amt"
							 value="" onchange="changeChargesOR()"/>
						</td>
					</tr>

				</table>
				<table id="hideJobsOnDays" border="1" style="display: none;width: 87%;">
					<tr>
						<td width="10">
							<font size="2";>From</font>
						</td>
						<td width="20">
							<input class="datePicker" readonly="readonly" type="text" style="width: 75px; height: 15px;font-size: 14px;" name="fromDate" id="fromDate" 
											value="" />
						</td>
						<td width="10">
							<font size="2">To</font>
						</td>
						<td width="20">
							<input class="datePicker" readonly="readonly" type="text" style="width: 75px; height: 15px;font-size: 14px;" name="toDate" id="toDate"
											value="" />
						</td>
						<td>Su<input type="checkbox" name="Sun" id="Sun" value="Sun"></input>
							Mo<input type="checkbox" name="Mon" id="Mon" value="Mon"></input>
							Tu<input type="checkbox" name="Tue" id="Tue" value="Tue"></input>
							We<input type="checkbox" name="Wed" id="Wed" value="Wed"></input>
							Th<input type="checkbox" name="Thu" id="Thu" value="Thu"></input>
							Fr<input type="checkbox" name="Fri" id="Fri" value="Fri"></input>
							Sa<input type="checkbox" name="Sat" id="Sat" value="Sat"></input>
						</td>
					</tr>
				</table>
				<table border="1">
					<tr>
						<td width="100%" colspan="4" style="background-color:lightgreen">
							<table width="100%">
								<tr>
									<td width="55%" style="background-color: lightgreen"><b><font size="2">P/U:</font><input class="ui-autocomplete-input" name="address" id="address" tabindex="3" placeholder="Type Address Here" type="text" size="29" onfocus="Data(1,1)" onblur="clearAddress()" onkeypress="checkKeyPress()"></input></b>
									<img alt="" src="images/address_book.gif" style="height: 20px;width:20px;" onclick="Data(1,2)"/>
									</td>
									<td width="30%" style="background-color: lightgreen"><b><font size="2">LM:</font><input  type="text" name="slandmark" size="16" id="slandmark" tabindex="4" autocomplete="off" value="<%=openBO.getSlandmark()==null?"":openBO.getSlandmark()%>"
											 onkeypress="checkKeyPress()"/></b>
			  						<ajax:autocomplete
  									fieldId="slandmark"
  									popupId="model-popup1"
  									targetId="slandmark"
  									baseUrl="autocomplete.view"
  									paramName="landmark"
  									className="autocomplete"
  									progressStyle="throbbing"/> 		
									</td>
									<td width="10%" style="background-color: lightgreen"><b><font size="2">New</font><input type="checkbox"
											name="newLandMark1" id="newLandMark1" ></input></b>
									</td>
								</tr>
							</table>
						</td>
							<td width="100%" colspan="4" style="background-color:#fa8072">
							<table  width="100%">
								<tr>
									<td width="55%" style="background-color:#fa8072"><b><font size="2">D/O:</font><input class="ui-autocomplete-input" placeholder="Type D/O Address Here" name="address" id="endAddress" tabindex="6" type="text" size="29" onfocus="Data(2,3)" onblur="clearAddress()" onkeypress="checkKeyPress()"></input></b>
									<img alt="" src="images/address_book.gif" style="height: 20px;width:20px;" onclick="Data(2,2)"/>
										</td>
									<td width="30%" style="background-color: #fa8072"><b><font size="2">LM:</font><input  type="text" name="elandmark" size="16" id="elandmark" tabindex="7" autocomplete="off" value="<%=openBO.getSlandmark()==null?"":openBO.getSlandmark()%>"
											 onkeypress="checkKeyPress()" /></b>
									<ajax:autocomplete
  									fieldId="elandmark"
  									popupId="model-popup1"
  									targetId="elandmark"
  									baseUrl="autocomplete.view"
  									paramName="landmark"
  									className="autocomplete"
  									progressStyle="throbbing"/> 
  									</td>
									<td width="10%" style="background-color: #fa8072"><b><font size="2">New</font><input type="checkbox"
											name="newLandMark2" id="newLandMark2" ></input></b>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td width="5%" style="background-color:lightgreen">Add1</td>
						<td width="20%" style="background-color:lightgreen">
							<input type="text" name="sadd1" size="25" id="sadd1" readonly="readonly" value="<%= openBO.getSadd1()  %>" autocomplete="off"
											class="form-autocomplete" onblur="Data(1)" onchange="change(1)" onkeypress="checkKeyPress()" />
						</td>
						<td width="5%" style="background-color:lightgreen">Add2</td>
						<td width="20%" style="background-color:lightgreen">
							<input type="text" name="sadd2" size="25" id="sadd2" tabindex="5" value="<%= openBO.getSadd2()  %>" autocomplete="off"
											class="form-autocomplete" placeholder="Apt Number etc."/>
						</td>
						<td width="5%" style="background-color: #fa8072">Add1</td>
						<td width="20%" style="background-color: #fa8072">
							<input type="text" name="eadd1" size="25" id="eadd1" readonly="readonly" value="<%= openBO.getEadd1()  %>" autocomplete="off"
											class="form-autocomplete" onblur="Data(2)" onchange="change(2)" onkeypress="checkKeyPress()"/>
						</td>
						<td width="5%" style="background-color: #fa8072">Add2</td>
						<td width="20%" style="background-color: #fa8072">
							<input type="text" name="eadd2" size="25" id="eadd2" tabindex="8" value="<%= openBO.getEadd2()  %>" autocomplete="off"
											class="form-autocomplete" placeholder="Apt Number etc."/>
						</td>
						
					</tr>
					<tr>
						<td width="5%" style="background-color:lightgreen">City</td>
						<td width="20%" style="background-color:lightgreen">
							<input type="text" name="scity" size="10" id="scity" readonly="readonly" class="form-autocomplete" value="<%= openBO.getScity()  %>"  onchange="change(1)"/>
						</td>
						<td width="5%" style="background-color:lightgreen">State</td>
						<td width="20%" style="background-color:lightgreen">
							<input type="text" name="sstate" size="3" id="sstate" value="<%=openBO.getSstate() %>"  readonly="readonly" onchange="change(1)"/> Zip<input type="text"
									name="szip" size="5" id="szip" value="<%= openBO.getSzip()%>" ></input>
							<input type="button" name="clearPU" size="10" id="clearPU" value="ClearP/U" onclick="clearPUAddressOR()"/>
						</td>
						<td width="5%" style="background-color: #fa8072">City</td>
						<td width="20%" style="background-color: #fa8072">
							<input type="text" name="ecity" id="ecity" size="10" readonly="readonly" value="<%= openBO.getEcity()  %>"  />
						</td>
						<td width="5%" style="background-color: #fa8072">State</td>
						<td width="20%" style="background-color: #fa8072">
							<input type="text" name="estate" size="3" readonly="readonly" id="estate" value="<%=openBO.getEstate()==""?openBO.getSstate():openBO.getEstate() %>" onblur="Data1()" /> 
								Zip
							<input type="text" name="ezip" size="5" id="ezip" value="<%= openBO.getEzip()  %>"/>
							<input type="button" name="clearDO" size="10" id="clearDO" value="ClearD/O" onclick="clearDOAddressOR()"/>
						</td>
					</tr>
				</table>
					<table id="hideFlightInformation" border="1" width="87%" style="display: none" bgcolor="#f8f8ff">
									<td colspan="4">Flight Information</td>
									<tr><td>AirlineName/Code</td><td><input type="text" name="airName" id="airName" value="<%= openBO.getAirName()==null?"":openBO.getAirName()%>" size="2" maxsize="2" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td><td>FlightNo<input type="text" name="airNo" id="airNo" value="<%= openBO.getAirNo()==null?"":openBO.getAirNo()%>" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td></tr><tr><td>From</td><td><input type="text" name="airFrom" id="airFrom" value="<%= openBO.getAirFrom()==null?"":openBO.getAirFrom()%>" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td><td>To<input type="text" name="airTo" id="airTo" value="<%= openBO.getAirTo()==null?"":openBO.getAirTo()%>" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td></tr>
							</table>
				<table>
					<tr>
						<td>
							<input type="button"  id="Clear" name="Clear" value="Clear" onclick="clearOpenRequest()"/>
							<input type="button"  id="getCompleted" name="getCompleted" value="Get Completed" onclick="getCompletedJobs()"/>
							Distance By:
							<select name="distanceCalc" id="distanceCalc">
								<option value="shortest">Shortest</option>
								<option value="fastest">Fastest</option>
							</select>	
							<input type="button" id="getDistance" value="Get Distance" onclick="distance();" />
							<input type="button" id="getRoute" value="Driver Route" onclick="showDriverRoute();" />
						</td>
					</tr>
					<tr>
						<td>
							<input type="button"  id="close" name="close" class="lft" value="Close" onclick="fnClose()"/>
							<input type="button"  id="delete" name="delete" class="lft" value="Cancel Job" onclick="deleteOpenRequest()"/>
						<% if(session.getAttribute("user") != null) {  %>
							Queue Name:
							<%ArrayList al_q = (ArrayList)request.getAttribute("al_q");%>
									<select name='queueno' id='queueno'>
										<option value=''>Select</option>
										<%
										if(al_q!=null){
											for(int i=0;i<al_q.size();i=i+2){ 
										%>
										<option value='<%=al_q.get(i).toString() %>'
											<%=openBO.getQueueno().equals(al_q.get(i).toString())?"selected":"" %>><%=al_q.get(i+1).toString() %></option>
											<%}
										}%>
									</select> 
						<% } else{  %>
							<input type="hidden" name="queueno" id="queueno" value="<%=openBO.getQueueno() %>" />
						<%} %> 
							<input type="button" id="preAuth" name="preAuth" class="lft" value="Pre Auth" onclick="enterCC(1)"/>
							<input type="button"  id="getTripId" name="getTripId" class="lft" value="Get Trip" onclick="getTripAuto()"/>
							<a id="Done" style="background-color: green;color: white;-moz-border-radius:10px 10px 10px 10px;text-decoration: none;font-size: large;font-weight:bold; font-style:oblique;font-family:fantasy; border-radius: 4px;box-shadow:3px 3px 1px grey;padding: 2px;" href="javascript:driverAllocationCheck();">Book Job</a>
						</td>
					</tr>
						<tr> <td><input type="button" style="color: black;font-size:small;font-weight: bold;background-color: #4EE2EC;" id="futureJobs" name="futeureJobs" onclick="ORJobHistory()" value="Job History" size="1"/>
					        <input type="button" style="color: black;font-size:small;font-weight: bold;background-color: #4EE2EC;" id="jobhistory" name="jobhistory" onclick="ORResJobs()" value="Job Reservation" size="1"/>	</td></tr>
					    
				</table>
				<table id="forMeter" style="width: 2%;"></table>	
				<table id="callerExtra" style="display: visible;">
					<tr>
						<td>Caller's Phone</td>
						<td>
							<input type="text" name="callerPhone" id="callerPhone" value="" size="11"></input></td>
						<td>Cust. Ref No</td>
						<td>
							#1<input type="text" name="refNum" id="refNum" value="<%= openBO.getRefNumber() %>"" size="5"></input>
							#2<input type="text" name="refNum1" id="refNum1" value="<%= openBO.getRefNumber1() %>"" size="5"></input>
							#3<input type="text" name="refNum2" id="refNum2" value="<%= openBO.getRefNumber2() %>"" size="5"></input>
							#4<input type="text" name="refNum3" id="refNum3" value="<%= openBO.getRefNumber3() %>"" size="5"></input>
						</td>
					</tr>
					<tr>
				<td>Caller's Name</td>
				<td><input type="text" name="callerName" id="callerName" value=""></input></td>
				</tr>
				</table>
						<div id="confirmBox"style="display: none;background-color: #eee;border-radius: 5px;
		    border: 1px solid #aaa;position: fixed;width: 600px;left: 50%;margin-left: -400px;
		    margin-top: -250px;padding: 6px 8px 8px;box-sizing: border-box;text-align: center;">
			    <div class="message"></div>
			    <span class="button yes" id="updateYes">Yes Clear</span>
			    <span class="button no" id="updateNo">No Keep Driver</span>
		</div>
		<div id="confirmBoxJobs" style="display: none;background-color: #eee;border-radius: 5px;
		    border: 1px solid #aaa;position: fixed;width: 600px;left: 50%;margin-left: -400px;
		    margin-top: -250px;padding: 6px 8px 8px;box-sizing: border-box;text-align: center;">
			    <div class="message"></div>
			    <span class="button yes" id="confirmYes">Yes Update/Delete All</span>
			    <span class="button no" id="confirmNo">No Only This</span>
		</div>
		</div>
	</div>
	<div id="map_canvas" style="display: none"></div>
		<div class="ui-layout-south ui-widget-content ui-state-error" style="display: none;height:10px">
		    	<table style="width: 100%">
					<tr>
						<td style="width: 15%;">
							<a id="allotDriver"  href="javascript:Driver();"><%=driverOrCab %>&Rating</a>
						</td>
						<td style="width: 15%;">
							<a id="displayTable" style="border-width: 20px; text-align: center;" href="javascript:openSplReq();">Special Request</a>
						</td>
						<td style="width: 15%;">
							<a id="displayTableDriverComments" style="border-width: 20px;  text-align: center;" href="javascript:openDriverComments();"><%=driverOrCab %> Comments</a>
						</td>
						<td style="width: 15%;">
							<a id="displayTableComments" style="border-width: 20px;  text-align: center;" href="javascript:openDispatchComments();" >Dispatch Comments</a>
						</td>
						    <td width="30%" align="center"><a id="displayFlightInformation" style="border-width: 20px; text-align: center;" href="javascript:openFlightInformation();">Flight Information</a></td>
				
						<td style="width: 40%;">
							<div id="distance" style="size: 20px;font-weight: bold;font-size: 16px">
				    			Distance : <span id="results"></span>
	    					</div>
	    				</td>
					</tr>
				</table>
				<table>
				<tr>
				<td style="width: 25%;white-space: inherit;">
				<table id="hideDriver" style="display: none;">
					<tr>
						<td width="20"><%=driverOrCab %></td>
						<td width="35">
							<input type="text" size="15" id="driver" name="driver" onblur="checkCabOrDriver()" value="<%= openBO.getDriverid() %>"/>
						</td>
						<td>Rating</td>
						<td style="width: 40px;">
							<select name="jobRating" id="jobRating">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</td>
					</tr>
				</table>
				</td>
				<td style="width: 25%;white-space: inherit;">
				<table id="hideTable"  style="display: none;">
					<%if(dvList!=null &&  dvList.size()>0) {%>
						<tr>
							<td>Special Request</td>
							<td>
								<table>
									<%int driverCounter=0;
									  int vehicleCounter=0;%>
									<%for(int i=0;i<dvList.size();i++){ %>
										<%if(dvList.get(i).getDriverVehicleSW()==1){ %>
											<td onmouseover="showLongDesc('<%=dvList.get(i).getLongDesc()%>')">
											<input type="checkbox" name="dchk<%=driverCounter%>" id="dchk<%=driverCounter%>" value="<%=dvList.get(i).getKey()==null?"":dvList.get(i).getKey()%>" onclick="checkFlagGroup('1','<%=driverCounter%>','<%=dvList.get(i).getGroupId()%>','<%=dvList.get(i).getLongDesc()%>')"/><%=dvList.get(i).getShortDesc() %>
											<input type="hidden" name="dgroup<%=driverCounter %>" id="dgroup<%=driverCounter%>" value="<%=dvList.get(i).getGroupId()%>" />
											<input type="hidden" name="dLD<%=driverCounter %>" id="dLD<%=driverCounter%>" value="<%=dvList.get(i).getLongDesc()%>" />
		 									</td>
		 									<%driverCounter++; %>
		 								<%}else if(dvList.get(i).getDriverVehicleSW()==2) { %>
											<td onmouseover="showLongDesc('<%=dvList.get(i).getLongDesc()%>')">
											<input type="checkbox" name="vchk<%=vehicleCounter %>" id="vchk<%=vehicleCounter%>" value="<%=dvList.get(i).getKey()==null?"":dvList.get(i).getKey() %>" onclick="checkFlagGroup('2','<%=vehicleCounter%>','<%=dvList.get(i).getGroupId()%>','<%=dvList.get(i).getLongDesc()%>')"/><%=dvList.get(i).getShortDesc()%>
											<input type="hidden" name="vgroup<%=vehicleCounter %>" id="vgroup<%=vehicleCounter%>" value="<%=dvList.get(i).getGroupId()%>" />
											<input type="hidden" name="vLD<%=vehicleCounter %>" id="vLD<%=vehicleCounter%>" value="<%=dvList.get(i).getLongDesc()%>" />
											</td>
											<%vehicleCounter++; %>
		 								<%} %>
									<%} %>
									<td>
										<input type="hidden" name="drprofileSize" id="drprofileSize"
													value="<%=driverCounter%>" /></td>			
									<td><input type="hidden" name="vprofileSize" id="vprofileSize"
													value="<%=vehicleCounter%>" /></td>	
	 								<td><input type="button" value="LookUp" onclick="getTripForSplRequest()"/></td>
								</table>
							</td>
						</tr>
					<%} else {%>
						<input type="hidden" name="drprofileSize" id="drprofileSize" value="" /></td>			
						<td><input type="hidden" name="vprofileSize" id="vprofileSize" value="" /></td>	
					<%} %>
				</table></td>
				<td style="width: 25%;white-space: inherit;">
				<table id="hideTableDriverComments"  style="display: none;">
					<tr>
						<td width="20"><%=driverOrCab %> Comments</td>
					</tr>
					<tr>
						<td>
							<textarea rows="2" cols="30" name="specialIns1" id="specialIns1" style="color: gray;"  onfocus="clearSplIns(2);this.style.color='#000000'" placeholder="Temporary Comments" ></textarea></td><td width="25" colspan="3"><textarea rows="2" cols="30" name="specialIns" id="specialIns" style="color: gray;" onfocus="clearSplIns(1);this.style.color='#000000'" placeholder="Permanent Comments"></textarea>
						</td>
					</tr>
				</table>
				</td>
				<td style="width: 25%;white-space: inherit;">
				<table id="hideTableComments" style="display: none;">
					<tr>
						<td width="20">Dispatch Comments</td>
					</tr>
					<tr>
						<td>
							<textarea rows="2" cols="30" name="Comments1" id="Comments1" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'" placeholder="Temporary Comments"></textarea></td><td width="25" colspan="3"><textarea rows="2" cols="30" name="Comments" id="Comments" style="color: gray;" onfocus="clearComments(1);this.style.color='#000000'" placeholder="Permanent Comments"></textarea>
						</td>
					</tr>
				</table>
				</td>
				<td style="width: 25%;white-space: inherit;">
				<table id="hideFlightInformation" border="1" width="100%" style="display: none" bgcolor="#f8f8ff">
									<td colspan="2">Flight Information</td>
									<tr><td>AirlineName:</td><td><input type="text" name="airName" id="airName" value="<%= openBO.getAirName()==null?"":openBO.getAirName()%>" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td><td>AirlineNo:<input type="text" name="airNo" id="airNo" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td></tr><tr><td>From:</td><td><input type="text" name="airFrom" id="airFrom" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td><td>To:<input type="text" name="airTo" id="airTo" style="color: gray;"  onfocus="clearComments(2);this.style.color='#000000'"></td></tr>
							</table></td>	
			</tr></table>
			<div id="phoneError" style="display: none;size: 20px;font-weigt: bold;font-size: 16px"></div>
			<table style="width: 100%;">
			<tr>
			<td>
			<table id="chargesTable" style="width: 60%;">
				<tr>
					<th style="width: 10%;background-color: #d3d3d3; ">Charge Type</th>
					<th style="width: 10%;background-color: #d3d3d3;">Amount</th>
				</tr>
			</table>
			<table id="chargesButton" style="width: 60%;" id="bodypage" class="driverChargTab">
				<tr>
					<td>
		  				<div class="btnBlue padT10 padR5 fltLft ">
           					<input type="button" name="add" value="Add Charges" onclick="calCharges()" class="lft"/>
            			</div>
            		</td>
            		<td>
            			<div class="btnBlue padT10 padR5 fltLft ">
                 			<input type="button" name="remove" value="Remove"  onclick="delrow()" class="lft" />
                    	</div>
                 	</td>
                	<td>
            			<div class="btnBlue padT10 padR5 fltLft ">
                 			<input type="button" name="clearAll" value="Clear All"  onclick="clearCharges()" class="lft" />
                     	</div>
                	</td>
					<td>
						<div id="dialog_box" style="display: none;">
   							Really delete?
						</div>
					</td>		
				</tr>
			</table>
			</td>
			<td>
			<div id="callerId" style="margin-left: 650px">
				<table>
					<tr>
						<td><div id='tidOR' style="margin-left: -600px;"></div></td>
						<td><div id=button_0 style="display: none;"><div id="CallAcceptedBy_0" class="hover"><em><input id=acceptor0 value=""></input></em><input type="button" value="" id="callerIdButton0" name="callerIdButton0" onclick="callerIdPopulate(0)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -480px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,0)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_0 style="display: none;"><div id="CallsAcceptedBy_0" class="hover"><em><input id=acceptors0 value=""></input></em><input type="button" value="" id="callerIdButtons0" name="callerIdButtons1" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;margin-left: -480px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,0)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_1 style="display: none;"><div id="CallAcceptedBy_1" class="hover"><em><input id=acceptor1 value=""></input></em><input type="button" value="" id="callerIdButton1" name="callerIdButton2" onclick="callerIdPopulate(1)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -440px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,1)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_1 style="display: none;"><div id="CallsAcceptedBy_1" class="hover"><em><input id=acceptors1 value=""></input></em><input type="button" value="" id="callerIdButtons1" name="callerIdButtons2" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;margin-left: -440px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,1)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_2 style="display: none;"><div id="CallAcceptedBy_2" class="hover"><em><input id=acceptor2 value=""></input></em><input type="button" value="" id="callerIdButton2" name="callerIdButton3" onclick="callerIdPopulate(2)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -400px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,2)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_2 style="display: none;"><div id="CallsAcceptedBy_2" class="hover"><em><input id=acceptors2 value=""></input></em><input type="button" value="" id="callerIdButtons2" name="callerIdButtons3" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;margin-left: -400px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,2)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_3 style="display: none;"><div id="CallAcceptedBy_3" class="hover"><em><input id=acceptor3 value=""></input></em><input type="button" value="" id="callerIdButton3" name="callerIdButton4" onclick="callerIdPopulate(3)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -360px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,3)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_3 style="display: none;"><div id="CallsAcceptedBy_3" class="hover"><em><input id=acceptors3 value=""></input></em><input type="button" value="" id="callerIdButtons3" name="callerIdButtons4" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;;margin-left: -360px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,3)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_4 style="display: none;"><div id="CallAcceptedBy_4" class="hover"><em><input id=acceptor4 value=""></input></em><input type="button" value="" id="callerIdButton4" name="callerIdButton5" onclick="callerIdPopulate(4)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -320px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,4)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_4 style="display: none;"><div id="CallsAcceptedBy_4" class="hover"><em><input id=acceptors4 value=""></input></em><input type="button" value="" id="callerIdButtons4" name="callerIdButtons5" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;;margin-left: -320px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,4)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_5 style="display: none;"><div id="CallAcceptedBy_5" class="hover"><em><input id=acceptor5 value=""></input></em><input type="button" value="" id="callerIdButton5" name="callerIdButton6" onclick="callerIdPopulate(5)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -280px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,5)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_5 style="display: none;"><div id="CallsAcceptedBy_5" class="hover"><em><input id=acceptors5 value=""></input></em><input type="button" value="" id="callerIdButtons5" name="callerIdButtons6" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;margin-left: -280px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,5)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_6 style="display: none;"><div id="CallAcceptedBy_6" class="hover"><em><input id=acceptor6 value=""></input></em><input type="button" value="" id="callerIdButton6" name="callerIdButton7" onclick="callerIdPopulate(6)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -240px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,6)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_6 style="display: none;"><div id="CallsAcceptedBy_6" class="hover"><em><input id=acceptors6 value=""></input></em><input type="button" value="" id="callerIdButtons6" name="callerIdButtons7" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;margin-left: -240px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,6)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_7 style="display: none;"><div id="CallAcceptedBy_7" class="hover"><em><input id=acceptor7 value=""></input></em><input type="button" value="" id="callerIdButton7" name="callerIdButton8" onclick="callerIdPopulate(7)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left:  -200px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,7)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_7 style="display: none;"><div id="CallsAcceptedBy_7" class="hover"><em><input id=acceptors7 value=""></input></em><input type="button" value="" id="callerIdButtons7" name="callerIdButtons8" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;margin-left: -200px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,7)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_8 style="display: none;"><div id="CallAcceptedBy_8" class="hover"><em><input id=acceptor8 value=""></input></em><input type="button" value="" id="callerIdButton8" name="callerIdButton9" onclick="callerIdPopulate(8)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -160px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,8)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_8 style="display: none;"><div id="CallsAcceptedBy_8" class="hover"><em><input id=acceptors8 value=""></input></em><input type="button" value="" id="callerIdButtons8" name="callerIdButtons9" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;margin-left: -160px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,8)" onmouseout="hideHover()"></input></div></div></td>
						<td><div id=button_9 style="display: none;"><div id="CallAcceptedBy_9" class="hover"><em><input id=acceptor9 value=""></input></em><input type="button" value="" id="callerIdButton9" name="callerIdButton10" onclick="callerIdPopulate(9)" style="-moz-border-radius:8px 8px 8px 8px;background-color: #FDD017;color: #342826;width: 40px;height: 40px;margin-left: -120px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(0,9)" onmouseout="hideHover()"></input></div></div>
						<div id=buttons_9 style="display: none;"><div id="CallsAcceptedBy_9" class="hover"><em><input id=acceptors9 value=""></input></em><input type="button" value="" id="callerIdButtons9" name="callerIdButtons10" style="-moz-border-radius:8px 8px 8px 8px;background-color: #008000;color: #FFFFFF;width: 40px;height: 40px;margin-left: -120px;font-size: large;font-weight: bolder;font-family: 'Arbutus', cursive;" onmouseover="callAccepted(1,9)" onmouseout="hideHover()"></input></div></div></td>
					</tr>
				</table>
			</div>
			</td>
			</tr>
			</table>
			<div id="showHover" style="display: none">
				<table>
					<tr>
						<td colspan='5' align='center' style="background-color:#CEECF5;size: 300px">
							<input style="background-color: #CEECF5;color: black;font-style: italic;font-size:medium;" id="hoverValue" value="" size="65"/>
						</td>
					</tr>
				</table>
			</div>
			<table id="shortcutsForOR">
			<tr><td>
            <input type="button" name="shortcuts" value="Shortcuts"  onclick="shortcutNew()" class="lft" />
			<font size="3">Alt+</font>(<font size="3">Delete-</font><font size="2">ClearAll;</font> <font size="3">Q-</font><font size="2">Clear P/U;</font> <font size="3">W-</font><font size="2">Clear D/O;</font> <font size="3">P-</font><font size="2">Focus Phone;</font> <font size="3">Y-</font><font size="2">Focus P/U;</font> <font size="3">T-</font><font size="2">Focus D/O;</font> <font size="3">X-</font><font size="2">Close;</font> <font size="3">Up Arrow-</font><font size="2">Spl Req;</font> <font size="3">Down Arrow-</font><font size="2">Dispatch Comments;</font> <font size="3">Right Arrow-</font><font size="2">Driver Comments</font>)
			</td></tr></table>
		</div>
		<div id="reducedScreen" class="rightCal" style="width: 45%; color: black;display: none;">
		<div id="successPage1"  style="display: none;color:#52D017;size: 20px;font-weight: bold;">
		</div>
		<div id="errorPage1"  style="display: none;color:#E41B17;size: 20px;font-weight: bold;">
		</div>
		<table width="100%" border="1" cellspacing="0" cellpadding="0"
			class="driverChargTab" bgcolor="#f8f8ff">
		
				<tr>
					<td>
						<table id="bodypage" border="1" width="100%"
							style="background-color: #81F7F3;">
		<table id="bodypage" border="1" width="100%" bgcolor="#f8f8ff">
			<tr>
				<td colspan="3" align="center"
					style="background-color: #d3d3d3;"><font size="4"><b>Create A Job</b>
									</font>
									</td>
		</tr></table></table></td></tr>
		<tr><td>
		<table id="bodypage" border="1" width="100%" class="driverChargTab" bgcolor="#f8f8ff">
			<tr>
			<td width="25%" class="firstCol">Phone No</td>
			<td width="25%"><input type="text"  name="phoneSmall" id="phoneSmall" tabindex="1"
										value="<%= openBO.getPhone()%>" autocomplete="off" 
		onblur="previousAddress();openDetails(phone,'details');"/>
		</td>
						<td width="25%" class="firstCol">Name</td>
			<td width="25%"><input type="text"  name="nameSmall" id="nameSmall" tabindex="1"
										value="<%= openBO.getName()  %>"/>
		</td>
		</tr>
											<tr>
									<td width="25%" class="firstCol">
										Date&Time</td>
									<td width="25%"><input type="text"
										style="width: 90px; height: 15px;font-size: 14px;" name="sdateSmall" id="sdateSmall" readonly="readonly"
		value="<%= openBO.getSdate()!=null?openBO.getSdate():"" %>" onclick="displayCalendar(document.masterForm.sdateSmall,'mm/dd/yyyy',this);" />
		<%if(adminBO.getORTime().equals("textBox")){ %>
		<input type="text" id="shrsSmall" name="shrsSmall" maxlength="4" style="width: 41px; height: 15px;font-size: 14px;"
		value="<%= openBO.getShrs()!=null?openBO.getShrs():""%>" /> 
		<%}else{ %>
		<select name="shrsSmall" id="shrsSmall" style="border: 1px solid #C0C0C0; font-size:14px; height:25px; text-align:left; width:74px; margin:0; padding:0" >
		   <option value="Now" selected >Now</option>
		   <option value="NCH" >NoChange</option>
		   <option value="0000" <%=openBO.getShrs().equals("2400")?"selected":"" %>>12:00 A</option>
		                                    <option value="0015" <%=openBO.getShrs().equals("2415")?"selected":"" %>>12:15 A</option>
		                                    <option value="0030" <%=openBO.getShrs().equals("2430")?"selected":"" %>>12:30 A</option>
		                                    <option value="0045" <%=openBO.getShrs().equals("2445")?"selected":"" %>>12:45 A</option>
		                                    <option value="0100" <%=openBO.getShrs().equals("0100")?"selected":"" %>>01:00 A</option>
		                                    <option value="0115" <%=openBO.getShrs().equals("0115")?"selected":"" %>>01:15 A</option>
		                                    <option value="0130" <%=openBO.getShrs().equals("0130")?"selected":"" %>>01:30 A</option>
		                                    <option value="0145" <%=openBO.getShrs().equals("0145")?"selected":"" %>>01:45 A</option>
		                                    <option value="0200" <%=openBO.getShrs().equals("0200")?"selected":"" %>>02:00 A</option>
		                                    <option value="0215" <%=openBO.getShrs().equals("0215")?"selected":"" %>>02:15 A</option>
		                                    <option value="0230" <%=openBO.getShrs().equals("0230")?"selected":"" %>>02:30 A</option>
		                                    <option value="0245" <%=openBO.getShrs().equals("0245")?"selected":"" %>>02:45 A</option>
		                                    <option value="0300" <%=openBO.getShrs().equals("0300")?"selected":"" %>>03:00 A</option>
		                                    <option value="0315" <%=openBO.getShrs().equals("0315")?"selected":"" %>>03:15 A</option>
		                                    <option value="0330" <%=openBO.getShrs().equals("0330")?"selected":"" %>>03:30 A</option>
		                                    <option value="0345" <%=openBO.getShrs().equals("0345")?"selected":"" %>>03:45 A</option>
		                                    <option value="0400" <%=openBO.getShrs().equals("0400")?"selected":"" %>>04:00 A</option>
		                                    <option value="0415" <%=openBO.getShrs().equals("0415")?"selected":"" %>>04:15 A</option>
		                                    <option value="0430" <%=openBO.getShrs().equals("0430")?"selected":"" %>>04:30 A</option>
		                                    <option value="0445" <%=openBO.getShrs().equals("0445")?"selected":"" %>>04:45 A</option>
		                                    <option value="0500" <%=openBO.getShrs().equals("0500")?"selected":"" %>>05:00 A</option>
		                                    <option value="0515" <%=openBO.getShrs().equals("0515")?"selected":"" %>>05:15 A</option>
		                                    <option value="0530" <%=openBO.getShrs().equals("0530")?"selected":"" %>>05:30 A</option>
		                                    <option value="0545" <%=openBO.getShrs().equals("0545")?"selected":"" %>>05:45 A</option>
		                                    <option value="0600" <%=openBO.getShrs().equals("0600")?"selected":"" %>>06:00 A</option>
		                                    <option value="0615" <%=openBO.getShrs().equals("0615")?"selected":"" %>>06:15 A</option>
		                                    <option value="0630" <%=openBO.getShrs().equals("0630")?"selected":"" %>>06:30 A</option>
		                                    <option value="0645" <%=openBO.getShrs().equals("0645")?"selected":"" %>>06:45 A</option>
		                                    <option value="0700" <%=openBO.getShrs().equals("0700")?"selected":"" %>>07:00 A</option>
		                                    <option value="0715" <%=openBO.getShrs().equals("0715")?"selected":"" %>>07:15 A</option>
		                                    <option value="0730" <%=openBO.getShrs().equals("0730")?"selected":"" %>>07:30 A</option>
		                                    <option value="0745" <%=openBO.getShrs().equals("0745")?"selected":"" %>>07:45 A</option>
		                                    <option value="0800" <%=openBO.getShrs().equals("0800")?"selected":"" %>>08:00 A</option>
		                                    <option value="0815" <%=openBO.getShrs().equals("0815")?"selected":"" %>>08:15 A</option>
		                                    <option value="0830" <%=openBO.getShrs().equals("0830")?"selected":"" %>>08:30 A</option>
		                                    <option value="0845" <%=openBO.getShrs().equals("0845")?"selected":"" %>>08:45 A</option>
		                                    <option value="0900" <%=openBO.getShrs().equals("0900")?"selected":"" %>>09:00 A</option>
		                                    <option value="0915" <%=openBO.getShrs().equals("0915")?"selected":"" %>>09:15 A</option>
		                                    <option value="0930" <%=openBO.getShrs().equals("0930")?"selected":"" %>>09:30 A</option>
		                                    <option value="0945" <%=openBO.getShrs().equals("0945")?"selected":"" %>>09:45 A</option>
		                                    <option value="1000" <%=openBO.getShrs().equals("1000")?"selected":"" %>>10:00 A</option>
		                                    <option value="1015" <%=openBO.getShrs().equals("1015")?"selected":"" %>>10:15 A</option>
		                                    <option value="1030" <%=openBO.getShrs().equals("1030")?"selected":"" %>>10:30 A</option>
		                                    <option value="1045" <%=openBO.getShrs().equals("1045")?"selected":"" %>>10:45 A</option>
		                                    <option value="1100" <%=openBO.getShrs().equals("1100")?"selected":"" %>>11:00 A</option>
		                                    <option value="1115" <%=openBO.getShrs().equals("1115")?"selected":"" %>>11:15 A</option>
		                                    <option value="1130" <%=openBO.getShrs().equals("1130")?"selected":"" %>>11:30 A</option>
		                                    <option value="1145" <%=openBO.getShrs().equals("1145")?"selected":"" %>>11:45 A</option>
		                                    <option value="1200" <%=openBO.getShrs().equals("1200")?"selected":"" %>>12:00 P</option>
		                                    <option value="1215" <%=openBO.getShrs().equals("1215")?"selected":"" %>>12:15 P</option>
		                                    <option value="1230" <%=openBO.getShrs().equals("1230")?"selected":"" %>>12:30 P</option>
		                                    <option value="1245" <%=openBO.getShrs().equals("1245")?"selected":"" %>>12:45 P</option>
		                                    <option value="1300" <%=openBO.getShrs().equals("1300")?"selected":"" %>>01:00 P</option>
		                                    <option value="1315" <%=openBO.getShrs().equals("1315")?"selected":"" %>>01:15 P</option>
		                                    <option value="1330" <%=openBO.getShrs().equals("1330")?"selected":"" %>>01:30 P</option>
		                                    <option value="1345" <%=openBO.getShrs().equals("1345")?"selected":"" %>>01:45 P</option>
		                                    <option value="1400" <%=openBO.getShrs().equals("1400")?"selected":"" %>>02:00 P</option>
		                                    <option value="1415" <%=openBO.getShrs().equals("1415")?"selected":"" %>>02:15 P</option>
		                                    <option value="1430" <%=openBO.getShrs().equals("1430")?"selected":"" %>>02:30 P</option>
		                                    <option value="1445" <%=openBO.getShrs().equals("1445")?"selected":"" %>>02:45 P</option>
		                                    <option value="1500" <%=openBO.getShrs().equals("1500")?"selected":"" %>>03:00 P</option>
		                                    <option value="1515" <%=openBO.getShrs().equals("1515")?"selected":"" %>>03:15 P</option>
		                                    <option value="1530" <%=openBO.getShrs().equals("1530")?"selected":"" %>>03:30 P</option>
		                                    <option value="1545" <%=openBO.getShrs().equals("1545")?"selected":"" %>>03:45 P</option>
		                                    <option value="1600" <%=openBO.getShrs().equals("1600")?"selected":"" %>>04:00 P</option>
		                                    <option value="1615" <%=openBO.getShrs().equals("1615")?"selected":"" %>>04:15 P</option>
		                                    <option value="1630" <%=openBO.getShrs().equals("1630")?"selected":"" %>>04:30 P</option>
		                                    <option value="1645" <%=openBO.getShrs().equals("1645")?"selected":"" %>>04:45 P</option>
		                                    <option value="1700" <%=openBO.getShrs().equals("1700")?"selected":"" %>>05:00 P</option>
		                                    <option value="1715" <%=openBO.getShrs().equals("1715")?"selected":"" %>>05:15 P</option>
		                                    <option value="1730" <%=openBO.getShrs().equals("1730")?"selected":"" %>>05:30 P</option>
		                                    <option value="1745" <%=openBO.getShrs().equals("1745")?"selected":"" %>>05:45 P</option>
		                                    <option value="1800" <%=openBO.getShrs().equals("1800")?"selected":"" %>>06:00 P</option>
		                                    <option value="1815" <%=openBO.getShrs().equals("1815")?"selected":"" %>>06:15 P</option>
		                                    <option value="1830" <%=openBO.getShrs().equals("1830")?"selected":"" %>>06:30 P</option>
		                                    <option value="1845" <%=openBO.getShrs().equals("1845")?"selected":"" %>>06:45 P</option>
		                                    <option value="1900" <%=openBO.getShrs().equals("1900")?"selected":"" %>>07:00 P</option>
		                                    <option value="1915" <%=openBO.getShrs().equals("1915")?"selected":"" %>>07:15 P</option>
		                                    <option value="1930" <%=openBO.getShrs().equals("1930")?"selected":"" %>>07:30 P</option>
		                                    <option value="1945" <%=openBO.getShrs().equals("1945")?"selected":"" %>>07:45 P</option>
		                                    <option value="2000" <%=openBO.getShrs().equals("2000")?"selected":"" %>>08:00 P</option>
		                                    <option value="2015" <%=openBO.getShrs().equals("2015")?"selected":"" %>>08:15 P</option>
		                                    <option value="2030" <%=openBO.getShrs().equals("2030")?"selected":"" %>>08:30 P</option>
		                                    <option value="2045" <%=openBO.getShrs().equals("2045")?"selected":"" %>>08:45 P</option>
		                                    <option value="2100" <%=openBO.getShrs().equals("2100")?"selected":"" %>>09:00 P</option>
		                                    <option value="2115" <%=openBO.getShrs().equals("2115")?"selected":"" %>>09:15 P</option>
		                                    <option value="2130" <%=openBO.getShrs().equals("2130")?"selected":"" %>>09:30 P</option>
		                                    <option value="2145" <%=openBO.getShrs().equals("2145")?"selected":"" %>>09:45 P</option>
		                                    <option value="2200" <%=openBO.getShrs().equals("2200")?"selected":"" %>>10:00 P</option>
		                                    <option value="2215" <%=openBO.getShrs().equals("2215")?"selected":"" %>>10:15 P</option>
		                                    <option value="2230" <%=openBO.getShrs().equals("2230")?"selected":"" %>>10:30 P</option>
		                                    <option value="2245" <%=openBO.getShrs().equals("2245")?"selected":"" %>>10:45 P</option>
		                                    <option value="2300" <%=openBO.getShrs().equals("2300")?"selected":"" %>>11:00 P</option>
		                                    <option value="2315" <%=openBO.getShrs().equals("2315")?"selected":"" %>>11:15 P</option>
		                                    <option value="2330" <%=openBO.getShrs().equals("2330")?"selected":"" %>>11:30 P</option>
		                                    <option value="2345" <%=openBO.getShrs().equals("2345")?"selected":"" %>>11:45 P</option>
		                                    </select>
		<%} %></td></tr>
		
		</table></td></tr>
		<tr><td>
		<table border="1" width="100%" bgcolor="#f8f8ff">
				<tr>
					<td width="100%" colspan="4">
						<table  width="100%">
							<tr>
								<td width="55%" style="background-color: lightgreen"><b><font size="2">P/U:</font><input class="ui-autocomplete-input" name="addressRed" id="addressRed" tabindex="3" type="text" size="29" onfocus="Data(1,1)" onblur="clearAddress()" onkeypress="checkKeyPress()"></input></b>
		<img alt="" src="images/address_book.gif" style="height: 20px;width:20px;" onclick="Data(1,2)"/>
		</td>
		<td width="30%" style="background-color: lightgreen"><b><font size="2">LM:</font><input  type="text" name="slandmarkSmall" size="16" id="slandmarkSmall" tabindex="4" autocomplete="off" value="<%=openBO.getSlandmark()==null?"":openBO.getSlandmark()%>"
		onkeypress="checkKeyPress()"/></b>
		<ajax:autocomplete
			fieldId="slandmarkSmall"
			popupId="model-popup1"
			targetId="slandmarkSmall"
			baseUrl="autocomplete.view"
			paramName="landmark"
			className="autocomplete"
			progressStyle="throbbing"/> 		
		</td>
		<td width="10%" style="background-color: lightgreen"><b><font size="2">New</font><input type="checkbox"
								name="newLandMark1Small" id="newLandMark1Small" ></input></b>
						</td>
					</tr>
				</table></td>
		</tr>
		<tr>
			<td class="firstCol" width="10%">Add1</td>
			<td width="40%"><input type="text" name="sadd1Small" size="25" id="sadd1Small" readonly="readonly"
								value="<%= openBO.getSadd1()  %>" autocomplete="off"
		class="form-autocomplete" onblur="Data(1)" onchange="change(1)" onkeypress="checkKeyPress()" />
		</td>
		<td class="firstCol" width="10%">Add2</td>
		<td width="40%"><input type="text" name="sadd2Small" size="25" id="sadd2Small" tabindex="5" 
							value="<%= openBO.getSadd2()  %>" autocomplete="off"
								class="form-autocomplete" 
								 />
			</td>
		
		</tr>
		<tr>
			<td class="firstCol" width="10%">City</td>
			<td width="40%"><input type="text" name="scitySmall" size="10" id="scitySmall" readonly="readonly" class="form-autocomplete"
								value="<%= openBO.getScity()  %>"  onchange="change(1)"/>
		</td>
		<td class="firstCol" width="10%">State</td>
		<td width="40%"><input type="text" name="sstateSmall" size="3"
			id="sstateSmall" value="<%=openBO.getSstate() %>"  readonly="readonly"
		 onchange="change(1)"/> Zip<input type="text"
		name="szipSmall" size="5" id="szipSmall" value="<%= openBO.getSzip()%>"
		></input>
		<img align="right" alt="" id="imgReplace" src="images/plus_icons.png"  onclick="showDropSmall()"/>
				</td>
		</tr>
					</table></td></tr></table>
		<table id="dropSmall" border="1" width="100%" bgcolor="#f8f8ff" style="display: none">
			<tr>
		<td width="100%" colspan="4">
				<table  width="100%">
				<tr>
					<td width="55%" style="background-color:#fa8072"><b><font size="2">D/O:</font><input class="ui-autocomplete-input" name="addressRed" id="endAddressRed" tabindex="6" type="text" size="29" onfocus="Data(2,3)" onblur="clearAddress()" onkeypress="checkKeyPress()"></input></b>
		<img alt="" src="images/address_book.gif" style="height: 20px;width:20px;" onclick="Data(2,2)"/>
			</td>
		<td width="30%" style="background-color: #fa8072"><b><font size="2">LM:</font><input  type="text" name="elandmarkSmall" size="16" id="elandmarkSmall" tabindex="7" autocomplete="off" value="<%=openBO.getSlandmark()==null?"":openBO.getSlandmark()%>"
		onkeypress="checkKeyPress()" /></b>
		<ajax:autocomplete
				fieldId="elandmarkSmall"
				popupId="model-popup1"
				targetId="elandmarkSmall"
				baseUrl="autocomplete.view"
				paramName="landmark"
				className="autocomplete"
				progressStyle="throbbing"/> 
				</td>
		<td width="10%" style="background-color: #fa8072"><b><font size="2">New</font><input type="checkbox"
							name="newLandMark2Small" id="newLandMark2Small" ></input></b>
					</td>
				</tr>
			</table>
				</td>
			</tr>
		<tr>
		<td class="firstCol" width="10%">Add1</td>
		<td width="40%"><input type="text" name="eadd1Small" size="25" id="eadd1Small" readonly="readonly"
							value="<%= openBO.getEadd1()  %>" autocomplete="off"
		class="form-autocomplete" onblur="Data(2)" onchange="change(2)" onkeypress="checkKeyPress()"/>
		</td>
		<td class="firstCol" width="10%">Add2</td>
		<td width="40%"><input type="text" name="eadd2Small" size="25" id="eadd2Small" tabindex="8" 
							value="<%= openBO.getEadd2()  %>" autocomplete="off"
								class="form-autocomplete"  
								 />
			</td>
		
		</tr>
		<tr>
			<td class="firstCol " width="10%">City</td>
			<td width="40%"><input type="text" name="ecitySmall" id="ecitySmall" size="10" readonly="readonly"
								value="<%= openBO.getEcity()  %>"  />
		</td>
		<td class="firstCol" width="10%">State</td>
		<td width="40%"><input type="text" name="estateSmall" size="3" readonly="readonly"
			id="estateSmall" value="<%=openBO.getEstate()==""?openBO.getSstate():openBO.getEstate() %>" onblur="Data1()"
		/> Zip<input type="text"
		name="ezipSmall" size="5" id="ezipSmall" value="<%= openBO.getEzip()  %>" />
				</tr>
			</table>
		<tr><td><table border="1" width="100%" bgcolor="#f8f8ff"> 			
		<tr>
		<div class="btnBlueNew1">
			<td style="width: 10%;">
		<div class="rht">
		<input type="button"  id="close" name="close" 
			class="lft" value="Close" onclick="fnClose()"/>
		</div></td></div>
							<div class="btnBlueNew1">
			<td style="width: 15%;">
		<div class="rht">
			<input type="button"  id="Advance" name="Advance" 
				class="lft" value="Advance Screen" onclick="getAdvScreen()"/>
			</div></td>
			</div>
		<div class="btnBlueNew1">
			<td style="width: 15%;">
		<div class="rht">
			<input type="button"  id="Done" name="Done" 
				class="lft" value="Book Job" onclick="submitSmall()"/>
					</div></td>
					</div></tr>
					</table></td></tr>
		</div>
		<div id="detailsAutoPopulated1" style="display: none;width: 100%;">
			<jsp:include page="/jsp/OpenRequestOne.jsp"/>
		</div>
			 <div id="ORHistory" class="jqmWindow" style=" display: none; align: center;left:5px;  position: absolute;">
                        <jsp:include page="/Company/OpenRequestHistory.jsp" /> 
                           </div>
                        <div id="ORJobSummary" class="jqmWindow" style=" display: none;   position: absolute; ">
                        <jsp:include page="/Company/openRequestSummary.jsp" /> 
                          </div>
  
		<div id="commentsByAcct" style="font-weight: bolder;color:#7E2217;background-color:#BDEDFF;border-color:#FF3333;border-width: 3px;-moz-border-radius:12px 12px 12px 12px;display: none;width: 100%;">
		</div>
				<div id="showShortcut" style="display: none">
	 	<table>
	 	<tr style='color: blue'>
	 	<td>
	 	<font style="font-weight: bold;">Alt+Delete-</font>ClearAll
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
	 	<font style="font-weight: bold;">Alt+Q-</font>Clear P/U
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
		<font style="font-weight: bold;">Alt+W-</font>Clear D/O
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
		<font style="font-weight: bold;">Alt+P-</font>Focus Phone
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
		<font style="font-weight: bold;">Alt+Y-</font>Focus P/U
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
		<font style="font-weight: bold;">Alt+T-</font>Focus D/O
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
		<font style="font-weight: bold;">Alt+X-</font>Close
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
		<font style="font-weight: bold;">Alt+Up Arrow-</font>Spl Req
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
		<font style="font-weight: bold;">Alt+Down Arrow-</font>Dispatch Comments
	 	</td></tr>
	 	<tr style='color: blue'>
	 	<td>
		<font style="font-weight: bold;">Alt+Right Arrow-</font>Driver Comments
	 	</td></tr>
	 	</table>
		</div>
		<div id="preAuthCC" style="display: none;width: 100%;">
			<jsp:include page="/jsp/PreAuthorize.jsp"/>
		</div>
		<div id="MapDetails" style="display: none;width: 100%;"></div>
		<div id="tripsForFlags" style="display: none;width: 100%;"></div>
		<div id="LandDetails" style="display: none;width: 100%;"></div>
		<div id="completedJobs" style="display: none;width: 100%;">
			<jsp:include page="/jsp/OpenRequestCompleted.jsp"/>
		</div>
		<div id="phoneProvider" style="width: 50%;display: none;width: 100%;">
	 	<table style="width: 30%;margin-left: 10%;"><tr style='color: blue'>
	 	<td style="width: 15%;"><select name="provider" id="provider">
		<option value="">Select</option>
		<option value="@txt.att.net">ATT</option>
		<option value="@myboostmobile.com">Boost Mobile</option>
		<option value="@sms.mycricket.com">Cricket</option>
		<option value="@mymetropcs.com">MetroPCS</option>
		<option value="@smtext.com">Simple Mobile</option>
		<option value="@tmomail.net">TMobile</option>
		<option value="@vtext.com">Verizon</option>
		<option value="@vmobl.com">Virgin</option>
		<option value="@messaging.sprintpcs.com">Sprint</option>
		</select></td>
	 	<td style="width: 15%;"><input type="button" value="Confirm" id="confirm" onclick="enterEmailOR()"/></td>
	 	</tr></table>
		</div>
</form>
</body>
</html>


