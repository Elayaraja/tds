<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="com.tds.tdsBO.OpenRequestFieldOrderBO"%>
<%@ page language="java" %>
<%@ page import="com.tds.constants.docLib.*" %>
<%@ page import="com.tds.controller.FileDocForm" %> 
<%@ page language="java" import="java.util.*" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.FleetBO;" %>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%
	String ctxPath = request.getContextPath();
	
%>

<HTml>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<HEAD>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		fleetList=(ArrayList)session.getAttribute("fleetList");
	}else{
		fleetList=null;
	} %>
<script type="text/javascript">

function checkFiles(){
var ext = $('#filePath').val().split('.').pop().toLowerCase();
var voucherNumber=document.getElementById("voucherNumber").value;
if($.inArray(ext, ['csv']) == -1) {
    alert('invalid extension!..You Can Upload only the CSV files');
    return false;
}
if(voucherNumber!=""){
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='SystemSetupAjax?event=checkVoucher&voucherNumber='+voucherNumber;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!="ok"){
		alert(text);
		return false;
	}
}
	return true;
}
function urlChange(){
	var vendor=document.getElementById("vendor").value;
	var sharedRide=document.getElementById("sharedRide").value;
	var dontDispatch=document.getElementById("dontDispatch").value;
	var phone=document.getElementById("noPhone").value;
	var voucher=document.getElementById("voucher").value;
	var voucherNumber="";
	if(voucher=="1"){
		voucherNumber=document.getElementById("voucherNumber").value;
	}
	var fleetsize = document.getElementById("fleetSize").value;
	var fleetV;
	if(fleetsize!=0){
		fleetV = document.getElementById("fleetV").value;	
		//alert(fleetno);
	}else{
		fleetV=0;
	}
	document.documents.action = "control?action=openrequest&event=fileUploadForOR&module=dispatchView&uploadFile=upload&vendor="+vendor+'&sharedRide='+sharedRide+'&dontDispatch='+dontDispatch+'&noPhone='+phone+'&voucher='+voucher+'&voucherNumber='+voucherNumber+'&fleetSize='+fleetsize+'&fleetno='+fleetV;
}
checked = false;
function checkSharedRide() {
	var form= document.documents;
	if (checked == false) {
		checked = true;
	} else {
		checked=false;
	}
	if(checked){
		 document.getElementById("sharedRide").value=1;
	}else{
		 document.getElementById("sharedRide").value="";
	}
}
checkedPh = false;
function checkPhone() {
	var form= document.documents;
	if (checkedPh == false) {
		checkedPh = true;
	} else {
		checkedPh=false;
	}
	if(checkedPh){
		 document.getElementById("noPhone").value=1;
	}else{
		 document.getElementById("noPhone").value="";
	}
}

var checkedVo = false;
function checkVoucher() {
	var form= document.documents;
	if (checkedVo == false) {
		checkedVo = true;
	} else {
		checkedVo=false;
	}
	if(checkedVo){
		 document.getElementById("voucher").value=1;
		 $("#voucherNumber").show("slow");
	}else{
		 document.getElementById("voucher").value="";
		 $("#voucherNumber").hide("slow");
		 document.getElementById("voucherNumber").value="";
	}
}

checkedDD = false;
function checkDontDispatch() {
	var form= document.documents;
	if (checkedDD == false) {
		checkedDD = true;
	} else {
		checkedDD=false;
	}
	if(checkedDD){
		 document.getElementById("dontDispatch").value=1;
	}else{
		 document.getElementById("dontDispatch").value="";
	}
}
</script>
<TITLE>TDS(Taxi Dispatching System)</TITLE></HEAD>  
<%ArrayList<OpenRequestFieldOrderBO> vendorList=new ArrayList<OpenRequestFieldOrderBO>(); %>
<%if(request.getAttribute("vendorList")!=null){
	vendorList=(ArrayList<OpenRequestFieldOrderBO>)request.getAttribute("vendorList");
}

%>
  <BODY>
  <%if(request.getAttribute("errors")!=null && !request.getAttribute("errors").toString().equals("")){%>
  <div id="error" class="alert-error">
  "Errors while uploading some/all jobs"
  </div>
  <%} else { if(request.getParameter("uploadFile") != null) { %>
  <div id= "no error" class="alert-no error">
  "No errors while uploading. And "<%=request.getParameter("no_of_jobs") %> "jobs were uploaded successfully"
  </div>
  <%}}%>
  
 <%--  <form  name="masterForm"  action="control" method="post">
  <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <input type="hidden" name="action" value="openrequest"/>
		<input type="hidden" name="event" value="fileUploadForOR"/> --%>
    <FORM name="documents" ENCTYPE="multipart/form-data" ACTION="control?action=openrequest&event=fileUploadForOR&module=dispatchView&uploadFile=upload&vendor=0" METHOD=POST onsubmit="return checkFiles() ">
    <jsp:useBean id="voucherBo" class="com.tds.tdsBO.VoucherBO" scope="request"/>
			<c class="nav-header"><center>File Upload For Jobs</center></c>
            <input type="hidden" name="fleetSize" id="fleetSize" value="<%=fleetList!=null?fleetList.size():0%>">
         <table class="navbar-static-top" align="center">
                    <tr><td colspan="2" align="center"><b>Choose the file To Upload:</b>
					</td>
                    <td colspan="2"><input name="filePath" id="filePath" TYPE="file" ></td>
                    </tr>
               <TR>
               <td>
               SharedRide:
               </td><td>
               <input type="checkbox" name="sharedRide" id="sharedRide" value="" onclick="checkSharedRide(),urlChange()">
             	</td>
             	<td>
             	Don't Dispatch
             	</td><td>
               <input type="checkbox" name="dontDispatch" id="dontDispatch" value="" onclick="checkDontDispatch(),urlChange()">
               </td>
               </tr>
               <tr>
               	<td>
             	No Phone
             	</td><td>
               <input type="checkbox" name="noPhone" id="noPhone" value="" onclick="checkPhone(),urlChange()">
               </td>
               <td>
             	Voucher
             	</td><td>
               <input type="checkbox" name="voucher" id="voucher" value="" onclick="checkVoucher(),urlChange()">
               <!-- <input type="text"  name="voucherNumber" id="voucherNumber" value="" onblur="urlChange()" style="display: none;"/> -->
               <input type="text" autocomplete="off" name="voucherNumber" id="voucherNumber" value="<%= !voucherBo.getVno().equals("")?voucherBo.getVno():""%>" onblur="urlChange()" style="display: none;" /> 
                  <ajax:autocomplete 
  					fieldId="voucherNumber"
  					popupId="model-popup1"
  					targetId="voucherNumber"
  					baseUrl="autocomplete.view"
  					paramName="getAccountList"
  					className="autocomplete"
  					progressStyle="throbbing" />
               </td>
               </TR>
               
               
               <tr>
               <td>Select Fleet</td>
               <td align="center">
                <%if(fleetList!=null && fleetList.size()>0){%>
                <select name="fleetV" id="fleetV" onclick="urlChange()" >
       				<%for(int i=0;i<fleetList.size();i++){ %>
       					<option value="<%=fleetList.get(i).getFleetNumber()%>" ><%=fleetList.get(i).getFleetName() %></option>
				<% }%>
				</select>
       			<%}
                else{%>
                No Fleets
       			<%}%>
				</td></tr>
				 

			  <tr><td colspan="2">
               <input name="pathToAction" id="pathToAction" type="hidden" value=""/>
               <p align="right"></p></td>
               </tr>               
 				<tr><td colspan="4" align="center">
 				<select name="vendor" id="vendor" onchange="urlChange()" >
				<option value="1" selected="selected">Select</option>
				 <%if(vendorList!=null){
					for(int i=0;i<vendorList.size();i++){ %>
					<option value="<%=vendorList.get(i).getVendor() %>" ><%=vendorList.get(i).getVendorName() %></option>
				<% }}%> 
				 </select>
				 </td></tr>
 <tr>
 <td colspan="4" align="center">
 <INPUT TYPE="submit" name="uploadFile" id="uploadFile" VALUE="Upload File"  />
 </td>
 </tr>
             </table>
 </FORM>
 
 <%if(request.getAttribute("errors")!=null && !request.getAttribute("errors").toString().equals("")){%>
  <div id="error" class="alert-error">
  <%=request.getAttribute("errors").toString() %>
  </div>
  <%}
 %>
</BODY>
</HTML>
