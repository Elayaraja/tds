<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.CompanyMasterBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<title>TDS(Taxi Dispatching System)</title>
<script type="text/javascript">
function loadAnchr()
{
	document.getElementById("ancid").style.display = 'block';
	document.getElementById("filediv").innerHTML="";
	document.getElementById("fileid").value="";
	document.getElementById("filename").value="";
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}

 </script> 
</head>
<body>
<form name="companyForm" action="control" method="post" onsubmit="return showProcess()">
<jsp:useBean id="companyBO"   class="com.tds.tdsBO.CompanyMasterBO" scope="request"  />
<% if(request.getAttribute("companyBO") != null)
		companyBO = (CompanyMasterBO)request.getAttribute("companyBO");
	%>
<jsp:setProperty name="companyBO"  property="*"/>
<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"/>
<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.saveCompany %>"/>
<input type="hidden" name="systemsetup" value="6"/>
				<c class="nav-header"><center>Company Master</center></c>
				<%
						String error ="";
						if(request.getAttribute("errors") != null) {
						error = (String) request.getAttribute("errors");
						}
					%>	<div id="errorpage" class="alert-error">
						<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %>
						</div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="navbar-static-top">
                    <%
						final char m_dispatchStatus[] = {'a','x','m'};
						final String m_dispatchValue[] = {"Automatic Allocation", "Manual Allocation","Computer Aided"};
					%>
					<tr>
					<td >Company&nbsp;Name</td>
					<td>
					<input type="text" name="cname"  value="<%= companyBO.getCname() %>" >
					</td>
					<td >Company Description</td>
					<td>
					<input type="text" name="cdesc"  value="<%= companyBO.getCdesc() %>">
					</td>
					</tr>
					<tr>
					<td >Address1</td>
					<td>
					<input type="text" name="add1" value="<%= companyBO.getAdd1() %>" >
					</td>
					<td >Address2</td>
					<td>
					<input type="text" name="add2" value="<%= companyBO.getAdd2() %>" >
					</td>
					</tr>
					<tr>
					<td >City</td>
					<td>
					 <input type="text" name="city" value="<%= companyBO.getCity() %>" > 
					</td>
					<td >State</td>
					<td>
					 <input type="text" name="state" value="<%= companyBO.getState() %>" >
					</td>
					</tr>
					<tr>
					<td >Zip&nbsp;Code</td>
					<td>
					<input type="text" name="zip" value="<%= companyBO.getZip() %>"  maxlength="5">
					</td>
					<td >Phone&nbsp;No</td>
					<td>
					<input type="text" name="phoneno"  value="<%= companyBO.getPhoneno() %>">
					</td>
					</tr>
					<tr>
					<td >Bank&nbsp;Account&nbsp;No</td>			
					<td>
					<input type="text" name="bankacno" value="<%= companyBO.getBankacno() %>" >
					</td>
					<td >Bank&nbsp;Name</td>
					<td>
					<input type="text" name="bankname" value="<%= companyBO.getBankname() %>" >
					</td>
					</tr>
					<tr>
					<td >Bank&nbsp;Routing&nbsp;No</td>
					<td>
					<input type="text" name="bankroutingno" value="<%= companyBO.getBankroutingno() %>" >
					</td>
					<td >Dispatch&nbsp;Status</td>
					<td>
					<Select name="dispatchStatus" >
					<%
						for(int counter = 0; counter < m_dispatchStatus.length; counter ++) {
						if(companyBO.getDipatchStatus() == m_dispatchStatus[counter]) {
					%>
						<option value="<%= m_dispatchStatus[counter] %>" selected="selected"><%= m_dispatchValue[counter] %></option>
					<%
					} else {
					%>
						<option value="<%= m_dispatchStatus[counter] %>"><%= m_dispatchValue[counter] %></option>
					<%
					}
						
					}
					%>
					</Select>
					</td>
					</tr>
					<tr>
					<td >Email</td>
					<td colspan="4">
					<input type="text" name="Email" value="<%= companyBO.getEmail() %>" >
					</td>
					</tr>
					<tr>
					<td >Attachmnet</td>
					<td colspan="4" align="center">
					<%
			 				String filename1="";
			 				filename1 = request.getAttribute("filename")==null?"":(String)request.getAttribute("filename");
							String filename_split1[]=filename1.split("<br>");
							String fileid = request.getAttribute("fileid")==null?"":(String)request.getAttribute("fileid");
						%>
					 <div id = "filediv" >
						<%
						  {
							for(int i=0;i<filename_split1.length;i++)
							{	
								out.println("<a href='/TDS/control?action=file&event=fileDownload&fileid="+fileid+"&filename="+filename_split1[i]+"' 'onclick=showProcess()'>"+filename_split1[i]+"</a><br>");
							}
						  }
						%>
					</div>
						<a href='javascript:doNothing()' <%if(fileid!="" && fileid!="0") {%> style="display: none;" <%} %> id="ancid" onclick="window.open('/TDS/control?action=file&event=fileattachstart','cal','screenX=0 screenY=0 scrollbars=no height=200 width=700')">Logo Upload</a>
						<a href='javascript:doNothing()' onclick="loadAnchr();">Clear</a>
						<input class="textdatafield" type="hidden" size="105" readonly name="filename" id="filename" value="<%=(filename1==""?"":filename1)%>">
						 <input type="hidden" name="fileid" id="fileid" value="<%=fileid %>">
					</td>
					</tr>
					
					<%	if(request.getAttribute("pagefor") != null && request.getAttribute("pagefor").equals("Submit")){ %>
					<tr>
					<td colspan="4" align="center">
                        <input type="submit" value="Create Company" name="submit" >
                    </td>
                    </tr>
                    <%}else{ %>
                    <tr>
					<td colspan="4" align="center">
                        <input type="submit" value="Update" name="submit"  >
                    </td>
                    </tr>
                    <%} %>
                    <tr>
                    <td colspan="4">
						Note : Upload Company Logo Of Max Size 403X82 For Better Resolution
					</td>
					</tr>							                   
                    </table>
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
</form>
</body>
</html>
