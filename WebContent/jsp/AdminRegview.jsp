<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Registration View</title>
<script type="text/javascript">
	function cal(ivalue) {
	document.getElementById("ivalue").value=document.getElementById(ivalue).value;
	}
</script>
<script type="text/javascript">
function deleteOperator(driverNumber,x){
	var driver=driverNumber;
	var confirmation=confirm("Records on  "+driver+" going to delete");
	if (confirmation==true){
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='RegistrationAjax?event=deleteDriver&module=operationView&driverNum='+driver+'&driverOrOperator='+1;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text=="1"){
		var table=document.getElementById("operatorSummary");
		table.deleteRow(x.parentNode.parentNode.rowIndex);
	}
 }
}
	
</script>
</head>
<body>
<form method="post" action="control" name="admin">
  <input type="hidden" name="<%=TDSConstants.actionParam%>"	value="<%=TDSConstants.registrationAction%>"> 
	<input	type="hidden" name="<%=TDSConstants.eventParam%>" value="<%=TDSConstants.saveAdminRegistraion%>">
	<input type="hidden" name="systemsetup" value="6">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
        	 
            
            <div class="contentDVInWrap">
            <div class="contentDVIn">
            	<div >
                	
                    <div></div>
                </div>
                
                <div>
<div style="width: 590px">
                	<h1>Operators</h1>
                	</div>
                    <table width="70%" id="operatorSummary" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <%ArrayList a_list=(ArrayList)request.getAttribute("adminBO");
						if(a_list != null){%>
                      <tr>
                        <th width="15%">User Name</th>
                        <th width="15%">UserType</th>
                        <th width="15%">Edit</th>
                        <th width="15%">Delete</th>
                      </tr>
                      
                      	<%for(int i=0;i<a_list.size();i++) {
						AdminRegistrationBO adminBO=(AdminRegistrationBO) a_list.get(i);%>
						<%if(i%2==0){ %>
						<tr>
							<td><%=adminBO.getUname() %></td>
							<td><%=adminBO.getUsertype() %></td>
						    <td>
						    	<div class="wid60 marAuto">
                        		<div class="btnBlue">
                        		<div class="rht">
                        	 		<input type="hidden" name="uname<%=i %>" id="uname<%=i %>" value="<%=adminBO.getUid() %>">
								<input type="hidden" name="ivalue" id="ivalue" value="">
								<input type="submit" name="submit" value="Edit" class="lft" onclick="cal('uname<%=i %>');">
                          	<td >&nbsp;<input type="button" name="imgDeleteOperator" id="imgDeleteOperator" value="Delete" onclick="deleteOperator('<%=adminBO.getUid() %>',this)"></input></td>
                        	 		                       	 	
                        	 	</div>
                            	<div class="clrBth"></div>
                        		</div>
                        		<div class="clrBth"></div>
                    			</div>		
						    </td>
						</tr>
						<%}else{ %>
						<tr>
							<td style="background-color:lightgreen"><%=adminBO.getUname() %></td>
							<td style="background-color:lightgreen"><%=adminBO.getUsertype() %></td>
						    <td style="background-color:lightgreen">
						    	<div class="wid60 marAuto">
                        		<div class="btnBlue">
                        		<div class="rht">
                        	 		<input type="hidden" name="uname<%=i %>" id="uname<%=i %>" value="<%=adminBO.getUid() %>">
								<input type="hidden" name="ivalue" id="ivalue" value="">
								<input type="submit" name="submit" value="Edit" class="lft" onclick="cal('uname<%=i %>');">
                        	 		                       	 	
                        	 	</div>
                            	<div class="clrBth"></div>
                        		</div>
                        		<div class="clrBth"></div>
                    			</div>		
						    </td>
			             	<td style="background-color:lightgreen">&nbsp;<input type="button" name="imgDeleteOperator" id="imgDeleteOperator" value="Delete" onclick="deleteOperator('<%=adminBO.getUid() %>',this)"></input></td>
						    
						</tr>
						<%} %>
					<%}} %>
						</table>                             
                </div>    
              </div>
            	
                <div class="clrBth"></div>
            </div>
            </div>
            </div>
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
		</div>
	</div>
</form>
</body>
</html>