<%@page import="com.tds.tdsBO.MessageBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
	ArrayList al_openRequest = new ArrayList();	
	ArrayList message_data = new ArrayList();	
	OpenRequestBO openRequestBO;
	if(request.getAttribute("openRequest") != null) {
		al_openRequest = (ArrayList) request.getAttribute("openRequest");
	}	
%>
<%= "<xml version=\"1.0\" >"+
	"<server>"+
	"<Messages>"%>
<%
	if(request.getAttribute("message")!= null){
		message_data = (ArrayList)request.getAttribute("message");
		System.out.println("JSP message :"+message_data);
		for(int mcount=0;mcount<message_data.size();mcount++){
			MessageBO messBo = (MessageBO)message_data.get(mcount);
			out.println("<Message>");
			out.println("<MessageCode>"+messBo.getMessagecode()+"</MessageCode>");
			out.println("<MessageDesc>"+messBo.getDesc()+"</MessageDesc>");
			out.println("</Message>");
		}
	}
	else{
%>
<%=	"<Message>"+
	"<MessageCode>"+
	"</MessageCode>"+
	"<MessageDesc>"+
	"</MessageDesc>"+
	"<MessageVariable>"+
	"</MessageVariable>"+
	"</Message>	"+
	"<Message>	"+
	"<MessageCode>"+
	"</MessageCode>"+
	"<MessageDesc>"+
	"</MessageDesc>"+
	"<MessageVariable>"+
	"</MessageVariable>"+
	"</Message>"%>
<%} %>	
<%="</Messages>"+
"<OpenRequests>" %>
	<%
		for(int counter=0;counter<al_openRequest.size();counter++) {
			openRequestBO = (OpenRequestBO) al_openRequest.get(counter);
			out.println("<OpenRequest>");
			out.println("<RequestID>"+openRequestBO.getTripid()+"</RequestID>");
			out.println("<StartLO>"+openRequestBO.getScity()+" "+openRequestBO.getSstate()+"</StartLO>");
			out.println("<EndLO>"+openRequestBO.getEcity()+" "+openRequestBO.getEstate()+"</EndLO>");
			out.println("<Time>"+openRequestBO.getShrs()+":"+openRequestBO.getSmin()+"</Time>");
			out.println("<Date>"+openRequestBO.getSdate()+"</Date>");
			out.println("<Latitude>"+openRequestBO.getSlat()+","+openRequestBO.getEdlatitude()+"</Latitude>");
			out.println("<Longitude>"+openRequestBO.getSlong()+","+openRequestBO.getEdlongitude()+"</Longitude>");
			out.println("<NumPass></NumPass>");
			out.println("<StartAddress>"+openRequestBO.getSadd1()+" "+openRequestBO.getSadd2()+"<StartAddress>");
			out.println("<SpecialRequirement></SpecialRequirement>");
			out.println("<SpecialRide></SpecialRide>");
			out.println("<PaymentType></PaymentType>");
			out.println("</OpenRequest>");
		}
	%>
<%="</OpenRequests></server>" %>