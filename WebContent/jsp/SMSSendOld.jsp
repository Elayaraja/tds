<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO;"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
function disabledriver()
{
	if(document.getElementById('all').checked)
	{
		document.getElementById('driver_id').value="";
		 
		document.getElementById('driver_id').disabled = true;
	} else {
		 
		document.getElementById('driver_id').disabled = false;
	}
} 
function cal(){
	
	var a=document.getElementById('driver_id').value;
	var c=document.getElementById('msg').value;
	if(a == "" && document.getElementById('all').checked == false) { 
		document.getElementById('validation').innerHTML="<font color='red'>DriverId Invalid</font>";
		return false;
	}
	 
	 if(c==""){
			document.getElementById('validation').innerHTML="<font color='red'>Invalid Description</font>";
			return false;
			} 
	return true;
		
	} 
function formsubmit(){
	var result=cal();
	if(result==true)
	{
		document.getElementById('Button').value="Submit";
		
		document.masterForm.submit();
	}
}
</script> 
</head>
<body>
<form name="masterForm" id="masterForm" action="control" method="post" >
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	<input type="hidden" name="action" value="openrequest"/>
	<input type="hidden" name="event" value="<%=TDSConstants.getSendSMS %>"/>
	
 
             	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div class="rightCol">
<div class="rightColIn">
<h1>Message Sent</h1>
                     <%String pages = (String)request.getAttribute("page"); %>
                     
                     <%= (pages !=null && pages.length()>0)?pages :""  %>
                     

  

                        		<%
									String error = (String) request.getAttribute("error");
								%>
							   <%= (error !=null && error.length()>0)?"You must correct the following error<br>"+error :"" %>
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
                    <td>
                    <table id="bodypage" width="440"    >
                    <tr>
                    	<td  class="firstCol">Driver Id</td>
                    	<td> <input type="text"    onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)" onblur="caldid(id,'drivername','dName')" name="driver_id" id="driver_id" size="15" value="<%=request.getParameter("driver_id")==null?"":request.getParameter("driver_id") %>"/> 
                    	</td>
                    </tr>
                    <tr>
                    <td></td>
                    <td>
                    <input type="checkbox"  name="all"       style="vertical-align: middle"   id="all"  <%=(request.getParameter("all") != null && request.getParameter("all").equals("1"))?"checked":"" %> onclick="disabledriver()"/><font color="blue" >Select All</font>
                    </td>
                    </tr>
                    <tr>
                    	<td  class="firstCol">Send to Logged Out Also</td>
                    	<td><input type="checkbox"  name="sendLO" id="sendLO"  <%=(request.getParameter("sendLO") != null && request.getParameter("sendLO").equals("1"))?"checked":"" %>/></td>
                    </tr>
                    <tr>
                    	<td class="firstCol">Voice Message</td>
                    	<td><input type="checkbox"  name="vmsg" id="vmsg"  <%=(request.getParameter("vmsg") != null && request.getParameter("vmsg").equals("1"))?"checked":"" %>/>
                    	</td>
                    </tr>
                    <tr>
                    	<td colspan="2" align="center" >
                    	<div>
                    			<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>"/>		
							 	<div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>
                                    							
                    	</div>
                    	</td>
                    </tr>
                    <tr>
                    	<td class="firstCol" >Message Body</td>
                    	<td><textarea  name="msg" id="msg" cols="25" rows="10"  ></textarea></td>
                    </tr>
                    <tr align="center" >
                    	<td  colspan="2"  align="center"  >
                    	
                    	<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="hidden" name="Button" id="Button" value=""/>
									<input type="button" value="Submit"  onclick="formsubmit();" class="lft"/>
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>	
                    		
                    	</td>
                    </tr>
                    </table>
                    </td>
                    </tr>
                    </table>
						
</div>
</div>                   
           
            	
                <div class="clrBth"></div>
        
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
