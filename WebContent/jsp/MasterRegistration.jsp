
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.DriverRegistrationBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.charges.bean.ChargesBO"%>
        <%@page import="com.tds.tdsBO.FleetBO"%>

<%@page import="java.util.ArrayList"%><html
	xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" language="javascript"></script>



<script type="text/javascript">

function closeDriverProfile() {
	  var driver = document.getElementById('registration');
	  if(driver.value=="D") {
	   var drivertable = document.getElementById('Driver');
	   drivertable.style.display = '';
	      var buttonvalue = document.getElementById('NextScreen');
		  buttonvalue.value = "NextScreen";
	   
	  }else{
		  var drivertable = document.getElementById('Driver');  
		  drivertable.style.display = 'none';
	  }
	  if(driver.value=="O")
		  {
		  var buttonvalue = document.getElementById('NextScreen');
		  buttonvalue.value = "Provide Access";
		  }
  }
  function getUserAccess()
 {
	  var userid = document.getElementById('uid');
	  var userdesc = document.getElementById('registration');
	  var status = document.getElementById('status'); 
	  
      if(userdesc.value!="D")
	  {
        window.location.href = "control?action=userrolerequest&module=systemsetupView&event=getUserList&userid="+userid.value+"&uname="+userid.value+"&utype="+userdesc.value+"&userDesc="+userdesc.value+"&status="+status.value;
	  }else{
		  window.location.href = "control?action=registration&event=driverMapping&subevent=home&module=operationView";	  
	  }
 } 
 function submitInfo()
 {
	document.getElementById('Information').value="1";
 }
 function getLast3IDs(){
	 var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url='RegistrationAjax?event=getLast3Ids&module=systemsetupView';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("driverIDReference").innerHTML=text;
 }
</script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
function clearErrorInfo(){
	document.getElementById("errorpage").innerHTML ="";
}
function loadFile(docId){
	document.getElementById("docLoaderImage").src='searchDocs?do=viewDoc&docId='+docId;
}

$(document).ready ( function(){	
	closeDriverProfile();
		
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url='SystemSetupAjax?event=getDocumentId&module=systemsetupView&driverId='+document.getElementById("driver").value;
		//var url='SystemSetupAjax?event=getDocumentId&module=systemsetupView&driverId=103001';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("docLoader").innerHTML=text;
});
</script>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>

<title>TDS (Taxi Dispatch System)</title>
</head>
<%
	String frequen[] = {"No","Yes"};
	ArrayList al_list = (ArrayList)request.getAttribute("drFlag");
	ArrayList<ChargesBO> charges=new ArrayList<ChargesBO>();
%>
<body  >

	<form name="masterForm" action="control" method="post">
		<jsp:useBean id="driverBO" class="com.tds.tdsBO.DriverRegistrationBO" scope="request" />
		<jsp:useBean id="openBO" class="com.tds.tdsBO.OpenRequestBO" scope="request" />
		<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>" />
		<%
final char regkey[] = {'S','D','O'};
final String registrationValue[] = {"Select","Driver","Operator"};
%>
		<%
if(request.getAttribute("driverDetails")!=null){
	driverBO = (DriverRegistrationBO)request.getAttribute("driverDetails");
}if(request.getAttribute("defaultZone")!=null){
	openBO = (OpenRequestBO)request.getAttribute("defaultZone");
}if(request.getAttribute("template")!=null){
	charges =(ArrayList<ChargesBO>)request.getAttribute("template");
}
%>

		<input type="hidden" name="<%= TDSConstants.actionParam %>" value="<%= TDSConstants.registrationAction %>"></input>
			 <%if(request.getAttribute("edit")!=null){ %>
				<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.editUpdateDriverDetail %>"> </input>
				<input type="hidden" name="seq" value="<%=driverBO.getSno()%>"></input> 
				<input type="hidden" name="Information" id="Information" value="" /> 
			 <%}else{ %>
					<input type="hidden" name="<%= TDSConstants.eventParam %>" value="<%= TDSConstants.driversavedetails %>"> </input>
					<input type="hidden" name="wizard" value="yes"> </input>
					<%} %>


							<div class="leftCol">
								<div class="clrBth"></div>
							</div>
							<div class="rightCol">
								<div class="rightColIn">
									<h1>Driver/Operator Registration</h1>
									<%
						String error ="";
						if(request.getAttribute("errors") != null) {
						error = (String) request.getAttribute("errors");
						}
		
 					%>
									<div id="errorpage">
										<%=request.getAttribute("page")!=null?(String)request.getAttribute("page"):"" %>
										<%= error.length()>0?"You must fullfilled the following error<br>"+error :"" %>
									</div>

									<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
										<tr>
											<td>
												<table id="bodypage" width="540">
													<tr>
														<td class="firstCol">Registration:</td>
														<td><select name="registration" id="registration" onchange="closeDriverProfile()">
																<%for(int i=0;i<regkey.length;i++) {
			                	     if(driverBO.getRegistration() == regkey[i]) {
			                	    	 %>

																<option value="<%=regkey[i] %>" selected="selected" name="driver" id="driver"
																   onmouseup="closeDriverProfile()"><%= registrationValue[i]%></option>
																<%}else { %>
																<option value="<%=regkey[i] %>" name="driver" id="driver"
																	onclick="closeDriverProfile()"><%= registrationValue[i]%></option>
																<%} %>
																<%} %>
														</select></td>

														<td class="firstCol">User&nbsp;Id</td>

														<td>
															<%if(request.getAttribute("edit")!=null){ %> <input
															type="text" name="uid" id="uid" readonly
															value="<%= driverBO.getUid()  %>" /> <%}else{ %> <input
															type="text" name="uid" id="uid"
															value="<%= driverBO.getUid()  %>" onblur="clearErrorInfo()"/> <%} %>
														</td>

													</tr>

													<tr>
														<td class="firstCol">First&nbsp;Name</td>
														<td><input type="text" name="fname"
															value="<%= driverBO.getFname()  %>" /></td>
														<td class="firstCol">Last&nbsp;Name</td>
														<td><input type="text" name="lname"
															value="<%= driverBO.getLname()  %>" /></td>
													</tr>

													<tr>
														<td class="firstCol">Address1</td>
														<td><input type="text" name="add1"
															value="<%= driverBO.getAdd1()  %>" /></td>
														<td class="firstCol">Address2</td>
														<td><input type="text" name="add2"
															value="<%= driverBO.getAdd2()  %>" /></td>
													</tr>

													<tr>
														<td class="firstCol">City</td>
														<td><input type="text" name="city"
															value="<%= driverBO.getCity() %>" /></td>
														<td class="firstCol">State</td>
														<td><input type="text" name="state"
															value="<%= driverBO.getState()  %>" /></td>
													</tr>

													<tr>
														<td class="firstCol">Zip&nbsp;Code</td>
														<td><input type="text" name="zip"
															value="<%= driverBO.getZip()%>" /></td>
														<td class="firstCol">BankAct</td>
														<td><input type="text" name="acct"
															value="<%= driverBO.getAcct()%>" /></td>
													</tr>

													<tr>
														<td class="firstCol">Phone&nbsp;No</td>
														<td><input type="text" name="phone"
															value="<%= driverBO.getPhone()%>" /></td>
														
															<td class="firstCol">Driver&nbsp;Extension</td>
														<td><input type="text" name="drExt"
															value="<%= driverBO.getDriExten()  %>" /></td>
							
														<td class="firstCol" style="display: none">Car&nbsp;Make</td>
														<td style="display: none"><input type="text"
															name="cmake" value="<%= driverBO.getCmake()  %>" /></td>
													</tr>

													<tr>
														<td class="firstCol" style="display: none">Car&nbsp;Year</td>
														<td style="display: none"><input type="text"
															name="cyear" value="<%= driverBO.getCyear()  %>" /></td>
														<td class="firstCol" style="display: none">Car&nbsp;Model</td>
														<td style="display: none"><input type="text"
															name="cmodel" value="<%= driverBO.getCmodel()  %>" /></td>
													</tr>

													<tr>
														<td class="firstCol" style="display: none">Cab&nbsp;No</td>
														<td style="display: none"><input type="text"
															name="cabno" value="<%= driverBO.getCabno()  %>" /></td>
               
													</tr>

													<tr style="display: none;">
																				<td class="firstCol" style="display: none">Pass&nbsp;No</td>
														<td style="display: none"><input type="text" name="passno"
															value="<%= driverBO.getPassno()  %>" /></td>
													
														<td class="firstCol" style="display: none">Frequency&nbsp;Of&nbsp;Deposit</td>
														<td align="center" style="display: none">
															<div class="div2">
																<div class="div2In">
																	<div class="inputBXWrap wid130">
																		<select name="fdeposit" class="inputBX">
																			<%
							for(int count = 0;count<frequen.length;count++) {
							if(driverBO.getFdeposit() != "" && Integer.parseInt(driverBO.getFdeposit()) == count) {
					%>
																			<option value="<%= count  %>" selected="selected"><%= frequen[count] %></option>
																			<% 		} else {
					%>
																			<option value="<%= count  %>"><%= frequen[count] %></option>
																			<%
						}
					}
				%>
																		</select>
																	</div>
																</div>
															</div></td>
													</tr>

													<tr>
														<td class="firstCol">Status</td>
														<td align="center">
															<div class="div2">
																<div class="div2In">
																	<div class="inputBXWrap wid130">
																		<select name="status" id="status" class="inputBX">
																			<% System.out.println("DRIVER STATUS::"+driverBO.getStatus()); 
                															if(driverBO.getStatus().equalsIgnoreCase("1")) {%>
																			<option value="1" <%=driverBO.getStatus().equalsIgnoreCase("1")?"selected":"" %>><%="Active" %></option>
																			<option value="0" <%=driverBO.getStatus().equalsIgnoreCase("0")?"selected":"" %>><%="In Active" %></option>
																			<%}else { %>
																			<option value="1" <%=driverBO.getStatus().equalsIgnoreCase("1")?"selected":"" %>><%="Active" %></option>
																			<option value="0" <%=driverBO.getStatus().equalsIgnoreCase("0")?"selected":"" %>><%="In Active" %></option>
																			<%} %>
																		</select>
																	</div>
																</div>
															</div></td>
														<td class="firstCol">E-Mail&nbsp;Host&nbsp;Name</td>
														<td><input type="text" name="ehost"
															value="<%= driverBO.getEhostid()  %>" /></td>
												</tr>
													
													<tr><td class="firstCol">Social Security Number</td>
														<td><input type="text" name="soSecurityNo" id="soSecurityNo" value="<%=driverBO.getSocialSecurityNo()%>"/></td>
												
												</tr>
													
												</table></td>
										</tr>
									</table>
									<br></br>
									
									<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="driverChargTab">
										<tr>
											<td>
												<table id="Driver" width="540">
												<tr>
											<td>
												<table id="Driver" width="540">
												<tr><td class="firstCol">Driver License</td>
														<td ><input type="text" name="drLicense" id="drLicense" value="<%= driverBO.getDrLicense() %>" /></td>
														<td class="firstCol">Expiry date</td>
														<td ><input type="text" name="drExpiryDt" id="drExpiryDt" onfocus="showCalendarControl(this);" readonly="readonly"value="<%= driverBO.getDrLicenseExpiry()  %>"/></td></tr>
												<tr><td class="firstCol">Hack License</td>
														<td ><input type="text" name="hkLicense" id="hkLicense" value="<%= driverBO.getHkLicense()  %>"/></td>
														<td class="firstCol">Expiry date</td>
														<td ><input type="text" name="hkExpiryDt" id="hkExpiryDt" onfocus="showCalendarControl(this);" readonly="readonly" value="<%= driverBO.getHkLicenseExpiry()  %>"/></td>
														</tr>
																<%-- 		<%if(request.getAttribute("edit")!=null){ %> --%>
														<tr><td class="firstCol">Driver Rating</td>
														<td> <select name="driverRatings" id="driverRatings" >
                    
                      <option value="1" <%=driverBO.getDriverRatings()==1?"selected=selected":"" %> >1</option>
                      <option value="2" <%=driverBO.getDriverRatings()==2?"selected=selected":"" %> >2</option>
                      <option value="3" <%=driverBO.getDriverRatings()==3?"selected=selected":"" %> >3</option>
                      <option value="4" <%=driverBO.getDriverRatings()==4?"selected=selected":"" %> >4</option>
                      <option value="5" <%=driverBO.getDriverRatings()==5?"selected=selected":"" %> >5</option>
               
                        </select></td>
														
														</tr>
											<%-- 	<%} %> --%>
														<td class="firstCol">PaymentUserId</td>
														<td>
															<%if(request.getAttribute("edit")!=null){ %> <input
															type="text" name="payUserId" id="payUserId"
															value="<%=driverBO.getPayUserId() %>"></input> <%}else { %>
															<input type="text" name="payUserId" id="payUserId" value=""></input> <%} %>
														</td>
														<td class="firstCol">PaymentMerchant</td>
														<td>
															<%if(request.getAttribute("edit")!=null){ %> <input
															type="text" name="payMerchand" id="payMerchand"
															value="<%=driverBO.getPayMerchant() %>"></input> <%}else { %>
															<input type="text" name="payMerchand"id="payMerchand" value=""></input>
															<%} %>
														</td>
													</tr>
													<td>
													<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>
	<div id="multiFleet">
	<%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
	
%>
                       <%if(fleetList!=null && fleetList.size()>0){%>
                         <select name="fleet" id="fleet" >
                       <%if(fleetList!=null){
	for(int i=0;i<fleetList.size();i++){ %>
	<option value="<%=fleetList.get(i).getFleetNumber()%>" <%=(fleetList.get(i).getFleetNumber().equals(adminBo.getAssociateCode())?"selected=selected":"")%> ><%=fleetList.get(i).getFleetName() %></option>
<% }}%> 
                        </select>
                        <input type="button" name="fleet" id="fleet" value="Change Fleet" onclick="changeFleet()"/>
                       <%} %>
                       </div>
													</td>
													<tr>
														<td class="firstCol">PaymentPassword</td>
														<td><input type="password" name="payPassWord" id="payPassWord"
															value="<%=driverBO.getPayPassword()%>" /></td>
														<td class="firstCol">PaymentRe-Type&nbsp;Password</td>
														<td><input type="password" name="payrePassWord" id="payrePassWord"
															value="<%=driverBO.getPayRePassword() %>"></input></td>
													</tr>
														<%if(al_list!=null && !al_list.isEmpty()) {%>
														<td class="firstCol">Driver Profile <input
															type="hidden" name="profileSize"
															value="<%=al_list.size()/2 %>"></input>
														</td>
														<%for(int i=0,j=0;i<al_list.size();i=i+2,j++){ %>
														<td><input type="checkbox"
															<%
											 if(request.getParameter("chk"+j)!=null)
											 {
												 out.print("checked");
											 } else {
												 if(driverBO.getAl_list()!=null)
												 {
													 for(int k=0;k<driverBO.getAl_list().size();k++)
													 {
														 if(driverBO.getAl_list().get(k).toString().equals(al_list.get(i+1)))
														 {
															 out.print("checked");
															 break;
														 }
													 }
												 }
											 }
										
										%>
															name="chk<%=j %>" value="<%=al_list.get(i+1) %>" /><%=al_list.get(i) %>
														</td>
														<%}%>
														<%}%>
													</tr>
							<% if(session.getAttribute("user") != null) {  %>
							<%ArrayList al_q = (ArrayList)request.getAttribute("al_q");%>
								<td class="firstCol" style="width: 10%;">Zone</td><td style="width: 35%;"><select name='queueno' id='queueno'>
										<option value=''>Select</option>
										<%
										if(al_q!=null){
										for(int i=0;i<al_q.size();i=i+2){ 
									%>
										<option value='<%=al_q.get(i).toString() %>'
											<%=openBO.getQueueno().equals(al_q.get(i).toString())?"selected":"" %>><%=al_q.get(i+1).toString() %></option>
										<%
									}
									} %>
								</select> <% } else{  %>

									<div class="div2">
										<div class="div2In">
											<div class="inputBXWrap">
												<input type="hidden" name="queueno" id="queueno"
													value="<%=openBO.getQueueno() %>" />
											</div>
										</div>
									</div> <%} %>
									<%if(charges.size()>0){ %>
									<td class="firstCol" style="width: 10%;">Template</td><td><Select name="templateType">
	
		<%for(int i=0;i<charges.size();i++){%>
		
		<option value="<%=charges.get(i).getPayTemplateKey()%>" <%=charges.get(i).getPayTemplateKey()==driverBO.getTemplate()?"selected":"" %>  ><%=charges.get(i).getPayTemplateDesc() %></option>	
	<%} %></Select>
	</td><%} %>
	
	
									 </td>
														
												</table></td>
										</tr>
<table width="100%" border="0" cellspacing="0" cellpadding="0"
										class="driverChargTab">
								<tr>		
									<td colspan="8">
									<div style="margin-left: 0px;">
																<div class="btnBlue">
																	<div class="rht">
																		<input type="button" name="top3" id="top3"
																			value="Largest DriverIDs " class="lft" onclick="getLast3IDs()"></input>
																	</div>
																</div>
															</div>
															<div 
																style="margin-left: 300px;">
																<div class="btnBlue">
																	<div class="rht">
																		<%if(request.getAttribute("edit")!=null){ %>
																		<input type="button" name="Button" value="Update"
																			onclick="submitInfo();javascript:submit()"
																			class="lft"></input>
																		<%}   else if(request.getAttribute("edit")==null){    %>
																		<input type="button" name="button" value="Submit"
																			onclick="javascript:submit()" class="lft"></input>

																		<%} %>
																	</div>
																</div>
															</div>

														
															<div 
																style="margin-left: 550px;">
																<div class="btnBlue">
																	<div class="rht">
																		<%-- <input type="button" value="Get it"  class="lft" onclick="getUserAccess('<%=driverBO.getUid()%>','<%=driverBO.getRegistration() %>','<%=driverBO.getStatus()%>')"></input> --%>

																		<input type="submit" name="NextScreen" id="NextScreen"
																			value="NextScreen" class="lft"></input>
																	</div>
																	<div class="clrBth"></div>
																</div>
																<div class="clrBth"></div>
															</div></td></tr>
										
							<tr>
							
							<div class="wid60 marAuto padT10"
																style="margin-left: 120px;">
															<td class="firstCol">Password</td>
															<td><input type="password" name="password"
																id="password" value="" /></td>
															<td class="firstCol">RE-Type&nbsp;Password</td>
															<td><input type="password" name="repassword"
																id="repassword" value="" /></td>
														</div></tr>
													<tr>
														<td colspan="8">
														<div class="clrBth"></div>
														<div class="wid60 marAuto padT10"
																style="margin-left: 300px;">
									<div class="btnBlue">
										<div class="rht">
											<%if(request.getAttribute("edit")!=null){ %>
											<input type="button" align="middle" name="changePassword"
												id="changePassword" value="Change Password"
												onclick="javascript:submit()" class="lft"></input> <input
												type="hidden" name="changing" id="changing"
												value="changePassword" />
											<%} %>
											<div class="clrBth"></div>
										</div>
										<div class="clrBth"></div>
									</div></div></td>
												</tr></table>
												
<%if(request.getAttribute("edit")!=null){ %>
															<div id="docLoader">
	</div>
	
	 
            <table id="documentImages">
            	<tr>
            	</tr>
            	</table>
	</div>
																		<%} %>



									</table>

							<div id="driverIDReference" style="width:30%;height:10%;margin-left:60%;margin-top:-30%;position:fixed;" ></div>															</div>
			
									<!-- </td>


									</tr>


									</table>
 -->
								</div>
							</div>
							<div class="clrBth"></div>
							<table>
							</table>
							
							


							<div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
	</form>
</body>
</html>


