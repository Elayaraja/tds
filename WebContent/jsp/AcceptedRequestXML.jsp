<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.OpenRequestBO;"%>
<%
	ArrayList al_acceptRequestData = new ArrayList();	
	OpenRequestBO openBO;
	if(request.getAttribute("acceptrequestdata") != null) {
		al_acceptRequestData = (ArrayList) request.getAttribute("acceptrequestdata");
	}
%>
<%= "<xml version=\"1.0\" >"+
	"<server>"+
	"<Messages>"+
	"<Message>	"+
	"<MessageCode>"+
	"</MessageCode>"+
	"<MessageDesc>"+
	"</MessageDesc>"+
	"<MessageVariable>"+
	"</MessageVariable>"+
	"</Message>	"+
	"<Message>	"+
	"<MessageCode>"+
	"</MessageCode>"+
	"<MessageDesc>"+
	"</MessageDesc>"+
	"<MessageVariable>"+
	"</MessageVariable>"+
	"</Message>	"+
	"</Messages>"+
	"<AcceptedRequests>"
%>
	<%
		for(int counter=0;counter<al_acceptRequestData.size();counter++){
			openBO = (OpenRequestBO)al_acceptRequestData.get(counter);
			out.println("<AcceptedRequest>");
			out.println("<TripID>"+openBO.getTripid()+"</TripID>");
			out.println("<Driverid>"+openBO.getDriverid()+"</Driverid>");
			out.println("<Phone>"+openBO.getPhone()+"</Phone>");
			out.println("<Saddress1>"+openBO.getSadd1()+"</Saddress1>");
			out.println("<Saddress2>"+openBO.getSadd2()+"</Saddress2>");
			out.println("<Scity>"+openBO.getScity()+"</Scity>");
			out.println("<Sstate>"+openBO.getSstate()+"</Sstate>");
			out.println("<Szip>"+openBO.getSzip()+"</Szip>");
			out.println("<Eaddress1>"+openBO.getEadd1()+"</Eaddress1>");
			out.println("<Eaddress2>"+openBO.getEadd2()+"</Eaddress2>");			
			out.println("<Ecity>"+openBO.getEcity()+"</Ecity>");
			out.println("<Estate>"+openBO.getEstate()+"</Estate>");
			out.println("<Ezip>"+openBO.getEzip()+"</Ezip>");
			out.println("</AcceptedRequest>");
		}		
	%>
<%= "</AcceptedRequests></server>" %>