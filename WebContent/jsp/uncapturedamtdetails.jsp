<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.common.util.TDSConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.VoucherBO"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></link>
<script type="text/javascript" src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>TDS(Taxi Dispatch System)</title>
<style>
#foo {
font-weight:bold;
color:blue;
position:bottom;
}
h2{
font-family:italic;
color:grey;
}
</style>

<script type="text/javascript">
function removesel(){
 	showProcess();
	var checkvalue="";
		for(var j=0;j<Number(document.getElementById('size').value);j++) 
		if(document.getElementById("tripid"+j).checked)
		{
			//alert(document.getElementById("tripid"+j).value)
			checkvalue+=document.getElementById("tripid"+j).value+";";
		}
		//alert(checkvalue);
		document.getElementById("checkedval").value=checkvalue;
		return true;
	}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>

</head>

<body>
<form method="post" action="control" name="masterForm" onsubmit="removesel()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<input type="hidden" name="<%= TDSConstants.actionParam %>" id="action" value="<%= TDSConstants.registrationAction %>"/>
<input type="hidden" name="<%= TDSConstants.eventParam %>" id="event" value="unsettleddetailssubmit"/>

 
                <div>
<div>
                	<h2><center>UnCaptured Voucher Details</center></h2>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                    <tr>
                    <%StringBuffer error = (StringBuffer) request.getAttribute("error"); %>
					<td colspan="7">
					<div id="errorpage">
					<%= (error !=null && error.length()>0)?""+error:"" %>
					</div>
					</td>
					</tr>
					
					 <% ArrayList voucherdata=(ArrayList)request.getAttribute("voucherdata");
	 					if(voucherdata != null && voucherdata.size()>0){ 
					%>
					<tr>
					<td>
					<input type="hidden" name="size" id="size" value="<%=voucherdata.size() %>"/>
					</td>
					</tr>
                     <tr>
                        
                        <th width="15%" class="firstCol">S.No</th>
                        <th width="20%">Voucher No</th>
                        <th width="15%">Rider Name</th>
                        <th width="20%">Voucher Desc</th>
                        <th width="30%">Service Date</th>
                        <th width="30%">Driver Id</th>
                        <th width="30%">Trip Id</th>
                        <th width="30%">Amount</th>
                        <th width="30%">Tip</th>
                        <th width="30%">Final Amt</th>
                        <th width="30%">Payment Received</th>
                   
                   </tr>
                   
                   <%for(int count=0;count<voucherdata.size();count++){
					VoucherBO vbdata = (VoucherBO)voucherdata.get(count);
					if(count%2==0){%>
					
					<tr align="center">
					<td><%=count+1%></td>
					<td><%=vbdata.getVno() %></td>
					<td><%=vbdata.getVuser() %></td>
					<td><%=vbdata.getVdesc()  %></td>
					<td><%=vbdata.getFrom_date()%></td>
					<td><%=vbdata.getVdriver()%></td>
					<td><%=vbdata.getVtripid()%></td>
					<td><%=vbdata.getVcode()%></td>
					<td><%=vbdata.getTip()%></td>
					<td><%=vbdata.getVamount()%></td>
					<td>
						<input type="checkbox" name="tripid<%=count %>" id="tripid<%=count %>" <%=request.getParameter("tripid"+count)!=null?"selected":"" %> value="<%=vbdata .getVtripid()%>"/>
					</td>
					</tr>
					<%}else{ %>
					<tr align="center">
					<td style="Background-color:lightgreen"><%=count+1%></td>
					<td style="Background-color:lightgreen"><%=vbdata.getVno() %></td>
					<td style="Background-color:lightgreen"><%=vbdata.getVuser() %></td>
					<td style="Background-color:lightgreen"><%=vbdata.getVdesc()  %></td>
					<td style="Background-color:lightgreen"><%=vbdata.getFrom_date()%></td>
					<td style="Background-color:lightgreen"><%=vbdata.getVdriver()%></td>
					<td style="Background-color:lightgreen"><%=vbdata.getVtripid()%></td>
					<td style="Background-color:lightgreen"><%=vbdata.getVcode()%></td>
					<td style="Background-color:lightgreen"><%=vbdata.getTip()%></td>
					<td style="Background-color:lightgreen"><%=vbdata.getVamount()%></td>
					<td style="Background-color:lightgreen">
						<input type="checkbox" name="tripid<%=count %>" id="tripid<%=count %>" <%=request.getParameter("tripid"+count)!=null?"selected":"" %> value="<%=vbdata .getVtripid()%>"/>
					</td>
					</tr>
				<%}} %>
						
				<tr align="center" >
				<td  align="center" colspan="11" >
				<div class="wid60 marAuto padT10"/>
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="hidden" name="checkedval" id="checkedval" value=""/>
							<input type="submit" name="Submit" value="RECEIVED" class="lft"/>
		                    <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>
				</td>
				</tr>
				
				<%} %>
				</table>
				                                        
                </div>    
              </div>
            	
                <div ></div>
           
            <footer>Copyright &copy; 2010 Get A Cab</footer>
 
</form>
</body>
</html>
