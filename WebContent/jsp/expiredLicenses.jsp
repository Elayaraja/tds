<%@page import="com.tds.bean.docLib.DocBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page
	import="java.util.ArrayList,com.tds.tdsBO.LostandFoundBO,java.util.List,com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%! ArrayList<DocBean> expiredLicenses = new ArrayList<DocBean>(); %>
<% 
	if(request.getAttribute("docTypeData") != null) {
		expiredLicenses = (ArrayList) request.getAttribute("docTypeData");
	}
%>
<head>

<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>

<script type="text/javascript">


function addDrivers(row){
	var drivers= document.getElementById("drivers").value;
	if(document.getElementById("check_"+row).checked==true){
	if(drivers==""){
		$("#drivers").val(document.getElementById("check_"+row).value);
		//drivers= document.getElementById("check_"+row).value;
	}else{
		$("#drivers").val(drivers+";"+document.getElementById("check_"+row).value);
		//drivers= drivers+";"+document.getElementById("check_"+row).value;
	}
	}else{
		if(drivers.indexOf(";"+document.getElementById("check_"+row).value+";")!=-1){
			//var driver=document.getElementById("demo").innerHTML; 
			var n=drivers.replace(";"+document.getElementById("check_"+row).value+";",";");
			$("#drivers").val(n);
		}else if(drivers.indexOf(document.getElementById("check_"+row).value+";")!=-1){
			//var driver=document.getElementById("demo").innerHTML; 
			var n=drivers.replace(document.getElementById("check_"+row).value+";","");
			$("#drivers").val(n);
		}else if(drivers.indexOf(";"+document.getElementById("check_"+row).value)!=-1){
			//var driver=document.getElementById("demo").innerHTML; 
			var n=drivers.replace(";"+document.getElementById("check_"+row).value,"");
			$("#drivers").val(n);
		}else if(drivers.indexOf(document.getElementById("check_"+row).value)!=-1){
			//var driver=document.getElementById("demo").innerHTML; 
			document.getElementById("drivers").value="";
			$("#drivers").val("");
			/* var n=drivers.replace(document.getElementById("check_"+row).value,"");
			$("#drivers").val(n); */
		}
	}
}
function showMessageBox(){
	var driver= document.getElementById("drivers").value;
	if(driver!=""){
 		$("#messageBox").show("slow");
	}else{
		alert(" You Haven't select any driver");
	}
}
function send(){
	showProcess();
	var driver= document.getElementById("drivers").value;
	if(driver==""){
		alert(" You Haven't select any driver");
	}else{
	var message= document.getElementById("message").value;
	var s=confirm("Do you want to send this message \" "+message+"\" to "+driver);
	if(s==true){
		  var xmlhttp=null;
			if (window.XMLHttpRequest){
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		url='SystemSetupAjax?event=sendMessage&module=systemsetupView&message='+message+'&drivers='+driver;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("message").value="";
		$("#messageBox").hide("slow");

	}else{
		alert("Your message discarded");
		document.getElementById("message").value="";
		$("#messageBox").hide("slow");

	}
	}
}
function sendMessage(){
	//var driver= document.getElementById("drivers").value;
	var message= document.getElementById("message").value;
	var s=confirm("Do you want to send this message "+message+" to "+driver);
	if(s==true){
		  var xmlhttp=null;
			if (window.XMLHttpRequest)
			{
				xmlhttp = new XMLHttpRequest();
			} else {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		url='SystemSetupAjax?event=sendMessage&module=systemsetupView&message='+message;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		document.getElementById("message").value="";
		$("#messageBox").hide("slow");

	}else{
		alert("Your message discarded");
		document.getElementById("message").value="";
		$("#messageBox").hide("slow");

	}
	
	
}

//checked = false;
function checkedAll() {
	if (document.getElementById("checkall").checked == true) {
		checked = true;
	} else {
		checked = false;
	}
	for ( var i = 0; i < document.expiringDocuments.check.length; i++) {
		document.expiringDocuments.check[i].checked = checked;
		addDrivers(i);
	}
}
function checkSelectall(row) {
	
	if (document.getElementById("check_"+row).checked==true) {
		//checked = true;
		 var n = $("input:checked").length;
		if(n==document.expiringDocuments.check.length){
			document.getElementById("checkall").checked=true;
		}
	} else {
		document.getElementById("checkall").checked=false;
	}
	
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
	return true;
}
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>TDS(Taxi Dispatching System)</title>
</head>

<body>
	<input type="hidden" name="operation" value="2" />
				<br />
				<c class="nav-header"><center>Documents Expires in this week</center></c> 
				<br /><br />
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<form name="expiringDocuments" action="control" method="post">
								<input type="hidden" name="module" id="module" value="<%=request.getParameter("module") == null ? "" : request.getParameter("module")%>" />
								<input type="hidden" name="<%=TDSConstants.actionParam%>" value="<%=TDSConstants.requestAction%>"/> 
								<input type="hidden" name="<%=TDSConstants.eventParam%>"	value="reviewDocuments" />

										<table id="bodypage" width="540">
																			
											<thead>
													<tr>	<th>Document Name</th>
															<th>Document Type</th>
															<th>Expiry Date</th>
															<th>Driver Number</th>
															<th colspan="2">Select All:
															<input
																type='checkbox' name='checkall' id="checkall"
																onclick='checkedAll();'/></th>
														</tr>
														</thead>
														<tbody>
														<%
												if (expiredLicenses.size() > 0) {
															boolean colorLightGreen = true;
																String colorPattern;
															for (int count = 0; count < expiredLicenses.size(); count++) {
																	//	m_lostandFoundBO = (LostandFoundBO) m_lostandFoundList.get(count);
																	colorLightGreen = !colorLightGreen;
																	if (colorLightGreen) {
																		colorPattern = "style=\"background-color:lightsteelblue\"";
																	} else {
																		colorPattern = "";
																	}
														%>
														
														<tr>
															<td <%=colorPattern%>> <%=expiredLicenses.get(count).getDocName()%></td>
															<td <%=colorPattern%>> <%=expiredLicenses.get(count).getDocType()%></td>

															<td <%=colorPattern%>> <%=expiredLicenses.get(count).getDocExpiry()%></td>
															<td <%=colorPattern%>> <%=expiredLicenses.get(count).getDocUploadedBy()%></td>
															<td <%=colorPattern%>> 
															<input type="checkbox" name="check" id="check_<%=count%>"
																value="<%=expiredLicenses.get(count).getDocUploadedBy()%>"
																onclick="addDrivers(<%=count%>);checkSelectall(<%=count%>)" />
															</td>

															<%
																}
															%>
														</tr>
														<br />
														<br />
														<tr>
															<td colspan="5" align="center"><input type="hidden"
																name="drivers" id="drivers" value="" /> <input
																type="button" name="sendMessage" id="sendMessage"
																value="sendMessage" onclick="showMessageBox()" /></td>
														</tr>
														<br />
														<table id="messageBox" style="display: none;" width="100%">
														<tr>
														<td colspan="2">Subject:								
														</td><td colspan="3">
														<input type="text" name="subject" value=""/>
														</td>
														</tr>
														<tr >
																<td colspan="2">Message:</td>
															<td colspan="3">
															<textarea name="message" id="message" rows="4" cols="30"></textarea> </td>
															</tr>
															<tr>
															<td>
															<input type="button" name="button" value="submit message"
																onclick="send()" /></td>
														</tr>
														
														</table>
														<%
														}
														else{%> 
															<tr><TD colspan=3 align="center" >NO RECORD FOUND</TD></TR>
														<%}															
														%> 
														</tbody>
													</table>
													</form>
													</td>
											</tr>

										</table>
																
										<c class="footerDV">Copyright &nbsp; 2010 Get A Cab</div>
</body>
</html>

