<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.OpenRequestBO" %>
<%@page import="java.util.ArrayList"%>

<%ArrayList al_list = new  ArrayList();
if(request.getAttribute("landmarkDetails")!=null ) {
	al_list = (ArrayList)request.getAttribute("landmarkDetails");
}
%>
<%if(al_list!=null && al_list.size()>1) {%>
	    <NumRows>NumRows=2</NumRows>###
<%} else if (al_list!=null && al_list.size()==1){ %>
	    <NumRows>NumRows=1</NumRows>###
<!-- <input type="hidden" name="tableSize" id="tableSize" value="1"/>
 --><%} %>
 <input type="hidden"  name='landMarkSelect' id='landMarkSelect' value="<%=request.getAttribute("type") %>"></input>
<%if(al_list!=null && !al_list.isEmpty()) {%> 

<form name="land" >
 <table align="center" border="1"   width="100%"    >
 	<tr>
 		<td width="8%" bgcolor="#6495ED">Select</td>
 		<td width="10%" bgcolor="#6495ED">LAND MARK</td>
 		<td width="17%" bgcolor="#6495ED">ADD1</td>
 		<td width="17%" bgcolor="#6495ED">ADD2</td>
 		<td width="10%" bgcolor="#6495ED">CITY</td>
 		<td width="10%" bgcolor="#6495ED">STATE</td>
 		<td width="10%" bgcolor="#6495ED">ZIP</td>
 		<td width="10%" bgcolor="#6495ED">LATITUDE</td>
 		<td width="8%" bgcolor="#6495ED">LONGITUDE</td>
 	</tr>
 	<%for(int i=0;i<al_list.size();i++) { 
 		OpenRequestBO  land = (OpenRequestBO)al_list.get(i);
 	%> 
 	
 	<%if(i%2 == 0) { %>
 	
 	
 	<tr >
 		<td bgcolor="#F0F8FF" ><input  type="radio"   name="landSelect"  id="landSelect<%=i %>"  onclick="landValues(<%=i%>)" onkeyup="KeyCheck(event,'landSelect<%=i %>','landSelect<%=i %>')" >  </td>
 		<td bgcolor="#F0F8FF" ><%=land.getSlandmark() %><input type="hidden"  id="landmarkS<%=i %>"  name="landmarkS" value="<%=land.getSlandmark()%>" > </td>
 		<td bgcolor="#F0F8FF" ><%=land.getSadd1() %>  <input   type="hidden"  id="ladd1<%=i %>"   name="ladd1"   value="<%=land.getSadd1() %>">  </td>
 		<td bgcolor="#F0F8FF" ><%=land.getSadd2() %>  <input   type="hidden"  id="ladd2<%=i %>"   name="ladd2" value="<%=land.getSadd2() %>"  > </td>
 		<td bgcolor="#F0F8FF" ><%=land.getScity() %>  <input   type="hidden"  id="lcity<%=i %>"   name="lcity"  value="<%=land.getScity() %>"  >   </td>
 		<td bgcolor="#F0F8FF" ><%=land.getSstate() %> <input   type="hidden"  id="lstate<%=i %>"  name="lstate"   value="<%=land.getSstate() %>">    </td>
 		<td bgcolor="#F0F8FF" ><%=land.getSzip() %>  <input   type="hidden"  id="lzip<%=i %>"  name="lzip"   value="<%=land.getSzip() %>"></td>
 		<td bgcolor="#F0F8FF" ><%=land.getSlat() %> <input   type="hidden"  id="llatide<%=i %>"  name="llatide"   value="<%=land.getSlat() %>"></td>
 		<td bgcolor="#F0F8FF" ><%=land.getSlong() %> <input   type="hidden"  id="llogtude<%=i %>"  name="llogtude"   value="<%=land.getSlong() %>"></td> 	
 		<td bgcolor="#F0F8FF" ><%=land.getComments()==null?"":land.getComments() %> <input   type="hidden"  id="lcomments<%=i %>"  name="lcomments"   value="<%=land.getComments()==null?"":land.getComments() %>"></td> 	
		<input   type="hidden"  id="landmarkKey<%=i%>"  name="landmarkKey<%=i%>"   value="<%=land.getLandMarkKey() %>"> 	
 	</tr>
 	<%} else {%>
 	
 
 	
 	<tr>
 		<td  style="background-color:lightgreen"><input  type="radio"   name="landSelect"  id="landSelect<%=i %>"  onclick="landValues(<%=i %>)"  onkeyup="KeyCheck(event,'landSelect<%=i %>','landSelect<%=i %>')" >  </td>
 		<td  style="background-color:lightgreen"><%=land.getSlandmark() %><input type="hidden"  id="landmarkS<%=i %>"  name="landmarkS" value="<%=land.getSlandmark()%>" > </td>
 		<td  style="background-color:lightgreen"><%=land.getSadd1() %>  <input   type="hidden"  id="ladd1<%=i %>"   name="ladd1"   value="<%=land.getSadd1() %>">  </td>
 		<td  style="background-color:lightgreen"><%=land.getSadd2() %>  <input   type="hidden"  id="ladd2<%=i %>"   name="ladd2" value="<%=land.getSadd2() %>"  > </td>
 		<td  style="background-color:lightgreen"><%=land.getScity() %>  <input   type="hidden"  id="lcity<%=i %>"   name="lcity"  value="<%=land.getScity() %>"  >   </td>
 		<td  style="background-color:lightgreen"><%=land.getSstate() %> <input   type="hidden"  id="lstate<%=i %>"  name="lstate"   value="<%=land.getSstate() %>">    </td>
 		<td  style="background-color:lightgreen"><%=land.getSzip() %>  <input   type="hidden"  id="lzip<%=i %>"  name="lzip"   value="<%=land.getSzip() %>"></td>
 		<td  style="background-color:lightgreen"><%=land.getSlat() %> <input   type="hidden"  id="llatide<%=i %>"  name="llatide"   value="<%=land.getSlat() %>"></td>
 		<td  style="background-color:lightgreen"><%=land.getSlong() %> <input   type="hidden"  id="llogtude<%=i %>"  name="llogtude"   value="<%=land.getSlong() %>"></td> 	
 		<td  style="background-color:lightgreen"><%=land.getComments()==null?"":land.getComments() %> <input   type="hidden"  id="lcomments<%=i %>"  name="lcomments"   value="<%=land.getComments()==null?"":land.getComments() %>"></td> 	
		<input   type="hidden"  id="landmarkKey<%=i%>"  name="landmarkKey<%=i%>"   value="<%=land.getLandMarkKey() %>"> 	
 	</tr>
 	
 	<%} %>
 	<%} %>
 	
 	
 	
 	
 	<!--<td> if use a iframe or popup enable the close follwing row  </td>
 	
 	--><!--<tr   align="center" >
 		<td colspan="9"  align="center" > <input type="submit"   value="Close"   id="close"   name="close"  onclick="windowClose()" > </td>
 	</tr>
 -->
 
 
 </table>
 
</form>
<% } %> 
