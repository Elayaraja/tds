var amount = 0;
var total = 0;
var initialTip = 0;
var arrMarkerDriver = [];
var latitude;
var longitude;
var accuracy;
var map;
var geocoder;
var markerDriver;
//var markerAll;
var markerjob;
var mins, secs, TimerRunning, TimerID, TimerID1, thisJobIsRunning, thisJobIsRunningForDriver;
var refreshingFunctionIsRunning=false;


function rememberMe() {
	if (localStorage.loginCheck == "loggedIn") {
		return;
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ="/TDS/CustomerMobileAction?event=clientLogin&newEntry=YES";
	xmlhttp.open("GET", url, false);
	xmlhttp.send();
	var text = xmlhttp.responseText;
	if (text == "1") {
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		document.getElementById("newFooterMenu").disabled = false;
	} else {
		$(".Loggedout").show();
		$(".LoggedIn").hide();
		document.getElementById("newFooterMenu").disabled = true;
		$("#addressMain").hide();
		$("#pickUp").hide();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
	}
}

function backToHome() {
	$(window).attr('location', '#home');
	$.mobile.changePage("#home");
	if (localStorage.loginCheck == "loggedIn") {
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		document.getElementById("newFooterMenu").disabled = false;
		if ($('#navlist').css('display') == 'none') {
			Android.homePage("");
		} else {
			$("#customerImage1").hide();
		}
	} else {
		$(".Loggedout").show();
		$(".LoggedIn").hide();
		document.getElementById("newFooterMenu").disabled = true;
		$("#addressMain").hide();
		$("#pickUp").hide();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
	}
}

function bookTrip() {
	var sessionId=document.getElementById("sessionTemp").value;
	if(sessionId=="" || sessionId=="undefined" || document.getElementById("sessionTemp").value.length <10){
		setMessage("Please login to continue");
		dialogShow();
		return;
	}
	$(".ui-loader").show();
	if (document.getElementById("sDate").value != ""
			&& document.getElementById("time").value == "") {
		alert("Enter a 4 digit Time Value");
		return;
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var repeatJob="";
	var driverList=document.getElementById("driverProfile").value;
	var vehicleList=document.getElementById("vehicleProfile").value;
	if(typeof driverList != "" && driverList!=null){
		 repeatJob=repeatJob+'&drprofileSize='+driverList;	
		 for (var i=0; i<driverList;i++){
			 if(document.getElementById('dchk'+i).checked==true){
				 repeatJob=repeatJob+'&dchk'+i+'='+document.getElementById('dchk'+i).value;
			 }
		 }
	 }
	 if(typeof vehicleList != "" &&vehicleList!=null){
		 repeatJob=repeatJob+'&vprofileSize='+vehicleList;	
		 for (var i=0;i<vehicleList;i++){
			 if(document.getElementById('vchk'+i).checked==true){
				 repeatJob=repeatJob+'&vchk'+i+'='+document.getElementById('vchk'+i).value;
			 }
		 }
	 }
	var url ="/TDS/CustomerMobileAction?event=openRequest&sLatitude="
			+ document.getElementById("sLatitude").value + "&sLongitude="
			+ document.getElementById("sLongitude").value + "&userName="
			+ document.getElementById("userName").value + "&sAddress1="
			+ document.getElementById("sAddress1").value + "&phoneNumber="
			+ document.getElementById("phoneNumber").value + "&ccode="
			+ document.getElementById('ccode').value + "&sDate="
			+ document.getElementById("sDate").value + "&time="
			+ document.getElementById("time").value + "&submit=YES&eLatitude="
			+ +document.getElementById("eLatitude").value + "&eLongitude="
			+ +document.getElementById("eLongitude").value + "&eAddress1="
			+ document.getElementById("eAddress1").value+repeatJob;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != "" && text != null) {
		localStorage.tripId = text;
		localStorage.address = document.getElementById("sAddress1").value;
	}
	document.getElementById("sDate").value = "";
	document.getElementById("time").value = "";
	document.getElementById("sLatitude").value = "";
	document.getElementById("sLongitude").value = "";
	document.getElementById("eLatitude").value = "";
	document.getElementById("eLongitude").value = "";
	document.getElementById("sAddress1").value = "";
	document.getElementById("eAddress1").value = "";
	document.getElementById("phoneNumber").value = "";
	if(typeof driverList != "" && driverList!=null){
		 for (var i=0; i<driverList;i++){
			 document.getElementById('dchk'+i).checked=false;
		 }
	 }
	 if(typeof vehicleList != "" &&vehicleList!=null){
		 for (var i=0;i<vehicleList;i++){
			document.getElementById('vchk'+i).checked=false;
		 }
	 }
	document.getElementById("jobStatus").value="4";
	$.mobile.changePage("#home");
	if (localStorage.loginCheck == "loggedIn") {
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		document.getElementById("newFooterMenu").disabled = false;
		Android.homePage("");
		InitTCIL();
	} else {
		$("#popupBasic").html("Sorry your job is not booked,Please try again !!");
		$("#popupBasic").popup("open");
		$(".Loggedout").show();
		$(".LoggedIn").hide();
		document.getElementById("newFooterMenu").disabled = true;
		$("#addressMain").hide();
		$("#pickUp").hide();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
	}
	$("#addressMain").hide();
	$("#pickUp").hide();
	$("#dropOff").hide();
	$("#dropOffFinal").hide();
	$(".ui-loader").hide();
	if (text != "" && text != null) {
		var msg = "";
		if (parseInt(text) != 'NaN') {
			msg = "Trip was booked !TripID:" + text;
			localStorage.tripId = text;
			$("#addressMain").hide();
			$("#pickUp").hide();
			$("#dropOff").hide();
			$("#dropOffFinal").hide();
		} else {
			msg = "Trip not Booked";
		}
		setMessage(msg);
		dialogShow();
		refreshingFunctionIsRunning=false;
	}
	$("#specialFLags").hide();
}
function totalling() {
	var tip1 = document.getElementById("tip").value;
	total = parseInt(amount) + parseInt(tip1) - parseInt(initialTip);
	document.getElementById("totalAmount").innerHTML = total + ".00";
}
function showCards() {
	$.mobile.showPageLoadingMsg();
	showCards1();
}
function hideDetails() {
	$("#jobPopUp").hide();
}
function showCards1() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ="/TDS/CustomerMobileAction?event=showCards";
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	$("#existingCards").show();
	document.getElementById("existingCardsTbody").innerHTML = "";
	var obj = "{\"cardArray\":" + text + "}";
	var jsonObj = JSON.parse(obj.toString());
	$("#existingCardsTbody").append("<tr>");
	$("#existingCardsTbody").append("<td> Card Number");
	$("#existingCardsTbody").append("</td>");
	$("#existingCardsTbody").append("</tr>");
	for (var i = 0; i < jsonObj.cardArray.length; i++) {
		$("#existingCardsTbody").append("<tr>");
		$("#existingCardsTbody").append(
				"<td> <input type=\"radio\" name=\"cards\" id=\"cards\" value=\""
						+ jsonObj.cardArray[i].getNumber + "\">"
						+ jsonObj.cardArray[i].getCard + "</input>" + "</td>");
		$("#existingCardsTbody").append("</tr>");
	}
	$("#existingCardsTbody").append("</tr>");
	$("#existingCardsTbody").append("<tr>");
	$("#existingCardsTbody").append(
			"<td> <input type=\"button\" name=\"pay\" id=\"pay\" value=\"pay\">"
					+ "</td>");
	$("#existingCardsTbody").append("</tr>");
	$.mobile.hidePageLoadingMsg();
}
function test(tripId) {
	document.getElementById("tripId").value = tripId;
	$.mobile.changePage("#paymentScreen");
}
function showPaymentTable() {
	$("#cardDetails").show();
}
function cancelTrip(tripId) {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (tripId == '') {
		tripId = localStorage.tripId;
		$("#jobConfirmation").hide();
//		$("#pickUp").show();
	}
	var url ="/TDS/CustomerMobileAction?event=cancelTrip&tripId=" + tripId
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		var msg = "Your Trip is Cancelled";
		setMessage(msg);
		dialogShow();
		$("#addressMain").show();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
		$("#pickUp").show();
		document.getElementById("jobLati").value = "";
		document.getElementById("jobLongi").value = "";
		loadLocation();
		localStorage.tripId = "";
		document.getElementById("jobStatus").value = "50";
	}
}
function hideCancellation() {
	$("#jobConfirmation").hide();
}
function deleteAddress(x, key, switches) {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ="/TDS/CustomerMobileAction?event=deleteFavAddress&addKey=" + key;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1 && switches == "") {
		setMessage("Deleted Succesfully");
		dialogShow();
	} else {
		document.getElementById('favAddressTbody').innerHTML = "";
		var url ="/TDS/CustomerMobileAction?event=favouriteAddresses&responseType=HTML5";
		xmlhttp.open("GET", url, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
				+ localStorage.sessionId);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"favAddressD\":" + text + "}";
		jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.favAddressD.length; i++) {
			$("#favAddressTbody").append("<tr>");
			$("#favAddressTbody").append(
					"<td>" + jsonObj.favAddressD[i].addKey + "</td>");
			$("#favAddressTbody").append(
					"<td>" + jsonObj.favAddressD[i].address + "</td>");
			$("#favAddressTbody").append(
					"<td>" + jsonObj.favAddressD[i].tagAdd + "</td>");
			$("#favAddressTbody")
					.append(
							"<td><input type=\"button\" id=\"deleteFavAdd\" value=\"Delete\" onclick=\"deleteAddress(this,"
									+ jsonObj.favAddressD[i].addKey
									+ ",1);\"/></td>");
			$("#favAddressTbody").append("</tr>");
		}
	}
}
function payFare() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ="/TDS/CustomerServiceAjax?event=paymentFromCreditCard&tripId="
			+ document.getElementById("tripId").value + "amount="
			+ document.getElementById("totalAmount").innerHTML;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		var url1 = "/TDS/CustomerMobileAction?event=payFare";
		xmlhttp.open("GET", url1, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
				+ localStorage.sessionId);
		xmlhttp.send(null);
		var text1 = xmlhttp.responseText;
		if (text1 > 1) {
			alert("payment Done");
		}
	}
}
$('#home').live("pageshow", function() {
	loadLocation();
});
$(document).delegate('#home', 'pageinit', function() {
	$("#popupBasic").popup();

	$("#popupLogin").bind({
		popupafteropen : function(event, ui) {
			setTimeout(function() {
				$("#popupLogin").popup("close");
			}, 1000);

		}
	});
	$("#popupBasic").bind({
		popupafteropen : function(event, ui) {
			setTimeout(function() {
				$("#popupBasic").popup("close");
			}, 1000);
		}
	});
	if (localStorage.loginCheck == "loggedIn") {
		loadLocation();
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		document.getElementById("newFooterMenu").disabled = false;
		Android.homePage("");
	} else {
		loadLocation();
		$(".Loggedout").show();
		$(".LoggedIn").hide();
		document.getElementById("newFooterMenu").disabled = true;
	}
});
$(document).delegate(
				'#paymentScreen',
				'pageinit',
				function() {
					Android.homePage("");
					document.getElementById("paymentTbody").innerHTML = "";
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					var url ="/TDS/CustomerMobileAction?event=charges&tripId="
							+ document.getElementById("tripId").value;
					xmlhttp.open("GET", url, false);
					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);
					xmlhttp.send(null);
					var text = xmlhttp.responseText;
					if (text != "") {
						var obj = "{\"chargesArray\":" + text + "}";
						var jsonObj = JSON.parse(obj.toString());
						for (var i = 0; i < jsonObj.chargesArray.length; i++) {
							amount = parseInt(total)
									+ parseInt(jsonObj.chargesArray[i].getAmount);
							total = amount;
							$("#paymentTbody").append("<tr>");
							if ((jsonObj.chargesArray[i].getChargeType) == 0) {
								initialTip = jsonObj.chargesArray[i].getAmount;
								$("#paymentTbody").append(
										"<td>" + "Tip" + "</td>");
								$("#paymentTbody")
										.append(
												"<td> <input type=\"text\" name=\"tip\" id=\"tip\" value=\""
														+ jsonObj.chargesArray[i].getAmount
														+ "\" onblur=\"totalling()\">"
														+ "</td>");
							} else {
								$("#paymentTbody")
										.append(
												"<td>"
														+ jsonObj.chargesArray[i].getType
														+ "</td>");
								$("#paymentTbody")
										.append(
												"<td>"
														+ jsonObj.chargesArray[i].getAmount
														+ "</td>");
							}

						}
						$("#paymentTbody").append("<tr>");
						$("#paymentTbody").append("<td> Total </td>");
						$("#paymentTbody")
								.append(
										"<td id=\"totalAmount\">" + amount
												+ ".00</td>");
						$("#paymentTbody").append("</tr>");
						$("#paymentTbody").append("<tr>");
						$("#paymentTbody")
								.append(
										"<td> <input type=\"button\" name=\"Paymentbutton\" id=\"Paymentbutton\" value=\"Pay\" onclick=\"showPaymentTable()\"");
						$("#paymentTbody").append("</td>");
						$("#paymentTbody")
								.append(
										"<td> <input type=\"button\" name=\"payFromExistingCards\" id=\"payFromExistingCards\" value=\"Existing Cards\" onclick=\"showCards()\"");
						$("#paymentTbody").append("</td>");
						$("#paymentTbody").append("</tr>");
					} else {
						loadLocation();
					}
				});
$(document).delegate(
				'#favAddressDetails',
				'pageinit',
				function() {
					Android.homePage("");
					document.getElementById("favAddressTbody").innerHTML = "";
					var url ="/TDS/CustomerMobileAction?event=favouriteAddresses&HTML5=yes";
					var xmlhttp = null;
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.open("GET", url, false);

					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);
					xmlhttp.send(null);
					var text = xmlhttp.responseText;

					var obj = "{\"favAddressD\":" + text + "}";
					jsonObj = JSON.parse(obj.toString());
					for (var i = 0; i < jsonObj.favAddressD.length; i++) {
						$("#favAddressTbody").append("<tr>");
						$("#favAddressTbody").append(
								"<td>" + jsonObj.favAddressD[i].addKey
										+ "</td>");
						$("#favAddressTbody").append(
								"<td>" + jsonObj.favAddressD[i].address
										+ "</td>");
						$("#favAddressTbody").append(
								"<td>" + jsonObj.favAddressD[i].tagAdd
										+ "</td>");
						$("#favAddressTbody")
								.append(
										"<td><input type=\"button\" id=\"deleteFavAdd\" value=\"Delete\" onclick=\"deleteAddress(this,"
												+ jsonObj.favAddressD[i].addKey
												+ ",1);\"/></td>");
						$("#favAddressTbody").append("</tr>");
					}
				});
$(document).delegate(
				'#TripsOnBoard',
				'pageinit',
				function() {
					Android.homePage("");
					document.getElementById("tripHistoryTbody").innerHTML = "";
					if (localStorage.loginCheck == "loggedIn") {
						var url ="/TDS/CustomerMobileAction?event=tripHistory&responseType=HTML5";
						var xmlhttp = null;
						if (window.XMLHttpRequest) {
							xmlhttp = new XMLHttpRequest();
						} else {
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						}
						xmlhttp.open("GET", url, false);
						xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
								+ localStorage.sessionId);
						xmlhttp.send(null);
						var textValue = xmlhttp.responseText;
						if (textValue != "") {
							var obj = "{\"tripHistory\":" + textValue + "}";
							jsonObj = JSON.parse(obj.toString());
							for (var i = 0; i < jsonObj.tripHistory.length; i++) {
								//alert("lat:"+jsonObj.tripHistory[i].stlat+"+lon:"+jsonObj.tripHistory[i].stlon);
								$("#tripHistoryTbody").append("<tr>");
								$("#tripHistoryTbody").append("<td  style=\"background-color: lightgrey\">" + jsonObj.tripHistory[i].getName+ "</td>");
								$("#tripHistoryTbody").append("<td style=\"background-color: lightgrey\">" + jsonObj.tripHistory[i].getPhoneNumber+ "</td>");
								$("#tripHistoryTbody").append("<td style=\"background-color: lightgrey\">" + jsonObj.tripHistory[i].getAddress+ "</td>");
								$("#tripHistoryTbody").append("<td style=\"background-color: lightgrey\"><input type=\"button\" name=\"reBook\" id=\"reBook\" value=\"Re-Book\" style=\"color:#000080\" onclick=\"fixValueshistory('"
										+ jsonObj.tripHistory[i].getAddress
										+ "','"
										+ jsonObj.tripHistory[i].stlat
										+ "','"
										+ jsonObj.tripHistory[i].stlon
										+ "','"
										+ jsonObj.tripHistory[i].getPhoneNumber
										+ "');\"></td>");
								$("#tripHistoryTbody").append("</tr>");
							}
						}
					}else{
						var msg = "Please Log In.";
						setMessage(msg);
						dialogShow();
					}
				});
$(document).delegate(
				'#reserveTrips',
				'pageinit',
				function() {
					Android.homePage("");
					document.getElementById("reservedTripsTbody").innerHTML = "";
					var url = "/TDS/CustomerMobileAction?event=tripSummary";
					var xmlhttp = null;
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.open("GET", url, false);
					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);
					xmlhttp.send(null);
					var textSum = xmlhttp.responseText;

					if (textSum != "") {
						var obj = "{\"tripSummary\":" + textSum + "}";
						jsonObj = JSON.parse(obj.toString());
						for (var i = 0; i < jsonObj.tripSummary.length; i++) {

							$("#reservedTripsTbody").append("<tr>");
							$("#reservedTripsTbody").append(
									"<td>" + jsonObj.tripSummary[i].getName
											+ "</td>");
							$("#reservedTripsTbody").append(
									"<td >" + jsonObj.tripSummary[i].getTripId
											+ "</td>");
							$("#reservedTripsTbody").append(
									"<td>" + jsonObj.tripSummary[i].getAddress
											+ "</td>");
							$("#reservedTripsTbody")
									.append(
											"<td>"
													+ jsonObj.tripSummary[i].getPhoneNumber
													+ "</td>");
							$("#reservedTripsTbody").append(
									"<td>" + jsonObj.tripSummary[i].getCity
											+ "</td>");
							$("#reservedTripsTbody")
									.append(
											"<td> <input type=\"button\" name=\"cancelTrip\" id=\"cancelTrip\" value=\"Cancel\" onClick=\"cancelTrip("
													+ jsonObj.tripSummary[i].getTripId
													+ ");\" /></td>");
							$("#reservedTripsTbody").append("</tr>");
						}
					}
				});
$(document)
		.delegate(
				'#accountSummary',
				'pageinit',
				function() {
					document.getElementById("accountSummaryTbody").innerHTML = "";
					var url ="/TDS/CustomerMobileAction?event=clientAccountSummary&responseType=HTML5";
					var xmlhttp = null;
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.open("GET", url, false);
					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);
					xmlhttp.send(null);
					var text = xmlhttp.responseText;

					if (text != "") {
						var obj = "{\"accountSum\":" + text + "}";
						jsonObj = JSON.parse(obj.toString());

						for (var i = 0; i < jsonObj.accountSum.length; i++) {
							$("#accountSummaryTbody").append("<tr>");
							$("#accountSummaryTbody")
									.append(
											"<td>"
													+ jsonObj.accountSum[i].getPaymentType
													+ "</td>");
							$("#accountSummaryTbody")
									.append(
											"<td><a href='' data-rel='external'>"
													+ jsonObj.accountSum[i].getVoucherNumber
													+ "</td>");
							$("#accountSummaryTbody")
									.append(
											"<td>"
													+ jsonObj.accountSum[i].getVoucherExpiryDate
													+ "</td>");
							$("#accountSummaryTbody")
									.append(
											"<td>"
													+ jsonObj.accountSum[i].getCardNumber
													+ "</td>");
							$("#accountSummaryTbody")
									.append(
											"<td>"
													+ jsonObj.accountSum[i].getCardExpiryDate
													+ "</td>");
							$("#accountSummaryTbody").append("</tr>");
						}
					}
				});
function registerUser() {
	var userId = document.getElementById("userIdForR").value;
	var passWord = document.getElementById("passwordForR").value;
	var userName = document.getElementById("userNameForR").value;
	var email = document.getElementById("emailAddressForR").value;
	var phNo = document.getElementById("phoneNumberRegistration").value;
	var url ="/TDS/CustomerMobileAction?event=clientRegistration&responseType=HTML5&ccode="
			+ document.getElementById('ccode').value + "&os=G&userId=" + userId
			+ "&password=" + passWord + "&userName=" + userName + "&emailId="
			+ email + "&phoneNumber=" + phNo + "&registerButton=yes";
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == "Registered") {
		$.mobile.changePage("#login_page1");
		$(
				"<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>Registered Successfully,Now You can login!!</h1></div>")
				.css({
					"display" : "block",
					"opacity" : 0.96,
					"top" : $(window).height() / 2,
					"align" : "center"
				}).appendTo($.mobile.pageContainer).delay(800).fadeOut(4000,
						function() {
							$(this).remove();
						});
	} else {
		$(
				"<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>Not Registered, Try Again!!</h1></div>")
				.css({
					"display" : "block",
					"opacity" : 0.96,
					"top" : $(window).height() / 2,
					"align" : "center"
				}).appendTo($.mobile.pageContainer).delay(800).fadeOut(4000,
						function() {
							$(this).remove();
						});
	}
}
function accountRegistrationForUser() {
	var userId = "";
	var paymentType = document.getElementById("paymentTypeForAcctReg").value;
	var userName = document.getElementById("userNameForAcctReg").value;
	var cardNumber = document.getElementById("cardNumberForAcctReg").value;
	var voucherNumber = document.getElementById("voucherNumberForAcctReg").value;
	var cardExpiryDate = document.getElementById("cardExpiryDateForAcctReg").value;
	var url ="/TDS/CustomerMobileAction?event=clientAccountRegistration&responseType=HTML5&ccode="
			+ document.getElementById('ccode').value + "&os=G&userName="
			+ userName + "&paymentType=" + paymentType + "&cardNumber="
			+ cardNumber + "&voucherNumber=" + voucherNumber
			+ "&cardExpiryDate=" + cardExpiryDate
			+ "&registerButton=yes&mode=1";
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	$(window).attr('location', '#home');
	$.mobile.changePage("#home");
	if (localStorage.loginCheck == "loggedIn") {
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		document.getElementById("newFooterMenu").disabled = false;
		Android.homePage("");
	} else {
		$(".Loggedout").show();
		$(".LoggedIn").hide();
		$("#addressMain").hide();
		$("#pickUp").hide();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
		document.getElementById("newFooterMenu").disabled = true;
	}
}
function showNavTCIL() {
	if (localStorage.loginCheck == 'loggedIn') {
		if ($('#navlist').css('display') == 'none') {
			$("#customerImage1").hide();
			$("#navlist").show();
			$("#showingNav").val("1");
		} else {
			$("#customerImage1").show();
			$("#navlist").hide();
			$("#showingNav").val("0");
		}
	}
}
function logout() {
	var url ="/TDS/CustomerMobileAction?event=clientLogin&responseType=HTML5&submit=YES&ccode="
			+ document.getElementById('ccode').value + "&os="
			+ document.getElementById('os').value + "&userId="
			+ localStorage.userId;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == "loggedOut") {
		document.getElementById("jobStatus").value="";
		document.getElementById("sessionTemp").value="";
		hideMarkers();
		$(".Loggedout").show();
		$(".LoggedIn").hide();
		$("#pickUp").hide();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
		$("#addressMain").hide();
		document.getElementById("newFooterMenu").disabled = true;
		document.getElementById("internalFlag").innerHTML="";
		localStorage.loginCheck = text;
		localStorage.userPermanentName = "";
		localStorage.userEmailId = "";
		localStorage.userId = "";
		localStorage.phoneNo = "";
		localStorage.tripId = "";
		document.getElementById("userName").value = "";
		document.getElementById("phoneNumber").value = "";
		$('#navlist').hide();
		StopTimer();
	}
	document.getElementById("loadFlagValue").value="0";
	//cancelCreate();
}
function loadSettings() {
	$('#ccode').val(localStorage.ccode);
}
function saveSettings() {
	localStorage.ccode = $('#ccode').val();
}
function setMessage(message) {
	document.getElementById("messageTo").innerHTML = message;
}
function showFullDetails() {
	if (localStorage.loginCheck == "loggedIn") {
		var status=document.getElementById("popupBasic").innerHTML;
		if(status=="" || localStorage.tripId==""){
			document.getElementById("jobPopupMessage").innerHTML = "<font color='red' style='font-weight: bolder;'>Currently No Trips For You</font>";
			$("#cancelJobButton").hide();
		}else {
			document.getElementById("jobPopupMessage").innerHTML = "<font color='red' style='font-weight: bolder;'>"+status+""
			+"Your Trip Starts "
			+ "from " + localStorage.address + " " + ""
			+ "with Unique Id " + localStorage.tripId
			+"</font>";
		}
		$("#jobPopUp").show();
	} else {
		setMessage("Please login to continue");
		dialogShow();
	}
}
function cancelConfirmation() {
	$("#jobConfirmation").show();
	$("#jobPopUp").hide();
}

jQuery.fn.center = function() {
	this.css("position", "absolute");
	this.css("top", Math.max(0,
			(($(window).height() - $(this).outerHeight()) / 5)
					+ $(window).scrollTop())
			+ "px");
	this.css("left", "0px");
	return this;
}
$(document).bind('mobileinit', function() {
	$.mobile.loader.prototype.options.text = "loading";
	$.mobile.loader.prototype.options.textVisible = false;
	$.mobile.loader.prototype.options.theme = "a";
	$.mobile.loader.prototype.options.html = "";
});
function loginToServer() {
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var rememberMe = "";
	if (document.getElementById("rememberMe").checked == true) {
		rememberMe = 1;
	}
	var url ='/TDS/'
			+ 'CustomerMobileAction?event=clientLogin&userId='
			+ document.getElementById('userId').value + '&password='
			+ document.getElementById('password').value + '&assoccode='
			+ localStorage.ccode + '&loginOrLogout=1&rememberMe=' + rememberMe;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!=""){
		//alert("text"+text);
		var obj = "{\"loginstatus\":" + text + "}";
		jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.loginstatus.length; i++) {
			//alert(jsonObj.loginstatus[i].status);
			localStorage.loginCheck = jsonObj.loginstatus[i].status;
			localStorage.message = jsonObj.loginstatus[i].getMessage;
			if(localStorage.loginCheck == "loggedIn"){
				localStorage.userId = jsonObj.loginstatus[i].userId;
				localStorage.phoneNo = jsonObj.loginstatus[i].phoneNo;
				localStorage.userPermanentName = jsonObj.loginstatus[i].name;
				localStorage.userEmailId = jsonObj.loginstatus[i].emailId;
				localStorage.sessionId = jsonObj.loginstatus[i].sessionId;
				document.getElementById("sessionTemp").value=jsonObj.loginstatus[i].sessionId;
			}
		}
	}
	/*if (text.indexOf(";") != -1) {
		var arrayVal = text.split(";");
		for (var i = 0; i < arrayVal.length; i = i + 1) {
			if (arrayVal[i].indexOf("status") != -1) {
				var status = arrayVal[i].split("=");
				if (status[0] == "status") {
					localStorage.loginCheck = status[1];
				}
			} else if (arrayVal[i].indexOf("userId") != -1) {
				var userId = arrayVal[i].split("=");
				if (userId[0] == "userId") {
					localStorage.userId = userId[1];
				}
			} else if (arrayVal[i].indexOf("phoneNo") != -1) {
				var phNo = arrayVal[i].split("=");
				if (phNo[0] == "phoneNo") {
					localStorage.phoneNo = phNo[1];
				}
			} else if (arrayVal[i].indexOf("name") != -1) {
				var nameVal = arrayVal[i].split("=");
				if (nameVal[0] == "name") {
					localStorage.userPermanentName = nameVal[1];
				}
			} else if (arrayVal[i].indexOf("emailId") != -1) {
				var emailId = arrayVal[i].split("=");
				if (emailId[0] == "emailId") {
					localStorage.userEmailId = emailId[1];
				}
			} else if (arrayVal[i].indexOf("sessionId") != -1) {
				var sessionId = arrayVal[i].split("=");
				if (sessionId[0] == "sessionId") {
					localStorage.sessionId = sessionId[1];
					document.getElementById("sessionTemp").value=sessionId[1];
				}
			}
		}
	}*/
	$(window).attr('location', '#home');
	$.mobile.changePage("#home");
	if (localStorage.loginCheck == "loggedIn") {
		$(".Loggedout").hide();
		$(".LoggedIn").show();
		$("#popupBasic").popup("open");
		$("#addressMain").show();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
		$("#pickUp").show();
		Android.homePage("");
		cancelCreate();
		refreshingFunctionIsRunning=false;
		InitTCIL();
	} else {
		alert(""+localStorage.message+"!!");
		$("#popupBasic").popup("open");
		$(".Loggedout").show();
		$(".LoggedIn").hide();
		$("#addressMain").hide();
		$("#pickUp").hide();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
	}
}
function retry() {
	$.mobile.changePage("#pageinit");
}
function showFullAddress(){
	setMessage(document.getElementById("addressCustomerTD").value);
	dialogShow();
}
function updateGCMKey(pushKey) {
	if (pushKey != "") {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url ='/TDS/'
				+ 'CustomerMobileAction?event=gPI&register=YES&pushKey='
				+ pushKey;
		xmlhttp.open("GET", url, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
				+ localStorage.sessionId);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
	}
}

$("[data-rel=back]").click(function() {
	if (document.getElementById("navlist") != null) {
		$('#navlist').hide();
	}
});
$(document).delegate('div[data-role=page]', 'pageinit', function() {
	loadSettings();
	var internet = "true";
	if (internet == "true") {
		if (document.getElementById('ccode').value == "") {
			var ccode = "157";
			if (ccode != null) {
				document.getElementById("ccode").value = ccode;
				saveSettings();
			} else {
				alert("Enter your company code");
			}
			$("#addressMain").show();
			$("#dropOff").hide();
			$("#dropOffFinal").hide();
			$("#pickUp").show();
		}
		if (localStorage.loginCheck == "loggedIn") {
			$(".LoggedIn").show();
			$(".Loggedout").hide();
			document.getElementById("newFooterMenu").disabled = false;
			Android.homePage("");
		} else {
			loadLocation();
			$(".Loggedout").show();
			$(".LoggedIn").hide();
			$("#addressMain").hide();
			$("#pickUp").hide();
			$("#dropOff").hide();
			$("#dropOffFinal").hide();
			document.getElementById("newFooterMenu").disabled = true;
		}
	} else {
		$("#noInternetConnection").show();
	}
});

function companyCode() {
	if (ccode != null) {
		document.getElementById("ccode").value = ccode;
	} else {
		alert("Enter your company code");
	}
}

TimerRunning = false, i = 1;

function InitTCIL()// call the Init function when u need to start the timer
{
	localStorage.tripId="";
	mins = 1;
	secs = 0;
	StartTimer();
}
function StopTimer() {
	if (TimerRunning){
		window.clearInterval(TimerID);
	}
	TimerRunning = false;
	secs = 10;
}
function StartTimer() {
	StopTimer();
	if (localStorage.loginCheck != "loggedIn") {
		return;
	}
	TimerRunning = true;
	document.getElementById("newFooterMenu").disabled = false;
	TimerID = setInterval("activitesInTimer()", 5000);
}
function activitesInTimer() {
	if(refreshingFunctionIsRunning){
		return;
	}
	refreshingFunctionIsRunning = true;
	TimerRunning = false;
	jobLocation();
	if (document.getElementById("showingNav").value == "1") {
		$("#customerImage1").hide();
	}
	thisJobIsRunning = false;
	thisJobIsRunningForDriver = false;
	refreshingFunctionIsRunning = false;
}

function driverLocation123() {
	if($('#afterBooking').is(':visible')) {
		var latitude = "";
		var longitude = "";
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = "";
		if (localStorage.tripId != null && localStorage.tripId != ""
				&& localStorage.tripId != "undefined") {
			url ='/TDS/'
					+ 'CustomerMobileAction?event=driverLocation&tripId='
					+ localStorage.tripId;
		} else {
			url ='/TDS/'
					+ 'CustomerMobileAction?event=driverLocation';
		}
		xmlhttp.open("GET", url, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text != null && text != "") {
			var obj = "{\"driverLocation\":" + text + "}";
			var jsonObj = JSON.parse(obj.toString());
			for (var i = 0; i < jsonObj.driverLocation.length; i++) {
				latitude = jsonObj.driverLocation[i].getLatitude;
				longitude = jsonObj.driverLocation[i].getLongitude;
			}
			if (latitude != "") {
				document.getElementById("driverLati").value = latitude;
				document.getElementById("driverLongi").value = longitude;
			}
		}
	}
}
function fixPickUpKSA() {
	$("#sAddress1").val(localStorage.address);
	$("#phoneNumber").val(localStorage.phoneNo);
	$("#sLatitude").val(localStorage.lat);
	$("#sLongitude").val(localStorage.longi);
//	bookJob();
	$("#pickUp").hide();
	$("#dropOff").show();
}
/*function fixPickUp(){
	// setMessage("Enter D/O Or press book")
	// dialogShow();
	$("#popupBasic").html("<p> Book Job !! <p>");
	$("#popupBasic").popup("open");
	$("#pickUp").hide();
	$("#afterPickUp").show();
	$("#afterMap").hide();
}*/
function fixDropOffKSA() {
	if (document.getElementById("sAddress1").value != "") {
		$("#eAddress1").val(localStorage.address);
		$("#eLatitude").val(localStorage.lat);
		$("#eLongitude").val(localStorage.longi);
	} else {
		setMessage("Enter P/U then do D/O");
		dialogShow();
	}
	bookJob();
}
/*
 * function todayDate() { var curdate = new Date(); var cDate =
 * curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate(); var
 * cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 :
 * "0"+(curdate.getMonth()+1); var cYear = curdate.getFullYear(); var hors =
 * curdate.getHours() ; var min = curdate.getMinutes() >=10 ?
 * curdate.getMinutes() : "0"+curdate.getMinutes(); //it is pm if hours from 12
 * onwards var suffix = (hors >= 12)? 'PM' : 'AM';
 * 
 * //only -12 from hours if it is greater than 12 (if not back at mid night)
 * hors = (hors > 12)? hors -12 : hors; if(suffix=='PM'){ hors=hors>=10 ? hors :
 * "0"+hors; } //if 00 then it is 12 am hors = (hors == '00')? 12 : hors; hors =
 * hors >=10 ?hors : "0"+hors; var hrsmin = hors+":"+min+" "+suffix;
 * 
 * document.getElementById("sDate").value =cMonth+"/"+cDate+"/"+cYear;
 * document.getElementById("time").value=hrsmin; }
 */
function bookJob() {
	Android.homePage("");
	// todayDate();
	if(document.getElementById("internalFlag").innerHTML.indexOf("Driver Vehicle Profile") <=0 ){
		loadSpecialFlags();
	}
	if (document.getElementById("sAddress1").value != "") {
		$.mobile.changePage("#bookForm");
		// alert(localStorage.userPermanentName);
		$("#userName").val(localStorage.userPermanentName);
		
		if (localStorage.loginCheck == "loggedIn") {
			$("#favouriteAddresses").show();
			
		} else {
			$("#favouriteAddresses").hide();
			$("#pickUp").hide();
			$("#dropOff").hide();
			$("#dropOffFinal").hide();
		}

	} else {
		setMessage("Enter Atleast P/U address to book");
		dialogShow();
	}
}

function getDriverDetails(driverId) {
	return;
	$("#driverInternal").html('<iframe src=/TDS/'
					+ 'OpenRequestAjax?event=getDriverDocument&driverId='
					+ driverId + '></iframe>');
//	$("#driverDetail").jqm();
	$("#driverDetail").show();
}
function drDetailsHide(){
	$("#driverDetail").hide();
}
function Pad(number) // pads the mins/secs with a 0 if its less than 10
{
	if (number < 10)
		number = 0 + "" + number;
	return number;
}
function afterJobBooking()
{
	for (var i = 0; i < arrMarkerDriver.length; i++) {
		arrMarkerDriver[i].setMap(null);
//		labelGlobal[i].setMap(null);
	}
	arrMarkerDriver.length=0;
//	labelGlobal.length=0;
	var latitude = "";
	var longitude = "";
	var dLati="";
	var dLongi="";
	var status = "";
	var regNum="";
	var vMake="";
	var vType="";
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = "";
		if (localStorage.tripId != null && localStorage.tripId != ""
			&& localStorage.tripId != "undefined") {
			url ='/TDS/CustomerMobileAction?event=jobLocation&tripId=' + localStorage.tripId;
		} else {
			url = '/TDS/CustomerMobileAction?event=jobLocation';
		}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != "" && text != null) {
		var obj = "{\"jobLocation\":" + text + "}";
		var jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.jobLocation.length; i++) {
			latitude = jsonObj.jobLocation[i].getLatitude;
			longitude = jsonObj.jobLocation[i].getLongitude;
			status = jsonObj.jobLocation[i].SA;
			localStorage.SA = status;
			localStorage.tripId = jsonObj.jobLocation[i].TI;
			localStorage.driver = jsonObj.jobLocation[i].driver;
			localStorage.cab = jsonObj.jobLocation[i].cab;
			dLati=jsonObj.jobLocation[i].dLat;
			dLongi=jsonObj.jobLocation[i].dLongi;
			regNum=jsonObj.jobLocation[i].vReg;
			vMake=jsonObj.jobLocation[i].vMake;
			vType=jsonObj.jobLocation[i].vType;
		}
		var jobStatus = document.getElementById("jobStatus").value;
		if (latitude != "") {
			document.getElementById("jobLati").value = latitude;
			document.getElementById("jobLongi").value = longitude;
			document.getElementById("jobStatus").value = status;
			document.getElementById("driverLati").value=dLati;
			document.getElementById("driverLongi").value=dLongi;
		}
		$("#popupBasic").popup("close");
		if (localStorage.tripId != "" || localStorage.tripId != null
				|| localStorage.tripId != "undefined") {
			if (status < '4' || status == '8') {
				$("#statusBar").html('Contacting Drivers');
			}
		}
		var popupstatus;
		if (status == jobStatus) {
			updateDisplay(regNum,vMake,vType);
			return;
		}
		if (status == '4') {
			popupstatus="<p>Dispatch Has been Started <p>";
		} else if (status == '40') {
			setMessage("Your job allocated");
			popupstatus="<p>Job Allocated !!</p><br/>DriverID:"+ localStorage.driver + ":CabNo:"+ localStorage.cab;
			getDriverDetails(localStorage.driver);
		} else if (status == '25') {
			setMessage("Your job allocated");
			popupstatus="<p>Job Accepted !!</p><br/>DriverID:"+ localStorage.driver + ":CabNo:"+ localStorage.cab;
			getDriverDetails(localStorage.driver);
		} else if (status == '43') {
			popupstatus="<p>Driver is OnRoute !! </p><br/>DriverID:"+ localStorage.driver + ":CabNo:" + localStorage.cab;
		} else if (status == '45') {
			popupstatus="<p>Driver is waiting outside!! </p><br/>DriverID:"+ localStorage.driver + ":CabNo:"+ localStorage.cab;
			setMessage("Driver is waiting outside");
			dialogShow();
		}else if (status == '47') {
			popupstatus="<p>Your trip is Started!! </p><br/>DriverID:" + localStorage.driver + ":CabNo:"+ localStorage.cab;
		} else if (status == '48') {
			popupstatus="<p>Driver Cant Find You In The Address!!</p>";
			setMessage("Driver Cant Find You In The Address");
			dialogShow();
		} else if (status == '25') {
			popupstatus="<p>Couldn't Find Service !! <p>";
		} else if (status == '30') {
			popupstatus="<p>Your job Has been BroadCasted !! <p>";
		} else if (status == '61') {
			popupstatus="<p>Trip Completed,Thank you For Using!!<p>";
			document.getElementById("jobLati").value = "";
			document.getElementById("jobLongi").value = "";
			localStorage.tripId = "";
			$("#statusIcon").hide();
			$("#addressMain").show();
			$("#dropOff").hide();
			$("#dropOffFinal").hide();
			$("#pickUp").show();
		} else if (status == '50') {
			popupstatus="<p>Trip Canceled !! <p>";
			localStorage.tripId = "";
			$("#addressMain").show();
			$("#dropOff").hide();
			$("#dropOffFinal").hide();
			$("#pickUp").show();
		} else if (status == '55') {
			popupstatus="<p>Trip Canceled !! <p>";
			$("#addressMain").show();
			$("#dropOff").hide();
			$("#dropOffFinal").hide();
			$("#pickUp").show();
			localStorage.tripId = "";
		}
		document.getElementById("jobStatus").value = status;
		$("#popupBasic").html(popupstatus);
		$("#popupBasic").popup("open");
	}
	updateDisplay(regNum,vMake,vType);
}
function beforeJobBooking()
{
	for (var i = 0; i < arrMarkerDriver.length; i++) {
		arrMarkerDriver[i].setMap(null);
//		labelGlobal[i].setMap(null);
	}
//	arrMarkerDriver =[];
	arrMarkerDriver.length=0;
//	labelGlobal.length=0;
	geocoder = new google.maps.Geocoder();
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = '/TDS/CustomerMobileAction?event=driverList&sLat='+localStorage.lat+"&sLong="+localStorage.longi;
	var parameters='assoccode=157';
	xmlhttp.open("POST",url, false);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.setRequestHeader("Content-length", parameters.length);
	xmlhttp.setRequestHeader("Connection", "close");
	xmlhttp.send(parameters);
	var driversJsonResponse = xmlhttp.responseText;
	var obj = "{\"latLongArray\":" + driversJsonResponse + "}";
	var latitude="";
	var longitude="";
	var cabNumber="";
	var jsonObj = JSON.parse(obj.toString());
	var driverImage = "driverCab.png";
	//alert("jsonrespndse length"+jsonObj.latLongArray.length);
	for ( var i = 0; i < jsonObj.latLongArray.length; i++) {
		//alert("calling marker add "+ i)
		latitude=jsonObj.latLongArray[i].getLatitude;
		longitude=jsonObj.latLongArray[i].getLongitude;
		cabNumber=jsonObj.latLongArray[i].getCabNo;
		driverId=jsonObj.latLongArray[i].DR;
		var latLng = new google.maps.LatLng(latitude, longitude);
		var markerAll = new google.maps.Marker({
			position : latLng,
			map : map,
			icon : driverImage
		});
		markerAll.setMap(map);
		arrMarkerDriver.push(markerAll);
		//alert("arrMarkerDriver"+arrMarkerDriver.length);
//		var driverLabel = new Label({
//			map : map
//		});
//		driverLabel.set('position',latLng);
//		driverLabel.set('text', cabNumber);
//		driverLabel.setMap(map);
//		labelGlobal.push(driverLabel);
//		var infoWindow2 = new google.maps.InfoWindow({
//			content : "<DIV id = \"infoWindowJob\" style=\"background-color: LightYellow; overflow: hidden;color:black\"></div>"});
//		google.maps.event.addListener(markerAll, 'click',getDriverDetails(jsonObj.latLongArray[i].DR));
		google.maps.event.addListener(markerAll, 'click' ,function(){
			//alert("about to call driver image")
			var infowindow = new google.maps.InfoWindow({
			    content: '<iframe src="/TDS/CustomerMobileAction?event=getDriverDocument&driverId='+driverId+'"></iframe></t><font style="color:black;font-weight: bolder;font-size:large;">Driver Id:</font>&nbsp;&nbsp;&nbsp;<font style="color:red;font-weight: bolder;font-size:medium;">'+driverId+'<t>&nbsp;&nbsp;&nbsp;<font style="color:black;font-size:large;">Cab Number:</font>&nbsp;&nbsp;&nbsp;'+cabNumber+'</font>'
			    /*'<iframe src=OpenRequestAjax?event=getDriverDocument&driverId='+ driverId + '></iframe>'*/
			  });
			  infowindow.open(map,markerAll);
			  refreshingFunctionIsRunning=true;
			  $("#performAction").show();
		});
	}
}
function cancelMapRefresh(){
	  refreshingFunctionIsRunning=false;
	  $("#performAction").hide();
}
function jobLocation() {
	if(localStorage.tripId !="") {
		afterJobBooking();
	} else  {
		beforeJobBooking();
	}
}


function cancelCreate(){
	var status=document.getElementById("jobStatus").value;
	if(status=="" || status=="50" || status=="55" || status=="61"){
		$("#addressMain").show();
		$("#dropOff").hide();
		$("#dropOffFinal").hide();
		$("#pickUp").show();
	}
}
function loadLocation() {
	navigator.geolocation.getCurrentPosition(success_handler_first,
			error_handler, {
				enableHighAccuracy : true,
				maximumAge : 60000,
				timeout : 10000,
				frequency : 3000
			});
}
function loadLocationResume(){
	navigator.geolocation.watchPosition(success_handler_update, error_handler,
			{
				enableHighAccuracy : true,
				maximumAge : 60000,
				timeout : 10000,
				frequency : 3000
			});
}
var objectNew=[];
function loadSpecialFlags(){
	if(document.getElementById("loadFlagValue").value=="0"){
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var driverCounter=0;
		var vehCounter=0;
		var ccode=$('#ccode').val();
		var url='/TDS/CustomerMobileAction?event=getFlagsForCompany&compCode='+ccode;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var jsonObj = "{\"flagDetails\":"+text+"}";
		var obj = JSON.parse(jsonObj.toString());
		objectNew=JSON.parse(jsonObj.toString());
		if(obj.flagDetails.length>0){
			var $tbl = $('<table style="width:100%"">').attr('id', 'flagValues');
			//var tbl;
			document.getElementById("internalFlag").innerHTML="";
			$tbl.append($('<td colspan="20" style="width:100%;text-decoration: underline;" align="center">').text('Driver Vehicle Profile'));	
			for(var j=0;j<obj.flagDetails.length;j++){
				if(obj.flagDetails[j].SW=="1"){
					$tbl.append($('<tr>').append(
							$('<td style="margin-left:10%">').text(obj.flagDetails[j].LD),
							$('<td stle="margin-left:80%">').append($('<input/>').attr('type', 'checkbox').attr('class', 'squaredOne').attr('name', 'dchk'+driverCounter).attr('id', 'dchk'+driverCounter).val(obj.flagDetails[j].K).attr('onclick',"checkGroup(1,'"+driverCounter+"','"+obj.flagDetails[j].GI+"','"+obj.flagDetails[j].LD+"')")),
							$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'dgroup'+driverCounter).attr('id','dgroup'+driverCounter).val(obj.flagDetails[j].GI)),
							$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'dLD'+driverCounter).attr('id','dLD'+driverCounter).val(obj.flagDetails[j].LD))
					));
					driverCounter=Number(driverCounter)+1;
					$("#internalFlag").append($tbl);
				} else {
					$tbl.append($('<tr>').append(
							$('<td style="margin-left:10%">').text(obj.flagDetails[j].LD),
							$('<td stle="margin-left:80%">').append($('<input/>').attr('type', 'checkbox').attr('class', 'squaredOne').attr('name', 'vchk'+vehCounter).attr('id', 'vchk'+vehCounter).val(obj.flagDetails[j].K).attr('onclick',"checkGroup(2,'"+vehCounter+"','"+obj.flagDetails[j].GI+"','"+obj.flagDetails[j].LD+"')")),
							$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vgroup'+vehCounter).attr('id','vgroup'+vehCounter).val(obj.flagDetails[j].GI)),
							$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vLD'+vehCounter).attr('id','vLD'+vehCounter).val(obj.flagDetails[j].LD))
					));
					vehCounter=Number(vehCounter)+1;
					$("#internalFlag").append($tbl);
				}
			}
			
			$("#driverProfile").val(driverCounter);
			$("#vehicleProfile").val(vehCounter);
		}
	}
	document.getElementById("loadFlagValue").value="1";
}
function checkGroup(type,row,groupId,longDesc){
	var drProf=$("#driverProfile").val();
	var veProf=$("#vehicleProfile").val();
	if(type==1 && groupId!=""){
		if(document.getElementById("dchk"+row).checked==true){
			for(var i=0;i<drProf;i++){
				if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
					document.getElementById("dchk"+row).checked=false;
					setMessage("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
					 dialogShow();
					break;
				}
			}
			for(var i=0;i<veProf;i++){
				if(document.getElementById("vchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
					document.getElementById("dchk"+row).checked=false;
					 setMessage("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
					 dialogShow();
					 break;
				}
			}
		}
	} else if(type==2 && groupId!=""){
		if(document.getElementById("vchk"+row).checked==true){
			for(var i=0;i<veProf;i++){
				if(document.getElementById("vchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
					document.getElementById("vchk"+row).checked=false;
					 setMessage("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
					 dialogShow();
					 break;
				}
			}
			for(var i=0;i<drProf;i++){
				if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
					document.getElementById("vchk"+row).checked=false;
					 setMessage("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
					 dialogShow();
					 break;
				}
			}
		}
	}
}

function splFlagHide(){
	$("#specialFLags").hide();
}
function splFlagShow(){
	$("#specialFLags").show();
}
function LongClick(map, length) {
	this.length_ = length;
	var me = this;
	me.map_ = map;
	google.maps.event.addListener(map, 'mousedown', function(e) {
		me.onMouseDown_(e);
	});
	google.maps.event.addListener(map, 'mouseup', function(e) {
		me.onMouseUp_(e);
	});
}
LongClick.prototype.onMouseUp_ = function(e) {
	var now = +new Date;
	if (now - this.down_ > this.length_) {
		google.maps.event.trigger(this.map_, 'longpress', e);
	}
};
LongClick.prototype.onMouseDown_ = function() {
	this.down_ = +new Date;
};

function success_handler_first(position) {
	$('#customerImage1').css(
			'margin-top',
			$("#placeholder").height() / 2 - $('#placeholder').position().top
					+ $("#headId").height() / 3 + 'px');
	$('#customerImage1').css('margin-left',
			($('#placeholder').width() / 2) - 16 + 'px');
//	$('#addressCustomerTD').css('height',
//			$('#placeholder').height() / 10 + 'px');
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	accuracy = position.coords.accuracy;
	$("#latitudeOnLoad").val(latitude);
	$("#longitudeOnLoad").val(longitude);
	var point = new google.maps.LatLng(latitude, longitude);
	var geocoder = new google.maps.Geocoder();
	geocoder
			.geocode(
					{
						latLng : point
					},
					function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							if (results[0]) {
								var arrAddress = results[0].address_components;

								var streetnum = "";
								var route = "";
								var itemCountry = "";
								var itemPc = "";
								$
										.each(
												arrAddress,
												function(i, address_component) {
													if (address_component.types[0] == "street_number") {
														/* $("#sadd1Dash").val(address_component.long_name); */
														streetnum = address_component.long_name;
													}

													if (address_component.types[0] == "route") {
														route = address_component.long_name;
													}

													if (address_component.types[0] == "postal_code") {
														itemPc = address_component.long_name;
													}
													document.getElementById("addressCustomerTD").value = results[0].formatted_address;
													localStorage.address = results[0].formatted_address;
													localStorage.lat = latitude;
													localStorage.longi = longitude;

													document
															.getElementById("addressOnLoad").value = results[0].formatted_address;
												});
							}
						}
					});
	initializeCustomer();
	navigator.geolocation.watchPosition(success_handler_update, error_handler,
			{
				enableHighAccuracy : true,
				maximumAge : 60000,
				timeout : 10000,
				frequency : 3000
			});
}

function success_handler_update(position) {
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	accuracy = position.coords.accuracy;
	geocoder = new google.maps.Geocoder();
	latlng = new google.maps.LatLng(latitude, longitude);
	if (document.getElementById("dragend").value == 'false') {
		map.setCenter(latlng);
	}
}

function bookJobCall(latitude, longitude) {

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'CustomerMobileAction?event=openRequest';
	if (latitude != null && latitude != "") {
		url = url + '&latitude=' + latitude;
	}
	if (longitude != null && longitude != "") {
		url = url + '&longitude=' + longitude;
	}

	window.open(url, '_self');

}
function latLngHandler(position) {
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	if (!latitude || !longitude) {
		document.getElementById("status").innerHTML = "HTML5 Geolocation supported, but location data is currently unavailable.";
		return;
	}
	var latlng = new google.maps.LatLng(latitude, longitude);
	return latlng;
}

var markerCustomer;
function initializeCustomer() {
	var blueDot = "http://maps.google.com/mapfiles/kml/paddle/grn-circle-lv.png";
	geocoder = new google.maps.Geocoder();
	latlng = new google.maps.LatLng(latitude, longitude);
	var options = {
		zoom : 18,
		center : latlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		draggableCursor : 'crosshair'
	};

	map = new google.maps.Map(document.getElementById("placeholder"), options);
	// google.maps.event.trigger(map, 'resize');
	google.maps.event
			.addListener(
					map,
					'dragend',
					function() {
						dragend();
						var c = map.getCenter();
						latitude = c.lat();
						longitude = c.lng();
						latlng = new google.maps.LatLng(latitude, longitude);
						geocoder = new google.maps.Geocoder();
						$('#dragLati').val(latitude);
						$('#dragLongi').val(longitude);

						var point = new google.maps.LatLng(latitude, longitude);
						var geocoder = new google.maps.Geocoder();
						geocoder
								.geocode(
										{
											latLng : point
										},
										function(results, status) {
											if (status == google.maps.GeocoderStatus.OK) {
												if (results[0]) {
													var arrAddress = results[0].address_components;

													var streetnum = "";
													var route = "";
													var itemCountry = "";
													var itemPc = "";
													$
															.each(
																	arrAddress,
																	function(i,
																			address_component) {
																		if (address_component.types[0] == "street_number") {
																			/* $("#sadd1Dash").val(address_component.long_name); */
																			streetnum = address_component.long_name;
																		}
																		if (address_component.types[0] == "locality") {
																			// $("#scityDash").val(address_component.long_name);

																		}
																		if (address_component.types[0] == "route") {
																			route = address_component.long_name;
																		}
																		if (address_component.types[0] == "country") {
																			itemCountry = address_component.long_name;
																		}
																		if (address_component.types[0] == "postal_code") {
																			itemPc = address_component.long_name;
																			// $("#szipDash").val(address_component.long_name);
																		}
																		document.getElementById("addressCustomerTD").value = results[0].formatted_address;
																		localStorage.address = results[0].formatted_address;
																		localStorage.lat = latitude;
																		localStorage.longi = longitude;
																		document
																				.getElementById("addressDragged").value = results[0].formatted_address;

																	});
												}
											}
										});
					});
	new LongClick(map, 800);
	var markerCustomer;
	var externalImage = "male.png";
	var driverImage = "driverCab.png";
	// GEOCODER
	geocoder = new google.maps.Geocoder();
	/*
	 * markerCustomer = new google.maps.Marker({ position : latlng, map : map,
	 * icon : externalImage });
	 */
	map.setCenter(latlng);
	// updateDisplay();
	$("#customerImage1").show();
	// $("#addressCustomer").show();
}

function updateDisplay(regNum,make,type) {
	latlng = new google.maps.LatLng(latitude, longitude);
	var externalImage = "male.png";
	var driverImage = "driverCab.png";
	// GEOCODER
	var driverLatLng="";
	var latlng1="";
	geocoder = new google.maps.Geocoder();
	if (document.getElementById("driverLati").value != "" && document.getElementById("driverLati").value != "0.0") {
		driverLatLng = new google.maps.LatLng(
				document.getElementById("driverLati").value, document.getElementById("driverLongi").value);
		markerDriver = new google.maps.Marker({
			position : driverLatLng,
			map : map,
			icon : driverImage
		});
		markerDriver.setMap(null);
		markerDriver.setMap(map);
		$("#driverValueMark").val("1");
		google.maps.event.addListener(markerDriver, 'click' ,function(){
			var infoNew = new google.maps.InfoWindow({
			    content: '<iframe src="/TDS/CustomerMobileAction?event=getDriverDocument&driverId='+localStorage.driver+'"></iframe></t><font style="color:black;font-weight: bolder;font-size:large;">Driver Id:</font>&nbsp;<font style="color:red;font-weight: bolder;font-size:medium;">'+localStorage.driver+'<t>&nbsp;<font style="color:black;font-size:large;">Cab Number:</font>&nbsp;'+localStorage.cab+''
			    +'<p><font style="color:black;font-weight: bolder;font-size:large;">Reg Num:</font>&nbsp;<font style="color:red;font-weight: bolder;font-size:medium;">'+regNum+'<t>&nbsp;<font style="color:black;font-size:large;">Veh Type:</font>&nbsp;'+make+'-'+type+'</font>'
			  });
			infoNew.open(map,markerDriver);
		    refreshingFunctionIsRunning=true;
		    $("#performAction").show();
		});
		document.getElementById("driverLati").value = "";
		document.getElementById("driverLongi").value = "";
	}
	if (document.getElementById("jobLati").value != "" && document.getElementById("jobLati").value!="0.0") {
		latlng1 = new google.maps.LatLng(
				document.getElementById("jobLati").value, document
						.getElementById("jobLongi").value);
		markerjob = new google.maps.Marker({
			position : latlng1,
			map : map,
			icon : externalImage,
			animation : google.maps.Animation.DROP
		});
		markerjob.setMap(null);
		markerjob.setMap(map);
		$("#jobMarker").val("1");
		document.getElementById("jobLati").value = "";
		document.getElementById("jobLongi").value = "";
	} 
	if(driverLatLng!="" && latlng1!=""){
		var bounds = new google.maps.LatLngBounds();
		bounds.extend(latlng1);
		bounds.extend(driverLatLng);
		map.fitBounds(bounds);
	} else {
		loadLocation();
	}
	//map.fitbounds
	$("#customerImage1").show();
}
function dialogShow() {
	// $.mobile.changePage( "#myDialog", { role: "dialog" } );
	$("#myDialog").show();
}
function dialogHide() {
	$("#myDialog").hide();
}
function confirmationShow() {
	// $.mobile.changePage( "#myDialog", { role: "dialog" } );
	$("#myConfirmation").show();
}
function confirmationHide() {
	$("#myConfirmation").hide();
}

function mapselected() {
	$("#pickUp").hide();
	$("#dropOff").hide();
	$("#myConfirmation").hide();
	$("#dropOffFinal").show();
}
function tagselected() {
	$("#myConfirmation").hide();
	if (localStorage.loginCheck == "loggedIn") {
		pickUpAddresses();
	} else {
		var msg = "Please Login to Us.";
		setMessage(msg);
		dialogShow();
	}
}
function infoCallback(infowindow, marker) { 
	return function() { 
		infowindow.open(map, marker);
	}; 
}
function hideMarkers() {
	loadLocation();
}
function error_handler(error) {
	initializeCustomer();
	var locationError = '';

	switch (error.code) {
	case 0:
		locationError = "There was an error while retrieving your location: "
				+ error.message;
		break;
	case 1:
		locationError = "The user prevented this page from retrieving a location.";
		break;
	case 2:
		locationError = "The browser was unable to determine your location: "
				+ error.message;
		break;
	case 3:
		locationError = "The browser timed out before retrieving the location.";
		break;
	}
	document.getElementById("status").innerHTML = locationError;
	document.getElementById("status").style.color = "#D03C02";
}
function dragend() {
	document.getElementById("dragend").value = "true";
	$("#focusPlace").show();
	// cancel();
}
function focusPlaceFunction() {
	document.getElementById("dragend").value = "false";
	loadLocation();
	$("#jobMarker").val("0");
	$("#driverValueMark").val("0");
	document.getElementById("dragLati").value = "";
	document.getElementById("dragLongi").value = "";
	document.getElementById("addressDragged").value = "";
	$("#addressMain").show();
	$("#dropOff").hide();
	$("#dropOffFinal").hide();
	$("#pickUp").show();
	$("#focusPlace").hide();
	/*
	 * $("#afterPickUp").hide(); $("#pickUp").show();
	 */
}

function showLoginKSA() {
	$.mobile.changePage("#login_page1");
}

function closeDiv() {
	$("#tagAddress").hide('slow');
}
function closeTag() {
	$("#addressTagWindow").hide();
}
function insertAddress(switches) {
	var address = "";
	var tagName = "";
	var latitude = "";
	var longitude = "";
	if (switches == "") {
		tagName = document.getElementById("tagNameForHome").value;
		address = document.getElementById("sAddressTagForHome").value;
		latitude = localStorage.lat;
		longitude = localStorage.longi;
	} else {
		tagName = document.getElementById("tagName").value;
		address = document.getElementById("sAddressTag").value;
		latitude = document.getElementById("sLatitude").value;
		longitude = document.getElementById("sLongitude").value;
	}
	if (address != "" && address != null) {
		var url = '/TDS/CustomerMobileAction?event=checkAddress&address='
				+ address;
		xmlhttp.open("GET", url, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
				+ localStorage.sessionId);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == 0) {
			if (tagName != "") {
				if (window.XMLHttpRequest) {
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				var url ='/TDS/CustomerMobileAction?event=tagAddress&tagName='
						+ tagName + '&address=' + address + '&latitude='
						+ latitude + '&longitude=' + longitude;
				xmlhttp.open("GET", url, false);
				xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
						+ localStorage.sessionId);
				xmlhttp.send(null);
				var text = xmlhttp.responseText;
				if (text == 1) {
					if (switches == '1') {
						$.mobile.changePage("#bookForm");
					} else if (switches == '') {
						$.mobile.changePage("#home");
						$("#tagNameForHome").val("");
						$("#sAddressTagForHome").val("");
					}
				}
			} else {
				setMessage("Enter Tag Name to save this Address");
				dialogShow();
			}
		} else {
			var msg = "This address is already tagged, Please check Tagged Addresses";

			setMessage(msg);
			dialogShow();
		}

	}
}
function pickUpAddresses() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ='/TDS/CustomerMobileAction?event=taggedAddressestoShow';
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != null && text != "") {
		document.getElementById("taggedAddressestoShowBody").innerHTML = "";
		var obj = "{\"favAddress\":" + text + "}";
		var jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.favAddress.length; i++) {

			$("#taggedAddressestoShowBody").append("<tr>");
			$("#taggedAddressestoShowBody").append(
					"<td>" + jsonObj.favAddress[i].tagAdd + "</td>");
			$("#taggedAddressestoShowBody").append(
					"<td style=\"overflow:hidden\" >"
							+ jsonObj.favAddress[i].address + "</td>");
			$("#taggedAddressestoShowBody")
					.append(
							"<td style=\"background-color: lightgrey\">"
									+ "<input type=\"button\" name=\"fixAsPu\" id=\"fixAsPu\" value=\"P/U\" onclick=\"fixValues('"
									+ jsonObj.favAddress[i].address
									+ "','"
									+ jsonObj.favAddress[i].getLatitude
									+ "','"
									+ jsonObj.favAddress[i].getLongitude
									+ "');\">"
									+ "<input type=\"button\" name=\"fixAsDo\" id=\"fixAsDo\" value=\"D/O\" onclick=\"fixValuesDropOff('"
									+ jsonObj.favAddress[i].address + "','"
									+ jsonObj.favAddress[i].getLatitude + "','"
									+ jsonObj.favAddress[i].getLongitude
									+ "');\"></td>");

			$("#taggedAddressestoShowBody").append("</tr>");

		}
	}

	$.mobile.changePage("#taggedAddresstoShow");
}
function showTagDiv(switches) {
	if (localStorage.loginCheck == "loggedIn") {
		var address = "";
		if (switches == 1) {
			address = document.getElementById("sAddress1").value;
		} else if (switches == "") {
			address = document.getElementById("addressCustomerTD").value;
		} else {
			address = document.getElementById("eAddress1").value;
		}
		if (address == "" || address == null) {
			var msg = "Please Enter an address.";
			setMessage(msg);
			dialogShow();
		} else {
			$("#sAddressTagForHome").val(address);
			$.mobile.changePage("#addressTagWindowForHome");
			taggedAddresses();
		}
	} else {
		var msg = "Please Log In.";
		setMessage(msg);
		dialogShow();
	}
}
function taggedAddresses() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ='/TDS/CustomerMobileAction?event=taggedAddressestoShow';
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != null && text != "") {
		document.getElementById("taggedAddressesForHomeBody").innerHTML = "";
		var obj = "{\"favAddress\":" + text + "}";
		var jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.favAddress.length; i++) {
			$("#taggedAddressesForHomeBody").append("<tr>");
			$("#taggedAddressesForHomeBody").append(
					"<td  style=\"background-color: lightgrey\">"
							+ jsonObj.favAddress[i].tagAdd + "</td>");
			$("#taggedAddressesForHomeBody")
					.append(
							"<td  style=\"background-color: lightgrey\"><input type=\"button\" name=\"showAddress\" id=\"showAddress\" value=\"Address\" onclick=\"popAddress('"
									+ jsonObj.favAddress[i].address
									+ "')\"> </td>");
			$("#taggedAddressesForHomeBody")
					.append(
							"<td colspan=\"3\" style=\"background-color: lightgrey\">"
									+ "<input type=\"button\" name=\"fixAsPu\" id=\"fixAsPu\" value=\"P/U\" onclick=\"fixValues('"
									+ jsonObj.favAddress[i].address
									+ "','"
									+ jsonObj.favAddress[i].getLatitude
									+ "','"
									+ jsonObj.favAddress[i].getLongitude
									+ "');\">"
									+ "<input type=\"button\" name=\"fixAsDo\" id=\"fixAsDo\" value=\"D/O\" onclick=\"fixValuesDropOff('"
									+ jsonObj.favAddress[i].address
									+ "','"
									+ jsonObj.favAddress[i].getLatitude
									+ "','"
									+ jsonObj.favAddress[i].getLongitude
									+ "');\">"
									+ "<input type=\"button\" name=\"deleteAddress\" id=\"deleteAddress\" value=\"Delete\" onclick=\" deleteAddress(this,'"
									+ jsonObj.favAddress[i].addKey
									+ "','');\"></td>");

			$("#taggedAddressesForHomeBody").append("</td>");
			$("#taggedAddressesForHomeBody").append("</tr>");
		}
	}

}
function popAddress(address) {
	setMessage(address);
	dialogShow();
}
function fixValues(address, latitude, longitude) {
	$("#tagAddress").hide('slow');
	document.getElementById("sAddress1").value = address;
	document.getElementById("sLatitude").value = latitude;
	document.getElementById("sLongitude").value = longitude;
	bookJob();
	// $.mobile.changePage("#bookForm");
	// $('#taggedAddresstoShow').jqmHide();
	// $("#taggedAddresstoShow").hide();
}

function fixValueshistory(address,latitude, longitude, phone) {
	$("#tagAddress").hide('slow');
	document.getElementById("sAddress1").value = address;
	document.getElementById("sLatitude").value = latitude;
	document.getElementById("sLongitude").value = longitude;
//	document.getElementById("eAddress1").value = endadress;
//	document.getElementById("eLatitude").value = edlatitude;
//	document.getElementById("eLongitude").value = edlongitude;
	document.getElementById("phoneNumber").value = phone;
	bookJob();
	// $.mobile.changePage("#bookForm");
	// $('#taggedAddresstoShow').jqmHide();
	// $("#taggedAddresstoShow").hide();
}

function fixValuesDropOff(address, latitude, longitude) {
	$("#tagAddress").hide('slow');
	document.getElementById("eAddress1").value = address;
	document.getElementById("eLatitude").value = latitude;
	document.getElementById("eLongitude").value = longitude;
	bookJob();
	// $('#taggedAddresstoShow').jqmHide();
	// $("#taggedAddresstoShow").hide();
}
function checkUserId() {
	var userId = document.getElementById("userIdForR").value;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url ="/TDS/CustomerMobileAction?event=userIdCheck&responseType=HTML5&submit=YES&ccode="
			+ localStorage.ccode + "&userId=" + userId;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		// document.getElementById("userId").focus();
		$(
				"<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>Already Exists</h1></div>")
				.css({
					"display" : "block",
					"opacity" : 0.96,
					"top" : $(window).height() / 2,
					"align" : "center"
				}).appendTo($.mobile.pageContainer).delay(800).fadeOut(4000,
						function() {
							$(this).remove();
						});
	}

}

function checkPassword() {
	var password = document.getElementById("passwordForR").value;
	var rePassword = document.getElementById("rePasswordForR").value;
	if (password != "" && rePassword != "") {
		if (password == rePassword) {
		} else {
			/*
			 * document.getElementById("passwordForR").value = "";
			 * document.getElementById("rePasswordForR").value = "";
			 */
			document.getElementById("rePasswordForR").focus();
			// document.getElementById("errorText").value="Password not
			// matching";
			// if(document.getElementById("os").value=="G"){
			Android.showToast("Password doesn't match");

		}
	}
}

