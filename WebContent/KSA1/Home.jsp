<!DOCTYPE html>
<html>
<head>
<title>GetAcab Customer App</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="custAppTCIL.js"></script>
<script type="text/javascript" src="label.js"></script>
<script type="text/javascript" src="Preferences.js"></script>
<script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<link type="text/css" href="custApp.css" rel="stylesheet" /> 
<script type="text/javascript" src="http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog.min.js"></script>
<link type="text/css" href="http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog.min.css" rel="stylesheet" /> 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<link rel="stylesheet" type="text/css" href="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.min.css" /> 
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.core.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.calbox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/i18n/jquery.mobile.datebox.i18n.en_US.utf8.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.core.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.calbox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.datebox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.flipbox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.durationbox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.durationflipbox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.slidebox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.customflip.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.custombox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/i18n/jquery.mobile.datebox.i18n.en_US.utf8.js"></script>
<style type="text/css">
.squaredOne {
  width: 150px;
  height: 50px;
  background: #333;
  margin: 20px 60px;
  border-radius: 50px;
  position: relative;
  color: white;
  background-color: silver;
}
</style>
<script type="text/javascript">
$(document).ready ( function(){
	InitTCIL();showNavTCIL();rememberMe();
});
</script>	
<script>
$(document).ready(function(){
	  $("#languagelist").hide();
	});
$(document).ready(function(){
	  $("#language").click(function(){
	    $("#languagelist").toggle(500);
	  });
	});
	
$(document).ready(function(){
	  $("#contactlist").hide();
	});
$(document).ready(function(){
	  $("#contact").click(function(){
	    $("#contactlist").toggle(500);
	  });
	});
</script>
</head>
<body>

	<form id="bookForm" method="post"
		action="CustomerMobileAction?event=openRequest">
		<input type="hidden" name="tripId" id="tripId" value="" /> <input
			type="hidden" name="status" id="status" value="" /> <input
			type="hidden" name="dragend" id="dragend" value="false" /> <input
			type="hidden" id="driverLati" value="" /> <input type="hidden"
			id="driverLongi" value="" /> <input type="hidden" id="dragLati"
			value="" /> <input type="hidden" id="dragLongi" value="" /> <input
			type="hidden" name="address" id="address" value="" /> <input
			type="hidden" name="latitude" id="latitude" value="" /> <input
			type="hidden" name="longitude" id="longitude" value="" /> 
			<input	type="hidden" id="jobMarker" value="0" /> 
			<input	type="hidden" id="driverValueMark" value="0" /> 
			<input	type="hidden" id="switches" value="" /> <input type="hidden"
			id="addressDragged" value="" /> <input type="hidden"
			id="addressOnLoad" value="" /> <input type="hidden"
			id="latitudeOnLoad" value="" /> <input type="hidden"
			id="longitudeOnLoad" value="" /> <input type="hidden" id="jobLati"
			value="" /><input type="hidden" id="jobLongi" value="" /> <input
			type="hidden" id="pushKey" name="pushKey" value="" /> 
			<input type="hidden" id="jobStatus" name="jobStatus" value="" /> <input
			type="hidden" name="focus" id="focus" value="1" />
			<input type="hidden" id="showingNav" value="0"/>
			<input type="hidden" id="loadFlagValue" value="0"/>
			<input type="hidden" id="driverProfile" value=""/>
			<input type="hidden" id="vehicleProfile" value=""/>
			<input type="hidden" id="sessionTemp" value=""/>
	</form>
	<!-- PageMap -->
	<div data-role="page" class="page-map" id="home"
		style="width: 100%; height: 100%; padding: 0" onload="loadLocation()">
		<div data-role="header">
			<div>  </div>
			<div data-role="navbar" id="newFooterMenu" style="width: 100%;background-color: black;height: 4%;">
			<ul>
			<li><a href="javascript:showFullDetails()"><img alt="" src="trips.png"/><font style="font-size: large;">Trip Status</font></a></li>
			<li><a href="javascript:showTagDiv('')"><img alt="" src="addtag.png"/><font style="font-size: large;">Favourite</font></a></li>
			<li><a href="#TripsOnBoard"><img alt="" src="historyJob.png"/><font style="font-size: large;">History</font></a></li>
			<li><a href="#Settings"><img alt="" src="settings.png"/><font style="font-size: large;">Settings</font></a></li>
			</ul>
			</div>
		</div>
		<div id="addressMain" style="background: transparent;position: absolute;opacity:0.7;z-index: 100;background-color: black;color: yellow ;font-weight: bolder;width: 100%;overflow: hidden;white-space: nowrap;height: 10%;" >
		<table style="width: 100%"><tr>
		<td style="background-color: yellow;margin-right:92%;width: 8%;height: 4px;" align="center"><img alt="" src="addressL.png" align="middle"/></td>
		<td style="height: 3%;width: 84%;" align="center"><input type="text" name="addressCustomerTD" id="addressCustomerTD" placeholder="Address" style="font-weight: bolder;color: maroon;"></td>
		<td style="background-color: yellow;margin-left: 92%;width: 8%;height: 4px;" align="center"><img alt="" src="addressR.png" align="middle" onclick="fixPickUpKSA()"/></td>
		</table>
		</div>
		<div id="performAction" style="display: none;opacity:1.5;z-index: 100;width: 40%;height: 10%;margin-top: 14%;margin-left: 15%;" >
	         <input type="button" value="Start Refresh" style="background-color: red;color: white ;" onclick="cancelMapRefresh();"/>       
		</div>		
		
		<div data-role="footer" class="Loggedout" style="height: 8%;">
			<img class="LoggedOut" alt="" style="width:10%;height:75%; z-index: 200;position:absolute;margin-left:2%" onclick="showLoginKSA()" src="icon_user_loggedout.png"/>
			<img alt="" src="MyTaxi.png" style="width:45%;height:100%;margin-left: 32%;"/>
			<a href="#login_page1" style="display:none;"></a> 
 		</div>
		<div data-role="footer" class="LoggedIn" style="height: 8%;">
		<img class="LoggedIn" alt="" style="width:10%;height:75%; z-index: 50;position:absolute;margin-left:10%" onclick="logout()" src="icon_user.png"/>
		<img alt="" src="MyTaxi.png"  style="width:45%;height:100%;margin-left: 32%;"/>
 		</div>
 		
		<div data-role="footer" class="LoggedIn" style="margin-bottom: 16%;height: 4%;">
			<div data-role="navbar" id="pickUp">
	           <ul>
					<li><a  href="javascript:fixPickUpKSA()" onclick="fixPickUpKSA()"><font style="color: black;text-indent: 1%;background-color: yellow;font-weight: bolder;font-size: x-large;text-shadow: orange;text-align: center;z-index: 2000">Pick Me Up Here</font></a></li>
				</ul>
			</div>	
			<div data-role="navbar" id="dropOff" style="display: none;margin-left: 10%;">
	           <ul>
					<li><a href="javascript:confirmationShow" onclick="confirmationShow()" align="center"><font style="color: black;text-indent: 1%;background-color: yellow;font-weight: bolder;font-size: x-large;text-shadow: orange;text-align: center;z-index: 2000" >Drop Off</font></a></li>
					<li><a href="javascript:bookJob" onclick="bookJob()" align="center"><font style="color: black;text-indent: 1%;background-color: yellow;font-weight: bolder;font-size: x-large;text-shadow: orange;text-align: center;z-index: 2000">Book Job</font></a></li>
				</ul>
			</div>	
			<div data-role="navbar" id="dropOffFinal" style="display: none;">
	           <ul>
					<li><a  href="javascript:fixDropOffKSA" onclick="fixDropOffKSA()"><font style="color: black;text-indent: 1%;background-color: yellow;font-weight: bolder;font-size: x-large;text-shadow: orange;text-align: center;">Drop Me Off Here</font></a></li>
				</ul>
			</div>	
		</div>	
		
			
		<img id="customerImage1" src="customer.png" style="position: absolute; z-index: 3;"></img> 
					<div data-role="content" style="width: 100%; height: 90%; padding: 0;">
		<div id="placeholder" style="width: 100%; height: 90%; padding: 0;">
        </div>
                        <div data-role="popup" id="popupBasic">
                        </div>
		</div>
		  <img id="focusPlace"
			style="background: transparent;opacity:0.8;display: none; position: absolute; z-index: 300;margin-top:-20%;margin-left:88%;text-decoration: "
			src="location.png"
			onclick="focusPlaceFunction()"></img> 
<!-- 		<div data-role="footer">

          <ul  data-role="listview" data-inset="true" data-theme ="a"
				data-dividertheme="a" id="navlist" style="display: none;height:300px;z-index:4000">
				<li  data-role="list-divider">Accounts  Trip Details</li>
				<li style="height: 17%"><a href="#reserveTrips" data-transition="slide" >Future
                Trips</a>
				</li>
				<li style="height: 17%"><a href="#TripsOnBoard" data-transition="slide">Trip On
                Board</a>
				</li>
				<li style="height: 17%"><a href="#accountRegistration" data-transition="slide">Account
                Registration</a>
				</li>
				<li style="height: 17%"><a href="#accountSummary" data-transition="slide">Account
                Summary</a>
				</li>
				<li style="height: 17%"><a href="#favAddressDetails" data-transition="slide">Favourite
                Address</a>
				</li>
                </ul>   
              
            <div data-role="navbar" id="pickUp">
            <ul>
			<li><a  href="javascript:fixPickUp()" data-icon="star" data-rel="popup"   data-theme ="e"
				 onclick="fixPickUp()" >Pick Me Up Here</a></li>
			                </ul>
		</div>
		    <div data-role="navbar" id="afterBooking"  style="display: none;">
           	<ul><li><b class="status" id="statusBar" style="text-align: center;"></b></li></ul>
            <ul>
			<li><a  href="javascript:showFullDetails()" class="status" id="statusIcon" data-icon="star" data-rel="popup"   data-theme ="e"
				  >Status</a></li>
			                </ul>
		</div>
            <div data-role="navbar" id="afterPickUp" style="display:none">
                <ul>
                                       <li><a   data-icon="check" data-theme ="e"
                        onclick="confirmationShow()" >Drop Off</a></li>
                        <li><a href="javascript:bookJob()"  data-icon="forward"  data-theme ="e"
                        onclick="bookJob()" >Book</a></li>
                        </ul>
                        </div>
            
		   <div data-role="navbar" id="afterMap" style="display:none">
               <ul>
			<li><a  href="javascript:bookJob()" data-icon="star" data-rel="popup"   data-theme ="e"
				 onclick="fixDropOff()" >Drop Me Off Here</a></li>
			                </ul>
                        </div>
               
                </div>
 -->                <!-- /footer -->
       </div>
                        
        <!--   Page map close-->       
                
         <!--  Login Page-->       
                <div data-role="page" id="login_page1" data-theme ="a"
                style="width: 100%; height: 100%; padding: 0" align="center">
		<div data-role="header">
			<h1>Get A Cab</h1>
            <a class="homeIcon" href="#home"
                data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
            
           <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
            
		</div>
		<div data-role="content"  align="center"
			style="width: 100%; height: 90%; padding: 0">
			<input id="responseType" name="responseType" type="hidden"
				value="HTML5">
			<div class="ui-body ui-body-a" align="center">
				<div class="ui-field-contain ui-body ui-br" data-role="fieldcontain"
					data-theme ="a">
					<input type="text" name="userId" id="userId" value="1234"
						placeholder="Enter your UserID" data-clear-btn="true" />
				</div>
				<div class="ui-field-contain ui-body ui-br" data-role="fieldcontain">
					<input type="password" name="password" id="password" value="1234"
						placeholder="Enter your password" data-clear-btn="true" />
				</div>
				<div class="ui-field-contain ui-body ui-br" data-role="fieldcontain">
				<table style="width: 100%;">
				<tr><td style="width: 40%;">Remember Me:</td><td style="margin-right:72%;"><input class="squaredOne" type="checkbox" name="rememberMe" id="rememberMe" value="1" Style="width:20px; height:30px; margin-top:-10px"/></td></tr></table>
				</div>
   				<div class="ui-field-contain ui-body ui-br" data-role="fieldcontain">
					<a><button class="ui-btn-hidden" value="submit-value"
							name="submit" data-theme ="a" type="button"
							onclick="loginToServer()">Login</button> </a>
				</div>
			</div>
		</div>

		<div data-role="footer" data-theme ="a">
			<div data-role="navbar">
				<ul>
					<li><a href="#register">Register</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	         <!--  Login Page close-->       
	
	<!-- /footer -->
	<!-- Dialog  -->

	<div data-role="dialog" id="preference"
		style="width: 100%; height: 100%; padding: 0">
		<div data-role="header">
			<h1>Preferences</h1>
		</div>
		<div data-role="content" style="width: 100%; height: 90%; padding: 0;"
			align="center">
			<label for="name">Web Address</label><br /> <input type="text"
				name="webUrl" id="webUrl" value="192.168.1.101:8080"
				data-clear-btn="true" autocomplete="on" /><br /> <a
				data-rel="back"> <input type="button" name="saveval"
				id="saveval" value="Save" data-rel="back" data-theme ="a"
				onclick="saveSettings();" /> </a>

		</div>
		<div data-role="footer"></div>
		<input type="hidden" name="os" id="os" value="G" /> <input
			type="hidden" name="ccode" id="ccode" value="157" data-rel=back />
	</div>
	<!-- Dialog close -->
<!--Book Form  -->
	<div data-role="page" id="bookForm"
		style="width: 100%; height: 100%; padding: 0;" data-theme ="a">
		<div data-role="header">
			<h1>Book A Job</h1>
            <a class="homeIcon" href="#home"
                data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
            
            <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
		</div>
		<div data-role="content" style="width: 100%; height: 90%; padding: 0;">
			
				<input type="hidden" id="sLatitude" name="sLatitude" value="" /> <input
					type="hidden" id="sLongitude" name="sLongitude" value="" />
					<input type="hidden" id="eLatitude" name="eLatitude" value="" /> <input
					type="hidden" id="eLongitude" name="eLongitude" value="" />
			<table style="width: 100%; height: 90%; padding: 0;">
					<tr>
						<td ><img  src="user_icon.png"/></td><td colspan="2"><input id="userName" name="userName" type="text"
							style="font-size: large;" " placeholder="Enter your Name"
							autofocus required>
						</td>
					</tr>
					<tr>
						<td ><img  src="address.png"/></td><td colspan="2"><input id="sAddress1" name="sAddress1" type="text"
							style="font-size: large;" onblur="showTagDiv()" value=""
							placeholder="Enter your P/U Address" required />
						</td>
					</tr>
					<tr>
						<td ><img  src="address.png"/></td><td colspan="2"><input id="eAddress1" name="eAddress1" type="text"
							style="font-size: large;" value=""
							placeholder="Enter your D/O Address" required />
						</td>
					</tr>
					<tr id="favouriteAddresses" style="display: none;">
					</tr>
<!-- 					<tr>
					<td ><img  src="specialrequest.png" onclick="splFlagShow()"/>
					</td>
					</tr>
 -->					<tr>
						<td ><img  src="phone.png"/></td><td colspan="2"><input id="phoneNumber" name="phoneNumber" type="text"
							value="" placeholder="Enter your phoneNumber" required />
						</td>
					</tr>
					<tr>
					
						<td >
						
						<img  src="expirydate.png"/></td>
					<td colspan="1">	
						<input name="sDate" id="sDate" type="text" placeholder="Today" readonly="readonly" data-role="datebox" data-options='{"mode": "datebox", "useNewStyle":true}'  />
						</td><td colspan="1">
						<input name="time" id="time" type="text" placeholder="Now" readonly="readonly" data-options='{"mode":"timebox", "useNewStyle":true}' data-role="datebox"  />

						</td>
					</tr>
					<tr><td colspan="3"><table style="width: 100%"><tr>
						<td colspan="1.25" ></td>
						<td colspan="0.25" align="center"  ><a data-rel ="back"><input type="button" id="button" name="button"
								value="Cancel" style="z-index: 2000;" onclick="cancelCreate()"> </a>
						</td>
						<td colspan="0.25" align="center"  ><input type="button" id="button" name="button"
								value="More" style="z-index: 2000;" onclick="splFlagShow()">
						</td>
						<td colspan="0.25" align="center"  ><a><input type="button" id="button" name="button"
								value=" Book " style="z-index: 2000;" onclick="bookTrip()"> </a>
						</td>
						<td colspan="1.25" ></td>
					</tr></table></td></tr>
					<tr id="tagAddress"	style=	"background-color: lightgray; width: 98%; height: 5%; display: none;">
						<td colspan="3" align="center">Do you want to tag this address? <input type="button"
							name="yes" id="yes" value="yes" onclick="tagAddresses()" />
							&nbsp; <input type="button" name="no" id="no" value="no"
							onclick="closeDiv()" /></td>
					</tr>
				</table>
		</div>
		
	</div>
	<!--Book Form  close-->
	
	<!--Address tag window  -->
		<div data-role="page"  id="addressTagWindow" onload="" style="width: 100%; height: 100%; padding: 0;" data-theme ="a">
		<div data-role="header" >
			<h1>Tag Address</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                           <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
            
		</div>
		<div data-role="content" style="position:fixed;">
								<table style="margin-left: 5%;">
							
									<tr>
										<td><input id="tagName" name="tagName" type="text"
											value="" placeholder="Tag Name"></td>
									</tr>
									<tr>
										<td><input id="sAddressTag" name="sAddressTag"
											type="text" value="" /></td>
									</tr>
									<tr>
										<td><input type="button" id="submit" name="submit"
											onclick="insertAddress('1')" value="Tag">
										</td>
									</tr>
								</table>
								</div>
			
							</div>
							
                    
							
				<!--Address tag window close -->
				 <div data-role="page" id="addressTagWindowForHome"
                            style="width: 100%; height: 100%; padding: 0;" >
                            <div data-role="header" data-theme ="a" style="width: 100%;position:fixed;z-index:300;">
                            
			<h1>Tag Address</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                           <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
            
		</div>
		<div data-role="content" style="padding: 0;" >
								<table style="margin-top: 10%">
								
									<tr>
										<td><input id="tagNameForHome" name="tagName" type="text"
											value="" placeholder="Tag name"></td>
									
										<td><input id="sAddressTagForHome" name="sAddressTag"
											type="text" value="" /></td>
									
										<td><input type="button" id="submit" name="submit"
											onclick="insertAddress('')" value="Tag">
										</td>
									</tr>
								</table>
								</div>
   
                   
                            <table data-role="table" id="taggedAddressForHomeTb" 
                            data-mode="reflow"
                            data-mode="columntoggle" style="width: 100%;z-index:300;"
                            class="ui-responsive table-stroke ui-table ui-table-inset ui-table-columntoggle ui-stripe">
                            
                         
                            <thead>
                              
                            <tr bgcolor="gray">
                            <th>Name</th>
                            <th>Address</th>
                            <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="taggedAddressesForHomeBody">
                            </tbody></table>
			</div>			
				                 <div data-role="page" id="taggedAddresstoShow"
                            style="width: 100%; height: 100%; padding: 0;" >
                            <div data-role="header" data-theme ="a" style="width: 100%;position:fixed;z-index:300;">
                            <h1>Fav Addresses</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                            
                            <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
                            
                            </div>
                            <div data-role="content" padding: 0;">
                            
                            <table data-role="table" id="taggedAddresstoShowTb" data-mode="reflow" 
                            data-mode="columntoggle"
                            class="ui-responsive table-stroke ui-table ui-table-inset ui-table-columntoggle ui-stripe">
                            <thead>
                            <tr bgcolor="gray">
                            <th>Name</th>
                            <th>Address</th>
                            <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="taggedAddressestoShowBody"  >
                            </tbody></table></div>
                           </div>			
							
							
		<!-- 	<div data-role="page" id="taggedAddresstoShow" style="width: 100%; height: 100%; padding: 0;overflow: scroll;" data-theme ="a">
			<table id="taggedAddresstoShowTb" data-mode="reflow"
                            data-mode="columntoggle"
                            class="ui-responsive table-stroke ui-table ui-table-inset ui-table-columntoggle ui-stripe">
				<div data-role="header" data-theme ="a">
                            <h1>Fav. Addresses</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                            
                            <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
                            
                            </div>
                            
				
				<tr>
				<th>Name</th>
				<th>Address</th>
				<th>Action</th>
				</tr>
				<tbody id="taggedAddressestoShowBody">
				
				</tbody>
				</table>
			</div>				 -->
	
	<!-- Register -->						
	<div data-role="page" id="register"
		style="width: 100%; height: 100%; padding: 0;" data-theme ="a">
		<div data-role="header" data-theme ="a">
			<h1>Register</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                            
                           <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
            
		</div>
		<input type="hidden" name="os" id="os" value="G" />
		<div data-role="content" style="width: 100%; height: 90%; padding: 0;">
			<table style="width: 100%; height: 90%; padding: 0;">
				<tr>
					<td colspan="1"><img  src="user_id.jpg"/></td><td colspan="2"><input id="userIdForR" name="userIdForR" type="text"
						onblur="checkUserId()" placeholder="Create your User id" required>
					</td>
				</tr>
				<tr>
					<td colspan="1"> <img  src="password.png"/></td><td colspan="2"><input id="passwordForR" name="passwordForR"
						type="password" value="" placeholder="Enter the password" required>
					</td>
				</tr>
				<tr>
					<td  colspan="1"> <img  src="password.png"/></td><td colspan="2"><input id="rePasswordForR" name="rePasswordForR"
						type="password" value="" placeholder="Re-type Password" required
						onblur="checkPassword()">
					</td>
				</tr>
				<tr>
					<td colspan="1"><img src="user.png"/></td><td colspan="2"><input id="userNameForR" name="userNameForR" type="text"
						placeholder="Enter your Name" required></td>
				</tr>
				<tr>
					<td colspan="1"><img  src="email.png"/></td><td colspan="2"><input id="emailAddressForR" name="emailAddressForR"
						type="text" placeholder="Enter your Email Address" required>
					</td>
				</tr>
				<tr>
					<td colspan="1">	 <img  src="phone.png"/></td><td colspan="2"><input id="phoneNumberRegistration"
						name="phoneNumberRegistration" type="text" value=""
						placeholder="Enter your phoneNumber" required>
					</td>
				</tr>
				<tr>
					<td colspan="3"><input type="button" id="registerButton"
						name="registerButton" value="Register" onclick="registerUser()">
					</td>
				</tr>
			</table>
		</div>
		<div data-role="footer" data-theme ="a">
			<div data-role="navbar">
				<ul>
					<li><a href="#login_page1">Login</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
		<!-- Register close-->						
	
	<!-- Settings -->
	<div data-role="page" id="Settings" >
                    <div data-role="header" data-theme="e"> 
                    <h1> Settings</h1>
                    </div>
                    <div data-role="controlgroup" style="width:100%" align="center" id="content">
                    <table><tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                     <tr></tr>
                    <tr></tr>
                    <tr></tr>
                     <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    </table>
                    <ul data-role="listview" data-theme="a">
                      <li><a href="#personalInfo">Personal Info</a></li>
                      <table><tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    </table>
                      <li id="language"><a>Language</a></li>
                      <table><tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    </table>
                      	<ul data-role="listview" data-theme="a" id="languagelist" style="width:80%" align="center">
                      		<li align="center"><center>English<center></center></li>
                      		 <table><tr></tr>
                    <tr></tr>
                    <tr></tr>
                    <tr></tr>
                    </table>
                      	</ul>
                      <li id="contact"><a> Contact Us</a></li>
                      	<ul data-role="listview" data-theme="a" id="contactlist" style="width:80%" align="center">
                      		<li align="center"><center>GetACab, Bangalore<center></li>
                      	</ul>
                    </ul>
                    </div>
                    </div>
                  
	<!-- Settings -->
<!-- Trips On Board -->
	<div data-role="page" id="TripsOnBoard"
		style="width: 100%; height: 100%; padding: 0;overflow-x:hidden" data-theme ="e" >
		<div data-role="header" data-theme ="a" style="width: 100%;">
			<h1>Trips History</h1>
                           <a class="homeIcon" href="#home" data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                           <a href="#"  data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
		</div>
		<div data-role="content" data-theme ="a" style="width: 100%; height: 100%; padding: 0;" >
			<font style="font-size: large; font-weight: bold;">Recent Trips</font>
			<table data-role="table" id="tripHistoryTb" data-mode="reflow"
                            data-mode="columntoggle" style="width: 100%;z-index:300;"
                            class="ui-responsive table-stroke ui-table ui-table-inset ui-table-columntoggle ui-stripe ">
				<thead>
					<tr bgcolor="grey">
						<th>Name</th>
						<th>Ph#</th>
						<th>Address</th>
						<th>Re-Book</th>
					</tr>
				</thead>
				<tbody id="tripHistoryTbody" style="color: #254117;">
				</tbody>
			</table>
		</div>
		
			</div>
<!-- Trips On Board close-->

	<!-- Account registration -->
	<div data-role="page" id="accountRegistration"
		style="width: 100%; height: 100%; padding: 0;" data-theme ="a">
		<div data-role="header" data-theme ="a">
			<h1>Account Registration</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                            
                           <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
            
		</div>
		<div data-role="content" style="width: 100%; height: 90%; padding: 0;">
			<table style="width: 100%; height: 90%; padding: 0;">
				<tr>
					<td colspan="1"><img  src="user_name.jpg"/></td><td colspan="2"><input id="userNameForAcctReg" name="userNameForAcctReg"
						type="text" value="" placeholder="Enter your Name" autofocus
						required> <!-- 					   <tr><td>        <input id="userIdForAcctReg" name="userIdForAcctReg" type="number"  value="" placeholder="Enter your User id" required></td></tr>
 -->
				<tr>
					<td colspan="3"><select id="paymentTypeForAcctReg"
						name="paymentTypeForAcctReg" value="" required>
							<option value="voucher">Voucher</option>
							<option value="cash">Cash</option>
							<option value="creditcard">Credit Card</option>
					</select>
					</td>
				</tr>
				<tr>
					<td colspan="1"><img  src="creditcard.png"/></td><td colspan="2"><input id="cardNumberForAcctReg"
						name="cardNumberForAcctReg" type="text" value=""
						placeholder="Enter Card Number" required></td>
				</tr>
				<tr>
					<td colspan="1"><img  src="vouchernumber.png"/></td><td colspan="2"><input id="voucherNumberForAcctReg"
						name="voucherNumberForAcctReg" type="text" value=""
						placeholder="Enter your Voucher Number" required></td>
				</tr>
				<tr>
					<td colspan="1"><img  src="expirydate.png"/></td><td colspan="2"><input id="cardExpiryDateForAcctReg"
						name="cardExpiryDateForAcctReg" type="text" value=""
						placeholder="Enter your Card Expiry Date" required></td>
				</tr>
				<tr>
					<td colspan="3"><input type="button" id="registerButtonForAcctReg"
						name="registerButtonForAcctReg" value="Register"
						onclick="accountRegistrationForUser()">
					</td>
				</tr>
			</table>
		</div>
			</div>
			<!-- Account registration Close-->
		
			<!-- Fav address  -->
		
	<div data-role="page" id="favAddressDetails"
		style="width: 100%; height: 100%; padding: 0;" data-theme ="e">
		<div data-role="header" data-theme ="a">
			<h1>Favourite Address</h1>
                            <a class="homeIcon" href="#home" data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                           <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
		</div>
		<div data-role="content" style="width: 100%; height: 90%; padding: 0;">
			<table data-role="table" id="favAddressTb" data-mode="reflow"
				data-mode="columntoggle"
				class="ui-responsive table-stroke ui-table ui-table-inset ui-table-columntoggle ui-stripe">
				<thead>
					<tr bgcolor="orange">
						<th>Address key</th>
						<th>Address</th>
						<th>Tag Address</th>
					</tr>
				</thead>
				<tbody id="favAddressTbody">
				</tbody>
			</table>

		</div>
		<div data-role="footer" data-theme ="a"></div>
	</div>
	<!-- Fav address close -->
			<!-- Account Summary  -->
	
	<div data-role="page" id="accountSummary"
		style="width: 100%; height: 100%; " data-theme ="e">
		<div data-role="header" data-theme ="a">
			<h1>Account Summary</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                            
                           <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
            
		</div>
		<div data-role="content" >
                            <table data-role="table" id="accountSummaryTb" style="width: 100%; height: 100%; ">
				<thead>
					<tr bgcolor="orange">
						<th>P/T</th>
						<th>A/C#</th>
						<th>ExD</th>
						<th>C#</th>
						<th>CExD</th>
					</tr>
				</thead>
                            <tbody id="accountSummaryTbody"   >
				</tbody>
				<!-- <tr>
					<td onclick="backToHome()"><input type="button"
						name="homeFromAcctSummary" onClick="backToHome()" value="Home"></input>
					</td>
				</tr> -->
			</table>

		</div>
			</div>
		
		<!-- Account Summary close -->
		
			<!-- Primary screen  -->
		
	<div data-role="page" id="paymentScreen"
		style="width: 100%; height: 100%; padding: 0;" data-theme ="a">
		<div data-role="header" data-theme ="a">
			<h1>Payment</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                            
                           <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
            
		</div>
		
		<div id="progressbar"></div>
		<div data-role="content" style="width: 100%; height: 90%; padding: 0;">
			<table data-role="table" id="paymentTb" data-mode="reflow"
				data-mode="columntoggle"
				class="ui-responsive table-stroke ui-table ui-table-inset ui-table-columntoggle ui-stripe">
				<thead>
					<tr>
						<th>Charge Type</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody id="paymentTbody">
				</tbody>
			</table>
			<table id="cardDetails" style="display: none;">
				<tr>
					<td>Card Number</td>
					<td><input type="text" name="cardNumber" id="cardNumber"
						value="" />
					</td>
				</tr>
				<tr>
					<td>CVV</td>
					<td><input type="text" name="cvv" id="cvv" value="" />
					</td>
				</tr>
				<tr>
					<td><input type="button" id="payNow" name="payNow" value="pay"
						onClick="payFare()" />
					</td>

				</tr>

			</table>
			<table id="existingCards" style="display: none;">
				<tbody id="existingCardsTbody">
				</tbody>
				<tr>
					<td onclick="backToHome()"><input type="button"
						name="homeFromAcctSummary" onClick="backToHome()" value="Home"></input>
					</td>
				</tr>
			</table>
		</div></div>
		<!-- Primary screen close -->
		
		<!-- internet connection  -->
		<div id="noInternetConnection" style="display: none;">
		
		</div>
				<!-- close internet connection  -->
		
		<!-- Dailaog -->
			<div id="jobPopUp" data-overlay-theme="e" data-role="dialog" data-external-page="true" tabindex="0" class="ui-page ui-body-c ui-overlay-e ui-dialog ui-page-active" role="dialog" style="min-height: 320px;display: none;margin-top: 40%;z-index:5000;position:fixed;">
		<div data-theme ="a" data-role="header" class="ui-corner-top ui-overlay-shadow ui-header ui-bar-b" role="banner"><a data-iconpos="notext" data-icon="delete" href="javascript:hideDetails();" class="ui-btn-left ui-btn ui-btn-icon-notext ui-btn-corner-all ui-shadow ui-btn-up-b" title="Close" data-theme ="a"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow"></span></span></a>
			<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Job Details</h1>
		</div>

		<div  data-theme ="a" data-role="content" class="ui-overlay-shadow ui-content ui-body-d" role="main">
			<div id="jobPopupMessage"></div>
			
			<a id="cancelJobButton" data-inline="true" data-theme ="a" data-role="button"  class="ui-btn ui-btn-up-a ui-btn-inline ui-btn-corner-all ui-shadow"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text"  onclick="javascript:cancelConfirmation();">Cancel Job</span></span></a>       
			<a data-inline="true" data-theme ="a" data-role="button"  class="ui-btn ui-btn-up-a ui-btn-inline ui-btn-corner-all ui-shadow"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text"  onclick="javascript:hideDetails();">Close</span></span></a>       
			
		</div>
	</div>
		<div id="jobConfirmation" data-overlay-theme="e" data-role="dialog" data-external-page="true" tabindex="0" class="ui-page ui-body-c ui-overlay-e ui-dialog ui-page-active" role="dialog" style="min-height: 300px;display: none;margin-top: 30%;z-index:5000;">
		<div data-theme ="a" data-role="header" class="ui-corner-top ui-overlay-shadow ui-header ui-bar-b" role="banner"><a data-iconpos="notext" data-icon="delete" href="javascript:hideCancellation();" class="ui-btn-left ui-btn ui-btn-icon-notext ui-btn-corner-all ui-shadow ui-btn-up-b" title="Close" data-theme ="a"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow"></span></span></a>
			<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Cancellation</h1>
		</div>

		<div  data-theme ="a" data-role="content" class="ui-overlay-shadow ui-content ui-body-d" role="main">
			<table id="messageToForFav">
			<tr><td>
			Do You want to cancel this job?
			</td>
			</tr>
			<tr><td>
						<a data-inline="true" data-theme ="a" data-role="button"  class="ui-btn ui-btn-up-a ui-btn-inline ui-btn-corner-all ui-shadow"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text"  onclick="javascript:cancelTrip('');">Yes</span></span></a>       
			</td>
			<td>
						<a data-inline="true" data-theme ="a" data-role="button"  class="ui-btn ui-btn-up-a ui-btn-inline ui-btn-corner-all ui-shadow"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text"  onclick="javascript:hideCancellation();">No</span></span></a>       
			</td>
			</tr>
			
			</table>
		</div>
	</div>
		<div id="myDialog" data-overlay-theme="e" data-role="dialog" data-external-page="true" tabindex="0" class="ui-page ui-body-c ui-overlay-e ui-dialog ui-page-active" role="dialog" style="min-height: 320px;display: none;margin-top: 40%;z-index:5500;position:fixed;">
		<div data-theme ="a" data-role="header" class="ui-corner-top ui-overlay-shadow ui-header ui-bar-b" role="banner">
		<a data-iconpos="notext" data-icon="delete" href="javascript:dialogHide();" class="ui-btn-left ui-btn ui-btn-icon-notext ui-btn-corner-all ui-shadow ui-btn-up-b" title="Close" data-theme ="a"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow"></span></span></a>
			<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Information</h1>
		</div>

		<div  data-theme ="a" data-role="content" class="ui-overlay-shadow ui-content ui-body-d" role="main">
			<div id="messageTo"></div>
			
			<a data-inline="true" data-theme ="a" data-role="button" onclick="javascript:dialogHide();" class="ui-btn ui-btn-up-a ui-btn-inline ui-btn-corner-all ui-shadow"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text"  onclick="javascript:dialogHide();">Got it</span></span></a>       
		</div>
	</div>
	
	<div id="specialFLags"  data-overlay-theme="e" data-role="dialog" data-external-page="true" tabindex="0" class="ui-page ui-body-c ui-overlay-e ui-dialog ui-page-active" role="dialog" style="min-height: 300px;display: none;margin-top: 7%;z-index:5000;">
		<a data-iconpos="notext" data-icon="delete" href="javascript:splFlagHide();" class="ui-btn-left ui-btn ui-btn-icon-notext ui-btn-corner-all ui-shadow ui-btn-up-b" title="Close" data-theme ="a" style="margin-left: 90%;height: 8%;width: 8%;text-align: center;"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow"></span></span></a>
		<div id="internalFlag" style="font-size: x-large;" data-theme ="a" data-role="header" class="ui-corner-top ui-overlay-shadow ui-header ui-bar-b" role="banner">
		</div>
	</div>
	
	<div id="driverDetail" data-overlay-theme="e" data-role="dialog" data-external-page="true" tabindex="0" class="ui-page ui-body-c ui-overlay-e ui-dialog ui-page-active" role="dialog" style="min-height: 300px;display: none;margin-top: 7%;z-index:5000;">
		<a data-iconpos="notext" data-icon="delete" href="javascript:drDetailsHide();" class="ui-btn-left ui-btn ui-btn-icon-notext ui-btn-corner-all ui-shadow ui-btn-up-b" title="Close" data-theme ="a" style="margin-left: 70%;height: 20%;width: 30%;text-align: center;"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow"></span></span></a>
		<div id="driverInternal" style="font-size: x-large;width: 40%;height: 30%;margin-top: 30%;margin-left: 20%;" data-theme ="a" data-role="header" class="ui-corner-top ui-overlay-shadow ui-header ui-bar-b" role="banner">
		</div>
	</div>
	<div id="myConfirmation" data-overlay-theme="e" data-role="dialog" data-external-page="true" tabindex="0" class="ui-page ui-body-c ui-overlay-e ui-dialog ui-page-active" role="dialog" style="min-height: 300px;display: none;margin-top: 30%;z-index:5000;">
		<div data-theme ="a" data-role="header" class="ui-corner-top ui-overlay-shadow ui-header ui-bar-b" role="banner">
		<a data-iconpos="notext" data-icon="delete" href="javascript:confirmationHide();" class="ui-btn-left ui-btn ui-btn-icon-notext ui-btn-corner-all ui-shadow ui-btn-up-b" title="Close" data-theme ="a"><span class="ui-btn-inner ui-btn-corner-all"><span class="ui-btn-text">Close</span><span class="ui-icon ui-icon-delete ui-icon-shadow"></span></span></a>
			<h1 class="ui-title" tabindex="0" role="heading" aria-level="1">Pick Drop Off From</h1>
		</div>
		<div  data-theme ="a" data-role="content" class="ui-overlay-shadow ui-content ui-body-d" role="main">
			<table id="messageToForFav">
			<tr><td>
			<table>
			<tr><td>			
			<img alt="" onclick="mapselected()" style="width:50%;height:30%" src="map.png"></img>
			</td></tr>
			<tr><td>From Map</td></tr>
			</table>
			</td><td>
			<table>
			<tr><td>									
			<img alt="" onclick="tagselected()" style="width:50%;height:60%" src="tag.png"></img>
			</td></tr>
			<tr><td>Tagged</td></tr>
			</table>
			</td>
			</tr></table>
		</div>
	</div>
	<!--Dialogue close  -->
                            <!--reserved trips -->
                            <div data-role="page" id="reserveTrips"
                            style="width: 100%; height: 100%; padding: 0;" data-theme ="e">
                            <div data-role="header" data-theme ="a">
                            <h1>Reserved Trips</h1>
                            <a class="homeIcon" href="#home"
                            data-icon="home" data-iconpos="notext" data-iconpos="notext"></a>
                            
                            <a href="#"   data-icon="back"  data-rel ="back" class="ui_button-right" data-iconpos="notext" ></a>
                            
                            </div>
                            <div data-role="content" padding: 0;">
                            
                            <table data-role="table" id="reservedTripsTable" data-mode="reflow"
                            data-mode="columntoggle"
                            class="ui-responsive table-stroke ui-table ui-table-inset ui-table-columntoggle ui-stripe">
                            <thead>
                            <tr bgcolor="orange">
                            <th>Name</th>
                            <th>TID</th>
                            <th>Ph#</th>
                            <th>Address</th>
                            <th>City</th>
                            <th><th>
                            </tr>
                            </thead>
                            <tbody id="reservedTripsTbody">
                            </tbody></table></div>
                           </div>

                            <!--reserved trips close -->
                            </body>
</html>



