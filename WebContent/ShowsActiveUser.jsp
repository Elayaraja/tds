<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
var mins,secs,TimerRunning,TimerID,TimerID1;
TimerRunning=false,i=1;

function Init() //call the Init function when u need to start the timer
{
   mins=<%=request.getAttribute("sessionTimeOut").toString().split(":")[0]%>;
   secs=<%=request.getAttribute("sessionTimeOut").toString().split(":")[1]%>;
   //StopTimer();
   StartTimer();
}

function StopTimer()
{
	
	//alert(mins);
	//alert(secs);	 

   if(TimerRunning)
      clearTimeout(TimerID);
   TimerRunning=false;

	document.getElementById('tid').innerHTML = "Kindly Reload The Page";
	location.reload(true);
   //mins=document.getElementById('sec').value;
   //secs=document.getElementById('sec').value;
   //StartTimer();
}

function StartTimer()
{
 
   TimerRunning=true;
   //window.status="Time Remaining "+Pad(mins)+":"+Pad(secs);
   document.getElementById('tid').innerHTML = Pad(mins)+":"+Pad(secs);
   TimerID=self.setTimeout("StartTimer()",1000);
   
   //Check();
   
   if(mins==0 && secs==0)
   if(secs==0)
   {
      StopTimer();
      //dashBoardAjax2();
   }
   if(secs==0)
   {
      mins--;
      secs=60;
   }
   secs--;

}

function Check()
{
   if(mins==5 && secs==0)
      alert("You have only five minutes remaining");
   else if(mins==0 && secs==0)
   {
      alert("Your alloted time is over.");
   }
}

function Pad(number) //pads the mins/secs with a 0 if its less than 10
{
   if(number<10)
      number=0+""+number;
   return number;
}


</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<body onload="Init()">
<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
        	 
            
            <div class="contentDV">
            <div class="contentDVInWrap">
            <div class="contentDVIn">
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<h1>Show Active Count</h1>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                      <tr>
                      <td>
                      	<table id="bodypage" border="">
                      	<tr>
 						<td>Session Expired in:</TD><TD><div id='tid'><%=request.getAttribute("sessionTimeOut") %></div></td>
 						</tr>
 						</table>
 						</td>
 						</tr>
 						</table>    
                    
                </div>    
              </div>
            	
                <div class="clrBth"></div>
            </div>
            </div>
            </div>
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
		</div>
	</div>
</div>
</body>
</html>
