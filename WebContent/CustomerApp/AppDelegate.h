//
//  AppDelegate.h
//  GetACab
//
//  Created by Ram on 29/07/13.
//  Copyright (c) 2013 Ram. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
