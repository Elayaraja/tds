var amount = 0;
var total = 0;
var initialTip = 0;

function rememberMe() {
	// alert("I am coming");
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = "http://" + document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=clientLogin&newEntry=YES";
	// alert("hello-->"+url);
	xmlhttp.open("GET", url, false);
	xmlhttp.send();
	var text = xmlhttp.responseText;
	// alert(text);
	if (text == "1") {
		$(".LoggedIn").show();
		$(".Loggedout").hide();
	} else {
		$(".Loggedout").show();
		$(".LoggedIn").hide();
	}
}

function backToHome() {
	$(window).attr('location', '#home');
	$.mobile.changePage("#home");
	if (localStorage.loginCheck == "loggedIn") {
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		if ($('#navlist').css('display') == 'none') {
			Android.homePage("");
		} else {
//			Android.homePage("hide Option");
			$("#customerImage1").hide();
		}
		// loadLocation();
		// updateDisplay();
		// Init();
	} else {
		$(".Loggedout").show();
		$(".LoggedIn").hide();
//		if ($('#home').is(':visible')) {
//			Android.homePage("in home");
//		} else {
//			Android.homePage("exit home");
//		}
	}
}

function bookTrip() {
	$(".ui-loader").show();
	if (document.getElementById("sDate").value != ""
			&& document.getElementById("time").value == "") {
		alert("Enter a 4 digit Time Value");
		return;
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var repeatJob="";
	var driverList=document.getElementById("driverProfile").value;
	var vehicleList=document.getElementById("vehicleProfile").value;
	if(typeof driverList != "" && driverList!=null){
		 repeatJob=repeatJob+'&drprofileSize='+driverList;	
		 for (var i=0; i<driverList;i++){
			 if(document.getElementById('dchk'+i).checked==true){
				 repeatJob=repeatJob+'&dchk'+i+'='+document.getElementById('dchk'+i).value;
			 }
		 }
	 }
	 if(typeof vehicleList != "" &&vehicleList!=null){
		 repeatJob=repeatJob+'&vprofileSize='+vehicleList;	
		 for (var i=0;i<vehicleList;i++){
			 if(document.getElementById('vchk'+i).checked==true){
				 repeatJob=repeatJob+'&vchk'+i+'='+document.getElementById('vchk'+i).value;
			 }
		 }
	 }
	var url = "http://" + document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=openRequest&sLatitude="
			+ document.getElementById("sLatitude").value + "&sLongitude="
			+ document.getElementById("sLongitude").value + "&userName="
			+ document.getElementById("userName").value + "&sAddress1="
			+ document.getElementById("sAddress1").value + "&phoneNumber="
			+ document.getElementById("phoneNumber").value + "&ccode="
			+ document.getElementById('ccode').value + "&sDate="
			+ document.getElementById("sDate").value + "&time="
			+ document.getElementById("time").value + "&submit=YES&eLatitude="
			+ +document.getElementById("eLatitude").value + "&eLongitude="
			+ +document.getElementById("eLongitude").value + "&eAddress1="
			+ document.getElementById("eAddress1").value+repeatJob;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != "" && text != null) {
		localStorage.tripId = text;
		localStorage.address = document.getElementById("sAddress1").value;
	}
	document.getElementById("sDate").value = "";
	document.getElementById("time").value = "";
	document.getElementById("sLatitude").value = "";
	document.getElementById("sLongitude").value = "";
	document.getElementById("eLatitude").value = "";
	document.getElementById("eLongitude").value = "";
	document.getElementById("sAddress1").value = "";
	document.getElementById("eAddress1").value = "";
	document.getElementById("phoneNumber").value = "";
	if(typeof driverList != "" && driverList!=null){
		 for (var i=0; i<driverList;i++){
			 document.getElementById('dchk'+i).checked=false;
		 }
	 }
	 if(typeof vehicleList != "" &&vehicleList!=null){
		 for (var i=0;i<vehicleList;i++){
			document.getElementById('vchk'+i).checked=false;
		 }
	 }
	$.mobile.changePage("#home");
	if (localStorage.loginCheck == "loggedIn") {
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		Android.homePage("");
		Init();
	} else {
		$("#popupBasic").html(
				"Sorry your job is not booked,Please try again !!");
		$("#popupBasic").popup("open");
		$(".Loggedout").show();
		$(".LoggedIn").hide();
//		Android.homePage("exit home");
	}
	$("#afterBooking").show();
	$("#afterPickUp").hide();
	$("#pickUp").hide();
	$("#afterMap").hide();
	$(".ui-loader").hide();
	if (text != "" && text != null) {
		var msg = "";
		if (parseInt(text) != 'NaN') {
			msg = "Trip was booked !TripID:" + text;
			localStorage.tripId = text;
			$("#afterBooking").show();
			$("#pickUp").hide();
			$("#afterPickUp").hide();
			$("#afterMap").hide();
		} else {
			msg = "Trip not Booked";
		}
		setMessage(msg);
		dialogShow();
	}
	$("#specialFLags").hide();
}
function totalling() {
	var tip1 = document.getElementById("tip").value;
	total = parseInt(amount) + parseInt(tip1) - parseInt(initialTip);
	document.getElementById("totalAmount").innerHTML = total + ".00";
}
function showCards() {
	$.mobile.showPageLoadingMsg();
	showCards1();
}
function hideDetails() {
	$("#jobPopUp").hide();
}
function showCards1() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = "http://" + document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=showCards";
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	$("#existingCards").show();
	document.getElementById("existingCardsTbody").innerHTML = "";
	var obj = "{\"cardArray\":" + text + "}";
	var jsonObj = JSON.parse(obj.toString());
	$("#existingCardsTbody").append("<tr>");
	$("#existingCardsTbody").append("<td> Card Number");
	$("#existingCardsTbody").append("</td>");
	$("#existingCardsTbody").append("</tr>");
	for (var i = 0; i < jsonObj.cardArray.length; i++) {
		$("#existingCardsTbody").append("<tr>");
		$("#existingCardsTbody").append(
				"<td> <input type=\"radio\" name=\"cards\" id=\"cards\" value=\""
						+ jsonObj.cardArray[i].getNumber + "\">"
						+ jsonObj.cardArray[i].getCard + "</input>" + "</td>");
		$("#existingCardsTbody").append("</tr>");
	}
	$("#existingCardsTbody").append("</tr>");
	$("#existingCardsTbody").append("<tr>");
	$("#existingCardsTbody").append(
			"<td> <input type=\"button\" name=\"pay\" id=\"pay\" value=\"pay\">"
					+ "</td>");
	$("#existingCardsTbody").append("</tr>");
	$.mobile.hidePageLoadingMsg();
}
function test(tripId) {
	document.getElementById("tripId").value = tripId;
	$.mobile.changePage("#paymentScreen");
}
function showPaymentTable() {
	$("#cardDetails").show();
}
function cancelTrip(tripId) {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (tripId == '') {
		tripId = localStorage.tripId;
		$("#jobConfirmation").hide();
		$("#pickUp").show()
	}
	var url = "http://" + document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=cancelTrip&tripId=" + tripId
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		var msg = "Your Trip is Cancelled";
		setMessage(msg);
		dialogShow();
		$("#pickUp").show();
	}
}
function hideCancellation() {
	$("#jobConfirmation").hide();
}
function deleteAddress(x, key, switches) {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = "http://" + document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=deleteFavAddress&addKey=" + key;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1 && switches == "") {
		setMessage("Deleted Succesfully");
		dialogShow();
	} else {
		document.getElementById('favAddressTbody').innerHTML = "";
		var url = "http://"
				+ document.getElementById('webUrl').value
				+ "/TDS/CustomerMobileAction?event=favouriteAddresses&responseType=HTML5";
		xmlhttp.open("GET", url, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
				+ localStorage.sessionId);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"favAddressD\":" + text + "}";
		jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.favAddressD.length; i++) {
			$("#favAddressTbody").append("<tr>");
			$("#favAddressTbody").append(
					"<td>" + jsonObj.favAddressD[i].addKey + "</td>");
			$("#favAddressTbody").append(
					"<td>" + jsonObj.favAddressD[i].address + "</td>");
			$("#favAddressTbody").append(
					"<td>" + jsonObj.favAddressD[i].tagAdd + "</td>");
			$("#favAddressTbody")
					.append(
							"<td><input type=\"button\" id=\"deleteFavAdd\" value=\"Delete\" onclick=\"deleteAddress(this,"
									+ jsonObj.favAddressD[i].addKey
									+ ",1);\"/></td>");
			$("#favAddressTbody").append("</tr>");
		}
	}
}
function payFare() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = "http://" + document.getElementById('webUrl').value
			+ "/TDS/CustomerServiceAjax?event=paymentFromCreditCard&tripId="
			+ document.getElementById("tripId").value + "amount="
			+ document.getElementById("totalAmount").innerHTML;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		var url1 = "http://" + document.getElementById('webUrl').value
				+ "/TDS/CustomerMobileAction?event=payFare";
		xmlhttp.open("GET", url1, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
				+ localStorage.sessionId);
		xmlhttp.send(null);
		var text1 = xmlhttp.responseText;
		if (text1 > 1) {
			alert("payment Done");
		}
	}
}
$('#home').live("pageshow", function() {
	// Init();
	loadLocation();
	// updateDisplay();
});
/*
 * $(document).ready(function() { $(".tracks_marquee").hover(function() {
 * $(this).attr("scrollamount","1"); $(this).start(); }, function() {
 * $(this).attr("scrollamount","0"); $(this).stop(); }) })
 */

$(document).delegate('#home', 'pageinit', function() {
	$("#popupBasic").popup();

	$("#popupLogin").bind({
		popupafteropen : function(event, ui) {
			setTimeout(function() {
				// do something special
				$("#popupLogin").popup("close");
			}, 1000);

		}
	});
	$("#popupBasic").bind({
		popupafteropen : function(event, ui) {
			setTimeout(function() {
				// do something special
				$("#popupBasic").popup("close");
			}, 1000);
		}
	});
	if (localStorage.loginCheck == "loggedIn") {
		loadLocation();
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		Android.homePage("");
	} else {
		loadLocation();
//		Android.homePage("exit home");
		$(".Loggedout").show();
		$(".LoggedIn").hide();
		// Android.homePage("exit home");
	}
});
$(document)
		.delegate(
				'#paymentScreen',
				'pageinit',
				function() {
					Android.homePage("");
					document.getElementById("paymentTbody").innerHTML = "";

					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					var url = "http://"
							+ document.getElementById('webUrl').value
							+ "/TDS/CustomerMobileAction?event=charges&tripId="
							+ document.getElementById("tripId").value;
					xmlhttp.open("GET", url, false);
					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);
					xmlhttp.send(null);
					var text = xmlhttp.responseText;
					if (text != "") {
						var obj = "{\"chargesArray\":" + text + "}";
						var jsonObj = JSON.parse(obj.toString());
						for (var i = 0; i < jsonObj.chargesArray.length; i++) {
							amount = parseInt(total)
									+ parseInt(jsonObj.chargesArray[i].getAmount);
							total = amount;
							// $("#paymentTbody").innerHTML("");
							$("#paymentTbody").append("<tr>");
							if ((jsonObj.chargesArray[i].getChargeType) == 0) {
								initialTip = jsonObj.chargesArray[i].getAmount;
								$("#paymentTbody").append(
										"<td>" + "Tip" + "</td>");
								$("#paymentTbody")
										.append(
												"<td> <input type=\"text\" name=\"tip\" id=\"tip\" value=\""
														+ jsonObj.chargesArray[i].getAmount
														+ "\" onblur=\"totalling()\">"
														+ "</td>");
							} else {
								$("#paymentTbody")
										.append(
												"<td>"
														+ jsonObj.chargesArray[i].getType
														+ "</td>");
								$("#paymentTbody")
										.append(
												"<td>"
														+ jsonObj.chargesArray[i].getAmount
														+ "</td>");
							}

						}
						$("#paymentTbody").append("<tr>");
						$("#paymentTbody").append("<td> Total </td>");
						$("#paymentTbody")
								.append(
										"<td id=\"totalAmount\">" + amount
												+ ".00</td>");
						$("#paymentTbody").append("</tr>");
						$("#paymentTbody").append("<tr>");
						$("#paymentTbody")
								.append(
										"<td> <input type=\"button\" name=\"Paymentbutton\" id=\"Paymentbutton\" value=\"Pay\" onclick=\"showPaymentTable()\"");
						$("#paymentTbody").append("</td>");
						$("#paymentTbody")
								.append(
										"<td> <input type=\"button\" name=\"payFromExistingCards\" id=\"payFromExistingCards\" value=\"Existing Cards\" onclick=\"showCards()\"");
						$("#paymentTbody").append("</td>");
						$("#paymentTbody").append("</tr>");
					} else {
						loadLocation();

					}
				});
$(document)
		.delegate(
				'#favAddressDetails',
				'pageinit',
				function() {

					Android.homePage("");
					document.getElementById("favAddressTbody").innerHTML = "";
					var url = "http://"
							+ document.getElementById('webUrl').value
							+ "/TDS/CustomerMobileAction?event=favouriteAddresses&HTML5=yes";
					var xmlhttp = null;
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.open("GET", url, false);

					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);
					xmlhttp.send(null);
					var text = xmlhttp.responseText;

					var obj = "{\"favAddressD\":" + text + "}";
					jsonObj = JSON.parse(obj.toString());
					for (var i = 0; i < jsonObj.favAddressD.length; i++) {
						$("#favAddressTbody").append("<tr>");
						$("#favAddressTbody").append(
								"<td>" + jsonObj.favAddressD[i].addKey
										+ "</td>");
						$("#favAddressTbody").append(
								"<td>" + jsonObj.favAddressD[i].address
										+ "</td>");
						$("#favAddressTbody").append(
								"<td>" + jsonObj.favAddressD[i].tagAdd
										+ "</td>");
						$("#favAddressTbody")
								.append(
										"<td><input type=\"button\" id=\"deleteFavAdd\" value=\"Delete\" onclick=\"deleteAddress(this,"
												+ jsonObj.favAddressD[i].addKey
												+ ",1);\"/></td>");
						$("#favAddressTbody").append("</tr>");
					}
				});
$(document)
		.delegate(
				'#TripsOnBoard',
				'pageinit',
				function() {

					Android.homePage("");
					// alert("tripsOnboard");
					document.getElementById("tripHistoryTbody").innerHTML = "";
					var url = "http://"
							+ document.getElementById('webUrl').value
							+ "/TDS/CustomerMobileAction?event=tripHistory&responseType=HTML5";
					var xmlhttp = null;
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.open("GET", url, false);
					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);

					xmlhttp.send(null);
					var textValue = xmlhttp.responseText;
					// alert("textvalue:"+textValue);
					if (textValue != "") {
						var obj = "{\"tripHistory\":" + textValue + "}";
						jsonObj = JSON.parse(obj.toString());
						for (var i = 0; i < jsonObj.tripHistory.length; i++) {
							$("#tripHistoryTbody").append("<tr>");
							$("#tripHistoryTbody").append(
									"<td>" + jsonObj.tripHistory[i].getName
											+ "</td>");
							$("#tripHistoryTbody").append(
									"<td>" + jsonObj.tripHistory[i].getTripId
											+ "</td>");
							$("#tripHistoryTbody")
									.append(
											"<td>"
													+ jsonObj.tripHistory[i].getPhoneNumber
													+ "</td>");
							$("#tripHistoryTbody").append(
									"<td>" + jsonObj.tripHistory[i].getAddress
											+ "</td>");
							$("#tripHistoryTbody").append(
									"<td>" + jsonObj.tripHistory[i].getCity
											+ "</td>");
							$("#tripHistoryTbody").append("</tr>");
						}
					}
				});
$(document)
		.delegate(
				'#reserveTrips',
				'pageinit',
				function() {
					Android.homePage("");
					document.getElementById("reservedTripsTbody").innerHTML = "";
					var url = "http://"
							+ document.getElementById('webUrl').value
							+ "/TDS/CustomerMobileAction?event=tripSummary";
					var xmlhttp = null;
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.open("GET", url, false);
					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);
					xmlhttp.send(null);
					var textSum = xmlhttp.responseText;

					if (textSum != "") {
						var obj = "{\"tripSummary\":" + textSum + "}";
						jsonObj = JSON.parse(obj.toString());
						for (var i = 0; i < jsonObj.tripSummary.length; i++) {

							$("#reservedTripsTbody").append("<tr>");
							$("#reservedTripsTbody").append(
									"<td>" + jsonObj.tripSummary[i].getName
											+ "</td>");
							$("#reservedTripsTbody").append(
									"<td >" + jsonObj.tripSummary[i].getTripId
											+ "</td>");
							$("#reservedTripsTbody").append(
									"<td>" + jsonObj.tripSummary[i].getAddress
											+ "</td>");
							$("#reservedTripsTbody")
									.append(
											"<td>"
													+ jsonObj.tripSummary[i].getPhoneNumber
													+ "</td>");
							// $("#reservedTripsTbody").append(
							// "<td style='display:none;'> <input
							// type=\"hidden\" name=\"addressReserved\"
							// id=\"addressReserved\" value="+
							// jsonObj.tripSummary[i].getAddress+"/>"
							// + "</td>");
							$("#reservedTripsTbody").append(
									"<td>" + jsonObj.tripSummary[i].getCity
											+ "</td>");
							$("#reservedTripsTbody")
									.append(
											"<td> <input type=\"button\" name=\"cancelTrip\" id=\"cancelTrip\" value=\"Cancel\" onClick=\"cancelTrip("
													+ jsonObj.tripSummary[i].getTripId
													+ ");\" /></td>");
							$("#reservedTripsTbody").append("</tr>");
						}
					}
				});
$(document)
		.delegate(
				'#accountSummary',
				'pageinit',
				function() {

					// Android.homePage("");
					document.getElementById("accountSummaryTbody").innerHTML = "";
					var url = "http://"
							+ document.getElementById('webUrl').value
							+ "/TDS/CustomerMobileAction?event=clientAccountSummary&responseType=HTML5";
					var xmlhttp = null;
					if (window.XMLHttpRequest) {
						xmlhttp = new XMLHttpRequest();
					} else {
						xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
					}
					xmlhttp.open("GET", url, false);
					xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
							+ localStorage.sessionId);
					xmlhttp.send(null);
					var text = xmlhttp.responseText;

					if (text != "") {
						var obj = "{\"accountSum\":" + text + "}";
						jsonObj = JSON.parse(obj.toString());

						for (var i = 0; i < jsonObj.accountSum.length; i++) {
							$("#accountSummaryTbody").append("<tr>");
							$("#accountSummaryTbody")
									.append(
											"<td>"
													+ jsonObj.accountSum[i].getPaymentType
													+ "</td>");
							$("#accountSummaryTbody")
									.append(
											"<td><a href='' data-rel='external'>"
													+ jsonObj.accountSum[i].getVoucherNumber
													+ "</td>");
							$("#accountSummaryTbody")
									.append(
											"<td>"
													+ jsonObj.accountSum[i].getVoucherExpiryDate
													+ "</td>");
							$("#accountSummaryTbody")
									.append(
											"<td>"
													+ jsonObj.accountSum[i].getCardNumber
													+ "</td>");
							$("#accountSummaryTbody")
									.append(
											"<td>"
													+ jsonObj.accountSum[i].getCardExpiryDate
													+ "</td>");
							$("#accountSummaryTbody").append("</tr>");
						}
					}
				});
function registerUser() {
	var userId = document.getElementById("userIdForR").value;
	var passWord = document.getElementById("passwordForR").value;
	var userName = document.getElementById("userNameForR").value;
	var email = document.getElementById("emailAddressForR").value;
	var phNo = document.getElementById("phoneNumberRegistration").value;
	var url = "http://"
			+ document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=clientRegistration&responseType=HTML5&ccode="
			+ document.getElementById('ccode').value + "&os=G&userId=" + userId
			+ "&password=" + passWord + "&userName=" + userName + "&emailId="
			+ email + "&phoneNumber=" + phNo + "&registerButton=yes";
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == "Registered") {
		$.mobile.changePage("#login_page1");

		$(
				"<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>Registered Successfully,Now You can login!!</h1></div>")
				.css({
					"display" : "block",
					"opacity" : 0.96,
					"top" : $(window).height() / 2,
					"align" : "center"
				}).appendTo($.mobile.pageContainer).delay(800).fadeOut(4000,
						function() {
							$(this).remove();
						});
	} else {
		$(
				"<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>Not Registered, Try Again!!</h1></div>")
				.css({
					"display" : "block",
					"opacity" : 0.96,
					"top" : $(window).height() / 2,
					"align" : "center"
				}).appendTo($.mobile.pageContainer).delay(800).fadeOut(4000,
						function() {
							$(this).remove();
						});
	}
}
function reservedPopUp(address) {
	alert("calling");
}
function accountRegistrationForUser() {
	var userId = "";
	var paymentType = document.getElementById("paymentTypeForAcctReg").value;
	var userName = document.getElementById("userNameForAcctReg").value;
	var cardNumber = document.getElementById("cardNumberForAcctReg").value;
	var voucherNumber = document.getElementById("voucherNumberForAcctReg").value;
	var cardExpiryDate = document.getElementById("cardExpiryDateForAcctReg").value;
	var url = "http://"
			+ document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=clientAccountRegistration&responseType=HTML5&ccode="
			+ document.getElementById('ccode').value + "&os=G&userName="
			+ userName + "&paymentType=" + paymentType + "&cardNumber="
			+ cardNumber + "&voucherNumber=" + voucherNumber
			+ "&cardExpiryDate=" + cardExpiryDate
			+ "&registerButton=yes&mode=1";
	// alert(url);
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	$(window).attr('location', '#home');
	$.mobile.changePage("#home");
	if (localStorage.loginCheck == "loggedIn") {
		$(".LoggedIn").show();
		$(".Loggedout").hide();
		Android.homePage("");
		// Init();
	} else {
		$(".Loggedout").show();
		$(".LoggedIn").hide();
//		Android.homePage("exit home");
	}
	/*
	 * if(text=="Registered"){ $.mobile.changePage("#login_page1"); $( "<div
	 * class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>Registered
	 * Successfully,Now You can login!!</h1></div>" ) .css({ "display":
	 * "block", "opacity": 0.96, "top": $(window).height()/2 ,"align":"center" })
	 * .appendTo( $.mobile.pageContainer ) .delay( 800 ) .fadeOut( 4000,
	 * function() { $( this ).remove(); }); }else{ $( "<div class='ui-loader
	 * ui-overlay-shadow ui-body-e ui-corner-all'><h1>Not Registered, Try
	 * Again!!</h1></div>" ) .css({ "display": "block", "opacity": 0.96,
	 * "top": $(window).height()/2 ,"align":"center" }) .appendTo(
	 * $.mobile.pageContainer ) .delay( 800 ) .fadeOut( 4000, function() { $(
	 * this ).remove(); }); }
	 */
}
function showNav() {
	if (localStorage.loginCheck == 'loggedIn') {
		if ($('#navlist').css('display') == 'none') {
			$("#customerImage1").hide();
			$("#navlist").show();
			$("#showingNav").val("1");
//			Android.homePage("hide Option");
		} else {
			$("#customerImage1").show();
			$("#navlist").hide();
			$("#showingNav").val("0");
//			Android.homePage("home");
		}
	} else {
		var msg = "Please Log In To Use More.";
		setMessage(msg);
		dialogShow();
	}
}
function logout() {
	var url = "http://"
			+ document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=clientLogin&responseType=HTML5&submit=YES&ccode="
			+ document.getElementById('ccode').value + "&os="
			+ document.getElementById('os').value + "&userId="
			+ localStorage.userId;
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == "loggedOut") {
		hideMarkers();
		$(".Loggedout").show();
		$(".LoggedIn").hide();
//		Android.homePage("exit home");
		localStorage.loginCheck = text;
		localStorage.userPermanentName = "";
		localStorage.userEmailId = "";
		localStorage.userId = "";
		localStorage.phoneNo = "";
		localStorage.tripId = "";
		document.getElementById("userName").value = "";
		document.getElementById("phoneNumber").value = "";
		$('#navlist').hide();
		StopTimer();
		/* localStorage.clear(); */
	}
	document.getElementById("loadFlagValue").value="0";
}
function loadSettings() {
	$('#ccode').val(localStorage.ccode);
	// $('#webUrl').val(localStorage.webUrl);
}
function saveSettings() {
	localStorage.ccode = $('#ccode').val();
	// localStorage.webUrl = $('#webUrl').val();
}
function setMessage(message) {
	document.getElementById("messageTo").innerHTML = message;
}
function showFullDetails() {
	// alert("Show full details
	// Status:"+localStorage.SA+"TripId:"+localStorage.tripId);
	document.getElementById("jobPopupMessage").innerHTML = "Your Trip Starts "
			+ "From " + localStorage.address + " " + ""
			+ "and Your Unique Id is " + localStorage.tripId;
	$("#jobPopUp").show();

}
function cancelConfirmation() {
	$("#jobConfirmation").show();
	$("#jobPopUp").hide();
}

jQuery.fn.center = function() {
	this.css("position", "absolute");
	this.css("top", Math.max(0,
			(($(window).height() - $(this).outerHeight()) / 5)
					+ $(window).scrollTop())
			+ "px");
	this.css("left", "0px");
	// this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth())
	// / 2) +
	// $(window).scrollLeft()) + "px");
	return this;
}
// jQuery(document).ready(function($){
// $(".status").center("true");
//                      
// });
$(document).bind('mobileinit', function() {

	$.mobile.loader.prototype.options.text = "loading";
	$.mobile.loader.prototype.options.textVisible = false;
	$.mobile.loader.prototype.options.theme = "a";
	$.mobile.loader.prototype.options.html = "";
});
function loginToServer() {

	// $.mobile.pageLoading();
	var xmlhttp = null;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var rememberMe = "";
	if (document.getElementById("rememberMe").checked == true) {
		rememberMe = 1;
	}
	var url = 'http://' + document.getElementById('webUrl').value + '/TDS/'
			+ 'CustomerMobileAction?event=clientLogin&userId='
			+ document.getElementById('userId').value + '&password='
			+ document.getElementById('password').value + '&assoccode='
			+ localStorage.ccode + '&loginOrLogout=1&rememberMe=' + rememberMe;
	// alert(url);
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	// alert(text);
	if (text.indexOf(";") != -1) {
		var arrayVal = text.split(";");
		for (var i = 0; i < arrayVal.length; i = i + 1) {
			if (arrayVal[i].indexOf("status") != -1) {
				var status = arrayVal[i].split("=");
				if (status[0] == "status") {
					// alert(status[1]);
					localStorage.loginCheck = status[1];
				}
			} else if (arrayVal[i].indexOf("userId") != -1) {
				var userId = arrayVal[i].split("=");
				if (userId[0] == "userId") {
					localStorage.userId = userId[1];
				}
			} else if (arrayVal[i].indexOf("phoneNo") != -1) {
				var phNo = arrayVal[i].split("=");
				if (phNo[0] == "phoneNo") {
					localStorage.phoneNo = phNo[1];
				}
			} else if (arrayVal[i].indexOf("name") != -1) {
				var nameVal = arrayVal[i].split("=");
				if (nameVal[0] == "name") {
					localStorage.userPermanentName = nameVal[1];
				}
			} else if (arrayVal[i].indexOf("emailId") != -1) {
				var emailId = arrayVal[i].split("=");
				if (emailId[0] == "emailId") {
					localStorage.userEmailId = emailId[1];
				}
			} else if (arrayVal[i].indexOf("sessionId") != -1) {
				var sessionId = arrayVal[i].split("=");
				if (sessionId[0] == "sessionId") {
					localStorage.sessionId = sessionId[1];

				}
			}
		}
	}
	// $.mobile.pageLoading( true );
	$(window).attr('location', '#home');
	$.mobile.changePage("#home");
	if (localStorage.loginCheck == "loggedIn") {
		$(".Loggedout").hide();
		$(".LoggedIn").show();
		$("#popupBasic").html("<p>You are LoggedIn Successfully!! <p>");
		$("#popupBasic").popup("open");
		// document.getElementById("nameOfCustomer").innerHTML=localStorage.userPermanentName;
		Android.homePage("");
		Init();
	} else {
		$("#popupBasic").html("<p>Please try Again !! <p>");
		$("#popupBasic").popup("open");
		$(".Loggedout").show();
		$(".LoggedIn").hide();
//		Android.homePage("exit home");
	}
}
function retry() {
	$.mobile.changePage("#pageinit");
}
function updateGCMKey(pushKey) {
	// alert("in--->" + pushKey);
	// Android.homePage("home");
	if (pushKey != "") {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'http://' + document.getElementById('webUrl').value + '/TDS/'
				+ 'CustomerMobileAction?event=gPI&register=YES&pushKey='
				+ pushKey;
		xmlhttp.open("GET", url, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
				+ localStorage.sessionId);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		// AndroidpushKey();

	}
}

$("[data-rel=back]").click(function() {
	// do the var magic
	if (document.getElementById("navlist") != null) {
		$('#navlist').hide();
	}
});
$(document).delegate('div[data-role=page]', 'pageinit', function() {

	loadSettings();
	// jobLocation();
	var internet = "true";
	if (internet == "true") {
		if (document.getElementById('ccode').value == "") {
			// var ccode = prompt("Please enter your Company Code", "");
			var ccode = "123";
			if (ccode != null) {
				document.getElementById("ccode").value = ccode;
				/*
				 * $( "<div class='ui-loader ui-overlay-shadow ui-body-e
				 * ui-corner-all'><h1>Metro Taxi</h1></div>") .css({
				 * "display" : "block", "opacity" : 0.96, "top" :
				 * $(window).height() / 2, "align" : "center"
				 * }).appendTo($.mobile.pageContainer) .delay(800).fadeOut(4000,
				 * function() { $(this).remove(); });
				 */
				saveSettings();
			} else {
				alert("Enter your company code");
			}
			$("#addressCustomerTD").show();
		}
		if (localStorage.loginCheck == "loggedIn") {
			$(".LoggedIn").show();
			$(".Loggedout").hide();
			Android.homePage("");
			// Init();
		} else {
			loadLocation();
			$(".Loggedout").show();
			$(".LoggedIn").hide();
//			Android.homePage("exit home");
		}
	} else {
		// document.getElementById("noInternetConnection").style
		$("#noInternetConnection").show();
	}
});
var arrMarkerDriver = [];
var arrMarkerJob = [];

function companyCode() {
	// var ccode = prompt("Please enter your Company Code", "");
	var ccode = "105";
	if (ccode != null) {
		document.getElementById("ccode").value = ccode;
	} else {
		alert("Enter your company code");
	}
}

var mins, secs, TimerRunning, TimerID, TimerID1, thisJobIsRunning, thisJobIsRunningForDriver;
TimerRunning = false, i = 1;

function Init()// call the Init function when u need to start the timer
{
	mins = 1;
	secs = 0;
	/*
	 * var pushKey = Android.pushKey(); // alert("I am in--->"+pushKey);
	 * 
	 * updateGCMKey(pushKey);
	 */
	StopTimer();
}
function StopTimer() {
	if (TimerRunning)
		clearTimeout(TimerID);
	TimerRunning = false;
	secs = 10;
	if (localStorage.loginCheck == "loggedIn") {
		StartTimer();
	}
}
function StartTimer() {
	TimerRunning = true;
	TimerID = self.setTimeout("StartTimer()", 1000);
	if (secs == 0) {
		StopTimer();
		jobLocation();
		driverLocation();
		if (document.getElementById("showingNav").value == "1") {
			$("#customerImage1").hide();
		}
		 updateDisplay();
	}
	thisJobIsRunning = false;
	thisJobIsRunningForDriver = false;
	secs--;

}
function driverLocation() {
	var latitude = "";
	var longitude = "";
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = "";
	if (localStorage.tripId != null && localStorage.tripId != ""
			&& localStorage.tripId != "undefined") {
		url = 'http://' + document.getElementById('webUrl').value + '/TDS/'
				+ 'CustomerMobileAction?event=driverLocation&tripId='
				+ localStorage.tripId;
	} else {
		url = 'http://' + document.getElementById('webUrl').value + '/TDS/'
				+ 'CustomerMobileAction?event=driverLocation';
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != null && text != "") {
		var obj = "{\"driverLocation\":" + text + "}";
		var jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.driverLocation.length; i++) {
			latitude = jsonObj.driverLocation[i].getLatitude;
			longitude = jsonObj.driverLocation[i].getLongitude;
		}
		if (latitude != "") {
			document.getElementById("driverLati").value = latitude;
			document.getElementById("driverLongi").value = longitude;
		}
	}
	if (document.getElementById("dragend").value == 'false') {
		// loadLocation();
		updateDisplay();
	}
}
function fixPickUp() {
	$("#sAddress1").val(localStorage.address);
	$("#phoneNumber").val(localStorage.phoneNo);
	$("#sLatitude").val(localStorage.lat);
	$("#sLongitude").val(localStorage.longi);
	// setMessage("Enter D/O Or press book")
	// dialogShow();
	$("#popupBasic").html("<p>Enter D/o Address Or Book Job !! <p>");
	$("#popupBasic").popup("open");
	$("#pickUp").hide();
	$("#afterPickUp").show();
	$("#afterMap").hide();
	loadSpecialFlags();
}
function fixDropOff() {
	if (document.getElementById("sAddress1").value != "") {
		$("#eAddress1").val(localStorage.address);
		$("#eLatitude").val(localStorage.lat);
		$("#eLongitude").val(localStorage.longi);

		// setMessage("Press book")
		// dialogShow();
	} else {
		setMessage("Enter P/U then do D/O")
		dialogShow();
	}
}
/*
 * function todayDate() { var curdate = new Date(); var cDate =
 * curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate(); var
 * cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 :
 * "0"+(curdate.getMonth()+1); var cYear = curdate.getFullYear(); var hors =
 * curdate.getHours() ; var min = curdate.getMinutes() >=10 ?
 * curdate.getMinutes() : "0"+curdate.getMinutes(); //it is pm if hours from 12
 * onwards var suffix = (hors >= 12)? 'PM' : 'AM';
 * 
 * //only -12 from hours if it is greater than 12 (if not back at mid night)
 * hors = (hors > 12)? hors -12 : hors; if(suffix=='PM'){ hors=hors>=10 ? hors :
 * "0"+hors; } //if 00 then it is 12 am hors = (hors == '00')? 12 : hors; hors =
 * hors >=10 ?hors : "0"+hors; var hrsmin = hors+":"+min+" "+suffix;
 * 
 * document.getElementById("sDate").value =cMonth+"/"+cDate+"/"+cYear;
 * document.getElementById("time").value=hrsmin; }
 */
function bookJob() {
	Android.homePage("");
	// todayDate();
	if (document.getElementById("sAddress1").value != "") {
		$.mobile.changePage("#bookForm");
		// alert(localStorage.userPermanentName);
		$("#userName").val(localStorage.userPermanentName);

		if (localStorage.loginCheck == "loggedIn") {
			$("#favouriteAddresses").show();
		} else {
			$("#favouriteAddresses").hide();
		}

	} else {
		setMessage("Enter Atleast P/U address to book")
		dialogShow();
	}
}

function getDriverDetails(driverId) {
	// setMessage(msg);
	$("#messageTo").html(
			'<iframe style="height: 150px;width:120px;" src=http://' + document.getElementById('webUrl').value
					+ '/TDS/'
					+ 'OpenRequestAjax?event=getDriverDocument&driverId='
					+ driverId + '></iframe>');
	dialogShow();
	// $("#popupBasic").popup("open");
}

function Pad(number) // pads the mins/secs with a 0 if its less than 10
{
	if (number < 10)
		number = 0 + "" + number;
	return number;
}
function jobLocation() {

	var latitude = "";
	var longitude = "";
	var status = "";
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = ""
	if (localStorage.tripId != null && localStorage.tripId != ""
			&& localStorage.tripId != "undefined") {
		url = 'http://' + document.getElementById('webUrl').value + '/TDS/'
				+ 'CustomerMobileAction?event=jobLocation&tripId='
				+ localStorage.tripId;
	} else {
		url = 'http://' + document.getElementById('webUrl').value + '/TDS/'
				+ 'CustomerMobileAction?event=jobLocation';
	}
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != "" && text != null) {
		var obj = "{\"jobLocation\":" + text + "}";
		var jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.jobLocation.length; i++) {
			latitude = jsonObj.jobLocation[i].getLatitude;
			longitude = jsonObj.jobLocation[i].getLongitude;
			status = jsonObj.jobLocation[i].SA;
			localStorage.SA = status;
			localStorage.tripId = jsonObj.jobLocation[i].TI;
			localStorage.driver = jsonObj.jobLocation[i].driver;
			localStorage.cab = jsonObj.jobLocation[i].cab;
		}
		var jobStatus = document.getElementById("jobStatus").value;
		if (latitude != "") {
			document.getElementById("jobLati").value = latitude;
			document.getElementById("jobLongi").value = longitude;
			document.getElementById("jobStatus").value = status;
		}
		$("#popupBasic").popup("close");
		if (localStorage.tripId != "" || localStorage.tripId != null
				|| localStorage.tripId != "undefined") {
			if (status < '4' || status == '8') {
				$("#statusIcon").html('Contacting Drivers');
			}
		}
		if (status != jobStatus) {
			if (status == '4') {
				$(".status").css('background-color', "#FFFF00");
				$("#statusIcon").html(localStorage.address);
				$("#popupBasic").html("<p>Dispatch Has been Started <p>");
				$("#popupBasic").popup("open");
				// setMessage("your job dispatch started")
				// dialogShow();
				// alert("");
			} else if (status == '40') {
				setMessage("your job allocated");
				$(".status").css('background-color', "#A9F5F2");
				$("#statusIcon").html(localStorage.address);
				$("#popupBasic").html(
						"<p>Job Allocated !!</p><br/>DriverID:"
								+ localStorage.driver + ":CabNo:"
								+ localStorage.cab);
				$("#popupBasic").popup("open");
				getDriverDetails(localStorage.driver);
				// dialogShow();
				// alert("your job allocated");
			} else if (status == '25') {
				setMessage("your job allocated");
				$(".status").css('background-color', "#A9F5F2");
				$("#statusIcon").html(localStorage.address);
				$("#popupBasic").html(
						"<p>Job Accepted !!</p><br/>DriverID:"
								+ localStorage.driver + ":CabNo:"
								+ localStorage.cab);
				$("#popupBasic").popup("open");
				getDriverDetails(localStorage.driver);
				// dialogShow();
				// alert("your job allocated");
			} else if (status == '43') {
				$(".status").css('background-color', "#FAAC58");
				$("#statusIcon").html(localStorage.address);
				$("#popupBasic").html(
						"<p>Driver is OnRoute !! </p><br/>DriverID:"
								+ localStorage.driver + ":CabNo:"
								+ localStorage.cab);
				$("#popupBasic").popup("open");
				// setMessage("Driver is Onroute");
				// dialogShow();
				// alert('Driver is Onroute');
			} else if (status == '45') {
				$("#popupBasic").html(
						"<p>Driver is waiting outside!! </p><br/>DriverID:"
								+ localStorage.driver + ":CabNo:"
								+ localStorage.cab);
				$("#popupBasic").popup("open");
				$(".status").css('background-color', "#74DF00");
				$("#statusIcon").html(localStorage.address);

				// setMessage("Your trip is started")
				// dialogShow();
				// alert('Your trip is started');
			}else if (status == '47') {
				$("#popupBasic").html(
						"<p>Your trip is Started!! </p><br/>DriverID:"
								+ localStorage.driver + ":CabNo:"
								+ localStorage.cab);
				$("#popupBasic").popup("open");
				$(".status").css('background-color', "#74DF00");
				$("#statusIcon").html(localStorage.address);

				// setMessage("Your trip is started")
				// dialogShow();
				// alert('Your trip is started');
			} else if (status == '48') {
				$(".status").css('background-color', "red");
				$("#statusIcon").html(localStorage.address);
//				$("#popupBasic").html("<p>NoShow !! <p>");
//				$("#popupBasic").popup("open");
				 setMessage("Driver Cant Find You In The Address")
				 dialogShow();
				// alert('driver reported NoShow');
			} else if (status == '25') {
				$(".status").css('background-color', "red");
				$("#popupBasic").html("<p>Couldn't Find Service !! <p>");
				$("#popupBasic").popup("open");
				// setMessage("couldnt find drivers")
				// dialogShow();
				$("#statusIcon").html("CFD");
				// alert('couldnt find drivers');
			} else if (status == '30') {
				$(".status").css('background-color', "red");
				$("#statusIcon").html(localStorage.address);
				$("#popupBasic")
						.html("<p>Your job Has been BroadCasted !! <p>");
				$("#popupBasic").popup("open");
				// setMessage("your trip is broadcasted")
				// dialogShow();
				// alert('your trip is broadcasted');
				// document.getElementById("popuprel2").innerHTML="your trip is
				// broadcasted";
				// popupStatus();
			} else if (status == '61') {
				$(".status").css('background-color', "red");

				// setMessage("your trip is ended.Thanks for using")
				// dialogShow();
				$("#statusIcon").html(localStorage.address);
				$("#popupBasic").html(
						"<p>Trip Completed,Thank you For Using!!<p>");
				$("#popupBasic").popup("open");
				$("#statusIcon").html("");
				document.getElementById("jobLati").value = "";
				document.getElementById("jobLongi").value = "";
				// alert('your trip is ended.Thanks for using');
				loadLocation();
				localStorage.tripId = "";
				document.getElementById("jobStatus").value = "61";
				$("#statusIcon").hide();
				$("#pickUp").show();
			} else if (status == '50') {
				$(".status").css('background-color', "red");

				// setMessage("your trip is Cancelled")
				// dialogShow();
				$("#statusIcon").html(localStorage.address);
				$("#popupBasic").html("<p>Trip Canceled !! <p>");
				$("#popupBasic").popup("open");
				$("#statusIcon").html("");

				// alert('your trip is Cancelled');
				loadLocation();
				localStorage.tripId = "";
				$("#pickUp").show();
				document.getElementById("jobStatus").value = "50";
			} else if (status == '55') {
				$(".status").css('background-color', "red");
				$("#popupBasic").html("<p>Trip Canceled !! <p>");
				$("#popupBasic").popup("open");
				$("#statusIcon").html("");
				$("#pickUp").show();
				// setMessage("your trip is Cancelled")
				// dialogShow();
				// alert('your trip is Cancelled');
				loadLocation();
				localStorage.tripId = "";
				$("#statusIcon").html(localStorage.address);
				document.getElementById("jobStatus").value = "55";
			}
		}
	} else {
		var jobStatus = document.getElementById("jobStatus").value;
		if (jobStatus == '4' || jobStatus == '43' || jobStatus == '47'
				|| jobStatus == '48' || jobStatus == '40') {
			if (jobStatus == '4') {
				$(".status").css('background-color', "yellow");
			} else if (jobStatus == '47') {
				$(".status").css('background-color', "green");

			} else if (jobStatus == '43') {
				$(".status").css('background-color', "orange");

			} else if (jobStatus == '47') {
				$(".status").css('background-color', "blue");

			}
			$("#popupBasic").html(
					"<p>Trip Completed,Thank you for using !! <p>");
			$("#popupBasic").popup("open");
			$("#statusIcon").html("");
			document.getElementById("jobLati").value = "";
			document.getElementById("jobLongi").value = "";
			$("#statusIcon").hide();
			$("#pickUp").show();
			// setMessage("your trip is ended.Thanks for using")
			// dialogShow();
			// alert('your trip is ended.Thanks for using');
			loadLocation();
			document.getElementById("jobStatus").value = "";
		}
	}
	if (document.getElementById("dragend") == 'false') {
		// loadLocation();
		updateDisplay();
	}
}
var latitude;
var longitude;
var accuracy;
var map;
var geocoder;
var markerDriver;
var markerjob;

function loadLocation() {
	navigator.geolocation.getCurrentPosition(success_handler_first,
			error_handler, {
				enableHighAccuracy : true,
				maximumAge : 60000,
				timeout : 10000,
				frequency : 3000
			});
}
var objectNew=[];
function loadSpecialFlags(){
	if(document.getElementById("loadFlagValue").value=="0"){
		var xmlhttp=null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var driverCounter=0;
		var vehCounter=0;
		var ccode=$('#ccode').val();
		var url='http://' + document.getElementById('webUrl').value
		+ '/TDS/CustomerMobileAction?event=getFlagsForCompany&compCode='+ccode;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var jsonObj = "{\"flagDetails\":"+text+"}";
		var obj = JSON.parse(jsonObj.toString());
		objectNew=JSON.parse(jsonObj.toString());
		if(obj.flagDetails.length>0){
			var $tbl = $('<table style="width:100%"">').attr('id', 'flagValues');
			//var tbl;
			$tbl.append($('<td colspan="20" style="width:100%;text-decoration: underline;" align="center">').text('Driver Vehicle Profile'));	
			document.getElementById("internalFlag").innerHTML="";
			for(var j=0;j<obj.flagDetails.length;j++){
				if(obj.flagDetails[j].SW=="1"){
					$tbl.append($('<tr>').append(
							$('<td style="margin-left:10%">').text(obj.flagDetails[j].LD).append($('<input/>').attr('type', 'checkbox').attr('name', 'dchk'+driverCounter).attr('id', 'dchk'+driverCounter).val(obj.flagDetails[j].K).attr('onclick',"checkGroup(1,'"+driverCounter+"','"+obj.flagDetails[j].GI+"','"+obj.flagDetails[j].LD+"')")),
							$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'dgroup'+driverCounter).attr('id','dgroup'+driverCounter).val(obj.flagDetails[j].GI)),
							$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'dLD'+driverCounter).attr('id','dLD'+driverCounter).val(obj.flagDetails[j].LD))
					));
					driverCounter=Number(driverCounter)+1;
					$("#internalFlag").append($tbl);
				} else {
					$tbl.append($('<tr>').append(
							$('<td style="margin-left:10%">').text(obj.flagDetails[j].LD).append($('<input/>').attr('type', 'checkbox').attr('name', 'vchk'+vehCounter).attr('id', 'vchk'+vehCounter).val(obj.flagDetails[j].K).attr('onclick',"checkGroup(2,'"+vehCounter+"','"+obj.flagDetails[j].GI+"','"+obj.flagDetails[j].LD+"')")),
							$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vgroup'+vehCounter).attr('id','vgroup'+vehCounter).val(obj.flagDetails[j].GI)),
							$('<td>').append($('<input/>').attr('type', 'hidden').attr('name', 'vLD'+vehCounter).attr('id','vLD'+vehCounter).val(obj.flagDetails[j].LD))
					));
					vehCounter=Number(vehCounter)+1;
					$("#internalFlag").append($tbl);
				}
			}
			
			$("#driverProfile").val(driverCounter);
			$("#vehicleProfile").val(vehCounter);
		}
	}
	document.getElementById("loadFlagValue").value="1";
}
function checkGroup(type,row,groupId,longDesc){
	var drProf=$("#driverProfile").val();
	var veProf=$("#vehicleProfile").val();
	if(type==1 && groupId!=""){
		if(document.getElementById("dchk"+row).checked==true){
			for(var i=0;i<drProf;i++){
				if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
					document.getElementById("dchk"+row).checked=false;
					setMessage("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
					 dialogShow();
					break;
				}
			}
			for(var i=0;i<veProf;i++){
				if(document.getElementById("vchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
					document.getElementById("dchk"+row).checked=false;
					 setMessage("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
					 dialogShow();
					 break;
				}
			}
		}
	} else if(type==1 && groupId!=""){
		if(document.getElementById("vchk"+row).checked==true){
			for(var i=0;i<veProf;i++){
				if(document.getElementById("vchk"+i).checked==true && document.getElementById("vgroup"+i).value==groupId && i!=row){
					document.getElementById("vchk"+row).checked=false;
					 setMessage("You cannot select 2 values from same group."+document.getElementById("vLD"+i).value+"&"+longDesc);
					 dialogShow();
					 break;
				}
			}
			for(var i=0;i<drProf;i++){
				if(document.getElementById("dchk"+i).checked==true && document.getElementById("dgroup"+i).value==groupId && i!=row){
					document.getElementById("vchk"+row).checked=false;
					 setMessage("You cannot select 2 values from same group."+document.getElementById("dLD"+i).value+"&"+longDesc);
					 dialogShow();
					 break;
				}
			}
		}
	}
}

function splFlagHide(){
	$("#specialFLags").hide();
}
function splFlagShow(){
	$("#specialFLags").show();
}
function LongClick(map, length) {
	this.length_ = length;
	var me = this;
	me.map_ = map;
	google.maps.event.addListener(map, 'mousedown', function(e) {
		me.onMouseDown_(e)
	});
	google.maps.event.addListener(map, 'mouseup', function(e) {
		me.onMouseUp_(e)
	});
}
LongClick.prototype.onMouseUp_ = function(e) {
	var now = +new Date;
	if (now - this.down_ > this.length_) {
		google.maps.event.trigger(this.map_, 'longpress', e);
	}
};
LongClick.prototype.onMouseDown_ = function() {
	this.down_ = +new Date;
};

function success_handler_first(position) {
	$('#customerImage1').css(
			'margin-top',
			$("#placeholder").height() / 2 - $('#placeholder').position().top
					+ $("#headId").height() / 3 + 'px');
	$('#customerImage1').css('margin-left',
			($('#placeholder').width() / 2) - 16 + 'px');
	$('#addressCustomerTD').css('height',
			$('#placeholder').height() / 10 + 'px');
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	accuracy = position.coords.accuracy;
	$("#latitudeOnLoad").val(latitude);
	$("#longitudeOnLoad").val(longitude);
	var point = new google.maps.LatLng(latitude, longitude);
	var geocoder = new google.maps.Geocoder();
	geocoder
			.geocode(
					{
						latLng : point
					},
					function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
							if (results[0]) {
								var arrAddress = results[0].address_components;

								var streetnum = "";
								var route = "";
								var itemCountry = "";
								var itemPc = "";
								$
										.each(
												arrAddress,
												function(i, address_component) {
													if (address_component.types[0] == "street_number") {
														/* $("#sadd1Dash").val(address_component.long_name); */
														streetnum = address_component.long_name;
													}

													if (address_component.types[0] == "route") {
														route = address_component.long_name;
													}

													if (address_component.types[0] == "postal_code") {
														itemPc = address_component.long_name;
													}
													document
															.getElementById("addressCustomerTD").innerHTML = results[0].formatted_address;
													localStorage.address = results[0].formatted_address;
													localStorage.lat = latitude;
													localStorage.longi = longitude;

													document
															.getElementById("addressOnLoad").value = results[0].formatted_address;
												});
							}
						}
					});
	initializeCustomer();
	navigator.geolocation.watchPosition(success_handler_update, error_handler,
			{
				enableHighAccuracy : true,
				maximumAge : 60000,
				timeout : 10000,
				frequency : 3000
			});
}

function success_handler_update(position) {
	// alert("success");
	// markerCustomer.setMap(null);
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	accuracy = position.coords.accuracy;
	// alert("success:"+latitude+":"+longitude);
	geocoder = new google.maps.Geocoder();
	latlng = new google.maps.LatLng(latitude, longitude);
	if (document.getElementById("dragend").value == 'false') {
		map.setCenter(latlng);
	}
}

function bookJobCall(latitude, longitude) {

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'CustomerMobileAction?event=openRequest';
	if (latitude != null && latitude != "") {
		url = url + '&latitude=' + latitude;
	}
	if (longitude != null && longitude != "") {
		url = url + '&longitude=' + longitude;
	}

	window.open(url, '_self');

}
function latLngHandler(position) {
	latitude = position.coords.latitude;
	longitude = position.coords.longitude;
	if (!latitude || !longitude) {
		document.getElementById("status").innerHTML = "HTML5 Geolocation supported, but location data is currently unavailable.";
		return;
	}
	var latlng = new google.maps.LatLng(latitude, longitude);
	return latlng;
}

var markerCustomer;
function initializeCustomer() {
	var blueDot = "http://maps.google.com/mapfiles/kml/paddle/grn-circle-lv.png";
	geocoder = new google.maps.Geocoder();
	latlng = new google.maps.LatLng(latitude, longitude);
	var options = {
		zoom : 18,
		center : latlng,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		draggableCursor : 'crosshair'
	};

	map = new google.maps.Map(document.getElementById("placeholder"), options);
	// google.maps.event.trigger(map, 'resize');
	google.maps.event
			.addListener(
					map,
					'dragend',
					function() {
						dragend();
						var c = map.getCenter();
						latitude = c.lat();
						longitude = c.lng();
						latlng = new google.maps.LatLng(latitude, longitude);
						geocoder = new google.maps.Geocoder();
						$('#dragLati').val(latitude);
						$('#dragLongi').val(longitude);

						var point = new google.maps.LatLng(latitude, longitude);
						var geocoder = new google.maps.Geocoder();
						geocoder
								.geocode(
										{
											latLng : point
										},
										function(results, status) {
											if (status == google.maps.GeocoderStatus.OK) {
												if (results[0]) {
													var arrAddress = results[0].address_components;

													var streetnum = "";
													var route = "";
													var itemCountry = "";
													var itemPc = "";
													$
															.each(
																	arrAddress,
																	function(i,
																			address_component) {
																		if (address_component.types[0] == "street_number") {
																			/* $("#sadd1Dash").val(address_component.long_name); */
																			streetnum = address_component.long_name;
																		}
																		if (address_component.types[0] == "locality") {
																			// $("#scityDash").val(address_component.long_name);

																		}
																		if (address_component.types[0] == "route") {
																			route = address_component.long_name;
																		}
																		if (address_component.types[0] == "country") {
																			itemCountry = address_component.long_name;
																		}
																		if (address_component.types[0] == "postal_code") {
																			itemPc = address_component.long_name;
																			// $("#szipDash").val(address_component.long_name);
																		}
																		document
																				.getElementById("addressCustomerTD").innerHTML = results[0].formatted_address;
																		localStorage.address = results[0].formatted_address;
																		localStorage.lat = latitude;
																		localStorage.longi = longitude;
																		document
																				.getElementById("addressDragged").value = results[0].formatted_address;

																	});
												}
											}
										});
					});
	new LongClick(map, 800);
	var markerCustomer;
	var externalImage = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
	var driverImage = "greenCar.png";
	// GEOCODER
	geocoder = new google.maps.Geocoder();
	/*
	 * markerCustomer = new google.maps.Marker({ position : latlng, map : map,
	 * icon : externalImage });
	 */
	map.setCenter(latlng);
	// updateDisplay();
	$("#customerImage1").show();
	// $("#addressCustomer").show();
}

function updateDisplay() {
	latlng = new google.maps.LatLng(latitude, longitude);
	var externalImage = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
	var driverImage = "greencar.png";
	// GEOCODER
	geocoder = new google.maps.Geocoder();
	if (document.getElementById("driverLati").value != ""
			&& document.getElementById("driverLati").value != "0.0") {
		latlng1 = new google.maps.LatLng(
				document.getElementById("driverLati").value, document
						.getElementById("driverLongi").value);

		markerDriver = new google.maps.Marker({
			position : latlng1,
			map : map,
			icon : driverImage
		});
		markerDriver.setMap(null);
		markerDriver.setMap(map);
		document.getElementById("driverLati").value = "";
		document.getElementById("driverLongi").value = "";
	}
	if (document.getElementById("jobLati").value != ""
			&& document.getElementById("jobMarker").value == "0") {
		latlng1 = new google.maps.LatLng(
				document.getElementById("jobLati").value, document
						.getElementById("jobLongi").value);
		markerjob = new google.maps.Marker({
			position : latlng1,
			map : map,
			icon : externalImage,
			animation : google.maps.Animation.DROP
		});
		// markerjob.setMap(null);
		markerjob.setMap(map);
		$("#jobMarker").val("1");
		document.getElementById("jobLati").value = "";
		document.getElementById("jobLongi").value = "";
	}

	$("#customerImage1").show();
}
function dialogShow() {
	// $.mobile.changePage( "#myDialog", { role: "dialog" } );
	$("#myDialog").show();
}
function dialogHide() {
	$("#myDialog").hide();
}
function confirmationShow() {
	// $.mobile.changePage( "#myDialog", { role: "dialog" } );
	$("#myConfirmation").show();
}
function confirmationHide() {
	$("#myConfirmation").hide();
}

function mapselected() {
	$("#afterPickUp").hide();
	$("#pickUp").hide();
	$("#afterMap").show();
	$("#myConfirmation").hide();
}
function tagselected() {
	$("#myConfirmation").hide();
	if (localStorage.loginCheck == "loggedIn") {
		pickUpAddresses();
	} else {
		var msg = "Please Login to Us.";
		setMessage(msg);
		dialogShow();
	}
}

function hideMarkers() {
	loadLocation();
}
function error_handler(error) {
	initializeCustomer();
	var locationError = '';

	switch (error.code) {
	case 0:
		locationError = "There was an error while retrieving your location: "
				+ error.message;
		break;
	case 1:
		locationError = "The user prevented this page from retrieving a location.";
		break;
	case 2:
		locationError = "The browser was unable to determine your location: "
				+ error.message;
		break;
	case 3:
		locationError = "The browser timed out before retrieving the location.";
		break;
	}
	document.getElementById("status").innerHTML = locationError;
	document.getElementById("status").style.color = "#D03C02";
}
function dragend() {
	document.getElementById("dragend").value = "true";
	$("#focusPlace").show();
	// cancel();
}
function focusPlaceFunction() {
	document.getElementById("dragend").value = "false";
	loadLocation();
	$("#jobMarker").val("0");
	document.getElementById("addressCustomerTD").innerHTML = "Book A Job";
	document.getElementById("dragLati").value = "";
	document.getElementById("dragLongi").value = "";
	document.getElementById("addressDragged").value = "";
	$("#addressCustomerTD").show();
	$("#focusPlace").hide();
	/*
	 * $("#afterPickUp").hide(); $("#pickUp").show();
	 */
}

function showLogin() {

	$.mobile.changePage("#login_page1");

}

function closeDiv() {
	$("#tagAddress").hide('slow');
}
function closeTag() {
	$("#addressTagWindow").hide();
}
function insertAddress(switches) {
	var address = "";
	var tagName = "";
	var latitude = "";
	var longitude = "";
	if (switches == "") {
		tagName = document.getElementById("tagNameForHome").value;
		address = document.getElementById("sAddressTagForHome").value;
		latitude = localStorage.lat;
		longitude = localStorage.longi;
	} else {
		tagName = document.getElementById("tagName").value;
		address = document.getElementById("sAddressTag").value;
		latitude = document.getElementById("sLatitude").value;
		longitude = document.getElementById("sLongitude").value;
	}
	if (address != "" && address != null) {
		var url = 'http://' + document.getElementById('webUrl').value
				+ '/TDS/CustomerMobileAction?event=checkAddress&address='
				+ address;
		xmlhttp.open("GET", url, false);
		xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
				+ localStorage.sessionId);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == 0) {
			if (tagName != "") {
				if (window.XMLHttpRequest) {
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				var url = 'http://' + document.getElementById('webUrl').value
						+ '/TDS/CustomerMobileAction?event=tagAddress&tagName='
						+ tagName + '&address=' + address + '&latitude='
						+ latitude + '&longitude=' + longitude;
				xmlhttp.open("GET", url, false);
				xmlhttp.setRequestHeader('Cookie', 'JSESSIONID='
						+ localStorage.sessionId);
				xmlhttp.send(null);
				var text = xmlhttp.responseText;
				if (text == 1) {
					if (switches == '1') {
						$.mobile.changePage("#bookForm");
					} else if (switches == '') {
						$.mobile.changePage("#home");
						$("#tagNameForHome").val("");
						$("#sAddressTagForHome").val("");
					}
				}
			} else {
				setMessage("Enter Tag Name to save this Address");
				dialogShow();
			}
		} else {
			var msg = "This address is already tagged, Please check Tagged Addresses";

			setMessage(msg);
			dialogShow();
		}

	}
}
function pickUpAddresses() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'http://' + document.getElementById('webUrl').value
			+ '/TDS/CustomerMobileAction?event=taggedAddressestoShow';
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != null && text != "") {
		document.getElementById("taggedAddressestoShowBody").innerHTML = "";
		var obj = "{\"favAddress\":" + text + "}";
		var jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.favAddress.length; i++) {

			$("#taggedAddressestoShowBody").append("<tr>");
			$("#taggedAddressestoShowBody").append(
					"<td>" + jsonObj.favAddress[i].tagAdd + "</td>");
			$("#taggedAddressestoShowBody").append(
					"<td style=\"overflow:hidden\" >"
							+ jsonObj.favAddress[i].address + "</td>");
			$("#taggedAddressestoShowBody")
					.append(
							"<td style=\"background-color: lightgrey\">"
									+ "<input type=\"button\" name=\"fixAsPu\" id=\"fixAsPu\" value=\"P/U\" onclick=\"fixValues('"
									+ jsonObj.favAddress[i].address
									+ "','"
									+ jsonObj.favAddress[i].getLatitude
									+ "','"
									+ jsonObj.favAddress[i].getLongitude
									+ "');\">"
									+ "<input type=\"button\" name=\"fixAsDo\" id=\"fixAsDo\" value=\"D/O\" onclick=\"fixValuesDropOff('"
									+ jsonObj.favAddress[i].address + "','"
									+ jsonObj.favAddress[i].getLatitude + "','"
									+ jsonObj.favAddress[i].getLongitude
									+ "');\"></td>");

			$("#taggedAddressestoShowBody").append("</tr>");

		}
	}

	$.mobile.changePage("#taggedAddresstoShow");
}
function showTagDiv(switches) {
	if (localStorage.loginCheck == "loggedIn") {
		var address = "";
		if (switches == 1) {
			address = document.getElementById("sAddress1").value;
		} else if (switches == "") {
			address = document.getElementById("addressCustomerTD").innerHTML;
		} else {
			address = document.getElementById("eAddress1").value;
		}
		if (address == "" || address == null) {
			var msg = "Please Enter an address.";
			setMessage(msg);
			dialogShow();
		} else {
			$("#sAddressTagForHome").val(address);
			$.mobile.changePage("#addressTagWindowForHome");
			taggedAddresses();
		}
	} else {
		var msg = "Please Log In.";
		setMessage(msg);
		dialogShow();
	}
}
function taggedAddresses() {
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'http://' + document.getElementById('webUrl').value
			+ '/TDS/CustomerMobileAction?event=taggedAddressestoShow';
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text != null && text != "") {
		document.getElementById("taggedAddressesForHomeBody").innerHTML = "";
		var obj = "{\"favAddress\":" + text + "}";
		var jsonObj = JSON.parse(obj.toString());
		for (var i = 0; i < jsonObj.favAddress.length; i++) {

			$("#taggedAddressesForHomeBody").append("<tr>");
			$("#taggedAddressesForHomeBody").append(
					"<td  style=\"background-color: lightgrey\">"
							+ jsonObj.favAddress[i].tagAdd + "</td>");
			$("#taggedAddressesForHomeBody")
					.append(
							"<td  style=\"background-color: lightgrey\"><input type=\"button\" name=\"showAddress\" id=\"showAddress\" value=\"Address\" onclick=\"popAddress('"
									+ jsonObj.favAddress[i].address
									+ "')\"> </td>");
			$("#taggedAddressesForHomeBody")
					.append(
							"<td colspan=\"3\" style=\"background-color: lightgrey\">"
									+ "<input type=\"button\" name=\"fixAsPu\" id=\"fixAsPu\" value=\"P/U\" onclick=\"fixValues('"
									+ jsonObj.favAddress[i].address
									+ "','"
									+ jsonObj.favAddress[i].getLatitude
									+ "','"
									+ jsonObj.favAddress[i].getLongitude
									+ "');\">"
									+ "<input type=\"button\" name=\"fixAsDo\" id=\"fixAsDo\" value=\"D/O\" onclick=\"fixValuesDropOff('"
									+ jsonObj.favAddress[i].address
									+ "','"
									+ jsonObj.favAddress[i].getLatitude
									+ "','"
									+ jsonObj.favAddress[i].getLongitude
									+ "');\">"
									+ "<input type=\"button\" name=\"deleteAddress\" id=\"deleteAddress\" value=\"Delete\" onclick=\" deleteAddress(this,'"
									+ jsonObj.favAddress[i].addKey
									+ "','');\"></td>");

			$("#taggedAddressesForHomeBody").append("</td>");
			$("#taggedAddressesForHomeBody").append("</tr>");

		}
	}

}
function popAddress(address) {
	setMessage(address);
	dialogShow();
}
function fixValues(address, latitude, longitude) {
	$("#tagAddress").hide('slow');
	document.getElementById("sAddress1").value = address;
	document.getElementById("sLatitude").value = latitude;
	document.getElementById("sLongitude").value = longitude;
	bookJob();
	// $.mobile.changePage("#bookForm");
	// $('#taggedAddresstoShow').jqmHide();
	// $("#taggedAddresstoShow").hide();
}
function fixValuesDropOff(address, latitude, longitude) {
	$("#tagAddress").hide('slow');
	document.getElementById("eAddress1").value = address;
	document.getElementById("eLatitude").value = latitude;
	document.getElementById("eLongitude").value = longitude;
	bookJob();
	// $('#taggedAddresstoShow').jqmHide();
	// $("#taggedAddresstoShow").hide();
}
function checkUserId() {
	var userId = document.getElementById("userIdForR").value;
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = "http://"
			+ document.getElementById('webUrl').value
			+ "/TDS/CustomerMobileAction?event=userIdCheck&responseType=HTML5&submit=YES&ccode="
			+ localStorage.ccode + "&userId=" + userId;
	xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader('Cookie', 'JSESSIONID=' + localStorage.sessionId);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if (text == 1) {
		// document.getElementById("userId").focus();
		$(
				"<div class='ui-loader ui-overlay-shadow ui-body-e ui-corner-all'><h1>Already Exists</h1></div>")
				.css({
					"display" : "block",
					"opacity" : 0.96,
					"top" : $(window).height() / 2,
					"align" : "center"
				}).appendTo($.mobile.pageContainer).delay(800).fadeOut(4000,
						function() {
							$(this).remove();
						});
	}

}

function checkPassword() {
	var password = document.getElementById("passwordForR").value;
	var rePassword = document.getElementById("rePasswordForR").value;
	if (password != "" && rePassword != "") {
		if (password == rePassword) {
		} else {
			/*
			 * document.getElementById("passwordForR").value = "";
			 * document.getElementById("rePasswordForR").value = "";
			 */
			document.getElementById("rePasswordForR").focus();
			// document.getElementById("errorText").value="Password not
			// matching";
			// if(document.getElementById("os").value=="G"){
			Android.showToast("Password doesn't match");

		}
	}
}
