<!--
This sample code is designed to connect to SLIM CD using the HTTP POST method.
-->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
  "http://www.w3.org/TR/html4/loose.dtd">
<HTML lang='en'>
<HEAD>
	<TITLE> Sample HTTP POST Implementation</TITLE>
</HEAD>
<BODY>

<%@ page import="java.util.*" %>
<%@ page import="java.net.*" %>
<%@ page import="java.io.*" %>
<%@ page import="javax.net.ssl.*" %>
<%@ page import="java.net.URLEncoder" %>
<%

// This sample code is designed to post to SLIM CD's test accounts
// using the HTTPS POST method.


//
// THIS URL SHOULD BE CHANGED TO HTTPS once you install the certificate on your system!
//
URL post_url = new URL("http://trans.slimcd.com/wswebservices/transact.asmx/PostHTTP");

Hashtable post_values = new Hashtable();
  
post_values.put ("clientid", "1032");
post_values.put ("siteid", "228226448");
post_values.put ("priceid", "74");
post_values.put ("password", "289075");
post_values.put ("key", "SVD-072-5QQ6-5K58");
  
post_values.put ("product", "JavaPost");
post_values.put ("ver", "1.0");

post_values.put ("transtype", "SALE");
post_values.put ("cardnumber", "4444333322221111");
post_values.put ("expmonth", "12");
post_values.put ("expyear", "15");
post_values.put ("amount", "10.00");

post_values.put ("first_name", "Test");
post_values.put ("last_name", "Person");
post_values.put ("address", "123 Test St");
post_values.put ("city", "Coral Springs");
post_values.put ("state", "FL");
post_values.put ("zip", "33071");
// Additional fields can be added here

// This section takes the input fields and converts them to the proper format
// for an http post.  For example: "clientid=value&password=value"

StringBuffer post_string = new StringBuffer();
Enumeration keys = post_values.keys();
while( keys.hasMoreElements() ) {
  String key = URLEncoder.encode(keys.nextElement().toString(),"UTF-8");
  String value = URLEncoder.encode(post_values.get(key).toString(),"UTF-8");
  post_string.append(key + "=" + value + "&");
}

// Now, we make one more field with all the data

StringBuffer final_post_string = new StringBuffer();

final_post_string.append("postdata=" + URLEncoder.encode(post_string.toString(),"UTF-8")) ;


// Open a URLConnection to the specified post url
URLConnection connection = post_url.openConnection();
connection.setDoOutput(true);
connection.setUseCaches(false);

// this line is not necessarily required but fixes a bug with some servers
connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

// submit the post_string and close the connection
DataOutputStream requestObject = new DataOutputStream( connection.getOutputStream() );
requestObject.write(final_post_string.toString().getBytes());
requestObject.flush();
requestObject.close();

// process and read the gateway response
BufferedReader rawResponse = new BufferedReader(new InputStreamReader(connection.getInputStream()));
String line;
long start,end ;

String responseData = rawResponse.readLine();

start = responseData.indexOf("response=") ;
if (start < 0) {
	responseData = rawResponse.readLine() ;
	start = responseData.indexOf("response=") ;
}

rawResponse.close();	                     // no more data
end = responseData.indexOf("</string>") ;


out.println("<hr>" + responseData.length() + " bytes: " + responseData.toString() + "<hr>");


// split the response into an array
String [] responses = responseData.split("&amp;");

// The results are output to the screen in the form of an html numbered list.
out.println("<OL>");
for(Iterator iter=Arrays.asList(responses).iterator(); iter.hasNext();) {

	out.println("<LI>" + iter.next().toString() + "&nbsp;</LI>");

}
out.println("</OL>");


// individual elements of the array could be accessed to read certain response
// for example, look for "response", "description", "approved", "avsreply", "authcode", etc.



%>
</BODY>
</HTML>