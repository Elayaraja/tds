<%@ page language="java" import="javazoom.upload.*,java.util.*" %>
<%@ page errorPage="ExceptionHandler.jsp" %>
<%@ page import="javazoom.upload.parsing.CosUploadFile" %> 



 
<%@page import="com.common.util.TDSProperties"%>
<%
	String url = TDSProperties.getValue("FolderStore") ;
%>
<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean" >  
 
<jsp:setProperty name="upBean" property="whitelist" value="*.csv" />
<jsp:setProperty name="upBean" property="overwritepolicy" value="nametimestamp" /> 
<%
	upBean.setFolderstore(url);
%> 

</jsp:useBean>

 
<head>
<title>CSV Upload</title>
<style TYPE="text/css">


.style1
 {
	font-size: 12px;
	font-family: Verdana;
 }


</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body bgcolor="#FDF5E6" text="#000000">
<ul class="style1">
<%
  String fn="";  
if (MultipartFormDataRequest.isMultipartFormData(request))
{
   // Uses MultipartFormDataRequest to parse the HTTP request.
   MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
   String todo = null;
   if (mrequest != null) todo = mrequest.getParameter("todo");
   if ( (todo != null) && (todo.equalsIgnoreCase("upload")) )
   {
          Hashtable files = mrequest.getFiles();
          
          if ( (files != null) && (!files.isEmpty()) )
          {
              UploadFile file = ((UploadFile) files.get("uploadfile"));  
              upBean.store(mrequest); 
              Vector history = upBean.getHistory();
              UploadParameters up = (UploadParameters) history.elementAt(0);
              if(up.getAltFilename()!=null)
              {
              	 fn=up.getAltFilename();
              }
              else
              {
              	fn=((UploadFile) files.get("uploadfile")).getFileName();
              }
              
              if (file != null) 
              	out.println("<center><font color='blue' size='3'><li>"+" Attached File : "+file.getFileName()+" ("+file.getFileSize()+" bytes)"+"<BR> Content Type : "+file.getContentType()+"</font></center>");
              // Uses the bean now to store specified by jsp:setProperty at the top.
              //upBean.store(mrequest, "uploadfile"); 
          }
          else
          { 
            out.println("<li>No uploaded files"); 
          }
   }
   else out.println("<BR> todo="+todo);
}

  %>
</ul>
 
<%if(fn!=null && fn.equals(""))
  { %>
	<TABLE align="center" width="650">
	 <TR>
	 	<TD align="center">
			<TABLE id="formtitle" align="center">
				<TR>
					<TD align="left"><Font size="5" color="blue"><b>&nbsp;CSV File&nbsp;Upload</b></font> 
					</TD>
				</TR>
			</TABLE>
		</TD>
	 </TR>
	</TABLE>
 <%} %>
 <%System.out.println("In Simple Upload Jsp = "+fn);%>
 <input type="hidden" name="fn" id="fn"  value="<%=fn%>">
  <input type="hidden" name="include" id="include" value="<%=request.getParameter("include")==null?"0":request.getParameter("include") %>">
 
<form method="post" action="/TDS/control?action=CSV&event=csvUploadStart" name="upform" enctype="multipart/form-data">
<script type="text/javascript">
function chckIncl()
{
	if(document.getElementById('incl').checked)
	{
		document.getElementById('include').value = "1";
	} else {
		document.getElementById('include').value = "0";
	}
}
</script>
	
  <table width="50%" border="0" cellspacing="1" cellpadding="1" align="center" class="style1"> 
  <%if(fn!=null && fn.equals("")){ %>
    <tr>
      <td align="left">
      	 <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;File&nbsp;Name&nbsp;:&nbsp;<b>
      	 <input type="file" id="uploadfile" name="uploadfile" size="50">
      </td>
    </tr> 
    <tr>
    	<td></td>
    </tr>
    <%} %>
    <tr> 
      <td align="center">
      <%if(fn!=null && fn.equals("")){ %>
		 	<input type="hidden" name="todo" value="upload">
      		<input type="submit" name="Submit" value="Upload">   
        	<input type="reset" name="Reset" value="Clear">
       <%} else {%>
       		 <B>Include Heading </B> 
       		 <input type="checkbox" name="incl" id="incl" value="1" <%=(request.getParameter("incl")==null?"":"checked") %> onclick="chckIncl()">
			 <input type="button"  value="Process" onclick="LoadMe()">
		<%} %>
        </td>
    </tr>
  </table>
  <script type="text/javascript">
 function LoadMe()
 {	
 
 	//if(opener.document.getElementById("filename").value!="")
 	//{	
 	  //   opener.document.getElementById("filename").value=opener.document.getElementById("filename").value+"<br>"+document.getElementById("fn").value;
 	   // var filename = opener.document.getElementById("filename").value; 
 	   // var filearr = filename.split('<br>');
 	   // var anch_filename="";
 	   // var i=0;
 	   // for(i=0;i<filearr.length;i++){
 	   // 	anch_filename=anch_filename+"<br><a href='/TDS/control?action=file&event=fileDownload&fileid="+document.getElementById("fi").value+"&filename="+filearr[i]+"'>"+filearr[i]+"</a>";
 	   // }
 	    //opener.document.getElementById("ancid").style.display = 'none';
 	   	//opener.document.getElementById("filediv").innerHTML = anch_filename;
   	//}
   	//else
   	//{ 
   	
   		// alert("Include:"+document.getElementById("fn").value);
   		window.location.href = "/TDS/control?action=CSV&event=csvUploadStart&event1=process&fname="+document.getElementById("fn").value+"&include="+document.getElementById('include').value;
   	 	
   	//}   
  // self.close();
 }

  	
  </script> 
 
</form>
</body>
 