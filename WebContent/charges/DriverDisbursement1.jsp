<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="java.math.*"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="com.ibm.icu.text.Bidi"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.CashSettlement" %>
   <%@page import="com.common.util.TDSProperties"%>
   
    <%@page import="com.tds.tdsBO.FleetBO"%>


<%@page import="com.charges.bean.DriverChargesBean"%>
<%@page import="com.tds.action.OpenRequest"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.charges.bean.DisbursementMaster"%>
<%@page import="com.charges.constant.IChargeConstants"%><%@page import="com.charges.bean.ChargesBO"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.charges.constant.IPaymentToDriver"%>
<%@page import="java.util.Map"%>

<%@page import="com.charges.bean.LeaseBean"%>
<%@page import="com.charges.bean.MiscBean"%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<script>

function ajaxCall(url, callback) {
	//alert("Inside ajaxcall");
    var req = init();
    req.onreadystatechange = processRequest;
    function init() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    } 
    
    function processRequest () {
      // readyState of 4 signifies request is complete
      if (req.readyState == 4) {
        // status of 200 signifies sucessful HTTP call
        if (req.status == 200) {
          if (callback) callback(req.responseText);
        }
      }
    }

    this.doGet = function() {
      req.open("GET", url, true);
      req.send(null);
    }
}



//------ Ajax InterAction  Over    -------------------------------

function fnRemoveLease(leaseID)
{
	document.getElementById('chargeType').value = 2;
	var url = '/TDS/AjaxClass?event=removeLease&id='+leaseID;
	var ajax = new ajaxCall(url,refreshLeaseAndMisc);
	ajax.doGet();
	 
}
function loadLeaseAmount(){
	var url = '/TDS/AjaxClass?event=getLeaseAmount&vechType='+document.getElementById('vechType').value;
	//alert(url);
	var ajax = new ajaxCall(url,fillLeaseAmount);
	ajax.doGet();

}
function fillLeaseAmount(responseText)
{
	var leaseAmount = parseFloat(responseText);
	document.getElementById('leaseAmount').value=leaseAmount;
}
function loadMiscAmount(){
	if($('#miscName_temp').is(':visible')){
		var key=document.getElementById("miscName_temp").value;
		var amount=document.getElementById(key).value;
		document.getElementById('miscAmount_temp').value=amount;
	}else{
		document.getElementById('miscAmount_manual').value=amount;
	}
}
function fnRemoveMisc(miscID)
{
	document.getElementById('chargeType').value = 1;
	var url = '/TDS/AjaxClass?event=removeMisce&id='+miscID;
	var ajax = new ajaxCall(url,refreshLeaseAndMisc);
	ajax.doGet();
	 
}
function fnCallLease()
{
	document.getElementById('chargeType').value = 2
	fnCallShowLeaseAndMisc();
}
function fnCallMisc()
{
	document.getElementById('chargeType').value = 1
	fnCallShowLeaseAndMisc();
}
function fnCallShowLeaseAndMisc(){
	if(document.getElementById('chargeType').value == 2){
		$("#addLease").jqm({modal:true});
		$("#addLease").jqmShow();
	}else{
		$("#addMisc").jqm({modal:true});
		$("#addMisc").jqmShow();
	}
}
function currentDateDisb() {
	var curdate = new Date();
	var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
	var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
	var cYear = curdate.getFullYear();
	var hors = curdate.getHours() >=10 ? curdate.getHours() : "0"+curdate.getHours();
	var min = curdate.getMinutes() >=10 ? curdate.getMinutes() : "0"+curdate.getMinutes();
	var hrsmin = hors+""+min;  
	document.getElementById("process_Date").value =cMonth+"/"+cDate+"/"+cYear;
	//document.masterForm.shrs.value  = hrsmin; 
 }

function refreshLeaseAndMisc()
{
	if(document.getElementById('chargeType').value == 2)
	{
		var url = '/TDS/AjaxClass?event=getLeaseDetail&driverId='+document.getElementById('dd_drv_id').value;
		var ajax = new ajaxCall(url,fillLeaseDetail);
		ajax.doGet();
	} else {
		var url = '/TDS/AjaxClass?event=getMiscDetail&driverId='+document.getElementById('dd_drv_id').value;
		var ajax = new ajaxCall(url,fillMiscDetail);
		ajax.doGet();
	}
}

function fillLeaseDetail(responseText)
{
	//alert(responseText);
	var resText = responseText.split("###");
	document.getElementById("leaseid").innerHTML = resText[0];
	document.getElementById("leaseTotal").value =  parseFloat(resText[1]).toFixed(2);
	calculateJobWiseSubTotal();
}

function fillMiscDetail(responseText)
{
	//alert(responseText);
	var resText = responseText.split("###");
	document.getElementById("miscid").innerHTML = resText[0];
	document.getElementById("miscTotal").value =  parseFloat(resText[1]).toFixed(2);
	calculateJobWiseSubTotal();
}
function showOrHideBalance(){
	 var img=document.getElementById("imgReplaceBalance").src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgReplaceBalance").src="images/minus_icons.png";
			$("#openingBalanceDiv").show("slow");
		}else{
			document.getElementById("imgReplaceBalance").src="images/plus_icons.png";
			$("#openingBalanceDiv").hide("slow");
		}
}
function showOrHideLease(){
	 var img=document.getElementById("imgReplaceLease").src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgReplaceLease").src="images/minus_icons.png";
			$("#leaseid").show("slow");
		}else{
			document.getElementById("imgReplaceLease").src="images/plus_icons.png";
			$("#leaseid").hide("slow");
		}
}
function showOrHideMisc(){
	 var img=document.getElementById("imgReplaceMisc").src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgReplaceMisc").src="images/minus_icons.png";
			$("#miscid").show("slow");
		}else{
			document.getElementById("imgReplaceMisc").src="images/plus_icons.png";
			$("#miscid").hide("slow");
		} 
}

function showorHideChargesTable(row){
	 var img=document.getElementById("imgForCharges_"+row).src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgForCharges_"+row).src="images/minus_icons.png";
			$("#chargesTable_"+row).show("slow");
		}else{
			document.getElementById("imgForCharges_"+row).src="images/plus_icons.png";
			$("#chargesTable_"+row).hide("slow");
		}
}
function calculateJobWiseSubTotal(){
	var numberOfJobs = document.getElementById("tripsAndNumber");
	var total=0.0;
	var subTotal=0.0;
	document.getElementById("orsize").value=numberOfJobs.rows.length;
	for(var i=0;i<numberOfJobs.rows.length;i++){
			var rows=numberOfJobs.rows[i].cells[1].innerHTML;
			var tripId=numberOfJobs.rows[i].cells[0].innerHTML;
			subTotal=0.0;
			for(var j=0;j<rows;j++){
				subTotal=parseFloat(subTotal)+parseFloat(document.getElementById("drvAmt"+tripId+"_"+j).value);
			}
			document.getElementById("subTotal_"+tripId).value=subTotal.toFixed(2);
			if(document.getElementById("chck"+tripId).checked==true){
				total=parseFloat(total)+parseFloat(subTotal);
			}
	}
	if(document.getElementById("orsize").value>0){
	document.getElementById("grandTotal").value=total.toFixed(2);
	}
	total=total+parseFloat(document.getElementById("leaseTotal").value)+parseFloat(document.getElementById("openingBalance").value)+parseFloat(document.getElementById("miscTotal").value);
	document.getElementById("closingBalance").value=total.toFixed(2);

}
function calculateFinalTotalWithCheckedItems(){
	
}
function submitLease(){
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
		var url = '/TDS/control?action=ChargesAction&event=storeLease&button=Submit&ddid='+document.getElementById("dd_driver_id").value+'&leaseAmount='+document.getElementById("leaseAmount").value+'&vechType='+document.getElementById("vechType").value+'&cabNo='+document.getElementById("cabNo").value;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text==1){
			document.getElementById("resultLease").innerHTML="Stored Successfully";
		}else{
			document.getElementById("resultLease").innerHTML="UnSuccessful";
		}
		document.getElementById("vechType").value="";
		document.getElementById("cabNo").value="";
		document.getElementById("leaseAmount").value="";
		refreshLeaseAndMisc();
}
function submitMisc(){
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
		var url = '';
		if($("#miscManual").is(':visible')){
			url='/TDS/control?action=ChargesAction&event=storeMisc&button=Submit&ddid='+document.getElementById("dd_driver_id").value+'&miscAmount='+document.getElementById("miscAmount_manual").value+'&customField='+document.getElementById("customField_manual").value+'&miscName='+document.getElementById("miscName_manual").value+'&cabNo='+document.getElementById("cabNo").value;
		}else{
			url='/TDS/control?action=ChargesAction&event=storeMisc&button=Submit&ddid='+document.getElementById("dd_driver_id").value+'&miscAmount='+document.getElementById("miscAmount_temp").value+'&customField='+document.getElementById("customField_temp").value+'&miscName='+document.getElementById("miscDesc_temp").value+'&cabNo='+document.getElementById("cabNo").value;
		}
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text==1){
			document.getElementById("resultMisc").innerHTML="Stored Successfully";
		}else{
			document.getElementById("resultMisc").innerHTML="UnSuccessful";
		}
		document.getElementById("miscName_temp").value="";
		document.getElementById("miscAmount_temp").value="";
		document.getElementById("customField_temp").value="";
		document.getElementById("miscName_manual").value="";
		document.getElementById("miscAmount_manual").value="";
		document.getElementById("customField_manual").value="";
		refreshLeaseAndMisc();
}
function removeMiscPopUp(){
	$("#addMisc").jqmHide();
}
function removeLeasePopUp(){
	$("#addLease").jqmHide();
}
function showMiscType(){
	if(document.getElementById("showMisc").value=="1"){
		$("#miscManual").hide();
		$("#miscTemplate").show();
	}
	if(document.getElementById("showMisc").value=="2"){
		$("#miscTemplate").hide();
		$("#miscManual").show();
	}
}

</script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/OpenRequestDetails.js"></script>
<script type="text/javascript" src=<%=TDSProperties.getValue("googleMapV3") %>></script>
<script type="text/javascript" src="js/MapForORDetails.js"></script>

<link type="text/css" href="js/jqModal.css" rel="stylesheet" /> 
<script>
   window.history.forward(-1);
</script>
<title>Disbursement</title>
<%
	DisbursementMaster master = request.getAttribute("disbursementMaster")==null?new DisbursementMaster():(DisbursementMaster)request.getAttribute("disbursementMaster");
	ArrayList<OpenRequestBO> listOfJobs = (ArrayList)request.getAttribute("listOfJobs") == null ? new ArrayList():(ArrayList)request.getAttribute("listOfJobs");
	ArrayList<DriverChargesBean> listOfCharges = (ArrayList)request.getAttribute("listOfCharges") == null ? new ArrayList():(ArrayList)request.getAttribute("listOfCharges");
	ArrayList<LeaseBean>  listOfLeases = (ArrayList)request.getAttribute("listOfLeases") == null ? new ArrayList():(ArrayList)request.getAttribute("listOfLeases");
	ArrayList<MiscBean>  listOfMiscCharges = (ArrayList)request.getAttribute("listOfMiscCharges") == null ? new ArrayList():(ArrayList)request.getAttribute("listOfMiscCharges");
	ArrayList<DriverChargesBean> listOfPayment = (ArrayList)request.getAttribute("listOfPayment") == null ? new ArrayList():(ArrayList)request.getAttribute("listOfPayment");
	ArrayList<ChargesBO> charges = (ArrayList)request.getAttribute("charges") == null ? new ArrayList():(ArrayList)request.getAttribute("charges");
	ArrayList<String> totals = (ArrayList)request.getAttribute("totals") == null ? new ArrayList():(ArrayList)request.getAttribute("totals");
	ArrayList<String> lease = (ArrayList)request.getAttribute("lease") == null ? new ArrayList():(ArrayList)request.getAttribute("lease");
	ArrayList<ChargesBO> misc = (ArrayList)request.getAttribute("misc") == null ? new ArrayList():(ArrayList)request.getAttribute("misc");
	ArrayList<DriverChargesBean>  chargesForPayment=(ArrayList)request.getAttribute("chargesForPayment")==null?new ArrayList():(ArrayList)request.getAttribute("chargesForPayment");
	ArrayList<DriverChargesBean> paymentForCharges = (ArrayList)request.getAttribute("paymentForCharges") == null ? new ArrayList():(ArrayList)request.getAttribute("paymentForCharges");

	//ArrayList<String> listOfDrivers = (ArrayList)request.getAttribute("listOfDrivers") == null ? new ArrayList():(ArrayList)request.getAttribute("listOfDrivers");
%>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); 
	 	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");%>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>
<script type="text/javascript">
 $(document).ready(function () {  
	currentDateDisb();
	}); 
 $(document).ready(function () {
     $("#ckbCheckAll").click(function () {
         $(".checkBoxClass").attr('checked', this.checked);
         calculateJobWiseSubTotal();
     });
 });
function showRepot(repo,key)
{
	 

	
	if(repo == 1)
	{
		//var path = <%=request.getContextPath()%>;
		window.open('<%=request.getContextPath()%>/frameset?__report=BIRTReport/DriverDisburshment.rptdesign&__format=pdf&pay_id='+key);
	} else {
		 
	}
}

/* function calculateTotal1()
{
	var total=0.0;
	var grandTotal=0.0;
	for(var i=0;i<document.getElementById("orsize").value;i++){
		var tripId=document.getElementById("tripId"+i).value;
		alert(tripId);etProces
		alert(document.getElementById(tripId+"_num".innerHTML);
		for(int j=0;j<parseInt(document.getElementById(tripId+"_num".innerHTML);j++){
		total = parseFloat(total) + parseFloat(document.getElementById("driverAmt"+tripId+"_"+j).value);
		}
	}
		grandTotal=parseFloat(total)+parseFloat(grandTotal);
} */

function submitThisForm()
{
	document.getElementById("submitTemplate").value="1";	
	$("#getButton").click();
	document.getElementById("submitTemplate").value="0";
	/* document.masterForm.submit(); */
}
function checkFields(){
	var driver=document.getElementById("dd_driver_id").value;
	if(driver==""){
		alert("Enter a driver Id to get the disbursement detail");
		return false;
	}
		return true;
}
function sendBack(){
	window.location.href="/TDS/control";
}
function fixDriverValue(driverId){
	document.getElementById("dd_drv_id").value=driverId;
}
function changeFleet(){
	 var fleet=document.getElementById("fleet").value;
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		<%	AdminRegistrationBO adminBO=(AdminRegistrationBO)session.getAttribute("user");%>
	url='SystemSetupAjax?event=changeFleet&fleet='+fleet;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	alert(text);
}
function showLookup(){
	document.getElementById("fromTime").value="";
	document.getElementById("toTime").value="";
	if(document.getElementById("from_date").value!="" && document.getElementById("to_date").value!="" && document.getElementById("dd_driver_id").value!=""){
		$("#lookUPTD").show();	
	}else{
		$("#lookUPTD").hide();	
	}
}
function driverShiftDetails(){
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	url='SystemSetupAjax?event=driverShiftDetails&dId='+document.getElementById("dd_driver_id").value+'&fDate='+document.getElementById("from_date").value+'&tDate='+document.getElementById("to_date").value;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text!=""){
		var obj = "{\"shiftList\":" + text + "}";
		var jsonObj = JSON.parse(obj.toString());
		$("#driverShiftDetailsBody").append("<tr >");
		$("#driverShiftDetailsBody").append("<td colspan=\"3\"><center><h2> Driver Shift Details</h2></center> </td>");
		$("#driverShiftDetailsBody").append("</tr>");
		$("#driverShiftDetailsBody").append("<tr>");
		$("#driverShiftDetailsBody").append("<td><h4>Action</h4></td>");
		$("#driverShiftDetailsBody").append("<td><h4>Shift Open</h4></td>");
		$("#driverShiftDetailsBody").append("<td><h4>Shift Close</h4></td>");
		$("#driverShiftDetailsBody").append("</tr>");
		for ( var i = 0; i < jsonObj.shiftList.length; i++) {
			$("#driverShiftDetailsBody").append("<tr>");
			$("#driverShiftDetailsBody")
					.append(
							"<td> <input type=\"radio\" name=\"timing\" id=\"timing\" value=\""+ jsonObj.shiftList[i].getOpenTime+"\"  onclick=\"setOpenAndCloseTime('"+jsonObj.shiftList[i].getOpenTime+"','"+jsonObj.shiftList[i].getCloseTime+"')\">"
									+ jsonObj.shiftList[i].getCloseTime+ "</input>"
									+ "</td>"
							+"<td> "+jsonObj.shiftList[i].getOpenTime
									+ "</td>"
							+"<td> "+jsonObj.shiftList[i].getCloseTime
							+"</td>"
									);
			
			$("#driverShiftDetailsBody").append("</tr>");
		}
	
	}
	$("#driverShiftPop").jqm();
	$("#driverShiftPop").jqmShow();
	
}
function setOpenAndCloseTime(openTime,closeTime){
	document.getElementById("fromTime").value=openTime;
	document.getElementById("toTime").value=closeTime;
}
function verifySingleVoucher(i,transId){
	//if(document.getElementById("generate"+i).checked){
		var xmlhttp = null;
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {

			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'finance?event=verifyVoucher&transId='+transId;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if(text!="0"){
		document.getElementById("verifySingle"+i).disabled =true;
		} 
		
	//}
	}
	
function taxesCalculate(tripid,counter,i){
	//alert("tripId,Counter,i - "+tripid+","+counter+","+i);
	var tax1 = document.getElementById("temp_tax1_"+tripid+"_"+counter).value;
	var tax2 = document.getElementById("temp_tax2_"+tripid+"_"+counter).value;
	var tax3 = document.getElementById("temp_tax3_"+tripid+"_"+counter).value;
	//alert("tax1:"+tax1+" tax2:"+tax2+" tax3:"+tax3);
	var total = Number(tax1) + Number(tax2) + Number(tax3);
	
	//alert("total"+total);
	document.getElementById("drvAmt"+tripid+"_"+counter).value=total.toFixed(2);
	calculateJobWiseSubTotal();
}
	
</script>
<style>
.pretty-table
{
  padding: 0;
  margin: 0;
  border-collapse: collapse;
  border: 1px solid #333;
  font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
  font-size: 0.9em;
  color: #000;
  background: #bcd0e4 url("widget-table-bg.jpg") top left repeat-x;
}

.pretty-table caption
{
  caption-side: bottom;
  font-size: 0.9em;
  font-style: italic;
  text-align: right;
  padding: 0.5em 0;
}

.pretty-table th, .pretty-table td
{
  border: 1px dotted #666;
  padding: 0.5em;
  text-align: left;
  color: #632a39;
}

.pretty-table th[scope=col]
{
  color: #000;
  background-color: #8fadcc;
  text-transform: uppercase;
  font-size: 0.9em;
  border-bottom: 2px solid #333;
  border-right: 2px solid #333;
}

.pretty-table th+th[scope=col]
{
  color: #fff;
  background-color: #7d98b3;
  border-right: 1px dotted #666;
}

.pretty-table td[scope=border]
{
  color: #fff;
  background-color: #7d98b3;
  border: threedshadow;
  border-right: 1px dotted #666;
}

.pretty-table th[scope=row]
{
  background-color: #b8cfe5;
  border-right: 2px solid #333;
}

.pretty-table tr.alt th, .pretty-table tr.alt td
{
  color: #2a4763;
}

.pretty-table tr:hover th[scope=row], .pretty-table tr:hover td
{
  background-color: #632a2a;
  color: #fff;
}</style>
<script type="text/javascript">
	showRepot('<%=request.getParameter("showRepo")==null?"":"1" %>',<%=request.getParameter("key")==null?"0":request.getParameter("key") %>);
</script>
<body style="background-color: #F2F2F2;" onload="calculateJobWiseSubTotal();">
<form  name="masterForm" action="control" method="post" onsubmit="return checkFields()">
<input type="hidden" name="submitTemplate" id="submitTemplate" value=""/>
<input type="hidden" name="action" value="ChargesAction"/>
<input type="hidden" name="screenValue" id="screenValue" value="1" />		
	<input type="hidden" name="event" value="showDriverDisbursement"/> 
	<table style="width:100%">
	<tr><td style="width:5%">
<img src="images/home.png" onclick="sendBack()"></img></td>
<td align="center"  style="width:80%">  

<h1 style="font-weight: bold;" align="center"><font style="color: gray;">Driver Disbursement</font></h1>
</td>
<td align="center"  style="width:15%">
	
     <div id="multiFleet">
                       <%if(fleetList!=null && fleetList.size()>0){%>
                         <select name="fleet" id="fleet" onchange="changeFleet()" >
                       <%if(fleetList!=null){
	for(int i=0;i<fleetList.size();i++){ %>
	<option value="<%=fleetList.get(i).getFleetNumber()%>" <%=fleetList.get(i).getFleetNumber().equals(adminBo.getAssociateCode())?"selected=selected":""%> ><%=fleetList.get(i).getFleetName() %></option>
<% }}%> 
                        </select>
                       <%} %>
                       </div>
                       
                      </td></tr></table>
   <table  style="width: 10%" align="center" >
                    <tr>
					<td class="firstCol">From</td>
					<td>
					<input type="text" name="from_date" id="from_date" onfocus="showCalendarControl(this);" onblur="showLookup()"  value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>"  class="dontEdit" readonly="readonly"   />
					</td>
					<td class="firstCol">To</td>
					<td>
					<input type="text" name="to_date" id="to_date" onfocus="showCalendarControl(this);" onblur="showLookup()" value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>"  class="dontEdit" readonly="readonly" />
					</td>
						<td class="firstCol">Trip&nbsp;Id</td>
					<td>
					<input type="text" name="tripID" value="<%=request.getParameter("tripID")==null?"":request.getParameter("tripID") %>"   />
					</td>
					<td class="firstCol">Driver&nbsp;Id</td>
					<td colspan="2">
											<input type="hidden" name="fromTime" id="fromTime" value=""/>
											<input type="hidden" name="toTime" id="toTime" value=""/>
					
						<input type="hidden" name="chargeType" id="chargeType" value="<%=master.getChargeCalcType() %>"  />
									<input type="hidden" id ="orsize" name="orsize" value="0"/>
														<input type="hidden" id ="paymentSize" name="paymentSize" value="<%=listOfPayment!=null?listOfPayment.size():"0" %>"/>
					
					<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)"  onblur="caldid(id,'drivername','dName');showLookup()"  id="dd_driver_id" name="dd_driver_id" size="15"   autocomplete="off" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>" onblur="showLookup()" class="form-autocomplete" />
								 <input type="hidden" name="dd_drv_id" id="dd_drv_id" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>"/>
						  		 <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 
				  					<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>"/>
							  		<div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>	 
					</td><td>
					<input type="submit" name="Button" id = "getButton" class="lft" style="text-align:center"  value="Get" />
					</td>
					<td id="lookUPTD" style="display: none;">
					<input type="button" name="lookup" id="lookup" value="LookUp" onclick="driverShiftDetails()"></input>
					</td>
					</tr>
					<div style="color: red;" >
					<%if(listOfJobs.size()>0 ){  %>
					<blink><%=request.getAttribute("message") == null?"":(String)request.getAttribute("message") %> </blink>
					</div>
				<%} %>
				<%if(listOfJobs.size()>0 || request.getParameter("Button")!=null || request.getParameter("template")!=null){ %>	
					<tr>
					<td>
						<label >Date:</label>
					</td>
					<td>
						<input type="text" name="process_Date" id="process_Date"  onfocus="showCalendarControl(this);" value="<%=master.getProcessDate()%>"  class="dontEdit" readonly="readonly"  />
					</td>
				<td><label >Program</label>
				</td>
				<td>
				
					<select name="template" onchange="submitThisForm()">
						<%for(int i=0;i<charges.size();i++) { %>
						
							<option value="<%=(charges.get(i)).getPayTemplateKey() %>" 
							<%if(master.getMasterkey() !=null && master.getMasterkey().equals(""+charges.get(i).getPayTemplateKey())){%> selected <%} %>
							><%=(charges.get(i)).getPayTemplateDesc() %></option>
							
						<%} %>
					</select>
				</td>
				
					</tr>
</table>

<div id="calc" style="width:30%;margin-left: 80%;height:270px;margin-top:0%;position: fixed;">
  <object type="text/html" data="charges/calc.html"
            style="width:100%; height:100%; margin:1%;">
    </object></div>
       <div id="jobDetailsLogsDiv"  style="background-color:#ceecf5;display:none;position: absolute; z-index: 3000;margin-left: 10%"><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeJobsDetailsLogsPopUp()">
   <table id="tabForDetails" style="z-index: 3000; width: 750px; cursor: pointer;">
		<tr style="background-color: black;">
		<td id="detailsOR" onclick="openORDetails('')">
		<font id="detailsORFont" color="white">Details</font>
		</td> 
		<td id="detailsLogs" style="background-color: white;" onclick="showLogs()">
		<font id="detailsLogsFont" color="black">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
  <div id="jobDetailsLogsPopUp"></div></div>
<table  class="pretty-table"  style="margin-left:20%;width:47%">
<tr style="background-color: silver;">
<td colspan="4">
<font style="font-weight: bold;">Opening Balance:</font>
</td><td align="right" colspan="2">
<input type="text" size="4"  class="dontEdit" readonly="readonly"  name="openingBalance" id="openingBalance" value="<%=String.format("%.2f", master.getOpeningBalance()) %>" ></input>
</td>
</tr>
<tr><thead>
<%if(listOfPayment.size()>0){ %>
<th scope="col">Date</th>
<th scope="col">Acct</th>
<th scope="col" >Charged amt</th>
<th scope="col" colspan="2">Driver Pmt Amt</th>
<%}else{ %>
<th scope="col">Date</th>
<th scope="col">Description</th>
<th scope="col" >Charged amt</th>
<th scope="col" colspan="2">Driver Pmt Amt</th>
<%} %>


<th>    <input type="checkbox" id="ckbCheckAll" checked="checked" onchange="calculateJobWiseSubTotal()" onclick="calculateJobWiseSubTotal()" /></th>
</thead></tr>
<%if(listOfCharges.size()>0){ %>
<%String previousTripCharges="YYY";%>
<%String trip="";%>
<%int countCharges=0; %>
<%for(int i=0;i < listOfCharges.size();i++){
if(!(listOfCharges.get(i).getTripID()).equals(previousTripCharges)){ %>

	<%previousTripCharges=listOfCharges.get(i).getTripID();%>
	<%countCharges=0; boolean foundjobCharges = false; %>
<%for(int j=0;j<listOfJobs.size();j++){ %>

<%if((listOfCharges.get(i).getTripID()).equals(listOfJobs.get(j).getTripid())){ %>
<%if(i>0){ %>
</table></td></tr>
<%} %>
<tr><td  scope="border" colspan="6" style="border: threedshadow;"><table  class="pretty-table" style="border: thick;width:100%">

<%foundjobCharges = true; %>
<tr class="alt">
<td scope="row">
<%=listOfJobs.get(j).getSdate() %>
</td>
<td scope="row">
<input type="hidden" id="tripId<%=j %>" value="<%=listOfJobs.get(j).getTripid() %>"/>
TripId:<%=listOfJobs.get(j).getTripid() %></td>
<input type="hidden" name="tripId" id="tripId" value="<%=listOfJobs.get(j).getTripid() %>"/>
<td scope="row">
P/U:
<%=listOfJobs.get(j).getSadd1()%>
</td>
<td align="center" scope="row" ><input type="button" id="detailShow_<%=i %>" value="Details" onclick="openORDetails(<%=listOfCharges.get(i).getTripID()%>)"/></td>
<td align="right" scope="row">
	<input type="text"  class="dontEdit" readonly="readonly"  size="4" name="subTotal_<%=listOfCharges.get(i).getTripID()%>" id="subTotal_<%=listOfCharges.get(i).getTripID()%>" value="" align="right"/>
	</td>
	<td scope="row" colspan="0.5"> 
	<input type="checkbox"  class="checkBoxClass" id="chck<%=listOfCharges.get(i).getTripID()%>" name="chck<%=listOfCharges.get(i).getTripID() %>" value="1" checked="checked" onchange="calculateJobWiseSubTotal()" onclick="calculateJobWiseSubTotal()"/>
	</td>
</td>

</tr>
<%j=listOfJobs.size(); %>
<%}%>
	<%} %>
<%if(!foundjobCharges){%>
	<tr >
		<input type="checkbox"  class="checkBoxClass" id="chck<%=listOfCharges.get(i).getTripID()%>" name="chck<%=listOfCharges.get(i).getTripID() %>" value="1" checked="checked" onchange="calculateJobWiseSubTotal()" onclick="calculateJobWiseSubTotal()"/>
	<td >Date:<%=listOfCharges.get(i).getDC_PROCESSING_DATE_TIME() %>
</td>
	<td >
	TripId:<%=listOfCharges.get(i).getTripID() %>
<input type="hidden" name="tripId" id="tripId" value="<%=listOfCharges.get(i).getTripID() %>"/></td>
	<td>
	P/U:
	</td>
	<td align="center"  ><input type="button" id="detailShow_<%=i %>" value="Details" onclick="openORDetails(<%=listOfCharges.get(i).getTripID()%>)"/></td>
	<td align="right" colspan="2" >
	<input type="text" class="dontEdit" readonly="readonly" size="4" align="right" name="subTotal_<%=listOfCharges.get(i).getTripID()%>" id="subTotal_<%=listOfCharges.get(i).getTripID()%>" value="" align="right"/>
	</td>

	</tr>

<% }%>

<% }%>
<tr  class="alt"><td><%=listOfCharges.get(i).getDC_PROCESSING_DATE_TIME() %></td>
<td><%=listOfCharges.get(i).getDC_TYPE_DESC()==null?"":listOfCharges.get(i).getDC_TYPE_DESC() %></td>
<%if(master.getChargeCalcType()==IChargeConstants.PAYMENT_TYPE_CHARGE){ %>
<td><tt>Amt. Received</tt>
<br />
<tt><%=listOfCharges.get(i).getTotalamount() %></tt></td>
<td>
&nbsp;&nbsp;&nbsp;<tt style="size: 4">Tax1</tt>&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;<tt style="size: 4">Tax2</tt>&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;<tt style="size: 4">Tax3</tt>
	<br />
	<input type="text" size="4" id="temp_tax1_<%=listOfCharges.get(i).getTripID()%>_<%=countCharges %>" name="temp_tax1_<%=listOfCharges.get(i).getTripID()%>_<%=countCharges %>" value="<%= listOfCharges.get(i).getDC_DRIVER_PMT_TAX1_AMT()==null?"0":listOfCharges.get(i).getDC_DRIVER_PMT_TAX1_AMT() %>"
		onkeyup="taxesCalculate('<%=listOfCharges.get(i).getTripID() %>', '<%=countCharges %>', '<%=i %>')" />
	<input type="text" size="4" id="temp_tax2_<%=listOfCharges.get(i).getTripID()%>_<%=countCharges %>" name="temp_tax2_<%=listOfCharges.get(i).getTripID()%>_<%=countCharges %>" value="<%= listOfCharges.get(i).getDC_DRIVER_PMT_TAX2_AMT()==null?"0":listOfCharges.get(i).getDC_DRIVER_PMT_TAX2_AMT() %>"
		onkeyup="taxesCalculate('<%=listOfCharges.get(i).getTripID() %>', '<%=countCharges %>', '<%=i %>')" />
	<input type="text" size="4" id="temp_tax3_<%=listOfCharges.get(i).getTripID()%>_<%=countCharges %>" name="temp_tax3_<%=listOfCharges.get(i).getTripID()%>_<%=countCharges %>" value="<%= listOfCharges.get(i).getDC_DRIVER_PMT_TAX3_AMT()==null?"0":listOfCharges.get(i).getDC_DRIVER_PMT_TAX3_AMT() %>"
		onkeyup="taxesCalculate('<%=listOfCharges.get(i).getTripID() %>', '<%=countCharges %>', '<%=i %>')" />
	
	<%-- <tt style="width: 30%"><%=listOfCharges.get(i).getDC_DRIVER_PMT_TAX1_AMT()==null?"0":listOfCharges.get(i).getDC_DRIVER_PMT_TAX1_AMT() %></tt>&nbsp;
	&nbsp;<tt style="width: 30%"><%=listOfCharges.get(i).getDC_DRIVER_PMT_TAX2_AMT()==null?"0":listOfCharges.get(i).getDC_DRIVER_PMT_TAX2_AMT() %></tt>&nbsp;
	&nbsp;<tt style="width: 30%"><%=listOfCharges.get(i).getDC_DRIVER_PMT_TAX3_AMT()==null?"0":listOfCharges.get(i).getDC_DRIVER_PMT_TAX3_AMT() %></tt> --%>
</td>
<%} else{%>
<td colspan="2"><%=listOfCharges.get(i).getTotalamount() %></td>
<%} %>

<td align="right" ><input type="text" size="4" id="drvAmt<%=listOfCharges.get(i).getTripID()%>_<%=countCharges %>" name="drvAmt<%=listOfCharges.get(i).getTripID()%>_<%=countCharges %>" value="<%= listOfCharges.get(i).getDC_DRIVER_PMT_AMT()==null?"0":String.format("%.2f",listOfCharges.get(i).getDC_DRIVER_PMT_AMT()) %>"
												onchange="calculateJobWiseSubTotal()"
											/>
					<input type="hidden" name="txNo<%=listOfCharges.get(i).getTripID()%>_<%=countCharges%>" value="<%=listOfCharges.get(i).getTXNO() %>" />						
										<input type="hidden" name="chargeType<%=listOfCharges.get(i).getTripID()%>_<%=countCharges%>" value="<%=listOfCharges.get(i).getDC_TYPE_CODE() %>" />						
											</td>
											
<%countCharges++; %>
</tr>

<%if(i==(listOfCharges.size()-1)){ %>
</table></td></tr>
<%}else{ 
if(!listOfCharges.get(i).getTripID().equals(listOfCharges.get(i+1).getTripID())){%>

<%for(int j=0;j<paymentForCharges.size();j++){ %>
<%if(listOfCharges.get(i).getTripID().equals(paymentForCharges.get(j).getTripID())){ %>
<tr>
<td colspan="2"><%=paymentForCharges.get(j).getPaymentMode()==null?"Voucher/CC":paymentForCharges.get(j).getPaymentMode() %>:<%=paymentForCharges.get(j).getVcardno().equals("")?"XXXX":paymentForCharges.get(j).getVcardno() %></td>
<td colspan="2"><%=paymentForCharges.get(j).getTotalamount() %>
<%String  btnDisable ="";
							 if(paymentForCharges.get(j).getVerifiedStatus()>1 || paymentForCharges.get(j).getDC_TYPE_CODE()==1){
									btnDisable = "disabled=disabled";
								}%>
<input type="button" name="verifySingle" id="verifySingle<%=i%>" value="Mark as verified"<%=btnDisable %> onclick="verifySingleVoucher(<%=i%>,<%=paymentForCharges.get(j).getTXNO()%>)"></input>

</td>
</tr>
<%} %>
<%} %>
<%} %>
<%} %>
<%} %>
<tr>
				<td colspan="4" align="center"> <font size="3" ><b> Total</b> </font></td>
				<td colspan="1" align="right">
						<input type="text" name="grantTotal"  id="grandTotal" size="4"  class="dontEdit" readonly="readonly"  name="" value=""/>
				</td>
			</tr>
			
		<%}else{ %>	
<%String prevTrip="YYY";%>
<%int count=0; %>
<%for(int i=0;i < listOfPayment.size();i++){
if(!(listOfPayment.get(i).getTripID()).equals(prevTrip)){ %>
<%if(i>0){ %>
</table></td></tr>
<%} %>
<tr><td  scope="border" colspan="6" style="border: threedshadow;"><table  class="pretty-table" style="border: thick;width:100%">

	<%prevTrip=listOfPayment.get(i).getTripID();%>
	<%count=0; boolean foundjob = false; %>
<%for(int j=0;j<listOfJobs.size();j++){ %>
<%if((listOfPayment.get(i).getTripID()).equals(listOfJobs.get(j).getTripid())){ %>
<%foundjob = true; %>
<tr  class="alt">
<td >
<%=listOfJobs.get(j).getSdate() %>
</td>
<td  >
<input type="hidden" id="tripId<%=j %>" value="<%=listOfJobs.get(j).getTripid() %>"/>
TripId:<%=listOfJobs.get(j).getTripid() %>
<input type="hidden" name="tripId" id="tripId" value="<%=listOfPayment.get(i).getTripID() %>"/></td>

<td>
P/U:
<%=listOfJobs.get(j).getSadd1()%>
</td>
<td align="center" scope="row" ><input type="button" id="detailShow_<%=i %>" value="Details" onclick="openORDetails(<%=listOfPayment.get(i).getTripID()%>)"/></td>
<td align="right" scope="row">
	<input type="text"  class="dontEdit" readonly="readonly"  size="4" align="right" name="subTotal_<%=listOfPayment.get(i).getTripID()%>" id="subTotal_<%=listOfPayment.get(i).getTripID()%>" value="" align="right"/>
	
	</td>
<td colspan="0.5">
<input type="checkbox"  class="checkBoxClass" id="chck<%=listOfJobs.get(j).getTripid()%>" name="chck<%=listOfJobs.get(j).getTripid() %>" value="1" checked="checked" onchange="calculateJobWiseSubTotal()" onclick="calculateJobWiseSubTotal()"/>
</td>

</tr>
<%j=listOfJobs.size(); %>
<%}%>
	<%} %>
<%if(!foundjob){%>
	<tr >
	<td >
		<input type="checkbox"  class="checkBoxClass" id="chck<%=listOfPayment.get(i).getTripID()%>" name="chck<%=listOfPayment.get(i).getTripID() %>" value="1" checked="checked" onchange="calculateJobWiseSubTotal()" onclick="calculateJobWiseSubTotal()"/>
	
	<%=listOfPayment.get(i).getDC_PROCESSING_DATE_TIME() %>
</td>
	<td >
	TripId:<%=listOfPayment.get(i).getTripID() %>
	<input type="hidden" name="tripId" id="tripId" value="<%=listOfPayment.get(i).getTripID() %>"/></td>
	<td scope="col">
	P/U:
	</td>
	<td align="center"  ><input type="button" id="detailShow_<%=i %>" value="Details" onclick="openORDetails(<%=listOfPayment.get(i).getTripID()%>)"/></td>
	<td align="right">
		<input type="text"  class="dontEdit" readonly="readonly"  size="4" name="subTotal_<%=listOfPayment.get(i).getTripID()%>" id="subTotal_<%=listOfPayment.get(i).getTripID()%>" value="" align="right"/>
	
	</td>

	</tr><table  class="pretty-table"  style="margin-left:20%;width:47%">
<tr style="background-color: silver;">
<td colspan="4">
<font style="font-weight: bold;">Opening Balance:</font>
</td><td align="right" colspan="2">
<input type="text" size="4"  class="dontEdit" readonly="readonly"  name="openingBalance" id="openingBalance" value="<%=master.getOpeningBalance() %>" ></input>
</td>
</tr>

<% }%>

<% }%>
<tr  class="alt"><td><%=listOfPayment.get(i).getDC_PROCESSING_DATE_TIME() %></td>
<td><%=listOfPayment.get(i).getDC_TYPE_DESC() %>:<%=listOfPayment.get(i).getVcardno() %></td>
<td colspan="2"><%=listOfPayment.get(i).getTotalamount() %>
<%String  btnDisable ="";
							 if(listOfPayment.get(i).getVerifiedStatus()>1){
									btnDisable = "disabled=disabled";
								}%>
<input type="button" name="verifySingle" id="verifySingle<%=i%>" value="Mark as verified"<%=btnDisable %> onclick="verifySingleVoucher(<%=i%>,<%=listOfPayment.get(i).getTXNO()%>)"></input>
</td>
<td align="right" colspan="2"><input type="text" size="4" id="drvAmt<%=listOfPayment.get(i).getTripID()%>_<%=count %>" name="drvAmt<%=listOfPayment.get(i).getTripID()%>_<%=count%>" value="<%=listOfPayment.get(i).getDC_DRIVER_PMT_AMT()==null?"0":String.format("%.2f",listOfPayment.get(i).getDC_DRIVER_PMT_AMT()) %>"
												onchange="calculateJobWiseSubTotal()"
											/>
					<input type="hidden" name="txNo<%=listOfPayment.get(i).getTripID()%>_<%=count %>" value="<%=listOfPayment.get(i).getTXNO()%>"/>						
																<input type="hidden" name="chargeType<%=listOfPayment.get(i).getTripID()%>_<%=count%>" value="<%=listOfPayment.get(i).getDC_TYPE_CODE() %>" />						
											</td>
							
											
<%count++; %>

</tr>
<%for(int j=0;j<chargesForPayment.size();j++){ %>
<%if(listOfPayment.get(i).getTripID().equals(chargesForPayment.get(j).getTripID())){ %>
<tr>
<td><%=chargesForPayment.get(j).getDC_TYPE_DESC()==null?"":chargesForPayment.get(j).getDC_TYPE_DESC() %></td>
<td colspan="2"><%=chargesForPayment.get(j).getTotalamount() %></td>
</tr>
<%} %>
<%} %>
<%if(i==(listOfPayment.size()-1)){ %>
</table></td></tr>
<%} %>
<%} %>
<tr>
				<td colspan="4" align="center"> <font size="3" ><b>Total</b> </font></td>
				<td colspan="1" align="right">
						<input type="text" name="grantTotal"  id="grandTotal" size="4"  class="dontEdit" readonly="readonly"  name="" value=""/>
				</td>
			</tr>
<%} %>
</table>
	<%}else if(request.getParameter("dd_driver_id")!=null && !request.getParameter("dd_driver_id").equals("")){%>
		<table  class="pretty-table"  style="margin-left:20%;width:47%">
<tr style="background-color: silver;">
<td colspan="4">
<font style="font-weight: bold;">Opening Balance:</font>
</td><td align="right" colspan="2">
<input type="text" size="4"  class="dontEdit" readonly="readonly"  name="openingBalance" id="openingBalance" value="<%=master.getOpeningBalance() %>" ></input>
</td>
</tr>
</table>
	<%} %>
	<%if(request.getParameter("dd_driver_id")!=null && !request.getParameter("dd_driver_id").equals("")){ %>
		<table class="pretty-table" width="47%"   style="margin-left:20%" >
				<tr>
			<%
				double leaseTotal = 0.0;
			%>
		
					<td colspan="6">
					<div id="leaseid" >
						<table  width="100%" border="1" cellspacing="1" cellpadding="0" >
							<tr style="background-color: aqua;">
								<th scope="col">Vehicle Type</th>
								<th scope="col">Cab No</th>
								<th scope="col">Lease Amount</th>
								<th scope="col">Remove</th>
							</tr>
			<%
				for(int i=0;i<listOfLeases.size();i++)
				{
					LeaseBean leaseBean = listOfLeases.get(i);
					leaseTotal = leaseTotal + leaseBean.getVechAmount();
			%>
				
							<tr>
								<td>
									 <%=leaseBean.getVechDesc() %>
								</td>
								<td>
									 <%=leaseBean.getVechileNo() %>
								</td>
								<td>
									 <%=leaseBean.getVechAmount() %>
								</td>
								<td>
									<a href="javascript:doNothing()" onclick="fnRemoveLease(<%=leaseBean.getLeaseID() %>)">Remove</a>
								</td>
							</tr>
						
			<%} %>
			<tr>
			<td>
						<a href="javascript:doNothing()"  onclick="fnCallLease()">Add</a>
					</td>
				</tr>
			</table></div></td></tr></table>
					
			<table class="pretty-table"  width="47%"  style="margin-left:20%" >	
			<tr>
				<td colspan="5" align="center" > <font size="3" ><b> Lease Total</b> </font></td>
				<td colspan="1" align="right">
						<input type="text" name="leaseTotal"  id="leaseTotal" size="4"  class="dontEdit" readonly="readonly"  name="" value="<%=leaseTotal %>"/>
				</td>
				
			</tr>	</table>
						<table class="pretty-table" width="47%"  style="margin-left:20%">	
			
			<%
				double miscTotal = 0.0;
			%>
			<tr>
					<td colspan="6">
					<div id="miscid" >
					
						<table width="100%" class="pretty-table"  >
							<tr >
								<th scope="col">Misc Desc</th>
							 <th scope="col">Comments</th>
								<th scope="col">Misc Amount</th>
								<th scope="col">Remove</th>
							</tr>
			<%
				for(int i=0;i<listOfMiscCharges.size();i++)
				{
					MiscBean miscBean = listOfMiscCharges.get(i);
					miscTotal = miscTotal + miscBean.getMiscAmount();
			%>
				
							<tr>
								<td>
									 <%=miscBean.getMiscDesc() %>
								</td>
								 <td>
								 <%=miscBean.getCustomField()==null?"":miscBean.getCustomField()%>
								 </td>
								<td>
									 <%=miscBean.getMiscAmount() %>
								</td>
								<td>
									<a href="javascript:doNothing()"onclick="fnRemoveMisc(<%=miscBean.getMiscid() %>)">Remove</a>
								</td>
							</tr>
					
			<%} %>
			<tr>
					<td>
						<a href="javascript:doNothing()"  onclick="fnCallMisc()">Add</a>
					</td>
				</tr>		
						</div>
					</td>
				</tr>
				</table>
			</div></td></tr>
				<tr>
				<td colspan="5" align="center"> <font size="3" ><b> Misc Total</b> </font></td>
				<td colspan="1" align="right">
						<input type="text" name="miscTotal"  id="miscTotal" size="4"  class="dontEdit" readonly="readonly"  name="" value="<%=miscTotal %>"/>
				</td>
			</tr>
			<tr>
			<td align="center" colspan="5">
			Closing Balance
			</td>
			<td align="right">	
			<input type="text" name="closingBalance"  id="closingBalance" size="4"  class="dontEdit" readonly="readonly"  name="" value=""/>
			</td></tr>
	</div></td></tr>
	<tr>
	<td>
					<label> Payment</label>
				</td>
				<td>
					<%HashMap<String,String> hm_pay = IPaymentToDriver.getDriverPayType(); %>
					<select name="paymentToType">
					<%
						for (Map.Entry<String,String> entry : hm_pay.entrySet()) {
					%>
						<option value="<%=entry.getKey() %>"><%=entry.getValue() %></option>
					<%		
						}
					
					%>
					</select>
				</td>
	<td  align="center">
	<input type="submit" name="recordPayment" value="Record Payment"/>
	</td>
	</tr></table>
	<%} %>
	
	
	
	
	<table id="tripsAndNumber" style="display: none;">
	<%for(int k=0;k<totals.size();k=k+2){ %>
		<tr>
		<td id="tripId_<%=k/2%>"><%=(String)totals.get(k)%></td>
		<td id="total_<%=k/2%>"><%=(String)totals.get(k+1)%></td>
		<%-- <td id="subTotal_<%=(String)totals.get(k)%>"></td> --%>
		<td>
		<input type="hidden" name="tripID_<%=k/2%>" value="<%=(String)totals.get(k)%>"/>
		<input type="hidden" name="totalIter_<%=k/2%>" value="<%=(String)totals.get(k+1)%>"/>
		</td>
		</tr>
		
	<%} %>
	
	</table>
	
</div>
<!-- 	<input type="text" id="total" value=""/>
 -->	
 <div class="jqmWindow" id="addLease">
 <table><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeLeasePopUp()">
 <tr id="resultLease">
 </tr>
				<tr>
					<td align="center"> <font color="blue">Lease Deatils</font>
					</td>
				</tr>	
				<tr>
					<td>
						<table>
							<tr>
								<th>Vehicle Type</th>
								<th>Cab No</th>
								<th>Lease Amount</th>
							</tr>
							<tr>
								<td>
									<Select name="vechType" id='vechType' onchange="loadLeaseAmount()">
										<option value=""></option>
										<%
										for(int j=0;j<lease.size();j++) 
										{
											
										%>
										<option value="<%=lease.get(j) %>"><%=lease.get(j) %></option>
										<%} %>	
									</Select>
								</td>
								<td>
									<input type="text" name="cabNo" id="cabNo" value="" size="6">
								</td>
								<td>
									<input type="text" name="leaseAmount" id="leaseAmount" value="0.00" size="10">
								</td>
							</tr>
							<tr>
						<td colspan="3" align="center">
							<input type="button" name="leaseButton" value="Submit" onclick="submitLease()">
						</td>
					</tr>
						</table>
					</td>
				</tr>	
				</table>
 </div>

 <div class="jqmWindow" id="addMisc">
 	<table><img alt="" align="right" src="images/Dashboard/close.png" onclick="removeMiscPopUp()"></img>
 	 <tr id="resultMisc">
 	 
 	</tr>
 	<tr>
 	<select name="showMisc" id='showMisc' onChange="showMiscType()" >
 	
 											<option value="1">Template</option>
 											<option value="2">Manual Misc</option>
 	</select>
 	</tr>
					<tr >
						<th> Misc Description</th>
						<th> Custom Field</th>
						<th> Amount</th>
					</tr>
					<tr id="miscTemplate">
					<td>
									<Select name="miscName_temp" id='miscName_temp' onchange="loadMiscAmount()"  >
										<option value=""></option>
										<%
										for(int j=0;j<misc.size();j++) 
										{
											
										%>
										<option value="<%=misc.get(j).getKey() %>"><%=misc.get(j).getMiscDesc() %> </option>
										
										<%} %>	
									</Select>
									<%for(int j=0;j<misc.size();j++) 
									{
										 %>
										 <input type="hidden" id="miscDesc_temp" value="<%=misc.get(j).getMiscDesc()%>"/>
										 <input type="hidden" id="<%=misc.get(j).getKey() %>" value="<%=misc.get(j).getAmount()%>"/>
										 <%} %>
								</td>
							<td> <input type="text" name="customField_temp" id="customField_temp" value=""/></td>
						<td> <input type="text" name="miscAmount_temp" id="miscAmount_temp" value=""/></td>
					</tr> 
					<tr id="miscManual" style="display: none;" >
						<td> <input type="text" name="miscName_manual" id="miscName_manual" value=""/></td>
						<td> <input type="text" name="customField_manual" id="customField_manual" value=""/></td>
						<td> <input type="text" name="miscAmount_manual" id="miscAmount_manual" value=""/></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="button" name="button" value="Submit" onclick="submitMisc()"/>
						</td>
					</tr>
				</table>
				
 </div>
  		 <div id="driverShiftPop" class="jqmWindow" style="display: none;">
 <table id="driverShiftDetails" style="width:100%;border: thin;border-color: black" >
 <tbody id="driverShiftDetailsBody" style="border: thick;">
 
 </tbody>
 </table>
 
 </div>
	
		<div id="jobDetailsMapDash" class="jqmWindow" style="display: none;height:430px;width: 750px;margin-left:10%;z-index:3000;">
		<table id="tabForDetails" style="z-index: 3000; width: 750px; cursor: pointer;">
		<tr style="background-color: black;">
		<td id="detailsOR" onclick="openORDetails('')">
		<font id="detailsORFont" color="white">Details</font>
		</td> 
		<td id="detailsLogs" onclick="showLogs()">
		<font id="detailsLogsFont" color="white">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
				<div id="jobDetailsMapDashDiv" style="height:330px;width: 550px;margin-left:10%;">
	
	</div>
		</div>
		
			<div id="paymentDetailsDash" class="jqmWindow" style="display: none;height:330px;width: 550px;margin-left:10%;">
			<table id="tabForDetails" style="z-index: 3000; width: 550px; cursor: pointer;">
		<tr style="background-color: black;">
		<td id="detailsOR" onclick="openORDetails('')">
		<font id="detailsORFont" color="white">Details</font>
		</td> 
		<td id="detailsLogs" onclick="showLogs()">
		<font id="detailsLogsFont" color="white">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
				<div id="paymentDetailsDashDiv" style="height:330px;width: 550px;margin-left:10%;">
	
	</div>
		</div>
		
 
  <div id="popUpOR" style="z-index:3000">
 		 <div id="ORDash" class="jqmWindow" style="display: none;height:330px;width: 550px;margin-left:10%">
 		 <table id="tabForDetails" style="z-index: 3000; width: 550px; cursor: pointer;margin-top:-3%;">
		<tr style="background-color: black;">
		<td id="detailsOR" style="background-color: white;" onclick="openORDetails('')">
		<font id="detailsORFont" color="black">Details</font>
		</td> 
		<td id="detailsLogs" onclick="showLogs()">
		<font id="detailsLogsFont" color="white">Logs</font>
		</td>
		<td  id="detailsPayment" onclick="showPayment()">
		<font id="detailsPaymentFont" color="white">Payment Details</font>
		</td>
		<td id="mapForJobDetails" onclick="jobDetailsMap()" >
		<font id="mapForJobDetailsFont" color="white">Map</font>
		</td>
		</tr>
		</table>
			<jsp:include page="/jsp/OpenRequestForDetails.jsp" />
		</div>
		
		</div>
 </form>		
</body>
</html>
