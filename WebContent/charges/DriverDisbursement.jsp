<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="com.ibm.icu.text.Bidi"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.cmp.bean.CashSettlement" %>
   


<%@page import="com.charges.bean.DriverChargesBean"%>
<%@page import="com.tds.action.OpenRequest"%>
<%@page import="com.tds.tdsBO.OpenRequestBO"%>
<%@page import="com.charges.bean.DisbursementMaster"%>
<%@page import="com.charges.constant.IChargeConstants"%><%@page import="com.charges.bean.ChargesBO"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.charges.constant.IPaymentToDriver"%>
<%@page import="java.util.Map"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.charges.bean.LeaseBean"%>
<%@page import="com.charges.bean.MiscBean"%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
</head>
<script>

function ajaxCall(url, callback) {
	//alert("Inside ajaxcall");
    var req = init();
    req.onreadystatechange = processRequest;
    function init() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    } 
    
    function processRequest () {
      // readyState of 4 signifies request is complete
      if (req.readyState == 4) {
        // status of 200 signifies sucessful HTTP call
        if (req.status == 200) {
          if (callback) callback(req.responseText);
        }
      }
    }

    this.doGet = function() {
      req.open("GET", url, true);
      req.send(null);
    }
}



//------ Ajax InterAction  Over    -------------------------------

function fnRemoveLease(leaseID)
{
	var url = '/TDS/AjaxClass?event=removeLease&id='+leaseID;
	var ajax = new ajaxCall(url,refreshLeaseAndMisc);
	ajax.doGet();
	 
}

function fnRemoveMisc(miscID)
{
	var url = '/TDS/AjaxClass?event=removeMisce&id='+miscID;
	var ajax = new ajaxCall(url,refreshLeaseAndMisc);
	ajax.doGet();
	 
}

function fnCallLease()
{
	document.getElementById('chargeType').value = 2
	fnCallShowLeaseAndMisc();
}
function fnCallMisc()
{
	document.getElementById('chargeType').value = 1
	fnCallShowLeaseAndMisc();
}
function fnCallShowLeaseAndMisc(){
	var argument = new Array();
	argument[0] = document.getElementById("dd_drv_id").value;
	argument[1]= document.getElementById("assoccode").value;
	argument[2]= document.getElementById("dName").value;
	argument[3] = document.getElementById("process_Date").value;
	var retValue = window.showModalDialog("/TDS/control?action=ChargesAction&event=showChargeAdd&type="+document.getElementById('chargeType').value,argument,"dialogWidth:800px;dialogHeight:600px;dialogleft:400px;dialogTop:400px");

	refreshLeaseAndMisc();
	
	
	//calculateTotal();
}

function refreshLeaseAndMisc()
{
	if(document.getElementById('chargeType').value == 2)
	{
		var url = '/TDS/AjaxClass?event=getLeaseDetail&assoccode='+document.getElementById('assoccode').value+'&driverId='+document.getElementById('dd_drv_id').value;
		var ajax = new ajaxCall(url,fillLeaseDetail);
		ajax.doGet();
	} else {
		var url = '/TDS/AjaxClass?event=getMiscDetail&assoccode='+document.getElementById('assoccode').value+'&driverId='+document.getElementById('dd_drv_id').value;
		var ajax = new ajaxCall(url,fillMiscDetail);
		ajax.doGet();
	}
}

function fillLeaseDetail(responseText)
{
	//alert(responseText);
	var resText = responseText.split("###");
	document.getElementById("leaseid").innerHTML = resText[0];
	document.getElementById("leaseTotal").value =  parseFloat(resText[1]).toFixed(2);
	calculateTotal();
}

function fillMiscDetail(responseText)
{
	//alert(responseText);
	var resText = responseText.split("###");
	document.getElementById("miscid").innerHTML = resText[0];
	document.getElementById("miscTotal").value =  parseFloat(resText[1]).toFixed(2);
	calculateTotal();
}
function showOrHideBalance(){
	 var img=document.getElementById("imgReplaceBalance").src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgReplaceBalance").src="images/minus_icons.png";
			$("#openingBalanceDiv").show("slow");
		}else{
			document.getElementById("imgReplaceBalance").src="images/plus_icons.png";
			$("#openingBalanceDiv").hide("slow");
		}
}
function showOrHideLease(){
	 var img=document.getElementById("imgReplaceLease").src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgReplaceLease").src="images/minus_icons.png";
			$("#leaseid").show("slow");
		}else{
			document.getElementById("imgReplaceLease").src="images/plus_icons.png";
			$("#leaseid").hide("slow");
		}
}
function showOrHideMisc(){
	 var img=document.getElementById("imgReplaceMisc").src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgReplaceMisc").src="images/minus_icons.png";
			$("#miscid").show("slow");
		}else{
			document.getElementById("imgReplaceMisc").src="images/plus_icons.png";
			$("#miscid").hide("slow");
		}
}

function showorHideChargesTable(row){
	 var img=document.getElementById("imgForCharges_"+row).src;
		if(img.indexOf("plus_icons.png")!= -1){
			document.getElementById("imgForCharges_"+row).src="images/minus_icons.png";
			$("#chargesTable_"+row).show("slow");
		}else{
			document.getElementById("imgForCharges_"+row).src="images/plus_icons.png";
			$("#chargesTable_"+row).hide("slow");
		}
}
</script>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script>
   window.history.forward(-1);
</script>
<title>Insert title here</title>
<%
	DisbursementMaster master = request.getAttribute("master")==null?new DisbursementMaster():(DisbursementMaster)request.getAttribute("master");
	ArrayList<OpenRequestBO> al_List = master.getAl_opr();
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver") == null ? new ArrayList():(ArrayList)request.getAttribute("al_driver");
	ArrayList al_tmplt = (ArrayList)request.getAttribute("al_tmplt") == null ? new ArrayList():(ArrayList)request.getAttribute("al_tmplt");
   
%>

<script type="text/javascript">


function showRepot(repo,key)
{
	 

	
	if(repo == 1)
	{
		//var path = <%=request.getContextPath()%>;
		window.open('<%=request.getContextPath()%>/frameset?__report=BIRTReport/DriverDisburshment.rptdesign&__format=pdf&pay_id='+key);
	} else {
		 
	}
}

function calculateTotal()
{
	var grantTotal=0.0;
	var orsize = document.getElementById("orsize").value;
	
	for(var i=0 ;i < parseInt(orsize);i++)
	{
		//alert(document.getElementById('chck'+i).checked);
		if(document.getElementById('chck'+i).checked == true)	
		{
			var total=0.0;
			var dtlsize = document.getElementById("drcsize_"+i).value
			
			for(var j=0 ;j < parseInt(dtlsize);j++)
			{
				total = parseFloat(total) + parseFloat(document.getElementById("driverAmt"+i+"_"+j).value);
			}
			document.getElementById("totalamount"+i).value=total;
			grantTotal = grantTotal + total;
		}
	}
	document.getElementById("grantTotal").value=parseFloat(grantTotal).toFixed(2);
	var openingBalance = document.getElementById("openingBalance").value;

	var leaseTotal = document.getElementById("leaseTotal").value;
	var openingBalance = document.getElementById("openingBalance").value;
	var miscTotal = document.getElementById("miscTotal").value;
	
	 
	document.getElementById("closingBalance").value = (parseFloat(openingBalance) + parseFloat(leaseTotal) + parseFloat(miscTotal)+ parseFloat(grantTotal)).toFixed(2); 
}

function submitThisForm()
{
	document.masterForm.Button[0].click();
}
function checkFields(){
	var driver=document.getElementById("dd_driver_id").value;
	if(driver==""){
		alert("Enter a driver Id to get the disbursement detail");
		return false;
	}
		return true;
}
</script>
<script type="text/javascript">
	showRepot('<%=request.getParameter("showRepo")==null?"":"1" %>',<%=request.getParameter("key")==null?"0":request.getParameter("key") %>);
</script>
<body>
<form  name="masterForm" action="control" method="post" onsubmit="return checkFields()">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	<input type="hidden" name="action" value="ChargesAction"/>
	<input type="hidden" name="event" value="showDriverDisbursement"/> 
	<input type="hidden" name="assoccode" id="assoccode" value="<%=((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode() %>"/>
	<input type="hidden" name="oprtype" value=""/>
	<input type="hidden" name="chargeType" value="<%=master.getChargeCalcType() %>"/>
	<input type="hidden" name="size" id="size" value="<%=al_List!=null?al_List.size():"0" %>"/>
	<input type="hidden" name="financial" value="3"/>

 
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
                	<h1>Driver Disbursement </h1>
                	<%	
					String error = (String) request.getAttribute("error");
					%>
					<div id="errorpage">
					<%= (error !=null && error.length()>0)?""+error :"" %>
					</div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
                    <td>
                    <table id="bodypage" width="540" >
                    <tr>
					<td class="firstCol">From&nbsp;Date</td>
					<td>
					<input type="text" name="from_date" onfocus="showCalendarControl(this);"  value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly">
					</td>
					<td class="firstCol">To&nbsp;Date</td>
					<td>
					<input type="text" name="to_date"  onfocus="showCalendarControl(this);" value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly">
					</td>
					</tr>
					
					<%if(!((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equalsIgnoreCase("Driver")) {%>
					<tr>
					<td class="firstCol">Driver&nbsp;Id</td>
					<td>
					<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)"  onblur="caldid(id,'drivername','dName');enableButton();disableDIV();" id="dd_driver_id" name="dd_driver_id" size="15"   autocomplete="off" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>" class="form-autocomplete">
								 <input type="hidden" name="dd_drv_id" id="dd_drv_id" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>">
						  		 <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 
				  					<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
							  		<div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>	 
					</td>
					</tr>
					<%} %>
				<tr>
					<td align="center" colspan="4">
						<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="submit" name="Button" id = "getButton" class="lft" style="text-align:center"  value="Get" >&nbsp;&nbsp;&nbsp;
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    	</div>			
				    </td>
				</tr> 
				<tr>
				<td>
					<label for="Driver Name">Process Date:</label>
				</td>
				<td>
					<input type="text" name="process_Date" id="process_Date"  onfocus="showCalendarControl(this);" value="<%=master.getProcessDate()%>" readonly="readonly">
				</td>
				<td style="display: none;">
					<select name="chargeType" id='chargeType' >
						<option value="1">Misc.</option>
						<option value="2">Lease</option>
					</select>
				</td>
				<td style="display: none;"><a href="javascript:doNothing()"  onclick="fnCallShowLeaseAndMisc()">Add</a></td>
			</tr>
				</table>
                    </td></tr>
                    </table>             
                </div>    
              </div> 
             <%if(master!=null && master.getDrivername() !=null && !master.getDrivername().equals("")) {%> 
            <div class="rightCol padT10">
			<div class="rightColIn padT10">
		 	<div id='disdetail'>
			<h5 align="center">Payment Details</h5>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
			<tr bgcolor="#505050">
				<td colspan="3" align="left" > <font size="3" ><b> Opening Balance Amount</b> </font></td>
				<td colspan="4" align="right">
						<input type="text" name="openingBalance"  id="openingBalance" size="4" readonly="readonly" name="" value="<%=(master.getOpeningBalance()).setScale(2,5).toString()%>"/>
				</td>
			</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
			<tr>
				<td colspan="3" align="left"> <font size="3" ><b> Grant Charge/CC Total Details</b> </font></td>
				<td align="right"><img alt="" id="imgReplaceBalance" src="images/minus_icons.png" onclick="showOrHideBalance()" /></td>
				</tr>
				</table>
				<table id="openingBalanceDiv"  width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
			
			<tr>
				<td>
				<input type="hidden" id ="orsize" name="orsize" value="<%=al_List!=null?al_List.size():"0" %>"/>
				<label for="Driver Name">Driver Name:</label>
				</td>
				<td><label for="Driver Name"><%=master.getDrivername() %></label>
				</td>
				</tr>
				<tr>
				<td><label for="Driver Name">Template Name</label>
				</td>
				<td>
					<select name="template" onchange="submitThisForm()">
						<%for(int i=0;i<al_tmplt.size();i++) { %>
							<option value="<%=((ChargesBO)al_tmplt.get(i)).getPayTemplateKey() %>" 
							<%=(master.getMasterkey() !=null && master.getMasterkey().equals(""+((ChargesBO)al_tmplt.get(i)).getPayTemplateKey()))?"selected=selected":"" %>
							><%=((ChargesBO)al_tmplt.get(i)).getPayTemplateDesc() %></option>
							
						<%} %>
					</select>
				</td>
			</tr>
			<tr>
				
				<td>
					<label for="Driver Name"> Payment</label>
				</td>
				<td>
					<%HashMap<String,String> hm_pay = IPaymentToDriver.getDriverPayType(); %>
					<select name="paymentToType">
					<%
						for (Map.Entry<String,String> entry : hm_pay.entrySet()) {
					%>
						<option value="<%=entry.getKey() %>"><%=entry.getValue() %></option>
					<%		
						}
					
					%>
					</select>
				</td>
				</tr></table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
			<%double grantamount=0; %>
			<%if(al_List!=null) {%>
			<%for(int i=0;i<al_List.size();i++) { %>
				<% OpenRequestBO openRequestBO = (OpenRequestBO)al_List.get(i); %>
				<% ArrayList<DriverChargesBean>  al_drivercharges = openRequestBO.getAl_drivercharges();%>
					<tr>
						<td>
						<input type="checkbox" id="chck<%=i %>" name="chck<%=i %>" value="1" checked="checked" onclick="calculateTotal()"/>
						</td>
						<td>
						<label for="Driver Name">Rider Name:</label>
						</td>
						<td><font color="blue"><%=openRequestBO.getName() %></font>
						</td>
						<td>
						<label for="Driver Name">Start Address:</label>
						</td>
						<td><font color="blue"><%=openRequestBO.getSadd1() +","+ openRequestBO.getSadd1()+","+openRequestBO.getScity() %></font>
						</td>
						<td>
						<label for="Driver Name">End Address:</label>
						</td>
						<td><font color="blue"><%=openRequestBO.getEadd1()+","+ openRequestBO.getEadd1()+","+openRequestBO.getEcity() %></font>
						</td>
						<td align="right">
						<img id="imgForCharges_<%=i%>" src="images/minus_icons.png" onclick="showorHideChargesTable(<%=i%>)"/>
						</td>
					</tr>
					<tr>
						<td colspan="10" align="center">	
							<table align="center" id="chargesTable_<%=i%>" style="width:100%">
									<tr >
										<th class="firstCol">
											Mode
										</th>
										<th>
											Charge Type
										</th>
									<%if(master.getChargeCalcType() == IChargeConstants.PAYMENT_TYPE_CC){ %>
										<th>
											Amount
										</th>	
										<th>
											Tip Amount
										</th>	
									<%} %>
									<th>
											Total Amount
									</th>
									<th>
											Driver Disbursement Amount
									</th>	
									</tr>
									<tr>
										<td>
											<input type="hidden" id="drcsize_<%=i %>"  name="drcsize_<%=i %>" value="<%=al_drivercharges.size() %>"/>
										</td>
										
									</tr>
							
				<%for(int j=0;j<al_drivercharges.size();j++) {%>
				<%
					DriverChargesBean chargesBean = al_drivercharges.get(j);
				%>
									<tr>	
										<td>
											<B><%=chargesBean.getPaymentMode() %></B>
										</td>
										<td>
											<B><%=chargesBean.getDC_TYPE_DESC()==null?"":chargesBean.getDC_TYPE_DESC() %></B>
											<input type="hidden" name="tripid<%=i+"_"+j%>" id="tripid<%=i+"_"+j%>" value="<%=chargesBean.getTripID() %>">
											<input type="hidden" name="chargeType<%=i+"_"+j%>" id="chargeType<%=i+"_"+j%>" value="<%=chargesBean.getDC_TYPE_CODE() %>">
											<input type="hidden" name="txno<%=i+"_"+j%>" id="chargeType<%=i+"_"+j%>" value="<%=chargesBean.getTXNO() %>">
										</td>
										
										<%if(master.getChargeCalcType() == IChargeConstants.PAYMENT_TYPE_CC){ %>
										<td>
											<%=chargesBean.getAmount() %>
										</td>	
										<td>
											<%=chargesBean.getTip() %>
										</td>	
									<%} %>	
										<td>
											<%=chargesBean.getTotalamount() %>
										</td>
										<td align="right">
											<input type="text" size="4" id="driverAmt<%=i+"_"+j%>" name="driverAmt<%=i+"_"+j%>" value="<%=chargesBean.getDC_DRIVER_PMT_AMT().setScale(2,5).toString() %>"
												onchange="calculateTotal()"
											>
										</td>
									</tr>
							
				<%} %>
								<tr>
									<td>
										Total Amount
									</td>
									<td>
									</td>
									<%if(master.getChargeCalcType() == IChargeConstants.PAYMENT_TYPE_CC){ %>
										<td></td><td></td>	
									<%} %>
									<td></td>
									<td align="right">
										<input type="text" name="totalamount<%=i%>"  readonly="readonly" id="totalamount<%=i%>" size="4" value="<%=new BigDecimal(openRequestBO.getTotal_amount()).setScale(2,5).toString() %>"/>
										<%
											grantamount = grantamount + openRequestBO.getTotal_amount();
										%>
									</td>
								</tr>
									</table>
			<%} }%>
			<tr>
				<td colspan="7" align="left"> <font size="3" ><b> Grant Charge/CC Total</b> </font></td>
				<td colspan="1" align="right">
						<input type="text" name="grantTotal"  id="grantTotal" size="4" readonly="readonly" name="" value="<%=new BigDecimal(grantamount).setScale(2,5).toString()%>"/>
				</td>
			</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
			<tr>
					<td colspan="7" align="left"> <font size="3" ><b> Lease Details</b> </font></td>
					<td colspan="1" align="right">
						<img alt="" id="imgReplaceLease" src="images/minus_icons.png" onclick="showOrHideLease()" />
				</td></tr>
				</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
				<tr>
			<%
				ArrayList<LeaseBean> al_lease = (ArrayList<LeaseBean>)request.getAttribute("al_lease");
				double leaseTotal = 0.0;
			%>
		
					<td colspan="4">
					<div id="leaseid" >
						<table  width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
							<tr>
								<th>Vehicle Type</th>
								<th>Cab No</th>
								<th>Lease Amount</th>
								<th>Remove</th>
							</tr>
			<%
				for(int i=0;i<al_lease.size();i++)
				{
					LeaseBean leaseBean = al_lease.get(i);
					leaseTotal = leaseTotal + leaseBean.getVechAmount();
			%>
				
							<tr>
								<td>
									 <%=leaseBean.getVechDesc() %>
								</td>
								<td>
									 <%=leaseBean.getVechileNo() %>
								</td>
								<td>
									 <%=leaseBean.getVechAmount() %>
								</td>
								<td>
									<a href="javascript:doNothing()" onclick="fnRemoveLease(<%=leaseBean.getLeaseID() %>)">Remove</a>
								</td>
							</tr>
						
			<%} %>
			<tr>
			<td>
						<a href="javascript:doNothing()"  onclick="fnCallLease()">Add</a>
					</td>
				</tr>
			</table></div></td></tr></table>
					
			<table  width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">	
			<tr>
				<td colspan="7" align="left"> <font size="3" ><b> Lease Total</b> </font></td>
				<td colspan="1" align="right">
						<input type="text" name="leaseTotal"  id="leaseTotal" size="4" readonly="readonly" name="" value="<%=leaseTotal %>"/>
				</td>
				
			</tr>	</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
				<tr>
				<td colspan="7" align="left"> <font size="3" ><b> Misc Details</b> </font></td>
				<td align="right">
						<img alt="" id="imgReplaceMisc" src="images/minus_icons.png" onclick="showOrHideMisc()" />
				</td>
			</tr>	</table>
						<table  width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">	
			
			<%
				ArrayList<MiscBean> al_misc = (ArrayList<MiscBean>)request.getAttribute("al_misc");
				double miscTotal = 0.0;
			%>
			<tr>
					<td colspan="8">
					<div id="miscid" >
					
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
							<tr>
								<th>Misc Desc</th>
							 
								<th>Misc Amount</th>
								<th>Remove</th>
							</tr>
			<%
				for(int i=0;i<al_misc.size();i++)
				{
					MiscBean miscBean = al_misc.get(i);
					miscTotal = miscTotal + miscBean.getMiscAmount();
			%>
				
							<tr>
								<td>
									 <%=miscBean.getMiscDesc() %>
								</td>
								 
								<td>
									 <%=miscBean.getMiscAmount() %>
								</td>
								<td>
									<a href="javascript:doNothing()"onclick="fnRemoveMisc(<%=miscBean.getMiscid() %>)">Remove</a>
								</td>
							</tr>
					
			<%} %>
			<tr>
					<td>
						<a href="javascript:doNothing()"  onclick="fnCallMisc()">Add</a>
					</td>
				</tr>		
						</div>
					</td>
				</tr>
				</table>
				
			
			<tr>
				<td colspan="3" align="left"> <font size="3" ><b> Misc Total</b> </font></td>
				<td colspan="4" align="right">
						<input type="text" name="miscTotal"  id="miscTotal" size="4" readonly="readonly" name="" value="<%=miscTotal %>">
				</td>
			</tr>
			<tr>
				<td colspan="3" align="left"><font size="3" ><b> Closing Balance Amount</b></font></td>
				<td colspan="4" align="right">
						<input type="text" name="closingBalance"  id="closingBalance" size="4" readonly="readonly" name="" value="<%=(master.getOpeningBalance()).add(new BigDecimal(grantamount)).add(new BigDecimal(leaseTotal)).add(new BigDecimal(miscTotal)).setScale(2,5).toString()%>">
				</td>
			</tr>
				
			    <tr>
					<td colspan="7" align="center">
						<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="submit" name="Button" value="Record Payment"   class="lft">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    	</div>			
				    </td>
				</tr> 
						
		   </table>       
             </div>  
             <% if(request.getParameter("Button")!=null) {%>
		
				<%=request.getAttribute("message") == null?"":(String)request.getAttribute("message") %> 
			
			<%} %>   
                    
                </div>    
              </div>            
            	<%} %>
                <div class="clrBth"></div>
       
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
