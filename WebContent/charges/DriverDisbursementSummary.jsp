<%@page import="com.lowagie.text.Document"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDisbursment"%>
<%@ page errorPage="ExceptionHandler.jsp" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<%@page import="com.charges.bean.DisbursementMaster"%>

<%@page import="com.charges.bean.DisbursementRecordMaster"%><html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="Ajax/PageNo.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<script src="js/CalendarControl.js" language="javascript"></script>
<script type="text/javascript" src="js/jquery.js"></script>

<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/label.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
		<script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

<title>Insert title here</title>
<%
	ArrayList al_List = (ArrayList)request.getAttribute("al_list");
 ArrayList al_List1 = (ArrayList)request.getAttribute("al_list1");

	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
%>
<script type="text/javascript">
var driverid="";
var drivId="";
function deteDisbursment(key,row)
{
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
	 var url = 'OpenRequestAjax';
	 var parameters='event=reverseDisbursement&disbursementKey='+key;
	 xmlhttp.open("POST",url, false);
	 xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	 xmlhttp.setRequestHeader("Content-length", parameters.length);
	 xmlhttp.setRequestHeader("Connection", "close");
	 xmlhttp.send(parameters);
	 var text = xmlhttp.responseText;
	 if(text=="001"){
		 document.getElementById("reverseTD_"+row).innerHTML="Reversed Transaction";
	 }else{
		 alert("Reversal unsuccessful");
	 }
}
 function page(div_id,button){
	 //alert(div_id);
	 $('#pageNo').fadeOut(1000);
	 $('#pageId').fadeIn(1000);
	// document.getElementById('pageNo').innerHTML = "";
 }
 
 function tagPerson(dname,no){
	   
	  if(document.getElementById("tagname_"+no).checked==true){
			 $("#carddetails1").show();
		  driverid=driverid+dname+";";
		  var driver=driverid.split(";");
		  if(driver[0]==dname){
			  drivId=drivId+dname+";";
		  document.getElementById('driverId').value = drivId;
		  }else{
			  alert("DriverId is Mismatch");
			  document.getElementById("tagname_"+no).checked=false;
		  }
	}
	     }
 function tagname(){
	 var name=document.getElementById("namein").value;
	 var drivId=document.getElementById("driverId").value;
	 var xmlhttp = null;
	 if (window.XMLHttpRequest)
	 {
		 xmlhttp = new XMLHttpRequest();
	 } else {
		 xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	 }
      if(name!=null && name!=""){	 
		var url='ChargesAction?event=tagdisbursment&tName='+name+"&driverId="+drivId;
		//alert(url);
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text=xmlhttp.responseText;
		if(text=="success"){
			driverid=0;
			 $('#carddetails1').hide();
	   }else{			 
		   $('#carddetails1').hide();
       }	}
      else{
    	  alert("Enter tag name");
      }
    
 }
 function closew(){
	   $('#carddetails1').hide();
		driverid=0;
	 }
</script>
 <style type="text/css">
</style>
</head>

<body>
<% DisbursementMaster master = request.getAttribute("disbursementMaster")==null?new DisbursementMaster():(DisbursementMaster)request.getAttribute("disbursementMaster");
	
%>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
		
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>
	
<form name="masterForm" action="control" method="post">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	<input type="hidden" name="action" value="ChargesAction">
	<input type="hidden" name="event" value="driverDisbursementSummary">
	<input type="hidden" name="size" id="size" value="<%=al_List!=null?al_List.size():"0" %>">
	<input type="hidden" name="financial" value="3">

            	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
		<div class="rightColIn">
                	<h2><center>Driver Disbursement Summary</center></h2>
                	
                	
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
                    <tr>
                    <td>
                    <table id="bodypage" width="540" >
                    <td class="firstCol">From&nbsp;Date</td>
					<td>
					<input type="text" name="from_date" id="from_date" onfocus="showCalendarControl(this);"  value="<%=request.getParameter("from_date")==null?"":request.getParameter("from_date") %>" readonly="readonly">
					</td></tr>
					<tr><td class="firstCol">To&nbsp;Date</td>
					<td>
					<input type="text" name="to_date" id="to_date" onfocus="showCalendarControl(this);" value="<%=request.getParameter("to_date")==null?"":request.getParameter("to_date") %>" readonly="readonly">
					</td>
					</tr>
		             <tr><td class="firstCol">TagName</td>
					<td><input type="text" name="tagName" id="tagName"></input></td></tr>
					
					<%if(!((AdminRegistrationBO)session.getAttribute("user")).getUsertypeDesc().equalsIgnoreCase("Driver")) {%>
					<tr>
					<td class="firstCol">Driver&nbsp;Id</td>
					<td colspan="7" align="left">
					<input type="text" onfocus="appendAssocode('<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>',id)"  onblur="caldid(id,'drivername','dName');enableButton();disableDIV();" id="dd_driver_id" name="dd_driver_id" size="15"   autocomplete="off" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>" class="form-autocomplete">
								 <input type="hidden" name="dd_drv_id" id="dd_drv_id" value="<%=request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id") %>">
						  		 <ajax:autocomplete
				  					fieldId="dd_driver_id"
				  					popupId="model-popup1"
				  					targetId="dd_driver_id"
				  					baseUrl="autocomplete.view"
				  					paramName="DRIVERID"
				  					className="autocomplete"
				  					progressStyle="throbbing" /> 
				  					<input type="hidden" name="dName" id='dName' value="<%=request.getParameter("dName")==null?"":request.getParameter("dName") %>">
							  		<div id="drivername"><%=request.getParameter("dName")==null?"":request.getParameter("dName") %></div>	 
					</td>
					</tr>
					<%} %>
				
				<tr>
					<td colspan="7" align="center">
						<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	<input type="submit" name="Button"  value="Get" class="lft" >
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    	</div>			
				    </td>
				</tr> 
				</table>
                    </td></tr>
                    </table>             
                </div>    
              </div>
             
              <div class="rightCol padT10">
			<div class="rightColIn padT10">
			 <div class="pageNo" id="pageNo">
			 <%boolean unreversed=true; %>
              <% if(al_List !=null && !al_List.isEmpty()) { %>
                   <table width="100%" border="1"  style="background-color:white;overflow:scroll;height:70%;" cellspacing="0" cellpadding="0" class="driverChargTab">  
                  <tr style="background-color:cadetblue;color: white;">
                        <th width="15%"  style="text-align:center;">Driver Name</th>
                        <th width="15%" style="text-align:center;">OperatorId</th>
                        <th width="15%" style="text-align:center;">Payment Date</th>
                        <th width="15%" style="text-align:center;">Open Amount</th>
                        <th width="15%" style="text-align:center;">Amount</th>
                        <th width="15%" style="text-align:center;">Tagname</th>
                        <th width="20%" style="text-align:center;">Delete</th>
                         <th width="20%" style="text-align:center;">*</th>
                         <th width="25%" style="text-align:center;">Fleet</th>
                      </tr>
                      <%String pre_d="",curr_d=""; %>
							<%double amount=0;
								boolean colorLightGreen = true;
								String colorPattern;%>
							<%for(int i=0;i<al_List.size();i++) {
								colorLightGreen = !colorLightGreen;
								if(colorLightGreen){
									colorPattern="style=\"background-color:rgb(236, 236, 229);text-align:center\""; 
									}
								else{
									colorPattern="style=\"text-align:center\"";
								}
								
								DisbursementRecordMaster disbursment = (DisbursementRecordMaster)al_List.get(i); %>
								
								<%curr_d = disbursment.getDriverid(); %>
								<%if(i==0 || curr_d.equals(pre_d)){ %>
									<%amount = amount + (disbursment.getOpeningBalance()); %>
								<%} %>
							 	<%if((!curr_d.equals(pre_d) && i!=0)) { %> 
							 	
							 	<tr>	<td colspan="4" align="center"  style="background-color:#D3F0E9;" >Total</td>
										<td align="center" style="background-color:#D3F0E9;"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
										<td style="background-color:#D3F0E9;"></td>
										<%amount = Double.parseDouble(""+disbursment.getOpeningBalance()); %>
										<%unreversed=true;%>
					 				</tr> 
							 	<%} %>
<%-- 							 	<%if(i%2==0){ %>
 --%>							 	<tr>
									<td <%=colorPattern %> ><%=disbursment.getDrivername()+"("+disbursment.getDriverid()+")"%></td>
									<td <%=colorPattern %> > <%=disbursment.getOperatorid()%></td>
									<td  <%=colorPattern %> ><%=disbursment.getProcessDate()%></td>
							       	<%if(disbursment.getDriverid().equals(master.getDriverid())){ %>
									<td <%=colorPattern %> ><%="$ "+master.getClosingBalance() %></td>
  									<%}else { %>
  									
  									<td <%=colorPattern %>>$0.00</td>
  									
  									<%} %>
									
  								    <td <%=colorPattern %>>
										<a href="<%=request.getContextPath()%>/control?action=ChargesAction&event=driverDisbursementSummary&Button=showDisDetailSummary&module=financeView&pss_key=<%=disbursment.getuID() %>" target="_blank"><%="$ "+new BigDecimal(disbursment.getOpeningBalance()).setScale(2,5) %></a><br>
									</td>
									<td  <%=colorPattern %> ><%=disbursment.getTagname()%></td>
		
									<td <%=colorPattern %> id="reverseTD_<%=i%>" >
									<%if(disbursment.getDisbursmentStatus()!=2 && unreversed){ %>
										<a href="#" onclick="deteDisbursment(<%=disbursment.getuID() %>,<%=i%>)"> Reverse </a><br>
										<%unreversed=false; %>
									<%}else if(!unreversed && disbursment.getDisbursmentStatus()!=2){ %>
									Reverse the previous Transaction
									<%}else{ %>
									Reversed Transaction
									<%} %>
									</td><td <%=colorPattern %> ><input type="checkbox" id="tagname_<%=i %>" value="tagname" onclick="tagPerson(<%=disbursment.getDriverid()%>,<%=i%>);" ></input></td>
                                 <%if(fleetList!=null && fleetList.size()>0){%>
	                             <td <%=colorPattern %> align="center">
		                         <select name="fleetDriver_<%=i %>" id="fleetDriver_<%=i %>"  style="width:140px;" onchange="changeFleetForDriver()" >
		                         <%if(fleetList!=null){
									for(int fleet=0;fleet<fleetList.size();fleet++){ %>
									<% AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");%>
	
										<option value="<%=fleetList.get(fleet).getFleetNumber()%>" <%=(fleetList.get(fleet).getFleetNumber().equals(adminBo.getAssociateCode())?"selected=selected":"")%> ><%=fleetList.get(fleet).getFleetName() %></option>
										<%=fleetList.get(fleet).getFleetNumber()%>
									<% }
								  }%> 
	                        	</select>
	                        </td>
	                     <%} %>
                       	
							</tr>	
<%-- 							<%}else{ %>
							<tr>
									<td style="background-color:rgb(187, 239, 247);text-align:center;"><%=disbursment.getDrivername() %></td>
									<td style="background-color:rgb(187, 239, 247);text-align:center;"><%=disbursment.getOperatorid() %></td>
									<td style="background-color:rgb(187, 239, 247);text-align:center;"><%=disbursment.getProcessDate() %></td>
										<%if(disbursment.getDriverid()==master.getDriverid()){ %>
									
					             <td style="text-align:center;background-color:rgb(187, 239, 247);"><%="$ "+master.getClosingBalance() %></td>
  									<%} %>
  								    <td></td>
  									<td style="background-color:rgb(187, 239, 247);text-align:center;">
										<a href="#" onclick="window.open('<%=request.getContextPath()%>/control?action=ChargesAction&event=driverDisbursementSummary&module=financeView&Button=showDisDetailSummary&pss_key=<%=disbursment.getuID() %>')"><%="$ "+new BigDecimal(disbursment.getOpeningBalance()).setScale(2,5) %></a><br>
									</td>
								  	<td style="background-color:rgb(187, 239, 247);text-align:center;"><%=disbursment.getTagname() %></td>
								
									<td id="reverseTD_<%=i%>" style="background-color:rgb(187, 239, 247);text-align:center;">
									<%if(disbursment.getDisbursmentStatus()!=2 && unreversed){ %>
										<a href="#" onclick="deteDisbursment(<%=disbursment.getuID() %>,<%=i%>)"> Reverse </a><br>
										<%unreversed=false; %>
									<%}else if(!unreversed && disbursment.getDisbursmentStatus()!=2){ %>
									Reverse the previous Transaction
									<%}else{ %>
									Reversed Transaction
									<%} %>
									</td><td style="text-align:center;background-color:rgb(187, 239, 247);"><input type="checkbox" id="tagname" value="tagname" onclick="tagPerson(<%=disbursment.getDriverid()%>);"></input></td>
	                                      <%if(fleetList!=null && fleetList.size()>0){%>
	                        <td align="center">
		                         <select name="fleetDriver_<%=i %>" id="fleetDriver_<%=i %>"  style="width:140px;" onchange="changeFleetForDriver()" >
		                         <%if(fleetList!=null){
									for(int fleet=0;fleet<fleetList.size();fleet++){ %>
									<% AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");%>
	
										<option value="<%=fleetList.get(fleet).getFleetNumber()%>" <%=(fleetList.get(fleet).getFleetNumber().equals(adminBo.getAssociateCode())?"selected=selected":"")%> ><%=fleetList.get(fleet).getFleetName() %></option>
										<%=fleetList.get(fleet).getFleetNumber()%>
									<% }
								  }%> 
	                        	</select>
	                        </td>
	                     <%} %>
                        
									</tr>	
								<%} %> --%>
							<%if(i==al_List.size()-1) { %>
									<tr>
										<td colspan="4" align="center" style="background-color:#D3F0E9">Total</td>
										<td align="center" style="background-color:#D3F0E9"><%="$ "+new BigDecimal(amount).setScale(2,5) %></td>
										<td style="background-color:#D3F0E9"></td>
										<td style="background-color:#D3F0E9"></td>
										<%amount = 0; %>
								  </tr>
								<%} %>
								<%pre_d = curr_d; %>
							<%} %>
						</table></div>
						<%} %>
						<table>
							<tr>
							<td valign="top" align="center" width="100%">
							<div id="pageId" style="display:none;">
							<%if(request.getParameter("pageButton")!=null){%>
								<jsp:include page="/Company/Disbursement.jsp" />
							</div><%} %>
							</td>
		</tr> 
        <tr>
				</tr></table>
				<%--
				<table id="nextPage">
						<tr><td>
						<%if(al_List !=null && !al_List.isEmpty()) { 
					String rowCount=(String)request.getAttribute("count");
					int count = Integer.parseInt(rowCount);
					for(int i=1;i<=count;i++){
					<input type="button"value="<%=i %>"onclick="page('pageNo',this);pageNo(<%=i%>,'<%=request.getParameter("from_date")%>','<%=request.getParameter("to_date")%>','<%=((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode() %>','<%=request.getParameter("dd_driver_id")%>');" />
										<%} }%>
						</td></tr>
				</table> --%>
                  </div>    
              </div>
             
    </div>  
</form>
<div  id="carddetails1" class="jqmWindow" style="display:none;width:420px;" >
		 <table id="carddetails">
  <tr><td>Tag with</td><td><input type="text" name="namein" id="namein" value="" ></td></tr>
  <tr><td><input type="hidden" id="driverId" name="driverId"></input></td></tr>
  </table><div align="center">          
            <input type="button" name="buttonSubmit" class="lft" value="Submit" onclick="tagname();"/>
               <input type="button" name="buttonClose" class="lft" value="Close" onclick="closew();"/>
         
         </div>
  
  </div>
</body>
</html>
   