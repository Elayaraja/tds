
<%@page import="java.util.ArrayList"%>
<%@page import="com.charges.bean.ChargeDetail"%>
<%@page import="java.math.BigDecimal"%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form  name="masterForm" action="control" method="post" onsubmit="return submitThis()">
 <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        
            	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
		<div class="rightColIn">
		<h1>Driver Disbursement Details</h1>
 	<table id="bodypage" >	
		<tr>
		<td>  
			 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">  
<%
 	   ArrayList al_List = (ArrayList)request.getAttribute("al_sublist");
		
		double totalDriverAmount = 0;
 	%>		 	
	<% if(al_List !=null && !al_List.isEmpty()) { %>
		<tr> 
			<td colspan="4" align="center">	
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
				 <tr>
				 	<td>Mode</td>
				 	<td>Trip Id</td>
				 	<td>Approval Code</td>
				 	<td>Total Amount</td>
				 	<td>Driver Amount</td>
				 </tr>	
				 <%for(int i=0;i<al_List.size();i++) {%>
				 <tr>
				 	<% ChargeDetail chargeDetail = (ChargeDetail)al_List.get(i); %>
				 	<td><%=chargeDetail.getMtype() %></td>
				 	<td><%=chargeDetail.getTripid() %></td>
				 	<td><%=chargeDetail.getApprovalcode() %></td>
				 	<td align="right"><%=chargeDetail.getTotal() %></td>
				 	<% totalDriverAmount = totalDriverAmount +chargeDetail.getDriverAmt() ; %>
				 	<td align="right"><%=chargeDetail.getDriverAmt() %></td>
				 </tr> 	
				  <%} %>
				  <tr>
				  	<td colspan="4"> Grant Total
				  	</td>
				  	<td align="right">
				  		<%= new BigDecimal(totalDriverAmount).setScale(2,5) %>
				  	</td>
				  </tr>
				</table>
			</td>
		</tr>		
	<%} %>
</table>
</td>
</tr>
</table>
</div>
</div>
</form>
</body>
</html>	
	

					