<%@page import="com.charges.bean.ChargesBO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Charge Types</title>
<%ArrayList<ChargesBO> charges =(ArrayList<ChargesBO>)request.getAttribute("charges"); %>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
	function delrow() {
		try {
			var table = document.getElementById("chargesTable");
			var rowCount = table.rows.length;
			var size=document.getElementById('fieldSize').value;
			if(Number(size)>0){
				var temp = (Number(size) - 1);
				document.getElementById('fieldSize').value = temp;
				rowCount--;
				table.deleteRow(rowCount);
			}
		} catch (e) {
			alert(e.message);
		}

	}


	function cal() {
		var i = Number(document.getElementById("fieldSize").value) + 1;
		document.getElementById('fieldSize').value = i;
		var table = document.getElementById("chargesTable");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);

		var element = document.createElement("input");
		element.type = "text";
		element.className = "form-autocomplete";
		element.name = "key" + i;
		element.id = "key" + i;
		element.align = 'right';
		element.size = '10';
		cell.appendChild(element);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "description" + i;
		element2.id = "description" + i;
		element2.size = '10';
		cell1.appendChild(element2);

		var element4 = document.createElement("select");
		element4.name = "amountType" + i;
		element4.id = "amountType" + i;
		element4.id = "amountType" + i;

		var theOption = document.createElement("option");
		theOption.text = "By Charges";
		theOption.value = "1";
		element4.options.add(theOption);
		theOption = document.createElement("option");

		theOption.text = "By CC/Voucher";
		theOption.value = "2";
		element4.options.add(theOption);
		theOption = document.createElement("option");
		cell2.appendChild(element4);
	}
</script>
</head>
<body>
<form name="chargeType" action="control" method="post">
<input type="hidden" name="action" value="chargesAction"/>
<input type="hidden" name="event" value="chargeTemplateMaster"/>
<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
<input type="hidden" name="fieldSize" id="fieldSize" value="<%=charges==null?"":charges.size()%>"/> 
	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div class="rightCol">
<div class="rightColIn">	
<h1 style="width: 100%;color:white;height:40%;background-color:rgb(41, 53, 65);font-size:150%;text-align:center;">Insert Payment Template</h1>
<table  id="chargesTable" align="center"  style="width:60%;background-color:rgb(209, 214, 214)" id="bodypage" >
<tr>
 <%
	String error = "";
	if (request.getAttribute("error") != null) {
		error = (String) request.getAttribute("error");
%>
<td colspan="7">
	<div id="errorpage" style="font-size: x-large;">
		<%=error.length() > 0 ? "" + error : ""%>
	</div>
<%
	}
%></td>
</tr>
<tr style="background-color:rgb(186, 228, 226);" >          
	<th style="width: 15%;">Key</th>
	<th style="width: 15%;">Description</th>
	<th style="width: 15%;">Amount Type</th>
</tr>
<% if(charges != null && charges.size() >0  ) {  
	for(int count = 0; count < charges.size(); count++) { 
%>
<tr>
	<td><input type="text" name="key<%=count+1%>"  value="<%= charges.get(count).getPayTemplateKey()%>"/></td>
	<td><input type="text" name="description<%=count+1%>"  value="<%= charges.get(count).getPayTemplateDesc()%>"/></td>
	<td><select name="amountType<%=count+1%>">                                     
		<option value="1" <%=charges.get(count).getPayTemplateAmountType()==1?"selected":"" %>>By Charges</option>
        <option value="2" <%=charges.get(count).getPayTemplateAmountType()==2?"selected":"" %>>By CC/Voucher</option>
	</select> </td>
</tr>
<%} }%>
</table>
			  
              <div align="center" >
            <input type="button" name="add" value="ADD" onclick="cal()" class="lft">
            <input type="button" name="remove" value="Remove"  onclick="delrow()" class="lft" >
            <input type="submit" name="buttonSubmit" class="lft" value="Submit"/>
                                    
        </div>
             
            <!--  <div class="btnBlue padT10 padR5 fltLft ">
                  <div class="rht">
                  <input type="button" name="remove" value="Remove"  onclick="delrow()" class="lft" >
                               </div>
                          </div>
                           <div class="btnBlue padT10 padR5 fltLft ">
                               <div class="rht">
                                 <input type="submit" name="buttonSubmit" class="lft" value="Submit"/>
                               </div>
                          </div>

    <div class="clrBth"></div>
</div>	-->		
</div>
</form>
</body>
</html>