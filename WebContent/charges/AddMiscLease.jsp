<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.ArrayList"%><html>
<head>
<script type="text/javascript">

function ajaxCall(url, callback) {
	//alert("Inside ajaxcall");
    var req = init();
    req.onreadystatechange = processRequest;
    function init() {
      if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
      } else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
      }
    } 
    
    function processRequest () {
      // readyState of 4 signifies request is complete
      if (req.readyState == 4) {
        // status of 200 signifies sucessful HTTP call
        if (req.status == 200) {
          if (callback) callback(req.responseText);
        }
      }
    }

    this.doGet = function() {
      req.open("GET", url, true);
      req.send(null);
    }
}



//------ Ajax InterAction  Over    -------------------------------

function loadLeaseAmount(){
	var url = '/TDS/AjaxClass?event=getLeaseAmount&assoccode='+document.getElementById('assoccode').value+'&vechType='+document.getElementById('vechType').value;
	//alert(url);
	var ajax = new ajaxCall(url,fillLeaseAmount);
	ajax.doGet();

}

function fillLeaseAmount(responseText)
{
	var leaseAmount = parseFloat(responseText);
	document.getElementById('leaseAmount').value=leaseAmount;
}

function init()
{
	document.getElementById('ddid').value = dialogArguments[0];
	document.getElementById('assoccode').value = dialogArguments[1];
	document.getElementById('ddname').value = dialogArguments[2];
	document.getElementById('process_Date').value = dialogArguments[3];
	
	//document.getElementById('drName').innerHTML =dialogArguments[2]; 
	document.getElementById('drName').innerHTML =document.getElementById('ddname').value;
	
}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<form action="control" method="post" >
<input type="hidden" name="action" value="ChargesAction"/>
<%if(request.getAttribute("type").toString().equals("1")){ %>
<input type="hidden" name="event" value="storeMisc"/>
<%} else { %>
<input type="hidden" name="event" value="storeLease"/>
<%} %>

<table>
		<tr>	
			<td>
				ADD Misc. Charges to Driver :
				<input type="hidden" name="ddid" id='ddid' value="<%=request.getParameter("ddid") %>">
				<input type="hidden" name="ddname" id='ddname' value="<%=request.getParameter("ddname") %>">
				<input type="hidden" name="assoccode" id='assoccode' value="<%=request.getParameter("assoccode") %>">
				<input type="hidden" name="process_Date" id='process_Date' value="<%=request.getParameter("process_Date") %>">
				<input type="hidden" name="type" id='type' value="<%=request.getAttribute("type").toString() %>">
				
			</td>
			<td>
				<div id='drName'></div>
			</td>
		</tr>
		 
<%if(request.getAttribute("type").toString().equals("1")){ %>
	
		<tr>
			<td colspan="2">
				<table>
					<tr>
						<th> Misc Description</th>
						<th> Amount</th>
					</tr>
					<tr>
						<td> <input type="text" name="miscName" id="miscName" value=""></td>
						<td> <input type="text" name="miscAmount" id="miscAmount" value=""></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" name="button" value="Submit">
						</td>
					</tr>
				</table>
			</td>
			</tr>
			
<%} else { %>

	
		<tr>
			<td colspan="2">
			<table>
				<tr>
					<td align="center"> <font color="blue">Lease Deatils</font>
					</td>
				</tr>	
				<tr>
					<td>
						<table>
							<tr>
								<th>Vechical Type</th>
								<th>Cab No</th>
								<th>Lease Amount</th>
							</tr>
							<tr>
								<td>
									<Select name="vechType" id='vechType' onchange="loadLeaseAmount()">
										<option value=""></option>
										<%
										ArrayList<String> al_vecType = (ArrayList<String>)request.getAttribute("al_Vech");
										for(int j=0;j<al_vecType.size();j++) 
										{
											
										%>
										<option value="<%=al_vecType.get(j) %>"><%=al_vecType.get(j) %></option>
										<%} %>	
									</Select>
								</td>
								<td>
									<input type="text" name="cabNo" id="cabNo" value="" size="6">
								</td>
								<td>
									<input type="text" name="leaseAmount" id="leaseAmount" value="0.00" size="10">
								</td>
							</tr>
							<tr>
						<td colspan="3" align="center">
							<input type="submit" name="button" value="Submit">
						</td>
					</tr>
						</table>
					</td>
				</tr>	
				</table>
			</td>
			</tr>
			
<%} %>
</td>
</tr>
<tr>
	<td>
		<%=request.getAttribute("msg")==null?"":request.getAttribute("msg").toString() %>
	</td>
</tr>
	</table>
</form>
<script type="text/javascript">init()</script>
</body>
</html>
