<%@page import="com.charges.bean.ChargesBO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Charge Types</title>
<%
	ArrayList<ChargesBO> charges =(ArrayList<ChargesBO>)request.getAttribute("charges");
%>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>

<script type="text/javascript">
$(document).ready ( function(){
//	document.getElementById("formExample").innerHTML=document.getElementById("exampleFormula").value;
	$(".venki").attr("placeholder", "a%<keyvalue>+b%<keyvalue>");
});
function delrow() {
		try {
			var table = document.getElementById("chargesTable");
			var rowCount = table.rows.length;
			var size = document.getElementById('fieldSize').value;
			if (Number(size) > 0) {
				var temp = (Number(size) - 1);
				document.getElementById('fieldSize').value = temp;
				rowCount--;
				table.deleteRow(rowCount);
			}
		} catch (e) {
			alert(e.message);
		}

	}
	function cal() {
		var i = Number(document.getElementById("fieldSize").value) + 1;
		document.getElementById('fieldSize').value = i;
		var table = document.getElementById("chargesTable");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);
		var cell3 = row.insertCell(3);

		var element = document.createElement("input");
		element.type = "text";
		element.className = "form-autocomplete";
		element.name = "key" + i;
		element.id = "key" + i;
		element.align = 'right';
		element.size = '10';
		cell.appendChild(element);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "description" + i;
		element2.id = "description" + i;
		element2.size = '10';
		cell1.appendChild(element2);

		var element4 = document.createElement("input");
		element4.type = "text";
		element4.name = "staticAmount" + i;
		element4.id = "staticAmount" + i;
		element4.id = "staticAmount" + i;
		element4.size = '10';
		cell2.appendChild(element4);

		var element5 = document.createElement("input");
		element5.type = "text";
		element5.name = "formula" + i;
		element5.id = "formula" + i;
		element5.className = "venki";
		element5.size = '10';
		element5.onblur=function(){checkFormula(i,0);};
		cell3.appendChild(element5);
	
	}
	
	function checkFormula(rowValue,submitCheck){
		var formula = document.getElementById("formula"+rowValue).value;
		var key = document.getElementById("key" + rowValue).value;
		var desc = document.getElementById("description" + rowValue).value;
		var staticAmt = document.getElementById("staticAmount" + rowValue).value;
		if(formula.indexOf("%"+key)>=0){
			document.getElementById("formula"+rowValue).value="";
			alert("Formula contains same key value");
		} else {
			formula=formula.replace("%","pct").replace("+","plus").replace("%","pct").replace("+","plus").replace("%","pct").replace("+","plus").replace("%","pct").replace("+","plus");
			if(submitCheck=="1"){
				var xmlhttp = null;
				if (window.XMLHttpRequest) {
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				var url = 'RegistrationAjax?event=updateFormula&key='+ key+'&formula='+formula+'&desc='+desc+'&amt='+staticAmt;
				xmlhttp.open("GET", url, false);
				xmlhttp.send(null);
			}
		}
	}

	function deleteDocType(serialNumber, x) {
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		url = 'CustomerServiceAjax?event=deleteChargeType&module=financeView&serialNumber='
				+ serialNumber;
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		if (text == "001") {
			var table = document.getElementById("chargesTable");
			table.deleteRow(x.parentNode.parentNode.rowIndex);
			document.getElementById("fieldSize").value = "0";
		} else {
			alert("Sorry couldnt delete the entry please try again");
		}
	}
	function submitCharges() {
		var size = document.getElementById("fieldSize").value;
		var fareKey = "";
		var extraKey = "";
		var tipKey="";
		var ccKey="";
		if (size == 0) {
			alert("Add any new charges and then submit");
			return false;
		}
		for (var i = 1; i <= size; i++) {
			if (document.getElementById("key" + i).value == 1) {
				fareKey = "1";
			}
			if (document.getElementById("key" + i).value == 2) {
				extraKey = "1";
			}
			if (document.getElementById("key" + i).value == 3) {
				ccKey = "1";
			}
			if (document.getElementById("key" + i).value == 0) {
				tipKey = "1";
			}
		}
		if ((fareKey == "" || extraKey == "" || tipKey=="" || ccKey=="") && (<%=charges ==null ||  charges.size()== 0%> )) {
			alert("Key Values '0','1','2' and '3' Are Mandatory");
			return false;
		} else {
			document.getElementById("buttonSubmit").value = "submit";
			document.chargeType.submit();
		}
	}
	
</script>
</head>
<body>
	<form name="chargeType" action="control" method="post">
		<input type="hidden" name="action" value="chargesAction" /> 
		<input type="hidden" name="event" value="chargeType" /> 
		<input type="hidden" name="module" id="module" value="<%=request.getParameter("module") == null ? "" : request.getParameter("module")%>" />
		<input type="hidden" name="fieldSize" id="fieldSize" value="0" /> 
		<input type="hidden" name="buttonSubmit" id="buttonSubmit" value="" />
		<input type="hidden" name="exampleFormula" id="exampleFormula" value="10%<KeyValue>+5%<KeyVlue>" />

		<div>
			<h1
				style="width: 100%; color: white; height: 40%; background-color: rgb(15, 51, 75); font-size: 150%; text-align: center;">Insert
				Charge Types</h1>
 				<div id="formExample" style="width: 50%;color: red;font-weight: bold;margin-left: 10%;">
				</div>
			<table id="chargesTable" align="center"
				style="width: 60%; background-color:rgb(209, 214, 214);" id="bodypage"
				class="driverChargTab">

				<tr>
					<%
						String error = "";
						if (request.getAttribute("error") != null) {
							error = (String) request.getAttribute("error");
					%>
					<td colspan="7">
						<div id="errorpage" style="font-size: x-large;">
							<%=error.length() > 0 ? "" + error : ""%>
						</div>
					</td>
					<%
						}
					%>
				</tr>
				<tr style="background-color:rgb(186, 228, 226);">
					<th style="width: 5%;">Key</th>
					<th style="width: 5%;">Description</th>
					<th style="width: 5%;">Static Amount</th>
					<th style="width: 5%;">Dynamic Charges</th>
					<th style="width: 5%;">Delete</th>
				</tr>
				<%
					if (charges != null && charges.size() > 0) {
						boolean colorlightGreen = true;
						String color;

						for (int count = 0; count < charges.size(); count++) {
							colorlightGreen = !colorlightGreen;
							if (colorlightGreen) {
								color = "style=\"background-color:rgb(242, 250, 250);text-align:center;\"";
							} else
								color = "style=\"background-color:white;text-align:center;\"";
				%>
				<tr>
					<td><input type="text" id="key<%=count %>" value="<%=charges.get(count).getPayTypeKey()%>" /></td>
					<td><input type="text" id="description<%=count %>" value="<%=charges.get(count).getPayTypeDesc()%>" /></td>
					<td><input type="text" id="staticAmount<%=count %>" value="<%=charges.get(count).getPayTypeAmount()%>" /></td>
					<td><input type="text" id="formula<%=count %>" class="venki"
						value="<%=charges.get(count).getDynamicFormula()==null?"":charges.get(count).getDynamicFormula().replaceAll("plus", "+").replaceAll("pct", "%")%>" /></td>
					<td><input type="button" value="Delete"
						<%=charges.get(count).getPayTypeKey() <= 3 ? "disabled"
							: ""%>
						onclick="deleteDocType('<%=charges.get(count).getPayTypeKey()%>',this)"></input>
					</td>
					<td><input type="button" value="Update"  onclick="checkFormula('<%=count%>',1)"></input>
					</td>
				</tr>
				<%
					}
					}
				%>
				<tr>
					<td></td>
				</tr>
			</table>
			<div align="center">
				<input type="button" name="add" value="ADD" onclick="cal()"
					class="lft"> <input type="button" name="remove"
					value="Remove" onclick="delrow()" class="lft"> <input
					type="button" name="buttonSubmit" class="lft" value="Submit"
					onclick="return submitCharges()" />
			</div>




		</div>
	</form>
</body>
</html>