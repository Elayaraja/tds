<%@page import="com.charges.bean.ChargesBO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Charge Types</title>
<%ArrayList<ChargesBO> charges =(ArrayList<ChargesBO>)request.getAttribute("charges"); %>
<%ArrayList<ChargesBO> chargeType =(ArrayList<ChargesBO>)request.getAttribute("chargeTypes"); %>
<%ArrayList<ChargesBO> chargeDetail =(ArrayList<ChargesBO>)request.getAttribute("chargeDetail"); %>
<script type="text/javascript" src="js/ajaxcore.js"></script>
<script type="text/javascript" src="Ajax/driveriddrivername.js"></script>
<script type="text/javascript">
	function delrow() {
		try {
			var table = document.getElementById("chargesTable");
			var rowCount = table.rows.length;
			var size=document.getElementById('fieldSize').value;
			if(Number(size)>0){
				var temp = (Number(size) - 1);
				document.getElementById('fieldSize').value = temp;
				rowCount--;
				table.deleteRow(rowCount);
			}
		} catch (e) {
			alert(e.message);
		}

	}

	function newfunc(){
		alert("onchange");
	}
	
	function new2func(count){
		//alert("count:"+count);
		var tax1 = document.getElementById("tax1temp"+count).value;
		var tax2 = document.getElementById("tax2temp"+count).value;
		var tax3 = document.getElementById("tax3temp"+count).value;
		//alert("tax1:"+tax1+" tax2:"+tax2+" tax3:"+tax3);
		var total = Number(tax1) + Number(tax2) + Number(tax3);
		//alert("total"+total);
		document.getElementById("amountDetail"+count).value=total;
	}
	
	
	function cal() {
		var i = Number(document.getElementById("fieldSize").value) + 1;
		document.getElementById('fieldSize').value = i;
		var table = document.getElementById("chargesTable");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell = row.insertCell(0);
		var cell1 = row.insertCell(1);
		var cell2 = row.insertCell(2);
		var cell3 = row.insertCell(3);
		var cell4 = row.insertCell(4);
		var cell5 = row.insertCell(5);
		var cell6 = row.insertCell(6);
		var cell7 = row.insertCell(7);

		var element = document.createElement("select");
		element.name = "templateType" + i;
		element.id = "templateType" + i;
		element.id = "templateType" + i;
		<%if(charges!=null && charges.size()>0){
			for(int i=0;i<charges.size();i++){%>
		var theOption = document.createElement("option");
		theOption.text = "<%=charges.get(i).getPayTemplateDesc()%>";
		theOption.value = "<%=charges.get(i).getPayTemplateKey()%>";
		element.options.add(theOption);
		<%}}%>
		cell.appendChild(element);

		var element1 = document.createElement("select");
		element1.name = "payType" + i;
		element1.id = "payType" + i;
		element1.id = "payType" + i;
		<%if(chargeType!=null && chargeType.size()>0){
			for(int i=0;i<chargeType.size();i++){%>
		var theOption = document.createElement("option");
		theOption.text = "<%=chargeType.get(i).getPayTypeDesc()%>";
		theOption.value = "<%=chargeType.get(i).getPayTypeKey()%>";
		element1.options.add(theOption);
		<%}}%>
		cell1.appendChild(element1);

		var element2 = document.createElement("input");
		element2.type = "text";
		element2.name = "amountDetail" + i;
		element2.id = "amountDetail" + i;
		element2.size = '5';
		element2.value="0";
		cell5.appendChild(element2);

		var element3 = document.createElement("select");
		element3.name = "amountType" + i;
		element3.id = "amountType" + i;
		element3.id = "amountType" + i;
		var theOption = document.createElement("option");
		theOption.text = "%";
		theOption.value = "1";
		element3.options.add(theOption);
		theOption = document.createElement("option");
		theOption.text = "$";
		theOption.value = "2";
		element3.options.add(theOption);
		theOption = document.createElement("option");
		cell6.appendChild(element3);

		var element4 = document.createElement("input");
		element4.type = "text";
		element4.name = "key" + i;
		element4.id = "key" + i;
		element4.size = '5';
		cell7.appendChild(element4);
		
		
		var element5 = document.createElement("input");
		element5.type = "text";
		element5.name = "tax1temp" + i;
		element5.id = "tax1temp" + i;
		element5.value="0";
		element5.onkeyup="new2func('"+i+"')";
		cell2.appendChild(element5);
		
		var element6 = document.createElement("input");
		element6.type = "text";
		element6.name = "tax2temp" + i;
		element6.id = "tax2temp" + i;
		element6.value="0";
		element6.onkeyup="new2func('"+i+"')";
		cell3.appendChild(element6);
		
		var element7 = document.createElement("input");
		element7.type = "text";
		element7.name = "tax3temp" + i;
		element7.id = "tax3temp" + i;
		element7.value="0";
		element7.onkeyup="new2func('"+i+"')";
		cell4.appendChild(element7);
		
		/* cell2.onkeyup=new2func(i);
		cell3.onkeyup=new2func(i);
		cell4.onkeyup=new2func(i); */
}
</script>
</head>
<body>
<form name="chargeType" action="control" method="post">
<input type="hidden" name="action" value="chargesAction"/>
<input type="hidden" name="event" value="chargeTemplateDetail"/>
<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
<input type="hidden" name="fieldSize" id="fieldSize" value="<%=chargeDetail==null?"":chargeDetail.size()%>"/> 
	<div class="leftCol"> 
                    <div class="clrBth"></div>
                </div> 
                <div>
<div>	
<h2><center>Edit Payment Template</center></h2>
<table width="100%" id="chargesTable"  class="driverChargTab">

<%
	String error = "";
	if (request.getAttribute("error") != null) {
		error = (String) request.getAttribute("error");
%>
<tr style="width: 60%">
<td colspan="7">
	<div id="errorpage" style="font-size: x-large;">
		<%=error.length() > 0 ? "" + error : ""%>
	</div></td>
	</tr>
<%}%>

<tr style="width: 60%">          
	<th style="width: 10%;">Template</th>
	<th style="width: 10%;">Charge Type</th>
	
	<th style="width: 5%;">Tax1</th>
	<th style="width: 5%;">Tax2</th>
	<th style="width: 5%;">Tax3</th>
	
	<th style="width: 10%;">Amount</th>
	<th style="width: 10%;">Amount Type</th>
	<th style="width: 5%;">Key</th>
</tr>
<% if(chargeDetail != null && chargeDetail.size() >0  ) {  
	for(int count = 0; count < chargeDetail.size(); count++) { 
%>
<tr style="width: 60%">
	<td><Select name="templateType<%=count+1 %>">
	<%if(charges != null && charges.size()>0) {
		for(int i=0;i<charges.size();i++){%>
		<option value="<%=charges.get(i).getPayTemplateKey()%>" <%=chargeDetail.get(count).getPayTemplateKey()==charges.get(i).getPayTemplateKey()?"selected":"" %>><%=charges.get(i).getPayTemplateDesc() %></option>	
	<%} }%></Select></td>
	<td><Select name="payType<%=count+1 %>">
	<%if(chargeType != null && chargeType.size()>0) {
		for(int j=0;j<chargeType.size();j++){%>
		<option value="<%=chargeType.get(j).getPayTypeKey()%>" <%=chargeDetail.get(count).getPayTypeKey()==chargeType.get(j).getPayTypeKey()?"selected":"" %>><%=chargeType.get(j).getPayTypeDesc() %></option>	
	<%} }%></Select></td>
	
	<td><input type="text" id="tax1temp<%=count+1%>" name="tax1temp<%=count+1%>"  value="<%= chargeDetail.get(count).getTax1()%>" onkeyup="new2func('<%=count+1%>')"/></td>
	<td><input type="text" id="tax2temp<%=count+1%>" name="tax2temp<%=count+1%>"  value="<%= chargeDetail.get(count).getTax2()%>" onkeyup="new2func('<%=count+1%>')"/></td>
	<td><input type="text" id="tax3temp<%=count+1%>" name="tax3temp<%=count+1%>"  value="<%= chargeDetail.get(count).getTax3()%>" onkeyup="new2func('<%=count+1%>')"/></td>

	<td><input type="text" id="amountDetail<%=count+1%>" name="amountDetail<%=count+1%>"  value="<%= chargeDetail.get(count).getAmount()%>"/></td>
	<td><select name="amountType<%=count+1%>">                                     
		<option value="1" <%=chargeDetail.get(count).getPayTemplateAmountType()==1?"selected":"" %>>$</option>
        <option value="2" <%=chargeDetail.get(count).getPayTemplateAmountType()==2?"selected":"" %>>%</option>
	</select> 
	</td>
	<td><input type="text" name="key<%=count+1%>"  value="<%= chargeDetail.get(count).getKey()%>"/></td>
</tr>
<%} }%>
</table>
            <div align="center">
            <input type="button" name="add" value="ADD" onclick="cal()" class="lft">
            
             
                  <input type="button" name="remove" value="Remove"  onclick="delrow()" class="lft" >
                               
                           
                                 <input type="submit" name="buttonSubmit" class="lft" value="Submit"/>
                               </div>
                          

    

</form>
</body>
</html>