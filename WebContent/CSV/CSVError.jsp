<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.PenalityBean"%>
<html>
<head>
<style type="text/css">

#ele_li li
{
background-image:url(images/li_img.png);
list-style-type:none;
padding-left:18px;
background-repeat:no-repeat;
margin-top:3px;
}
</style>
<link href="css/dashboard.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function setOpacity( value ) {
	document.getElementById("styled_popup").style.opacity = value / 10;
	document.getElementById("styled_popup").style.filter = 'alpha(opacity=' + value * 10 + ')';
}

function fadeInMyPopup() {
	for( var i = 0 ; i <= 100 ; i++ )
		setTimeout( 'setOpacity(' + (i / 10) + ')' , 8 * i );
}

function fadeOutMyPopup() {
	for( var i = 0 ; i <= 100 ; i++ ) {
		setTimeout( 'setOpacity(' + (10 - i / 10) + ')' , 8 * i );
	}

	setTimeout('closeMyPopup()', 800 );
}

function closeMyPopup() {
	document.getElementById("styled_popup").style.display = "none"
}

function fireMyPopup() {
	setOpacity( 0 );
	document.getElementById("styled_popup").style.display = "block";
	fadeInMyPopup();
}

</script>
<script>
	window.history.foward(1);
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<%
	ArrayList al_error = (ArrayList)request.getAttribute("al_error");
	ArrayList al_driver = (ArrayList)request.getAttribute("al_driver");
	String total = (String)request.getAttribute("total");
	String err = (String)request.getAttribute("err");
	String charge = (String)request.getAttribute("charge");
	String credit = (String)request.getAttribute("credit");
	String error = (String)request.getAttribute("error")==null?"":(String)request.getAttribute("error");
%>
<body>
<table id="pagebox">	
		<tr>
		<td>  
			<table id="bodypage">	
				<tr>
					<td colspan="7" align="center">		
						<div id="title">
							<h2> Credit/Charge Error</h2>
						</div>
					</td>		
				</tr>
				 
				<tr>
					<td>
						Total No of Records:<Font color="blue"> <%=total %></Font> 
					</td>
				</tr>
				<tr>	
					<td>
						Total No of Error Records:<Font color="red"><%=err %> </Font>
					</td>
				</tr>
				<tr>	
					<td>
						Total Credit Amount:<Font color="blue"><%="$"+credit %> </Font>
					</td>
				</tr>
				<tr>	
					<td>
						Total Charge Amount:<Font color="blue"><%="$"+charge %> </Font>
					</td>
				</tr>
				<tr>
					<td><input type="button" value="Show Errors" onClick='fireMyPopup()' class="button"></td>
				</tr>
				<tr>
		<td>
		<div id='styled_popup' name='styled_popup' style='width: 380px; height: 300px; display:none; position: absolute; top: 150px; left: 50px; zoom: 1'>
			<table width='380' cellpadding='0' cellspacing='0' border='0'>
				<tr>
					<td><img height='23' width='356' src='images/x11_title.gif'></td>
					<td>
						<div style="margin-left:-25px;"><a href='javascript:fadeOutMyPopup();'>
                		<img height='23' width='24' src='images/x11_close.gif' border='0'></a></div>
                	</td>
				</tr>
				<tr>
					<td>
						<div >
						<table bgcolor="white">
						<tr>
							<td colspan='2'> 
						 		<ul compact="compact" type="disc">
						 		<div id="ele_li">
						 			<%=error %>
							 	</div>	
								</ul>
							</td>
						</tr>
						<tr>
						<td colspan="2" align="center" ><input type="button" value="Close" class="button" onclick="fadeOutMyPopup();" ></td>
						</tr>
						</table>
						</div>
					</td>
				</tr>
		  </table>
		</div>
		</td>
	</tr>
				
			  	<tr>
					<td align="center">
			 		 	<table border=1 id="bodypage">	
			 		 	
			 		 	
					 		<%if(al_error!=null){ %>
							<tr><td>Driver Name</td><td>Description</td><td>Amount</td><td>Effective Date</td><td>Type</td></tr>
							
								<% for(int i=0;i<al_error.size();i++) { 
									PenalityBean penalityBean = (PenalityBean)al_error.get(i);								
								%>
									<tr>
										<td style="text-align: center">
											<% for(int j=0 ;j<al_driver.size();j=j+2){ %>
												<%=(al_driver.get(j).toString().equalsIgnoreCase(penalityBean.getB_driver_id()))?al_driver.get(j+1):""%>
											<%} %>
										</td>
										<td style="text-align: center"><%=penalityBean.getB_desc()%></td>
										<td style="text-align: right">$<%=penalityBean.getB_amount() %></td>
										<td style="text-align: center"><%=penalityBean.getB_p_date() %></td>
										<td><%=penalityBean.getBtype().equalsIgnoreCase("1")?"Credit":penalityBean.getBtype().equalsIgnoreCase("2")?"Charge":"Miscellaneous"%></td>
									 </tr>
								<%} %>
								 
							<%}else {%>
								<tr><td colspan="7">No Error Record Found<td></tr>
								
							<%} %>
							<tr><td colspan="7"><a href="/TDS/control?action=registration&event=driverPenalityMaster" >Click Here to View Details</a></td></tr>
							
						</table>
					</td>
				</tr>	
				
			</table>
		</td>
		</tr>
	</table>
</body>
</html>