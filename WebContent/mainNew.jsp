<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 --%>
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <!DOCTYPE html >

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.tds.tdsBO.FleetBO"%>
<%@page import="java.util.ArrayList"%>
 <html lang="en">
<head>
<%ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>(); %>
	 
	<%if(session.getAttribute("fleetList")!=null) {
	fleetList=(ArrayList)session.getAttribute("fleetList");
	
	} %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TDS (Taxi Dispatch System)</title>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<link rel="stylesheet" type="text/css" href="jqueryNew/bootstrap.css">
<link rel="stylesheet" type="text/css" href="jqueryNew/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
<link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">
 <link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" media="screen"></link>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
        }
        .iconsForShorcut:hover{
        	background-color:gray;
        	cursor:pointer;
        	border-radius:60px;
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
<script type="text/javascript">

function changeFleetHeader(fleet,i){
	  var xmlhttp=null;
		if (window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		<%	AdminRegistrationBO adminBO=(AdminRegistrationBO)session.getAttribute("user");%>
	url='SystemSetupAjax?event=changeFleet&fleet='+fleet;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	alert(text);
	$(".fleetbutton").css('backgroundColor', '');
	$("#fleet_"+i).css('backgroundColor', 'green');
}


var mins,secs,TimerRunning,TimerID,TimerID1;
TimerRunning=false,i=1;

function Init()
{
	var sessiontime =<%= (session.getMaxInactiveInterval())/60 %>;
 mins= sessiontime;
   secs=0;
   StopTimer();
 
}

function StopTimer()
{
	 
   if(TimerRunning)
      clearTimeout(TimerID);
   TimerRunning=false;
   StartTimer();
}

function StartTimer()
{
   TimerRunning=true;
   //window.status="Time Remaining "+Pad(mins)+":"+Pad(secs);
   if(document.getElementById('minutes')!=null){
 	 	document.getElementById('minutes').innerHTML=Pad(mins);
  	 	document.getElementById('seconds').innerHTML=Pad(secs);
		TimerID=self.setTimeout("StartTimer()",1000);
		   
		   Check();
		   
		   if(mins==0 && secs==0)
		   if(secs==0)
		   {
		      //StopTimer();
		       clearTimeout(TimerID);
		      
		   }
		   if(secs==0)
		   {
		      mins--;
		      secs=60;
		   }
		   secs--;
	}
}
function showProcess(){
	if(document.getElementById("TDSAppLoading")!=null){
		$("#TDSAppLoading").show();
	}
}
function Check()
{
   if(mins==5 && secs==0){
      //alert("You have only five minutes remaining");
      if(document.getElementById('tid')!=null)
     	 document.getElementById('tid').innerHTML="<font color='blue'>You have less than 5 mins</font>"
   }else if(mins==0 && secs==0)
   {
   		if(document.getElementById('tid')!=null) 

   	   	document.getElementById('headerPage1').style.display='none';
   		//document.getElementById('headerPage2').style.display='block';
      	document.getElementById('tiderror').innerHTML="<font color='red'>Session expired please login again </font><a href='/TDS/control?action=registration&event=checkUser'>Login</a>"
      	StopTimer();  		
   }
}

function Pad(number) //pads the mins/secs with a 0 if its less than 10
{
   if(number<10)
      number=0+""+number;
   return number;
}
function dispatch1(a) {
	var div = document.getElementById("menudetail"); 
	var i;
	alert(div.childNodes.length);
	 for (i=0;i<div.childNodes.length;i++) 
		{    
			//alert(div.childNodes[i].id);
			if(div.childNodes[i].id == a) {  
					document.getElementById(div.childNodes[i].id).style.display='block';
		      	 } else {    
		      		document.getElementById(div.childNodes[i].id).style.display='none';
		      	} 			   
		}
}
function dispatch(a)
{

	var browserName=navigator.appName; 
	//alert(browserName);
	if (browserName=="Microsoft Internet Explorer")
	{ 
		var div = document.getElementById("menudetail"); 
		var i;
		//alert(div.childNodes.length);
		 for (i=0;i<div.childNodes.length;i++) { 
			 if(div.childNodes[i].id == a) {  
				document.getElementById(div.childNodes[i].id).style.display='block';
			 } else {    
			     document.getElementById(div.childNodes[i].id).style.display='none';
			 } 			   
		} 
	}
	else 
	{  
	var divCollection = document.getElementsByClassName("menudetail"); 
	for (var i=0; i<divCollection.length; i++) {
    	if(divCollection[i].getAttribute("id") == a) { 
   	  		divCollection[i].style.display='block';
     	} else {    
   	  		divCollection[i].style.display='none';
     	} 
	} 
// 	document.getElementById("showOptions").style.display="none";
//	document.getElementById("module").value=a;  
   } 
}
function openDateBox(){
	$("#getReportCSV").jqm();
	$("#getReportCSV").jqmShow();
}
function getDetailsForResponse(){
	if(document.getElementById("fromDate").value=="" && document.getElementById("toDate").value==""){
		alert("Please give atleast any one date");
	} else {
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'TestServlet?event=getDetails&fromDate='+document.getElementById("fromDate").value+'&toDate='+document.getElementById("toDate").value;
//		xmlhttp.open("GET", url, false);
		window.open(url,'Reports');
		$("#getReportCSV").jqmHide();
	}
}
 </script>
</head>
<body  onload="Init()">
	<div class="jqmWindow" id="getReportCSV">
	<table>
		<center><h3>Off Airport Reports</h3></center>	
		<tr>
			<td>From Date&nbsp;<input type="text" name="fromDate" id="fromDate" value="" onclick="displayCalendar(this,'mm/dd/yyyy',this);">
			<td>To Date&nbsp;<input type="text" name="toDate" id="toDate" value="" onclick="displayCalendar(this,'mm/dd/yyyy',this);">
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="button" value="Get" name="getDetails" onclick="getDetailsForResponse()"/></td>
		</tr>
	</table>
	</div>
    <div class="navbar">
        <div class="navbar-inner">
        <ul class="nav pull-right" >
                   <c:choose >	
		          <c:when test="${ not empty sessionScope.user }" >
		          	<li style="margin-left: -30%;"><a href="#" class="hidden-phone visible-tablet visible-desktop" id="dispatch" title="Dispatch" onclick="dispatch('dispatchView');"><font style="font-size: medium;">Dispatch</font></a></li>
                    <li style="margin-left: -15%;"><a href="#" class="hidden-phone visible-tablet visible-desktop" title="Operations" onclick="dispatch('operationView');"><font style="font-size: medium;">Operations</font></a></li>
                    <li style="margin-left: 0%;"><a href="#" class="hidden-phone visible-tablet visible-desktop" title="Finance" onclick="dispatch('financeView');"><font style="font-size: medium;">Finance</font></a></li>
                    <li style="margin-left: 0%;"><a href="control?action=openrequest&event=Report" class="hidden-phone visible-tablet visible-desktop" title="Reports"><font style="font-size: medium;">Reports</font></a></li>
                    <li style="margin-left: 0%;"><a href="#" class="hidden-phone visible-tablet visible-desktop" title="System Setup" onclick="dispatch('systemsetupView');"><font style="font-size: medium;">System Setup</font></a></li>
                    <li id="fat-menu" class="dropdown" style="margin-left: 20%;">
                        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-user"></i> ${sessionScope.user.userNameDisplay}
                            <i class="icon-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li onclick="showProcess()"><a tabindex="-1" href="control?action=systemsetup&event=userProfile&module=systemsetupView">My Account</a></li>
                            <li class="divider"></li>
                            <li class="divider visible-phone"></li>
                            <li onclick="showProcess()"><a tabindex="-1" href="control?action=registration&event=logout">Logout</a></li>
                        </ul>
                    </li>
                    <li style="margin-top: 1%;">
						<span id="minutes" style="color: white;font-size: medium;"></span>
						<font style="color: white;font-size: medium;">:</font>
                    </li>
                    <li style="margin-top: 1%;">	
                    	<span id="seconds" style="color: white;font-size: medium;"></span>
                    </li>
                     </c:when>
<%-- 					<c:otherwise>
							<li><a href="control?action=registration&event=checkUser" class="hidden-phone visible-tablet visible-desktop" role="button"><font style="font-size: medium;">Login</font></a></li>
							<li><a href="control?action=registration&event=saveDriver&subevent=load" class="hidden-phone visible-tablet visible-desktop" role="button" title="Diver Reg"><font style="font-size: medium;">Driver Reg</font></a></li>
							<li><a href="control?action=registration&event=savecompany" class="hidden-phone visible-tablet visible-desktop" role="button" title="Company Reg"><font style="font-size: medium;">Company Reg</font></a></li>
							<li><a href="control?action=openrequest&event=saveRequest" class="hidden-phone visible-tablet visible-desktop" role="button" title="Open Request"><font style="font-size: medium;">Open Request</font></a></li>
							<li><a href="control?action=openrequest&event=getreceipt" class="hidden-phone visible-tablet visible-desktop" role="button" title="Get Receipt"><font style="font-size: medium;">Get Receipt</font></a></li>
				</c:otherwise>
 --%>                    </c:choose>
			</ul>
                <a class="brand"><span class="first"><img alt="" src="jqueryNew/logo2.png" /></span> <span class="second">Get-A-Cab</span>
                <span class="second">
                         <%if(fleetList!=null&&fleetList.size()>0){%>
                         	<br>	
							  <%for(int i=0;i<fleetList.size();i++){ %>
									<td>
										<input type="button" name="fleet_<%=i%>" class="fleetbutton"  value ="<%=fleetList.get(i).getFleetName()%>" id ="fleet_<%=i%>" <%=fleetList.get(i).getFleetNumber().equalsIgnoreCase(adminBO.getAssociateCode())?"style='background-color:green'":""%> onclick="changeFleetHeader('<%=fleetList.get(i).getFleetNumber()%>','<%=i%>')" />
	 								</td>
							  <%} %>
  						<%} %>
                </span>
                </a>
	 			<img src="images/loadingBar.gif" style="height:8px;width:100%;display:none" id="TDSAppLoading"/>
 			</div>
 		</div>
 		
    <div class="sidebar-nav">
		<% 
		String leftscreen = "/leftNavChildNew.jsp";
		if(request.getAttribute("leftscreen") != null) {
			leftscreen = (String)request.getAttribute("leftscreen");
		}  
		if(request.getParameter("leftscreen") != null) {
			leftscreen = (String)request.getParameter("leftscreen");
		}

		%> 
		<jsp:include flush="true" page="<%= leftscreen %>" /> 
	</div>
	 <div class="content">
		   <div class="header">
            <div class="stats">
		</div>
			</div>
			
	 <div class="container-fluid">
          <div class="row-fluid">
		<% 
		String screen = "";
	//	String screen ="";
		if(request.getParameter("screen") != null) {
			screen = (String)request.getParameter("screen");
		}if(request.getAttribute("screen") != null) {
			screen = (String)request.getAttribute("screen");
		}
		%> 
		<jsp:include flush="true" page="<%= screen %>" /> 
				<%if(screen.equals("")) {%>
            <div>
				<table style="margin-top: 2%;width: 100%;" cellpadding="10px" align="center">
					<tr>
						<td colspan="6" align="center"><font style="font-style: italic;color:#F87217;font-size: large;font-weight: bold;text-decoration: underline;">Quick Links</font></td>
					</tr>
					<tr>
						<td colspan="3" align="center"><font style="font-style: italic;color:#F87217;font-size: large;font-weight: bold;text-decoration: underline;">Dispatch</font></td>
						<td colspan="3" align="center"><font style="font-style: italic;color:#F87217;font-size: large;font-weight: bold;text-decoration: underline;">Operation</font></td>
					</tr>
					<tr>
						<td align="center"><a href="control?action=openrequest&event=saveRequest&module=dispatchView"><img class="iconsForShorcut" alt="" src="images/CreateAJob.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Call Taker</font>
						</a></td>
						<td align="center"><a href="DashBoard?event=dispatchValues"><img class="iconsForShorcut" alt="" src="images/Dashboard.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Dashboard</font>
						</a></td>
						<td align="center"><a href="control?action=openrequest&event=sharedRide"><img class="iconsForShorcut" alt="" src="images/SharedRide.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Shared Ride</font>
						</a></td>
						<td align="center"><a href="control?action=registration&event=custInfo&module=operationView"><img class="iconsForShorcut" alt="" src="images/ClientAcct.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Client Account</font>
						</a></td>
						<td align="center"><a href="control?action=TDS_DOCUMENT_UPLOAD&event=passKeyGen&module=operationView&do=disp"><img class="iconsForShorcut" alt="" src="images/DriverDoc.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Documents</font>
						</a></td>
						<td align="center"><a href="control?action=CustomerService&event=createComplaints&module=operationView"><img class="iconsForShorcut" alt="" src="images/Complaints.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Complaints</font>
						</a><br><br></td>
					</tr>
					<tr>
						<td colspan="3" align="center"><font style="font-style: italic;color:#F87217;font-size: large;font-weight: bold;text-decoration: underline;">Finance</font></td>
						<td colspan="1" align="center"><font style="font-style: italic;color:#F87217;font-size: large;font-weight: bold;text-decoration: underline;">Reports</font></td>
						<td colspan="2" align="center"><font style="font-style: italic;color:#F87217;font-size: large;font-weight: bold;text-decoration: underline;">System Settings</font></td>
					</tr>
					<tr>
						<td align="center"><a href="ChargesAction?event=showDriverDisbursement&module=financeView"><img class="iconsForShorcut" alt="" src="images/Disbursement.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Disbursement</font>
						</a></td>
						<td align="center"><a href="control?action=finance&event=generateInvoice&module=financeView"><img class="iconsForShorcut" alt="" src="images/Invoice.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Invoice</font>
						</a></td>
						<td align="center"><a href="control?action=manualccp&event=getSummarytoAlterlimit&module=financeView"><img class="iconsForShorcut" alt="" src="images/CreditCard.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Credit Card Txn</font>
						</a></td>
						<td align="center"><a href="control?action=openrequest&event=Report"><img class="iconsForShorcut" alt="" src="images/reportMain.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Reports</font>
						</a></td>
						<td align="center"><a href="control?action=systemsetup&event=systemProperties&module=systemsetupView"><img class="iconsForShorcut" alt="" src="images/SysSetup.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Settings</font>
						</a></td>
						<td align="center"><a href="control?action=registration&event=driverSave&subevent=driver&module=systemsetupView"><img class="iconsForShorcut" alt="" src="images/Registration.png" onclick="showProcess()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Registration</font>
						</a></td>
					</tr>
 			</table>
			</div>
			<div>
				<table style="margin-top: 10%;width: 100%;">
				<tr>
					<td align="center"><img alt="" src="images/reportMain.png" onclick="openDateBox()"/><br>
						<font style="font-style: italic;color:#000000;font-size: medium;">Off Airport Reports</font>
					</td>
				</tr>
				</table>
			</div>
		<%} %>
		</div>
	</div>
	</div>	
		
	    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
	
</body>
</html>