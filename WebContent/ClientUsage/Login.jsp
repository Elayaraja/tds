<!DOCTYPE html>

<html style="height:100%;width:100%">
<head>
<title>Login</title>



<style>



body
{
    font: 12px 'Lucida Sans Unicode', 'Trebuchet MS', Arial, Helvetica;    
    margin: 0;
    background-color: #d9dee2;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebeef2), to(#d9dee2));
    background-image: -webkit-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -moz-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -ms-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -o-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: linear-gradient(top, #ebeef2, #d9dee2);    
}

/*--------------------*/

#login
{
    background-color: #fff;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));
    background-image: -webkit-linear-gradient(top, #fff, #eee);
    background-image: -moz-linear-gradient(top, #fff, #eee);
    background-image: -ms-linear-gradient(top, #fff, #eee);
    background-image: -o-linear-gradient(top, #fff, #eee);
    background-image: linear-gradient(top, #fff, #eee);  
  
    z-index: 0;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;  
    -webkit-box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),
          0 1px 1px rgba(0, 0, 0, .2),
          0 3px 0 #fff,
          0 4px 0 rgba(0, 0, 0, .2),
          0 6px 0 #fff,  
          0 7px 0 rgba(0, 0, 0, .2);
    -moz-box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),  
          1px 1px   0 rgba(0,   0,   0,   .1),
          3px 3px   0 rgba(255, 255, 255, 1),
          4px 4px   0 rgba(0,   0,   0,   .1),
          6px 6px   0 rgba(255, 255, 255, 1),  
          7px 7px   0 rgba(0,   0,   0,   .1);
    box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),  
          0 1px 1px rgba(0, 0, 0, .2),
          0 3px 0 #fff,
          0 4px 0 rgba(0, 0, 0, .2),
          0 6px 0 #fff,  
          0 7px 0 rgba(0, 0, 0, .2);
}

#login:before
{
    content: '';
   /*  position: relative; */
    z-index: -1;
    /* border: 1px dashed #ccc;
    top: 5px;
    bottom: 5px;
    left: 5px;
    right: 5px; */
    -moz-box-shadow: 0 0 0 1px #fff;
    -webkit-box-shadow: 0 0 0 1px #fff;
    box-shadow: 0 0 0 1px #fff;
}

/*--------------------*/

h1
{
    text-shadow: 0 1px 0 rgba(255, 255, 255, .7), 0px 2px 0 rgba(0, 0, 0, .5);
    text-transform: uppercase;
    text-align: center;
    color: #666;
    margin: 0 0 30px 0;size: 40px
    letter-spacing: 4px;
    font: normal 26px/1 Verdana, Helvetica;
   /*  position: relative; */
}

h1:after, h1:before
{
    background-color: #777;
    content: "";
    height: 1px;
   /*  position: absolute; */
    top: 15px;
    width: 60%;   
}

h1:after
{ 
    background-image: -webkit-gradient(linear, left top, right top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(left, #777, #fff);
    background-image: -moz-linear-gradient(left, #777, #fff);
    background-image: -ms-linear-gradient(left, #777, #fff);
    background-image: -o-linear-gradient(left, #777, #fff);
    background-image: linear-gradient(left, #777, #fff);      
    right: 0;
}

h1:before
{
    background-image: -webkit-gradient(linear, right top, left top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(right, #777, #fff);
    background-image: -moz-linear-gradient(right, #777, #fff);
    background-image: -ms-linear-gradient(right, #777, #fff);
    background-image: -o-linear-gradient(right, #777, #fff);
    background-image: linear-gradient(right, #777, #fff);
    left: 0;
}

/*--------------------*/


/*--------------------*/

#inputs input
{
   padding: 20px 0px 0px 0px;
    margin: 0 0 10px 0;
    width: 80%; /* 353 + 2 + 45 = 400 */
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 5px;
    -moz-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
    -webkit-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
    box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
}

#username
{
    background-position: 5px -8px !important;
}

#password
{
    background-position: 5px -58px !important;
}

#inputs input:focus
{
    background-color: #fff;
    outline: none;
    -moz-box-shadow: 0 0 0 1px #e8c291 inset;
    -webkit-box-shadow: 0 0 0 1px #e8c291 inset;
    box-shadow: 0 0 0 1px #e8c291 inset;
}

/*--------------------*/


#submit
{		
    background-color: #ffb94b;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#fddb6f), to(#ffb94b));
    background-image: -webkit-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -moz-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -ms-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -o-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: linear-gradient(top, #fddb6f, #ffb94b);
    
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    
    text-shadow: 0 1px 0 rgba(255,255,255,0.5);
    
     -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
     -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
     box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;    
    
    

    float: left;
    padding: 0;
    cursor: pointer;
    font: bold 15px Arial, Helvetica;
    color: #8f5a0a;
}

#submit:hover,#submit:focus
{		
    background-color: #fddb6f;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ffb94b), to(#fddb6f));
    background-image: -webkit-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -moz-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -ms-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -o-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: linear-gradient(top, #ffb94b, #fddb6f);
}	

#submit:active
{		
    outline: none;
   
     -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
     -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
     box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;		
}

#submit::-moz-focus-inner
{
  border: none;
}

#actions a
{
    color: #3151A2;    
    float: right;
    margin-left: 1%;
}

/*--------------------*/

#back
{
    display: block;
    text-align: center;
   /*  position: relative; */
    top: 60px;
    color: #999;
}


</style>
<script src="js/jquery.js"></script>
<script type="text/javascript">
$("#bodyPageLogin").css('height',$(window).height()+'px');
$("#login").css('height',$(window).height()+'px');
$(document).ready(function() {
if(document.getElementById("os").value=="G"){
	Android.homePage("");
	}
});
</script>
</head>

<%
 if(request.getAttribute("error")!=null) {%>
	<div id="error" style="margin-left: 10%;width: 100%;"><input style="width: 80%;font-size: xx-large;font-weight: bolder;color: red;" type="image" value='<%=request.getAttribute("error")==null?"":request.getAttribute("error")%>'/></div>
    <%} %>
<body id="bodyPageLogin" style="min-width:100%;min-height :100%;">
<form id="login" action="CustomerMobileAction?event=clientLogin" method="post" style="width:100%;height :100%;position:absolute;">
<input type="hidden" name="event" value="clientLogin"/>
	<input type="hidden" name="ccode" id="ccode" value="<%=request.getParameter("ccode")%>" /> 
	<input type="hidden" name="os" id="os" value="<%=request.getParameter("os")%>" /> 
	<input type="hidden" name="phoneNumber" id="phoneNumber" value="<%=request.getParameter("phoneNumber")%>" /> 
    <input id="password" name="loginOrLogout" type="hidden" value="1">
        <input id="responseType" name="responseType" type="hidden" value="HTML5">
        <table id="tableLogin" style="height:100%;width:100%"><tr><td>
    <h1 style="font-size:large;font-weight: bold;" align="center">Log In</h1>
    </td></tr><tr><td>
    <fieldset id="inputs" style="border:none;">
    <img src="css/images/client/user.png"/>
        <input id="userId" name="userId" type="text" style="font-size: large;" placeholder="Username" autofocus required>   
    </fieldset >
    </td></tr><tr><td>
    <fieldset id="inputs" style="border:none;">
    <img  src="css/images/client/password.png"/>
            <input id="password" name="password" type="password" style="font-size: large;" placeholder="Password" required>
    </fieldset>
    </td></tr><tr><td align="right">
    <fieldset id="actions" style="border:none;"   >
        <input type="submit"  id="submit" name="submit" style="width: 50%;height:10%;font-size: large;font-weight: bold;" value="Log in"><br/>
    </fieldset>  
        </td></tr><tr><td align="left">
        <fieldset id="actions" style="border:none;"   >
    
    <a href="CustomerMobileAction?event=clientRegistration&ccode=<%=request.getParameter("ccode") %>&phoneNumber=<%=request.getParameter("phoneNumber")%>&os=<%=request.getParameter("os") %>&responseType=HTML5" style="font-size:large;">Register</a>
   </fieldset>
    </td></tr><tr><td>        <fieldset id="actions" style="border:none;"   >

        <a href="" style="font-size: large;">Forgot your password?</a>
       
    </fieldset>
    </td></tr></table>
</form>

 
</body>
</html>