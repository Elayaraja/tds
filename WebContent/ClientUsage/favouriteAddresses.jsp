<!DOCTYPE html>

<%@page import="com.tds.dao.CustomerMobileDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.CustomerMobileBO"%>

<%@page import="com.common.util.TDSProperties"%><html>
<head>
<script type="text/javascript"
	src="js/jquery.js"></script>
<!-- BEGIN Demo CSS - You can safely ignore this part -->
<link rel="stylesheet" href="css/client/demo.css">
<link rel="stylesheet" href="css/client/table.css">

<!-- END Demo CSS -->
<style>
h1
{
    text-shadow: 0 1px 0 rgba(255, 255, 255, .7), 0px 2px 0 rgba(0, 0, 0, .5);
    text-transform: uppercase;
    text-align: center;
    color: #666;
    margin: 0 0 30px 0;size: 40px
    letter-spacing: 4px;
    font: normal 26px/1 Verdana, Helvetica;
}

h1:after, h1:before
{
    background-color: #777;
    content: "";
    top: 15px;
    width: 60%;   
}

h1:after
{ 
    background-image: -webkit-gradient(linear, left top, right top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(left, #777, #fff);
    background-image: -moz-linear-gradient(left, #777, #fff);
    background-image: -ms-linear-gradient(left, #777, #fff);
    background-image: -o-linear-gradient(left, #777, #fff);
    background-image: linear-gradient(left, #777, #fff);      
    right: 0;
}

h1:before
{
    background-image: -webkit-gradient(linear, right top, left top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(right, #777, #fff);
    background-image: -moz-linear-gradient(right, #777, #fff);
    background-image: -ms-linear-gradient(right, #777, #fff);
    background-image: -o-linear-gradient(right, #777, #fff);
    background-image: linear-gradient(right, #777, #fff);
    left: 0;
}
  /*   .three {  
       border: 1px solid #CCCCCC;
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 1px 1px #CCCCCC;
            background-color: #808080;
            background: -webkit-gradient(linear, left top, left bottom, from(#606060), to(#909090));
            background: -moz-linear-gradient(top, #606060, #909090);
            color: #dadada;
    }  
    .head
    {
        background-color: White;
            vertical-align:middle;
            padding: 0.6em;
            font-size:0.8em;
    }
    .three td, .three th
    {
     border-left: 1px solid #CCCCCC;
    border-top: 1px solid #CCCCCC;
    text-align: left;
    }
	.headingTd
	{
	 background-color: #DCE9F9;
    background-image: -moz-linear-gradient(center top , #EBF3FC, #DCE9F9);
    border-top: medium none;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.8) inset;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	} */
</style>

<!-- BEGIN Navigation bar CSS - This is where the magic happens -->
<link rel="stylesheet" href="css/client/navbar.css">

<!-- END Navigation bar CSS -->

<!-- BEGIN JavaScript -->
<script src="js/jquery.js"></script>
<script type="text/javascript">
function bookJob(address,latitude,longitude,name){
	var os=document.getElementById("os").value;
	var ccode=document.getElementById("ccode").value;
	var phoneNumber=document.getElementById("phoneNumber").value;
	 window.location='CustomerMobileAction?event=openRequest&responseType=HTML5&ccode='+ccode+'&phoneNumber='+phoneNumber+'&os='+os+'&latitude='+latitude+'&longitude='+longitude+'&address='+address+'&fromFavAdd=yes';

}

$(document).ready(function(){
	$('#bodypage').css('width',$(window).width()+'px');
	$('body').css('width',$(window).width()+'px');
	$('#tripsForm').css('width',$(window).width()+'px');
	if(document.getElementById("os").value=="G"){
		Android.homePage("");
		}
});
</script>

	<body  style="background-color: 	black;">
	<form id="tripsForm">
	<input type="hidden" name="ccode" id="ccode" value="<%=request.getParameter("ccode")%>" /> 
	<input type="hidden" name="phoneNumber" id="phoneNumber" value="<%=request.getParameter("phoneNumber")%>" /> 
		<input type="hidden" name="os" id="os" value="<%=request.getParameter("os")%>" /> 
	<table class="three" id="bodypage" >
	<tr><td colspan="5" class="headingTd" style="font-size: medium;font-weight: bolder; font-family: fantasy; color: #3B3131;" ><center>Favourite Address</center></td></tr>
	<%if(request.getAttribute("favouriteAddresses")!=null){ 
	
		ArrayList<CustomerMobileBO> favouriteAddresses=(ArrayList<CustomerMobileBO>)request.getAttribute("favouriteAddresses");%>
		 	<tr class="head">
		<td class="headingTd" style="font-size: small;font-weight: bolder; font-family: fantasy; color: #3B3131;" >Address</td>
		<td class="headingTd" style="font-size: small;font-weight: bolder; font-family: fantasy; color: #3B3131;" >Tagged Name</td>
		<td class="headingTd" style="font-size: small;font-weight: bolder; font-family: fantasy; color: #3B3131;" >Book</td>
	</tr>
<% 
	boolean colorLightGray = true;
	String colorPattern;%>
	<%for(int i=0;i<favouriteAddresses.size();i++) { 
		colorLightGray = !colorLightGray;
		if(colorLightGray){
			colorPattern="style=\"background-color:gray\""; 
			}
		else{
			colorPattern="style=\"background-color:#F2F2F2\"";
		}
	%>
 	<tr> 
		<td align="center" style="font-size:x-small; font-family: fantasy; color: white;"  <%=colorPattern%>><%=favouriteAddresses.get(i).getAddress()%>  </td>
		<td align="center" style="font-size:x-small; font-family: fantasy; color: white;"  <%=colorPattern%>><%=favouriteAddresses.get(i).getTagAddress()%>  </td>
		<td align="center" style="font-size:x-small; font-family: fantasy; color: white;" <%=colorPattern%>><input type="button" name="button" value="Book" onclick="bookJob('<%=favouriteAddresses.get(i).getAddress()%>','<%=favouriteAddresses.get(i).getLatitude()%>','<%=favouriteAddresses.get(i).getLongitude()%>','<%=favouriteAddresses.get(i).getName()%>');"/>  </td>
	</tr>
	<% } }%>
	
	
	
</table>
	<a href ="CustomerMobileAction?event=clientLogin&home=yes&os=<%=request.getParameter("os")%>">Home</a>
	</form>
</body>
</html>