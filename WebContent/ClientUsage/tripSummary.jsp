<!DOCTYPE html>

<%@page import="com.tds.dao.CustomerMobileDAO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.CustomerMobileBO"%>

<%@page import="com.common.util.TDSProperties"%><html>
<head>
<!-- BEGIN Demo CSS - You can safely ignore this part -->
<link rel="stylesheet" href="css/client/demo.css">
<link rel="stylesheet" href="css/client/table.css">

<!-- END Demo CSS -->
<style>
h1
{
    text-shadow: 0 1px 0 rgba(255, 255, 255, .7), 0px 2px 0 rgba(0, 0, 0, .5);
    text-transform: uppercase;
    text-align: center;
    color: #666;
    margin: 0 0 30px 0;
    letter-spacing: 4px;
    font: normal 26px/1 Verdana, Helvetica;
    position: relative;
}

h1:after, h1:before
{
    content: "";
    height: 1px;
    position: absolute;
    top: 15px;
    width: 60%;   
}
 
</style>

<!-- BEGIN Navigation bar CSS - This is where the magic happens -->
<link rel="stylesheet" href="css/client/navbar.css">

<!-- END Navigation bar CSS -->

<!-- BEGIN JavaScript -->
<script src="js/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#bodypage').css('width',$(window).width()+'px');
	$('body').css('width',$(window).width()+'px');
	if(document.getElementById("os").value=="G"){
		Android.homePage("");
		}
});
</script>

	<body style="background-color: 	black;">
		<table class="three" id="bodypage" width="100%">
		<tr><td colspan="5" class="headingTd" style="font-size: medium;font-weight: bolder; font-family: fantasy; color: #3B3131;">
	    <center>Reserved Trips</center></td></tr>
	<%if(request.getAttribute("tripSummary")!=null){ 
	
		ArrayList<CustomerMobileBO> tripSummary=(ArrayList<CustomerMobileBO>)request.getAttribute("tripSummary");%>
	
 	<tr class="head">
		<td class="headingTd" style="font-size: small;font-weight: bolder; font-family: fantasy; color: #3B3131;" >Name</td>
		<td class="headingTd" style="font-size: small;font-weight: bolder; font-family: fantasy; color: #3B3131;" >Trip ID</td>
		<td class="headingTd" style="font-size: small;font-weight: bolder; font-family: fantasy; color: #3B3131;" >Phone Number</td>
		<td class="headingTd" style="font-size: small;font-weight: bolder; font-family: fantasy; color: #3B3131;" >Address</td>
		<td class="headingTd" style="font-size: small;font-weight: bolder; font-family: fantasy; color: #3B3131;" >City</td>
	</tr>
<% 
	boolean colorLightGray = true;
	String colorPattern;%>
	<%for(int i=0;i<tripSummary.size();i++) { 
		colorLightGray = !colorLightGray;
		if(colorLightGray){
			colorPattern="style=\"background-color:gray\""; 
			}
		else{
			colorPattern="style=\"background-color:#F2F2F2\"";
		}
	%>
 	<tr> 
		<td align="center" style="font-size: small; font-family: fantasy; color: white;"  <%=colorPattern%>><%=tripSummary.get(i).getName()%>  </td>
		<td align="center" style="font-size: small; font-family: fantasy; color: white;"  <%=colorPattern%>><%=tripSummary.get(i).getTripId()%>  </td>
		<td align="center" style="font-size: small; font-family: fantasy; color: white;" <%=colorPattern%>><%=tripSummary.get(i).getPhoneNumber()%>  </td>
		<td align="center" style="font-size: small; font-family: fantasy; color: white;" <%=colorPattern%>><%=tripSummary.get(i).getAddress()%>  </td> 
		<td align="center" style="font-size: small; font-family: fantasy; color: white;" <%=colorPattern%>><%=tripSummary.get(i).getCity()%>  </td>
	</tr>
	<% } }%>
	
	
	
</table>
	<a href ="CustomerMobileAction?event=clientLogin&home=yes">Home</a>
</body>
</html>