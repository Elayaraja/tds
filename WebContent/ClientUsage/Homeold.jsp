<!DOCTYPE html>

<%@page import="com.common.util.TDSProperties"%><html>
<head>
<!-- BEGIN Demo CSS - You can safely ignore this part -->
<link rel="stylesheet" href="css/client/demo.css">
<!-- END Demo CSS -->

<!-- BEGIN Navigation bar CSS - This is where the magic happens -->
<link rel="stylesheet" href="css/client/navbar.css">
<!-- END Navigation bar CSS -->

<!-- BEGIN JavaScript -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript"> 
$(document).ready(function(){

	// Requried: Navigation bar drop-down
	$("nav ul li").hover(function() {
		$(this).addClass("active");
		$(this).find("ul").show().animate({opacity: 1}, 400);
		},function() {
		$(this).find("ul").hide().animate({opacity: 0}, 200);
		$(this).removeClass("active");
	});
	
	// Requried: Addtional styling elements
	$('nav ul li ul li:first-child').prepend('<li class="arrow"></li>');
	$('nav ul li:first-child').addClass('first');
	$('nav ul li:last-child').addClass('last');
	$('nav ul li ul').parent().append('<span class="dropdown"></span>').addClass('drop');

});
</script>


<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Welcome to TDS</title>
<link rel="stylesheet" type="text/css" href="css/client/blue.css" media="screen, projection, tv">
<script language="javascript" type="text/javascript" src="js/jquery.js"></script>
<script language="javascript" type="text/javascript" src="js/client/jquery.cookie.js"></script>
<script src='<%= TDSProperties.getValue("googleMapV3") %>'
	type="text/javascript"></script>
</head>
<body onload="loadLocation()">
<header>
<div class="wrapper">
		<!-- BEGIN Dark navigation bar -->
		<nav class="dark">
			<ul class="clear">
				<li><a href="#">Home</a></li>
				<li><a href="#">Create a Job</a></li>
			 	<li><a href="#">Services</a>
					<ul>
						<li><a href="#">Trips</a></li>
						<li><a href="#"></a></li>
					</ul>
				</li>
			 	<li><a href="#">Account</a>
			 	<ul>
						<li><a href="#">Create</a></li>
						<li><a href="#">Modify</a></li>
					</ul>
			 	</li>
			</ul>
		</nav>
		<!-- END Dark navigation bar -->
		
	</div>
</header>
<section>
<div id="container">
	<div class="inner-container">
		<div class="box box-50">
			<div class="boxin" style="width:85%;margin-left:3%">
				<!-- 
				<div class="header">
					<h3>HTML5 Geo-Location</h3>
				</div>
			 -->	
				<!-- 
				<div class="content">
				 	<ul class="simple">
                        <li class="even">
                            <strong><span id="status"></span></strong>
                        </li>
						<li class="odd">
                            <strong>Your Latitude</strong>
                            <span id="latitude"></span>
                        </li>
                        <li class="even">
                            <strong>Your Longitude</strong>
                            <span id="longitude"></span>
                        </li>
                        <li class="odd">
                            <strong>Accuracy (in Meters)</strong>
                            <span id="accuracy"></span>
                        </li>
						</ul> 
                        <div id="placeholder" style="margin: 20px 0px 10px; padding-left: 20px; width: 100%; height: 100%; position: relative;">
                        <i>Note: May take a few seconds to get the location.</i>
                        </div>
                </div>
                 -->
                 <div class="content">
                 	  <ul class="simple">
                        <li class="even">
                            <strong><span id="status"></span></strong>
                        </li>
                       </ul>
                      <!--   	
                 	 <div id="placeholder"  style="margin: 20px 0px 10px; padding-left: 20px; width: 100%; height: 100%; position: relative;">
                        <i>Note: May take a few seconds to get the location.</i>
                        </div>
                         -->
                         <div id="placeholder" style="width: 400px; height: 300px" >
                        <i>Note: May take a few seconds to get the location.</i>
                        </div>
                 </div>
            </div>
        </div>
    </div>
</div>
</section>


<script type="text/javascript">
            
    var latitude;
    var longitude;
    var accuracy;
    
    function loadLocation() {
    
        if(navigator.geolocation) {
            document.getElementById("status").innerHTML = "HTML5 Geolocation is supported in your browser.";
            document.getElementById("status").style.color = "#1ABC3C";
            
            if($.cookie("posLat")) {
                latitude = $.cookie("posLat");
                longitude = $.cookie("posLon");
                accuracy = $.cookie("posAccuracy");
                //document.getElementById("status").innerHTML = "Location data retrieved from cookies. <a id=\"clear_cookies\" href=\" javascript:clear_cookies();\" style=\"cursor:pointer; margin-left: 15px;\"> clear cookies</a>";
                updateDisplay();
                
            } else {
                navigator.geolocation.getCurrentPosition(
                                    success_handler, 
                                    error_handler, 
                                    {timeout:10000});
            }
        }
    }

    function success_handler(position) {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        accuracy = position.coords.accuracy;
        
        if (!latitude || !longitude) {
            document.getElementById("status").innerHTML = "HTML5 Geolocation supported, but location data is currently unavailable.";
            return;
        }
        
        updateDisplay();
        
        $.cookie("posLat", latitude);
        $.cookie("posLon", longitude);
        $.cookie("posAccuracy", accuracy);
      
    }
    
   /* function updateDisplay() {
        //var gmapdata = '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;ie=UTF8&amp;hq=&amp;ll=' + latitude + ',' + longitude + '&amp;output=embed"></iframe>';
        var gmapdata = '<img src="http://maps.google.com/maps/api/staticmap?center=' + latitude + ',' + longitude + '&zoom=16&size=425x350&sensor=false" />';
                
        document.getElementById("placeholder").innerHTML = gmapdata;
        //document.getElementById("latitude").innerHTML = latitude;
        //document.getElementById("longitude").innerHTML = longitude;
        //document.getElementById("accuracy").innerHTML = accuracy;

         var baseIcon = new GIcon(G_DEFAULT_ICON);
        baseIcon.iconSize = new GSize(20, 34);
        baseIcon.shadowSize = new GSize(37, 34);
        baseIcon.iconAnchor = new GPoint(9, 34);
        baseIcon.infoWindowAnchor = new GPoint(9, 2);
        
        
        // var map = new GMap2(document.getElementById("placeholder"));
        var latlng = new GLatLng(latitude, longitude);
        gmapdata.addOverlay(createMarker(latlng));

         function createMarker(point) {

             var letteredIcon = new GIcon(baseIcon);

             // Set up our GMarkerOptions object
             markerOptions = { icon:letteredIcon };
             var marker = new GMarker(point, markerOptions);
              
             return marker;
           }
        
    } */

    var latlng = "";
    function updateDisplay() {

    	latlng= new google.maps.LatLng(latitude, longitude);
    	var externalImage = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
    	var options = {
				zoom: 12,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				draggableCursor: 'crosshair'
		};

		var map = new google.maps.Map(document.getElementById("placeholder"), options);
		//GEOCODER
		geocoder = new google.maps.Geocoder();
		marker = new google.maps.Marker({
			position : latlng,
			map : map,
			icon : externalImage
		});
    	
        	
    }
    
    
    function error_handler(error) {
        var locationError = '';
        
        switch(error.code){
        case 0:
            locationError = "There was an error while retrieving your location: " + error.message;
            break;
        case 1:
            locationError = "The user prevented this page from retrieving a location.";
            break;
        case 2:
            locationError = "The browser was unable to determine your location: " + error.message;
            break;
        case 3:
            locationError = "The browser timed out before retrieving the location.";
            break;
        }

        document.getElementById("status").innerHTML = locationError;
        document.getElementById("status").style.color = "#D03C02";
    }
    
    function clear_cookies() {
        $.cookie('posLat', null);
        document.getElementById("status").innerHTML = "Cookies cleared.";
    }
    
   
</script>
</body>
</html>