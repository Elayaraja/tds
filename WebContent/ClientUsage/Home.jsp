<!DOCTYPE html>

<%@page import="com.common.util.TDSProperties"%><html>
<head>
<!-- BEGIN Demo CSS - You can safely ignore this part -->
<link rel="stylesheet" href="css/client/demo.css">
<!-- END Demo CSS -->

<!-- BEGIN Navigation bar CSS - This is where the magic happens -->
<link rel="stylesheet" href="css/client/navbar.css">
<!-- END Navigation bar CSS -->

<!-- BEGIN JavaScript -->
<script type="text/javascript"
	src="js/jquery.js"></script>
<style type="text/css">
#addressCustomer {
	-moz-border-radius: 8px 8px 8px 8px;
	border-radius: 8px;
	background-color: wheat;
	font-size: large;
	font-weight: bolder;
	font-family: fantasy;
	font-size: large;
	
	border-color: buttonshadow;
	margin-left: auto;
	margin-right: auto;
	}
	#addressCustomer:hover{
	background-color:khaki;
	cursor: pointer;
	}
</style>

<script type="text/javascript"> 

$(document).ready(function(){
	if(document.getElementById("os").value=="G"){
		Android.homePage("exit home");
	}
	$('#latitude').val("");
	$('#longitude').val("");
	$("#address").val("");
	$(document).css('height',$(window).height()+'px');
	$('#placeholder').css('height',$(window).height()/1.177865613+'px');
	$('#placeholder').css('width',$(window).width()-10+'px');
	$('#focusPlace').css('margin-top',$("#placeholder").height()/1.1-$('#placeholder').position().top/2+'px');
	$('#focusPlace').css('margin-left',($("#placeholder").width()/1.02)-24+'px');
	$('#customerImage1').css('margin-top',$("#placeholder").height()/2-$('#placeholder').position().top+'px');
	$('#customerImage1').css('margin-left',($("#placeholder").width()/2)-24+'px');
	$('#addressCustomer').css('height',$(window).height()/15.775+'px');
	$('#addressCustomer').css('margin-top',$(window).height()/1.502380952+'px');
	$('#addressCustomer').css('width',$(window).width()-10+'px');
	$('#menuTR').css('margin-top',$(window).height()/1.23+'px');
	
	// Requried: Navigation bar drop-down
	$("nav ul li").hover(function() {
		$(this).addClass("active");
		$(this).find("ul").show().animate({opacity: 1}, 400);
		},function() {
		$(this).find("ul").hide().animate({opacity: 0}, 200);
		$(this).removeClass("active");
	});
	
	// Requried: Addtional styling elements
	$('nav ul li ul li:first-child').prepend('<li class="arrow"></li>');
	$('nav ul li:first-child').addClass('first');
	$('nav ul li:last-child').addClass('last');
	$('nav ul li ul').parent().append('<span class="dropdown"></span>').addClass('drop');
	companyCode();
	Init();
});
function companyCode(){
	var ccode=prompt("Please enter your Company Code","");

	if (ccode!=null ) {
	  document.getElementById("ccode").value=ccode;
	}else{
		 alert("Enter your company code"); 
	  }
}
</script>
<script type="text/javascript">
	var mins, secs, TimerRunning, TimerID, TimerID1, thisJobIsRunning, thisJobIsRunningForDriver;
	TimerRunning = false, i = 1;

	function Init()//call the Init function when u need to start the timer
	{
		mins = 1;
		secs = 0;
		StopTimer();
	}

	function StopTimer() {
		if (TimerRunning)
		clearTimeout(TimerID);
		TimerRunning = false;
		secs = 10;
		StartTimer();
	}
	function StartTimer() {

		TimerRunning = true;
		TimerID = self.setTimeout("StartTimer()", 1000);
		if (secs == 0) {
			StopTimer();
		//	driverLocation();
		//	updateDisplay();
		}
		thisJobIsRunning = false;
		thisJobIsRunningForDriver = false;
		secs--;

	}
	 function bookJob(){
	    	var latitude="";
	    	var longitude="";
	    	var address="";
			if(document.getElementById("dragend").value=="true"){	
				latitude=document.getElementById("dragLati").value;
				longitude=document.getElementById("dragLongi").value;
	    		address=document.getElementById("addressDragged").value;
			}else if(document.getElementById("dragend").value=="false"){
				address=document.getElementById("addressOnLoad").value;	
				latitude=document.getElementById("latitudeOnLoad").value;
				longitude=document.getElementById("longitudeOnLoad").value;
			}
	    	document.getElementById("latitude").value=latitude;
	    	document.getElementById("longitude").value=longitude;
	    	document.getElementById("address").value=address;
	    	$('#addressDragged').val("");
	    	document.forms["home"].submit();
	    }
	function Pad(number) //pads the mins/secs with a 0 if its less than 10
	{
		if (number < 10)
			number = 0 + "" + number;
		return number;
	}
	
	function logoutCustomer(){
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'CustomerMobileAction?event=clientLogin&responseType=HTML5&submit=YES';
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
		
	}
function loginPage(){
	var ccode=document.getElementById("ccode").value;
	var phoneNumber=document.getElementById("phoneNumber").value;
	var os=document.getElementById("os").value;
	var pushKey="";
	if(document.getElementById("os").value=="G"){
		pushKey=Android.pushKey();
		$("#pushKey").val(pushKey);
	}
	 window.location='CustomerMobileAction?event=clientLogin&responseType=HTML5&page=loginPage&ccode='+ccode+'&phoneNumber='+phoneNumber+'&os='+os;

}
function bookJobPage(){
	if(document.getElementById("os").value=="G"){
	Android.showToast("Please do login to see your favourites addresses those you tagged");
	}
	var os=document.getElementById("os").value;
	var ccode=document.getElementById("ccode").value;
	var phoneNumber=document.getElementById("phoneNumber").value;
	 window.location='CustomerMobileAction?event=openRequest&responseType=HTML5&page=loginPage&ccode='+ccode+'&phoneNumber='+phoneNumber+'&os='+os;
}
</script>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>GetACab's Customer App</title>


<script  type="text/javascript" src="js/jquery.js"></script>
<!-- <script  type="text/javascript"
	src="js/client/jquery.cookie.js"></script> -->
<script src='<%= TDSProperties.getValue("googleMapV3") %>'
	type="text/javascript"></script>
</head>
<body onload="loadLocation(); " style="background-color: black;height:90%">
<form name="home" id="home" action="CustomerMobileAction?event=openRequest" method="post">
	
    	<input type="hidden" name="event" value="openRequest"/>
	   	<input type="hidden" name="status" id="status" value="" /> 
	    <input type="hidden" name="dragend" id="dragend" value="false" /> 
		<input type="hidden" name="ccode" id="ccode" value="<%=request.getParameter("ccode")%>" /> 
		<input type="hidden" name="phoneNumber" id="phoneNumber" value="<%=request.getParameter("phoneNumber")%>" /> 
		<input type="hidden" id="driverLati" value="" /> 
		<input type="hidden" id="driverLongi" value="" />
		<input type="hidden" id="dragLati" value="" /> 
		<input type="hidden" id="dragLongi" value="" />
		<input type="hidden" name="address" id="address" value="" /> 
		<input type="hidden" name="latitude" id="latitude" value="" /> 
		<input type="hidden" name="longitude" id="longitude" value="" />
		<input type="hidden" id="switches" value="" />
		<input type="hidden" id="addressDragged" value=""/>
		<input type="hidden" id="addressOnLoad" value=""/>
		<input type="hidden" id="latitudeOnLoad" value=""/>
		<input type="hidden" id="longitudeOnLoad" value=""/>
		<input type="hidden" id="pushKey" name="pushKey" value=""/>
		<input type="hidden" id="os" value="<%=request.getParameter("os")%>"/>
	<table  style="width:100%;height:100%;position:absolute;margin-top:-0.5%" >
	<tr  style="width:50%;height:5%" align="center"><td align="center">
	<!-- <img id="companyLogo" src="css/logo.jpg" style="width:5%;height:70%"/> -->
	</td></tr>
	<tr id="addressCustomer"  align="center" style="width:100%;height:10%;position:absolute;background-color: wheat;z-index:100" onclick="bookJob()"><td id="addressCustomerTD" align="center" colspan="2">
	Book A Job
	</td></tr><tr style="width:99.8%;height:60%;position:absolute;"><td colspan="2" style="width:99.8%">	
						<div class="content" style="width: 100%;">
							<ul class="simple" style="display: none;">
								<li class="even"><strong><span id="status"></span>
								</strong></li>
							</ul>
				
                         <input type="hidden" name="addIPhone1" id="addIPhone1" value=""></input> 
								<input type="hidden" name="addIPhone1" id="addIPhone1" value=""></input>
							<div id="placeholder" style="position: absolute;width:99.8%">

								<i>Note: May take a few seconds to get the location.</i>
							</div>
							
					
					</div>
			</td></tr>
				<%if(session.getAttribute("clientUser")!=null ){ %>
		
		<%}else{ %>
			<tr id="menuTR" style="width:99.8%;height:10%;background-color: gray;position:absolute;margin-top:41%;border:1%;border-color:white;" align="center">
			<td colspan="2"  style="position:absolute;width:100%" bordercolor="white" align="center" >
			<div class="wrapper" style="width: 100%">
			<!-- BEGIN Dark navigation bar -->
			<nav class="dark">
				<ul class="clear" id="topNav">
					
					<li style="width: 50%"><a
						href="javascript:bookJobPage()"><font
							style="font-size: large;">Book</font>
					</a>
					</li>
						<li style="width: 50%"><a
						href="javascript:loginPage()"><font
							style="font-size: large;">Login</font>
					</a>
					</li>
					
				</ul>
				
			</nav>
			</div>
					</td></tr>
				<%} %>				
	</table>
	<script src="js/jquery.js"></script>
     <!--    <script src="js/modernizr.js"></script> -->
		<script>
			(function($){
				
				//cache nav
				var nav = $("#topNav");
				
				//add indicator and hovers to submenu parents
				nav.find("li").each(function() {
					if ($(this).find("ul").length > 0) {
						$("<span>").text("").appendTo($(this).children(":first"));

						//show subnav on hover
						$(this).mouseenter(function() {
							$(this).find("ul").stop(true, true).slideDown();
						});
						
						//hide submenus on exit
						$(this).mouseleave(function() {
							$(this).find("ul").stop(true, true).slideUp();
						});
					}
				});
			})(jQuery);
		</script>

	<script type="text/javascript">
    
   
      var latitude;
      var longitude;
      var accuracy;
      var map;
  	var geocoder;
      function loadLocation() {
            navigator.geolocation.getCurrentPosition(
                        success_handler_first, 
                        error_handler, 
                        {enableHighAccuracy:true, maximumAge:60000,timeout:10000,frequency:3000});
     /*   	if(navigator.geolocation) {
       		alert("in if");
               document.getElementById("status").innerHTML = "HTML5 Geolocation is supported in your browser.";
               document.getElementById("status").style.color = "#1ABC3C";
               alert("Before GeoLocation current location ");
               navigator.geolocation.getCurrentPosition(
            		 
                           success_handler_first, 
                           error_handler, 
                           {enableHighAccuracy:true, maximumAge:60000,timeout:10000,frequency:3000});
   					 //     updasteDisplay();
                   
           } else {
        	   alert("in else");
        	   navigator.geolocation.getCurrentPosition(
              		 
                       success_handler_first, 
                       error_handler, 
                       {enableHighAccuracy:true, maximumAge:60000,timeout:10000,frequency:3000});
           } */
      }


	  function LongClick(map, length) {
		    this.length_ = length;
		    var me = this;
		    me.map_ = map;
		    google.maps.event.addListener(map, 'mousedown', function(e) { me.onMouseDown_(e) });
		    google.maps.event.addListener(map, 'mouseup', function(e) { me.onMouseUp_(e) });
		  }
		  LongClick.prototype.onMouseUp_ = function(e) {
		    var now = +new Date;
		    if (now - this.down_ > this.length_) {
		      google.maps.event.trigger(this.map_, 'longpress', e);
		    }
		  };
		  LongClick.prototype.onMouseDown_ = function() {
		    this.down_ = +new Date;
		  };

    
    function success_handler_first(position) {
    	latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        accuracy = position.coords.accuracy;
        $("#latitudeOnLoad").val(latitude);
        $("#longitudeOnLoad").val(longitude);
        var point = new google.maps.LatLng(latitude, longitude);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({latLng: point}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                                var arrAddress=  results[0].address_components;
                               
                                var streetnum="";
                                var route="";
                                var itemCountry="";
                                var itemPc="";
                                $.each(arrAddress, function (i, address_component) {
                                        if (address_component.types[0] == "street_number"){
                                            /*     $("#sadd1Dash").val(address_component.long_name); */
                                                streetnum = address_component.long_name;
                                        }
                                        if (address_component.types[0] == "locality"){
                                            //    $("#scityDash").val(address_component.long_name);
                                           
                                        }
                                        if (address_component.types[0] == "route"){ 
                                                route = address_component.long_name;
                                        }
                                        if (address_component.types[0] == "country"){ 
                                                itemCountry = address_component.long_name;
                                        }
                                        if (address_component.types[0] == "postal_code"){ 
                                                itemPc = address_component.long_name;
                                              //  $("#szipDash").val(address_component.long_name);
                                        }
                                       document.getElementById("addressCustomerTD").innerHTML=results[0].formatted_address;
                               		document.getElementById("addressOnLoad").value=results[0].formatted_address;
                                });
                        }
                }
        });
        //alert("latitude-->"+latitude);
        //alert("longitude-->"+longitude);
        //alert("success:"+latitude+":"+longitude);
        
		initializeCustomer();
        navigator.geolocation.watchPosition(
                success_handler_update, 
                error_handler, 
                {enableHighAccuracy:true, maximumAge:60000,timeout:10000,frequency:3000});

    }

    function success_handler_update(position) {
		//alert("success");
        markerCustomer.setMap(null);
    	latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        accuracy = position.coords.accuracy;
        //alert("success:"+latitude+":"+longitude);
		geocoder = new google.maps.Geocoder();
	   	latlng= new google.maps.LatLng(latitude, longitude);
		if(document.getElementById("dragend").value=='false'){
			map.setCenter(latlng);
		   	}
		}

    function bookJobCall(latitude,longitude) {
 
      if (window.XMLHttpRequest)
  	{
  		xmlhttp = new XMLHttpRequest();
  	} else {
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
  	var url = 'CustomerMobileAction?event=openRequest';
  	if(latitude!=null && latitude!=""){
  		url=url+'&latitude='+latitude;
  	}
  	if(longitude!=null && longitude!=""){
  		url=url+'&longitude='+longitude;
  	}
  
  window.open(url,'_self');  
  
    }
    function latLngHandler(position){
    	 latitude = position.coords.latitude;
         longitude = position.coords.longitude;
         if (!latitude || !longitude) {
             document.getElementById("status").innerHTML = "HTML5 Geolocation supported, but location data is currently unavailable.";
             return;
         }
         var latlng= new google.maps.LatLng(latitude, longitude);
         return latlng;
    }
   
    var markerCustomer;
	function initializeCustomer(){
		 var blueDot="http://maps.google.com/mapfiles/kml/paddle/grn-circle-lv.png";
		geocoder = new google.maps.Geocoder();
	   	latlng= new google.maps.LatLng(latitude, longitude);
	   	var options = {
				zoom: 18,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				draggableCursor: 'crosshair'
		};

		map = new google.maps.Map(document.getElementById("placeholder"), options);
		  google.maps.event.addListener(map, 'dragend', function() {
			  dragend();
			  var c=map.getCenter();
			  latitude=c.lat();
			  longitude=c.lng(); 
				latlng= new google.maps.LatLng(latitude, longitude);
			 geocoder = new google.maps.Geocoder();
			  $('#dragLati').val(latitude);
			  $('#dragLongi').val(longitude);
			  
		  var point = new google.maps.LatLng(latitude, longitude);
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({latLng: point}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                          if (results[0]) {
                                  var arrAddress=  results[0].address_components;
                                 
                                  var streetnum="";
                                  var route="";
                                  var itemCountry="";
                                  var itemPc="";
                                  $.each(arrAddress, function (i, address_component) {
                                          if (address_component.types[0] == "street_number"){
                                              /*     $("#sadd1Dash").val(address_component.long_name); */
                                                  streetnum = address_component.long_name;
                                          }
                                          if (address_component.types[0] == "locality"){
                                              //    $("#scityDash").val(address_component.long_name);
                                             
                                          }
                                          if (address_component.types[0] == "route"){ 
                                                  route = address_component.long_name;
                                          }
                                          if (address_component.types[0] == "country"){ 
                                                  itemCountry = address_component.long_name;
                                          }
                                          if (address_component.types[0] == "postal_code"){ 
                                                  itemPc = address_component.long_name;
                                                //  $("#szipDash").val(address_component.long_name);
                                          }
                                         document.getElementById("addressCustomerTD").innerHTML=results[0].formatted_address;
                                 		document.getElementById("addressDragged").value=results[0].formatted_address;

                                  });
                          }
                  }
          });
		  });
 		new LongClick(map, 800);
		var markerCustomer;
	 	var externalImage = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
 		var driverImage = "images/greenCar.png";
		//GEOCODER
		geocoder = new google.maps.Geocoder();
		markerCustomer = new google.maps.Marker({
			position : latlng,
			map : map,
			icon : externalImage
		});
	
		map.setCenter(latlng);
		updateDisplay();
		$("#customerImage1").show();
		$("#addressCustomer").show();
	} 

	

     function updateDisplay() {
		//	markerCustomer.setMap(null);
    	 if(document.getElementById("switches").value=="1"){
 			markerCustomer.setMap(null);
 	 }
    	 latlng= new google.maps.LatLng(latitude, longitude);
    	var externalImage = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
    	var driverImage = "images/greenCar.png";
		//GEOCODER
		geocoder = new google.maps.Geocoder();
		markerCustomer = new google.maps.Marker({
			position : latlng,
			map : map,
			icon : externalImage
		});

		if(document.getElementById("driverLati").value!=""){
	    	latlng1= new google.maps.LatLng(document.getElementById("driverLati").value, document.getElementById("driverLongi").value);
			markerDriver = new google.maps.Marker({
				position : latlng1,
				map : map,
				icon : driverImage
			});
		}
		$("#customerImage1").show();
		$("#addressCustomer").show();

	}

	function error_handler(error) {
		var locationError = '';

		switch (error.code) {
		case 0:
			locationError = "There was an error while retrieving your location: "
					+ error.message;
			break;
		case 1:
			locationError = "The user prevented this page from retrieving a location.";
			break;
		case 2:
			locationError = "The browser was unable to determine your location: "
					+ error.message;
			break;
		case 3:
			locationError = "The browser timed out before retrieving the location.";
			break;
		}
		document.getElementById("status").innerHTML = locationError;
		document.getElementById("status").style.color = "#D03C02";
	}
	function dragend(){
		document.getElementById("dragend").value = "true";
		$("#focusPlace").show();

		//cancel();
	}
	function focusPlaceFunction(){
		document.getElementById("dragend").value="false";
		loadLocation();
		document.getElementById("addressCustomerTD").innerHTML="Book A Job";
		document.getElementById("dragLati").value="";
		document.getElementById("dragLongi").value="";
		document.getElementById("addressDragged").value="";
		$("#focusPlace").hide();
	}
</script>
<img id="customerImage1" style="position: absolute;"
		src="css/images/client/customer.png"></img>
		<img id="focusPlace" style="display:none;position: absolute;width:5%;height:5%" 
		src="images/Dashboard/location.jpg" onclick="focusPlaceFunction()"></img>
		</form>
</body>
</html>