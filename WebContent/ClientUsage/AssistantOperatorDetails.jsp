<!DOCTYPE html >
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.tdsBO.VoucherBO"%>
<%@page import="com.tds.tdsBO.AssistantOperatorBo"%>
<%@page import="com.tds.tdsBO.ClientAuthenticationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<title>Assistant Operator Details</title>
	<link href='http://fonts.googleapis.com/css?family=Croissant+One' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="jqueryUI/layout-default-latest.css" />
	<!-- <link rel="stylesheet" type="text/css" href="jqueryUI/jquery.ui.base.css" />
	<link rel="stylesheet" type="text/css" href="jqueryUI/jquery.ui.theme.css" /> -->
		<link rel="stylesheet" type="text/css" href="http://layout.jquery-dev.net/lib/css/themes/base/jquery.ui.all.css" />
	<!-- REQUIRED scripts for layout widget -->
	<script type="text/javascript" src="jqueryUI/jquery-latest.js"></script>
	<script type="text/javascript" src="jqueryUI/jquery-ui-latest.js"></script>
	<script type="text/javascript" src="jqueryUI/jquery.layout-latest.js"></script>
	<script type="text/javascript" src="jqueryUI/jquery.layout.resizePaneAccordions-latest.js"></script>
    <script type="text/javascript" src="jqueryUI/themeswitchertool.js"></script> 
	<script type="text/javascript" src="jqueryUI/debug.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places,geocode&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I"></script>	<script type="text/javascript" src="js/mainHTML.js"></script>
	<script type="text/javascript" src="js/mainHTML.js"></script>
	<script type="text/javascript" src="js/AssistantOperator.js"></script>
	<link type="text/css"  href="css/AssistantOperator.css" rel="stylesheet" />
	<script type="text/javascript">
	<%		
		AssistantOperatorBo  clientBean = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
	    ArrayList<VoucherBO>  voucherBo = (ArrayList<VoucherBO>)request.getAttribute("vouchers");
	%>
	function enableOrDisbaleTabs(){
		<%if(clientBean!=null){ %>
		$("#tabs").tabs({
			enabled: [1,2,3,4,5]
		});
	<%}else{%>
	$("#tabs").tabs({
		disabled: [1,2,3,4,5]
	});
	<%}%>
	}
	  $(document).ready ( function(){
	  	<%if(voucherBo!=null && voucherBo.size()>0){%>
		document.getElementById("compCode").value=<%=voucherBo.get(0).getVassoccode()%>;
		var table = document.getElementById("vouchersForAsst");
		var rowCount = table.rows.length;
		var row = table.insertRow(rowCount);
		var cell1 = row.insertCell(0);

		var element1 = document.createElement("select");
		element1.name = "voucherNum";
		element1.id = "voucherNum";
		var theOption = document.createElement("option");
		theOption.text = "Select";
		theOption.value = "";
		element1.options.add(theOption);
		<%for(int i=0;i<voucherBo.size();i++){%>
			var theOption = document.createElement("option");
			theOption.text = "<%=voucherBo.get(i).getVno()%>";
			theOption.value = "<%=voucherBo.get(i).getVno()%>";
			element1.onchange=function(){setCompCode(<%=voucherBo.get(i).getVassoccode()%>)};
			element1.options.add(theOption);
		<%}%>
		cell1.appendChild(element1);
	  	<%} else {%>
			var table = document.getElementById("vouchersForAsst");
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var cell1 = row.insertCell(0);
			var element = document.createElement("hidden");
			element.name = "voucherNum";
			element.id = "voucherNum";
			element.value="";
			cell1.appendChild(element);

	  	<%}%>
	  });
	  function setCompCode(assoccode){
			document.getElementById("compCode").value=assoccode;
			document.getElementById("payType").value="VC";
	  }
	</script>
</head>
<body >

<div class="ui-layout-north ui-widget-content" style="display: none;" align="right">
<span id="userName" style="font-family: 'Croissant One', cursive;"><%=request.getAttribute("userName")!=null?(String)request.getAttribute("userName"):"" %></span>
<button id="settings" style="background-image: url('images/Dashboard/Settings.png') ;background-repeat: no-repeat;"  onclick="showMenu()"></button>	
</div>
<div class="ui-layout-south ui-widget-content ui-state-error" style="display: none;height:10px"><div id="errorValue"><center><%=request.getAttribute("error")!=null?request.getAttribute("error"):"Please Login And Continue !!!" %> </center></div></div><div class="ui-layout-center" style="display: none;"> 
	<h3 class="ui-widget-header"><center>Assistant Operator Details</center></h3>
	<div class="ui-layout-content ui-widget-content">
		<div id="tabs" class="tabs-bottom">
<ul>
<li><a href="#tabs-1">Login</a></li>
<li><a href="#tabs-2">Create A Job</a></li>
<li><a href="#tabs-3" 	onclick="jobDetails1()">Job Details</a></li>
<li><a href="#tabs-4">History & logs</a></li>
<li><a href="#tabs-5">Reservations</a></li>
<li><a href="#tabs-6">Vouchers</a></li>
<li><a href="#tabs-7">Register</a></li>

</ul>
<div class="tabs-spacer"></div>
<div id="tabs-1">
<%if(request.getAttribute("status")==null||(String)request.getAttribute("status")=="Logout"){ %>
<form action="AssistantOperator?event=assistantLogin" id="createAccount"method="post"> 
		<label for="name">Name:</label> 
		<input type="text" name="userId" id="userId" required placeholder="Enter the UserName" onkeypress=" return checkSubmit(event)"/>
		<label for="password">Password:</label> 
		<input type="password" name="password" id="password" required placeholder="Enter the Password" onkeypress="return checkSubmit(event)"/>
		<input type="button" value="Login" onClick="createAccountFn('li')"/>
			</form>
	<%}else{%>
	<%		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
 %>
	<form action="AssistantOperator?event=assistantLogin&logout=logout" id="createAccount"method="post"> 
	<table><tr>
	<th>User&nbsp;Name</th><th>Phone&nbsp;Number</th><th>Email</th>
	</tr>
	<tr><td><%=clientBO.getName() %></td><td><%=clientBO.getPhNo() %></td><td><%=clientBO.getEmail() %></td></tr>
	</table>
	<a href="AssistantOperator?event=assistantLogin&logout=logout" >Logout</a></form>
	<%} %>
	</div>
	<div id="tabs-2">
		<p><jsp:include page="/ClientUsage/createAJobForAssistant.jsp" /> </p>
 	</div>
 	<div id="tabs-3" >
	<table id="customerTable" ></table>
 	</div>
 	<div id="tabs-4" >
	<table><tr><td><input class="datePicker" type="text" style="border: 1px solid #000000;" name="startDate" id="startDate" placeholder="Enter the From-Date!"  value="" required="required"/></td><td>
	<input class="datePicker" type="text" style="border: 1px solid #000000;" name="endDate" id="endDate" placeholder="Enter the To-Date!"  value="" required="required"/></td><td>
	<input type="text" name="searchForHistory" id="searchForHistory" value="" placeholder="Enter the value !"/></td><td>
	<input type="button" name="searchHistory" id="searchLogsHistory" value="Search" onClick="jobHistory()"/></td><tr></table>
	<table id="historyTable" ></table>
 	
 	</div>
 	
 	<div id="tabs-5" >
 	<table><tr><td><input class="datePicker" type="text" style="border: 1px solid #000000;" name="fDate" id="fDate" placeholder="Enter the From-Date"  value="" required="required"/></td><td>
	<input class="datePicker" type="text" style="border: 1px solid #000000;" name="tDate" id="tDate" placeholder="Enter the To-Date !"  value="" required="required"/></td><td>
	<input type="text" name="searchForReserve" id ="searchForReserve" value ="" placeholder="Enter the value !"/></td><td>
	<input type="button" name="searchReservations" id="searchReservations" value="Search" onClick="jobReservations()"/></td></tr></table>
	
	<table id="reservationsTbl" ></table>
 	</div>
 	
 	<div id="tabs-6">
 	<table><tr>
 		<td><input type="text" name="fDateForVoucher" id="fDateForVoucher" value ="" class="datePicker" placeholder="Enter Start Date"></td>
 		<td><input type="text" name="tDateForVoucher" id="tDateForVoucher" value ="" class="datePicker"  placeholder="Enter To Date"></td>
 		<td><input type="button" name ="submitSearch" id ="submitSearch" value="search" onClick="submitSearchForVouchers()"></td></tr></table>
 		<table id="voucherDetailTb"></table>
 		<div align="right"><input type="button" name="verifyVouchers" id="verifyVouchers"   value="Verify" onClick="verifySelectedVouchers()"/></div>
 	</div>
 	<div id="tabs-7">
 		<h6>Register the Following Details</h6>
 	<table style="width:100%;height:100%">
 	    <tr><td>Name*</td><td><input type="text" required="required" placeholder=" Enter the Name" name="regName" id="regName" value=""/></td>
 	   <td>UserId*</td><td><input type="text"  required="required" placeholder=" Enter the UserId" name="regUserId" id="regUserId" value=""/></td>
 	 <td>Password*</td><td><input type="password"  required="required" placeholder=" Enter the password" name="regPass" id="regPass" value=""/></td></tr>
 	 	<tr><td>E-mail ID*</td><td><input type="email"   required="required" placeholder=" Enter the E-Mail" name="regEmail"  id="regEmail" value=""/></td>
 	 	<td>Address*</td><td><input type="text"  required="required" placeholder=" Enter the Address" name="regAddress" id="regAddress" class="ui-autocomplete-input" onblur="appendAddress()" value=""/></td>
 	 	<td>City*</td><td><input type="text"  required="required" placeholder=" Enter the City" name="regCity" id="regCity" value=""/></td></tr>
 	 	<tr><td>Zip Code*</td><td><input type="number"  required="required" placeholder=" Enter the Zip" name="regZip" id="regZip" value=""/></td>
 	 	<td>Ph No*</td><td><input type="number"  required="required" placeholder=" Enter the Ph No !" name="regPhNo" id="regPhNo" value=""/></td></tr>
 	 	<td>Time Zone</td><td><select id="timeZone" name="timeZone" ><option value="US/Pacific">Pacific</option><option value="US/Eastern">Eastern</option></select></td></tr>
 	 	<tr><td colspan="6"><input type="button"  required="required" value="Register" onclick="createUser()"/></td></tr>
 	 	
 	</table>
 	</div>
</div>
</div>
</div>

<div class="ui-layout-west" style="display: none;">
	<div id="accordion1" class="basic">
			<h3><a href="#">Show Map </a> </h3>
			<div id="map_canvas"  ></div>
			<h3><a href="#">Finance</a></h3>
			    <div>Yet to be Designed !!!</div>
	</div>
</div>
<!-- 	<div  id="loading" class="jqmWindow" style="display:none"><br/><br/><br/><img src="images/loading.gif" ></img></div>
 -->
<ul id="menu" style="display:none">
<li class="ui-state-disabled"><a href="#">Settings</a></li>
<li><a href="#" onClick="removeUITheme(); myLayout.resizeAll()">RemoveTheme</a></li>

<li ><a href="#" >ThemeSwitcher</a>
<ul>
<li ><a href="#" id="themeSwitch"></a></li>
</ul></li>
<li><a href="control">GetACab</a></li>
<li><a href="AssistantOperator?event=assistantLogin&logout=logout" >Logout</a></li>
</ul>
<div id="dialog-confirm"  class="loading" title="Loading Please Wait !!">
  <p align="center"><img src="images/loading.gif" ></img></p>
</div>
<input type="hidden" name="defaultState" id="defaultState" value=""/>
		<input type="hidden" name="defaultCountry" id="defaultCountry" value=""/>
		<input type="hidden" name="tempAdd" id="tempAdd" value=""/>
</body>
</html> 