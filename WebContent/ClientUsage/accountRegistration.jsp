<!DOCTYPE html>

<%@page import="com.tds.tdsBO.CustomerMobileBO"%>
<%@page import="java.util.ArrayList"%>
<html style="height:100%;width:100%">
<head>
<title>User Account Registration</title>



<style>


body
{
    font: 12px 'Lucida Sans Unicode', 'Trebuchet MS', Arial, Helvetica;    
    margin: 0;
    background-color: #d9dee2;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebeef2), to(#d9dee2));
    background-image: -webkit-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -moz-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -ms-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -o-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: linear-gradient(top, #ebeef2, #d9dee2);    
}

/*--------------------*/

#login
{
    background-color: #fff;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));
    background-image: -webkit-linear-gradient(top, #fff, #eee);
    background-image: -moz-linear-gradient(top, #fff, #eee);
    background-image: -ms-linear-gradient(top, #fff, #eee);
    background-image: -o-linear-gradient(top, #fff, #eee);
    background-image: linear-gradient(top, #fff, #eee);  
    width: 98%;
   /*  margin: -150px 0 0 -230px; */
    padding: 1%;
    /* left:23%; */
    z-index: 0;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;  
    -webkit-box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),
          0 1px 1px rgba(0, 0, 0, .2),
          0 3px 0 #fff,
          0 4px 0 rgba(0, 0, 0, .2),
          0 6px 0 #fff,  
          0 7px 0 rgba(0, 0, 0, .2);
    -moz-box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),  
          1px 1px   0 rgba(0,   0,   0,   .1),
          3px 3px   0 rgba(255, 255, 255, 1),
          4px 4px   0 rgba(0,   0,   0,   .1),
          6px 6px   0 rgba(255, 255, 255, 1),  
          7px 7px   0 rgba(0,   0,   0,   .1);
    box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),  
          0 1px 1px rgba(0, 0, 0, .2),
          0 3px 0 #fff,
          0 4px 0 rgba(0, 0, 0, .2),
          0 6px 0 #fff,  
          0 7px 0 rgba(0, 0, 0, .2);
}

#login:before
{
    content: '';
    z-index: -1;
    border: 1px dashed #ccc;
    top: 5px;
    bottom: 5px;
    left: 5px;
    right: 5px;
    -moz-box-shadow: 0 0 0 1px #fff;
    -webkit-box-shadow: 0 0 0 1px #fff;
    box-shadow: 0 0 0 1px #fff;
}

/*--------------------*/

h1
{
    text-shadow: 0 1px 0 rgba(255, 255, 255, .7), 0px 2px 0 rgba(0, 0, 0, .5);
    text-transform: uppercase;
    text-align: center;
    color: #666;
    margin: 0 0 30px 0;ize: 40px
    letter-spacing: 4px;
    font: normal 26px/1 Verdana, Helvetica;
}

h1:after, h1:before
{
    background-color: #777;
    content: "";
    height: 1px;
    top: 15px;
    width: 60%;   
}

h1:after
{ 
    background-image: -webkit-gradient(linear, left top, right top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(left, #777, #fff);
    background-image: -moz-linear-gradient(left, #777, #fff);
    background-image: -ms-linear-gradient(left, #777, #fff);
    background-image: -o-linear-gradient(left, #777, #fff);
    background-image: linear-gradient(left, #777, #fff);      
    right: 0;
}

h1:before
{
    background-image: -webkit-gradient(linear, right top, left top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(right, #777, #fff);
    background-image: -moz-linear-gradient(right, #777, #fff);
    background-image: -ms-linear-gradient(right, #777, #fff);
    background-image: -o-linear-gradient(right, #777, #fff);
    background-image: linear-gradient(right, #777, #fff);
    left: 0;
}

/*--------------------*/

fieldset
{
    border: 0;
    padding: 0;
    margin: 0;
}

/*--------------------*/

#inputs input
{
	 padding: 20px 0px 0px 0px;
    margin: 0 0 10px 0;
    width: 80%; /* 353 + 2 + 45 = 400 */
    border: 1px solid #ccc;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 5px;
    -moz-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
    -webkit-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
    box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
}

#username
{
    background-position: 5px -8px !important;
}

#password
{
    background-position: 5px -58px !important;
}

#inputs input:focus
{
    background-color: #fff;
    border-color: #e8c291;
    outline: none;
    -moz-box-shadow: 0 0 0 1px #e8c291 inset;
    -webkit-box-shadow: 0 0 0 1px #e8c291 inset;
    box-shadow: 0 0 0 1px #e8c291 inset;
}

/*--------------------*/

#submit
{		
    background-color: #ffb94b;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#fddb6f), to(#ffb94b));
    background-image: -webkit-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -moz-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -ms-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -o-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: linear-gradient(top, #fddb6f, #ffb94b);
    
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    
    text-shadow: 0 1px 0 rgba(255,255,255,0.5);
    
     -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
     -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
     box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;    
    
    border-width: 1px;
    border-style: solid;
    border-color: #d69e31 #e3a037 #d5982d #e3a037;

    float: left;
    height: 75px;
    padding: 0;
    width: 50%;
    cursor: pointer;
    font: bold 15px Arial, Helvetica;
    color: #8f5a0a;
}

#submit:hover,#submit:focus
{		
    background-color: #fddb6f;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ffb94b), to(#fddb6f));
    background-image: -webkit-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -moz-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -ms-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -o-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: linear-gradient(top, #ffb94b, #fddb6f);
}	

#submit:active
{		
    outline: none;
   
     -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
     -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
     box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;		
}

#submit::-moz-focus-inner
{
  border: none;
}

#actions a
{
    color: #3151A2;    
    float: right;
    line-height: 55px;
    margin-left: 10px;
}

/*--------------------*/

#back
{
    display: block;
    text-align: center;
    top: 60px;
    color: #999;
}



</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
function checkUserId(){
	var userId=document.getElementById("userId").value;
	if(userId.length>6 || userId.length<6 ){
		document.getElementById("userId").value="";
		document.getElementById("userId").focus();
	}else{
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'CustomerMobileAction?event=userIdCheck&responseType=HTML5&submit=YES&userId='+userId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	
	alert(text);
	if(text==1){
		document.getElementById("userId").value="";
		document.getElementById("userId").style.box-shadow('0 1px 1px #CCCCCC inset, 0 1px 0 #FFFFFF');
		document.getElementById("userId").focus();
		alert("UserId already Exists");
	}
	}
	
}
function checkPassword(){
	var password=document.getElementById("password").value;
	var rePassword=document.getElementById("rePassword").value;
	if(password!="" && rePassword!=""){
		if(password==rePassword){
		}else{
			document.getElementById("password").value="";
			document.getElementById("rePassword").value="";
			document.getElementById("password").focus();
			document.getElementById("errorText").value="Password not matching";
			alert("password not matching");
		}
	}
}
/* function numbersOnly(){
	alert("calling");
	$("#userId").keypress(function(e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which<48 || e.which>57)) {
			//display error message
			return false;
		}
	});
} */
$(document).ready(function() {
	if(document.getElementById("os").value=="G"){
		Android.homePage("");
	}
	$("#userId").keypress(function(e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which<48 || e.which>57)) {
			//display error message
			return false;
		}
	});
});
</script>
<script src="js/jquery.js"></script>
<script type="text/javascript">
$('#bodyPage').css('height',$(window).height+'px');
$('#login').css('height',$(window).height+'px');
$("input").css('height',$(window).height()/6+'px');
$("input").css('padding',$(window).height()/10+'px');

</script>
</head>
<input id="errorText" style="width: 80%;display:none; font-size: xx-large;font-weight: bolder;color: red;" type="image" value='<%=request.getAttribute("error")==null?"":request.getAttribute("error")%>'/>
 <% CustomerMobileBO  accountDetail=new CustomerMobileBO();%>
 <%if(request.getAttribute("error")!=null) {%>
	<div id="error" style="margin-left: 10%;width: 100%;"><input  style="width: 80%;font-size: xx-large;font-weight: bolder;color: red;" type="image" value='<%=request.getAttribute("error")==null?"":request.getAttribute("error")%>'/></div>
    <%} %>
     <%if(request.getAttribute("accountDetail")!=null) {
    	accountDetail=(CustomerMobileBO)request.getAttribute("accountDetail");
    } %>
<body style="width:100%;height :95%;">
<form id="login" action="CustomerMobileAction?event=clientAccountRegistration" method="post" style="width:98%;height :100%;">
<input type="hidden" name="event" value="clientAccountRegistration"/>
	<input type="hidden" name="os" id="os" value="<%=request.getParameter("os")%>" /> 
        <input id="responseType" name="responseType" type="hidden" value="HTML5">
    <h1 style="width: 95%;font-size:large;font-weight: bold;" align="center">Account Registration</h1>
    <table id="tableRegistration" style="height:90%;width:100%">
    <tr><td><fieldset id="inputs" >
               <img src="css/images/client/user.png"/>
        <input id="userName" name="userName" type="text" style="font-size: large;height:12.5%;width:80%;" value="<%=accountDetail.getName()==null?"":accountDetail.getName() %>" placeholder="Enter your Name" autofocus required>   
          </fieldset></td></tr><tr><td><fieldset id="inputs"  >
           <img  src="css/images/client/userId.jpg"/>
        <input id="userId" name="userId" type="number" min="100000" max="999999" style="font-size: large;height:12.5%;width:80%;" value="<%=accountDetail.getUserId()==null ?"":accountDetail.getUserId()%>" placeholder="Enter your User id" required>
      </fieldset></td></tr><tr><td><fieldset id="inputs" >
	           <img  src="css/images/client/coins.png"/>
        <select id="paymentType" name="paymentType"  style="font-size: large;height:12.5%;width:80%" value="<%=accountDetail.getPaymentType()==null?"":accountDetail.getPaymentType() %>" required>
        <option <%=accountDetail.getPaymentType().equals("voucher")?"selected":"" %> value="voucher">Voucher</option>
		<option <%=accountDetail.getPaymentType().equals("cash")?"selected":"" %> value="cash" >Cash</option>
		<option <%=accountDetail.getPaymentType().equals("creditcard")?"selected":"" %> value="creditcard" >Credit Card</option> 
		                       	</select>
      </fieldset></td></tr><tr><td><fieldset id="inputs"  >
           <img  src="css/images/client/creditCard.png"/>
        <input id="cardNumber" name="cardNumber" type="text" style="font-size: large;height:12.5%;width:80%" value="<%=accountDetail.getCardNumber()==null?"":accountDetail.getCardNumber() %>"placeholder="Enter Card Number" required>
      </fieldset></td></tr><tr><td><fieldset id="inputs"  >
           <img  src="css/images/client/voucherNumber.png"/>
        <input id="voucherNumber" name="voucherNumber" type="text" style="font-size: large;height:12.5%;width:80%;" value="<%=accountDetail.getVoucherNumber()==null?"":accountDetail.getVoucherNumber() %>" placeholder="Enter your Voucher Number" required>
      </fieldset></td></tr><tr><td><fieldset id="inputs" >
           <img  src="css/images/client/expiryDate.png"/>

        <input id="cardExpiryDate" name="cardExpiryDate" type="text" style="font-size: large;height:12.5%;width:80%;" value="<%=accountDetail.getCardExpiryDate()==null?"":accountDetail.getCardExpiryDate() %>" placeholder="Enter your Card Expiry Date" required>
      </fieldset></td></tr><%-- <tr><td><fieldset id="inputs" >
                 <img  src="css/images/client/expiryDate.png"/>
        <input id="voucherExpiryDate" name="voucherExpiryDate" type="text" style="font-size: large;height:12.5%;width:80%;" value="<%=accountDetail.getVoucherExpiryDate()==null?"":accountDetail.getVoucherExpiryDate() %>" placeholder="Enter your Voucher Expiry Date" required>
        <input id="mode" name="mode" type="hidden" value="1">
        <input id="responseType" name="responseType" type="hidden" value="HTML5">
              </fieldset></td></tr> --%><tr><td>
    <fieldset id="actions" style="width: 98%;" >
        <input type="submit" id="submit" name="submit" style="width: 98%;height:12.5%;font-size: large;font-weight: bold;" value="Register">
    </fieldset></td></tr></table>
</form>
</body>
</html>