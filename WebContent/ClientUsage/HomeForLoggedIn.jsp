<!DOCTYPE html>

<%@page import="com.common.util.TDSProperties"%><html>
<head>
<!-- BEGIN Demo CSS - You can safely ignore this part -->
<link rel="stylesheet" href="css/client/demo.css">
<link rel="stylesheet" href="css/client/customPopup.css">
<!-- END Demo CSS -->

<!-- BEGIN Navigation bar CSS - This is where the magic happens -->
<link rel="stylesheet" href="css/client/navbar.css">
<!-- END Navigation bar CSS -->

<!-- BEGIN JavaScript -->
<script type="text/javascript"
	src="js/jquery.js"></script>
	<script type="text/javascript"
	src="js/custom.js"></script>
<style type="text/css">
#addressCustomer {
	-moz-border-radius: 8px 8px 8px 8px;
	border-radius: 8px;
	background-color: wheat;		
	
	font-size: large;
	font-weight: bolder;
	font-family: fantasy;
	font-size: large;
	
	border-color: buttonshadow;
	margin-left: auto;
	margin-right: auto;
	}
	#addressCustomer:hover{
	background-color:khaki;
	cursor: pointer;
	}
</style>

<script type="text/javascript"> 

$(document).ready(function(){
	$('#latitude').val("");
	$('#longitude').val("");
	$("#address").val("");
	$(document).css('height',$(window).height()+'px');
	$('#placeholder').css('height',$(window).height()/1.177865613+'px');
	$('#placeholder').css('width',$(window).width()-10+'px');
	$('#focusPlace').css('margin-top',$("#placeholder").height()/1.1-$('#placeholder').position().top/2+'px');
	$('#focusPlace').css('margin-left',($("#placeholder").width()/1.02)-24+'px');
	$('#customerImage1').css('margin-top',$("#placeholder").height()/2-$('#placeholder').position().top+'px');
	$('#customerImage1').css('margin-left',($("#placeholder").width()/2)-24+'px');
	$('#addressCustomer').css('height',$(window).height()/15.775+'px');
	$('#addressCustomer').css('margin-top',$(window).height()/1.502380952+'px');
	$('#addressCustomer').css('width',$(window).width()-10+'px');
	$('#menuTR').css('margin-top',$(window).height()/1.23+'px');
	$('#menuUL').css('margin-top','-'+$(window).height()/2.2+'px');

	// Requried: Navigation bar drop-down
	$("nav ul li").hover(function() {
		$(this).addClass("active");
		$(this).find("ul").show().animate({opacity: 1}, 400);
		},function() {
		$(this).find("ul").hide().animate({opacity: 0}, 200);
		$(this).removeClass("active");
	});
	
	// Requried: Addtional styling elements
	$('nav ul li ul li:first-child').prepend('<li class="arrow"></li>');
	$('nav ul li:first-child').addClass('first');
	$('nav ul li:last-child').addClass('last');
	$('nav ul li ul').parent().append('<span class="dropdown"></span>').addClass('drop');
	//companyCode();
	Init();
});

</script>
<script type="text/javascript">
	var mins, secs, TimerRunning, TimerID, TimerID1, thisJobIsRunning, thisJobIsRunningForDriver;
	TimerRunning = false, i = 1;

	function Init()//call the Init function when u need to start the timer
	{
		mins = 1;
		secs = 0;
		StopTimer();
	}

	function StopTimer() {
		if (TimerRunning)
		clearTimeout(TimerID);
		TimerRunning = false;
		secs = 10;
		StartTimer();
	}
	function StartTimer() {

		TimerRunning = true;
		TimerID = self.setTimeout("StartTimer()", 1000);
		if (secs == 0) {
			StopTimer();
			if(document.getElementById("dragend").value=='false'){
			jobLocation();
			driverLocation();
			updateDisplay();
			}
		}
		thisJobIsRunning = false;
		thisJobIsRunningForDriver = false;
		secs--;

	}
	 function bookJob(){
		 var latitude="";
	    	var longitude="";
	    	var address="";
			if(document.getElementById("dragend").value=="true"){	
				latitude=document.getElementById("dragLati").value;
				longitude=document.getElementById("dragLongi").value;
	    		address=document.getElementById("addressDragged").value;
			}else if(document.getElementById("dragend").value=="false"){
				address=document.getElementById("addressOnLoad").value;	
				latitude=document.getElementById("latitudeOnLoad").value;
				longitude=document.getElementById("longitudeOnLoad").value;
			}
	    	document.getElementById("latitude").value=latitude;
	    	document.getElementById("longitude").value=longitude;
	    	document.getElementById("address").value=address;
	    	$('#addressDragged').val("");
	    	document.forms["home"].submit();
	    }
	function Pad(number) //pads the mins/secs with a 0 if its less than 10
	{
		if (number < 10)
			number = 0 + "" + number;
		return number;
	}
	function driverLocation(){
		var latitude="";
		var longitude="";
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'CustomerMobileAction?event=driverLocation';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"driverLocation\":"+text+"}";
		var jsonObj = JSON.parse(obj.toString());
		for(var i=0; i<jsonObj.driverLocation.length;i++){
			 latitude=jsonObj.driverLocation[i].getLatitude;
			 longitude=jsonObj.driverLocation[i].getLongitude;
		}
		if(latitude!=""){
			document.getElementById("driverLati").value=latitude;
			document.getElementById("driverLongi").value=longitude;
		}
		if(document.getElementById("dragend")=='false'){
		//loadLocation();
		updateDisplay();
		}
	}
	function logoutCustomer(){
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'CustomerMobileAction?event=clientLogin&responseType=HTML5&submit=YES';
		xmlhttp.open("GET", url, true);
		xmlhttp.send(null);
		
	}
	function jobLocation(){
		var latitude="";
		var longitude="";
		var status="";
		if (window.XMLHttpRequest)
		{
			xmlhttp = new XMLHttpRequest();
		} else {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var url = 'CustomerMobileAction?event=jobLocation';
		xmlhttp.open("GET", url, false);
		xmlhttp.send(null);
		var text = xmlhttp.responseText;
		var obj = "{\"jobLocation\":"+text+"}";
		var jsonObj = JSON.parse(obj.toString());
		for(var i=0; i<jsonObj.jobLocation.length;i++){
			 latitude=jsonObj.jobLocation[i].getLatitude;
			 longitude=jsonObj.jobLocation[i].getLongitude;
			 status=jsonObj.jobLocation[i].SA;
		}
	//	alert(latitude);
		//alert(longitude);
		var jobStatus=document.getElementById("jobStatus").value;
		if(latitude!=""){
			document.getElementById("jobLati").value=latitude;
			document.getElementById("jobLongi").value=longitude;
			document.getElementById("jobStatus").value=status;
		}
		if(status!=jobStatus){
			if(status=='40'){
				alert("your job allocated");
				document.getElementById("popuprel2").innerHTML="your job allocated";
				popupStatus();
			}else if(status=='43'){
				alert('Driver is Onroute');
				document.getElementById("popuprel2").innerHTML="Driver is Onroute";
				popupStatus();
			}else if(status=='47'){
				alert('Your trip is started');
				document.getElementById("popuprel2").innerHTML="Your trip is started";
				popupStatus();
			}else if(status='48'){
				alert('driver reported NoShow');
				document.getElementById("popuprel2").innerHTML="driver reported NoShow";	
				popupStatus();
			}else if(status=='25'){
				alert('couldnt find drivers');
				document.getElementById("popuprel2").innerHTML="couldnt find drivers";
				popupStatus();
			}else if (status=='30'){
				alert('your trip is broadcasted');
				document.getElementById("popuprel2").innerHTML="your trip is broadcasted";
				popupStatus();
			}
		}
		if(document.getElementById("dragend")=='false'){
		//loadLocation();
		updateDisplay();
		}
	}
	
	$(document).ready(function(){
	jobLocation();
	driverLocation();
	if(document.getElementById("os").value=="G"){
		Android.homePage("home");
	}
	});

</script>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>GetACab's Customer App</title>


<script  type="text/javascript" src="js/jquery.js"></script>
<script  type="text/javascript"
	src="js/client/jquery.cookie.js"></script>
<script src='<%= TDSProperties.getValue("googleMapV3") %>'
	type="text/javascript"></script>
</head>
<body onload="loadLocation(); " style="background-color: black;height:90%">
<form name="home" id="home" action="CustomerMobileAction?event=openRequest" method="post">
    	<input type="hidden" name="event" value="openRequest"/>
	    <input type="hidden" name="dragend" id="dragend" value="false" /> 
    	<input type="hidden" name="os" id="os" value="<%=request.getParameter("os")%>" /> 
		<input type="hidden" name="ccode" id="ccode" value="<%=request.getParameter("ccode")%>" /> 
		<input type="hidden" name="phoneNumber" id="phoneNumber" value="<%=request.getParameter("phoneNumber")%>" /> 
		<input type="hidden" id="driverLati" value="" /> 
		<input type="hidden" id="driverLongi" value="" />
		<input type="hidden" id="dragLati" value="" /> 
		<input type="hidden" id="dragLongi" value="" />
			<input type="hidden" id="jobLati" value="" /> 
		<input type="hidden" id="jobLongi" value="" />
		<input type="hidden" name="address" id="address" value="" /> 
		<input type="hidden" name="latitude" id="latitude" value="" /> 
		<input type="hidden" name="longitude" id="longitude" value="" />
		<input type="hidden" id="switches" value="" />
		<input type="hidden" id="addressDragged" value=""/>
		<input type="hidden" id="jobStatus" value=""/>
		<input type="hidden" id="addressOnLoad" value=""/>
		<input type="hidden" id="latitudeOnLoad" value=""/>
		<input type="hidden" id="longitudeOnLoad" value=""/>
		<input type="hidden" id="pageName" value="home"/>
	<table  style="width:100%;height:100%;position:absolute;margin-top:-0.5%" >
	<tr  style="width:50%;height:5%" align="center"><td align="center">
	<!-- <img id="companyLogo" src="css/logo.jpg" style="width:5%;height:70%"/> -->
	</td></tr>
	<tr id="addressCustomer"  align="center" style="width:100%;height:10%;position:absolute;background-color: wheat;z-index:100" onclick="bookJob()"><td id="addressCustomerTD" align="center" colspan="2">
	Book A Job
	</td></tr><tr style="width:99.8%;height:60%;position:absolute;"><td colspan="2" style="width:99.8%">	
						<div class="content" style="width: 100%;">
							<ul class="simple" style="display: none;">
								<li class="even"><strong><span id="status"></span>
								</strong></li>
							</ul>
				
                         <input type="hidden" name="addIPhone1" id="addIPhone1" value=""></input> 
								<input type="hidden" name="addIPhone1" id="addIPhone1" value=""></input>
							<div id="placeholder" style="position: absolute;width:99.8%">

								<i>Note: May take a few seconds to get the location.</i>
							</div>
							
					
					</div>
			</td></tr>
				<%if(session.getAttribute("clientUser")!=null ){ %>
			<tr id="menuTR" style="width:99.8%;height:10%;background-color: gray;position:absolute;margin-top:41%;border:1%;border-color:white;" align="center">
			<td colspan="2"  style="position:absolute;width:100%" bordercolor="white" align="center" >
			<div class="wrapper" style="width: 100%">
			<!-- BEGIN Dark navigation bar -->
			<nav class="dark">
				<ul class="clear" id="topNav">
					<li style="width: 33%;"><a href="#"><font
							style="font-size: large;">Home</font>
					</a>
						<ul id="menuUL" style="margin-left: 6%; width: 93%;">
							<li style="width: 33%;"><a
								href="CustomerMobileAction?event=tripSummary&responseType=HTML5&ccode=<%=request.getParameter("ccode")%>&os=<%=request.getParameter("os")%>"><font
									style="font-size: medium;">Reserved Trips</font>
							</a>
							<li style="width: 33%;"><a href="CustomerMobileAction?event=clientAccountRegistration&responseType=HTML5&ccode=<%=request.getParameter("ccode")%>&os=<%=request.getParameter("os")%>"><font
									style="font-size: medium;">Account Registration</font>
							</a></li>
							<li style="width: 33%;"><a
								href="CustomerMobileAction?event=tripHistory&responseType=HTML5&ccode=<%=request.getParameter("ccode")%>&os=<%=request.getParameter("os")%>"><font
									style="font-size: medium;">Trip History</font>
							</a>
							<li style="width: 33%;"><a
								href="CustomerMobileAction?event=clientAccountSummary&responseType=HTML5&ccode=<%=request.getParameter("ccode")%>&os=<%=request.getParameter("os")%>"><font
									style="font-size: medium;">Account Summary</font>
							</a>
							<li style="width: 33%;"><a
								href="CustomerMobileAction?event=favouriteAddresses&responseType=HTML5&ccode=<%=request.getParameter("ccode")%>&os=<%=request.getParameter("os")%>"><font
									style="font-size: medium;">Favourite Addresses</font>
							</a>
						</ul></li>
					<li style="width: 33%"><a
						href="CustomerMobileAction?event=openRequest&responseType=HTML5&ccode=<%=request.getParameter("ccode")%>&os=<%=request.getParameter("os")%>"><font
							style="font-size: large;">Book</font>
					</a>
					</li>
					<li style="width: 33.8%"><a
						href="CustomerMobileAction?event=clientLogin&responseType=HTML5&submit=YES&ccode=<%=request.getParameter("ccode")%>&os=<%=request.getParameter("os")%>"><font
							style="font-size: large;">Logout</font>
					</a>
					</li>

				</ul>
				
			</nav>
			</div>
					</td></tr>
		<%}else{ %>
			<tr id="menuTR" style="width:100%;height:10%;background-color: gray;position:absolute;margin-top:41%;border:1%;border-color:white;" align="center">
			<td colspan="2"  style="position:absolute;" bordercolor="white" align="center" >
			<div class="active" style="position:absolute;" align="center">
				<a href="javascript:loginPage()"   ><font style="font-size: large;" >Login</font></a>
					</div>
				<%} %>				
	</td></tr></table>
	<script src="js/jquery.js"></script>
        <script src="js/modernizr.js"></script>
		<script>
			(function($){
				
				//cache nav
				var nav = $("#topNav");
				
				//add indicator and hovers to submenu parents
				nav.find("li").each(function() {
					if ($(this).find("ul").length > 0) {
						$("<span>").text("").appendTo($(this).children(":first"));

						//show subnav on hover
						$(this).mouseenter(function() {
							$(this).find("ul").stop(true, true).slideDown();
						});
						
						//hide submenus on exit
						$(this).mouseleave(function() {
							$(this).find("ul").stop(true, true).slideUp();
						});
					}
				});
			})(jQuery);
		</script>

	<script type="text/javascript">
    
   
      var latitude;
      var longitude;
      var accuracy;
      var map;
  	var geocoder;
      function loadLocation() {
            navigator.geolocation.getCurrentPosition(
         		 
                        success_handler_first, 
                        error_handler, 
                        {enableHighAccuracy:true, maximumAge:60000,timeout:10000,frequency:3000});
            updateDisplay();
     /*   	if(navigator.geolocation) {
       		alert("in if");
               document.getElementById("status").innerHTML = "HTML5 Geolocation is supported in your browser.";
               document.getElementById("status").style.color = "#1ABC3C";
               alert("Before GeoLocation current location ");
               navigator.geolocation.getCurrentPosition(
            		 
                           success_handler_first, 
                           error_handler, 
                           {enableHighAccuracy:true, maximumAge:60000,timeout:10000,frequency:3000});
   					 //     updasteDisplay();
                   
           } else {
        	   alert("in else");
        	   navigator.geolocation.getCurrentPosition(
              		 
                       success_handler_first, 
                       error_handler, 
                       {enableHighAccuracy:true, maximumAge:60000,timeout:10000,frequency:3000});
           } */
      }


	  function LongClick(map, length) {
		    this.length_ = length;
		    var me = this;
		    me.map_ = map;
		    google.maps.event.addListener(map, 'mousedown', function(e) { me.onMouseDown_(e) });
		    google.maps.event.addListener(map, 'mouseup', function(e) { me.onMouseUp_(e) });
		  }
		  LongClick.prototype.onMouseUp_ = function(e) {
		    var now = +new Date;
		    if (now - this.down_ > this.length_) {
		      google.maps.event.trigger(this.map_, 'longpress', e);
		    }
		  };
		  LongClick.prototype.onMouseDown_ = function() {
		    this.down_ = +new Date;
		  };

    
    function success_handler_first(position) {
		//alert("success");
		//alert("in success handler first");
    	latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        accuracy = position.coords.accuracy;
        //alert("latitude-->"+latitude);
        $("#latitudeOnLoad").val(latitude);
        $("#longitudeOnLoad").val(longitude);
        var point = new google.maps.LatLng(latitude, longitude);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({latLng: point}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                                var arrAddress=  results[0].address_components;
                               
                                var streetnum="";
                                var route="";
                                var itemCountry="";
                                var itemPc="";
                                $.each(arrAddress, function (i, address_component) {
                                        if (address_component.types[0] == "street_number"){
                                            /*     $("#sadd1Dash").val(address_component.long_name); */
                                                streetnum = address_component.long_name;
                                        }
                                        if (address_component.types[0] == "locality"){
                                            //    $("#scityDash").val(address_component.long_name);
                                           
                                        }
                                        if (address_component.types[0] == "route"){ 
                                                route = address_component.long_name;
                                        }
                                        if (address_component.types[0] == "country"){ 
                                                itemCountry = address_component.long_name;
                                        }
                                        if (address_component.types[0] == "postal_code"){ 
                                                itemPc = address_component.long_name;
                                              //  $("#szipDash").val(address_component.long_name);
                                        }
                                       document.getElementById("addressCustomerTD").innerHTML=results[0].formatted_address;
                               		document.getElementById("addressOnLoad").value=results[0].formatted_address;
                                });
                        }
                }
        });
      
		initializeCustomer();
        navigator.geolocation.watchPosition(
                success_handler_update, 
                error_handler, 
                {enableHighAccuracy:true, maximumAge:60000,timeout:10000,frequency:3000});

    }

    function success_handler_update(position) {
		//alert("success");
        markerCustomer.setMap(null);
    	latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        accuracy = position.coords.accuracy;
        //alert("success:"+latitude+":"+longitude);
		geocoder = new google.maps.Geocoder();
	   	latlng= new google.maps.LatLng(latitude, longitude);
		if(document.getElementById("dragend").value=='false'){
			map.setCenter(latlng);
		   	}
		}

    function bookJobCall(latitude,longitude) {
 
      if (window.XMLHttpRequest)
  	{
  		xmlhttp = new XMLHttpRequest();
  	} else {
  		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
  	}
  	var url = 'CustomerMobileAction?event=openRequest';
  	if(latitude!=null && latitude!=""){
  		url=url+'&latitude='+latitude;
  	}
  	if(longitude!=null && longitude!=""){
  		url=url+'&longitude='+longitude;
  	}
  
  window.open(url,'_self');  
  
    }
    function latLngHandler(position){
    	 latitude = position.coords.latitude;
         longitude = position.coords.longitude;
         if (!latitude || !longitude) {
             document.getElementById("status").innerHTML = "HTML5 Geolocation supported, but location data is currently unavailable.";
             return;
         }
         var latlng= new google.maps.LatLng(latitude, longitude);
         return latlng;
    }
   
    var markerCustomer;
	function initializeCustomer(){
		 var blueDot="http://maps.google.com/mapfiles/kml/paddle/grn-circle-lv.png";
		geocoder = new google.maps.Geocoder();
	   	latlng= new google.maps.LatLng(latitude, longitude);
	   	var options = {
				zoom: 18,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				draggableCursor: 'crosshair'
		};

		map = new google.maps.Map(document.getElementById("placeholder"), options);
		  google.maps.event.addListener(map, 'dragend', function() {
			  dragend();
			  var c=map.getCenter();
			  latitude=c.lat();
			  longitude=c.lng(); 
				latlng= new google.maps.LatLng(latitude, longitude);
			 geocoder = new google.maps.Geocoder();
			  $('#dragLati').val(latitude);
			  $('#dragLongi').val(longitude);
			  
		  var point = new google.maps.LatLng(latitude, longitude);
          var geocoder = new google.maps.Geocoder();
          geocoder.geocode({latLng: point}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                          if (results[0]) {
                                  var arrAddress=  results[0].address_components;
                                 
                                  var streetnum="";
                                  var route="";
                                  var itemCountry="";
                                  var itemPc="";
                                  $.each(arrAddress, function (i, address_component) {
                                          if (address_component.types[0] == "street_number"){
                                              /*     $("#sadd1Dash").val(address_component.long_name); */
                                                  streetnum = address_component.long_name;
                                          }
                                          if (address_component.types[0] == "locality"){
                                              //    $("#scityDash").val(address_component.long_name);
                                             
                                          }
                                          if (address_component.types[0] == "route"){ 
                                                  route = address_component.long_name;
                                          }
                                          if (address_component.types[0] == "country"){ 
                                                  itemCountry = address_component.long_name;
                                          }
                                          if (address_component.types[0] == "postal_code"){ 
                                                  itemPc = address_component.long_name;
                                                //  $("#szipDash").val(address_component.long_name);
                                          }
                                         document.getElementById("addressCustomerTD").innerHTML=results[0].formatted_address;
                                 		document.getElementById("addressDragged").value=results[0].formatted_address;

                                  });
                          }
                  }
          });
		  });
 		new LongClick(map, 800);
		var markerCustomer;
	 	var externalImage = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
 		var driverImage = "images/greenCar.png";
		//GEOCODER
		geocoder = new google.maps.Geocoder();
		markerCustomer = new google.maps.Marker({
			position : latlng,
			map : map,
			icon : externalImage
		});
		map.setCenter(latlng);
		updateDisplay();
		$("#customerImage1").show();
		$("#addressCustomer").show();
		document.getElementById("switches").value="1";
	} 

	

	 function updateDisplay() {
		 if(document.getElementById("switches").value=="1"){
 			markerCustomer.setMap(null);
		 }
    	 latlng= new google.maps.LatLng(latitude, longitude);
    	 var externalImage = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
    	var driverImage = "images/greenCar.png";
 		var jobImage;
 		var status=document.getElementById("status").value;
 		if(status=='40' || status=='43' || status=='47' || status=='49'){
 			jobImage  = "images/Dashboard/jobAllocated.png";
 		}else{
 			jobImage="images/Dashboard/job.png";
 		}
		//GEOCODER
		geocoder = new google.maps.Geocoder();
		markerCustomer = new google.maps.Marker({
			position : latlng,
			map : map,
			icon : externalImage
			
		}); 
	if(document.getElementById("jobLati").value!=""){
		latlngJob= new google.maps.LatLng(document.getElementById("jobLati").value, document.getElementById("jobLongi").value);
		markerJob = new google.maps.Marker({
			position : latlngJob,
			map : map,
			icon : jobImage
		});
	}
		if(document.getElementById("driverLati").value!=""){
	    	latlng1= new google.maps.LatLng(document.getElementById("driverLati").value, document.getElementById("driverLongi").value);
			markerDriver = new google.maps.Marker({
				position : latlng1,
				map : map,
				icon : driverImage
			});
		}
		$("#customerImage1").show();
		$("#addressCustomer").show();
	}

	function error_handler(error) {
		var locationError = '';

		switch (error.code) {
		case 0:
			locationError = "There was an error while retrieving your location: "
					+ error.message;
			break;
		case 1:
			locationError = "The user prevented this page from retrieving a location.";
			break;
		case 2:
			locationError = "The browser was unable to determine your location: "
					+ error.message;
			break;
		case 3:
			locationError = "The browser timed out before retrieving the location.";
			break;
		}
		document.getElementById("status").innerHTML = locationError;
		document.getElementById("status").style.color = "#D03C02";
	}
	function dragend(){
		document.getElementById("dragend").value = "true";
		$("#focusPlace").show();	

		//cancel();
	}
	function focusPlaceFunction(){
		document.getElementById("dragend").value="false";
		loadLocation();
		document.getElementById("addressCustomerTD").innerHTML="Book A Job";
		document.getElementById("dragLati").value="";
		document.getElementById("dragLongi").value="";
		document.getElementById("addressDragged").value="";
		$("#focusPlace").hide();
	}
	function popupStatus(){
		$('#popuprel2').fadeIn();
		$('body').append('<div id="fade"></div>');
		/* $('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); */
		var popuptopmargin = ($('#popuprel2').height() + 10) / 2;
		var popupleftmargin = ($('#popuprel2').width() + 10) / 2;
		$('#popuprel2').css({
		'margin-top' : -popuptopmargin,
		'margin-left' : -popupleftmargin
		});
		$('#fade').click(function() {
		$('#fade , #popuprel , #popuprel2 , #popuprel3').fadeOut();
		return false;
		});
	}
</script>
<div class="popupbox2" id="popuprel2">

</div>
<img id="customerImage1" style="position: absolute;"
		src="css/images/client/customer.png"></img>
		<img id="focusPlace" style="display:none;position: absolute;width:5%;height:5%" 
		src="images/Dashboard/location.jpg" onclick="focusPlaceFunction()"></img>
		</form>
</body>
</html>