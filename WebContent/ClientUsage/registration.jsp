<!DOCTYPE html>

<html >
<head>
<title>User Registration</title>



<style>



body
{
    font: 12px 'Lucida Sans Unicode', 'Trebuchet MS', Arial, Helvetica;    
    margin: 0;
    background-color: #d9dee2;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebeef2), to(#d9dee2));
    background-image: -webkit-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -moz-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -ms-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -o-linear-gradient(top, #ebeef2, #d9dee2);
    background-imagstyle="height:100%;width:100%"e: linear-gradient(top, #ebeef2, #d9dee2);    
}

/*--------------------*/

#login
{
    background-color: #fff;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));
    background-image: -webkit-linear-gradient(top, #fff, #eee);
    background-image: -moz-linear-gradient(top, #fff, #eee);
    background-image: -ms-linear-gradient(top, #fff, #eee);
    background-image: -o-linear-gradient(top, #fff, #eee);
    background-image: linear-gradient(top, #fff, #eee);  
   /*  margin: -150px 0 0 -230px; */
    /* left:23%; */
    z-index: 0;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;  
    -webkit-box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),
          0 1px 1px rgba(0, 0, 0, .2),
          0 3px 0 #fff,
          0 4px 0 rgba(0, 0, 0, .2),
          0 6px 0 #fff,  
          0 7px 0 rgba(0, 0, 0, .2);
    -moz-box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),  
          1px 1px   0 rgba(0,   0,   0,   .1),
          3px 3px   0 rgba(255, 255, 255, 1),
          4px 4px   0 rgba(0,   0,   0,   .1),
          6px 6px   0 rgba(255, 255, 255, 1),  
          7px 7px   0 rgba(0,   0,   0,   .1);
    box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),  
          0 1px 1px rgba(0, 0, 0, .2),
          0 3px 0 #fff,
          0 4px 0 rgba(0, 0, 0, .2),
          0 6px 0 #fff,  
          0 7px 0 rgba(0, 0, 0, .2);
}

#login:before
{
    content: '';
    position: absolute;
    z-index: -1;
    border: 1px dashed #ccc;
    -moz-box-shadow: 0 0 0 1px #fff;
    -webkit-box-shadow: 0 0 0 1px #fff;
    box-shadow: 0 0 0 1px #fff;
}

/*--------------------*/

h1
{
    text-shadow: 0 1px 0 rgba(255, 255, 255, .7), 0px 2px 0 rgba(0, 0, 0, .5);
    text-transform: uppercase;
    text-align: center;
    color: #666;
    margin: 0 0 30px 0;ize: 40px
    letter-spacing: 4px;
    font: normal 26px/1 Verdana, Helvetica;
}

h1:after, h1:before
{
    background-color: #777;
    content: "";
    height: 1px;
    top: 15px;
    width: 60%;   
}

h1:after
{ 
    background-image: -webkit-gradient(linear, left top, right top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(left, #777, #fff);
    background-image: -moz-linear-gradient(left, #777, #fff);
    background-image: -ms-linear-gradient(left, #777, #fff);
    background-image: -o-linear-gradient(left, #777, #fff);
    background-image: linear-gradient(left, #777, #fff);      
    right: 0;
}

h1:before
{
    background-image: -webkit-gradient(linear, right top, left top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(right, #777, #fff);
    background-image: -moz-linear-gradient(right, #777, #fff);
    background-image: -ms-linear-gradient(right, #777, #fff);
    background-image: -o-linear-gradient(right, #777, #fff);
    background-image: linear-gradient(right, #777, #fff);
    left: 0;
}

/*--------------------*/

fieldset
{
    border: 0;
    padding: 0;
    margin: 0;
}

/*--------------------*/

#inputs input
{
/*     background: url(css/images/client/login-sprite.png) no-repeat;
 */     padding: 20px 0px 0px 0px;
    margin: 0 0 10px 0;
    width: 100%; /* 353 + 2 + 45 = 400 */
    border: 1px solid #ccc;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 5px;
    -moz-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
    -webkit-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
    box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
}

#username
{
    background-position: 5px -8px !important;
}

#password
{
    background-position: 5px -58px !important;
}

#inputs input:focus
{
    background-color: #fff;
    border-color: #e8c291;
    outline: none;
    -moz-box-shadow: 0 0 0 1px #e8c291 inset;
    -webkit-box-shadow: 0 0 0 1px #e8c291 inset;
    box-shadow: 0 0 0 1px #e8c291 inset;
}

/*--------------------*/


#submit
{		
    background-color: #ffb94b;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#fddb6f), to(#ffb94b));
    background-image: -webkit-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -moz-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -ms-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -o-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: linear-gradient(top, #fddb6f, #ffb94b);
    
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    
    text-shadow: 0 1px 0 rgba(255,255,255,0.5);
    
     -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
     -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
     box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;    
    
    border-width: 1px;
    border-style: solid;
    border-color: #d69e31 #e3a037 #d5982d #e3a037;

    float: left;
    padding: 0;
    cursor: pointer;
    font: bold 15px Arial, Helvetica;
    color: #8f5a0a;
}

#submit:hover,#submit:focus
{		
    background-color: #fddb6f;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ffb94b), to(#fddb6f));
    background-image: -webkit-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -moz-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -ms-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -o-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: linear-gradient(top, #ffb94b, #fddb6f);
}	

#submit:active
{		
    outline: none;
   
     -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
     -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
     box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;		
}

#submit::-moz-focus-inner
{
  border: none;
}

#actions a
{
    color: #3151A2;    
    float: right;
    line-height: 55px;
    margin-left: 10px;
}

/*--------------------*/

#back
{
    display: block;
    text-align: center;
    top: 60px;
    color: #999;
}



</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
function checkUserId(){
	var userId=document.getElementById("userId").value;
	if (window.XMLHttpRequest){
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'CustomerMobileAction?event=userIdCheck&responseType=HTML5&submit=YES&userId='+userId;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text==1){
//		document.getElementById("userId").focus();

		if(document.getElementById("os").value=="G"){
		Android.showToast("UserId already Exists");
		}else{
			alert("already exists");
		}
	}
	
}
function checkPassword(){
	var password=document.getElementById("password").value;
	var rePassword=document.getElementById("rePassword").value;
	if(password!="" && rePassword!=""){
		if(password==rePassword){
		}else{
			document.getElementById("password").value="";
			document.getElementById("rePassword").value="";
			document.getElementById("password").focus();
			document.getElementById("errorText").value="Password not matching";
			if(document.getElementById("os").value=="G"){
			Android.showToast("password not matching");
			}
		}
	}
}
/* function numbersOnly(){
	alert("calling");
	$("#userId").keypress(function(e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which<48 || e.which>57)) {
			//display error message
			return false;
		}
	});
} */
$(document).ready(function() {
if(document.getElementById("os").value=="G"){
	Android.homePage("");
	}
});
</script>
</head>
<input id="errorText" style="width: 80%;display:none; font-size: xx-large;font-weight: bolder;color: red;" type="image" value='<%=request.getAttribute("error")==null?"":request.getAttribute("error")%>'/>
 <%if(request.getAttribute("error")!=null) {%>
	<div id="error" style="margin-left: 10%;width: 100%;"><%=request.getAttribute("error")==null?"":request.getAttribute("error")%></div>
    <%} %>
<body >
<form id="login" action="CustomerMobileAction?event=clientRegistration" method="post" style="width:100%;height :100%;position:absolute;">
<input type="hidden" name="event" value="clientRegistration"/>
<input type="hidden" name="ccode" id="ccode" value="<%=request.getParameter("ccode")%>" /> 
	<input type="hidden" name="os" id="os" value="<%=request.getParameter("os")%>" /> 
	<input type="hidden" name="responseType" id="responseType" value="HTML5" /> 

	<input type="hidden" name="phoneNumber" id="phoneNumber" value="<%=request.getParameter("phoneNumber")%>" /> 
    <table id="tableRegistration" style="height:90%;width:100%">
    <tr><td>
    <h1 style="width: 99%;font-size:large;font-weight: bold;" align="center">User Registration</h1>
   </td></tr>
    <tr><td><fieldset id="inputs" >
        <img  src="css/images/client/userId.jpg"/>
        <input id="userId" name="userId" type="text" style="font-size: large;width:80%" onblur="checkUserId()"  placeholder="Create your User id" required>
             </fieldset></td></tr><tr><td><fieldset id="inputs">
          <img  src="css/images/client/password.png"/>
        <input id="password" name="password" type="password" style="font-size: large;width:80%" value="" placeholder="Enter the password" required onblur="checkPassword()" >
             </fieldset></td></tr><tr><td><fieldset id="inputs" >
                 <img  src="css/images/client/password.png"/>
        <input id="rePassword" name="rePassword" type="password" style="font-size: large;width:80%" value=""placeholder="Re-type Password" required onblur="checkPassword()" >
      </fieldset></td></tr><tr><td><fieldset id="inputs"  >
           <img src="css/images/client/user.png"/>
        <input id="userName" name="userName" type="text" style="font-size: large;width:80%" placeholder="Enter your Name"  required>   
      </fieldset></td></tr><tr><td><fieldset id="inputs"  >
     	 <img  src="css/images/client/email.png"/>
        <input id="emailAddress" name="emailAddress" type="text" style="font-size: large;width:80%" placeholder="Enter your Email Address" required>
      </fieldset></td></tr><tr><td><fieldset id="inputs"  >
           	 <img  src="css/images/client/phone.png"/>
        <input id="phoneNumberRegistration" name="phoneNumberRegistration" type="text" style="font-size: large;width:80%" value="<%=(request.getParameter("phoneNumber").equals("null")||request.getParameter("phoneNumber")==null)?"":request.getParameter("phoneNumber")%>" placeholder="Enter your phoneNumber" required>
        <input id="responseType" name="responseType" type="hidden" value="HTML5">
      </fieldset></td></tr><tr><td>
    <fieldset id="actions" >
        <input type="submit" id="submit" name="submit" style="width: 90%;font-size: large;font-weight: bold;" value="Register"><br/>
    </fieldset>
    </td></tr></table>
</form></body></html>