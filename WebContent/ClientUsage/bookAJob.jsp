<!DOCTYPE html>

<html style="height:100%;width:100%">
<head>
<title>Booking A Job</title>



<style>


body
{
    font: 12px 'Lucida Sans Unicode', 'Trebuchet MS', Arial, Helvetica;    
    margin: 0;
    background-color: #d9dee2;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebeef2), to(#d9dee2));
    background-image: -webkit-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -moz-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -ms-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: -o-linear-gradient(top, #ebeef2, #d9dee2);
    background-image: linear-gradient(top, #ebeef2, #d9dee2);    
}

/*--------------------*/

#login
{
    background-color: #fff;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));
    background-image: -webkit-linear-gradient(top, #fff, #eee);
    background-image: -moz-linear-gradient(top, #fff, #eee);
    background-image: -ms-linear-gradient(top, #fff, #eee);
    background-image: -o-linear-gradient(top, #fff, #eee);
    background-image: linear-gradient(top, #fff, #eee);  
 
    z-index: 0;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;  
    -webkit-box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),
          0 1px 1px rgba(0, 0, 0, .2),
          0 3px 0 #fff,
          0 4px 0 rgba(0, 0, 0, .2),
          0 6px 0 #fff,  
          0 7px 0 rgba(0, 0, 0, .2);
    -moz-box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),  
          1px 1px   0 rgba(0,   0,   0,   .1),
          3px 3px   0 rgba(255, 255, 255, 1),
          4px 4px   0 rgba(0,   0,   0,   .1),
          6px 6px   0 rgba(255, 255, 255, 1),  
          7px 7px   0 rgba(0,   0,   0,   .1);
    box-shadow:
          0 0 2px rgba(0, 0, 0, 0.2),  
          0 1px 1px rgba(0, 0, 0, .2),
          0 3px 0 #fff,
          0 4px 0 rgba(0, 0, 0, .2),
          0 6px 0 #fff,  
          0 7px 0 rgba(0, 0, 0, .2);
}

#login:before
{
    content: '';
    z-index: -1;
   
    -moz-box-shadow: 0 0 0 1px #fff;
    -webkit-box-shadow: 0 0 0 1px #fff;
    box-shadow: 0 0 0 1px #fff;
}

/*--------------------*/

h1
{
    text-shadow: 0 1px 0 rgba(255, 255, 255, .7), 0px 2px 0 rgba(0, 0, 0, .5);
    text-transform: uppercase;
    text-align: center;
    color: #666;
    margin: 0 0 30px 0;size: 40px
    letter-spacing: 4px;
    font: normal 26px/1 Verdana, Helvetica;
}

h1:after, h1:before
{
    background-color: #777;
    content: "";
    top: 15px;
    width: 60%;   
}

h1:after
{ 
    background-image: -webkit-gradient(linear, left top, right top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(left, #777, #fff);
    background-image: -moz-linear-gradient(left, #777, #fff);
    background-image: -ms-linear-gradient(left, #777, #fff);
    background-image: -o-linear-gradient(left, #777, #fff);
    background-image: linear-gradient(left, #777, #fff);      
    right: 0;
}

h1:before
{
    background-image: -webkit-gradient(linear, right top, left top, from(#777), to(#fff));
    background-image: -webkit-linear-gradient(right, #777, #fff);
    background-image: -moz-linear-gradient(right, #777, #fff);
    background-image: -ms-linear-gradient(right, #777, #fff);
    background-image: -o-linear-gradient(right, #777, #fff);
    background-image: linear-gradient(right, #777, #fff);
    left: 0;
}

/*--------------------*/

fieldset
{
    border: 0;
    padding: 0;
    margin: 0;
}

/*--------------------*/

#inputs input
{
/*     background: url(css/images/client/login-sprite.png) no-repeat;
 */    padding: 20px 0px 0px 0px;
    margin: 0 0 10px 0;
  /*   width: 80%; */ /* 353 + 2 + 45 = 400 */
    border: 1px solid #ccc;
    -moz-border-radius: 10px;
    -webkit-border-radius: 10px;
    border-radius: 5px;
    -moz-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
    -webkit-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
    box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
}

#username
{
    background-position: 5px -8px !important;
}

#password
{
    background-position: 5px -58px !important;
}

#inputs input:focus
{
    background-color: #fff;
    border-color: #e8c291;
    outline: none;
    -moz-box-shadow: 0 0 0 1px #e8c291 inset;
    -webkit-box-shadow: 0 0 0 1px #e8c291 inset;
    box-shadow: 0 0 0 1px #e8c291 inset;
}

/*--------------------*/

#submit
{		
    background-color: #ffb94b;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#fddb6f), to(#ffb94b));
    background-image: -webkit-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -moz-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -ms-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: -o-linear-gradient(top, #fddb6f, #ffb94b);
    background-image: linear-gradient(top, #fddb6f, #ffb94b);
    
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    
    text-shadow: 0 1px 0 rgba(255,255,255,0.5);
    
     -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
     -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
     box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;    
    
    border-width: 1px;
    border-style: solid;
    border-color: #d69e31 #e3a037 #d5982d #e3a037;

    float: left;
    height: 1%;
    padding: 0;
    width: 50%;
    cursor: pointer;
    font: bold 15px Arial, Helvetica;
    color: #8f5a0a;
}

#submit:hover,#submit:focus
{		
    background-color: #fddb6f;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ffb94b), to(#fddb6f));
    background-image: -webkit-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -moz-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -ms-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: -o-linear-gradient(top, #ffb94b, #fddb6f);
    background-image: linear-gradient(top, #ffb94b, #fddb6f);
}	

#submit:active
{		
    outline: none;
   
     -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
     -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
     box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;		
}

#submit::-moz-focus-inner
{
  border: none;
}

#actions a
{
    color: #3151A2;    
    float: right;
    line-height: 55px;
    margin-left: 10px;
}

/*--------------------*/

#back
{
    display: block;
    text-align: center;
    top: 60px;
    color: #999;
}



</style>

<script src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqModal.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.16.custom.min.js"></script>
<link type="text/css" href="js/jqModal.css" rel="stylesheet" />
<script type="text/javascript">
$('#bodyPage').css('height',$(window).height+'px');
$('#login').css('height',$(window).height+'px');
$("input").css('height',$(window).height()/6+'px');
$("input").css('padding',$(window).height()/10+'px');


function showTagDiv(){
	if(document.getElementById("os").value=="G"){
		Android.homePage("");
	}
	var address=document.getElementById("sAddress1").value;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'CustomerMobileAction?event=checkAddress&address='+address;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text==0){
	if(address!="" && address!=null){
		$("#tagAddress").show('slow');	
	}
	}
}
function tagAddresses(){
	 $('#addressTagWindow').jqm();
	 $('#addressTagWindow').jqmShow();
}
function closeDiv(){
	$("#tagAddress").hide('slow');
}
function insertAddress(){
	var tagName=document.getElementById("tagName").value;
	var address=document.getElementById("sAddress1").value;
	var latitude=document.getElementById("sLatitude").value;
	var longitude=document.getElementById("sLongitude").value;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'CustomerMobileAction?event=tagAddress&tagName='+tagName+'&address='+address+'&latitude='+latitude+'&longitude='+longitude;
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	if(text==1){
		alert('successfully Tagged');
		$('#addressTagWindow').jqmHide();
	}
}
function pickUpAddresses(){
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url = 'CustomerMobileAction?event=taggedAddressestoShow';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text = xmlhttp.responseText;
	document.getElementById("taggedAddresstoShow").innerHTML=text;
	$('#taggedAddresstoShow').jqm();
	$('#taggedAddresstoShow').jqmShow();
}
function fixValues(address,latitude,longitude){
	document.getElementById("sAddress1").value=address;
	document.getElementById("sLatitude").value=latitude;
	document.getElementById("sLongitude").value=longitude;
	$('#taggedAddresstoShow').jqmHide();

}
</script>
</head>

<%
 if(request.getAttribute("error")!=null) {%>
	<div id="error" style="margin-left: 10%;width: 100%;"><%=request.getAttribute("error")==null?"":request.getAttribute("error")%></div>
    <%} %>
<body style="width:100%;height :100%;" onload="showTagDiv();">
<form id="login"  action="CustomerMobileAction?event=openRequest" method="post" style="width:100%;height :100%;">
 <%if(request.getParameter("latitude")!=null && request.getParameter("longitude")!=null){%>
	 <input type="hidden" id="sLatitude" name="sLatitude" value="<%=request.getParameter("latitude")%>"/>
	 <input type="hidden" id="sLongitude" name="sLongitude" value="<%=request.getParameter("longitude")%>"/>
 <%}%>
	<input type="hidden" name="ccode" id="ccode" value="<%=request.getParameter("ccode")%>" /> 
	<%-- 	<input type="hidden" name="phoneNumber" id="phoneNumber" value="<%=request.getParameter("phoneNumber")%>" /> --%> 
		<input type="hidden" name="os" id="os" value="<%=request.getParameter("os")%>" /> 
     <input type="hidden" name="event" value="openRequest"/>
    <h1 style="width: 95%;font-size:large;font-weight: bold;" align="center">Book A Job</h1>
        <table id="tableRegistration" style="height:90%;width:100%">
    
    <%if(request.getParameter("latitude")!=null && !request.getParameter("latitude").equals("") ) {%>
    We take your address from Map.Don't worry about address field.
    <%} %>
    <tr><td><fieldset id="inputs" style="width: 98%;" >
              <input id="userName" name="userName" type="text" style="font-size: large;width:90%;height:20%" value="<%=request.getAttribute("name")==null?"":request.getAttribute("name")%>" placeholder="Enter your Name" autofocus required>   
            </fieldset></td></tr><tr><td><fieldset id="inputs" style="width: 98%;" >
      
       <%if(request.getParameter("latitude")!=null && request.getParameter("longitude")!="" && request.getParameter("address")!="" ){ 
      		if( request.getParameter("fromFavAdd")!=null){%>
      		<input type="hidden" name="fromHome" id="fromHome" value="1" /> 
      <%}else{ %>
      <input type="hidden" name="fromFavAdd" id="fromFavAdd" value="1" />
      <%} %>
     
                      <input id="sAddress1" name="sAddress1" type="text" style="font-size: large;width:90%;height:20%" onblur="showTagDiv()" value="<%=request.getParameter("address") %>" placeholder="Enter your Address" required/>
       
       <%}else{ %>
               <input id="sAddress1" name="sAddress1" type="text" style="font-size: large;width:90%;height:20%" onblur="showTagDiv()" placeholder="Enter your Address" required/>
       <%} %>
       <%if(session.getAttribute("clientUser")!=null){%>
       <input type="button" name="taggedAddresses" id="taggedAddresses" value="Tagged Addresses" onclick="pickUpAddresses()"/>
     
     <%} %>
      </fieldset></td></tr>
      
      <tr><td><fieldset id="inputs" style="width: 98%;" >
       
        <input id="phoneNumber" name="phoneNumber" type="text" style="font-size: large;width:90%;height:20%" value="<%=request.getAttribute("phoneNumber")==null?"":request.getAttribute("phoneNumber")%>" placeholder="Enter your phoneNumber" required>
	      </fieldset></td></tr><tr><td><fieldset id="inputs" style="width: 98%;" >
	
	<!--       </fieldset></td></tr><tr><td><fieldset id="inputs" style="width: 98%;" >
	 
	   <input id="sDate" name="sDate" type="text" style="font-size: xx-large;width:90%;height:20%" value=""placeholder="Enter Date" required>
           </fieldset></td></tr><tr><td><fieldset id="inputs" style="width: 98%;" >
     
        <input id="sTime" name="sTime" type="text" style="font-size: xx-large;width:90%;height:20%" placeholder="Enter Time" required>
        -->      </fieldset></td></tr><tr><td><fieldset id="inputs" style="width: 98%;" >
        
        <input id="responseType" name="responseType" type="hidden" value="HTML5">
   </fieldset></td></tr><tr><td>
    <fieldset id="actions" style="width: 98%;" >
        <input type="submit" id="submit" name="submit" style="width: 90%;height:20%;font-size: large;font-weight: bold;" value="Book"><br/>
    </fieldset>
   </td></tr>
   <tr id="tagAddress" style="background-color: lightgray;width:98%;height:5%;display: none;"><td>
   Do you want to tag this address? <input type="button" name="yes" id="yes" value="yes" onclick="tagAddresses()"/> &nbsp; <input type="button" name="no" id="no" value="no" onclick="closeDiv()"/>
   
   </td></tr>
    <tr><td>
   <div class="jqmWindow" id="addressTagWindow" style="height:30%;overflow: scroll;overflow-y;" >
   					<%-- 	<jsp:include page="/ClientUsage/tagAddress.jsp"/> --%>
   					    <h1 style="width: 95%;font-size:large;font-weight: bold;" align="center">Tag Address</h1>
   		  
   					 <table  style="height:90%;width:100%">
    <tr><td><fieldset id="inputs" style="width: 98%;" >
              <input id="tagName" name="tagName" type="text" style="font-size: large;width:90%;height:20%" value="" placeholder="Enter the Address Tag" autofocus >   
            </fieldset></td></tr><tr><td><fieldset id="inputs" style="width: 98%;" >
      
       <%if(request.getAttribute("latitude")!=null && request.getAttribute("longitude")!="" && request.getAttribute("address")!=""){ %>
      		<input type="hidden" name="fromHome" id="fromHome" value="1" /> 
      
                      <input id="sAddress1" name="sAddress1" type="text" style="font-size: large;width:90%;height:20%" onblur="showTagDiv()" value="<%=request.getAttribute("address") %>" placeholder="Enter your Address" required/>
       
       <%}else{ %>
               <input id="sAddress1" name="sAddress1" type="text" style="font-size: large;width:90%;height:20%" onblur="showTagDiv()" placeholder="Enter your Address"/>
       <%} %>
      </fieldset></td></tr><tr><td>
    <fieldset id="actions" style="width: 98%;" >
        <input type="button" id="submit" name="submit" style="width: 90%;height:20%;font-size: large;font-weight: bold;" onclick="insertAddress()" value="Tag"><br/>
    </fieldset>
    </td></tr></table></div>
    
     <div class="jqmWindow"  id="taggedAddresstoShow"  style="height:5%;width:80%;margin-left:-10%">
   		</div>
   </td></tr>
   
   </table>
   </form>
   </body>
   </html>