<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@page import="com.tds.tdsBO.ConsoleBO"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Job Console View</title>

<script type="text/javascript">

document.writeln("<div class=\"jqmWindow\" id = \"pbar\" ></div>");
function progressBar() {
	if (percent < 100) {
	percent = percent + 1;
	$("#pbar").jqm();
	$("#pbar").jqmShow();
	/* document.getElementById("pbar").innerHTML = "<center><h6><font color=\"blue\">WelCome to Operator Dashboard</h6></b></center></br>Please Wait...<font color=\"lime\"size=\"2px\"> Loading : <img src=\"images/Dashboard/progress2.gif\">"+ "<font size=\"1px\" ><b><a>" + getBar()+"</a></b></font></font></font>";
	window.status = "Loading : " + percent + "%" + " " + getBar();
	google.maps.event.addListener(map, 'idle', function() { 
		percent=100;
		setTimeout ("progressBar()", timePeriod); 
		
		} );
	setTimeout ("progressBar()", timePeriod); 

	}
	else {
	$("#pbar").jqmHide();
 */	document.getElementById("pbar").innerHTML = "";
	window.status = "Please Wait the Page was ReLoading...";
	document.body.style.display = "";
	}
	}



var mins,secs,TimerRunning,TimerID;
function Pad(number) //pads the mins/secs with a 0 if its less than 10
{
	if(number<10)
		number=0+""+number;
	return number;
}
function Init()//call the Init function when u need to start the timer
{
	mins=1;
	secs=0;
	StopTimer();
}
function StopTimer()
{
	if(TimerRunning)
	clearTimeout(TimerID);
	TimerRunning=false;
	secs= document.getElementById('sec').value;
	StartTimer();
}
function StartTimer(){	
	TimerRunning=true;
	document.getElementById('tid').innerHTML = "Time Remaining for Refresh :"+Pad(secs);
	Pad(secs);
	TimerID=self.setTimeout("StartTimer()",1000);
	if(secs==0)
	{
			StopTimer();
			location.reload(true);
		}
	secs--;
}
</script> 

 <%
 ArrayList<ConsoleBO> jobList= (ArrayList<ConsoleBO>) request.getAttribute("jobList"); 
 %>

 <style type="text/css">
 
 
        /*** central column on page ***/
        div#divContainer
        {
            margin: 0 auto;
            font-family: Calibri;
            padding: 0.5em 1em 1em 1em;
 
            /* rounded corners */
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
 
            /* add gradient */
            background-color: rgb(46, 100, 121);
            //#59585d;
            background: -webkit-gradient(linear, left top, left bottom, from(rgb(46, 100, 121)), to(rgb(46, 100, 121)));
            background: -moz-linear-gradient(top, #00FFFF, #00FFFF);
 
            /* add box shadows */
            -moz-box-shadow: 5px 5px 10px rgba(0,0,0,0.3);
            -webkit-box-shadow: 5px 5px 10px rgba(0,0,0,0.3);
            box-shadow: 5px 5px 10px rgba(0,0,0,0.3);
        }
 
        h1 {color:#FFE47A; font-size:1.5em;}
 
       </style>

</head>
<body onload="Init()" bgcolor="#000000">
<br>
<br>
<br>
 <input type="hidden" name="sec" id="sec" value="10">
    <!-- CENTTERED COLUMN ON THE PAGE-->
    <div id="divContainer" style="height:100%; width:80%; " align="center">
        <!-- HTML5 TABLE FORMATTED VIA CSS3-->
        <div>
        <table width="100%">
        <tr>
        <td width="20%"><a href="control" style="text-decoration:none">
        <img alt="" src="images/Dashboard/Home.png" style="height: 45px; width: 65px">
        </a></td>
        <td colspan="13" align="right" width="70%">              
        <div id="tid" style="color: #00FFFF"></div> </td></tr>
        </table>        
        </div>       
        <div style="display: block;overflow-y: scroll;max-height: 640px;">
        <table width="100%" height="100%" align="center" bgcolor="white" style="color: green;overflow:scroll;">
        
            <!-- TABLE HEADER-->
            <thead>            
                <tr align="center" style=" background-color: green;color: #00FFFF" >                  
                  <th>TripID</th>                                                                                      
                  <th>Trip Status</th>
                  <th>Date & Time</th>       
                  <th>Reason</th>   
                </tr>
            </thead>
            <!-- TABLE BODY: MAIN CONTENT-->
            <tbody>
           <%for(int i=0;i<jobList.size();i++) {%>
                <tr align="center">
                <%String color="" ;%>
                <%String status;   
                int a=jobList.get(i).getStatus();
					if(a==30){status="Broadcast";
					}else if(a==101){status="Driver Sent Message";
					}else if(a==99 || a==70){status="Trip On Hold";
					}else if(a==80){status="Getting Offered Trip Details";
					}else if(a==101){status="Driver Sent Message";
			        }else if(a==65){status="Driver No Show";
			        }else if(a==61){status="Driver Completed";
			        }else if(a==55){status="Operator Cancelled";
			        }else if(a==51){status="No Show";
			        }else if(a==50){status="Customer Cancelled Job";
			        }else if(a==49){status="Driver Soon To Complete Job";
			        }else if(a==48){status="Driver Reporting NoShow";
			        }else if(a==47){status="Trip Started";	
			        }else if(a==45){status="Trip Ended";
			        }else if(a==41){status="IVR Call";
					}else if(a==43){status="On Rotue To Pickup";
					}else if(a==40){status="Driver On Route for Job";
					}else if(a==38){status="DashBoard Manual Change";
					}else if(a==35){status="Trip Started";
					}else if(a==30){status="Job Rejected";
					}else if(a==27){status="IVR Call";
					}else if(a==25){status="Driver Job Accepted";
					}else if(a==23 || a==21){status="IVR Call";
					}else if(a==20){status="Updated Dispatch Start Time";
					}else if(a==15){status="Performing Dispatch";
					}else if(a==8 || a==75){status="Job Re-Dispatched";
					}else if(a==5){status="Job Updated";					
					}else if(a==4){status="DashBoard Manual Change";
					}else if(a==3){status="Couldnt Find Drivers";					
					}else if(a==1){status="Job Created";						
					}else if(a==0){status="Unknown";					
					}else if(a==100){
						status="Warning";	
						color="style=\"color:red\"";
					}else{						
						status="Dispatching";						
					}%>					
                	<td><%=jobList.get(i).getTripid() %></td>                	             	                	
                	<td <%=color %>><%=status%></td> 
                	<td <%=color %>><%=jobList.get(i).getTime() %></td>
					<td <%=color %>><%=jobList.get(i).getReason() %></td> 					
				</tr>
 			<%}%> 
            </tbody>
 		</table>
 		</div>
 </div>
        
        <%-- 
        <table width="100%">
        <tr><td><a href="control" style="text-decoration:none"><img alt="" src="images/Dashboard/Home.png" style="height: 45px; width: 65px">
        </a></td><td colspan="13" align="right">        
             <div id="tid"></div> </td></tr>
        </table>
        <table class="formatHTML5" width="100%" align="center">
            <!-- TABLE HEADER-->
            <thead>            
                <tr align="center">
                  
                  <th>TripID</th>
                  <th>P.Time</th>
                  <th>Reason</th>                                                    
                  <th>Trip Status</th>                  
                </tr>
            </thead>
            <!-- TABLE BODY: MAIN CONTENT-->
            <tbody>
           <%for(int i=0;i<jobList.size();i++) {%>
                <tr align="center">
                <%String status;   
                int a=jobList.get(i).getStatus();
					if(a==30){
						status="Broadcast";
			        }else if(a==40){status="Allocated";
					}else if(a==43){status="On Rotue To Pickup";
					}else if(a==50){status="Customer Cancelled Request";
					}else if(a==51){status="No Show";
					}else if(a==4){status="Dispatching";
					}else if(a==0){status="Unknown";
					}else if(a==47){status="Trip Started";
					}else if(a==99){status="Trip On Hold";
					}else if(a==3){status="Couldnt Find Drivers";
					}else if(a==48){status="Driver Reporting NoShow";
					}else{						
						status="Dispatching";						
					}%>
					<td><%=i+1+")"%></td>
                	<td><%=jobList.get(i).getTripid() %></td>
                	<td><%=jobList.get(i).getTime() %></td>                	
                	<td><%=jobList.get(i).getReason() %></td>
                	<td><%=status %></td> 
										
				</tr>
 			<%}%> 
            </tbody>
 
           <tfoot>
                <tr><td colspan="12"> </td></tr>
            </tfoot> 
        </table>
    </div>
     --%>
</body>
</html>