<%@page import="com.sun.xml.xsom.impl.scd.Iterators.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Daily Job Count</title>
<script src="jspdf.min.js"></script>
<script src="jspdf.plugin.autotable.src.js"></script>
<script type="text/javascript">
	function printDiv() {
		document.getElementById("buttonArea").style.display = 'none';
	}
	function printdiv(printpage) {
		var headstr = "<html><head><title></title></head><body>";
		var footstr = "</body>";
		var newstr = document.all.item(printpage).innerHTML;
		var oldstr = document.body.innerHTML;
		document.body.innerHTML = headstr + newstr + footstr;
		window.print();
		document.body.innerHTML = oldstr;
		return false;
	}
	function download() {

	}
</script>
<style type="text/css">
.header {
	background-color: lightgrey;
	height: 90%;
	weidth: 100%;
	align: center;
	overflow: scroll;
}

.footer {
	background-color: lightgrey;
	height: 10%;
	weidth: 100%;
	align: center;
}

body {
	height: 100%;
	padding: 0;
	margin: 0;
}

table,th,td {
	border: 1px solid black;
	border-collapse: collapse;
	padding: 5px;
	width: 80%;
	height: 100%;
	text-align: center;
	vertical-align: middle;
	text-align: center;
}
/*  Define the background color for all the ODD background rows  */
.printArea tr:nth-child(odd) {
	background: #b8d1f3;
}
/*  Define the background color for all the EVEN background rows  */
.printArea tr:nth-child(even) {
	background: #dae5f4;
}
</style>
</head>
<body align="center">
	<form>
		<input type="hidden" name="serverValues" id="serverValues"
			value=<%=request.getAttribute("values")%> />
		<!-- <div class="header"> -->
		<!-- <input type="submit" value="print" onclick="printdiv('printArea')" /> -->
		<!-- Tables -->
		<div id="printArea" class="printArea">
			<!-- table-1 -->
			<%
				if (request.getAttribute("page").equals("dailyDispatchPerformance")) {
			%>
			<input type="hidden" name="serverExtraValues" id="serverExtraValues"
				value=<%=request.getAttribute("extraValues")%> />
			<table align=center valign=center id="dailyDispatchPerformanceTable"
				width="100">
				<tr>
					<th>Daily Dispatch Performance</th>
					<th><%=request.getAttribute("sDate")%></th>
					<th><%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center width=100>
				<tr>
					<th>NAME</th>
					<th>AVG(TIMETOALLOCATE)</th>
					<th>AVG(TIMETOPICKUP)</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverExtraValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].name
								+ "</td><td>"
								+ obj.serverDetails[j].timeToAllocate
								+ "</td><td>"
								+ obj.serverDetails[j].timeTopickup
								+ "</td></tr>");
					}
				</script>
			</table>
			<br />
			<table align=center valign=center width=100>
				<tr>
					<th>TRIP ID</th>
					<th>NAME</th>
					<th>DATE</th>
					<th>LAT</th>
					<th>LON</th>
					<th>E.LAT</th>
					<th>E.LON</th>
					<th>T.A</th>
					<th>T.P</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].tripid
								+ "</td><td>" + obj.serverDetails[j].name
								+ "</td><td>" + obj.serverDetails[j].date
								+ "</td><td>" + obj.serverDetails[j].lat
								+ "</td><td>" + obj.serverDetails[j].lon
								+ "</td><td>" + obj.serverDetails[j].elat
								+ "</td><td>" + obj.serverDetails[j].elon
								+ "</td><td>"
								+ obj.serverDetails[j].timeToAllocate
								+ "</td><td>"
								+ obj.serverDetails[j].timeTopickup
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-2 -->
			<%
				if (request.getAttribute("page").equals("dailyJobCount")) {
			%>
			<table align=center valign=center id="dailyJobCountTable">
				<tr>
					<th colspan="2">Daily Job Count&nbsp;&nbsp;&nbsp;&nbsp;Date:<%=request.getAttribute("date")%></th>
				</tr>
				<tr>
					<th>STATUS</th>
					<th>TOTAL</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].status
								+ "</td><td>" + obj.serverDetails[j].total
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-3-->
			<%
				if (request.getAttribute("page").equals("operatorPerformance")) {
			%>
			<table align=center valign=center id="operatorPerformanceTable">
				<tr>
					<th colspan="2">OPERATOR DAILY
						STATISTICS&nbsp;&nbsp;&nbsp;&nbsp;Date:<%=request.getAttribute("date")%></th>
				</tr>
				<tr>
					<th>OPERATOR</th>
					<th>TOTAL</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>"
								+ obj.serverDetails[j].creator + "</td><td>"
								+ obj.serverDetails[j].total + "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
			
			<!-- table-4-->
			<%
				if (request.getAttribute("page").equals("monthlyConsolidatedStatisticsGraph")) {
			%>
			<table align=center valign=center id="monthlyConsolidatedStatisticsGraphTable">
				<tr>
					<th>MONTHLY CONSOLIDATED STATISTICS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;StartDate:<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EndDate:<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<table align=center valign=center id="monthlyConsolidatedStatisticsGraphTable2">
				<tr>
					<th style="width: 25%">MONTH</th>
					<th style="width: 50%">TRIP STATUS</th>
					<th style="width: 25%">TOTAL JOBS</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>"
								+ obj.serverDetails[j].mnth
								+ "</td><td>" + obj.serverDetails[j].trpSts
								+ "</td><td>" + obj.serverDetails[j].tot
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
			<!-- table-5-->
			<%
				if (request.getAttribute("page").equals("monthlyJobCountGraph")) {
			%>
			<table align=center valign=center id="monthlyJobCountGraphTable">
				<tr>
					<th>MONTHLY JOB COUNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;StartDate:<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EndDate:<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<table align=center valign=center id="monthlyJobCountGraphTable1">
				<tr>
					<th style="width: 25%">DATE</th>
					<th style="width: 50%">TRIP STATUS</th>
					<th style="width: 25%">TOTAL JOBS</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>"
								+ obj.serverDetails[j].date
								+ "</td><td>" + obj.serverDetails[j].trpSts
								+ "</td><td>" + obj.serverDetails[j].tot
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
			<!-- table-6-->
			<%
				if (request.getAttribute("page").equals("jobStatistics")) {
			%>
			<table align=center valign=center id="jobStatisticstABLE">
				<tr>
					<th>JOB STATISTICS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;StartDate:<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EndDate:<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<table align=center valign=center id="jobStatisticstABLE2">
				<tr>
					<th style="width: 25%">SERVICE DATE</th>
					<th style="width: 50%">TRIP STATUS</th>
					<th style="width: 25%">TOTAL JOBS</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>"
								+ obj.serverDetails[j].serviceDate
								+ "</td><td>" + obj.serverDetails[j].tripStatus
								+ "</td><td>" + obj.serverDetails[j].totalJobs
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			<!-- table-7-->
			<%
				if (request.getAttribute("page").equals("tripDetail")) {
			%>
			<table align=center valign=center id="tripDetailTable">
				<tr>
					<th>TRIP DETAIL</th>
				</tr>
			</table>
			<table align=center valign=center id="tripDetailTable2">
				<tr>
					<th style="width: 25%">TRIP ID</th>
					<th style="width: 25%">TRIP STARTED</th>
					<th style="width: 25%">ONSITE</th>
					<th style="width: 25%">TRIP ENDED</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].tripid
								+ "</td><td>" + obj.serverDetails[j].tripStart
								+ "</td><td>" + obj.serverDetails[j].onsite
								+ "</td><td>" + obj.serverDetails[j].tripEnd
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-8-->
			<%
				if (request.getAttribute("page").equals("specailFlags")) {
			%>
			<table align=center valign=center id="specailFlagsTable1">
				<tr>
					<th>SPECTIAL FLAG REPORT</th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="specailFlagsTable2">
				<tr>
					<th style="width: 50%">TOTAL</th>
					<th style="width: 50%">FLAGS</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					document.write("<tr><td>" + obj.serverDetails.length
							+ "</td><td></td></tr>");
				</script>
			</table>
			<br />
			<table align=center valign=center id="specailFlagsTable3">
				<tr>
					<th style="width: 12%">TRIP ID</th>
					<th style="width: 12%">NAME</th>
					<th style="width: 12%">PHONE</th>
					<th style="width: 12%">P/U ADDRESS</th>
					<th style="width: 12%">D/O ADDRESS</th>
					<th style="width: 12%">DRIVER ID</th>
					<th style="width: 12%">DATE</th>
					<th style="width: 12%">AMOUNT</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document
								.write("<tr><td>"
										+ obj.serverDetails[j].tripId
										+ "</td><td>"
										+ obj.serverDetails[j].name
										+ "</td><td>"
										+ obj.serverDetails[j].phone
										+ "</td><td>"
										+ obj.serverDetails[j].from
										+ "</td><td>"
										+ obj.serverDetails[j].to
										+ "</td><td>"
										+ obj.serverDetails[j].driverId
										+ "</td><td>"
										+ obj.serverDetails[j].date
										+ "</td><td>"
										+ (parseFloat(obj.serverDetails[j].amt,
												10) + parseFloat(
												obj.serverDetails[j].payAcc, 10))
										+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-9-->
			<%
				if (request.getAttribute("page").equals("driverTripSheet")) {
			%>
			<table align=center valign=center id="driverTripSheetTable">
				<tr>
					<th>DRIVER TRIP AND SHIFT DETAILS
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DATE :<%=request.getAttribute("date")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="driverTripSheetTable1">
				<tr>
					<th style="width: 12%">TIME</th>
					<th style="width: 12%">NAME</th>
					<th style="width: 12%">OPERATOR ID</th>
					<th style="width: 12%">OPEN TIME</th>
					<th style="width: 12%">OPENED BY</th>
					<th style="width: 12%">CLOSE TIME</th>
					<th style="width: 12%">CLOSED BY</th>
					<th style="width: 12%">VEHICLE NO</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].time
								+ "</td><td>" + obj.serverDetails[j].name
								+ "</td><td>" + obj.serverDetails[j].opId
								+ "</td><td>" + obj.serverDetails[j].opTime
								+ "</td><td>" + obj.serverDetails[j].opBy
								+ "</td><td>" + obj.serverDetails[j].cTime
								+ "</td><td>" + obj.serverDetails[j].cBy
								+ "</td><td>" + obj.serverDetails[j].vNo
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-10-->
			<%
				if (request.getAttribute("page").equals("ReservedJobs")) {
			%>
			<table align=center valign=center id="ReservedJobsTable">
				<tr>
					<th>RESERVED JOBS</th>
				</tr>
			</table>
			<table align=center valign=center id="ReservedJobsTable1">
				<tr>
					<th style="width: 33%">ACCOUNT NO</th>
					<th style="width: 33%">START DATE</th>
					<th style="width: 33%">END DATE</th>
				</tr>
				<tr>
					<td><%=request.getAttribute("acNo")%></td>
					<td><%=request.getAttribute("sDate")%></td>
					<td><%=request.getAttribute("eDate")%></td>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="ReservedJobsTable2">
				<tr>
					<th style="width: 10%">TRIP ID</th>
					<th style="width: 10%">NAME</th>
					<th style="width: 10%">PHONE NUMBER</th>
					<th style="width: 10%">DATE</th>
					<th style="width: 10%">P/U ADDRESS</th>
					<th style="width: 10%">D/O ADDRESS</th>
					<th style="width: 10%">AMOUNT</th>
					<th style="width: 10%">INSTRUCTIONS</th>
					<th style="width: 10%">CAB</th>
					<th style="width: 10%">DRIVER ID</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].tripId
								+ "</td><td>" + obj.serverDetails[j].name
								+ "</td><td>" + obj.serverDetails[j].phone
								+ "</td><td>" + obj.serverDetails[j].date
								+ "</td><td>" + obj.serverDetails[j].padd
								+ "</td><td>" + obj.serverDetails[j].dadd
								+ "</td><td>" + obj.serverDetails[j].amt
								+ "</td><td>" + obj.serverDetails[j].ins
								+ "</td><td>" + obj.serverDetails[j].vNo
								+ "</td><td>" + obj.serverDetails[j].driverId
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-11-->
			<%
				if (request.getAttribute("page").equals("invoiceByDriver")) {
			%>
			<table align=center valign=center id="invoiceByDriverTable">
				<tr>
					<th>INVOICE BY DRIVER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start Date
						:<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="invoiceByDriverTable1">
				<tr>
					<th style="width: 10%">NAME</th>
					<th style="width: 10%">PICKUP DATE</th>
					<th style="width: 10%">DRIVER ID</th>
					<th style="width: 10%">TRANS ID</th>
					<th style="width: 10%">TRIP ID</th>
					<th style="width: 10%">ADDRESS</th>
					<th style="width: 10%">VOUCHER NO</th>
					<th style="width: 10%">AMOUNT</th>
					<th style="width: 10%">TIP</th>
					<th style="width: 10%">TOTAL</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document
								.write("<tr><td>"
										+ obj.serverDetails[j].name
										+ "</td><td>"
										+ obj.serverDetails[j].pDate
										+ "</td><td>"
										+ obj.serverDetails[j].dId
										+ "</td><td>"
										+ obj.serverDetails[j].trnsId
										+ "</td><td>"
										+ obj.serverDetails[j].tripId
										+ "</td><td>"
										+ obj.serverDetails[j].add
										+ "</td><td>"
										+ obj.serverDetails[j].vouNo
										+ "</td><td>"
										+ obj.serverDetails[j].amt
										+ "</td><td>"
										+ obj.serverDetails[j].tip
										+ "</td><td>"
										+ (parseFloat(obj.serverDetails[j].amt,
												10) + parseFloat(
												obj.serverDetails[j].tip, 10))
										+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-12-->
			<%
				if (request.getAttribute("page").equals("orangeTaxi")) {
			%>
			<table align=center valign=center id="orangeTaxiTable">
				<tr>
					<th>REGULATORY TRIP REPORTS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start
						Date :<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="orangeTaxiTable1">
				<tr>
					<th style="width: 13%">DATE</th>
					<th style="width: 12%">PERMIT #</th>
					<th style="width: 12%">D/F</th>
					<th style="width: 12%">TRIP ID</th>
					<th style="width: 12%">TIME</th>
					<th style="width: 12%">FROM</th>
					<th style="width: 12%">TO</th>
					<th style="width: 14%">STATUS</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].date
								+ "</td><td>" + obj.serverDetails[j].ssNo
								+ "</td><td>" + obj.serverDetails[j].dispatch
								+ "</td><td>" + obj.serverDetails[j].tripId
								+ "</td><td>" + obj.serverDetails[j].time
								+ "</td><td>" + obj.serverDetails[j].qNo
								+ "</td><td>" + obj.serverDetails[j].eQno
								+ "</td><td>" + obj.serverDetails[j].tripStatus
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-13-->
			<%
				if (request.getAttribute("page").equals("driverDisbursement")) {
			%>
			<table align=center valign=center id="driverDisbursement">
				<tr>
					<th>DRIVER DISBURSEMENT
						DETAILS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start Date :<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="driverDisbursement1">
				<tr>
					<th style="width: 12%">TRIP ID</th>
					<th style="width: 16%">NAME</th>
					<th style="width: 12%">DRIVER ID</th>
					<th style="width: 20%">SERVICE DATE</th>
					<th style="width: 20%">PROCESS DATE</th>
					<th style="width: 10%">CHARGE TYPE</th>
					<th style="width: 10%">AMOUNT</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].tripId
								+ "</td><td>" + obj.serverDetails[j].name
								+ "</td><td>" + obj.serverDetails[j].driverId
								+ "</td><td>" + obj.serverDetails[j].sdate
								+ "</td><td>" + obj.serverDetails[j].pDate
								+ "</td><td>" + obj.serverDetails[j].cType
								+ "</td><td>" + obj.serverDetails[j].amt
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

			<!-- table-14-->
			<%
				if (request.getAttribute("page").equals("dispatcherShiftDetails")) {
			%>
			<table align=center valign=center id="dispatcherShiftDetailsTable">
				<tr>
					<th>DISPATCHER SHIFT DETAILS AND PERFORMANCE OF THE
						DAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DATE:<%=request.getAttribute("date")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="dispatcherShiftDetailsTable1">
				<tr>
					<th style="width: 13%">OPERATOR</th>
					<th style="width: 13%">OPERATOR ID</th>
					<th style="width: 16%">LOGGEDIN TIME</th>
					<th style="width: 16%">SHIFT OPEN TIME</th>
					<th style="width: 16%">SHIFT CLOSE TIME</th>
					<th style="width: 13%">OPENED BY</th>
					<th style="width: 13%">CLOSED BY</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].op
								+ "</td><td>" + obj.serverDetails[j].opId
								+ "</td><td>" + obj.serverDetails[j].tTim
								+ "</td><td>" + obj.serverDetails[j].oTim
								+ "</td><td>" + obj.serverDetails[j].cTim
								+ "</td><td>" + obj.serverDetails[j].oBy
								+ "</td><td>" + obj.serverDetails[j].cBy
								+ "</td></tr>");
					}
				</script>
				<input type="hidden" name="serverExtraValues" id="serverExtraValues"
					value=<%=request.getAttribute("extraValues")%> />
				<br />
				<table align=center valign=center id="dispatcherShiftDetailsTable3">
					<tr>
						<th style="width: 50%">OPERATOR</th>
						<th style="width: 50%">TOTAL</th>
					</tr>
					<script type="text/javascript">
						var res = document.getElementById('serverExtraValues').value;
						var jsonObj = "{\"serverDetails\":" + res + "}";
						var obj = JSON.parse(jsonObj.toString());
						for (var j = 0; j < obj.serverDetails.length; j++) {
							document
									.write("<tr><td>"
											+ obj.serverDetails[j].optr
											+ "</td><td>"
											+ obj.serverDetails[j].total
											+ "</td></tr>");
						}
					</script>
				</table>
				<%
					}
				%>



				<!-- table-15-->
				<%
					if (request.getAttribute("page").equals("responseOfACallByCity")) {
				%>
				<table align=center valign=center id="responseOfACallByCityTable">
					<tr style="width: 100%">
						<th >RESPONSE OF CALL BY CITY
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;START DATE:<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;END
							DATE:<%=request.getAttribute("eDate")%></th>
					</tr>
				</table>
				<br />
				<table align=center valign=center id="responseOfACallByCityTable1">
					<tr>
						<th style="width: 10%">TRIP ID</th>
						<th style="width: 12%">CITY</th>
						<th style="width: 12%">DATE</th>
						<th style="width: 12">START LAT</th>
						<th style="width: 12%">START LONG</th>
						<th style="width: 12%">END LAT</th>
						<th style="width: 12%">END LONG</th>
						<th style="width: 10%">TO ALLOCATE</th>
						<th style="width: 8%">TO PICKUP</th>
					</tr>
					<script type="text/javascript">
						var res = document.getElementById('serverValues').value;
						var jsonObj = "{\"serverDetails\":" + res + "}";
						var obj = JSON.parse(jsonObj.toString());
						for (var j = 0; j < obj.serverDetails.length; j++) {
							document.write("<tr><td>"
									+ obj.serverDetails[j].tripId + "</td><td>"
									+ obj.serverDetails[j].city + "</td><td>"
									+ obj.serverDetails[j].date + "</td><td>"
									+ obj.serverDetails[j].slat + "</td><td>"
									+ obj.serverDetails[j].slon + "</td><td>"
									+ obj.serverDetails[j].elat + "</td><td>"
									+ obj.serverDetails[j].elon + "</td><td>"
									+ obj.serverDetails[j].tta + "</td><td>"
									+ obj.serverDetails[j].ttp + "</td></tr>");
						}
					</script>
					</table>
					<input type="hidden" name="serverExtraValues"
						id="serverExtraValues"
						value=<%=request.getAttribute("extraValues")%> />
					<br />
					<table align=center valign=center id="responseOfACallByCityTable2">
						<tr>
							<th style="width: 25%">NAME</th>
							<th style="width: 25%">CITY</th>
							<th style="width: 25%">TIME TO ALLOCATE</th>
							<th style="width: 25%">TIME TO PICKUP</th>
						</tr>
						<script type="text/javascript">
							var res = document
									.getElementById('serverExtraValues').value;
							var jsonObj = "{\"serverDetails\":" + res + "}";
							var obj = JSON.parse(jsonObj.toString());
							for (var j = 0; j < obj.serverDetails.length; j++) {
								document.write("<tr><td>"
										+ obj.serverDetails[j].name
										+ "</td><td>"
										+ obj.serverDetails[j].city
										+ "</td><td>"
										+ obj.serverDetails[j].tta
										+ "</td><td>"
										+ obj.serverDetails[j].ttp
										+ "</td></tr>");
							}
						</script>
					</table>
					<%
						}
					%>
					
					
					<!-- table-16-->
			<%
				if (request.getAttribute("page").equals("DriverDeactivationHistory")) {
			%>
			<table align=center valign=center id="DriverDeactivationHistoryTable">
				<tr>
					<th>DRIVER DEACTIVATION HISTORY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start
						Date :<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="DriverDeactivationHistory1">
				<tr>
					<th style="width: 13%">NAME</th>
					<th style="width: 12%">DRIVER ID</th>
					<th style="width: 12%">DESCRIPTION</th>
					<th style="width: 12%">FROM DATE</th>
					<th style="width: 12%">TO DATE</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].name
								+ "</td><td>" + obj.serverDetails[j].drId
								+ "</td><td>" + obj.serverDetails[j].des
								+ "</td><td>" + obj.serverDetails[j].fDate
								+ "</td><td>" + obj.serverDetails[j].tDate
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
			<!-- table-17-->
			<%
				if (request.getAttribute("page").equals("OperatorTODriverJobCount")) {
			%>
			<table align=center valign=center id="OperatorTODriverJobCountTable">
				<tr>
					<th>TOTAL JOB ALLOCATED TO DRIVER BY OPERATOR PER DAY&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date :<%=request.getAttribute("date")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="OperatorTODriverJobCountTable1">
				<tr>
					<th style="width: 20%">OPERATOR</th>
					<th style="width: 20%">OPERATOR ID</th>
					<th style="width: 20%">DRIVER</th>
					<th style="width: 20%">DRIVER ID</th>
					<th style="width: 20%">TOTAL</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].opr
								+ "</td><td>" + obj.serverDetails[j].oprId
								+ "</td><td>" + obj.serverDetails[j].driver
								+ "</td><td>" + obj.serverDetails[j].driverId
								+ "</td><td>" + obj.serverDetails[j].total
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
			<!-- table-18-->
			<%
				if (request.getAttribute("page").equals("manuallyChangedTrip")) {
			%>
			<table align=center valign=center id="manuallyChangedTripTable">
				<tr>
					<th>MANUALLY CHANGED TRIPS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start
						Date :<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="manuallyChangedTrip1">
				<tr>
					<th style="width: 25%">TRIP ID</th>
					<th style="width: 25%">TIME</th>
					<th style="width: 25%">CHANGED BY</th>
					<th style="width: 25%">REASON</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].tripId
								+ "</td><td>" + obj.serverDetails[j].time
								+ "</td><td>" + obj.serverDetails[j].by
								+ "</td><td>" + obj.serverDetails[j].reason
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
				<!-- table-19-->
			<%
				if (request.getAttribute("page").equals("driverShiftDetail")) {
			%>
			<table align=center valign=center id="driverShiftDetailTable">
				<tr>
					<th>DRIVER SHIFT DETAILS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start
						Date :<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="driverShiftDetailTable1">
				<tr>
					<th style="width: 10%">DRIVER ID</th>
					<th style="width: 8%">VEHICLE NO</th>
					<th style="width: 18%">START TIME</th>
					<th style="width: 18%">END TIME</th>
					<th style="width: 18%">BREAK START AT</th>
					<th style="width: 18%">BREAK END AT</th>
					<th style="width: 10%">STATUS</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].drId
								+ "</td><td>" + obj.serverDetails[j].vNo
								+ "</td><td>" + obj.serverDetails[j].sTim
								+ "</td><td>" + obj.serverDetails[j].eTim
								+ "</td><td>" + obj.serverDetails[j].boTim
								+ "</td><td>" + obj.serverDetails[j].bcTim
								+ "</td><td>" + obj.serverDetails[j].status
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
			
			
			<!-- table-20-->
			<%
				if (request.getAttribute("page").equals("DriverJobDetails")) {
			%>
			<table align=center valign=center id="DriverJobDetailsTables">
				<tr>
					<th>DRIVER JOB DETAILS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start
						Date :<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="DriverJobDetailsTables1">
				<tr>
					<th style="width: 10%">DRIVER ID</th>
					<th style="width: 15%">TRIP ID</th>
					<th style="width: 15%">CREATED BY</th>
					<th style="width: 20%">DATE</th>
					<th style="width: 10%">REASON</th>
					<th style="width: 15%">START ADDR</th>
					<th style="width: 15%">END ADDR</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].dvrId
								+ "</td><td>" + obj.serverDetails[j].trpId
								+ "</td><td>" + obj.serverDetails[j].crtBy
								+ "</td><td>" + obj.serverDetails[j].serDat
								+ "</td><td>" + obj.serverDetails[j].res
								+ "</td><td>" + obj.serverDetails[j].sAdd
								+ "</td><td>" + obj.serverDetails[j].eAdd
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
			<!-- table-21-->
			<%
				if (request.getAttribute("page").equals("driverLogouts")) {
			%>
			<table align=center valign=center id="driverLogoutsTable1">
				<tr>
					<th>DRIVER LOGOUT DETAILS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start
						Date :<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="driverShiftDetailTable1">
				<tr>
					<th style="width: 25%">DRIVER ID</th>
					<th style="width: 25%">LOGGED OUT TIME</th>
					<th style="width: 25%">LOGGED OUT BY</th>
					<th style="width: 25%">REASON</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].drId
								+ "</td><td>" + obj.serverDetails[j].loTim
								+ "</td><td>" + obj.serverDetails[j].loBy
								+ "</td><td>" + obj.serverDetails[j].reas
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>
			
			<!-- table-22-->
			<%
				if (request.getAttribute("page").equals("tripLogs")) {
			%>
			<table align=center valign=center id="tripLogsTable">
				<tr>
					<th>TRIP LOG DETAIL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Start
						Date :<%=request.getAttribute("sDate")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End
						Date :<%=request.getAttribute("eDate")%></th>
				</tr>
			</table>
			<br />
			<table align=center valign=center id="tripLogsTAble1">
				<tr>
					<th style="width: 25%">TRIP ID</th>
					<th style="width: 25%">CHANGED BY</th>
					<th style="width: 25%">TIME</th>
					<th style="width: 25%">REASON</th>
				</tr>
				<script type="text/javascript">
					var res = document.getElementById('serverValues').value;
					var jsonObj = "{\"serverDetails\":" + res + "}";
					var obj = JSON.parse(jsonObj.toString());
					for (var j = 0; j < obj.serverDetails.length; j++) {
						document.write("<tr><td>" + obj.serverDetails[j].tripId
								+ "</td><td>" + obj.serverDetails[j].cngBy
								+ "</td><td>" + obj.serverDetails[j].tim
								+ "</td><td>" + obj.serverDetails[j].reas
								+ "</td></tr>");
					}
				</script>
			</table>
			<%
				}
			%>

					</div>
					<!-- 		</div>
 -->
					<!-- 	<input type="submit" value="Download" onclick="download()" /> -->
					</form>
</body>
</html>