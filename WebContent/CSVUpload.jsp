<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" import="javazoom.upload.*,java.util.*" %>
<%@ page errorPage="ExceptionHandler.jsp" %>
<%@ page import="javazoom.upload.parsing.CosUploadFile" %> 
<%@page import="com.common.util.TDSProperties"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<%
	String url = TDSProperties.getValue("FolderStore") ;
%>
<jsp:useBean id="upBean" scope="page" class="javazoom.upload.UploadBean" >  
 
<jsp:setProperty name="upBean" property="whitelist" value="*.csv" />
<jsp:setProperty name="upBean" property="overwritepolicy" value="nametimestamp" /> 
<%
	upBean.setFolderstore(url);
%> 
</jsp:useBean>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CSV Upload</title>
<style TYPE="text/css">
.style1
 {
	font-size: 12px;
	font-family: Verdana;
 }


</style>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript">
function chckIncl()
{
	if(document.getElementById('incl').checked)
	{
		document.getElementById('include').value = "1";
	} else {
		document.getElementById('include').value = "0";
	}
}
</script>
 <script type="text/javascript">
 function LoadMe()
 {	
 
 	//if(opener.document.getElementById("filename").value!="")
 	//{	
 	  //   opener.document.getElementById("filename").value=opener.document.getElementById("filename").value+"<br>"+document.getElementById("fn").value;
 	   // var filename = opener.document.getElementById("filename").value; 
 	   // var filearr = filename.split('<br>');
 	   // var anch_filename="";
 	   // var i=0;
 	   // for(i=0;i<filearr.length;i++){
 	   // 	anch_filename=anch_filename+"<br><a href='/TDS/control?action=file&event=fileDownload&fileid="+document.getElementById("fi").value+"&filename="+filearr[i]+"'>"+filearr[i]+"</a>";
 	   // }
 	    //opener.document.getElementById("ancid").style.display = 'none';
 	   	//opener.document.getElementById("filediv").innerHTML = anch_filename;
   	//}
   	//else
   	//{ 
   	
   		// alert("Include:"+document.getElementById("fn").value);
   		window.location.href = "/TDS/control?action=CSV&event=csvUploadStart&module=financeView&event1=process&fname="+document.getElementById("fn").value+"&include="+document.getElementById('include').value;
   	 	
   	//}   
  // self.close();
 }
	
  </script> 
</head>

<body>
<ul class="style1">
<input type="hidden" name="financial" value="3">
<%
  String fn="";  
if (MultipartFormDataRequest.isMultipartFormData(request))
{
   // Uses MultipartFormDataRequest to parse the HTTP request.
   MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
   String todo = null;
   if (mrequest != null) todo = mrequest.getParameter("todo");
   if ( (todo != null) && (todo.equalsIgnoreCase("upload")) )
   {
          Hashtable files = mrequest.getFiles();
          
          if ( (files != null) && (!files.isEmpty()) )
          {
              UploadFile file = ((UploadFile) files.get("uploadfile"));  
              upBean.store(mrequest); 
              Vector history = upBean.getHistory();
              UploadParameters up = (UploadParameters) history.elementAt(0);
              if(up.getAltFilename()!=null)
              {
              	 fn=up.getAltFilename();
              }
              else
              {
              	fn=((UploadFile) files.get("uploadfile")).getFileName();
              }
              
              if (file != null) 
              	out.println("<center><font color='blue' size='3'><li>"+" Attached File : "+file.getFileName()+" ("+file.getFileSize()+" bytes)"+"<BR> Content Type : "+file.getContentType()+"</font></center>");
              // Uses the bean now to store specified by jsp:setProperty at the top.
              //upBean.store(mrequest, "uploadfile"); 
          }
          else
          { 
            out.println("<li>No uploaded files"); 
          }
   }
   else out.println("<BR> todo="+todo);
}

  %>
</ul>
<%System.out.println("In Simple Upload Jsp = "+fn);%>
 <input type="hidden" name="fn" id="fn"  value="<%=fn%>">
  <input type="hidden" name="include" id="include" value="<%=request.getParameter("include")==null?"0":request.getParameter("include") %>">
 <form method="post" action="/TDS/control?action=CSV&event=csvUploadStart&module=financeView" name="upform" enctype="multipart/form-data">
 <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
 
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
<div class="rightColIn">
				<%if(fn!=null && fn.equals(""))
  				{ %>
                	<h1>CSV File Upload</h1>
                	<%} %>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab" class="style1">
                     <%if(fn!=null && fn.equals("")){ %>
                      <tr>
                      <td class="firstCol">File&nbsp;Name</td>
                      <td>
                      <input type="file" id="uploadfile" name="uploadfile" size="50">
                      </td>
                      <td></td>
                      </tr>
                      <%} %>
                      </table>
                       <div class="div2"><div class="div2In">
                         <%if(fn!=null && fn.equals("")){ %>
                         <div class="btnBlue padT10 padR5 fltLft ">
                         <div class="rht">
		 					<input type="hidden" name="todo" value="upload">
      						<input type="submit" name="Submit" value="Upload" class="lft">
      						</div></div>
      						<div class="btnBlue padT10 padR5 fltLft ">
                         <div class="rht">   
        					<input type="reset" name="Reset" value="Clear" class="lft">
        					</div></div>
       					<%} else {%>
       		 			<B>Include Heading </B> 
       		 			<div class="btnBlue padT10 padR5 fltLft ">
                         <div class="rht">
       		 			<input type="checkbox" name="incl" id="incl" value="1" <%=(request.getParameter("incl")==null?"":"checked") %> onclick="chckIncl()">
			 			<input type="button"  value="Process" onclick="LoadMe()" class="lft">
						<%} %>
                          </div>
                          </div>
                        </div>
                        </div>
                                                 
                </div>    
              </div>
            	
                <div class="clrBth"></div>
          
                        
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</form>
</body>
</html>
