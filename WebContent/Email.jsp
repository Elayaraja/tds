<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
<title>Contact GAC</title>
<script type="text/javascript" src="js/jquery.js"></script>

<script type="text/javascript">
function sendEmail(){
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var content=document.getElementById("emailContent").value;
	if(content!=""){
		var fixedCont=content.replace("&","and").replace("%","percentage").replace("'","hiphon");
		var url = 'RegistrationAjax';
		var parameters='event=contactUs&content='+fixedCont;
		xmlhttp.open("POST",url, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("Content-length", parameters.length);
		xmlhttp.setRequestHeader("Connection", "close");
		xmlhttp.send(parameters);
		closeScreen();
	} else {
		alert("Please enter something");
	}
}
function closeScreen(){
	window.location.href="/TDS/control";
}
</script>
</head>

<body>
 
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
                <div class="rightCol">
			<div class="rightColIn">
 		<div id="sendMail" ><textarea rows="10" cols="80" name="emailContent" id="emailContent"></textarea>
			<tr><td>
			<input type="button" id="send" value="Send" onclick="sendEmail()"/></td>
			<td><input type="button" id="close" value="Cancel" onclick="closeScreen()"/></td>
			</tr>
 		</div>
                    
                </div>    
              </div>
            	
                <div class="clrBth"></div>
          
            
            <div class="footerDV">Copyright &copy; 2010 Get A Cab</div>
 
</body>
</html>
