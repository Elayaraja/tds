function enterOpenRequest(){
	var phone = document.getElementById('phone').value;
	var name = document.getElementById('name').value;
	var email=document.getElementById('eMail').value;
	var add1 = document.getElementById('sadd1').value;
	var add2 = document.getElementById('sadd2').value;
	var city = document.getElementById('scity').value;
	var state = document.getElementById('sstate').value;
	var zip = document.getElementById('szip').value;
	var eadd1 = document.getElementById('eadd1').value;
	var eadd2 = document.getElementById('eadd2').value;
	var ecity = document.getElementById('ecity').value;
	var estate = document.getElementById('estate').value;
	var ezip = document.getElementById('ezip').value;
	var date = document.getElementById('sdate').value;
	if(date==""){
		var curdate = new Date();
		var cDate = curdate.getDate() >= 10 ? curdate.getDate() : "0"+curdate.getDate();
		var cMonth = curdate.getMonth()+1 >=10 ? curdate.getMonth()+1 : "0"+(curdate.getMonth()+1);
		var cYear = curdate.getFullYear();
		date=cMonth+"/"+cDate+"/"+cYear;	 
	} else {
		date=dojo.date.locale.format(dijit.byId("sdate").value,{datePattern: "MM/dd/yyyy", selector: "date"});	
	}
	var lati = document.getElementById('sLatitude').value;
	var longi = document.getElementById('sLongitude').value;
	var elati = document.getElementById('eLatitude').value;
	var elongi = document.getElementById('eLongitude').value;
	var time;
	var premiumCustomer=0;
	if(document.getElementById('shrs').value=="Now"){
		time = '2525';
	}  else {
		time = document.getElementById('shrs').value;
		premiumCustomer=2;
	}	
	var dispComments="";
	if(document.getElementById('Comments1').value!="Temporary Comments"){
		dispComments=document.getElementById('Comments1').value;
	} 
	var comments =dispComments;
	comments=comments.replace("&","and").replace("%","pct");
	var updateStaticComment=dispComments.replace("&","and").replace("%","pct");
	var permComments="";
	if(document.getElementById('specialIns1').value!="Temporary Comments"){
		permComments=document.getElementById('specialIns1').value;
	}
	var splIns =permComments;
	splIns=splIns.replace("&","and").replace("%","pct");
	var updateDriverComments=permComments.replace("&","and").replace("%","pct");
	var tripStatus="";
	var updateAll="";
	var tripId = "";
	var masterKey = "";
	var addressKey= "";
	var addressKeyTo="";
	var advTime= "-1";
	var fromDate = "";
	var toDate = "";
	var payType="";
	var amount="";
	var account="";
	var zone="";
	var landMarkKey="";
	var noOfPassengers="1";
	var numOfCabs="1";
	var driver="";
	var cab="";
	var nextScreen="";
	var repeatJob="&sharedRide=0";
	var xmlhttp = null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if(add1!=""){
		var url = 'http://www.getacabdemo.com/TDS/OpenRequestAjax';
		var parameters='event=saveOpenRequestHTML&source=OR&saveOpenReq=saveOpenReq&phone='+phone+'&name='+name+'&sadd1='+add1+'&sadd2='+add2+'&scity='+city+'&sstate='+state+'&szip='+zip+'&sdate='+date+'&shrs='+time+'&sLongitude='+longi+'&sLatitude='+lati+'&tripId='+tripId+'&masterAddress='+masterKey+'&userAddress='+addressKey+'&userAddressTo='+addressKeyTo+'&Comments='+comments+'&specialIns='+splIns+'&eadd1='+eadd1+'&eadd2='+eadd2+'&ecity='+ecity+'&estate='+estate+'&ezip='+ezip+'&edlongitude='+elongi+'&edlatitude='+elati+'&advanceTime='+advTime+'&fromDate='+fromDate+'&toDate='+toDate+'&paytype='+payType+'&acct='+account+'&amt='+amount+'&driver='+driver+'&cabNo='+cab+'&nextScreen='+nextScreen+'&landMarkKey='+landMarkKey+'&slandmark=&elandmark=&updateStaticComment='+updateStaticComment+'&updateDriverComment='+updateDriverComments+'&queueno='+zone+'&numberOfPassengers='+noOfPassengers+'&numOfCabs='+numOfCabs+'&premiumCustomer='+premiumCustomer+'&eMail='+email+'&updateAll='+updateAll+'&tripStatus='+tripStatus+repeatJob;
		xmlhttp.open("POST",url, true);
		xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("Content-length", parameters.length);
		xmlhttp.setRequestHeader("Connection", "close");
		xmlhttp.send(parameters);
		clearOpenRequest(1);
	} else {
		document.getElementById("errorPage").innerHTML="Please provide correct address";
		document.getElementById("errorPage").style.display="block";
		document.getElementById("successPage").style.display="none";
	}
}
function clearOpenRequest(value){
	document.getElementById('phone').value="";
	document.getElementById('name').value="";
	document.getElementById('eMail').value="";
	document.getElementById('sdate').value="";
	document.getElementById('sadd1').value = "";
	document.getElementById('sadd2').value = "";
	document.getElementById('scity').value = "";
	document.getElementById('szip').value = "";
	document.getElementById('specialIns1').value="";
	document.getElementById('Comments1').value = "";
	document.getElementById('eadd1').value= "";
	document.getElementById('eadd2').value= "";
	document.getElementById('ecity').value= "";
	document.getElementById('ezip').value= "";
	document.getElementById('shrs').value='Now';
	document.getElementById('sLatitude').value="";
	document.getElementById('sLongitude').value="";
	document.getElementById('eLatitude').value="";
	document.getElementById('eLongitude').value="";
	document.getElementById('address').value="";
	document.getElementById('endAddress').value="";
	document.getElementById('ccNum').value="";
	document.getElementById('expDate').value="";
	document.getElementById('ccName').value="";
	document.getElementById("errorPage").style.display="none";
	document.getElementById("successPage").style.display="none";
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	if(month<10){
		month = "0"+month;
	}
	if(day<10){
		day = "0"+day;
	}
	var date= month+ "/" + day + "/" +year ;
	document.getElementById('sdate').value=date;
	document.getElementById("phone").focus();
	initializeMapHTML(3);
	if(value==1){
		window.open('http://www.suburbantaxiofpinellas.com/thankyou.html', '_blank');
		window.focus();
		//window.location="http://www.suburbantaxiofpinellas.com/thankyou.html";
	}
}

