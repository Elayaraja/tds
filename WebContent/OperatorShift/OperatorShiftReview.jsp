<%@page import="java.io.FileNotFoundException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>

<%@page import="com.common.util.TDSConstants"%>
<%@page import="com.tds.cmp.bean.OperatorShift" %>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>

<%
ArrayList<OperatorShift>  csregBO = (ArrayList<OperatorShift>)request.getAttribute("csregBO");  
OperatorShift operatorBean=null;
%> 


<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link type="text/css" rel="stylesheet" href="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css" 	media="screen"></link>
<script type="text/javascript" 	src="newcalender/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js"></script> 
<title>Operator Shift Review</title>
</head>
<body>
		<form name="masterForm" action="openrequest" method="post"  />
		
   <%
	AdminRegistrationBO adminBo = (AdminRegistrationBO)session.getAttribute("user");
   %>
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
        <input type="hidden" name="action" value="openrequest"/>
		<input type="hidden" name="event" value="operatorShiftReview"/>
		<input type="hidden" name="operation" value="2"/>
        <input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />

                	<h1 align="center" >Operator&nbsp;Shift&nbsp;Review</h1>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="width: 700px">
                    <tr>
                    <td >Operator&nbsp;Id</td>
                    <td>
                    	<input type="text"  name="operatorId"  value=""> 
                    </td>
                    <td >Date</td>
                    <td>
                    	<input type="text"  name="date"  value="" onclick="displayCalendar(document.masterForm.date,'mm/dd/yyyy',this)"> 
                    </td>
                <tr>
				<td colspan="7" >
					<div class="wid60 marAuto padT10">
                        <div class="btnBlue">
                        	<div class="rht">
                        	 <input type="submit" name="getDetails" value="Get Details" class="lft">
                        	 </div>
                            <div class="clrBth"></div>
                        </div>
                        <div class="clrBth"></div>
                    </div>			
			  	</td>
				</tr> 
				<%if(csregBO!=null && csregBO.size() > 0){ %> 
				<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">                    
            <tr>
            <td>
         	<table id="deleteZone" style="width: 650px" id="bodypage" width="540">
				<tr><td style="font-weight: bold;background-color: #5CB3FF" align="center">Operator Id</td>
				<td style="font-weight: bold;background-color: #5CB3FF" align="center">Date</td>
				<td style="font-weight: bold;background-color: #5CB3FF" align="center">Time</td>
				<td style="font-weight: bold;background-color: #5CB3FF" align="center">Opened By</td>
				<td style="font-weight: bold;background-color: #5CB3FF" align="center">Closed By</td>
				<td style="font-weight: bold;background-color: #5CB3FF" align="center">Status</td></tr>
				<% 
	boolean colorLightGreen = true;
	String colorPattern;%>
				<%for(int count = 0; count < csregBO.size(); count++) { 

		colorLightGreen = !colorLightGreen;
		if(colorLightGreen){
			colorPattern="style=\"background-color:lightgreen\""; 
			}
		else{
			colorPattern="style=\"background-color:lightgrey;\"";
		}
		operatorBean = (OperatorShift) csregBO.get(count);
	%>
	<tr>
				<td <%=colorPattern%> align="center">
				<%=operatorBean.getOperatorId()==null?"":operatorBean.getOperatorId() %>
				</td>
				<td <%=colorPattern%> align="center">
				<%=operatorBean.getCloseTime()==null?"":operatorBean.getCloseTime() %>
				</td>
				<td <%=colorPattern%> align="center">
				<%=operatorBean.getOpenTime()==null?"":operatorBean.getOpenTime() %>
				</td>
				<td <%=colorPattern%> align="center">
				<%=operatorBean.getOpenedBy()==null?"":operatorBean.getOpenedBy() %>
				</td>
				<td <%=colorPattern%> align="center">
				<%=operatorBean.getClosedBy()==null?"":operatorBean.getClosedBy() %>
				</td>
				<%if(operatorBean.getStatus().equalsIgnoreCase("A")){ %>
				<td <%=colorPattern%> align="center">Available</td>
				<%} else if(operatorBean.getStatus().equalsIgnoreCase("B")){ %>
				<td <%=colorPattern%> align="center">Break</td>
				<%} %>
</tr>
				<%} %>
			  </table></td></tr>
			</table>
            <%} %></body></html>     	
                    	
                    	
                    	
                    	
                    	
                    