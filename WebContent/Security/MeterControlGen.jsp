
<%@page import="com.tds.cmp.bean.CallerIDBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<title>Create  Password For Meter Control</title>
<script type="text/javascript">
$(document).ready( function(){
	var xmlhttp=null;
	if (window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	} else {
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	var url='SystemSetupAjax?event=meterriggenflags';
	xmlhttp.open("GET", url, false);
	xmlhttp.send(null);
	var text=xmlhttp.responseText;
	var jsonObj = "{\"flagDetails\":"+text+"}";
	var obj = JSON.parse(jsonObj.toString());
	$("#getup").append("<table id=\"bodypage\" width=\"750\" >");
	$("#getup").append("<tr><td>Password</td><td><input type=\"text\" name=\"password\" id=\"password\" value=\""+obj.flagDetails[0].password+"\"></td></tr>");
});
</script>
</head>
<body>
	<form name="SecurityAction" action="control" method="post" >
		<input type="hidden" name="action" value="Security" />
		<input type="hidden" name="event" value="meterIdGen" />
		<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
		<c class="nav-header"><center>Create Password</center></c>
		<% if(request.getAttribute("meterId")!=null){
			String callBean=(String)request.getAttribute("meterId");
		 %>
	        <table id="bodypage" width="750" >
				<tr>
					<td>Password</td>
					<td>
						<input type="text" name="password" id="password" value="<%=callBean==null?"":callBean%>"  />
					</td>
				</tr>
			</table>
			<%}else{%>
				<div id="getup"></div>
			<%} %>
		<input type="submit" name="button" value="Generate Password" style="width:150px; height:25px; border:2px blue; background: skyblue; font-weight: bold" align="middle" />
	</form>
</body>
</html>