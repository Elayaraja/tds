
<%@page import="com.tds.security.bean.TagSystemBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tds.cmp.bean.DriverDeactivation"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
	<link href="css/CalendarControl.css" rel="stylesheet" type="text/css">
	<script src="js/CalendarControl.js" language="javascript"></script>
	<script>

		function dateValidation() {
			var fromdate = document.getElementById("StartDate").value;
			var todate = document.getElementById("ToDate").value;
			var fromDate = fromdate.substring(6, 10) + fromdate.substring(0, 2)
					+ fromdate.substring(3, 5);
			var toDate = todate.substring(6, 10) + todate.substring(0, 2)
					+ todate.substring(3, 5);
			if (Number(toDate) > Number(fromDate)) {
				document.getElementById("buttonValue").value = "Generate Password";
				document.SecurityAction.submit();
				return true;
			} else {
				alert("Check Your Start And End Date");
				document.getElementById('StartDate').value = "";
				document.getElementById('ToDate').value = "";
				return false;
			}
		}
		function checkFields(){
			
			if(document.getElementById("userId").value!="" ){
				document.getElementById("passgen").disabled=false;
			}
		}
		
		function delrow(){
			try {
				if(document.getElementById("size").value>0){
			 var table = document.getElementById("ipSummary");
			 var rowCount = table.rows.length;
			 var temp=(Number(document.getElementById("size").value)-1);
			document.getElementById('size').value=temp;
			rowCount--;
				
			table.deleteRow(rowCount);
				}
			}catch(e){alert(e.message);}
			
			
		}

		function cal(){
			var i=Number(document.getElementById("size").value)+1;
			document.getElementById('size').value=i;
			var table = document.getElementById("ipSummary");
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			 var cell = row.insertCell(0);
			  var cell1 = row.insertCell(1);
		 
			  
			  var element = document.createElement("input");
			  element.type = "text";
			  element.name="IPAddress"+i;
			  element.id="IPAddress"+i;
			  element.align='right'; 
			  element.size = '10';
			  cell.appendChild(element);
			  
			  
			  var element1 = document.createElement("input");
			  element1.type = "text";
			  element1.name="expireDate"+i;
			  element1.id="expireDate"+i;
			  element1.align='right'; 
			  element1.size = '10';
			  element1.value="";
			  element1.onfocus=function(){showCalendarControl(this);};
			  cell1.appendChild(element1);
			  
			}
		function deleteIP(ipAddress,x){
			  var xmlhttp=null;
				if (window.XMLHttpRequest)
				{
					xmlhttp = new XMLHttpRequest();
				} else {
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
			url='SystemSetupAjax?event=deleteIP&module=systemsetupView&delete=YES&ipAddress='+ipAddress;
			xmlhttp.open("GET", url, false);
			xmlhttp.send(null);
			var text = xmlhttp.responseText;
			if(text=="Deleted Successfully"){
				var table=document.getElementById("ipSummary");
				table.deleteRow(x.parentNode.parentNode.rowIndex);
			}
		} 
	</script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tag Computers</title>
</head>
<body>
<%ArrayList<TagSystemBean> ipAddressList= (ArrayList<TagSystemBean>)(request.getAttribute("ipAddressList")==null?new ArrayList<TagSystemBean>():request.getAttribute("ipAddressList"));%>
	<% String pass=(String)request.getAttribute("password"); %>
	 <%String result="";
			if(request.getAttribute("page")!=null) {
			result = (String)request.getAttribute("page");
			}
			%>
			<div id="errorpage" style="">
			<%=result.length()>0? result:"" %>
			</div> 
	<form name="SecurityAction" action="control" method="post"  />
	<input type="hidden" name="action" value="Security" />
	<input type="hidden" name="event" value="passKeyGen" />
	<input type="hidden" name="buttonValue" id="buttonValue" value="" />
	<input type="hidden" name="size" id="size" value=""/>
	<input type="hidden" name="module" id="module" value="<%=request.getParameter("module")==null?"":request.getParameter("module")%>" />
	<div class="leftCol"></div>
	<div class="clrBth"></div>
                <div class="rightCol">
				<div class="rightColIn">
	<c class="nav-header"><center>TAG COMPUTERS</center></c>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab">
		<tr>
        <td>
        <table id="bodypage" width="750" >
        <tr>
		<td>Start Date</td>
		<td><input type="text" name="StartDate" id="StartDate" value="<%=request.getParameter("StartDate")==null?"":request.getParameter("StartDate") %>" readonly="readonly" onfocus="showCalendarControl(this);" onblur="checkFields()"/></td> 
		<iframe width="20" height="178" name="gToday:normal:agenda.startdate" id="gToday:normal:agenda.startdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
		</iframe>
		<td>End Date</td>
		<td><input type="text" name="ToDate" id="ToDate" value="<%=request.getParameter("ToDate")==null?"":request.getParameter("ToDate") %>" readonly="readonly" onfocus="showCalendarControl(this);" onblur="checkFields()" /></td>
		<iframe width="20" height="178" name="gToday:normal:agenda.startdate" id="gToday:normal:agenda.startdate" src="calender/WeekPicker/ipopeng.htm" scrolling="no" frameborder="0" style="visibility: visible; z-index: 999; position: absolute; top: -500px; left: -500px;">
		</iframe>
		</tr>
		<tr>
		<td >UserId</td>
		<td><input type="text" name="userId" id="userId" value="<%=request.getParameter("userId")==null?"":request.getParameter("userId") %>" onblur="checkFields()"  /></td>
		<td class="firstCol">Generated Password</td>
		<td><input type="text" name="password" id="password" value="<%=request.getParameter("password")==null?"":""+pass %>" onblur="checkFields()" /></td>
		</tr>
		<tr>
		<td colspan="7" align="center"> <input type="button" name="button" id="passgen" disabled="disabled" value="Generate Password" onclick="return dateValidation()" style="width:150px; height:25px; border:2px blue; background: skyblue; font-weight: bold" align="middle" /></td>
		</tr>
		<tr>
		<td colspan="7" align="center"> <input type="submit" name="tag" value="Tag this computer" style="width:150px; height:25px; border:2px blue; background: skyblue; font-weight: bold" align="middle"/></td>
		</tr>
		  
	</table></td></tr></table>
	  	<table width="750" style="left: 80px;" align="center" id="ipSummary">
       	<tr >
       	 <th width="50%" >IPAddress</th>
          <th width="50%" >Exp. Date</th>
       	</tr>
       	<% 
	boolean colorLightGreen = true;
	String colorPattern;%>


								<%
								for(int i=0;i<ipAddressList.size();i++){
								colorLightGreen = !colorLightGreen;
								if(colorLightGreen){
									colorPattern="style=\"background-color:lightgreen\""; 
									}
								else{
									colorPattern="";
								}
							%>
							<tr>

									<td ><input type="text" size="10" readonly="readonly" id="ip_<%=i%>" value="<%=ipAddressList.get(i).getIpAddress() %>"/></td>
									<td ><input type="text" size="10" readonly="readonly" id="ip_<%=i%>" value="<%=ipAddressList.get(i).getExpDate() %>"/></td>
									<td><input type="image"
										name="imgDeleteIP" id="imgDeleteIP" value="Delete"
										onclick="deleteIP('<%=ipAddressList.get(i).getIpAddress() %>',this)"></input>
									</td>
								</tr>
								
								<%} %>
								
       	
       	</table>
		<table>
      		<tr>
				<td><input type="button" name="Button" value="ADD" onclick="cal()" class="lft"></input></td>
				<td><input type="button" name="Button" value="Remove"  onclick="delrow()" class="lft" ></input></td>
				<td><input type="submit" name="button" class="lft" value="Submit"/></td>
			</tr>
      </table> 
	</div></div>
</body>
</html>