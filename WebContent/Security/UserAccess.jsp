<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.tds.security.bean.UserAccessRights"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.security.bean.Module_detail"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
function checkFollow(source,target)
{
	//alert(source);
	//alert(document.getElementById(source).value);
	if(document.getElementById('s_'+source).checked){
		for(var i = parseInt(source);i<=parseInt(document.getElementById(source).value);i++)
		{
			if(document.getElementById('s_'+i)!=null)
	 			document.getElementById('s_'+i).checked = true;
		}
	} else {
		for(var i = parseInt(source);i<=parseInt(document.getElementById(source).value);i++)
		{
			if(document.getElementById('s_'+i)!=null)
	 			document.getElementById('s_'+i).checked = false;
		}
	}
}
function checkAll(source){
	document.getElementById('s_'+source).checked=true;
	for(var i = parseInt(source);i<=parseInt(document.getElementById(source).value);i++)
	{
		if(document.getElementById('s_'+i)!=null)
 			document.getElementById('s_'+i).checked = true;
	}
}
function unCheckAll(source){
	document.getElementById('s_'+source).checked=false;

	for(var i = parseInt(source);i<=parseInt(document.getElementById(source).value);i++)
	{
		if(document.getElementById('s_'+i)!=null)
 			document.getElementById('s_'+i).checked = false;
	}
}
function checkUserValidation(){
	if(document.getElementById("newUser").value==""){
		alert("Select Any User And Then Submit");
		return false;
	}
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>

<body>
<%
	ArrayList al_list = (ArrayList)request.getAttribute("al_list");
%>
<form name="masterForm" action="control" method="post">
<input type="hidden" name="action" value="userrolerequest">
<input type="hidden" name="event" value="getUserList">
<input type="hidden" name="size" value="<%=al_list!=null?al_list.size():"0"%>">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
<div class="wrapper1">
	<div class="wrapper2">
    	<div class="wrapper3">
         
            
            <div class="contentDV">
            <div class="contentDVInWrap">
            <div class="contentDVIn">
            	<div class="leftCol">
                	
                    <div class="clrBth"></div>
                </div>
                
              
               <table width="100%" border="0" cellspacing="0" cellpadding="0" class="driverChargTab" align="center">
                    <%
					String error = (String) request.getAttribute("error");
					%>
					<tr>
					<td colspan="7" align="center">		
						<div id="title">
							<h2 style="background-color:#fcd374"> User Access Privileges </h2>
						</div>
					</td>		
				</tr>
					<tr>
					<td colspan="7">
						<div id="errorpage">
							<%= (error !=null && error.length()>0)?"You must fullfilled the following error<br>"+error :"" %>
						</div>
					</td>
					</tr>
					<tr>
					<td colspan="5" align="center"><B>User:</B><%=request.getParameter("uname") %><input type="hidden" name="userid" value="<%=request.getParameter("userid") %>"><input type="hidden" name="uname" value="<%=request.getParameter("uname") %>"></td>
					</tr>
					
					<tr>
					<td>
						 
					<table border="1" class="driverChargTab">
					<tr><td>
					<input type="button" value="Select All" onclick="checkAll(1000)"/>
					<input type="button" value="UnSelect All" onclick="unCheckAll(1000)"/>
					</td>
					<td colspan="3">
					User Name:<input type="text" name="newUser" id="newUser" value="">
					
					<input type="submit" name="copyOrImage" value="Copy/Image" onclick="return checkUserValidation()">
					</td>
					</tr>
					 <%
							int setTD = 0,setTR=0,setTRcounter=0;
					 %>
					 <%String module = ""; %>				
					 <%for(int i=0;i<al_list.size();i++){ %>
					 <%if(i==0 || setTRcounter == 0){ %>
					 <tr> <%} %>
					 <%Module_detail rights = (Module_detail)al_list.get(i); %>
					 <%if(setTD == 0){ setTRcounter = setTRcounter+1;%>
					 <td valign="top">
					 
						 	<table class="driverChargTab" >
								<tr>
								<td>
									<B><%=rights.getModule_name() %></B>
									<input type="hidden" name="module_no<%=i %>" value="<%=rights.getModule_no() %>">
									<% module = ""+rights.getModule_no(); %>
								</td>
								<td>	
									<input  type="checkbox" id="s_<%=rights.getModule_no() %>" name="access_<%=i %>" <%=rights.isAccess()?"checked":"" %> onclick="checkFollow('<%=rights.getModule_no() %>','')">		
								</td>
								</tr>
										 		
									<%setTD= rights.isChange()?0:1; %>
									<%=rights.isChange()?"<input type='hidden' id='"+module+"' value='"+rights.getModule_no()+"' ></td></table>":"" %>
									<%if(setTRcounter ==3 && rights.isChange()){ setTD=0;setTR=0;setTRcounter=0;out.print("</tr>"); }%>
									<%}else { %>
								<tr>
								<td>
									 <%=rights.getModule_name() %>
									 <input type="hidden" name="module_no<%=i %>" value="<%=rights.getModule_no() %>">
								</td>
								<td>	
									 <input  type="checkbox" id="s_<%=rights.getModule_no() %>" name="access_<%=i %>" <%=rights.isAccess()?"checked":"" %>>		
								</td>
								</tr>
									<%setTD= rights.isChange()?0:1; %>			
									<%=rights.isChange()?"<input type='hidden' id='"+module+"' value='"+rights.getModule_no()+"' ></td></table>":"" %>
								    <%if(setTRcounter ==3 && rights.isChange()){ setTD=0;setTR=0;setTRcounter=0;out.print("</tr>"); }%>
										<%}%>			 	
								 <%}%>	 	
						</table>
						
						</td>
						</tr>
						<tr>
					<td colspan="3" align="center">
									<div class="wid60 marAuto padT10">
                        			<div class="btnBlue">
                        			<div class="rht">
                        			<input type="submit" name="Button" value="Submit" class="lft">
                        			</div>
                            		<div class="clrBth"></div>
                        			</div>
                        			<div class="clrBth"></div>
                    				</div> 
					</td>
					</tr>
					<tr>
					</tr>
					
				</table>
					
				</td>
				</tr>
				</table>
					
						 	
                <div class="clrBth"></div>
               
            </div>
            </div>
            </div>
           
		</div>
	</div>
</div>

</form>
</body>
</html>
