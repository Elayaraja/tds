<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.tds.security.bean.UserAccessRights"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://ajaxtags.org/tags/ajax" prefix="ajax"%>
<%@page import="com.tds.security.bean.Module_detail"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" type="text/css" href="css/ajaxtags.css" />
<link href="css/CalendarControl.css" rel="stylesheet" type="text/css"/>
<script src="js/CalendarControl.js" type='text/javascript'></script>
<script type='text/javascript' src="jqueryNew/jquery-1.7.1.min.js"></script>
<script type='text/javascript' src="jqueryNew/jquery-ui-1.8.17.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" 	src="jqueryNew/bootstrap.js"></script> 
<script type="text/javascript" 	src="jqueryNew/bootstrap.min.js"></script> 
<script type="text/javascript" 	src="jqueryNew/graphDemo.js"></script> 
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
function checkFollow(source,target)
{
	//alert(source);
	//alert(document.getElementById(source).value);
	if(document.getElementById('s_'+source).checked==true){
		for(var i = parseInt(source);i<=parseInt(document.getElementById(source).value);i++)
		{
			if(document.getElementById('s_'+i)!=null)
	 			document.getElementById('s_'+i).checked = true;
		}
	} else {
		for(var i = parseInt(source);i<=parseInt(document.getElementById(source).value);i++)
		{
			if(document.getElementById('s_'+i)!=null)
	 			document.getElementById('s_'+i).checked = false;
		}
	}
}
function checkAll(source){
	for(var i = 1000;i<=parseInt(source);i++)
	{
		if(document.getElementById('s_'+i)!=null)
 			document.getElementById('s_'+i).checked = true;
	}
}
function unCheckAll(source){
	for(var i = 1000;i<=parseInt(source);i++)
	{
		if(document.getElementById('s_'+i)!=null)
 			document.getElementById('s_'+i).checked = false;
	}
}
function checkUserValidation(){
	if(document.getElementById("newUser").value==""){
		alert("Select Any User And Then Submit");
		return false;
	}
}
var myLayout; // a var is required because this page utilizes: myLayout.allowOverflow() method



function closeProperties()
{
	window.location.href="/TDS/control";
}
function showDesc(row_id){
	$("#descriptionModule").html(document.getElementById("module_desc"+row_id).value).show();
}

</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>

<body>
<%
	ArrayList al_list = (ArrayList)request.getAttribute("al_list");
%>
<form name="masterForm" action="control" method="post">
<input type="hidden" name="action" value="userrolerequest">
<input type="hidden" name="event" value="getUserList">
<input type="hidden" name="size" id="size" value="<%=al_list!=null?al_list.size():"0"%>">
<input  type="hidden"   name="module"    id="module"  value="<%=request.getParameter("module")==null?"":request.getParameter("module") %>"     />
	<c class="nav-header"><center>User Access Privileges</center></c>
	<h4 class="carousel-caption" align="center" style="color: white;vertical-align: top;"><B>User:</B><%=request.getParameter("uname") %>
	<input type="hidden" name="userid" value="<%=request.getParameter("userid") %>">
	<input type="hidden" name="uname" value="<%=request.getParameter("uname") %>"></h4>
	<div class="navbar-static-top">
               <table align="center" class="thumbnail">
                    <%
					String error = (String) request.getAttribute("error");
					%>
					<tr>
					<td colspan="7">
						<div id="errorpage" class="alert-error">
							<%= (error !=null && error.length()>0)?"You must fullfilled the following error<br>"+error :"" %>
						</div>
					</td>
					</tr>
					<tr>
					<td>
					<table border="1">
					<tr><td colspan="4" align="center">
					<input type="button" value="Select All" onclick="checkAll(9000)"/>
					<input type="button" value="UnSelect All" onclick="unCheckAll(9000)"/>
					
					User Name:<input type="text" name="newUser" id="newUser" value="">
					
					<input type="submit" name="copyOrImage" value="Copy/Image" onclick="return checkUserValidation()">
					</td>
					</tr>
					 <%
							int setTD = 0,setTR=0,setTRcounter=0;
					 %>
					 <%String module = ""; %>				
					 <%for(int i=0;i<al_list.size();i++){ %>
					 <%if(i==0 || setTRcounter == 0){ %>
					 <tr> <%} %>
					 <%Module_detail rights = (Module_detail)al_list.get(i); %>
					 <%if(setTD == 0){ 
						 setTRcounter = setTRcounter+1;%>
					 <td valign="top">
						 	<table>
								<tr>
								<td>
									<B><%=rights.getModule_name() %></B>
									<input type="hidden" name="module_no<%=i %>" value="<%=rights.getModule_no() %>">
									<input type="hidden" name="module_desc<%=i %>" id="module_desc<%=i %>" value="<%=rights.getM_desc() %>">
									<% module = ""+rights.getModule_no(); %>
								</td>
								<td>	
									<input  type="checkbox" id="s_<%=rights.getModule_no() %>" name="access_<%=i %>" <%=rights.isAccess()?"checked":"" %> onclick="checkFollow('<%=rights.getModule_no() %>','')">		
								</td>
								</tr>
										 		
									<%setTD= rights.isChange()?0:1; %>
									<%=rights.isChange()?"<input type='hidden' id='"+module+"' value='"+rights.getModule_no()+"' ></td></table>":"" %>
									<%if(setTRcounter ==4 && rights.isChange()){ 
										setTD=0;setTR=0;setTRcounter=0;out.print("</tr>"); }%>
									<%}

					 			else { %>
								<tr>
								<td>
									 <a onmouseover="showDesc(<%=i%>)">
									 <%=rights.getModule_name() %></a>
									 <input type="hidden" name="module_no<%=i %>" value="<%=rights.getModule_no() %>">
									 <input type="hidden" name="module_desc<%=i %>" id="module_desc<%=i %>" value="<%=rights.getM_desc() %>">
								</td>
								<td>	
									 <input  type="checkbox" id="s_<%=rights.getModule_no() %>" name="access_<%=i %>" <%=rights.isAccess()?"checked":"" %> onclick="showDesc(<%=i%>)">		
								</td>
								</tr>
									<%setTD= rights.isChange()?0:1; %>			
									<%=rights.isChange()?"<input type='hidden' id='"+module+"' value='"+rights.getModule_no()+"' ></td></table>":"" %>
								    <%if(setTRcounter ==4 && rights.isChange()){ 
								    	setTD=0;setTR=0;setTRcounter=0;out.print("</tr>"); }%>
										<%}%>			 	
								 <%}%>	 	
						</table>
						</td>
						</tr>
						<tr>
							<td colspan="5" align="center">
                        			<input type="submit" name="Button" value="Submit" class="lft"/>
					</td>
					</tr>
					<tr>
					</tr>
				</table>
				</td>
				</tr>
				</table>
	</div>

</form>
</body>
</html>
