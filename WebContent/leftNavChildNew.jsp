<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="java.util.HashMap"%>
<%@page import="com.tds.tdsBO.AdminRegistrationBO"%>
<%@page import="com.common.util.TDSConstants"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body> 
<table   border="0" style="width:100%">
 <Tr> 
 <td> 
 <div   id="menudetail" >
<% 	AdminRegistrationBO adminBO = null;
			if(session.getAttribute("user") != null) { 
				adminBO= (AdminRegistrationBO)session.getAttribute("user");
%>
<%if(adminBO.getRoles().contains("1000")) { %>
 <div    class="menudetail"    id="dispatchView"  <%if(request.getParameter("module")!=null && request.getParameter("module").equals("dispatchView")){ %> style="display:block"<% } else { %> style="display:none" <% }%>>
 						<a href="#" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>DISPATCH</a>
 		
	   <ul id="verticalmenu" class="nav nav-list collapse in">
		<%if(adminBO.getRoles().contains("1036")) { %>
	    	 <li  ><a href="DashBoard?event=dispatchValues">Operator&nbsp;DashBoard</a></li>
	    <%} %>
	    <%if(adminBO.getRoles().contains("1051")) { %>
	    	<li  > <a href="control?action=openrequest&event=sharedRide" >Shared&nbsp;Ride&nbsp;Details</a>
	    	</li>
	    <%} %>
	    <%if(adminBO.getRoles().contains("1036")) { %>
	    	<li > 
	    		<a href="DashBoard?event=cannedMsg" >Canned&nbsp;Messages</a>
	    	</li>
	    <%} %>
	    <%if(adminBO.getRoles().contains("1003")) { %>
	    <li >
	    	<a href="control?action=openrequest&event=saveRequest&module=dispatchView" >Call&nbsp;Taker</a>
	    </li>
		<%}%>
		<%if(adminBO.getRoles().contains("1013")) { %>
		<li  >
			<a href="control?action=openrequest&event=operatorShift&module=dispatchView" >Operator&nbsp;Shift</a>
		</li>
		<%}%>
		<%if(adminBO.getRoles().contains("1020")) { %>
		<li >
			<a href="control?action=openrequest&event=openRequestReview&module=dispatchView" >OR Review</a>
		</li>
		<%}%>
		<%if(adminBO.getRoles().contains("1022")) { %>
                <li>
                        <a href="control?action=openrequest&event=openRequestHistoryWithLogs&module=dispatchView" >Job History & Logs</a>
                </li>
                <%}%>
                 <%if(adminBO.getRoles().contains("1026")) { %>
                <li >
                        <a href="control?action=openrequest&event=sharedJobLogs&module=dispatchView" >Shared Logs</a>
                </li>
                <%}%>
                <%if(adminBO.getRoles().contains("1023")) { %>
                <li >
                        <a href="control?action=openrequest&module=dispatchView&event=openRequestSummaryMain" >Reservation Summary</a>
                </li>
                <%}%>
                <%if(adminBO.getRoles().contains("1034")) { %>
                <li >
                        <a href="control?action=openrequest&event=fileUploadForOR&module=dispatchView" >OpenRequest File Upload</a>
                </li>
                <%}%>
                 <%if(adminBO.getRoles().contains("1036")) { %>
                <li >
                        <a href="DashBoard?event=jobDetails&jsp=yes&status=40" >Job details</a>
                </li>
                <%}%>
                
                <%if(adminBO.getRoles().contains("1052")) { %>
                <li >
                        <a href="DashBoard?event=consoleView&jsp=yes" >Console View</a>
                </li>
                <%}%>
                <%if(adminBO.getRoles().contains("1053")) { %>
                <li >
                        <a href="DashBoard?event=messageConsole&jsp=yes" >Message History</a>
                </li>
                <%}%>
                
	    </ul>
 
 </div>
<%} %>
 
<%if(adminBO.getRoles().contains("2000")) { %>
<% 
%>

 <div    class="menudetail"   id="operationView"  <%if(request.getParameter("module")!=null && request.getParameter("module").equals("operationView")){ %> style="display:block"<% } else { %> style="display:none" <% }%>>
				<a href="#" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>OPERATIONS</a>
		<%if(adminBO.getRoles().contains("2016") || adminBO.getRoles().contains("2022") || adminBO.getRoles().contains("2023") || adminBO.getRoles().contains("2021") || adminBO.getRoles().contains("2001")|| adminBO.getRoles().contains("2002") || adminBO.getRoles().contains("2008") || adminBO.getRoles().contains("2026")){ %>
	 			 <a href="#drivers" class="nav-header collapsed" data-toggle="collapse">Drivers<i class="icon-chevron-up"></i></a>
	    <%} %>  
	    	   <ul id="drivers" class="nav nav-list collapse"> 
     <%if(adminBO.getRoles().contains("2016"))  {%> 
    <li ><a href="mainNew.jsp?screen=/jsp/driverMapping.jsp&module=operationView">Driver&nbsp;Zone&nbsp;Mapping</a></li> 
      <%}if(adminBO.getRoles().contains("2022")) { %>	
		<li><a href="control?action=registration&event=CabDriverMapping&module=operationView" >Map&nbsp;Driver&nbsp;To&nbsp;Cab </a></li>
	 <%}if(adminBO.getRoles().contains("2023")) { %>	
		<li><a href="mainNew.jsp?screen=/cab/CabDriverMappingSummary.jsp&module=operationView" >Review&nbsp;Driver&nbsp;Cab&nbsp;Mapping</a></li>
   <%}if(adminBO.getRoles().contains("2021")) { %>	
     	<li><a href="mainNew.jsp?screen=/cab/driverlogout.jsp&module=operationView" >Log&nbsp;Out&nbsp;Driver</a></li>	 
  <%} if(adminBO.getRoles().contains("2001")) { %>
    	 <li><a href="mainNew.jsp?screen=/Company/DriverDeactivation.jsp&module=operationView" >Deactivate&nbsp;Driver</a></li>
   <%}if(adminBO.getRoles().contains("2002")) { %>	
    	<li><a href="mainNew.jsp?screen=/Company/DeActivationSummary.jsp&module=operationView" >Activate&nbsp;Driver</a></li>
    <%}if(adminBO.getRoles().contains("2008")) { %>	
    	<li><a href="mainNew.jsp?screen=/jsp/zoneentry.jsp&module=operationView" >Zone&nbsp;Login</a></li>
	 	<%if(adminBO.getRoles().contains("2026")) { %>	
			<li><a href="mainNew.jsp?screen=/jsp/driverLocation.jsp&module=operationView" >Driver Search</a></li>
		<%}%>
	<%}if(adminBO.getRoles().contains("2027")) { %>	
    	<li><a href="mainNew.jsp?screen=/jsp/driverLocationView.jsp&module=operationView" >Driver Location Search</a></li>
	<%}if(adminBO.getRoles().contains("2009")) { %>	
    	<li><a href="mainNew.jsp?screen=/jsp/driverZoneActivity.jsp&module=operationView" >Driver&nbsp;Zone&nbsp;Activity</a></li>
    <%} if(adminBO.getRoles().contains("2031")) { %>	
    	<li><a href="mainNew.jsp?screen=/jsp/driverZoneActivityHistory.jsp&module=operationView" >Driver&nbsp;Zone&nbsp;History</a></li>
    	
    <%} if(adminBO.getRoles().contains("2033")) { %>
    	<li><a href="mainNew.jsp?screen=/jsp/driverReports.jsp&module=operationView" >Driver&nbsp;Report</a></li>
    	
    <%}if(true) { %>	
		<li><a href="mainNew.jsp?screen=/jsp/DriverRating.jsp&module=operationView" >Driver&nbsp;Rating</a></li>
	<%} %> 
	</ul>
	
   <%if(adminBO.getRoles().contains("2005") || adminBO.getRoles().contains("2006")  ){ %>
   	 			 <a href="#cab" class="nav-header collapsed" data-toggle="collapse">Cab<i class="icon-chevron-up"></i></a>
	    <%} %> 
	    	   	  <ul id="cab"  class="nav nav-list collapse">
    <%if(adminBO.getRoles().contains("2005")) { %>	
    	<li><a href="mainNew.jsp?screen=/jsp/CabMaintenance.jsp&module=operationView" >Create&nbsp;Cab&nbsp;Maintenance</a></li>
    <%}if(adminBO.getRoles().contains("2006")) { %>	
    	<li><a href="mainNew.jsp?&screen=/jsp/MaintenanceSummary.jsp&module=operationView" >Review&nbsp;Cab&nbsp;Maintenance</a></li>
    <%}%>
    </ul>
   <%if(adminBO.getRoles().contains("2018") || adminBO.getRoles().contains("2030")){ %>
	     	 			 <a href="#disbursement" class="nav-header collapsed" data-toggle="collapse">Disbursement<i class="icon-chevron-up"></i></a>
	    <%} %>
	    <ul class="nav nav-list collapse" id="disbursement">
    <%if(adminBO.getRoles().contains("2018"))  {%> 
    <li><a href="mainNew.jsp?screen=/Company/disbursementMessage.jsp&module=operationView">Disb&nbsp;Message</a></li>
    <%}if(adminBO.getRoles().contains("2032"))  {%> 
    <li><a href="mainNew.jsp?screen=/Company/disbursementSummary.jsp&module=operationView">Disb&nbsp;Msg&nbsp;Summary</a></li>
    <%}%>
    </ul>
  <%if(adminBO.getRoles().contains("2019") || adminBO.getRoles().contains("2020") || adminBO.getRoles().contains("5308") ){ %>
	   	     	 			 <a href="#landmarks" class="nav-header collapsed" data-toggle="collapse">Landmarks<i class="icon-chevron-up"></i></a>
	    <%} %> 
	    	   	 <ul id="landmarks" class="nav nav-list collapse"> 
    <%if(adminBO.getRoles().contains("2019"))  {%> 
    <li><a href="control?action=registration&event=landmark&module=operationView">Create&nbsp;Landmark</a></li>
    <%}if(adminBO.getRoles().contains("2020")) { %>	
     	<li><a href="mainNew.jsp?screen=/cab/landmarkSummary.jsp&module=operationView" >Search&nbsp;Edit&nbsp;LandMark</a></li>
      	<%}if(adminBO.getRoles().contains("5308")) { %>	
		<li><a href="mainNew.jsp?screen=/cab/landMarkPending.jsp&module=operationView" >Approve Pending Landmark  </a></li>
	  <%}%>
</ul>
  <%if(adminBO.getRoles().contains("2013") || adminBO.getRoles().contains("2025") || adminBO.getRoles().contains("2024") || adminBO.getRoles().contains("2007") || adminBO.getRoles().contains("2003")){ %>
		 <a href="#customerCare" class="nav-header collapsed" data-toggle="collapse">Customer Care<i class="icon-chevron-up"></i></a>
	    <%} %>
  	 	 <ul id="customerCare"  class="nav nav-list collapse">
	     <%if(adminBO.getRoles().contains("2007")) { %>	
     	<li><a href="mainNew.jsp?screen=/jsp/LostAndFound.jsp&module=operationView" >Lost&nbsp;and&nbsp;Found</a></li>
    <%}if(adminBO.getRoles().contains("2003")) { %>	
     	<li><a href="mainNew.jsp?screen=/jsp/LostandFoundSummary.jsp&module=operationView" >Search&nbsp;Lost&nbsp;and&nbsp;Found</a></li> 
    <%}%>
    <%if(adminBO.getRoles().contains("2013")) { %>	
	<li><a href="control?action=registration&event=custInfo&module=operationView" >Client Account</a></li>
	
	<%}if(adminBO.getRoles().contains("2025")) { %>	
	<li><a href="mainNew.jsp?screen=/CustomerService/InsertComplaints.jsp&module=operationView" >Create&nbsp;Complaints</a></li>
	<%}if(adminBO.getRoles().contains("2024")) { %>	
	<li><a href="mainNew.jsp?screen=/CustomerService/Complaints.jsp&module=operationView" >Review&nbsp;Complaints</a></li>
	<%}if(adminBO.getRoles().contains("2074")) { %>	
	<li><a href="mainNew.jsp?screen=/jsp/LoginAttempts.jsp&module=operationView" >Login&nbsp;Attempts</a></li>
	<%}%>

</ul>
	<%if(adminBO.getRoles().contains("5321") || adminBO.getRoles().contains("5325") || adminBO.getRoles().contains("5324") ){ %>
	 		 <a href="#documentManagement" class="nav-header collapsed" data-toggle="collapse">Doc Mgmt<i class="icon-chevron-up"></i></a>
	    <%} %>
		    <ul id="documentManagement"  class="nav nav-list collapse">
	
		<%if(adminBO.getRoles().contains("5321")) { %>   
    			<li><a href="control?action=TDS_DOCUMENT_UPLOAD&event=passKeyGen&module=operationView&do=disp" >Upload Documents</a></li>
    	<%}%> 
    	 <%if(adminBO.getRoles().contains("5325")) { %>    
    			<li><a href="control?action=TDS_DOCUMENT_SEARCH&event=passKeyGen&module=operationView&do=searchOption" >Search Documents</a></li>
    	<%}%>
    	<%if(adminBO.getRoles().contains("5324")) { %>
    					<li><a href="control?action=TDS_DOCUMENT_APPROVAL&event=passKeyGen&module=operationView&do=dispAdmin" >Approve Documents</a></li>
    	<%}%>
    	<%if(adminBO.getRoles().contains("5327")) { %>
    					<li><a href="mainNew.jsp?screen=/jsp/expiredLicenses.jsp&module=operationView" >Expiring Documents</a></li>
    	<%}%>
     </ul>
     <%if(adminBO.getRoles().contains("2044")){ %>
	 		 <a href="#Details" class="nav-header collapsed" data-toggle="collapse"> Details <i class="icon-chevron-up"></i></a>
	      <%} %>
		    <ul id="Details"  class="nav nav-list collapse"> 
			<%if(adminBO.getRoles().contains("2044")){ %>
				<%--<li> <a href="control?action=openrequest&event=customerdetail&module=operationView">Customer Details</a></li> --%>
				<li> <a href="mainNew.jsp?screen=/jsp/CustomerDetail.jsp&module=operationView">Customer Details</a></li>
  			 <%} %>
<%} %>
</ul>
</div>
 
 <%if(adminBO.getRoles().contains("3000")) { %>
<div     class="menudetail"   id="financeView"   <%if(request.getParameter("module")!=null && request.getParameter("module").equals("financeView")) { %> style="display:block" <%}else { %>  style="display:none" <%} %>>
  		<a href="#" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>FINANCE</a>
	    <%if(adminBO.getRoles().contains("3001") || adminBO.getRoles().contains("3002") || adminBO.getRoles().contains("3021") || adminBO.getRoles().contains("3005")){ %>
			 <a href="#creditCardProcess" class="nav-header collapsed" data-toggle="collapse">CC Process<i class="icon-chevron-up"></i></a>
	    <%} %>
	    <ul id="creditCardProcess"  class="nav nav-list collapse" >
    <%if(adminBO.getRoles().contains("3001")) { %>
    	<li><a href="mainNew.jsp?screen=/CreditCardProcess/ManualCreditCardProcess.jsp&module=financeView" >Process&nbsp;CC</a></li>
    <%}if(adminBO.getRoles().contains("3002")) { %>
    	<li><a href="mainNew.jsp?screen=/CreditCardProcess/TransactionSummary.jsp&module=financeView" >Reverse&nbsp;CC TXNs</a></li>
     <%}if(adminBO.getRoles().contains("3021")) { %>
    	<li><a href="mainNew.jsp?screen=/CreditCardProcess/TransactionSummary.jsp&module=financeView" >Review CC TXNs</a></li>
   	<%}if(adminBO.getRoles().contains("3005")) { %>
    	<li><a href="mainNew.jsp?screen=/CreditCardProcess/UnCaptureTransactionSummary.jsp&module=financeView" > UnCaptured&nbsp;CC&nbsp;TXNS</a></li>
     <%}%>
     </ul>
      <%if(adminBO.getRoles().contains("3003") || adminBO.getRoles().contains("3004") || adminBO.getRoles().contains("3006") ){ %>
			 <a href="#voucherProcess" class="nav-header collapsed" data-toggle="collapse">Voucher Process<i class="icon-chevron-up"></i></a>
	    <%} %>
	    <ul id="voucherProcess" class="nav nav-list collapse" >
   	<%if(adminBO.getRoles().contains("3003")) { %>	
    	<li><a href="mainNew.jsp?screen=/jsp/CreateVoucher.jsp&module=financeView" >Create&nbsp;Account</a></li>
    <%}if(adminBO.getRoles().contains("3004")) { %>	
    	<li><a href="mainNew.jsp?screen=/jsp/EditVoucher.jsp&module=financeView" >Change&nbsp;Account</a></li>
    <%}if(adminBO.getRoles().contains("3006")) { %>	    		
    	<li><a href="mainNew.jsp?screen=/jsp/uncapturedamtdetails.jsp&module=financeView" >Tag&nbsp;Voucher&nbsp;Payment</a></li>
    <%}%>
    </ul>
     <%if(adminBO.getRoles().contains("3007") || adminBO.getRoles().contains("3008") || adminBO.getRoles().contains("2004") || adminBO.getRoles().contains("3025") || adminBO.getRoles().contains("6000") ){ %>
	  			 		 <a href="#driverDisbursements" class="nav-header collapsed" data-toggle="collapse">Disbursements<i class="icon-chevron-up"></i></a>
	    <%} %>
	    		    <ul id="driverDisbursements"  class="nav nav-list collapse">
    <%if(adminBO.getRoles().contains("3007")) { %>	
    	<li><a href="ChargesAction?event=showDriverDisbursement&module=financeView" >Driver&nbsp;Disbursement</a></li>
    <%}if(adminBO.getRoles().contains("3008")) { %>	
    	<li><a href="mainNew.jsp?screen=/charges/DriverDisbursementSummary.jsp&module=financeView" >Review&nbsp;Driver&nbsp;Disbursement</a></li> 
    <%}if(adminBO.getRoles().contains("2004")) { %>	
    	<li><a href="mainNew.jsp?screen=/Company/DriverPenality.jsp&module=financeView" >Misc&nbsp;Driver&nbsp;Credit/Charge</a></li>
    <%}if(adminBO.getRoles().contains("3025")) { %>	
		<li><a href="mainNew.jsp?screen=/Financial/ReversePayment.jsp&module=financeView" >Reverse&nbsp;Disbursement</a></li>
   	<%}if(adminBO.getRoles().contains("6000")) { %>
				<li><a href="mainNew.jsp?screen=/CSVUpload.jsp&module=financeView" >Upload Misc Charges/Credits</a></li>
	<%}if(adminBO.getRoles().contains("3213")) { %>
	<li><a href="mainNew.jsp?screen=/Financial/PrepaidCard.jsp&module=financeView" >DriverCard Details</a></li>
			 <%} %>
	</ul>
	<%if(adminBO.getRoles().contains("3022") || adminBO.getRoles().contains("3100") || adminBO.getRoles().contains("3024") || adminBO.getRoles().contains("3023") ){ %>
	 		 <a href="#payment" class="nav-header collapsed" data-toggle="collapse">Payment<i class="icon-chevron-up"></i></a>
	    <%} %>
	    	    		    <ul id="payment"  class="nav nav-list collapse">
    <%if(adminBO.getRoles().contains("3022")) { %>
		<li><a href="mainNew.jsp?screen=/CreditCardProcess/manualSettleDriver.jsp&module=financeView" >Manual Settlement</a></li>	 
	<%}if(adminBO.getRoles().contains("3100")) { %>
		<li><a href="mainNew.jsp?screen=/Financial/MultipleCharges.jsp&module=financeView" >Create&nbsp;Multipe&nbsp;Charges</a></li>	 
	<%}if(adminBO.getRoles().contains("3024")) { %>
		<li><a href="mainNew.jsp?screen=/Financial/ChequeDetail.jsp&module=financeView" >Mark&nbsp;Paid&nbsp;Cheques</a></li>	 
    <%}if(adminBO.getRoles().contains("3023")) { %>
		<li><a href="mainNew.jsp?screen=/Financial/CashRegister.jsp&module=financeView" >Cash&nbsp;Register</a></li>	 
 	<%} %>
 	</ul>
 		<%if(adminBO.getRoles().contains("3061") || adminBO.getRoles().contains("3062") ){ %>
	   		 		 <a href="#invoiceProcess" class="nav-header collapsed" data-toggle="collapse">Invoice Process<i class="icon-chevron-up"></i></a>
	    <%} %>
	   	    	    		    <ul id="invoiceProcess"  class="nav nav-list collapse">
 	<%if(adminBO.getRoles().contains("3061")) { %>
		<li><a href="mainNew.jsp?screen=/Financial/GenerateInvoice.jsp&module=financeView" >Generate&nbsp;Invoice</a></li>	 
    <%}if(adminBO.getRoles().contains("3062")) { %>
		<li><a href="mainNew.jsp?screen=/Financial/InvoicePayment.jsp&module=financeView" >Mark&nbsp;For&nbsp;Invoice&nbsp;Payment</a></li>	 
 
    <%} %>
   </ul>
   	    <%if(adminBO.getRoles().contains("3200") || adminBO.getRoles().contains("3201") || adminBO.getRoles().contains("3202") ){ %>
	   	   		   		 		 <a href="#chargeProcess" class="nav-header collapsed" data-toggle="collapse">Charges<i class="icon-chevron-up"></i></a>
	   	   	
	    <%} %>
 	    		    <ul id="chargeProcess"  class="nav nav-list collapse">
    <%if(adminBO.getRoles().contains("3200")) { %>
    	<li><a href="control?action=chargesAction&event=chargeType&module=financeView" >Create&nbsp;Charge&nbsp;Type</a></li>
    <%}if(adminBO.getRoles().contains("3201")) { %>
    	<li><a href="control?action=chargesAction&event=chargeTemplateMaster&module=financeView" >Create&nbsp;Charge&nbsp;Template</a></li>
     <%}if(adminBO.getRoles().contains("3202")) { %>
    	<li><a href="control?action=chargesAction&event=chargeTemplateDetail&module=financeView" >Edit&nbsp;Charge&nbsp;Template</a></li>
     <%}%>
     <% if(adminBO.getRoles().contains("3208")) { %>
    	<li><a href="control?action=chargesAction&event=addLeaseRates&module=financeView" >Lease Setup</a></li>
     <%}%>
 <% if(adminBO.getRoles().contains("3210")) { %>
    	<li><a href="control?action=chargesAction&event=addMiscCharges&module=financeView" >Misc Setup</a></li>
     <%}%><% if(adminBO.getRoles().contains("3211")) { %>
    	<li><a href="mainNew.jsp?screen=/charges/miscChargeUpload.jsp&module=financeView" >Upload Misc Charges</a></li>
     <%}%>
     </ul>
</div>
<%} %>
<%if(adminBO.getRoles().contains("4000")) { %>
 <div id="reportView"  style="display:none"  >
  <ul id="verticalmenu" class="glossymenu">
	<li><a href="control?action=openrequest&event=Report"  >Reports</a></li>
	</ul>
	</div>
<%} %>
<div  id="logoutView"  style="display:none" >
  <ul id="verticalmenu" class="glossymenu">
<li><a href="control?action=registration&event=logout" >Logout</a></li>
</ul>
</div>
<%if(adminBO.getRoles().contains("5000")) { %>
<div id="systemsetupView"  class="menudetail"     <%if(request.getParameter("module")!=null && request.getParameter("module").equals("systemsetupView")) { %> style="display:block" <%}else { %>  style="display:none" <%} %>>
  		<a href="#" class="nav-header" data-toggle="collapse"><i class="icon-dashboard"></i>SYSTEM SETUP</a>
		<%if(adminBO.getRoles().contains("5102") || adminBO.getRoles().contains("5106")|| adminBO.getRoles().contains("5103") || adminBO.getRoles().contains("5104") || adminBO.getRoles().contains("5105") || adminBO.getRoles().contains("5207") || adminBO.getRoles().contains("5121")  || adminBO.getRoles().contains("5321") || adminBO.getRoles().contains("5324") || adminBO.getRoles().contains("5325") ) { %>
	 <a href="#userAdmin" class="nav-header collapsed" data-toggle="collapse">Users<i class="icon-chevron-up"></i></a>
	
		    <ul id="userAdmin"  class="nav nav-list collapse">
			<%if(adminBO.getRoles().contains("2015"))  {%> 
    			<li><a href="mainNew.jsp?screen=/jsp/MasterRegistrationJQuery.jsp&module=systemsetupView">Driver/Operator&nbsp;Registration</a></li> 
			<%}if(adminBO.getRoles().contains("5102")) { %>
		    	<li><a href="mainNew.jsp?screen=/jsp/SummaryDriverRegistration.jsp&module=systemsetupView" >Update&nbsp;Driver</a> </li>
		     <%}if(adminBO.getRoles().contains("5106")) { %>
		    	<li><a href="mainNew.jsp?screen=/jsp/AdminRegViewDetails.jsp&module=systemsetupView" >Update&nbsp;Operators</a></li>
		    <%}if(adminBO.getRoles().contains("5103")) { %>
		    	<li><a href="mainNew.jsp?screen=/jsp/userList.jsp&module=systemsetupView" >View/Change User&nbsp;Permissions</a></li>
		    <%}if(adminBO.getRoles().contains("5104")) { %><!--
		    	<li><a href="control?action=registration&event=companyentry" >Company&nbsp;Registration</a></li>
		     --><%}if(adminBO.getRoles().contains("5105")) { %>
		    	<li><a href="control?action=registration&event=savecompany&submit=View&module=systemsetupView" >Edit Company</a></li>
		    <%} %>
		<%} %>	</ul>
		<%if(adminBO.getRoles().contains("5201") || adminBO.getRoles().contains("5202") || adminBO.getRoles().contains("5205") || adminBO.getRoles().contains("5206")) { %>
					 <a href="#zones_leftNav" class="nav-header collapsed" data-toggle="collapse">Zones<i class="icon-chevron-up"></i></a>
					    <ul id="zones_leftNav"  class="nav nav-list collapse">
			<%if(adminBO.getRoles().contains("5201")) { %>
				<li><a href="mainNew.jsp?screen=/SystemSetup/QueueCoordinate.jsp&module=systemsetupView" >Create&nbsp;Zones</a></li>
			<%}if(adminBO.getRoles().contains("5202")) { %>	
			    <li><a href="mainNew.jsp?screen=/SystemSetup/QueueCoordinateSummary.jsp&module=systemsetupView" >Review Zones</a></li>
			<%}if(adminBO.getRoles().contains("5205")) { %>	
			    <li><a href="mainNew.jsp?screen=/SystemSetup/InsertAdjacentzones.jsp&module=systemsetupView" >Insert Adjacent Zones</a></li>
			<%}if(adminBO.getRoles().contains("5206")) { %>	
			    <li><a href="mainNew.jsp?screen=/SystemSetup/ReviewAdjacentzones.jsp&module=systemsetupView" >Review Adjacent Zones</a></li> 
		</ul><%} %>
		<%if(adminBO.getRoles().contains("5117") || adminBO.getRoles().contains("5118")){ %>
						 <a href="#system_cab" class="nav-header collapsed" data-toggle="collapse">Cab<i class="icon-chevron-up"></i></a>
		
		     <%}%>
		     					    <ul id="system_cab"  class="nav nav-list collapse">
		     
		<%if(adminBO.getRoles().contains("5117")) { %>	
		    <!-- 	<li><a href="mainNew.jsp?screen=/cab/CabRegistraition.jsp&module=systemsetupView&status=Insert"  >Register A Cab</a></li> -->
		    <li><a href="control?action=registration&event=cabRegistration&module=systemsetupView&status=Insert"  >Register A Cab</a></li>
		    <%}if(adminBO.getRoles().contains("5118")) { %>	
		    	<li><a href="mainNew.jsp?screen=/cab/CabRegistraitionSummary.jsp&module=systemsetupView" >Review Cabs</a></li>
		    <%} %>	
		<%} %>
		</ul>
	 	
		<%if(adminBO.getRoles().contains("5401") || adminBO.getRoles().contains("5402") || adminBO.getRoles().contains("5302")) { %>	
				 <a href="#financial" class="nav-header collapsed" data-toggle="collapse">Financial<i class="icon-chevron-up"></i></a>
			     					    <ul id="financial"  class="nav nav-list collapse">
	<%if(adminBO.getRoles().contains("5401")) { %>
				<li><a href="control?action=registration&event=cmpPaymentSettelmentSch&module=systemsetupView" >Payment Settlement Schedule</a></li>
			<%}if(adminBO.getRoles().contains("5402")) { %>	
			    <li><a href="control?action=registration&event=driverChargesMaster&module=systemsetupView" >Recurring Driver Charges</a>	</li>
			<%} %>
			<%if(adminBO.getRoles().contains("5302")) { %>
				<li><a href="control?action=registration&event=companyccsetup&module=systemsetupView" >Credit Card Charges</a></li>
			<%} %>
			<%if(adminBO.getRoles().contains("5304")) { %> 
				<li><a href="control?action=Security&event=vantivSetup&module=systemsetupView" >CC Vantiv Setup </a></li>
			<%} %>		
			</ul>
			<%} %>
			<%if(adminBO.getRoles().contains("5303") || adminBO.getRoles().contains("5320") || adminBO.getRoles().contains("5326") || adminBO.getRoles().contains("5328") || adminBO.getRoles().contains("5121") || adminBO.getRoles().contains("5207") ||  adminBO.getRoles().contains("5133")||(adminBO.getRoles().contains("2764"))) { %>	
							 <a href="#system" class="nav-header collapsed" data-toggle="collapse">System<i class="icon-chevron-up"></i></a>
				     					    <ul id="system"  class="nav nav-list collapse">
			<%if(adminBO.getRoles().contains("5303")) { %>	
				<li><a href="control?action=systemsetup&event=systemProperties&module=systemsetupView" >System &nbsp;Properties</a></li>
		<%} %> 
		<%if(adminBO.getRoles().contains("5313")) { %>	
				<li><a href="control?action=systemsetup&event=dispatchProperties&module=systemsetupView" >Dispatch &nbsp;Properties</a></li>
		<%} %> 
		<%if(adminBO.getRoles().contains("5323")) { %>	
				<li><a href="control?action=systemsetup&event=documentTypes&module=systemsetupView" >Document &nbsp;Types</a></li>
		<%} %> 
		<%if(adminBO.getRoles().contains("5413")) { %>	
				<li><a href="mainNew.jsp?screen=/SystemSetup/openRequestFieldOrderFirst.jsp&module=systemsetupView" >Upload Document Settings</a></li>
		<%} %> 
		<%if(adminBO.getRoles().contains("5414")) { %>	
				<li><a href="mainNew.jsp?screen=/SystemSetup/reviewUploadFileMapping.jsp&module=systemsetupView" >Review Upload File Mapping</a></li>
		<%} %> 
		<%if(adminBO.getRoles().contains("5424")) { %>	
				<li><a href="mainNew.jsp?screen=/SystemSetup/Fleet.jsp&module=systemsetupView" >Fleet Mapping</a></li>
		<%} %> 
		
		<%if(adminBO.getRoles().contains("5320")) { %>	
				<li><a href="control?action=Security&event=passKeyGen&module=systemsetupView" >Authorize Computers</a></li>
			<%}%> 
			
    	<%if(adminBO.getRoles().contains("5326")) { %>
    					<li><a href="control?action=phoneIVR&event=insertAsterisk&module=systemsetupView" >Phone Asterisk</a></li>
    	<%}%> 
    	<%if(adminBO.getRoles().contains("5328")) { %>
    					<li><a href="mainNew.jsp?screen=/SystemSetup/insertCC.jsp&module=systemsetupView" >CC Detail</a></li>
    	<%}if(adminBO.getRoles().contains("5207")) { %>
				<li><a href="mainNew.jsp?screen=/Security/CallerIdGen.jsp&module=systemsetupView" >Caller&nbsp;ID&nbsp;Password</a></li>
		<%}if(adminBO.getRoles().contains("5217")) { %>
		<li><a href="mainNew.jsp?screen=/Security/MeterControlGen.jsp&module=systemsetupView" >Meter&nbsp;Controller</a></li>
        <%}if(adminBO.getRoles().contains("5121")) { %>	
		    	<li><a href="control?action=registration&event=cmpyFlagSetup&module=systemsetupView" >Setup Driver Cab Properties</a></li> 		   
		<%}if(adminBO.getRoles().contains("5133")) { %>	
		    	<li><a href="control?action=registration&event=rateforZone&module=systemsetupView" >Zone Rates</a></li> 		   
		<%}%> 
	
	     	
		</ul>
	<%} %>

<%} %>

</div>

<%}else{ %>
<div   id="driverView"  style="display:none" >
  <ul id="verticalmenu" class="glossymenu">
 	 <li>
		<a href="control?action=registration&event=saveDriver&subevent=load">Driver&nbsp;Registration</a>
	</li>
	<li>
		<a href="control?action=registration&event=savecompany" >Company&nbsp;Registration</a>
	</li>

	<li>
		<a href="control?action=openrequest&event=saveRequest" >Open&nbsp;Request&nbsp;Entry</a>
	</li>
 
	<li>
		<a href="control?action=openrequest&event=getreceipt" >Get&nbsp;Receipt</a>			
	</li>

	<li>
		<a href="control?action=registration&event=showsActiveUser" >Active User</a>			
	</li>
 
 	<li>
		<a href="control?action=registration&event=checkUser" >Login</a>
	</li>
		</ul>
</div>
 <%} %>
 <div class="menudetail"       id="passengerView"  <%if(request.getParameter("module")!=null && request.getParameter("module").equals("passengerView")){ %> style="display:block"<% } else { %> style="display:none" <% }%>>
	 <ul   id="verticalmenu" class="glossymenu">
	 <li><a href="control?action=registration&event=passengerEntry&subEvent=psrEntry&module=passengerView" class="systemSetup" title="Active User"   >Passenger</a></li>
	 <li><a href="control?action=registration&event=passengerEntry&subEvent=passengerSummary&module=passengerView" class="systemSetup" title="Active User">PassengerSummary</a></li> 
	 </ul>
</div>
 
</td>
 </Tr>

</table>
</body>
</html>