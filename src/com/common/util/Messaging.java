package com.common.util;

import java.util.ArrayList;

import javax.servlet.ServletContext;

import org.apache.log4j.Category;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.process.Email;
import com.tds.process.FCMPush;
import com.tds.process.OneToOneSMS;
import com.tds.tdsBO.ApplicationPoolBO;
import com.common.util.HandleMemoryMessage;
import com.tds.util.MailUtil;



public class Messaging extends Thread{

	private final Category cat = Category.getInstance(Messaging.class.getName());

	public ArrayList<DriverCabQueueBean> p_driverIdArray;
	public String p_driverId;
	private boolean broadcastMsg = false;
	public String[] message;
	public String customerMessage = "";
	public String customerPushKey;
	private String serviceProvider = "";
	public ApplicationPoolBO poolBo;
	public String messageType = "";
	public ServletContext context = null;
	public String messageKey = "0";
	public String associationCode = "";
	public String timeZone="";
	
	public boolean useGooglePush = false;
	public boolean useIVR = false;	
	
	public Messaging(ServletContext contextVar, ArrayList<DriverCabQueueBean> pDriverId,String[] message,ApplicationPoolBO poolBo, String associationCode) {
		super();
		this.p_driverIdArray = pDriverId;
		this.message = message;
		this.poolBo = poolBo;
		this.broadcastMsg = true;
		this.serviceProvider = "Sprint";
		this.context = contextVar;
		this.associationCode = associationCode;
	}
	
	
	
	public Messaging(ServletContext contextVar, DriverCabQueueBean pDriverId,String[] message,ApplicationPoolBO poolBo, String associationCode) {
		super();
		ArrayList<DriverCabQueueBean> dlList = new ArrayList<DriverCabQueueBean>();
		dlList.add(pDriverId);
		//this(dlList, message,poolBo);
		this.p_driverIdArray = dlList;
		this.message = message;
		this.poolBo = poolBo;
		this.broadcastMsg = true;
		this.serviceProvider = "Sprint";
		this.context = contextVar;
		this.associationCode = associationCode;

	}

	public Messaging(ServletContext contextVar, ArrayList<DriverCabQueueBean> pDriverId,String[] message,ApplicationPoolBO poolBo, String messageKey, boolean useGooglePushAlone, String associationCode) {
		super();

		this.p_driverIdArray = pDriverId;
		this.message = message;
		this.poolBo = poolBo;
		this.broadcastMsg = true;
		this.serviceProvider = "Sprint";
		this.messageKey = messageKey;
		this.useGooglePush = useGooglePushAlone;
		this.context = contextVar;
		this.associationCode = associationCode;
	}

	public Messaging(ServletContext contextVar, String str,ApplicationPoolBO poolBo, DriverCabQueueBean customerBO) {
		super();
		// TODO Auto-generated constructor stub
		//System.out.println("in messaging:"+str);
		this.customerMessage = str;
		this.associationCode = customerBO.getAssocode();
		this.poolBo = poolBo;
		this.broadcastMsg = true;
		this.serviceProvider = "Sprint";
		this.context = contextVar;
		this.customerPushKey = customerBO.getGkey();
		//this.messageKey = "APA91bEe8yXzjvMrLw9qash0hT0CBm2kORl2Snb-9Tzij0qNdKcVPyB2V-I7tLZyOkcQ5Qrzz9JmGhBzl3H0Ww4hIWO4-rjqeQ5ejdQAq5rGuBtWqeam3bT1NnIdoFPrU_7tjN8lQIneLVpzrdAquKYgAew2vCJ52Q";
	}


	@Override
	public void run()
	{
		
		ArrayList sprintPhoneNumbers = new ArrayList();
		ArrayList nonSprintEmails = new ArrayList();
		ArrayList<String> googlePush = new ArrayList<String>();
		
		if(p_driverIdArray!=null && p_driverIdArray.size()>0){
			//String message in the profile
			HandleMemoryMessage.sendMessage(context, associationCode, p_driverIdArray, message[0]);
			//Since Sprint #s dont need to be converted to emails address, just extracting the phone numbers in an Array
			//Hence commented below.
			//ArrayList<DriverCabQueueBean> sprintDrivers = new ArrayList<DriverCabQueueBean>();
			ArrayList<DriverCabQueueBean> nonSprintDrivers = new ArrayList<DriverCabQueueBean>();
			MailUtil mailUtil = new MailUtil();
			//Splits the input Array based on provider.
			//Since I dont need to conver Sprint to email address, just extracting the phone numbers in an Array
			for(int i = 0; i < p_driverIdArray.size();i++){
				if(p_driverIdArray.get(i).getProvider().equalsIgnoreCase("Sprint")){
					sprintPhoneNumbers.add(p_driverIdArray.get(i).getPhoneNo());
				} else if (!p_driverIdArray.get(i).getProvider().equals("")){
					nonSprintDrivers.add(p_driverIdArray.get(i));
				}
				if(p_driverIdArray.get(i).getPushKey() != ""){
					googlePush.add(p_driverIdArray.get(i).getPushKey());
				}
			}
			//mailUtil.sendMail(poolBo.getSMTP_HOST_NAME(), poolBo.getSMTP_HOST_NAME(), poolBo.getSMTP_AUTH_PWD(), TDSProperties.getValue("mailFromAddress"), nonSprintEmails, message);
			//Sending Using googlePush
			if (googlePush.size() > 0) {
				System.out.println("MessageViaGoogle:" + googlePush.toString() + ":" + message[0]);
				FCMPush.sendMessage(context, messageKey, message[0], googlePush);
				if (useGooglePush) {
					return;
				}
			}
			if(true){
				return;
			}
			//Sending Sprint  SMS
			if(sprintPhoneNumbers.size()>0){
				cat.info("MessageViaSprint:"+sprintPhoneNumbers.toString()+":"+message);
				OneToOneSMS sprintSMS = new OneToOneSMS(sprintPhoneNumbers, message[1], poolBo, 0);
				sprintSMS.start();
			}
	
			//Converts to emails 
			
			mailUtil.translateMultipleSMSToMail(nonSprintDrivers);
			String [] nonSprintTemp = new String[nonSprintDrivers.size()];
			
			int j=0;
			for(int i = 0;i<nonSprintDrivers.size();i++){
				//nonSprintEmails.add(nonSprintDrivers.get(i).getEmailAddress());
				if(nonSprintDrivers.get(i).getEmailAddress()!=null){
					nonSprintTemp[j] = nonSprintDrivers.get(i).getEmailAddress();
					j++;
				}
			}
			String [] nonSprint = new String[j];
			for(int k=0;k<j;k++){
				nonSprint[k] = nonSprintTemp[k];
			//	System.out.println("Message is gong to be sent to "+ nonSprint[k]);
			}
			//Sending other SMS as emails
			if (nonSprint.length>0){
	
				cat.info("MessageViaNonSprint:"+nonSprint.toString()+":"+message[1]);
				//System.out.println("EmailOptions"+poolBo.getSMTP_HOST_NAME()+poolBo.getSMTP_AUTH_USER()+poolBo.getSMTP_AUTH_PWD()+poolBo.getEmailFromAddress()+poolBo.getSMTP_PORT()+poolBo.getProtocol());
				Email otherProviderSMS = new Email(poolBo.getSMTP_HOST_NAME(),poolBo.getSMTP_AUTH_USER(),poolBo.getSMTP_AUTH_PWD(),poolBo.getEmailFromAddress(),poolBo.getSMTP_PORT(),poolBo.getProtocol());
				try{
					otherProviderSMS.sendMail(nonSprint, message[1]);
				} catch (Exception e ){
					System.out.println(e.toString());
				}
			}
		}else{
				//System.out.println("MessageViaGoogleFor Customer:" + customerMessage.toString());
				//System.out.println("Google key"+customerPushKey);
				FCMPush.sendMessageCustomer(context, customerPushKey, customerMessage);
				if (useGooglePush) {
					return;
				}
				return;
		}

	}
	
}