package com.common.util;

import java.util.ArrayList;

import javax.servlet.ServletContext;

import nsidc.spheres.Point;

import com.tds.cmp.bean.ZoneTableBeanSP;

public class CheckZone {

	public static boolean isLatLongInZone(double latitude, double longitude, ZoneTableBeanSP zone){
		zone.getZoneCoord().contains(new Point(latitude, longitude));
		return false;
	}
	
	public static String checkLatLongAgainstAllZones(double latitude, double longitude, ArrayList<ZoneTableBeanSP> zones){
		
		return "";
	}
	public static ArrayList<ZoneTableBeanSP> loadAllZones(String associationCode){
		ArrayList<ZoneTableBeanSP> zones = new ArrayList<ZoneTableBeanSP>();
		return zones;	
	}
	
	public static String loadAllZonesAndCheckZone(double latitude, double longitude, String associationCode){
		ArrayList<ZoneTableBeanSP> zones = new ArrayList<ZoneTableBeanSP>();
		
		
		return "";	
	}

public static String checkZone(ArrayList<ZoneTableBeanSP> allZones, String currentZone, double latitude, double longitude){
		String zoneNumber = "";
		if(currentZone != null && !currentZone.equals("")){
			double zoneFindStartTime = System.currentTimeMillis();
			for(int i = 0; i<allZones.size(); i++){
				if(allZones.get(i).getZoneKey().equalsIgnoreCase(currentZone)){
					if(allZones.get(i).getZoneCoord().contains(new Point(latitude, longitude))){
						zoneNumber= currentZone;
						break;
					}
				} 
			}
			if(zoneNumber.equals("")){
				currentZone=null;
			}
		}
		if((currentZone == null || currentZone.equals("")) && allZones != null ){
			double zoneFindStartTime = System.currentTimeMillis();
			for(int i = 0; i<allZones.size(); i++){
				if(longitude < allZones.get(i).getEastLongitude() && longitude > allZones.get(i).getWestLongitude() && latitude < allZones.get(i).getNorthLatitude()
						&& latitude > allZones.get(i).getSouthLatitude()){
					if(allZones.get(i).getZoneCoord().contains(new Point(latitude, longitude))){
						zoneNumber = allZones.get(i).getZoneKey();
						break;
					}
				}
			}
		}
		return zoneNumber;
	}		

public static String checkZoneStatus(ArrayList<ZoneTableBeanSP> allZones, String currentZone, double latitude, double longitude){
	String zoneNumber = "";
	if(currentZone != null && !currentZone.equals("")){
		for(int i = 0; i<allZones.size(); i++){
			if(allZones.get(i).getZoneKey().equalsIgnoreCase(currentZone)){
				if(allZones.get(i).getZoneCoord().contains(new Point(latitude, longitude))){
					zoneNumber= currentZone;
					break;
				}
			} 
		}
		if(zoneNumber.equals("")){
			currentZone=null;
		}
	}
	if((currentZone == null || currentZone.equals("")) && allZones != null ){
		for(int i = 0; i<allZones.size(); i++){
			if(longitude < allZones.get(i).getEastLongitude() && longitude > allZones.get(i).getWestLongitude() && latitude < allZones.get(i).getNorthLatitude()
					&& latitude > allZones.get(i).getSouthLatitude()){
				if(allZones.get(i).getZoneCoord().contains(new Point(latitude, longitude))){
					zoneNumber = allZones.get(i).getZoneSwitch();
					break;
				}
			}
		}
	}
	return zoneNumber;
}


	public static ArrayList returnZonesForAssociatoinCode(ServletContext context, String associationCode){
		ArrayList zoneList = new ArrayList();
		ArrayList<ZoneTableBeanSP> zones = (ArrayList<ZoneTableBeanSP>) context.getAttribute(associationCode + "Zones");
		if(zones != null){
			for(int i = 0; i<zones.size();i++){
				zoneList.add(zones.get(i).getZoneKey());
				zoneList.add(zones.get(i).getZoneDesc());
			}
		}
		
		return zoneList;
	}
	
	
}
