package com.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;

import javax.servlet.ServletConfig;

import com.tds.cmp.bean.DriverVehicleBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.SecurityDAO;
import com.tds.dao.UtilityDAO;
import com.tds.process.LoadZones;
import com.tds.process.QueueProcess;

public class SystemUtils {

	public static void stopDispatchJob(ServletConfig config, String associationCode){
//		Timer stopTimer = (Timer) config.getServletContext().getAttribute(associationCode+"Timer");
//		if(stopTimer!=null){
//			stopTimer.cancel();
//			stopTimer.purge();
//			config.getServletContext().removeAttribute(associationCode+"Timer");
//			stopTimer = null;
//		}
		
		QueueProcess queueProcess = (QueueProcess) config.getServletContext().getAttribute(associationCode+"Process");
		if(queueProcess!=null){
			queueProcess.setDispatchingJobs(false);
			//config.getServletContext().removeAttribute(associationCode+"Process");
			//
			//queueProcess = null;
		}
	}
	public static void startDispatchJob(ServletConfig config, String associationCode,String masterAssoccode){
		long queueInterval = 10 * 1000  ;
		stopDispatchJob(config, associationCode);
		Timer q_timer = new Timer();	 
		QueueProcess performQueue = new QueueProcess(associationCode,masterAssoccode,config);
		q_timer.schedule(performQueue,(30*1000) , queueInterval); 		
		config.getServletContext().setAttribute(associationCode+"Timer",q_timer);
		config.getServletContext().setAttribute(associationCode+"Process",performQueue);
	}
	public static void reloadZones(ServletConfig config, String associationCode){
		long startTime = System.currentTimeMillis();
		config.getServletContext().removeAttribute(associationCode + "Zones");
		config.getServletContext().removeAttribute(associationCode + "ZonesLoaded");
		config.getServletContext().setAttribute(associationCode + "ZonesLoaded", true);
		System.out.println("ReLoading zone for company" + associationCode);
		ArrayList<ZoneTableBeanSP> zones = LoadZones.returnZonesForAssocCode(associationCode);
		config.getServletContext().setAttribute(associationCode + "Zones", zones);
	//	System.out.println("ZonesLoaded for Assoc"+associationCode+ " in "+ (System.currentTimeMillis()-startTime)+ " milliseconds");
	}

//	public static void reloadProperties(ServletConfig config,ArrayList properties){
//		for(int i=0;i<properties.size();i=i+2){
//			config.getServletContext().removeAttribute(""+properties.get(i));
//			config.getServletContext().setAttribute("" + properties.get(i), properties.get(i+1));
//		}
//	}

	public static int reloadCompanyFlags(ServletConfig config, String associationCode){
		
		config.getServletContext().removeAttribute(associationCode + "Flags");
		HashMap<String, DriverVehicleBean> companyFlags =UtilityDAO.getCmpyVehicleFlag(associationCode) ;
		if(companyFlags!=null){
			config.getServletContext().setAttribute(associationCode + "Flags", companyFlags);
		}else{
			return 0;
		}
		return 1;
	}
	public static String getCompanyFlagDescription(ServletConfig config, String associationCode,String flags,String delimiter, int shortOrLongDescription){
		HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) config.getServletContext().getAttribute(associationCode+"Flags");
		String returnDescription="";
		if(companyFlags!=null){
			for(int i=0;i<flags.length();i++){
				if(shortOrLongDescription==0){
					returnDescription = returnDescription + companyFlags.get(flags.substring(i,i+1)).getShortDesc() + delimiter;                     
				}else {
					returnDescription = returnDescription + companyFlags.get(flags.substring(i,i+1)).getLongDesc() + delimiter;                     
				}
			}
		}
		return returnDescription;
	}
}
