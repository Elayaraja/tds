package com.common.util;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tds.dao.EditHistoryJobsDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;

/**
 * Servlet implementation class EditJobs
 */
public class EditJobs extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditJobs() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			doProcess(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try{
			doProcess(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		//System.out.println("edit jobs servlet calling");
		HttpSession session = request.getSession(false);
		//System.out.println("session:"+session);
		if(session==null){
			//System.out.println("session is null");
			response.getWriter().write("Credentials not Found, Please login and try again..");
			return;
		}
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		if(adminBO==null){
			//System.out.println("adminBo is empty");
			response.getWriter().write("Credentials not Found, Please login and try again..");
			return;
		}
		if(!adminBO.getMasterAssociateCode().equalsIgnoreCase("103")){
			//System.out.println("association code not matched");
			response.getWriter().write("Sorry, You dont have access to view this page..");
			return;
		}
		String eventParam = "";
		if(request.getParameter(TDSConstants.eventParam) == null) {
			gotoHistoryJobsEditPage(request,response,adminBO);
			return;
		}
		eventParam = request.getParameter(TDSConstants.eventParam);
		if(eventParam.equalsIgnoreCase("getDetails")){
			getHostoryDetails(request,response,adminBO);
		}else if(eventParam.equalsIgnoreCase("updateDetails")){
			updateHistoryTimings(request,response,adminBO);
		}else if(eventParam.equalsIgnoreCase("updateEA")){
			updateEA(request,response,adminBO);
		}
	}

	private void updateHistoryTimings(HttpServletRequest request, HttpServletResponse response, AdminRegistrationBO adminBO) throws ServletException, IOException{
		// TODO Auto-generated method stub
		String tripId = request.getParameter("tripId");
		String stTime = request.getParameter("stdate");
		String edTime = request.getParameter("eddate");
		if(tripId==null || tripId.equals("") || stTime==null || stTime.equals("") || edTime==null || edTime.equals("")){
			response.getWriter().write("DataProblem");
			return;
		}
		int ok = EditHistoryJobsDAO.updateHistoryTimings(adminBO, tripId, stTime, edTime);
		if(ok>0){
			response.getWriter().write("Done");
		}else{
			response.getWriter().write("Failed");
		}
	}

	private void updateEA(HttpServletRequest request, HttpServletResponse response, AdminRegistrationBO adminBO) throws ServletException, IOException{
		// TODO Auto-generated method stub
		String tripId = request.getParameter("tripId");
		
		String eadd1 = request.getParameter("eadd1");
		String eadd2 = request.getParameter("eadd2");
		String ecity = request.getParameter("ecity");
		String epost = request.getParameter("epost");
		String elati = request.getParameter("elati");
		String elong = request.getParameter("elong");
		
		
		/*if(tripId==null || tripId.equals("") || stTime==null || stTime.equals("") || edTime==null || edTime.equals("")){
			response.getWriter().write("DataProblem");
			return;
		}*/
		
		int ok = EditHistoryJobsDAO.updateHistoryEA(adminBO, tripId, eadd1, eadd2, ecity, epost, elati, elong);
		if(ok>0){
			response.getWriter().write("Done");
		}else{
			response.getWriter().write("Failed");
		}
	}
	
	private void getHostoryDetails(HttpServletRequest request, HttpServletResponse response, AdminRegistrationBO adminBO) throws ServletException, IOException{
		// TODO Auto-generated method stub
		String voucherNo = request.getParameter("vNo");
		String fdate = request.getParameter("fdate");
		String tdate = request.getParameter("tdate");
		if(fdate==null || fdate.equals("") || tdate==null || tdate.equals("")){
			response.getWriter().write("DateProblem");
			return;
		}
		
		/*String fromDate=request.getParameter("fDate")==null?"":request.getParameter("fDate");
		String toDate=request.getParameter("tDate")==null?"":request.getParameter("tDate");
		String fromDateFormatted=TDSValidation.getTimeStampDateFormat(fromDate);
		String toDateFormatted=TDSValidation.getTimeStampDateFormat(toDate);*/
		
		String formatedFDate = TDSValidation.getTimeStampDateFormat(fdate);
		String formatedTDate = TDSValidation.getTimeStampDateFormat(tdate);
		ArrayList<OpenRequestBO> openReqHistory = EditHistoryJobsDAO.getHistotyJobsByVoucherandDate(adminBO, voucherNo, formatedFDate, formatedTDate);
		if(openReqHistory==null || openReqHistory.size()<1){
			System.out.println("Nodata");
			response.getWriter().write("NoData");
			return;
		}
		
		JSONArray array = new JSONArray();
		for(int i=0;i<openReqHistory.size();i++){
			JSONObject details = new JSONObject();
			try {
				details.put("TI", openReqHistory.get(i).getTripid()==null?"":openReqHistory.get(i).getTripid());
				details.put("TD", openReqHistory.get(i).getSdate()==null?"":openReqHistory.get(i).getSdate());
				details.put("PH", openReqHistory.get(i).getPhone()==null?"":openReqHistory.get(i).getPhone());
				details.put("SA", openReqHistory.get(i).getSadd1()==null?"":openReqHistory.get(i).getSadd1());
				details.put("EA", openReqHistory.get(i).getEadd1()==null?"":openReqHistory.get(i).getEadd1());
				details.put("TS", openReqHistory.get(i).getTripStatus()==null?"":openReqHistory.get(i).getTripStatus());
				details.put("RF", openReqHistory.get(i).getRefNumber()==null?"":openReqHistory.get(i).getRefNumber());
				details.put("SITIME",openReqHistory.get(i).getOR_SMS_SENT()==null?"":openReqHistory.get(i).getOR_SMS_SENT());
				details.put("CRTIME",openReqHistory.get(i).getCreatedTime()==null?"":openReqHistory.get(i).getCreatedTime());
				details.put("COTIME",openReqHistory.get(i).getCompletedTime()==null?"":openReqHistory.get(i).getCompletedTime());
				details.put("TRS",openReqHistory.get(i).getTripStatus()==null?"":openReqHistory.get(i).getTripStatus());
				details.put("PT",openReqHistory.get(i).getPaytype()==null?"":openReqHistory.get(i).getPaytype());
				details.put("ACCT",openReqHistory.get(i).getAcct()==null?"":openReqHistory.get(i).getAcct());

				array.put(details);			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println("details:"+array.toString());
		response.getWriter().write(array.toString());
	}

	private void gotoHistoryJobsEditPage(HttpServletRequest request, HttpServletResponse response, AdminRegistrationBO adminBo) throws ServletException, IOException{
		// TODO Auto-generated method stub
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "/jsp/ORHEdit.jsp";
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
}
