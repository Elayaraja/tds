package com.common.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import javax.servlet.ServletConfig;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.CompanyBean;

public class TDSProperties {
	private static Category cat;
	static {
		cat = TDSController.cat;
	}
	
	public static String getValue(String p_propertyName) {
		String returnvalue = "";
		try {
//			InputStream inStream = TDSProperties.class.getResourceAsStream("../../../TDSProperties.properties");
//			Properties properties = new Properties();
//			properties.load(inStream);
//			Enumeration keyenum = properties.keys();
//			Enumeration valueenum = properties.elements();
//			while (keyenum.hasMoreElements()) {
//				key = keyenum.nextElement().toString();
//				value = valueenum.nextElement().toString();
//				if(p_propertyName.equals(key)) {
//					returnvalue = value;
//				}
//			}
//			inStream.close();
			
			if(CompanyBean.getCompanyProperties()==null || CompanyBean.getCompanyProperties().isEmpty()){
//				System.out.println("Loading properties for first time--->"+p_propertyName);
				ArrayList properties = TDSProperties.getValueAll();
				HashMap<String, String> hashProperties = new HashMap<String, String>();
				for(int i=0;i<properties.size();i=i+2){
					hashProperties.put(properties.get(i)+"", properties.get(i+1)+"");
				}
				CompanyBean.setCompanyProperties(hashProperties);
			}
			returnvalue=CompanyBean.getCompanyProperties().get(p_propertyName);
			
			if(returnvalue==null && p_propertyName.contains("googleMapV3")){
				returnvalue = CompanyBean.getCompanyProperties().get("googleMapV3");
			}
			if(returnvalue==null && p_propertyName.contains("WASL_Company")){
				returnvalue = "";
			}
		} catch(Exception e) {
			//cat.info("In IOException ");
		}
		
		return returnvalue; 
	}
	public static ArrayList getValueAll() {
		ArrayList resultArray=new ArrayList();
		try {
			InputStream inStream = TDSProperties.class.getResourceAsStream("../../../TDSProperties.properties");
			Properties properties = new Properties();
			properties.load(inStream);
			Enumeration keyenum = properties.keys();
			Enumeration valueenum = properties.elements();
			while (keyenum.hasMoreElements()) {
				resultArray.add(keyenum.nextElement().toString());
				resultArray.add(valueenum.nextElement().toString());
			}
			inStream.close();
		} catch (IOException ioex) {
			//cat.info("In IOException ");
		}
		return resultArray;
	}
}
