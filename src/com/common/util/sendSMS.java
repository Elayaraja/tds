package com.common.util;
import com.nexmo.messaging.sdk.NexmoSmsClient;
import com.nexmo.messaging.sdk.messages.TextMessage;
import com.tds.cmp.bean.SMSBean;
import com.tds.dao.IVRDAO;
import com.tds.dao.PhoneDAO;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Sms;
import com.twilio.sdk.resource.list.SmsList;

import java.util.HashMap;
import java.util.Map;
public class sendSMS {
	// Find your Account Sid and Token at twilio.com/user/account
	public static String ACCOUNT_SID = "";
	public static String AUTH_TOKEN = "";
	public static void main(String Message,String phoneNumber,String assoccode) throws TwilioRestException {
		if(!phoneNumber.equals("") && !phoneNumber.contains("00000000") && !phoneNumber.contains("null")){
			SMSBean smsBean=PhoneDAO.getSMSParameter(assoccode,1);
			if(smsBean.getPhone()!=null && !smsBean.getPhone().equals("")){
				IVRDAO.smsEntry(assoccode, Message, phoneNumber, smsBean.getType());
				if(smsBean.getType()==2){
					ACCOUNT_SID=smsBean.getUserName();
					AUTH_TOKEN=smsBean.getPassword();
					TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
					// Build a filter for the SmsList
					Map<String, String> params = new HashMap<String, String>();
					params.put("Body", Message);
					params.put("To", "+"+phoneNumber);
					params.put("From", "+18633014780");
					SmsFactory messageFactory = client.getAccount().getSmsFactory();
					Sms message = messageFactory.create(params);
					System.out.println(message.getSid());
				} else {
					TextMessage txtMsg = new TextMessage(smsBean.getPhone(),phoneNumber, Message);
					try{
						NexmoSmsClient smsSender = new NexmoSmsClient(smsBean.getUserName(), smsBean.getPassword());
						smsSender.submitMessage(txtMsg);
					} catch (Exception e){
						System.out.println(e.toString());
					}
				}
			}
		}
	}
	
	//For bulk sms, i dont want getting sms parameter from table again and again
	public static void main1(String Message,String phoneNumber,String assoccode,SMSBean smsBean) throws TwilioRestException {
		if(!phoneNumber.equals("") && !phoneNumber.contains("00000000") && !phoneNumber.contains("null")){
			//SMSBean smsBean=PhoneDAO.getSMSParameter(assoccode,1);
			if(smsBean.getPhone()!=null && !smsBean.getPhone().equals("")){
				IVRDAO.smsEntry(assoccode, Message, phoneNumber, smsBean.getType());
				if(smsBean.getType()==2){
					ACCOUNT_SID=smsBean.getUserName();
					AUTH_TOKEN=smsBean.getPassword();
					TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
					// Build a filter for the SmsList
					Map<String, String> params = new HashMap<String, String>();
					params.put("Body", Message);
					params.put("To", "+"+phoneNumber);
					params.put("From", "+18633014780");
					SmsFactory messageFactory = client.getAccount().getSmsFactory();
					Sms message = messageFactory.create(params);
					System.out.println(message.getSid());
				} else {
					//System.out.println("nexmo message");
					//System.out.println("ph:"+smsBean.getPhone()+" ---- username:"+smsBean.getUserName()+" ---- password:"+smsBean.getPassword());
					TextMessage txtMsg = new TextMessage(smsBean.getPhone(),phoneNumber, Message);
					try{
						NexmoSmsClient smsSender = new NexmoSmsClient(smsBean.getUserName(), smsBean.getPassword());
						smsSender.submitMessage(txtMsg);
					} catch (Exception e){
						System.out.println(e.toString());
					}
				}
			}
		}
	}
}
