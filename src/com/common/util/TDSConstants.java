package com.common.util;

import org.apache.woden.wsdl20.Description;

public interface TDSConstants {
	
// Queue Constant
	
	// Trip status flags 
	// Subgroup 1-10
	// 5 --> Job accepted and moved to another table?
	// 4 --> Job being is being processed in queue
	// 3 --> Job not yet started the queue processes

	// Subgroup 11-20
	// 10 --> Closest Distance

	// Subgroup 21-30
	// 15 --> BroadCast Job

	//public static final int acceptedRequest = 40;
	
	/*public static final int queueProcessLowerLimt = 0;
	public static final int queueProcessUpperLimt = 10;
	public static final int performingQueueProcess = 2;
	public static final int newRequestNotStartedQueueYet = 4;
	
	//CD--> Closest driver
	public static final int cdProcessLowerLimt = 11;
	public static final int cdProcessUpperLimt = 20;
	public static final int performingCDProcesses = 12;
	public static final int newRequestCDProcessesNotStarted = 14;
	*/
	public static final int appVersion = 1;
	public static final int tripEmpty = 10;

	public static final int srAllocatedToCab=37;
	
	public static final int jobAllocated = 40;
	public static final int onRotueToPickup = 43;
	public static final int onSite = 45;
	public static final int tripStarted = 47;
	public static final int driverReportedNoShow = 48;

	
	public static final int performingDispatchProcesses = 4;
	public static final int manualAllocation = 5;
	public static final int performingDispatchProcessesCantFindDrivers = 3;
	public static final int newRequestDispatchProcessesNotStarted = 8;
	public static final int dummyTripId = 10;

	public static final int staleCall=20;
	public static final int broadcastRequest = 30;

	public static final int customerCancelledRequest = 50;
	public static final int noShowRequest = 51;	
	public static final int companyCouldntService = 52;
	public static final int operatorCancelledRequest=55;
	
	public static final int jobNonAcceptance = 53;
	public static final int jobMeterPayment = 102;
	
	public static final int tripCompleted = 61;
	public static final int jobSTC = 49;
	public static final int manualChange = 105;

	public static final int srJobHold = 90;
	public static final int tripOnHold = 99;
	//public static final int couldntFindDrivers = 25;
	public static final int tripWASL_updated = 143;
	public static final int warning=100;
	//Trip Source
	public static final int mobile_android = 0;
	public static final int Company = 1;
	public static final int flagTrip = 2;
	public static final int web = 3;
	public static final int mobile_IOS = 4;
	public static final int uploadFile = 5;
	public static final int assistant = 6;

	

	/*
	 * Basic controller fields
	 */
	public static final String actionParam = "action";
	public static final String eventParam = "event";
	String multiDriverCharges="multiCharges";

	
	/*
	 * Action constants
	 */
	public static final String insertauditRequest = "insert into TDS_AUDIT(UNIQUE_KEY,ASSOCIATION_CODE,USER_ID,DESCRIPTION,STARTVALUE,TOVALUE,MODULE,DATE_TIME) values(?,?,?,?,?,?,?,now())";
	public static final String insertmanualcreditRequest="insert into TDS_MANUAL_CREDIT(OPERATOR_ID,DRIVER_ID,CREDIT_CARD_NUMBER,DATE_TIME,AMOUNT,TIP_AMT,DESCRIPTION) values (?,?,?,now(),?,?,?)";

	
	public static final String registrationAction = "registration";
	
	public static final String requestAction = "openrequest";
	
	public static final String serviceAction = "servicerequest";
	
	public static final String userRoleAction = "userrolerequest";
	
	/*
	 * Events Constants 
	 */
	
	public static final String addDriverRegistration = "addDriver";
	public static final String saveDriverRegistration = "saveDriver";
	public static final String checkUserRegistration = "checkUser";
	public static final String editDriverDetail = "editDR";
	public static final String driverSummary= "summaryDR";
	//for passenger
	public static final String passSummary= "passengerSummary";
	public static final String editUpdateDriverDetail ="editUpdateDD";

	public static final String updateDriverAvailability = "updateDriver";
	
	public static final String createAdminRegistration = "adminregis";
	public static final String disburshmentmessageDetails = "disburshmentmessage";
	
	public static final String saveAdminRegistraion = "saveadminRegistration";
	
	public static final String createVoucher = "createVoucher";
	public static final String saveVoucher = "saveVoucher";
	public static final String editVoucher="editVoucher";
	public static final String getAllVoucher="getAllVoucher";
	public static final String getVoucherEditPage="getVoucherEditPage";
	public static final String checkDriverRequest ="checkDriverRequest"; 
	public static final String changeDriverRequest="changeDriverRequest";
	
	public static final String createDrvierBehavior = "createBehavior";
	public static final String behaviorList = "behaviorSummary";
	public static final String behaviorByID = "behaviorById";
	
	public static final String createMaintenanceDetail = "createMaintence";
	public static final String maintenanceList = "maintenanceSummary";
	public static final String maintenanceByID = "maintenanceById";
	
	public static final String createLostorFoundDetail = "createLostandFound";
	public static final String lostandFoundList = "lostandFoundSummary";
	public static final String lostandFoundByID = "lostandFoundById";
	
	public static final String createOpenRequest = "openRequest";
	public static final String saveOpenRequest = "saveRequest";
	public static final String getOpenRequestasXML = "xmlOpenRequest";
	
	public static final String acceptOpenRequestByWEB = "acceptRequestWeb";
	
	public static final String updateStartTrip = "startTrip";
	public static final String updateEndTrip = "endTrip";
	public static final String checkPayment = "checkPayment";
	public static final String updateCompletedRequest = "updateCompletedRequest";
	public static final String dOpenRequest = "dOpenRequest";
	public static final String inProgressRequestXML = "inProgressXML";
	public static final String inAcceptedRequestXML = "inAcceptedXML";
	public static final String getDriverAcceptance = "driverAccept";
	public static final String logoutevent = "logout";
	public static final String companyevent = "companyentry";
	public static final String saveCompany = "savecompany";
	public static final String driversavedetails ="driverSave";
	public static final String drivermappingdetails ="driverMapping";

	public static final String 	queuemappingdetails ="cmpyQueueMap";
	public static final String 	driverZoneActivity ="driverZoneActivity";
	//for passengerEntry
	public static final String passengerEntry ="passengerEntry";
	public static final String passengerEditUpdate ="passengerEdit"; 
	public static final String passengerSummary="/jsp/passengerSummary.jsp";
	public static final String getOpenJSP="/jsp/OpenRequestOne.jsp";
	
	public static final String getAcceptedandOpenRequest="getORAR";
	
	public static final String getOpenRequestCriteria = "getopenCriteria";
	public static final String openRequestCriteria = "openCriteria";
	
	public static final String getAcceptRequestCriteria = "getAcceptCriteria";
	

    public static final String viewopenRequestJSP = "/jsp/ViewOpenRequest.jsp";
    public static final String viewlandmarkJSP = "/jsp/landmark.jsp";
    
	public static final String tripSummaryReport = "tripSummaryReport";
	public static final String voucherSummaryReoprt = "voucherSummaryReoprt";
	public static final String driverPaymentReport = "driverpaymentreport";
	public static final String companyPaymentReport = "companypaymentreport";
	
	public static final String getAvailableDriver = "availableDriver";
	
	public static final String getAcceptedRequestDriverXML = "getDriverAcceptXML";
	public static final String doNightPaymentPendingDetail = "doNightPaymentPendingDetail";
	public static final String doNightPaymentPending = "doNightPaymentPending";
	public static final String doNightPaymentSummary = "doNightPaymentSummary";
	
	public static final String getDriversMap = "getDriversLocation";
	public static final String getLoginXML = "loginxml";
	
	public static final String insertNoShow = "noshow";
	
	public static final String getDriverLocation = "driverlocation";
	
	public static final String getUserList="getUserList";
	public static final String getUserRole="getuserrole";
	public static final String updateUserRole="updateRoles";
	
	public static final String driverQueueAction = "driverQueueService";
	
	public static final String dropDriverinQueue = "dropDriverQueue";
	
	public static final String queuecoordinate = "createqueueCoordinate";
	public static final String queuecoordinatesummary = "queuesummary";
	
	/*
	 * Url Constants 
	 */
	
	public static final String registrationURL = "/registration";
	
	public static final String requestURL = "/openrequest";
	
	public static final String serviceURL = "/servicerequest";
	
	public static final String userRoleURL = "/userRole";
	
	/*
	 * JSP Constants
	 */
	
	public static final String registrationJSP = "/jsp/MasterRegistrationJQuery.jsp";
	public static final String registrationJSP1 = "/jsp/MasterRegistration1.jsp";
	public static final String openRequestJSP = "/jsp/OpenRequestFormat01.jsp";
	public static final String updateopenRequestJSP = "/jsp/EditOpenRequest.jsp";
	public static final String getOpenRequestXML = "/jsp/GetOpenRequestXML.jsp";
	public static final String getSuccessJSP = "/Success.jsp";
	public static final String getFailureJSP = "/failure.jsp";
	public static final String getMainJSP = "/mainNew.jsp";
	public static final String getMainNewJSP = "/mainNew.jsp";
	public static final String getMainWithoutSessionJSP = "/mainNew.jsp";
	public static final String getOpenMap = "/jsp/mapdetails.jsp";

	public static final String getOpenMap1 = "/cab/mapdetails.jsp";

	public static final String getLoginJSP = "/rightOption.jsp";
	public static final String getmenuItemJSP = "/leftNav.jsp"; 
	public static final String getWelcomeJSP = "/Welcome.jsp";
	public static final String getInProgressRequestXML = "/jsp/InProgressRequestXML.jsp";
	public static final String getAcceptedRequestXML="/jsp/AcceptedRequestXML.jsp";
	public static final String creatAdminRegistration = "/jsp/AdminRegistration.jsp";
	public static final String acceptOpenRequestJSP = "/jsp/OpenRequestAccepted.jsp";
	public static final String createVoucherJSP = "/jsp/CreateVoucher.jsp";
	public static final String getVoucherEditJSP="/jsp/EditVoucher.jsp";
	public static final String sumDriverRegister="/jsp/SummaryDriverRegistration.jsp";
	
	public static final String getAcceptedRequestToEditJSP="/jsp/AcceptedRequest.jsp";
	public static final String getMessageXML = "/jsp/MessageRequestXML.jsp";
	
	public static final String driverAvailSummary = "/jsp/DriverAvailableSummary.jsp";
	
	
	//REPORT
	public static final String voucherSummaryJSPReoprt="/jsp/GetVoucherReport.jsp";
	public static final String tripSummaryJSPReport = "/jsp/CompleteRequestSummary.jsp";
	public static final String driverPaymentJSPReport = "/jsp/DriverPaymentReport.jsp";
	public static final String companyPaymentJSPReport = "/jsp/CompanyPaymentReport.jsp";
	
	
	
	public static final String userListJSP="/jsp/userList.jsp";
	public static final String userRole="/jsp/UserRole.jsp";
	
	public static final String CompanyVouchercreation = "CompanycreateVoucher";
	public static final String getCompanyVoucherEditPage="getCompanyVoucherEditPage";
	public static final String createCompanyVoucherJSP = "/jsp/CompanyVoucherCreation.jsp";
	public static final String getCompanyVoucherEditJSP="/jsp/EditCompanyVoucher.jsp";
	public static final String getAllcompanyVoucher="getAllcompanyVoucher";
	public static final String CompanyVoucherSave = "saveCompanyVoucher";
	
	public static final String openRequestCriteriaJSP = "/jsp/OpenRequestCriteria.jsp";
	
	public static final String getCompanyMasterJSP = "/jsp/CompanyMaster.jsp";
	
	public static final String acceptRequestCriteriaJSP="/jsp/AcceptedRequestCriteria.jsp";
	
	public static final String driverBehaviorJSP = "/jsp/DriverBehavior.jsp";
	
	public static final String behaviorSummaryJSP = "/jsp/DriverBehaviorSummary.jsp";
	
	public static final String cabMaintenanceJSP = "/jsp/CabMaintenance.jsp";
	
	public static final String cabMaintenanceSummaryJSP = "/jsp/MaintenanceSummary.jsp";
	
	public static final String lostandFoundJSP = "/jsp/LostAndFound.jsp";
	public static final String FDRegisterJSP = "/jsp/FDRegister.jsp";
	
	public static final String lostandFoundSummaryJSP = "/jsp/LostandFoundSummary.jsp";
	
	public static final String queueCoordinateJSP = "/SystemSetup/QueueCoordinate.jsp";
	public static final String queueSummaryJSP ="/SystemSetup/QueueCoordinateSummary.jsp";
	
	public static final String getMapJSP = "/SystemSetup/GetMap.jsp";
	public static final String getReviewMapJSP = "/SystemSetup/GetReviewMap.jsp";
	
	/*
	 * 
	 * Jsp for Mobile Calls
	 * 
	 */
	
	/**
	 *  @author vimal
	 *  @see Description 
	 *  This Constant is used to call the mobile service Login page.
	 */
	public static final String mLoginJSP = "/HttpService/MLogin.jsp";
	public static final String mLogoutJSP = "/HttpService/MLogout.jsp";
	public static final String mDriverAvailabilityJSP = "/HttpService/MDriverAvailability.jsp";
	public static final String mAcceptedRequestJSP = "/HttpService/MAcceptDriver.jsp";
	public static final String mStartTrip = "/HttpService/MStartTrip.jsp";
	public static final String mEndTrip = "/HttpService/MEndTrip.jsp";
	public static final String mdriverCount = "/HttpService/MDriverCount.jsp";
	public static final String mTripDetailJSP = "/HttpService/MTripDetail.jsp";
	public static final String mpublicTripJSP = "/HttpService/MPublicRequest.jsp";
	public static final String macceptedTripDetailJSP = "/HttpService/MAcceptedTripDetail.jsp";
	public static final String mqueueDetailJSP = "/HttpService/MQueueDetail.jsp";
	
	
	/*
	 * DB Connection
	 * 
	 */
	
	public static final String JNDI_CONNN = "jdbc/tds";
	
	
	/*
	 *  Mobile Device Data
	 * 
	 */
	
	/*
	 * Mobile Actions
	 * 
	 */
	
	public static final String getMobileRequest = "mobilerequest";
	
	/*
	 * Mobile Events
	 * 
	 */
	public static final String mobileLoginService = "mlogin";
	public static final String mobileDriverLocation = "mdriverlocation";
	public static final String mobileDriverQueue = "mdriverqueue";
	public static final String mobilelogout = "mlogout";
	public static final String mobileAcceptTrip = "maccepttrip";
	public static final String mobileonRouteToPickup = "monroute";
	public static final String mobileRejectTrip = "mrejecttrip";
	public static final String mobileStartTrip = "mstartTrip";
	public static final String mobileEndTrip = "mendTrip";
	public static final String mobileNoShow = "mnoshow";
	
	public static final String mobileJobNoResponse = "mNoResponse";

	public static final String mobileIsPayment = "mcheckPayment";
	public static final String mobileMakePayment = "mMakePayment";
	
	public static final String mobileOpenRequest = "mOpenRequest";
	
	public static final String mobileTripDetail = "mTripDetail";
	public static final String mobileAcceptedTripDetail = "mTripAcceptedDetail";
	public static final String mobilePublicTripDetail = "mPublicTrip";
	
	public static final String mobileUploadSignature = "msignature";
	
	public static final String totalDriverinQueueNo = "mnoofdriver";
	
	public static final String mobileQueueDetail = "qsetup";
	public static final String mobileQueueBoundries = "qBoundries";
	public static final String mobileGetZonePosition = "mobileGetZonePosition";
	public static final String mobileGetZonesAndJobsSummary = "mobileZoneJobSummary";
	
	
	public static final String DisbursementJSP="/Company/Disbursement.jsp";
	/*
	 * Mobile URL
	 */
	
	public static final String mobileRequestURL = "/mobilerequest";
	
	String mobilePaymentURL = "/mobilepayment";
	String getMobilePayment = "mobilepayment";
	
	String paymentProcessURL="/paymentprocess";
	String getpaymentProcess="paymentprocess";
	
	String mCCProcessURL="/manualccp";
	String getMCCProcess="manualccp";
	
	String pingURL="/ping";
	String getPing="ping";
	
	String adminURL="/admin";
	String getadmin="admin";
	
	String systemSetupURL="systemsetup";
	String getSystemSetup="systemsetup";

	String financeURL="finance";
	String getFinance="finance";
	
	String customerServiceURL="CustomerService";
	String customerService="CustomerService";
	
	String reDirectURL="/redirect";
	String  getRedirect="redirect";
	String getSecurity="Security";
	String securityURL="/Security";
	String sendMail="sendmail";
	
	String FileURL="/file";
	String fileUpload="file";
	
	String DriverSettledSummery = "/jsp/DriverSettledSummery.jsp";
	String SummarryInduvigual = "/jsp/SummarryInduvigual.jsp";
	
	String getDriverSetteled = "driverSettled";
	String getSummarryInduvigual = "SummarryInduvigual";
	
	String getSendSMS = "sendSMS";
	String gotoSMSJSP = "/Company/SMSSend.jsp";
	
	String getCSV="CSV";
	String csvURL="/CSV";

	String reversepaymentActionURL="reversepaymentAction";
	String getreversepaymentAction="reversepaymentAction";

	
	String manualSettle="manualSettle";
		
	public static final String getDocApproval = "TDS_DOCUMENT_APPROVAL";
	public static final String documentUploadUrl="/uploadDocs";
	public static final String documentApproveUrl = "/uploadDocs";
	public static final String getDocumentUpload = "TDS_DOCUMENT_UPLOAD";	
	public static final String documentSearchUrl = "/searchDocs";
	

	public static final String getReviewMapAllJSP = "/SystemSetup/GetAllZone.jsp";
	public static final String getAdjZoneMap = "/SystemSetup/GetAllAdjZone.jsp";
	public static final String getOpenShortJSP="/jsp/OpenRequestOneForDashBoard.jsp";
	public static final String getOpenRequestJSP = "/jsp/OpenPreviousRequestForDashBoard.jsp";

	public static final String openRequestJSP01 = "/jsp/OpenRequestFormat01.jsp";
	public static final String openRequestJSP02 = "/jsp/OpenRequestFormat03.jsp";
	public static final String openRequestJSP03 = "/jsp/OpenRequestFormatBC.jsp";
	public static final String openRequestJSP04 = "/jsp/OpenRequestFormatHTML.jsp";

	public static final int insertJob = 1;
	public static final int updateJob = 5;
	public static final int restartTrip = 10;
	public static final int performingDispatch = 15;
	public static final int broadcastTrip = 20;
	public static final int ivrCall = 21;
	public static final int jobAccepted = 25;
	public static final int ivrPassDriverAcceptedCall = 27;
	public static final int jobRejected = 30;
	public static final int startedTrip = 35;
	public static final int driverOnRoute = 40;
	public static final int ivrPassDriverOnRouteCall = 41;
	public static final int driverOnSite = 42;
	public static final int ivrPassDriverOnsiteCall = 23;
	public static final int tripEnded = 45;
	public static final int jobCompleted = 50;
	public static final int customerCancel = 55;
	public static final int operatorNoShow = 60;
	public static final int driverNoShow = 65;
	//public static final int onHoldTrip = 70;
	public static final int paymentReceived = 70;
	public static final int reDispatch = 75;
	public static final int driverGettingDetails = 80;
	
	//have to change always
	public static final int versionNo =149;
	
	public static final String meterDisable = "MAA";
	public static final String meterEnable = "MAD";
	public static final String meterGetFare = "MRF";
	public static final String meterGetStatistics = "MRS";
	public static final String meterGetVersion = "MRV";
	
	public static final String btz_login ="0";
	public static final String btz_jobAcceptLogout ="1";
	public static final String btz_jobStartLogout ="2";
	public static final String btz_logout ="3";
	public static final String btz_ServerLogout ="4";
	public static final String btz_samezone ="5";
	
}
