package com.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.dao.ChargesDAO;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.MailReport;
import com.tds.controller.ORComparator;
import com.tds.dao.DispatchDAO;
import com.tds.dao.EmailSMSDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.ZoneDAO;
import com.common.util.CheckZone;
import com.common.util.Messaging;
import com.gac.mobile.dao.MobileDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueCoordinatesBO;

public class OpenRequestUtil {

	public static String saveOpenRequest(HttpServletRequest request, HttpServletResponse response,ServletConfig srvletConfig) throws UnsupportedEncodingException{
		request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
		String errors = "";
		int result=0;
		String error="";
		String m_queueId = "";
		String m_queueIdRoundTrip="";
		int dispatchAdvaceTime=15*60;
		int dispatchAdvaceTimeRT=15*60;
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		OpenRequestBO openRequestBO =new OpenRequestBO();
		if((request.getParameter("saveOpenReq")!= null && request.getParameter("saveOpenReq").equalsIgnoreCase("saveOpenReq")) || (request.getAttribute("openrequestBOfromAction")!=null) ||(request.getAttribute("openrequestBO")!=null)) {
			if(request.getAttribute("openrequestBOfromAction")!=null){
				openRequestBO = (OpenRequestBO) request.getAttribute("openrequestBOfromAction");
				if(request.getAttribute("adminBoFromAction")!=null){
					adminBO = (AdminRegistrationBO) request.getAttribute("adminBoFromAction");
				}
			}else{
				openRequestBO =(OpenRequestBO) request.getAttribute("openrequestBO");
			}
			error = openRequestValidation(openRequestBO);
			String landMark="";

			if(request.getAttribute("assoCode")!=null){
				//System.out.println("assocded:"+request.getAttribute("assoCode"));
				openRequestBO.setAssociateCode((String)request.getAttribute("assoCode"));
			}else{
				openRequestBO.setAssociateCode(request.getParameter("assoCode")==null?adminBO.getAssociateCode():request.getParameter("assoCode"));
			}			
			//			System.out.println("asscocode:"+openRequestBO.getAssociateCode());
			openRequestBO.setCreatedBy(adminBO.getUid());
			if(request.getParameter("newFromLandMark")!=null && request.getParameter("newFromLandMark")!="" ){
				if(request.getParameter("slandmark")!=""){
					landMark=request.getParameter("slandmark");
					result=RequestDAO.insertNewLandMark(adminBO.getAssociateCode(), openRequestBO,1,landMark);
				} else {
					errors="Please Provide From LandMark Name";
				}
			} if(request.getParameter("newToLandMark")!=null && request.getParameter("newToLandMark")!="" ){
				if(request.getParameter("elandmark")!=""){
					landMark=request.getParameter("elandmark");
					result=RequestDAO.insertNewLandMark(adminBO.getAssociateCode(), openRequestBO,2,landMark);
				} else {
					openRequestBO = (OpenRequestBO) request.getAttribute("openrequestBO");
					for (int i=0;i<openRequestBO.getNumOfCabs();i++){
						result = RequestDAO.saveOpenRequest(openRequestBO,adminBO,1);
						openRequestBO.setTripid(result+"");
						DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTime, adminBO.getMasterAssociateCode());
						if(request.getParameter("chargesLength")!=null && result >0){
							int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
							String[] amount=new String[numberOfRows+1];
							String[] key=new String[numberOfRows+1];
							for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
								amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
								key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
							}
							ChargesDAO.insertDriverCharge(openRequestBO.getAssociateCode(),amount,key,result,numberOfRows,openRequestBO.getDriverid(),adminBO.getMasterAssociateCode());
						}
					}
					errors="Please Provide To LandMark Name";
				}					
			}
			if(error.length()>0 && errors.length()>0){
				error=error+errors;
			}else if(error.length()>0 && errors.length()==0){

			}else if(error.length()==0 && errors.length()>0){
				error=errors;
			}
			else {
				result = 0;
				if(request.getParameter("queueno") != null && request.getParameter("queueno").length() >0 ) {
					openRequestBO.setQueueno(request.getParameter("queueno"));
					List<QueueCoordinatesBO> queueProp = SystemPropertiesDAO.getQueueCoOrdinateList(openRequestBO.getQueueno(), adminBO.getMasterAssociateCode(), "");
					dispatchAdvaceTime = openRequestBO.getAdvanceTime().equals("-1")?Integer.parseInt(queueProp.get(0).getQDelayTime())*60:Integer.parseInt(openRequestBO.getAdvanceTime())*60;
				} else if(openRequestBO.getSlat()!=null && !openRequestBO.getSlat().equals("0") && !openRequestBO.getSlong().equals("0")  ) {
					ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) srvletConfig.getServletContext().getAttribute((adminBO.getMasterAssociateCode() +"Zones"));
					m_queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(openRequestBO.getSlat()),Double.parseDouble(openRequestBO.getSlong()));
					if(m_queueId.equals("")){
						ZoneTableBeanSP zonesId = ZoneDAO.getDefaultZone(adminBO.getMasterAssociateCode());
						m_queueId = zonesId.getZoneKey();
						dispatchAdvaceTime = openRequestBO.getAdvanceTime().equals("-1")?zonesId.getAdvance():Integer.parseInt(openRequestBO.getAdvanceTime())*60;
						openRequestBO.setDrProfile(zonesId.getZnProfile()!=null?zonesId.getZnProfile():openRequestBO.getDrProfile());
					} else {
						List<QueueCoordinatesBO> queueProp = SystemPropertiesDAO.getQueueCoOrdinateList(m_queueId, adminBO.getMasterAssociateCode(), "");
						dispatchAdvaceTime = openRequestBO.getAdvanceTime().equals("-1")?Integer.parseInt(queueProp.get(0).getQDelayTime())*60:Integer.parseInt(openRequestBO.getAdvanceTime())*60;
						openRequestBO.setDrProfile(queueProp.get(0).getZnprofile()!=null?queueProp.get(0).getZnprofile():openRequestBO.getDrProfile());
					}
					m_queueIdRoundTrip = m_queueId;
					openRequestBO.setQueueno(m_queueId);
				}
				if(openRequestBO.getEdlatitude()!=null && !openRequestBO.getEdlatitude().equals("") && !openRequestBO.getEdlatitude().equals("0")){
					ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) srvletConfig.getServletContext().getAttribute((adminBO.getMasterAssociateCode() +"Zones"));
					m_queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(openRequestBO.getEdlatitude()),Double.parseDouble(openRequestBO.getEdlongitude()));
					if(m_queueId.equals("")){
						ZoneTableBeanSP zonesId = ZoneDAO.getDefaultZone(adminBO.getMasterAssociateCode());
						m_queueId = zonesId.getZoneKey();
						dispatchAdvaceTimeRT = openRequestBO.getAdvanceTime().equals("-1")?zonesId.getAdvance():Integer.parseInt(openRequestBO.getAdvanceTime())*60;
						openRequestBO.setDrProfile(zonesId.getZnProfile()!=null?zonesId.getZnProfile():openRequestBO.getDrProfile());
					} else {
						List<QueueCoordinatesBO> queueProp = SystemPropertiesDAO.getQueueCoOrdinateList(m_queueId, adminBO.getMasterAssociateCode(), "");
						dispatchAdvaceTimeRT = openRequestBO.getAdvanceTime().equals("-1")?Integer.parseInt(queueProp.get(0).getQDelayTime())*60:Integer.parseInt(openRequestBO.getAdvanceTime())*60;
						openRequestBO.setDrProfile(queueProp.get(0).getZnprofile()!=null?queueProp.get(0).getZnprofile():openRequestBO.getDrProfile());
					}
					openRequestBO.setEndQueueno(m_queueId);
				}
				if(openRequestBO.getSlat()!=null && (openRequestBO.getSlat().equals("0") || openRequestBO.getSlong().equals("0") )){
					openRequestBO.setChckTripStatus(TDSConstants.tripOnHold);
				} else {
					if(openRequestBO.getSadd1().equalsIgnoreCase("Flag Trip")||openRequestBO.getSadd1().equalsIgnoreCase("OnRoute") || openRequestBO.getName().equalsIgnoreCase("Flag Trip")){
						openRequestBO.setChckTripStatus(TDSConstants.tripStarted);
					}else{
						openRequestBO.setChckTripStatus(TDSConstants.newRequestDispatchProcessesNotStarted);
					}
				}
				String tripId = request.getParameter("tripId")==null?"":request.getParameter("tripId");
				if(tripId!=null && !tripId.equals("")){
					ApplicationPoolBO poolBO = (ApplicationPoolBO) srvletConfig.getServletContext().getAttribute("poolBO");
					DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getAssociateCode(), openRequestBO.getDriverid(), 1);
					if(request.getParameter("updateAll").equals("") && (!openRequestBO.getVehicleNo().equals("")||!openRequestBO.getDriverid().equals(""))){
						openRequestBO.setChckTripStatus(TDSConstants.srAllocatedToCab);
					} else if(request.getParameter("updateAll").equals("1")){
						String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "OC", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
						Messaging oneSMS = new Messaging(srvletConfig.getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
						oneSMS.start();
						openRequestBO.setDriverid("");
						openRequestBO.setVehicleNo("");
						openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);
						if(!openRequestBO.getRepeatGroup().equals("")){
							ServiceRequestDAO.updateOpenRequestStatusAndDrivers(openRequestBO.getTripid(), openRequestBO.getChckTripStatus()+"", openRequestBO.getDriverid(), openRequestBO.getVehicleNo());
						}
					} else if(request.getParameter("updateAll").equals("0")){
						openRequestBO.setChckTripStatus(Integer.parseInt(request.getParameter("tripStatus")));						
					}
					openRequestBO.setTripid(tripId);
					String firstObjects = getJSONResponse(openRequestBO.getTripid(), adminBO, srvletConfig);
					
					result=RequestDAO.updateOpenRequest(openRequestBO,adminBO);

					String newObjects = getJSONResponse(openRequestBO.getTripid(), adminBO, srvletConfig);
					try {
						ORComparator testOR = new ORComparator(firstObjects, newObjects,openRequestBO.getTripid(),adminBO,dispatchAdvaceTime);
						testOR.start();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTime, adminBO.getMasterAssociateCode());
					if(request.getParameter("chargesLength")!=null && result >0){
						int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
						String[] amount=new String[numberOfRows+1];
						String[] key=new String[numberOfRows+1];
						for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
							amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
							key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
						}
						ChargesDAO.insertDriverCharge(openRequestBO.getAssociateCode(),amount,key,Integer.parseInt(openRequestBO.getTripid()),numberOfRows,openRequestBO.getDriverid(),adminBO.getMasterAssociateCode());
					}
				}else if(request.getParameter("tripIdAuto")!=null && !request.getParameter("tripIdAuto").equals("") && request.getParameter("roundTrip") ==null && (openRequestBO.getDays()==null || openRequestBO.getDays().equals("0000000"))){
					openRequestBO.setTripid(request.getParameter("tripIdAuto"));

					String firstObjects = getJSONResponse(openRequestBO.getTripid(), adminBO, srvletConfig);
					
					result=RequestDAO.updateOpenRequest(openRequestBO,adminBO);

					String newObjects = getJSONResponse(openRequestBO.getTripid(), adminBO, srvletConfig);
					try {
						ORComparator testOR = new ORComparator(firstObjects, newObjects,openRequestBO.getTripid(),adminBO,dispatchAdvaceTime);
						testOR.start();

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTime, adminBO.getMasterAssociateCode());
					if(request.getParameter("chargesLength")!=null && result >0){
						int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
						String[] amount=new String[numberOfRows+1];
						String[] key=new String[numberOfRows+1];
						for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
							amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
							key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
						}
						ChargesDAO.insertDriverCharge(openRequestBO.getAssociateCode(),amount,key,Integer.parseInt(openRequestBO.getTripid()),numberOfRows,openRequestBO.getDriverid(),adminBO.getMasterAssociateCode());
					}
				} else {
					if((request.getAttribute("openRequestBO")!=null) || (openRequestBO.getDays()!=null && openRequestBO.getDays().equals("0000000"))){
						if(request.getParameter("roundTripExist") !=null){
							openRequestBO.setMultiJobTag(System.currentTimeMillis());
							RequestDAO.updateMultiTag(request.getParameter("roundTripExist"),adminBO.getMasterAssociateCode(),openRequestBO.getMultiJobTag());
						}
						if((!openRequestBO.getDriverid().equals("")||!openRequestBO.getVehicleNo().equals("")) && openRequestBO.getTripSource()==TDSConstants.web){
							openRequestBO.setChckTripStatus(TDSConstants.srAllocatedToCab);
							if(!openRequestBO.getDriverid().equals("") && openRequestBO.getDriverid().substring(0,1).equalsIgnoreCase("F")) {
								openRequestBO.setChckTripStatus(TDSConstants.jobAllocated);
								openRequestBO.setDriverid(openRequestBO.getDriverid().replace("F", "").replace("f", ""));
							}if(!openRequestBO.getVehicleNo().equals("") && openRequestBO.getVehicleNo().substring(0,1).equalsIgnoreCase("F")){
								openRequestBO.setChckTripStatus(TDSConstants.jobAllocated);
								openRequestBO.setVehicleNo(openRequestBO.getVehicleNo().replace("F", "").replace("f", ""));
							}
						}
						for (int i=0;i<openRequestBO.getNumOfCabs();i++){
							result = RequestDAO.saveOpenRequest(openRequestBO,adminBO,1);
							openRequestBO.setTripid(result+"");
							DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTime, adminBO.getMasterAssociateCode());
							if(request.getParameter("chargesLength")!=null && result >0){
								int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
								String[] amount=new String[numberOfRows+1];
								String[] key=new String[numberOfRows+1];
								for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
									amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
									key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
								}
								ChargesDAO.insertDriverCharge(openRequestBO.getAssociateCode(),amount,key,result,numberOfRows,openRequestBO.getDriverid(),adminBO.getMasterAssociateCode());
							}
						}
						//						if((request.getParameter("roundTrip") !=null) ){
						//							openRequestBO=setOpenRequestRoundTrip(openRequestBO, request);
						//							openRequestBO.setEndQueueno(m_queueIdRoundTrip);
						//							for (int i=0;i<openRequestBO.getNumOfCabs();i++){
						//								result = RequestDAO.saveOpenRequest(openRequestBO,adminBO,1);
						//								openRequestBO.setTripid(result+"");
						//								DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTimeRT, adminBO.getAssociateCode());
						//								if(request.getParameter("chargesLength")!=null && result >0){
						//									int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
						//									String[] amount=new String[numberOfRows+1];
						//									String[] key=new String[numberOfRows+1];
						//									for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
						//										amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
						//										key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
						//									}
						//									ChargesDAO.insertDriverCharge(adminBO.getAssociateCode(),amount,key,result,numberOfRows,"");
						//								}
						//							}
						//						}		
					} else {
						String fromDate=(request.getParameter("fromDate")==null?"":request.getParameter("fromDate"));
						String toDate=(request.getParameter("toDate")==null?"":request.getParameter("toDate"));
						ArrayList<String> multipleJobs=RegistrationDAO.getAllDates(openRequestBO.getDays(),fromDate,toDate);
						openRequestBO.setMultiJobTag(System.currentTimeMillis());
						for(int i=0;i<multipleJobs.size();i++){
							openRequestBO.setSdate(multipleJobs.get(i));
							for (int j=0;j<openRequestBO.getNumOfCabs();j++){
								result = RequestDAO.saveOpenRequest(openRequestBO,adminBO,1);
								openRequestBO.setTripid(result+"");
								DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTime, adminBO.getMasterAssociateCode());
								if(request.getParameter("chargesLength")!=null && result >0){
									int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
									String[] amount=new String[numberOfRows+1];
									String[] key=new String[numberOfRows+1];
									for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
										amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
										key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
									}
									ChargesDAO.insertDriverCharge(openRequestBO.getAssociateCode(),amount,key,result,numberOfRows,openRequestBO.getDriverid(),adminBO.getMasterAssociateCode());
								}
							}
						}
						//						if(request.getParameter("roundTrip") !=null){
						//							openRequestBO=setOpenRequestRoundTrip(openRequestBO, request);
						//							openRequestBO.setEndQueueno(m_queueIdRoundTrip);
						//							for(int i=0;i<multipleJobs.size();i++){
						//								openRequestBO.setSdate(multipleJobs.get(i));
						//								for (int j=0;j<openRequestBO.getNumOfCabs();j++){
						//									result = RequestDAO.saveOpenRequest(openRequestBO,adminBO,1);
						//									openRequestBO.setTripid(result+"");
						//									DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTimeRT, adminBO.getAssociateCode());
						//									if(request.getParameter("chargesLength")!=null && result >0){
						//										int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
						//										String[] amount=new String[numberOfRows+1];
						//										String[] key=new String[numberOfRows+1];
						//										for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
						//											amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
						//											key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
						//										}
						//										ChargesDAO.insertDriverCharge(adminBO.getAssociateCode(),amount,key,result,numberOfRows,"");
						//									}
						//								}
						//								if(request.getParameter("tripIdHistory")!=null){
						//									AuditDAO.insertJobLogs(request.getParameter("tripIdHistory")+" Redispatched", result+"", adminBO.getAssociateCode(), TDSConstants.newRequestDispatchProcessesNotStarted, adminBO.getUid(), "", "",adminBO.getMasterAssociateCode());
						//								}
						//							}
						//						}
					}
				}
			}
		}
		if(openRequestBO.getSadd1().equalsIgnoreCase("Flag Trip")||openRequestBO.getSadd1().equalsIgnoreCase("OnRoute")){
			String tripId = Integer.toString(result);
			return "TripId="+tripId+";Status=OK";
		}else{
			if((error.length()<=0||error.equals("But Couldn't Send Email")||error.equals("But Doesn't Have Access To Send Mail")) && result>=1){
				String tripId = Integer.toString(result==1?Integer.parseInt(openRequestBO.getTripid()):result);
				ArrayList<String> emails=EmailSMSDAO.emailUsers(adminBO.getMasterAssociateCode());
				if(openRequestBO.getEmail()!=null && !openRequestBO.getEmail().equals("") && emails!=null && emails.size()>0 && (result+"").length()>3){
					String url1="http://www.getacabdemo.com";
					if(request.getServerName().contains("getacabdemo.com")){
						url1="http://www.getacabdemo.com";
					} else {
						url1="https://www.gacdispatch.com";
					}
					try {
						MailReport mailPass = new MailReport(url1, tripId, openRequestBO.getEmail(), adminBO.getTimeZone(), emails, "jobMail" , adminBO.getMasterAssociateCode(),1,"Your trip has been booked");
						mailPass.start();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				return error+" "+"with TripId="+tripId+";Status=OK";
			} else {
				return error+";Status=false";
			}
		}
	}

	public static OpenRequestBO setOpenRequestRoundTrip(OpenRequestBO requestBO,HttpServletRequest request) {
		requestBO.setEadd1(request.getParameter("sadd1"));
		requestBO.setEadd2(request.getParameter("sadd2"));
		requestBO.setEcity(request.getParameter("scity"));
		requestBO.setEstate(request.getParameter("sstate"));
		requestBO.setEzip(request.getParameter("szip"));
		if(request.getParameter("sLongitude").equals("")){
			requestBO.setEdlongitude("0");		
		} else {
			requestBO.setEdlongitude(request.getParameter("sLongitude"));
		}
		if(request.getParameter("sLatitude").equals("")){
			requestBO.setEdlatitude("0");
		} else {
			requestBO.setEdlatitude(request.getParameter("sLatitude"));
		}
		requestBO.setSadd1(request.getParameter("eadd1"));
		requestBO.setSadd2(request.getParameter("eadd2"));
		requestBO.setScity(request.getParameter("ecity"));
		requestBO.setSstate(request.getParameter("estate"));
		requestBO.setSzip(request.getParameter("ezip"));
		if(request.getParameter("edlatitude") != null) {
			requestBO.setSlat(request.getParameter("edlatitude"));
		} else {
			requestBO.setSlat("0");
		}
		if(request.getParameter("edlongitude") != null) {
			requestBO.setSlong(request.getParameter("edlongitude"));
		} else {
			requestBO.setSlong("0");
		}
		if(request.getParameter("fromDate")==null || request.getParameter("fromDate").equals("")){
			requestBO.setSdate(request.getParameter("sdateTrip"));
		}else{
			requestBO.setSdate(request.getParameter("fromDate"));
		}
		requestBO.setShrs(request.getParameter("shrsTrip"));
		requestBO.setQueueno(requestBO.getEndQueueno());
		return requestBO;
	}
	public static String openRequestValidation(OpenRequestBO p_openRequestBO) {
		StringBuffer errors = new StringBuffer();
		//		if(!TDSValidation.isValidZipCode(p_openRequestBO.getSzip()) && p_openRequestBO.getSzip().length()>0) {
		//			errors.append("You entered invalid Zip Code(From)<br>");
		//		}
		if(!TDSValidation.isOnlyNumbers(p_openRequestBO.getShrs()) || p_openRequestBO.getShrs().length() != 4 ){
			errors.append("Pls Provide FourDigit Time Format<br>");
		}
		if(TDSValidation.isOnlyNumbers(p_openRequestBO.getShrs()) && p_openRequestBO.getShrs().length() == 4 && !p_openRequestBO.getShrs().equals("2525")){
			if(!TDSValidation.get24timeFormat(p_openRequestBO.getShrs()))	
				errors.append("You entered invalid Time Format<br>");
		}
		if(p_openRequestBO.getPhone().equals("")) {
			errors.append("Phone No is mandatory<br>");
		} 
		if(p_openRequestBO.getName().equals("")) {
			errors.append("Rider Name is mandatory<br>");
		} 
		if(!p_openRequestBO.getEmail().equals("")){
			if(!p_openRequestBO.getEmail().contains("@")){
				errors.append("Please Enter A Valid Mail Id");
			}
		}
		//		if(p_openRequestBO.getDontDispatch()==1 && (!p_openRequestBO.getDriverid().equals("")||!p_openRequestBO.getVehicleNo().equals(""))){
		//			errors.append("Driver/Cab allocated.So you cannot give do not dispatch");
		//		}
		//		if(!TDSValidation.chechPhoneNo(p_openRequestBO.getPhone()) && p_openRequestBO.getPhone().length() >0) {
		//			errors.append("You entered invalid Phone No<br>");
		//		} 
		if(p_openRequestBO.getSdate().length() == 10) {
			String userDate[] = p_openRequestBO.getSdate().split("/") ;
			if(!TDSValidation.isValidDate(userDate[1], userDate[0], userDate[2])){
				errors.append("You Entered invalid Date<br>");
			}
		} else {
			errors.append("Please Provide a Service Date<br>");
		}
		return errors.toString();
	}

	public static String jsonTest1(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		JSONArray array = new JSONArray();
		ArrayList<OpenRequestBO> Address = RequestDAO.getAddress("103","5618293323");
		for(int i=0;i<Address.size();i++){
			JSONObject address = new JSONObject();
			try {
				address.put("A1", Address.get(i).getSadd1()==null?"":Address.get(i).getSadd1());
				address.put("A2", Address.get(i).getSadd2()==null?"":Address.get(i).getSadd2());
				address.put("C", Address.get(i).getScity()==null?"":Address.get(i).getScity());
				address.put("S", Address.get(i).getSstate()==null?"":Address.get(i).getSstate());
				address.put("Z", Address.get(i).getSzip()==null?"":Address.get(i).getSzip());
				address.put("P", Address.get(i).getPaytype()==null?"":Address.get(i).getPaytype());
				address.put("A", Address.get(i).getAcct()==null?"":Address.get(i).getAcct());
				address.put("La", Address.get(i).getSlat()==null?"":Address.get(i).getSlat());
				address.put("Lo", Address.get(i).getSlong()==null?"":Address.get(i).getSlong());
				address.put("M", Address.get(i).getMasterAddressKey()==null?"":Address.get(i).getMasterAddressKey());
				array.put(address);			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		return responseString;
	}

	public static String getJSONResponse(String tripId,AdminRegistrationBO adminBO,ServletConfig srvletConfig){
		JSONArray array = new JSONArray();
		JSONObject address = new JSONObject();
		OpenRequestBO openRequestBO = ServiceRequestDAO.getOpenRequestByTripID(tripId, adminBO, new ArrayList<String>());
		try {
			address.put("StartAddress", openRequestBO.getSadd1()==null?"":openRequestBO.getSadd1());
//			address.put("Add2", openRequestBO.getSadd2()==null?"":openRequestBO.getSadd2());
//			address.put("City", openRequestBO.getScity()==null?"":openRequestBO.getScity());
//			address.put("State", openRequestBO.getSstate()==null?"":openRequestBO.getSstate());
//			address.put("Zip", openRequestBO.getSzip()==null?"":openRequestBO.getSzip());
			address.put("PayType", openRequestBO.getPaytype()==null?"":openRequestBO.getPaytype());
			address.put("Acct", openRequestBO.getAcct()==null?"":openRequestBO.getAcct());
//			address.put("Lat", openRequestBO.getSlat()==null?"":openRequestBO.getSlat());
//			address.put("Lon", openRequestBO.getSlong()==null?"":openRequestBO.getSlong());
			address.put("Name", openRequestBO.getName()==null?"":openRequestBO.getName());
			address.put("EndAddress", openRequestBO.getEadd1()==null?"":openRequestBO.getEadd1());
//			address.put("EAdd2", openRequestBO.getEadd2()==null?"":openRequestBO.getEadd2());
//			address.put("ECity", openRequestBO.getEcity()==null?"":openRequestBO.getEcity());
//			address.put("EState", openRequestBO.getEstate()==null?"":openRequestBO.getEstate());
//			address.put("EZip", openRequestBO.getEzip()==null?"":openRequestBO.getEzip());
//			address.put("ELat", openRequestBO.getEdlatitude()==null?"":openRequestBO.getEdlatitude());
			address.put("AdvTime", openRequestBO.getAdvanceTime()==null?"":openRequestBO.getAdvanceTime());
			address.put("Zone", openRequestBO.getQueueno()==null?"":openRequestBO.getQueueno());
			address.put("LandMark", openRequestBO.getSlandmark()==null?"":openRequestBO.getSlandmark());
			address.put("ELandMark", openRequestBO.getElandmark()==null?"":openRequestBO.getElandmark());
			address.put("Date", openRequestBO.getSdate()==null?"":openRequestBO.getSdate());
			address.put("Time", openRequestBO.getShrs()==null?"":openRequestBO.getShrs());
			address.put("Phone", openRequestBO.getPhone()==null?"":openRequestBO.getPhone());
			address.put("Dr.Comments", openRequestBO.getSpecialIns()==null?"":openRequestBO.getSpecialIns());
			address.put("Comments", openRequestBO.getComments()==null?"":openRequestBO.getComments());
			address.put("Driver", openRequestBO.getDriverid()==null?"":openRequestBO.getDriverid());
			address.put("Vehicle", openRequestBO.getVehicleNo()==null?"":openRequestBO.getVehicleNo());
			address.put("Flag", openRequestBO.getDrProfile()==null?"":openRequestBO.getDrProfile());
			String driverFlagValue = SystemUtils.getCompanyFlagDescription(srvletConfig, adminBO.getMasterAssociateCode(),  openRequestBO.getDrProfile()==null?"":openRequestBO.getDrProfile(), ";", 0);
			address.put("FLags", driverFlagValue);
			address.put("TripId", openRequestBO.getTripid()==null?"":openRequestBO.getTripid());
			address.put("SharedRide", openRequestBO.getTypeOfRide()==null?"":openRequestBO.getTypeOfRide());
			address.put("multiJobTag", openRequestBO.getRepeatGroup()==null?"":openRequestBO.getRepeatGroup());
			address.put("No.Of.Pass", openRequestBO.getNumOfPassengers()==0?"":openRequestBO.getNumOfPassengers());
			address.put("DropTime", openRequestBO.getDropTime()==null?"":openRequestBO.getDropTime());
			address.put("Prem.Cust",openRequestBO.getPremiumCustomer());
			address.put("AdvanceTime",openRequestBO.getAdvanceTime()==null?"-1":openRequestBO.getAdvanceTime());
			address.put("EMail",openRequestBO.getEmail()==null?"":openRequestBO.getEmail());
			address.put("Rating",openRequestBO.getJobRating());
			address.put("Fleet",openRequestBO.getAssociateCode());
			address.put("CallerPh",openRequestBO.getCallerPhone()==null?"":openRequestBO.getCallerPhone());
			address.put("CallerName",openRequestBO.getCallerName()==null?"":openRequestBO.getCallerName());
			address.put("MeterType",openRequestBO.getPaymentMeter());
			address.put("AirName", openRequestBO.getAirName()==null?"":openRequestBO.getAirName());
			address.put("AirNo", openRequestBO.getAirNo()==null?"":openRequestBO.getAirNo());
			address.put("AirFROM", openRequestBO.getAirFrom()==null?"":openRequestBO.getAirFrom());
			address.put("AirTO", openRequestBO.getAirTo()==null?"":openRequestBO.getAirTo());
			
			address.put("Ref", openRequestBO.getRefNumber()==null?"":openRequestBO.getRefNumber());
			address.put("Ref1", openRequestBO.getRefNumber1()==null?"":openRequestBO.getRefNumber1());
			address.put("Ref2", openRequestBO.getRefNumber2()==null?"":openRequestBO.getRefNumber2());
			address.put("Ref3", openRequestBO.getRefNumber3()==null?"":openRequestBO.getRefNumber3());
			
			array.put(address);			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return array.toString();
	}

	public static String pushToCustomer(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
		JSONArray array = new JSONArray();
		ArrayList<OpenRequestBO> Address = RequestDAO.getAddress("103","5618293323");
		for(int i=0;i<Address.size();i++){
			JSONObject address = new JSONObject();
			try {
				address.put("A1", Address.get(i).getSadd1()==null?"":Address.get(i).getSadd1());
				address.put("A2", Address.get(i).getSadd2()==null?"":Address.get(i).getSadd2());
				address.put("C", Address.get(i).getScity()==null?"":Address.get(i).getScity());
				address.put("S", Address.get(i).getSstate()==null?"":Address.get(i).getSstate());
				address.put("Z", Address.get(i).getSzip()==null?"":Address.get(i).getSzip());
				address.put("P", Address.get(i).getPaytype()==null?"":Address.get(i).getPaytype());
				address.put("A", Address.get(i).getAcct()==null?"":Address.get(i).getAcct());
				address.put("La", Address.get(i).getSlat()==null?"":Address.get(i).getSlat());
				address.put("Lo", Address.get(i).getSlong()==null?"":Address.get(i).getSlong());
				address.put("M", Address.get(i).getMasterAddressKey()==null?"":Address.get(i).getMasterAddressKey());
				array.put(address);			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		return responseString;
	}


}
