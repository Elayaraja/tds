package com.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetAddress {
	public static String getGeoAddress(String lat, String lon){
		String latlon = lat+","+lon;
		System.out.println("latlon:"+latlon);
		String formatted_address = "";
		try {
			URL url = new URL("http://maps.googleapis.com/maps/api/geocode/json?latlng="+latlon+"&sensor=true&key=AIzaSyARyq57c0Fvj2DEmsvbP4B-pmROaXSoK0I");
			// making connection
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}

			// Reading data's from url
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			String out="";
			//System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				//System.out.println(output);
				out+=output;
			}
			// Converting Json formatted string into JSON object
			System.out.println("OUT:"+out);
			JSONObject json = new JSONObject(out);
			JSONArray results=json.getJSONArray("results");
			JSONObject rec = results.getJSONObject(0);
			//JSONArray address_components=rec.getJSONArray("address_components");
			/*for(int i=0;i<address_components.length();i++){
				JSONObject rec1 = address_components.getJSONObject(i);
				//trace(rec1.getString("long_name"));
				JSONArray types=rec1.getJSONArray("types");
				String comp=types.getString(0);
	
				if(comp.equals("locality")){
					System.out.println("city ————-"+rec1.getString("long_name"));
				}
				else if(comp.equals("country")){
					System.out.println("country ———-"+rec1.getString("long_name"));
				}
			}*/
			formatted_address = rec.getString("formatted_address");
			System.out.println("formatted_address————–"+formatted_address);
			conn.disconnect();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("MAL exception : "+e.getMessage());
			//e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("I/O exception : "+e.getMessage());
			//e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			System.out.println("json exception : "+e.getMessage());
			//e.printStackTrace();
		}
		
		return formatted_address;
		
	}
}
