package com.common.util;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TDSValidation {
	
	public static boolean chckAllFlgExist(String drPrf,String openReqPrf)
 	{
		
		openReqPrf = openReqPrf.replace('|', ' ');
		char[] openReqCharArray = openReqPrf.toCharArray();
		
		if(drPrf.equals(""))
		{
			return false;
		}
		
		for(int i=0;i<openReqCharArray.length;i++)
		{
			if(drPrf.indexOf(openReqCharArray[i]) ==-1)
			{
				return false;
			}
		}
		
		return true;
	}
	public static String dateFormatmmddyyyy(String p_date,String dateFormat) {
		String returnDate = "";
		String[] date=null;
		if(p_date!=null){
			if(dateFormat.equals("1")){
				returnDate=p_date;
			}else if(dateFormat.equals("2")){
				date=p_date.split("/");
				returnDate=date[1]+"/"+date[0]+"/"+date[2];
			}else if(dateFormat.equals("2")){
				date=p_date.split("/");
				returnDate=date[1]+"/"+date[0]+"/"+date[2];
			}else if(dateFormat.equals("3")){
				returnDate=p_date.replace("-", "/");
			}else if(dateFormat.equals("4")){
				date=p_date.split("-");
				returnDate=date[1]+"/"+date[0]+"/"+date[2];
			}else if(dateFormat.equals("5")){
				date=p_date.split("-");
				returnDate=date[1]+"/"+date[2]+"/"+date[0];
			}
		}
		return returnDate;
	}
	public static String formatIntoHHMMSS(int secsIn)
	{

	int hours = secsIn / 3600,
	remainder = secsIn % 3600,
	minutes = remainder / 60,
	seconds = remainder % 60;

	return ( (hours < 10 ? "0" : "") + hours
	+ ":" + (minutes < 10 ? "0" : "") + minutes
	+ ":" + (seconds< 10 ? "0" : "") + seconds );

	}
	public static boolean is24HrsFormat(String p_time) {
	  	
		if(p_time.length() !=4)
		{
			return false;
		} else {
			
			 
				if ((isOnlyNumbers(p_time.substring(0,2)) == true)	
						&& (Integer.parseInt(p_time.substring(0,2)) <= 24) 
						&& (isOnlyNumbers(p_time.substring(2,4)) == true)
						&& (Integer.parseInt(p_time.substring(2,4)) <= 60)) {
					if	((Integer.parseInt(p_time.substring(0,2)) == 24) )
						if((Integer.parseInt(p_time.substring(2,4)) == 0))
							return true;
						else
							return false;
					else
						return true;
				} else {
					return false;
				}
 			 
		}
	 	 
		
	}
	
	public static String getUserDateFormatDMYHM(String p_date) {
		String returnDate = "";
		if(p_date == null){
			return "";
		}
		else if(p_date.equalsIgnoreCase("")){
			return "";
		}
		else {
			returnDate = p_date.substring(8,10)+"/"+p_date.substring(5,7)+"/"+p_date.substring(0,4)+" "+p_date.substring(10,16);
		}
		return returnDate;
	}

	public static String get24HrsFormat(String p_time,String am) {
		
		int hour=0,min=0;
		 
		 if(p_time.indexOf(":")>0)
			{
			  hour= Integer.parseInt(p_time.substring(0,p_time.indexOf(":")));
			  min= Integer.parseInt(p_time.substring(p_time.indexOf(":")+1,p_time.length()));
			 
			 //System.out.println("===Hour:" + hour);
			 //System.out.println("===Mins:" + min);
			  
			 if(am.equalsIgnoreCase("PM"))
			 {
				 if(hour == 12)
					 hour = 0;
					  
				 hour = hour+12;
			 
			 } else if(hour == 12)
			 {
				 hour = 0;
			 }
			 
			}
		 
		 //System.out.println(""+(hour<10?("0"+hour):hour)+":"+(min<10?("0"+min):min)+":00");
		 
		 return ((hour<10?("0"+hour):hour)+":"+(min<10?("0"+min):min)+":00");
	}
	public static boolean get24timeFormat(String p_time) {
		
		   int hour=0,min=0;
				  hour=Integer.parseInt(p_time.substring(0,2));
				  min= Integer.parseInt(p_time.substring(2,4));
				  //System.out.println("===Hour:" + hour);
				  //System.out.println("===Mins:" + min);
				  
				 if(hour > 24 || hour < 0 || min > 59 || min < 0)
				 {
					 return false;
				 } else {
					 return true;
				 }
				 
				}

	public static boolean isDoubleNumber(String p_str) {
			
		boolean status = false;
		try{
			
			Double.parseDouble(p_str);
			status = true;
		}catch (NumberFormatException e) {
			// TODO: handle exception
			status =false;
		}
		
		return status;
	}
	public static boolean isOnlyNumbers(String p_str) {
		boolean status = false;
		int counter = 0;
		if(p_str.equalsIgnoreCase("")) {
			return status;
		}
		while (counter < p_str.length()) {
			if ((p_str.charAt(counter) == '0')
					|| (p_str.charAt(counter) == '1')
					|| (p_str.charAt(counter) == '2')
					|| (p_str.charAt(counter) == '3')
					|| (p_str.charAt(counter) == '4')
					|| (p_str.charAt(counter) == '5')
					|| (p_str.charAt(counter) == '6')
					|| (p_str.charAt(counter) == '7')
					|| (p_str.charAt(counter) == '8')
					|| (p_str.charAt(counter) == '9')) {
			} else {
				if (!(counter == 0 && p_str.charAt(counter) == '-')) {
					return status;
				}
			}
			counter = counter + 1;
		}
		if(counter == p_str.length()) {
			status = true;
		}
		return status;// true->Yes, It contains only Numbers
	}
	
	public static boolean isOnlyAlphabetsAndNumbersWithAllowedSpecialChars(String p_str) {// To check
		// whether, it
		// contains any
		// special
		// characters
		// other than
		// Numbers,
		// Alphabets
		boolean status = false;
		int counter = 0;
		while (counter < p_str.length()) {
			if ((p_str.charAt(counter) == '~')
					|| (p_str.charAt(counter) == '`')
					|| (p_str.charAt(counter) == '!')
					|| (p_str.charAt(counter) == '#')
					|| (p_str.charAt(counter) == '$')
					|| (p_str.charAt(counter) == '%')
					|| (p_str.charAt(counter) == '^')
					|| (p_str.charAt(counter) == '&')
					|| (p_str.charAt(counter) == '*')
					|| (p_str.charAt(counter) == '(')
					|| (p_str.charAt(counter) == ')')) {
				return status;
			}
			if ((p_str.charAt(counter) == '+')
					|| (p_str.charAt(counter) == '=')
					|| (p_str.charAt(counter) == '|')
					|| (p_str.charAt(counter) == '\\')
					|| (p_str.charAt(counter) == '{')
					|| (p_str.charAt(counter) == '[')
					|| (p_str.charAt(counter) == '}')
					|| (p_str.charAt(counter) == ']')
					|| (p_str.charAt(counter) == ':')
					|| (p_str.charAt(counter) == ';')
					|| (p_str.charAt(counter) == '"')) {
				return status;
			}
			if ((p_str.charAt(counter) == '\'')
					|| (p_str.charAt(counter) == '<')
					|| (p_str.charAt(counter) == ',')
					|| (p_str.charAt(counter) == '>')
					|| (p_str.charAt(counter) == '.')
					|| (p_str.charAt(counter) == '?')
					|| (p_str.charAt(counter) == '/')
					|| (p_str.charAt(counter) == '-')
					|| (p_str.charAt(counter) == ' ')
					|| (p_str.charAt(counter) == '\b')) {
				return status;
			}
			counter = counter + 1;
		}
		return true;// true->Yes, It contains only Number & Alphabets
	}
	
	public static boolean isValidDate(String p_date, String p_month, String p_year) {
		// Return Value true -> Yes, It is a valid Date
		// false -> No, It is not a valid Date

		boolean status = false;
		String date = p_date;
		String month = p_month;
		String year = p_year;
		int maxDate = 0;

		if (date.equals("") && month.equals("") && year.equals("")) {
			return status;
		} else {
			// Empty or numeric Validation
			if (date.equals("") || isOnlyNumbers(date) != true || date.length() != 2)
				return status;
			if (month.equals("") || isOnlyNumbers(month) != true || month.length() != 2)
				return status;
			if (year.equals("") || isOnlyNumbers(year) != true || year.length() != 4)
				return status;

			/*if ((Integer.parseInt(date) == 0) || (Integer.parseInt(month) == 0) || (Integer.parseInt(year) == 0))
				return status;*/

			// Feb month validation
			if ((Integer.parseInt(year) % 4) == 0)	maxDate = 29;
			else	maxDate = 28;

			if (Integer.parseInt(month) > 12) {		return status;}
			else {
				if (month.equals("2") || month.equals("02")) {
					if (Integer.parseInt(date) > maxDate) {	return status;	}
				} else if (month.equals("1") || month.equals("01")
						|| month.equals("3") || month.equals("03")
						|| month.equals("5") || month.equals("05")
						|| month.equals("7") || month.equals("07")
						|| month.equals("8") || month.equals("08")
						|| month.equals("10") || month.equals("10")
						|| month.equals("12") || month.equals("12")) {
					if (Integer.parseInt(date) > 31) {	return status;}
				} else {
					if (Integer.parseInt(date) > 30) {	return status;	}
				}
			}
			return true;
		}
	}
	
	
	
	public static boolean chechPhoneNo(String p_phno) {
		boolean status = false;
		//if(isOnlyNumbers(p_phno)) {
			if(p_phno.length() == 10) {
				status = true;
			}
		//}
		return status;
	}
	
	public static boolean isValidZipCode(String p_zipcode) {
		boolean status = false;
		//if(isOnlyNumbers(p_zipcode)) {
		if(p_zipcode.length() == 5 || p_zipcode.length() == 10 ) {
			status = true;
			//}
			
		}
		return status;
	}
	
	public static boolean isValidHour(String p_time) {
		if ((p_time != null) && (p_time.length() <= 2)
				&& (isOnlyNumbers(p_time) == true)
				&& (Integer.parseInt(p_time) <= 24) ) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isValidMinutes(String p_min) {
		if ((p_min != null) && (p_min.length() <= 2)
				&& (isOnlyNumbers(p_min) == true)
				&& (Integer.parseInt(p_min) <= 60)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isRightPhoneNumber(String p_str) {
		boolean status = false;
		int counter = 0;
		while (counter < p_str.length()) {
			if ((isOnlyNumbers(p_str.substring(counter, counter + 1)) == true)) {
			} else {
				if ((p_str.charAt(counter) != '(')
						&& (p_str.charAt(counter) != ')')
						&& (p_str.charAt(counter) != '-')
						&& (p_str.charAt(counter) != '_')
						&& (p_str.charAt(counter) != ' ')
						&& (p_str.charAt(counter) != '\b')) {
					return status;
				}
			}
			counter = counter + 1;
		}
		return true;// true->Yes, It is a valid Phone Number
	}
	
	public static String getUserDateFormat(String p_date) {
		String returnDate = "";
		if(p_date == null){
			return "";
		}
		else if(p_date.equalsIgnoreCase("")){
			return "";
		}
		else if(p_date.length() == 8) {
			returnDate = p_date.substring(4,6)+"/"+p_date.substring(6,8)+"/"+p_date.substring(0,4);
		}
		return returnDate;
	}
	
	 
	
	public static String getDBPhoneFormat(String p_phone) {
		String result = "";
		try{
		if(p_phone.length() == 12) {
			//System.out.println("Phone "+p_phone);
			String phone[] = p_phone.split("-");
			result = phone[0] + phone[1] + phone[2];
			//System.out.println("Phone "+result);
		} else if(p_phone.length() == 10){
			result = p_phone;
		}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
	}
	
	public static String getViewPhoneFormat (String p_phone) {
			if(p_phone == null){
				return "";
			}else if(p_phone.equalsIgnoreCase("")){
				return "";
			}
			else{
				return p_phone.substring(0,3)+"-"+p_phone.substring(3,6)+"-"+p_phone.substring(6,10);}
	}
	
	public static String getDBdateFormat(String p_date) {
		String result = "";
		if(p_date.length() == 10) {
			String date[] = p_date.split("/");
			result = date[2]+date[0]+date[1];
		}
		return result;
	}
	public static String getTimeStampDateFormat(String p_date) {
		String result = "";
		if(p_date.length() == 10) {
			String date[] = p_date.split("/");
			result = date[2]+"-"+date[0]+"-"+date[1];
		}
		return result;
	}
	
	
	public static String getTimsStampDate(String date, String time) {
		 String result="";
		 String result1="";
		 String result2="";

		 String stampdate="";
		 String stamptime=""; 
		 if(date.length() == 10) {
			 String data[] =date.split("/");
			 result = data[2]+"-"+data[0]+"-"+data[1]+" "; 
		 }
		 //System.out.println("Time value:"+time+"length of time:"+time.length());
		 if(time.length()==4){
			 result1 = time.substring(0,2)+":"+time.substring(2,4)+":00";
		 }else{
		 Calendar cal = Calendar.getInstance();
		 cal.getTime();
		 SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		 //System.out.println( sdf.format(cal.getTime()) );
		 result1 = sdf.format(cal.getTime()) ;
		 //System.out.println("resull1::"+result1);
		 }
		 result2 = result+""+result1; 
		 //System.out.println("resul::"+result2);
		 return result2;
	} 
	public static String gettimeStampTime(String p_time) {
		String result="";
		result = p_time.substring(0,2)+":"+p_time.substring(2,4).concat(":00");
		return result;
	}
	
	public static String getUserTimeFormat(String p_time) {
		String result = "";
		if(p_time == null){
			return "";
		}
		else if(p_time.equalsIgnoreCase("")){
			return "";
		}
		else if(p_time.length() == 4) {
			result = p_time.substring(0,2)+":"+p_time.substring(2,4);
		}else if(p_time.length()==3){
			result = p_time.substring(0,1)+":"+p_time.substring(1,3);
		}
		return result;
	}

	public static String getUserDateFormatMDY(String p_date) {
		String returnDate = "";
		if(p_date == null){
			return "";
		}
		else if(p_date.equalsIgnoreCase("")){
			return "";
		}
		else  {
			returnDate = p_date.substring(5,7)+"/"+p_date.substring(8,10)+"/"+p_date.substring(0,4);
		}
		return returnDate;
	}
	

	public static String getDBTimeFormat(String p_time) {
		String result = "";
		if(p_time.length() == 5) {
			String time[] = p_time.split(":");
			result = time[0]+time[1];
		}
		return result;
	}
	
	public static int isValidTime(String p_time)
	{	
		if(p_time=="")
			return 0;
		if(p_time.indexOf(":")>0)
		{
		String hour=p_time.substring(0,p_time.indexOf(":"));
		String min=p_time.substring(p_time.indexOf(":")+1,p_time.length());
		if ((hour != null) && (hour.length() <= 2)
				&& (TDSValidation.isOnlyNumbers(hour))
				&& (Integer.parseInt(hour) <= 24) && (min != null) && (min.length() <= 2)
				&& (TDSValidation.isOnlyNumbers(min))
				&& (Integer.parseInt(min) <= 59)) {
			return 1;
		} else {
			return 0;
			   }
		}
		return 0;
	}
	
	public static int isValid12HrsTime(String p_time)
	{	
		if(p_time=="")
			return 0;
		if(p_time.indexOf(":")>0)
		{
		String hour=p_time.substring(0,p_time.indexOf(":"));
		String min=p_time.substring(p_time.indexOf(":")+1,p_time.length());
		if ((hour != null) && (hour.length() <= 2)
				&& (TDSValidation.isOnlyNumbers(hour))
				&& (Integer.parseInt(hour) <= 12) && (min != null) && (min.length() <= 2)
				&& (TDSValidation.isOnlyNumbers(min))
				&& (Integer.parseInt(min) <= 59)) {
			return 1;
		} else {
			return 0;
			   }
		}
		return 0;
	}
	
	public static byte[] getDefaultsign(String Path) {
		
		byte [] buffer = null;
		try{
				      FileInputStream fileInuptStream = new FileInputStream(Path);
				      BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInuptStream);
				      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				      
				      int start = 0;
				      int length = 1024;
				      int offset = -1;
				      buffer = new byte [length];  
				      
				      while((offset = bufferedInputStream.read(buffer, start, length)) != -1)
				      {
				            byteArrayOutputStream.write(buffer, start, offset);
				      }
			 
				      bufferedInputStream.close();  
				      byteArrayOutputStream.flush();     
				      buffer = byteArrayOutputStream.toByteArray();
				       			 		
				} catch (Exception ee) {
					System.out.println("default image storage error::"+ee);
				}
				return buffer;
	}
	public static String getNowFomat(String dateAndTime){
		String result="";
		String time="";
		int hour;
		int min;
		int sec;
		String M;
		
		//System.out.println("date and time in the validation:"+dateandtime);
		result=dateAndTime.substring(5,7)+"-"+dateAndTime.substring(8,10)+"-"+dateAndTime.substring(0,4);
		time = dateAndTime.substring(11);
		hour= Integer.parseInt(time.substring(0, 2));
		min=Integer.parseInt(time.substring(3,5));
		sec=Integer.parseInt(time.substring(6,8));
		if(hour>12)
		{
			hour=hour-12;
			M="PM";
		}
		else
			M="AM";
		result=result+" "+hour+":"+min+":"+sec+"  "+M;	
	return result;
		}
	
	
	//Added by saravanan for Date validation
	public static boolean validateDate(String dateStr, String formatStr){

	if (formatStr == null) return false;
		SimpleDateFormat df = new SimpleDateFormat(formatStr);
		Date testDate = null;
		try{
			testDate = df.parse(dateStr);
		}
		catch (ParseException e){
			// invalid date format
			return false;
		}
			// now test for legal values of parameters
			if (!df.format(testDate).equals(dateStr)) return false;
			return true;
	}	
	
	
	//Added by saravanan for future Date validation
	public static boolean allowDate(String dateStr, boolean allowPast, String formatStr){

		if (formatStr == null) return false;
			SimpleDateFormat df = new SimpleDateFormat(formatStr);
			Date testDate = null;
			try{
				testDate = df.parse(dateStr);
			}
			catch (ParseException e){
				// invalid date format
				return false;
			}

			if (!allowPast){			
				// initialise the calendar to midnight to prevent
				// the current day from being rejected
				Calendar cal = Calendar.getInstance();	
				cal.set(Calendar.HOUR_OF_DAY, 0);		
				cal.set(Calendar.MINUTE, 0);		
				cal.set(Calendar.SECOND, 0);		
				cal.set(Calendar.MILLISECOND, 0);		
				if (cal.getTime().after(testDate)) return false;
			}
			
			return true;
			
		}	
	
		//Added by saravanan for Month and Date single validation
		public static String getValidDate(String p_date) {
			String result = "";
			if(p_date.length() != 10) {
				String date[] = p_date.split("/");				
				if(date[0].length() == 1){						
					date[0] = "0"+date[0];
				}
				if(date[1].length() == 1){						
					date[1] = "0"+date[1];
				}				
				result = date[0].concat("/").concat(date[1]).concat("/").concat(date[2]);				
			}else{
				result = p_date;
			}
			return result;
		}
	
}
