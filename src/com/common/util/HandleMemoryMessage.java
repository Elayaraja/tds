package com.common.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import javax.servlet.ServletContext;

import org.apache.log4j.Category;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.controller.TDSController;
import com.tds.dao.MessageDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.tdsBO.DriverLocationBO;

@SuppressWarnings("deprecation")
public class HandleMemoryMessage {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(HandleMemoryMessage.class.getName());

	}

	public static void sendMessage(ServletContext config, String associationCode,ArrayList<DriverCabQueueBean> drivers, String message){
		if(TDSProperties.getValue("pushMessageProtocol").equalsIgnoreCase("1")){
			MessageDAO.storeMessageToDB(associationCode, drivers, message);
		} else {
			for(int i=0;i<drivers.size();i++){
				sendMessage(config, associationCode, drivers.get(i).getDriverid(), message);
			}
		}
	}
	public static void sendMessage(ServletContext config, String associationCode,String[] drivers, String message){
		for(int i=0;i<drivers.length;i++){
			sendMessage(config, associationCode, drivers[i], message);
		}
	}
	public static void sendMessage(ServletContext context, String associationCode,String driver, String message){
		if(TDSProperties.getValue("pushMessageProtocol").equalsIgnoreCase("1")){
			MessageDAO.storeMessageToDBForADriver(associationCode, driver, message);
		} else {
			String previousMessage=(String) context.getAttribute(associationCode+"-"+driver+"MSG");
			if(previousMessage==null){
				context.setAttribute(associationCode+"-"+driver+"MSG", message);
			} else {
				context.setAttribute(associationCode+"-"+driver+"MSG", previousMessage+"^"+message);
			}
		}
	}

	public static void removeMessage(ServletContext context, String associationCode,ArrayList<DriverCabQueueBean> drivers){
		for(int i=0;i<drivers.size();i++){
			removeMessage(context,associationCode,drivers.get(i).getDriverid());
		}
	}
	public static void removeMessage(ServletContext context, String associationCode,String[] drivers){
		for(int i=0;i<drivers.length;i++){
			removeMessage(context,associationCode,drivers[i]);
		}
	}
	public static void removeMessage(ServletContext context, String associationCode,String driverID){
		if(TDSProperties.getValue("pushMessageProtocol").equalsIgnoreCase("1")){
			MessageDAO.removeDriverMessage(driverID, associationCode);
		} else {
			context.removeAttribute(associationCode+"-"+driverID+"MSG");
		}
	}
	public static String readMessage(ServletContext context, String associationCode,String driver,DriverLocationBO driverLocBO,int type,String dummyCode){
		String earlyMessage="";
		String updateGPS = SystemPropertiesDAO.getParameter(associationCode, "storeHistory");
		int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
		DriverLocationBO prevDriverLocationBO = null;
		if(isUpdateGPS==1){
			prevDriverLocationBO = ServiceRequestDAO.getPrevDriverLocationBO(associationCode, driverLocBO.getDriverid());
		}
		
		if(TDSProperties.getValue("pushMessageProtocol").equalsIgnoreCase("1")){
			if(type==1){
				driverLocBO.setStatus("");
			}
			ServiceRequestDAO.updateDriverLocation(driverLocBO,associationCode);
			earlyMessage=MessageDAO.getMessageAndUpdateForDriver(driverLocBO,type,associationCode,dummyCode);
		} else {
			ServiceRequestDAO.updateDriverLocation(driverLocBO,associationCode);
			earlyMessage=(String) context.getAttribute(associationCode+"-"+driver+"MSG");
			context.removeAttribute(associationCode+"-"+driver+"MSG");
		}
		if(isUpdateGPS==1){
			if(prevDriverLocationBO!=null){
				if(!driverLocBO.getLatitude().equals(0.0) && !driverLocBO.getLatitude().equals(0)){
					//System.out.println("prev lat and lon---"+prevDriverLocationBO.getLatitude()+"--"+prevDriverLocationBO.getLongitude());
					if(prevDriverLocationBO.getLatitude().equals(""+roundDecimal(driverLocBO.getLatitude(),6)) && prevDriverLocationBO.getLongitude().equals(""+roundDecimal(driverLocBO.getLongitude(),6))){
						//System.out.println("same lat and lon");
					}else{
						ServiceRequestDAO.insertDriverGPS(associationCode, driverLocBO.getDriverid(), driverLocBO.getLatitude(), driverLocBO.getLongitude(), driverLocBO.getVehicleNo(), 8, "");
					}
				}else{
					//System.out.println("lat 0");
				}
				
			}else{
				//System.out.println("previous empty");
				ServiceRequestDAO.insertDriverGPS(associationCode, driverLocBO.getDriverid(), driverLocBO.getLatitude(), driverLocBO.getLongitude(), driverLocBO.getVehicleNo(), 8, "");
			}
		}
		return earlyMessage==null?"":earlyMessage;
	}
	
	private static String roundDecimal(String value, final int decimalPlace) {
		double value1 = Double.parseDouble(value);
		BigDecimal bd = new BigDecimal(value1);
		
		bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
		String returnValue = ""+bd;
		
		//System.out.println("value:"+returnValue);
		return returnValue;
	}
}
