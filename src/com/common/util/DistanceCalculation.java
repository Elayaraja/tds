package com.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public final class DistanceCalculation {

	public static double distance(double lat1,double lat2,double lon1,double lon2){
	 double earthRadius = 3958.75;
	  double dLat = Math.toRadians(lat2-lat1);
	    double dLng = Math.toRadians(lon2-lon1);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	               Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
	               Math.sin(dLng/2) * Math.sin(dLng/2);
	    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	    double distance = earthRadius * c;
		
	
		return distance;
	}

    public static String getdistance(String lat,String lon,String lat1,String lon1){
    
      Double stlat = Double.parseDouble(lat);
     Double stlon = Double.parseDouble(lon);
     Double slatlon = stlat + stlon;
     Double enlat = Double.parseDouble(lat1);
     Double enlon = Double.parseDouble(lon1);
     Double enlatlon = stlat + stlon;
   	String postData="&from='"+slatlon+"'&to='"+enlatlon+"'";
	String str="https://open.mapquestapi.com/directions/v1/route?key=ZX3FD32ZdJ8LfqzB2cKPMD6WDiuSbwTE&outFormat=json&routeType=fastest&timeType=1&enhancedNarrative=false&locale=en_US&unit=m&drivingStyle=2&highwayEfficiency=21.0";
	String response = "";
		String getdistance="";
    	try {
			URL url1=new URL(str);
			System.setProperty("http.keepAlive", "true");
			HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(5000);
		//	conn.setRequestProperty("cookie", "JSESSIONID=" + SessionPropertiesBean.getSessionID());
			PrintStream ps = new PrintStream(conn.getOutputStream());
			ps.write(postData.getBytes());
			ps.flush();
			ps.close();

			conn.connect();

			if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
				InputStream is = conn.getInputStream();
				StringBuffer b = new StringBuffer();
				BufferedReader r = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = r.readLine()) != null)
					b.append(line);
				is.close();
				response = b.toString();
				String[] dis=response.split("\"distance\":");
				String[] finalDistance=dis[1].split(",");
			     getdistance=finalDistance[0];
				conn.disconnect();

			} else {
				response = "GRACIERROR;";
			}

		}
		catch (Exception e) {
			if (e != null) {
				e.printStackTrace();
			}
			response = "GRACIERROR;" + e.getMessage();
		}
		return getdistance;
		}}

		


