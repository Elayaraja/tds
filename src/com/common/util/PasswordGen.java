package com.common.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import com.google.gson.Gson;
import com.tds.controller.SystemUnavailableException;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.AdminRegistrationBO;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONArray;
import org.json.JSONObject;

public final class PasswordGen{

public static String generatePassword(String KindOfPassword, int numberOfCharacters){
	String alphaNumeric = "abcdefghijklmnopqrstuvwxyzABCDEFGHOJKLMNOPQRSTUVWXYZ0123456789";
	String number = "0123456789";
	String returnValue = "";
	int maxRandom = 10;

	if(KindOfPassword.equals("A")){
		maxRandom = 62;
	}

	for(int i=0;i<numberOfCharacters; i++){
		int random = (int)(Math.random()*maxRandom);
		if(KindOfPassword.equals("A")){
			returnValue = returnValue + alphaNumeric.substring(random, random+1);
		}else
			returnValue = returnValue + number.substring(random, random+1);				
	}
	return returnValue;
 
}

}
