package com.common.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.tds.cmp.bean.SMSBean;

public class SendNexmoMessages {
	public static void makeProcess(SMSBean smsBean, String toPhone, String message) {

		System.out.println("Started Nexmo sms");
		String response = "<Status StatusCode = \"Retry\"/>";
		HttpURLConnection conn;
		PrintStream ps;
		InputStream is;
		StringBuffer b;
		BufferedReader r;

		System.setProperty("http.keepAlive", "true");
		try {
			URL url1;
			String encodedData = "type=text&api_key="+smsBean.getUserName()+"&api_secret="+smsBean.getPassword()+"&to="+toPhone+"&text="+message;
			if(smsBean.getType()==1){
				url1 = new URL("https://rest.nexmo.com/sc/us/2fa/json?");
				encodedData = encodedData+"&pin="+smsBean.getPhone();
			}else{
				url1 = new URL("https://rest.nexmo.com/sms/xml?");
				encodedData = encodedData+"&from="+smsBean.getPhone();
			}
			
			conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			conn.setRequestProperty("Connection","Keep-Alive");
			conn.setRequestProperty("Cache-Control","no-cache");
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

			//System.out.println("Encode Data--->"+encodedData);
			conn.setRequestProperty("Content-Length",""+encodedData.length());

			DataOutputStream requestObject = new DataOutputStream( conn.getOutputStream() );
			requestObject.write(encodedData.getBytes());
			requestObject.flush();
			requestObject.close();

			is = conn.getInputStream();
			b = new StringBuffer();
			r = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = r.readLine()) != null)
				b.append(line);
			System.out.println("Reply From Site:" + b.toString());
			is.close();
			response = b.toString();
			conn.disconnect();
		}catch(Exception e){
			response = "GRACIERROR;" + e.getMessage();
			System.out.println(response);
		}
		return;
	}

}
