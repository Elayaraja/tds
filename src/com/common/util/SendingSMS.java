package com.common.util;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.woden.wsdl20.Description;

/**
 * @author vimal
 * @category Class
 * @see Description
 * This is class is used to send a Mail Using Java Mail API
 * 
 */
public class SendingSMS {

	/*private static String SMTP_HOST_NAME = TDSProperties.getValue("mailHostName");// "smtp.gmail.comsmtp.mail.yahoo.co.in"
	private static String SMTP_AUTH_USER = TDSProperties.getValue("mailUserName");
	private static String SMTP_AUTH_PWD = TDSProperties.getValue("mailPassword");
	private static String emailFromAddress = TDSProperties.getValue("mailFromAddress");
	private static String mailPortNo  = TDSProperties.getValue("mailPortNo");*/
	//private Category _cat = TDSController.cat;
	
	private static String SMTP_HOST_NAME = "";
	private static String SMTP_AUTH_USER = "";
	private static String SMTP_AUTH_PWD = "";
	private static String emailFromAddress = "";
	private static String mailPortNo  = "";
	private static String mailProtocol  = "";
	
	public SendingSMS(String SMTP_HOST_NAME,String SMTP_AUTH_USER,String SMTP_AUTH_PWD,String emailFromAddress,String mailPortNo,String mailProtocol)
	{
		this.SMTP_AUTH_PWD = SMTP_AUTH_PWD;
		this.SMTP_AUTH_USER = SMTP_AUTH_USER;
		this.SMTP_HOST_NAME  = SMTP_HOST_NAME;
		this.emailFromAddress = emailFromAddress;
		this.mailPortNo = mailPortNo;
		this.mailProtocol= mailProtocol;
	}
	/**
	 * @param p_phoneNo
	 * @param p_subject
	 * @param p_message
	 * @throws MessagingException
	 * @author vimal
	 * @see Description
	 * This sendMail method is used to send mail through JavaMail API The input fields are to address, subject
	 * Message.
	 * 
	 */
	public void sendMail(String toAddress,String p_message) throws MessagingException {
		//System.out.println("Send Mail Method "+toAddress);
		//_cat.info("Send Mail Method "+toAddress);
		Properties m_properties  = System.getProperties();
		m_properties.put("mail.transport.protocol", mailProtocol);
		m_properties.put("mail.smtp.port", mailPortNo);
		m_properties.put("mail.smtp.host",SMTP_HOST_NAME);
		m_properties.put("mail.smtp.auth", "true");
		m_properties.put("mail.smtp.starttls.enable","true");
		Authenticator m_authentication =  new SMTPAuthenticator();
		Session m_session = Session.getDefaultInstance(m_properties,m_authentication);
		m_session.setDebug(true);
		Message m_message = new MimeMessage(m_session);
		Address m_internetFromAddress = new InternetAddress(emailFromAddress);
		m_message.setFrom(m_internetFromAddress);
		Address m_internetToAddress = new InternetAddress(toAddress); //!!!! After Test Enable !!!!
		//Address m_internetToAddress = new InternetAddress("vijayan.j@amshuhu.com");
		m_message.setRecipient(Message.RecipientType.TO, m_internetToAddress);
		//m_message.setSubject(p_subject);
		m_message.setText(p_message.trim());//( p_message,"text/plain");
		Transport.send(m_message);
	}
	
	/**
	 * @param p_toAddress
	 * @param p_message
	 * @throws MessagingException
	 * @author vimal
	 * @see Description
	 * This sendMail method is used to send mail through JavaMail API. Send a mail to multiple address, subject
	 * Message.
	 */
	public void sendMail(String p_toAddress[],String p_message) throws MessagingException {
		int m_counter = 0;
		Properties m_properties  = System.getProperties();
		m_properties.put("mail.transport.protocol", mailProtocol);
		m_properties.put("mail.smtp.port", mailPortNo);
		m_properties.put("mail.smtp.host",SMTP_HOST_NAME);
		m_properties.put("mail.smtp.auth", "true");
		m_properties.put("mail.smtp.starttls.enable","true");
		Authenticator m_authentication =  new SMTPAuthenticator();
		Session m_session = Session.getDefaultInstance(m_properties,m_authentication);
		m_session.setDebug(true);
		Message m_message = new MimeMessage(m_session);
		Address m_internetFromAddress = new InternetAddress(emailFromAddress);
		m_message.setFrom(m_internetFromAddress);
		Address[] m_internetToAddress = new InternetAddress[p_toAddress.length];
		for(m_counter=0;m_counter < p_toAddress.length; m_counter ++) {
			m_internetToAddress[m_counter] = new InternetAddress(p_toAddress[m_counter]);
		}
		m_message.setRecipients(Message.RecipientType.TO, m_internetToAddress);
		//m_message.setSubject(p_subject);
		m_message.setText(p_message.trim());//( p_message,"text/plain");
		Transport.send(m_message);
	}
	
	/**
	 * @author vimal
	 * @see Description
	 * SimpleAuthenticator is used to do simple authentication
	 * when the SMTP server requires it.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator
	{

	    @Override
		public PasswordAuthentication getPasswordAuthentication()
	    {
	        String username = SMTP_AUTH_USER;
	        String password = SMTP_AUTH_PWD;
	        return new PasswordAuthentication(username, password);
	    }
	}
}
