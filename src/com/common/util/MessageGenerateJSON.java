package com.common.util;

import java.util.ArrayList;

import javax.servlet.ServletConfig;

import org.apache.log4j.Category;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.MessageInMemory;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.tdsBO.DispatchPropertiesBO;
import com.tds.tdsBO.OpenRequestBO;

public class MessageGenerateJSON {

	public static ServletConfig config;
	private final Category cat = Category.getInstance(MessageGenerateJSON.class.getName());

 	public static String[] generateMessage(OpenRequestBO openRequest, String command, long msgID, String associationCode){
		DispatchPropertiesBO disProp = SystemPropertiesDAO.getDispatchPropeties(associationCode);
		String message[]=generateMessage(openRequest, command, msgID,disProp, associationCode);
		return message;
	}

 	@SuppressWarnings("unchecked")
	public static String[] generateMessage(OpenRequestBO openRequest, String command, long msgID,DispatchPropertiesBO dispatchProperties, String associationCode){
 		String[] message = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONArray subArray = new JSONArray();
		JSONObject jobj = new JSONObject();
		JSONObject header = new JSONObject();
		try {
	    	header.put("rV", 1.2);
	    	header.put("Act", "GM");
	    	header.put("rC", 200);
	    	header.put("Command", command);	    	
	    	header.put("TI",openRequest.getTripid()); 
	    	header.put("msgID",msgID);
	    	
	    	jobj.put("rV", 1.2);
	    	jobj.put("Command", command);
	    	jobj.put("TI",openRequest.getTripid()); 
	    	jobj.put("msgID",msgID);
	    	jobj.put("STATUS","SUCCESS");	
	    	
	    	jobj.put("SLA",openRequest.getSlat());
			jobj.put("SLO",openRequest.getSlong());
			
	    	if(command.equalsIgnoreCase("BCJ")){
				jobj.put("SA1",openRequest.getSadd1());
				jobj.put("SA2",openRequest.getSadd2());
				jobj.put("SC",(dispatchProperties.getsCity_onOffer()==0?"":openRequest.getScity()));
	    	}else{
	    		String ccode = TDSProperties.getValue("Dr_OnOfferUnAvailableCCode");
	    		//System.out.println("ccode:"+ccode);
	    		int unavailable = 0;
	    		
	    		if(ccode!=null && !ccode.equals("")){
	    			try{
	    				String[] arrayOfCode = ccode.split(",");
	    				if(arrayOfCode!=null && arrayOfCode.length>0){
	    					for(int i=0; i<arrayOfCode.length;i++){
	    						//System.out.println("arrayCode:"+arrayOfCode[i]);
	    						if(arrayOfCode[i].equalsIgnoreCase(associationCode)){
	    							unavailable = 1;
	    							i = arrayOfCode.length+1;
	    						}
	    					}
	    				}
	    			}catch(Exception e){
	    				System.out.println("MessageGenerateJson Problem on ccode");
	    			}
	    		}
	    		
	    		//System.out.println("Unavailable:"+unavailable+"---"+associationCode);
	    		if(unavailable>0){
	    			jobj.put("SA1",(dispatchProperties.getsAddress1_onOffer()==0?"Unavailable":openRequest.getSadd1()));
					jobj.put("SA2",(dispatchProperties.getsAddress2_onOffer()==0?"":openRequest.getSadd2()));
					jobj.put("SC",(dispatchProperties.getsCity_onOffer()==0?"":openRequest.getScity()));
	    		}else{
	    			jobj.put("SA1",(dispatchProperties.getsAddress1_onOffer()==0?"":openRequest.getSadd1()));
					jobj.put("SA2",(dispatchProperties.getsAddress2_onOffer()==0?"":openRequest.getSadd2()));
					jobj.put("SC",(dispatchProperties.getsCity_onOffer()==0?"":openRequest.getScity()));
	    		}
		    	
	    	}
	    	
	    	jobj.put("CN",(dispatchProperties.getName_onOffer()==0?"":openRequest.getName()));
			jobj.put("ST",openRequest.getStartTimeStamp());
			jobj.put("PT",(dispatchProperties.getPaymentType_onOffer()==0?"":openRequest.getPaytype()));
			jobj.put("TS",openRequest.getTripStatus());
			jobj.put("TA",(dispatchProperties.getFareAmt_onOffer()==0?"":(openRequest.getAmt()==null?"":openRequest.getAmt())));
			jobj.put("SZ",(dispatchProperties.getZone_onOffer()==0?"":openRequest.getQueueno()));
			jobj.put("EA1",(dispatchProperties.geteAddress1_onOffer()==0?"":openRequest.getEadd1()));
			jobj.put("EA2",(dispatchProperties.geteAddress2_onOffer()==0?"":openRequest.getEadd2()));
			jobj.put("EC",(dispatchProperties.geteCity_onOffer()==0?"":openRequest.getEcity()));
			jobj.put("EZ",(dispatchProperties.getEndZone_onOffer()==0?"":openRequest.getEndQueueno()));
			jobj.put("AT",openRequest.getQC_SMS_RES_TIME());
			jobj.put("MTy",openRequest.getPaymentMeter());
			jobj.put("EM", openRequest.getEmail());
			jobj.put("MinS",openRequest.getMinSpeed());
			jobj.put("RPMn",openRequest.getRatePerMin());
			jobj.put("RPMl",openRequest.getRatePerMile());
			jobj.put("StAmt",openRequest.getStartAmt());
			jobj.put("LT",openRequest.getDispatchLeadTime());
			jobj.put("ANA", openRequest.getAirName());
			jobj.put("ANO", openRequest.getAirNo());
			jobj.put("AFR", openRequest.getAirFrom());
			jobj.put("ATO", openRequest.getAirTo());
			jobj.put("SRPT",openRequest.getSharedRidePaymentType());
			jobj.put("NOP", openRequest.getNumOfPassengers());
			jobj.put("SRC", openRequest.getTripSource());
			
			mainArray.put(0, header);
			mainArray.put(1, jobj);
			subArray.put(header);
		} catch (Exception e) {
			e.printStackTrace();
		}			
		message[0]= mainArray.toString();
		message[1]= subArray.toString();
		
		return message;
	}
 	
	public static String[] generateMessageAfterAcceptance(OpenRequestBO openRequest, String command, long msgID, String associationCode){
		DispatchPropertiesBO disProp = SystemPropertiesDAO.getDispatchPropeties(associationCode);
		String message[]=generateMessageAfterAcceptance(openRequest, command, msgID,disProp);
		return message;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] generateMessageAfterAcceptance(OpenRequestBO openRequest, String command, long msgID, DispatchPropertiesBO dispatchProperties){
		String[] message = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONArray subArray = new JSONArray();
		JSONObject jobj1 = new JSONObject();
		JSONObject header = new JSONObject();

		try{
			header.put("rV", 1.2); 
			header.put("Act", "GMAC");
	    	header.put("rC", 200);
	    	header.put("Command",command); 
	    	header.put("TI",openRequest.getTripid()); 
			header.put("msgID",msgID);
			
			jobj1.put("rV", 1.2);
	    	jobj1.put("Command", command);
	    	jobj1.put("TI",openRequest.getTripid()); 
	    	jobj1.put("msgID",msgID);
	    	jobj1.put("STATUS","SUCCESS");	
			jobj1.put("SLA",openRequest.getSlat());
			jobj1.put("SLO",openRequest.getSlong());
			jobj1.put("SA1",(dispatchProperties.getsAddress1_onAccept()==0?"":openRequest.getSadd1()));
			jobj1.put("SA2",(dispatchProperties.getsAddress2_onAccept()==0?"":openRequest.getSadd2()));
			jobj1.put("SC",(dispatchProperties.getsCity_onAccept()==0?"":openRequest.getScity()));
			jobj1.put("ELA",openRequest.getEdlatitude());
			jobj1.put("ELO",openRequest.getEdlongitude());
			jobj1.put("EA1",(dispatchProperties.geteAddress1_onAccept()==0?"":openRequest.getEadd1()));
			jobj1.put("EA2",(dispatchProperties.geteAddress2_onAccept()==0?"":openRequest.getEadd2()));
			jobj1.put("EC",(dispatchProperties.geteCity_onAccept()==0?"":openRequest.getEcity()));
			jobj1.put("CN",(dispatchProperties.getName_onAccept()==0?"":openRequest.getName()));			
			jobj1.put("CP",(dispatchProperties.getPhoneNumber_onAccept()==0?"":openRequest.getPhone()));
			jobj1.put("ST",openRequest.getStartTimeStamp());
			jobj1.put("PT",(dispatchProperties.getPaymentType_onAccept()==0?"":openRequest.getPaytype()));
			jobj1.put("PA",openRequest.getAcct());
			jobj1.put("PN",openRequest.getAcctName());
			jobj1.put("SPI",openRequest.getSpecialIns());
			jobj1.put("LM",openRequest.getSlandmark());
			jobj1.put("ELM",openRequest.getElandmark());
			jobj1.put("TA",(dispatchProperties.getFareAmt_onAccept()==0?"":openRequest.getAmt()));
			jobj1.put("TS",openRequest.getTripStatus());
			jobj1.put("SZ",(dispatchProperties.getZone_onAccept()==0?"":openRequest.getQueueno()));
			jobj1.put("EZ",(dispatchProperties.getEndZone_onAccept()==0?"":openRequest.getEndQueueno()));
			jobj1.put("MN",openRequest.getRouteNumber().equals("0")?"":openRequest.getRouteNumber());
			jobj1.put("PKU",openRequest.getPickupOrder());
			jobj1.put("PSW",openRequest.getPickupOrderSwitch());
			jobj1.put("SRPT",openRequest.getSharedRidePaymentType());
			jobj1.put("LT",openRequest.getDispatchLeadTime());
			jobj1.put("MTy",openRequest.getPaymentMeter());
			jobj1.put("MinS",openRequest.getMinSpeed());
			jobj1.put("RPMn",openRequest.getRatePerMin());
			jobj1.put("RPMl",openRequest.getRatePerMile());
			jobj1.put("EM", openRequest.getEmail());
			jobj1.put("StAmt",openRequest.getStartAmt());
			jobj1.put("DRU",openRequest.getDropOffOrder());
			jobj1.put("ANA", openRequest.getAirName());
			jobj1.put("ANO", openRequest.getAirNo());
			jobj1.put("AFR", openRequest.getAirFrom());
			jobj1.put("ATO", openRequest.getAirTo());
			jobj1.put("NOP", openRequest.getNumOfPassengers());
			jobj1.put("SRC", openRequest.getTripSource());
			if(openRequest.isDriverLogStatus()){
				jobj1.put("DZS", "LO");
			}
			mainArray.put(0, header);
			mainArray.put(1, jobj1);
			subArray.put(header);
		}catch(Exception e){
			e.printStackTrace();
		}
		message[0] = mainArray.toString();
		message[1] = subArray.toString();
		return message;
	}

	public static String[] generateMessageOnSite(OpenRequestBO openRequest, String command, long msgID, String associationCode){
		DispatchPropertiesBO disProp = SystemPropertiesDAO.getDispatchPropeties(associationCode);
		String message[]=generateMessageOnSite(openRequest, command, msgID,disProp);
		return message;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] generateMessageOnSite(OpenRequestBO openRequest, String command, long msgID, DispatchPropertiesBO dispatchProperties){
		String[] message = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONArray subArray = new JSONArray();
		JSONObject jobj1 = new JSONObject();
		JSONObject header = new JSONObject();
		try{
			header.put("rV", 1.2);
			header.put("Act", "GMAC");
	    	header.put("rC", 200);
			header.put("Command",command); 
			header.put("TI",openRequest.getTripid()); 
			header.put("msgID",msgID);
			
			jobj1.put("rV", 1.2);
	    	jobj1.put("Command", command);
	    	jobj1.put("TI",openRequest.getTripid()); 
	    	jobj1.put("msgID",msgID);
			jobj1.put("STATUS","SUCCESS");			
			jobj1.put("SLA",openRequest.getSlat());
			jobj1.put("SLO",openRequest.getSlong());
			jobj1.put("SA1",(dispatchProperties.getsAddress1_onSite()==0?"":openRequest.getSadd1()));
			jobj1.put("SA2",(dispatchProperties.getsAddress2_onSite()==0?"":openRequest.getSadd2()));
			jobj1.put("SC",(dispatchProperties.getsCity_onSite()==0?"":openRequest.getScity()));
			jobj1.put("ELA",openRequest.getEdlatitude());
			jobj1.put("ELO",openRequest.getEdlongitude());
			jobj1.put("EA1",(dispatchProperties.geteAddress1_onSite()==0?"":openRequest.getEadd1()));
			jobj1.put("EA2",(dispatchProperties.geteAddress2_onSite()==0?"":openRequest.getEadd2()));
			jobj1.put("EC",(dispatchProperties.geteCity_onSite()==0?"":openRequest.getEcity()));
			jobj1.put("CN",(dispatchProperties.getName_onSite()==0?"":openRequest.getName()));
			jobj1.put("CP",(dispatchProperties.getPhoneNumber_onSite()==0?"":openRequest.getPhone()));
			jobj1.put("ST",openRequest.getStartTimeStamp());
			jobj1.put("PT",(dispatchProperties.getPaymentType_onSite()==0?"":openRequest.getPaytype()));
			jobj1.put("PA",openRequest.getAcct());
			jobj1.put("PN",openRequest.getAcctName());
			jobj1.put("SPI",openRequest.getSpecialIns());
			jobj1.put("LM",openRequest.getSlandmark());
			jobj1.put("EM", openRequest.getEmail());
			jobj1.put("ELM",openRequest.getElandmark());
			jobj1.put("TA",(dispatchProperties.getFareAmt_onSite()==0?"":openRequest.getAmt()));
			jobj1.put("TS",openRequest.getTripStatus());
			jobj1.put("SZ",(dispatchProperties.getZone_onSite()==0?"":openRequest.getQueueno()));
			jobj1.put("EZ",(dispatchProperties.getEndZone_onSite()==0?"":openRequest.getEndQueueno()));
			jobj1.put("MN",openRequest.getRouteNumber().equals("0")?"":openRequest.getRouteNumber());
			jobj1.put("PKU",openRequest.getPickupOrder());
			jobj1.put("PSW",openRequest.getPickupOrderSwitch());
			jobj1.put("SRPT",openRequest.getSharedRidePaymentType());
			jobj1.put("PSW",openRequest.getPickupOrderSwitch());
			jobj1.put("LT",openRequest.getDispatchLeadTime());
			jobj1.put("MTy",openRequest.getPaymentMeter());
			jobj1.put("MinS",openRequest.getMinSpeed());
			jobj1.put("RPMn",openRequest.getRatePerMin());
			jobj1.put("RPMl",openRequest.getRatePerMile());
			jobj1.put("StAmt",openRequest.getStartAmt());
			jobj1.put("DRU",openRequest.getDropOffOrder());
			jobj1.put("ANA", openRequest.getAirName());
			jobj1.put("ANO", openRequest.getAirNo());
			jobj1.put("AFR", openRequest.getAirFrom());
			jobj1.put("ATO", openRequest.getAirTo());
			jobj1.put("NOP", openRequest.getNumOfPassengers());
			jobj1.put("SRC", openRequest.getTripSource());
			
			mainArray.put(0, header);
			mainArray.put(1, jobj1);
			subArray.put(header);			
		}catch(Exception e){
			e.printStackTrace();
		}
		message[0] = mainArray.toString();
		message[1] = subArray.toString();
		return message;
	}

	@SuppressWarnings("unchecked")
	public static String[] generateMessageAfterCompletion(OpenRequestBO openRequest, String command, long msgID, String associationCode){
		String[] message = new String[2];
		
		JSONArray mainArray = new JSONArray();
		JSONArray subArray = new JSONArray();
		JSONObject header = new JSONObject();

		try{
			header.put("rV", 1.2); 
			header.put("Act", "GMAC");
	    	header.put("rC", 200);
			header.put("Command",command); 
			header.put("TI",openRequest.getTripid()); 
			header.put("TS",openRequest.getTripStatus()); 
			header.put("msgID",msgID);
			
			mainArray.put(header);
			subArray.put(header);
		}catch(Exception e){
			e.printStackTrace();
		}
		message[0] = mainArray.toString();
		message[1] = subArray.toString();
		return message;
	}

	@SuppressWarnings("unchecked")
	public static String[] generateVoicePushMessage(String PushVoiceMessageID, long msgID){
		String[] message = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONObject jobj = new JSONObject();
		JSONArray subArray = new JSONArray();
		JSONObject header = new JSONObject();
		try{
	    	header.put("rV", 1.2);
	    	header.put("msgID", msgID);
	    	header.put("Command", "PVM");
	    	subArray.put(header);

	    	jobj.put("rV", 1.2);
	    	jobj.put("Command", "PVM");
	    	jobj.put("msgID", msgID);
	    	jobj.put("PVMID", PushVoiceMessageID);
			mainArray.put(0, jobj);
			mainArray.put(1, header);

		} catch (Exception e){
			e.printStackTrace();
		}
		message[0] = mainArray.toString();//"Command=PVM;R=2;msgID=" + msgID+";PVMID="+PushVoiceMessageID;
		message[1] = subArray.toString();
		return message;
	}

	@SuppressWarnings("unchecked")
	public static String[] generatePrivateMessage(String privateMessage, String command, long msgID){
		String message[] = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONObject jobj = new JSONObject();
		JSONArray subArray = new JSONArray();
		JSONObject header = new JSONObject();
		try{
	    	header.put("rV", 1.2);
	    	header.put("msgID", msgID);
	    	header.put("Message", privateMessage);
	    	header.put("Command", command);
	    	subArray.put(header);
	    	
			jobj.put("rV", 1.2);
	    	jobj.put("Message", privateMessage);
	    	jobj.put("Command", command);
	    	jobj.put("msgID", msgID);
			mainArray.put(0, header);
			mainArray.put(1, jobj);
		} catch (Exception e){
			e.printStackTrace();
		}

		message[0] = mainArray.toString();
		message[1] = subArray.toString();
		
//		message[0] = "ResVer=1.1;Command=" + command + 
//		        ";Message=" + privateMessage + 
//		        ";msgID=" + msgID ;
//		message[1] = "ResVer=1.1;Command=" + command + 
//        ";msgID=" + msgID ;
		return message;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] generateMeterMessage(String privateMessage, String command, long msgID){
		String message[] = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONObject jobj = new JSONObject();
		JSONArray subArray = new JSONArray();
		JSONObject header = new JSONObject();
		try{
	    	header.put("rV", 1.2);
	    	header.put("msgID", msgID);
	    	header.put("Command", command);
	    	subArray.put(header);
	    	
			jobj.put("rV", 1.2);
	    	jobj.put("SC", privateMessage);
	    	jobj.put("Command", command);
	    	jobj.put("msgID", msgID);
			mainArray.put(0, header);
			mainArray.put(1, jobj);
		} catch (Exception e){
			e.printStackTrace();
		}

		message[0] = mainArray.toString();
		message[1] = subArray.toString();
		
//		message[0] = "ResVer=1.1;Command=" + command + 
//		        ";SC=" + privateMessage + 
//		        ";msgID=" + msgID ;
//		message[1] = "ResVer=1.1;Command=" + command + 
//        ";msgID=" + msgID ;
		return message;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] resetDriverApp( long msgID){
		String message[] = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONObject jobj = new JSONObject();
		JSONArray subArray = new JSONArray();
		JSONObject header = new JSONObject();
		try{
	    	header.put("rV", 1.2);
	    	header.put("msgID", msgID);
	    	header.put("Command", "RDA");
	    	subArray.put(header);
	    	
			jobj.put("rV", 1.2);
	    	jobj.put("Command", "RDA");
	    	jobj.put("msgID", msgID);
	    	mainArray.put(0,header);
	    	mainArray.put(1,jobj);
		} catch (Exception e){
			e.printStackTrace();
		}

		message[0] = mainArray.toString();
		message[1] = subArray.toString();

//		message[0] = "ResVer=1.1;Command=RDA"+ 
//		        ";msgID=" + msgID ;
//		message[1] = "ResVer=1.1;Command=RDA"+
//        ";msgID=" + msgID ;
		return message;
	}
	
//	public static String[] logOutDriver( long msgID){
//		String message[] = new String[2];
//		message[0] = "ResVer=1.1;Command=LO"+ 
//		        ";msgID=" + msgID ;
//		message[1] = "ResVer=1.1;Command=LO"+
//        ";msgID=" + msgID ;
//		return message;
//	}

	@SuppressWarnings("unchecked")
	public static String[] approveDevice( long msgID,String cabNo){
		String message[] = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONObject jobj = new JSONObject();
		JSONArray subArray = new JSONArray();
		JSONObject header = new JSONObject();
		try{
	    	header.put("rV", 1.2);
	    	header.put("msgID", msgID);
	    	header.put("Command", "APD");
	    	subArray.put(header);
	    	
			jobj.put("rV", 1.2);
	    	jobj.put("cab", cabNo);
	    	jobj.put("Command", "APD");
	    	jobj.put("msgID", msgID);
			mainArray.put(0, header);
			mainArray.put(1, jobj);
		} catch (Exception e){
			e.printStackTrace();
		}

		message[0] = mainArray.toString();
		message[1] = subArray.toString();

//		message[0] = "ResVer=1.1;Command=APD"+";cab="+cabNo+
//		        ";msgID=" + msgID ;
//		message[1] = "ResVer=1.1;Command=APD"+
//        ";msgID=" + msgID ;
		return message;
	}
	
	@SuppressWarnings("unchecked")
	public static String[] generateMeterOffMessage(String fare,String extras, long msgID){
		String message[] = new String[2];
		JSONArray mainArray = new JSONArray();
		JSONObject jobj = new JSONObject();
		JSONArray subArray = new JSONArray();
		JSONObject header = new JSONObject();
		try{
	    	header.put("rV", 1.2);
	    	header.put("msgID", msgID);
	    	header.put("Command", "MO");
	    	subArray.put(header);
	    	
			jobj.put("rV", 1.2);
	    	jobj.put("fare", fare);
	    	jobj.put("extras", extras);
	    	jobj.put("Command", "MO");
	    	jobj.put("msgID", msgID);
			mainArray.put(0, header);
			mainArray.put(1, jobj);
		} catch (Exception e){
			e.printStackTrace();
		}

		message[0] = mainArray.toString();
		message[1] = subArray.toString();

//		message[0] = "ResVer=1.1;Command=MO"+ 
//		        ";fare=" + fare + ";extras="+extras+
//		        ";msgID=" + msgID ;
//		message[1] = "ResVer=1.1;Command=MO"+ 
//        ";msgID=" + msgID ;
		return message;
	}
	
	public static void storeMessageInMemeoryDelete(ServletConfig config, ArrayList<DriverCabQueueBean> drivers, MessageInMemory messageInMemory, String module){
		for(int i=0;i<drivers.size();i++){
			config.getServletContext().setAttribute(drivers.get(i).getDriverid()+ module, messageInMemory);
		}
	}
	
	public static void storeMessageInMemeoryDelete(ServletConfig config, DriverCabQueueBean driver, String messageInMemory, String module){
		config.getServletContext().setAttribute(driver.getDriverid()+ module, messageInMemory);
	}

}
