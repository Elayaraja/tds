package com.common.util;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.ZoneDAO;
import com.tds.tdsBO.ZoneCoordinatesBo;

public class Feed {
	@SuppressWarnings("unchecked")
	public static String getAllZones(String masterAssoccode, ArrayList<ZoneTableBeanSP> allZonesForCompany) {
		return getAllZones(masterAssoccode,allZonesForCompany, "");
	}
	public static String getAllZones(String masterAssoccode, ArrayList<ZoneTableBeanSP> allZonesForCompany, String driverID) {

		ArrayList<ZoneCoordinatesBo> zList = new ArrayList<ZoneCoordinatesBo>();
		zList = ZoneDAO.readQueueBoundriesForDash(masterAssoccode);
		Double centreLat = null;
		Double centreLon = null;
		JSONArray cooridates = new JSONArray();
		JSONArray latLongList = new JSONArray();
		JSONObject driverFirst = new JSONObject();
		JSONObject latLong = new JSONObject();
		if(!driverID.equals("")){
			//Read the zone mapping table
			ArrayList<ZoneCoordinatesBo> accessList = ZoneDAO.readZoneLevelAccess(masterAssoccode, driverID);

			//Populate zList to indicate if drive has access or not
			for(int counter= 0; counter < accessList.size();counter++){
				for(int zoneCounter= 0; zoneCounter < zList.size();zoneCounter++){
					if(zList.get(zoneCounter).getQueueId().equals(accessList.get(counter).getQueueId())){
						if(accessList.get(counter).getQLoginSwitch().equalsIgnoreCase("R")){
							zList.get(zoneCounter).setQLoginSwitch("A");
						} else{
							zList.get(zoneCounter).setQLoginSwitch("R");
						}
						
					}
				}
			}
			
		}

		String previousZone = "";
		int count = 0;
		try {
			for (int i = 0; i < zList.size(); i++) {
				latLong = new JSONObject();
				if (!previousZone.equals(zList.get(i).getQueueId())) {
					if (!previousZone.equals("")) {
						driverFirst.put("ZC", latLongList);
						cooridates.put(driverFirst);
						driverFirst = new JSONObject();
						latLongList = new JSONArray();
					}

					centreLon = (allZonesForCompany.get(count).getEastLongitude() + allZonesForCompany.get(count).getWestLongitude()) / 2;
					centreLat = (allZonesForCompany.get(count).getSouthLatitude() + allZonesForCompany.get(count).getNorthLatitude()) / 2;
					driverFirst.put("CLAT", centreLat);
					driverFirst.put("CLON", centreLon);
					driverFirst.put("ZK", zList.get(i).getQueueId());
					driverFirst.put("ZD", zList.get(i).getQDescription());
					driverFirst.put("eLat", allZonesForCompany.get(count).getZoneCoord().getExternalPoint().lat);
					driverFirst.put("eLon", allZonesForCompany.get(count).getZoneCoord().getExternalPoint().lon);

					latLong.put("seqtime", zList.get(i).getqSequence());
					latLong.put("latitude", zList.get(i).getLatitude());
					latLong.put("longitude", zList.get(i).getLongitude());

					driverFirst.put("latitude", zList.get(i).getLatitude());
					driverFirst.put("longitude", zList.get(i).getLongitude());
					driverFirst.put("QID", zList.get(i).getQueueId());
					driverFirst.put("QDesc", zList.get(i).getQDescription());
					driverFirst.put("QResTime", zList.get(i).getSmsResponseTime());
					driverFirst.put("QDelayTime", zList.get(i).getQDelayTime());
					driverFirst.put("ELA", allZonesForCompany.get(count).getZoneCoord().getExternalPoint().lat);
					driverFirst.put("ELO", allZonesForCompany.get(count).getZoneCoord().getExternalPoint().lon);
					driverFirst.put("V", zList.get(i).getVersionNumber());
					driverFirst.put("LS", zList.get(i).getQLoginSwitch());
					latLongList.put(latLong);
					count++;
				} else {
					latLong.put("seqtime", zList.get(i).getqSequence());
					latLong.put("latitude", zList.get(i).getLatitude());
					latLong.put("longitude", zList.get(i).getLongitude());
					latLongList.put(latLong);
				}
				previousZone = zList.get(i).getQueueId();
			}
			driverFirst.put("ZC", latLongList);
			cooridates.put(driverFirst);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return cooridates.toString();
	}

}