package com.common.util;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import com.tds.controller.SystemUnavailableException;
import com.tds.dao.UtilityDAO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import com.tds.dao.UtilityDAO;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import sun.misc.BASE64Encoder;
import sun.misc.CharacterEncoder;

public final class PasswordHash{

	public synchronized static String encrypt(String password) throws SystemUnavailableException
	{
		MessageDigest md = null;
		try
		{
			md = MessageDigest.getInstance("SHA"); //step 2
		}
		catch(NoSuchAlgorithmException e)
		{
			throw new SystemUnavailableException(e.getMessage());
		}
		try
		{
			md.update(password.getBytes("UTF-8")); //step 3
		}
		catch(UnsupportedEncodingException e)
		{
			throw new SystemUnavailableException(e.getMessage());
		}

		byte raw[] = md.digest(); //step 4
		String hash = (new BASE64Encoder()).encode(raw); //step 5
		return hash; //step 6
	}
}
