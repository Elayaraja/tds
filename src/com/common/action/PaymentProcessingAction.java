package com.common.action;

import java.io.IOException;

import javax.servlet.ServletException;

import com.charges.constant.ICCConstant;
import com.charges.dao.ChargesDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.payment.PaymentCatgories;
import com.tds.payment.PaymentGateway;
import com.tds.payment.PaymentGatewaySlimCD;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.util.ServiceTxnProcess;

public class PaymentProcessingAction {
			
	public static PaymentProcessBean initiatePayProcess(ApplicationPoolBO poolBO,PaymentProcessBean processBean,String from,String assoccode) throws ServletException,IOException
	{
		 
		//System.out.println("Card Issuer:"+processBean.getB_card_issuer() +"Card No:"+  processBean.getB_card_no() +"Card Type:"+ processBean.getB_card_type());
		CompanyMasterBO cmpBean= SystemPropertiesDAO.getCCProperties(assoccode);
		
		//if( processBean.getB_card_issuer()!="" && processBean.getB_card_no()!="" && processBean.getB_card_type()!="" && TDSValidation.isDoubleNumber(processBean.getB_amount()))
		if( processBean.getB_card_issuer()!="" && processBean.getB_card_no()!="" )
		{
			// Here Stuff to Web Service Auth and Capture
			//ServiceTxnProcess txnProcess = new ServiceTxnProcess();
			if(from.equals("Mobile")){
				//processBean = txnProcess.Authorize(poolBO,processBean);
				if(cmpBean.getCcProvider()==2){
					processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Auth);
				} else {
					processBean = PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Auth);
				}
			} else {
				//processBean = txnProcess.AuthorizeAndCapture(poolBO,processBean);
				if(cmpBean.getCcProvider()==2){
					processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Sale);
				} else {
					processBean = PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Sale);
				}
			}
		
		} else {
			if(from.equalsIgnoreCase("Invoice")){
				if(cmpBean.getCcProvider()==2){
					processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Sale);
				} else {
					processBean = PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Sale);
				}
			}else{
				processBean.setB_approval_status("3");			
				processBean.setB_approval_code("");
				processBean.setB_trans_id("");
			}
		}
		
		return processBean; 
	}
	
	public static PaymentProcessBean makePayment(PaymentProcessBean processBean,String Path,ApplicationPoolBO poolBO, AdminRegistrationBO adminBO)
	{
		//ProcessingDAO.insertPaymentIntoWork(processBean,Path,poolBO, adminBO);
		ChargesDAO.insertPaymentIntoCC(processBean, Path, poolBO, adminBO,ICCConstant.PREAUTH,ICCConstant.GAC);
		return processBean;
	}
	
public static PaymentProcessBean capturePayment(ApplicationPoolBO poolBO,PaymentProcessBean processBean,String assoccode){
		// Here Stuff to Web Service (Transaction Process Adjust())
		CompanyMasterBO cmpBean= SystemPropertiesDAO.getCCProperties(assoccode);
		ServiceTxnProcess txnProcess = new ServiceTxnProcess();
		//processBean = txnProcess.Capture(poolBO,processBean);
		if(cmpBean.getCcProvider()==2){
			processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Force);
		} else {
			processBean = PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Force);
		}
		/*if(processBean.isAuth_status())
			ProcessingDAO.alterTransct(processBean.getB_tip_amt(),processBean.getB_trans_id());*/
		return processBean;
	}

	
	public static PaymentProcessBean voidPayment(ApplicationPoolBO poolBO,PaymentProcessBean processBean,String assoccode)
	{
		// Here Stuff to Web Service (Transaction Void())
		CompanyMasterBO cmpBean= SystemPropertiesDAO.getCCProperties(assoccode);
		//ServiceTxnProcess txnProcess = new ServiceTxnProcess();
		if(processBean.isReturnById()){
			//processBean = txnProcess.ReturnById(poolBO, processBean);
			if(cmpBean.getCcProvider()==2){
				processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Return);
			} else {
				processBean = PaymentGateway.makeProcess(processBean,cmpBean,PaymentCatgories.Return);
			}
		}else{
			//processBean = txnProcess.Undo(poolBO,processBean);
			if(cmpBean.getCcProvider()==2){
				processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Void);
			} else {
				processBean = PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Void);
			}
			/*processBean = capturePayment(poolBO,processBean);
			if(processBean.getB_approval_status().equalsIgnoreCase("1"))
			{
				ProcessingDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), processBean.getB_trans_id(), processBean.getB_cap_trans(), processBean.getB_sign());
				processBean = txnProcess.ReturnById(poolBO, processBean);
			} */
		}
		return processBean;
	}
	
	
}
