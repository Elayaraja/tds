package com.charges.constant;

public class IChargeConstants {

	public static final int PAYMENT_TYPE_CHARGE = 1;
	public static final int PAYMENT_TYPE_CC = 2;
	
	public static String getDisplayString(int pType)
	{
		 switch(pType)
		 {
		case PAYMENT_TYPE_CHARGE:
		 		return "Pay by Charge";
		case PAYMENT_TYPE_CC:	
		 		return "Pay by Card/Voucher";
		default:
		 		return "";
		 }
	}

}
