package com.charges.constant;

public class ICCConstant {

	public static final int PREAUTH=1;
	public static final int SETTLED=2;
	public static final int VOID=3;
	public static final int RETURNED=4;
	
	public static final int GAC=0;
	public static final int CMT=1;
	public static final int VANTIV=2;
	
	public static String getDisplayString(int pType)
	{
		 switch(pType)
		 {
		case PREAUTH:
		 		return "Pre Auth";
		case SETTLED:	
		 		return "Setteled";
		case VOID:	
	 		return "Voided";
		case RETURNED:	
	 		return "Returned";
	
		default:
		 		return "";
		 }
	}
	
}
