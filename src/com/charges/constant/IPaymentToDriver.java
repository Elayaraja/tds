package com.charges.constant;

import java.util.HashMap;

public class IPaymentToDriver {

	public static final int RECEIVE_CASH_FROM_DRIVER=1;
	public static final int PAY_CASH_TO_DRIVER=2;
	public static final int RECEIVE_CHECK_FROM_DRIVER=3;
	public static final int PAY_CHECK_TO_DRIVER=4;
	public static final int PAY_CASH_TO_DRIVER_ACC=5;
	public static HashMap<String, String> hm_payType;
	
	public static HashMap<String, String> getDriverPayType()
	{
		hm_payType = new HashMap<String, String>();
		
		hm_payType.put(""+RECEIVE_CASH_FROM_DRIVER, "Receive Cash from Driver");
		hm_payType.put(""+PAY_CASH_TO_DRIVER, "Pay Cash to Driver");
		hm_payType.put(""+RECEIVE_CHECK_FROM_DRIVER, "Receive Check from Driver");
		hm_payType.put(""+PAY_CHECK_TO_DRIVER, "Pay Check to Driver");
		hm_payType.put(""+PAY_CASH_TO_DRIVER_ACC, "Pay Cash to Driver Account");
		
		return hm_payType;
	}

	
}
