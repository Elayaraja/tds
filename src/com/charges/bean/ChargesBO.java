package com.charges.bean;

public class ChargesBO {

	private int payTypeKey;
	private String payTypeDesc="";
	private String payTypeAmount="";
	private int payTemplateKey=0;
	private String payTemplateDesc="";
	private int payTemplateAmountType;
	private int key;
	private String numberOfPassengers="";
	private String startRate="";
	private String ratePerMile="";
	private String vehicleDesc="";
	private String miscDesc="";
	private String driverId="";
	private String processDate="";
	private String customField="";
	private String transid;
	private String tripid;
    private String driverid;
    private String companyid;
    private String chargeno;
    private String desc;
    private String amount;
    private String chargedate;
    private String processingdate;
    private String invoiceno;
    private String totalvoucher;
    private String totalamount;
	private int position;
	private String dynamicFormula="";
	
	private String tax1="";
	private String tax2="";
	private String tax3="";
	
	public String getTax1() {
		return tax1;
	}
	public void setTax1(String tax1) {
		this.tax1 = tax1;
	}
	public String getTax2() {
		return tax2;
	}
	public void setTax2(String tax2) {
		this.tax2 = tax2;
	}
	public String getTax3() {
		return tax3;
	}
	public void setTax3(String tax3) {
		this.tax3 = tax3;
	}
	public String getDriverId() {
		return driverId;
	}
	
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getMiscDesc() {
		return miscDesc;
	}
	public void setMiscDesc(String miscDesc) {
		this.miscDesc = miscDesc;
	}
	public String getNumberOfPassengers() {
		return numberOfPassengers;
	}
	public void setNumberOfPassengers(String numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}
	public String getStartRate() {
		return startRate;
	}
	public void setStartRate(String startRate) {
		this.startRate = startRate;
	}
	public String getRatePerMile() {
		return ratePerMile;
	}
	public void setRatePerMile(String ratePerMile) {
		this.ratePerMile = ratePerMile;
	}
	public String getVehicleDesc() {
		return vehicleDesc;
	}
	public void setVehicleDesc(String vehicleDesc) {
		this.vehicleDesc = vehicleDesc;
	}

	
	
	
	
	
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public int getPayTemplateKey() {
		return payTemplateKey;
	}
	public void setPayTemplateKey(int payTemplateKey) {
		this.payTemplateKey = payTemplateKey;
	}
	public String getPayTemplateDesc() {
		return payTemplateDesc;
	}
	public void setPayTemplateDesc(String payTemplateDesc) {
		this.payTemplateDesc = payTemplateDesc;
	}
	public int getPayTemplateAmountType() {
		return payTemplateAmountType;
	}
	public void setPayTemplateAmountType(int payTemplateAmountType) {
		this.payTemplateAmountType = payTemplateAmountType;
	}
	public int getPayTypeKey() {
		return payTypeKey;
	}
	public void setPayTypeKey(int payTypeKey) {
		this.payTypeKey = payTypeKey;
	}
	public String getPayTypeDesc() {
		return payTypeDesc;
	}
	public void setPayTypeDesc(String payTypeDesc) {
		this.payTypeDesc = payTypeDesc;
	}
	public String getPayTypeAmount() {
		return payTypeAmount;
	}
	public void setPayTypeAmount(String payTypeAmount) {
		this.payTypeAmount = payTypeAmount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getAmount() {
		return amount;
	}
	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}
	public String getProcessDate() {
		return processDate;
	}
	public void setCustomField(String customField) {
		this.customField = customField;
	}
	public String getCustomField() {
		return customField;
	}
	public String getTransid() {
		return transid;
	}
	public void setTransid(String transid) {
		this.transid = transid;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	public String getChargeno() {
		return chargeno;
	}
	public void setChargeno(String chargeno) {
		this.chargeno = chargeno;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getChargedate() {
		return chargedate;
	}
	public void setChargedate(String chargedate) {
		this.chargedate = chargedate;
	}
	public String getProcessingdate() {
		return processingdate;
	}
	public void setProcessingdate(String processingdate) {
		this.processingdate = processingdate;
	}
	public String getInvoiceno() {
		return invoiceno;
	}
	public void setInvoiceno(String invoiceno) {
		this.invoiceno = invoiceno;
	}
	public String getTotalvoucher() {
		return totalvoucher;
	}
	public void setTotalvoucher(String totalvoucher) {
		this.totalvoucher = totalvoucher;
	}
	public String getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(String totalamount) {
		this.totalamount = totalamount;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public String getTripid() {
		return tripid;
	}
	public void setTripid(String tripid) {
		this.tripid = tripid;
	}
	public String getDynamicFormula() {
		return dynamicFormula;
	}
	public void setDynamicFormula(String dynamicFormula) {
		this.dynamicFormula = dynamicFormula;
	}
	
}
