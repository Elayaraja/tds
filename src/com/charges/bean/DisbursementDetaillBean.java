package com.charges.bean;

import java.math.BigDecimal;

import com.charges.constant.IChargeConstants;

public class DisbursementDetaillBean implements Cloneable {

	private int CTM_PAYMENT_TYPE;
	private String CTD_KEY;

	private BigDecimal CTD_AMOUNT;
	private BigDecimal CTD_PERCENT;
	
	private int CTD_PAYMENT_SWITCH;
	private String CTD_MASTER_CODE;
	private int CTD_TYPE_CODE;
	private String CTM_PAYMENT_TYPE_DESC;
	private String CTM_PROGRAM_NAME;
	private String CTM_PROGRAM_ID;
	
	public Object clone(){
	  try{
		  DisbursementDetaillBean cloned = (DisbursementDetaillBean)super.clone();
		  return cloned;
	  } catch(CloneNotSupportedException e){
		  System.out.println(e);
		  return null;
	  }
	}
	
	public BigDecimal getCTD_PERCENT() {
		return CTD_PERCENT;
	}


	public void setCTD_PERCENT(BigDecimal cTDPERCENT) {
		CTD_PERCENT = cTDPERCENT;
	}
	public String getCTM_PROGRAM_ID() {
		return CTM_PROGRAM_ID;
	}
	public void setCTM_PROGRAM_ID(String cTMPROGRAMID) {
		CTM_PROGRAM_ID = cTMPROGRAMID;
	}
	private String CT_DESCRIPRION;
	
	public String getCT_DESCRIPRION() {
		return CT_DESCRIPRION;
	}
	public void setCT_DESCRIPRION(String cTDESCRIPRION) {
		CT_DESCRIPRION = cTDESCRIPRION;
	}
	public String getCTM_PROGRAM_NAME() {
		return CTM_PROGRAM_NAME;
	}
	public void setCTM_PROGRAM_NAME(String cTMPROGRAMNAME) {
		CTM_PROGRAM_NAME = cTMPROGRAMNAME;
	}
	public String getCTM_PAYMENT_TYPE_DESC() {
		return CTM_PAYMENT_TYPE_DESC;
	}
	public void setCTM_PAYMENT_TYPE_DESC(int CTM_PAYMENT_TYPE) {
		CTM_PAYMENT_TYPE_DESC = IChargeConstants.getDisplayString(CTM_PAYMENT_TYPE);
	}
	
	public String getCTD_KEY() {
		return CTD_KEY;
	}
	public int getCTM_PAYMENT_TYPE() {
		return CTM_PAYMENT_TYPE;
	}
	public void setCTM_PAYMENT_TYPE(int cTMPAYMENTTYPE) {
		CTM_PAYMENT_TYPE = cTMPAYMENTTYPE;
		setCTM_PAYMENT_TYPE_DESC(cTMPAYMENTTYPE);
	}
	public void setCTD_KEY(String cTDKEY) {
		CTD_KEY = cTDKEY;
	}
	public BigDecimal getCTD_AMOUNT() {
		return CTD_AMOUNT;
	}
	public void setCTD_AMOUNT(BigDecimal cTDAMOUNT) {
		CTD_AMOUNT = cTDAMOUNT;
	}
	public int getCTD_PAYMENT_SWITCH() {
		return CTD_PAYMENT_SWITCH;
	}
	public void setCTD_PAYMENT_SWITCH(int cTDPAYMENTSWITCH) {
		CTD_PAYMENT_SWITCH = cTDPAYMENTSWITCH;
	}
	public String getCTD_MASTER_CODE() {
		return CTD_MASTER_CODE;
	}
	public void setCTD_MASTER_CODE(String cTDMASTERCODE) {
		CTD_MASTER_CODE = cTDMASTERCODE;
	}
	public int getCTD_TYPE_CODE() {
		return CTD_TYPE_CODE;
	}
	public void setCTD_TYPE_CODE(int cTDTYPECODE) {
		CTD_TYPE_CODE = cTDTYPECODE;
	}
	
	public BigDecimal getCTD_TAX1_AMOUNT() {
		return CTD_TAX1_AMOUNT;
	}

	public void setCTD_TAX1_AMOUNT(BigDecimal cTD_TAX1_AMOUNT) {
		CTD_TAX1_AMOUNT = cTD_TAX1_AMOUNT;
	}

	public BigDecimal getCTD_TAX2_AMOUNT() {
		return CTD_TAX2_AMOUNT;
	}

	public void setCTD_TAX2_AMOUNT(BigDecimal cTD_TAX2_AMOUNT) {
		CTD_TAX2_AMOUNT = cTD_TAX2_AMOUNT;
	}

	public BigDecimal getCTD_TAX3_AMOUNT() {
		return CTD_TAX3_AMOUNT;
	}

	public void setCTD_TAX3_AMOUNT(BigDecimal cTD_TAX3_AMOUNT) {
		CTD_TAX3_AMOUNT = cTD_TAX3_AMOUNT;
	}
	private BigDecimal CTD_TAX1_AMOUNT;
	private BigDecimal CTD_TAX2_AMOUNT;
	private BigDecimal CTD_TAX3_AMOUNT;	
}
