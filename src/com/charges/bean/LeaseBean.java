package com.charges.bean;

import java.sql.Date;

public class LeaseBean {
	private String leaseID;
	public String getLeaseID() {
		return leaseID;
	}
	public void setLeaseID(String leaseID) {
		this.leaseID = leaseID;
	}
	private String assocode;
	private String driverid;
	private String oprid;
	private String vechDesc;
	private double vechAmount;
	private Date processDate;
	private String vechileNo;
	
	public String getVechileNo() {
		return vechileNo;
	}
	public void setVechileNo(String vechileNo) {
		this.vechileNo = vechileNo;
	}
	public String getAssocode() {
		return assocode;
	}
	public void setAssocode(String assocode) {
		this.assocode = assocode;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getOprid() {
		return oprid;
	}
	public void setOprid(String oprid) {
		this.oprid = oprid;
	}
	public String getVechDesc() {
		return vechDesc;
	}
	public void setVechDesc(String vechDesc) {
		this.vechDesc = vechDesc;
	}
	public double getVechAmount() {
		return vechAmount;
	}
	public void setVechAmount(double vechAmount) {
		this.vechAmount = vechAmount;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	
	
	
}
