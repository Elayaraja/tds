package com.charges.bean;

public class ChargeDetail {
	private String mtype;
	private String tripid;
	private String approvalcode;
	private double total;
	private double driverAmt;
	private String driverID;
	
	
	public String getDriverID() {
		return driverID;
	}
	public void setDriverID(String driverID) {
		this.driverID = driverID;
	}
	public String getMtype() {
		return mtype;
	}
	public void setMtype(String mtype) {
		this.mtype = mtype;
	}
	public String getTripid() {
		return tripid;
	}
	public void setTripid(String tripid) {
		this.tripid = tripid;
	}
	public String getApprovalcode() {
		return approvalcode;
	}
	public void setApprovalcode(String approvalcode) {
		this.approvalcode = approvalcode;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getDriverAmt() {
		return driverAmt;
	}
	public void setDriverAmt(double driverAmt) {
		this.driverAmt = driverAmt;
	}
	
	
	
	
}
