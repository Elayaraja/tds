package com.charges.bean;

import java.math.BigDecimal;


public class DriverChargesBean {
	

	private String tripID ;
	private String DC_DRIVER_ID;
	private String DC_ASSOC_CODE ;
	private int DC_TYPE_CODE;
 	private String DC_PROCESSING_DATE_TIME;
	private BigDecimal DC_DRIVER_PMT_TAX1_AMT;
	private BigDecimal DC_DRIVER_PMT_TAX2_AMT;
	private BigDecimal DC_DRIVER_PMT_TAX3_AMT;
	private BigDecimal DC_DRIVER_PMT_AMT;
	private int DC_DRIVER_PMT_STATUS;
	private int DC_DRIVER_PMT_NUM;
	private String DC_TYPE_DESC;
	private String PAYMENT_TYPE;
	private String TXNO;
	private String vcardno;
	private String issuer;
	private String paymentMode;
	private BigDecimal amount;
	private BigDecimal tip;
	private BigDecimal totalamount;
	private int verifiedStatus;

	public BigDecimal getDC_DRIVER_PMT_TAX1_AMT() {
		return DC_DRIVER_PMT_TAX1_AMT;
	}
	public void setDC_DRIVER_PMT_TAX1_AMT(BigDecimal dC_DRIVER_PMT_TAX1_AMT) {
		DC_DRIVER_PMT_TAX1_AMT = dC_DRIVER_PMT_TAX1_AMT;
	}
	public BigDecimal getDC_DRIVER_PMT_TAX2_AMT() {
		return DC_DRIVER_PMT_TAX2_AMT;
	}
	public void setDC_DRIVER_PMT_TAX2_AMT(BigDecimal dC_DRIVER_PMT_TAX2_AMT) {
		DC_DRIVER_PMT_TAX2_AMT = dC_DRIVER_PMT_TAX2_AMT;
	}
	public BigDecimal getDC_DRIVER_PMT_TAX3_AMT() {
		return DC_DRIVER_PMT_TAX3_AMT;
	}
	public void setDC_DRIVER_PMT_TAX3_AMT(BigDecimal dC_DRIVER_PMT_TAX3_AMT) {
		DC_DRIVER_PMT_TAX3_AMT = dC_DRIVER_PMT_TAX3_AMT;
	}
	
	public int getVerifiedStatus() {
		return verifiedStatus;
	}
	public void setVerifiedStatus(int verifiedStatus) {
		this.verifiedStatus = verifiedStatus;
	}
	public String getTripID() {
		return tripID;
	}
	public void setTripID(String tripID) {
		this.tripID = tripID;
	}

	public String getDC_TYPE_DESC() {
		return DC_TYPE_DESC;
	}
	public void setDC_TYPE_DESC(String dCTYPEDESC) {
		DC_TYPE_DESC = dCTYPEDESC;
	}
	public String getPAYMENT_TYPE() {
		return PAYMENT_TYPE;
	}
	public void setPAYMENT_TYPE(String pAYMENTTYPE) {
		PAYMENT_TYPE = pAYMENTTYPE;
	}
	
	
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getTXNO() {
		return TXNO;
	}
	public void setTXNO(String tXNO) {
		TXNO = tXNO;
	}
	public String getVcardno() {
		return vcardno;
	}
	public void setVcardno(String vcardno) {
		this.vcardno = vcardno;
	}
	public String getIssuer() {
		return issuer;
	}
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getTip() {
		return tip;
	}
	public void setTip(BigDecimal tip) {
		this.tip = tip;
	}
	public BigDecimal getTotalamount() {
		return totalamount;
	}
	public void setTotalamount(BigDecimal totalamount) {
		this.totalamount = totalamount;
	}
		
	public String getDC_DRIVER_ID() {
		return DC_DRIVER_ID;
	}
	public void setDC_DRIVER_ID(String dCDRIVERID) {
		DC_DRIVER_ID = dCDRIVERID;
	}
	public String getDC_ASSOC_CODE() {
		return DC_ASSOC_CODE;
	}
	public void setDC_ASSOC_CODE(String dCASSOCCODE) {
		DC_ASSOC_CODE = dCASSOCCODE;
	}
	public int getDC_TYPE_CODE() {
		return DC_TYPE_CODE;
	}
	public void setDC_TYPE_CODE(int dCTYPECODE) {
		DC_TYPE_CODE = dCTYPECODE;
	}
 	public String getDC_PROCESSING_DATE_TIME() {
		return DC_PROCESSING_DATE_TIME;
	}
	public void setDC_PROCESSING_DATE_TIME(String dCPROCESSINGDATETIME) {
		DC_PROCESSING_DATE_TIME = dCPROCESSINGDATETIME;
	}
	public BigDecimal getDC_DRIVER_PMT_AMT() {
		return DC_DRIVER_PMT_AMT;
	}
	public void setDC_DRIVER_PMT_AMT(BigDecimal dCDRIVERPMTAMT) {
		DC_DRIVER_PMT_AMT = dCDRIVERPMTAMT;
	}
	public int getDC_DRIVER_PMT_STATUS() {
		return DC_DRIVER_PMT_STATUS;
	}
	public void setDC_DRIVER_PMT_STATUS(int dCDRIVERPMTSTATUS) {
		DC_DRIVER_PMT_STATUS = dCDRIVERPMTSTATUS;
	}
	public int getDC_DRIVER_PMT_NUM() {
		return DC_DRIVER_PMT_NUM;
	}
	public void setDC_DRIVER_PMT_NUM(int dCDRIVERPMTNUM) {
		DC_DRIVER_PMT_NUM = dCDRIVERPMTNUM;
	}
	
	

}
