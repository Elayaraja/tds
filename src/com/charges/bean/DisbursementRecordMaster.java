package com.charges.bean;



import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;



public class DisbursementRecordMaster {

	private String driverid;
	private String drivername;
	private double grantamount;
	private String operatorid;
	private int disbursmentStatus;
	private Date processDate;
	private int chargeCalcType;
	private String paymentToType;
	private String uID;
	private String tagname;
	public String getuID() {
		return uID;
	}
	public void setuID(String uID) {
		this.uID = uID;
	}
	private double openingBalance=0.0;
	private double closingBalance=0.0;
	private ArrayList<DisbursementRecordDetail> recordDetails;
	
	public ArrayList<DisbursementRecordDetail> getRecordDetails() {
		return recordDetails;
	}
	public void setRecordDetails(ArrayList<DisbursementRecordDetail> recordDetails) {
		this.recordDetails = recordDetails;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getDrivername() {
		return drivername;
	}
	public void setDrivername(String drivername) {
		this.drivername = drivername;
	}
	public double getGrantamount() {
		return grantamount;
	}
	public void setGrantamount(double grantamount) {
		this.grantamount = grantamount;
	}
	public String getOperatorid() {
		return operatorid;
	}
	public void setOperatorid(String operatorid) {
		this.operatorid = operatorid;
	}
	public int getDisbursmentStatus() {
		return disbursmentStatus;
	}
	public void setDisbursmentStatus(int disbursmentStatus) {
		this.disbursmentStatus = disbursmentStatus;
	}
	
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	
	public int getChargeCalcType() {
		return chargeCalcType;
	}
	public void setChargeCalcType(int chargeCalcType) {
		this.chargeCalcType = chargeCalcType;
	}
	public String getPaymentToType() {
		return paymentToType;
	}
	public void setPaymentToType(String paymentToType) {
		this.paymentToType = paymentToType;
	}
	public double getOpeningBalance() {
		return openingBalance;
	}
	public void setOpeningBalance(double openingBalance) {
		this.openingBalance = openingBalance;
	}
	public double getClosingBalance() {
		return closingBalance;
	}
	public void setClosingBalance(double closingBalance) {
		this.closingBalance = closingBalance;
	}
	public String getTagname() {
		return tagname;
	}
	public void setTagname(String tagname) {
		this.tagname = tagname;
	}
	
		
}
