package com.charges.bean;

import java.sql.Date;



public class MiscBean {
	private String miscid;
	private String assocode;
	private String driverid;
	private String oprid;
	private String miscDesc;
	private String miscType;
	private double miscAmount;
	private Date processDate;
	private String customField;	
	public String getCustomField() {
		return customField;
	}
	public void setCustomField(String customField) {
		this.customField = customField;
	}
	public String getMiscid() {
		return miscid;
	}
	public void setMiscid(String miscid) {
		this.miscid = miscid;
	}
	public Date getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}
	private String returnKey;
	private int payStatus;
	public String getAssocode() {
		return assocode;
	}
	public void setAssocode(String assocode) {
		this.assocode = assocode;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getOprid() {
		return oprid;
	}
	public void setOprid(String oprid) {
		this.oprid = oprid;
	}
	public String getMiscDesc() {
		return miscDesc;
	}
	public void setMiscDesc(String miscDesc) {
		this.miscDesc = miscDesc;
	}
	public String getMiscType() {
		return miscType;
	}
	public void setMiscType(String miscType) {
		this.miscType = miscType;
	}
	public double getMiscAmount() {
		return miscAmount;
	}
	public void setMiscAmount(double miscAmount) {
		this.miscAmount = miscAmount;
	}
	public String getReturnKey() {
		return returnKey;
	}
	public void setReturnKey(String returnKey) {
		this.returnKey = returnKey;
	}
	public int getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(int payStatus) {
		this.payStatus = payStatus;
	}
	
	
	
}
