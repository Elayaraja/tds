package com.charges.bean;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.tds.tdsBO.OpenRequestBO;

public class DisbursementMaster {
	
	private ArrayList<OpenRequestBO> al_opr;
	private double grantTotal;
	private String masterkey;
	private String driverid;
	private String drivername;
	private String processDate;
	private String paymentToType;
	private BigDecimal openingBalance=BigDecimal.ZERO;
	private BigDecimal closingBalance=BigDecimal.ZERO;
	
	
	
	
	private int chargeCalcType;
	
	public int getChargeCalcType() {
		return chargeCalcType;
	}
	public void setChargeCalcType(int chargeCalcType) {
		this.chargeCalcType = chargeCalcType;
	}
	public ArrayList<OpenRequestBO> getAl_opr() {
		return al_opr;
	}
	public void setAl_opr(ArrayList<OpenRequestBO> alOpr) {
		al_opr = alOpr;
	}
	public double getGrantTotal() {
		return grantTotal;
	}
	public void setGrantTotal(double grantTotal) {
		this.grantTotal = grantTotal;
	}
	public String getMasterkey() {
		return masterkey;
	}
	public void setMasterkey(String masterkey) {
		this.masterkey = masterkey;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getDrivername() {
		return drivername;
	}
	public void setDrivername(String drivername) {
		this.drivername = drivername;
	}
	public String getProcessDate() {
		return processDate;
	}
	public void setProcessDate(String processDate) {
		this.processDate = processDate;
	}
	public String getPaymentToType() {
		return paymentToType;
	}
	public void setPaymentToType(String paymentToType) {
		this.paymentToType = paymentToType;
	}
	public void setOpeningBalance(BigDecimal openingBalance) {
		this.openingBalance = openingBalance;
	}
	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}
	public void setClosingBalance(BigDecimal closingBalance) {
		this.closingBalance = closingBalance;
	}
	public BigDecimal getClosingBalance() {
		return closingBalance;
	}	
	
	

}
