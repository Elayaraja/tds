package com.charges.bean;

public class DisbursementRecordDetail {

	private String txNo;
	private String tripID;
	private double driverPayAmount;
	private String ccVoucherFlag;
	public String getTxNo() {
		return txNo;
	}
	public void setTxNo(String txNo) {
		this.txNo = txNo;
	}
	public String getTripID() {
		return tripID;
	}
	public void setTripID(String tripID) {
		this.tripID = tripID;
	}
	public double getDriverPayAmount() {
		return driverPayAmount;
	}
	public void setDriverPayAmount(double driverPayAmount) {
		this.driverPayAmount = driverPayAmount;
	}
	public String getCcVoucherFlag() {
		return ccVoucherFlag;
	}
	public void setCcVoucherFlag(String ccVoucherFlag) {
		this.ccVoucherFlag = ccVoucherFlag;
	}
	
	
	
}
