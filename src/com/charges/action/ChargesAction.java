package com.charges.action;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;
import javazoom.upload.UploadParameters;

import org.apache.log4j.Category;
import org.json.JSONArray;
import org.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.charges.bean.DisbursementDetaillBean;
import com.charges.bean.DisbursementMaster;
import com.charges.bean.DisbursementRecordDetail;
import com.charges.bean.DisbursementRecordMaster;
import com.charges.bean.DriverChargesBean;
import com.charges.bean.LeaseBean;
import com.charges.bean.MiscBean;
import com.charges.constant.IChargeConstants;
import com.charges.dao.ChargesDAO;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;
import com.tds.action.RegistrationAction;
import com.tds.controller.SystemUnavailableException;
import com.tds.controller.TDSController;
import com.tds.dao.UtilityDAO;
import com.tds.dao.ZoneDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.util.Finance;

public class ChargesAction extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public static ServletConfig config;
	private final Category cat = TDSController.cat;



	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ RegistrationAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Get Method  ");
		try {
			doProcess(request,response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Access Post method from JSP page or Browser
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Post Method");
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Access request from both getter and setter methods
	 */

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException, SystemUnavailableException {
		cat.info("In Process Method");


		HttpSession m_session = request.getSession(false);

		String eventParam = "";
		boolean m_sessionStatus = false;
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		//System.out.println("The user requested event as "+ eventParam);
		cat.info("The user requested event as "+ eventParam);
		if(m_session != null && m_session.getAttribute("user") != null) {
			if(eventParam.equalsIgnoreCase("chargeType")) {
				chargeType(request,response); 
			} else if(eventParam.equalsIgnoreCase("chargeTemplateMaster")){
				chargeTemplate(request,response);
			}else if(eventParam.equalsIgnoreCase("chargeTemplateDetail")) {
				editChargeTemplate(request,response);
			} else if(eventParam.equalsIgnoreCase("showDriverDisbursement")) {
				calculateDisburement(request, response);
			} else if(eventParam.equalsIgnoreCase("showChargeAdd")){
				showChargeAdd(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteLeaseRate")){
				deleteLeaseRate(request,response);
			} else if(eventParam.equalsIgnoreCase("addLeaseRates")){
				addLeaseRates(request,response);
			}else if(eventParam.equalsIgnoreCase("addMiscCharges")){
				addMiscCharges(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteMiscCharges")){
				deleteMiscCharges(request,response);
			}else if(eventParam.equals("storeMisc")){
				storeMisc(request,response);
			}else if(eventParam.equals("storeLease")){
				storeLease(request,response);
			}else if(eventParam.equalsIgnoreCase("driverDisbursementSummary")){
				driverDisbursementSummary(request,response);
			}else if(eventParam.equalsIgnoreCase("uploadMiscCharge")){
				uploadMiscCharge(request,response);
			}else if(eventParam.equalsIgnoreCase("tmpCalcChar")){
				calculateDisburement(request,response);
			}else if(eventParam.equalsIgnoreCase("tagdisbursment")){
				tagdisbursment(request,response);
			}


		}
	}
	public void driverDisbursementSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//	ArrayList al_list = null;
		HttpSession session = request.getSession();
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		ArrayList<DisbursementRecordMaster> al_list=new ArrayList<DisbursementRecordMaster>();

		if(request.getParameter("Button")!=null && request.getParameter("Button").equals("Get")) {
			String driver = "";
			String tagname=""; 
			if(!adminBO.getUsertypeDesc().equalsIgnoreCase("Driver"))
			{
				driver = request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id");
			} else {

				driver = adminBO.getUid();

			}
			tagname=request.getParameter("tagName")==null?"":request.getParameter("tagName");
			al_list = ChargesDAO.getDriverDisbrushmentSummary(TDSValidation.getDBdateFormat(request.getParameter("from_date")), TDSValidation.getDBdateFormat(request.getParameter("to_date")), adminBO.getAssociateCode(),driver,adminBO.getUid(),adminBO.getTimeZone(),tagname);
			DisbursementMaster disbursementMaster=ChargesDAO.getBalanceDetailsForSummary1(adminBO, al_list);
			request.setAttribute("al_list", al_list);
			request.setAttribute("disbursementMaster", disbursementMaster);
			request.setAttribute("screen", "/charges/DriverDisbursementSummary.jsp");
		}  else if(request.getParameter("Button")!=null && request.getParameter("Button").equals("showDisDetailSummary")){
			ArrayList<DriverChargesBean> listOfCharges = new ArrayList<DriverChargesBean>();
			ArrayList<OpenRequestBO> listOfTrips= new ArrayList<OpenRequestBO>();
			ArrayList<DriverChargesBean> listOfPayments = new ArrayList<DriverChargesBean>();
			ArrayList<OpenRequestBO>listOfJobs=new ArrayList<OpenRequestBO>();
			ArrayList<String> totals = new ArrayList<String>();
			String prevTrip="";
			String paymentKey=request.getParameter("pss_key")==null?"":request.getParameter("pss_key");
			listOfJobs=ChargesDAO.getTripDetailsForSummary(adminBO,paymentKey);
			ArrayList<String> listOfTripIDs = new ArrayList<String>();
			for(int k=0;k<listOfJobs.size();k++){
				listOfTripIDs.add(listOfJobs.get(k).getTripid());
			}
			listOfCharges = ChargesDAO.getDriverChargeDetailsForSummary(adminBO.getAssociateCode(),paymentKey,adminBO.getMasterAssociateCode(),listOfTripIDs);
			for(int i=0;i<listOfCharges.size();i++){
				OpenRequestBO orBO=new OpenRequestBO();
				if(!listOfCharges.get(i).getTripID().equals(prevTrip)){
					orBO.setTripid(listOfCharges.get(i).getTripID());
					prevTrip = listOfCharges.get(i).getTripID();
				}
				listOfTrips.add(orBO);
			}
			listOfPayments = ChargesDAO.getDriverPaymentDetailsForSummary(adminBO.getAssociateCode(), paymentKey,listOfTripIDs,adminBO.getTimeZone());
			if(listOfPayments.size()==0){
				listOfPayments = ChargesDAO.getDriverCCPaymentDetailsForSummary(adminBO.getAssociateCode(), paymentKey,listOfTripIDs,adminBO.getTimeZone());
			}
			for(int i=0;i<listOfPayments.size();i++){
				OpenRequestBO orBO=new OpenRequestBO();
				if(!listOfPayments.get(i).getTripID().equals(prevTrip)){
					orBO.setTripid(listOfPayments.get(i).getTripID());
					prevTrip = listOfPayments.get(i).getTripID();
				}
				listOfTrips.add(orBO);
			}
			if(listOfPayments.size()>0){
				int l = 1;
				String prevTripForSummary = listOfPayments.get(0).getTripID();
				for(int m=1;m< listOfPayments.size();m++){
					if(!prevTripForSummary.equals(listOfPayments.get(m).getTripID())){
						totals.add(prevTripForSummary);
						totals.add(l+"");
						l=1;
						prevTripForSummary=listOfPayments.get(m).getTripID();
					} else {
						l++;
					}
				}
				totals.add(listOfPayments.get(listOfPayments.size()-1).getTripID());
				totals.add(l+"");
			}else if(listOfPayments.size()>0) {
				int l = 1;
				String prevTripForSummary = listOfCharges.get(0).getTripID();
				for(int m=1;m< listOfCharges.size();m++){
					System.out.println(listOfCharges.get(m).getTripID());
					if(!prevTripForSummary.equals(listOfCharges.get(m).getTripID())){
						totals.add(prevTripForSummary);
						totals.add(l+"");
						l=1;
						prevTripForSummary=listOfCharges.get(m).getTripID();
					} else {
						l++;
					}
				}
				totals.add(listOfCharges.get(listOfCharges.size()-1).getTripID());
				totals.add(l+"");

			}

			//Get Balance Record
			DisbursementMaster disbursementMaster=ChargesDAO.getBalanceDetailsForSummary(adminBO, paymentKey);
			//Get Lease Data
			ArrayList<LeaseBean> listOfLeases = ChargesDAO.getLeaseBean(adminBO.getAssociateCode(), paymentKey);

			//Get Misc Charges Data
			ArrayList<MiscBean> listOfMiscCharges = ChargesDAO.getMiscBean(adminBO.getAssociateCode(),"",adminBO.getMasterAssociateCode(),paymentKey);

			//NO trips then just return
			request.setAttribute("listOfJobs", listOfJobs);
			request.setAttribute("listOfCharges",listOfCharges);
			request.setAttribute("listOfLeases", listOfLeases);
			request.setAttribute("listOfMiscCharges", listOfMiscCharges);
			request.setAttribute("listOfPayment", listOfPayments);
			request.setAttribute("disbursementMaster", disbursementMaster);
			request.setAttribute("lease", ChargesDAO.getVechicalType(adminBO.getMasterAssociateCode()));
			request.setAttribute("misc",ChargesDAO.getMiscSetup(adminBO));
			request.setAttribute("totals", totals);
			//request.setAttribute("screen", "/charges/DriverDisbursement1.jsp"); 

			//al_sublist = ChargesDAO.getDriverDisbrushmentDetail(request.getParameter("pss_key")==null?"":request.getParameter("pss_key"),adminBO);
			//request.setAttribute("al_sublist", al_sublist);
			request.setAttribute("screen", "/charges/DriverDisbursementDetailNew.jsp");
			getServletContext().getRequestDispatcher("/charges/DriverDisbursementDetailNew.jsp").forward(request, response);
			return;
		} else {
			request.setAttribute("screen", "/charges/DriverDisbursementSummary.jsp");
		}

		request.setAttribute("al_list", al_list);
		request.setAttribute("al_driver", UtilityDAO.getDriverList(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(),0));

		requestDispatcher.forward(request, response);

	}
	public void storeLease(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String button = request.getParameter("button");
		LeaseBean leaseBean = new LeaseBean();
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		if(button.equals("Submit"))
		{
			collectLeaseBean(request,leaseBean,adminBO);
			try{
				ChargesDAO.storeLeaseBean(leaseBean);
				response.getWriter().write("1");
			} catch (SQLException e) {
				// TODO: handle exception
				response.getWriter().write("0");
			}
		}
		//	showChargeAdd(request, response);
	}

	private void collectLeaseBean(HttpServletRequest request, LeaseBean leaseBean,AdminRegistrationBO adminBO) {
		leaseBean.setAssocode(adminBO.getAssociateCode());
		leaseBean.setDriverid(request.getParameter("ddid"));
		leaseBean.setVechAmount(Double.parseDouble(request.getParameter("leaseAmount")));
		leaseBean.setVechDesc(request.getParameter("vechType"));
		leaseBean.setOprid(adminBO.getUid());
		leaseBean.setVechileNo(request.getParameter("cabNo"));
		String date = request.getParameter("process_Date")==null?"":request.getParameter("process_Date");
		DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
		try {
			java.util.Date processDate = df.parse(date);
			leaseBean.setProcessDate(new java.sql.Date(processDate.getTime()));
		} catch (ParseException e) {
			leaseBean.setProcessDate(new java.sql.Date(new java.util.Date().getTime()));
			e.printStackTrace();
		}

	}

	public void storeMisc(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String button = request.getParameter("button");
		MiscBean miscBean = new MiscBean();
		if(button.equals("Submit"))
		{
			collectMiscBean(request,miscBean);
			try{
				ChargesDAO.storeMiscBean(miscBean);
				response.getWriter().write("1");
			} catch (SQLException e) {
				// TODO: handle exception
				response.getWriter().write("0");
			}
		}
		//showChargeAdd(request, response);
	}
	private void collectMiscBean(HttpServletRequest request, MiscBean miscBean) {
		miscBean.setAssocode(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode());
		miscBean.setDriverid(request.getParameter("ddid"));
		miscBean.setMiscAmount(Double.parseDouble(request.getParameter("miscAmount")));
		miscBean.setMiscDesc(request.getParameter("miscName"));
		miscBean.setOprid(((AdminRegistrationBO)request.getSession().getAttribute("user")).getUid());
		miscBean.setCustomField(request.getParameter("customField")==null?"":request.getParameter("customField"));
		String date = request.getParameter("process_Date")==null?"":request.getParameter("process_Date");

		DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
		try {
			java.util.Date processDate = df.parse(date);
			miscBean.setProcessDate(new java.sql.Date(processDate.getTime()));
		} catch (ParseException e) {
			miscBean.setProcessDate(new java.sql.Date(new java.util.Date().getTime()));
			e.printStackTrace();
		}

	}
	public void showChargeAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession m_session = request.getSession(true);
		ArrayList<String> lease= ChargesDAO.getVechicalType(((AdminRegistrationBO)m_session.getAttribute("user")).getAssociateCode());
		JSONArray totalLease = new JSONArray();
		if(lease.size()>0){
			for(int i=0;i<lease.size();i++){
				JSONObject LeaseDetails = new JSONObject();
				totalLease.put(LeaseDetails);
			}
			response.getWriter().write(totalLease.toString());
		}
		//request.setAttribute("screen", "/charges/AddMiscLease.jsp");
		//request.setAttribute("type", request.getParameter("type"));
		/*	RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);*/

	}


	public void chargeType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("buttonSubmit")==null || request.getParameter("buttonSubmit").equals("")){
			ArrayList<ChargesBO> charges=ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),0);
			request.setAttribute("charges", charges);
		} else if(request.getParameter("buttonSubmit")!=null && request.getParameter("buttonSubmit").equalsIgnoreCase("submit")){
			int numberOfRows = Integer.parseInt(request.getParameter("fieldSize"));
			String[] amount=new String[numberOfRows+1];
			String[] key=new String[numberOfRows+1];
			String[] description=new String[numberOfRows+1];
			String[] formula=new String[numberOfRows+1];
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				amount[rowIterator]=(request.getParameter("staticAmount"+rowIterator).equals("")?"0":request.getParameter("staticAmount"+rowIterator));
				key[rowIterator]=request.getParameter("key"+rowIterator);
				description[rowIterator]=request.getParameter("description"+rowIterator);
				formula[rowIterator]=request.getParameter("formula"+rowIterator).replaceAll("%", "pct").replaceAll("\\+", "plus");
			}
			int result=ChargesDAO.insertChargeTypes(adminBO.getMasterAssociateCode(),amount,key,description,formula,numberOfRows);
			if(result==0){
				ArrayList<ChargesBO> charges=ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),0);
				request.setAttribute("error", "Fail to insert charge types");
				request.setAttribute("charges", charges);
			} else {
				ZoneDAO.updateVersion(adminBO.getMasterAssociateCode(), 2);
				ArrayList<ChargesBO> charges=ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),0);
				request.setAttribute("error", "Charge types successfully inserted");
				request.setAttribute("charges", charges);
			}
		} 
		request.setAttribute("screen", "/charges/ChargeType.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response); 
	}
	public void chargeTemplate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		int includeType = 2;
		if(request.getParameter("buttonSubmit")!=null && request.getParameter("buttonSubmit").equalsIgnoreCase("submit")){
			int numberOfRows = Integer.parseInt(request.getParameter("fieldSize"));
			String[] amountType=new String[numberOfRows+1];
			String[] key=new String[numberOfRows+1];
			String[] description=new String[numberOfRows+1];
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				amountType[rowIterator]=(request.getParameter("amountType"+rowIterator).equals("")?"0":request.getParameter("amountType"+rowIterator));
				key[rowIterator]=request.getParameter("key"+rowIterator);
				description[rowIterator]=request.getParameter("description"+rowIterator);
			}
			int result=ChargesDAO.insertChargeTemplate(adminBO.getMasterAssociateCode(),amountType,key,description,numberOfRows);
			if(result==0){
				request.setAttribute("error", "Fail to insert charge templates");
			} else {
				request.setAttribute("error", "Charge templates successfully inserted");
			}
		} 
		ArrayList<ChargesBO> charges=ChargesDAO.getChargeTemplate(adminBO.getMasterAssociateCode(),includeType);
		request.setAttribute("charges", charges);
		request.setAttribute("screen", "/charges/ChargeTemplateMaster.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response); 
	}
	public void editChargeTemplate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		if(request.getParameter("buttonSubmit")!=null && request.getParameter("buttonSubmit").equalsIgnoreCase("submit")){
			int numberOfRows = Integer.parseInt(request.getParameter("fieldSize"));
			String[] amountType=new String[numberOfRows+1];
			String[] key=new String[numberOfRows+1];
			String[] amount=new String[numberOfRows+1];
			String[] payTypeKey=new String[numberOfRows+1];
			String[] templateKey=new String[numberOfRows+1];

			String[] templateTax1=new String[numberOfRows+1];
			String[] templateTax2=new String[numberOfRows+1];
			String[] templateTax3=new String[numberOfRows+1];

			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				amountType[rowIterator]=(request.getParameter("amountType"+rowIterator).equals("")?"0":request.getParameter("amountType"+rowIterator));
				key[rowIterator]=request.getParameter("key"+rowIterator);
				amount[rowIterator]=request.getParameter("amountDetail"+rowIterator);
				payTypeKey[rowIterator]=request.getParameter("payType"+rowIterator);
				templateKey[rowIterator]=request.getParameter("templateType"+rowIterator);

				templateTax1[rowIterator]=request.getParameter("tax1temp"+rowIterator);
				templateTax2[rowIterator]=request.getParameter("tax2temp"+rowIterator);
				templateTax3[rowIterator]=request.getParameter("tax3temp"+rowIterator);
			}
			int result=ChargesDAO.insertChargeTemplateDetail(adminBO.getMasterAssociateCode(),amountType,key,amount,templateKey,payTypeKey,numberOfRows,templateTax1,templateTax2,templateTax3);

			if(result==0){
				request.setAttribute("error", "Fail to insert charge template detail");
			} else {
				request.setAttribute("error", "Charge template detail successfully inserted");
			}
		} 
		ArrayList<ChargesBO> chargeDetail=ChargesDAO.getChargeTemplateDetail(adminBO.getMasterAssociateCode());
		ArrayList<ChargesBO> charges=ChargesDAO.getChargeTemplate(adminBO.getMasterAssociateCode(),0);
		ArrayList<ChargesBO> chargeType=ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),0);
		request.setAttribute("charges", charges);
		request.setAttribute("chargeTypes", chargeType);
		request.setAttribute("chargeDetail", chargeDetail);
		request.setAttribute("screen", "/charges/ChargeTemplateDetail.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response); 
	}

	private void collectDisbursementData(HttpServletRequest request,HttpSession m_session,DisbursementRecordMaster recordMaster){

		ArrayList<DisbursementRecordDetail> recordDetails = new ArrayList<DisbursementRecordDetail>();
		double grantTotal = 0.0;
		int orsize = request.getParameter("orsize") ==null?0:Integer.parseInt(request.getParameter("orsize"));

		for(int i=0;i<orsize;i++)
		{
			System.out.println(request.getParameter("tripID_"+i));
			if(request.getParameter("chck"+request.getParameter("tripID_"+i))!=null)
			{
				int drcsize = request.getParameter("totalIter_"+i) ==null?0:Integer.parseInt(request.getParameter("totalIter_"+i));
				for(int j=0;j<drcsize;j++)
				{
					DisbursementRecordDetail recordDetail = new DisbursementRecordDetail();
					recordDetail.setDriverPayAmount(Double.parseDouble(request.getParameter("drvAmt"+request.getParameter("tripID_"+i)+"_"+j)));
					recordDetail.setTripID(request.getParameter("tripID_"+i));
					recordDetail.setTxNo(request.getParameter("txNo"+request.getParameter("tripID_"+i)+"_"+j));
					recordDetail.setCcVoucherFlag(request.getParameter("chargeType"+request.getParameter("tripID_"+i)+"_"+j));
					recordDetails.add(recordDetail);
				}
			}
		}
		grantTotal = request.getParameter("grantTotal")==null?0.0:request.getParameter("grantTotal").equals("")?0.0:Double.parseDouble(request.getParameter("grantTotal"));

		String date = request.getParameter("process_Date");

		DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
		try {
			java.util.Date processDate = df.parse(date);
			recordMaster.setProcessDate(new java.sql.Date(processDate.getTime()));
		} catch (ParseException e) {
			recordMaster.setProcessDate(new java.sql.Date(new java.util.Date().getTime()));
			e.printStackTrace();
		}

		recordMaster.setRecordDetails(recordDetails);
		recordMaster.setGrantamount(grantTotal);
		recordMaster.setDriverid(request.getParameter("dd_driver_id"));
		recordMaster.setOperatorid(((AdminRegistrationBO)m_session.getAttribute("user")).getUid());
		recordMaster.setChargeCalcType(Integer.parseInt(request.getParameter("chargeType")));
		recordMaster.setPaymentToType(request.getParameter("paymentToType"));
		recordMaster.setClosingBalance(Double.parseDouble(request.getParameter("closingBalance")));



	}
	public void addLeaseRates(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In AddLeaseRates method");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		HttpSession session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		ArrayList<ChargesBO> getLeaseRate= new ArrayList<ChargesBO>();
		int result=0;
		if(request.getParameter("button")!=null){
			int numberOfRows = Integer.parseInt(request.getParameter("size"));
			String[] vehicleDescription=new String[numberOfRows+1];
			String[] leaseAmount=new String[numberOfRows+1];
			String[] numberOfPassengers=new String[numberOfRows+1];
			String[] startRate=new String[numberOfRows+1];
			String[] ratePerMile=new String[numberOfRows+1];
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				vehicleDescription[rowIterator]=request.getParameter("vehicleDescription"+rowIterator);
				leaseAmount[rowIterator]=request.getParameter("leaseAmount"+rowIterator);
				numberOfPassengers[rowIterator]=request.getParameter("numberOfPassengers"+rowIterator);
				startRate[rowIterator]=request.getParameter("startRate"+rowIterator);
				ratePerMile[rowIterator]=request.getParameter("ratePerMile"+rowIterator);
			}
			result=ChargesDAO.insertLeaseRates(vehicleDescription,leaseAmount,numberOfPassengers,startRate,ratePerMile,numberOfRows,adminBO);
			if(result==0){
				request.setAttribute("errors","Enter all the Values correctly");
			}
		}
		getLeaseRate=ChargesDAO.getLeaseRates(adminBO);
		request.setAttribute("getLeaseRate", getLeaseRate);
		request.setAttribute("screen","/Financial/insertLeaseRates.jsp");
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);

	}



	public void deleteLeaseRate(HttpServletRequest request, HttpServletResponse response){
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String serialNumber=request.getParameter("serialNumber");
		int result=ChargesDAO.deleteLease(adminBO.getAssociateCode(), serialNumber);
		try {
			response.getWriter().write(result+"");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void addMiscCharges(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In addMiscCharges method");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		HttpSession session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		ArrayList<ChargesBO> getMiscCharges= new ArrayList<ChargesBO>();
		int result=0;
		if(request.getParameter("button")!=null){
			int numberOfRows = Integer.parseInt(request.getParameter("size"));
			/*String[] driverId=new String[numberOfRows+1];*/
			String[] miscDetails=new String[numberOfRows+1];
			String[] miscAmount=new String[numberOfRows+1];
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				/*driverId[rowIterator]=request.getParameter("driverId"+rowIterator);*/
				miscDetails[rowIterator]=request.getParameter("miscDesc"+rowIterator);
				miscAmount[rowIterator]=request.getParameter("amount"+rowIterator);
			}
			result=ChargesDAO.insertMiscSetup(miscDetails,miscAmount,numberOfRows,adminBO);
			if(result==0){
				request.setAttribute("errors","Enter all the Values correctly");
			}
		}
		request.setAttribute("getMiscCharges", ChargesDAO.getMiscSetup(adminBO));
		request.setAttribute("screen","/Financial/insertMiscCharges.jsp");
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);

	}

	public void deleteMiscCharges(HttpServletRequest request, HttpServletResponse response){
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String serialNumber=request.getParameter("serialNumber");
		int result=ChargesDAO.deleteMiscCharges(adminBO.getAssociateCode(), serialNumber);
		try {
			response.getWriter().write(result+"");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void uploadMiscCharge(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		String fn=""; 
		ArrayList<ChargesBO> chargesList = new ArrayList<ChargesBO>();
		//Starting to Write the file in a specific Folder.
		if(request.getParameter("uploadFile")!=null){ 
			//The path is given here to store the file.
			String path = "/tmp";				
			try{
				//MultiPart request renamed it into name value pair called RemoteIpValue
				MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
				//getting the files
				Hashtable files = mrequest.getFiles();
				//Getting the path for the file.
				UploadFile file = ((UploadFile) files.get("filePath")); 

				if (MultipartFormDataRequest.isMultipartFormData(request)){
					// Uses MultipartFormDataRequest to parse the HTTP request.
					if ( (files != null) && (!files.isEmpty()) ){
						UploadBean upBean=new UploadBean();
						upBean.setFolderstore(path);
						upBean.store(mrequest); 
						Vector history = upBean.getHistory();
						UploadParameters up = (UploadParameters) history.elementAt(0);
						if(up.getAltFilename()!=null){
							fn=up.getAltFilename();
						}else{
							fn=((UploadFile) files.get("filePath")).getFileName();
						}
						if (file != null) 
							request.setAttribute("fileName", file.getFileName() );
					}
				}
				// Open the file that is the first 
				// command line parameter
				//csv file containing data
				String strFile = path+"/"+fn;
				//create BufferedReader to read csv file
				BufferedReader br = new BufferedReader( new FileReader(strFile));
				String strLine = "";

				//int count = 0;
				String[] values=null;
				FileInputStream fstream = new FileInputStream(strFile);
				// Get the object of DataInputStream
				DataInputStream in = new DataInputStream(fstream);
				br = new BufferedReader(new InputStreamReader(in));
				while ((strLine = br.readLine()) != null){
					ChargesBO charBo=new ChargesBO();
					if(strLine!=null && !strLine.equalsIgnoreCase("")){
						//						if(request.getParameter("splitBy").equals("1")){
						//							values=strLine.split(";");
						//						} else {
						values=strLine.split(",");
						//						}
						charBo.setDriverId(values[0]);
						charBo.setKey(Integer.parseInt(values[1]));
						charBo.setProcessDate(values[2]);
						charBo.setMiscDesc(values[3]);
						charBo.setCustomField(values[4]);
						charBo.setAmount(values[5]);

						chargesList.add(charBo);
					}
				}
				ChargesDAO.insertMiscChargesUpload(adminBO.getAssociateCode(),chargesList);
				request.setAttribute("error", "Successfully uploaded");
			}catch (Exception e){//Catch exception if any
				System.err.println("Error: " + e.getMessage());
				request.setAttribute("error", "Failed to upload");
			}
		}
		request.setAttribute("screen", "/charges/miscChargeUpload.jsp"); 
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}

	public void calculateDisburement(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
		//	AdminRegistrationBO adminBO =  new AdminRegistrationBO();
		//	adminBO.setAssociateCode("105");
		//	adminBO.setMasterAssociateCode("105");
		String tripID=request.getParameter("tripID")==null?"":request.getParameter("tripID");
		String tmpltID = request.getParameter("template");
		String driverID = request.getParameter("dd_driver_id")==null?"":request.getParameter("dd_driver_id");
		if(request.getParameter("submitTemplate")==null || !request.getParameter("submitTemplate").equals("1"))
		{
			tmpltID = ChargesDAO.getTemplateForDriver(adminBO.getAssociateCode(), driverID);

		} 

		if(request.getParameter("recordPayment")!=null){
			DisbursementRecordMaster recordMaster = new DisbursementRecordMaster();
			try {
				collectDisbursementData(request,session,recordMaster);
				ChargesDAO.storeDisbursementRecord(recordMaster,adminBO);
				request.setAttribute("message", "Record Stored Sucessfully");
			}catch (SQLException e) {
				// TODO: handle exception
				request.setAttribute("message", "Failed to insert please Try again...." );
				e.printStackTrace();
			}
			//request.setAttribute("message", "Record Stored Sucessfully");
			request.setAttribute("screen", "/charges/DriverDisbursement1.jsp"); 
			getServletContext().getRequestDispatcher("/charges/DriverDisbursement1.jsp").forward(request, response);
			return;
		}
		String processingDate = request.getParameter("process_Date")==null?"":request.getParameter("process_Date");
		String startDate = request.getParameter("from_date")==null?"":request.getParameter("from_date");
		String endDate = request.getParameter("to_date")==null?"":request.getParameter("to_date");
		String startTime = request.getParameter("fromTime")==null?"":request.getParameter("fromTime");
		String endTime = request.getParameter("toTime")==null?"":request.getParameter("toTime");
		DisbursementMaster disbursementMaster = new DisbursementMaster();
		ArrayList<DriverChargesBean> listOfCharges = new ArrayList<DriverChargesBean>();
		ArrayList<DisbursementDetaillBean> chargeCalcPctData  = null;
		ArrayList<DriverChargesBean> listOfPayments = new ArrayList<DriverChargesBean>();
		ArrayList<DriverChargesBean> chargesForPayment= new ArrayList<DriverChargesBean>();
		ArrayList<DriverChargesBean> paymentForCharges= new ArrayList<DriverChargesBean>();
		ArrayList<OpenRequestBO> listOfTrips= new ArrayList<OpenRequestBO>();
		ArrayList<OpenRequestBO>listOfJobs=new ArrayList<OpenRequestBO>();
		//ArrayList<String> listOfDrivers= ChargesDAO.getDriverList(adminBO);
		String prevTrip="";
		int paymentType = 0;
		//???
		//Which has been has been pressed?
		//1 Search
		//2 Submit and record payment
		//3 Change template

		//Get the Charge Template

		//Get All Templates for company
		ArrayList<ChargesBO> charges=ChargesDAO.getChargeTemplate(adminBO.getMasterAssociateCode(),2);
		disbursementMaster.setMasterkey(tmpltID);
		//Using the template get calc % Details
		chargeCalcPctData = ChargesDAO.getDriverChargePctTemplate(adminBO.getMasterAssociateCode(),driverID,startDate,endDate,tmpltID, disbursementMaster);
		if(request.getParameter("Button")!=null){
			//Get Trip Details f all trips that have not been paid
			listOfJobs=ChargesDAO.getTripDetails(adminBO,tripID,driverID,startDate,endDate,startTime,endTime);
			ArrayList<String> listOfTripIDs = new ArrayList<String>();
			for(int k=0;k<listOfJobs.size();k++){
				listOfTripIDs.add(listOfJobs.get(k).getTripid());
			}
			//Get Charges Records
			if(disbursementMaster.getChargeCalcType()==IChargeConstants.PAYMENT_TYPE_CHARGE){
				listOfCharges = ChargesDAO.getDriverChargeDetails(adminBO.getAssociateCode(), driverID, "", "","","", tmpltID,adminBO.getMasterAssociateCode(),listOfTripIDs,adminBO.getTimeZone());
				paymentForCharges= ChargesDAO.getDriverPaymentDetails(adminBO.getAssociateCode(), driverID, "", "","","",listOfTripIDs,adminBO.getMasterAssociateCode(),adminBO.getTimeZone());
				for(int i=0;i<listOfCharges.size();i++){
					OpenRequestBO orBO=new OpenRequestBO();
					if(!listOfCharges.get(i).getTripID().equals(prevTrip)){
						orBO.setTripid(listOfCharges.get(i).getTripID());
						prevTrip = listOfCharges.get(i).getTripID();
					}
					listOfTrips.add(orBO);
				}
				if(listOfCharges.size()==0){

				}
			}
			//Get Payment Records
			if(disbursementMaster.getChargeCalcType()==IChargeConstants.PAYMENT_TYPE_CC){
				listOfPayments = ChargesDAO.getDriverPaymentDetails(adminBO.getAssociateCode(), driverID, "", "","","", listOfTripIDs,adminBO.getMasterAssociateCode(),adminBO.getTimeZone());
				chargesForPayment= ChargesDAO.getDriverChargeDetails(adminBO.getAssociateCode(), driverID, "", "","","", tmpltID,adminBO.getMasterAssociateCode(),listOfTripIDs,adminBO.getTimeZone());
				if(listOfPayments.size()>0){
					for(int i=0;i<listOfPayments.size();i++){
						OpenRequestBO orBO=new OpenRequestBO();
						if(!listOfPayments.get(i).getTripID().equals(prevTrip)){
							orBO.setTripid(listOfPayments.get(i).getTripID());
							prevTrip = listOfPayments.get(i).getTripID();
						}
						listOfTrips.add(orBO);
					}
				}
				for(int i=0;i<chargesForPayment.size();i++){
					if(chargesForPayment.get(i).getDC_TYPE_CODE()==3){
						chargesForPayment.remove(i);
						break;
					}
				}
			}
		}
		//Get Balance Record
		ChargesDAO.getBalanceDetails(adminBO, driverID, disbursementMaster);
		//Get Lease Data
		ArrayList<LeaseBean> listOfLeases = ChargesDAO.getLeaseBean(adminBO.getAssociateCode(), driverID);

		//Get Misc Charges Data
		ArrayList<MiscBean> listOfMiscCharges = ChargesDAO.getMiscBean(adminBO.getAssociateCode(),driverID,adminBO.getMasterAssociateCode(),"");

		//NO trips then just return
		if( listOfPayments.size()==0  && listOfCharges.size()==0){
			if(request.getParameter("Button")!=null){
				request.setAttribute("message", "No records Found...." );
			}
			request.setAttribute("listOfJobs", listOfJobs);
			request.setAttribute("listOfCharges",listOfCharges);
			request.setAttribute("listOfLeases", listOfLeases);
			request.setAttribute("listOfMiscCharges", listOfMiscCharges);
			request.setAttribute("listOfPayment", listOfPayments);
			request.setAttribute("disbursementMaster", disbursementMaster);
			request.setAttribute("charges", charges);
			request.setAttribute("lease", ChargesDAO.getVechicalType(adminBO.getMasterAssociateCode()));
			request.setAttribute("misc",ChargesDAO.getMiscSetup(adminBO));
			request.setAttribute("screen", "/charges/DriverDisbursement1.jsp"); 
			getServletContext().getRequestDispatcher("/charges/DriverDisbursement1.jsp").forward(request, response);
			return;
		}
		ArrayList<String> totals = new ArrayList<String>();
		//Calculate Charges
		if(disbursementMaster.getChargeCalcType()==IChargeConstants.PAYMENT_TYPE_CC){
			Finance.calculateChargesOnPayment(chargeCalcPctData, listOfPayments);
			int l = 1;
			String prevTripForSummary = listOfPayments.get(0).getTripID();
			for(int m=1;m< listOfPayments.size();m++){
				if(!prevTripForSummary.equals(listOfPayments.get(m).getTripID())){
					totals.add(prevTripForSummary);
					totals.add(l+"");
					l=1;
					prevTripForSummary=listOfPayments.get(m).getTripID();
				} else {
					l++;
				}
			}
			totals.add(listOfPayments.get(listOfPayments.size()-1).getTripID());
			totals.add(l+"");
		} else {
			Finance.calculateChargesOnCharges(chargeCalcPctData, listOfCharges);
			int l = 1;
			String prevTripForSummary = listOfCharges.get(0).getTripID();
			for(int m=1;m< listOfCharges.size();m++){
				if(!prevTripForSummary.equals(listOfCharges.get(m).getTripID())){
					totals.add(prevTripForSummary);
					totals.add(l+"");
					l=1;
					prevTripForSummary=listOfCharges.get(m).getTripID();
				} else {
					l++;
				}
			}
			totals.add(listOfCharges.get(listOfCharges.size()-1).getTripID());
			totals.add(l+"");

		}

		//Populate all the data into the response and call jsp
		request.setAttribute("listOfCharges",listOfCharges);
		request.setAttribute("listOfJobs", listOfJobs);
		request.setAttribute("listOfLeases", listOfLeases);
		request.setAttribute("listOfMiscCharges", listOfMiscCharges);
		request.setAttribute("listOfPayment", listOfPayments);
		request.setAttribute("chargesForPayment", chargesForPayment);
		request.setAttribute("disbursementMaster", disbursementMaster);
		request.setAttribute("lease", ChargesDAO.getVechicalType(adminBO.getMasterAssociateCode()));
		request.setAttribute("misc",ChargesDAO.getMiscSetup(adminBO));
		request.setAttribute("charges", charges);
		request.setAttribute("totals", totals);
		request.setAttribute("lease", ChargesDAO.getVechicalType(adminBO.getMasterAssociateCode()));
		request.setAttribute("paymentForCharges", paymentForCharges);

		//	request.setAttribute("screen", "/charges/driverDisbursement.jsp"); 
		//	PrintWriter out = response.getWriter();
		//		for(int l = 0;l <listOfPayments.size();l++){
		//			System.out.println(listOfPayments.get(l).getTotalamount().toString()+" "+listOfPayments.get(l).getDC_DRIVER_PMT_AMT()+"</br>");
		//		}
		request.setAttribute("screen", "/charges/DriverDisbursement1.jsp"); 
		getServletContext().getRequestDispatcher("/charges/DriverDisbursement1.jsp").forward(request, response);
	}

	public void tagdisbursment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String tripId = request.getParameter("driverId");
		String tagname = request.getParameter("tName");
		int result=ChargesDAO.tagnameDisbursement(tripId,tagname,adminBO);
		if(result==1){
			response.getWriter().write("success");
		}else{
			response.getWriter().write("Failure to update");

		}
		//request.setAttribute("screen", "/charges/ChargeType.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response); 
	}

}