package com.charges.dao;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import org.apache.log4j.Category;

import com.charges.bean.ChargeDetail;
import com.charges.bean.ChargesBO;
import com.charges.bean.DisbursementDetaillBean;
import com.charges.bean.DisbursementMaster;
import com.charges.bean.DisbursementRecordDetail;
import com.charges.bean.DisbursementRecordMaster;
import com.charges.bean.DriverChargesBean;
import com.charges.bean.LeaseBean;
import com.charges.bean.MiscBean;
import com.charges.constant.ICCConstant;
import com.charges.constant.IChargeConstants;
import com.charges.constant.ITransactionType;
import com.common.util.TDSValidation;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import com.tds.cmp.bean.TransactionSummaryBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.process.OneToOneSMS;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.ClientAuthenticationBO;
import com.tds.tdsBO.OpenRequestBO;

public class ChargesDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(ChargesDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ChargesDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}
	//New Code
	public static ArrayList<DisbursementRecordMaster> getDriverDisbrushmentSummary(String from_date,String to_date, String assocode,String driver,String oprid,String timeZone,String tagname)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getDriverDisbrushmentSummary Start--->"+startTime);
		ArrayList<DisbursementRecordMaster> record=new ArrayList<DisbursementRecordMaster>();

		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		String query = "select DD_DISP_ID,DD_STATUS,DD_EFFECTIVE_DATE_TIME, " +
				" DD_DRIVERID,DR_FNAME,DR_LNAME,CONVERT_TZ(DD_PROCESSING_TIME,'UTC','"+timeZone+"') as p_date,DD_AMOUNT,DD_PAY_SOURCE,DD_TAG,DD_OPER_ID" +
				" from TDS_DRIVER_Disbursement, TDS_DRIVER " +
				" where " +
				" DD_DRIVERID=DR_SNO " ;
		if(!from_date.equals("")){
			query = query + " and date_format(CONVERT_TZ(DD_PROCESSING_TIME,'UTC','"+timeZone+"'),'%Y%m%d') >= '"+from_date+"'";
		}
		if(!to_date.equals(""))
		{
			query = query + " and date_format(CONVERT_TZ(DD_PROCESSING_TIME,'UTC','"+timeZone+"'),'%Y%m%d') <= '"+to_date+"'";
		}
		if(!driver.equals(""))
		{
			query =query + " and DD_DRIVERID = '"+driver+"'";
		}
   
		if(!tagname.equals(""))
		{
			query =query + " and DD_TAG = '"+tagname+"'";
		}

		query = query + "  and DD_ASSOCCODE = '"+assocode+"' order by DD_DISP_ID desc";

		try {
			m_pst = m_conn.prepareStatement(query);
			//System.out.println("query:"+m_pst.toString());
			rs = m_pst.executeQuery();
			while(rs.next())
			{
				DisbursementRecordMaster recordMaster = new DisbursementRecordMaster();
				recordMaster.setOpeningBalance(rs.getDouble("DD_AMOUNT"));
				recordMaster.setDriverid(rs.getString("DD_DRIVERID"));
				recordMaster.setDrivername(rs.getString("DR_FNAME")+" "+rs.getString("DR_LNAME"));
				recordMaster.setProcessDate(rs.getDate("p_date"));
				recordMaster.setuID(rs.getString("DD_DISP_ID"));
				recordMaster.setDisbursmentStatus(rs.getInt("DD_STATUS"));
				recordMaster.setOperatorid(rs.getString("DD_OPER_ID"));
				recordMaster.setTagname(rs.getString("DD_TAG"));
				record.add(recordMaster);
			}


		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getDriverDisbrushmentSummary-->"+sqex.getMessage());
			System.out.println("TDSException RegistrationDAO.getDriverDisbrushmentSummary-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getDriverDisbrushmentSummary End--->"+(System.currentTimeMillis()-startTime));
		return record;
	}


	//	public static ArrayList getDriverDisbrushmentDetail(String key)
	//	{
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getDriverDisbrushmentDetail Start--->"+startTime);
	//		ArrayList al_list = new ArrayList();
	//		TDSConnection m_dbConn = null;
	//		Connection m_conn = null;
	//		PreparedStatement m_pst = null;
	//		ResultSet rs = null;
	//		try {
	//			String query = "select * from" +
	//			"(" +
	//			"select" +
	//			"  CCD_DRIVER_ID as driverID,'CC' as Mtype,CCD_TRIP_ID as TripID,CCD_APPROVAL_CODE as approvecode,CCD_TOTAL_AMOUNT as Total,CCD_DRIVER_PMT_AMT as driverAmt,CCD_DRIVER_PMT_NUM as DRIVER_PMT_NUM" +
	//			" from TDS_CREDIT_CARD_DETAIL" +
	//			" UNION" +
	//			" select" +
	//			" VD_DRIVER_ID  as driverID,'Voucher' as Mtype,VD_TRIP_ID as TripID,VDD_APPROVAL_CODE as approvecode,VD_TOTAL as Total,VD_DRIVER_PMT_AMT as driverAmt,VD_DRIVER_PMT_NUM as DRIVER_PMT_NUM" +
	//			" from TDS_VOUCHER_DETAIL" +
	//			" UNION " +
	//			"select" +
	//			" DC_DRIVER_ID as driverID,'Charge' as Mtype,DC_TRIP_ID as TripID,'' as approvecode,DC_AMOUNT as Total,DC_DRIVER_PMT_AMT as driverAmt,DC_DRIVER_PMT_NUM as DRIVER_PMT_NUM" +
	//			" from TDS_DRIVER_CHARGE_CALC" +
	//			" UNION " +
	//			"select" +
	//			" DL_DRIVER_ID  as driverID,'Lease' as Mtype,'' as TripID,'' as approvecode,DL_AMOUNT as Total,DL_AMOUNT as driverAmt, DL_DRIVER_PMT_NUM as DRIVER_PMT_NUM" +
	//			" from TDS_DRIVER_LEASE" +
	//			" UNION " +
	//			"select" +
	//			"  DM_DRIVER_ID as driverID,'Misc.' as Mtype,'' as TripID,'' as approvecode,DM_MISC_AMOUNT as Total,DM_MISC_AMOUNT as driverAmt,DM_DRIVER_PMT_NUM as DRIVER_PMT_NUM" +
	//			" from TDS_DRIVER_MISC" +
	//			") as A where DRIVER_PMT_NUM = '"+key+"'";
	//			m_dbConn = new TDSConnection();
	//			m_conn = m_dbConn.getConnection();
	//			m_pst = m_conn.prepareStatement(query);
	//			rs = m_pst.executeQuery();
	//			while(rs.next())
	//			{
	//				ChargeDetail chargeDetail = new ChargeDetail();
	//				chargeDetail.setApprovalcode(rs.getString("approvecode"));
	//				chargeDetail.setDriverAmt(rs.getDouble("driverAmt"));
	//				chargeDetail.setMtype(rs.getString("mtype"));
	//				chargeDetail.setTotal(rs.getDouble("total"));
	//				chargeDetail.setTripid(rs.getString("tripid"));
	//				chargeDetail.setDriverID(rs.getString("driverID"));
	//				al_list.add(chargeDetail);
	//			}
	//		}  catch (SQLException sqex) {
	//			cat.error("TDSException RegistrationDAO.getDriverDisbrushmentSummary-->"+sqex.getMessage());
	//			System.out.println("TDSException RegistrationDAO.getDriverDisbrushmentSummary-->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//		} finally {
	//			m_dbConn.closeConnection();
	//		}
	//		cat.info("ChargesDAO.getDriverDisbrushmentDetail End--->"+(System.currentTimeMillis()-startTime));
	//		return al_list;
	//	}

	public static PaymentProcessBean getCCDetails(String assoccode,String transId,int type)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs;
		PaymentProcessBean proBean = new PaymentProcessBean();
		String Query="";
		if(type==1){
			Query=" AND CCD_TXN_NO='"+transId+"'";
		} else {
			Query=" AND CCD_TRIP_ID='"+transId+"'";
		}
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst = conn.prepareStatement("SELECT * FROM TDS_CREDIT_CARD_DETAIL WHERE CCD_ASSOC_CODE=?"+Query);
			m_pst.setString(1, assoccode);
			rs=m_pst.executeQuery();
			if(rs.next()){
				proBean.setB_trip_id(rs.getString("CCD_TRIP_ID"));
				proBean.setB_cap_trans(rs.getString("CCD_TXN_NO"));
				proBean.setB_card_full_no(rs.getString("CCD_CARD_NO"));
				proBean.setB_amount(rs.getString("CCD_AMOUNT"));
				proBean.setB_trans_id(rs.getString("CCD_TRANS_ID_1"));
				proBean.setB_cardHolderName(rs.getString("CCD_CARD_HOLDER_NAME"));
				proBean.setB_tip_amt(rs.getString("CCD_TIP_AMOUNT"));
				proBean.setB_approval_code(rs.getString("CCD_APPROVAL_CODE"));
			}
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
		return proBean;
	}


	public static PaymentProcessBean getCCDetailsToCustomer(ClientAuthenticationBO cmBo,String tripId)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getCCDetailsToCustomer Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs;
		PaymentProcessBean proBean =null;
		String Query="";

		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst = conn.prepareStatement("SELECT * FROM TDS_CREDIT_CARD_DETAIL WHERE CCD_ASSOC_CODE=? AND CCD_TRIP_ID=? ORDER BY CCD_TXN_NO DESC");
			m_pst.setString(1, cmBo.getAssoccode());
			m_pst.setString(2, tripId);
			rs=m_pst.executeQuery();
			if(rs.next()){
				if(!rs.getString("CCD_APPROVED_BY").equalsIgnoreCase(cmBo.getUserId())){
					proBean = new PaymentProcessBean();
					proBean.setB_trip_id(rs.getString("CCD_TRIP_ID"));
					proBean.setB_cap_trans(rs.getString("CCD_TXN_NO"));
					proBean.setB_card_full_no(rs.getString("CCD_CARD_NO"));
					proBean.setB_amount(rs.getString("CCD_AMOUNT"));
					proBean.setB_trans_id(rs.getString("CCD_TRANS_ID_1"));
					proBean.setB_cardHolderName(rs.getString("CCD_CARD_HOLDER_NAME"));
					proBean.setB_tip_amt(rs.getString("CCD_TIP_AMOUNT"));
					proBean.setB_approval_code(rs.getString("CCD_APPROVAL_CODE"));
					proBean.setAuthCapFlag(rs.getString("CCD_AUTHCAPFLG"));
				}
			}
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.getCCDetailsToCustomer-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getCCDetailsToCustomer-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getCCDetailsToCustomer End--->"+(System.currentTimeMillis()-startTime));
		return proBean;
	}


	public static PaymentProcessBean getCCStatusForTripId(AdminRegistrationBO adminBo,String tripId, String transid)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getCCStatusForTripId Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs;
		PaymentProcessBean proBean =null;
		String Query="";

		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst = conn.prepareStatement("SELECT * FROM TDS_CREDIT_CARD_DETAIL WHERE CCD_ASSOC_CODE=? AND CCD_TRANS_ID_1=? AND CCD_TRIP_ID=? ");
			m_pst.setString(1, adminBo.getMasterAssociateCode());
			m_pst.setString(2, transid);
			m_pst.setString(3, tripId);
			rs=m_pst.executeQuery();
			if(rs.next()){
				proBean = new PaymentProcessBean();
				proBean.setB_trip_id(rs.getString("CCD_TRIP_ID"));
				proBean.setB_cap_trans(rs.getString("CCD_TXN_NO"));
				proBean.setB_card_full_no(rs.getString("CCD_CARD_NO"));
				proBean.setB_amount(rs.getString("CCD_AMOUNT"));
				proBean.setB_trans_id(rs.getString("CCD_TRANS_ID_1"));
				proBean.setB_cardHolderName(rs.getString("CCD_CARD_HOLDER_NAME"));
				proBean.setB_tip_amt(rs.getString("CCD_TIP_AMOUNT"));
				proBean.setB_approval_code(rs.getString("CCD_APPROVAL_CODE"));
				proBean.setAuthCapFlag(rs.getString("CCD_AUTHCAPFLG"));
				proBean.setTotal(rs.getString("CCD_TOTAL_AMOUNT"));
			}
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.getCCStatusForTripId-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getCCStatusForTripId-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getCCStatusForTripId End--->"+(System.currentTimeMillis()-startTime));
		return proBean;
	}

	public static PaymentProcessBean getCCStatusForTripIdBeforeAlter(ClientAuthenticationBO clientBo,String tripId, String transid)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getCCStatusForTripId Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs;
		PaymentProcessBean proBean =null;
		String Query="";

		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst = conn.prepareStatement("SELECT * FROM TDS_CREDIT_CARD_DETAIL WHERE CCD_ASSOC_CODE=? AND CCD_TRANS_ID_1=? AND CCD_TRIP_ID=? ");
			m_pst.setString(1, clientBo.getAssoccode());
			m_pst.setString(2, transid);
			m_pst.setString(3, tripId);
			System.out.println("query"+m_pst.toString());
			rs=m_pst.executeQuery();
			if(rs.next()){
				proBean = new PaymentProcessBean();
				proBean.setB_trip_id(rs.getString("CCD_TRIP_ID"));
				proBean.setB_cap_trans(rs.getString("CCD_TXN_NO"));
				proBean.setB_card_full_no(rs.getString("CCD_CARD_NO"));
				proBean.setB_amount(rs.getString("CCD_AMOUNT"));
				proBean.setB_trans_id(rs.getString("CCD_TRANS_ID_1"));
				proBean.setB_cardHolderName(rs.getString("CCD_CARD_HOLDER_NAME"));
				proBean.setB_tip_amt(rs.getString("CCD_TIP_AMOUNT"));
				proBean.setB_approval_code(rs.getString("CCD_APPROVAL_CODE"));
				proBean.setAuthCapFlag(rs.getString("CCD_AUTHCAPFLG"));
				proBean.setTotal(rs.getString("CCD_TOTAL_AMOUNT"));
			}
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.getCCStatusForTripId-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getCCStatusForTripId-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getCCStatusForTripId End--->"+(System.currentTimeMillis()-startTime));
		return proBean;
	}


	public static void removeLease(String leaseID)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.removeLease Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst = conn.prepareStatement("DELETE from TDS_DRIVER_LEASE where DL_TXN_NO = ?");
			m_pst.setString(1, leaseID);
			m_pst.execute();
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.removeLease End--->"+(System.currentTimeMillis()-startTime));
	}

	public static void removeMisc(String miscID)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.removeMisc Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst = conn.prepareStatement("DELETE from TDS_DRIVER_MISC where DM_ID  = ?");
			m_pst.setString(1, miscID);
			m_pst.execute();
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.removeMisc End--->"+(System.currentTimeMillis()-startTime));
	}

	public static ArrayList<LeaseBean> getLeaseBean(String assocode,String driver)  
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getLeaseBean Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		ArrayList<LeaseBean> al_lease = new ArrayList<LeaseBean>();
		try {
			m_pst = conn.prepareStatement("select * from TDS_DRIVER_LEASE where DL_DRIVER_ID=? and DL_ASSOC_CODE=? AND DL_DRIVER_PMT_STATUS=0");
			m_pst.setString(1, driver);
			m_pst.setString(2, assocode);
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{
				LeaseBean leaseBean = new LeaseBean();
				leaseBean.setLeaseID(rs.getString("DL_TXN_NO"));
				leaseBean.setVechAmount(rs.getDouble("DL_AMOUNT"));
				leaseBean.setVechDesc(rs.getString("DL_VECHILE_DESC"));
				leaseBean.setVechileNo(rs.getString("DL_VECHILE_NO"));
				al_lease.add(leaseBean);
			}
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getLeaseBean End--->"+(System.currentTimeMillis()-startTime));
		return al_lease;
	}
	public static ArrayList<MiscBean> getMiscBean(String assocode,String driver,String masterAssociationCode,String payKey) 
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getMiscBean Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		ArrayList<MiscBean> al_misc = new ArrayList<MiscBean>();
		String query="";
		if(!payKey.equals("")){
			query="select * from TDS_DRIVER_MISC where DM_DRIVER_PMT_NUM='"+payKey+"' and (DM_ASSOCCODE=? OR DM_ASSOCCODE=?) AND DM_DRIVER_PMT_STATUS=1";
		} else {
			query="select * from TDS_DRIVER_MISC where DM_DRIVER_ID='"+driver+"' and (DM_ASSOCCODE=? OR DM_ASSOCCODE=?) AND DM_DRIVER_PMT_STATUS=0";
		}
		try {
			m_pst = conn.prepareStatement(query);
			m_pst.setString(1, assocode);
			m_pst.setString(2, masterAssociationCode);
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{
				MiscBean miscBean = new MiscBean();
				miscBean.setMiscid(rs.getString("DM_ID"));
				miscBean.setMiscAmount(rs.getDouble("DM_MISC_AMOUNT"));
				miscBean.setMiscDesc(rs.getString("DM_MISC_DESC"));
				miscBean.setCustomField(rs.getString("DM_CUSTOM_FIELD"));
				al_misc.add(miscBean);
			}
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getMiscBean End--->"+(System.currentTimeMillis()-startTime));
		return al_misc;
	}
	public static void storeLeaseBean(LeaseBean leaseBean) throws SQLException
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.storeLeaseBean Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst = conn.prepareStatement("insert into TDS_DRIVER_LEASE (DL_DRIVER_ID,DL_ASSOC_CODE,DL_AMOUNT,DL_PROCESSING_DATE_TIME,DL_VECHILE_NO,DL_VECHILE_DESC,DL_OPRID,DL_DRIVER_PMT_STATUS)values(?,?,?,?,?,?,?,?)");
			m_pst.setString(1, leaseBean.getDriverid());
			m_pst.setString(2, leaseBean.getAssocode());
			m_pst.setDouble(3, leaseBean.getVechAmount());
			m_pst.setDate(4, leaseBean.getProcessDate());
			m_pst.setString(5, leaseBean.getVechileNo());
			m_pst.setString(6, leaseBean.getVechDesc());
			m_pst.setString(7, leaseBean.getOprid());
			m_pst.setInt(8, 0);
			m_pst.execute();	
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			sqex.printStackTrace();
			throw sqex;
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.storeLeaseBean End--->"+(System.currentTimeMillis()-startTime));
	}
	public static void storeMiscBean(MiscBean miscBean) throws SQLException
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.storeMiscBean Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("insert into TDS_DRIVER_MISC (DM_ASSOCCODE,DM_DRIVER_ID,DM_MISC_DESC,DM_MISC_AMOUNT,DM_PROCESSING_TIME,DM_DRIVER_PMT_STATUS,DM_CUSTOM_FIELD)values(?,?,?,?,?,0,?)");
			pst.setString(1, miscBean.getAssocode());
			pst.setString(2, miscBean.getDriverid());
			pst.setString(3, miscBean.getMiscDesc());
			pst.setDouble(4, miscBean.getMiscAmount());
			pst.setDate(5,  miscBean.getProcessDate());
			pst.setString(6, miscBean.getCustomField());
			pst.execute();
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.storeMiscBean-->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.printStackTrace();
			throw sqex;
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.storeMiscBean End--->"+(System.currentTimeMillis()-startTime));
	}
	public static ArrayList<String> getVechicalType(String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getVechicalType Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ArrayList<String> al_Vech = new ArrayList<String>();
		double amount=0.00;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("SELECT * FROM TDS_LEASE_RATES WHERE LR_ASSOCCODE='"+assocode+"' ");
			ResultSet rs = pst.executeQuery();
			while(rs.next())
			{
				al_Vech.add(rs.getString("LR_VECHICAL_DESC"));
			}
		}catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.getChargeTypes-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getChargeTypes-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getVechicalType End--->"+(System.currentTimeMillis()-startTime));
		return al_Vech;
	}

	public static String getLeaseAmount(String assocode,String VechType)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getLeaseAmount Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		double amount=0.00;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("SELECT LR_LEASE_AMOUNT FROM TDS_LEASE_RATES WHERE LR_ASSOCCODE='"+assocode+"' and LR_VECHICAL_DESC='"+VechType+"' ");
			ResultSet rs = pst.executeQuery();
			if(rs.next())
			{
				amount = rs.getDouble("LR_LEASE_AMOUNT");
			}
		}catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.getChargeTypes-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getChargeTypes-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getLeaseAmount End--->"+(System.currentTimeMillis()-startTime));
		return new BigDecimal(amount).setScale(2,5).toString();
	}

	public static ArrayList<ChargesBO> getChargeTypes(String assoccode,int type){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getChargeTypes Start--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<ChargesBO> chargeTypes = new ArrayList<ChargesBO>();
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		String query="";
		if(type!=0){
			query = " AND CT_KEY='"+type+"'";
		}
		try {
			pst = conn.prepareStatement("SELECT * FROM TDS_CHARGE_TYPE WHERE CT_ASSOCIATION_CODE='"+assoccode+"'"+query+" ORDER BY CAST(CT_KEY as SIGNED INTEGER)");
			ResultSet rs = pst.executeQuery();
			while(rs.next())
			{
				ChargesBO charBO = new ChargesBO();
				charBO.setPayTypeKey(rs.getInt("CT_KEY"));
				charBO.setPayTypeDesc(rs.getString("CT_DESCRIPTION"));
				charBO.setPayTypeAmount(rs.getString("CT_AMOUNT"));
				charBO.setDynamicFormula(rs.getString("CT_FORMULA"));
				chargeTypes.add(charBO);
			}
		}
		catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.getChargeTypes-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getChargeTypes-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getChargeTypes End--->"+(System.currentTimeMillis()-startTime));
		return chargeTypes;
	}
	public static int insertChargeTypes(String assoccode,String[] amount,String[] key,String[] description,String[] formula,int size) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertChargeTypes Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			for(int i=1;i<=size;i++){
				pst= con.prepareStatement("INSERT INTO TDS_CHARGE_TYPE(CT_ASSOCIATION_CODE,CT_KEY,CT_DESCRIPTION,CT_AMOUNT,CT_CREATED_DATE,CT_FORMULA)VALUES(?,?,?,?,NOW(),?)");
				pst.setString(1, assoccode);
				pst.setString(2,key[i]);
				pst.setString(3,description[i]);
				pst.setString(4,amount[i]);
				pst.setString(5,formula[i]);
				result=pst.executeUpdate();
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.insertChargeTypes--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.insertChargeTypes--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertChargeTypes End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int deleteChargeTypes(String assoccode,String key) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.deleteChargeTypes Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("DELETE FROM TDS_CHARGE_TYPE WHERE CT_ASSOCIATION_CODE='"+assoccode+"' AND CT_KEY='"+key+"'");
			pst.execute();
			result=1;
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.deleteChargeTypes--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.deleteChargeTypes--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.deleteChargeTypes End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int insertChargeTemplate(String assoccode,String[] amountType,String[] key,String[] description,int size) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertChargeTemplate Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM TDS_CHARGE_TEMPLATE_MASTER  WHERE CTM_ASSOCIATION_CODE='"+assoccode+"'");
			pst.execute();
			for(int i=1;i<=size;i++){
				pst= con.prepareStatement("INSERT INTO TDS_CHARGE_TEMPLATE_MASTER (CTM_ASSOCIATION_CODE,CTM_KEY,CTM_PROGRAM_NAME,CTM_PAYMENT_TYPE,CTM_CREATED_DATE)VALUES(?,?,?,?,NOW())");
				pst.setString(1, assoccode);
				pst.setString(2,key[i]);
				pst.setString(3,description[i]);
				pst.setString(4,amountType[i]);
				result=pst.executeUpdate();
			}
			con.commit();
			con.setAutoCommit(true);
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.insertChargeTemplate--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.insertChargeTemplate--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertChargeTemplate End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static ArrayList<ChargesBO> getChargeTemplateDetail(String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getChargeTemplateDetail Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ArrayList<ChargesBO> chargeTemplateDetail = new ArrayList<ChargesBO>();
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("SELECT * FROM TDS_CHARGE_TEMPLATE_DETAIL WHERE CTD_ASSOCIATION_CODE='"+assoccode+"'");
			ResultSet rs = pst.executeQuery();
			while(rs.next())
			{
				ChargesBO charBO = new ChargesBO();
				charBO.setKey(rs.getInt("CTD_KEY"));
				charBO.setPayTemplateKey(rs.getInt("CTD_MASTER_CODE"));
				charBO.setPayTypeKey(rs.getInt("CTD_TYPE_CODE"));
				charBO.setAmount(rs.getString("CTD_AMOUNT"));
				charBO.setPayTemplateAmountType(rs.getInt("CTD_PAYMENT_SWITCH"));
				charBO.setTax1(rs.getString("CTD_TAX1"));
				charBO.setTax2(rs.getString("CTD_TAX2"));
				charBO.setTax3(rs.getString("CTD_TAX3"));

				chargeTemplateDetail.add(charBO);
			}
		}
		catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.getChargeTemplateDetail-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getChargeTemplateDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getChargeTemplateDetail End--->"+(System.currentTimeMillis()-startTime));
		return chargeTemplateDetail;
	}
	public static int insertChargeTemplateDetail(String assoccode,String[] amountType,String[] key,String[] amount,String[] templateKey,String[] typeKey,int size,String[] templateTax1,String[] templateTax2,String[] templateTax3) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertChargeTemplateDetail Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM TDS_CHARGE_TEMPLATE_DETAIL  WHERE CTD_ASSOCIATION_CODE='"+assoccode+"'");
			pst.execute();
			for(int i=1;i<=size;i++){
				pst= con.prepareStatement("INSERT INTO TDS_CHARGE_TEMPLATE_DETAIL (CTD_ASSOCIATION_CODE,CTD_KEY,CTD_PAYMENT_SWITCH,CTD_AMOUNT,CTD_MASTER_CODE,CTD_TYPE_CODE,CTD_TAX1,CTD_TAX2,CTD_TAX3)VALUES(?,?,?,?,?,?,?,?,?)");
				pst.setString(1, assoccode);
				pst.setString(2,key[i]);
				pst.setString(3,amountType[i]);
				pst.setString(4,amount[i]);
				pst.setString(5,templateKey[i]);
				pst.setString(6,typeKey[i]);

				pst.setString(7, templateTax1[i]);
				pst.setString(8, templateTax2[i]);
				pst.setString(9, templateTax3[i]);

				result=pst.executeUpdate();
			}
			con.commit();
			con.setAutoCommit(true);
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.insertChargeTemplateDetail--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.insertChargeTemplateDetail--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertChargeTemplateDetail End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int insertDriverCharge(String assoccode,String[] amount,String[] key,int tripId,int size,String driverId,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertDriverCharge Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM TDS_DRIVER_CHARGE_CALC  WHERE DC_ASSOC_CODE='"+assoccode+"' AND DC_TRIP_ID='"+tripId+"' AND DC_MASTER_ASSOCCODE='"+masterAssoccode+"'");
			pst.execute();
			for(int i=1;i<=size;i++){
				pst= con.prepareStatement("INSERT INTO TDS_DRIVER_CHARGE_CALC (DC_ASSOC_CODE,DC_TRIP_ID,DC_TYPE_CODE,DC_AMOUNT,DC_PROCESSING_DATE_TIME,DC_DRIVER_ID,DC_MASTER_ASSOCCODE)VALUES(?,?,?,?,NOW(),?,?)");
				pst.setString(1, assoccode);
				pst.setInt(2,tripId);
				pst.setString(3,key[i]);
				pst.setString(4,amount[i]);
				pst.setString(5,driverId);
				pst.setString(6, masterAssoccode);
				pst.executeUpdate();
			}
			con.commit();
			result=1;
			con.setAutoCommit(true);
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.insertDriverCharge--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.insertDriverCharge--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertDriverCharge End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int insertDriverChargeSToMobile(String assoccode,String[] amount,String[] key,String tripId,int size,String driverId,String tipCheck,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertDriverChargeSToMobile Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			con.setAutoCommit(false);
			if(tipCheck.equalsIgnoreCase("no")){
				pst=con.prepareStatement("DELETE FROM TDS_DRIVER_CHARGE_CALC  WHERE DC_TRIP_ID='"+tripId+"' AND (DC_ASSOC_CODE='"+assoccode+"' OR DC_MASTER_ASSOCCODE='"+masterAssoccode+"')");
				pst.execute();
			}
			for(int i=0;i<size;i++){
				pst= con.prepareStatement("INSERT INTO TDS_DRIVER_CHARGE_CALC (DC_ASSOC_CODE,DC_TRIP_ID,DC_TYPE_CODE,DC_AMOUNT,DC_PROCESSING_DATE_TIME,DC_DRIVER_ID,DC_MASTER_ASSOCCODE)VALUES(?,?,?,?,NOW(),?,?)");
				pst.setString(1, assoccode);
				pst.setString(2,tripId);
				pst.setString(3,key[i]);
				pst.setString(4,amount[i]);
				pst.setString(5,driverId);
				pst.setString(6, masterAssoccode);
				pst.executeUpdate();
			}
			con.commit();
			result=1;
			con.setAutoCommit(true);
		}catch ( SQLException sqex) {
			System.out.println("Amt--->"+amount.length+",Key--->"+key.length);
			System.out.println("TDSException ChargesDAO.insertDriverChargeSToMobile--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.insertDriverChargeSToMobile--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertDriverChargeSToMobile End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<ChargesBO> getChargeTemplate(String assoccode,int TDS_CHARGE_TYPE){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getChargeTemplate Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null; 
		ArrayList<ChargesBO> chargeTemplate = new ArrayList<ChargesBO>();
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			String query = "SELECT * FROM TDS_CHARGE_TEMPLATE_MASTER WHERE CTM_ASSOCIATION_CODE='"+assoccode+"'";
			if(TDS_CHARGE_TYPE==0){
				query = query + "and CTM_PAYMENT_TYPE='"+IChargeConstants.PAYMENT_TYPE_CHARGE+"'";
			} else if(TDS_CHARGE_TYPE==1) {
				query = query + "and CTM_PAYMENT_TYPE='"+IChargeConstants.PAYMENT_TYPE_CC+"'";
			}
			pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			while(rs.next())
			{
				ChargesBO charBO = new ChargesBO();
				charBO.setPayTemplateKey(rs.getInt("CTM_KEY"));
				charBO.setPayTemplateDesc(rs.getString("CTM_PROGRAM_NAME"));
				charBO.setPayTemplateAmountType(rs.getInt("CTM_PAYMENT_TYPE"));
				chargeTemplate.add(charBO);
			}
		}
		catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.getChargeTemplate-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getChargeTemplate End--->"+(System.currentTimeMillis()-startTime));
		return chargeTemplate;
	}

	//	public static void fillDisbursmentData(ResultSet rs, DisbursementDetaillBean detaillBean) throws SQLException
	//	{
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
	//		detaillBean.setCTD_AMOUNT(rs.getBigDecimal("CTD_AMOUNT"));
	//		detaillBean.setCTD_KEY(rs.getString("CTD_KEY"));
	//		detaillBean.setCTD_PAYMENT_SWITCH(rs.getInt("CTD_PAYMENT_SWITCH"));
	//		detaillBean.setCTD_TYPE_CODE(rs.getInt("CTD_TYPE_CODE"));
	//		detaillBean.setCTM_PROGRAM_NAME(rs.getString("CTM_PROGRAM_NAME"));
	//		detaillBean.setCTM_PROGRAM_ID(rs.getString("CTM_KEY"));
	//		detaillBean.setCT_DESCRIPRION(rs.getString("CT_DESCRIPTION"));
	//		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
	//	}

	public static String getTemplateForDriver(String assocode,String driverID)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getTemplateForDriver Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try{
			m_pst = m_conn.prepareStatement("select DR_PROGRAM_TMPLT from TDS_DRIVER where DR_SNO='"+driverID+"' and DR_ASSOCODE='"+assocode+"'");
			ResultSet rs = m_pst.executeQuery();
			if(rs.next()){
				return rs.getString("DR_PROGRAM_TMPLT");
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getTemplateForDriver End--->"+(System.currentTimeMillis()-startTime));
		return null;
	}

	//	public static void fillNestedDriverCharges(ResultSet rs,DriverChargesBean chargesBean,int type) throws SQLException
	//	{
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
	//		if(type == IChargeConstants.PAYMENT_TYPE_CHARGE){
	//			chargesBean.setTXNO(rs.getString("DC_TXN_NO"));
	//			chargesBean.setDC_TYPE_CODE(rs.getInt("DC_TYPE_CODE"));
	//			chargesBean.setTotalamount(rs.getBigDecimal("DC_AMOUNT"));
	//			chargesBean.setPaymentMode("Charges");
	//		} else {
	//			chargesBean.setDC_TYPE_CODE(rs.getInt("DC_TYPE_CODE"));
	//			chargesBean.setTXNO(rs.getString("TX_NO"));
	//			chargesBean.setVcardno(rs.getString("vcardno"));
	//			chargesBean.setIssuer(rs.getString("issuer"));
	//			chargesBean.setAmount(rs.getBigDecimal("amount"));
	//			chargesBean.setTip(rs.getBigDecimal("tip"));
	//			chargesBean.setTotalamount(rs.getBigDecimal("total"));
	//			chargesBean.setPaymentMode(rs.getString("Mode"));
	//		}
	//		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
	//	}

	//	public static void fillNestedOpenrequest(String tripId, OpenRequestBO openRequestBO) throws SQLException
	//	{
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
	//		TDSConnection m_dbConn = null;
	//		Connection m_conn = null;
	//		PreparedStatement m_pst = null;
	//		m_dbConn = new TDSConnection();
	//		m_conn = m_dbConn.getConnection();
	//		String Curr_DC_TRIP_ID="",PRV_DC_TRIP_ID="";
	//		int i=0;
	//		try {
	//			m_pst = m_conn.prepareStatement("select * from TDS_OPENREQUEST_HISTORY where ORH_TRIP_ID='"+tripId+"' ");
	//			ResultSet rs = m_pst.executeQuery();
	//			if(rs.next())
	//			{
	//				openRequestBO.setTripid(rs.getString("ORH_TRIP_ID"));
	//				openRequestBO.setPhone(rs.getString("ORH_PHONE"));
	//				openRequestBO.setSadd1(rs.getString("ORH_STADD1") != null?rs.getString("ORH_STADD1"):"");
	//				openRequestBO.setSadd2(rs.getString("ORH_STADD2") != null?rs.getString("ORH_STADD2"):"");
	//				openRequestBO.setScity(rs.getString("ORH_STCITY") != null?rs.getString("ORH_STCITY"):"");
	//				openRequestBO.setSstate(rs.getString("ORH_STSTATE")!= null?rs.getString("ORH_STSTATE"):"");
	//				openRequestBO.setSzip(rs.getString("ORH_STZIP")!= null?rs.getString("ORH_STZIP"):"");
	//				openRequestBO.setEadd1(rs.getString("ORH_EDADD1")!= null?rs.getString("ORH_EDADD1"):"");
	//				openRequestBO.setEadd2(rs.getString("ORH_EDADD2") != null?rs.getString("ORH_EDADD2"):"");
	//				openRequestBO.setEcity(rs.getString("ORH_EDCITY") != null?rs.getString("ORH_EDCITY"):"");
	//				openRequestBO.setEstate(rs.getString("ORH_EDSTATE") != null?rs.getString("ORH_EDSTATE"):"");
	//				openRequestBO.setEzip(rs.getString("ORH_EDZIP") != null?rs.getString("ORH_EDZIP"):"");
	//				/*				openRequestBO.setSaircode(rs.getString("ORH_SAIRCODE") != null?rs.getString("ORH_SAIRCODE"):"");
	//				openRequestBO.setEaircode(rs.getString("ORH_EAIRCODE") != null?rs.getString("ORH_EAIRCODE"):"");
	//				 */				openRequestBO.setAssociateCode(rs.getString("ORH_ASSOCCODE")!= null ? rs.getString("ORH_ASSOCCODE"):"");
	//				 /* openRequestBO.setSdate(rs.getString("ORH_SERVICEDATE_OFFSET"));
	//				 openRequestBO.setRequestTime(rs.getString("ORH_ENTEREDTIME"));
	//				  */ 
	//
	//				 openRequestBO.setQueueno(rs.getString("ORH_QUEUENO"));
	//				 openRequestBO.setDriverid(rs.getString("ORH_DRIVERID")); 
	//				 openRequestBO.setElandmark(rs.getString("ORH_ELANDMARK")); 
	//				 /*openRequestBO.setSlat(rs.getString("ORH_STLATITUDE"));
	//				 openRequestBO.setEdlatitude(rs.getString("ORH_EDLATITUDE"));
	//				 openRequestBO.setSlong(rs.getString("ORH_STLONGITUDE"));
	//				 openRequestBO.setEdlongitude(rs.getString("ORH_EDLONGITUDE"));
	//				 openRequestBO.setSlandmark(rs.getString("ORH_LANDMARK"));
	//				 openRequestBO.setSintersection(rs.getString("ORH_INTERSECTION"));
	//				 openRequestBO.setStartTimeStamp(rs.getString("ORH_SERVICEDATE_OFFSET"));*/
	//				 openRequestBO.setPaytype((rs.getString("ORH_PAYTYPE")));
	//				 openRequestBO.setAcct(rs.getString("ORH_PAYACC"));
	//				 openRequestBO.setAmt(new BigDecimal(rs.getString("ORH_AMT")));
	//				 String reqTime = rs.getString("ORH_SERVICETIME");
	//				 openRequestBO.setShrs(reqTime.substring(0,2));
	//				 openRequestBO.setSmin(reqTime.substring(2,4));
	//				 openRequestBO.setIsBeandata(1);
	//				 openRequestBO.setName(rs.getString("ORH_NAME"));
	//				 openRequestBO.setSpecialIns(rs.getString("ORH_SPLINS"));
	//				 openRequestBO.setTripStatus(rs.getString("ORH_TRIP_STATUS"));
	//				 openRequestBO.setRouteNumber(rs.getString("ORH_ROUTE_NUMBER"));
	//			}
	//		}catch (Exception sqex)
	//		{
	//			cat.error("ChargesDAO.getDriverDisbrusment -->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//		} finally {
	//			m_dbConn.closeConnection();
	//		}
	//		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
	//	}

	public static void storeDisbursementRecord(DisbursementRecordMaster recordMaster,AdminRegistrationBO adminBO) throws SQLException
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.storeDisbursementRecord Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst,m_pst1 = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		m_conn.setAutoCommit(false);
		int returnKey=0;
		try{
			m_pst = m_conn.prepareStatement("insert into tds.TDS_DRIVER_Disbursement (DD_DRIVERID,DD_OPER_ID,DD_STATUS,DD_EFFECTIVE_DATE_TIME,DD_AMOUNT,DD_PAY_SOURCE,DD_PROCESSING_TIME,DD_ASSOCCODE)values(?,?,?,?,?,?,NOW(),?)",Statement.RETURN_GENERATED_KEYS);
			m_pst.setString(1, recordMaster.getDriverid());
			m_pst.setString(2, recordMaster.getOperatorid());
			m_pst.setInt(3, 1);
			m_pst.setDate(4, recordMaster.getProcessDate());
			m_pst.setDouble(5, recordMaster.getGrantamount());
			m_pst.setString(6, recordMaster.getPaymentToType());
			m_pst.setString(7, adminBO.getAssociateCode());
			int result = m_pst.executeUpdate();
			ResultSet rs = m_pst.getGeneratedKeys();
			if (rs.next()) {
				returnKey = rs.getInt(1);
			}
			if(result > 0){
				ArrayList<DisbursementRecordDetail> recordDetails = recordMaster.getRecordDetails();
				String prevTrip="";
				StringBuffer tripIDs=new StringBuffer();
				if(recordDetails.size()>0){
					for(int i=0;i<recordDetails.size()-1;i++){
						if(!recordDetails.get(i).getTripID().equals(prevTrip)){
							tripIDs.append("'"+recordDetails.get(i).getTripID()+"',");
							prevTrip=recordDetails.get(i).getTripID();
						}
					}
					tripIDs.append("'"+recordDetails.get(recordDetails.size()-1).getTripID()+"'");
				}else{
					tripIDs.append("''");
				}
				if(recordMaster.getChargeCalcType() == IChargeConstants.PAYMENT_TYPE_CHARGE) {	
					m_pst = m_conn.prepareStatement("Update TDS_DRIVER_CHARGE_CALC SET DC_DRIVER_PMT_AMT=?,DC_DRIVER_PMT_STATUS=1,DC_DRIVER_PMT_NUM=? where DC_TRIP_ID=? and DC_TXN_NO=? and (DC_ASSOC_CODE=? OR DC_MASTER_ASSOCCODE=?)");
					for(int i =0;i<recordDetails.size();i++)
					{
						DisbursementRecordDetail recordDetail = recordDetails.get(i);
						m_pst.setDouble(1, recordDetail.getDriverPayAmount());
						m_pst.setInt(2, returnKey);
						m_pst.setString(3, recordDetail.getTripID());
						m_pst.setString(4, recordDetail.getTxNo());
						m_pst.setString(5,adminBO.getAssociateCode());
						m_pst.setString(6, adminBO.getMasterAssociateCode());
						m_pst.execute();
					}
					if(recordDetails.size()>0){
						m_pst1 = m_conn.prepareStatement("Update TDS_DRIVER_CHARGE_CALC SET DC_DRIVER_PMT_NUM=?,DC_DRIVER_PMT_STATUS=1 where DC_TRIP_ID IN ("+tripIDs+") and (DC_ASSOC_CODE=? OR DC_MASTER_ASSOCCODE=?)");
						m_pst1.setInt(1, returnKey);
						m_pst1.setString(2, adminBO.getAssociateCode());
						m_pst1.setString(3, adminBO.getMasterAssociateCode());
						m_pst1.executeUpdate();
						m_pst1 = m_conn.prepareStatement("Update TDS_CREDIT_CARD_DETAIL SET CCD_DRIVER_PMT_STATUS=2,CCD_DRIVER_PMT_NUM=? where CCD_TRIP_ID IN ("+tripIDs+") and (CCD_ASSOC_CODE=? OR CCD_ASSOC_CODE=?)");
						m_pst1.setInt(1, returnKey);
						m_pst1.setString(2, adminBO.getAssociateCode());
						m_pst1.setString(3,adminBO.getMasterAssociateCode());
						m_pst1.executeUpdate();
						m_pst1 = m_conn.prepareStatement("Update TDS_VOUCHER_DETAIL SET VD_DRIVER_PMT_STATUS=2,VD_DRIVER_PMT_NUM=? where VD_TRIP_ID IN ("+tripIDs+") AND (VD_ASSOC_CODE=? OR VD_ASSOC_CODE=?)");
						m_pst1.setInt(1, returnKey);
						m_pst1.setString(2, adminBO.getAssociateCode());
						m_pst1.setString(3, adminBO.getMasterAssociateCode());
						m_pst1.executeUpdate();
					}
				} else {
					for(int i =0;i<recordDetails.size();i++)
					{
						DisbursementRecordDetail recordDetail = recordDetails.get(i);
						if(ITransactionType.CCARD == Integer.parseInt(recordDetail.getCcVoucherFlag()))
						{
							m_pst = m_conn.prepareStatement("Update TDS_CREDIT_CARD_DETAIL SET CCD_DRIVER_PMT_AMT=?,CCD_DRIVER_PMT_STATUS=1,CCD_DRIVER_PMT_NUM=? where CCD_TRIP_ID=? and CCD_TXN_NO=? and (CCD_ASSOC_CODE=? OR CCD_ASSOC_CODE=?)");
						} else {
							m_pst = m_conn.prepareStatement("Update TDS_VOUCHER_DETAIL SET VD_DRIVER_PMT_AMT=?,VD_DRIVER_PMT_STATUS=1,VD_DRIVER_PMT_NUM=? where VD_TRIP_ID=? and VD_TXN_ID=? and (VD_ASSOC_CODE=? OR VD_ASSOC_CODE=?)");
						}
						m_pst.setDouble(1, recordDetail.getDriverPayAmount());
						m_pst.setInt(2, returnKey);
						m_pst.setString(3, recordDetail.getTripID());
						m_pst.setString(4, recordDetail.getTxNo());
						m_pst.setString(5, adminBO.getAssociateCode());
						m_pst.setString(6,adminBO.getMasterAssociateCode());
						m_pst.execute();
					}
					if(recordDetails.size()>0){
						m_pst1 = m_conn.prepareStatement("Update TDS_DRIVER_CHARGE_CALC SET DC_DRIVER_PMT_NUM=?,DC_DRIVER_PMT_STATUS=2  where DC_TRIP_ID IN ("+tripIDs+") and (DC_ASSOC_CODE=? OR DC_MASTER_ASSOCCODE=?)");
						m_pst1.setInt(1, returnKey);
						m_pst1.setString(2, adminBO.getAssociateCode());
						m_pst1.setString(3, adminBO.getMasterAssociateCode());
						m_pst1.executeUpdate();
					}
				}
				if(recordDetails.size()>0){
					m_pst1 = m_conn.prepareStatement("Update TDS_OPENREQUEST_HISTORY SET ORH_PMT_NUM=? where ORH_TRIP_ID IN ("+tripIDs+") and ORH_MASTER_ASSOCCODE=?");
					m_pst1.setInt(1, returnKey);
					m_pst1.setString(2, adminBO.getMasterAssociateCode());
					m_pst1.executeUpdate();
				}
				updateLeaseRecord(m_conn, m_pst, recordMaster, returnKey);
				updateMiscRecord(m_conn, m_pst, recordMaster, returnKey);
				createBalanceRecord(m_conn, m_pst, recordMaster, returnKey,adminBO);
				m_conn.commit();
				m_conn.setAutoCommit(true);
			}
		} catch (SQLException e) {
			m_conn.rollback();
			throw e;
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.storeDisbursementRecord End--->"+(System.currentTimeMillis()-startTime));
	}

	private static void updateLeaseRecord(Connection m_conn,PreparedStatement m_pst,DisbursementRecordMaster recordMaster, int returnKey) throws SQLException
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.updateLeaseRecord Start--->"+startTime);
		m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER_LEASE SET DL_DRIVER_PMT_STATUS =1 ,DL_DRIVER_PMT_NUM = "+returnKey+" WHERE DL_DRIVER_PMT_STATUS=0 and DL_DRIVER_ID='"+recordMaster.getDriverid()+"'");
		m_pst.execute();
		cat.info("ChargesDAO.updateLeaseRecord End--->"+(System.currentTimeMillis()-startTime));
	}
	private static void updateMiscRecord(Connection m_conn,PreparedStatement m_pst,DisbursementRecordMaster recordMaster, int returnKey) throws SQLException
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.updateMiscRecord Start--->"+startTime);
		m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER_MISC SET DM_DRIVER_PMT_STATUS =1 ,DM_DRIVER_PMT_NUM = "+returnKey+" WHERE DM_DRIVER_PMT_STATUS=0 and DM_DRIVER_ID='"+recordMaster.getDriverid()+"'");
		m_pst.execute();
		cat.info("ChargesDAO.updateMiscRecord End--->"+(System.currentTimeMillis()-startTime));
	}

	private static void createBalanceRecord(Connection m_conn,PreparedStatement m_pst,DisbursementRecordMaster recordMaster, int returnKey,AdminRegistrationBO adminBO) throws SQLException
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.createBalanceRecord Start--->"+startTime);
		m_pst = m_conn.prepareStatement("update TDS_BALANCE set BL_STATUS = 1,BL_CARRY_OVER_TO="+returnKey+" where BL_DRIVERID = '"+recordMaster.getDriverid()+"' and BL_STATUS = 0 and BL_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
		m_pst.executeUpdate();
		m_pst = m_conn.prepareStatement("insert into tds.TDS_BALANCE (BL_DISB_ID,BL_DRIVERID,BL_OPER_ID,BL_STATUS,BL_PROCESSING_TIME,BL_AMOUNT,BL_PAY_SOURCE,BL_ASSOCCODE)values(?,?,?,?,?,?,?,?)");
		m_pst.setInt(1, returnKey);
		m_pst.setString(2, recordMaster.getDriverid());
		m_pst.setString(3, recordMaster.getOperatorid());
		m_pst.setInt(4, 0);
		m_pst.setDate(5, recordMaster.getProcessDate());
		m_pst.setDouble(6, recordMaster.getClosingBalance());
		m_pst.setString(7, recordMaster.getPaymentToType());
		m_pst.setString(8, adminBO.getAssociateCode());
		m_pst.executeUpdate();
		cat.info("ChargesDAO.createBalanceRecord End--->"+(System.currentTimeMillis()-startTime));
	}
	public static ArrayList<ChargesBO> getORCharges(String assoccode,String tripId, String masterAssoccode){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getORCharges Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ArrayList<ChargesBO> chargeForTrip = new ArrayList<ChargesBO>();
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("SELECT DC_TYPE_CODE,DC_AMOUNT FROM TDS_DRIVER_CHARGE_CALC WHERE DC_ASSOC_CODE='"+assoccode+"' AND DC_TRIP_ID='"+tripId+"' AND DC_MASTER_ASSOCCODE='"+masterAssoccode+"'");
			ResultSet rs = pst.executeQuery();
			while(rs.next())
			{
				ChargesBO charBO = new ChargesBO();
				charBO.setPayTypeKey(rs.getInt("DC_TYPE_CODE"));
				charBO.setAmount(rs.getString("DC_AMOUNT"));
				chargeForTrip.add(charBO);
			}
		}
		catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.getORCharges-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getORCharges-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getORCharges End--->"+(System.currentTimeMillis()-startTime));
		return chargeForTrip;
	}

	public static int insertPaymentIntoCC(PaymentProcessBean processBean,String path,ApplicationPoolBO poolBO, AdminRegistrationBO adminBO,int authStatus, int approvedBy) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertPaymentIntoCC Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		ResultSet rs;
		int txnNum=0;
		try {
			if(authStatus==1){
				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1,CCD_INVOICE_UPDATE,CCD_APPROVED_SOURCE) values" +
						"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			} else {
				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1,CCD_INVOICE_UPDATE,CCD_CAPTUREDATE,CCD_APPROVED_SOURCE) values" +
						"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?,?,NOW(),?)",Statement.RETURN_GENERATED_KEYS);

			}
			m_pst.setString(1, processBean.getB_driverid());
			m_pst.setString(2, processBean.getB_trip_id());
			m_pst.setString(3, processBean.getB_card_issuer());
			m_pst.setString(4, processBean.getB_card_type());
			m_pst.setString(5, processBean.getB_paymentType());
			m_pst.setString(6, processBean.getB_card_no());
			if(processBean.getB_approval_status().equals("1")){
				m_pst.setDouble(7, Double.parseDouble(processBean.getB_amount()!=null&&!processBean.getB_amount().equals("")?processBean.getB_amount():"0.00"));
				m_pst.setDouble(8, Double.parseDouble(processBean.getB_tip_amt()!=null&&!processBean.getB_tip_amt().equals("")?processBean.getB_tip_amt():"0.00"));
				m_pst.setDouble(9, Double.parseDouble(processBean.getB_amount()!=null&&!processBean.getB_amount().equals("")?processBean.getB_amount():"0.00"));
			} else {
				//Declained made zero
				m_pst.setDouble(7, 0.0);
				m_pst.setDouble(8, 0.0);
				m_pst.setDouble(9, 0.0);
			}
			m_pst.setString(10, processBean.getB_trans_id());
			m_pst.setString(11, processBean.getB_trans_id2());
			System.out.println("Approval Status -->" + processBean.getB_approval_status());
			m_pst.setInt(12, Integer.parseInt(processBean.getB_approval_status()));
			m_pst.setString(13, processBean.getB_approval_code());			 
			m_pst.setBytes(14, TDSValidation.getDefaultsign(path));
			m_pst.setString(15, adminBO.getAssociateCode());
			m_pst.setString(16, processBean.getB_cardHolderName());
			m_pst.setString(17, adminBO.getUid());
			m_pst.setInt(18, authStatus);
			m_pst.setString(19, (authStatus == ICCConstant.SETTLED?processBean.getB_trans_id():""));
			m_pst.setInt(20, processBean.getCc_Invoice());
			m_pst.setInt(21, approvedBy);
			System.out.println("Insert CC: "+m_pst.toString());
			m_pst.execute();
			rs=m_pst.getGeneratedKeys();
			if(rs.next()){
				txnNum = rs.getInt(1);
			}
		} catch (SQLException sqex) {
			try {
				//				if(m_conn!=null){
				//					m_conn.close();
				//				}

				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1,CCD_INVOICE_UPDATE,CCD_APPROVED_SOURCE) values" +
						"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?,?)");
				m_pst.setString(1, processBean.getB_driverid());
				m_pst.setString(2, processBean.getB_trip_id());
				m_pst.setString(3, processBean.getB_card_issuer());
				m_pst.setString(4, processBean.getB_card_type());
				m_pst.setString(5, processBean.getB_paymentType());
				m_pst.setString(6, processBean.getB_card_no());
				if(processBean.getB_approval_status().equals("1")){
					m_pst.setDouble(7, Double.parseDouble(processBean.getB_amount()));
					m_pst.setDouble(8, Double.parseDouble(processBean.getB_tip_amt()));
					m_pst.setDouble(9, Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt())  );
				} else {
					m_pst.setDouble(7, 0.0);
					m_pst.setDouble(8, 0.0);
					m_pst.setDouble(9, 0.0);
				}
				m_pst.setString(10, processBean.getB_trans_id());
				m_pst.setString(11, processBean.getB_trans_id2());
				m_pst.setInt(12, Integer.parseInt(processBean.getB_approval_status()));
				m_pst.setString(13, processBean.getB_approval_code());			 
				m_pst.setBytes(14, TDSValidation.getDefaultsign(path));
				m_pst.setString(15, adminBO.getAssociateCode());
				m_pst.setString(16, processBean.getB_cardHolderName());
				m_pst.setString(17, adminBO.getUid());
				m_pst.setInt(18, authStatus);
				m_pst.setString(19, (authStatus == ICCConstant.SETTLED?processBean.getB_trans_id():""));
				m_pst.setInt(20, processBean.getCc_Invoice());
				m_pst.setInt(21, approvedBy);
				m_pst.execute();
				rs=m_pst.getGeneratedKeys();
				if(rs.next()){
					txnNum = rs.getInt(1);
				}
				//				m_conn.close();
			} catch (SQLException e) {
				cat.error("TDSException ProcessingDAO.insertPaymentIntoWork-->"+e.getMessage());
				System.out.println("TDSException ProcessingDAO.insertPaymentIntoWork.SecontTime-->"+e.getMessage());
				e.printStackTrace();
				//				try {
				//					if(m_conn!=null)
				//					{
				//						m_conn.close();
				//					}
				//				} catch (SQLException e1) {
				//					// TODO: handle exception
				//				}
				e.printStackTrace();
			}
			ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,sqex.getMessage(),poolBO,1);
			oneDirect.start();
			cat.error("TDSException ProcessingDAO.insertPaymentIntoWork-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.insertPaymentIntoWork-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.insertPaymentIntoCC End--->"+(System.currentTimeMillis()-startTime));
		return txnNum;
	}

	public static void updateSignIntoCC(byte[] b,String trans_id1,String trans_id2) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.updateSignIntoCC Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		Blob blob = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("update TDS_CREDIT_CARD_DETAIL set CCD_RIDER_SIGNATURE = ? where (CCD_TRANS_ID_1 = ? AND CCD_TRANS_ID_2 is null) or (CCD_TRANS_ID_1 is null AND CCD_TRANS_ID_2 = ?) ");
			m_pst.setBytes(1, b==null?new byte[0]:b);
			m_pst.setString(2, trans_id1);
			m_pst.setString(3, trans_id2);
			int st = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.updateSignIntoWork-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.updateSignIntoWork-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.updateSignIntoCC End--->"+(System.currentTimeMillis()-startTime));
	}

	public static int alterTransct(String tip_amount, String amt,String trans_id1,String trans_id2,String cap_tran_id1,String cap_tran_id2,byte b[],String userID) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.alterTransct Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		Blob blob;
		int status = 0; 
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("update TDS_CREDIT_CARD_DETAIL set CCD_AUTHCAPFLG=?,CCD_TIP_AMOUNT = ?,CCD_AMOUNT=?,CCD_TOTAL_AMOUNT = ?,CCD_CAPTURED_BY = ?,CCD_CAPTURE_ID1 =?,CCD_CAPTURE_ID2=?,CCD_CAPTUREDATE=now(),CCD_RIDER_SIGNATURE=? where CCD_TRANS_ID_1 = ? AND CCD_AUTHCAPFLG=1");
			m_pst.setInt(1, ICCConstant.SETTLED);
			m_pst.setDouble(2, Double.parseDouble(tip_amount));
			m_pst.setDouble(3, Double.parseDouble(amt));
			m_pst.setDouble(4, (Double.parseDouble(amt)+Double.parseDouble(tip_amount)));
			m_pst.setString(5, userID);
			m_pst.setString(6, cap_tran_id1);
			m_pst.setString(7, cap_tran_id2);
			//blob = new SerialBlob(b==null?new byte[0]:b);
			//m_pst.setBlob(5, blob);
			m_pst.setBytes(8, b==null?new byte[0]:b);
			m_pst.setString(9, trans_id1);
			//m_pst.setString(10, trans_id2);
			status = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.alterTransct-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException ProcessingDAO.alterTransct-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.alterTransct End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static PaymentProcessBean chckCapture(PaymentProcessBean processBean)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.chckCapture Start--->"+startTime);
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("select CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_RIDER_SIGNATURE,case CCD_AUTHCAPFLG when '"+ICCConstant.SETTLED+"' then 1 else 0 end as flg from TDS_CREDIT_CARD_DETAIL " +
					" where CCD_TRANS_ID_1=?");
			m_pst.setString(1, processBean.getB_trans_id());
			//	m_pst.setString(2, processBean.getB_trans_id2());
			//	m_pst.setString(3, processBean.getB_trans_id());
			//	m_pst.setString(4, processBean.getB_trans_id2());
			m_rs = m_pst.executeQuery();
			if(m_rs.first() && m_rs.getInt("flg") == 1)
			{
				processBean.setReturnById(true);
				processBean.setB_amount(m_rs.getString("CCD_AMOUNT"));
				processBean.setB_tip_amt(m_rs.getString("CCD_TIP_AMOUNT"));
				processBean.setB_sign(m_rs.getBytes("CCD_RIDER_SIGNATURE"));
				processBean.setError_log("0");
			}else if(m_rs.first() && m_rs.getInt("flg") == 0){
				processBean.setB_amount(m_rs.getString("CCD_AMOUNT"));
				processBean.setB_tip_amt(m_rs.getString("CCD_TIP_AMOUNT"));
				processBean.setB_sign(m_rs.getBytes("CCD_RIDER_SIGNATURE"));
				processBean.setReturnById(false);
				processBean.setError_log("0");
			} else {
				processBean.setReturnById(true);
				processBean.setError_log("1");
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ChargesDAO.chckCapture-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.chckCapture-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.chckCapture End--->"+(System.currentTimeMillis()-startTime));
		return processBean;
	}


	public static PaymentProcessBean chckReturnOrUndo(PaymentProcessBean processBean)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.chckReturnOrUndo Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("select CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_RIDER_SIGNATURE,case CCD_AUTHCAPFLG when '"+ICCConstant.SETTLED+"' then 1 else 0 end as flg from TDS_CREDIT_CARD_DETAIL " +
					" where CCD_TRANS_ID_1 = ?");
			m_pst.setString(1, processBean.getB_trans_id());
			//			m_pst.setString(2, processBean.getB_trans_id2());
			//			m_pst.setString(3, processBean.getB_trans_id());
			//			m_pst.setString(4, processBean.getB_trans_id2());
			System.out.println("Select Query--->"+m_pst);
			m_rs = m_pst.executeQuery();
			if(m_rs.first() && m_rs.getInt("flg") == 1)
			{
				processBean.setB_amount(m_rs.getString("CCD_AMOUNT"));
				processBean.setB_tip_amt(m_rs.getString("CCD_TIP_AMOUNT"));
				processBean.setB_sign(m_rs.getBytes("CCD_RIDER_SIGNATURE"));
				processBean.setReturnById(true);
				processBean.setTxn_total(""+(Double.parseDouble(m_rs.getString("CCD_AMOUNT"))+Double.parseDouble(m_rs.getString("CCD_TIP_AMOUNT"))));
				processBean.setError_log("0");
			}else if(m_rs.first() && m_rs.getInt("flg") == 0){
				processBean.setB_amount(m_rs.getString("CCD_AMOUNT"));
				processBean.setB_tip_amt(m_rs.getString("CCD_TIP_AMOUNT"));
				processBean.setTxn_total(""+(Double.parseDouble(m_rs.getString("CCD_AMOUNT"))+Double.parseDouble(m_rs.getString("CCD_TIP_AMOUNT"))));
				processBean.setB_sign(m_rs.getBytes("CCD_RIDER_SIGNATURE"));
				processBean.setReturnById(false);
				processBean.setError_log("0");
			} 
		} catch (SQLException sqex) {
			cat.error("TDSException ChargesDAO.chckReturnOrUndo-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.chckReturnOrUndo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.chckReturnOrUndo End--->"+(System.currentTimeMillis()-startTime));
		return processBean;
	}

	public static boolean voidTransAction(String trans_id,String trans_id2,String desc,String userID)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.voidTransAction Start--->"+startTime);
		boolean status = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_conn.setAutoCommit(false);
			m_pst = m_conn.prepareStatement("select * from TDS_CREDIT_CARD_DETAIL " +
					" where CCD_TRANS_ID_1 = ? ");
			m_pst.setString(1, trans_id);
			//			m_pst.setString(2, trans_id2);
			//			m_pst.setString(3, trans_id);
			//			m_pst.setString(4, trans_id2);
			m_rs = m_pst.executeQuery();
			if(m_rs.first())
			{
				status = true;
				m_pst = m_conn.prepareStatement("update TDS_CREDIT_CARD_DETAIL set CCD_VOIDEDED_BY='"+userID+"', CCD_VOID_FLG = true,CCD_VOID_DATE = now() " +
						" where CCD_TRANS_ID_1 = ? ");
				m_pst.setString(1, trans_id);
				//				m_pst.setString(2, trans_id2);
				//				m_pst.setString(3, trans_id);
				//				m_pst.setString(4, trans_id2);
				m_pst.execute();
				m_pst = m_conn.prepareStatement("insert into TDS_CREDIT_CARD_DETAIL(CCD_TRIP_ID,CCD_DRIVER_ID, CCD_ASSOC_CODE, CCD_CARD_ISSUER, CCD_CARD_TYPE, " +
						"CCD_CARD_HOLDER_NAME, CCD_SWIPETYPE, CCD_CARD_NO, CCD_AMOUNT, CCD_TIP_AMOUNT, CCD_TOTAL_AMOUNT, CCD_TRANS_ID_1, CCD_TRANS_ID_2, CCD_PROCESSING_DATE_TIME, CCD_APPROVAL_STATUS, CCD_APPROVAL_CODE, CCD_APPROVED_BY, CCD_DRIVER_PMT_AMT, " +
						"CCD_RIDER_SIGNATURE, CCD_REC_SET, CCD_CAPTURE_ID1, CCD_CAPTURE_ID2, CCD_CAPTUREDATE, CCD_CAPTURED_BY, CCD_AUTHCAPFLG, CCD_VOID_FLG, CCD_VOID_DATE," +
						"CCD_VOIDED_FLG, CCD_VOIDEDED_BY, CCD_STATUS_CODE_1, CCD_STATUS_CODE_2, CCD_SERVICE_PROVIDER )" +
						" SELECT " +
						" CCD_TRIP_ID,CCD_DRIVER_ID, CCD_ASSOC_CODE, CCD_CARD_ISSUER, CCD_CARD_TYPE, " +
						"CCD_CARD_HOLDER_NAME, CCD_SWIPETYPE, CCD_CARD_NO, -CCD_AMOUNT, -CCD_TIP_AMOUNT, -CCD_TOTAL_AMOUNT, " +
						" concat('R',CCD_TRANS_ID_1), concat('R',CCD_TRANS_ID_2), now(), CCD_APPROVAL_STATUS, CCD_APPROVAL_CODE, CCD_APPROVED_BY, " +
						" CCD_DRIVER_PMT_AMT, " +
						"CCD_RIDER_SIGNATURE, CCD_REC_SET, concat('R',CCD_CAPTURE_ID1), concat('R',CCD_CAPTURE_ID2), " +
						" CCD_CAPTUREDATE, CCD_CAPTURED_BY, CCD_AUTHCAPFLG, CCD_VOID_FLG, CCD_VOID_DATE," +
						" 1 , CCD_VOIDEDED_BY, CCD_STATUS_CODE_1, CCD_STATUS_CODE_2, CCD_SERVICE_PROVIDER " +
						" From TDS_CREDIT_CARD_DETAIL " +
						" WHERE  CCD_TRANS_ID_1 =?");

				//" select TW_DRIVERID,TW_ASSOCCODE, TW_TRIPID,TW_CARDISSUER,TW_CARDTYPE,TW_SWIPETYPE,TW_CARDNO,-TW_AMOUNT,-TW_TIPAMOUNT,-TW_TOTALAMOUNT,
				// concat('R',TW_TRANSID),now(),TW_APPROVALSTATUS,TW_APPROVALCODE,TW_RIDERSIGN,TW_CAPTUREID,TW_CAPTUREDATE,TW_AUTHCAPFLG,false,'0000-00-00 00:00:00',true,TW_STATUSCODE from TDS_WORK where TW_CAPTUREID = '"+ trans_id+"'");
				m_pst.setString(1, trans_id);
				//				m_pst.setString(2, trans_id2);
				//				m_pst.setString(3, trans_id);
				//				m_pst.setString(4, trans_id2);
				m_pst.execute();
			} 

			m_pst = m_conn.prepareStatement("insert into TDS_VOID_DESC values(?,?)");
			m_pst.setString(1, trans_id);
			m_pst.setString(2, desc);
			m_pst.execute();
			m_conn.commit();
			m_conn.setAutoCommit(true);
		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.voidTransAction-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.voidTransAction-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			try {
				m_conn.rollback();
			} catch (SQLException e) {
				// TODO: handle exception
			}
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.voidTransAction End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	public static ArrayList getUnCaptureTransactionToReverse(String fr_date,String to_date,String amount,String assoccode,String driver,String timeZone){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getUnCaptureTransactionToReverse Start--->"+startTime);
		ArrayList al_list = new ArrayList();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String query="";
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		query = "select " +
				"	concat(CCD_DRIVER_ID,'-',DR_FNAME) as Driver,CCD_TRIP_ID as Trip,CCD_CARD_ISSUER as c_issuer,CCD_AUTHCAPFLG," +
				"	CCD_CARD_TYPE as card_type,CCD_SWIPETYPE  as s_type,CCD_CARD_NO as card_no," +
				"	format(CCD_AMOUNT,2) as amount,format(CCD_TIP_AMOUNT,2) as tip,format(CCD_TOTAL_AMOUNT,2) as total," +
				"	CASE CCD_TRANS_ID_1 IS NULL WHEN TRUE THEN CCD_TRANS_ID_2 ELSE CCD_TRANS_ID_1 END as trans_id," +
				"	date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') as date," +
				"	CCD_APPROVAL_STATUS as status,0 as per,0 as txn_amt," +
				"	0 as total,CCD_VOID_FLG,date_format(CONVERT_TZ(CCD_VOID_DATE,'UTC','"+timeZone+"'),'%m/%d/%Y %h:%s %p') as CCD_VOID_DATE,CCD_VOIDED_FLG " +
				" from " +
				"	TDS_CREDIT_CARD_DETAIL,TDS_ADMINUSER,TDS_DRIVER  WHERE DR_SNO = AU_SNO and  AU_SNO = CCD_DRIVER_ID and AU_ASSOCCODE ='"+ assoccode + "'  and CCD_APPROVAL_STATUS = 1 " +
				" and CCD_AUTHCAPFLG != '"+ICCConstant.SETTLED+"' and " +
				" CCD_VOID_FLG !=1 and CCD_VOIDED_FLG!=1";

		if(driver!=""){
			query+=" AND CCD_DRIVER_ID ='"+driver+"'";
		}
		if(fr_date!="" && query.length() > 0){
			query+=" AND date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(fr_date)+"'";
		}
		if(to_date!="" && query.length() > 0){
			query+=" AND date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(to_date)+"'";
		}
		if(amount!="" && query.length() > 0){
			query+=" AND CCD_AMOUNT >='"+amount+"'";
		}
		try {
			m_pst = m_conn.prepareStatement(query);
			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				TransactionSummaryBean summaryBean = new TransactionSummaryBean();
				summaryBean.setT_amout(m_rs.getString("amount"));
				summaryBean.setT_card_no(m_rs.getString("card_no"));
				summaryBean.setT_processDate(m_rs.getString("date"));
				summaryBean.setT_tip(m_rs.getString("tip"));
				summaryBean.setT_trip(m_rs.getString("Trip"));
				summaryBean.setT_trans_id(m_rs.getString("trans_id"));
				summaryBean.setT_total(m_rs.getString("total"));
				summaryBean.setT_authcapflg(m_rs.getString("CCD_AUTHCAPFLG"));
				summaryBean.setT_driver(m_rs.getString("driver"));
				summaryBean.setT_voidflg(m_rs.getBoolean("CCD_VOID_FLG"));
				summaryBean.setT_voidedflg(m_rs.getBoolean("CCD_VOIDED_FLG"));
				summaryBean.setT_voiddate(m_rs.getString("CCD_VOID_DATE"));
				al_list.add(summaryBean);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.getUnCaptureTransactionToReverse-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException ProcessingDAO.getUnCaptureTransactionToReverse-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getUnCaptureTransactionToReverse End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	//	public static ArrayList<String> getDriverList(AdminRegistrationBO adminBO){
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
	//		ArrayList<String> listOfDrivers = new ArrayList<String>();
	//		TDSConnection m_dbConn = new TDSConnection();
	//		Connection m_conn = m_dbConn.getConnection();;
	//		PreparedStatement m_pst = null;
	//		ResultSet m_rs = null;
	//		String query="SELECT DR_SNO FROM TDS_DRIVER where DR_ASSOCODE='"+adminBO.getAssociateCode()+"'";
	//		try {
	//			m_pst = m_conn.prepareStatement(query);
	//			m_rs = m_pst.executeQuery();
	//			while(m_rs.next()){
	//				listOfDrivers.add(m_rs.getString("DR_SNO"));
	//			}
	//		} catch (SQLException sqex) {
	//			cat.error("TDSException ChargesDAO.getDriverList-->"+sqex.getMessage());
	//			System.out.println("TDSException ChargesDAO.getDriverList-->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//		} finally {
	//			m_dbConn.closeConnection();
	//		}
	//		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
	//		return listOfDrivers;
	//	}

	public static ArrayList getTransactionToReverse(int start,int end,String fr_date,String to_date,String amount,String assoccode,String driver,String approval_code,String card_no,String approval,String timeZone)	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getTransactionToReverse Start--->"+startTime);
		ArrayList al_list = new ArrayList();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String query="";
		query = "select CCD_APPROVAL_CODE," +
				"	concat(CCD_DRIVER_ID,'-',DR_FNAME) as Driver,CCD_TRIP_ID as Trip,CCD_CARD_ISSUER as c_issuer,CCD_AUTHCAPFLG," +
				"	CCD_CARD_TYPE as card_type,CCD_SWIPETYPE  as s_type,CCD_CARD_NO as card_no," +
				"	format(CCD_AMOUNT,2) as amount,format(CCD_TIP_AMOUNT,2) as tip,format(CCD_TOTAL_AMOUNT,2) as total," +
				"	CCD_TRANS_ID_1 as trans_id," +
				" date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') as date," +
				"	CCD_APPROVAL_STATUS as status,0 as per,0 as txn_amt," +
				"	0 as total,CCD_VOID_FLG,date_format(CONVERT_TZ(CCD_VOID_DATE,'UTC','"+timeZone+"'),'%m/%d/%Y %h:%s %p') as CCD_VOID_DATE,CCD_VOIDED_FLG " +
				" from " +
				"	TDS_CREDIT_CARD_DETAIL,TDS_ADMINUSER,TDS_DRIVER  WHERE DR_SNO = AU_SNO and  AU_SNO = CCD_DRIVER_ID and AU_ASSOCCODE ='"+ assoccode + "' ";
		if(driver!="" && driver!=null ){
			query+=" AND CCD_DRIVER_ID ='"+driver+"'";
		}
		if(fr_date!="" &&  fr_date!=null ){
			if(query.length() > 0) {
				query+=" AND date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(fr_date)+"'";
			}
		}
		if(to_date!="" &&  to_date!=null ){
			if( query.length() > 0) {
				query+=" AND date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(to_date)+"'";
			}
		}
		if(amount!="" &&   amount!=null ){
			if(query.length() > 0) {
				query+=" AND CCD_AMOUNT >='"+amount+"'";
			}
		}
		if(approval_code!="" &&    approval_code!=null  ){
			if( approval_code.length() > 0) {
				query+=" AND CCD_APPROVAL_CODE ='"+approval_code+"'";
			}
		}
		if(card_no!=""  &&    card_no!=null ){
			if(card_no.length() > 0) {
				query+=" AND CCD_CARD_NO ='"+card_no+"'";
			}
		}
		if(approval ==null)
		{
			query+="  and CCD_APPROVAL_STATUS =1";
		}
		query = query + " order by CCD_PROCESSING_DATE_TIME desc" ;
		query = query + " limit "+start+","+end+"";
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(query);
			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				TransactionSummaryBean summaryBean = new TransactionSummaryBean();
				summaryBean.setT_amout(m_rs.getString("amount"));
				summaryBean.setT_card_no(m_rs.getString("card_no"));
				summaryBean.setT_processDate(m_rs.getString("date"));
				summaryBean.setT_tip(m_rs.getString("tip"));
				summaryBean.setT_trip(m_rs.getString("Trip"));
				summaryBean.setT_trans_id(m_rs.getString("trans_id"));
				summaryBean.setT_total(m_rs.getString("total"));
				summaryBean.setT_authcapflg(m_rs.getString("CCD_AUTHCAPFLG"));
				summaryBean.setT_driver(m_rs.getString("driver"));
				summaryBean.setT_voidflg(m_rs.getBoolean("CCD_VOID_FLG"));
				summaryBean.setT_voidedflg(m_rs.getBoolean("CCD_VOIDED_FLG"));
				summaryBean.setT_voiddate(m_rs.getString("CCD_VOID_DATE"));
				summaryBean.setT_approval_code(m_rs.getString("CCD_APPROVAL_CODE"));
				summaryBean.setT_approval_status(m_rs.getString("status"));
				al_list.add(summaryBean);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.getTransactionToReverse-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.getTransactionToReverse-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getTransactionToReverse End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList getTransactionToReverse1(int start,int end,String fr_date,String to_date,String amount,String assoccode,String driver,String approval_code,String card_no,String approval,String timeZone,String fleetNo)	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getTransactionToReverse1 Start--->"+startTime);
		ArrayList al_list = new ArrayList();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String query="";
		query = "select CCD_APPROVAL_CODE," +
				"AU_ASSOCCODE,concat(CCD_DRIVER_ID,'-',DR_FNAME) as Driver,CCD_TRIP_ID as Trip,CCD_CARD_ISSUER as c_issuer,CCD_AUTHCAPFLG," +
				"CCD_CARD_TYPE as card_type,CCD_SWIPETYPE  as s_type,CCD_CARD_NO as card_no," +
				"format(CCD_AMOUNT,2) as amount,format(CCD_TIP_AMOUNT,2) as tip,format(CCD_TOTAL_AMOUNT,2) as total," +
				"CCD_TRANS_ID_1 as trans_id," +
				"date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') as date," +
				"CCD_APPROVAL_STATUS as status,0 as per,0 as txn_amt," +
				"0 as total,CCD_VOID_FLG,date_format(CONVERT_TZ(CCD_VOID_DATE,'UTC','"+timeZone+"'),'%m/%d/%Y %h:%s %p') as CCD_VOID_DATE,CCD_VOIDED_FLG " +
				" from " +
				"TDS_CREDIT_CARD_DETAIL,TDS_ADMINUSER,TDS_DRIVER  WHERE DR_SNO = AU_SNO and  AU_SNO = CCD_DRIVER_ID ";
		if(!fleetNo.equals("")){
			query+="and AU_MASTER_ASSOCCODE ='"+assoccode+"' and AU_ASSOCCODE ='"+ fleetNo + "'";
		}else{
			query+="and AU_MASTER_ASSOCCODE ='"+ assoccode + "'";
		}

		if(driver!="" && driver!=null ){
			query+=" AND CCD_DRIVER_ID ='"+driver+"'";
		}
		if(fr_date!="" &&  fr_date!=null ){
			if(query.length() > 0) {
				query+=" AND date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(fr_date)+"'";
			}
		}
		if(to_date!="" &&  to_date!=null ){
			if( query.length() > 0) {
				query+=" AND date_format(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(to_date)+"'";
			}
		}
		if(amount!="" &&   amount!=null ){
			if(query.length() > 0) {
				query+=" AND CCD_AMOUNT >='"+amount+"'";
			}
		}
		if(approval_code!="" &&    approval_code!=null  ){
			if( approval_code.length() > 0) {
				query+=" AND CCD_APPROVAL_CODE ='"+approval_code+"'";
			}
		}
		if(card_no!=""  &&    card_no!=null ){
			if(card_no.length() > 0) {
				query+=" AND CCD_CARD_NO ='"+card_no+"'";
			}
		}
		if(approval ==null)
		{
			query+="  and CCD_APPROVAL_STATUS =1";
		}
		query = query + " order by CCD_PROCESSING_DATE_TIME desc" ;
		query = query + " limit "+start+","+end+"";
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(query);
			System.out.println("Search query: "+m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				TransactionSummaryBean summaryBean = new TransactionSummaryBean();
				summaryBean.setT_amout(m_rs.getString("amount"));
				summaryBean.setT_card_no(m_rs.getString("card_no"));
				summaryBean.setT_processDate(m_rs.getString("date"));
				summaryBean.setT_tip(m_rs.getString("tip"));
				summaryBean.setT_trip(m_rs.getString("Trip"));
				summaryBean.setT_trans_id(m_rs.getString("trans_id"));
				summaryBean.setT_total(m_rs.getString("total"));
				summaryBean.setT_authcapflg(m_rs.getString("CCD_AUTHCAPFLG"));
				summaryBean.setT_driver(m_rs.getString("driver"));
				summaryBean.setT_voidflg(m_rs.getBoolean("CCD_VOID_FLG"));
				summaryBean.setT_voidedflg(m_rs.getBoolean("CCD_VOIDED_FLG"));
				summaryBean.setT_voiddate(m_rs.getString("CCD_VOID_DATE"));
				summaryBean.setT_approval_code(m_rs.getString("CCD_APPROVAL_CODE"));
				summaryBean.setT_approval_status(m_rs.getString("status"));
				summaryBean.setT_fleetno(m_rs.getString("AU_ASSOCCODE"));
				al_list.add(summaryBean);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.getTransactionToReverse1-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.getTransactionToReverse1-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getTransactionToReverse1 End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList<ChargesBO> getChargesForTrips(String tripId,String assoCode,String masterAssoccode){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getChargesForTrips Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ArrayList<ChargesBO> chargeDetails= new ArrayList<ChargesBO>();
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("SELECT DC_TYPE_CODE,DC_AMOUNT,DC_DRIVER_PMT_AMT,DC_DRIVER_PMT_STATUS,DC_DRIVER_PMT_NUM FROM TDS_DRIVER_CHARGE_CALC WHERE DC_TRIP_ID ='"+tripId+"' AND DC_MASTER_ASSOCCODE='"+masterAssoccode+"'");
			//System.out.println("Get Charges--->"+pst);
			ResultSet rs = pst.executeQuery();
			while(rs.next())
			{
				ChargesBO charBO = new ChargesBO();
				charBO.setKey(rs.getInt("DC_TYPE_CODE"));
				charBO.setAmount(rs.getString("DC_AMOUNT"));
				chargeDetails.add(charBO);
			}
		}
		catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.getChargesForTrips-->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getChargesForTrips End--->"+(System.currentTimeMillis()-startTime));
		return chargeDetails;
	}
	public static void updateChargeDriver(String assoccode,String tripID,String driver,String masterAssoccode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.updateChargeDriver Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement m_pst = null;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst = conn.prepareStatement("UPDATE TDS_DRIVER_CHARGE_CALC SET DC_DRIVER_ID='"+driver+"' WHERE DC_TRIP_ID='"+tripID+"' AND DC_MASTER_ASSOCCODE='"+masterAssoccode+"'");
			m_pst.executeUpdate();
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.updateChargeDriver-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.updateChargeDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.updateChargeDriver End--->"+(System.currentTimeMillis()-startTime));
	}
	public static int reverseDisbursement(String disbursementId,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.reverseDisbursement Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		int result=0;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("UPDATE TDS_DRIVER_Disbursement SET DD_STATUS=2 WHERE DD_DISP_ID="+disbursementId+" AND (DD_ASSOCCODE='"+adminBO.getAssociateCode()+"' OR DD_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"')");
			result=pst.executeUpdate();
			if(result>=1){
				result=0;
				pst = conn.prepareStatement("Update TDS_DRIVER_CHARGE_CALC SET DC_DRIVER_PMT_STATUS=0 WHERE DC_DRIVER_PMT_NUM="+disbursementId+" AND (DC_ASSOC_CODE='"+adminBO.getAssociateCode()+"' OR DC_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"')");
				result=pst.executeUpdate();
				result=0;
				pst=conn.prepareStatement("Update TDS_CREDIT_CARD_DETAIL SET CCD_DRIVER_PMT_STATUS=0 WHERE CCD_DRIVER_PMT_NUM="+disbursementId+" AND (CCD_ASSOC_CODE='"+adminBO.getAssociateCode()+"' OR CCD_ASSOC_CODE='"+adminBO.getMasterAssociateCode()+"')");
				result=pst.executeUpdate();
				pst =conn.prepareStatement("Update TDS_VOUCHER_DETAIL SET VD_DRIVER_PMT_STATUS=0 where VD_DRIVER_PMT_NUM="+disbursementId+" AND (VD_ASSOC_CODE='"+adminBO.getAssociateCode()+"' OR VD_ASSOC_CODE='"+adminBO.getMasterAssociateCode()+"')");
				result=pst.executeUpdate();
				pst=conn.prepareStatement("UPDATE TDS_DRIVER_LEASE SET DL_DRIVER_PMT_STATUS=0 WHERE DL_DRIVER_PMT_NUM ="+disbursementId+" AND (DL_ASSOC_CODE='"+adminBO.getAssociateCode()+"' OR DL_ASSOC_CODE='"+adminBO.getMasterAssociateCode()+"')");
				result=pst.executeUpdate();
				pst = conn.prepareStatement("UPDATE TDS_DRIVER_MISC SET DM_DRIVER_PMT_STATUS =0 WHERE DM_DRIVER_PMT_NUM ="+disbursementId+" AND DM_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
				result=pst.executeUpdate();
				pst=conn.prepareStatement("UPDATE TDS_BALANCE SET BL_STATUS=0 WHERE BL_CARRY_OVER_TO="+disbursementId+" and (BL_ASSOCCODE='"+adminBO.getAssociateCode()+"' OR BL_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"')");
				result=pst.executeUpdate();
				pst=conn.prepareStatement("UPDATE TDS_OPENREQUEST_HISTORY SET ORH_PMT_NUM=0 WHERE ORH_PMT_NUM="+disbursementId+" AND (ORH_ASSOCCODE='"+adminBO.getAssociateCode()+"' OR ORH_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"')");
				result=pst.executeUpdate();
				pst=conn.prepareStatement("DELETE FROM TDS_BALANCE  WHERE BL_DISB_ID="+disbursementId+" and (BL_ASSOCCODE='"+adminBO.getAssociateCode()+"' OR BL_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"')");
				result=pst.executeUpdate();
			}
		}
		catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.reverseDisbursement-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.reverseDisbursement-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.reverseDisbursement End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static boolean insertVouchers(String tripID,String vcNumber,String assoccode,String driverid,String Amount)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertVouchers Start--->"+startTime);
		PreparedStatement m_pst = null;
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		ResultSet rs=null;
		boolean success=false;
		try {
			m_pst = conn.prepareStatement("DELETE FROM TDS_VOUCHER_DETAIL WHERE VD_TRIP_ID = '"+tripID+"'");
			m_pst.execute();
			m_pst = conn.prepareStatement("SELECT * FROM TDS_GOVT_VOUCHER WHERE VC_VOUCHERNO = '"+vcNumber+"'");
			rs=m_pst.executeQuery();
			if(rs.next()){
				m_pst = conn.prepareStatement("INSERT INTO TDS_VOUCHER_DETAIL(VD_TRIP_ID,VD_VOUCHERNO,VD_RIDER_NAME,VD_COSTCENTER,VD_COMPANY_CODE,VD_ASSOC_CODE,VD_DESCR,VD_PROCESSING_DATE_TIME,VD_DELAYDAYS,VD_DRIVER_ID,VD_AMOUNT,VD_EXPIRY_DATE,VD_USER,VD_CONTACT,VD_FREQUENCY,VD_TOTAL,VD_PAYMENT_RECEIVED_STATUS,VD_IMAGE_TYPE,VD_MASTER_ASSOCCODE) VALUES (?,?,?,?,?,?,?,NOW(),?,?,?,?,?,?,?,?,2,?,?)");
				m_pst.setString(1, tripID );
				m_pst.setString(2, vcNumber );
				m_pst.setString(3, rs.getString("VC_RIDERNAME"));
				m_pst.setString(4, rs.getString("VC_COSTCENTER"));
				m_pst.setString(5, rs.getString("VC_CCODE"));
				m_pst.setString(6, assoccode);
				m_pst.setString(7, rs.getString("VC_DESCR"));
				m_pst.setString(8, rs.getString("VC_DELAYDAYS"));
				m_pst.setString(9, driverid);
				m_pst.setString(10,Amount);
				m_pst.setString(11, rs.getString("VC_EXPIRYDATE"));
				m_pst.setString(12, rs.getString("VC_USER"));
				m_pst.setString(13, rs.getString("VC_CONTACT_PERSON"));
				m_pst.setString(14, rs.getString("VC_FREQ"));
				m_pst.setString(15,Amount);
				m_pst.setString(16, rs.getString("VC_FORMAT"));
				m_pst.setString(17, rs.getString("VD_MASTER_ASSOCCODE"));
				m_pst.execute();
				success=true;
			}
		}catch (SQLException sqex)
		{
			cat.error("TDSException ChargesDAO.insertVouchers-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.insertVouchers-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.insertVouchers End--->"+(System.currentTimeMillis()-startTime));
		return success;
	}
	public static int matchVoucherCharges(String assoccode,String[] amount,String[] key,int size,String voucherNo) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.matchVoucherCharges Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM TDS_GOVT_CHARGES WHERE GC_ASSOC_CODE='"+assoccode+"' AND GC_VOUCHER_NO='"+voucherNo+"'");
			pst.execute();
			for(int i=1;i<=size;i++){
				pst= con.prepareStatement("INSERT INTO TDS_GOVT_CHARGES (GC_ASSOC_CODE,GC_TYPE_CODE,GC_AMOUNT,GC_VOUCHER_NO) VALUES (?,?,?,?)");
				pst.setString(1, assoccode);
				pst.setString(2,key[i]);
				pst.setString(3,amount[i].replace("$", "").replace(" ", ""));
				pst.setString(4,voucherNo);
				pst.execute();
			}
			con.commit();
			con.setAutoCommit(true);
			result=1;
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.matchVoucherCharges--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.matchVoucherCharges--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.matchVoucherCharges End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<ChargesBO> getVoucherCharge(String assoccode,String voucherNo) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getVoucherCharge Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ArrayList<ChargesBO> charges=new ArrayList<ChargesBO>();
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst= con.prepareStatement("SELECT GC_TYPE_CODE,GC_AMOUNT FROM TDS_GOVT_CHARGES WHERE GC_VOUCHER_NO='"+voucherNo+"' AND GC_ASSOC_CODE='"+assoccode+"'");
			rs=pst.executeQuery();
			while(rs.next()){
				ChargesBO chargeVoucher=new ChargesBO();
				chargeVoucher.setPayTypeKey(rs.getInt("GC_TYPE_CODE"));
				chargeVoucher.setPayTypeAmount(rs.getString("GC_AMOUNT"));
				charges.add(chargeVoucher);
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.getVoucherCharge--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.getVoucherCharge--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.getVoucherCharge End--->"+(System.currentTimeMillis()-startTime));
		return charges;
	}
	public static ArrayList<ChargesBO> getVoucherCharges(String assoccode,String voucher){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getVoucherCharges Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ArrayList<ChargesBO> chargeForVoucher = new ArrayList<ChargesBO>();
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("SELECT GC_TYPE_CODE,GC_AMOUNT FROM TDS_GOVT_CHARGES WHERE GC_ASSOC_CODE='"+assoccode+"' AND GC_VOUCHER_NO='"+voucher+"'");
			ResultSet rs = pst.executeQuery();
			while(rs.next())
			{
				ChargesBO charBO = new ChargesBO();
				charBO.setPayTypeKey(rs.getInt("GC_TYPE_CODE"));
				charBO.setAmount(rs.getString("GC_AMOUNT"));
				chargeForVoucher.add(charBO);
			}
		}
		catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.getVoucherCharges-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getVoucherCharges-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getVoucherCharges End--->"+(System.currentTimeMillis()-startTime));
		return chargeForVoucher;
	}
	public static int insertLeaseRates(String[] vehicleDescription,String[] leaseAmount,String[] numberOfPassengers,String[] startRate,String[] ratePerMile,int size,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertLeaseRates Start--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		try {
			if(vehicleDescription[1]!=""){
				for(int i=1;i<=size;i++){
					pst= con.prepareStatement("INSERT INTO TDS_LEASE_RATES (LR_ASSOCCODE,LR_VECHICAL_DESC,LR_LEASE_AMOUNT,LR_NO_OF_PASSANGER,LR_START_RATE,LR_PER_MILE_RATE)VALUES(?,?,?,?,?,?) ");
					pst.setString(1, adminBO.getAssociateCode());
					pst.setString(2,vehicleDescription[i]);
					pst.setString(3,leaseAmount[i]);
					pst.setString(4,numberOfPassengers[i]);
					pst.setString(5,startRate[i]);
					pst.setString(6,ratePerMile[i]);
					pst.execute();
					result=1;
				}
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.insertLeaseRates--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.insertLeaseRates--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertLeaseRates End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<ChargesBO> getLeaseRates(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getLeaseRates Start--->"+startTime);
		ArrayList<ChargesBO>getLeaseRate = new ArrayList<ChargesBO>();
		TDSConnection dbcon =  new TDSConnection();
		Connection con =dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs= null;
		String query="SELECT * FROM TDS_LEASE_RATES WHERE LR_ASSOCCODE='"+adminBO.getAssociateCode()+"' ";
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while (rs.next()) {
				ChargesBO chargesBO =new ChargesBO();
				chargesBO.setKey(rs.getInt("LR_ID"));
				chargesBO.setVehicleDesc(rs.getString("LR_VECHICAL_DESC"));
				chargesBO.setPayTypeAmount(rs.getString("LR_LEASE_AMOUNT"));
				chargesBO.setNumberOfPassengers(rs.getString("LR_NO_OF_PASSANGER"));
				chargesBO.setStartRate(rs.getString("LR_START_RATE"));
				chargesBO.setRatePerMile(rs.getString("LR_PER_MILE_RATE"));
				getLeaseRate.add(chargesBO);
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.getLeaseRates--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.getLeaseRates--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.getLeaseRates End--->"+(System.currentTimeMillis()-startTime));
		return getLeaseRate;
	}
	public static int deleteLease(String assoccode,String serialNumber){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.deleteLease Start--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		String query="DELETE FROM TDS_LEASE_RATES WHERE LR_ASSOCCODE='"+assoccode+"' AND LR_ID='"+serialNumber+"'";
		try{
			pst = con.prepareStatement(query);
			result=pst.executeUpdate();
		}catch(Exception sqex){
			cat.error("TDSException ChargesDAO.deleteLease-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.deleteLease-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.deleteLease End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	//	public static int insertMiscCharges(String driverId,String[] miscDetails,String[] miscAmount,int size,AdminRegistrationBO adminBO) {
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
	//		TDSConnection dbcon = new TDSConnection();
	//		Connection con = dbcon.getConnection();
	//		PreparedStatement pst = null;
	//		int result=0;
	//		try {
	//			if(miscDetails[1]!=""){
	//				for(int i=1;i<=size;i++){
	//					pst= con.prepareStatement("INSERT INTO TDS_DRIVER_MISC (DM_ASSOCCODE,DM_DRIVER_ID,DM_MISC_DESC,DM_MISC_AMOUNT)VALUES(?,?,?,?) ");
	//					pst.setString(1, adminBO.getAssociateCode());
	//					pst.setString(2,driverId);
	//					pst.setString(3,miscDetails[i]);
	//					pst.setString(4,miscAmount[i]);
	//					pst.execute();
	//					result=1;
	//				}
	//			}
	//		}catch ( SQLException sqex) {
	//			System.out.println("TDSException ChargesDAO.insertMiscCharges--->"+sqex.getMessage());
	//			cat.error("TDSException ChargesDAO.insertMiscCharges--->"+sqex.getMessage());
	//			sqex.getStackTrace();
	//		} finally {
	//			dbcon.closeConnection();
	//		}
	//		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
	//		return result;
	//	}

	//	public static ArrayList<ChargesBO> getMiscCharges(AdminRegistrationBO adminBO) {
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
	//		ArrayList<ChargesBO>getMiscCharges = new ArrayList<ChargesBO>();
	//		TDSConnection dbcon =  new TDSConnection();
	//		Connection con =dbcon.getConnection();
	//		PreparedStatement pst = null;
	//		ResultSet rs= null;
	//		String query="SELECT * FROM TDS_DRIVER_MISC WHERE DM_ASSOCCODE='"+adminBO.getAssociateCode()+"' ";
	//		try {
	//			pst= con.prepareStatement(query);
	//			rs=pst.executeQuery();
	//			while (rs.next()) {
	//				ChargesBO chargesBO =new ChargesBO();
	//				chargesBO.setKey(rs.getInt("DM_ID"));
	//				chargesBO.setMiscDesc(rs.getString("DM_MISC_DESC"));
	//				chargesBO.setAmount(rs.getString("DM_MISC_AMOUNT"));
	//				chargesBO.setDriverId(rs.getString("DM_DRIVER_ID"));
	//				getMiscCharges.add(chargesBO);
	//			}
	//
	//		}catch ( SQLException sqex) {
	//			System.out.println("TDSException ChargesDAO.getMiscCharges--->"+sqex.getMessage());
	//			cat.error("TDSException ChargesDAO.getMiscCharges--->"+sqex.getMessage());
	//			sqex.getStackTrace();
	//		} finally {
	//			dbcon.closeConnection();
	//		}
	//		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
	//		return getMiscCharges;
	//	}

	public static int deleteMiscCharges(String assoccode,String serialNumber){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.deleteMiscCharges Start--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		String query="DELETE FROM TDS_DRIVER_MISC WHERE DM_ASSOCCODE='"+assoccode+"' AND DM_ID='"+serialNumber+"'";
		try{
			pst = con.prepareStatement(query);
			result=pst.executeUpdate();
		}catch(Exception sqex){
			cat.error("TDSException ChargesDAO.deleteMiscCharges-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.deleteMiscCharges-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.deleteMiscCharges End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int insertMiscChargesUpload(String assoccode,ArrayList<ChargesBO> charges){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertMiscChargesUpload Start--->"+startTime);
		PreparedStatement pst = null;
		int result=0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			for(int i=0;i<charges.size();i++){
				pst = con.prepareStatement("INSERT INTO TDS_DRIVER_MISC (DM_ASSOCCODE,DM_DRIVER_ID,DM_MISC_DESC,DM_MISC_AMOUNT,DM_MISC_TYPE,DM_PROCESSING_TIME,DM_CUSTOM_FIELD) VALUES (?,?,?,?,?,?,?)");
				pst.setString(1, assoccode);
				pst.setString(2, charges.get(i).getDriverId());
				pst.setString(3, charges.get(i).getMiscDesc());
				pst.setString(4, charges.get(i).getAmount());
				pst.setInt(5, charges.get(i).getKey());
				pst.setString(6, charges.get(i).getProcessDate());
				pst.setString(7, charges.get(i).getCustomField());
				result=pst.executeUpdate();
			}
		}catch(Exception sqex){
			cat.error("TDSException ChargesDAO.deleteMiscCharges-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.deleteMiscCharges-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertMiscChargesUpload End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<DisbursementDetaillBean> getDriverChargePctTemplate(String assocode,String driverID,String startdDate, String endDate,String tmpltID, DisbursementMaster disbursementMaster)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getDriverChargePctTemplate Start--->"+startTime);
		PreparedStatement m_pst = null;
		ArrayList<DisbursementDetaillBean> al_list = new ArrayList<DisbursementDetaillBean>();
		ArrayList<DriverChargesBean> al_drcharges = new ArrayList<DriverChargesBean>();
		ArrayList<OpenRequestBO> al_opr = new ArrayList<OpenRequestBO>();
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs, rs1 = null;
		int paymentType = 0;
		try {
			m_pst = m_conn.prepareStatement("select * from TDS_CHARGE_TEMPLATE_MASTER WHERE CTM_KEY='"+tmpltID+"' and CTM_ASSOCIATION_CODE='"+assocode+"'");
			rs = m_pst.executeQuery();
			if(rs.next()){
				paymentType = rs.getInt("CTM_PAYMENT_TYPE");
				disbursementMaster.setChargeCalcType(paymentType);
			}
			if(paymentType == IChargeConstants.PAYMENT_TYPE_CC)
			{
				m_pst = m_conn.prepareStatement("select * from TDS_COMPANY_CC_SETUP WHERE CCC_COMPANY_ID='"+assocode+"' AND CCC_TEMPLATE_TYPE='"+tmpltID+"'");
				rs1 = m_pst.executeQuery();
				while(rs1.next())
				{
					DisbursementDetaillBean detaillBean = new DisbursementDetaillBean();
					detaillBean.setCTM_PROGRAM_NAME(rs.getString("CTM_PROGRAM_NAME"));
					detaillBean.setCTM_PROGRAM_ID(rs1.getString("CCC_TEMPLATE_TYPE"));
					detaillBean.setCTD_AMOUNT(rs1.getBigDecimal("CCC_TXN_AMOUNT"));
					detaillBean.setCTD_PERCENT(rs1.getBigDecimal("CCC_TXN_AMT_PERCENTAGE"));
					detaillBean.setCTD_TYPE_CODE(rs1.getInt("CCC_CARD_ISSUER"));
					detaillBean.setCT_DESCRIPRION(rs1.getInt("CCC_CARD_ISSUER")==1?"CC":"VC");
					al_list.add(detaillBean);
				}
			} else if(paymentType == IChargeConstants.PAYMENT_TYPE_CHARGE){
				m_pst = m_conn.prepareStatement("SELECT * FROM " +
						" TDS_CHARGE_TEMPLATE_MASTER master,TDS_CHARGE_TEMPLATE_DETAIL detail,TDS_CHARGE_TYPE type" +
						" where detail.CTD_MASTER_CODE=master.CTM_KEY and type.CT_KEY= detail.CTD_TYPE_CODE and" +
						" type.CT_ASSOCIATION_CODE=CTD_ASSOCIATION_CODE and CTD_ASSOCIATION_CODE  = CTM_ASSOCIATION_CODE" +
						" and CTM_KEY='"+tmpltID+"' and CTM_ASSOCIATION_CODE='"+assocode+"'");
				rs = m_pst.executeQuery();
				while(rs.next())
				{
					DisbursementDetaillBean detaillBean = new DisbursementDetaillBean();
					detaillBean.setCTD_AMOUNT(rs.getBigDecimal("CTD_AMOUNT"));
					detaillBean.setCTD_KEY(rs.getString("CTD_KEY"));
					detaillBean.setCTD_PAYMENT_SWITCH(rs.getInt("CTD_PAYMENT_SWITCH"));
					detaillBean.setCTD_TYPE_CODE(rs.getInt("CTD_TYPE_CODE"));
					detaillBean.setCTM_PROGRAM_NAME(rs.getString("CTM_PROGRAM_NAME"));
					detaillBean.setCTM_PROGRAM_ID(rs.getString("CTM_KEY"));
					detaillBean.setCT_DESCRIPRION(rs.getString("CT_DESCRIPTION"));

					/*System.out.println("Sql");
					System.out.println("tax1:tax2:tax3 - "+rs.getString("CTD_TAX1")+":"+rs.getString("CTD_TAX2")+":"+rs.getString("CTD_TAX3"));*/

					detaillBean.setCTD_TAX1_AMOUNT(new BigDecimal(rs.getString("CTD_TAX1").equals(null)?"0":rs.getString("CTD_TAX1")));
					detaillBean.setCTD_TAX2_AMOUNT(new BigDecimal(rs.getString("CTD_TAX2").equals(null)?"0":rs.getString("CTD_TAX2")));
					detaillBean.setCTD_TAX3_AMOUNT(new BigDecimal(rs.getString("CTD_TAX3").equals(null)?"0":rs.getString("CTD_TAX3")));

					/*System.out.println("Bean");
					System.out.println("tax1:tax2:tax3 - "+detaillBean.getCTD_TAX1_AMOUNT()+":"+detaillBean.getCTD_TAX2_AMOUNT()+":"+detaillBean.getCTD_TAX3_AMOUNT());*/

					al_list.add(detaillBean);
				}
			}
		} catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getDriverChargePctTemplate-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getDriverChargePctTemplate-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getDriverChargePctTemplate End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList<DriverChargesBean> getDriverChargeDetails(String assocode,String driverID,String startdDate, String endDate,String stTime,String endTime,String tmpltID, String masterAssociationCode, ArrayList<String>listOfTripIDs,String timeZone)	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getDriverChargeDetails Start--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<DriverChargesBean> listOfCharges = new ArrayList<DriverChargesBean>();
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs, rs1 = null;
		StringBuffer tripIDs=new StringBuffer();
		if(listOfTripIDs.size()>0){
			for(int i=0;i<listOfTripIDs.size()-1;i++){
				tripIDs.append("'"+listOfTripIDs.get(i)+"',");
			}
			tripIDs.append("'"+listOfTripIDs.get(listOfTripIDs.size()-1)+"'");
		}else{
			tripIDs.append("''");
		}
		String query = "select *,DATE_FORMAT(CONVERT_TZ(DC_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') as date from TDS_DRIVER_CHARGE_CALC "+
				"LEFT JOIN  TDS_CHARGE_TYPE ON CT_KEY = DC_TYPE_CODE AND CT_ASSOCIATION_CODE = '"+masterAssociationCode+"' "+
				"where DC_DRIVER_PMT_STATUS !=1 AND " +
				"(DC_ASSOC_CODE='"+assocode+"' OR DC_MASTER_ASSOCCODE='"+masterAssociationCode+"') and DC_DRIVER_ID='"+driverID+"' ";
		if(!startdDate.equals(""))
		{
			query = query + " and date_format(CONVERT_TZ(DC_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') >= '"+startdDate+"'";
		}
		if(!endDate.equals(""))
		{
			query = query + " and date_format(CONVERT_TZ(DC_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') <= '"+endDate+"'";
		}
		if(!stTime.equals(""))
		{
			query = query + " and CONVERT_TZ(DC_PROCESSING_DATE_TIME,'UTC','"+timeZone+"') <= '"+stTime+"'";
		}
		if(!endTime.equals(""))
		{
			query = query + " and CONVERT_TZ(DC_PROCESSING_DATE_TIME,'UTC','"+timeZone+"') <= '"+endTime+"'";
		}
		query = query + "and DC_TRIP_ID IN ("+tripIDs+") Order by DC_TRIP_ID";
		try {
			pst = m_conn.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next() )
			{
				DriverChargesBean chargesBean = new DriverChargesBean();
				chargesBean.setTXNO(rs.getString("DC_TXN_NO"));
				chargesBean.setDC_TYPE_CODE(rs.getInt("DC_TYPE_CODE"));
				chargesBean.setTotalamount(rs.getBigDecimal("DC_AMOUNT").setScale(2, RoundingMode.CEILING));
				//chargesBean.setDC_DRIVER_PMT_AMT(rs.getBigDecimal("DC_AMOUNT").setScale(2, RoundingMode.CEILING));
				chargesBean.setDC_DRIVER_PMT_AMT(rs.getBigDecimal("DC_DRIVER_PMT_AMT")==null?new BigDecimal(0.00):rs.getBigDecimal("DC_DRIVER_PMT_AMT").setScale(2, RoundingMode.CEILING));
				chargesBean.setPaymentMode("Charges");
				chargesBean.setTripID(rs.getString("DC_TRIP_ID"));
				chargesBean.setDC_PROCESSING_DATE_TIME(rs.getString("date"));
				chargesBean.setDC_TYPE_DESC(rs.getString("CT_DESCRIPTION"));
				listOfCharges.add(chargesBean);
			}
		} catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getDriverChargeDetails-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getDriverChargeDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getDriverChargeDetails End--->"+(System.currentTimeMillis()-startTime));
		return listOfCharges;
	}

	public static ArrayList<DriverChargesBean> getDriverPaymentDetails(String assocode,String driverID,String startdDate, String endDate,String stTime,String endTime, ArrayList<String>listOfTripIDs,String masterAssoccode,String timeZone){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getDriverPaymentDetails Start--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<DriverChargesBean> listOfPayments = new ArrayList<DriverChargesBean>();
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		StringBuffer tripIDs=new StringBuffer();

		if(listOfTripIDs.size()>0){
			for(int i=0;i<listOfTripIDs.size()-1;i++){
				tripIDs.append("'"+listOfTripIDs.get(i)+"',");
			}
			tripIDs.append("'"+listOfTripIDs.get(listOfTripIDs.size()-1)+"'");
		}else{
			tripIDs.append("''");
		}
		String query  ="Select * from" +
				"(" +
				"select" +
				" '"+ITransactionType.CCARD+"'  as DC_TYPE_CODE,CCD_TRIP_ID TRIPID,CCD_DRIVER_ID driverID,CCD_CAPTURED_BY capturedId, CCD_TXN_NO TX_NO,CCD_CARD_NO vcardno,CCD_CARD_ISSUER ISSUER,CCD_TOTAL_AMOUNT total,'Card' as 'Mode', " +
				"DATE_FORMAT(CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y')  as enteredDate,CCD_AMOUNT amount,CCD_TIP_AMOUNT tip ,CCD_APPROVAL_STATUS status,CCD_ASSOC_CODE assoccode" +
				" from TDS_CREDIT_CARD_DETAIL" +
				" WHERE CCD_DRIVER_PMT_STATUS != 1 " +
				" UNION " +
				"select" +
				"  '"+ITransactionType.VOUCHER+"' as DC_TYPE_CODE,VD_TRIP_ID TRIPID,VD_DRIVER_ID driverID,VD_USER capturedId,VD_TXN_ID TX_NO,VD_VOUCHERNO vcardno,VD_COSTCENTER ISSUER,VD_TOTAL total,'Voucher' as 'Mode',  " +
				"DATE_FORMAT( CONVERT_TZ(VD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') as enteredDate, VD_AMOUNT amount,VD_TIP tip,VD_VERIFIED status,VD_ASSOC_CODE assoccode"+
				" from TDS_VOUCHER_DETAIL" +
				" WHERE VD_DRIVER_PMT_STATUS != 1 " +
				") as A  WHERE 1=1 AND (driverID = '"+driverID+"' OR capturedId='"+driverID+"')";
		if(!startdDate.equals(""))
		{
			query = query + " and date_format(CONVERT_TZ(enteredDate,'UTC','"+timeZone+"'),'%m/%d/%Y') >= '"+startdDate+"'";
		}
		if(!endDate.equals(""))
		{
			query = query + " and date_format(CONVERT_TZ(enteredDate,'UTC','"+timeZone+"'),'%m/%d/%Y') <= '"+endDate+"' ";
		}
		if(!stTime.equals(""))
		{
			query = query + " and CONVERT_TZ(enteredDate,'UTC','"+timeZone+"') >= '"+stTime+"'";
		}
		if(!endDate.equals(""))
		{
			query = query + " and CONVERT_TZ(enteredDate,'UTC','"+timeZone+"') <= '"+endTime+"' ";
		}

		query = query + "  and TRIPID IN ("+tripIDs+") AND (assoccode='"+assocode+"' OR assoccode='"+masterAssoccode+"') Order by TRIPID";
		try {
			pst = m_conn.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				DriverChargesBean chargesBean = new DriverChargesBean();
				chargesBean.setDC_TYPE_CODE(rs.getInt("DC_TYPE_CODE"));
				chargesBean.setTXNO(rs.getString("TX_NO"));
				chargesBean.setVcardno(rs.getString("vcardno"));
				chargesBean.setIssuer(rs.getString("issuer"));
				chargesBean.setAmount(rs.getBigDecimal("amount").setScale(2, RoundingMode.CEILING));
				chargesBean.setTip(rs.getBigDecimal("tip"));
				chargesBean.setTotalamount(rs.getBigDecimal("total").setScale(2, RoundingMode.CEILING));
				chargesBean.setPaymentMode(rs.getString("Mode"));
				chargesBean.setTripID(rs.getString("TRIPID"));
				chargesBean.setDC_PROCESSING_DATE_TIME(rs.getString("enteredDate"));
				chargesBean.setVerifiedStatus(rs.getInt("status"));
				listOfPayments.add(chargesBean);
			}
		} catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getDriverPaymentDetails-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getDriverPaymentDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getDriverPaymentDetails End--->"+(System.currentTimeMillis()-startTime));
		return listOfPayments;
	}

	public static ArrayList<OpenRequestBO> getTripDetails(AdminRegistrationBO adminBO,String tripID,String driverID,String startDate,String endDate,String stTime,String endTime)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getTripDetails Start--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<OpenRequestBO> listOfTrips = new ArrayList<OpenRequestBO>();
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		String query="select *,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as date from TDS_OPENREQUEST_HISTORY where ORH_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND ORH_PMT_NUM=0";
		if(tripID!=null && ! tripID.equals("")){
			query=query+" AND ORH_TRIP_ID='"+tripID+"'";
		}
		if(driverID!=null && ! driverID.equals("")){
			query=query+" AND ORH_DRIVERID='"+driverID+"'";
		}
		if(stTime.equals("")){
			if(startDate!=null && !startDate.equals("")){
				query=query+" AND date_format(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y')>='"+startDate+"'";
			}
			if(endDate!=null && !endDate.equals("")){
				query=query+" AND date_format(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y')<='"+endDate+"'";

			}
		}
		if(!stTime.equals(""))
		{
			query = query + " and CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"') >= '"+stTime+"'";
		}
		if(!endTime.equals(""))
		{
			query = query + " and CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"') <= '"+endTime+"'";
		}
		query=query+" ORDER BY ORH_TRIP_ID";
		try {
			pst = m_conn.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next())
			{
				OpenRequestBO openRequestBO= new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("ORH_TRIP_ID"));
				openRequestBO.setSadd1(rs.getString("ORH_STADD1") != null?rs.getString("ORH_STADD1"):"");
				openRequestBO.setEadd1(rs.getString("ORH_EDADD1")!= null?rs.getString("ORH_EDADD1"):"");
				openRequestBO.setDriverid(rs.getString("ORH_DRIVERID")); 
				openRequestBO.setPaytype((rs.getString("ORH_PAYTYPE")));
				openRequestBO.setAcct(rs.getString("ORH_PAYACC"));
				openRequestBO.setAmt(new BigDecimal(rs.getString("ORH_AMT")));
				String reqTime = rs.getString("ORH_SERVICETIME");
				openRequestBO.setShrs(reqTime.substring(0,2));
				openRequestBO.setSmin(reqTime.substring(2,4));
				openRequestBO.setIsBeandata(1);
				openRequestBO.setName(rs.getString("ORH_NAME"));
				openRequestBO.setTripStatus(rs.getString("ORH_TRIP_STATUS"));
				openRequestBO.setSdate(rs.getString("date"));
				listOfTrips.add(openRequestBO);
			}
		}catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getTripDetails-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getTripDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getTripDetails End--->"+(System.currentTimeMillis()-startTime));
		return listOfTrips;
	}


	public static void getBalanceDetails(AdminRegistrationBO adminBO, String driverID, DisbursementMaster disbursementMaster ){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getBalanceDetails Start--->"+startTime);
		PreparedStatement pst = null;
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		try {
			pst = m_conn.prepareStatement("SELECT BL_AMOUNT FROM TDS_BALANCE WHERE BL_DRIVERID='"+driverID+"' and BL_STATUS=0");
			rs = pst.executeQuery();
			if(rs.next())
			{
				disbursementMaster.setOpeningBalance(rs.getBigDecimal("BL_AMOUNT"));
			} else {
				disbursementMaster.setOpeningBalance(BigDecimal.ZERO);
			}
		}catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getBalanceDetails-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getBalanceDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getBalanceDetails End--->"+(System.currentTimeMillis()-startTime));
	}

	public static ArrayList<ChargesBO> getMiscSetup(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getMiscSetup Start--->"+startTime);
		ArrayList<ChargesBO>getMiscCharges = new ArrayList<ChargesBO>();
		TDSConnection dbcon =  new TDSConnection();
		Connection con =dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs= null;
		String query="SELECT * FROM TDS_MISC_SETUP WHERE MS_ASSOCCODE='"+adminBO.getAssociateCode()+"' ";
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while (rs.next()) {
				ChargesBO chargesBO =new ChargesBO();
				chargesBO.setKey(rs.getInt("MS_MISC_KEY"));
				chargesBO.setMiscDesc(rs.getString("MS_MISC_TYPE"));
				chargesBO.setAmount(rs.getString("MS_AMOUNT"));
				getMiscCharges.add(chargesBO);
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.getMiscCharges--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.getMiscCharges--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.getMiscSetup End--->"+(System.currentTimeMillis()-startTime));
		return getMiscCharges;
	}

	public static int insertMiscSetup(String[] miscDetails,String[] miscAmount,int size,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.insertMiscSetup Start--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		try {
			if(miscDetails[1]!=""){
				for(int i=1;i<=size;i++){
					pst= con.prepareStatement("INSERT INTO TDS_MISC_SETUP (MS_ASSOCCODE,MS_MISC_TYPE,MS_AMOUNT)VALUES(?,?,?) ");
					pst.setString(1, adminBO.getAssociateCode());
					pst.setString(2,miscDetails[i]);
					pst.setString(3,miscAmount[i]);
					pst.execute();
					result=1;
				}
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.insertMiscSetup--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.insertMiscSetup--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertMiscSetup End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static ArrayList<DriverChargesBean> getDriverChargeDetailsForSummary(String assocode,String key,String masterAssoccode,ArrayList<String> listOfTripIDs)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getDriverChargeDetailsForSummary Start--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<DriverChargesBean> listOfCharges = new ArrayList<DriverChargesBean>();
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs= null;
		StringBuffer tripIDs=new StringBuffer();
		if(listOfTripIDs.size()>0){
			for(int i=0;i<listOfTripIDs.size()-1;i++){
				tripIDs.append("'"+listOfTripIDs.get(i)+"',");
			}
			tripIDs.append("'"+listOfTripIDs.get(listOfTripIDs.size()-1)+"'");
		}else{
			tripIDs.append("''");
		}
		String query="select * from TDS_DRIVER_CHARGE_CALC LEFT JOIN  TDS_CHARGE_TYPE ON CT_KEY = DC_TYPE_CODE AND CT_ASSOCIATION_CODE='"+masterAssoccode+"'  where (DC_ASSOC_CODE='"+assocode+"' OR DC_MASTER_ASSOCCODE='"+masterAssoccode+"') AND DC_DRIVER_PMT_NUM ='"+key+"' AND DC_DRIVER_PMT_STATUS !='2' AND DC_TRIP_ID IN ("+tripIDs+") ORDER BY DC_TRIP_ID";
		try {
			pst = m_conn.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next() )
			{
				DriverChargesBean chargesBean = new DriverChargesBean();
				chargesBean.setTXNO(rs.getString("DC_TXN_NO"));
				chargesBean.setDC_TYPE_CODE(rs.getInt("DC_TYPE_CODE"));
				chargesBean.setTotalamount(rs.getBigDecimal("DC_AMOUNT"));
				chargesBean.setDC_DRIVER_PMT_AMT(rs.getBigDecimal("DC_DRIVER_PMT_AMT"));
				chargesBean.setPaymentMode("Charges");
				chargesBean.setTripID(rs.getString("DC_TRIP_ID"));
				chargesBean.setDC_PROCESSING_DATE_TIME(rs.getString("DC_PROCESSING_DATE_TIME"));
				chargesBean.setDC_TYPE_DESC(rs.getString("CT_DESCRIPTION"));
				listOfCharges.add(chargesBean);
			}
		} catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getDriverChargeDetailsForSummary-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getDriverChargeDetailsForSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getDriverChargeDetailsForSummary End--->"+(System.currentTimeMillis()-startTime));
		return listOfCharges;
	}
	public static ArrayList<DriverChargesBean> getDriverPaymentDetailsForSummary(String assocode,String key,ArrayList<String> listOfTripIDs,String timeZone){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getDriverPaymentDetailsForSummary Start--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<DriverChargesBean> listOfPayments = new ArrayList<DriverChargesBean>();
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		StringBuffer tripIDs =new StringBuffer();
		if(listOfTripIDs.size()>0){
			for(int i=0;i<listOfTripIDs.size()-1;i++){
				tripIDs.append("'"+listOfTripIDs.get(i)+"',");
			}
			tripIDs.append("'"+listOfTripIDs.get(listOfTripIDs.size()-1)+"'");
		}else{
			tripIDs.append("''");
		}
		String query  ="select" +
				" VD_DRIVER_ID  as driverID,'Voucher' as DC_TYPE_CODE,VD_TRIP_ID as TripID,VDD_APPROVAL_CODE as approvecode,VD_TXN_ID TX_NO,VD_TOTAL as Total,VD_DRIVER_PMT_AMT as driverAmt,VD_DRIVER_PMT_NUM as DRIVER_PMT_NUM,VD_VOUCHERNO vcardno,VD_COSTCENTER ISSUER,VD_TOTAL total,'Voucher' as 'Mode', " +
				" DATE_FORMAT( CONVERT_TZ(VD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') as enteredDate, VD_AMOUNT amount,VD_TIP tip,VD_VERIFIED status from TDS_VOUCHER_DETAIL where VD_DRIVER_PMT_NUM='"+key+"' AND VD_DRIVER_PMT_STATUS !='2' AND VD_TRIP_ID IN ("+tripIDs+") ORDER BY VD_TRIP_ID";
		try {
			pst = m_conn.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				DriverChargesBean chargesBean = new DriverChargesBean();
				chargesBean.setDC_TYPE_DESC(rs.getString("DC_TYPE_CODE"));
				chargesBean.setTXNO(rs.getString("TX_NO"));
				chargesBean.setVcardno(rs.getString("vcardno"));
				chargesBean.setIssuer(rs.getString("issuer"));
				chargesBean.setDC_DRIVER_PMT_AMT(rs.getBigDecimal("amount"));
				chargesBean.setTip(rs.getBigDecimal("tip"));
				chargesBean.setTotalamount(rs.getBigDecimal("total"));
				chargesBean.setPaymentMode(rs.getString("Mode"));
				chargesBean.setTripID(rs.getString("TRIPID"));
				chargesBean.setDC_PROCESSING_DATE_TIME(rs.getString("enteredDate"));
				chargesBean.setVerifiedStatus(rs.getInt("status"));
				listOfPayments.add(chargesBean);
			}
		} catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getDriverPaymentDetailsForSummary-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getDriverPaymentDetailsForSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getDriverPaymentDetailsForSummary End--->"+(System.currentTimeMillis()-startTime));
		return listOfPayments;
	}
	public static ArrayList<DriverChargesBean> getDriverCCPaymentDetailsForSummary(String assocode,String key,ArrayList<String> listOfTripIDs,String timeZone){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getDriverCCPaymentDetailsForSummary Start--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<DriverChargesBean> listOfPayments = new ArrayList<DriverChargesBean>();
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		StringBuffer tripIDs =new StringBuffer();
		if(listOfTripIDs.size()>0){
			for(int i=0;i<listOfTripIDs.size()-1;i++){
				tripIDs.append("'"+listOfTripIDs.get(i)+"',");
			}
			tripIDs.append("'"+listOfTripIDs.get(listOfTripIDs.size()-1)+"'");
		}else{
			tripIDs.append("''");
		}
		String query  ="select" +
				" CCD_DRIVER_ID  as driverID,'CC' as DC_TYPE_CODE,CCD_TRIP_ID as TripID,CCD_APPROVAL_CODE as approvecode,CCD_TRANS_ID_1 TX_NO,CCD_TOTAL_AMOUNT as Total,CCD_DRIVER_PMT_AMT as driverAmt,CCD_DRIVER_PMT_NUM as DRIVER_PMT_NUM,CCD_CARD_NO vcardno,CCD_CARD_ISSUER as issuer,CCD_TOTAL_AMOUNT total,'CC' as 'Mode', " +
				" DATE_FORMAT( CONVERT_TZ(CCD_PROCESSING_DATE_TIME,'UTC','"+timeZone+"'),'%m/%d/%Y') as enteredDate, CCD_AMOUNT amount,CCD_TIP_AMOUNT tip,0 status from TDS_CREDIT_CARD_DETAIL where CCD_DRIVER_PMT_NUM='"+key+"' AND CCD_TRIP_ID IN ("+tripIDs+") ORDER BY CCD_TRIP_ID";
		try {
			pst = m_conn.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				DriverChargesBean chargesBean = new DriverChargesBean();
				chargesBean.setDC_TYPE_DESC(rs.getString("DC_TYPE_CODE"));
				chargesBean.setTXNO(rs.getString("TX_NO"));
				chargesBean.setVcardno(rs.getString("vcardno"));
				chargesBean.setIssuer(rs.getString("issuer"));
				chargesBean.setDC_DRIVER_PMT_AMT(rs.getBigDecimal("amount"));
				chargesBean.setTip(rs.getBigDecimal("tip"));
				chargesBean.setTotalamount(rs.getBigDecimal("total"));
				chargesBean.setPaymentMode(rs.getString("Mode"));
				chargesBean.setTripID(rs.getString("TRIPID"));
				chargesBean.setDC_PROCESSING_DATE_TIME(rs.getString("enteredDate"));
				chargesBean.setVerifiedStatus(rs.getInt("status"));
				listOfPayments.add(chargesBean);
			}
		} catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getDriverPaymentDetailsForSummary-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getDriverPaymentDetailsForSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getDriverCCPaymentDetailsForSummary End--->"+(System.currentTimeMillis()-startTime));
		return listOfPayments;
	}

	public static DisbursementMaster getBalanceDetailsForSummary(AdminRegistrationBO adminBO,String key ){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getBalanceDetailsForSummary Start--->"+startTime);
		PreparedStatement pst = null;
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		DisbursementMaster disbursementMaster = new DisbursementMaster();
		String carryOver="";
		try {
			pst = m_conn.prepareStatement("SELECT BL_AMOUNT,BL_CARRY_OVER_TO FROM TDS_BALANCE WHERE BL_DISB_ID='"+key+"' and BL_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
			System.out.println("balance--->"+pst.toString());
			rs = pst.executeQuery();
			if(rs.next()){
				disbursementMaster.setClosingBalance(rs.getBigDecimal("BL_AMOUNT"));
				carryOver=rs.getString("BL_CARRY_OVER_TO");
			} else {
				disbursementMaster.setClosingBalance(BigDecimal.ZERO);
			}
			pst = m_conn.prepareStatement("SELECT BL_AMOUNT FROM TDS_BALANCE WHERE BL_CARRY_OVER_TO='"+key+"' and BL_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
			rs = pst.executeQuery();
			if(rs.next()){
				disbursementMaster.setOpeningBalance(rs.getBigDecimal("BL_AMOUNT"));
			}
		}catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getBalanceDetailsForSummary-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getBalanceDetailsForSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getBalanceDetailsForSummary End--->"+(System.currentTimeMillis()-startTime));
		return disbursementMaster;
	}

	public static DisbursementMaster getBalanceDetailsForSummary1(AdminRegistrationBO adminBO,ArrayList<DisbursementRecordMaster> list ){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getBalanceDetailsForSummary Start--->"+startTime);

		PreparedStatement pst = null;
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		String carryOver="";
		ArrayList<DisbursementMaster> record = new ArrayList<DisbursementMaster>();
		DisbursementMaster recordMaster = new DisbursementMaster();

		try {

			for(int i=0;i<list.size();i++){
				pst = m_conn.prepareStatement("SELECT BL_AMOUNT,BL_CARRY_OVER_TO FROM TDS_BALANCE WHERE BL_DISB_ID='"+list.get(i).getuID()+"' and BL_ASSOCCODE='"+adminBO.getAssociateCode()+"' and BL_DRIVERID='"+list.get(i).getDriverid()+"'");
				rs = pst.executeQuery();
				if(rs.next()){
					recordMaster.setClosingBalance(rs.getBigDecimal("BL_AMOUNT"));
					carryOver=rs.getString("BL_CARRY_OVER_TO");
				} else {
					recordMaster.setClosingBalance(BigDecimal.ZERO);
				}
				pst = m_conn.prepareStatement("SELECT BL_AMOUNT FROM TDS_BALANCE WHERE BL_CARRY_OVER_TO='"+list.get(i).getuID()+"' and BL_ASSOCCODE='"+adminBO.getAssociateCode()+"' and BL_DRIVERID='"+list.get(i).getDriverid()+"'");
				rs = pst.executeQuery();
				if(rs.next()){
					recordMaster.setOpeningBalance(rs.getBigDecimal("BL_AMOUNT"));
				}
				recordMaster.setDriverid(list.get(i).getDriverid());
			}
		}catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getBalanceDetailsForSummary-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getBalanceDetailsForSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getBalanceDetailsForSummary End--->"+(System.currentTimeMillis()-startTime));
		return recordMaster;
	}
	//	public static ArrayList<LeaseBean> getLeaseBeanForSummary(String assocode,String key)  
	//	{
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
	//		TDSConnection dbConn = null;
	//		Connection conn = null;
	//		PreparedStatement m_pst = null;
	//		dbConn = new TDSConnection();
	//		conn = dbConn.getConnection();
	//		ArrayList<LeaseBean> al_lease = new ArrayList<LeaseBean>();
	//		try {
	//			m_pst = conn.prepareStatement("select * from TDS_DRIVER_LEASE where DL_DRIVER_PMT_NUM=? and DL_ASSOC_CODE=? ");
	//			m_pst.setString(1, key);
	//			m_pst.setString(2, assocode);
	//			ResultSet rs = m_pst.executeQuery();
	//			while(rs.next())
	//			{
	//				LeaseBean leaseBean = new LeaseBean();
	//				leaseBean.setLeaseID(rs.getString("DL_TXN_NO"));
	//				leaseBean.setVechAmount(rs.getDouble("DL_AMOUNT"));
	//				leaseBean.setVechDesc(rs.getString("DL_VECHILE_DESC"));
	//				leaseBean.setVechileNo(rs.getString("DL_VECHILE_NO"));
	//				al_lease.add(leaseBean);
	//			}
	//
	//		}catch (SQLException sqex)
	//		{
	//			cat.error("TDSException ChargesDAO.getLeaseBeanForSummary-->"+sqex.getMessage());
	//			System.out.println("TDSException ChargesDAO.getLeaseBeanForSummary-->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//		} 
	//		finally {
	//			dbConn.closeConnection();
	//		}
	//		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
	//		return al_lease;
	//	}

	//	public static ArrayList<MiscBean> getMiscBeanForDetails(String assocode,String key) 
	//	{
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ChargesDAO.getCCDetails Start--->"+startTime);
	//		TDSConnection dbConn = null;
	//		Connection conn = null;
	//		PreparedStatement m_pst = null;
	//		dbConn = new TDSConnection();
	//		conn = dbConn.getConnection();
	//		ArrayList<MiscBean> al_misc = new ArrayList<MiscBean>();
	//		try {
	//			m_pst = conn.prepareStatement("select * from TDS_DRIVER_MISC where DM_DRIVER_PMT_NUM=? and DM_ASSOCCODE=? AND DM_DRIVER_PMT_STATUS=0");
	//			m_pst.setString(1, key);
	//			m_pst.setString(2, assocode);
	//			ResultSet rs = m_pst.executeQuery();
	//			while(rs.next())
	//			{
	//				MiscBean miscBean = new MiscBean();
	//				miscBean.setMiscid(rs.getString("DM_ID"));
	//				miscBean.setMiscAmount(rs.getDouble("DM_MISC_AMOUNT"));
	//				miscBean.setMiscDesc(rs.getString("DM_MISC_DESC"));
	//				miscBean.setCustomField(rs.getString("DM_CUSTOM_FIELD"));
	//				al_misc.add(miscBean);
	//			}
	//		}catch (SQLException sqex)
	//		{
	//			cat.error("TDSException ChargesDAO.getMiscBeanForDetails-->"+sqex.getMessage());
	//			System.out.println("TDSException ChargesDAO.getMiscBeanForDetails-->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//		} 
	//		finally {
	//			dbConn.closeConnection();
	//		}
	//		cat.info("ChargesDAO.getCCDetails End--->"+(System.currentTimeMillis()-startTime));
	//		return al_misc;
	//	}

	public static ArrayList<OpenRequestBO> getTripDetailsForSummary(AdminRegistrationBO adminBO,String key)
	{
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.getTripDetailsForSummary Start--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<OpenRequestBO> listOfJobs = new ArrayList<OpenRequestBO>();
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		String query="select *,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as date from TDS_OPENREQUEST_HISTORY where ORH_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND ORH_PMT_NUM= '"+key+"'";
		query=query+" ORDER BY ORH_TRIP_ID";
		try {
			pst = m_conn.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next())
			{
				OpenRequestBO openRequestBO= new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("ORH_TRIP_ID"));
				openRequestBO.setSadd1(rs.getString("ORH_STADD1") != null?rs.getString("ORH_STADD1"):"");
				openRequestBO.setEadd1(rs.getString("ORH_EDADD1")!= null?rs.getString("ORH_EDADD1"):"");
				openRequestBO.setDriverid(rs.getString("ORH_DRIVERID")); 
				openRequestBO.setPaytype((rs.getString("ORH_PAYTYPE")));
				openRequestBO.setAcct(rs.getString("ORH_PAYACC"));
				openRequestBO.setAmt(new BigDecimal(rs.getString("ORH_AMT")));
				String reqTime = rs.getString("ORH_SERVICETIME");
				openRequestBO.setShrs(reqTime.substring(0,2));
				openRequestBO.setSmin(reqTime.substring(2,4));
				openRequestBO.setIsBeandata(1);
				openRequestBO.setName(rs.getString("ORH_NAME"));
				openRequestBO.setTripStatus(rs.getString("ORH_TRIP_STATUS"));
				openRequestBO.setSdate(rs.getString("date"));
				listOfJobs.add(openRequestBO);
			}
		}catch(Exception sqex){
			cat.error("TDSException ChargesDAO.getTripDetails-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.getTripDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info("ChargesDAO.getTripDetailsForSummary End--->"+(System.currentTimeMillis()-startTime));
		return listOfJobs;
	}
	public static int updateChargeFormula(String key,String assoccode,String formula,String description,String amount) {
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.updateChargeFormula Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst= con.prepareStatement("UPDATE TDS_CHARGE_TYPE SET CT_FORMULA='"+formula+"',CT_DESCRIPTION='"+description+"',CT_AMOUNT='"+amount+"' WHERE CT_ASSOCIATION_CODE='"+assoccode+"' AND CT_KEY='"+key+"'");
			result=pst.executeUpdate();
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.updateChargeFormula--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.updateChargeFormula--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertChargeTemplate End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<ChargesBO> checkDynamicCharges(String key,String assoccode) {
		long startTime = System.currentTimeMillis();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<ChargesBO> newFormula = new ArrayList<ChargesBO>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst= con.prepareStatement("SELECT CT_KEY,CT_FORMULA FROM TDS_CHARGE_TYPE WHERE CT_FORMULA LIKE '%<"+key+">%' AND CT_ASSOCIATION_CODE='"+assoccode+"'");
			rs=pst.executeQuery();
			while(rs.next()){
				ChargesBO charges = new ChargesBO();
				charges.setKey(rs.getInt("CT_KEY"));
				charges.setDynamicFormula(rs.getString("CT_FORMULA"));
				newFormula.add(charges);
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.updateChargeFormula--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.updateChargeFormula--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.insertChargeTemplate End--->"+(System.currentTimeMillis()-startTime));
		return newFormula;
	}

	public static ArrayList<ChargesBO> getDynamicCharges(String key,String assoccode) {
		long startTime = System.currentTimeMillis();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<ChargesBO> newFormula = new ArrayList<ChargesBO>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst= con.prepareStatement("SELECT CT_KEY,CT_AMOUNT FROM TDS_CHARGE_TYPE WHERE CT_KEY='"+key+"' AND CT_ASSOCIATION_CODE='"+assoccode+"'");
			rs=pst.executeQuery();
			while(rs.next()){
				ChargesBO charges = new ChargesBO();
				charges.setKey(rs.getInt("CT_KEY"));
				charges.setAmount(rs.getString("CT_AMOUNT"));
				newFormula.add(charges);
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException ChargesDAO.getDynamicCharges--->"+sqex.getMessage());
			cat.error("TDSException ChargesDAO.getDynamicCharges--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("ChargesDAO.getDynamicCharges End--->"+(System.currentTimeMillis()-startTime));
		return newFormula;
	}

	
	public static int tagnameDisbursement(String driverid,String name,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info("ChargesDAO.tagnameDisbursement Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		int result=0;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			String driver[]=driverid.split(";");
			for(int i=0;i<driver.length;i++){
				String driverId=driver[i];
				pst = conn.prepareStatement("UPDATE TDS_DRIVER_Disbursement SET DD_TAG='"+name+"' WHERE DD_DRIVERID='"+driverId+"' AND (DD_ASSOCCODE='"+adminBO.getAssociateCode()+"' OR DD_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"')");
				pst.executeUpdate();
			}
			result=1;
		}
		catch (Exception sqex)
		{
			cat.error("TDSException ChargesDAO.tagnameDisbursement-->"+sqex.getMessage());
			System.out.println("TDSException ChargesDAO.tagnameDisbursement-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbConn.closeConnection();
		}
		cat.info("ChargesDAO.tagnameDisbursement End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


}
