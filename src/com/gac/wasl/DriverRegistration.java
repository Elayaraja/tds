package com.gac.wasl;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class DriverRegistration {

	public static WaslIntegrationBO RegisterDriver(String json_str) {
		// TODO Auto-generated method stub
		
		WaslIntegrationBO wasl_BO = new WaslIntegrationBO();
		StringBuffer wasl_response = new StringBuffer();
		try{
			URL siteURL = new URL("https://wasl.elm.sa/WaslPortalWeb/rest/DriverRegistration/send");
			//URL siteURL = new URL("http://localhost:8080/SHOWER/send");
			
			HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			connection.setRequestProperty("Accept-Charset", "UTF-8");
			//connection.set
			//String urlParameters = "{\"apiKey\":\"D42D418A-4FEE-42C9-9005-18C06F497DC6\", \"captainIdentityNumber\":\"1040961052\", \"dateOfBirth\":\"01-07-1376\",\"emailAddress\":\"almashari@rizamgroup.com\",\"mobileNumber\":\"966506443366\"}";
			String urlParameters = json_str;
			
			connection.setDoOutput(true);
			System.out.println(urlParameters);
			//System.out.println(URLEncoder.encode("ر","UTF-8"));
			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(wr, "UTF-8"));
			writer.write(urlParameters);
			//wr.writeBytes(urlParameters);
			writer.close();
			wr.flush();
			wr.close();
			
			int responseCode = connection.getResponseCode();
			System.out.println("Respnse code is"+ responseCode);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				wasl_response.append(inputLine);
			}
			in.close();
			
			wasl_BO.setResponseCode(responseCode);
			
			if(responseCode == 200){
				System.out.println("WASL driver Responce : "+wasl_response.toString());
				try{
					JSONObject jobj = new JSONObject(wasl_response.toString());
					wasl_BO.setResultCode(jobj.getInt("resultCode"));
					wasl_BO.setReferenceNumber(""+jobj.getInt("referenceNumber"));
					wasl_BO.setError_message(jobj.getString("resultMessage"));
					wasl_BO.setValid(jobj.getBoolean("valid"));
				}catch(JSONException j){
					System.out.println("WASL Exception : "+j.getMessage());
					wasl_BO.setValid(false);
					wasl_BO.setError_message(j.getMessage());
				}
			}else{
				wasl_BO.setValid(false);
				wasl_BO.setError_message("Invalid Data");
			}
			
		} catch (Exception e) {
			System.out.println("Exception : "+e.toString());
			//e.printStackTrace();
			wasl_BO.setValid(false);
			wasl_BO.setError_message(e.getMessage());
		}

		return wasl_BO;
	}

}