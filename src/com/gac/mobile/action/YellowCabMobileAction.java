package com.gac.mobile.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.gac.mobile.dao.MobileDAO;
import com.gac.mobile.dao.YellowCabDAO;
import com.tds.controller.SystemUnavailableException;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.DriverLocationBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class YellowCabMobileAction
 */
public class YellowCabMobileAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public YellowCabMobileAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException, SystemUnavailableException {
		String eventParam = request.getParameter(TDSConstants.eventParam);
		if(eventParam.equalsIgnoreCase("driverLocation")) {
			driverLocation(request, response);
		}else if(eventParam.equalsIgnoreCase("cabNoForPhNo")){
			cabNoForPhNo(request, response);
		}	else if(eventParam.equalsIgnoreCase("loginCab")){
			loginCab(request, response);
		}else if(eventParam.equalsIgnoreCase("postsms")){
			postSMS(request, response);
		}
	}
	public void driverLocation(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {

		DriverLocationBO driverLocationBO = new DriverLocationBO();

		driverLocationBO.setAssocCode("102");
		driverLocationBO.setLatitude(request.getParameter("latitude"));
		driverLocationBO.setLongitude(request.getParameter("latitude"));
			
		int m_queueStatus = YellowCabDAO.updateDriverLocation(driverLocationBO);
		
		response.getWriter().write("UPDATE=SUCCESS;");	
	}
	public void cabNoForPhNo(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		String phoneNo =p_request.getParameter("phoneNo");
		String cabNo=MobileDAO.getCabNoForPhone(m_adminBO.getAssociateCode(), phoneNo);
		p_response.getWriter().write("CAB="+cabNo+";");
	}
	public void loginCab(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		//HttpSession m_session = p_request.getSession(false);
		//AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		String phoneNo =p_request.getParameter("phoneNo");
		String cabNo =p_request.getParameter("cabNo");
		String driverId=YellowCabDAO.loginDriver(phoneNo,cabNo,"102","123");
		p_response.getWriter().write("Login=Succcess");
	}

	public void postSMS(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		
		String sms =p_request.getParameter("sms");
		//System.out.println("ycabmi:"+sms);
	}
}
