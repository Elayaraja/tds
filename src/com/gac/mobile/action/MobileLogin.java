package com.gac.mobile.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Category;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.charges.dao.ChargesDAO;
import com.common.util.HandleMemoryMessage;
import com.common.util.PasswordGen;
import com.common.util.PasswordHash;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.gac.mobile.dao.MobileDAO;
import com.tds.cmp.bean.CabRegistrationBean;
import com.tds.cmp.bean.CompanySystemProperties;
import com.tds.cmp.bean.OperatorShift;
import com.tds.controller.SystemUnavailableException;
import com.tds.controller.TDSController;
import com.tds.dao.CreditCardDAO;
import com.tds.dao.FinanceDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.DispatchPropertiesBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.VantivBO;

public class MobileLogin extends HttpServlet{
	private static final long serialVersionUID = 1L;
	private static Category cat =TDSController.cat;
	static {
		cat = Category.getInstance(MobileLogin.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given Servlet is "+MobileLogin.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		// cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		// cat.info("Class Name "+ ServiceRequestAction.class.getName());
		// cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			e.printStackTrace();
		}
	}

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		System.out.println("Driver Mobile login");
		String eventParam = "";
		//		String versionNo = "";
		//		boolean internalMethod = false;
		if (request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
			AdminRegistrationBO adminBO = new AdminRegistrationBO();
			if (eventParam.equalsIgnoreCase(TDSConstants.mobileLoginService)) {
				getMLoginService(request, response,adminBO);
			}
		}
	}
	
	public void getMLoginService(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException, SystemUnavailableException {
		
		/*if(request.getCharacterEncoding()!=null){
			System.out.println("char encodeing : "+request.getCharacterEncoding());
		}else{
			System.out.println("Re do char encoding");
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
		}*/
		
		String userName = "";
		String password = "";
		String passwordHash = "";
		String provider = "";
		String phoneno = "";
		String version = "";
		String cabno = "";
		int cab_type = 0;
		String temp_cab_no = "";
		String reason = "";
		String googleRegistrationKey = "";
		int status = 1;
		int mapstatus = 1;
		String ipAddress = request.getRemoteAddr();
		String driverAlarm = "0";
		String odoMeterValue = "";
		String docs="";
		String sessionId="";
		String timeoutdriver = "";
		String androidId = "";
		
		DriverLocationBO driverLocationBO = new DriverLocationBO();
		if (request.getParameter("loginName") != null) {
			userName = request.getParameter("loginName");
		}
		if (request.getParameter("cabNo") != null) {
			cabno = request.getParameter("cabNo");
			//System.out.println("cab No : "+cabno);
			//System.out.println("Specific cab no:"+java.net.URLDecoder.decode(request.getParameter("cabNo"), "UTF-8"));
		}
		if(request.getParameter("aId")!=null){
			androidId = request.getParameter("aId");
			System.out.println("aId:"+androidId+" for DriverID:"+userName+" with cab no : "+cabno);
		}
		if (request.getParameter("password") != null) {
			password = request.getParameter("password");
			passwordHash = PasswordHash.encrypt(password);
		}
		if (request.getParameter("provider") != null) {
			provider = request.getParameter("provider");
		}

		if (request.getParameter("phoneno") != null) {
			phoneno = "";
		}
		if (request.getParameter("version") != null) {
			version = request.getParameter("version");
		}
		
		if (request.getParameter("googRegKey") != null) {
			googleRegistrationKey = request.getParameter("googRegKey");
		}
		if (request.getParameter("odoMeterValue") != null) {
			odoMeterValue = request.getParameter("odoMeterValue");
		}
		if (!userName.equals("") && !password.equals("") && !cabno.equals("")) {
			adminBO = RegistrationDAO.getUserAvailable(userName, passwordHash, provider, phoneno, cabno);
			
			if (request.getParameter("cab_type") != null) {
				cab_type = Integer.parseInt(request.getParameter("cab_type"));
				temp_cab_no = cabno;
				if(cab_type!=0){
					cabno = MobileDAO.getCabNobySeqNo(adminBO.getAssociateCode(), cabno, cab_type);
				}
				System.out.println("Cabno : "+cabno+" changed from "+temp_cab_no);
				
			}else{
				System.out.println("No sequence number. Cabno : "+cabno);
			}
			
			// Check a driver status Active or Inactive status
			// TODO Do Driver active status only if above returns true
			if (adminBO.getIsAvailable() == 1) {
				timeoutdriver = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(),"TimeOutDriver");		
				int simultaneousLogin = MobileDAO.isLoggedIn(adminBO.getAssociateCode(), userName,cabno);
				boolean logoutSession = MobileDAO.logDriverOut(userName,cabno,timeoutdriver,adminBO.getMasterAssociateCode(),0);
				System.out.println("Check Driver <---"+userName+"---> For Timeout Logout While Login & Its--->"+logoutSession);
//				if(logoutSession){
//					MobileDAO.insertLogoutDetail("", adminBO.getUid(), adminBO.getUid(), "Timeout While Login",adminBO.getMasterAssociateCode());
//				}
				if (simultaneousLogin == 1 && !logoutSession) {
					reason = "Driver Already Logged In";
					RegistrationDAO.InsertLoginAttempts(adminBO.getUid(), passwordHash, ipAddress, reason);

					JSONArray array = new JSONArray();
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", reason);
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
					return;
				}
				if(adminBO.getPaymentUserID()==null || adminBO.getPaymentUserID().equals("")){
					CabRegistrationBean cbBean=MobileDAO.getPaymentId(adminBO.getAssociateCode(),cabno);
					adminBO.setPaymentUserID(cbBean.getPayId());
					adminBO.setPayPassword(cbBean.getPayPass());
				}
				status = MobileDAO.getDriverActive(adminBO);
				// For check a cab available or not for login driver
				if (status == 0) {
					if (SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "CheckCabNoOnLogin").equalsIgnoreCase("Yes")) {
						mapstatus = MobileDAO.getCabDriverMap(adminBO.getAssociateCode(), userName, cabno);
						if (mapstatus != 0) {
							reason = reason + " Cab is not Available";
							JSONArray array = new JSONArray();
							JSONObject loginValues = new JSONObject();
							try {
								loginValues.put("rV", 1.2);
								loginValues.put("Act", "LI");
								loginValues.put("rC", 400);
								loginValues.put("Login", "InValid");
								loginValues.put("Msg", reason);
								array.put(loginValues);
							} catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
							response.getWriter().write(array.toString());
							// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
							return;
						}
					} else {
						mapstatus = 0;
					}

				} else {
					reason = reason + "The Driver is Deactivated";
					JSONArray array = new JSONArray();
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", reason);
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
					// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
					return;
				}
				if (mapstatus == 0) {
					mapstatus = MobileDAO.checkCabAllocation(adminBO.getAssociateCode(), userName, cabno);
				}
			}

			/**
			 * @param request
			 * @param response
			 * @throws ServletException
			 * @throws IOException
			 * @author Venki
			 * @Check The driver should have the mandatory documents which the
			 *        company mentioned in their profile.
			 */
			if (mapstatus == 0 && adminBO.getCheckDocuments() == 1) {
				int resultOfDocumentCheck = MobileDAO.checkDocuments(adminBO.getUid(), adminBO.getAssociateCode());
				if (resultOfDocumentCheck == 0) {
					mapstatus = 1;
					reason = reason + "Mandatory Documents Not Available or Expired";
					JSONArray array = new JSONArray();
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", reason);
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
					// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
					return;
				}
			}
			CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());
			docs=MobileDAO.checkDocumentsGoingToExpity(adminBO.getUid(), adminBO.getAssociateCode());
			/**
			 * @param request
			 * @param response
			 * @throws ServletException
			 * @throws IOException
			 * @author Venki
			 * @Check Driver balance account before he logs in.If it is 0, then dont log him in and ask
			 * 		for a new account.
			 */

			if (mapstatus == 0 && cspBO.isCheckBalance()) {
				double balance = FinanceDAO.checkDriverBalance(userName,adminBO.getMasterAssociateCode());
				if (balance < cspBO.getDriverBalanceAmount()) {
					mapstatus = 1;
					reason = reason + "Your pre-paid card balance is not enough";
					JSONArray array = new JSONArray();
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", reason);
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
					// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
					return;
				}
			}
			
			//Check, if company wants to verify driver trying to Log-In from same mobile, not from different mobile
			//System.out.println("checkmobile:"+cspBO.getCheckDriverMobile());
			if (cspBO.getCheckDriverMobile()==1){
				if(androidId.equals("")){
					if(cspBO.getCheckDr_Mob_Version()==1){
						reason = reason + "Update new version from playstore and then Login";
						JSONArray array = new JSONArray();
						JSONObject loginValues = new JSONObject();
						try {
							loginValues.put("rV", 1.2);
							loginValues.put("Act", "LI");
							loginValues.put("rC", 400);
							loginValues.put("Login", "InValid");
							loginValues.put("Msg", reason);
							array.put(loginValues);
						} catch (Exception e) {
							e.printStackTrace();
							// TODO: handle exception
						}
						System.out.println("Reason:"+reason);
						response.getWriter().write(array.toString());
						// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
						return;
					}else{
						System.out.println("Check driver Android Id -> No AndroidId -> But skipped due to check version is NO");
					}
				}else{
					if(!adminBO.getAndroidId().equals("")){
						if(!adminBO.getAndroidId().equalsIgnoreCase(androidId)){
							reason = reason + "You can't login from this Mobile. Contact Administrator for more details";
							JSONArray array = new JSONArray();
							JSONObject loginValues = new JSONObject();
							try {
								loginValues.put("rV", 1.2);
								loginValues.put("Act", "LI");
								loginValues.put("rC", 400);
								loginValues.put("Login", "InValid");
								loginValues.put("Msg", reason);
								array.put(loginValues);
							} catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
							System.out.println("Reason:"+reason);
							response.getWriter().write(array.toString());
							// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
							return;
						}
					}else{
						//Insert android-Id
						RegistrationDAO.updateAndroidIdFirstTimeforDriver(androidId, adminBO.getUid(), adminBO.getAssociateCode());
					}
				}
			}
			
			// Check the login table to see if driver is already logged in. If
			// driver is logged in
			// if(status == 0 && mapstatus == 0) {

			// }
			if (status == 0 && mapstatus == 0) {
				String cabFlags = MobileDAO.getCabProperties(adminBO.getAssociateCode(), cabno);
				adminBO.setDriver_flag(adminBO.getDriver_flag() + cabFlags);

				OperatorShift osBean = new OperatorShift();
				if (adminBO.getIsAvailable() == 1) {
					HandleMemoryMessage.removeMessage(getServletContext(), adminBO.getMasterAssociateCode(), adminBO.getUid());
					//					getServletConfig().getServletContext().removeAttribute(adminBO.getUid() + "LO");

					adminBO.setGoogRegKey(googleRegistrationKey);
					adminBO.setVehicleNo(cabno);
					if (getServletConfig().getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "ZonesLoaded") == null) {
						SystemUtils.reloadZones(this, adminBO.getMasterAssociateCode());
					}
					adminBO.setCompanyList(SecurityDAO.getAllAssoccode(adminBO.getMasterAssociateCode()));
					for(int i=0;i<adminBO.getCompanyList().size();i++){
						if(!adminBO.getCompanyList().get(i).contains(adminBO.getMasterAssociateCode())){
							adminBO.setChangeCompanyCode(adminBO.getCompanyList().get(i));
							break;
						}
					}
					ArrayList<OperatorShift> shiftDetail = RequestDAO.readAllOperator(adminBO.getMasterAssociateCode(), adminBO.getUid());
					if (shiftDetail != null && shiftDetail.size() > 0) {
						osBean.setAssociateCode(adminBO.getMasterAssociateCode());
						osBean.setOperatorId(adminBO.getUid());
						osBean.setStatus("B");
						osBean.setKey(shiftDetail.get(0).getKey());
						osBean.setClosedBy(adminBO.getUid());
						osBean.setVehicleNo(adminBO.getVehicleNo());

						RequestDAO.updateShiftRegisterDetail(osBean);
					} else {
						osBean.setOperatorId(adminBO.getUid());
						osBean.setAssociateCode(adminBO.getMasterAssociateCode());
						osBean.setStatus("A");
						osBean.setOpenedBy(adminBO.getUid());
						osBean.setVehicleNo(adminBO.getVehicleNo());
						osBean.setOdometerValue(Double.parseDouble(odoMeterValue == null ? "0.0" : odoMeterValue.equals("") ? "0.0" : odoMeterValue));
						adminBO.setShiftMode(1);
						RequestDAO.insertShiftRegisterMaster(osBean);
					}

					sessionId = PasswordGen.generatePassword("A", 32);

					String mobVersion = request.getParameter("mobVersion")==null?"0.0.0":request.getParameter("mobVersion");
					RegistrationDAO.InsertLoginDetail(adminBO.getAssociateCode(), adminBO.getUid(), sessionId, "1", 0, System.currentTimeMillis(), adminBO.getProvider(), adminBO.getPhnumber(), version, adminBO.getDriver_flag(), cabno, googleRegistrationKey, ipAddress, adminBO.getMasterAssociateCode(),mobVersion);
					adminBO.setDefaultLanguage(cspBO.getDefaultLanguage());
					adminBO.setCalculateZone(cspBO.getCalculateZones());
					adminBO.setPhonePrefix(cspBO.getMobilePrefix());
					adminBO.setSmsPrefix(cspBO.getSmsPrefix());
					adminBO.setSendPushToCustomer(cspBO.getSendPushToCustomer());
					int locationStatus = 0;
					driverLocationBO.setLatitude("0");
					driverLocationBO.setLongitude("0");

					driverLocationBO.setDriverid(adminBO.getUid());
					driverLocationBO.setAssocCode(adminBO.getAssociateCode());
					driverLocationBO.setPhone(adminBO.getPhnumber());
					driverLocationBO.setProvider(adminBO.getProvider());
					driverLocationBO.setPushKey(googleRegistrationKey);
					driverLocationBO.setProfile(adminBO.getDriver_flag());
					driverLocationBO.setVehicleNo(cabno);
					driverLocationBO.setAppVersion(version);
					driverLocationBO.setDrName(adminBO.getUserNameDisplay());
					driverLocationBO.setDriverRatings(adminBO.getDriverRating());
					String loginTime = ServiceRequestDAO.getLoginTime(adminBO.getAssociateCode(), cspBO.getWindowTime(), adminBO.getUid());
					RequestDAO.updateCabNumber(adminBO, driverLocationBO);
					driverAlarm = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "driverAlarm");
					if (driverAlarm == null || driverAlarm.equals("")) {
						driverAlarm = "0";
						adminBO.setDriverAlarm(0);
					} else {
						adminBO.setDriverAlarm(Integer.parseInt(driverAlarm));
					}
					if (!SecurityDAO.getDriverAvailabilityStatus(adminBO, adminBO.getDriverAlarm())) {
						driverLocationBO.setStatus("N");
					} else {
						driverLocationBO.setStatus("Y");
					}
					
					ServiceRequestDAO.updateDriverPhoneAndProvider(driverLocationBO, loginTime, cspBO.getTimeZoneArea(),adminBO.getMasterAssociateCode());
					String wasl_ccode = TDSProperties.getValue("WASL_Company");
					if(adminBO.getAssociateCode().equals(wasl_ccode)){
						ArrayList<CabRegistrationBean> cab_list = RequestDAO.getCabSummary(adminBO.getAssociateCode(), cabno, "", "", "");
						JSONArray array = new JSONArray();
						try {
							JSONObject obj = new JSONObject();
							if(cab_list!=null && cab_list.size()>0){
								obj.put("vehicleReferenceNumber", cab_list.get(0).getCab_reference_id());
							}else{
								obj.put("vehicleReferenceNumber", "");
							}
							
							obj.put("captainReferenceNumber", adminBO.getWasl_reference_id());
							array.put(obj);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						RegistrationDAO.updateWaslReferenceInLocation(array.toString(), adminBO.getAssociateCode(), driverLocationBO.getDriverid(), cabno);
					}					

					DispatchPropertiesBO dispatchCallBO = SystemPropertiesDAO.getDispatchCallPropeties(adminBO.getMasterAssociateCode());
					adminBO.setCall_onAccept(dispatchCallBO.getCall_onAccept());
					adminBO.setCall_onRoute(dispatchCallBO.getCall_onRoute());
					adminBO.setCall_onSite(dispatchCallBO.getCall_onSite());
					adminBO.setMessage_onAccept(dispatchCallBO.getMessage_onAccept());
					adminBO.setMessage_onRoute(dispatchCallBO.getMessage_onRoute());
					adminBO.setMessage_onSite(dispatchCallBO.getMessage_onSite());
					adminBO.setMessageAccept(dispatchCallBO.getMessageAccept());
					adminBO.setMessageRoute(dispatchCallBO.getMessageRoute());
					adminBO.setMessageSite(dispatchCallBO.getMessageSite());
					adminBO.setJob_No_Show_Property(cspBO.getNo_show());
					adminBO.setSessionId(sessionId);
				}
			} else {
				reason = reason + "UserName or password is incorrect";
				cat.info("Reason--->" + reason);
				JSONArray array = new JSONArray();
				JSONObject loginValues = new JSONObject();
				try {
					loginValues.put("rV", 1.2);
					loginValues.put("Act", "LI");
					loginValues.put("rC", 400);
					loginValues.put("Login", "InValid");
					loginValues.put("Msg", reason);
					array.put(loginValues);
				} catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
				response.getWriter().write(array.toString());
				// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
				return;
			}

		} else {
			reason = reason + "UserName And Password Not Provided";
			cat.info("Reason--->" + reason);
			JSONArray array = new JSONArray();
			JSONObject loginValues = new JSONObject();
			try {
				loginValues.put("rV", 1.2);
				loginValues.put("Act", "LI");
				loginValues.put("rC", 400);
				loginValues.put("Login", "InValid");
				loginValues.put("Msg", reason);
				array.put(loginValues);
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			response.getWriter().write(array.toString());
			// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
			return;
		}
		ArrayList<ChargesBO> charBo=ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),3);
		String ccAmount="0.00";
		if(charBo.size()>0){
			ccAmount = charBo.get(0).getPayTypeAmount();
		}
		if (sessionId != null && !sessionId.equals("")) {

			OperatorShift osBean = new OperatorShift();
			osBean.setAssociateCode(adminBO.getMasterAssociateCode());
			osBean.setOperatorId(adminBO.getUid());
			// After enter Operator Id check whether the register is opened or closed
			OperatorShift registerOpen = RequestDAO.readShiftRegisterMaster(osBean.getAssociateCode(), osBean.getOperatorId());
			if (registerOpen.isRegisterOpen()) {

			} else {
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setStatus("A");
				osBean.setOpenedBy(adminBO.getUid());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setStatus("A");
				osBean.setOpenedBy(adminBO.getUid());
				adminBO.setShiftMode(1);
				RequestDAO.insertShiftRegisterMaster(osBean);
			}
			if (odoMeterValue != null && !odoMeterValue.equals("")) {
				MobileDAO.insertOdoMeterValues(adminBO.getAssociateCode(), odoMeterValue, "", adminBO.getUid(), adminBO.getVehicleNo(), "Login");
			}
			int odoMetervalue = MobileDAO.getLastUpdatedOdoMeterValue(adminBO.getAssociateCode(), adminBO.getVehicleNo());
			CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());

			String meterValues = MobileDAO.getCabDefaultMeterRate(adminBO.getAssociateCode(), cabno, adminBO.getMasterAssociateCode());
			try{
				JSONArray meterArray = new JSONArray(meterValues);
				if(meterArray!=null && meterArray.length()>0){
					JSONObject meterObject = meterArray.getJSONObject(0);
					cspBO.setMinimumSpeed(meterObject.getString("minimunSpeed"));
					cspBO.setRatePerMile(Double.parseDouble(meterObject.getString("ratePerMile")));
					cspBO.setRatePerMinute(Double.parseDouble(meterObject.getString("ratePerMinute")));
					cspBO.setStartAmount(meterObject.getString("startAmount"));
					adminBO.setDefaultMeterRate("YES");
				} else {
					adminBO.setDefaultMeterRate("NO");
				}

			}catch(JSONException e){
				e.printStackTrace();
			}
			
			adminBO.setDriverListMobile(cspBO.getDriverListMobile());
			adminBO.setEnable_stadd_flag(cspBO.getEnable_stAdd_flagTrip());
			
			ArrayList<String> versions = SystemPropertiesDAO.getVersion(adminBO.getMasterAssociateCode());
			if(adminBO.getPaymentUserID()==null || adminBO.getPaymentUserID().equals("")){
				VantivBO vantivBean = CreditCardDAO.getVantivUserIdPassword(adminBO);
				adminBO.setPaymentUserID(vantivBean.getUserId());
				adminBO.setPayPassword(vantivBean.getPassword());
			}
			String dbDate = MobileDAO.getCurrentTime(adminBO);
			String[] dateAndTime = dbDate.split(" ");
			String ymd = dateAndTime[0];
			String[] time = dateAndTime[1].split(":");
			String hour = time[0];
			String minute = time[1];

			ObjectMapper mapper = new ObjectMapper();
			String finalJSON = mapper.writeValueAsString(adminBO);
			System.out.println("Insert Session Values For Driver While Login--->"+adminBO.getUid());
			MobileDAO.insertSessionObjects(sessionId,finalJSON.replaceAll("null", "''"),adminBO.getUid());
			String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
			int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
			if(isUpdateGPS==1){
				ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), "", "", cabno, 0, "");
			}
			JSONArray array = new JSONArray();
			JSONObject loginValues = new JSONObject();
			try {
				loginValues.put("rV", 1.2);
				loginValues.put("Act", "LI");
				loginValues.put("rC", 200);
				loginValues.put("Login", "Valid");
				loginValues.put("SessionID", sessionId);
				loginValues.put("GPID", TDSProperties.getValue("GCMPID"));
				loginValues.put("DAT", driverAlarm);
				loginValues.put("zoneLogout", cspBO.getZoneLogoutTime());
				loginValues.put("minimumSpeed", cspBO.getMinimumSpeed());
				loginValues.put("ratePerMile", cspBO.getRatePerMile());
				loginValues.put("ratePerMinute", cspBO.getRatePerMinute());
				loginValues.put("startAmount", cspBO.getStartAmount());
				loginValues.put("odoMeterValue", odoMetervalue);
				loginValues.put("meter", cspBO.isMeterMandatory());
				loginValues.put("startMeter", cspBO.isStartMeterAutomatically());
				loginValues.put("hideMeter", cspBO.isStartmeterHidden());
				loginValues.put("hideMeter", cspBO.isStartmeterHidden());
				loginValues.put("noProgressAlert", cspBO.getNoProgress());
				loginValues.put("meterOnAlert", cspBO.getMeterOn());
				loginValues.put("remoteNoTripAlert", cspBO.getRemoteNoTrip());
				loginValues.put("noTripTooSoonAlert", cspBO.getNoTripTooSoon());
				loginValues.put("distanceNoProgressAlert", cspBO.getDistanceNoProgress());
				loginValues.put("distanceMeterOnAlert", cspBO.getDistanceMeterOn());
				loginValues.put("distanceRemoteNOTripAlert", cspBO.getDistanceRemoteNoTrip());
				loginValues.put("distanceNoTripTooSoonAlert", cspBO.getDistanceNoTripTooSoon());
				loginValues.put("ccAmt", ccAmount);
				loginValues.put("mV", cspBO.getMeterTypes());
				loginValues.put("pr1", cspBO.getPreline1());
				loginValues.put("pr2", cspBO.getPreline2());
				loginValues.put("pr3", cspBO.getPreline3());
				loginValues.put("pr4", cspBO.getPreline4());
				loginValues.put("pr5", cspBO.getPreline5());
				loginValues.put("po1", cspBO.getPostline1());
				loginValues.put("po2", cspBO.getPostline2());
				loginValues.put("po3", cspBO.getPostline3());
				loginValues.put("po4", cspBO.getPostline4());
				loginValues.put("po5", cspBO.getPostline5());
				loginValues.put("po6", cspBO.getPostline6());
				loginValues.put("po7", cspBO.getPostline7());
				loginValues.put("da", driverLocationBO.getStatus());
				loginValues.put("drN", adminBO.getUserNameDisplay());
				loginValues.put("ymd", ymd);
				loginValues.put("hour", hour);
				loginValues.put("minute", minute);
				loginValues.put("jobAction", cspBO.getJobAction());
				loginValues.put("drDirection", cspBO.getDriverDirection()==1?true:false);
				loginValues.put("autoBooking", cspBO.isBookAutomatically());
				loginValues.put("paymentForJobs", cspBO.getPaymentMandatory());
				loginValues.put("Vantiv_Id", adminBO.getPaymentUserID());
				loginValues.put("Vantiv_pass", adminBO.getPayPassword());
				loginValues.put("MeterRate", adminBO.getDefaultMeterRate());
				loginValues.put("customerAppPayment", cspBO.getPaymentForCustomerApp());
				loginValues.put("zonesStep", cspBO.getZonesStepaway());
				loginValues.put("seeZoneStatus",cspBO.getSeeZoneStatus());
				loginValues.put("sysOption", adminBO.getSettingsAccess()==0?true:false);
				loginValues.put("name", adminBO.getUserNameDisplay());
				if(!docs.equals("")){
					loginValues.put("loginMessage", "Your document "+docs+" is about to expire");
				}
				for (int i = 0; i < versions.size(); i = i + 2) {
					loginValues.put(versions.get(i) ,versions.get(i+1));
				}
				array.put(loginValues);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.getWriter().write(array.toString());
		} else {
			cat.info("Reason--->" + reason);
			JSONArray array = new JSONArray();
			JSONObject loginValues = new JSONObject();
			try {
				loginValues.put("rV", 1.2);
				loginValues.put("Act", "LI");
				loginValues.put("rC", 400);
				loginValues.put("Login", "InValid");
				loginValues.put("Msg", reason);
				array.put(loginValues);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			response.getWriter().write(array.toString());
			RegistrationDAO.InsertLoginAttempts(adminBO.getUid(), passwordHash, ipAddress, reason);
		}
	}
}
