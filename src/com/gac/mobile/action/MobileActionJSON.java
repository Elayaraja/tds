package com.gac.mobile.action;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;
import org.apache.woden.wsdl20.Description;
import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.charges.constant.ICCConstant;
import com.charges.dao.ChargesDAO;
import com.common.action.PaymentProcessingAction;
import com.common.util.CheckSpecialFlags;
import com.common.util.DistanceCalculation;
import com.common.util.Feed;
import com.common.util.GetAddress;
import com.common.util.HandleMemoryMessage;
import com.common.util.MessageGenerateJSON;
import com.common.util.OpenRequestUtil;
import com.common.util.PasswordGen;
import com.common.util.PasswordHash;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.common.util.TDSValidation;
import com.common.util.sendSMS;
import com.gac.mobile.dao.MobileDAO;
import com.google.gson.Gson;
import com.ios.push.CertificateBO;
import com.logica.smpp.debug.Debug;
import com.lowagie.text.pdf.codec.Base64.InputStream;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.tds.cmp.bean.CabRegistrationBean;
import com.tds.cmp.bean.CompanySystemProperties;
import com.tds.cmp.bean.DriverAndJobs;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverRestrictionBean;
import com.tds.cmp.bean.MessageBeen;
import com.tds.cmp.bean.MessageInMemory;
import com.tds.cmp.bean.MeterType;
import com.tds.cmp.bean.OperatorShift;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.MailReport;
import com.tds.controller.SystemUnavailableException;
import com.tds.controller.TDSController;
import com.tds.dao.AuditDAO;
import com.tds.dao.CreditCardDAO;
import com.tds.dao.CustomerMobileDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.EmailSMSDAO;
import com.tds.dao.MessageDAO;
import com.tds.dao.FinanceDAO;
import com.tds.dao.ProcessingDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SharedRideDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.VoiceMessageDAO;
import com.tds.dao.ZoneDAO;
import com.tds.ivr.IVRDriverPassengerConf;
import com.tds.ivr.IVRPassengerCallerOutBound;
import com.tds.payment.PaymentCatgories;
import com.tds.payment.PaymentGateway;
import com.tds.payment.PaymentGatewaySlimCD;
import com.tds.pp.bean.PaymentProcessBean;
import com.common.util.CheckZone;
import com.common.util.Messaging;
import com.common.util.SendingSMS;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.CreditCardBO;
import com.tds.tdsBO.CustomerMobileBO;
import com.tds.tdsBO.DispatchPropertiesBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.tds.tdsBO.VantivBO;
import com.tds.tdsBO.VoucherBO;
import com.tds.util.CustomerUtil;
import com.twilio.sdk.TwilioRestException;

public class MobileActionJSON extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//private final Category cat = Category.getInstance(MobileActionJSON.class.getName());
	private static Category cat =TDSController.cat;

	String topic        = "MQTT Examples";
    String content      = "Message from MqttPublishSample";
    int qos             = 2;
    String broker       = "tcp://chat.takeuz.com:1883";
    String clientId     = "JavaSample";
    MemoryPersistence persistence = new MemoryPersistence();
    MqttClient sampleClient=null;
	static {
		cat = Category.getInstance(MobileActionJSON.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+MobileActionJSON.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		// cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		// cat.info("Class Name "+ ServiceRequestAction.class.getName());
		// cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			e.printStackTrace();
		}
	}

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		String eventParam = "";
		//		String versionNo = "";
		//		boolean internalMethod = false;
		if (request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
			if(!eventParam.equalsIgnoreCase("vup") && !eventParam.equalsIgnoreCase("UploadVoiceDriver")){
				request.setCharacterEncoding("UTF-8");
				response.setCharacterEncoding("UTF-8");
			}
		}
		//		if (request.getParameter("version") != null) {
		//			versionNo = request.getParameter("version");
		//		}
		String sessionId = "";
		Cookie[] delCookies = request.getCookies();
		if (delCookies != null) {
			for (int i = 0; i < delCookies.length; i++) {
				if (delCookies[i].getName().equals("JSESSIONID")) {
					sessionId = delCookies[i].getValue();
					break;
				}
			}
		}
		if(sessionId==null || sessionId.equals("")){
			String myUrl = request.getRequestURL().toString();
			if(myUrl.contains("jsessionid")){
				String[] sessionParse = myUrl.split("jsessionid=");
				sessionId = sessionParse[1];
			}
		}
		AdminRegistrationBO adminBO = new AdminRegistrationBO();
		StringBuffer sessionValues = MobileDAO.getSessionValues(sessionId);
		
		if (sessionId != null && !sessionId.equals("") && sessionValues!=null && sessionValues.length()>10 && !eventParam.equalsIgnoreCase(TDSConstants.mobileLoginService)) {
			adminBO = new Gson().fromJson(sessionValues.toString(), AdminRegistrationBO.class); 
			if (eventParam.equalsIgnoreCase(TDSConstants.mobileDriverLocation)) {
				setMDriverLocation(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("dlfe")) {
				setMDriverLocationFrontEnd(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("cca")) {
				chckUserCardInfo(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("ccadj")) {
				alterPayment(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("authorizeAndVoid")) {
				authorizeAndVoid(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("cCStatus")) {
				getCreditCardProcessingStatus(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("ccv")) {
				voidPayment(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("vvalid")) {
				validateVoucher(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("vup")) {
				voucherUpdate(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("zoneLogout")) {
				zoneLogout(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("makeCall")) {
				callPassenger(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("vsign")) {
				getVoucherSign(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("getMStatus")) {
				getMStatus(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("getDriverUnsettle")) {
				getDriverUnSettle(request, response,adminBO);	
			}else if (eventParam.equalsIgnoreCase("getMdriSettle")) {
				getMdriSettle(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("uploadVoiceFromOperator1")) {
				uploadVoiceFromOperator(request, response);
			}else if (eventParam.equalsIgnoreCase("X")) {
				getVoucherOrCC(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("loginToQueue")) {
				loginToQueue(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileDriverQueue)) {
				setMQueueLocation(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobilelogout)) {
				mobileLogout(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileAcceptTrip)) {
				setMDriverAcceptance(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileonRouteToPickup)) {
				setMOnRouteToPickup(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileRejectTrip)) {
				setMDriverRejection(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileJobNoResponse)) {
				setMJobNoResponse(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("endTrip")) {
				geMtEndTrip(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("endRoute")) {
				endRoute(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("startTrip")) {
				getMStartTrip(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("stc")) {
				jobSTC(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("sFTrip")) {
				getStartFlagTrip(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("startFlagTrip")) {
				startFlagTrip(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileNoShow)) {
				getMNoShow(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileIsPayment)) {
				getMCheckPayment(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileMakePayment)) {
				getMMakePayment(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileOpenRequest)) {
				// getMOpenRequest(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileTripDetail)) {
				getMTripDetail(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileAcceptedTripDetail)) {
				getMAcceptedTripDetail(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobilePublicTripDetail)) {
				getMPublicRequest(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileUploadSignature)) {
				gettingSignature(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.totalDriverinQueueNo)) {
				getNoOfDriverinQueueNo(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileQueueDetail)) {
				getQueueDataList(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("zoneall")) {
				getCompleteQueueDataList(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileQueueBoundries)) {
				getQueueBoundries(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileGetZonePosition)) {
				getZonePosition(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileGetZonesAndJobsSummary)) {
				getZoneJobSummary(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("autoReLogin")) {
				autoRelogin(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("companyJobStatusSummary")) {
				companyJobStatusSummary(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("mVoid")) {
				getMVoid(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("numberOfDriversAndJobs")) {
				getNumberOfDriversAndJobs(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("numberOfDrivers")) {
				getNumberOfDrivers(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("allocatedJobs")) {
				allocatedJobs(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("checkJobStatus")) {
				checkJobStatus(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("driverLocation")) {
				driverLocation(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("driverList")) {
				driverList(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("getFullMessage")) {
				getFullMessage(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("mAllocatedTrips")) {
				getAllocatedTrips(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("checkEndtrip")) {
				checkEndtrip(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("makeDriverUnavailable")) {
				makeDriverUnavailable(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("ugregkey")) {
				updateGooglePushId(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("changePassword")) {
				changePassword(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("sendLogFile")) {
				getLogFile(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("onSite")) {
				setMOnSite(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("getChargeTypes")) {
				getChargeTypes(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("insertChargeTypes")) {
				insertChargeTypes(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("shiftBreak")) {
				shiftBreak(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("insertCharges")) {
				insertCharges(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("insertChargesFromMobile")) {
				insertChargesFromMobile(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("getChargesForTrip")) {
				getChargesForTrip(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("UpdatePaymentAmount")) {
				updatePaymentForTrip(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("getMinimumSpeedAndRatePerMile")) {
				getMinimumSpeedAndRatePerMile(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("updateOdoMeterValue")) {
				updateOdoMeterValue(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("getLastUpdateOdoMeterValues")) {
				getLastUpdateOdoMeterValues(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("sendMessage")) {
				sendMessage(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("currentZoneStatus")) {
				driverZonesList(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("currentZoneHisStatus")) {
				driverZonesHistory(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("sNAMsg")) {
				sendNotAvailabeMessage(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("getCannedMessages")) {
				getCannedMessages(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("customerApp")) {
				customerApp(request, response);
			} else if (eventParam.equalsIgnoreCase("getMeterTypes")) {
				getMeterTypes(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("submitOnsiteError")) {
				submitOnsiteError(request, response,adminBO);
			} else if (eventParam.equalsIgnoreCase("taxidriverlocation")) {
				taxiChargeDriverLocation(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("msgAck")) {
				sendAck(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("getDelayMessage")) {
				getDelayMessage(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("getDriverDetails")) {
				getDriverDocument(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("driverrating")) {
				driverRating(request, response);
			}else if (eventParam.equalsIgnoreCase("ccToServer")) {
				ccToServer(request, response,adminBO);
			}else if(eventParam.equalsIgnoreCase("DownloadVoice")) {			 
				downloadVoice(request,response,adminBO);
			}else if(eventParam.equalsIgnoreCase("UploadVoiceDriver")) {			 
				uploadVoiceFromDriver(request,response,adminBO);
			}else if (eventParam.equalsIgnoreCase("mVerify")) {
				verifyPrepaidCards(request, response,adminBO);
			}else if (eventParam.equalsIgnoreCase("getAllDriverDetails")) {
				getAllDriverDetails(request, response);
			} else if(eventParam.equalsIgnoreCase("Ping")){
				JSONArray array = new JSONArray();
				JSONObject jasonObject = new JSONObject();
				try {
					jasonObject.put("rV", 1.2);
					jasonObject.put("Act", "Ping");
					jasonObject.put("rC", 200);
					jasonObject.put("MSG", "Alive");				
				} catch (JSONException e) {
					e.printStackTrace();
				}
				array.put(jasonObject);
				System.out.println("Driver Ping For Session -> "+sessionId==null?"":sessionId+" ----- "+array.toString());
				response.getWriter().write(array.toString());
			}
		} else if (eventParam.equalsIgnoreCase(TDSConstants.mobileLoginService)) {
			getMLoginService(request, response,adminBO);
		} else if (eventParam.equalsIgnoreCase("mVerify")) {
			verifyPrepaidCards(request, response,adminBO);
		} else if(eventParam.equalsIgnoreCase("forgetPassword")){
			forgetPassword(request,response);
		} else if (eventParam.equalsIgnoreCase("getLatestVersion")) {
			getLatestVersion(request, response,adminBO);
		} else if(eventParam.equalsIgnoreCase("getPassKey")){
			getPasswordKey(request,response);
		} else if(eventParam.equalsIgnoreCase(TDSConstants.mobileDriverLocation)){
			JSONArray array = new JSONArray();
			JSONObject jasonObject = new JSONObject();
			System.out.println("Driver Logout Message For Session--->"+sessionId==null?"":sessionId);
			try {
				jasonObject.put("rV", 1.2);
				jasonObject.put("RC", "false");
				jasonObject.put("Act", "DLU");
				jasonObject.put("rC", 400);
				jasonObject.put("MSG", "Your Session Expired");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			array.put(jasonObject);
			response.getWriter().write(array.toString());
		}else if (eventParam.equalsIgnoreCase("getAllDriverDetails")) {
			getAllDriverDetails(request, response);
		} else if (eventParam.equalsIgnoreCase("uploadVoiceFromOperator1")) {
			uploadVoiceFromOperator(request, response);
		} else if(eventParam.equalsIgnoreCase("Ping")){
			JSONArray array = new JSONArray();
			JSONObject jasonObject = new JSONObject();
			try {
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "Ping");
				jasonObject.put("rC", 400);
				jasonObject.put("MSG", "Your Session Expired");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			array.put(jasonObject);
			System.out.println("Driver Ping For expired Session -> "+sessionId==null?"":sessionId+" ----- "+array.toString());
			response.getWriter().write(array.toString());
			return;
		}
	}


	private void getDelayMessage(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO)throws ServletException, IOException{
		JSONArray array=new JSONArray();
		try {
			ArrayList<MessageBeen> resp=MessageDAO.getDelayMessage(adminBO.getUid(),adminBO.getTimeZone());
			for(int i=0;i<resp.size();i++)
			{
				JSONObject obj=new JSONObject();
				obj.put("", resp.get(i).getMessage());
				obj.put("", resp.get(i).getMessageId());
				obj.put("",resp.get(i).getDeliverTime());
				array.put(obj);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (org.codehaus.jettison.json.JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	private void sendAck(HttpServletRequest request,HttpServletResponse response,AdminRegistrationBO adminBO) {
		MessageDAO.setMessageAck(adminBO.getUid(),request.getParameter("msgId"),adminBO.getTimeZone());
	}

	public void getMVoid(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {

		PaymentProcessBean processBean = new PaymentProcessBean();
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");

		processBean.setB_trans_id(request.getParameter("transid") == null ? "" : request.getParameter("transid"));

		processBean = ChargesDAO.chckReturnOrUndo(processBean);

		processBean.setMerchid(adminBO.getMerchid());
		processBean.setServiceid(adminBO.getServiceid());
		processBean = PaymentProcessingAction.voidPayment(poolBO, processBean, adminBO.getAssociateCode());

		if (processBean.getB_approval_status().equalsIgnoreCase("1")) {
			response.getWriter().write("Transaction Voided");
		} else if (processBean.getB_approval_status().equalsIgnoreCase("2")) {
			response.getWriter().write("Transaction Declined");
		} else {
			response.getWriter().write("Failure to Void");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
	}

	public void verifyPrepaidCards(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String cardNumber = request.getParameter("cNum");
		String pinNumber = request.getParameter("cPin");
		String driverId = request.getParameter("loginName");
		String password="";
		try {
			password = PasswordHash.encrypt(request.getParameter("password"));
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean result=FinanceDAO.checkPrepaidCard(cardNumber,pinNumber,driverId,password);
		JSONArray resultArray = new JSONArray();
		JSONObject message = new JSONObject();
		try {
			if(result){
				message.put("Verify", "Valid");
			}
			message.put("rV", 1.2);
			message.put("Act", "LI");
			message.put("rC", 200);
			message.put("message", result);
			resultArray.put(message);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		response.getWriter().write(resultArray.toString());
	}
	//	public void runInternalMethod(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
	//
	//		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletConfig().getServletContext().getAttribute("poolBO");
	//		OpenRequestBO opBo = (OpenRequestBO) (request.getSession()).getAttribute("ORA");
	//		ArrayList al = new ArrayList();
	//		al.add(ServiceRequestDAO.getDriverPhoneNumberForQueue(opBo.getDriverid()));
	//		response.getWriter().write("TDSTDS;A;" + opBo.getTripid() + ";" + opBo.getSttime() + ";" + "" + opBo.getDriverid() + ";" + System.currentTimeMillis() + ";" + opBo.getQueueno() + ";" + opBo.getQC_SMS_RES_TIME());
	//		if (request.getParameter("version") != null) {
	//			String version = request.getParameter("version");
	//		}
	//	}

	public void sentErrorReport(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		response.setContentType("text/xml");
		JSONArray mainArray = new JSONArray();
		JSONArray subArray = new JSONArray();
		try{
			JSONObject jasonObject = new JSONObject();
			jasonObject.put("rV", 1.2);
			jasonObject.put("rC", 400);
			jasonObject.put("D", "Your Session Expired");
			subArray.put(jasonObject);
			mainArray.put(0,subArray);
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(mainArray.toString());
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
	}

	public void getMLoginService(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException, SystemUnavailableException {
		String userName = "";
		String password = "";
		String passwordHash = "";
		String provider = "";
		String phoneno = "";
		String version = "";
		String cabno = "";
		int cab_type = 0;
		String temp_cab_no = "";
		String reason = "";
		String googleRegistrationKey = "";
		int status = 1;
		int mapstatus = 1;
		int loginStatus = 1;
		String ipAddress = request.getRemoteAddr();
		String driverAlarm = "0";
		String odoMeterValue = "";
		String docs="";
		String minimunSpeed = "";
		String ratePerMinute = "";
		String ratePerMile = "";
		String startAmount = "";
		String sessionId="";
		String timeoutdriver = "";
		String androidId = "";
		
		DriverLocationBO driverLocationBO = new DriverLocationBO();
		if (request.getParameter("loginName") != null) {
			userName = request.getParameter("loginName");
		}
		if(request.getParameter("aId")!=null){
			androidId = request.getParameter("aId");
			System.out.println("aId:"+androidId+" for DriverID:"+userName);
		}
		if (request.getParameter("password") != null) {
			password = request.getParameter("password");
			passwordHash = PasswordHash.encrypt(password);
		}
		if (request.getParameter("provider") != null) {
			provider = request.getParameter("provider");
		}

		if (request.getParameter("phoneno") != null) {
			phoneno = "";
		}
		if (request.getParameter("version") != null) {
			version = request.getParameter("version");
		}
		if (request.getParameter("cabNo") != null) {
			cabno = request.getParameter("cabNo");
		}
		if (request.getParameter("googRegKey") != null) {
			googleRegistrationKey = request.getParameter("googRegKey");
		}
		if (request.getParameter("odoMeterValue") != null) {
			odoMeterValue = request.getParameter("odoMeterValue");
		}
		if (!userName.equals("") && !password.equals("") && !cabno.equals("")) {
			adminBO = RegistrationDAO.getUserAvailable(userName, passwordHash, provider, phoneno, cabno);
			
			if (request.getParameter("cab_type") != null) {
				cab_type = Integer.parseInt(request.getParameter("cab_type"));
				temp_cab_no = cabno;
				if(cab_type!=0){
					cabno = MobileDAO.getCabNobySeqNo(adminBO.getAssociateCode(), cabno, cab_type);
				}
				System.out.println("Cabno : "+cabno+" changed from "+temp_cab_no);
			}else{
				System.out.println("No sequence number. Cabno : "+cabno);
			}
			
			// Check a driver status Active or Inactive status
			// TODO Do Driver active status only if above returns true
			if (adminBO.getIsAvailable() == 1) {
				timeoutdriver = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(),"TimeOutDriver");		
				int simultaneousLogin = MobileDAO.isLoggedIn(adminBO.getAssociateCode(), userName,cabno);
				boolean logoutSession = MobileDAO.logDriverOut(userName,cabno,timeoutdriver,adminBO.getMasterAssociateCode(),0);
				System.out.println("Check Driver <---"+userName+"---> For Timeout Logout While Login & Its--->"+logoutSession);
//				if(logoutSession){
//					MobileDAO.insertLogoutDetail("", adminBO.getUid(), adminBO.getUid(), "Timeout While Login",adminBO.getMasterAssociateCode());
//				}
				if (simultaneousLogin == 1 && !logoutSession) {
					reason = "Driver Already Logged In";
					RegistrationDAO.InsertLoginAttempts(adminBO.getUid(), passwordHash, ipAddress, reason);

					JSONArray array = new JSONArray();
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", reason);
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
					return;
				}
				if(adminBO.getPaymentUserID()==null || adminBO.getPaymentUserID().equals("")){
					CabRegistrationBean cbBean=MobileDAO.getPaymentId(adminBO.getAssociateCode(),cabno);
					adminBO.setPaymentUserID(cbBean.getPayId());
					adminBO.setPayPassword(cbBean.getPayPass());
				}
				status = MobileDAO.getDriverActive(adminBO);
				// For check a cab available or not for login driver
				if (status == 0) {
					if (SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "CheckCabNoOnLogin").equalsIgnoreCase("Yes")) {
						mapstatus = MobileDAO.getCabDriverMap(adminBO.getAssociateCode(), userName, cabno);
						if (mapstatus != 0) {
							reason = reason + " Cab is not Available";
							JSONArray array = new JSONArray();
							JSONObject loginValues = new JSONObject();
							try {
								loginValues.put("rV", 1.2);
								loginValues.put("Act", "LI");
								loginValues.put("rC", 400);
								loginValues.put("Login", "InValid");
								loginValues.put("Msg", reason);
								array.put(loginValues);
							} catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
							response.getWriter().write(array.toString());
							// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
							return;
						}
					} else {
						mapstatus = 0;
					}

				} else {
					reason = reason + "The Driver is Deactivated";
					JSONArray array = new JSONArray();
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", reason);
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
					// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
					return;
				}
				if (mapstatus == 0) {
					mapstatus = MobileDAO.checkCabAllocation(adminBO.getAssociateCode(), userName, cabno);
				}
			}

			/**
			 * @param request
			 * @param response
			 * @throws ServletException
			 * @throws IOException
			 * @author Venki
			 * @Check The driver should have the mandatory documents which the
			 *        company mentioned in their profile.
			 */
			if (mapstatus == 0 && adminBO.getCheckDocuments() == 1) {
				int resultOfDocumentCheck = MobileDAO.checkDocuments(adminBO.getUid(), adminBO.getAssociateCode());
				if (resultOfDocumentCheck == 0) {
					mapstatus = 1;
					reason = reason + "Mandatory Documents Not Available or Expired";
					JSONArray array = new JSONArray();
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", reason);
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
					// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
					return;
				}
			}
			CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());
			docs=MobileDAO.checkDocumentsGoingToExpity(adminBO.getUid(), adminBO.getAssociateCode());
			/**
			 * @param request
			 * @param response
			 * @throws ServletException
			 * @throws IOException
			 * @author Venki
			 * @Check Driver balance account before he logs in.If it is 0, then dont log him in and ask
			 * 		for a new account.
			 */

			if (mapstatus == 0 && cspBO.isCheckBalance()) {
				double balance = FinanceDAO.checkDriverBalance(userName,adminBO.getMasterAssociateCode());
				if (balance < cspBO.getDriverBalanceAmount()) {
					mapstatus = 1;
					reason = reason + "Your pre-paid card balance is not enough";
					JSONArray array = new JSONArray();
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", reason);
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
					// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
					return;
				}
			}
			
			//Check, if company wants to verify driver trying to Log-In from same mobile, not from different mobile
			//System.out.println("checkmobile:"+cspBO.getCheckDriverMobile());
			if (cspBO.getCheckDriverMobile()==1){
				if(androidId.equals("")){
					if(cspBO.getCheckDr_Mob_Version()==1){
						reason = reason + "Update new version from playstore and then Login";
						JSONArray array = new JSONArray();
						JSONObject loginValues = new JSONObject();
						try {
							loginValues.put("rV", 1.2);
							loginValues.put("Act", "LI");
							loginValues.put("rC", 400);
							loginValues.put("Login", "InValid");
							loginValues.put("Msg", reason);
							array.put(loginValues);
						} catch (Exception e) {
							e.printStackTrace();
							// TODO: handle exception
						}
						System.out.println("Reason:"+reason);
						response.getWriter().write(array.toString());
						// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
						return;
					}else{
						System.out.println("Check driver Android Id -> No AndroidId -> But skipped due to check version is NO");
					}
				}else{
					if(!adminBO.getAndroidId().equals("")){
						if(!adminBO.getAndroidId().equalsIgnoreCase(androidId)){
							reason = reason + "You can't login from this Mobile. Contact Administrator for more details";
							JSONArray array = new JSONArray();
							JSONObject loginValues = new JSONObject();
							try {
								loginValues.put("rV", 1.2);
								loginValues.put("Act", "LI");
								loginValues.put("rC", 400);
								loginValues.put("Login", "InValid");
								loginValues.put("Msg", reason);
								array.put(loginValues);
							} catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
							System.out.println("Reason:"+reason);
							response.getWriter().write(array.toString());
							// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
							return;
						}
					}else{
						//Insert android-Id
						RegistrationDAO.updateAndroidIdFirstTimeforDriver(androidId, adminBO.getUid(), adminBO.getAssociateCode());
					}
				}
			}
			
			// Check the login table to see if driver is already logged in. If
			// driver is logged in
			// if(status == 0 && mapstatus == 0) {

			// }
			if (status == 0 && mapstatus == 0) {
				String cabFlags = MobileDAO.getCabProperties(adminBO.getAssociateCode(), cabno);
				adminBO.setDriver_flag(adminBO.getDriver_flag() + cabFlags);

				OperatorShift osBean = new OperatorShift();
				if (adminBO.getIsAvailable() == 1) {
					HandleMemoryMessage.removeMessage(getServletContext(), adminBO.getMasterAssociateCode(), adminBO.getUid());
					//					getServletConfig().getServletContext().removeAttribute(adminBO.getUid() + "LO");

					adminBO.setGoogRegKey(googleRegistrationKey);
					adminBO.setVehicleNo(cabno);
					if (getServletConfig().getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "ZonesLoaded") == null) {
						SystemUtils.reloadZones(this, adminBO.getMasterAssociateCode());
					}
					adminBO.setCompanyList(SecurityDAO.getAllAssoccode(adminBO.getMasterAssociateCode()));
					for(int i=0;i<adminBO.getCompanyList().size();i++){
						if(!adminBO.getCompanyList().get(i).contains(adminBO.getMasterAssociateCode())){
							adminBO.setChangeCompanyCode(adminBO.getCompanyList().get(i));
							break;
						}
					}
					ArrayList<OperatorShift> shiftDetail = RequestDAO.readAllOperator(adminBO.getMasterAssociateCode(), adminBO.getUid());
					if (shiftDetail != null && shiftDetail.size() > 0) {
						osBean.setAssociateCode(adminBO.getMasterAssociateCode());
						osBean.setOperatorId(adminBO.getUid());
						osBean.setStatus("B");
						osBean.setKey(shiftDetail.get(0).getKey());
						osBean.setClosedBy(adminBO.getUid());
						osBean.setVehicleNo(adminBO.getVehicleNo());

						RequestDAO.updateShiftRegisterDetail(osBean);
					} else {
						osBean.setOperatorId(adminBO.getUid());
						osBean.setAssociateCode(adminBO.getMasterAssociateCode());
						osBean.setStatus("A");
						osBean.setOpenedBy(adminBO.getUid());
						osBean.setVehicleNo(adminBO.getVehicleNo());
						osBean.setOdometerValue(Double.parseDouble(odoMeterValue == null ? "0.0" : odoMeterValue.equals("") ? "0.0" : odoMeterValue));
						adminBO.setShiftMode(1);
						RequestDAO.insertShiftRegisterMaster(osBean);
					}

					sessionId = PasswordGen.generatePassword("A", 32);

					String mobVersion = request.getParameter("mobVersion")==null?"0.0.0":request.getParameter("mobVersion");
					RegistrationDAO.InsertLoginDetail(adminBO.getAssociateCode(), adminBO.getUid(), sessionId, "1", 0, System.currentTimeMillis(), adminBO.getProvider(), adminBO.getPhnumber(), version, adminBO.getDriver_flag(), cabno, googleRegistrationKey, ipAddress, adminBO.getMasterAssociateCode(),mobVersion);
					adminBO.setDefaultLanguage(cspBO.getDefaultLanguage());
					adminBO.setCalculateZone(cspBO.getCalculateZones());
					adminBO.setPhonePrefix(cspBO.getMobilePrefix());
					adminBO.setSmsPrefix(cspBO.getSmsPrefix());
					adminBO.setSendPushToCustomer(cspBO.getSendPushToCustomer());
					int locationStatus = 0;
					driverLocationBO.setLatitude("0");
					driverLocationBO.setLongitude("0");

					driverLocationBO.setDriverid(adminBO.getUid());
					driverLocationBO.setAssocCode(adminBO.getAssociateCode());
					driverLocationBO.setPhone(adminBO.getPhnumber());
					driverLocationBO.setProvider(adminBO.getProvider());
					driverLocationBO.setPushKey(googleRegistrationKey);
					driverLocationBO.setProfile(adminBO.getDriver_flag());
					driverLocationBO.setVehicleNo(cabno);
					driverLocationBO.setAppVersion(version);
					driverLocationBO.setDrName(adminBO.getUserNameDisplay());
					driverLocationBO.setDriverRatings(adminBO.getDriverRating());
					String loginTime = ServiceRequestDAO.getLoginTime(adminBO.getAssociateCode(), cspBO.getWindowTime(), adminBO.getUid());
					RequestDAO.updateCabNumber(adminBO, driverLocationBO);
					driverAlarm = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "driverAlarm");
					if (driverAlarm == null || driverAlarm.equals("")) {
						driverAlarm = "0";
						adminBO.setDriverAlarm(0);
					} else {
						adminBO.setDriverAlarm(Integer.parseInt(driverAlarm));
					}
					if (!SecurityDAO.getDriverAvailabilityStatus(adminBO, adminBO.getDriverAlarm())) {
						driverLocationBO.setStatus("N");
					} else {
						driverLocationBO.setStatus("Y");
					}
					
					ServiceRequestDAO.updateDriverPhoneAndProvider(driverLocationBO, loginTime, cspBO.getTimeZoneArea(),adminBO.getMasterAssociateCode());
					String wasl_ccode = TDSProperties.getValue("WASL_Company");
					if(adminBO.getAssociateCode().equals(wasl_ccode)){
						ArrayList<CabRegistrationBean> cab_list = RequestDAO.getCabSummary(adminBO.getAssociateCode(), cabno, "", "", "");
						JSONArray array = new JSONArray();
						try {
							JSONObject obj = new JSONObject();
							if(cab_list!=null && cab_list.size()>0){
								obj.put("vehicleReferenceNumber", cab_list.get(0).getCab_reference_id());
							}else{
								obj.put("vehicleReferenceNumber", "");
							}
							
							obj.put("captainReferenceNumber", adminBO.getWasl_reference_id());
							array.put(obj);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						RegistrationDAO.updateWaslReferenceInLocation(array.toString(), adminBO.getAssociateCode(), driverLocationBO.getDriverid(), cabno);
					}					

					DispatchPropertiesBO dispatchCallBO = SystemPropertiesDAO.getDispatchCallPropeties(adminBO.getMasterAssociateCode());
					adminBO.setCall_onAccept(dispatchCallBO.getCall_onAccept());
					adminBO.setCall_onRoute(dispatchCallBO.getCall_onRoute());
					adminBO.setCall_onSite(dispatchCallBO.getCall_onSite());
					adminBO.setMessage_onAccept(dispatchCallBO.getMessage_onAccept());
					adminBO.setMessage_onRoute(dispatchCallBO.getMessage_onRoute());
					adminBO.setMessage_onSite(dispatchCallBO.getMessage_onSite());
					adminBO.setMessageAccept(dispatchCallBO.getMessageAccept());
					adminBO.setMessageRoute(dispatchCallBO.getMessageRoute());
					adminBO.setMessageSite(dispatchCallBO.getMessageSite());
					adminBO.setJob_No_Show_Property(cspBO.getNo_show());
					adminBO.setSessionId(sessionId);
				}
			} else {
				reason = reason + "UserName or password is incorrect";
				cat.info("Reason--->" + reason);
				JSONArray array = new JSONArray();
				JSONObject loginValues = new JSONObject();
				try {
					loginValues.put("rV", 1.2);
					loginValues.put("Act", "LI");
					loginValues.put("rC", 400);
					loginValues.put("Login", "InValid");
					loginValues.put("Msg", reason);
					array.put(loginValues);
				} catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
				response.getWriter().write(array.toString());
				// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
				return;
			}

		} else {
			reason = reason + "UserName And Password Not Provided";
			cat.info("Reason--->" + reason);
			JSONArray array = new JSONArray();
			JSONObject loginValues = new JSONObject();
			try {
				loginValues.put("rV", 1.2);
				loginValues.put("Act", "LI");
				loginValues.put("rC", 400);
				loginValues.put("Login", "InValid");
				loginValues.put("Msg", reason);
				array.put(loginValues);
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			response.getWriter().write(array.toString());
			// response.getWriter().write("ResVer=1.1;Login=InValid;reason="+reason);
			return;
		}
		ArrayList<ChargesBO> charBo=ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),3);
		String ccAmount="0.00";
		if(charBo.size()>0){
			ccAmount = charBo.get(0).getPayTypeAmount();
		}
		if (sessionId != null && !sessionId.equals("")) {

			OperatorShift osBean = new OperatorShift();
			osBean.setAssociateCode(adminBO.getMasterAssociateCode());
			osBean.setOperatorId(adminBO.getUid());
			// After enter Operator Id check whether the register is opened or closed
			OperatorShift registerOpen = RequestDAO.readShiftRegisterMaster(osBean.getAssociateCode(), osBean.getOperatorId());
			if (registerOpen.isRegisterOpen()) {

			} else {
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setStatus("A");
				osBean.setOpenedBy(adminBO.getUid());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setStatus("A");
				osBean.setOpenedBy(adminBO.getUid());
				adminBO.setShiftMode(1);
				RequestDAO.insertShiftRegisterMaster(osBean);
			}
			if (odoMeterValue != null && !odoMeterValue.equals("")) {
				MobileDAO.insertOdoMeterValues(adminBO.getAssociateCode(), odoMeterValue, "", adminBO.getUid(), adminBO.getVehicleNo(), "Login");
			}
			int odoMetervalue = MobileDAO.getLastUpdatedOdoMeterValue(adminBO.getAssociateCode(), adminBO.getVehicleNo());
			CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());

			String meterValues = MobileDAO.getCabDefaultMeterRate(adminBO.getAssociateCode(), cabno, adminBO.getMasterAssociateCode());
			try{
				JSONArray meterArray = new JSONArray(meterValues);
				if(meterArray!=null && meterArray.length()>0){
					JSONObject meterObject = meterArray.getJSONObject(0);
					cspBO.setMinimumSpeed(meterObject.getString("minimunSpeed"));
					cspBO.setRatePerMile(Double.parseDouble(meterObject.getString("ratePerMile")));
					cspBO.setRatePerMinute(Double.parseDouble(meterObject.getString("ratePerMinute")));
					cspBO.setStartAmount(meterObject.getString("startAmount"));
					adminBO.setDefaultMeterRate("YES");
				} else {
					adminBO.setDefaultMeterRate("NO");
				}

			}catch(JSONException e){
				e.printStackTrace();
			}
			
			adminBO.setDriverListMobile(cspBO.getDriverListMobile());
			adminBO.setEnable_stadd_flag(cspBO.getEnable_stAdd_flagTrip());
			
			ArrayList<String> versions = SystemPropertiesDAO.getVersion(adminBO.getMasterAssociateCode());
			if(adminBO.getPaymentUserID()==null || adminBO.getPaymentUserID().equals("")){
				VantivBO vantivBean = CreditCardDAO.getVantivUserIdPassword(adminBO);
				adminBO.setPaymentUserID(vantivBean.getUserId());
				adminBO.setPayPassword(vantivBean.getPassword());
			}
			String dbDate = MobileDAO.getCurrentTime(adminBO);
			String[] dateAndTime = dbDate.split(" ");
			String ymd = dateAndTime[0];
			String[] time = dateAndTime[1].split(":");
			String hour = time[0];
			String minute = time[1];

			ObjectMapper mapper = new ObjectMapper();
			String finalJSON = mapper.writeValueAsString(adminBO);
			System.out.println("Insert Session Values For Driver While Login--->"+adminBO.getUid());
			MobileDAO.insertSessionObjects(sessionId,finalJSON.replaceAll("null", "''"),adminBO.getUid());
			String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
			int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
			if(isUpdateGPS==1){
				ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), "", "", cabno, 0, "");
			}
			JSONArray array = new JSONArray();
			JSONObject loginValues = new JSONObject();
			try {
				loginValues.put("rV", 1.2);
				loginValues.put("Act", "LI");
				loginValues.put("rC", 200);
				loginValues.put("Login", "Valid");
				loginValues.put("SessionID", sessionId);
				loginValues.put("GPID", TDSProperties.getValue("GCMPID"));
				loginValues.put("DAT", driverAlarm);
				loginValues.put("zoneLogout", cspBO.getZoneLogoutTime());
				loginValues.put("minimumSpeed", cspBO.getMinimumSpeed());
				loginValues.put("ratePerMile", cspBO.getRatePerMile());
				loginValues.put("ratePerMinute", cspBO.getRatePerMinute());
				loginValues.put("startAmount", cspBO.getStartAmount());
				loginValues.put("odoMeterValue", odoMetervalue);
				loginValues.put("meter", cspBO.isMeterMandatory());
				loginValues.put("startMeter", cspBO.isStartMeterAutomatically());
				loginValues.put("hideMeter", cspBO.isStartmeterHidden());
				loginValues.put("noProgressAlert", cspBO.getNoProgress());
				loginValues.put("meterOnAlert", cspBO.getMeterOn());
				loginValues.put("remoteNoTripAlert", cspBO.getRemoteNoTrip());
				loginValues.put("noTripTooSoonAlert", cspBO.getNoTripTooSoon());
				loginValues.put("distanceNoProgressAlert", cspBO.getDistanceNoProgress());
				loginValues.put("distanceMeterOnAlert", cspBO.getDistanceMeterOn());
				loginValues.put("distanceRemoteNOTripAlert", cspBO.getDistanceRemoteNoTrip());
				loginValues.put("distanceNoTripTooSoonAlert", cspBO.getDistanceNoTripTooSoon());
				loginValues.put("ccAmt", ccAmount);
				loginValues.put("mV", cspBO.getMeterTypes());
				loginValues.put("pr1", cspBO.getPreline1());
				loginValues.put("pr2", cspBO.getPreline2());
				loginValues.put("pr3", cspBO.getPreline3());
				loginValues.put("pr4", cspBO.getPreline4());
				loginValues.put("pr5", cspBO.getPreline5());
				loginValues.put("po1", cspBO.getPostline1());
				loginValues.put("po2", cspBO.getPostline2());
				loginValues.put("po3", cspBO.getPostline3());
				loginValues.put("po4", cspBO.getPostline4());
				loginValues.put("po5", cspBO.getPostline5());
				loginValues.put("po6", cspBO.getPostline6());
				loginValues.put("po7", cspBO.getPostline7());
				loginValues.put("da", driverLocationBO.getStatus());
				loginValues.put("drN", adminBO.getUserNameDisplay());
				loginValues.put("ymd", ymd);
				loginValues.put("hour", hour);
				loginValues.put("minute", minute);
				loginValues.put("jobAction", cspBO.getJobAction());
				loginValues.put("drDirection", cspBO.getDriverDirection()==1?true:false);
				loginValues.put("autoBooking", cspBO.isBookAutomatically());
				loginValues.put("paymentForJobs", cspBO.getPaymentMandatory());
				loginValues.put("Vantiv_Id", adminBO.getPaymentUserID());
				loginValues.put("Vantiv_pass", adminBO.getPayPassword());
				loginValues.put("MeterRate", adminBO.getDefaultMeterRate());
				loginValues.put("customerAppPayment", cspBO.getPaymentForCustomerApp());
				loginValues.put("zonesStep", cspBO.getZonesStepaway());
				loginValues.put("seeZoneStatus",cspBO.getSeeZoneStatus());
				loginValues.put("sysOption", adminBO.getSettingsAccess()==0?true:false);
				if(!docs.equals("")){
					loginValues.put("loginMessage", "Your document "+docs+" is about to expire");
				}
				loginValues.put("PS",cspBO.getEnable_power_save());
				
				for (int i = 0; i < versions.size(); i = i + 2) {
					loginValues.put(versions.get(i) ,versions.get(i+1));
				}
				array.put(loginValues);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.getWriter().write(array.toString());
		} else {
			cat.info("Reason--->" + reason);
			JSONArray array = new JSONArray();
			JSONObject loginValues = new JSONObject();
			try {
				loginValues.put("rV", 1.2);
				loginValues.put("Act", "LI");
				loginValues.put("rC", 400);
				loginValues.put("Login", "InValid");
				loginValues.put("Msg", reason);
				array.put(loginValues);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			response.getWriter().write(array.toString());
			RegistrationDAO.InsertLoginAttempts(adminBO.getUid(), passwordHash, ipAddress, reason);
		}
	}

	public void customerApp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = null;
		dispatcher = getServletContext().getRequestDispatcher("CustomerApp/Home.jsp");
		dispatcher.forward(request, response);
		// ServletRequest request;
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
	}

	public void getVoucherSign(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {

		String content_length = request.getHeader("content-length");

		try {
			BufferedReader reader1 = request.getReader();
			int a = 0, counter = 0;
			byte[] bytearray = new byte[Integer.parseInt(content_length)];
			while ((a = reader1.read()) != -1) {
				bytearray[counter] = (byte) a;
				counter++;
			}

			String trans_id = request.getParameter("transid") == null ? "" : request.getParameter("transid");
			ProcessingDAO.updateVoucherSign(trans_id, bytearray);
			if (request.getParameter("version") != null) {
				String version = request.getParameter("version");
			}
			response.getWriter().write("Received");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void getVoucherOrCC(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String content_length = request.getHeader("content-length");
		try {
			BufferedReader reader1 = request.getReader();
			int a = 0, counter = 0;
			byte[] bytearray = new byte[Integer.parseInt(content_length)];
			while ((a = reader1.read()) != -1) {
				bytearray[counter] = (byte) a;
				counter++;
			}
			String trans_id = request.getParameter("transid") == null ? "" : request.getParameter("transid");
			String trans_id2 = request.getParameter("transid") == null ? "" : request.getParameter("transid");
			if (UtilityDAO.isExistOrNot("select * from TDS_VOUCHER_DETAIL where VD_TXN_ID = '" + trans_id + "'"))
				ProcessingDAO.updateVoucherSign(trans_id, bytearray);
			else {
				ChargesDAO.updateSignIntoCC(bytearray, trans_id, trans_id2);
			}
			if (request.getParameter("version") != null) {
				String version = request.getParameter("version");
			}
			response.getWriter().write("Received");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void mobileLogout(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		OperatorShift osBean = new OperatorShift();
		
		String logout_message = "";
		if(request.getParameter("debugData")!=null ){
			System.out.println("Debug Data for DriverId:"+adminBO.getUid()+"--->"+request.getParameter("debugData") );
			logout_message = request.getParameter("debugData");
		}else{
			System.out.println("Logout without debug data for DriverId:"+adminBO.getUid() );
			logout_message = "Driver Logout From Mobile";
		}
		
		String latidue = request.getParameter("stLa");
		String longitude = request.getParameter("stLo");
		if (latidue == null || latidue.equals("")) {
			latidue = "0.000000";
		}
		if (longitude == null || longitude.equals("")) {
			longitude = "0.000000";
		}
		
		String driverId = "";
		String result="";
		if (adminBO != null) {
			String session_id = adminBO.getSessionId();
			ArrayList<OperatorShift> osShift = RequestDAO.readAllOperator(adminBO.getMasterAssociateCode(), adminBO.getUid());
			if (request.getParameter("endShift") != null && request.getParameter("endShift").equals("2")) {
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setStatus("A");
				osBean.setKey(osShift.size()>0?osShift.get(0).getKey():0);
				osBean.setClosedBy(adminBO.getUid());
				RequestDAO.updateShiftRegisterDetail(osBean);
			} else {
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setStatus("L");
				osBean.setKey(osShift.size()>0?osShift.get(0).getKey():0);
				osBean.setClosedBy(adminBO.getUid());
				osBean.setOdometerValue(Integer.parseInt(request.getParameter("odoMeterReading") == null ? "0" : request.getParameter("odoMeterReading")));
				RequestDAO.closeShiftRegisterMaster(osBean);
			}
			if (request.getParameter("odoMeterReading") != null && !request.getParameter("odoMeterReading").equals("")) {
				MobileDAO.insertOdoMeterValues(adminBO.getAssociateCode(), request.getParameter("odoMeterReading"), "", adminBO.getUid(), adminBO.getVehicleNo(), "Logout");
			}
			RegistrationDAO.InsertLoginDetail(adminBO.getAssociateCode(), adminBO.getUname(), session_id, "0", 0, System.currentTimeMillis(), adminBO.getProvider(), adminBO.getPhnumber(), "", "", "", "", "", adminBO.getMasterAssociateCode(),"");
			driverId = adminBO.getUname();

			ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
			if(currentQueue!=null && currentQueue.size()>0){
				ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+"-"+currentQueue.get(1)+". Due to driver logged out himself.");
			}
			MobileDAO.removeSessionObjects(session_id,adminBO.getUid());
			MobileDAO.insertLogoutDetail(session_id, adminBO.getUid(), adminBO.getUid(), logout_message ,adminBO.getMasterAssociateCode());
			ServiceRequestDAO.deleteDriverFromQueue(driverId, adminBO.getMasterAssociateCode());
			ServiceRequestDAO.updateDriverAvailabilty("L", driverId, adminBO.getAssociateCode());
			HandleMemoryMessage.removeMessage(getServletContext(), adminBO.getAssociateCode(), adminBO.getUid());

			String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
			int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
			if(isUpdateGPS==1){
				//ServiceRequestDAO.insertDriverGPS(associationCode, driverLocBO.getDriverid(), driverLocBO.getLatitude(), driverLocBO.getLongitude(), driverLocBO.getVehicleNo(), 8, "");
				ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), latidue, longitude, adminBO.getVehicleNo(), 1, "");
			}
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		JSONArray array = new JSONArray();
		JSONObject jobj = new JSONObject();
		try {
			jobj.put("rV", 1.2);
			jobj.put("Act", "LO");
			jobj.put("rC", 200);
			jobj.put("Status", "LoggedOut");
			array.put(jobj);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().print(array.toString());
	}

	private double roundDecimal(double value, final int decimalPlace) {
		BigDecimal bd = new BigDecimal(value);

		bd = bd.setScale(decimalPlace, RoundingMode.HALF_UP);
		value = bd.doubleValue();

		return value;
	}
	
	public void setMDriverLocation(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		DriverLocationBO driverLocationBO = new DriverLocationBO();
		String userType = "";
		String tripForNoShow = "";
		OpenRequestBO openBo = null;
		String[] messageNoShow = new String[2];
		boolean checkNoShowStatus = false;
		int gpsFeedTime=0;
		if (request.getParameter("latitude") != null) {
			driverLocationBO.setLatitude(request.getParameter("latitude"));
		} else {
			driverLocationBO.setLatitude("0");
		}
		if (request.getParameter("longitude") != null) {
			driverLocationBO.setLongitude(request.getParameter("longitude"));
		} else {
			driverLocationBO.setLongitude("0");
		}
		
		if (adminBO != null) {
			String timeoutdriver = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(),"TimeOutDriver");		
			boolean logoutSession = MobileDAO.logDriverOut(adminBO.getUid(),adminBO.getVehicleNo(),timeoutdriver.equals("0")?"30":timeoutdriver,adminBO.getMasterAssociateCode(),1);
			if(logoutSession){
				MobileDAO.insertLogoutDetail("", adminBO.getUid(), adminBO.getUid(), "Timeout While Location Update",adminBO.getMasterAssociateCode());
				ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
				if(currentQueue!=null && currentQueue.size()>0){
					ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". For driver logout session expired");
				}
				ServiceRequestDAO.deleteDriverFromQueue(adminBO.getUid(), adminBO.getMasterAssociateCode());
				JSONArray array = new JSONArray();
				JSONObject jasonObject = new JSONObject();
				try {
					jasonObject.put("rV", 1.2);
					jasonObject.put("RC", "false");
					jasonObject.put("Act", "DLU");
					jasonObject.put("rC", 400);
					jasonObject.put("MSG", "Your Session Expired");
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
				int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
				if(isUpdateGPS==1){
					ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), driverLocationBO.getLatitude(), driverLocationBO.getLongitude(), adminBO.getVehicleNo(), 1, "");
				}
				
				array.put(jasonObject);
				System.out.println("Driver logout --> Session Expired : DriverId:"+adminBO.getUid());
				response.getWriter().write(array.toString());
				return;
			}
			userType = adminBO.getUsertypeDesc();
			driverLocationBO.setDriverid(adminBO.getUid());
			String availableStatus=adminBO.getDriverAvailability();
			if (request.getParameter("availability") != null) {
				driverLocationBO.setStatus(request.getParameter("availability"));
				adminBO.setDriverAvailability(driverLocationBO.getStatus());
			} else {
				driverLocationBO.setStatus("0");
			}
			if (request.getParameter("version") != null) {
				String version = request.getParameter("version");
			}
			
			driverLocationBO.setAssocCode(adminBO.getAssociateCode());
			driverLocationBO.setVehicleNo(adminBO.getVehicleNo());
			
			//System.out.println("send message to cusomet:"+adminBO.getSendPushToCustomer());
			if(adminBO.getSendPushToCustomer()==1){
				sendGooglePushToCustomerIfDriverAccepts(driverLocationBO, adminBO);
			}
			
			if(request.getParameter("gpsfeeddealytime")!=null){
				gpsFeedTime = Integer.parseInt(request.getParameter("gpsfeeddealytime").equals("")?"0":request.getParameter("gpsfeeddealytime"));
			}
			if(gpsFeedTime >10){
				System.out.println("Driver-"+adminBO.getUid()+" feed delay is "+gpsFeedTime);
			}
			if(adminBO.getDriverAvailability().equals("N") && !adminBO.getDriverAvailability().equalsIgnoreCase(availableStatus)){
				ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
				if(currentQueue!=null && currentQueue.size()>0){
					ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". On driver location update");
				}
				ServiceRequestDAO.deleteDriverFromQueue(adminBO.getUid(), adminBO.getMasterAssociateCode());
			}
			if (request.getParameter("checkNoShow") != null && !request.getParameter("checkNoShow").equalsIgnoreCase("")) {
				checkNoShowStatus = true;
				tripForNoShow = request.getParameter("tripId");

				String jobsCode = DispatchDAO.getCompCode(tripForNoShow, adminBO.getMasterAssociateCode());
				if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
					openBo = RequestDAO.getOpenRequestStatus(tripForNoShow, adminBO,adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					openBo = RequestDAO.getOpenRequestStatus(tripForNoShow, adminBO, myString);
				}
				if (openBo != null) {
					if (openBo.getTripStatus().equals(TDSConstants.performingDispatchProcesses) || openBo.getTripStatus().equals(TDSConstants.newRequestDispatchProcessesNotStarted)) {
						messageNoShow = MessageGenerateJSON.generateMessage(openBo, "A", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
					} else {
						messageNoShow = MessageGenerateJSON.generateMessageAfterAcceptance(openBo, "AA", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
					}
				} else {
					JSONArray array = new JSONArray();
					JSONObject jasonObject = new JSONObject();
					try {
						jasonObject.put("rV", 1.2);
						jasonObject.put("Act", "DLU");
						jasonObject.put("rC", 400);
						jasonObject.put("Command", "NA");
						jasonObject.put("TI", tripForNoShow);
						jasonObject.put("D", "Job Not Available");
						jasonObject.put("TS", 99);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					array.put(jasonObject);
					messageNoShow[0] = array.toString();
				}
			}
			int messageHandlerType = request.getParameter("v") != null ? 1 : 0;
			
			String message = HandleMemoryMessage.readMessage(getServletContext(), adminBO.getMasterAssociateCode(), adminBO.getUid(), driverLocationBO, messageHandlerType,adminBO.getChangeCompanyCode());
			
			String debugdata = request.getParameter("debugData");
			if(debugdata!=null && !debugdata.equals("")){
				System.out.println("Debug data:"+debugdata);
				String distance = "";
				try {
					JSONArray array = new JSONArray(debugdata);
					JSONObject jobj = array.getJSONObject(0);
					double dist = Double.parseDouble((jobj.getString("LD")!=null && !jobj.getString("LD").equals(""))?jobj.getString("LD"):"0.0");
					//System.out.println("dist:"+dist);
					distance = roundDecimal(dist, 2) +"";
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("distance:"+distance);
				if(distance!=null && !distance.equals("") && !distance.equals("0.0")){
					ServiceRequestDAO.updateDriverDistance(driverLocationBO, distance);
				}
			}
			ObjectMapper mapper = new ObjectMapper();
			String finalJSON = mapper.writeValueAsString(adminBO);
			//			System.out.println("Insert/Update Session Values For Driver--->"+adminBO.getUid()+" For Location Update");
			MobileDAO.insertSessionObjects(adminBO.getSessionId(),finalJSON.replaceAll("null", "''"),adminBO.getUid());

			//read
			if (!message.equals("")) {
				response.getWriter().write(message);
			} else {
				if (checkNoShowStatus) {
					response.getWriter().write(messageNoShow[0]);
				} else {
					JSONArray array = new JSONArray();
					JSONObject jasonObject = new JSONObject();
					try {
						jasonObject.put("rV", 1.2);
						jasonObject.put("Act", "DLU");
						jasonObject.put("rC", 200);
						jasonObject.put("LocationUpdate", "Success");
					} catch (JSONException e) {
						e.printStackTrace();
					}
					array.put(jasonObject);
					response.getWriter().write(array.toString());
				}
			}
		}
	}

	public void shiftBreak(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		int shiftKey = RequestDAO.getShiftKey(adminBO);
		OperatorShift osbean = new OperatorShift();
		osbean.setKey(shiftKey);
		osbean.setOperatorId(adminBO.getUid());
		osbean.setOpenedBy(adminBO.getUid());
		osbean.setStatus("A");
		osbean.setAssociateCode(adminBO.getMasterAssociateCode());
		RequestDAO.updateShiftRegisterDetail(osbean);
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write("ResVer=1.1; Operator Break Success");

	}

	public void setMDriverLocationFrontEnd(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		DriverLocationBO driverLocationBO = new DriverLocationBO();
		String userType = "";
		//		StringBuffer buffer = new StringBuffer();
		JSONArray mainArray = new JSONArray();
		//		int queueStatus = -1;
		int status;
		int gpsFeedTime=0;
		if (adminBO != null) {
			userType = adminBO.getUsertypeDesc();
			driverLocationBO.setDriverid(adminBO.getUid());
		}
		driverLocationBO.setStatus(request.getParameter("a") != null ? request.getParameter("a") : "N");
		if (request.getParameter("la") != null) {
			driverLocationBO.setLatitude(request.getParameter("la"));
		} else {
			driverLocationBO.setLatitude("0");
		}
		if (request.getParameter("lo") != null) {
			driverLocationBO.setLongitude(request.getParameter("lo"));
		} else {
			driverLocationBO.setLongitude("0");
		}
		if(request.getParameter("gpsfeeddealytime")!=null){
			gpsFeedTime = Integer.parseInt(request.getParameter("gpsfeeddealytime").equals("")?"0":request.getParameter("gpsfeeddealytime"));
		}
		if(gpsFeedTime >10){
			System.out.println("Driver-"+adminBO.getUid()+" feed delay is "+gpsFeedTime);
		}

		// String
		// alarmTime=SecurityDAO.getAlarmTimeForDriverId(adminBO,adminBO.getDriverAlarm());
		// Check whether driver has a job that is due now. If he has dont allow
		// driver to be marked as avaialble
		if (driverLocationBO.getStatus().equals("Y")) {
			if (!SecurityDAO.getDriverAvailabilityStatus(adminBO, adminBO.getDriverAlarm())) {
				SecurityDAO.setDriverStatus("N", adminBO);
				driverLocationBO.setStatus("N");
				adminBO.setDriverAvailability("N");
			} else {
				OpenRequestBO openBo = RequestDAO.getHoldJobs(adminBO.getUid(), adminBO.getVehicleNo(), adminBO.getAssociateCode(), adminBO.getTimeZone());
				//	status=RequestDAO.updatedriverZone(openBo,adminBO);
				if (openBo != null) {
					// if(ServiceRequestDAO.updateOpenRequestStatus(adminBO.getAssociateCode(),openBo.getTripid(),
					// TDSConstants.performingDispatchProcesses+"" ,
					// adminBO.getUid(), "", adminBO.getVehicleNo(),"" )>0){
					ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
					DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getMasterAssociateCode(), adminBO.getUid(), 1);
					// if(openBo.getTripStatus()!=null &&
					// openBo.getTripStatus().equals("99")){
					openBo.setTripStatus(TDSConstants.performingDispatchProcesses + "");
					// }
					String[] message = MessageGenerateJSON.generateMessage(openBo, "A", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
					Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
					oneSMS.start();
					mainArray.put(message);
					// }
				}
				//				if (request.getParameter("version") != null) {
				//					String version = request.getParameter("version");
				//				}
			}
			String stepAway = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "zonesStep");
			if(!stepAway.equals("")){
				int stepAwayTime = Integer.parseInt(stepAway);
				String queueNo=RequestDAO.getCheckAvailabilty(adminBO,stepAwayTime);
				if(!queueNo.equals("")){
					if(ZoneDAO.getShortDistanceType(adminBO.getMasterAssociateCode(), queueNo)==1){
						driverLocationBO.setStatus("Z");
					}
				}
			}

			int shiftKey = RequestDAO.getShiftKey(adminBO);
			OperatorShift osbean = new OperatorShift();
			osbean.setKey(shiftKey);
			osbean.setOperatorId(adminBO.getUid());
			osbean.setOpenedBy(adminBO.getUid());
			osbean.setStatus("B");
			osbean.setAssociateCode(adminBO.getMasterAssociateCode());
			RequestDAO.updateShiftRegisterDetail(osbean);

		} else {
			adminBO.setDriverAvailability(driverLocationBO.getStatus());
			ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
			if(currentQueue!=null && currentQueue.size()>0){
				ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". On driver changing his availability");
			}
			ServiceRequestDAO.deleteDriverFromQueue(adminBO.getUid(), adminBO.getMasterAssociateCode());
			status=RequestDAO.updatedriverZone(adminBO);
		}
		//		if (request.getParameter("version") != null) {
		//			String version = request.getParameter("version");
		//		}

		//TODO Handling driver shift changes here. Have to look for the other possible places
		
		ObjectMapper mapper = new ObjectMapper();
		String finalJSON = mapper.writeValueAsString(adminBO);
		System.out.println("Insert/Update Session Values For Driver--->"+adminBO.getUid()+" For Location Update FE Status--->"+driverLocationBO.getStatus());
		MobileDAO.insertSessionObjects(adminBO.getSessionId(),finalJSON.replaceAll("null", "''"),adminBO.getUid());

		driverLocationBO.setAssocCode(adminBO.getAssociateCode());
		ServiceRequestDAO.updateDriverAvailability(driverLocationBO.getStatus(), adminBO.getUid(), adminBO.getAssociateCode());
		response.getWriter().write(mainArray.toString());
	}

	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to set the location of a Driver.
	 *      This method is getting latitude, longitude and availability status
	 *      of a driver.Also this method is getting driver id from the session.
	 *      Get the user detail using session attribute.
	 */
	/*
	 * public void chckUserCardInfo(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException, IOException {
	 * 
	 * PaymentProcessBean processBean = null;
	 * 
	 * HttpSession session = request.getSession(); StringBuffer
	 * resultStrBuffer = new StringBuffer(); ApplicationPoolBO poolBO =
	 * (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
	 * AdminRegistrationBO adminBO; adminBO = (AdminRegistrationBO)
	 * session.getAttribute("user"); PaymentProcessingAction pay = new
	 * PaymentProcessingAction();
	 * 
	 * processBean = magneticStripAndAmtParsing(request,response);
	 * processBean.setB_driverid(adminBO.getUid());
	 * processBean.setAdd1(request
	 * .getParameter("add1")==null?"":request.getParameter("add1"));
	 * processBean
	 * .setAdd2(request.getParameter("add2")==null?"":request.getParameter
	 * ("add2"));
	 * processBean.setCity(request.getParameter("city")==null?"":request
	 * .getParameter("city"));
	 * processBean.setState(request.getParameter("state"
	 * )==null?"":request.getParameter("state"));
	 * processBean.setZip(request.
	 * getParameter("zip")==null?"":request.getParameter("zip"));
	 * processBean.setCvv
	 * (request.getParameter("cvv")==null?"":request.getParameter("cvv"));
	 * processBean.setMerchid(adminBO.getMerchid());
	 * processBean.setServiceid(adminBO.getServiceid());
	 * 
	 * 
	 * /*if(request.getParameter("ccno") != null &&
	 * request.getParameter("ccno") !=""){ StringBuffer error =
	 * MaualCreditCardProcessing.validateCC(processBean.getB_driverid(),
	 * adminBO.getAssociateCode(), processBean.getB_amount(),
	 * processBean.getB_tip_amt(), request.getParameter("ccno"));
	 * 
	 * if(!(error.toString().length()>0)) {
	 * 
	 * } } processBean = pay.initiatePayProcess(poolBO,processBean,"Mobile");
	 * 
	 * if(processBean.getB_approval_status().equalsIgnoreCase("1")) {
	 * 
	 * PaymentProcessingAction.makePayment(processBean,request.getRealPath(
	 * "/images/nosign.gif"),poolBO, adminBO);
	 * resultStrBuffer.append("ResVer=1.1;STATUS=APPROVED;TXNID="
	 * +processBean.getB_trans_id
	 * ()+";APPROVALCODE="+processBean.getB_approval_code());
	 * 
	 * } else if(processBean.getB_approval_status().equalsIgnoreCase("2")) {
	 * 
	 * ProcessingDAO.insertPaymentIntoWork(processBean,request.getRealPath(
	 * "/images/nosign.gif"),poolBO, adminBO);
	 * resultStrBuffer.append("ResVer=1.1;STATUS=DECLINED;TXNID="
	 * +processBean.getB_trans_id
	 * ()+";APPROVALCODE="+processBean.getB_approval());
	 * 
	 * } else { resultStrBuffer.append(
	 * "ResVer=1.1;STATUS=FAILURE;MESSAGE=Fail to Process Your Request^Reason:^Check Your Data^Check Your Network;XXXXX"
	 * ); //code to insert Connection Exception }
	 * 
	 * response.getWriter().write(resultStrBuffer.toString());
	 * 
	 * }
	 */
	public void getRiderSign(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String content_length = request.getHeader("content-length");
		try {
			BufferedReader reader1 = request.getReader();
			int a = 0, counter = 0;
			byte[] bytearray = new byte[Integer.parseInt(content_length)];
			while ((a = reader1.read()) != -1) {
				bytearray[counter] = (byte) a;
				counter++;
			}
			/*
			 * File f=new
			 * File("/root/Desktop/Sample_"+System.currentTimeMillis()+".gif");
			 * FileOutputStream fop=new FileOutputStream(f);
			 * fop.write(bytearray); fop.close();
			 */

			/*
			 * ByteArrayInputStream bis = new ByteArrayInputStream(bytearray);
			 * Iterator readers = ImageIO.getImageReadersByFormatName("gif");
			 * 
			 * ImageReader reader = (ImageReader) readers.next(); Object source
			 * = bis; // File or InputStream, it seems file is OK
			 * 
			 * ImageInputStream iis = ImageIO.createImageInputStream(source);
			 * //Returns an ImageInputStream that will take its input from the
			 * given Object
			 * 
			 * reader.setInput(iis, true); ImageReadParam param =
			 * reader.getDefaultReadParam();
			 * 
			 * Image image = reader.read(0, param); //got an image file
			 * 
			 * BufferedImage bufferedImage = new
			 * BufferedImage(image.getWidth(null), image.getHeight(null),
			 * BufferedImage.TYPE_INT_RGB); //bufferedImage is the RenderedImage
			 * to be written Graphics2D g2 = bufferedImage.createGraphics();
			 * g2.drawImage(image, null, null); File imageFile = new
			 * File("/root/Desktop/snap.gif"); ImageIO.write(bufferedImage,
			 * "gif", imageFile);
			 */

			/*
			 * BufferedImage imag=ImageIO.read(new ByteArrayInputStream(bytes));
			 * ImageIO.write(imag, "gif", new File("/root/Desktop","snap.gif"));
			 */
			String trans_id = request.getParameter("transid") == null ? "" : request.getParameter("transid");
			String trans_id2 = request.getParameter("transid2") == null ? "" : request.getParameter("transid2");
			// ProcessingDAO.updateSignIntoWork(bytearray, trans_id);
			ChargesDAO.updateSignIntoCC(bytearray, trans_id, trans_id2);
			if (request.getParameter("version") != null) {
				String version = request.getParameter("version");
			}
			response.getWriter().write("ResVer=1.1;STATUS=Received");
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public PaymentProcessBean magneticStripAndAmtParsing(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		PaymentProcessBean processBean = null;
		processBean = getmagneticStripInfo(request);
		processBean.setB_asscode(adminBO.getAssociateCode());
		processBean.setB_service_key(adminBO.getServiceKey());
		processBean.setB_desc(request.getParameter("bDesc") == null ? "" : request.getParameter("bDesc"));
		processBean.setB_amount(request.getParameter("amt") == null ? "0" : request.getParameter("amt").trim());
		processBean.setB_tip_amt(request.getParameter("tip") == null ? "0" : request.getParameter("tip"));
		return processBean;
	}

	public PaymentProcessBean getmagneticStripInfo(HttpServletRequest request) {
		PaymentProcessBean processBean = new PaymentProcessBean();
		String card_no, card_issuer, card_type = "", exp_date, card_full_no;
		if (!request.getParameter("track1").equals("")) {
			// System.out.println("Getting Track 1");
			String trackData=request.getParameter("track1");
			card_issuer = request.getParameter("track1").substring(1, 2);
			card_type = ProcessingDAO.getCardType(request.getParameter("track1").substring(1, 9));
			card_no = request.getParameter("track1").split("\\^")[0].substring(request.getParameter("track1").split("\\^")[0].length() - 4, request.getParameter("track1").split("\\^")[0].length());
			exp_date = request.getParameter("track1").split("\\^")[2].substring(0, 4) + request.getParameter("track1").split("\\^")[2].substring(4, 6);
			card_full_no = request.getParameter("track1").split("\\^")[0].substring(1, request.getParameter("track1").split("\\^")[0].length());
			processBean.setB_paymentType("A");
		} else if (request.getParameter("ccno") != null && !"".equalsIgnoreCase(request.getParameter("ccno")) && request.getParameter("ccno").length() > 12) {
			cat.info("Getting Manual");
			card_issuer = request.getParameter("ccno").substring(0, 1);
			card_type = ProcessingDAO.getCardType(request.getParameter("ccno").substring(0, 8));
			card_no = request.getParameter("ccno").substring(request.getParameter("ccno").length() - 4, request.getParameter("ccno").length());
			exp_date = request.getParameter("exp") == null ? "" : request.getParameter("exp");
			card_full_no = request.getParameter("ccno");
			processBean.setB_paymentType("M");
		} else if (!request.getParameter("track2").equals("")) {
			cat.info("Getting Track 2");
			card_issuer = request.getParameter("track2").substring(0, 1);
			card_type = ProcessingDAO.getCardType(request.getParameter("track2").substring(0, 8));
			card_no = request.getParameter("track2").substring(request.getParameter("track2").split("=")[1].length() - 4, request.getParameter("track2").split("=")[1].length());
			exp_date = request.getParameter("track2").split("=")[1].substring(2, 4) + request.getParameter("track2").split("=")[1].substring(4, 6);
			card_full_no = request.getParameter("track2").split("=")[0];
			processBean.setB_paymentType("A");
		} else {
			card_full_no = "";
			card_issuer = "";
			card_no = "";
			exp_date = "";
		}
		processBean.setB_card_full_no(card_full_no);
		processBean.setTrack1(request.getParameter("track1"));
		processBean.setTrack2(request.getParameter("track2"));
		processBean.setB_card_issuer(card_issuer);
		processBean.setB_card_no(card_no);
		processBean.setB_card_type("C");
		// processBean.setB_cardExpiryDate(request.getParameter("expyear")+request.getParameter("expmonth"));
		processBean.setB_cardExpiryDate(exp_date);
		processBean.setB_trip_id(request.getParameter("trip") == null ? "" : request.getParameter("trip"));
		/*
		 * System.out.println(processBean.getB_card_full_no());
		 * System.out.println(processBean.getTrack1());
		 * System.out.println(processBean.getTrack2());
		 * System.out.println(processBean.getB_card_issuer());
		 * System.out.println(processBean.getB_card_no());
		 * System.out.println(processBean.getB_card_type());
		 * System.out.println(processBean.getB_cardExpiryDate());
		 */
		return processBean;
	}

	public void setMQueueLocation(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		cat.info("In Set Driver location method");
		DriverLocationBO driverLocationBO = new DriverLocationBO();
		Map driverMap = new HashMap();
		String userType = "";
		String userid = "";
		String userKey = "";
		String assoccode = "";
		String queueId = "";
		// DoQueueProcess queueIdProcess;
		int position = -1;
		List publicRequestXML = null;

		int locationStatus = -1;
		int queueStatus = -1;

		if (adminBO != null) {
			userType = adminBO.getUsertypeDesc();
			userid = adminBO.getUname();
			userKey = adminBO.getUid();
			assoccode = adminBO.getAssociateCode();
		}
		if (request.getParameter("latitude") != null) {
			driverLocationBO.setLatitude(request.getParameter("latitude"));
		} else {
			driverLocationBO.setLatitude("0");
		}
		if (request.getParameter("longitude") != null) {
			driverLocationBO.setLongitude(request.getParameter("longitude"));
		} else {
			driverLocationBO.setLongitude("0");
		}
		if (request.getParameter("availability") != null) {
			driverLocationBO.setStatus(request.getParameter("availability"));
		} else {
			driverLocationBO.setStatus("0");
		}
		if (userid != "") {
			driverLocationBO.setDriverid(userid);
		}
		if (userType.equalsIgnoreCase("Driver")) {
			locationStatus = ServiceRequestDAO.checkDriverLocation(driverLocationBO.getDriverid());
		}
		if (locationStatus == 0 && userType.equalsIgnoreCase("Driver")) {
			queueStatus = ServiceRequestDAO.insertDriverLocation(driverLocationBO,adminBO.getMasterAssociateCode());
			if (queueStatus > 0) {
				driverMap.put("location", "Latitude and Longitude are Inserted");
			} else {
				driverMap.put("location", "Latitude and Longitude failed to insert");
			}
		} else if (locationStatus > 0 && userType.equalsIgnoreCase("Driver")) {
			queueStatus = ServiceRequestDAO.updateDriverLocation(driverLocationBO,adminBO.getMasterAssociateCode());
			if (queueStatus > 0) {
				driverMap.put("location", "Latitude and Longitude are Updated");
				// publicRequestXML = RequestDAO.getOpenRequest(assoccode);
				driverMap.put("publicRequest", publicRequestXML);
			} else {
				driverMap.put("location", "Latitude and Longitude failed to update");
			}
			//System.out.println("updateDriverLocation ==" + queueStatus);
		}
		if (queueStatus > 0 && adminBO.getDispatchMethod().equals("q")) {
			queueId = ServiceRequestDAO.getQueueID(driverLocationBO.getLatitude(), driverLocationBO.getLongitude());
			//System.out.println("queueid ==" + queueId);
		} else {
			queueId = "0";
		}
		driverMap.put("queueId", queueId);
		if (!queueId.equalsIgnoreCase("0") && driverLocationBO.getStatus().equalsIgnoreCase("y") && !ServiceRequestDAO.checkDriverQueue(queueId, userid)) {
			// if(ServiceRequestDAO.checkQueueId(queueId)) {
			// position = ServiceRequestDAO.generateQueuePosition(queueId);
			// } else {
			// position = ServiceRequestDAO.insertQueuePostition(queueId);
			// }
			driverMap.put("position", position + "");
			// queueStatus =
			// ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(),
			// queueId,assoccode, position);
			//System.out.println("Insert in to Driver Queue " + queueStatus);
		} else if (!queueId.equalsIgnoreCase("0") && driverLocationBO.getStatus().equalsIgnoreCase("N")) {
			position = ServiceRequestDAO.getDriverPosition(driverLocationBO.getDriverid(), queueId);
			queueStatus = ServiceRequestDAO.deleteDriverFromQueue(driverLocationBO.getDriverid(), queueId);
			if (queueStatus > 0) {
				driverMap.put("queue", "Remove Driver from the Queue");
			}
			ServiceRequestDAO.updateQueueDetail(ServiceRequestDAO.getQueueList(queueId, position), queueId, poolBO);
		} else if (!queueId.equalsIgnoreCase("0") && ServiceRequestDAO.checkDriverQueue(queueId, userid)) {
			queueStatus = ServiceRequestDAO.deleteDriverFromQueue(driverLocationBO.getDriverid(), queueId);
			if (queueStatus > 0) {
				driverMap.put("queue", "Remove Driver from the Queue");
			}
			ServiceRequestDAO.updateQueueDetail(ServiceRequestDAO.getQueueList(queueId, position), queueId, poolBO);
			//System.out.println("Delete Driver from the Queue " + queueStatus);
		}
		if (queueStatus > 0 && driverLocationBO.getStatus().equalsIgnoreCase("y")) {
			/*
			 * if(TDSController._queueIdMap.containsKey(queueId)) {
			 * queueIdProcess =
			 * (DoQueueProcess)TDSController._queueIdMap.get(queueId);
			 * System.out
			 * .println("!!!!!!!!!!!!!!!!!!!!"+queueIdProcess.getName
			 * ()+"!!!!!!!!!!!!!"+queueIdProcess.isAlive());
			 * queueIdProcess.resume(); }
			 */
			driverMap.put("queue", "Driver was maintained in the Queue");
		} else if (queueStatus > 0 && driverLocationBO.getStatus().equalsIgnoreCase("N")) {
			driverMap.put("queue", "Remove Driver from the Queue");
		} else {
			driverMap.put("error", "Unable to do process");
		}
		request.setAttribute("availablility", driverMap);
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		getServletContext().getRequestDispatcher(TDSConstants.mDriverAvailabilityJSP).forward(request, response);
	}

	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @see Description
	 */
	/*
	 * public void setMDriverAcceptance(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException, IOException {
	 * //cat.info("In setDriverAcceptance method"); String tripid = ""; String
	 * assocCode = ""; String driverid = ""; String driverUserId = ""; int
	 * status = 0; String acceptedStatus = ""; int position = 0;
	 * HttpSession session = request.getSession(); OpenRequestBO
	 * openRequestBO = null; AdminRegistrationBO adminBO = null;
	 * 
	 * if(session.getAttribute("user") != null) { adminBO =
	 * (AdminRegistrationBO) session.getAttribute("user"); driverid =
	 * adminBO.getUid(); driverUserId = adminBO.getUname(); assocCode =
	 * adminBO.getAssociateCode();
	 * System.out.println("UserType:"+",Userid"+driverid); }
	 * if(request.getParameter("tripid") != null) { openRequestBO = new
	 * OpenRequestBO(); tripid = request.getParameter("tripid");
	 * openRequestBO.setTripid(tripid); }
	 * 
	 * System.out.println("Driver id "+driverid+" Trip id "+tripid);
	 * if(tripid != "" && driverid != "") { openRequestBO =
	 * RequestDAO.getOpenRequestBean(openRequestBO, ""); openRequestBO =
	 * RequestDAO.getOpenRequestTableData(openRequestBO);
	 * if(openRequestBO.getIsBeandata() > 0) {
	 * System.out.println("In True Condition ");
	 * openRequestBO.setTripid(tripid);
	 * openRequestBO.setDriverid(driverid);
	 * openRequestBO.setDriverUName(driverUserId);
	 * openRequestBO.setAssociateCode(assocCode); status =
	 * ServiceRequestDAO.insertAcceptedRequest( openRequestBO);
	 * System.out.println("Insert Status "+status); if(status > 0){
	 * acceptedStatus = "You Accepted this trip id"; position =
	 * ServiceRequestDAO.getDriverPosition(driverUserId ,
	 * openRequestBO.getQueueno());
	 * ServiceRequestDAO.deleteDriverFromQueue(driverUserId
	 * ,openRequestBO.getQueueno());
	 * ServiceRequestDAO.updateQueueDetail(ServiceRequestDAO.getQueueList(
	 * openRequestBO.getQueueno(),position), openRequestBO.getQueueno());
	 * //cat.info("Successfully Updated AcceptedRequest");/
	 * request.setAttribute("acceptedRequest", openRequestBO); } else {
	 * //cat.info("Failure to update AcceptedRequest"); acceptedStatus =
	 * "Failure to Accept the Trip"; } } else {
	 * //cat.info("Already Accepted this Trip"); acceptedStatus =
	 * "Somebody Accepted this trip id"; } } else {
	 * //cat.info("Failure to update AcceptedRequest"); acceptedStatus =
	 * "Your trip id is Invalid"; } request.setAttribute("acceptedStatus",
	 * acceptedStatus);
	 * getServletContext().getRequestDispatcher(TDSConstants.mAcceptedRequestJSP
	 * ).forward(request, response);
	 * 
	 * }
	 */

	public void setMDriverAcceptance(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		int status = 0;
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO.setTripid(request.getParameter("tripid"));
		String tripId = openRequestBO.getTripid();
		String latidue = request.getParameter("aTLa");
		String longitude = request.getParameter("aTLo");
		if (latidue == null || latidue.equals("")) {
			latidue = "0.00000";
		}
		if (longitude == null || longitude.equals("")) {
			longitude = "0.00000";
		}
		JSONArray mainArray = new JSONArray();
		String[] acceptedStatus = new String[2];
		String jobsCode = DispatchDAO.getCompCode(tripId, adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO, myString);
		}
		try {
			if (openRequestBO != null) {
				Double acceptDistance=ZoneDAO.getMaxDistance(adminBO.getMasterAssociateCode(),openRequestBO.getQueueno());
				Float distance=calculateDistance(Float.parseFloat(latidue), Float.parseFloat(longitude), Float.parseFloat(openRequestBO.getSlat()), Float.parseFloat(openRequestBO.getSlong()));
				if(acceptDistance > distance){
					openRequestBO.setTripid(request.getParameter("tripid"));
					openRequestBO.setDriverid(adminBO.getUid());
					openRequestBO.setVehicleNo(adminBO.getVehicleNo());
					ArrayList<DriverRestrictionBean> restrictedDrivers=DispatchDAO.readRestrictionForTrip(openRequestBO.getTripid(),adminBO.getMasterAssociateCode());
					boolean restrictionStatus=false;
					if(restrictedDrivers!=null && restrictedDrivers.size()>0){
						for(int i=0;i<restrictedDrivers.size();i++){
							if(restrictedDrivers.get(i).getStatus()==1 && !restrictedDrivers.get(i).getDriverId().contains(adminBO.getUid())){
								restrictionStatus=true;
							} else if(restrictedDrivers.get(i).getStatus()==2 && restrictedDrivers.get(i).getDriverId().contains(adminBO.getUid())){
								restrictionStatus=true;
							}
						}
					}
					if(!restrictionStatus){
						if (CheckSpecialFlags.verifyFlags(MobileDAO.getFlagsForJob(openRequestBO.getTripid(), adminBO.getAssociateCode()), MobileDAO.getFlagsForDriver(openRequestBO.getDriverid(), adminBO.getAssociateCode())) && adminBO.getDriverRating() >= openRequestBO.getJobRating()) {
							if(openRequestBO.getAssociateCode().equals(adminBO.getChangeCompanyCode())){
								openRequestBO.setAssociateCode(adminBO.getAssociateCode());
								AuditDAO.updateJobLogs(adminBO,openRequestBO.getTripid());
							}
							if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
								status = RequestDAO.updateOpenRequestOnDriverAccept(openRequestBO,adminBO,adminBO.getCompanyList());
							} else {
								ArrayList<String> myString = new ArrayList<String>();
								status = RequestDAO.updateOpenRequestOnDriverAccept(openRequestBO,adminBO, myString);
							}
							//						status = RequestDAO.updateOpenRequestOnDriverAccept(openRequestBO, adminBO);
							if (status >= 1) {
								if(openRequestBO.getRouteNumber()==null || openRequestBO.getRouteNumber().equals("") || openRequestBO.getRouteNumber().equals("0")){
									AuditDAO.insertJobLogs("Job Accepted", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.jobAccepted, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
								}else{
									//System.out.println("acceptence");
									AuditDAO.insertJobLogsForSharedRide("Job Accepted", openRequestBO.getRouteNumber(), adminBO.getAssociateCode(), TDSConstants.jobAccepted);
								}
								if (!SecurityDAO.getDriverAvailabilityStatus(adminBO, adminBO.getDriverAlarm())) {
									SecurityDAO.setDriverStatus("N", adminBO);
									ServiceRequestDAO.updateDriverAvailability("N", adminBO.getUid(), adminBO.getAssociateCode());
									String result = ServiceRequestDAO.getQueueIdByDriverIDandAssoccode(adminBO.getAssociateCode(), adminBO.getUid());
									//ServiceRequestDAO.Update_BtZ_ZoneLogout(adminBO.getUid(), adminBO.getVehicleNo(), result, "3","Driver logged out from zone since he got job", adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
									//String currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
									if(!result.equals("")){
										ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), result, adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+") logged out of the zone:"+result+". Due to driver accept the order("+openRequestBO.getTripid()+")");
									}
									ServiceRequestDAO.deleteDriverFromQueue(adminBO.getUid(), adminBO.getMasterAssociateCode());
									openRequestBO.setDriverLogStatus(true);
								}
								openRequestBO.setTripStatus(TDSConstants.jobAllocated + "");
								acceptedStatus = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO, "AA", 0, adminBO.getMasterAssociateCode());
								// JSONArray sample = new
								// JSONArray(acceptedStatus[0]);
								mainArray = new JSONArray(acceptedStatus[0]);
								// mainArray.put(acceptedStatus[0]);
								// buffer.append(acceptedStatus[0]);
								if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
									DispatchDAO.updateAcceptTime(adminBO.getAssociateCode(), openRequestBO.getTripid(),1,adminBO.getCompanyList());
								} else {
									ArrayList<String> myString = new ArrayList<String>();
									DispatchDAO.updateAcceptTime(adminBO.getAssociateCode(), openRequestBO.getTripid(),1, myString);
								}
								//							DispatchDAO.updateAcceptTime(adminBO.getAssociateCode(), openRequestBO.getTripid(), 1);
								if (adminBO.getCall_onAccept() == 1) {
									String phoneNumber = adminBO.getPhonePrefix() + ServiceRequestDAO.getPhoneNoByTripID(adminBO.getMasterAssociateCode(), adminBO.getUid(), openRequestBO.getTripid());
									if (phoneNumber.length() > 2) {
										IVRPassengerCallerOutBound ivr = new IVRPassengerCallerOutBound("7", "0", phoneNumber, adminBO.getMasterAssociateCode());
										ivr.start();
										AuditDAO.insertJobLogs("IVR Call Passenger For On Accept", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.ivrPassDriverAcceptedCall, "System", "", "", adminBO.getMasterAssociateCode());
									}
								}
								if (adminBO.getMessage_onAccept() == 1) {
									String phoneNumber = adminBO.getSmsPrefix() + ServiceRequestDAO.getPhoneNoByTripID(adminBO.getMasterAssociateCode(), adminBO.getUid(), openRequestBO.getTripid());
									try {
										String message=adminBO.getMessageAccept();
										if(message!=null && !message.equals("")){
											message=message.replace("[Driver]", adminBO.getUid()).replace("[CabNo]", adminBO.getVehicleNo()).replace("[PhNum]", adminBO.getPhnumber()).replace("[Name]", adminBO.getUserNameDisplay());
										} else {
											message="Your trip is allocated to a driver";
										}
										AuditDAO.insertJobLogs("SMS Passenger For On Accept", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.ivrPassDriverAcceptedCall, "System", "", "", adminBO.getMasterAssociateCode());
										sendSMS.main(message, phoneNumber, adminBO.getMasterAssociateCode());
									} catch (TwilioRestException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
								}

								if(openRequestBO.getTripSource()==0){
									pushFuctionCalling(openRequestBO,"Driver Accepted",adminBO.getAssociateCode());
								}
								if(openRequestBO.getTripSource()==4){
									CertificateBO certBo = CustomerMobileDAO.getIos_CertificateDetails(adminBO.getAssociateCode());
									DriverCabQueueBean customerBo = CustomerMobileDAO.getKeyForGooglePushById(openRequestBO,adminBO.getAssociateCode());
									//pushMSG("have to send message", certBo.getPath(), certBo.getPassword() , customerBo.getGkey());
									pushMSG1("Driver accepted your trip", certBo.getPath(), certBo.getPassword() , customerBo.getGkey(),1);
								}
								//ServiceRequestDAO.insertDriver_BooktoZoneHistory(adminBO.getUid(), adminBO.getVehicleNo(), openRequestBO.getQueueno(), "1","Driver accept the job; Trip id: "+openRequestBO.getTripid(), adminBO.getAssociateCode());
								//ServiceRequestDAO.Update_BtZ_DriverStatus(adminBO.getUid(), adminBO.getVehicleNo(), openRequestBO.getQueueno(), "1","Driver accept the job; Trip id: "+openRequestBO.getTripid(), adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
								
								String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
								int isUpdateGPS = (updateGPS!=null  && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
								if(isUpdateGPS==1){
									ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), latidue, longitude, adminBO.getVehicleNo(), 2, openRequestBO.getTripid());
								}
							} else {
								JSONObject jasonObject = new JSONObject();
								jasonObject.put("rV", 1.2);
								jasonObject.put("Act", "MDA");
								jasonObject.put("rC", 400);
								jasonObject.put("STATUS", "FAILURE");
								jasonObject.put("MESSAGE", "Failure to Accept the Trip");
								mainArray.put(jasonObject);
								// buffer.append("ResVer=1.1;STATUS=FAILURE;MESSAGE=Failure to Accept the Trip");
							}
						} else {
							ArrayList<String> tripIds = new ArrayList<String>();
							tripIds.add(openRequestBO.getTripid());
							RequestDAO.updateJobDriver(adminBO.getAssociateCode(), "", tripIds, "", 2);
							if(!openRequestBO.getRouteNumber().equals("") && !openRequestBO.getRouteNumber().equals("0")){
								AuditDAO.insertJobLogsForSharedRide("Job Accepted Failed Because Flag Doesnt Match", openRequestBO.getRouteNumber(), adminBO.getAssociateCode(), TDSConstants.jobAccepted);
								if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
									RegistrationDAO.updateJobDriverForShared(adminBO.getCompanyList(),adminBO.getAssociateCode(), "", openRequestBO.getRouteNumber(), "", 2);
								} else {
									ArrayList<String> myString = new ArrayList<String>();
									RegistrationDAO.updateJobDriverForShared(myString,adminBO.getAssociateCode(), "", openRequestBO.getRouteNumber(), "", 2);
								}
							} else {
								AuditDAO.insertJobLogs("Job Accepted Failed Because Flag Doesnt Match", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.jobAccepted, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
								RequestDAO.updateJobDriver(adminBO.getAssociateCode(), "", tripIds, "", 2);
							}
							JSONObject jasonObject = new JSONObject();
							jasonObject.put("rV", 1.2);
							jasonObject.put("Act", "MDA");
							jasonObject.put("rC", 400);
							jasonObject.put("STATUS", "FAILURE");
							jasonObject.put("MESSAGE", "Failure to Accept the Trip Because Flag Doesnt Match");
							mainArray.put(jasonObject);
						}
					} else {
						ArrayList<String> tripIds = new ArrayList<String>();
						tripIds.add(openRequestBO.getTripid());
						RequestDAO.updateJobDriver(adminBO.getAssociateCode(), "", tripIds, "", 2);
						if(!openRequestBO.getRouteNumber().equals("") && !openRequestBO.getRouteNumber().equals("0")){
							AuditDAO.insertJobLogsForSharedRide("Job Accepted Failed Because Voucher Reference Doesnt Match", openRequestBO.getRouteNumber(), adminBO.getAssociateCode(), TDSConstants.jobAccepted);
							if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
								RegistrationDAO.updateJobDriverForShared(adminBO.getCompanyList(),adminBO.getAssociateCode(), "", openRequestBO.getRouteNumber(), "", 2);
							} else {
								ArrayList<String> myString = new ArrayList<String>();
								RegistrationDAO.updateJobDriverForShared(myString,adminBO.getAssociateCode(), "", openRequestBO.getRouteNumber(), "", 2);
							}
						} else {
							AuditDAO.insertJobLogs("Job Accepted Failed Because Voucher Reference Doesnt Match", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.jobAccepted, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
							RequestDAO.updateJobDriver(adminBO.getAssociateCode(), "", tripIds, "", 2);
						}
						JSONObject jasonObject = new JSONObject();
						jasonObject.put("rV", 1.2);
						jasonObject.put("Act", "MDA");
						jasonObject.put("rC", 400);
						jasonObject.put("STATUS", "FAILURE");
						jasonObject.put("MESSAGE", "Failure to Accept the Trip Because Voucher Refrence Doesnt Match");
						mainArray.put(jasonObject);
					}
				} else {
					ArrayList<String> tripIds = new ArrayList<String>();
					tripIds.add(openRequestBO.getTripid());
					if(!openRequestBO.getRouteNumber().equals("") && !openRequestBO.getRouteNumber().equals("0")){
						AuditDAO.insertJobLogsForSharedRide("Job Accepted Failed Because Driver Too Far From Job", openRequestBO.getRouteNumber(), adminBO.getAssociateCode(), TDSConstants.jobAccepted);
						if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
							RegistrationDAO.updateJobDriverForShared(adminBO.getCompanyList(),adminBO.getAssociateCode(), "", openRequestBO.getRouteNumber(), "", 2);
						} else {
							ArrayList<String> myString = new ArrayList<String>();
							RegistrationDAO.updateJobDriverForShared(myString,adminBO.getAssociateCode(), "", openRequestBO.getRouteNumber(), "", 2);
						}
						//						RegistrationDAO.updateJobDriverForShared(adminBO.getAssociateCode(), "", openRequestBO.getRouteNumber(), "", 2);
					} else {
						AuditDAO.insertJobLogs("Job Accepted Failed Because Driver Too Far From Job", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.jobAccepted, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
						RequestDAO.updateJobDriver(adminBO.getAssociateCode(), "", tripIds, "", 2);
					}
					JSONObject jasonObject = new JSONObject();
					jasonObject.put("rV", 1.2);
					jasonObject.put("Act", "MDA");
					jasonObject.put("rC", 400);
					jasonObject.put("STATUS", "FAILURE");
					jasonObject.put("MESSAGE", "Failure to Accept the Trip Because Driver Too Far From Job");
					mainArray.put(jasonObject);
				}
			} else {
				if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
					openRequestBO = RequestDAO.getOpenRequestStatus(request.getParameter("tripid"), adminBO,adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					openRequestBO = RequestDAO.getOpenRequestStatus(request.getParameter("tripid"), adminBO, myString);
				}
				//				openRequestBO = RequestDAO.getOpenRequestStatus(request.getParameter("tripid"), adminBO);
				if (openRequestBO != null) {
					if (openRequestBO.getTripStatus().equals(TDSConstants.jobAllocated + "")) {
						acceptedStatus = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO, "AA", 0, adminBO.getAssociateCode());
						/*
						 * JSONArray sample = new
						 * JSONArray(acceptedStatus[0]); mainArray = sample;
						 */
						mainArray = new JSONArray(acceptedStatus[0]);
						// mainArray.put(acceptedStatus[0]);
						// buffer.append(acceptedStatus[0]);
					} else {
						JSONObject jasonObject = new JSONObject();
						jasonObject.put("rV", 1.2);
						jasonObject.put("Act", "MDA");
						jasonObject.put("rC", 400);
						jasonObject.put("STATUS", "FAILURE");
						jasonObject.put("MESSAGE", "Somebody Accepted this trip id/Trip Time Changed by Operator");
						mainArray.put(jasonObject);

						// buffer.append("ResVer=1.1;STATUS=FAILURE;MESSAGE=Somebody Accepted this trip id/Trip Time Changed by Operator");
					}
				} else {
					JSONObject jasonObject = new JSONObject();
					jasonObject.put("rV", 1.2);
					jasonObject.put("Act", "MDA");
					jasonObject.put("rC", 400);
					jasonObject.put("STATUS", "FAILURE");
					jasonObject.put("MESSAGE", "Somebody Accepted this trip id/Trip Time Changed by Operator");
					mainArray.put(jasonObject);
					// buffer.append("ResVer=1.1;STATUS=FAILURE;MESSAGE=Somebody Accepted this trip id/Trip Time Changed by Operator");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("DataSentToPhone:" + mainArray.toString());
		response.getWriter().write(mainArray.toString());
	}

	private void pushFuctionCalling(OpenRequestBO openBO,String message, String cCode) {
		// TODO Auto-generated method stub
		DriverCabQueueBean customerBo = new DriverCabQueueBean();
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		customerBo = CustomerMobileDAO.getKeyForGooglePushById(openBO,cCode);
		Messaging oneSMS = new Messaging(getServletContext(),message,poolBO,customerBo);
		oneSMS.start();
	}

	public void pushMSG1(final String msg,final String path, final String pswd,final String token, final int batch) {
		Thread thread = new Thread("IOS Push1") {
			public void run(){
				try {
					PushNotificationPayload payload = PushNotificationPayload.complex();
					payload.addAlert(msg);
					payload.addSound("bingbong.aiff");
					payload.addBadge(batch);
					Push.payload(payload, path, pswd, false, token);
				} catch (CommunicationException e) {
					System.out.println("CommunicationException");
					e.printStackTrace();
				} catch (KeystoreException e) {
					System.out.println("KeystoreException");
					e.printStackTrace();
				} catch (Exception e) {
					System.out.println("Exception");
					e.printStackTrace();
				}
			}
		};
		thread.start();
	}

	public void pushMSG(final String msg,final String path, final String pswd,final String token) {
		try{
			Thread thread = new Thread("IOS Push") {
				public void run(){
					ApnsService service = APNS.newService().withCert(path, pswd).withSandboxDestination().build();
					String payLoad = APNS.newPayload().alertBody(msg).build();
					service.push(token, payLoad);
				}
			};
			thread.start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public void setMOnRouteToPickup(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		int status = 0;
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO.setTripid(request.getParameter("tripid"));
		String latidue = request.getParameter("oRLa");
		String longitude = request.getParameter("oRLo");
		if (latidue == null || latidue.equals("")) {
			latidue = "0.00000";
		}
		if (longitude == null || longitude.equals("")) {
			longitude = "0.00000";
		}
		openRequestBO.setTripid(request.getParameter("tripid"));
		openRequestBO.setDriverid(adminBO.getUid());
		String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			status = RequestDAO.updateOpenRequestOnDriverOnRoute(openRequestBO,adminBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			status = RequestDAO.updateOpenRequestOnDriverOnRoute(openRequestBO, adminBO, myString);
		}
		JSONArray array = new JSONArray();
		try {
			JSONObject jasonObject = new JSONObject();
			if (status > 0) {
				AuditDAO.insertJobLogs("Driver on route for job", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.driverOnRoute, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "ORTP");
				jasonObject.put("rC", 200);
				jasonObject.put("STATUS", "SUCCESS");
				jasonObject.put("MESSAGE", "Trip marked as On Route");
				array.put(jasonObject);
				if (adminBO.getCall_onRoute() == 1) {
					String phoneNumber = adminBO.getPhonePrefix() + ServiceRequestDAO.getPhoneNoByTripID(adminBO.getMasterAssociateCode(), adminBO.getUid(), openRequestBO.getTripid());
					if (phoneNumber.length() > 2) {
						IVRPassengerCallerOutBound ivr = new IVRPassengerCallerOutBound("8", "0", phoneNumber, adminBO.getMasterAssociateCode());
						ivr.start();
						AuditDAO.insertJobLogs("IVR Call Passenger For On Route", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.ivrPassDriverOnRouteCall, "System", "", "", adminBO.getMasterAssociateCode());
					}
				}
				if (adminBO.getMessage_onRoute() == 1) {
					String phoneNumber = adminBO.getSmsPrefix() + ServiceRequestDAO.getPhoneNoByTripID(adminBO.getMasterAssociateCode(), adminBO.getUid(), openRequestBO.getTripid());
					try {
						String message=adminBO.getMessageRoute();
						if(message!=null && !message.equals("")){
							message=message.replace("[Driver]", adminBO.getUid()).replace("[CabNo]", adminBO.getVehicleNo()).replace("[PhNum]", adminBO.getPhnumber()).replace("[Name]", adminBO.getUserNameDisplay());
						} else {
							message="The driver is on route to pick you up";
						}
						AuditDAO.insertJobLogs("SMS Passenger For On Route", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.ivrPassDriverAcceptedCall, "System", "", "", adminBO.getMasterAssociateCode());
						sendSMS.main(message, phoneNumber, adminBO.getMasterAssociateCode());
					} catch (TwilioRestException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
					openRequestBO = ServiceRequestDAO.CustomerOpenRequest(adminBO.getCompanyList(),adminBO.getAssociateCode(), openRequestBO.getTripid());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					openRequestBO = ServiceRequestDAO.CustomerOpenRequest(myString,adminBO.getAssociateCode(), openRequestBO.getTripid());
				}
				if(openRequestBO.getTripSource()==0){
					pushFuctionCalling(openRequestBO,"Driver On Route",adminBO.getAssociateCode());
				}
				if(openRequestBO.getTripSource()==4){
					CertificateBO certBo = CustomerMobileDAO.getIos_CertificateDetails(adminBO.getAssociateCode());
					DriverCabQueueBean customerBo = CustomerMobileDAO.getKeyForGooglePushById(openRequestBO,adminBO.getAssociateCode());
					pushMSG1("Driver is on a route to pick you", certBo.getPath(), certBo.getPassword() , customerBo.getGkey(),2);
				}
				
				String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
				int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
				if(isUpdateGPS==1){
					ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), latidue, longitude, adminBO.getVehicleNo(), 3, openRequestBO.getTripid());
				}
			} else {
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "ORTP");
				jasonObject.put("rC", 400);
				jasonObject.put("STATUS", "FAILURE");
				jasonObject.put("MESSAGE", "Failure to Start the Trip");
				array.put(jasonObject);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(array.toString());
	}

	public void setMDriverRejection(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		int status = 0;
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO.setTripid(request.getParameter("tripid"));
		String rejectStatus = "";
		String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO, myString);
		}
		String latidue = request.getParameter("dRLa");
		String longitude = request.getParameter("dRLo");
		JSONArray mainArray = new JSONArray();
		try {
			if (latidue == null || latidue.equals("")) {
				latidue = "0.00000";
			}
			if (longitude == null || longitude.equals("")) {
				longitude = "0.00000";
			}
			if (openRequestBO != null) {
				openRequestBO.setTripid(request.getParameter("tripid"));
				// Mark a reject only if driver is the only one allocated to the
				// job
				if (openRequestBO.getDriverid().split(";").length == 1) {
					String driverId = openRequestBO.getDriverid();
					if (Integer.parseInt(openRequestBO.getTripStatus()) != TDSConstants.srJobHold && Integer.parseInt(openRequestBO.getTripStatus()) != TDSConstants.tripOnHold) {
						openRequestBO.setDriverid("");
						openRequestBO.setVehicleNo("");
					}
					if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
						status = RequestDAO.updateOpenRequestOnDriverReject(openRequestBO, adminBO, driverId,adminBO.getCompanyList());
					} else {
						ArrayList<String> myString = new ArrayList<String>();
						status = RequestDAO.updateOpenRequestOnDriverReject(openRequestBO, adminBO, driverId,myString);
					}
					AuditDAO.insertJobLogs("Job Rejected", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.jobRejected, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
				}
				if (status > 0) {
					JSONObject jasonObject = new JSONObject();
					jasonObject.put("rV", 1.2);
					jasonObject.put("Act", "MDR");
					jasonObject.put("rC", 200);
					jasonObject.put("STATUS", "SUCCESS");
					jasonObject.put("MESSAGE", "You Rejected this trip");
					mainArray.put(jasonObject);
				} else {
					JSONObject jasonObject = new JSONObject();
					jasonObject.put("rV", 1.2);
					jasonObject.put("Act", "MDR");
					jasonObject.put("rC", 400);
					jasonObject.put("STATUS", "FAILURE");
					jasonObject.put("MESSAGE", "Trip has changed");
					mainArray.put(jasonObject);
				}
			} else {
				if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
					openRequestBO = RequestDAO.getOpenRequestStatus(request.getParameter("tripid"), adminBO,adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					openRequestBO = RequestDAO.getOpenRequestStatus(request.getParameter("tripid"), adminBO, myString);
				}
				if (openRequestBO != null) {
					if (openRequestBO.getTripStatus().equals(TDSConstants.jobAllocated + "")) {
						String[] acceptedStatus = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO, "AA", 0, adminBO.getMasterAssociateCode());
						/*
						 * JSONArray sample = new
						 * JSONArray(acceptedStatus[0]); mainArray = sample;
						 */
						mainArray = new JSONArray(acceptedStatus[0]);
						// mainArray.put(acceptedStatus[0]);
						// rejectStatus = acceptedStatus[0];
					} else {
						JSONObject jasonObject = new JSONObject();
						jasonObject.put("rV", 1.2);
						jasonObject.put("Act", "MDR");
						jasonObject.put("rC", 400);
						jasonObject.put("STATUS", "FAILURE");
						jasonObject.put("MESSAGE", "Trip has changed");
						mainArray.put(jasonObject);
						// rejectStatus =
						// "ResVer=1.1;STATUS=FAILURE;MESSAGE=Trip has changed";
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(mainArray.toString());
	}

	private void setMJobNoResponse(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//		String tripId= request.getParameter("tripid");
		//		int status = 0;
		//		JSONArray mainArray = new JSONArray();
		//		JSONObject jasonObject = new JSONObject();
		//		OpenRequestBO openRequestBO = new OpenRequestBO();
		//		openRequestBO.setTripid(request.getParameter("tripid"));
		//		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		//		String latidue = "0.000000";
		//		String longitude = "0.000000";
		//		openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO);
		//		//Exception 3
		//		if(openRequestBO==null){
		//			openRequestBO = RequestDAO.getOpenRequestStatus(request.getParameter("tripid"), adminBO);
		//		}
		//		if(openRequestBO!=null && openRequestBO.getDriverid()!=null && openRequestBO.getDriverid().equals(adminBO.getUid())){
		//			openRequestBO.setDriverid("");
		//			openRequestBO.setVehicleNo("");
		//			status = RequestDAO.updateOpenRequestOnDriverNoResponse(openRequestBO, adminBO);
		//			AuditDAO.insertJobLogs("Job missed by DriverID:"+adminBO.getUid()+" & CabNo:"+adminBO.getVehicleNo(), openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.jobRejected, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
		//		}else{
		//			AuditDAO.insertJobLogs("Job missed by DriverID:"+adminBO.getUid()+" & CabNo:"+adminBO.getVehicleNo(), "", adminBO.getAssociateCode(), TDSConstants.jobRejected, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
		//		}
		JSONArray mainArray = new JSONArray();
		JSONObject jasonObject = new JSONObject();
		try {
			jasonObject.put("rV", 1.2);
			jasonObject.put("Act", "JobNotAnswered");
			jasonObject.put("rC", 200);
			jasonObject.put("STATUS", "SUCCESSFULLY UPDATED");
			mainArray.put(jasonObject);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.getWriter().write(mainArray.toString());
	}


	public void getStartFlagTrip(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO = DispatchDAO.getAllTripsForDriver(adminBO.getCompanyList(),adminBO.getUid(),adminBO.getVehicleNo(),SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "driverAlarm"),2);
		if(openRequestBO.getTripid()==null || openRequestBO.getTripid().equals("")){
			String errorReturn = "";
			String latidue = request.getParameter("stLa");
			String longitude = request.getParameter("stLo");
			if (latidue == null || latidue.equals("")) {
				latidue = "0.00000";
			}
			if (longitude == null || longitude.equals("")) {
				longitude = "0.00000";
			}
			openRequestBO.setSlat(latidue);
			openRequestBO.setSlong(longitude);
			openRequestBO.setDriverid(adminBO.getUid());
			openRequestBO.setAssociateCode(adminBO.getAssociateCode());
			openRequestBO.setDriverUName(adminBO.getUname());
			openRequestBO.setVehicleNo(adminBO.getVehicleNo());
			Calendar currentDate = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			String dateNow = sdf.format(currentDate.getTime());
			openRequestBO.setShrs("2525");
			openRequestBO.setPhone("0000000000");
			openRequestBO.setName("Flag Trip");
			openRequestBO.setSdate(dateNow);
			openRequestBO.setDays("0000000");
			openRequestBO.setTypeOfRide("0");
			openRequestBO.setTripSource(TDSConstants.flagTrip);
			if(request.getParameter("sAdd")==null || !request.getParameter("sAdd").equals("")){
				if(adminBO.getEnable_stadd_flag()>0){
					openRequestBO.setSadd1(GetAddress.getGeoAddress(latidue, longitude));
				}else{
					openRequestBO.setSadd1("Flag Trip");
				}
			}else{
				openRequestBO.setSadd1(request.getParameter("sAdd"));
			}
			//openRequestBO.setSadd1(request.getParameter("sAdd")==null?"Flag Trip":request.getParameter("sAdd"));
			openRequestBO.setAmt(new BigDecimal("0"));
			openRequestBO.setChckTripStatus(TDSConstants.tripStarted);
			openRequestBO.setAdvanceTime("-1");
			openRequestBO.setEadd1(request.getParameter("eAdd")==null?"":request.getParameter("eAdd"));
			openRequestBO.setEdlatitude(request.getParameter("eLat")==null?"":request.getParameter("eLat"));
			openRequestBO.setEdlongitude(request.getParameter("eLon")==null?"":request.getParameter("eLon"));
			request.setAttribute("openrequestBOfromAction", openRequestBO);
			request.setAttribute("adminBoFromAction", adminBO);
			errorReturn = OpenRequestUtil.saveOpenRequest(request, response, getServletConfig());
			int result = 0;
			String queueId="";
			JSONArray mainArray = new JSONArray();
			try {
				if (errorReturn.contains("OK")) {
					String[] tripIdAndStatus = errorReturn.split(";");
					String[] tripId = tripIdAndStatus[0].split("=");
					if (tripId[1] != null && !tripId[1].equals("")) {
						JSONObject jasonObject = new JSONObject();
						openRequestBO.setTripid(tripId[1]);
						AuditDAO.insertJobLogs("Trip Started", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.tripStarted, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
						jasonObject.put("rV", 1.2);
						jasonObject.put("Act", "SFT");
						jasonObject.put("rC", 200);
						jasonObject.put("STATUS", "SUCCESS");
						jasonObject.put("MESSAGE", "Trip Started Successfully");
						jasonObject.put("TI", tripId[1]);
						jasonObject.put("SA1", openRequestBO.getSadd1());
						jasonObject.put("SLA", openRequestBO.getSlat());
						jasonObject.put("SLO", openRequestBO.getSlong());
						jasonObject.put("ELA", openRequestBO.getEdlatitude());
						jasonObject.put("ELO", openRequestBO.getEdlongitude());
						jasonObject.put("EA1", openRequestBO.getEadd1());
						mainArray.put(jasonObject);
						String message = mainArray.toString();
						openRequestBO.setStatus_msg(message);
						String jobsCode = DispatchDAO.getCompCode(tripId[1], adminBO.getMasterAssociateCode());
						if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
							DispatchDAO.updateAcceptTime(adminBO.getAssociateCode(), openRequestBO.getTripid(),1,adminBO.getCompanyList());
						} else {
							ArrayList<String> myString = new ArrayList<String>();
							DispatchDAO.updateAcceptTime(adminBO.getAssociateCode(), openRequestBO.getTripid(),1, myString);
						}

						ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
						ServiceRequestDAO.deleteDriverFromQueue(adminBO.getUid(), adminBO.getMasterAssociateCode());
						if(currentQueue!=null && currentQueue.size()>0){
							ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". Due to start flagtrip("+tripId[1]+")");
						}

						ServiceRequestDAO.updateDriverAvailability("N", adminBO.getUid(), adminBO.getAssociateCode());
						adminBO.setDriverAvailability("N");
						ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) this.getServletContext().getAttribute((adminBO.getMasterAssociateCode() + "Zones"));
						queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(latidue),Double.parseDouble(longitude));
						
						String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
						int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
						if(isUpdateGPS==1){
							ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), latidue, longitude, adminBO.getVehicleNo(), 5, openRequestBO.getTripid());					}
						}
					
				} else if (errorReturn.contains("false")) {
					JSONObject jasonObject = new JSONObject();
					jasonObject.put("rV", 1.2);
					jasonObject.put("Act", "SFT");
					jasonObject.put("rC", 400);
					jasonObject.put("STATUS", "FAILURE");
					jasonObject.put("MESSAGE", "Failed To Start a Trip");
					mainArray.put(jasonObject);
					String message = mainArray.toString();
					openRequestBO.setStatus_msg(message);
					// openRequestBO.setStatus_msg("ResVer=1.1;STATUS=FAILURE;MESSAGE=Failed To Start a Trip");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		String finalJSON = mapper.writeValueAsString(adminBO);
		System.out.println("Insert/Update Session Values For Driver--->"+adminBO.getUid()+" For Starting Flag Trip");
		MobileDAO.insertSessionObjects(adminBO.getSessionId(),finalJSON.replaceAll("null", "''"),adminBO.getUid());
		response.getWriter().write(openRequestBO.getStatus_msg());
	}

	public void getMStatus(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		DriverCabQueueBean chstatus=new DriverCabQueueBean();
		String Cabno = request.getParameter("cabno");
		String driverid = request.getParameter("driverId");
		int version = request.getParameter("versionCode")!=null?Integer.parseInt(request.getParameter("versionCode")):0;
		chstatus=MobileDAO.getStatus(adminBO.getAssociateCode(),Cabno,driverid,adminBO.getTimeZone(),version);
		int position = 0;
		if(chstatus.getQueue()!=null && !chstatus.equals("")){
			position=ServiceRequestDAO.getQueuePosition(chstatus.getQueue(), adminBO.getUid(), adminBO.getAssociateCode());
		}
		JSONArray array=new JSONArray();
		JSONObject status=new JSONObject();
		try {
			status.put("driverid", chstatus.getDriverid());
			status.put("drivername", chstatus.getDrivername());
			status.put("cabno",chstatus.getVehicleNo());
			status.put("logintime",chstatus.getLogintime());
			status.put("availability",chstatus.getAvailability().equals("Z")?"Y":chstatus.getAvailability());
			status.put("zonepos",position==0?"Not In Any Zone":position);
			status.put("latitude", chstatus.getLatitude());
			status.put("longitude", chstatus.getLongitude());
			array.put(status);
		} catch (Exception e) {
			System.out.println("Error :" + e);
		}
		response.getWriter().write(array.toString());
	}

	public void getMStartTrip(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		OpenRequestBO openRequestBO = new OpenRequestBO();
		OpenRequestBO customer_OR_BO = new OpenRequestBO();
		String errorReturn = "";
		openRequestBO.setTripid(request.getParameter("tripid") == null ? "" : request.getParameter("tripid"));
		String latidue = request.getParameter("stLa");
		String longitude = request.getParameter("stLo");
		JSONArray array = new JSONArray();
		if (latidue == null || latidue.equals("")) {
			latidue = "0.00000";
		}
		if (longitude == null || longitude.equals("")) {
			longitude = "0.00000";
		}
		String odoMeter = "";
		odoMeter = request.getParameter("odoMeter");
		if (odoMeter != null && !odoMeter.equals("")) {
			MobileDAO.insertOdoMeterValues(adminBO.getAssociateCode(), odoMeter, request.getParameter("tripid"), adminBO.getUid(), request.getParameter("cabNo"), "End Trip");
		}
		openRequestBO.setDriverid(adminBO.getUid());
		openRequestBO.setAssociateCode(adminBO.getAssociateCode());
		openRequestBO.setDriverUName(adminBO.getUname());
		int result = 0;
		String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			result = ServiceRequestDAO.setStartTrip(openRequestBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			result = ServiceRequestDAO.setStartTrip(openRequestBO,myString);
		}
		String queueId="";
		try {
			if (result > 0) {
				ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) this.getServletContext().getAttribute((adminBO.getMasterAssociateCode() + "Zones"));
				queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(latidue),Double.parseDouble(longitude));
				JSONObject jasonObject = new JSONObject();
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "ST");
				jasonObject.put("rC", 200);
				jasonObject.put("STATUS", "SUCCESS");
				jasonObject.put("MESSAGE", "Trip Started Successfully");
				jasonObject.put("TI", openRequestBO.getTripid());
				array.put(jasonObject);
				String message = array.toString();
				AuditDAO.insertJobLogs("Trip Started", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.startedTrip, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
				openRequestBO.setStatus_msg(message);
				if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
					DispatchDAO.updateAcceptTime(adminBO.getAssociateCode(), openRequestBO.getTripid(),1,adminBO.getCompanyList());
					customer_OR_BO = ServiceRequestDAO.CustomerOpenRequest(adminBO.getCompanyList(),adminBO.getAssociateCode(), openRequestBO.getTripid());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					DispatchDAO.updateAcceptTime(adminBO.getAssociateCode(), openRequestBO.getTripid(),1, myString);
					customer_OR_BO = ServiceRequestDAO.CustomerOpenRequest(myString,adminBO.getAssociateCode(), openRequestBO.getTripid());
				}
				if(customer_OR_BO.getTripSource()==0){
					pushFuctionCalling(customer_OR_BO,"Driver On board",adminBO.getAssociateCode());
				}
				if(customer_OR_BO.getTripSource()==4){
					CertificateBO certBo = CustomerMobileDAO.getIos_CertificateDetails(adminBO.getAssociateCode());
					DriverCabQueueBean customerBo = CustomerMobileDAO.getKeyForGooglePushById(openRequestBO,adminBO.getAssociateCode());
					pushMSG1("Your trip is started successfully", certBo.getPath(), certBo.getPassword() , customerBo.getGkey(),4);
				}
				
				String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
				int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
				if(isUpdateGPS==1){
					ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), latidue, longitude, adminBO.getVehicleNo(), 5, openRequestBO.getTripid());
				}
			} else {
				JSONObject jasonObject = new JSONObject();
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "ST");
				jasonObject.put("rC", 200);
				jasonObject.put("STATUS", "FAILURE");
				jasonObject.put("MESSAGE", "Failed To Start a Trip");
				array.put(jasonObject);
				String message = array.toString();
				openRequestBO.setStatus_msg(message);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(openRequestBO.getStatus_msg());
	}

	public void jobSTC(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		OpenRequestBO openRequestBO = new OpenRequestBO();
		OpenRequestBO customer_OR_BO = new OpenRequestBO();
		openRequestBO.setTripid(request.getParameter("tripid") == null ? "" : request.getParameter("tripid"));
		String latidue = request.getParameter("stLa");
		String longitude = request.getParameter("stLo");
		JSONArray array = new JSONArray();
		if (latidue == null || latidue.equals("")) {
			latidue = "0.00000";
		}
		if (longitude == null || longitude.equals("")) {
			longitude = "0.00000";
		}
		openRequestBO.setDriverid(adminBO.getUid());
		openRequestBO.setAssociateCode(adminBO.getAssociateCode());
		openRequestBO.setDriverUName(adminBO.getUname());
		int result = 0;
		String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			result = RequestDAO.updateOpenRequestOnDriverSTC(openRequestBO, adminBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			result = RequestDAO.updateOpenRequestOnDriverSTC(openRequestBO, adminBO,myString);
		}
		try {
			if (result > 0) {
				JSONObject jasonObject = new JSONObject();
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "STC");
				jasonObject.put("rC", 200);
				jasonObject.put("STATUS", "SUCCESS");
				jasonObject.put("MESSAGE", "Soon to Clear, Successfully Updated");
				jasonObject.put("TI", openRequestBO.getTripid());
				array.put(jasonObject);
				String message = array.toString();
				AuditDAO.insertJobLogs("Soon To Clear", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.jobSTC, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
				openRequestBO.setStatus_msg(message);
				if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
					customer_OR_BO = ServiceRequestDAO.CustomerOpenRequest(adminBO.getCompanyList(),adminBO.getAssociateCode(), openRequestBO.getTripid());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					customer_OR_BO = ServiceRequestDAO.CustomerOpenRequest(myString,adminBO.getAssociateCode(), openRequestBO.getTripid());
				}
				if(customer_OR_BO.getTripSource()==0){
					pushFuctionCalling(customer_OR_BO,"Soon to Clear",adminBO.getAssociateCode());
				}
				if(customer_OR_BO.getTripSource()==4){
					CertificateBO certBo = CustomerMobileDAO.getIos_CertificateDetails(adminBO.getAssociateCode());
					DriverCabQueueBean customerBo = CustomerMobileDAO.getKeyForGooglePushById(openRequestBO,adminBO.getAssociateCode());
					pushMSG1("Driver will end your trip very soon", certBo.getPath(), certBo.getPassword() , customerBo.getGkey(),5);
				}
				
				String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
				int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
				if(isUpdateGPS==1){
					ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), latidue, longitude, adminBO.getVehicleNo(), 6, openRequestBO.getTripid());
				}
			} else {
				JSONObject jasonObject = new JSONObject();
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "STC");
				jasonObject.put("rC", 200);
				jasonObject.put("STATUS", "FAILURE");
				jasonObject.put("MESSAGE", "Not Successfully Soon to Clear Updated");
				array.put(jasonObject);
				String message = array.toString();
				openRequestBO.setStatus_msg(message);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(openRequestBO.getStatus_msg());
	}


	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used for end trip.
	 */
	public void geMtEndTrip(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		JSONArray array1 = new JSONArray();
		JSONObject resObj = new JSONObject();
		JSONArray array2 = new JSONArray();
		JSONObject jasonobject = new JSONObject();
		OpenRequestBO openRequestBO = ServiceRequestDAO.getOpenRequestByTripID(request.getParameter("tripid"), adminBO, null);
		String queueID="";
		boolean updateEndAddress = false;
		String latidue = request.getParameter("eLa");
		String longitude = request.getParameter("eLo");
		if (latidue == null || latidue.equals("")) {
			latidue = "0.00000";
		}
		if (longitude == null || longitude.equals("")) {
			longitude = "0.00000";
		}
		if(openRequestBO!=null){
			if(openRequestBO.getEdlatitude()==null || openRequestBO.getEdlatitude().equals("")){
				openRequestBO.setEdlatitude(latidue);
			}
			if(openRequestBO.getEdlongitude()==null || openRequestBO.getEdlongitude().equals("")){
				openRequestBO.setEdlongitude(longitude);
			}
		}else{
			System.out.println("GRACIERROR");
			response.getWriter().write("GRACIERROR;");
			return;
		}
		if (!latidue.equals("") && !latidue.equals("0.00000")) {
			ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) this.getServletContext().getAttribute((adminBO.getMasterAssociateCode() + "Zones"));
			//Exception 1
			String queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(openRequestBO.getEdlatitude()), Double.parseDouble(openRequestBO.getEdlongitude()));
			queueID = queueId;
			if (queueId.equals("")) {
				ZoneTableBeanSP zonesId = ZoneDAO.getDefaultZone(adminBO.getMasterAssociateCode());
				queueId = zonesId.getZoneKey();
			}
			openRequestBO.setEndQueueno(queueId);
		}

		String odoMeter = request.getParameter("odoMeter");
		if (odoMeter != null && !odoMeter.equals("")) {
			MobileDAO.insertOdoMeterValues(adminBO.getAssociateCode(), odoMeter, request.getParameter("tripid"), adminBO.getUid(), request.getParameter("cabNo"), "End Trip");
		}
		String address = request.getParameter("address").replace("'", "");
		if (address != null && !address.equals("") && (openRequestBO.getEadd1()==null || openRequestBO.getEadd1().equals(""))) {
			openRequestBO.setEadd1(address);
			openRequestBO.setEcity(request.getParameter("city")==null?"":request.getParameter("city"));
			openRequestBO.setEstate(request.getParameter("state")==null?"":request.getParameter("state"));
			//			openRequestBO.setEadd2("**DUP**");
			updateEndAddress = true;
		}
		
		/*System.out.println("End address:"+openRequestBO.getEadd1());
		System.out.println("End city:"+openRequestBO.getEcity());
		System.out.println("End state:"+openRequestBO.getEstate());*/
		
		openRequestBO.setDriverid(adminBO.getUid());
		openRequestBO.setDriverUName(adminBO.getUname());
		openRequestBO.setAssociateCode(adminBO.getAssociateCode());

		int result = 0;
		String jobsCode = DispatchDAO.getCompCode(request.getParameter("tripid"), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			result = ServiceRequestDAO.setEndTrip(openRequestBO,updateEndAddress,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			result = ServiceRequestDAO.setEndTrip(openRequestBO,updateEndAddress,myString);
		}
		//result = ServiceRequestDAO.setEndTrip(openRequestBO, updateEndAddress);
		AuditDAO.insertJobLogs("Trip Ended", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.tripCompleted, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
		
		String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
		int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
		if(isUpdateGPS==1){
			ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), latidue, longitude, adminBO.getVehicleNo(), 7, openRequestBO.getTripid());
		}
		
		//ServiceRequestDAO.insertDriver_BooktoZoneHistory(adminBO.getUid(), adminBO.getVehicleNo(), queueID, "3","Driver End Trip; TripId:"+openRequestBO.getTripid(), adminBO.getAssociateCode());
		//ServiceRequestDAO.Update_BtZ_DriverStatus(adminBO.getUid(), adminBO.getVehicleNo(), queueID, "3","Driver End Trip; Trip id: "+openRequestBO.getTripid(), adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
		// StringBuffer buffer = new StringBuffer();
		// String response = "";
		
		String wasl_ccode = TDSProperties.getValue("WASL_Company");
		if(openRequestBO.getAssociateCode().equals(wasl_ccode) && result>0){
			CustomerUtil process = new CustomerUtil(adminBO, openRequestBO, getServletConfig());
			process.run();
		}
		
		try {
			if (result > 0) {
				ArrayList<ChargesBO> al_list = ChargesDAO.getChargesForTrips(openRequestBO.getTripid(), adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
				if (al_list.size() > 0) {
					for (int i = 0; i < al_list.size(); i++) {
						// buffer.append("PK=" + al_list.get(i).getKey()+
						// ";PM="+al_list.get(i).getAmount()+"^");
						resObj = new JSONObject();
						try {
							resObj.put("PK", al_list.get(i).getKey());
							resObj.put("PM", al_list.get(i).getAmount());
							array1.put(resObj);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				if (openRequestBO.getPaytype().equals("CC")) {
					jasonobject = new JSONObject();
					PaymentProcessBean processBean = ChargesDAO.getCCDetails(openRequestBO.getAssociateCode(), openRequestBO.getTripid(), 2);
					// response =
					// "ResVer=1.1;STATUS=APPROVED;MESSAGE=Trip End Successfully;TXNID="+(processBean.getB_trans_id().equals("")?"0":processBean.getB_trans_id())+";APPROVALCODE="+(processBean.getB_approval_code().equals("")?"0":processBean.getB_approval_code())+";AMOUNT="+(processBean.getB_amount().equals("")?"0.00":processBean.getB_amount()+";NoT="+al_list.size()+";");
					jasonobject.put("rV", 1.2);
					jasonobject.put("Act", "ET");
					jasonobject.put("rC", 200);
					jasonobject.put("STATUS", "APPROVED");
					jasonobject.put("MESSAGE", "Trip Ended Successfully");
					jasonobject.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
					jasonobject.put("APPROVALCODE", (processBean.getB_approval_code().equals("") ? "0" : processBean.getB_approval_code()));
					jasonobject.put("AMOUNT", (processBean.getB_amount().equals("") ? "0.00" : processBean.getB_amount()));
					jasonobject.put("TripSource", openRequestBO.getTripSource()+"");
					jasonobject.put("CL", al_list.size());
					array2.put(jasonobject);

				} else if (openRequestBO.getPaytype().equals("VC")) {
					jasonobject = new JSONObject();
					VoucherBO voucherBO = ProcessingDAO.getVaidityOfVoucher(openRequestBO.getAcct(), adminBO.getMasterAssociateCode());
					// response =
					// "ResVer=1.1;STATUS=APPROVED;MESSAGE=Trip End Successfully;TXNID=0;PT=VC;AMOUNT="+(voucherBO.getVamount().equals("")?"0.00":voucherBO.getVamount())+";TA="+voucherBO.getNoTip()+";NoT="+al_list.size()+";";
					jasonobject.put("rV", 1.2);
					jasonobject.put("Act", "ET");
					jasonobject.put("rC", 200);
					jasonobject.put("STATUS", "APPROVED");
					jasonobject.put("MESSAGE", "Trip Ended Successfully");
					jasonobject.put("TXNID", 0);
					jasonobject.put("PT", "VC");
					jasonobject.put("AMOUNT", (voucherBO.getVamount().equals("") ? "0.00" : voucherBO.getVamount()));
					jasonobject.put("TA", voucherBO.getNoTip());
					jasonobject.put("CL", al_list.size());
					jasonobject.put("TripSource", openRequestBO.getTripSource()+"");
					array2.put(jasonobject);
				} else {
					// response =
					// "ResVer=1.1;STATUS=SUCCESS;MESSAGE=Trip End Successfully;TXNID=0"+";NoT="+al_list.size()+";";
					jasonobject = new JSONObject();
					jasonobject.put("rV", 1.2);
					jasonobject.put("Act", "ET");
					jasonobject.put("rC", 200);
					jasonobject.put("STATUS", "SUCCESS");
					jasonobject.put("MESSAGE", "Trip Ended Successfully");
					jasonobject.put("TXNID", 0);
					jasonobject.put("CL", al_list.size());
					jasonobject.put("TripSource", openRequestBO.getTripSource()+"");
					array2.put(jasonobject);
				}
				//				String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
				if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
					DispatchDAO.moveToHistory(openRequestBO.getTripid(), openRequestBO.getAssociateCode(), "",adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					DispatchDAO.moveToHistory(openRequestBO.getTripid(), openRequestBO.getAssociateCode(), "",myString);
				}
				//				DispatchDAO.moveToHistory(openRequestBO.getTripid(), openRequestBO.getAssociateCode(), "");
				ChargesDAO.updateChargeDriver(adminBO.getAssociateCode(), openRequestBO.getTripid(), adminBO.getUid(),adminBO.getMasterAssociateCode());

			} else {
				System.out.println("Error While End Trip For Trip Id--->"+request.getParameter("tripid"));
				jasonobject = new JSONObject();
				jasonobject.put("rV", 1.2);
				jasonobject.put("Act", "ET");
				jasonobject.put("rC", 300);
				jasonobject.put("STATUS", "WARN");
				jasonobject.put("CL", -1);
				jasonobject.put("TS", 99);
				jasonobject.put("TI", request.getParameter("tripid"));
				jasonobject.put("TripSource", openRequestBO.getTripSource()+"");
				jasonobject.put("MESSAGE", "Trip Already Ended");
				array2.put(jasonobject);
			}
			if(openRequestBO.getTripSource()==0){
				pushFuctionCalling(openRequestBO,"End Trip",adminBO.getAssociateCode());
			}
			if(openRequestBO.getTripSource()==4){
				CertificateBO certBo = CustomerMobileDAO.getIos_CertificateDetails(adminBO.getAssociateCode());
				DriverCabQueueBean customerBo = CustomerMobileDAO.getKeyForGooglePushById(openRequestBO,adminBO.getAssociateCode());
				//pushMSG("have to send message", certBo.getPath(), certBo.getPassword() , customerBo.getGkey());
				pushMSG1("Driver end the trip successfully", certBo.getPath(), certBo.getPassword() , customerBo.getGkey(),6);
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// System.out.println(""+ openRequestBO.getStatus_msg());
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		JSONArray mainArray = new JSONArray();
		try {
			mainArray.put(0, array2);
			mainArray.put(1, array1);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// response.getWriter().write(response+buffer.toString());
		//System.out.println("array2:"+array2.toString());
		//System.out.println("endtrip response"+mainArray.toString());
		response.getWriter().write(mainArray.toString());
	}

	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Descrition This method is used for no show.
	 */
	public void getMNoShow(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO.setTripid(request.getParameter("tripid"));
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		String latidue = request.getParameter("nSLa");
		String longitude = request.getParameter("nSLo");
		if (latidue == null || latidue.equals("")) {
			latidue = "0.00000";
		}
		if (longitude == null || longitude.equals("")) {
			longitude = "0.00000";
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		openRequestBO.setDriverid(adminBO.getUid());
		int result = 0;
		String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			result = RequestDAO.updateOpenRequestOnDriverReportingNoShow(openRequestBO, adminBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			result = RequestDAO.updateOpenRequestOnDriverReportingNoShow(openRequestBO, adminBO, myString);
		}
		//		int result = RequestDAO.updateOpenRequestOnDriverReportingNoShow(openRequestBO, adminBO);
		AuditDAO.insertJobLogs("Driver No Show", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.driverNoShow, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
		if(adminBO.getJob_No_Show_Property()==1){
			DriverCabQueueBean cabQueueBo = DispatchDAO.getDriverByDriverID(adminBO.getMasterAssociateCode(), adminBO.getUid(), 1);
			OpenRequestBO orBo = new OpenRequestBO(); 
			if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
				orBo = RequestDAO.getOpenRequestStatus(openRequestBO.getTripid(), adminBO,adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				orBo = RequestDAO.getOpenRequestStatus(openRequestBO.getTripid(), adminBO, myString);
			}

			//			RequestDAO.getOpenRequestStatus(openRequestBO.getTripid(), adminBO);
			ServiceRequestDAO.updateOpenRequestStatus(adminBO.getAssociateCode(), openRequestBO.getTripid(), TDSConstants.driverNoShow+"", "", "", "", "");
			//			String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
				DispatchDAO.moveToHistory(openRequestBO.getTripid(), openRequestBO.getAssociateCode(), "",adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				DispatchDAO.moveToHistory(openRequestBO.getTripid(), adminBO.getAssociateCode(), "",myString);
			}
			//			DispatchDAO.moveToHistory(openRequestBO.getTripid(), adminBO.getAssociateCode(),"");
			AuditDAO.insertJobLogs("System Closing Job.", openRequestBO.getTripid(),adminBO.getAssociateCode(),TDSConstants.driverNoShow, adminBO.getUid(),"","",adminBO.getMasterAssociateCode());
			String[] message = MessageGenerateJSON.generateMessage(orBo, "OC", System.currentTimeMillis(), adminBO.getAssociateCode());
			if(orBo.getDriverid()!=null&&!orBo.getDriverid().equalsIgnoreCase("")){
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBo, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
		}
		if (result == 1) {
			response.getWriter().write("ResVer=1.1;ReturnCode=001");
		} else {
			response.getWriter().write("ResVer=1.1;ReturnCode=000");
		}
		// openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "","0",
		// adminBO);
	}

	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to check payment from voucher or
	 *      credit card.
	 */
	public void getMCheckPayment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In checkCreditCard method ");
		String paymentType = "";
		String amount = "";
		String cardNo = "";
		String cardType = "";
		String cardName = "";
		String cardExpiryDate = "";
		String cardCuu = "";

		String voucherNo = "";
		String voucherName = "";

		StringBuffer resultStrBuffer = new StringBuffer();
		PrintWriter printWriter = response.getWriter();
		response.setContentType("text/xml");

		resultStrBuffer.append("<?xml version=\"1.0\"?>");
		resultStrBuffer.append("<CheckPayment>");
		if (request.getParameter("ptype") != null) {
			paymentType = request.getParameter("ptype");
		}

		if (request.getParameter("amount") != null) {
			amount = request.getParameter("amount");
		}

		if (paymentType.equalsIgnoreCase("CC")) {
			if (request.getParameter("cno") != null) {
				cardNo = request.getParameter("cno");
			}
			if (request.getParameter("ctype") != null) {
				cardType = request.getParameter("ctype");
			}
			if (request.getParameter("cname") != null) {
				cardName = request.getParameter("cname");
			}
			if (request.getParameter("cexpiry") != null) {
				cardExpiryDate = request.getParameter("cexpiry");
			}
			if (request.getParameter("ccuu") != null) {
				cardCuu = request.getParameter("ccuu");
			}

			if (cardNo != "" && cardCuu != "" && cardExpiryDate != "" && cardType != "" && cardName != "") {
				// Check for credit card Payment
				// forwardURL = TDSConstants.getSuccessJSP;
				resultStrBuffer.append("<Status>Failure</Status>");
				resultStrBuffer.append("<Description>Invalid Data for Credit Card Statement</Description>");
			} else {
				// Check for credit Card Payment
				// forwardURL = TDSConstants.getFailureJSP;
				resultStrBuffer.append("<Status>Success</Status>");
				resultStrBuffer.append("<Description>Credit Card Payment is Approved</Description>");
			}
		} else if (paymentType.equalsIgnoreCase("VC")) {
			if (request.getParameter("vno") != null) {
				voucherNo = request.getParameter("vno");
			}
			if (request.getParameter("vname") != null) {
				voucherName = request.getParameter("vname");
			}
			if (voucherNo != "" && voucherName != "") {
				int result = 0;
				result = ServiceRequestDAO.checkVoucherNo(voucherNo, voucherName);
				if (result > 0) {
					resultStrBuffer.append("<Status>Success</Status>");
					resultStrBuffer.append("<Description>Voucher detail is Present</Description>");
				} else {
					resultStrBuffer.append("<Status>Failure</Status>");
					resultStrBuffer.append("<Description>Invalid Data Voucher</Description>");
				}
			} else {
				resultStrBuffer.append("<Status>Failure</Status>");
				resultStrBuffer.append("<Description>Invalid Data Voucher</Description>");
			}
		} else if (paymentType.equalsIgnoreCase("GV")) {
			cat.info("In Government Voucher");
			if (request.getParameter("vno") != null) {
				voucherNo = request.getParameter("vno");
			}
			if (request.getParameter("vname") != null) {
				voucherName = request.getParameter("vname");
			}
		}
		// String forwardURL = "";
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		resultStrBuffer.append("</CheckPayment>");
		printWriter.write(resultStrBuffer.toString());
		printWriter.close();
	}

	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to make the payment through the
	 *      mobile Service <BR>
	 *      For this method, tripid=Trip id, amount=Amount, ptype=Payment Type,
	 *      pno=Payment No, vname=Voucher No as parameter <BR>
	 *      Their URL's are
	 * 
	 *      For Voucher <BR>
	 *      /control?action=mobilerequest&event=mMakePayment&ptype=&amount=&
	 *      tripid=&pno=&vname= <BR>
	 *      For CC <BR>
	 *      /control?action=mobilerequest&event=mMakePayment&ptype=&amount=&
	 *      tripid=&pno= <BR>
	 * 
	 */
	@SuppressWarnings("unchecked")
	public void getMMakePayment(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		cat.info("In updateCompletedRequest method ");
		String driverid = "";
		String tripid = "";
		String amount = "";
		String paymentType = "";
		String paymentNo = "";
		String vname = "";
		Map driverMap = null;
		String smsNo = "";

		boolean paymentMade = false;
		String messageSubject = "";
		String messageContent = "";
		SendingSMS sendSMS = null;
		String assocCode = null;
		if (adminBO != null) {
			driverid = adminBO.getUid();
			assocCode = adminBO.getAssociateCode();
		}
		StringBuffer resultStrBuffer = new StringBuffer();
		PrintWriter printWriter = response.getWriter();
		response.setContentType("text/xml");

		resultStrBuffer.append("<?xml version=\"1.0\"?>");
		resultStrBuffer.append("<MakePayment>");

		if (request.getParameter("tripid") != null) {
			tripid = request.getParameter("tripid");
		}
		if (request.getParameter("amount") != null) {
			amount = request.getParameter("amount");
		}
		if (request.getParameter("ptype") != null) {
			paymentType = request.getParameter("ptype");
		}
		if (request.getParameter("pno") != null) {
			paymentNo = request.getParameter("pno");
		}
		if (request.getParameter("vname") != null) {
			vname = request.getParameter("vname");
		}
		cat.info("Driver id " + driverid);
		cat.info("Trip id " + tripid);
		cat.info("Amount " + amount);
		cat.info("Payment Type " + paymentType);
		cat.info("Payment No " + paymentNo);

		@SuppressWarnings("rawtypes")
		HashMap hmp_payment = new HashMap();
		hmp_payment.put("driverid", driverid);
		hmp_payment.put("tripid", tripid);
		hmp_payment.put("amount", amount);
		hmp_payment.put("paymentType", paymentType);
		hmp_payment.put("assocCode", assocCode);
		if (paymentType.equalsIgnoreCase("CC")) {
			hmp_payment.put("paymentNo", paymentNo.substring(paymentNo.length() - 4, paymentNo.length()));

			if (paymentNo.charAt(0) == '3') {
				hmp_payment.put("cardType", "CCAX");
				//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Card Type  CCAX");
			} else if (paymentNo.charAt(0) == '4') {
				hmp_payment.put("cardType", "CCVI");
				//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Card Type  CCVI");
			} else if (paymentNo.charAt(0) == '5') {
				hmp_payment.put("cardType", "CCMC");
				//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Card Type  CCMC");
			} else if (paymentNo.charAt(0) == '6') {
				hmp_payment.put("cardType", "CCMC");
				//System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Card Type  CCMC");
			}
			hmp_payment.put("securityCode", "");
		} else {
			hmp_payment.put("paymentNo", paymentNo);
			if (paymentType.equalsIgnoreCase("VC")) {
				hmp_payment.put("cardType", "VCCO");
			} else if (paymentType.equalsIgnoreCase("GV")) {
				hmp_payment.put("cardType", "VCGO");
			}
		}
		if (driverid != "" && tripid != "" && amount != "" && paymentNo != "" && paymentType != "") {
			//System.out.println("In check loop");
			//System.out.println("Payment type:" + paymentType);
			if (paymentType.equals("VC") || paymentType.equals("GV")) {
				//System.out.println("In VC || GV");
				if (vname != "") {
					//System.out.println("Vname" + vname + ServiceRequestDAO.checkVoucherNo(paymentNo, vname));
					if (ServiceRequestDAO.checkVoucherNo(paymentNo, vname) == 1) {
						int result = ServiceRequestDAO.updateCompletedRequest(hmp_payment, adminBO);
						if (result > 0) {
							// cat.info("Try to insert client table");
							//System.out.println("~~~~~~~~~~~~In side the Voucher Type~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
							paymentMade = true;
							resultStrBuffer.append("<Status>Success</Status>");
							resultStrBuffer.append("<Description>Payment Made by Voucher</Description>");
							/*
							 * request.setAttribute("page",
							 * "Successfully updated TDS_COMPLETEDREQUEST ");
							 * screen = TDSConstants.getSuccessJSP;
							 */
						} else {
							resultStrBuffer.append("<Status>Failure</Status>");
							resultStrBuffer.append("<Description>Payment Update Error</Description>");
							/*
							 * request.setAttribute("page",
							 * "Failure to update TDS_COMPLETEDREQEST"); screen
							 * = TDSConstants.getFailureJSP;
							 */
						}
					} else if (ServiceRequestDAO.checkVoucherNo(paymentNo, vname) == 2 && ServiceRequestDAO.checkVoucherNo(paymentNo, vname) == 0) {
						/*
						 * request.setAttribute("page","Not a Valid Voucher No"
						 * ); screen = TDSConstants.getFailureJSP;
						 */
						resultStrBuffer.append("<Status>Failure</Status>");
						resultStrBuffer.append("<Description>Not a Valid Voucher No</Description>");
					} else {
						/*
						 * request.setAttribute("page",
						 * "Voucher No Entred is already in use"); screen =
						 * TDSConstants.getFailureJSP;
						 */
						resultStrBuffer.append("<Status>Failure</Status>");
						resultStrBuffer.append("<Description>Voucher Already Used</Description>");
					}
				} else {
					/*
					 * request.setAttribute("page","Please Enter Company Name")
					 * ; screen = TDSConstants.getFailureJSP;
					 */
					resultStrBuffer.append("<Status>Failure</Status>");
					resultStrBuffer.append("<Description>Please Get Company Name</Description>");
				}

			}
			if (paymentType.equals("CC")) {
				//System.out.println("Payment:Credit ");
				int result = ServiceRequestDAO.updateCompletedRequest(hmp_payment, adminBO);
				if (result > 0) {
					// cat.info("Try to insert client table");
					//System.out.println("Inside in the credit card Details");
					paymentMade = true;
					/*
					 * request.setAttribute("page",
					 * "Successfully updated TDS_COMPLETEDREQUEST "); screen =
					 * TDSConstants.getSuccessJSP;
					 */
					resultStrBuffer.append("<Status>Success</Status>");
					resultStrBuffer.append("<Description>Credit Payment is made</Description>");
				} else {
					/*
					 * request.setAttribute("page",
					 * "Failure to update TDS_COMPLETEDREQEST"); screen =
					 * TDSConstants.getFailureJSP;
					 */
					resultStrBuffer.append("<Status>Failure</Status>");
					resultStrBuffer.append("<Description>Payment Update Error</Description>");
				}
			}

		} else {
			/*
			 * request.setAttribute("page","You missied some fields"); screen
			 * = TDSConstants.getFailureJSP;
			 */
			resultStrBuffer.append("<Status>Failure</Status>");
			resultStrBuffer.append("<Description>Invalid Entry</Description>");
		}
		if (paymentMade) {
			ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
			if (paymentType.equalsIgnoreCase("CC")) {
				messageSubject = "TDSCCA";
				messageContent = messageSubject + ";" + tripid + ";" + "Y;" + amount;
			} else if (paymentType.equalsIgnoreCase("VC")) {
				messageSubject = "TDSVC";
				messageContent = messageSubject + ";" + tripid + ";" + "Y;" + amount;
			} else if (paymentType.equalsIgnoreCase("GV")) {
				messageSubject = "TDSGV";
				messageContent = messageSubject + ";" + tripid + ";" + "Y;" + amount;
			} else {
				messageSubject = "TDSINVALID";
				messageContent = messageSubject + ";" + tripid + ";" + "N;" + amount;
			}
			sendSMS = new SendingSMS(poolBO.getSMTP_HOST_NAME(), poolBO.getSMTP_AUTH_USER(), poolBO.getSMTP_AUTH_PWD(), poolBO.getEmailFromAddress(), poolBO.getSMTP_PORT(), poolBO.getProtocol());
			driverMap = ServiceRequestDAO.getDriverPhoneNumber(driverid);
			if (driverMap != null) {
				smsNo = driverMap.get("phoneNo").toString() + "@" + driverMap.get("smsId").toString();
			}
			//System.out.println("Driver Phone " + smsNo);
			try {

				// To ram 9703668956@messaging.sprintpcs.com
				sendSMS.sendMail(smsNo, messageContent);
			} catch (MessagingException p_messageExp) {
				p_messageExp.getStackTrace();
			}
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		resultStrBuffer.append("</MakePayment>");
		printWriter.write(resultStrBuffer.toString());
		printWriter.close();
	}

	/**
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to create a new trip from the mobile
	 *      devices.
	 */
	/*
	 * public void getMOpenRequest(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException, IOException {
	 * HttpSession session = request.getSession();
	 * cat.info("In directOpenRequest method "); String driverid = ""; String
	 * stlongitude = ""; String stlatitude = ""; AdminRegistrationBO
	 * adminBO;
	 * 
	 * StringBuffer resultStrBuffer = new StringBuffer(); PrintWriter
	 * printWriter = response.getWriter();
	 * response.setContentType("text/xml");
	 * 
	 * resultStrBuffer.append("<?xml version=\"1.0\"?>");
	 * resultStrBuffer.append("<MobileOpenRequest>");
	 * 
	 * if(session.getAttribute("user") != null) { adminBO =
	 * (AdminRegistrationBO) session.getAttribute("user"); driverid =
	 * adminBO.getUname(); }
	 * 
	 * if(request.getParameter("stlongitude") != null ) { stlongitude =
	 * request.getParameter("stlongitude"); }
	 * if(request.getParameter("stlatitude") != null ) { stlatitude =
	 * request.getParameter("stlatitude"); }
	 * 
	 * HashMap hmp_open = new HashMap(); hmp_open.put("driverid", driverid);
	 * hmp_open.put("stlatitude", stlatitude); hmp_open.put("stlongitude",
	 * stlongitude);
	 * 
	 * cat.info("Driver id "+driverid); cat.info("Latitude "+stlatitude);
	 * cat.info("Longitude "+stlongitude); int result =0; if(driverid != ""
	 * && stlatitude != "" && stlongitude !="") { result =
	 * ServiceRequestDAO.insertOpenRequest(hmp_open); } else {
	 * request.setAttribute("page","You missied some fields"); screen =
	 * TDSConstants.getFailureJSP;
	 * resultStrBuffer.append("<Status>Failure</Status>");
	 * resultStrBuffer.append("<Description>Invalid Data's</Description>"); }
	 * if(result > 0) { if(result >0 ) { request.setAttribute("page",
	 * "Successfully updated TDS_INPROGRESSREQUEST "); screen =
	 * TDSConstants.getSuccessJSP;
	 * resultStrBuffer.append("<Status>Success</Status>");
	 * resultStrBuffer.append
	 * ("<Description>Created new Request</Description>"); } else {
	 * request.setAttribute("page",
	 * "Failure to update TDS_INPROGRESSREQUEST"); screen =
	 * TDSConstants.getFailureJSP;
	 * resultStrBuffer.append("<Status>Failure</Status>");
	 * resultStrBuffer.append
	 * ("<Description>Creation New Trip Error</Description>"); } }
	 * resultStrBuffer.append("</MobileOpenRequest>");
	 * printWriter.write(resultStrBuffer.toString()); printWriter.close();
	 * }
	 */

	// function havent called
	public void getMAcceptedTripDetail(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		OpenRequestBO acceptedTripBO = new OpenRequestBO();
		String version = "";
		String tripID = "";
		if (request.getParameter("version") != null) {
			version = request.getParameter("version");
		}
		if (adminBO != null) {
			acceptedTripBO.setDriverid(adminBO.getUid());
		} else {
			return;
		}
		if (request.getParameter("tripid") != null) {
			tripID = request.getParameter("tripid");
		}
		ArrayList<String> tripStatus = new ArrayList<String>();
		tripStatus.add(TDSConstants.jobAllocated + "");
		tripStatus.add(TDSConstants.onRotueToPickup + "");
		tripStatus.add(TDSConstants.tripStarted + "");

		OpenRequestBO acceptedRequest = ServiceRequestDAO.getOpenRequestByTripID(tripID, adminBO, tripStatus);
		String[] message = MessageGenerateJSON.generateMessageAfterAcceptance(acceptedRequest, "FA", 0, adminBO.getMasterAssociateCode());
		if (message != null) {
			response.getWriter().write(message[0]);
		} else {
			response.getWriter().write("");
		}
	}

	public void getAllAcceptedTripDetail(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		OpenRequestBO acceptedTripBO = new OpenRequestBO();
		List acceptedTripList = null;
		if (adminBO != null) {
			acceptedTripBO.setDriverid(adminBO.getUid());
		} else {
			return;
		}
		if (request.getParameter("tripid") != null) {
			acceptedTripBO.setTripid(request.getParameter("tripid"));
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		request.setAttribute("acceptedTrip", acceptedTripList);
		getServletContext().getRequestDispatcher(TDSConstants.macceptedTripDetailJSP).forward(request, response);
	}

	@SuppressWarnings("null")
	public void getMTripDetail(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		OpenRequestBO openRequestBO = null;
		String tripID = request.getParameter("tripid");
		openRequestBO = new OpenRequestBO();
		openRequestBO.setTripid(tripID);
		openRequestBO.setDriverid(adminBO.getUid());
		openRequestBO.setAssociateCode(adminBO.getAssociateCode());
		String jobsCode = DispatchDAO.getCompCode(tripID, adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO, myString);
		}
		String version = "";
		if (request.getParameter("version") != null) {
			version = request.getParameter("version");
		}
		String[] message = new String[2];
		JSONArray array = new JSONArray();
		JSONObject jasonObject = new JSONObject();
		try {
			jasonObject.put("rV", 1.2);
			jasonObject.put("Act", "MTD");
			jasonObject.put("rC", 400);
			jasonObject.put("Command", "Invalid Trip");
			array.put(jasonObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		message[0] = array.toString();
		if (openRequestBO != null) {
			if (openRequestBO.getTripStatus().equals(TDSConstants.srJobHold + "") || openRequestBO.getTripStatus().equals(TDSConstants.tripOnHold + "")) {
				openRequestBO.setTripStatus(TDSConstants.performingDispatchProcesses + "");
			}
			message = MessageGenerateJSON.generateMessage(openRequestBO, "A", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
			AuditDAO.insertJobLogs("DR:Getting Offered Trip Details", tripID, adminBO.getAssociateCode(), TDSConstants.driverGettingDetails, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		} else {
			if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
				openRequestBO = RequestDAO.getOpenRequestStatus(tripID, adminBO,adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				openRequestBO = RequestDAO.getOpenRequestStatus(tripID, adminBO, myString);
			}
			//Exception 2
			if (openRequestBO!=null && openRequestBO.getTripStatus().equalsIgnoreCase(TDSConstants.jobAllocated + "")) {
				message = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO, "AA", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
				AuditDAO.insertJobLogs("DR:Getting Accepted Trip Details", tripID, adminBO.getAssociateCode(), TDSConstants.driverGettingDetails, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			} else {
				AuditDAO.insertJobLogs("DR:Trying to get details of trip", tripID, adminBO.getAssociateCode(), TDSConstants.driverGettingDetails, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
			}
		}
		response.getWriter().write(message[0]);
	}

	public void getMPublicRequest(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		// !!!! Public Request For Association Later Change into Parameterized
		// !!!!
		StringBuffer buffer = new StringBuffer();
		JSONArray mainArray = new JSONArray();
		ArrayList<OpenRequestBO> openRequestList = RequestDAO.getOpenRequest(adminBO.getAssociateCode(), adminBO.getTimeZone(),adminBO.getMasterAssociateCode());
		try {
			if (openRequestList != null) {
				JSONObject header = new JSONObject();
				header.put("rV", 1.2);
				header.put("Act", "PR");
				header.put("rC", 200);
				header.put("NoT", openRequestList.size());
				mainArray.put(0, header);
				// buffer.append("ResVer=1.1;NoT=" + openRequestList.size() +
				// ";~");
				
				int n=1;
				for (int i = 0; i < openRequestList.size(); i++) {
					OpenRequestBO requestBO = openRequestList.get(i);
					String message = new String();
					message = MessageGenerateJSON.generateMessage(requestBO, "BCJ", System.currentTimeMillis(), adminBO.getMasterAssociateCode())[0];
					// buffer.append(MessageGenerateJSON.generateMessage(requestBO,
					// "A", System.currentTimeMillis(),
					// adminBO.getMasterAssociateCode())[0] + "^");
					JSONArray subArray = new JSONArray(message);
					mainArray.put(n, subArray);
					n++;
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(mainArray.toString());
	}

	/**
	 * @author vimal
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 * @see Description This method is used to upload the user signature based
	 *      on the trip id. For this service user give trip id as parameter, and
	 *      the location the signature.
	 */
	public void gettingSignature(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {

		StringBuffer resultStrBuffer = new StringBuffer();
		StringBuffer dataBuffer = new StringBuffer();
		String dataLine;
		String tripId = "";
		String driverid = "";
		PrintWriter printWriter = response.getWriter();
		InputStream dataStream;
		BufferedReader bufferReader;
		if (adminBO != null) {
			driverid = adminBO.getUname();
		}
		if (request.getParameter("tripid") != null) {
			tripId = request.getParameter("tripid");
		}
		response.setContentType("text/xml");
		resultStrBuffer.append("<?xml version=\"1.0\"?>");
		resultStrBuffer.append("<UpdateSignature>");
		if (driverid != "" && tripId != "") {
			//System.out.println("Input Stream " + request.getInputStream());
			//System.out.println("Content length " + request.getContentLength());
			dataStream = new InputStream(request.getInputStream());
			// if(contentType != null &&
			// contentType.indexOf("multipart/form-data") >= 0) {
			bufferReader = new BufferedReader(new InputStreamReader(dataStream));
			while ((dataLine = bufferReader.readLine()) != null) {
				dataBuffer.append(dataLine);
			}
			//System.out.println("Data Bytes " + dataBuffer.toString());
			if (ServiceRequestDAO.updateSignature(dataBuffer.toString(), driverid, tripId)) {
				resultStrBuffer.append("<Status>Successfully Updated</Status>");
			} else {
				resultStrBuffer.append("<Status>Failed to Update</Status>");
			}
			/*
			 * } else {
			 * resultStrBuffer.append("<Status>Invalid Content Type</Status>"
			 * ); }
			 */
		} else {
			resultStrBuffer.append("<Status>Invalid Driverid or Tripid</Status>");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		resultStrBuffer.append("</UpdateSignature>");
		printWriter.write(resultStrBuffer.toString());
		printWriter.close();
	}

	public void getNoOfDriverinQueueNo(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String count = ServiceRequestDAO.noOfDriverinQueueID(adminBO.getAssociateCode());
		response.getWriter().write(count);
	}

	public void getQueueDataList(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		// StringBuffer buffer = new StringBuffer();
		List queueCoordinateList = SystemPropertiesDAO.getQueueCoOrdinateList("", adminBO.getMasterAssociateCode(), "");
		if (queueCoordinateList != null) {
			request.setAttribute("queueList", queueCoordinateList);
		}
		JSONArray array = new JSONArray();
		for (int i = 0; i < queueCoordinateList.size(); i++) {
			JSONObject details = new JSONObject();
			QueueCoordinatesBO coordinatesBO = (QueueCoordinatesBO) queueCoordinateList.get(i);
			// buffer.append(coordinatesBO.getQueueId()+"^"+coordinatesBO.getQDescription()+"^"+coordinatesBO.getQDelayTime()+"^"+coordinatesBO.getSmsResponseTime());
			if (!coordinatesBO.getQTypeofQueue().equalsIgnoreCase("D")) {
				ArrayList<Double> externalLatLon = ZoneDAO.readQueueExternalPoint(adminBO.getMasterAssociateCode(), coordinatesBO.getQueueId());
				/*
				 * buffer.append("QID=" + coordinatesBO.getQueueId() +
				 * ";QDesc="+ coordinatesBO.getQDescription() + ";QResTime="+
				 * coordinatesBO.getSmsResponseTime() +
				 * ";QDelayTime="+coordinatesBO.getQDelayTime() +";ELA="+
				 * externalLatLon.get(0) + ";ELO="+ externalLatLon.get(1) +
				 * ";V="+ coordinatesBO.getVersionNumber() + "^");
				 */
				try {
					//System.out.println(adminBO.getMasterAssociateCode());
					details.put("QID", coordinatesBO.getQueueId());
					details.put("QDesc", coordinatesBO.getQDescription());
					details.put("QResTime", coordinatesBO.getSmsResponseTime());
					details.put("QDelayTime", coordinatesBO.getSmsResponseTime());
					details.put("ELA", externalLatLon.get(0));
					details.put("ELO", externalLatLon.get(1));
					details.put("V", coordinatesBO.getVersionNumber());
					array.put(details);
				} catch (Exception e) {
					System.out.println("Error :" + e);
				}
			}
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(array.toString());
		/*
		 * AdminRegistrationBO adminBO = (AdminRegistrationBO)
		 * request.getSession().getAttribute("user"); ArrayList<ZoneTableBeanSP>
		 * allZonesForCompany = (ArrayList<ZoneTableBeanSP>)
		 * getServletConfig().getServletContext
		 * ().getAttribute((adminBO.getMasterAssociateCode() + "Zones")); String
		 * jsonResponse =
		 * Feed.getAllZones(adminBO.getMasterAssociateCode(),allZonesForCompany
		 * ); System.out.println("\nThis is getQueueDataList..............\n");
		 * response.getWriter().write(jsonResponse);
		 */
	}

	public void getCompleteQueueDataList(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getMasterAssociateCode() + "Zones"));
		String jsonResponse = Feed.getAllZones(adminBO.getMasterAssociateCode(), allZonesForCompany, adminBO.getUid());
		System.out.println("resp:"+jsonResponse);
		response.getWriter().write(jsonResponse);
	}

	public void getQueueBoundries(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		/*
		 * AdminRegistrationBO adminBO = (AdminRegistrationBO)
		 * request.getSession().getAttribute("user");
		 * 
		 * @SuppressWarnings("unchecked") ArrayList<ZoneTableBeanSP>
		 * allZonesForCompany = (ArrayList<ZoneTableBeanSP>)
		 * getServletConfig().getServletContext
		 * ().getAttribute((adminBO.getMasterAssociateCode() + "Zones")); String
		 * jsonResponse =
		 * Feed.getAllZones(adminBO.getMasterAssociateCode(),allZonesForCompany
		 * ); System.out.println("\nThis is getQueueBoundries..............\n");
		 * response.getWriter().write(jsonResponse);
		 */
		StringBuffer buffer = new StringBuffer();
		ArrayList<Double> latitude = new ArrayList<Double>();
		ArrayList<Double> longitude = new ArrayList<Double>();
		ArrayList<Double> maxAndMinLatitude = new ArrayList<Double>();
		ArrayList<Double> maxAndMinLongitude = new ArrayList<Double>();
		String zoneID = request.getParameter("QID");
		JSONArray array = new JSONArray();
		JSONObject details = new JSONObject();
		ZoneDAO.readQueueBoundries(adminBO.getMasterAssociateCode(), zoneID, latitude, longitude, maxAndMinLatitude, maxAndMinLongitude);
		for (int i = 0; i < latitude.size(); i++) {
			// buffer.append("O=" + i + ";LA=" + latitude.get(i) + ";LO="+
			// longitude.get(i) + "^");
			try {
				details.put("o", i);
				details.put("LA", latitude.get(i));
				details.put("LO", longitude.get(i));
				array.put(details);

			} catch (Exception e) {
				System.out.println("\ngetQueueBoundries error---->" + e + "\n");
			}
		}
		response.getWriter().write(array.toString());
	}

	public void loginToQueue(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		DriverLocationBO driverLocationBO = new DriverLocationBO();
		String userType = "";
		String userid = "";
		String userKey = "";
		String assoccode = "";
		String queueId = "";
		int getJobsFromAdjacentZones = 0;
		// DoQueueProcess queueIdProcess;
		int position = 0;
		String zoneStatus = "NotLoggedIn";
		// String zoneName = "";
		// List publicRequestXML = null;
		// int locationStatus = -1;
		int queueStatus = -1;
		if (adminBO != null) {
			userType = adminBO.getUsertypeDesc();
			userid = adminBO.getUid();
			userKey = adminBO.getUname();
			assoccode = adminBO.getAssociateCode();
		}
		if (request.getParameter("latitude") != null) {
			driverLocationBO.setLatitude(request.getParameter("latitude"));
		} else {
			driverLocationBO.setLatitude("0");
		}
		if (request.getParameter("longitude") != null) {
			driverLocationBO.setLongitude(request.getParameter("longitude"));
		} else {
			driverLocationBO.setLongitude("0");
		}
		if (request.getParameter("availability") != null) {
			driverLocationBO.setStatus(request.getParameter("availability"));
		} else {
			driverLocationBO.setStatus("0");
		}
		if (request.getParameter("queueName") != null) {
			queueId = request.getParameter("queueName");
		}
		if (request.getParameter("adjZones") != null) {
			// System.out.println("adjZones is "+
			// request.getParameter("adjZones"));
			if (request.getParameter("adjZones").equals("")) {
				getJobsFromAdjacentZones = 0;
			} else {
				getJobsFromAdjacentZones = Integer.parseInt(request.getParameter("adjZones"));
			}
		} else {
			getJobsFromAdjacentZones = 0;
		}

		if (userid != "") {
			driverLocationBO.setDriverid(userid);
		}
		if(request.getParameter("loginType")!=null && !request.getParameter("loginType").equalsIgnoreCase("0")){
			System.out.println("Driver--->"+adminBO.getUid()+" is auto login/spl login to zone--->"+queueId+" value--->"+request.getParameter("loginType"));
		}
		String alarmTime = SecurityDAO.getAlarmTimeForDriverId(adminBO);
		if (ServiceRequestDAO.checkQueueIsAvailableForDriver(adminBO.getMasterAssociateCode(), queueId, userid) && (alarmTime == null || adminBO.getDriverAlarm() < Integer.parseInt(alarmTime))) {
			// Check if driver is logged into another queue. If he is then
			// delete and then add into current queue.
			ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
			if (currentQueue==null || currentQueue.size()<=0) {
				queueStatus = ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(), queueId, assoccode, 1, adminBO.getDriver_flag(), adminBO.getPhnumber(), adminBO.getProvider(), adminBO.getGoogRegKey(), getJobsFromAdjacentZones, adminBO.getVehicleNo(), adminBO.getMasterAssociateCode());
				if(ZoneDAO.getShortDistanceType(adminBO.getMasterAssociateCode(), queueId)==1){
					ServiceRequestDAO.updateDriverAvailability("Z", adminBO.getUid(), adminBO.getAssociateCode());
				}
				//ServiceRequestDAO.Update_BtZ_ZoneLogin(driverLocationBO.getDriverid(), adminBO.getVehicleNo(), queueId, "0", "Driver Logged into Zone "+queueId, assoccode,adminBO.getMasterAssociateCode());
				ServiceRequestDAO.insert_DriverLoginToQueue(driverLocationBO.getDriverid(), adminBO.getVehicleNo(), queueId, assoccode, adminBO.getMasterAssociateCode(), "1", "Driver("+driverLocationBO.getDriverid()+") logged into zone:"+queueId);
				zoneStatus = "LoggedIn";
			} else if (currentQueue!=null || currentQueue.size() > 0 && !currentQueue.get(0).equalsIgnoreCase(queueId)) {
				// Delete from current queue and add into new queue
				ServiceRequestDAO.deleteDriverFromQueue(driverLocationBO.getDriverid(), adminBO.getMasterAssociateCode());
				queueStatus = ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(), queueId, assoccode, 1, adminBO.getDriver_flag(), adminBO.getPhnumber(), adminBO.getProvider(), adminBO.getGoogRegKey(), getJobsFromAdjacentZones, adminBO.getVehicleNo(), adminBO.getMasterAssociateCode());
				//ServiceRequestDAO.Update_BtZ_ZoneLogin(driverLocationBO.getDriverid(), adminBO.getVehicleNo(), queueId, "0", "Driver Logged into "+queueId+" from "+currentQueue, assoccode,adminBO.getMasterAssociateCode());

				ServiceRequestDAO.Update_DriverLogOutfromQueue(driverLocationBO.getDriverid(), adminBO.getVehicleNo(), currentQueue.get(0), assoccode, "2", "Driver("+driverLocationBO.getDriverid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". Before logged into new Zone.");
				ServiceRequestDAO.insert_DriverLoginToQueue(driverLocationBO.getDriverid(), adminBO.getVehicleNo(), queueId, assoccode, adminBO.getMasterAssociateCode(), "1", "Driver("+driverLocationBO.getDriverid()+") logged into zone:"+queueId);
				zoneStatus = "QueueSwitched";
				// position =
				// ServiceRequestDAO.insertQueuePostition(queueId);
			} else if (currentQueue!=null || currentQueue.size() > 0 && currentQueue.get(0).equalsIgnoreCase(queueId)) {
				zoneStatus = "AlreadyLoggedIn";
			}
			position = ServiceRequestDAO.getQueuePosition(queueId, userid, assoccode);
			// driverMap.put("position", +position+"");
			// queueStatus =
			// ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(),
			// queueId,assoccode, position);
			// System.out.println("Insert in to Driver Queue "+position);
		} else {
			System.out.println("Queue is not allowed for driver--->"+adminBO.getUid());
			zoneStatus = "NotAllowed";
			position = -1;
			// zoneName = queueId;
		}
		JSONArray totalArray = new JSONArray();
		JSONArray arrayZonelogin = new JSONArray();
		JSONArray arrayJobs = new JSONArray();
		try {
			ArrayList<OpenRequestBO> openRequestList = RequestDAO.getOpenRequest(adminBO.getAssociateCode(), adminBO.getTimeZone(),adminBO.getMasterAssociateCode());
			if (openRequestList != null) {
				JSONObject numberOfJobs = new JSONObject();
				numberOfJobs.put("NoT", openRequestList.size());
				//System.out.println("NoT" + openRequestList.size());
				arrayJobs.put(numberOfJobs);
				totalArray.put(0, arrayJobs);
			}
			// ArrayList<DriverAndJobs> zoneDriverAndJobs =
			// RegistrationDAO.getDriverAndJobSummary(adminBO.getMasterAssociateCode());
			// if(zoneDriverAndJobs.size()>0){
			JSONObject Zlogin = new JSONObject();
			Zlogin.put("ZoneStatus", zoneStatus);
			Zlogin.put("ZonePosition", position);
			Zlogin.put("ZoneName", queueId);
			//System.out.println("ZoneStatus" + zoneStatus);
			arrayZonelogin.put(Zlogin);
			totalArray.put(1, arrayZonelogin);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		response.getWriter().write(totalArray.toString());
	}

	/*
	 * public void loginToQueue(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException, IOException {
	 * ApplicationPoolBO poolBO = (ApplicationPoolBO)
	 * getServletContext().getAttribute("poolBO"); HttpSession session =
	 * request.getSession(); //
	 * System.out.println("In Set Driver location method");
	 * cat.info("In loginToQueue method"); DriverLocationBO driverLocationBO =
	 * new DriverLocationBO(); // Map driverMap = new HashMap();
	 * AdminRegistrationBO adminBO = null; String userType = ""; String userid
	 * = ""; String userKey = ""; String assoccode = ""; String queueId =
	 * ""; int getJobsFromAdjacentZones = 0; // DoQueueProcess queueIdProcess;
	 * int position = 0; String zoneStatus = "NotLoggedIn"; // String zoneName
	 * = ""; //List publicRequestXML = null;
	 * 
	 * //int locationStatus = -1; int queueStatus = -1;
	 * 
	 * if (session.getAttribute("user") != null) { adminBO =
	 * (AdminRegistrationBO) session.getAttribute("user"); userType =
	 * adminBO.getUsertypeDesc(); userid = adminBO.getUid(); userKey =
	 * adminBO.getUname(); assoccode = adminBO.getAssociateCode(); } if
	 * (request.getParameter("latitude") != null) {
	 * driverLocationBO.setLatitude(request.getParameter("latitude")); } else
	 * { driverLocationBO.setLatitude("0"); } if
	 * (request.getParameter("longitude") != null) {
	 * driverLocationBO.setLongitude(request.getParameter("longitude")); }
	 * else { driverLocationBO.setLongitude("0"); } if
	 * (request.getParameter("availability") != null) {
	 * driverLocationBO.setStatus(request.getParameter("availability")); }
	 * else { driverLocationBO.setStatus("0"); } if
	 * (request.getParameter("queueName") != null) { queueId =
	 * request.getParameter("queueName"); } if
	 * (request.getParameter("adjZones") != null) { //
	 * System.out.println("adjZones is "+ //
	 * request.getParameter("adjZones")); if
	 * (request.getParameter("adjZones").equals("")) {
	 * getJobsFromAdjacentZones = 0; } else { getJobsFromAdjacentZones =
	 * Integer.parseInt(request.getParameter("adjZones")); } } else {
	 * getJobsFromAdjacentZones = 0; }
	 * 
	 * if (userid != "") { driverLocationBO.setDriverid(userid); }
	 * 
	 * String alarmTime=SecurityDAO.getAlarmTimeForDriverId(adminBO); if
	 * (ServiceRequestDAO
	 * .checkQueueIsAvailableForDriver(adminBO.getMasterAssociateCode(),
	 * queueId, userid) && (alarmTime==null ||
	 * adminBO.getDriverAlarm()<Integer.parseInt(alarmTime))) { // Check if
	 * driver is logged into another queue. If he is then // delete and then add
	 * into current queue. String currentQueue =
	 * ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(userid); if
	 * (currentQueue.equalsIgnoreCase("")) { queueStatus =
	 * ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(),
	 * queueId, assoccode, 0, adminBO.getDriver_flag(),
	 * adminBO.getPhnumber(), adminBO.getProvider(),
	 * adminBO.getGoogRegKey(), getJobsFromAdjacentZones,
	 * adminBO.getVehicleNo(),adminBO.getMasterAssociateCode()); zoneStatus
	 * = "LoggedIn"; } else if (!currentQueue.equalsIgnoreCase(queueId)) { //
	 * Delete from current queue and add into new queue
	 * ServiceRequestDAO.deleteDriverFromQueue(driverLocationBO.getDriverid(),
	 * adminBO.getAssociateCode()); queueStatus =
	 * ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(),
	 * queueId, assoccode, 0, adminBO.getDriver_flag(),
	 * adminBO.getPhnumber(), adminBO.getProvider(),
	 * adminBO.getGoogRegKey(), getJobsFromAdjacentZones,
	 * adminBO.getVehicleNo(),adminBO.getMasterAssociateCode()); zoneStatus
	 * = "QueueSwitched"; // position = //
	 * ServiceRequestDAO.insertQueuePostition(queueId);
	 * 
	 * } else if (currentQueue.equalsIgnoreCase(queueId)) { zoneStatus =
	 * "AlreadyLoggedIn"; } position =
	 * ServiceRequestDAO.getQueuePosition(queueId, userid, assoccode); //
	 * driverMap.put("position", +position+""); // queueStatus = //
	 * ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(), //
	 * queueId,assoccode, position); //
	 * System.out.println("Insert in to Driver Queue "+position); } else {
	 * zoneStatus = "NotAllowed"; position = -1; // zoneName = queueId; }
	 * 
	 * response.getWriter().print("ZoneStatus=" + zoneStatus +
	 * ";ZonePosition=" + position + ";ZoneName=" + queueId + ";");
	 * 
	 * }
	 */

	public void zoneLogout(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String result="";
		JSONArray mainArray = new JSONArray();
		try {
			JSONObject jasonObject = new JSONObject();
			if(request.getParameter("reasonType")!=null && !request.getParameter("reasonType").equals("")){
				System.out.println("Logging Driver "+adminBO.getUid()+" Out From Zone,For "+request.getParameter("reasonType"));
			}
			if (!adminBO.getUid().equals("")) {
				result = ServiceRequestDAO.getQueueIdByDriverIDandAssoccode(adminBO.getAssociateCode(), adminBO.getUid());
				//ServiceRequestDAO.Update_BtZ_ZoneLogout(adminBO.getUid(), adminBO.getVehicleNo(), result, "3","Driver logged out himself out", adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
				if(!result.equals("")){
					ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), result, adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+")himself logged out of the zone:"+result);
				}
				ServiceRequestDAO.deleteDriverFromQueue(adminBO.getUid(), adminBO.getMasterAssociateCode());
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "ZLO");
				jasonObject.put("rC", 200);
				jasonObject.put("ZoneStatus", "LoggedOut");
				mainArray.put(jasonObject);
				// response.getWriter().print("ZoneStatus=LoggedOut");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().print(mainArray.toString());
	}

	public void callPassenger(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String tripID = request.getParameter("tripID");
		String minutes = request.getParameter("minutes");
		String messageNO = request.getParameter("messageNo");
		//System.out.println("Prefix To Append--->"+adminBO.getPhonePrefix());
		String phoneNumber = adminBO.getPhonePrefix() + ServiceRequestDAO.getPhoneNoByTripID(adminBO.getMasterAssociateCode(), adminBO.getUid(), tripID);
		String driverPhone = adminBO.getPhonePrefix() + adminBO.getPhnumber();
		if (phoneNumber.length() > 2 && !messageNO.equals("0")) {
			IVRPassengerCallerOutBound ivr = new IVRPassengerCallerOutBound(messageNO, minutes, phoneNumber, adminBO.getMasterAssociateCode());
			ivr.start();
		} else if (phoneNumber.length() > 2 && driverPhone.length() > 2) {
			IVRDriverPassengerConf ivr = new IVRDriverPassengerConf(phoneNumber, adminBO.getMasterAssociateCode(), driverPhone,tripID,adminBO.getUid());
			ivr.start();
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
	}

	public void sendCustomResponse(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		MessageInMemory customMessage = (MessageInMemory) getServletConfig().getServletContext().getAttribute(adminBO.getUid() + "Q");
		if (!customMessage.getTripID().equals("")) {
			OpenRequestBO openRequest = RequestDAO.getOpenRequestByTripID(customMessage.getTripID(), adminBO.getAssociateCode(), adminBO.getTimeZone());
			if (openRequest != null) {
				StringBuffer buffer = new StringBuffer();
				buffer.append("ResVer=1.1;Command=" + customMessage.getCommand());
				buffer.append(";TI=" + customMessage.getTripID());
				buffer.append(";SLA=" + openRequest.getSlat());
				buffer.append(";SLO=" + openRequest.getSlong());
				buffer.append(";SD=" + openRequest.getSdate());
				buffer.append(";SI=" + openRequest.getSintersection());
				buffer.append(";ST=" + openRequest.getSdate());
				buffer.append(";SA1=" + openRequest.getSadd1());
				buffer.append(";SA2=" + openRequest.getSadd2());
				buffer.append(";C=" + openRequest.getScity());
				buffer.append(";Z=" + openRequest.getSzip());
				System.out.println("Ram Custom Response" + buffer.toString());
				if (request.getParameter("version") != null) {
					String version = request.getParameter("version");
				}
				response.getWriter().write(buffer.toString());
			}
		}
	}

	public void getZonePosition(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
		JSONArray array = new JSONArray();
		try{
			JSONObject header = new JSONObject();			
			if (currentQueue!=null && currentQueue.size()>0) {
				int position = ServiceRequestDAO.getQueuePosition(currentQueue.get(0), adminBO.getUid(), adminBO.getAssociateCode());
				header.put("rV", 1.2);
				header.put("Act", "ZP");
				header.put("rC", 200);
				header.put("ZS", "LI");
				header.put("ZN", currentQueue.get(0));
				header.put("ZD", currentQueue.get(1));
				header.put("ZP", position);
				array.put(header);
				//buffer.append("ResVer=1.1;ZS=LI;ZP=" + position + ";ZN=" + currentQueue);
			} else {
				header.put("rV", 1.2);
				header.put("Act", "ZP");
				header.put("rC", 200);
				header.put("ZS", "LO");
				array.put(header);
				//buffer.append("ResVer=1.1;ZS=LO");
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());

	}

	public void getZoneJobSummary(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		MessageInMemory customMessage = (MessageInMemory) getServletConfig().getServletContext().getAttribute(adminBO.getUid() + "Q");
		if (!customMessage.getTripID().equals("")) {
			OpenRequestBO openRequest = RequestDAO.getOpenRequestByTripID(customMessage.getTripID(), adminBO.getAssociateCode(), adminBO.getTimeZone());
			if (openRequest != null) {
				StringBuffer buffer = new StringBuffer();
				buffer.append("Command=" + customMessage.getCommand());
				buffer.append(";TI=" + customMessage.getTripID());
				buffer.append(";SLA=" + openRequest.getSlat());
				buffer.append(";SLO=" + openRequest.getSlong());
				buffer.append(";SD=" + openRequest.getSdate());
				buffer.append(";SI=" + openRequest.getSintersection());
				buffer.append(";ST=" + openRequest.getSttime());
				buffer.append(";SA1=" + openRequest.getSadd1());
				buffer.append(";SA2=" + openRequest.getSadd2());
				buffer.append(";C=" + openRequest.getScity());
				buffer.append(";Z=" + openRequest.getSzip());
				if (request.getParameter("version") != null) {
					String version = request.getParameter("version");
				}
				response.getWriter().write(buffer.toString());
			}
		}
	}


	public void driverZonesList(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String currentzone = request.getParameter("currentZone");
		ArrayList<DriverCabQueueBean> driversettle = ServiceRequestDAO.getZoneDetails(currentzone, adminBO.getMasterAssociateCode(), adminBO.getAssociateCode());
		JSONArray array = new JSONArray();
		if (driversettle.size() > 0) {
			for (int i = 0; i < driversettle.size(); i++) {
				JSONObject status = new JSONObject();
				try {
					status.put("driverId", driversettle.get(i).getDriverid());
					status.put("cabNo", driversettle.get(i).getCabno());
					array.put(status);
				} catch (Exception e) {
					System.out.println("Error :" + e);
				}
			}
		}
		response.getWriter().write(array.toString());
	}

	public void driverZonesHistory(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String currentzone = request.getParameter("currentZone");//driverid
		String driverid = request.getParameter("driverId");
		ArrayList<QueueBean> driversettle = ServiceRequestDAO.getZoneHistoryDetails(currentzone, adminBO.getMasterAssociateCode(), adminBO.getAssociateCode(),driverid,adminBO.getTimeZone());
		JSONArray array = new JSONArray();
		if (driversettle.size() > 0) {
			for (int i = 0; i < driversettle.size(); i++) {
				JSONObject status = new JSONObject();
				try {
					status.put("driverId", driversettle.get(i).getDriverId());
					status.put("cabNo", driversettle.get(i).getVehicleNo());
					status.put("logoutime", driversettle.get(i).getDq_LogoutTime());
					status.put("reason", driversettle.get(i).getReason());

					array.put(status);
				} catch (Exception e) {
					System.out.println("Error :" + e);
				}
			}
		}
		response.getWriter().write(array.toString());
	}

	//not used in internet.java
	public void autoRelogin(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		if (adminBO != null) {
			buffer.append("ResVer=1.1;Login=Valid;" + "SessionID=" + adminBO.getSessionId() + ";");
			ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
			if (currentQueue!=null && currentQueue.size()>0) {
				int position = ServiceRequestDAO.getQueuePosition(currentQueue.get(0), adminBO.getUid(), adminBO.getAssociateCode());
				buffer.append("ZoneStatus=LoggedIn;ZonePosition=" + position + ";ZoneName=" + currentQueue);
			} else {
				buffer.append("ZoneStatus=NotLoggedIn;");
			}
		} else {
			response.getWriter().write("ResVer=1.1;Login=InValid;");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");

		}
		response.getWriter().write(buffer.toString());
	}
	//not calledc from internet.java
	public void companyJobStatusSummary(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		if (adminBO != null) {
			buffer.append("ResVer=1.1;Login=Valid;" + "SessionID=" + adminBO.getSessionId() + ";");
			ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
			if (currentQueue!=null && currentQueue.size()>0) {
				int position = ServiceRequestDAO.getQueuePosition(currentQueue.get(0), adminBO.getUid(), adminBO.getAssociateCode());
				buffer.append("ZoneStatus=LoggedIn;ZonePosition=" + position + ";ZoneName=" + currentQueue);
			} else {
				buffer.append("ZoneStatus=NotLoggedIn;");
			}
		} else {
			response.getWriter().write("ResVer=1.1;Login=InValid;");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(buffer.toString());
	}

	/**
	 * @author Venki
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void getNumberOfDriversAndJobs(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		if (adminBO != null) {
			JSONArray totalArray = new JSONArray();
			JSONObject totalObject = new JSONObject();
			JSONArray arrayZones = new JSONArray();
			JSONArray arrayJobs = new JSONArray();
			ArrayList<OpenRequestBO> openRequestList = RequestDAO.getOpenRequest(adminBO.getAssociateCode(), adminBO.getTimeZone(),adminBO.getMasterAssociateCode());
			if (openRequestList != null) {
				JSONObject numberOfJobs = new JSONObject();
				try {
					numberOfJobs.put("NOT", openRequestList.size());
					arrayJobs.put(numberOfJobs);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			ArrayList<DriverAndJobs> zoneDriverAndJobs = RegistrationDAO.getDriverAndJobSummary(adminBO.getMasterAssociateCode(),adminBO.getAssociateCode());
			if (zoneDriverAndJobs.size() > 0) {
				for (int i = 0; i < zoneDriverAndJobs.size(); i++) {
					JSONObject zoneDriverJobs = new JSONObject();
					try {
						zoneDriverJobs.put("Z", zoneDriverAndJobs.get(i).getZoneNumber() == null ? "" : zoneDriverAndJobs.get(i).getZoneNumber());
						zoneDriverJobs.put("ZD", zoneDriverAndJobs.get(i).getZoneDesc() == null ? "" : zoneDriverAndJobs.get(i).getZoneDesc());
						zoneDriverJobs.put("NJ", zoneDriverAndJobs.get(i).getNumberOfJobs() == null ? "" : zoneDriverAndJobs.get(i).getNumberOfJobs());
						zoneDriverJobs.put("ND", zoneDriverAndJobs.get(i).getNumberOfDrivers() == null ? "" : zoneDriverAndJobs.get(i).getNumberOfDrivers());
						zoneDriverJobs.put("FJ", zoneDriverAndJobs.get(i).getFuture1HourJobs() == null ? "" : zoneDriverAndJobs.get(i).getFuture1HourJobs());
						arrayZones.put(zoneDriverJobs);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				try {
					totalArray.put(0, arrayJobs);
					totalArray.put(1, arrayZones);
					// totalArray.put(totalObject);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				response.getWriter().write(totalArray.toString());
			} else {
				response.getWriter().write("");
			}
		}
	}

	public void getNumberOfDrivers(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		if (adminBO != null) {
			ArrayList<DriverAndJobs> zoneDriverAndJobs = RegistrationDAO.getDriverAndJobSummary(adminBO.getMasterAssociateCode(),adminBO.getAssociateCode());
			for (int i = 0; i < zoneDriverAndJobs.size(); i++) {
				buffer.append("Z=" + zoneDriverAndJobs.get(i).getZoneNumber() + ";ZD=" + zoneDriverAndJobs.get(i).getZoneDesc() + ";NJ=" + zoneDriverAndJobs.get(i).getNumberOfJobs() + ";ND=" + zoneDriverAndJobs.get(i).getNumberOfDrivers() + "^");
			}
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(buffer.toString());
	}

	//havent called from internet.java
	public void startFlagTrip(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {

		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO.setTripid(request.getParameter("tripid") == null ? "" : request.getParameter("tripid"));
		openRequestBO.setSlat(request.getParameter("startlat"));
		openRequestBO.setSlong(request.getParameter("startlong"));
		openRequestBO.setDriverid(adminBO.getUid());
		openRequestBO.setAssociateCode(adminBO.getAssociateCode());
		openRequestBO.setDriverUName(adminBO.getUname());
		openRequestBO.setTripSource(TDSConstants.flagTrip);
		int result = 0;
		result = RequestDAO.saveOpenRequestHistory(openRequestBO, adminBO);
		if (result > 0) {
			openRequestBO.setStatus_msg("ResVer=1.1;STATUS=SUCCESS;MESSAGE=Trip Started Successfully");
		} else {
			openRequestBO.setStatus_msg("ResVer=1.1;STATUS=FAILURE;MESSAGE=Failed To Start a Trip");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(openRequestBO.getStatus_msg());
	}

	//not used
	public void endFlagTrip(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO.setTripid(request.getParameter("tripid") == null ? "" : request.getParameter("tripid"));
		openRequestBO.setSlat(request.getParameter("endlat"));
		openRequestBO.setSlong(request.getParameter("endlong"));
		openRequestBO.setDriverid(adminBO.getUid());
		openRequestBO.setAssociateCode(adminBO.getAssociateCode());
		openRequestBO.setDriverUName(adminBO.getUname());
		openRequestBO.setTripSource(TDSConstants.flagTrip);
		int result = 0;
		result = RequestDAO.updateOpenRequestHistory(openRequestBO, adminBO);
		if (result > 0) {
			openRequestBO.setStatus_msg("ResVer=1.1;STATUS=SUCCESS;MESSAGE=Trip Started Successfully");
		} else {
			openRequestBO.setStatus_msg("ResVer=1.1;STATUS=FAILURE;MESSAGE=Failed To Start a Trip");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(openRequestBO.getStatus_msg());
	}

	public void allocatedJobs(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String cabNo = request.getParameter("cabNo") == null ? "" : request.getParameter("cabNo");
		String driverId = request.getParameter("driverId") == null ? "" : request.getParameter("driverId");
		ArrayList<OpenRequestBO> openBo = new ArrayList<OpenRequestBO>();
		String message = "";
		openBo = RequestDAO.getAllocatedJobs(adminBO.getMasterAssociateCode(), driverId, cabNo, adminBO.getTimeZone(),adminBO.getMasterAssociateCode());
		for (int i = 0; i < openBo.size(); i++) {
			message = message + MessageGenerateJSON.generateMessageAfterAcceptance(openBo.get(i), "AA", i, adminBO.getMasterAssociateCode()) + "^";
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(message);
	}

	public void checkJobStatus(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String tripId = request.getParameter("tripId");
		OpenRequestBO openBo = new OpenRequestBO();;
		String[] message = new String[2];
		if (!tripId.equals("")) {
			String jobsCode = DispatchDAO.getCompCode(tripId, adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
				openBo = RequestDAO.getOpenRequestStatus(tripId, adminBO,adminBO.getCompanyList());
				if(openBo!=null && Integer.parseInt(openBo.getTripStatus())==TDSConstants.jobAllocated && jobsCode.equals(adminBO.getChangeCompanyCode())){
					openBo.setAssociateCode(adminBO.getAssociateCode());
					if(RequestDAO.updateOpenRequestOnDriverAccept(openBo,adminBO,adminBO.getCompanyList())>0){
						if(openBo.getRouteNumber()==null || openBo.getRouteNumber().equals("") || openBo.getRouteNumber().equals("0")){
							AuditDAO.insertJobLogs("Job Allocated", openBo.getTripid(), adminBO.getAssociateCode(), TDSConstants.jobAccepted, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
						}else{
							AuditDAO.insertJobLogsForSharedRide("Job Allocated", openBo.getRouteNumber(), adminBO.getAssociateCode(), TDSConstants.jobAccepted);
						}
					}
				}
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				openBo = RequestDAO.getOpenRequestStatus(tripId, adminBO, myString);
			}
		}
		if (openBo != null) {
			if (openBo.getTripStatus().equals(TDSConstants.performingDispatchProcesses + "") || openBo.getTripStatus().equals(TDSConstants.newRequestDispatchProcessesNotStarted + "")) {
				message = MessageGenerateJSON.generateMessage(openBo, "A", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
			} else if (openBo.getTripStatus().equals(TDSConstants.jobCompleted + "") || openBo.getTripStatus().equals(TDSConstants.noShowRequest + "") || openBo.getTripStatus().equals(TDSConstants.customerCancel + "") || openBo.getTripStatus().equals(TDSConstants.operatorCancelledRequest + "") || openBo.getTripStatus().equals(TDSConstants.paymentReceived+"") ) {
				message = MessageGenerateJSON.generateMessageAfterCompletion(openBo, "AA", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
			} else if (openBo.getTripStatus().equals(TDSConstants.onSite + "") || openBo.getTripStatus().equals(TDSConstants.tripStarted + "") || openBo.getTripStatus().equals(TDSConstants.jobSTC + "")) {
				message = MessageGenerateJSON.generateMessageOnSite(openBo, "AA", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
			} else {
				message = MessageGenerateJSON.generateMessageAfterAcceptance(openBo, "AA", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
			}
		} else {
			JSONArray array = new JSONArray();
			JSONObject obj = new JSONObject();
			try {
				obj.put("rV", 1.2);
				obj.put("Act", "GM");
				obj.put("rC", 200);
				obj.put("Command", "NA");
				obj.put("TI", tripId);
				obj.put("D", "Job Not Available");
				obj.put("TS", "99");
				array.put(obj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			message[0] = array.toString();
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(message[0]);
	}

	public void driverLocation(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String resultZone = "";
		String zoneNo = (request.getParameter("zoneNumber") == null ? "" : request.getParameter("zoneNumber"));
		double latitude = Double.parseDouble(request.getParameter("latitude"));
		double longitude = Double.parseDouble(request.getParameter("longitude"));
		ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getAssociateCode() + "Zones"));
		if (allZonesForCompany != null && allZonesForCompany.size() > 0) {
			resultZone = CheckZone.checkZone(allZonesForCompany, zoneNo, latitude, longitude);
		} else {
			SystemUtils.reloadZones(this, adminBO.getAssociateCode());
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write("RZ=" + resultZone + ";");
	}
	public void taxiChargeDriverLocation(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String resultZone = "";
		String zoneNo = (request.getParameter("zoneNumber") == null ? "" : request.getParameter("zoneNumber"));
		double latitude = Double.parseDouble(request.getParameter("latitude"));
		double longitude = Double.parseDouble(request.getParameter("longitude"));
		ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getAssociateCode() + "Zones"));
		if (allZonesForCompany != null && allZonesForCompany.size() > 0) {
			resultZone = CheckZone.checkZoneStatus(allZonesForCompany, zoneNo, latitude, longitude);
		} else {
			SystemUtils.reloadZones(this, adminBO.getAssociateCode());
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(resultZone);
	}
	public void driverList(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		JSONArray array = new JSONArray();
		JSONArray array1 = new JSONArray();
		JSONArray mainarray = new JSONArray();
		ArrayList<DriverLocationBO> finalList = new ArrayList<DriverLocationBO>();
		ArrayList<DriverCabQueueBean> driverBO = DispatchDAO.allDrivers(adminBO.getAssociateCode(), 0.00, 0.00, 0.00, null, "");
		CompanySystemProperties csBean = SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());
		try {
			if (driverBO.size() == 0) {
				JSONObject startcheck = new JSONObject();
				startcheck.put("rV", 1.2);
				startcheck.put("Act", "DL");
				startcheck.put("rC", 400);
				startcheck.put("status", "No");
				array.put(startcheck);
				response.getWriter().write(array.toString());
				return;
			} else {
				JSONObject startcheck = new JSONObject();
				startcheck.put("rV", 1.2);
				startcheck.put("Act", "DL");
				startcheck.put("rC", 200);
				startcheck.put("status", "Done");
				array.put(startcheck);
			}
			if (csBean.getDriverListMobile() == 2) {
				DriverLocationBO newLocation = new DriverLocationBO();
				newLocation.setLatitude(driverBO.get(0).getCurrentLatitude() + "");
				newLocation.setLongitude(driverBO.get(0).getCurrentLongitude() + "");
				newLocation.setCounter(1);
				finalList.add(newLocation);
				for (int i = 1; i < driverBO.size(); i++) {
					boolean foundFinalListwithin2miles = false;
					for (int j = 0; j < finalList.size(); j++) {
						double distance = DistanceCalculation.distance(driverBO.get(i).getCurrentLatitude(), Double.parseDouble(finalList.get(j).getLatitude()), driverBO.get(i).getCurrentLongitude(), Double.parseDouble(finalList.get(j).getLongitude()));
						if (distance <= 2.00) {
							finalList.get(j).setCounter(finalList.get(j).getCounter() + 1);
							foundFinalListwithin2miles = true;
							break;
						}
					}
					if (!foundFinalListwithin2miles) {
						newLocation = new DriverLocationBO();
						newLocation.setLatitude(driverBO.get(i).getCurrentLatitude() + "");
						newLocation.setLongitude(driverBO.get(i).getCurrentLongitude() + "");
						newLocation.setCounter(1);
						finalList.add(newLocation);
					}
				}
				if (finalList.size() > 0) {
					for (int k = 0; k < finalList.size() - 1; k++) {
						JSONObject driversobj = new JSONObject();
						driversobj.put("LA", finalList.get(k).getLatitude());
						driversobj.put("L0", finalList.get(k).getLongitude());
						driversobj.put("Count", finalList.get(k).getCounter());
						driversobj.put("M", "1");
						array1.put(driversobj);

						// buffer.append("LA=" + finalList.get(k).getLatitude()+
						// ";LO=" + finalList.get(k).getLongitude() +
						// ";Count=" + finalList.get(k).getCounter() + ";M=1^");
					}
					JSONObject driversobj = new JSONObject();
					driversobj.put("LA", finalList.get(finalList.size() - 1).getLatitude());
					driversobj.put("LO", finalList.get(finalList.size() - 1).getLongitude());
					driversobj.put("Count", finalList.get(finalList.size() - 1).getCounter());
					driversobj.put("M", "1");
					array1.put(driversobj);

					// buffer.append("LA=" + finalList.get(finalList.size() -
					// 1).getLatitude() + ";LO=" +
					// finalList.get(finalList.size() - 1).getLongitude() +
					// ";Count=" + finalList.get(finalList.size() -
					// 1).getCounter()+";M=1");
				}
			} else {
				for (int i = 0; i < driverBO.size() - 1; i++) {
					JSONObject driversobj = new JSONObject();
					driversobj.put("LA", driverBO.get(i).getCurrentLatitude());
					driversobj.put("LO", driverBO.get(i).getCurrentLongitude());
					driversobj.put("Count", "0");
					driversobj.put("M", "2");
					array1.put(driversobj);

					// buffer.append("LA=" +
					// driverBO.get(i).getCurrentLatitude() + ";LO=" +
					// driverBO.get(i).getCurrentLongitude() + ";Count=0;M=2^");
				}
				JSONObject driversobj = new JSONObject();
				driversobj.put("LA", driverBO.get(driverBO.size() - 1).getCurrentLatitude());
				driversobj.put("LO", driverBO.get(driverBO.size() - 1).getCurrentLongitude());
				driversobj.put("Count", "0");
				driversobj.put("M", "2");
				array1.put(driversobj);

				// buffer.append("LA=" + driverBO.get(driverBO.size() -
				// 1).getCurrentLatitude() + ";LO=" +
				// driverBO.get(driverBO.size() - 1).getCurrentLongitude() +
				// ";Count=0;M=2");
			}

			// System.out.println("adminBO in mobileaction-->"+adminBO.getDriverListMobile());

			mainarray.put(0, array);
			mainarray.put(1, array1);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		// buffer.append("#"+adminBO.getDriverListMobile());

		// System.out.println("Result--->" + mainarray.toString());
		response.getWriter().write(mainarray.toString());
	}

	public void getFullMessage(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String command = request.getParameter("Command");
		String key = request.getParameter("key");
		String message = MobileDAO.getFullMessage(adminBO.getMasterAssociateCode(), key);
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write("Command=" + command + ";" + "Message=" + message);
	}

	public void getAllocatedTrips(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		JSONArray mainArray = new JSONArray();
		String cabNo = RegistrationDAO.getCabNumber(adminBO.getUid(), adminBO.getAssociateCode(), "");
		List openRequestList = RequestDAO.getAllocatedJobs(adminBO.getMasterAssociateCode(), adminBO.getUid(), adminBO.getVehicleNo(), adminBO.getTimeZone(),adminBO.getMasterAssociateCode());
		ArrayList<String> tripIds = new ArrayList<String>();
		try {
			if (openRequestList.size() > 0) {
				JSONObject header = new JSONObject();
				header.put("rV", 1.2);
				header.put("Act", "MAT");
				header.put("rC", 200);
				header.put("NoT", openRequestList.size());
				mainArray.put(0, header);
				int n = 1;
				for (int i = 0; i < openRequestList.size(); i++) {
					OpenRequestBO requestBO = (OpenRequestBO) openRequestList.get(i);
					String message = new String();
					message = MessageGenerateJSON.generateMessageAfterAcceptance(requestBO, "AA", System.currentTimeMillis(), adminBO.getMasterAssociateCode())[0];
					JSONArray subArray = new JSONArray(message);
					tripIds.add(requestBO.getTripid());
					if(!requestBO.getDriverid().equals(adminBO.getUid()) || !requestBO.getVehicleNo().equals(adminBO.getVehicleNo())){
						AuditDAO.insertJobLogs("Job Viewd/Updated By Driver", requestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.updateJob, adminBO.getUid(), "0.00000", "0.00000", adminBO.getMasterAssociateCode());
					}
					mainArray.put(n, subArray);
					n++;
				}
				RequestDAO.updateJobDriver(adminBO.getAssociateCode(), adminBO.getUid(), tripIds, cabNo, 2);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(mainArray.toString());
	}

	public void checkEndtrip(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		String tripId = request.getParameter("tripid");
		int total = SharedRideDAO.checkEndTrip(adminBO.getAssociateCode(), tripId);
		buffer.append("total=" + total);
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(buffer.toString());
	}

	public void makeDriverUnavailable(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		DriverLocationBO driverLocationBO = new DriverLocationBO();
		String userType = "";
		if (adminBO != null) {
			driverLocationBO.setDriverid(adminBO.getUname());
		} else {
			return;
		}
		if (request.getParameter("latitude") != null) {
			driverLocationBO.setLatitude(request.getParameter("latitude"));
		} else {
			driverLocationBO.setLatitude("0");
		}
		if (request.getParameter("longitude") != null) {
			driverLocationBO.setLongitude(request.getParameter("longitude"));
		} else {
			driverLocationBO.setLongitude("0");
		}
		if (request.getParameter("availability") != null) {
			driverLocationBO.setStatus(request.getParameter("availability"));
		} else {
			driverLocationBO.setStatus("0");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		driverLocationBO.setAssocCode(adminBO.getAssociateCode());
		ServiceRequestDAO.updateDriverLocation(driverLocationBO,adminBO.getMasterAssociateCode());
		ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
		if(currentQueue!=null && currentQueue.size()>0){
			ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". Due to Driver Unavailable");
		}
		ServiceRequestDAO.deleteDriverFromQueue(adminBO.getUid(), adminBO.getMasterAssociateCode());
		int messageHandlerType = request.getParameter("v") != null ? 1 : 0;
		String message = HandleMemoryMessage.readMessage(getServletContext(), adminBO.getMasterAssociateCode(), adminBO.getUid(), driverLocationBO, messageHandlerType,adminBO.getChangeCompanyCode());
		if (!message.equals("")) {
			response.getWriter().write(message);
		} else {
			response.getWriter().write("LocationUpdate=Success");
		}
	}

	public void updateGooglePushId(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String googleRegistrationKey = request.getParameter("googRegKey") == null ? "" : request.getParameter("googRegKey");
		int result = 0;
		if (!googleRegistrationKey.equals("")) {
			result = ServiceRequestDAO.updateGoogleKey(googleRegistrationKey,adminBO.getSessionId(), adminBO);
			adminBO.setGoogRegKey(googleRegistrationKey);

			ObjectMapper mapper = new ObjectMapper();
			String finalJSON = mapper.writeValueAsString(adminBO);
			System.out.println("Insert/Update Session Values For Driver--->"+adminBO.getUid()+" For GCM Id");
			MobileDAO.insertSessionObjects(adminBO.getSessionId(),finalJSON.replaceAll("null", "''"),adminBO.getUid());

		}
		if (result == 1) {
			JSONArray array = new JSONArray();
			JSONObject jasonObject = new JSONObject();
			try {
				jasonObject.put("rV", 1.2);
				jasonObject.put("Status", "Sucess");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			array.put(jasonObject);
			response.getWriter().write(array.toString());
		} else {
			JSONArray array = new JSONArray();
			JSONObject jasonObject = new JSONObject();
			try {
				jasonObject.put("rV", 1.2);
				jasonObject.put("Status", "Fail");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			array.put(jasonObject);
			response.getWriter().write(array.toString());
		}
	}

	public void changePassword(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String oldPassword = request.getParameter("oldPassword") == null ? "" : request.getParameter("oldPassword");
		if (oldPassword != null && !oldPassword.equals("")) {
			String oldPasswordHash = "";
			String newPasswordHash = "";
			String userId = adminBO.getUid();/*
			 * request.getParameter("driver"
			 * )
			 */
			String newPassword = request.getParameter("newPassword") == null ? "" : request.getParameter("newPassword");
			try {
				oldPasswordHash = PasswordHash.encrypt(oldPassword);
			} catch (Exception e) {
				System.out.println("Exception-->" + e.getMessage());
			}
			if (oldPassword.equals(newPassword)) {
				response.getWriter().write("The Old Password and new Password given for Update are same,which is not needed");
			} else {
				if (newPassword.length() > 4) {
					try {
						newPasswordHash = PasswordHash.encrypt(newPassword);
					} catch (Exception e) {
						System.out.println("Exception-->" + e.getMessage());
					}
					int update = ServiceRequestDAO.updateUserPassword(adminBO.getAssociateCode(), userId, oldPasswordHash, newPasswordHash);
					if (update == 1) {
						response.getWriter().write("Password Updated Succesfully.");
					}else{
						response.getWriter().write("Password failed to Re-set");
					}
				} else {
					response.getWriter().write("Password Strength too weak..Enter a password with more than 4 characters.");
				}
			}
		} else {
			response.getWriter().write("Enter your Old password ");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
	}

	public void endRoute(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		int result = 0;
		result = ServiceRequestDAO.endRoute(adminBO.getAssociateCode(), request.getParameter("routeNo"));
		String response1 = "";
		if (result > 0) {
			response1 = "ResVer=1.1;STATUS=SUCCESS;MESSAGE=Trip Ended Successfully";
		} else {
			response1 = "Failed To end a Trip-Trip Not Avalilable";
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(response1);
	}

	public void setMOnSite(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		int status = 0;
		StringBuffer buffer = new StringBuffer();
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO.setTripid(request.getParameter("tripid"));
		String[] acceptedStatus = new String[2];
		openRequestBO.setTripid(request.getParameter("tripid"));
		String latidue = request.getParameter("oSLa");
		String longitude = request.getParameter("oSLo");
		JSONArray mainArray = new JSONArray();
		float lat1 = Float.parseFloat(request.getParameter("oSLa"));
		float lat2 = Float.parseFloat(request.getParameter("tSLa"));
		float lon1 = Float.parseFloat(request.getParameter("oSLo"));
		float lon2 = Float.parseFloat(request.getParameter("tSLo"));
		float distance = calculateDistance(lat1, lon1, lat2, lon2);
		if (adminBO.getOnsiteDist() > 0 && distance > adminBO.getOnsiteDist()) {
			AuditDAO.insertJobLogs("Driver Submitting Fake Onsite", request.getParameter("tripid"), adminBO.getAssociateCode(), TDSConstants.warning, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
		}
		if (latidue == null || latidue.equals("")) {
			latidue = "0.00000";
		}
		if (longitude == null || longitude.equals("")) {
			longitude = "0.00000";
		}
		openRequestBO.setDriverid(adminBO.getUid());
		String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
			status = RequestDAO.updateOpenRequestOnDriverOnSite(openRequestBO,adminBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			status = RequestDAO.updateOpenRequestOnDriverOnSite(openRequestBO, adminBO, myString);
		}
		try {
			if (status > 0) {
				AuditDAO.insertJobLogs("Driver Onsite", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.onSite, adminBO.getUid(), latidue, longitude, adminBO.getMasterAssociateCode());
				if (adminBO.getCall_onSite() == 1) {
					String phoneNumber = adminBO.getPhonePrefix() + ServiceRequestDAO.getPhoneNoByTripID(adminBO.getMasterAssociateCode(), adminBO.getUid(), openRequestBO.getTripid());
					if (phoneNumber.length() > 2) {
						IVRPassengerCallerOutBound ivr = new IVRPassengerCallerOutBound("9", "0", phoneNumber, adminBO.getMasterAssociateCode());
						ivr.start();
						AuditDAO.insertJobLogs("IVR Call Passenger For On Site", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.ivrPassDriverOnsiteCall, "System", "", "", adminBO.getMasterAssociateCode());
					}
				}
				if (adminBO.getMessage_onSite() == 1) {
					String phoneNumber = adminBO.getSmsPrefix() + ServiceRequestDAO.getPhoneNoByTripID(adminBO.getMasterAssociateCode(), adminBO.getUid(), openRequestBO.getTripid());
					try {
						String message=adminBO.getMessageSite();
						if(message!=null && !message.equals("")){
							message=message.replace("[Driver]", adminBO.getUid()).replace("[CabNo]", adminBO.getVehicleNo()).replace("[PhNum]", adminBO.getPhnumber()).replace("[Name]", adminBO.getUserNameDisplay());
						} else {
							message="Your driver is waiting outside";
						}
						AuditDAO.insertJobLogs("SMS Passenger For On Site", openRequestBO.getTripid(), adminBO.getAssociateCode(), TDSConstants.ivrPassDriverAcceptedCall, "System", "", "", adminBO.getMasterAssociateCode());
						sendSMS.main(message, phoneNumber, adminBO.getMasterAssociateCode());
					} catch (TwilioRestException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(jobsCode!=null && !jobsCode.equals("") && (jobsCode.equals(adminBO.getMasterAssociateCode()) || jobsCode.equals(adminBO.getChangeCompanyCode()))){
					openRequestBO = RequestDAO.getOpenRequestStatus(openRequestBO.getTripid(), adminBO,adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					openRequestBO = RequestDAO.getOpenRequestStatus(openRequestBO.getTripid(), adminBO, myString);
				}
				acceptedStatus = MessageGenerateJSON.generateMessageOnSite(openRequestBO, "AA", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
				mainArray = new JSONArray(acceptedStatus[0]);
				if(openRequestBO.getTripSource()==0){
					pushFuctionCalling(openRequestBO,"Driver On Site",adminBO.getAssociateCode());
				}
				if(openRequestBO.getTripSource()==4){
					CertificateBO certBo = CustomerMobileDAO.getIos_CertificateDetails(adminBO.getAssociateCode());
					DriverCabQueueBean customerBo = CustomerMobileDAO.getKeyForGooglePushById(openRequestBO,adminBO.getAssociateCode());
					//pushMSG("have to send message", certBo.getPath(), certBo.getPassword() , customerBo.getGkey());
					pushMSG1("Driver Reporting 'On site'", certBo.getPath(), certBo.getPassword() , customerBo.getGkey(),3);
				}
				
				String updateGPS = SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "storeHistory");
				int isUpdateGPS = (updateGPS!=null && !updateGPS.equals(""))?Integer.parseInt(updateGPS):0;
				if(isUpdateGPS==1){
					ServiceRequestDAO.insertDriverGPS(adminBO.getMasterAssociateCode(), adminBO.getUid(), latidue, longitude, adminBO.getVehicleNo(), 4, openRequestBO.getTripid());
				}
			} else {
				JSONObject jasonObject = new JSONObject();
				jasonObject.put("rV", 1.2);
				jasonObject.put("Act", "OnSite");
				jasonObject.put("rC", 200);
				jasonObject.put("STATUS", "FAILURE");
				jasonObject.put("MESSAGE", "Failure to OnSite the Trip");
				mainArray.put(jasonObject);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(mainArray.toString());
	}

	public void getLogFile(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		int size = request.getContentLength();
		//System.out.println("Request size =" + size);
		byte[] bytes = new byte[size];
		ByteArrayOutputStream tempByteArray = new ByteArrayOutputStream();
		// InputStream servletInput = request.getInputStream();
		byte[] bytestemp = new byte[8172];
		int totalBytes = 0;
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		BufferedReader reader1 = request.getReader();
		FileOutputStream os = new FileOutputStream(adminBO.getUid() + "logFile" + System.currentTimeMillis() + ".txt");
		// while (reader1.readLine(bytestemp)!=-1) {
		// tempByteArray.write(bytestemp);
		// totalBytes = totalBytes+readInt;
		// os.write(bytestemp);
		//
		// }
	}

	public void insertChargesFromMobile(HttpServletRequest request,HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException,IOException{
		StringBuffer buffer = new StringBuffer();
		String tripId = request.getParameter("tripID");
		int length = Integer.parseInt(request.getParameter("length"));
		String[] keys = (request.getParameter("key")).split(";");
		String[] desc = (request.getParameter("desc")).split(";");
		String[] amt = (request.getParameter("amt")).split(";");
		String tipCheck =request.getParameter("forTip");
		String email =request.getParameter("emailId")==null?"":request.getParameter("emailId");
		int result = ChargesDAO.insertDriverChargeSToMobile(adminBO.getAssociateCode(), amt, keys, tripId, length, adminBO.getUid(),tipCheck,adminBO.getMasterAssociateCode());
		if(!email.equals("") && tipCheck.equalsIgnoreCase("no")){ 
			RequestDAO.updateEmail(adminBO,tripId,email);
		}
		if(result==1){
			BigDecimal totalAmt= BigDecimal.ZERO;
			for(int j=0;j<length;j++){
				totalAmt= totalAmt.add(new BigDecimal(amt[j]));
				//System.out.println("TotalAmt for trip "+tripId+"--->"+totalAmt);
			}
			RequestDAO.updateTripAmt(tripId,adminBO.getAssociateCode(),totalAmt,tipCheck,adminBO.getMasterAssociateCode());
			buffer.append("success");
			OpenRequestBO orBo=ServiceRequestDAO.getOpenRequestHistoryByTripID(tripId, adminBO);
			if(orBo.getTripStatus().equals(TDSConstants.paymentReceived+"")){
				AuditDAO.insertJobLogs_Payment("Payment Received", tripId, adminBO.getAssociateCode(), TDSConstants.paymentReceived, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode(), totalAmt+"");
			}
			ArrayList<String> emails=new ArrayList<String>();
			emails=EmailSMSDAO.emailUsers(adminBO.getMasterAssociateCode());
			if(orBo.getTripStatus()!=null && orBo.getEmail()!=null && emails!=null && !orBo.getEmail().equals("") &&  emails.size()>0 && orBo.getTripStatus().equals("70") && tipCheck.equalsIgnoreCase("yes")){
				String url1="http://www.getacabdemo.com";
				if(request.getServerName().contains("getacabdemo.com")){
					url1="http://www.getacabdemo.com";
				} else {
					url1="https://www.gacdispatch.com";
				}
				MailReport mailPass = new MailReport(url1, tripId, orBo.getEmail(), adminBO.getTimeZone(), emails, "passengerPayment" , adminBO.getMasterAssociateCode(),1,"Your trip has been completed");
				mailPass.start();
			}
		}else{
			System.out.println("fail updating voucher");
			buffer.append("failure");
		}
		response.getWriter().write(buffer.toString());
	}	


	public void insertChargeTypes(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		int tripId = Integer.parseInt(request.getParameter("tripID"));
		int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
		String[] amount = new String[numberOfRows + 1];
		String[] key = new String[numberOfRows + 1];
		for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
			amount[rowIterator] = (request.getParameter("cA" + rowIterator).equals("") ? "0" : request.getParameter("cA" + rowIterator));
			key[rowIterator] = request.getParameter("cK" + rowIterator);
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		ChargesDAO.insertDriverCharge(adminBO.getAssociateCode(), amount, key, tripId, numberOfRows, adminBO.getUid(),adminBO.getMasterAssociateCode());
	}

	public void getChargeTypes(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		ArrayList<ChargesBO> chargeList = ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),0);
		JSONArray array = new JSONArray();
		JSONObject obj = new JSONObject();
		if (chargeList.size() > 0) {
			for (int i = 0; i < chargeList.size(); i++) {
				obj = new JSONObject();
				try {
					obj.put("K", chargeList.get(i).getPayTypeKey());
					obj.put("D", chargeList.get(i).getPayTypeDesc());
					obj.put("A", chargeList.get(i).getPayTypeAmount());
					array.put(obj);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");

		}
		// response.getWriter().write(buffer.toString());
		response.getWriter().write(array.toString());
	}

	public void insertCharges(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		int tripId = Integer.parseInt(request.getParameter("tripID"));
		int length = Integer.parseInt(request.getParameter("length"));
		String[] keys = (request.getParameter("key")).split(";");
		String[] desc = (request.getParameter("desc")).split(";");
		String[] amt = (request.getParameter("amt")).split(";");
		int result = ChargesDAO.insertDriverCharge(adminBO.getAssociateCode(), amt, keys, tripId, length, adminBO.getUid(),adminBO.getMasterAssociateCode());
		if (result == 1) {
			buffer.append("success");
		} else {
			buffer.append("failure");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(buffer.toString());
	}

	public void getChargesForTrip(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		JSONArray mainArr = new JSONArray();
		JSONArray array = new JSONArray();
		JSONArray array2 = new JSONArray();
		try {
			String tripId = request.getParameter("tripID");
			ArrayList<ChargesBO> al_list = ChargesDAO.getChargesForTrips(tripId, adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
			if (al_list.size() > 0) {
				JSONObject header = new JSONObject();
				header.put("rV", 1.2);
				header.put("Act", "CFT");
				header.put("rC", 200);
				header.put("CL", al_list.size());
				array.put(header);
				// buffer.append("ResVer=1.1;NoT=" + al_list.size() + ";");
				for (int i = 0; i < al_list.size(); i++) {
					JSONObject jasonObject = new JSONObject();
					jasonObject.put("PK", al_list.get(i).getKey());
					jasonObject.put("PM", al_list.get(i).getAmount());
					// buffer.append("PK=" + al_list.get(i).getKey()+
					// ";PM="+al_list.get(i).getAmount()+"^");
					array2.put(jasonObject);
				}
			}
			mainArr.put(0, array);
			mainArr.put(1, array2);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(mainArr.toString());
	}

	public void updatePaymentForTrip(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String version="";
		if (request.getParameter("version") != null) {
			version = request.getParameter("version");
		}
		JSONArray mainArr = new JSONArray();
		JSONArray subject = new JSONArray();
		JSONArray charges = new JSONArray();
		JSONArray arrayResponse = new JSONArray();
		JSONObject objResponse = new JSONObject();
		try {
			String tripId = request.getParameter("tripid");
			JSONObject header = new JSONObject();
			header.put("version", version);
			header.put("meterAmt", request.getParameter("Amt"));
			header.put("isHidden", request.getParameter("hidden"));
			header.put("tripID", request.getParameter("tripid"));
			header.put("TimeAccuredInSecs",(request.getParameter("totalTimetaken")==null?"":request.getParameter("totalTimetaken"))+"");
			header.put("DistanceAccuredInMlies",(request.getParameter("totalDistance")==null?"":request.getParameter("totalDistance"))+"");
			header.put("TotalTimeCalculated",(request.getParameter("Calc_Time")==null?"":request.getParameter("Calc_Time"))+"");
			header.put("TotalDistanceCalculated",(request.getParameter("Calc_Distance")==null?"":request.getParameter("Calc_Distance"))+""); 
			header.put("StartAmount",(request.getParameter("StartAmount")==null?"":request.getParameter("StartAmount"))+"");
			header.put("MinimumSpeed",(request.getParameter("MinimumSpeed")==null?"":request.getParameter("MinimumSpeed"))+"");
			ArrayList<ChargesBO> al_list = ChargesDAO.getChargesForTrips(tripId, adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
			header.put("CL", al_list.size());
			subject.put(header);
			if (al_list.size() > 0) {
				for (int i = 0; i < al_list.size(); i++) {
					JSONObject jasonObject = new JSONObject();
					jasonObject.put("PK", al_list.get(i).getKey());
					jasonObject.put("PM", al_list.get(i).getAmount());
					charges.put(jasonObject);
				}
			}
			mainArr.put(0, subject);
			mainArr.put(1, charges);
			int result = AuditDAO.insertJobLogs_Payment("Meter Data Updated", tripId, adminBO.getAssociateCode(), TDSConstants.jobMeterPayment, adminBO.getUid(), request.getParameter("eLa"), request.getParameter("eLo"), adminBO.getMasterAssociateCode(), mainArr.toString());
			if(request.getParameter("totalDistance")!=null){
				System.out.println("going to update ino");
				cat.info("Code For Test MeterData~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				cat.info("GPS Meter Update for TripID:"+tripId+" & with the DriverID:"+adminBO.getUid()+"");
				cat.info("Meter Amount Updated:"+(request.getParameter("Amt")==null?"":request.getParameter("Amt"))+"");
				cat.info("Is HideMeter:"+(request.getParameter("hidden")==null?"":request.getParameter("hidden"))+"");
				cat.info("MeterData");
				cat.info("Total Time Accured In Seconds:"+(request.getParameter("totalTimetaken")==null?"":request.getParameter("totalTimetaken"))+"");
				cat.info("Total Distane Accured in Miles:"+(request.getParameter("totalDistance")==null?"":request.getParameter("totalDistance"))+"");
				cat.info("CalculatedData");
				cat.info("Total Time Calculated:"+(request.getParameter("Calc_Time")==null?"":request.getParameter("Calc_Time"))+"");
				cat.info("Total Distance Calculated:"+(request.getParameter("Calc_Distance")==null?"":request.getParameter("Calc_Distance"))+""); 
				cat.info("Default Meter Values");
				cat.info("StartAmount:"+(request.getParameter("StartAmount")==null?"":request.getParameter("StartAmount"))+"");
				cat.info("MinimumSpeed:"+(request.getParameter("MinimumSpeed")==null?"":request.getParameter("MinimumSpeed"))+"");
				cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

				cat.error("Code For Test MeterData~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				cat.error("GPS Meter Update for TripID:"+tripId+" & with the DriverID:"+adminBO.getUid()+"");
				cat.error("Meter Amount Updated:"+(request.getParameter("Amt")==null?"":request.getParameter("Amt"))+"");
				cat.error("Is HideMeter:"+(request.getParameter("hidden")==null?"":request.getParameter("hidden"))+"");
				cat.error("MeterData");
				cat.error("Total Time Accured In Seconds:"+(request.getParameter("totalTimetaken")==null?"":request.getParameter("totalTimetaken"))+"");
				cat.error("Total Distane Accured in Miles:"+(request.getParameter("totalDistance")==null?"":request.getParameter("totalDistance"))+"");
				cat.error("CalculatedData");
				cat.error("Total Time Calculated:"+(request.getParameter("Calc_Time")==null?"":request.getParameter("Calc_Time"))+"");
				cat.error("Total Distance Calculated:"+(request.getParameter("Calc_Distance")==null?"":request.getParameter("Calc_Distance"))+""); 
				cat.error("Default Meter Values");
				cat.error("StartAmount:"+(request.getParameter("StartAmount")==null?"":request.getParameter("StartAmount"))+"");
				cat.error("MinimumSpeed:"+(request.getParameter("MinimumSpeed")==null?"":request.getParameter("MinimumSpeed"))+"");
				cat.error("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

				/*System.out.println("Code For Test MeterData");
				System.out.println("GPS Meter Update for TripID:"+tripId+" & with the DriverID:"+adminBO.getUid());
				System.out.println("Meter Amount Updated:"+(request.getParameter("Amt")==null?"":request.getParameter("Amt")));
				System.out.println("Is HideMeter:"+(request.getParameter("hidden")==null?"":request.getParameter("hidden")));
				System.out.println("MeterData");
				System.out.println("Total Time Accured In Seconds:"+(request.getParameter("totalTimetaken")==null?"":request.getParameter("totalTimetaken")));
				System.out.println("Total Distane Accured in Miles:"+(request.getParameter("totalDistance")==null?"":request.getParameter("totalDistance")));
				System.out.println("CalculatedData");
				System.out.println("Total Time Calculated:"+(request.getParameter("Calc_Time")==null?"":request.getParameter("Calc_Time")));
				System.out.println("Total Distance Calculated:"+(request.getParameter("Calc_Distance")==null?"":request.getParameter("Calc_Distance"))); 
				System.out.println("Default Meter Values");
				System.out.println("StartAmount:"+(request.getParameter("StartAmount")==null?"":request.getParameter("StartAmount")));
				System.out.println("MinimumSpeed:"+(request.getParameter("MinimumSpeed")==null?"":request.getParameter("MinimumSpeed")));*/
			}else{
				//System.out.println("no values found or not the correct version");
			}
			if(result>0){
				objResponse.put("rV", 1.2);
				objResponse.put("Act", "UPT");
				objResponse.put("rC", 200);
				objResponse.put("message", "Succcess");
			}else{
				objResponse.put("rV", 1.2);
				objResponse.put("Act", "UPT");
				objResponse.put("rC", 300);
				objResponse.put("message", "Failure");
			}
			arrayResponse.put(objResponse);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		//System.out.println("response:"+arrayResponse.toString());
		response.getWriter().write(arrayResponse.toString());
	}


	public void getMinimumSpeedAndRatePerMile(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write("ResVer=1.1;minimumSpeed=" + cspBO.getMinimumSpeed() + ";ratePerMile=" + cspBO.getRatePerMile() + ";ratePerMinute=" + cspBO.getRatePerMinute());
	}

	public void updateOdoMeterValue(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String value = request.getParameter("value");
		String tripId = request.getParameter("tripId");
		String cabNo = request.getParameter("cabNo");
		String event = request.getParameter("eventValue");
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		MobileDAO.insertOdoMeterValues(adminBO.getAssociateCode(), value, tripId, adminBO.getUid(), cabNo, event);
	}

	public void getLastUpdateOdoMeterValues(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		String cabNo = request.getParameter("cabNo");
		int odoMeterValue = MobileDAO.getLastUpdatedOdoMeterValue(adminBO.getAssociateCode(), cabNo);
		if (odoMeterValue != 0) {
			buffer.append("ResVer=1.1;odoMeterValue=" + odoMeterValue);
		} else {
			buffer.append("ResVer=1.1;odoMeterValue=");
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(buffer.toString());
	}

	//	public void checkSession(HttpServletRequest request, HttpServletResponse response,String sessionId) throws ServletException, IOException {
	//		System.out.println("Check Session For--->"+sessionId);
	//		Cookie[] delCookies = request.getCookies();
	//		JSONArray array = new JSONArray();
	//		JSONObject obj = new JSONObject();
	//		try {
	//			if (sessionId != null && !sessionId.equals("")) {
	//				int sessionValid = MobileDAO.checkSession(sessionId);
	//				if (request.getParameter("version") != null) {
	//					String version = request.getParameter("version");
	//				}
	//				if (sessionValid == 1) {
	//					String sessionIdNew = PasswordGen.generatePassword("A", 32);
	//					AdminRegistrationBO adminBO = MobileDAO.changeSession(sessionId, sessionIdNew);
	//					sessionId = sessionIdNew;
	//
	//					CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());
	//					adminBO.setCalculateZone(cspBO.getCalculateZones());
	//					adminBO.setPhonePrefix(cspBO.getMobilePrefix());
	//					adminBO.setSmsPrefix(cspBO.getSmsPrefix());
	//
	//					String cabFlags = MobileDAO.getCabProperties(adminBO.getAssociateCode(), adminBO.getVehicleNo());
	//					adminBO.setDriver_flag(adminBO.getDriver_flag() + cabFlags);
	//
	//					int driverRating = MobileDAO.getDriverRating(adminBO);
	//					adminBO.setDriverRating(driverRating);
	//
	//					adminBO.setCompanyList(SecurityDAO.getAllAssoccode(adminBO.getMasterAssociateCode()));
	//					for(int i=0;i<adminBO.getCompanyList().size();i++){
	//						if(!adminBO.getCompanyList().get(i).contains(adminBO.getMasterAssociateCode())){
	//							adminBO.setChangeCompanyCode(adminBO.getCompanyList().get(i));
	//							break;
	//						}
	//					}
	//					adminBO.setTimeZone(SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(), "timeZoneArea"));
	//
	//					DispatchPropertiesBO dispatchCallBO = SystemPropertiesDAO.getDispatchCallPropeties(adminBO.getMasterAssociateCode());
	//					adminBO.setCall_onAccept(dispatchCallBO.getCall_onAccept());
	//					adminBO.setCall_onRoute(dispatchCallBO.getCall_onRoute());
	//					adminBO.setCall_onSite(dispatchCallBO.getCall_onSite());
	//					adminBO.setMessage_onAccept(dispatchCallBO.getMessage_onAccept());
	//					adminBO.setMessage_onRoute(dispatchCallBO.getMessage_onRoute());
	//					adminBO.setMessage_onSite(dispatchCallBO.getMessage_onSite());
	//					adminBO.setMessageAccept(dispatchCallBO.getMessageAccept());
	//					adminBO.setMessageRoute(dispatchCallBO.getMessageRoute());
	//					adminBO.setMessageSite(dispatchCallBO.getMessageSite());
	//
	//					if (getServletConfig().getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "ZonesLoaded") == null) {
	//						SystemUtils.reloadZones(this, adminBO.getMasterAssociateCode());
	//					}
	//
	//					if(adminBO.getPaymentUserID()==null || adminBO.getPaymentUserID().equals("")){
	//						VantivBO vantivBean = CreditCardDAO.getVantivUserIdPassword(adminBO);
	//						adminBO.setPaymentUserID(vantivBean.getUserId());
	//						adminBO.setPayPassword(vantivBean.getPassword());
	//					}
	//
	//					String driverAlarm = SystemPropertiesDAO.getParameter(adminBO.getAssociateCode(), "driverAlarm");
	//					if (driverAlarm == null || driverAlarm.equals("")) {
	//						driverAlarm = "0";
	//						adminBO.setDriverAlarm(0);
	//					} else {
	//						adminBO.setDriverAlarm(Integer.parseInt(driverAlarm));
	//					}
	//
	//					String meterValues = MobileDAO.getCabDefaultMeterRate(adminBO.getAssociateCode(), adminBO.getVehicleNo(), adminBO.getMasterAssociateCode());
	//					try{
	//						JSONArray meterArray = new JSONArray(meterValues);
	//						if(meterArray!=null && meterArray.length()>0){
	//							JSONObject meterObject = meterArray.getJSONObject(0);
	//							cspBO.setMinimumSpeed(meterObject.getString("minimunSpeed"));
	//							cspBO.setRatePerMile(Double.parseDouble(meterObject.getString("ratePerMile")));
	//							cspBO.setRatePerMinute(Double.parseDouble(meterObject.getString("ratePerMinute")));
	//							cspBO.setStartAmount(meterObject.getString("startAmount"));
	//							adminBO.setDefaultMeterRate("YES");
	//						} else {
	//							adminBO.setDefaultMeterRate("NO");
	//						}
	//					} catch (JSONException e){
	//						e.printStackTrace();
	//					}
	//					adminBO.setJob_No_Show_Property(cspBO.getNo_show());
	//					adminBO.setSessionId(sessionId);
	//
	//					ObjectMapper mapper = new ObjectMapper();
	//					String finalJSON = mapper.writeValueAsString(adminBO);
	//					MobileDAO.insertSessionObjects(sessionId,finalJSON.replaceAll("null", "''"),adminBO.getUid());
	//
	//					JSONObject jasonObject = new JSONObject();
	//					try {
	//						jasonObject.put("rV", 1.2);
	//						jasonObject.put("RC", "True");
	//						jasonObject.put("Act", "DLU");
	//						jasonObject.put("rC", 200);
	//						jasonObject.put("SessionId", sessionId);
	//					} catch (JSONException e) {
	//						e.printStackTrace();
	//					}
	//					array.put(jasonObject);
	//					response.getWriter().write(array.toString());
	//
	//				} else if (sessionValid == 0) {
	//					JSONObject jasonObject = new JSONObject();
	//					try {
	//						jasonObject.put("rV", 1.2);
	//						jasonObject.put("RC", "false");
	//						jasonObject.put("Act", "DLU");
	//						jasonObject.put("rC", 400);
	//						jasonObject.put("MSG", "Your Session Expired");
	//					} catch (JSONException e) {
	//						e.printStackTrace();
	//					}
	//					array.put(jasonObject);
	//					response.getWriter().write(array.toString());
	//				} else {
	//					JSONObject jasonObject = new JSONObject();
	//					try {
	//						jasonObject.put("rV", 1.2);
	//						jasonObject.put("RC", "false");
	//						jasonObject.put("Act", "DLU");
	//						jasonObject.put("rC", 500);
	//						jasonObject.put("MSG", "Your Session Expired");
	//					} catch (JSONException e) {
	//						e.printStackTrace();
	//					}
	//					array.put(jasonObject);
	//					response.getWriter().write(array.toString());
	//				}
	//			} else {
	//				JSONObject jasonObject = new JSONObject();
	//				try {
	//					jasonObject.put("rV", 1.2);
	//					jasonObject.put("RC", "false");
	//					jasonObject.put("Act", "DLU");
	//					jasonObject.put("rC", 400);
	//					jasonObject.put("MSG", "Your Session Expired");
	//				} catch (JSONException e) {
	//					e.printStackTrace();
	//				}
	//				array.put(jasonObject);
	//				response.getWriter().write(array.toString());
	//			}
	//		} catch (Exception e) {
	//			System.out.println("Error from mobileAction.checksession----." + e);
	//		}
	//	}

	public void sendMessage(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		String cabNo = request.getParameter("cabNo");
		String driverId = request.getParameter("driverId");
		String msg = request.getParameter("msg");
		int emc = Integer.parseInt(request.getParameter("emc"));
		int status = DispatchDAO.sendMessageToOperator(msg, driverId+"-"+cabNo, adminBO.getMasterAssociateCode(), 0, emc);
		if (msg != null && msg.contains("tripId:")) {
			AuditDAO.insertJobLogs("Driver Sent Message " + msg, msg.split(":")[1], adminBO.getAssociateCode(), 101, adminBO.getUid(), "0.00000", "0.00000", adminBO.getMasterAssociateCode());
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write(status + "");
	}

	public void sendNotAvailabeMessage(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		String cabNo = request.getParameter("cabNo");
		String driverId = request.getParameter("driverId");
		String msg = request.getParameter("msg");
		int emc = Integer.parseInt(request.getParameter("emc"));
		int status = DispatchDAO.sendMessageToOperator(msg, driverId+"-"+cabNo, adminBO.getMasterAssociateCode(), 0, emc);
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		
		int shiftKey = RequestDAO.getShiftKey(adminBO);
		OperatorShift osbean = new OperatorShift();
		osbean.setKey(shiftKey);
		osbean.setOperatorId(adminBO.getUid());
		osbean.setOpenedBy(adminBO.getUid());
		osbean.setStatus("A");
		osbean.setAssociateCode(adminBO.getMasterAssociateCode());
		RequestDAO.updateShiftRegisterDetail(osbean);

		response.getWriter().write(status + "");
	}


	public void getCannedMessages(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		StringBuffer buffer = new StringBuffer();
		ArrayList<String> ms_list = MobileDAO.getCannedMessages(adminBO.getMasterAssociateCode());
		JSONArray mainArray = new JSONArray();
		try{
			if (ms_list.size() > 0) {
				JSONObject header = new JSONObject();
				header.put("rV", 1.2);
				header.put("Act", "CM");
				header.put("rC", 200);
				header.put("NoT", ms_list.size());
				mainArray.put(0,header);
				//buffer.append("ResVer=1.1;NoT=" + ms_list.size() + ";");
				for (int i = 0; i < ms_list.size(); i++) {
					JSONObject jasonObject = new JSONObject();
					jasonObject.put("M", ms_list.get(i));
					mainArray.put(i+1,jasonObject);
					//buffer.append("M=" + ms_list.get(i) + ";");
				}
			}else {
				JSONObject header = new JSONObject();
				header.put("rV", 1.2);
				header.put("Act", "CM");
				header.put("rC", 400);
				mainArray.put(0,header);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");

		}
		response.getWriter().write(mainArray.toString());
	}

	public void getLatestVersion(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		if (request.getParameter("version") != null) {
			String version = request.getParameter("version");
		}
		response.getWriter().write("77");
	}

	public void chckUserCardInfo(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		PaymentProcessBean processBean = null;
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		PaymentProcessingAction pay = new PaymentProcessingAction();

		processBean = magneticStripAndAmtParsing(request, response,adminBO);
		processBean.setB_driverid(adminBO.getUid());

		processBean.setAdd1(request.getParameter("add1") == null ? "" : request.getParameter("add1"));
		processBean.setAdd2(request.getParameter("add2") == null ? "" : request.getParameter("add2"));
		processBean.setCity(request.getParameter("city") == null ? "" : request.getParameter("city"));
		processBean.setState(request.getParameter("state") == null ? "" : request.getParameter("state"));
		processBean.setZip(request.getParameter("zip") == null ? "" : request.getParameter("zip"));
		processBean.setCvv(request.getParameter("cvv") == null ? "" : request.getParameter("cvv"));
		processBean.setB_cardHolderName(request.getParameter("name") == null ? "" : request.getParameter("name"));

		processBean.setMerchid(adminBO.getMerchid());
		processBean.setServiceid(adminBO.getServiceid());
		processBean.setB_trip_id(request.getParameter("tripid") == null ? "" : request.getParameter("tripid"));

		processBean = pay.initiatePayProcess(poolBO, processBean, "Mobile", adminBO.getMasterAssociateCode());
		JSONArray mainArray = new JSONArray();
		JSONArray array = new JSONArray();
		JSONArray array2 =new JSONArray();
		JSONObject details = new JSONObject();
		try {
			details.put("rV", 1.2);
			if (processBean.getB_approval_status().equalsIgnoreCase("1")) {
				ServiceRequestDAO.updateOpenRequestHisStatusAlone(processBean.getB_trip_id(),TDSConstants.paymentReceived+"");
				PaymentProcessingAction.makePayment(processBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBO);
				details.put("STATUS", "APPROVED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
				details.put("AMOUNT", 0.0);
				array.put(details);
			} else if (processBean.getB_approval_status().equalsIgnoreCase("2")) {
				ChargesDAO.insertPaymentIntoCC(processBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBO, ICCConstant.PREAUTH,ICCConstant.GAC);
				details.put("STATUS", "DECLINED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
				details.put("AMOUNT", 0.0);
				array.put(details);
			} else {
				details.put("STATUS", "FAILURE");
				details.put("MESSAGE", "Fail to Process Your Request^Reason:^Check Your Data^Check Your Network;XXXXX");
				details.put("AMOUNT", 0.0);
				array.put(details);
			}
			mainArray.put(0, array);
			mainArray.put(1, array2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.getWriter().write(mainArray.toString());
	}

	public void alterPayment(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		PaymentProcessBean processBean = new PaymentProcessBean();
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		String tripID=request.getParameter("tripId");
		byte[] bytes = request.getAttribute("rider_sign") == null ? new byte[0] : (byte[]) request.getAttribute("rider_sign");
		processBean.setB_amount(request.getParameter("amt") == null ? "0" : request.getParameter("amt"));
		processBean.setB_trans_id(request.getParameter("tranid") == null ? "" : request.getParameter("tranid"));
		processBean.setB_tip_amt(request.getParameter("tip") == null ? "0" : request.getParameter("tip"));
		processBean.setMerchid(adminBO.getMerchid());
		processBean.setServiceid(adminBO.getServiceid());
		String transId = processBean.getB_trans_id();
		processBean = PaymentProcessingAction.capturePayment(poolBO, processBean, adminBO.getMasterAssociateCode());
		JSONArray array = new JSONArray();
		JSONObject details = new JSONObject();
		try {
			details.put("rV", 1.2);
			if (processBean.getB_approval_status().equalsIgnoreCase("1")) {
				ChargesDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), transId, processBean.getB_trans_id2(), processBean.getB_cap_trans(), "", bytes, adminBO.getUid());
				ServiceRequestDAO.updateOpenRequestHisStatusAlone(tripID, TDSConstants.paymentReceived + "");
				details.put("STATUS", "APPROVED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
				array.put(details);
				// resultStrBuffer.append("ResVer=1.1;STATUS=APPROVED;TXNID="+(processBean.getB_cap_trans().equals("")?"0":processBean.getB_cap_trans())+";APPROVALCODE="+(processBean.getB_approval().equals("")?"0":processBean.getB_approval()));
			} else if (processBean.getB_approval_status().equalsIgnoreCase("2")) {
				processBean = PaymentProcessingAction.initiatePayProcess(poolBO, processBean, "Web",adminBO.getMasterAssociateCode());
				if (processBean.getB_approval_status().equalsIgnoreCase("1")) {
					ChargesDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), transId, processBean.getB_trans_id2(), processBean.getB_cap_trans(), "", bytes, adminBO.getUid());
					ServiceRequestDAO.updateOpenRequestHisStatusAlone(tripID, TDSConstants.paymentReceived + "");
					details.put("STATUS", "APPROVED");
					details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
					details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
					processBean.setB_trans_id(request.getParameter("tranid") == null ? "" : request.getParameter("tranid"));
					processBean.setB_tip_amt("0.00");
					processBean = PaymentProcessingAction.voidPayment(poolBO, processBean,adminBO.getMasterAssociateCode());
					array.put(details);
				} else {
					details.put("STATUS", "DECLINED");
					details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
					details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
					array.put(details);
				}
				// resultStrBuffer.append("ResVer=1.1;STATUS=DECLINED;TXNID="+(processBean.getB_cap_trans().equals("")?"0":processBean.getB_cap_trans())+";APPROVALCODE="+(processBean.getB_approval().equals("")?"0":processBean.getB_approval()));
			} else {
				details.put("STATUS", "FAILURE");
				details.put("MESSAGE", "XXXXX");
				array.put(details);
				// resultStrBuffer.append("ResVer=1.1;STATUS=FAILURE;MESSAGE=XXXXX;XXXXX");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	public void voidPayment(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		PaymentProcessBean processBean = new PaymentProcessBean();
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		processBean.setB_trans_id(request.getParameter("transid") == null ? "" : request.getParameter("transid"));
		processBean.setB_trans_id2(request.getParameter("transid2") == null ? "" : request.getParameter("transid2"));
		processBean = ChargesDAO.chckReturnOrUndo(processBean);
		processBean = PaymentProcessingAction.voidPayment(poolBO, processBean, adminBO.getAssociateCode());
		JSONArray array = new JSONArray();
		JSONObject details = new JSONObject();
		try {
			details.put("rV", 1.2);
			if (processBean.getB_approval_status().equalsIgnoreCase("1")) {
				ChargesDAO.voidTransAction(processBean.getB_trans_id(), processBean.getB_trans_id2(), "", adminBO.getUid());
				details.put("STATUS", "APPROVED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
				array.put(details);
				// resultStrBuffer.append("ResVer=1.1;STATUS=APPROVED;TXNID="
				// + processBean.getB_trans_id() + ";APPROVALCODE=" +
				// processBean.getB_approval_code());
			} else if (processBean.getB_approval_status().equalsIgnoreCase("2")) {
				details.put("STATUS", "DECLINED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
				array.put(details);
				// resultStrBuffer.append("ResVer=1.1;STATUS=DECLINED;TXNID="
				// + processBean.getB_trans_id() + ";APPROVALCODE=" +
				// processBean.getB_approval_code());
			} else {
				details.put("STATUS", "FAILURE");
				details.put("MESSAGE", "XXXXX");
				array.put(details);
				// resultStrBuffer.append("ResVer=1.1;FAILURE;XXXXX;XXXXX");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}


	public void authorizeAndVoid(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		PaymentProcessBean processBean = null;
		PaymentProcessBean authorizedBean = null;
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		if(adminBO==null){
			System.out.println("yes");
		}else{
			System.out.println("No"+adminBO.toString());
			System.out.println("uid"+adminBO.getUid());
		}
		String masterKey="";
		int transId=0;

		JSONArray mainArray = new JSONArray();
		JSONArray array = new JSONArray();
		JSONObject details = new JSONObject();


		processBean = new PaymentProcessBean();
		processBean.setB_driverid(adminBO.getUid());
		String tripId=request.getParameter("tripID");
		String amount = request.getParameter("amount");
		String tip = request.getParameter("tip");

		authorizedBean = ChargesDAO.getCCDetails(adminBO.getAssociateCode(), tripId, 0);

		CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(adminBO.getMasterAssociateCode());
		masterKey = RequestDAO.getCCInfobyTripID(tripId);
		CreditCardBO ccBean= CreditCardDAO.getCardDetails(masterKey, adminBO.getAssociateCode());
		//masterkey here is customerid in creditcard table and sno in clientuser table

		processBean.setB_trip_id(tripId);
		processBean.setCvv("");
		processBean.setB_card_full_no("");
		processBean.setB_amount(amount);
		processBean.setB_cardExpiryDate(ccBean.getExpiryDate());
		processBean.setZip("");
		processBean.setB_cardHolderName(ccBean.getNameOnCard());
		processBean.setB_trans_id(ccBean.getCcInfoKey());
		processBean.setB_tip_amt("0");
		if(cmpBean.getCcProvider()==2){
			processBean= PaymentGatewaySlimCD.makeProcess(processBean, cmpBean, PaymentCatgories.Auth);
		} else {
			processBean=PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Auth);
		}
		try {
			if(processBean.isAuth_status()){
				//authorizedBean = PaymentProcessingAction.voidPayment(poolBO, authorizedBean, adminBO.getAssociateCode());
				if(cmpBean.getCcProvider()==2){
					authorizedBean = PaymentGatewaySlimCD.makeProcess(authorizedBean,cmpBean, PaymentCatgories.Void);
				} else {
					authorizedBean = PaymentGateway.makeProcess(authorizedBean,cmpBean, PaymentCatgories.Void);
				}
				ChargesDAO.voidTransAction(authorizedBean.getB_trans_id(), authorizedBean.getB_trans_id2(), "", adminBO.getUid());

				transId=CreditCardDAO.insertPaymentIntoCCFromDriver(processBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBO, ICCConstant.PREAUTH);

				ServiceRequestDAO.updateOpenRequestHisStatusAlone(tripId, TDSConstants.paymentReceived + "");
				details.put("STATUS", "APPROVED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval_status()));
				details.put("AMOUNT", amount);
				array.put(details);
			}else{
				details.put("STATUS", "DECLINED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval_status()));
				details.put("AMOUNT", amount);
				array.put(details);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("Result"+array.toString());
		response.getWriter().write(array.toString());
	}


	public void validateVoucher(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		VoucherBO voucherBO = new VoucherBO();
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		PaymentProcessBean processBean = new PaymentProcessBean();
		String tripID = request.getParameter("tripID");
		voucherBO = ProcessingDAO.getVaidityOfVoucher(request.getParameter("vno"), adminBO.getMasterAssociateCode());
		boolean ccStatus=true;
		JSONArray array = new JSONArray();
		JSONObject details = new JSONObject();
		if(ccStatus){
			try {
				details.put("rV", "1.2");
				if (voucherBO != null && voucherBO.isV_status() && tripID != null) {
					String transId="0";
					CreditCardBO ccFor_Voucher = null;
					//System.out.println("voucherBo not null:");
					if(!voucherBO.getCcNum().equals("") && voucherBO.getProcessMethod()==1){
						System.out.println("Has creditcard for voucher");
						ccFor_Voucher = CreditCardDAO.getCardDetailsByKey(voucherBO.getCcNum());
					}

					if(!voucherBO.getCcNum().equals("") && voucherBO.getProcessMethod()==1){
						CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(adminBO.getMasterAssociateCode());
						processBean.setB_trip_id(tripID);
						processBean.setCvv("");
						processBean.setB_card_full_no("");
						processBean.setB_amount(voucherBO.getFixedAmount()!=null && !voucherBO.getFixedAmount().equals("")?voucherBO.getFixedAmount():"0");
						processBean.setB_cardExpiryDate(ccFor_Voucher.getExpiryDate());
						processBean.setZip("");
						processBean.setB_cardHolderName(ccFor_Voucher.getNameOnCard());
						processBean.setB_tip_amt("0");
						processBean.setB_driverid(adminBO.getUid());
						processBean.setB_trans_id(ccFor_Voucher.getCcInfoKey());
						if(cmpBean.getCcProvider()==2){
							processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Auth);
						} else {
							processBean=PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Auth);
						}
						if(processBean.isAuth_status()){
							transId = processBean.getB_trans_id();
							ChargesDAO.insertPaymentIntoCC(processBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBO, ICCConstant.PREAUTH,ICCConstant.GAC);
						} else if(voucherBO.getDeclineStatus()==0){
							ccStatus=false;
						}
					}
					if(ccStatus){
						int txnId = ProcessingDAO.insertVoucherDetail(request.getParameter("vno"), adminBO.getAssociateCode(), tripID, adminBO.getUid(), "0.00", "0.00", null, "", "", adminBO.getMasterAssociateCode());
						details.put("STATUS", "APPROVED");
						details.put("TXNID", txnId);
						details.put("APPROVALCODE", txnId);
						details.put("MESSAGE", "abc");
						details.put("TD", voucherBO.getVdesc());
						details.put("VNO", request.getParameter("vno"));
						details.put("RN", voucherBO.getVname());
						details.put("AAMT", voucherBO.getVamount());
						details.put("TA", voucherBO.getNoTip());
						details.put("TxI", txnId);
						details.put("ccTxn", transId);
						details.put("DC", voucherBO.getDriverComments());
						array.put(details);
					} else {
						System.out.println("cc status false");
						try{
							details.put("STATUS", "DECLINED");
							details.put("TXNID", "");
							details.put("APPROVALCODE", "");
							details.put("MESSAGE", "");
							details.put("TD", "");
							array.put(details);
						}catch (Exception e) {
							e.printStackTrace();
						}
					}
				} else {
					if (voucherBO != null) {
						details.put("STATUS", voucherBO.getVreason());
						array.put(details);
					} else {
						details.put("STATUS", "DECLINED");
						details.put("TXNID", "");
						details.put("APPROVALCODE", "");
						details.put("MESSAGE", "");
						details.put("TD", "");
						array.put(details);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//System.out.println("VoucherValid: "+array.toString());
		response.getWriter().write(array.toString());
	}

	public void voucherUpdate(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		
		String tripID = request.getParameter("tripid");
		VoucherBO voucherBO = null;
		CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(adminBO.getMasterAssociateCode());
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		PaymentProcessBean processBean = new PaymentProcessBean();
		JSONArray array = new JSONArray();
		JSONObject obj = new JSONObject();
		
		try {
			int size = request.getContentLength();
			System.out.println("Signature Size:"+size);
			BufferedReader reader1 = new BufferedReader(request.getReader(), size);
			byte[] bytes = new byte[size];
			int a = 0, counter = 0;
			try{
				while ((a = reader1.read()) != -1) {
					bytes[counter] = (byte) a;
					counter++;
				}
			}catch(Exception e){
				e.getMessage();
				e.printStackTrace();
				
				obj.put("status", "Declined");
				obj.put("amtStatus", "Amount not Authorized");
				array.put(obj);
				response.getWriter().write(array.toString());
				return;
			}
			
			//Have to do later
			//request.setCharacterEncoding("UTF-8");
			//response.setCharacterEncoding("UTF-8");
			//System.out.println("Name: "+request.getParameter("passengername"));
			if (TDSValidation.isDoubleNumber(request.getParameter("amt")) && TDSValidation.isDoubleNumber(request.getParameter("tip")) && UtilityDAO.isExistOrNot("select * from TDS_GOVT_VOUCHER where VC_VOUCHERNO = '" + request.getParameter("vno") + "' and VC_AMOUNT >= " + (Double.parseDouble(request.getParameter("amt")) + Double.parseDouble(request.getParameter("tip"))) + " ")) {
				voucherBO = ProcessingDAO.updateVoucherDetail(request.getParameter("vno"), adminBO.getAssociateCode(), tripID, adminBO.getUid(), request.getParameter("amt"), request.getParameter("tip"), bytes, request.getRealPath("/images/nosign.gif"), request.getParameter("passengername"), Integer.parseInt(request.getParameter("txnId")));
				ProcessingDAO.updateVoucherSign(voucherBO.getVtrans(), bytes);
				if (tripID != null && !tripID.equals("")) {
					if(!request.getParameter("ccTxnId").equals("0")){
						processBean.setB_trip_id(tripID);
						processBean.setB_amount( request.getParameter("amt")==null?"0":request.getParameter("amt"));
						processBean.setB_trans_id(request.getParameter("ccTxnId")==null?"":request.getParameter("ccTxnId"));
						processBean.setB_tip_amt(request.getParameter("tip")==null?"0":request.getParameter("tip"));
						processBean.setMerchid(adminBO.getMerchid());
						processBean.setServiceid(adminBO.getServiceid());
						String transId=processBean.getB_trans_id();
						processBean = PaymentProcessingAction.capturePayment(poolBO,processBean,adminBO.getMasterAssociateCode());
						if(processBean.isAuth_status()){
							ChargesDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), transId,processBean.getB_trans_id2(), processBean.getB_cap_trans(), "", bytes,adminBO.getUid());
						}
					}
					ServiceRequestDAO.updateOpenRequestHisStatusAlone(tripID, TDSConstants.paymentReceived + "");
					AuditDAO.insertJobLogs("Voucher Pmt Received", tripID, adminBO.getAssociateCode(), TDSConstants.paymentReceived, adminBO.getUid(), "0.00000", "0.00000", adminBO.getMasterAssociateCode());
				}
				if (voucherBO.isV_status()) {
					// resultStrBuffer.append("Approved"+";"+request.getParameter("vno")+";"+voucherBO.getVtrans()+";"+Double.parseDouble(voucherBO.getVamount())+";"+Double.parseDouble(voucherBO.getTip()));
					obj.put("status", "Approved");
					obj.put("vno", request.getParameter("vno"));
					obj.put("transid", voucherBO.getVtrans());
					obj.put("amt", Double.parseDouble(voucherBO.getVamount()));
					obj.put("tripid", Double.parseDouble(voucherBO.getTip()));
					array.put(obj);
				} else {
					// resultStrBuffer.append("Declined;Amount not Authorized");
					obj.put("status", "Declined");
					obj.put("amtStatus", "Amount not Authorized");
					array.put(obj);
				}
			} else {
				// resultStrBuffer.append("Declined;Amount not Authorized");
				obj.put("status", "Declined");
				obj.put("amtStatus", "Amount not Authorized");
				array.put(obj);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("VoucherUpdate: "+array.toString());
		response.getWriter().write(array.toString());
	}

	public void getMeterTypes(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws IOException {
		JSONArray array = new JSONArray();
		ArrayList<MeterType> meters = SystemPropertiesDAO.getMeterValues(adminBO.getMasterAssociateCode());
		if (meters.size() > 0) {
			for (int i = 0; i < meters.size() ; i++) {
				JSONObject meterValues = new JSONObject();
				try {
					meterValues.put("MK", meters.get(i).getKey());
					meterValues.put("MN", meters.get(i).getMeterName() == null ? "" : meters.get(i).getMeterName());
					meterValues.put("SA", meters.get(i).getStartAmt() == null ? "" : meters.get(i).getStartAmt());
					meterValues.put("RMl", meters.get(i).getRatePerMile() == null ? "" : meters.get(i).getRatePerMile());
					meterValues.put("RMn", meters.get(i).getRatePerMin() == null ? "" : meters.get(i).getRatePerMin());
					meterValues.put("MS", meters.get(i).getMinSpeed());
					meterValues.put("DF", meters.get(i).getDefaultMeter());
					array.put(meterValues);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			response.getWriter().write(array.toString());
		} else {
			response.getWriter().write("");
		}
	}


	public void submitOnsiteError(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws IOException {
		String tripId = request.getParameter("tripId");
		float lat1 = Float.parseFloat(request.getParameter("lat1"));
		float lat2 = Float.parseFloat(request.getParameter("lat2"));
		float lon1 = Float.parseFloat(request.getParameter("lon1"));
		float lon2 = Float.parseFloat(request.getParameter("lon2"));
		float distance = calculateDistance(lat1, lon1, lat2, lon2);
		/*
		 * if(distance > adminBO.getOnsiteDist()){
		 * AuditDAO.insertJobLogs("Driver Submitting Fake Onsite", tripId,
		 * adminBO.getAssociateCode(),TDSConstants.onSite, adminBO.getUid(), "",
		 * "", adminBO.getMasterAssociateCode()); }
		 */
	}

	public float calculateDistance(float lat1, float lon1, float lat2, float lon2) {
		int earthRadius = 6371;
		float dLat = (float) Math.toRadians(lat2 - lat1);
		float dLon = (float) Math.toRadians(lon2 - lon1);
		float a = (float) (Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2));
		float c = (float) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
		float d = earthRadius * c;
		return d;
	}

	public void getDriverDocument(HttpServletRequest request,HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String driverId = adminBO.getUid();
		String result = null;
		result = MobileDAO.getDriverDocument(driverId);
		if(!result.equalsIgnoreCase("")){
			JSONArray array=new JSONArray();
			JSONObject obj=new JSONObject();
			String res[]=result.split(";");
			try {
				obj.put("name", res[0]);
				obj.put("phNumber", res[1]);
				obj.put("photo", res[2]);
				array.put(obj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			response.getWriter().write(array.toString());
		}else{
			response.getWriter().write("");
		}
	}
	public void getDriverUnSettle(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		ArrayList<ChargesBO> al_list = new ArrayList<ChargesBO>();
		try{
			JSONArray array = new JSONArray(request.getParameter("invoiceNo"));
			for (int i = 0; i < array.length(); i++) {
				ChargesBO settle = new ChargesBO();
				JSONObject statusobj=array.getJSONObject(i);
				if(statusobj.has("tripId")){
					settle.setTripid(statusobj.getString("tripId"));
				}
				if(statusobj.has("position")){
					settle.setPosition(statusobj.getInt("position"));
				}
				al_list.add(settle);	
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		MobileDAO.updatedriversettle(al_list,adminBO.getAssociateCode());
	}
	public void getMdriSettle(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		ChargesBO settle=new ChargesBO();
		String Cabno = request.getParameter("cabno");
		String driverid = request.getParameter("driverId");
		String position = request.getParameter("Position");
		ArrayList<ChargesBO> driversettle=MobileDAO.getdriversettle(adminBO.getAssociateCode(),Cabno,driverid,position);
		JSONArray array=new JSONArray();
		if (driversettle.size() > 0) {
			for (int i = 0; i < driversettle.size(); i++) {
				JSONObject status=new JSONObject();
				try {
					status.put("tripid",driversettle.get(i).getTripid());
					status.put("totalamount",driversettle.get(i).getTotalamount());
					array.put(status);
				} catch (Exception e) {
					System.out.println("Error :" + e);
				}
			}
		}
		response.getWriter().write(array.toString());
	}


	public void getCreditCardProcessingStatus(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		ChargesBO settle=new ChargesBO();
		String trasId = request.getParameter("transId");
		String tripid = request.getParameter("tripId");
		PaymentProcessBean processBean = null;
		JSONArray array = new JSONArray();
		JSONObject jasonObject = new JSONObject();
		processBean = ChargesDAO.getCCStatusForTripId(adminBO, tripid, trasId);
		try{
			if(processBean!=null){
				if(processBean.getAuthCapFlag().equalsIgnoreCase("2")){
					jasonObject.put("Message", "Status Updated");
				}else{
					jasonObject.put("Message", "Status Not Updated Yet");
				}
				jasonObject.put("Act", "CCStatus");
				jasonObject.put("STATUS", "APPROVED");
				jasonObject.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				jasonObject.put("APPROVALCODE", (processBean.getB_approval_status()));
				jasonObject.put("AMOUNT", processBean.getB_amount());
				jasonObject.put("TIP", processBean.getB_tip_amt());
				jasonObject.put("TOTAL", processBean.getTotal());
				jasonObject.put("FLAG", processBean.getAuthCapFlag());

				array.put(jasonObject);

			}else {
				jasonObject.put("Act", "CCStatus");
				jasonObject.put("STATUS", "DECLINED");
				array.put(jasonObject);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}


	public void driverRating(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("Driver Rating");
		String driverId = request.getParameter("driverID") != null ? request.getParameter("driverID") : "";
		String userId = request.getParameter("UserId") != null ? request.getParameter("UserId") : "";
		String tripID = request.getParameter("tripID") != null ? request.getParameter("tripID") : "";
		String rating = request.getParameter("rating") != null ? request.getParameter("rating") : "";
		String comments = request.getParameter("Comments") != null ? request.getParameter("Comments") : "";
		String ccode = request.getParameter("ccode") != null ? request.getParameter("ccode") : "";
		String cabNo = request.getParameter("cabNo") != null ? request.getParameter("cabNo") : "";
		int result;
		result = CustomerMobileDAO.driverrating(driverId, userId, tripID, rating, comments,ccode,cabNo);
		response.getWriter().write(result+"");
	}

	private void getPasswordKey(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String userID = request.getParameter("driverId");
		String fingerPrint=PasswordGen.generatePassword("A", 8);
		//String cCode = adminBO.getAssociateCode();

		int result1=0,result2=0;
		String phonenumber = "";
		int phlength = 0;
		CustomerMobileBO cmBo = new CustomerMobileBO();
		JSONArray array = new JSONArray();

		try{
			if(!userID.equals("")){
				cmBo = ServiceRequestDAO.customerUserIdVerification(userID);
				if(cmBo!=null){
					result2 = ServiceRequestDAO.customerRegeneratedPasswordKeyUpdate( cmBo.getAssoccode(),userID, fingerPrint);
					CompanySystemProperties cspBO = SystemPropertiesDAO.getCSPForCustomerAppCCProcess(cmBo.getAssoccode());
					if(result2>0 && !cmBo.getPhoneNumber().equals("")){
						try {
							sendSMS.main("Your 8-digit password Resetting key is "+fingerPrint, cspBO.getSmsPrefix()+cmBo.getPhoneNumber(), cmBo.getAssoccode());
						} catch (TwilioRestException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						phlength = cmBo.getPhoneNumber().length()-2;
						phonenumber=cmBo.getPhoneNumber().substring(cmBo.getPhoneNumber().length()-2);
						String stars="";
						for(int i =0; i<phlength; i++){
							stars = "*"+stars;
						}
						phonenumber=stars+phonenumber;

						JSONObject jObject = new JSONObject();
						jObject.put("Act", "GetPasswordkey");
						jObject.put("desc", "Success");
						jObject.put("status", 200);
						jObject.put("message", "Key is sended");
						jObject.put("phone", phonenumber);
						array.put(jObject);
					}else{
						JSONObject jObject = new JSONObject();
						jObject.put("Act", "GetPasswordkey");
						jObject.put("desc", "Failure");
						jObject.put("status", 300);
						jObject.put("message", "Cant able to update your key");
						array.put(jObject);
					}
				}else{
					JSONObject jObject = new JSONObject();
					jObject.put("Act", "GetPasswordkey");
					jObject.put("desc", "Failure");
					jObject.put("status", 400);
					jObject.put("message", "Not able to find your userId");
					array.put(jObject);
				}
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	public void forgetPassword(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String driverId = request.getParameter("driverId") == null ? "" : request.getParameter("driverId");
		String key = request.getParameter("seckey") == null ? "" : request.getParameter("seckey");
		String newPass = request.getParameter("newPassword") == null ? "" : request.getParameter("newPassword");
		String 	newPasswordHash="";
		if (newPass != null && !newPass.equals("")) {
			if (newPass.length() > 4) {
				try {
					newPasswordHash = PasswordHash.encrypt(newPass);
				} catch (Exception e) {
					System.out.println("Exception-->" + e.getMessage());
				}
				int update = ServiceRequestDAO.updateforgetPassword(driverId,key,newPasswordHash);
				if (update == 1) {
					response.getWriter().write("Password Updated Succesfully..");

				}else{
					response.getWriter().write("Your Key or User  id does not match..");
				}
			} else {
				response.getWriter().write("Password Strength too weak..Enter a password with more than 4 characters..");
			}
		}
	}
	private void ccToServer(HttpServletRequest request,HttpServletResponse response,AdminRegistrationBO adminBO) throws IOException {
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		String process=request.getParameter("process")!=null?request.getParameter("process"):null;
		int resultTxn=0;
		PaymentProcessBean payBean = new PaymentProcessBean();
		System.out.println("CC To Server--->"+request.getParameter("tripId"));
		payBean.setB_driverid(adminBO.getUid());
		payBean.setB_trip_id(request.getParameter("tripId"));
		payBean.setB_driverid(request.getParameter("driverId"));
		payBean.setB_cardHolderName(request.getParameter("cardHolderName"));
		payBean.setB_paymentType(request.getParameter("swipType"));
		payBean.setB_card_no(request.getParameter("cardNo"));
		payBean.setB_amount(request.getParameter("amt"));
		payBean.setB_approval_status(request.getParameter("approvalStatus"));
		payBean.setB_trans_id(request.getParameter("gateId"));
		payBean.setB_approval_code(request.getParameter("authCode"));		
		resultTxn=ChargesDAO.insertPaymentIntoCC(payBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBO, ICCConstant.SETTLED,ICCConstant.VANTIV);
		response.getWriter().write(resultTxn+"");
	}
	public void downloadVoice(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		String messageKey = request.getParameter("msgKey");
		String driverID = adminBO.getUid();
		byte[] vm = VoiceMessageDAO.getVoiceMessage(adminBO.getMasterAssociateCode(),driverID, messageKey);
		int loop = 0;
		if(vm != null){
			while(vm.length<8){
				loop++;
				if(loop>7){
					return;
				}
				vm = VoiceMessageDAO.getVoiceMessage(adminBO.getMasterAssociateCode(),driverID, messageKey);
				try{
					Thread.sleep(500);

				} catch(Exception e){
					loop=20;
					return;
				}
			}
			ServletOutputStream out = response.getOutputStream();
			response.setContentType("audio/mpeg");
			response.addHeader("Content-Disposition", "attachment; filename="+ messageKey);
			response.setContentLength((int) vm.length);
			out.write(vm);
		}
	}
	public void uploadVoiceFromDriver(HttpServletRequest request, HttpServletResponse response,AdminRegistrationBO adminBO) throws ServletException, IOException {
		int size = request.getContentLength();
		byte[] bytes = new byte[size];
		BufferedReader reader1 = request.getReader();
		int a = 0, counter = 0;
		while((a = reader1.read()) != -1){
			bytes[counter] = (byte)a;
			counter++;
		}
		String driverIDForDisplay = "";
		if(adminBO.getDispatchBasedOnVehicleOrDriver()==2){
			driverIDForDisplay = " Cab #"+adminBO.getVehicleNo();
			if(driverIDForDisplay.equals("")){
				driverIDForDisplay = adminBO.getUid();				
			}
		} else{
			driverIDForDisplay = adminBO.getUid();
		}
		int messageKey = VoiceMessageDAO.insertVoiceMessage(adminBO.getMasterAssociateCode(), driverIDForDisplay, bytes,1);
	}
	
	private void getAllDriverDetails(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<String> fleets = new ArrayList<String>();
		if(adminBO.getAssociateCode().equals(adminBO.getMasterAssociateCode()) && adminBO.getFleetDispatch()==1){
			fleets = adminBO.getCompanyList();
		} else {
			fleets.add(adminBO.getAssociateCode());
		}
		ArrayList<DriverCabQueueBean> listOfDriver = DispatchDAO.getDriverDetails(request.getParameter("assocode"));
		JSONArray messageArray = new JSONArray();
		for (int i = 0; i < listOfDriver.size(); i++) {
			JSONObject msgObj = new JSONObject();
			try {
				msgObj.put("dri", listOfDriver.get(i).getDriverid());
				msgObj.put("assocode", listOfDriver.get(i).getAssocode());
				messageArray.put(msgObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(messageArray.toString());

}
	
	public void uploadVoiceFromOperator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		int size = request.getContentLength();
		byte[] bytestemp = new byte[1];
		String driverId="";
		String[] driverIDsForMessage = null;
		String[] message =null;
		ByteArrayOutputStream tempByteArray = new ByteArrayOutputStream();
		ServletInputStream servletInput = request.getInputStream();
		//FileOutputStream os = new FileOutputStream("HareRama.wav");

		int readInt=0;

		while (-1 != (readInt =servletInput.read(bytestemp))) {
			tempByteArray.write(bytestemp);
			//os.write(bytestemp);
		}
		if(request.getParameter("driverid")!=null){
			driverId=request.getParameter("driverid");
			driverIDsForMessage = driverId.split(";");
		} 
		
		
		ArrayList<DriverCabQueueBean> al_list = new ArrayList<DriverCabQueueBean>();
		try{
			StringBuffer details = new StringBuffer();
			
			JSONArray array = new JSONArray(request.getParameter("driverIddtls"));
			for (int i = 0; i < array.length(); i++) {
				DriverCabQueueBean settle = new DriverCabQueueBean();
				JSONObject statusobj=array.getJSONObject(i);
				//System.out.println("driverId---->"+statusobj.getString("driverId"));
				//System.out.println("assocode---->"+statusobj.getString("assocode"));
				
				//Inform all drivers of the new message
				long msgID = System.currentTimeMillis();
				int messageKey = VoiceMessageDAO.insertVoiceMessage(statusobj.getString("assocode"),statusobj.getString("driverId"), tempByteArray.toByteArray(),2);
				 message = MessageGenerateJSON.generateVoicePushMessage(messageKey+"", msgID);
				 ArrayList<DriverCabQueueBean> driverDetails = UtilityDAO.getDriverListForMessage(statusobj.getString("assocode"), statusobj.getString("driverId")+"", 2, true);
				 if(driverDetails!=null){
				Messaging sendMessage = new Messaging(getServletContext(), driverDetails, message, poolBO, statusobj.getString("assocode"));
				sendMessage.run();
				 }
				if(statusobj.has("driverId")){
					settle.setDriverid(statusobj.getString("driverId"));
				}
				if(statusobj.has("assocode")){
					settle.setAssocode(statusobj.getString("assocode"));
				}
				al_list.add(settle);	
				
			}
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		/*int messageKey = VoiceMessageDAO.insertVoiceMessage(adminBO.getAssociateCode(), driverId, tempByteArray.toByteArray(),2);
		//Inform all drivers of the new message

		long msgID = System.currentTimeMillis();
		String[] message = MessageGenerateJSON.generateVoicePushMessage(messageKey+"", msgID);
		ArrayList<DriverCabQueueBean> driverDetails = UtilityDAO.getDriverListForMessaging(adminBO.getAssociateCode(), driverIDsForMessage, 0, true);
		Messaging sendMessage = new Messaging(getServletContext(), driverDetails, message, poolBO, adminBO.getAssociateCode());
		sendMessage.run();
		 */

	}
	
	public void sendGooglePushToCustomerIfDriverAccepts(final DriverLocationBO driverLocationBO, final AdminRegistrationBO adminBO){
		//System.out.println("Check have push lat,lon to customer or not.. started for driver:"+driverLocationBO.getDriverid()+" (or) adminID:"+adminBO.getUid());
		Thread thread = new Thread("Json Build with arguments") {
			public void run() {
				try {
					
					List openRequestList = RequestDAO.getAllocatedJobs(adminBO.getMasterAssociateCode(), adminBO.getUid(), adminBO.getVehicleNo(), adminBO.getTimeZone(),adminBO.getMasterAssociateCode());
					if(openRequestList==null || openRequestList.size()<1){
						//System.out.println("No jobs found for Driver:"+driverLocationBO.getDriverid());
						return;
					}
					
					//System.out.println("No. of jobs for driver:"+driverLocationBO.getDriverid()+" is "+openRequestList.size());
					for (int i = 0; i < openRequestList.size(); i++) {
						OpenRequestBO requestBO = (OpenRequestBO) openRequestList.get(i);
						if(requestBO.getTripSource()==0){
							JSONArray array = new JSONArray();
							JSONObject jobj = new JSONObject();
							jobj.put("HD", "DUC");
							jobj.put("M", "D");
							jobj.put("la", ""+driverLocationBO.getLatitude());
							jobj.put("lo", ""+driverLocationBO.getLongitude());
							jobj.put("dId", ""+driverLocationBO.getDriverid());
							jobj.put("tId", requestBO.getTripid());
							jobj.put("sId", ""+requestBO.getTripStatus());
							array.put(jobj);
							
							System.out.println("message to send:"+array.toString());
							//pushFuctionCalling(requestBO,array.toString(),adminBO.getAssociateCode());
							//mqtt
				            //sendMQTTMessage("", array.toString());
							String topic        = "MQTT Examples";
					        String content      = "Message from MqttPublishSample";
					        int qos             = 2;
					        String broker       = "tcp://chat.takeuz.com:1883";
					        String clientId     = "JavaSample";
					        MemoryPersistence persistence = new MemoryPersistence();
					        MqttClient sampleClient=null;

					        try {
					             sampleClient = new MqttClient(broker, clientId, persistence);
					            MqttConnectOptions connOpts = new MqttConnectOptions();
					            connOpts.setCleanSession(true);
					            connOpts.setCleanSession(true);
					            connOpts.setUserName("8148582763");
					            connOpts.setPassword("1215".toCharArray());

					            connOpts.setWill(topic,"abc".getBytes(StandardCharsets.UTF_8), 0, true);

					            System.out.println("Connecting to broker: "+broker);
					            sampleClient.connect(connOpts);
					          
					        } catch(MqttException me) {
					            System.out.println("reason "+me.getReasonCode());
					            System.out.println("msg "+me.getMessage());
					            System.out.println("loc "+me.getLocalizedMessage());
					            System.out.println("cause "+me.getCause());
					            System.out.println("excep "+me);
					            me.printStackTrace();

					        }

							MqttMessage message = new MqttMessage(array.toString().getBytes());
				            message.setQos(2);
				        	try {
								sampleClient.publish("PUSHMSG", message);
							} catch (MqttPersistenceException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (MqttException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
 
						}
					}
				}catch(JSONException e){
					System.out.println("JSON Exception");
					e.printStackTrace();
				}
			}

			
		};
		thread.start();
		
	}

}
