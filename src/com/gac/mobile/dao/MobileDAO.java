package com.gac.mobile.dao;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.log4j.Category;
import org.apache.xml.resolver.apps.resolver;
import org.groupby.parser.JSON.JSON;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.lowagie.text.pdf.codec.Base64;
import com.tds.cmp.bean.CabRegistrationBean;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.util.TDSSQLConstants;

public class MobileDAO {

	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(MobileDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+MobileDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static int getDriverActive(AdminRegistrationBO adminbo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO MobileDAO.getDriverActive");
		int result=0;
		cat.info("In Check getDriverStatus Method");

		PreparedStatement pst =null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			//TDOD Check this SQL!!!
			//pst = con.prepareStatement("SELECT * FROM TDS_DRIVER_DEACTIVATION where DATE_FORMAT(DD_TO_DATE_TIME,'%yyyy-%mm-%dd'  '%H:%i') <= DATE_FORMAT(now(),'%yyyy-%mm-%dd'  '%H:%i') and   DD_DRIVER_ID=? and   DD_ASSOCCODE=?");
			pst = con.prepareStatement("SELECT * FROM TDS_DRIVER_DEACTIVATION where DD_TO_DATE_TIME> now() AND DD_FROM_DATE_TIME < now() and   DD_DRIVER_ID=? and   DD_ASSOCCODE=?");
			pst.setString(1, adminbo.getUid());
			pst.setString(2, adminbo.getAssociateCode());
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.first()) {
				result =1; 
			} 
			/*else {
				pst1 = con.prepareStatement("DELETE from TDS_DRIVER_DEACTIVATION where DD_DRIVER_ID=? and DD_ASSOCCODE=?");
				pst1.setString(1,rs.getString("DD_DRIVER_ID"));
				pst1.setString(2,rs.getString("DD_ASSOCCODE"));
				pst1.execute();

			}*/

		}catch (SQLException sqex) {
			cat.error("TDSException MobileDAO.getDriverActive-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException MobileDAO.getDriverActive" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return  result;
	}

	public static String getDriverProperties(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO MobileDAO.getDriverProperties");
		String driverFlag="";
		cat.info("In Check getDriverStatus Method");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs=null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection(); 
			pst = con.prepareStatement("SELECT DR_FLAG FROM TDS_DRIVER where DR_USERID=? AND DR_ASSOCODE=?");
			pst.setString(1,adminBO.getUname());
			pst.setString(2,adminBO.getAssociateCode());
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				adminBO.setDriver_flag(rs.getString("DR_FLAG"));
				driverFlag = rs.getString("DR_FLAG");
			}
			rs.close();
			pst.close(); 
		}catch (SQLException sqex) {
			cat.error("TDSException MobileDAO.getDriverProperties-->"+sqex.getMessage());
			cat.error(pst.toString());

			//System.out.println("TDSException MobileDAO.getDriverProperties" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return  driverFlag;
	}

//	public static Blob viewSignForTripID (String tripID){
//		cat.info("TDS INFO FinanceDAO.viewSign");
//
//		PreparedStatement pst = null;
//
//		String query = "";
//
//		TDSConnection dbcon = new TDSConnection();
//		Connection con = dbcon.getConnection();
//		ResultSet rs=null;
//		Blob blobData = null;
//		try {
//			if (!tripID.equals("")) {
//				query = "select VD_RIDER_SIGN as sign_VC,CCD_RIDER_SIGNATURE as sign_CC FROM TDS_VOUCHER_DETAIL join  TDS_CREDIT_CARD_DETAIL where (VD_TRIP_ID='"+tripID+"' OR CCD_TRIP_ID='"+tripID+"')";
//				pst = con.prepareStatement(query);
//				rs = pst.executeQuery();
//				if (rs.next()) {
//					if(rs.getBlob("sign_VC").equals("")){
//						blobData = rs.getBlob("sign_CC");
//					}else{
//						blobData = rs.getBlob("sign_VC");
//					}
//					
//				}
//
//			}
//		} catch (Exception sqex) {
//			cat.error("TDSException FinanceDAO.viewSign-->"+sqex.getMessage());
//			cat.error(pst.toString());
//			System.out.println("TDSException FinanceDAO.viewSign-->"+sqex.getMessage());
//		} finally {
//			dbcon.closeConnection();
//		}
//		return blobData;
//	}
	public static int getCabDriverMap(String asscode, String driverid, String cabno) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);


		cat.info("TDS INFO MobileDAO.getCabDriverMap");
		int result =1;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs,rs1=null; 
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection(); 
			pst = con.prepareStatement("SELECT * FROM TDS_VEHICLE  where V_VNO='"+cabno+"' AND V_ASSOCCODE='"+asscode+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				//If not cab active
				if(rs.getInt("V_STATUS")==0){
					return 1;
				}
				
				//mapped already-->driver compare with current driver, if it is then login 
				// otherwise -->don't login
				
				pst = con.prepareStatement("SELECT * FROM TDS_CABDRIVERMAPPING  where CM_DRIVERID=? AND CM_ASSOCCODE=? AND CM_CABNO=? AND  CM_FROMDATE <=? AND CM_TODATE >=?");
				pst.setString(1, driverid);
				pst.setString(2, asscode);
				pst.setString(3, cabno);
				pst.setString(4, df.format(cal.getTime()));
				pst.setString(5, df.format(cal.getTime()));
//				System.out.println(pst.toString());
				rs = pst.executeQuery();
				if(rs.next()) {
					result=0; 
				} else { 
					pst = con.prepareStatement("SELECT * FROM TDS_CABDRIVERMAPPING  where CM_ASSOCCODE=? AND CM_CABNO=?  AND  CM_FROMDATE <=? AND CM_TODATE >=?"); 
					pst.setString(1,asscode);
					pst.setString(2, cabno);
					pst.setString(3, df.format(cal.getTime()));
					pst.setString(4, df.format(cal.getTime()));
			//		System.out.println(pst.toString());
					rs1 = pst.executeQuery(); 
					if(!rs1.next()) {
						result=0; 
					} 
				}
			}
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getCabDriverMap-->"+sqex.getMessage());
			cat.error(pst.toString());

			//System.out.println("TDSException MobileDAO.getCabDriverMap" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;		
	} 
	
	public static CabRegistrationBean getPaymentId(String asscode, String cabno) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst =null;
		ResultSet rs=null; 
		CabRegistrationBean cbBean = new CabRegistrationBean();
		try {
			pst = con.prepareStatement("SELECT V_PAY_ID,V_PAY_PASS FROM TDS_VEHICLE WHERE V_VNO='"+cabno+"' AND V_ASSOCCODE='"+asscode+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				cbBean.setPayId(rs.getString("V_PAY_ID"));
				cbBean.setPayPass(rs.getString("V_PAY_PASS"));
			}
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getCabDriverMap-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException MobileDAO.getCabDriverMap" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return cbBean;		
	} 

	public static String getCabProperties(String asscode, String cabno) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);


		cat.info("TDS INFO MobileDAO.getCabProperties");
		String cabFlags = "";
		String defaultMterRate="";
		//TDSConnection dbcon = null;
		//Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs = null; 

		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		
		try {
			pst = con.prepareStatement("SELECT V_FLAG FROM TDS_VEHICLE  where V_VNO=? AND V_ASSOCCODE=?");
			pst.setString(1, cabno);
			pst.setString(2, asscode);
			cat.info(pst.toString());
			rs = pst.executeQuery();

			if(rs.next()) {
				cabFlags = rs.getString("V_FLAG");	
			}  
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getCabProperties-->"+sqex.getMessage());
			//System.out.println("TDSException MobileDAO.getCabProperties" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return cabFlags;		
	} 
	public static String getCabDefaultMeterRate(String asscode, String cabno,String master) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO MobileDAO.getCabDefaultMeterRate");
		//TDSConnection dbcon = null;
		//Connection con = null;
		PreparedStatement pst =null,pst1=null;
		ResultSet rs = null,rs1=null; 
		boolean meterValuesSet=false;

		JSONArray meterArray = new JSONArray();
		
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		
		try {
			pst = con.prepareStatement("SELECT V_VMETER_RATE,MD_MINIMUM_SPEED,MD_START_AMOUNT,MD_RATE_PER_MILE,MD_RATE_PER_MIN FROM TDS_VEHICLE left join TDS_METER_DETAILS on MD_KEY=V_VMETER_RATE and MD_ASSOCIATION_CODE=? where V_VNO=? AND V_ASSOCCODE=?");
			pst.setString(1, master);
			pst.setString(2, cabno);
			pst.setString(3, asscode);
			cat.info(pst.toString());
			//System.out.println("Specified MeterValues on Login:"+pst.toString());
			rs = pst.executeQuery();
			if(rs.next()){
				if(rs.getString("V_VMETER_RATE")!=null && rs.getString("MD_MINIMUM_SPEED")!=null && rs.getString("MD_RATE_PER_MIN")!=null && rs.getString("MD_RATE_PER_MILE")!=null && rs.getString("MD_START_AMOUNT")!=null){
					meterValuesSet=true;
					JSONObject meterObject = new JSONObject();
					
					try {
						meterObject.putOpt("minimunSpeed", rs.getString("MD_MINIMUM_SPEED"));
						meterObject.putOpt("ratePerMinute", rs.getString("MD_RATE_PER_MIN"));
						meterObject.putOpt("ratePerMile", rs.getString("MD_RATE_PER_MILE"));
						meterObject.putOpt("startAmount", rs.getString("MD_START_AMOUNT"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					meterArray.put(meterObject);
				}
				//System.out.println("meterrate?:"+rs.getString("V_VMETER_RATE"));
				//System.out.println("MinimumSpeed:"+rs.getString("MD_MINIMUM_SPEED")+"/t StartAmount:"+rs.getString("MD_START_AMOUNT"));
			}
			if(!meterValuesSet){
				pst1 = con.prepareStatement("SELECT V_VMETER_RATE,MD_MINIMUM_SPEED,MD_START_AMOUNT,MD_RATE_PER_MILE,MD_RATE_PER_MIN FROM TDS_VEHICLE left join TDS_METER_DETAILS on MD_DEFAULT='1' and MD_ASSOCIATION_CODE=? where V_VNO=? AND V_ASSOCCODE=?");
				pst1.setString(1, master);
				pst1.setString(2, cabno);
				pst1.setString(3, asscode);
				cat.info(pst1.toString());
				//System.out.println("Default MeterValues on Login:"+pst1.toString());
				rs1 = pst1.executeQuery();
				if(rs1.next()){
					JSONObject meterObject = new JSONObject();
					if(rs1.getString("MD_MINIMUM_SPEED")!=null && rs1.getString("MD_RATE_PER_MIN")!=null && rs1.getString("MD_RATE_PER_MILE")!=null && rs1.getString("MD_START_AMOUNT")!=null){
					//System.out.println("MinimumSpeed:"+rs1.getString("MD_MINIMUM_SPEED")+"/t StartAmount:"+rs1.getString("MD_START_AMOUNT"));
						try {
							meterObject.putOpt("minimunSpeed", rs1.getString("MD_MINIMUM_SPEED"));
							meterObject.putOpt("ratePerMinute", rs1.getString("MD_RATE_PER_MIN"));
							meterObject.putOpt("ratePerMile", rs1.getString("MD_RATE_PER_MILE"));
							meterObject.putOpt("startAmount", rs1.getString("MD_START_AMOUNT"));
							meterArray.put(meterObject);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
			
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getCabDefaultMeterRate-->"+sqex.getMessage());
			//System.out.println("TDSException MobileDAO.getCabDefaultMeterRate" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		//System.out.println("Response on Meter RAtes: "+meterArray.toString());
		return meterArray.toString();		
	}
	
	public static int getDriversInZone(String asscode, String zoneNum) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO MobileDAO.getOpenJobs");
		int numberOfDrivers = 0;
		PreparedStatement pst =null;
		ResultSet rs = null; 
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 

		try {
			pst = con.prepareStatement("SELECT COUNT(*) as numOfDriver FROM TDS_QUEUE  where QU_ASSOCCODE=? AND QU_NAME=?");
			pst.setString(1, asscode);
			pst.setString(2, zoneNum);
			cat.info(pst.toString());
			rs = pst.executeQuery();

			if(rs.next()) {
				numberOfDrivers = rs.getInt("numOfDriver");				
			}  
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getDriversInZone-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getDriversInZone" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return numberOfDrivers;		
	} 
	public static String getCabNoForPhone(String asscode, String phoneNo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO MobileDAO.getCabNoForPhone");
		String cabNo="";
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs = null; 
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection(); 
			pst = con.prepareStatement("SELECT WX_CABNO FROM TDS_WRAPPER_XREF  where WX_ASSOCIATION_CODE=? AND WX_PHONE_NO=?");
			pst.setString(1, asscode);
			pst.setString(2, phoneNo);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.next()) {
				cabNo = rs.getString("WX_CABNO");				
			}  
			pst=con.prepareStatement("UPDATE TDS_WRAPPER_XREF SET WX_LOGIN_SWITCH='N' WHERE WX_CABNO=? AND WX_ASSOCIATION_CODE=?");
			pst.setString(1, cabNo);
			pst.setString(2, asscode);
			cat.info(pst.toString());
			pst.executeUpdate();
			pst=con.prepareStatement("UPDATE TDS_WRAPPER_XREF SET WX_LOGIN_SWITCH='Y' WHERE WX_PHONE_NO=? AND WX_ASSOCIATION_CODE=?");
			pst.setString(1, phoneNo);
			pst.setString(2, asscode);
			cat.info(pst.toString());
			pst.executeUpdate();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getCabNoForPhone-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getCabNoForPhone" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return cabNo;		
	} 
 
	public static String getFullMessage(String associationCode,String key) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO MobileDAO.getCabNoForPhone");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs = null; 
		String message = "";
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection(); 
			pst = con.prepareStatement("SELECT TM_MESSAGE FROM TDS_TEXT_MESSAGE WHERE TM_KEY='"+key+"' AND TM_ASSOCIATION_CODE='"+associationCode+"'");
			rs = pst.executeQuery();
			if(rs.next()) {
				message = rs.getString("TM_MESSAGE");				
			}  
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getFullMessage-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getFullMessage" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return message;
	}
	/**
	 * @param driverId
	 * @param associationCode
	 * @return int
	 * @author Venki
	 * @see Check the driver's Mandatory documents whether expired or not.
	 */
	
	public static int checkDocuments(String driverId,String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs = null; 
		int success=0; String query="";
		ArrayList arrayDoc=new ArrayList();
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try {
			pst = con.prepareStatement("SELECT MODULE_NAME FROM TDS_DOCUMENTS_MASTER WHERE DM_ASSOCCODE='"+associationCode+"' AND SERVERITY_TYPE='Critical'");
			rs = pst.executeQuery();
			while(rs.next()){
				arrayDoc.add(rs.getString("MODULE_NAME"));
			}
			if(arrayDoc!=null && arrayDoc.size()>0){
			    query="SELECT COUNT(DISTINCT(TDS_MODULE)) AS TOTAL FROM TDS_DOCUMENTS WHERE TDS_COMP_CODE ='"+associationCode+"' AND DOC_UPLOADED_BY ='"+driverId+"' AND DATE(DOC_EXP_DATE) >= DATE(NOW()) AND TDS_MODULE IN ( ";
				for(int i=0;i<arrayDoc.size()-1;i++){
					query= query+"'"+arrayDoc.get(i)+"'"+",";
				} 
				query=query+"'"+arrayDoc.get(arrayDoc.size()-1)+"'"+")";
				pst=con.prepareStatement(query);
				cat.info("MobileDAO.checkDocuments Query---->"+pst);
				rs = pst.executeQuery();
				if(rs.next()){
					int total=Integer.parseInt(rs.getString("TOTAL"));
					if(total==arrayDoc.size()){
						success=1;
					}
				}
			} else {
				return 1;
			}
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.checkDocuments-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.checkDocuments" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}	

	public static String checkDocumentsGoingToExpity(String driverId,String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs = null; 
		String docs="",query="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try {
			    query="SELECT DOC_TYPE,CASE WHEN DOC_EXP_DATE < NOW() THEN '0' ELSE '1' END AS STATUS FROM TDS_DOCUMENTS WHERE TDS_COMP_CODE ='"+associationCode+"' AND DOC_UPLOADED_BY ='"+driverId+"' AND DATE(DOC_EXP_DATE) <= DATE_SUB(NOW(),INTERVAL '-7' DAY)";
				cat.info("MobileDAO.checkDocuments Query---->"+pst);
				pst=con.prepareStatement(query);
				rs = pst.executeQuery();
				while(rs.next()){
					 docs+=rs.getString("DOC_TYPE")+",";
				}
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.checkDocumentsGoingToExpity-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.checkDocumentsGoingToExpity" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return docs;
	}	

	public static int checkCabAllocation(String asscode,String driverid,String cabno){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		int result =1;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst =null;
		ResultSet rs=null;

		try {
			pst = con.prepareStatement("SELECT DL_DRIVERID, DL_VEHICLE_NO  FROM TDS_DRIVERLOCATION WHERE DL_ASSOCCODE = '"+asscode+"'  AND DL_SWITCH <> 'L' AND (DL_DRIVERID ='"+driverid+"' OR  DL_VEHICLE_NO ='"+cabno+"' )");
			rs=pst.executeQuery();
			if(rs.next()){
				if(rs.getString("DL_DRIVERID").equals(driverid) && rs.getString("DL_VEHICLE_NO").equals(cabno)){		
					result=0;
				}
			} else {
				result=0;
			}
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.checkCabAllocation-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.checkCabAllocation" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;		
	}
	public static void insertOdoMeterValues(String asscode, String value,String tripId,String driverId,String cabNo,String event) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);


		cat.info("TDS INFO MobileDAO.insertOdoMeterValues");
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst =null;
		ResultSet rs = null; 
		try {

			pst=con.prepareStatement("INSERT INTO TDS_ODOMETER (OD_VALUE,OD_UPDATE_TIME,OD_TRIP_ID,OD_DRIVERID,OD_CAB_NO,OD_EVENT,OD_ASSOCCODE) VALUES(?,NOW(),?,?,?,?,?)");
			pst.setDouble(1, Double.parseDouble(value));
			pst.setString(2, tripId);
			pst.setString(3, driverId);
			pst.setString(4, cabNo);
			pst.setString(5, event);
			pst.setString(6, asscode);
			cat.info(pst.toString());
			pst.executeUpdate();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.insertOdoMeterValues-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.insertOdoMeterValues" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	} 
	public static int getLastUpdatedOdoMeterValue(String associationCode,String cabNo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);


		cat.info("TDS INFO MobileDAO.getLastUpdatedOdoMeterValue");
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst =null;
		ResultSet rs = null; 
		int odoMeterValue = 0;
		try {

			pst = con.prepareStatement("SELECT MAX(OD_VALUE) AS OD_VALUE  FROM TDS_ODOMETER WHERE OD_ASSOCCODE = '"+associationCode+"' AND OD_CAB_NO ='"+cabNo+"' ");
			rs = pst.executeQuery();
			if(rs.next()) {
				odoMeterValue = rs.getInt("OD_VALUE");				
			}  
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getLastUpdatedOdoMeterValue-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getLastUpdatedOdoMeterValue" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return odoMeterValue;
	}
	public static int checkSession(String sessionId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		int result =2;
		PreparedStatement pst =null;
		ResultSet rs;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_LOGINDETAILS WHERE LO_SESSIONID='"+sessionId+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				result=1;
			} else {
				result=0;
			}
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.checkSession-->"+sqex.getMessage());
			System.out.println("TDSException MobileDAO.checkSession" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;		 
	}
//	public static AdminRegistrationBO changeSession(String sessionId,String newId){
//		long startTime = System.currentTimeMillis();
//		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
//
//
//		PreparedStatement pst =null;
//		ResultSet rs=null;
//		AdminRegistrationBO adminBo=new AdminRegistrationBO();
//		TDSConnection dbcon = new TDSConnection();
//		Connection con = dbcon.getConnection(); 
//		try {
//			pst = con.prepareStatement("UPDATE TDS_LOGINDETAILS SET LO_SESSIONID='"+newId+"' WHERE LO_SESSIONID='"+sessionId+"'");
//			pst.executeUpdate();
//			pst=con.prepareStatement("SELECT * FROM TDS_LOGINDETAILS LEFT JOIN TDS_DRIVERLOCATION ON DL_DRIVERID=LO_DRIVERID AND DL_MASTER_ASSOCCODE=LO_MASTER_ASSOCCODE WHERE LO_SESSIONID='"+newId+"'");
//			rs=pst.executeQuery();
//			if(rs.next()){
//				adminBo.setUid(rs.getString("LO_DriverID"));
//				adminBo.setUname(rs.getString("LO_DriverID"));
//				adminBo.setAssociateCode(rs.getString("LO_ASSCODE"));
//				adminBo.setVehicleNo(rs.getString("LO_CABNO"));
//				adminBo.setGoogRegKey(rs.getString("LO_GOOG_REG_KEY"));
//				adminBo.setPhnumber(rs.getString("LO_DPHONENO"));
//				adminBo.setMasterAssociateCode(rs.getString("LO_MASTER_ASSOCCODE"));
//				adminBo.setUserNameDisplay(rs.getString("DL_DRNAME"));
//				adminBo.setProvider(rs.getString("DL_PROVIDER"));
//				adminBo.setDriver_flag(rs.getString("DL_PROFILE"));
//			}
//			pst.close();
//			rs.close();
//		} catch (SQLException  sqex) {
//			cat.error("TDSException MobileDAO.checkSession-->"+sqex.getMessage());
//		//	System.out.println("TDSException MobileDAO.checkSession" + sqex.getMessage());
//			sqex.printStackTrace();
//		} finally {
//			dbcon.closeConnection();
//		}
//		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
//
//		return adminBo;
//	}
	public static int isLoggedIn(String asscode,String driverid,String cabNo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs,rs1;
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try {
			pst = con.prepareStatement("SELECT DL_VEHICLE_NO FROM TDS_DRIVERLOCATION WHERE DL_ASSOCCODE = '"+asscode+"' AND DL_DRIVERID = '"+driverid+"' AND DL_SWITCH <> 'L'");
			rs=pst.executeQuery();
			pst = con.prepareStatement("SELECT DL_DRIVERID FROM TDS_DRIVERLOCATION WHERE DL_ASSOCCODE = '"+asscode+"' AND DL_VEHICLE_NO = '"+cabNo+"' AND DL_SWITCH <> 'L'");
			rs1=pst.executeQuery();
			if(rs.next() || rs1.next()){
				result=1;
			} 
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.isLoggedIn-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.isLoggedIn" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;		
	}
	public static String getFlagsForJob(String tripId,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		PreparedStatement pst =null;
		ResultSet rs=null;
		String flags="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst=con.prepareStatement("SELECT OR_DRCABFLAG FROM TDS_OPENREQUEST WHERE OR_TRIP_ID='"+tripId+"' AND OR_ASSOCCODE='"+assoccode+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				flags=rs.getString("OR_DRCABFLAG");
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getFlagsForJob-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getFlagsForJob" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return flags;
	}
	public static String getFlagsForDriver(String driverId,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		PreparedStatement pst =null;
		ResultSet rs=null;
		String flags="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst=con.prepareStatement("SELECT DL_PROFILE FROM TDS_DRIVERLOCATION WHERE DL_DRIVERID='"+driverId+"' AND DL_ASSOCCODE='"+assoccode+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				flags=rs.getString("DL_PROFILE");
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getFlagsForJob-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getFlagsForJob" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return flags;
	}
	public static String getPhoneNum(String driverId,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		PreparedStatement pst =null;
		ResultSet rs=null;
		String phNum="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst=con.prepareStatement("SELECT DR_PHONE FROM TDS_DRIVER WHERE DR_USERID='"+driverId+"' AND DR_ASSOCODE='"+assoccode+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				phNum=rs.getString("DR_PHONE");
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getPhoneNum-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getPhoneNum--->" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return phNum;
	}

	public static ArrayList<String> getCannedMessages(String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		PreparedStatement pst =null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		ArrayList<String> ms_list = new ArrayList<String>();
		try {
			pst=con.prepareStatement("SELECT CM_MESSAGE FROM TDS_CANNED_MESSAGE WHERE CM_ASSOCCODE ='"+assoccode+"' AND CM_CATEGORY ='0'");
			rs=pst.executeQuery();
			while(rs.next()){
				ms_list.add(rs.getString("CM_MESSAGE"));
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getCannedMessages-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getCannedMessages--->" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ms_list;
	}
	public static String getCurrentTime(AdminRegistrationBO adminbo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO MobileDAO.getDriverActive");
		String time="";
		cat.info("In Check getDriverStatus Method");
		PreparedStatement pst =null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		String date="";
		try {
			pst = con.prepareStatement("SELECT CONVERT_TZ(NOW(),'UTC','"+adminbo.getTimeZone()+"') AS TIME");
			//System.out.println("COnvert query :"+pst.toString());
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.first()) {
				date=rs.getString("TIME");
			} 
		}catch (SQLException sqex) {
			cat.error("TDSException MobileDAO.getDriverActive-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException MobileDAO.getDriverActive" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return  date;
	}
	public static ArrayList<DriverCabQueueBean> getActiveDrivers(String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		PreparedStatement pst =null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		ArrayList<DriverCabQueueBean> drivers_list = new ArrayList<DriverCabQueueBean>();
		try {
			pst=con.prepareStatement("SELECT  REPLACE(DR_PHONE,'-','') AS DR_PHONE,DR_USERID FROM TDS_DRIVER LEFT JOIN TDS_DRIVERLOCATION ON DR_USERID=DL_DRIVERID WHERE  DR_ASSOCODE=DL_ASSOCCODE AND DR_ASSOCODE ='"+assoccode+"' AND DR_ACTIVESTATUS ='1' AND DL_SWITCH='L' ORDER BY DR_USERID");
			rs=pst.executeQuery();
			while(rs.next()){
				DriverCabQueueBean drBean = new DriverCabQueueBean();
				drBean.setDriverid(rs.getString("DR_USERID"));
				drBean.setPhoneNo(rs.getString("DR_PHONE"));
				drivers_list.add(drBean);
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getActiveDrivers-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getActiveDrivers--->" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drivers_list;
	}
	public static String getDriverPhone(String passPhone,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		PreparedStatement pst =null;
		ResultSet rs=null;
		String driverPhone="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst=con.prepareStatement("SELECT DR_USERID,DR_PHONE FROM TDS_OPENREQUEST JOIN TDS_DRIVER ON OR_DRIVERID=DR_USERID WHERE OR_PHONE='"+passPhone+"' AND OR_ASSOCCODE='"+assoccode+"' ORDER BY OR_TRIP_ID");
			rs=pst.executeQuery();
			if(rs.next()){
				driverPhone=rs.getString("DR_PHONE");
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getDriverPhone-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getDriverPhone--->" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverPhone;
	}
	public static int getDriverRating(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		int driverRating=1;
		PreparedStatement pst =null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst = con.prepareStatement("SELECT DR_RATING FROM TDS_DRIVER where DR_USERID=? AND DR_ASSOCODE=?");
			pst.setString(1,adminBO.getUname());
			pst.setString(2,adminBO.getAssociateCode());
			rs=pst.executeQuery();
			if(rs.next()){
				//adminBO.setDriverRating(rs.getInt("DR_RATING"));
				driverRating = rs.getInt("DR_RATING");
			}
			rs.close();
			pst.close(); 
		}catch (SQLException sqex) {
			cat.error("TDSException MobileDAO.getDriverRating--->"+sqex.getMessage());
			cat.error(pst.toString());

		//	System.out.println("TDSException MobileDAO.getDriverRating--->" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return  driverRating;
	}
	public static int getDriverAccess(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		int driverAccess=1;
		PreparedStatement pst =null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst = con.prepareStatement("SELECT DR_SETTINGS_ACCESS FROM TDS_DRIVER where DR_USERID=? AND DR_ASSOCODE=?");
			pst.setString(1,adminBO.getUname());
			pst.setString(2,adminBO.getAssociateCode());
			rs=pst.executeQuery();
			if(rs.next()){
				driverAccess = rs.getInt("DR_SETTINGS_ACCESS");
			}
			rs.close();
			pst.close(); 
		}catch (SQLException sqex) {
			cat.error("TDSException MobileDAO.getDriverRating--->"+sqex.getMessage());
			cat.error(pst.toString());

		//	System.out.println("TDSException MobileDAO.getDriverRating--->" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return  driverAccess;
	}
	public static DriverCabQueueBean getStatus(String associationcode,String cabno,String driverid,String timezone,int version){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);


		PreparedStatement pst =null;
		ResultSet rs=null;
		DriverCabQueueBean status=new DriverCabQueueBean();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			if(version>89){
				pst=con.prepareStatement("SELECT DL_DRIVERID,DL_DRNAME,DL_VEHICLE_NO,DL_LATITUDE,DL_LONGITUDE,DATE_FORMAT(CONVERT_TZ(DL_LOGIN_TIME,'UTC','"+timezone+"'),'%Y-%m-%d %h:%i %p') AS TIME,DL_SWITCH,QU_NAME FROM TDS_DRIVERLOCATION LEFT JOIN TDS_QUEUE ON DL_ASSOCCODE=QU_ASSOCCODE AND DL_DRIVERID=QU_DRIVERID WHERE DL_ASSOCCODE='"+associationcode+"' AND DL_VEHICLE_NO='"+cabno+"' AND DL_DRIVERID='"+driverid+"' ");
			} else {
				pst=con.prepareStatement("SELECT DL_DRIVERID,DL_DRNAME,DL_VEHICLE_NO,DL_LATITUDE,DL_LONGITUDE,CONVERT_TZ(DL_LOGIN_TIME,'UTC','"+timezone+"') AS TIME,DL_SWITCH,QU_NAME FROM TDS_DRIVERLOCATION LEFT JOIN TDS_QUEUE ON DL_ASSOCCODE=QU_ASSOCCODE AND DL_DRIVERID=QU_DRIVERID WHERE DL_ASSOCCODE='"+associationcode+"' AND DL_VEHICLE_NO='"+cabno+"' AND DL_DRIVERID='"+driverid+"' ");
			}
			rs=pst.executeQuery();
			if(rs.next()){
				status.setDriverid(rs.getString("DL_DRIVERID"));
				status.setDrivername(rs.getString("DL_DRNAME"));
				status.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				status.setLogintime(rs.getString("TIME"));
				status.setAvailability(rs.getString("DL_SWITCH"));
				status.setLatitude(rs.getString("DL_LATITUDE"));
				status.setLongitude(rs.getString("DL_LONGITUDE"));
				status.setQueue(rs.getString("QU_NAME"));
				}
			//CONVERT_TZ(NOW(),'UTC','"+adminbo.getTimeZone()+"') AS TIME
		//	SELECT DR_USERID,DR_PHONE FROM TDS_OPENREQUEST JOIN TDS_DRIVER ON OR_DRIVERID=DR_USERID WHERE OR_PHONE='"+passPhone+"' AND OR_ASSOCCODE='"+assoccode+"' ORDER BY OR_TRIP_ID"
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getstatus-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getstatus" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}

	public static String getDriverDocument(String driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Blob blobData = null;
		String fileName = "";
		String fName="";
		String lName="";
		String phNumber="";
		String query = "";
		String result="";
		String array1;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			if (!driverId.equals("")) {
//				query = "SELECT DOC_DATA,DOCUMENT_NAME FROM TDS_DOCUMENTS WHERE DOC_TYPE = 'Driver Photo' AND DOC_UPLOADED_BY='"+driverId+"'";
				query = "SELECT DR_FNAME,DR_LNAME,DR_PHONE,DOC_DATA,DOCUMENT_NAME FROM TDS_DRIVER,TDS_DOCUMENTS WHERE TDS_DRIVER.DR_USERID = TDS_DOCUMENTS.DOC_UPLOADED_BY AND DOC_TYPE = 'Driver Photo' AND DOC_UPLOADED_BY='"+driverId+"'";
				pst = con.prepareStatement(query);
				rs = pst.executeQuery();
				System.out.println("ResultSet"+rs.toString());
				while (rs.next()) {
					fName=rs.getString(1);
					lName=rs.getString(2);
					phNumber=rs.getString(3);
					blobData = rs.getBlob(4);
					fileName = rs.getString(5);
				}
				if(blobData!=null){
					InputStream binaryStream = blobData.getBinaryStream();
					byte[] imageBytes = blobData.getBytes(1, (int) blobData.length());
					String s = new String(imageBytes); 
					String encode = Base64.encodeBytes(imageBytes);
					result = encode;
				}
				/*array1[0]=fName+" "+lName;
				array1[1]=phNumber;
				array1[2]=result;*/
			}
			
		} catch (SQLException sqex ) {
			cat.error("TDSException  .getDriverDocument->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CustomerMobileDAO.getDriverDocument-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		array1=fName+lName+";"+phNumber+";"+result;
		return array1;
	}

	public static void  updatedriversettle(ArrayList<ChargesBO> al_list,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst =null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			for(int j=0;j<al_list.size();j++){
				pst = con.prepareStatement("UPDATE TDS_VOUCHER_DETAIL SET VD_VERIFIED ='1' WHERE VD_TRIP_ID='"+al_list.get(j).getTripid()+"'");
				System.out.println("query"+pst); 
				pst.executeUpdate();
				pst.close();			
			}
		}catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.updatedriversettle-->"+sqex.getMessage());
			//System.out.println("TDSException MobileDAO.updatedriversettle" + sqex.getMessage());
			sqex.printStackTrace();
		} 		
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}
	
	public static ArrayList<ChargesBO>  getdriversettle(String associationcode,String cabno,String driverid,String position){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst =null;
		ResultSet rs=null;
		ArrayList<ChargesBO> settle=new ArrayList<ChargesBO> ();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			if(position.equals("0")){
				pst=con.prepareStatement("SELECT VD_TRIP_ID,VD_TOTAL FROM TDS_VOUCHER_DETAIL  where VD_DRIVER_PMT_STATUS='0' and VD_DRIVER_ID='"+driverid+"' and VD_ASSOC_CODE='"+associationcode+"'  and  VD_VERIFIED='0'");	
			}
			else if(position.equals("1")){
				pst=con.prepareStatement("SELECT VD_TRIP_ID,VD_TOTAL FROM TDS_VOUCHER_DETAIL  where VD_DRIVER_PMT_STATUS='0' and VD_DRIVER_ID='"+driverid+"' and VD_ASSOC_CODE='"+associationcode+"'  and  VD_VERIFIED<>'0'");	
			}
			else if(position.equals("2")){
				pst=con.prepareStatement("SELECT VD_TRIP_ID,VD_TOTAL FROM TDS_VOUCHER_DETAIL  where VD_DRIVER_PMT_STATUS<>'0' and VD_DRIVER_ID='"+driverid+"' and VD_ASSOC_CODE='"+associationcode+"'  and  VD_VERIFIED<>'0'");	
			}
			rs=pst.executeQuery();
			while(rs.next()){
				ChargesBO driver=new ChargesBO();
				driver.setTripid(rs.getString("VD_TRIP_ID"));
				driver.setTotalamount(rs.getString("VD_TOTAL"));
				settle.add(driver);
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getdriversettle-->"+sqex.getMessage());
		//	System.out.println("TDSException MobileDAO.getdriversettle" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return settle;
	}
	
	public static void  insertSessionObjects(String sessionId,String values,String driverId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst =null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		int rs = 0;
		try {
			pst = con.prepareStatement("UPDATE TDS_SESSION SET SS_VALUES = ?,SS_DRIVERID=? WHERE SS_ID=?");
			pst.setString(1, values);
			pst.setString(2,driverId);
			pst.setString(3,sessionId);
			rs = pst.executeUpdate();
			if(rs==0){
				pst = con.prepareStatement("INSERT INTO TDS_SESSION (SS_ID,SS_VALUES,SS_DRIVERID) VALUES (?,?,?)");
				pst.setString(1,sessionId);
				pst.setString(2, values);
				pst.setString(3, driverId);
				
				pst.execute();
			}
		}catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.insertSessionObjects-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 		
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}

	public static StringBuffer getSessionValues(String sessionId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst =null;
		ResultSet rs=null;
		StringBuffer values=new StringBuffer();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT SS_VALUES FROM TDS_SESSION WHERE SS_ID='"+sessionId+"'");
			rs = pst.executeQuery();
			if(rs.next()){
				values.append(rs.getString("SS_VALUES"));
			}
		}catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.insertSessionObjects-->"+sqex.getMessage());
			//System.out.println("TDSException MobileDAO.updatedriversettle" + sqex.getMessage());
			sqex.printStackTrace();
		} 		
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return values;
	}

	public static void removeSessionObjects(String sessionId,String driverId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst =null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("DELETE FROM TDS_SESSION WHERE SS_ID='"+sessionId+"' OR SS_DRIVERID='"+driverId+"'");
			pst.execute();
		}catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.insertSessionObjects-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 		
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}
	public static void insertLogoutDetail(String sessionId,String driverId,String doneBy,String reason,String companyCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst =null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("INSERT INTO TDS_DRIVER_LOGOUT (DL_DRIVERID,DL_SESSIONID,DL_TIME,DL_LOGGEDOUT_BY,DL_REASON,DL_COMPANY_CODE) VALUES (?,?,NOW(),?,?,?)");
			pst.setString(1, driverId);
			pst.setString(2, sessionId);
			pst.setString(3, doneBy);
			pst.setString(4, reason);
			pst.setString(5, companyCode);
			pst.execute();
		}catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.insertSessionObjects-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 		
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}
	public static boolean logDriverOut(String driverId,String cabNum,String timeOut,String masterAssoccode,int type){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst =null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		boolean returnMessage = false;
//		String extQuery="";
//		if(type==1){
//			extQuery = " AND DL_LASTUPDATE < DATE_SUB(NOW(),INTERVAL '"+timeOut+"' MINUTE)";
//		}
		try {
			pst = con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_SWITCH='L' WHERE DL_DRIVERID='"+driverId+"' AND DL_VEHICLE_NO='"+cabNum+"' AND DL_MASTER_ASSOCCODE='"+masterAssoccode+"' AND DL_LASTUPDATE < DATE_SUB(NOW(),INTERVAL '"+timeOut+"' MINUTE)");
			int updateSwitch=pst.executeUpdate();
			if(updateSwitch>0){
				pst = con.prepareStatement("DELETE FROM TDS_SESSION WHERE SS_DRIVERID='"+driverId+"'");
				pst.execute();
				pst = con.prepareStatement(TDSSQLConstants.deleteDriverFromQueue); 
				pst.setString(1, driverId);
				pst.setString(2, masterAssoccode);
				returnMessage = true;
			}
		}catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.insertSessionObjects-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 		
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnMessage;
	}
	
	public static String getCabNobySeqNo(String asscode,String cabNo, int cab_type){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String result = "";
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try {
			if(cab_type==1){
				pst = con.prepareStatement("SELECT V_VNO FROM TDS_VEHICLE WHERE V_ASSOCCODE ='"+asscode+"' AND V_PLATE_NO='"+cabNo+"' ");
			}else if (cab_type==2){
				pst = con.prepareStatement("SELECT V_VNO FROM TDS_VEHICLE WHERE V_ASSOCCODE ='"+asscode+"' AND V_RCNO = '"+cabNo+"' ");
			}else if(cab_type==3){
				pst = con.prepareStatement("SELECT V_VNO FROM TDS_VEHICLE WHERE V_ASSOCCODE ='"+asscode+"' AND V_VINNO = '"+cabNo+"' ");
			}else{
				pst = con.prepareStatement("SELECT V_VNO FROM TDS_VEHICLE WHERE V_ASSOCCODE ='"+asscode+"' AND V_VNO = '"+cabNo+"' ");
			}
			System.out.println("CAb No get : "+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				result = rs.getString("V_VNO");
			}else{
				result = cabNo;
			}
			
			
		} catch (SQLException  sqex) {
			cat.error("TDSException MobileDAO.getCabNobySeqNo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;		
	}
}
