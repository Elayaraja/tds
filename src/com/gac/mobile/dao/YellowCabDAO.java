package com.gac.mobile.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.DriverLocationBO;

public class YellowCabDAO {
	
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(YellowCabDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+YellowCabDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static String loginDriver(String phoneNo,String cabNo,String assoccode,String sessionId) {
		long startTime = System.currentTimeMillis();
		cat.info("YellowCabDAO.readZonesBoundries Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs = null; 
		String driver = "";
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 

		try {
			pst = con.prepareStatement("SELECT WX_DRIVERID FROM TDS_WRAPPER_XREF where WX_ASSOCIATION_CODE=? AND WX_DRIVERID=?");
			pst.setString(1, assoccode);
			pst.setString(2, phoneNo);
			rs = pst.executeQuery();
			if(rs.next()) {
				pst=con.prepareStatement("UPDATE TDS_WRAPPER_XREF SET WX_LOGIN_SWITCH='Y',WX_CABNO=?,WX_SESSION_ID=? WHERE WX_PHONE_NO=? AND WX_ASSOCIATION_CODE=?");
				pst.setString(1, cabNo);
				pst.setString(2, sessionId);
				pst.setString(3, phoneNo);
				pst.setString(4, assoccode);
				result=pst.executeUpdate();
				if(result>0){
					pst=con.prepareStatement("UPDATE TDS_WRAPPER_XREF SET WX_LOGIN_SWITCH='N' WHERE WX_CABNO=? AND WX_ASSOCIATION_CODE=? AND WX_DRIVERID <> ?");
					pst.setString(1, cabNo);
					pst.setString(2, assoccode);
					pst.setString(3, phoneNo);
					pst.executeUpdate();	
				}

			}   else {
				pst=con.prepareStatement("INSERT INTO TDS_WRAPPER_XREF (WX_LOGIN_SWITCH,WX_CABNO,WX_SESSION_ID,WX_PHONE_NO,WX_ASSOCIATION_CODE,WX_DRIVERID,WX_PASSWORD) VALUES ('Y',?,?,?,?,?,'1234')");
				pst.setString(1, cabNo);
				pst.setString(2, sessionId);
				pst.setString(3, phoneNo);
				pst.setString(4, assoccode);
				pst.setString(5, phoneNo);
				result=pst.executeUpdate();
				
				pst=con.prepareStatement("INSERT INTO TDS_DRIVERLOCATION (DL_DRIVERID,DL_LATITUDE,DL_LONGITUDE,DL_SWITCH,DL_ASSOCCODE,DL_PHONENO,DL_LASTUPDATE,DL_PROVIDER) VALUES (?,'0.00',0.00','Y',?,?,NOW(),'ATT')");
				pst.setString(1, phoneNo);
				pst.setString(2, assoccode);
				pst.setString(3, phoneNo);
				pst.executeUpdate();
			}
		} catch (SQLException  sqex) {
			cat.error("TDSException YellowCabDAO.getCabNoForPhone-->"+sqex.getMessage());
			//System.out.println("TDSException YellowCabDAO.getCabNoForPhone" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("YellowCabDAO.readZonesBoundries End--->"+(System.currentTimeMillis()-startTime));
		return driver;		
	} 
	public static int logoutDriver(String phoneNo,String cabNo,String assoccode,String sessionId) {
		long startTime = System.currentTimeMillis();
		cat.info("YellowCabDAO.readZonesBoundries Start--->"+startTime);
		int rV = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst =null;
		ResultSet rs = null; 
		String driver = "";
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try{	
			pst=con.prepareStatement("UPDATE TDS_WRAPPER_XREF SET WX_LOGIN_SWITCH='N' WHERE WX_ASSOCIATION_CODE=? AND WX_DRIVERID = ?");
			pst.setString(1, assoccode);
			pst.setString(2, phoneNo);
			rV = pst.executeUpdate();	
		} catch (SQLException  sqex) {
			cat.error("TDSException YellowCabDAO.logoutDriver-->"+sqex.getMessage());
			//System.out.println("TDSException YellowCabDAO.logoutDriver" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("YellowCabDAO.readZonesBoundries End--->"+(System.currentTimeMillis()-startTime));
		return rV;
	}

	public static int updateDriverLocation(DriverLocationBO driverLocBO) {
		long startTime = System.currentTimeMillis();
		cat.info("YellowCabDAO.readZonesBoundries Start--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_LATITUDE = ?,DL_LONGITUDE = ? DL_LASTUPDATE = NOW() WHERE DL_DRIVERID = ?");
			
			pst.setString(1, driverLocBO.getLatitude());
			pst.setString(2, driverLocBO.getLongitude()) ;
			pst.setString(3, driverLocBO.getDriverid());
			result = pst.executeUpdate();

			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			//System.out.println("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("YellowCabDAO.readZonesBoundries End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
}
