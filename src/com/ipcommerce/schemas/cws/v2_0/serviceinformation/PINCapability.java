
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PINCapability.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PINCapability">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="PINSupported"/>
 *     &lt;enumeration value="PINNotSupported"/>
 *     &lt;enumeration value="PINVerifiedByDevice"/>
 *     &lt;enumeration value="PINPadInoperative"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PINCapability")
@XmlEnum
public enum PINCapability {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("PINSupported")
    PIN_SUPPORTED("PINSupported"),
    @XmlEnumValue("PINNotSupported")
    PIN_NOT_SUPPORTED("PINNotSupported"),
    @XmlEnumValue("PINVerifiedByDevice")
    PIN_VERIFIED_BY_DEVICE("PINVerifiedByDevice"),
    @XmlEnumValue("PINPadInoperative")
    PIN_PAD_INOPERATIVE("PINPadInoperative");
    private final String value;

    PINCapability(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PINCapability fromValue(String v) {
        for (PINCapability c: PINCapability.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
