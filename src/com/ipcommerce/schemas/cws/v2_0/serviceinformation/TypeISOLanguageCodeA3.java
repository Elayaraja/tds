
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeISOLanguageCodeA3.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeISOLanguageCodeA3">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="AAR"/>
 *     &lt;enumeration value="ABK"/>
 *     &lt;enumeration value="ACE"/>
 *     &lt;enumeration value="ACH"/>
 *     &lt;enumeration value="ADA"/>
 *     &lt;enumeration value="ADY"/>
 *     &lt;enumeration value="AFA"/>
 *     &lt;enumeration value="AFH"/>
 *     &lt;enumeration value="AFR"/>
 *     &lt;enumeration value="AIN"/>
 *     &lt;enumeration value="AKA"/>
 *     &lt;enumeration value="AKK"/>
 *     &lt;enumeration value="ALB"/>
 *     &lt;enumeration value="ALE"/>
 *     &lt;enumeration value="ALG"/>
 *     &lt;enumeration value="ALT"/>
 *     &lt;enumeration value="AMH"/>
 *     &lt;enumeration value="ANG"/>
 *     &lt;enumeration value="ANP"/>
 *     &lt;enumeration value="APA"/>
 *     &lt;enumeration value="ARA"/>
 *     &lt;enumeration value="ARC"/>
 *     &lt;enumeration value="ARG"/>
 *     &lt;enumeration value="ARM"/>
 *     &lt;enumeration value="ARN"/>
 *     &lt;enumeration value="ARP"/>
 *     &lt;enumeration value="ARW"/>
 *     &lt;enumeration value="ASM"/>
 *     &lt;enumeration value="AST"/>
 *     &lt;enumeration value="ATH"/>
 *     &lt;enumeration value="AUS"/>
 *     &lt;enumeration value="AVA"/>
 *     &lt;enumeration value="AVE"/>
 *     &lt;enumeration value="AWA"/>
 *     &lt;enumeration value="AYM"/>
 *     &lt;enumeration value="AZE"/>
 *     &lt;enumeration value="BAD"/>
 *     &lt;enumeration value="BAI"/>
 *     &lt;enumeration value="BAK"/>
 *     &lt;enumeration value="BAL"/>
 *     &lt;enumeration value="BAM"/>
 *     &lt;enumeration value="BAN"/>
 *     &lt;enumeration value="BAQ"/>
 *     &lt;enumeration value="BAS"/>
 *     &lt;enumeration value="BAT"/>
 *     &lt;enumeration value="BEJ"/>
 *     &lt;enumeration value="BEL"/>
 *     &lt;enumeration value="BEM"/>
 *     &lt;enumeration value="BEN"/>
 *     &lt;enumeration value="BER"/>
 *     &lt;enumeration value="BHO"/>
 *     &lt;enumeration value="BIH"/>
 *     &lt;enumeration value="BIK"/>
 *     &lt;enumeration value="BIN"/>
 *     &lt;enumeration value="BIS"/>
 *     &lt;enumeration value="BLA"/>
 *     &lt;enumeration value="BNT"/>
 *     &lt;enumeration value="BOD"/>
 *     &lt;enumeration value="BOS"/>
 *     &lt;enumeration value="BRA"/>
 *     &lt;enumeration value="BRE"/>
 *     &lt;enumeration value="BTK"/>
 *     &lt;enumeration value="BUA"/>
 *     &lt;enumeration value="BUG"/>
 *     &lt;enumeration value="BUL"/>
 *     &lt;enumeration value="BUR"/>
 *     &lt;enumeration value="BYN"/>
 *     &lt;enumeration value="CAD"/>
 *     &lt;enumeration value="CAI"/>
 *     &lt;enumeration value="CAR"/>
 *     &lt;enumeration value="CAT"/>
 *     &lt;enumeration value="CAU"/>
 *     &lt;enumeration value="CEB"/>
 *     &lt;enumeration value="CEL"/>
 *     &lt;enumeration value="CES"/>
 *     &lt;enumeration value="CHA"/>
 *     &lt;enumeration value="CHB"/>
 *     &lt;enumeration value="CHE"/>
 *     &lt;enumeration value="CHG"/>
 *     &lt;enumeration value="CHI"/>
 *     &lt;enumeration value="CHK"/>
 *     &lt;enumeration value="CHM"/>
 *     &lt;enumeration value="CHN"/>
 *     &lt;enumeration value="CHO"/>
 *     &lt;enumeration value="CHP"/>
 *     &lt;enumeration value="CHR"/>
 *     &lt;enumeration value="CHU"/>
 *     &lt;enumeration value="CHV"/>
 *     &lt;enumeration value="CHY"/>
 *     &lt;enumeration value="CMC"/>
 *     &lt;enumeration value="COP"/>
 *     &lt;enumeration value="COR"/>
 *     &lt;enumeration value="COS"/>
 *     &lt;enumeration value="CPE"/>
 *     &lt;enumeration value="CPF"/>
 *     &lt;enumeration value="CPP"/>
 *     &lt;enumeration value="CRE"/>
 *     &lt;enumeration value="CRH"/>
 *     &lt;enumeration value="CRP"/>
 *     &lt;enumeration value="CSB"/>
 *     &lt;enumeration value="CUS"/>
 *     &lt;enumeration value="CYM"/>
 *     &lt;enumeration value="CZE"/>
 *     &lt;enumeration value="DAK"/>
 *     &lt;enumeration value="DAN"/>
 *     &lt;enumeration value="DAR"/>
 *     &lt;enumeration value="DAY"/>
 *     &lt;enumeration value="DEL"/>
 *     &lt;enumeration value="DEN"/>
 *     &lt;enumeration value="DEU"/>
 *     &lt;enumeration value="DGR"/>
 *     &lt;enumeration value="DIN"/>
 *     &lt;enumeration value="DIV"/>
 *     &lt;enumeration value="DOI"/>
 *     &lt;enumeration value="DRA"/>
 *     &lt;enumeration value="DSB"/>
 *     &lt;enumeration value="DUA"/>
 *     &lt;enumeration value="DUM"/>
 *     &lt;enumeration value="DUT"/>
 *     &lt;enumeration value="DYU"/>
 *     &lt;enumeration value="DZO"/>
 *     &lt;enumeration value="EFI"/>
 *     &lt;enumeration value="EGY"/>
 *     &lt;enumeration value="EKA"/>
 *     &lt;enumeration value="ELL"/>
 *     &lt;enumeration value="ELX"/>
 *     &lt;enumeration value="ENG"/>
 *     &lt;enumeration value="ENM"/>
 *     &lt;enumeration value="EPO"/>
 *     &lt;enumeration value="EST"/>
 *     &lt;enumeration value="EUS"/>
 *     &lt;enumeration value="EWE"/>
 *     &lt;enumeration value="EWO"/>
 *     &lt;enumeration value="FAN"/>
 *     &lt;enumeration value="FAO"/>
 *     &lt;enumeration value="FAS"/>
 *     &lt;enumeration value="FAT"/>
 *     &lt;enumeration value="FIJ"/>
 *     &lt;enumeration value="FIL"/>
 *     &lt;enumeration value="FIN"/>
 *     &lt;enumeration value="FIU"/>
 *     &lt;enumeration value="FON"/>
 *     &lt;enumeration value="FRA"/>
 *     &lt;enumeration value="FRE"/>
 *     &lt;enumeration value="FRM"/>
 *     &lt;enumeration value="FRO"/>
 *     &lt;enumeration value="FRR"/>
 *     &lt;enumeration value="FRS"/>
 *     &lt;enumeration value="FRY"/>
 *     &lt;enumeration value="FUL"/>
 *     &lt;enumeration value="FUR"/>
 *     &lt;enumeration value="GAA"/>
 *     &lt;enumeration value="GAY"/>
 *     &lt;enumeration value="GBA"/>
 *     &lt;enumeration value="GEM"/>
 *     &lt;enumeration value="GEO"/>
 *     &lt;enumeration value="GER"/>
 *     &lt;enumeration value="GEZ"/>
 *     &lt;enumeration value="GIL"/>
 *     &lt;enumeration value="GLA"/>
 *     &lt;enumeration value="GLE"/>
 *     &lt;enumeration value="GLG"/>
 *     &lt;enumeration value="GLV"/>
 *     &lt;enumeration value="GMH"/>
 *     &lt;enumeration value="GOH"/>
 *     &lt;enumeration value="GON"/>
 *     &lt;enumeration value="GOR"/>
 *     &lt;enumeration value="GOT"/>
 *     &lt;enumeration value="GRB"/>
 *     &lt;enumeration value="GRC"/>
 *     &lt;enumeration value="GRE"/>
 *     &lt;enumeration value="GRN"/>
 *     &lt;enumeration value="GSW"/>
 *     &lt;enumeration value="GUJ"/>
 *     &lt;enumeration value="GWI"/>
 *     &lt;enumeration value="HAI"/>
 *     &lt;enumeration value="HAT"/>
 *     &lt;enumeration value="HAU"/>
 *     &lt;enumeration value="HAW"/>
 *     &lt;enumeration value="HEB"/>
 *     &lt;enumeration value="HER"/>
 *     &lt;enumeration value="HIL"/>
 *     &lt;enumeration value="HIM"/>
 *     &lt;enumeration value="HIN"/>
 *     &lt;enumeration value="HIT"/>
 *     &lt;enumeration value="HMN"/>
 *     &lt;enumeration value="HMO"/>
 *     &lt;enumeration value="HRV"/>
 *     &lt;enumeration value="HSB"/>
 *     &lt;enumeration value="HUN"/>
 *     &lt;enumeration value="HUP"/>
 *     &lt;enumeration value="HYE"/>
 *     &lt;enumeration value="IBA"/>
 *     &lt;enumeration value="IBO"/>
 *     &lt;enumeration value="ICE"/>
 *     &lt;enumeration value="IDO"/>
 *     &lt;enumeration value="III"/>
 *     &lt;enumeration value="IJO"/>
 *     &lt;enumeration value="IKU"/>
 *     &lt;enumeration value="ILE"/>
 *     &lt;enumeration value="ILO"/>
 *     &lt;enumeration value="INA"/>
 *     &lt;enumeration value="INC"/>
 *     &lt;enumeration value="IND"/>
 *     &lt;enumeration value="INE"/>
 *     &lt;enumeration value="INH"/>
 *     &lt;enumeration value="IPK"/>
 *     &lt;enumeration value="IRA"/>
 *     &lt;enumeration value="IRO"/>
 *     &lt;enumeration value="ISL"/>
 *     &lt;enumeration value="ITA"/>
 *     &lt;enumeration value="JAV"/>
 *     &lt;enumeration value="JBO"/>
 *     &lt;enumeration value="JPN"/>
 *     &lt;enumeration value="JPR"/>
 *     &lt;enumeration value="JRB"/>
 *     &lt;enumeration value="KAA"/>
 *     &lt;enumeration value="KAB"/>
 *     &lt;enumeration value="KAC"/>
 *     &lt;enumeration value="KAL"/>
 *     &lt;enumeration value="KAM"/>
 *     &lt;enumeration value="KAN"/>
 *     &lt;enumeration value="KAR"/>
 *     &lt;enumeration value="KAS"/>
 *     &lt;enumeration value="KAT"/>
 *     &lt;enumeration value="KAU"/>
 *     &lt;enumeration value="KAW"/>
 *     &lt;enumeration value="KAZ"/>
 *     &lt;enumeration value="KBD"/>
 *     &lt;enumeration value="KHA"/>
 *     &lt;enumeration value="KHI"/>
 *     &lt;enumeration value="KHM"/>
 *     &lt;enumeration value="KHO"/>
 *     &lt;enumeration value="KIK"/>
 *     &lt;enumeration value="KIN"/>
 *     &lt;enumeration value="KIR"/>
 *     &lt;enumeration value="KMB"/>
 *     &lt;enumeration value="KOK"/>
 *     &lt;enumeration value="KOM"/>
 *     &lt;enumeration value="KON"/>
 *     &lt;enumeration value="KOR"/>
 *     &lt;enumeration value="KOS"/>
 *     &lt;enumeration value="KPE"/>
 *     &lt;enumeration value="KRC"/>
 *     &lt;enumeration value="KRL"/>
 *     &lt;enumeration value="KRO"/>
 *     &lt;enumeration value="KRU"/>
 *     &lt;enumeration value="KUA"/>
 *     &lt;enumeration value="KUM"/>
 *     &lt;enumeration value="KUR"/>
 *     &lt;enumeration value="KUT"/>
 *     &lt;enumeration value="LAD"/>
 *     &lt;enumeration value="LAH"/>
 *     &lt;enumeration value="LAM"/>
 *     &lt;enumeration value="LAO"/>
 *     &lt;enumeration value="LAT"/>
 *     &lt;enumeration value="LAV"/>
 *     &lt;enumeration value="LEZ"/>
 *     &lt;enumeration value="LIM"/>
 *     &lt;enumeration value="LIN"/>
 *     &lt;enumeration value="LIT"/>
 *     &lt;enumeration value="LOL"/>
 *     &lt;enumeration value="LOZ"/>
 *     &lt;enumeration value="LTZ"/>
 *     &lt;enumeration value="LUA"/>
 *     &lt;enumeration value="LUB"/>
 *     &lt;enumeration value="LUG"/>
 *     &lt;enumeration value="LUI"/>
 *     &lt;enumeration value="LUN"/>
 *     &lt;enumeration value="LUO"/>
 *     &lt;enumeration value="LUS"/>
 *     &lt;enumeration value="MAC"/>
 *     &lt;enumeration value="MAD"/>
 *     &lt;enumeration value="MAG"/>
 *     &lt;enumeration value="MAH"/>
 *     &lt;enumeration value="MAI"/>
 *     &lt;enumeration value="MAK"/>
 *     &lt;enumeration value="MAL"/>
 *     &lt;enumeration value="MAN"/>
 *     &lt;enumeration value="MAO"/>
 *     &lt;enumeration value="MAP"/>
 *     &lt;enumeration value="MAR"/>
 *     &lt;enumeration value="MAS"/>
 *     &lt;enumeration value="MAY"/>
 *     &lt;enumeration value="MDF"/>
 *     &lt;enumeration value="MDR"/>
 *     &lt;enumeration value="MEN"/>
 *     &lt;enumeration value="MGA"/>
 *     &lt;enumeration value="MIC"/>
 *     &lt;enumeration value="MIN"/>
 *     &lt;enumeration value="MKD"/>
 *     &lt;enumeration value="MKH"/>
 *     &lt;enumeration value="MLG"/>
 *     &lt;enumeration value="MLT"/>
 *     &lt;enumeration value="MNC"/>
 *     &lt;enumeration value="MNI"/>
 *     &lt;enumeration value="MNO"/>
 *     &lt;enumeration value="MOH"/>
 *     &lt;enumeration value="MOL"/>
 *     &lt;enumeration value="MON"/>
 *     &lt;enumeration value="MOS"/>
 *     &lt;enumeration value="MRI"/>
 *     &lt;enumeration value="MSA"/>
 *     &lt;enumeration value="MUN"/>
 *     &lt;enumeration value="MUS"/>
 *     &lt;enumeration value="MWL"/>
 *     &lt;enumeration value="MWR"/>
 *     &lt;enumeration value="MYA"/>
 *     &lt;enumeration value="MYN"/>
 *     &lt;enumeration value="MYV"/>
 *     &lt;enumeration value="NAH"/>
 *     &lt;enumeration value="NAI"/>
 *     &lt;enumeration value="NAP"/>
 *     &lt;enumeration value="NAU"/>
 *     &lt;enumeration value="NAV"/>
 *     &lt;enumeration value="NBL"/>
 *     &lt;enumeration value="NDE"/>
 *     &lt;enumeration value="NDO"/>
 *     &lt;enumeration value="NDS"/>
 *     &lt;enumeration value="NEP"/>
 *     &lt;enumeration value="NEW"/>
 *     &lt;enumeration value="NIA"/>
 *     &lt;enumeration value="NIC"/>
 *     &lt;enumeration value="NIU"/>
 *     &lt;enumeration value="NLD"/>
 *     &lt;enumeration value="NNO"/>
 *     &lt;enumeration value="NOB"/>
 *     &lt;enumeration value="NOG"/>
 *     &lt;enumeration value="NON"/>
 *     &lt;enumeration value="NOR"/>
 *     &lt;enumeration value="NQO"/>
 *     &lt;enumeration value="NSO"/>
 *     &lt;enumeration value="NUB"/>
 *     &lt;enumeration value="NWC"/>
 *     &lt;enumeration value="NYA"/>
 *     &lt;enumeration value="NYM"/>
 *     &lt;enumeration value="NYN"/>
 *     &lt;enumeration value="NYO"/>
 *     &lt;enumeration value="NZI"/>
 *     &lt;enumeration value="OCI"/>
 *     &lt;enumeration value="OJI"/>
 *     &lt;enumeration value="ORI"/>
 *     &lt;enumeration value="ORM"/>
 *     &lt;enumeration value="OSA"/>
 *     &lt;enumeration value="OSS"/>
 *     &lt;enumeration value="OTA"/>
 *     &lt;enumeration value="OTO"/>
 *     &lt;enumeration value="PAA"/>
 *     &lt;enumeration value="PAG"/>
 *     &lt;enumeration value="PAL"/>
 *     &lt;enumeration value="PAM"/>
 *     &lt;enumeration value="PAN"/>
 *     &lt;enumeration value="PAP"/>
 *     &lt;enumeration value="PAU"/>
 *     &lt;enumeration value="PEO"/>
 *     &lt;enumeration value="PER"/>
 *     &lt;enumeration value="PHI"/>
 *     &lt;enumeration value="PHN"/>
 *     &lt;enumeration value="PLI"/>
 *     &lt;enumeration value="POL"/>
 *     &lt;enumeration value="PON"/>
 *     &lt;enumeration value="POR"/>
 *     &lt;enumeration value="PRA"/>
 *     &lt;enumeration value="PRO"/>
 *     &lt;enumeration value="PUS"/>
 *     &lt;enumeration value="QUE"/>
 *     &lt;enumeration value="RAJ"/>
 *     &lt;enumeration value="RAP"/>
 *     &lt;enumeration value="RAR"/>
 *     &lt;enumeration value="ROA"/>
 *     &lt;enumeration value="ROH"/>
 *     &lt;enumeration value="ROM"/>
 *     &lt;enumeration value="RON"/>
 *     &lt;enumeration value="RUM"/>
 *     &lt;enumeration value="RUN"/>
 *     &lt;enumeration value="RUP"/>
 *     &lt;enumeration value="RUS"/>
 *     &lt;enumeration value="SAD"/>
 *     &lt;enumeration value="SAG"/>
 *     &lt;enumeration value="SAH"/>
 *     &lt;enumeration value="SAI"/>
 *     &lt;enumeration value="SAL"/>
 *     &lt;enumeration value="SAM"/>
 *     &lt;enumeration value="SAN"/>
 *     &lt;enumeration value="SAS"/>
 *     &lt;enumeration value="SAT"/>
 *     &lt;enumeration value="SCC"/>
 *     &lt;enumeration value="SCN"/>
 *     &lt;enumeration value="SCO"/>
 *     &lt;enumeration value="SCR"/>
 *     &lt;enumeration value="SEL"/>
 *     &lt;enumeration value="SEM"/>
 *     &lt;enumeration value="SGA"/>
 *     &lt;enumeration value="SGN"/>
 *     &lt;enumeration value="SHN"/>
 *     &lt;enumeration value="SID"/>
 *     &lt;enumeration value="SIN"/>
 *     &lt;enumeration value="SIO"/>
 *     &lt;enumeration value="SIT"/>
 *     &lt;enumeration value="SLA"/>
 *     &lt;enumeration value="SLK"/>
 *     &lt;enumeration value="SLO"/>
 *     &lt;enumeration value="SLV"/>
 *     &lt;enumeration value="SMA"/>
 *     &lt;enumeration value="SME"/>
 *     &lt;enumeration value="SMI"/>
 *     &lt;enumeration value="SMJ"/>
 *     &lt;enumeration value="SMN"/>
 *     &lt;enumeration value="SMO"/>
 *     &lt;enumeration value="SMS"/>
 *     &lt;enumeration value="SNA"/>
 *     &lt;enumeration value="SND"/>
 *     &lt;enumeration value="SNK"/>
 *     &lt;enumeration value="SOG"/>
 *     &lt;enumeration value="SOM"/>
 *     &lt;enumeration value="SON"/>
 *     &lt;enumeration value="SOT"/>
 *     &lt;enumeration value="SPA"/>
 *     &lt;enumeration value="SQI"/>
 *     &lt;enumeration value="SRD"/>
 *     &lt;enumeration value="SRN"/>
 *     &lt;enumeration value="SRP"/>
 *     &lt;enumeration value="SRR"/>
 *     &lt;enumeration value="SSA"/>
 *     &lt;enumeration value="SSW"/>
 *     &lt;enumeration value="SUK"/>
 *     &lt;enumeration value="SUN"/>
 *     &lt;enumeration value="SUS"/>
 *     &lt;enumeration value="SUX"/>
 *     &lt;enumeration value="SWA"/>
 *     &lt;enumeration value="SWE"/>
 *     &lt;enumeration value="SYR"/>
 *     &lt;enumeration value="TAH"/>
 *     &lt;enumeration value="TAI"/>
 *     &lt;enumeration value="TAM"/>
 *     &lt;enumeration value="TAT"/>
 *     &lt;enumeration value="TEL"/>
 *     &lt;enumeration value="TEM"/>
 *     &lt;enumeration value="TER"/>
 *     &lt;enumeration value="TET"/>
 *     &lt;enumeration value="TGK"/>
 *     &lt;enumeration value="TGL"/>
 *     &lt;enumeration value="THA"/>
 *     &lt;enumeration value="TIB"/>
 *     &lt;enumeration value="TIG"/>
 *     &lt;enumeration value="TIR"/>
 *     &lt;enumeration value="TIV"/>
 *     &lt;enumeration value="TKL"/>
 *     &lt;enumeration value="TLH"/>
 *     &lt;enumeration value="TLI"/>
 *     &lt;enumeration value="TMH"/>
 *     &lt;enumeration value="TOG"/>
 *     &lt;enumeration value="TON"/>
 *     &lt;enumeration value="TPI"/>
 *     &lt;enumeration value="TSI"/>
 *     &lt;enumeration value="TSN"/>
 *     &lt;enumeration value="TSO"/>
 *     &lt;enumeration value="TUK"/>
 *     &lt;enumeration value="TUM"/>
 *     &lt;enumeration value="TUP"/>
 *     &lt;enumeration value="TUR"/>
 *     &lt;enumeration value="TUT"/>
 *     &lt;enumeration value="TVL"/>
 *     &lt;enumeration value="TWI"/>
 *     &lt;enumeration value="TYV"/>
 *     &lt;enumeration value="UDM"/>
 *     &lt;enumeration value="UGA"/>
 *     &lt;enumeration value="UIG"/>
 *     &lt;enumeration value="UKR"/>
 *     &lt;enumeration value="UMB"/>
 *     &lt;enumeration value="URD"/>
 *     &lt;enumeration value="UZB"/>
 *     &lt;enumeration value="VAI"/>
 *     &lt;enumeration value="VEN"/>
 *     &lt;enumeration value="VIE"/>
 *     &lt;enumeration value="VOL"/>
 *     &lt;enumeration value="VOT"/>
 *     &lt;enumeration value="WAK"/>
 *     &lt;enumeration value="WAL"/>
 *     &lt;enumeration value="WAR"/>
 *     &lt;enumeration value="WAS"/>
 *     &lt;enumeration value="WEL"/>
 *     &lt;enumeration value="WEN"/>
 *     &lt;enumeration value="WLN"/>
 *     &lt;enumeration value="WOL"/>
 *     &lt;enumeration value="XAL"/>
 *     &lt;enumeration value="XHO"/>
 *     &lt;enumeration value="YAO"/>
 *     &lt;enumeration value="YAP"/>
 *     &lt;enumeration value="YID"/>
 *     &lt;enumeration value="YOR"/>
 *     &lt;enumeration value="YPK"/>
 *     &lt;enumeration value="ZAP"/>
 *     &lt;enumeration value="ZEN"/>
 *     &lt;enumeration value="ZHA"/>
 *     &lt;enumeration value="ZHO"/>
 *     &lt;enumeration value="ZND"/>
 *     &lt;enumeration value="ZUL"/>
 *     &lt;enumeration value="ZUN"/>
 *     &lt;enumeration value="ZZA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeISOLanguageCodeA3")
@XmlEnum
public enum TypeISOLanguageCodeA3 {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    AAR("AAR"),
    ABK("ABK"),
    ACE("ACE"),
    ACH("ACH"),
    ADA("ADA"),
    ADY("ADY"),
    AFA("AFA"),
    AFH("AFH"),
    AFR("AFR"),
    AIN("AIN"),
    AKA("AKA"),
    AKK("AKK"),
    ALB("ALB"),
    ALE("ALE"),
    ALG("ALG"),
    ALT("ALT"),
    AMH("AMH"),
    ANG("ANG"),
    ANP("ANP"),
    APA("APA"),
    ARA("ARA"),
    ARC("ARC"),
    ARG("ARG"),
    ARM("ARM"),
    ARN("ARN"),
    ARP("ARP"),
    ARW("ARW"),
    ASM("ASM"),
    AST("AST"),
    ATH("ATH"),
    AUS("AUS"),
    AVA("AVA"),
    AVE("AVE"),
    AWA("AWA"),
    AYM("AYM"),
    AZE("AZE"),
    BAD("BAD"),
    BAI("BAI"),
    BAK("BAK"),
    BAL("BAL"),
    BAM("BAM"),
    BAN("BAN"),
    BAQ("BAQ"),
    BAS("BAS"),
    BAT("BAT"),
    BEJ("BEJ"),
    BEL("BEL"),
    BEM("BEM"),
    BEN("BEN"),
    BER("BER"),
    BHO("BHO"),
    BIH("BIH"),
    BIK("BIK"),
    BIN("BIN"),
    BIS("BIS"),
    BLA("BLA"),
    BNT("BNT"),
    BOD("BOD"),
    BOS("BOS"),
    BRA("BRA"),
    BRE("BRE"),
    BTK("BTK"),
    BUA("BUA"),
    BUG("BUG"),
    BUL("BUL"),
    BUR("BUR"),
    BYN("BYN"),
    CAD("CAD"),
    CAI("CAI"),
    CAR("CAR"),
    CAT("CAT"),
    CAU("CAU"),
    CEB("CEB"),
    CEL("CEL"),
    CES("CES"),
    CHA("CHA"),
    CHB("CHB"),
    CHE("CHE"),
    CHG("CHG"),
    CHI("CHI"),
    CHK("CHK"),
    CHM("CHM"),
    CHN("CHN"),
    CHO("CHO"),
    CHP("CHP"),
    CHR("CHR"),
    CHU("CHU"),
    CHV("CHV"),
    CHY("CHY"),
    CMC("CMC"),
    COP("COP"),
    COR("COR"),
    COS("COS"),
    CPE("CPE"),
    CPF("CPF"),
    CPP("CPP"),
    CRE("CRE"),
    CRH("CRH"),
    CRP("CRP"),
    CSB("CSB"),
    CUS("CUS"),
    CYM("CYM"),
    CZE("CZE"),
    DAK("DAK"),
    DAN("DAN"),
    DAR("DAR"),
    DAY("DAY"),
    DEL("DEL"),
    DEN("DEN"),
    DEU("DEU"),
    DGR("DGR"),
    DIN("DIN"),
    DIV("DIV"),
    DOI("DOI"),
    DRA("DRA"),
    DSB("DSB"),
    DUA("DUA"),
    DUM("DUM"),
    DUT("DUT"),
    DYU("DYU"),
    DZO("DZO"),
    EFI("EFI"),
    EGY("EGY"),
    EKA("EKA"),
    ELL("ELL"),
    ELX("ELX"),
    ENG("ENG"),
    ENM("ENM"),
    EPO("EPO"),
    EST("EST"),
    EUS("EUS"),
    EWE("EWE"),
    EWO("EWO"),
    FAN("FAN"),
    FAO("FAO"),
    FAS("FAS"),
    FAT("FAT"),
    FIJ("FIJ"),
    FIL("FIL"),
    FIN("FIN"),
    FIU("FIU"),
    FON("FON"),
    FRA("FRA"),
    FRE("FRE"),
    FRM("FRM"),
    FRO("FRO"),
    FRR("FRR"),
    FRS("FRS"),
    FRY("FRY"),
    FUL("FUL"),
    FUR("FUR"),
    GAA("GAA"),
    GAY("GAY"),
    GBA("GBA"),
    GEM("GEM"),
    GEO("GEO"),
    GER("GER"),
    GEZ("GEZ"),
    GIL("GIL"),
    GLA("GLA"),
    GLE("GLE"),
    GLG("GLG"),
    GLV("GLV"),
    GMH("GMH"),
    GOH("GOH"),
    GON("GON"),
    GOR("GOR"),
    GOT("GOT"),
    GRB("GRB"),
    GRC("GRC"),
    GRE("GRE"),
    GRN("GRN"),
    GSW("GSW"),
    GUJ("GUJ"),
    GWI("GWI"),
    HAI("HAI"),
    HAT("HAT"),
    HAU("HAU"),
    HAW("HAW"),
    HEB("HEB"),
    HER("HER"),
    HIL("HIL"),
    HIM("HIM"),
    HIN("HIN"),
    HIT("HIT"),
    HMN("HMN"),
    HMO("HMO"),
    HRV("HRV"),
    HSB("HSB"),
    HUN("HUN"),
    HUP("HUP"),
    HYE("HYE"),
    IBA("IBA"),
    IBO("IBO"),
    ICE("ICE"),
    IDO("IDO"),
    III("III"),
    IJO("IJO"),
    IKU("IKU"),
    ILE("ILE"),
    ILO("ILO"),
    INA("INA"),
    INC("INC"),
    IND("IND"),
    INE("INE"),
    INH("INH"),
    IPK("IPK"),
    IRA("IRA"),
    IRO("IRO"),
    ISL("ISL"),
    ITA("ITA"),
    JAV("JAV"),
    JBO("JBO"),
    JPN("JPN"),
    JPR("JPR"),
    JRB("JRB"),
    KAA("KAA"),
    KAB("KAB"),
    KAC("KAC"),
    KAL("KAL"),
    KAM("KAM"),
    KAN("KAN"),
    KAR("KAR"),
    KAS("KAS"),
    KAT("KAT"),
    KAU("KAU"),
    KAW("KAW"),
    KAZ("KAZ"),
    KBD("KBD"),
    KHA("KHA"),
    KHI("KHI"),
    KHM("KHM"),
    KHO("KHO"),
    KIK("KIK"),
    KIN("KIN"),
    KIR("KIR"),
    KMB("KMB"),
    KOK("KOK"),
    KOM("KOM"),
    KON("KON"),
    KOR("KOR"),
    KOS("KOS"),
    KPE("KPE"),
    KRC("KRC"),
    KRL("KRL"),
    KRO("KRO"),
    KRU("KRU"),
    KUA("KUA"),
    KUM("KUM"),
    KUR("KUR"),
    KUT("KUT"),
    LAD("LAD"),
    LAH("LAH"),
    LAM("LAM"),
    LAO("LAO"),
    LAT("LAT"),
    LAV("LAV"),
    LEZ("LEZ"),
    LIM("LIM"),
    LIN("LIN"),
    LIT("LIT"),
    LOL("LOL"),
    LOZ("LOZ"),
    LTZ("LTZ"),
    LUA("LUA"),
    LUB("LUB"),
    LUG("LUG"),
    LUI("LUI"),
    LUN("LUN"),
    LUO("LUO"),
    LUS("LUS"),
    MAC("MAC"),
    MAD("MAD"),
    MAG("MAG"),
    MAH("MAH"),
    MAI("MAI"),
    MAK("MAK"),
    MAL("MAL"),
    MAN("MAN"),
    MAO("MAO"),
    MAP("MAP"),
    MAR("MAR"),
    MAS("MAS"),
    MAY("MAY"),
    MDF("MDF"),
    MDR("MDR"),
    MEN("MEN"),
    MGA("MGA"),
    MIC("MIC"),
    MIN("MIN"),
    MKD("MKD"),
    MKH("MKH"),
    MLG("MLG"),
    MLT("MLT"),
    MNC("MNC"),
    MNI("MNI"),
    MNO("MNO"),
    MOH("MOH"),
    MOL("MOL"),
    MON("MON"),
    MOS("MOS"),
    MRI("MRI"),
    MSA("MSA"),
    MUN("MUN"),
    MUS("MUS"),
    MWL("MWL"),
    MWR("MWR"),
    MYA("MYA"),
    MYN("MYN"),
    MYV("MYV"),
    NAH("NAH"),
    NAI("NAI"),
    NAP("NAP"),
    NAU("NAU"),
    NAV("NAV"),
    NBL("NBL"),
    NDE("NDE"),
    NDO("NDO"),
    NDS("NDS"),
    NEP("NEP"),
    NEW("NEW"),
    NIA("NIA"),
    NIC("NIC"),
    NIU("NIU"),
    NLD("NLD"),
    NNO("NNO"),
    NOB("NOB"),
    NOG("NOG"),
    NON("NON"),
    NOR("NOR"),
    NQO("NQO"),
    NSO("NSO"),
    NUB("NUB"),
    NWC("NWC"),
    NYA("NYA"),
    NYM("NYM"),
    NYN("NYN"),
    NYO("NYO"),
    NZI("NZI"),
    OCI("OCI"),
    OJI("OJI"),
    ORI("ORI"),
    ORM("ORM"),
    OSA("OSA"),
    OSS("OSS"),
    OTA("OTA"),
    OTO("OTO"),
    PAA("PAA"),
    PAG("PAG"),
    PAL("PAL"),
    PAM("PAM"),
    PAN("PAN"),
    PAP("PAP"),
    PAU("PAU"),
    PEO("PEO"),
    PER("PER"),
    PHI("PHI"),
    PHN("PHN"),
    PLI("PLI"),
    POL("POL"),
    PON("PON"),
    POR("POR"),
    PRA("PRA"),
    PRO("PRO"),
    PUS("PUS"),
    QUE("QUE"),
    RAJ("RAJ"),
    RAP("RAP"),
    RAR("RAR"),
    ROA("ROA"),
    ROH("ROH"),
    ROM("ROM"),
    RON("RON"),
    RUM("RUM"),
    RUN("RUN"),
    RUP("RUP"),
    RUS("RUS"),
    SAD("SAD"),
    SAG("SAG"),
    SAH("SAH"),
    SAI("SAI"),
    SAL("SAL"),
    SAM("SAM"),
    SAN("SAN"),
    SAS("SAS"),
    SAT("SAT"),
    SCC("SCC"),
    SCN("SCN"),
    SCO("SCO"),
    SCR("SCR"),
    SEL("SEL"),
    SEM("SEM"),
    SGA("SGA"),
    SGN("SGN"),
    SHN("SHN"),
    SID("SID"),
    SIN("SIN"),
    SIO("SIO"),
    SIT("SIT"),
    SLA("SLA"),
    SLK("SLK"),
    SLO("SLO"),
    SLV("SLV"),
    SMA("SMA"),
    SME("SME"),
    SMI("SMI"),
    SMJ("SMJ"),
    SMN("SMN"),
    SMO("SMO"),
    SMS("SMS"),
    SNA("SNA"),
    SND("SND"),
    SNK("SNK"),
    SOG("SOG"),
    SOM("SOM"),
    SON("SON"),
    SOT("SOT"),
    SPA("SPA"),
    SQI("SQI"),
    SRD("SRD"),
    SRN("SRN"),
    SRP("SRP"),
    SRR("SRR"),
    SSA("SSA"),
    SSW("SSW"),
    SUK("SUK"),
    SUN("SUN"),
    SUS("SUS"),
    SUX("SUX"),
    SWA("SWA"),
    SWE("SWE"),
    SYR("SYR"),
    TAH("TAH"),
    TAI("TAI"),
    TAM("TAM"),
    TAT("TAT"),
    TEL("TEL"),
    TEM("TEM"),
    TER("TER"),
    TET("TET"),
    TGK("TGK"),
    TGL("TGL"),
    THA("THA"),
    TIB("TIB"),
    TIG("TIG"),
    TIR("TIR"),
    TIV("TIV"),
    TKL("TKL"),
    TLH("TLH"),
    TLI("TLI"),
    TMH("TMH"),
    TOG("TOG"),
    TON("TON"),
    TPI("TPI"),
    TSI("TSI"),
    TSN("TSN"),
    TSO("TSO"),
    TUK("TUK"),
    TUM("TUM"),
    TUP("TUP"),
    TUR("TUR"),
    TUT("TUT"),
    TVL("TVL"),
    TWI("TWI"),
    TYV("TYV"),
    UDM("UDM"),
    UGA("UGA"),
    UIG("UIG"),
    UKR("UKR"),
    UMB("UMB"),
    URD("URD"),
    UZB("UZB"),
    VAI("VAI"),
    VEN("VEN"),
    VIE("VIE"),
    VOL("VOL"),
    VOT("VOT"),
    WAK("WAK"),
    WAL("WAL"),
    WAR("WAR"),
    WAS("WAS"),
    WEL("WEL"),
    WEN("WEN"),
    WLN("WLN"),
    WOL("WOL"),
    XAL("XAL"),
    XHO("XHO"),
    YAO("YAO"),
    YAP("YAP"),
    YID("YID"),
    YOR("YOR"),
    YPK("YPK"),
    ZAP("ZAP"),
    ZEN("ZEN"),
    ZHA("ZHA"),
    ZHO("ZHO"),
    ZND("ZND"),
    ZUL("ZUL"),
    ZUN("ZUN"),
    ZZA("ZZA");
    private final String value;

    TypeISOLanguageCodeA3(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeISOLanguageCodeA3 fromValue(String v) {
        for (TypeISOLanguageCodeA3 c: TypeISOLanguageCodeA3 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
