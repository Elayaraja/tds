
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Tenders complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tenders">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Credit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PINDebit" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="PINDebitReturnSupportType" type="{http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation}PINDebitReturnSupportType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tenders", propOrder = {
    "credit",
    "pinDebit",
    "pinDebitReturnSupportType"
})
public class Tenders {

    @XmlElement(name = "Credit")
    protected Boolean credit;
    @XmlElement(name = "PINDebit")
    protected Boolean pinDebit;
    @XmlElement(name = "PINDebitReturnSupportType")
    protected PINDebitReturnSupportType pinDebitReturnSupportType;

    /**
     * Gets the value of the credit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCredit() {
        return credit;
    }

    /**
     * Sets the value of the credit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCredit(Boolean value) {
        this.credit = value;
    }

    /**
     * Gets the value of the pinDebit property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPINDebit() {
        return pinDebit;
    }

    /**
     * Sets the value of the pinDebit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPINDebit(Boolean value) {
        this.pinDebit = value;
    }

    /**
     * Gets the value of the pinDebitReturnSupportType property.
     * 
     * @return
     *     possible object is
     *     {@link PINDebitReturnSupportType }
     *     
     */
    public PINDebitReturnSupportType getPINDebitReturnSupportType() {
        return pinDebitReturnSupportType;
    }

    /**
     * Sets the value of the pinDebitReturnSupportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link PINDebitReturnSupportType }
     *     
     */
    public void setPINDebitReturnSupportType(PINDebitReturnSupportType value) {
        this.pinDebitReturnSupportType = value;
    }

}
