
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerPresent.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerPresent">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="Present"/>
 *     &lt;enumeration value="Suspicious"/>
 *     &lt;enumeration value="BillPayment"/>
 *     &lt;enumeration value="Transponder"/>
 *     &lt;enumeration value="MOTO"/>
 *     &lt;enumeration value="VisaOpenNetworkTransaction"/>
 *     &lt;enumeration value="VisaCardPresentStripeUnreadable"/>
 *     &lt;enumeration value="MailFax"/>
 *     &lt;enumeration value="Ecommerce"/>
 *     &lt;enumeration value="TelARU"/>
 *     &lt;enumeration value="MOTOCC"/>
 *     &lt;enumeration value="VoiceResponse"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CustomerPresent")
@XmlEnum
public enum CustomerPresent {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("Present")
    PRESENT("Present"),
    @XmlEnumValue("Suspicious")
    SUSPICIOUS("Suspicious"),
    @XmlEnumValue("BillPayment")
    BILL_PAYMENT("BillPayment"),
    @XmlEnumValue("Transponder")
    TRANSPONDER("Transponder"),
    MOTO("MOTO"),
    @XmlEnumValue("VisaOpenNetworkTransaction")
    VISA_OPEN_NETWORK_TRANSACTION("VisaOpenNetworkTransaction"),
    @XmlEnumValue("VisaCardPresentStripeUnreadable")
    VISA_CARD_PRESENT_STRIPE_UNREADABLE("VisaCardPresentStripeUnreadable"),
    @XmlEnumValue("MailFax")
    MAIL_FAX("MailFax"),
    @XmlEnumValue("Ecommerce")
    ECOMMERCE("Ecommerce"),
    @XmlEnumValue("TelARU")
    TEL_ARU("TelARU"),
    MOTOCC("MOTOCC"),
    @XmlEnumValue("VoiceResponse")
    VOICE_RESPONSE("VoiceResponse");
    private final String value;

    CustomerPresent(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomerPresent fromValue(String v) {
        for (CustomerPresent c: CustomerPresent.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
