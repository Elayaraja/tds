
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServiceInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BankcardServices" type="{http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation}ArrayOfBankcardService" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceInformation", propOrder = {
    "bankcardServices"
})
public class ServiceInformation {

    @XmlElement(name = "BankcardServices", nillable = true)
    protected ArrayOfBankcardService bankcardServices;

    /**
     * Gets the value of the bankcardServices property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfBankcardService }
     *     
     */
    public ArrayOfBankcardService getBankcardServices() {
        return bankcardServices;
    }

    /**
     * Sets the value of the bankcardServices property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfBankcardService }
     *     
     */
    public void setBankcardServices(ArrayOfBankcardService value) {
        this.bankcardServices = value;
    }

}
