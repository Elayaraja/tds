
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReadCapability.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReadCapability">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="HasMSR"/>
 *     &lt;enumeration value="NoMSR"/>
 *     &lt;enumeration value="KeyOnly"/>
 *     &lt;enumeration value="Chip"/>
 *     &lt;enumeration value="ContactlessChip"/>
 *     &lt;enumeration value="ContactlessMSR"/>
 *     &lt;enumeration value="ECR"/>
 *     &lt;enumeration value="VSCCapable"/>
 *     &lt;enumeration value="RFIDCapable"/>
 *     &lt;enumeration value="EmvICC"/>
 *     &lt;enumeration value="MSREMVICC"/>
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="OCRReader"/>
 *     &lt;enumeration value="BarCodeReader"/>
 *     &lt;enumeration value="NotSpecified"/>
 *     &lt;enumeration value="ARUIVR"/>
 *     &lt;enumeration value="NoTerminal"/>
 *     &lt;enumeration value="NFCCapable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ReadCapability")
@XmlEnum
public enum ReadCapability {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("HasMSR")
    HAS_MSR("HasMSR"),
    @XmlEnumValue("NoMSR")
    NO_MSR("NoMSR"),
    @XmlEnumValue("KeyOnly")
    KEY_ONLY("KeyOnly"),
    @XmlEnumValue("Chip")
    CHIP("Chip"),
    @XmlEnumValue("ContactlessChip")
    CONTACTLESS_CHIP("ContactlessChip"),
    @XmlEnumValue("ContactlessMSR")
    CONTACTLESS_MSR("ContactlessMSR"),
    ECR("ECR"),
    @XmlEnumValue("VSCCapable")
    VSC_CAPABLE("VSCCapable"),
    @XmlEnumValue("RFIDCapable")
    RFID_CAPABLE("RFIDCapable"),
    @XmlEnumValue("EmvICC")
    EMV_ICC("EmvICC"),
    MSREMVICC("MSREMVICC"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    @XmlEnumValue("OCRReader")
    OCR_READER("OCRReader"),
    @XmlEnumValue("BarCodeReader")
    BAR_CODE_READER("BarCodeReader"),
    @XmlEnumValue("NotSpecified")
    NOT_SPECIFIED("NotSpecified"),
    ARUIVR("ARUIVR"),
    @XmlEnumValue("NoTerminal")
    NO_TERMINAL("NoTerminal"),
    @XmlEnumValue("NFCCapable")
    NFC_CAPABLE("NFCCapable");
    private final String value;

    ReadCapability(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ReadCapability fromValue(String v) {
        for (ReadCapability c: ReadCapability.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
