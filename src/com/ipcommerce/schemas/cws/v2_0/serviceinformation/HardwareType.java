
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HardwareType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HardwareType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="PC"/>
 *     &lt;enumeration value="DialTerminal"/>
 *     &lt;enumeration value="ElectronicCashRegister"/>
 *     &lt;enumeration value="InStoreController"/>
 *     &lt;enumeration value="Mainframe"/>
 *     &lt;enumeration value="ThirdPartyDeveloper"/>
 *     &lt;enumeration value="POSPort"/>
 *     &lt;enumeration value="POSPartner"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "HardwareType")
@XmlEnum
public enum HardwareType {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    PC("PC"),
    @XmlEnumValue("DialTerminal")
    DIAL_TERMINAL("DialTerminal"),
    @XmlEnumValue("ElectronicCashRegister")
    ELECTRONIC_CASH_REGISTER("ElectronicCashRegister"),
    @XmlEnumValue("InStoreController")
    IN_STORE_CONTROLLER("InStoreController"),
    @XmlEnumValue("Mainframe")
    MAINFRAME("Mainframe"),
    @XmlEnumValue("ThirdPartyDeveloper")
    THIRD_PARTY_DEVELOPER("ThirdPartyDeveloper"),
    @XmlEnumValue("POSPort")
    POS_PORT("POSPort"),
    @XmlEnumValue("POSPartner")
    POS_PARTNER("POSPartner");
    private final String value;

    HardwareType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static HardwareType fromValue(String v) {
        for (HardwareType c: HardwareType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
