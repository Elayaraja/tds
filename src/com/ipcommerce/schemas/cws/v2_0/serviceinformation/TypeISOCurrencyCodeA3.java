
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeISOCurrencyCodeA3.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeISOCurrencyCodeA3">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="AFN"/>
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="DZD"/>
 *     &lt;enumeration value="USD"/>
 *     &lt;enumeration value="EUR"/>
 *     &lt;enumeration value="AOA"/>
 *     &lt;enumeration value="XCD"/>
 *     &lt;enumeration value="ARS"/>
 *     &lt;enumeration value="AMD"/>
 *     &lt;enumeration value="AWG"/>
 *     &lt;enumeration value="AUD"/>
 *     &lt;enumeration value="AZN"/>
 *     &lt;enumeration value="BSD"/>
 *     &lt;enumeration value="BHD"/>
 *     &lt;enumeration value="BDT"/>
 *     &lt;enumeration value="BBD"/>
 *     &lt;enumeration value="BYR"/>
 *     &lt;enumeration value="BZD"/>
 *     &lt;enumeration value="BMD"/>
 *     &lt;enumeration value="BTN"/>
 *     &lt;enumeration value="BOB"/>
 *     &lt;enumeration value="BOV"/>
 *     &lt;enumeration value="BAM"/>
 *     &lt;enumeration value="BWP"/>
 *     &lt;enumeration value="BRL"/>
 *     &lt;enumeration value="BND"/>
 *     &lt;enumeration value="BGN"/>
 *     &lt;enumeration value="XOF"/>
 *     &lt;enumeration value="BIF"/>
 *     &lt;enumeration value="KHR"/>
 *     &lt;enumeration value="CAD"/>
 *     &lt;enumeration value="CVE"/>
 *     &lt;enumeration value="KYD"/>
 *     &lt;enumeration value="CLP"/>
 *     &lt;enumeration value="CLF"/>
 *     &lt;enumeration value="CNY"/>
 *     &lt;enumeration value="COP"/>
 *     &lt;enumeration value="COU"/>
 *     &lt;enumeration value="KMF"/>
 *     &lt;enumeration value="XAF"/>
 *     &lt;enumeration value="CDF"/>
 *     &lt;enumeration value="CRC"/>
 *     &lt;enumeration value="HRK"/>
 *     &lt;enumeration value="CUP"/>
 *     &lt;enumeration value="CYP"/>
 *     &lt;enumeration value="CZK"/>
 *     &lt;enumeration value="DKK"/>
 *     &lt;enumeration value="DJF"/>
 *     &lt;enumeration value="DOP"/>
 *     &lt;enumeration value="EGP"/>
 *     &lt;enumeration value="SVC"/>
 *     &lt;enumeration value="ERN"/>
 *     &lt;enumeration value="EEK"/>
 *     &lt;enumeration value="ETB"/>
 *     &lt;enumeration value="FKP"/>
 *     &lt;enumeration value="FJD"/>
 *     &lt;enumeration value="GMD"/>
 *     &lt;enumeration value="GEL"/>
 *     &lt;enumeration value="GHS"/>
 *     &lt;enumeration value="GIP"/>
 *     &lt;enumeration value="GTQ"/>
 *     &lt;enumeration value="GNF"/>
 *     &lt;enumeration value="GWP"/>
 *     &lt;enumeration value="GYD"/>
 *     &lt;enumeration value="HTG"/>
 *     &lt;enumeration value="HNL"/>
 *     &lt;enumeration value="HKD"/>
 *     &lt;enumeration value="HUF"/>
 *     &lt;enumeration value="ISK"/>
 *     &lt;enumeration value="INR"/>
 *     &lt;enumeration value="IDR"/>
 *     &lt;enumeration value="XDR"/>
 *     &lt;enumeration value="IRR"/>
 *     &lt;enumeration value="IQD"/>
 *     &lt;enumeration value="ILS"/>
 *     &lt;enumeration value="JMD"/>
 *     &lt;enumeration value="JPY"/>
 *     &lt;enumeration value="JOD"/>
 *     &lt;enumeration value="KZT"/>
 *     &lt;enumeration value="KES"/>
 *     &lt;enumeration value="KPW"/>
 *     &lt;enumeration value="KRW"/>
 *     &lt;enumeration value="KWD"/>
 *     &lt;enumeration value="KGS"/>
 *     &lt;enumeration value="LAK"/>
 *     &lt;enumeration value="LVL"/>
 *     &lt;enumeration value="LBP"/>
 *     &lt;enumeration value="LSL"/>
 *     &lt;enumeration value="LRD"/>
 *     &lt;enumeration value="LYD"/>
 *     &lt;enumeration value="LTL"/>
 *     &lt;enumeration value="MOP"/>
 *     &lt;enumeration value="MKD"/>
 *     &lt;enumeration value="MGA"/>
 *     &lt;enumeration value="MWK"/>
 *     &lt;enumeration value="MYR"/>
 *     &lt;enumeration value="MVR"/>
 *     &lt;enumeration value="MTL"/>
 *     &lt;enumeration value="MRO"/>
 *     &lt;enumeration value="MUR"/>
 *     &lt;enumeration value="MXN"/>
 *     &lt;enumeration value="MXV"/>
 *     &lt;enumeration value="MDL"/>
 *     &lt;enumeration value="MNT"/>
 *     &lt;enumeration value="MAD"/>
 *     &lt;enumeration value="MZN"/>
 *     &lt;enumeration value="MMK"/>
 *     &lt;enumeration value="NAD"/>
 *     &lt;enumeration value="NPR"/>
 *     &lt;enumeration value="ANG"/>
 *     &lt;enumeration value="XPF"/>
 *     &lt;enumeration value="NZD"/>
 *     &lt;enumeration value="NIO"/>
 *     &lt;enumeration value="NGN"/>
 *     &lt;enumeration value="NOK"/>
 *     &lt;enumeration value="OMR"/>
 *     &lt;enumeration value="PKR"/>
 *     &lt;enumeration value="PAB"/>
 *     &lt;enumeration value="PGK"/>
 *     &lt;enumeration value="PYG"/>
 *     &lt;enumeration value="PEN"/>
 *     &lt;enumeration value="PHP"/>
 *     &lt;enumeration value="PLN"/>
 *     &lt;enumeration value="QAR"/>
 *     &lt;enumeration value="RON"/>
 *     &lt;enumeration value="RUB"/>
 *     &lt;enumeration value="RWF"/>
 *     &lt;enumeration value="SHP"/>
 *     &lt;enumeration value="WST"/>
 *     &lt;enumeration value="STD"/>
 *     &lt;enumeration value="SAR"/>
 *     &lt;enumeration value="RSD"/>
 *     &lt;enumeration value="SCR"/>
 *     &lt;enumeration value="SLL"/>
 *     &lt;enumeration value="SGD"/>
 *     &lt;enumeration value="SKK"/>
 *     &lt;enumeration value="SBD"/>
 *     &lt;enumeration value="SOS"/>
 *     &lt;enumeration value="ZAR"/>
 *     &lt;enumeration value="LKR"/>
 *     &lt;enumeration value="SDG"/>
 *     &lt;enumeration value="SRD"/>
 *     &lt;enumeration value="SZL"/>
 *     &lt;enumeration value="SEK"/>
 *     &lt;enumeration value="CHF"/>
 *     &lt;enumeration value="CHW"/>
 *     &lt;enumeration value="CHE"/>
 *     &lt;enumeration value="SYP"/>
 *     &lt;enumeration value="TWD"/>
 *     &lt;enumeration value="TJS"/>
 *     &lt;enumeration value="TZS"/>
 *     &lt;enumeration value="THB"/>
 *     &lt;enumeration value="TOP"/>
 *     &lt;enumeration value="TTD"/>
 *     &lt;enumeration value="TND"/>
 *     &lt;enumeration value="TRY"/>
 *     &lt;enumeration value="TMM"/>
 *     &lt;enumeration value="UGX"/>
 *     &lt;enumeration value="UAH"/>
 *     &lt;enumeration value="AED"/>
 *     &lt;enumeration value="GBP"/>
 *     &lt;enumeration value="USS"/>
 *     &lt;enumeration value="USN"/>
 *     &lt;enumeration value="UYU"/>
 *     &lt;enumeration value="UYI"/>
 *     &lt;enumeration value="UZS"/>
 *     &lt;enumeration value="VUV"/>
 *     &lt;enumeration value="VEF"/>
 *     &lt;enumeration value="VND"/>
 *     &lt;enumeration value="YER"/>
 *     &lt;enumeration value="ZMK"/>
 *     &lt;enumeration value="ZWD"/>
 *     &lt;enumeration value="XBA"/>
 *     &lt;enumeration value="XBB"/>
 *     &lt;enumeration value="XBC"/>
 *     &lt;enumeration value="XBD"/>
 *     &lt;enumeration value="XPD"/>
 *     &lt;enumeration value="XPT"/>
 *     &lt;enumeration value="XAG"/>
 *     &lt;enumeration value="XAU"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeISOCurrencyCodeA3")
@XmlEnum
public enum TypeISOCurrencyCodeA3 {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    AFN("AFN"),
    ALL("ALL"),
    DZD("DZD"),
    USD("USD"),
    EUR("EUR"),
    AOA("AOA"),
    XCD("XCD"),
    ARS("ARS"),
    AMD("AMD"),
    AWG("AWG"),
    AUD("AUD"),
    AZN("AZN"),
    BSD("BSD"),
    BHD("BHD"),
    BDT("BDT"),
    BBD("BBD"),
    BYR("BYR"),
    BZD("BZD"),
    BMD("BMD"),
    BTN("BTN"),
    BOB("BOB"),
    BOV("BOV"),
    BAM("BAM"),
    BWP("BWP"),
    BRL("BRL"),
    BND("BND"),
    BGN("BGN"),
    XOF("XOF"),
    BIF("BIF"),
    KHR("KHR"),
    CAD("CAD"),
    CVE("CVE"),
    KYD("KYD"),
    CLP("CLP"),
    CLF("CLF"),
    CNY("CNY"),
    COP("COP"),
    COU("COU"),
    KMF("KMF"),
    XAF("XAF"),
    CDF("CDF"),
    CRC("CRC"),
    HRK("HRK"),
    CUP("CUP"),
    CYP("CYP"),
    CZK("CZK"),
    DKK("DKK"),
    DJF("DJF"),
    DOP("DOP"),
    EGP("EGP"),
    SVC("SVC"),
    ERN("ERN"),
    EEK("EEK"),
    ETB("ETB"),
    FKP("FKP"),
    FJD("FJD"),
    GMD("GMD"),
    GEL("GEL"),
    GHS("GHS"),
    GIP("GIP"),
    GTQ("GTQ"),
    GNF("GNF"),
    GWP("GWP"),
    GYD("GYD"),
    HTG("HTG"),
    HNL("HNL"),
    HKD("HKD"),
    HUF("HUF"),
    ISK("ISK"),
    INR("INR"),
    IDR("IDR"),
    XDR("XDR"),
    IRR("IRR"),
    IQD("IQD"),
    ILS("ILS"),
    JMD("JMD"),
    JPY("JPY"),
    JOD("JOD"),
    KZT("KZT"),
    KES("KES"),
    KPW("KPW"),
    KRW("KRW"),
    KWD("KWD"),
    KGS("KGS"),
    LAK("LAK"),
    LVL("LVL"),
    LBP("LBP"),
    LSL("LSL"),
    LRD("LRD"),
    LYD("LYD"),
    LTL("LTL"),
    MOP("MOP"),
    MKD("MKD"),
    MGA("MGA"),
    MWK("MWK"),
    MYR("MYR"),
    MVR("MVR"),
    MTL("MTL"),
    MRO("MRO"),
    MUR("MUR"),
    MXN("MXN"),
    MXV("MXV"),
    MDL("MDL"),
    MNT("MNT"),
    MAD("MAD"),
    MZN("MZN"),
    MMK("MMK"),
    NAD("NAD"),
    NPR("NPR"),
    ANG("ANG"),
    XPF("XPF"),
    NZD("NZD"),
    NIO("NIO"),
    NGN("NGN"),
    NOK("NOK"),
    OMR("OMR"),
    PKR("PKR"),
    PAB("PAB"),
    PGK("PGK"),
    PYG("PYG"),
    PEN("PEN"),
    PHP("PHP"),
    PLN("PLN"),
    QAR("QAR"),
    RON("RON"),
    RUB("RUB"),
    RWF("RWF"),
    SHP("SHP"),
    WST("WST"),
    STD("STD"),
    SAR("SAR"),
    RSD("RSD"),
    SCR("SCR"),
    SLL("SLL"),
    SGD("SGD"),
    SKK("SKK"),
    SBD("SBD"),
    SOS("SOS"),
    ZAR("ZAR"),
    LKR("LKR"),
    SDG("SDG"),
    SRD("SRD"),
    SZL("SZL"),
    SEK("SEK"),
    CHF("CHF"),
    CHW("CHW"),
    CHE("CHE"),
    SYP("SYP"),
    TWD("TWD"),
    TJS("TJS"),
    TZS("TZS"),
    THB("THB"),
    TOP("TOP"),
    TTD("TTD"),
    TND("TND"),
    TRY("TRY"),
    TMM("TMM"),
    UGX("UGX"),
    UAH("UAH"),
    AED("AED"),
    GBP("GBP"),
    USS("USS"),
    USN("USN"),
    UYU("UYU"),
    UYI("UYI"),
    UZS("UZS"),
    VUV("VUV"),
    VEF("VEF"),
    VND("VND"),
    YER("YER"),
    ZMK("ZMK"),
    ZWD("ZWD"),
    XBA("XBA"),
    XBB("XBB"),
    XBC("XBC"),
    XBD("XBD"),
    XPD("XPD"),
    XPT("XPT"),
    XAG("XAG"),
    XAU("XAU");
    private final String value;

    TypeISOCurrencyCodeA3(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeISOCurrencyCodeA3 fromValue(String v) {
        for (TypeISOCurrencyCodeA3 c: TypeISOCurrencyCodeA3 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
