
package com.ipcommerce.schemas.cws.v2_0.serviceinformation;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ipcommerce.schemas.cws.v2_0.serviceinformation package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _HardwareType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "HardwareType");
    private final static QName _BankcardServiceAVSData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "BankcardServiceAVSData");
    private final static QName _RequestAdvice_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "RequestAdvice");
    private final static QName _BankcardMerchantData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "BankcardMerchantData");
    private final static QName _AddressInfo_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "AddressInfo");
    private final static QName _TypeISOCurrencyCodeA3_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "TypeISOCurrencyCodeA3");
    private final static QName _ServiceInformation_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "ServiceInformation");
    private final static QName _TypeStateProvince_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "TypeStateProvince");
    private final static QName _RequestACI_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "RequestACI");
    private final static QName _TypeISOLanguageCodeA3_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "TypeISOLanguageCodeA3");
    private final static QName _CustomerPresent_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "CustomerPresent");
    private final static QName _ApplicationData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "ApplicationData");
    private final static QName _PINDebitReturnSupportType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "PINDebitReturnSupportType");
    private final static QName _Tenders_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "Tenders");
    private final static QName _ArrayOfMerchantProfile_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "ArrayOfMerchantProfile");
    private final static QName _TypeISOCountryCodeA3_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "TypeISOCountryCodeA3");
    private final static QName _BankcardService_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "BankcardService");
    private final static QName _PurchaseCardLevel_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "PurchaseCardLevel");
    private final static QName _PINCapability_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "PINCapability");
    private final static QName _MerchantProfileTransactionData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "MerchantProfileTransactionData");
    private final static QName _ArrayOfBankcardService_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "ArrayOfBankcardService");
    private final static QName _Operations_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "Operations");
    private final static QName _ReadCapability_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "ReadCapability");
    private final static QName _BankcardTransactionDataDefaults_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "BankcardTransactionDataDefaults");
    private final static QName _TenderType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "TenderType");
    private final static QName _MerchantProfileMerchantData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "MerchantProfileMerchantData");
    private final static QName _ApplicationLocation_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "ApplicationLocation");
    private final static QName _MerchantProfile_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "MerchantProfile");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ipcommerce.schemas.cws.v2_0.serviceinformation
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeleteMerchantProfile }
     * 
     */
    public DeleteMerchantProfile createDeleteMerchantProfile() {
        return new DeleteMerchantProfile();
    }

    /**
     * Create an instance of {@link SaveApplicationDataResponse }
     * 
     */
    public SaveApplicationDataResponse createSaveApplicationDataResponse() {
        return new SaveApplicationDataResponse();
    }

    /**
     * Create an instance of {@link BankcardTransactionDataDefaults }
     * 
     */
    public BankcardTransactionDataDefaults createBankcardTransactionDataDefaults() {
        return new BankcardTransactionDataDefaults();
    }

    /**
     * Create an instance of {@link Operations }
     * 
     */
    public Operations createOperations() {
        return new Operations();
    }

    /**
     * Create an instance of {@link AddressInfo }
     * 
     */
    public AddressInfo createAddressInfo() {
        return new AddressInfo();
    }

    /**
     * Create an instance of {@link ApplicationData }
     * 
     */
    public ApplicationData createApplicationData() {
        return new ApplicationData();
    }

    /**
     * Create an instance of {@link SignOnWithToken }
     * 
     */
    public SignOnWithToken createSignOnWithToken() {
        return new SignOnWithToken();
    }

    /**
     * Create an instance of {@link ArrayOfMerchantProfile }
     * 
     */
    public ArrayOfMerchantProfile createArrayOfMerchantProfile() {
        return new ArrayOfMerchantProfile();
    }

    /**
     * Create an instance of {@link SignOnWithTokenResponse }
     * 
     */
    public SignOnWithTokenResponse createSignOnWithTokenResponse() {
        return new SignOnWithTokenResponse();
    }

    /**
     * Create an instance of {@link SaveApplicationData }
     * 
     */
    public SaveApplicationData createSaveApplicationData() {
        return new SaveApplicationData();
    }

    /**
     * Create an instance of {@link GetApplicationDataResponse }
     * 
     */
    public GetApplicationDataResponse createGetApplicationDataResponse() {
        return new GetApplicationDataResponse();
    }

    /**
     * Create an instance of {@link GetServiceInformationResponse }
     * 
     */
    public GetServiceInformationResponse createGetServiceInformationResponse() {
        return new GetServiceInformationResponse();
    }

    /**
     * Create an instance of {@link MerchantProfile }
     * 
     */
    public MerchantProfile createMerchantProfile() {
        return new MerchantProfile();
    }

    /**
     * Create an instance of {@link SaveMerchantProfiles }
     * 
     */
    public SaveMerchantProfiles createSaveMerchantProfiles() {
        return new SaveMerchantProfiles();
    }

    /**
     * Create an instance of {@link DeleteApplicationDataResponse }
     * 
     */
    public DeleteApplicationDataResponse createDeleteApplicationDataResponse() {
        return new DeleteApplicationDataResponse();
    }

    /**
     * Create an instance of {@link BankcardServiceAVSData }
     * 
     */
    public BankcardServiceAVSData createBankcardServiceAVSData() {
        return new BankcardServiceAVSData();
    }

    /**
     * Create an instance of {@link Tenders }
     * 
     */
    public Tenders createTenders() {
        return new Tenders();
    }

    /**
     * Create an instance of {@link ArrayOfBankcardService }
     * 
     */
    public ArrayOfBankcardService createArrayOfBankcardService() {
        return new ArrayOfBankcardService();
    }

    /**
     * Create an instance of {@link ServiceInformation }
     * 
     */
    public ServiceInformation createServiceInformation() {
        return new ServiceInformation();
    }

    /**
     * Create an instance of {@link IsMerchantProfileInitializedResponse }
     * 
     */
    public IsMerchantProfileInitializedResponse createIsMerchantProfileInitializedResponse() {
        return new IsMerchantProfileInitializedResponse();
    }

    /**
     * Create an instance of {@link GetMerchantProfile }
     * 
     */
    public GetMerchantProfile createGetMerchantProfile() {
        return new GetMerchantProfile();
    }

    /**
     * Create an instance of {@link GetMerchantProfileResponse }
     * 
     */
    public GetMerchantProfileResponse createGetMerchantProfileResponse() {
        return new GetMerchantProfileResponse();
    }

    /**
     * Create an instance of {@link GetApplicationData }
     * 
     */
    public GetApplicationData createGetApplicationData() {
        return new GetApplicationData();
    }

    /**
     * Create an instance of {@link BankcardMerchantData }
     * 
     */
    public BankcardMerchantData createBankcardMerchantData() {
        return new BankcardMerchantData();
    }

    /**
     * Create an instance of {@link MerchantProfileTransactionData }
     * 
     */
    public MerchantProfileTransactionData createMerchantProfileTransactionData() {
        return new MerchantProfileTransactionData();
    }

    /**
     * Create an instance of {@link GetServiceInformation }
     * 
     */
    public GetServiceInformation createGetServiceInformation() {
        return new GetServiceInformation();
    }

    /**
     * Create an instance of {@link IsMerchantProfileInitialized }
     * 
     */
    public IsMerchantProfileInitialized createIsMerchantProfileInitialized() {
        return new IsMerchantProfileInitialized();
    }

    /**
     * Create an instance of {@link DeleteApplicationData }
     * 
     */
    public DeleteApplicationData createDeleteApplicationData() {
        return new DeleteApplicationData();
    }

    /**
     * Create an instance of {@link GetMerchantProfiles }
     * 
     */
    public GetMerchantProfiles createGetMerchantProfiles() {
        return new GetMerchantProfiles();
    }

    /**
     * Create an instance of {@link MerchantProfileMerchantData }
     * 
     */
    public MerchantProfileMerchantData createMerchantProfileMerchantData() {
        return new MerchantProfileMerchantData();
    }

    /**
     * Create an instance of {@link SaveMerchantProfilesResponse }
     * 
     */
    public SaveMerchantProfilesResponse createSaveMerchantProfilesResponse() {
        return new SaveMerchantProfilesResponse();
    }

    /**
     * Create an instance of {@link DeleteMerchantProfileResponse }
     * 
     */
    public DeleteMerchantProfileResponse createDeleteMerchantProfileResponse() {
        return new DeleteMerchantProfileResponse();
    }

    /**
     * Create an instance of {@link GetMerchantProfilesResponse }
     * 
     */
    public GetMerchantProfilesResponse createGetMerchantProfilesResponse() {
        return new GetMerchantProfilesResponse();
    }

    /**
     * Create an instance of {@link BankcardService }
     * 
     */
    public BankcardService createBankcardService() {
        return new BankcardService();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HardwareType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "HardwareType")
    public JAXBElement<HardwareType> createHardwareType(HardwareType value) {
        return new JAXBElement<HardwareType>(_HardwareType_QNAME, HardwareType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardServiceAVSData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "BankcardServiceAVSData")
    public JAXBElement<BankcardServiceAVSData> createBankcardServiceAVSData(BankcardServiceAVSData value) {
        return new JAXBElement<BankcardServiceAVSData>(_BankcardServiceAVSData_QNAME, BankcardServiceAVSData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestAdvice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "RequestAdvice")
    public JAXBElement<RequestAdvice> createRequestAdvice(RequestAdvice value) {
        return new JAXBElement<RequestAdvice>(_RequestAdvice_QNAME, RequestAdvice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardMerchantData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "BankcardMerchantData")
    public JAXBElement<BankcardMerchantData> createBankcardMerchantData(BankcardMerchantData value) {
        return new JAXBElement<BankcardMerchantData>(_BankcardMerchantData_QNAME, BankcardMerchantData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddressInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "AddressInfo")
    public JAXBElement<AddressInfo> createAddressInfo(AddressInfo value) {
        return new JAXBElement<AddressInfo>(_AddressInfo_QNAME, AddressInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeISOCurrencyCodeA3 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "TypeISOCurrencyCodeA3")
    public JAXBElement<TypeISOCurrencyCodeA3> createTypeISOCurrencyCodeA3(TypeISOCurrencyCodeA3 value) {
        return new JAXBElement<TypeISOCurrencyCodeA3>(_TypeISOCurrencyCodeA3_QNAME, TypeISOCurrencyCodeA3 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "ServiceInformation")
    public JAXBElement<ServiceInformation> createServiceInformation(ServiceInformation value) {
        return new JAXBElement<ServiceInformation>(_ServiceInformation_QNAME, ServiceInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeStateProvince }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "TypeStateProvince")
    public JAXBElement<TypeStateProvince> createTypeStateProvince(TypeStateProvince value) {
        return new JAXBElement<TypeStateProvince>(_TypeStateProvince_QNAME, TypeStateProvince.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestACI }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "RequestACI")
    public JAXBElement<RequestACI> createRequestACI(RequestACI value) {
        return new JAXBElement<RequestACI>(_RequestACI_QNAME, RequestACI.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeISOLanguageCodeA3 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "TypeISOLanguageCodeA3")
    public JAXBElement<TypeISOLanguageCodeA3> createTypeISOLanguageCodeA3(TypeISOLanguageCodeA3 value) {
        return new JAXBElement<TypeISOLanguageCodeA3>(_TypeISOLanguageCodeA3_QNAME, TypeISOLanguageCodeA3 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerPresent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "CustomerPresent")
    public JAXBElement<CustomerPresent> createCustomerPresent(CustomerPresent value) {
        return new JAXBElement<CustomerPresent>(_CustomerPresent_QNAME, CustomerPresent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "ApplicationData")
    public JAXBElement<ApplicationData> createApplicationData(ApplicationData value) {
        return new JAXBElement<ApplicationData>(_ApplicationData_QNAME, ApplicationData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PINDebitReturnSupportType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "PINDebitReturnSupportType")
    public JAXBElement<PINDebitReturnSupportType> createPINDebitReturnSupportType(PINDebitReturnSupportType value) {
        return new JAXBElement<PINDebitReturnSupportType>(_PINDebitReturnSupportType_QNAME, PINDebitReturnSupportType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tenders }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "Tenders")
    public JAXBElement<Tenders> createTenders(Tenders value) {
        return new JAXBElement<Tenders>(_Tenders_QNAME, Tenders.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfMerchantProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "ArrayOfMerchantProfile")
    public JAXBElement<ArrayOfMerchantProfile> createArrayOfMerchantProfile(ArrayOfMerchantProfile value) {
        return new JAXBElement<ArrayOfMerchantProfile>(_ArrayOfMerchantProfile_QNAME, ArrayOfMerchantProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeISOCountryCodeA3 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "TypeISOCountryCodeA3")
    public JAXBElement<TypeISOCountryCodeA3> createTypeISOCountryCodeA3(TypeISOCountryCodeA3 value) {
        return new JAXBElement<TypeISOCountryCodeA3>(_TypeISOCountryCodeA3_QNAME, TypeISOCountryCodeA3 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "BankcardService")
    public JAXBElement<BankcardService> createBankcardService(BankcardService value) {
        return new JAXBElement<BankcardService>(_BankcardService_QNAME, BankcardService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PurchaseCardLevel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "PurchaseCardLevel")
    public JAXBElement<PurchaseCardLevel> createPurchaseCardLevel(PurchaseCardLevel value) {
        return new JAXBElement<PurchaseCardLevel>(_PurchaseCardLevel_QNAME, PurchaseCardLevel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PINCapability }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "PINCapability")
    public JAXBElement<PINCapability> createPINCapability(PINCapability value) {
        return new JAXBElement<PINCapability>(_PINCapability_QNAME, PINCapability.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MerchantProfileTransactionData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "MerchantProfileTransactionData")
    public JAXBElement<MerchantProfileTransactionData> createMerchantProfileTransactionData(MerchantProfileTransactionData value) {
        return new JAXBElement<MerchantProfileTransactionData>(_MerchantProfileTransactionData_QNAME, MerchantProfileTransactionData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfBankcardService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "ArrayOfBankcardService")
    public JAXBElement<ArrayOfBankcardService> createArrayOfBankcardService(ArrayOfBankcardService value) {
        return new JAXBElement<ArrayOfBankcardService>(_ArrayOfBankcardService_QNAME, ArrayOfBankcardService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Operations }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "Operations")
    public JAXBElement<Operations> createOperations(Operations value) {
        return new JAXBElement<Operations>(_Operations_QNAME, Operations.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReadCapability }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "ReadCapability")
    public JAXBElement<ReadCapability> createReadCapability(ReadCapability value) {
        return new JAXBElement<ReadCapability>(_ReadCapability_QNAME, ReadCapability.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardTransactionDataDefaults }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "BankcardTransactionDataDefaults")
    public JAXBElement<BankcardTransactionDataDefaults> createBankcardTransactionDataDefaults(BankcardTransactionDataDefaults value) {
        return new JAXBElement<BankcardTransactionDataDefaults>(_BankcardTransactionDataDefaults_QNAME, BankcardTransactionDataDefaults.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TenderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "TenderType")
    public JAXBElement<TenderType> createTenderType(TenderType value) {
        return new JAXBElement<TenderType>(_TenderType_QNAME, TenderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MerchantProfileMerchantData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "MerchantProfileMerchantData")
    public JAXBElement<MerchantProfileMerchantData> createMerchantProfileMerchantData(MerchantProfileMerchantData value) {
        return new JAXBElement<MerchantProfileMerchantData>(_MerchantProfileMerchantData_QNAME, MerchantProfileMerchantData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ApplicationLocation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "ApplicationLocation")
    public JAXBElement<ApplicationLocation> createApplicationLocation(ApplicationLocation value) {
        return new JAXBElement<ApplicationLocation>(_ApplicationLocation_QNAME, ApplicationLocation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MerchantProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", name = "MerchantProfile")
    public JAXBElement<MerchantProfile> createMerchantProfile(MerchantProfile value) {
        return new JAXBElement<MerchantProfile>(_MerchantProfile_QNAME, MerchantProfile.class, null, value);
    }

}
