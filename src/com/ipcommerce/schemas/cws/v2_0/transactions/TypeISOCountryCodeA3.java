
package com.ipcommerce.schemas.cws.v2_0.transactions;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeISOCountryCodeA3.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeISOCountryCodeA3">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="AFG"/>
 *     &lt;enumeration value="ALA"/>
 *     &lt;enumeration value="ALB"/>
 *     &lt;enumeration value="DZA"/>
 *     &lt;enumeration value="ASM"/>
 *     &lt;enumeration value="AND"/>
 *     &lt;enumeration value="AGO"/>
 *     &lt;enumeration value="AIA"/>
 *     &lt;enumeration value="ATA"/>
 *     &lt;enumeration value="ATG"/>
 *     &lt;enumeration value="ARG"/>
 *     &lt;enumeration value="ARM"/>
 *     &lt;enumeration value="ABW"/>
 *     &lt;enumeration value="AUS"/>
 *     &lt;enumeration value="AUT"/>
 *     &lt;enumeration value="AZE"/>
 *     &lt;enumeration value="BHS"/>
 *     &lt;enumeration value="BHR"/>
 *     &lt;enumeration value="BGD"/>
 *     &lt;enumeration value="BRB"/>
 *     &lt;enumeration value="BLR"/>
 *     &lt;enumeration value="BEL"/>
 *     &lt;enumeration value="BLZ"/>
 *     &lt;enumeration value="BEN"/>
 *     &lt;enumeration value="BMU"/>
 *     &lt;enumeration value="BTN"/>
 *     &lt;enumeration value="BOL"/>
 *     &lt;enumeration value="BIH"/>
 *     &lt;enumeration value="BWA"/>
 *     &lt;enumeration value="BVT"/>
 *     &lt;enumeration value="BRA"/>
 *     &lt;enumeration value="IOT"/>
 *     &lt;enumeration value="BRN"/>
 *     &lt;enumeration value="BGR"/>
 *     &lt;enumeration value="BFA"/>
 *     &lt;enumeration value="BDI"/>
 *     &lt;enumeration value="KHM"/>
 *     &lt;enumeration value="CMR"/>
 *     &lt;enumeration value="CAN"/>
 *     &lt;enumeration value="CPV"/>
 *     &lt;enumeration value="CYM"/>
 *     &lt;enumeration value="CAF"/>
 *     &lt;enumeration value="TCD"/>
 *     &lt;enumeration value="CHL"/>
 *     &lt;enumeration value="CHN"/>
 *     &lt;enumeration value="CXR"/>
 *     &lt;enumeration value="CCK"/>
 *     &lt;enumeration value="COL"/>
 *     &lt;enumeration value="COM"/>
 *     &lt;enumeration value="COG"/>
 *     &lt;enumeration value="COD"/>
 *     &lt;enumeration value="COK"/>
 *     &lt;enumeration value="CRI"/>
 *     &lt;enumeration value="CIV"/>
 *     &lt;enumeration value="HRV"/>
 *     &lt;enumeration value="CUB"/>
 *     &lt;enumeration value="CYP"/>
 *     &lt;enumeration value="CZE"/>
 *     &lt;enumeration value="DNK"/>
 *     &lt;enumeration value="DJI"/>
 *     &lt;enumeration value="DMA"/>
 *     &lt;enumeration value="DOM"/>
 *     &lt;enumeration value="ECU"/>
 *     &lt;enumeration value="EGY"/>
 *     &lt;enumeration value="SLV"/>
 *     &lt;enumeration value="GNQ"/>
 *     &lt;enumeration value="ERI"/>
 *     &lt;enumeration value="EST"/>
 *     &lt;enumeration value="ETH"/>
 *     &lt;enumeration value="FLK"/>
 *     &lt;enumeration value="FRO"/>
 *     &lt;enumeration value="FJI"/>
 *     &lt;enumeration value="FIN"/>
 *     &lt;enumeration value="FRA"/>
 *     &lt;enumeration value="FXX"/>
 *     &lt;enumeration value="GUF"/>
 *     &lt;enumeration value="PYF"/>
 *     &lt;enumeration value="ATF"/>
 *     &lt;enumeration value="GAB"/>
 *     &lt;enumeration value="GMB"/>
 *     &lt;enumeration value="GEO"/>
 *     &lt;enumeration value="DEU"/>
 *     &lt;enumeration value="GHA"/>
 *     &lt;enumeration value="GIB"/>
 *     &lt;enumeration value="GRC"/>
 *     &lt;enumeration value="GRL"/>
 *     &lt;enumeration value="GRD"/>
 *     &lt;enumeration value="GLP"/>
 *     &lt;enumeration value="GUM"/>
 *     &lt;enumeration value="GTM"/>
 *     &lt;enumeration value="GGY"/>
 *     &lt;enumeration value="GIN"/>
 *     &lt;enumeration value="GNB"/>
 *     &lt;enumeration value="GUY"/>
 *     &lt;enumeration value="HTI"/>
 *     &lt;enumeration value="HMD"/>
 *     &lt;enumeration value="VAT"/>
 *     &lt;enumeration value="HND"/>
 *     &lt;enumeration value="HKG"/>
 *     &lt;enumeration value="HUN"/>
 *     &lt;enumeration value="ISL"/>
 *     &lt;enumeration value="IND"/>
 *     &lt;enumeration value="IDN"/>
 *     &lt;enumeration value="IRN"/>
 *     &lt;enumeration value="IRQ"/>
 *     &lt;enumeration value="IRL"/>
 *     &lt;enumeration value="IMN"/>
 *     &lt;enumeration value="ISR"/>
 *     &lt;enumeration value="ITA"/>
 *     &lt;enumeration value="JAM"/>
 *     &lt;enumeration value="JPN"/>
 *     &lt;enumeration value="JEY"/>
 *     &lt;enumeration value="JOR"/>
 *     &lt;enumeration value="KAZ"/>
 *     &lt;enumeration value="KEN"/>
 *     &lt;enumeration value="KIR"/>
 *     &lt;enumeration value="PRK"/>
 *     &lt;enumeration value="KOR"/>
 *     &lt;enumeration value="KWT"/>
 *     &lt;enumeration value="KGZ"/>
 *     &lt;enumeration value="LAO"/>
 *     &lt;enumeration value="LVA"/>
 *     &lt;enumeration value="LBN"/>
 *     &lt;enumeration value="LSO"/>
 *     &lt;enumeration value="LBR"/>
 *     &lt;enumeration value="LBY"/>
 *     &lt;enumeration value="LIE"/>
 *     &lt;enumeration value="LTU"/>
 *     &lt;enumeration value="LUX"/>
 *     &lt;enumeration value="MAC"/>
 *     &lt;enumeration value="MKD"/>
 *     &lt;enumeration value="MDG"/>
 *     &lt;enumeration value="MWI"/>
 *     &lt;enumeration value="MYS"/>
 *     &lt;enumeration value="MDV"/>
 *     &lt;enumeration value="MLI"/>
 *     &lt;enumeration value="MLT"/>
 *     &lt;enumeration value="MHL"/>
 *     &lt;enumeration value="MTQ"/>
 *     &lt;enumeration value="MRT"/>
 *     &lt;enumeration value="MUS"/>
 *     &lt;enumeration value="MYT"/>
 *     &lt;enumeration value="MEX"/>
 *     &lt;enumeration value="FSM"/>
 *     &lt;enumeration value="MDA"/>
 *     &lt;enumeration value="MCO"/>
 *     &lt;enumeration value="MNG"/>
 *     &lt;enumeration value="MNE"/>
 *     &lt;enumeration value="MSR"/>
 *     &lt;enumeration value="MAR"/>
 *     &lt;enumeration value="MOZ"/>
 *     &lt;enumeration value="MMR"/>
 *     &lt;enumeration value="NAM"/>
 *     &lt;enumeration value="NRU"/>
 *     &lt;enumeration value="NPL"/>
 *     &lt;enumeration value="NLD"/>
 *     &lt;enumeration value="ANT"/>
 *     &lt;enumeration value="NCL"/>
 *     &lt;enumeration value="NZL"/>
 *     &lt;enumeration value="NIC"/>
 *     &lt;enumeration value="NER"/>
 *     &lt;enumeration value="NGA"/>
 *     &lt;enumeration value="NIU"/>
 *     &lt;enumeration value="NFK"/>
 *     &lt;enumeration value="MNP"/>
 *     &lt;enumeration value="NOR"/>
 *     &lt;enumeration value="OMN"/>
 *     &lt;enumeration value="PAK"/>
 *     &lt;enumeration value="PLW"/>
 *     &lt;enumeration value="PSE"/>
 *     &lt;enumeration value="PAN"/>
 *     &lt;enumeration value="PNG"/>
 *     &lt;enumeration value="PRY"/>
 *     &lt;enumeration value="PER"/>
 *     &lt;enumeration value="PHL"/>
 *     &lt;enumeration value="PCN"/>
 *     &lt;enumeration value="POL"/>
 *     &lt;enumeration value="PRT"/>
 *     &lt;enumeration value="PRI"/>
 *     &lt;enumeration value="QAT"/>
 *     &lt;enumeration value="REU"/>
 *     &lt;enumeration value="ROU"/>
 *     &lt;enumeration value="RUS"/>
 *     &lt;enumeration value="RWA"/>
 *     &lt;enumeration value="SHN"/>
 *     &lt;enumeration value="KNA"/>
 *     &lt;enumeration value="LCA"/>
 *     &lt;enumeration value="SPM"/>
 *     &lt;enumeration value="VCT"/>
 *     &lt;enumeration value="WSM"/>
 *     &lt;enumeration value="SMR"/>
 *     &lt;enumeration value="STP"/>
 *     &lt;enumeration value="SAU"/>
 *     &lt;enumeration value="SEN"/>
 *     &lt;enumeration value="SRB"/>
 *     &lt;enumeration value="SCG"/>
 *     &lt;enumeration value="SYC"/>
 *     &lt;enumeration value="SLE"/>
 *     &lt;enumeration value="SGP"/>
 *     &lt;enumeration value="SVK"/>
 *     &lt;enumeration value="SVN"/>
 *     &lt;enumeration value="SLB"/>
 *     &lt;enumeration value="SOM"/>
 *     &lt;enumeration value="ZAF"/>
 *     &lt;enumeration value="SGS"/>
 *     &lt;enumeration value="ESP"/>
 *     &lt;enumeration value="LKA"/>
 *     &lt;enumeration value="SDN"/>
 *     &lt;enumeration value="SUR"/>
 *     &lt;enumeration value="SJM"/>
 *     &lt;enumeration value="SWZ"/>
 *     &lt;enumeration value="SWE"/>
 *     &lt;enumeration value="CHE"/>
 *     &lt;enumeration value="SYR"/>
 *     &lt;enumeration value="TWN"/>
 *     &lt;enumeration value="TJK"/>
 *     &lt;enumeration value="TZA"/>
 *     &lt;enumeration value="THA"/>
 *     &lt;enumeration value="TLS"/>
 *     &lt;enumeration value="TGO"/>
 *     &lt;enumeration value="TKL"/>
 *     &lt;enumeration value="TMP"/>
 *     &lt;enumeration value="TON"/>
 *     &lt;enumeration value="TTO"/>
 *     &lt;enumeration value="TUN"/>
 *     &lt;enumeration value="TUR"/>
 *     &lt;enumeration value="TKM"/>
 *     &lt;enumeration value="TCA"/>
 *     &lt;enumeration value="TUV"/>
 *     &lt;enumeration value="UGA"/>
 *     &lt;enumeration value="UKR"/>
 *     &lt;enumeration value="ARE"/>
 *     &lt;enumeration value="GBR"/>
 *     &lt;enumeration value="USA"/>
 *     &lt;enumeration value="UMI"/>
 *     &lt;enumeration value="URY"/>
 *     &lt;enumeration value="UZB"/>
 *     &lt;enumeration value="VUT"/>
 *     &lt;enumeration value="VEN"/>
 *     &lt;enumeration value="VNM"/>
 *     &lt;enumeration value="VGB"/>
 *     &lt;enumeration value="VIR"/>
 *     &lt;enumeration value="WLF"/>
 *     &lt;enumeration value="ESH"/>
 *     &lt;enumeration value="YEM"/>
 *     &lt;enumeration value="YUG"/>
 *     &lt;enumeration value="ZMB"/>
 *     &lt;enumeration value="ZWE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeISOCountryCodeA3")
@XmlEnum
public enum TypeISOCountryCodeA3 {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    AFG("AFG"),
    ALA("ALA"),
    ALB("ALB"),
    DZA("DZA"),
    ASM("ASM"),
    AND("AND"),
    AGO("AGO"),
    AIA("AIA"),
    ATA("ATA"),
    ATG("ATG"),
    ARG("ARG"),
    ARM("ARM"),
    ABW("ABW"),
    AUS("AUS"),
    AUT("AUT"),
    AZE("AZE"),
    BHS("BHS"),
    BHR("BHR"),
    BGD("BGD"),
    BRB("BRB"),
    BLR("BLR"),
    BEL("BEL"),
    BLZ("BLZ"),
    BEN("BEN"),
    BMU("BMU"),
    BTN("BTN"),
    BOL("BOL"),
    BIH("BIH"),
    BWA("BWA"),
    BVT("BVT"),
    BRA("BRA"),
    IOT("IOT"),
    BRN("BRN"),
    BGR("BGR"),
    BFA("BFA"),
    BDI("BDI"),
    KHM("KHM"),
    CMR("CMR"),
    CAN("CAN"),
    CPV("CPV"),
    CYM("CYM"),
    CAF("CAF"),
    TCD("TCD"),
    CHL("CHL"),
    CHN("CHN"),
    CXR("CXR"),
    CCK("CCK"),
    COL("COL"),
    COM("COM"),
    COG("COG"),
    COD("COD"),
    COK("COK"),
    CRI("CRI"),
    CIV("CIV"),
    HRV("HRV"),
    CUB("CUB"),
    CYP("CYP"),
    CZE("CZE"),
    DNK("DNK"),
    DJI("DJI"),
    DMA("DMA"),
    DOM("DOM"),
    ECU("ECU"),
    EGY("EGY"),
    SLV("SLV"),
    GNQ("GNQ"),
    ERI("ERI"),
    EST("EST"),
    ETH("ETH"),
    FLK("FLK"),
    FRO("FRO"),
    FJI("FJI"),
    FIN("FIN"),
    FRA("FRA"),
    FXX("FXX"),
    GUF("GUF"),
    PYF("PYF"),
    ATF("ATF"),
    GAB("GAB"),
    GMB("GMB"),
    GEO("GEO"),
    DEU("DEU"),
    GHA("GHA"),
    GIB("GIB"),
    GRC("GRC"),
    GRL("GRL"),
    GRD("GRD"),
    GLP("GLP"),
    GUM("GUM"),
    GTM("GTM"),
    GGY("GGY"),
    GIN("GIN"),
    GNB("GNB"),
    GUY("GUY"),
    HTI("HTI"),
    HMD("HMD"),
    VAT("VAT"),
    HND("HND"),
    HKG("HKG"),
    HUN("HUN"),
    ISL("ISL"),
    IND("IND"),
    IDN("IDN"),
    IRN("IRN"),
    IRQ("IRQ"),
    IRL("IRL"),
    IMN("IMN"),
    ISR("ISR"),
    ITA("ITA"),
    JAM("JAM"),
    JPN("JPN"),
    JEY("JEY"),
    JOR("JOR"),
    KAZ("KAZ"),
    KEN("KEN"),
    KIR("KIR"),
    PRK("PRK"),
    KOR("KOR"),
    KWT("KWT"),
    KGZ("KGZ"),
    LAO("LAO"),
    LVA("LVA"),
    LBN("LBN"),
    LSO("LSO"),
    LBR("LBR"),
    LBY("LBY"),
    LIE("LIE"),
    LTU("LTU"),
    LUX("LUX"),
    MAC("MAC"),
    MKD("MKD"),
    MDG("MDG"),
    MWI("MWI"),
    MYS("MYS"),
    MDV("MDV"),
    MLI("MLI"),
    MLT("MLT"),
    MHL("MHL"),
    MTQ("MTQ"),
    MRT("MRT"),
    MUS("MUS"),
    MYT("MYT"),
    MEX("MEX"),
    FSM("FSM"),
    MDA("MDA"),
    MCO("MCO"),
    MNG("MNG"),
    MNE("MNE"),
    MSR("MSR"),
    MAR("MAR"),
    MOZ("MOZ"),
    MMR("MMR"),
    NAM("NAM"),
    NRU("NRU"),
    NPL("NPL"),
    NLD("NLD"),
    ANT("ANT"),
    NCL("NCL"),
    NZL("NZL"),
    NIC("NIC"),
    NER("NER"),
    NGA("NGA"),
    NIU("NIU"),
    NFK("NFK"),
    MNP("MNP"),
    NOR("NOR"),
    OMN("OMN"),
    PAK("PAK"),
    PLW("PLW"),
    PSE("PSE"),
    PAN("PAN"),
    PNG("PNG"),
    PRY("PRY"),
    PER("PER"),
    PHL("PHL"),
    PCN("PCN"),
    POL("POL"),
    PRT("PRT"),
    PRI("PRI"),
    QAT("QAT"),
    REU("REU"),
    ROU("ROU"),
    RUS("RUS"),
    RWA("RWA"),
    SHN("SHN"),
    KNA("KNA"),
    LCA("LCA"),
    SPM("SPM"),
    VCT("VCT"),
    WSM("WSM"),
    SMR("SMR"),
    STP("STP"),
    SAU("SAU"),
    SEN("SEN"),
    SRB("SRB"),
    SCG("SCG"),
    SYC("SYC"),
    SLE("SLE"),
    SGP("SGP"),
    SVK("SVK"),
    SVN("SVN"),
    SLB("SLB"),
    SOM("SOM"),
    ZAF("ZAF"),
    SGS("SGS"),
    ESP("ESP"),
    LKA("LKA"),
    SDN("SDN"),
    SUR("SUR"),
    SJM("SJM"),
    SWZ("SWZ"),
    SWE("SWE"),
    CHE("CHE"),
    SYR("SYR"),
    TWN("TWN"),
    TJK("TJK"),
    TZA("TZA"),
    THA("THA"),
    TLS("TLS"),
    TGO("TGO"),
    TKL("TKL"),
    TMP("TMP"),
    TON("TON"),
    TTO("TTO"),
    TUN("TUN"),
    TUR("TUR"),
    TKM("TKM"),
    TCA("TCA"),
    TUV("TUV"),
    UGA("UGA"),
    UKR("UKR"),
    ARE("ARE"),
    GBR("GBR"),
    USA("USA"),
    UMI("UMI"),
    URY("URY"),
    UZB("UZB"),
    VUT("VUT"),
    VEN("VEN"),
    VNM("VNM"),
    VGB("VGB"),
    VIR("VIR"),
    WLF("WLF"),
    ESH("ESH"),
    YEM("YEM"),
    YUG("YUG"),
    ZMB("ZMB"),
    ZWE("ZWE");
    private final String value;

    TypeISOCountryCodeA3(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeISOCountryCodeA3 fromValue(String v) {
        for (TypeISOCountryCodeA3 c: TypeISOCountryCodeA3 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
