
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PINDebitUndoReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PINDebitUndoReason">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="NotApplicable"/>
 *     &lt;enumeration value="NoResponse"/>
 *     &lt;enumeration value="LateResponse"/>
 *     &lt;enumeration value="UnableToDeliverToPOS"/>
 *     &lt;enumeration value="CustomerCancellation"/>
 *     &lt;enumeration value="SuspectMalfunction"/>
 *     &lt;enumeration value="PartiallyCompleted"/>
 *     &lt;enumeration value="OriginalAmountIncorrect"/>
 *     &lt;enumeration value="ResponseIncomplete"/>
 *     &lt;enumeration value="CADFailure"/>
 *     &lt;enumeration value="UnableToDeliverResponse"/>
 *     &lt;enumeration value="ResponseTimeout"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PINDebitUndoReason")
@XmlEnum
public enum PINDebitUndoReason {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("NotApplicable")
    NOT_APPLICABLE("NotApplicable"),
    @XmlEnumValue("NoResponse")
    NO_RESPONSE("NoResponse"),
    @XmlEnumValue("LateResponse")
    LATE_RESPONSE("LateResponse"),
    @XmlEnumValue("UnableToDeliverToPOS")
    UNABLE_TO_DELIVER_TO_POS("UnableToDeliverToPOS"),
    @XmlEnumValue("CustomerCancellation")
    CUSTOMER_CANCELLATION("CustomerCancellation"),
    @XmlEnumValue("SuspectMalfunction")
    SUSPECT_MALFUNCTION("SuspectMalfunction"),
    @XmlEnumValue("PartiallyCompleted")
    PARTIALLY_COMPLETED("PartiallyCompleted"),
    @XmlEnumValue("OriginalAmountIncorrect")
    ORIGINAL_AMOUNT_INCORRECT("OriginalAmountIncorrect"),
    @XmlEnumValue("ResponseIncomplete")
    RESPONSE_INCOMPLETE("ResponseIncomplete"),
    @XmlEnumValue("CADFailure")
    CAD_FAILURE("CADFailure"),
    @XmlEnumValue("UnableToDeliverResponse")
    UNABLE_TO_DELIVER_RESPONSE("UnableToDeliverResponse"),
    @XmlEnumValue("ResponseTimeout")
    RESPONSE_TIMEOUT("ResponseTimeout");
    private final String value;

    PINDebitUndoReason(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PINDebitUndoReason fromValue(String v) {
        for (PINDebitUndoReason c: PINDebitUndoReason.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
