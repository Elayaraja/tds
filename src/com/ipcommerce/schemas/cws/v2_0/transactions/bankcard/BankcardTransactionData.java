
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.ipcommerce.schemas.cws.v2_0.transactions.TransactionData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.pro.BankcardTransactionDataPro;


/**
 * <p>Java class for BankcardTransactionData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankcardTransactionData">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions}TransactionData">
 *       &lt;sequence>
 *         &lt;element name="AccountType" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}AccountType" minOccurs="0"/>
 *         &lt;element name="ApprovalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CashBackAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CustomerPresent" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}CustomerPresent" minOccurs="0"/>
 *         &lt;element name="EmployeeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EntryMode" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}EntryMode" minOccurs="0"/>
 *         &lt;element name="GoodsType" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}GoodsType" minOccurs="0"/>
 *         &lt;element name="IndustryType" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}IndustryType" minOccurs="0"/>
 *         &lt;element name="InternetTransactionData" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}InternetTransactionData" minOccurs="0"/>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrderNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SignatureCaptured" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TerminalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankcardTransactionData", propOrder = {
    "accountType",
    "approvalCode",
    "cashBackAmount",
    "customerPresent",
    "employeeId",
    "entryMode",
    "goodsType",
    "industryType",
    "internetTransactionData",
    "invoiceNumber",
    "orderNumber",
    "signatureCaptured",
    "terminalId",
    "tipAmount"
})
@XmlSeeAlso({
    BankcardTransactionDataPro.class
})
public class BankcardTransactionData
    extends TransactionData
{

    @XmlElement(name = "AccountType")
    protected AccountType accountType;
    @XmlElement(name = "ApprovalCode", nillable = true)
    protected String approvalCode;
    @XmlElement(name = "CashBackAmount")
    protected BigDecimal cashBackAmount;
    @XmlElement(name = "CustomerPresent")
    protected CustomerPresent customerPresent;
    @XmlElement(name = "EmployeeId", nillable = true)
    protected String employeeId;
    @XmlElement(name = "EntryMode")
    protected EntryMode entryMode;
    @XmlElement(name = "GoodsType")
    protected GoodsType goodsType;
    @XmlElement(name = "IndustryType")
    protected IndustryType industryType;
    @XmlElement(name = "InternetTransactionData", nillable = true)
    protected InternetTransactionData internetTransactionData;
    @XmlElement(name = "InvoiceNumber", nillable = true)
    protected String invoiceNumber;
    @XmlElement(name = "OrderNumber", nillable = true)
    protected String orderNumber;
    @XmlElement(name = "SignatureCaptured")
    protected Boolean signatureCaptured;
    @XmlElement(name = "TerminalId", nillable = true)
    protected String terminalId;
    @XmlElement(name = "TipAmount")
    protected BigDecimal tipAmount;

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link AccountType }
     *     
     */
    public AccountType getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountType }
     *     
     */
    public void setAccountType(AccountType value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the approvalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * Sets the value of the approvalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalCode(String value) {
        this.approvalCode = value;
    }

    /**
     * Gets the value of the cashBackAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCashBackAmount() {
        return cashBackAmount;
    }

    /**
     * Sets the value of the cashBackAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCashBackAmount(BigDecimal value) {
        this.cashBackAmount = value;
    }

    /**
     * Gets the value of the customerPresent property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerPresent }
     *     
     */
    public CustomerPresent getCustomerPresent() {
        return customerPresent;
    }

    /**
     * Sets the value of the customerPresent property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerPresent }
     *     
     */
    public void setCustomerPresent(CustomerPresent value) {
        this.customerPresent = value;
    }

    /**
     * Gets the value of the employeeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * Sets the value of the employeeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeId(String value) {
        this.employeeId = value;
    }

    /**
     * Gets the value of the entryMode property.
     * 
     * @return
     *     possible object is
     *     {@link EntryMode }
     *     
     */
    public EntryMode getEntryMode() {
        return entryMode;
    }

    /**
     * Sets the value of the entryMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntryMode }
     *     
     */
    public void setEntryMode(EntryMode value) {
        this.entryMode = value;
    }

    /**
     * Gets the value of the goodsType property.
     * 
     * @return
     *     possible object is
     *     {@link GoodsType }
     *     
     */
    public GoodsType getGoodsType() {
        return goodsType;
    }

    /**
     * Sets the value of the goodsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link GoodsType }
     *     
     */
    public void setGoodsType(GoodsType value) {
        this.goodsType = value;
    }

    /**
     * Gets the value of the industryType property.
     * 
     * @return
     *     possible object is
     *     {@link IndustryType }
     *     
     */
    public IndustryType getIndustryType() {
        return industryType;
    }

    /**
     * Sets the value of the industryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link IndustryType }
     *     
     */
    public void setIndustryType(IndustryType value) {
        this.industryType = value;
    }

    /**
     * Gets the value of the internetTransactionData property.
     * 
     * @return
     *     possible object is
     *     {@link InternetTransactionData }
     *     
     */
    public InternetTransactionData getInternetTransactionData() {
        return internetTransactionData;
    }

    /**
     * Sets the value of the internetTransactionData property.
     * 
     * @param value
     *     allowed object is
     *     {@link InternetTransactionData }
     *     
     */
    public void setInternetTransactionData(InternetTransactionData value) {
        this.internetTransactionData = value;
    }

    /**
     * Gets the value of the invoiceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the value of the invoiceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceNumber(String value) {
        this.invoiceNumber = value;
    }

    /**
     * Gets the value of the orderNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the value of the orderNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderNumber(String value) {
        this.orderNumber = value;
    }

    /**
     * Gets the value of the signatureCaptured property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSignatureCaptured() {
        return signatureCaptured;
    }

    /**
     * Sets the value of the signatureCaptured property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSignatureCaptured(Boolean value) {
        this.signatureCaptured = value;
    }

    /**
     * Gets the value of the terminalId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * Sets the value of the terminalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalId(String value) {
        this.terminalId = value;
    }

    /**
     * Gets the value of the tipAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTipAmount() {
        return tipAmount;
    }

    /**
     * Sets the value of the tipAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTipAmount(BigDecimal value) {
        this.tipAmount = value;
    }

}
