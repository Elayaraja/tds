
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeTaxType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeTaxType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="CityTax"/>
 *     &lt;enumeration value="CountyTax"/>
 *     &lt;enumeration value="CountyParishTax"/>
 *     &lt;enumeration value="EnergyTax"/>
 *     &lt;enumeration value="FederalTax"/>
 *     &lt;enumeration value="EnvironmentalTax"/>
 *     &lt;enumeration value="GoodsServicesTax"/>
 *     &lt;enumeration value="HarmonizedTax"/>
 *     &lt;enumeration value="LuxuryTax"/>
 *     &lt;enumeration value="LocalSalesTax"/>
 *     &lt;enumeration value="MunicipalTax"/>
 *     &lt;enumeration value="OccupancyTax"/>
 *     &lt;enumeration value="OtherTax"/>
 *     &lt;enumeration value="QuebecSalesTax"/>
 *     &lt;enumeration value="RoomTax"/>
 *     &lt;enumeration value="StateLocalSalesTax"/>
 *     &lt;enumeration value="StateProvincialGoodsTax"/>
 *     &lt;enumeration value="StateSalesTax"/>
 *     &lt;enumeration value="StateProvincialTax"/>
 *     &lt;enumeration value="Unknown"/>
 *     &lt;enumeration value="VAT"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeTaxType")
@XmlEnum
public enum TypeTaxType {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("CityTax")
    CITY_TAX("CityTax"),
    @XmlEnumValue("CountyTax")
    COUNTY_TAX("CountyTax"),
    @XmlEnumValue("CountyParishTax")
    COUNTY_PARISH_TAX("CountyParishTax"),
    @XmlEnumValue("EnergyTax")
    ENERGY_TAX("EnergyTax"),
    @XmlEnumValue("FederalTax")
    FEDERAL_TAX("FederalTax"),
    @XmlEnumValue("EnvironmentalTax")
    ENVIRONMENTAL_TAX("EnvironmentalTax"),
    @XmlEnumValue("GoodsServicesTax")
    GOODS_SERVICES_TAX("GoodsServicesTax"),
    @XmlEnumValue("HarmonizedTax")
    HARMONIZED_TAX("HarmonizedTax"),
    @XmlEnumValue("LuxuryTax")
    LUXURY_TAX("LuxuryTax"),
    @XmlEnumValue("LocalSalesTax")
    LOCAL_SALES_TAX("LocalSalesTax"),
    @XmlEnumValue("MunicipalTax")
    MUNICIPAL_TAX("MunicipalTax"),
    @XmlEnumValue("OccupancyTax")
    OCCUPANCY_TAX("OccupancyTax"),
    @XmlEnumValue("OtherTax")
    OTHER_TAX("OtherTax"),
    @XmlEnumValue("QuebecSalesTax")
    QUEBEC_SALES_TAX("QuebecSalesTax"),
    @XmlEnumValue("RoomTax")
    ROOM_TAX("RoomTax"),
    @XmlEnumValue("StateLocalSalesTax")
    STATE_LOCAL_SALES_TAX("StateLocalSalesTax"),
    @XmlEnumValue("StateProvincialGoodsTax")
    STATE_PROVINCIAL_GOODS_TAX("StateProvincialGoodsTax"),
    @XmlEnumValue("StateSalesTax")
    STATE_SALES_TAX("StateSalesTax"),
    @XmlEnumValue("StateProvincialTax")
    STATE_PROVINCIAL_TAX("StateProvincialTax"),
    @XmlEnumValue("Unknown")
    UNKNOWN("Unknown"),
    VAT("VAT");
    private final String value;

    TypeTaxType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeTaxType fromValue(String v) {
        for (TypeTaxType c: TypeTaxType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
