
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CardSecurityData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CardSecurityData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AVSData" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}AVSData" minOccurs="0"/>
 *         &lt;element name="CVDataProvided" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}CVDataProvided" minOccurs="0"/>
 *         &lt;element name="CVData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="KeySerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardSecurityData", propOrder = {
    "avsData",
    "cvDataProvided",
    "cvData",
    "keySerialNumber",
    "pin"
})
public class CardSecurityData {

    @XmlElement(name = "AVSData", nillable = true)
    protected AVSData avsData;
    @XmlElement(name = "CVDataProvided")
    protected CVDataProvided cvDataProvided;
    @XmlElement(name = "CVData", nillable = true)
    protected String cvData;
    @XmlElement(name = "KeySerialNumber", nillable = true)
    protected String keySerialNumber;
    @XmlElement(name = "PIN", nillable = true)
    protected String pin;

    /**
     * Gets the value of the avsData property.
     * 
     * @return
     *     possible object is
     *     {@link AVSData }
     *     
     */
    public AVSData getAVSData() {
        return avsData;
    }

    /**
     * Sets the value of the avsData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AVSData }
     *     
     */
    public void setAVSData(AVSData value) {
        this.avsData = value;
    }

    /**
     * Gets the value of the cvDataProvided property.
     * 
     * @return
     *     possible object is
     *     {@link CVDataProvided }
     *     
     */
    public CVDataProvided getCVDataProvided() {
        return cvDataProvided;
    }

    /**
     * Sets the value of the cvDataProvided property.
     * 
     * @param value
     *     allowed object is
     *     {@link CVDataProvided }
     *     
     */
    public void setCVDataProvided(CVDataProvided value) {
        this.cvDataProvided = value;
    }

    /**
     * Gets the value of the cvData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCVData() {
        return cvData;
    }

    /**
     * Sets the value of the cvData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCVData(String value) {
        this.cvData = value;
    }

    /**
     * Gets the value of the keySerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeySerialNumber() {
        return keySerialNumber;
    }

    /**
     * Sets the value of the keySerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeySerialNumber(String value) {
        this.keySerialNumber = value;
    }

    /**
     * Gets the value of the pin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPIN() {
        return pin;
    }

    /**
     * Sets the value of the pin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPIN(String value) {
        this.pin = value;
    }

}
