
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EntryMode.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EntryMode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="Keyed"/>
 *     &lt;enumeration value="KeyedBadMagRead"/>
 *     &lt;enumeration value="TrackDataFromMSR"/>
 *     &lt;enumeration value="ChipReliable"/>
 *     &lt;enumeration value="ChipUnreliable"/>
 *     &lt;enumeration value="ContactlessMChipOrSmartCard"/>
 *     &lt;enumeration value="ContactlessStripe"/>
 *     &lt;enumeration value="TerminalNotUsed"/>
 *     &lt;enumeration value="BarCode"/>
 *     &lt;enumeration value="OCRReader"/>
 *     &lt;enumeration value="VSCCapable"/>
 *     &lt;enumeration value="ChipTrackDataFromRFID"/>
 *     &lt;enumeration value="MSRTrackDataFromRFID"/>
 *     &lt;enumeration value="NFCCapable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EntryMode")
@XmlEnum
public enum EntryMode {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("Keyed")
    KEYED("Keyed"),
    @XmlEnumValue("KeyedBadMagRead")
    KEYED_BAD_MAG_READ("KeyedBadMagRead"),
    @XmlEnumValue("TrackDataFromMSR")
    TRACK_DATA_FROM_MSR("TrackDataFromMSR"),
    @XmlEnumValue("ChipReliable")
    CHIP_RELIABLE("ChipReliable"),
    @XmlEnumValue("ChipUnreliable")
    CHIP_UNRELIABLE("ChipUnreliable"),
    @XmlEnumValue("ContactlessMChipOrSmartCard")
    CONTACTLESS_M_CHIP_OR_SMART_CARD("ContactlessMChipOrSmartCard"),
    @XmlEnumValue("ContactlessStripe")
    CONTACTLESS_STRIPE("ContactlessStripe"),
    @XmlEnumValue("TerminalNotUsed")
    TERMINAL_NOT_USED("TerminalNotUsed"),
    @XmlEnumValue("BarCode")
    BAR_CODE("BarCode"),
    @XmlEnumValue("OCRReader")
    OCR_READER("OCRReader"),
    @XmlEnumValue("VSCCapable")
    VSC_CAPABLE("VSCCapable"),
    @XmlEnumValue("ChipTrackDataFromRFID")
    CHIP_TRACK_DATA_FROM_RFID("ChipTrackDataFromRFID"),
    @XmlEnumValue("MSRTrackDataFromRFID")
    MSR_TRACK_DATA_FROM_RFID("MSRTrackDataFromRFID"),
    @XmlEnumValue("NFCCapable")
    NFC_CAPABLE("NFCCapable");
    private final String value;

    EntryMode(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EntryMode fromValue(String v) {
        for (EntryMode c: EntryMode.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
