
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BillPayment.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BillPayment">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="DeferredBilling"/>
 *     &lt;enumeration value="SinglePayment"/>
 *     &lt;enumeration value="Installment"/>
 *     &lt;enumeration value="Recurring"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BillPayment")
@XmlEnum
public enum BillPayment {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("DeferredBilling")
    DEFERRED_BILLING("DeferredBilling"),
    @XmlEnumValue("SinglePayment")
    SINGLE_PAYMENT("SinglePayment"),
    @XmlEnumValue("Installment")
    INSTALLMENT("Installment"),
    @XmlEnumValue("Recurring")
    RECURRING("Recurring");
    private final String value;

    BillPayment(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BillPayment fromValue(String v) {
        for (BillPayment c: BillPayment.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
