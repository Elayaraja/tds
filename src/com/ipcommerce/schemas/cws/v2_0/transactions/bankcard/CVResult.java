
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CVResult.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CVResult">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="Match"/>
 *     &lt;enumeration value="NoMatch"/>
 *     &lt;enumeration value="NotProcessed"/>
 *     &lt;enumeration value="NotIncluded"/>
 *     &lt;enumeration value="NoCodePresent"/>
 *     &lt;enumeration value="ShouldHaveBeenPresent"/>
 *     &lt;enumeration value="IssuerNotCertified"/>
 *     &lt;enumeration value="Invalid"/>
 *     &lt;enumeration value="NoResponse"/>
 *     &lt;enumeration value="NotApplicable"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CVResult")
@XmlEnum
public enum CVResult {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("Match")
    MATCH("Match"),
    @XmlEnumValue("NoMatch")
    NO_MATCH("NoMatch"),
    @XmlEnumValue("NotProcessed")
    NOT_PROCESSED("NotProcessed"),
    @XmlEnumValue("NotIncluded")
    NOT_INCLUDED("NotIncluded"),
    @XmlEnumValue("NoCodePresent")
    NO_CODE_PRESENT("NoCodePresent"),
    @XmlEnumValue("ShouldHaveBeenPresent")
    SHOULD_HAVE_BEEN_PRESENT("ShouldHaveBeenPresent"),
    @XmlEnumValue("IssuerNotCertified")
    ISSUER_NOT_CERTIFIED("IssuerNotCertified"),
    @XmlEnumValue("Invalid")
    INVALID("Invalid"),
    @XmlEnumValue("NoResponse")
    NO_RESPONSE("NoResponse"),
    @XmlEnumValue("NotApplicable")
    NOT_APPLICABLE("NotApplicable");
    private final String value;

    CVResult(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CVResult fromValue(String v) {
        for (CVResult c: CVResult.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
