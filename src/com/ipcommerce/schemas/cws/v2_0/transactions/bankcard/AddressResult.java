
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AddressResult.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AddressResult">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="NotIncluded"/>
 *     &lt;enumeration value="Match"/>
 *     &lt;enumeration value="NoMatch"/>
 *     &lt;enumeration value="IssuerNotCertified"/>
 *     &lt;enumeration value="NoResponseFromCardAssociation"/>
 *     &lt;enumeration value="UnknownResponseFromCardAssociation"/>
 *     &lt;enumeration value="NotVerified"/>
 *     &lt;enumeration value="BadFormat"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AddressResult")
@XmlEnum
public enum AddressResult {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("NotIncluded")
    NOT_INCLUDED("NotIncluded"),
    @XmlEnumValue("Match")
    MATCH("Match"),
    @XmlEnumValue("NoMatch")
    NO_MATCH("NoMatch"),
    @XmlEnumValue("IssuerNotCertified")
    ISSUER_NOT_CERTIFIED("IssuerNotCertified"),
    @XmlEnumValue("NoResponseFromCardAssociation")
    NO_RESPONSE_FROM_CARD_ASSOCIATION("NoResponseFromCardAssociation"),
    @XmlEnumValue("UnknownResponseFromCardAssociation")
    UNKNOWN_RESPONSE_FROM_CARD_ASSOCIATION("UnknownResponseFromCardAssociation"),
    @XmlEnumValue("NotVerified")
    NOT_VERIFIED("NotVerified"),
    @XmlEnumValue("BadFormat")
    BAD_FORMAT("BadFormat");
    private final String value;

    AddressResult(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AddressResult fromValue(String v) {
        for (AddressResult c: AddressResult.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
