
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sessionToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="differenceData" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions}Capture" minOccurs="0"/>
 *         &lt;element name="applicationProfileId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="merchantProfileId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sessionToken",
    "differenceData",
    "applicationProfileId",
    "merchantProfileId",
    "serviceId"
})
@XmlRootElement(name = "Capture")
public class Capture {

    @XmlElement(nillable = true)
    protected String sessionToken;
    @XmlElement(nillable = true)
    protected com.ipcommerce.schemas.cws.v2_0.transactions.Capture differenceData;
    @XmlElement(nillable = true)
    protected String applicationProfileId;
    @XmlElement(nillable = true)
    protected String merchantProfileId;
    @XmlElement(nillable = true)
    protected String serviceId;

    /**
     * Gets the value of the sessionToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * Sets the value of the sessionToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionToken(String value) {
        this.sessionToken = value;
    }

    /**
     * Gets the value of the differenceData property.
     * 
     * @return
     *     possible object is
     *     {@link com.ipcommerce.schemas.cws.v2_0.transactions.Capture }
     *     
     */
    public com.ipcommerce.schemas.cws.v2_0.transactions.Capture getDifferenceData() {
        return differenceData;
    }

    /**
     * Sets the value of the differenceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link com.ipcommerce.schemas.cws.v2_0.transactions.Capture }
     *     
     */
    public void setDifferenceData(com.ipcommerce.schemas.cws.v2_0.transactions.Capture value) {
        this.differenceData = value;
    }

    /**
     * Gets the value of the applicationProfileId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationProfileId() {
        return applicationProfileId;
    }

    /**
     * Sets the value of the applicationProfileId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationProfileId(String value) {
        this.applicationProfileId = value;
    }

    /**
     * Gets the value of the merchantProfileId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantProfileId() {
        return merchantProfileId;
    }

    /**
     * Sets the value of the merchantProfileId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantProfileId(String value) {
        this.merchantProfileId = value;
    }

    /**
     * Gets the value of the serviceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceId(String value) {
        this.serviceId = value;
    }

}
