
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ipcommerce.schemas.cws.v2_0.transactions.bankcard package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TaxExempt_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "TaxExempt");
    private final static QName _ArrayOfItemizedTax_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ArrayOfItemizedTax");
    private final static QName _AdviceResponse_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "AdviceResponse");
    private final static QName _AddressResult_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "AddressResult");
    private final static QName _TransactionSummaryData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "TransactionSummaryData");
    private final static QName _AccountType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "AccountType");
    private final static QName _BankcardTenderData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BankcardTenderData");
    private final static QName _IndustryType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "IndustryType");
    private final static QName _BankcardCaptureResponse_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BankcardCaptureResponse");
    private final static QName _Level2Data_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "Level2Data");
    private final static QName _PostalCodeResult_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "PostalCodeResult");
    private final static QName _LineItemDetail_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "LineItemDetail");
    private final static QName _ReturnedACI_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ReturnedACI");
    private final static QName _EntryMode_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "EntryMode");
    private final static QName _BankcardTransactionData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BankcardTransactionData");
    private final static QName _BillPayment_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BillPayment");
    private final static QName _Totals_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "Totals");
    private final static QName _GoodsType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "GoodsType");
    private final static QName _InternetTransactionData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "InternetTransactionData");
    private final static QName _TypeTaxType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "TypeTaxType");
    private final static QName _CommercialCardResponse_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "CommercialCardResponse");
    private final static QName _Resubmit_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "Resubmit");
    private final static QName _CVResult_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "CVResult");
    private final static QName _TypeCardType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "TypeCardType");
    private final static QName _ItemizedTax_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ItemizedTax");
    private final static QName _CVDataProvided_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "CVDataProvided");
    private final static QName _RequestAdvice_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "RequestAdvice");
    private final static QName _BankcardReturn_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BankcardReturn");
    private final static QName _BankcardTransaction_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BankcardTransaction");
    private final static QName _ExistingDebt_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ExistingDebt");
    private final static QName _ManagedBillingInstallments_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ManagedBillingInstallments");
    private final static QName _CustomerPresent_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "CustomerPresent");
    private final static QName _RequestACI_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "RequestACI");
    private final static QName _BankcardCapture_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BankcardCapture");
    private final static QName _ArrayOfLineItemDetail_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ArrayOfLineItemDetail");
    private final static QName _Tax_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "Tax");
    private final static QName _AVSData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "AVSData");
    private final static QName _IsTaxExempt_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "IsTaxExempt");
    private final static QName _ChargeType_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ChargeType");
    private final static QName _AVSResult_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "AVSResult");
    private final static QName _CardData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "CardData");
    private final static QName _BankcardUndo_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BankcardUndo");
    private final static QName _RequestCommercialCard_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "RequestCommercialCard");
    private final static QName _BankcardTransactionResponse_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "BankcardTransactionResponse");
    private final static QName _CardSecurityData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "CardSecurityData");
    private final static QName _PINDebitUndoReason_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "PINDebitUndoReason");
    private final static QName _Interval_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "Interval");
    private final static QName _ManagedBilling_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ManagedBilling");
    private final static QName _TypeUnitOfMeasure_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "TypeUnitOfMeasure");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Undo }
     * 
     */
    public Undo createUndo() {
        return new Undo();
    }

    /**
     * Create an instance of {@link ManagedBillingInstallments }
     * 
     */
    public ManagedBillingInstallments createManagedBillingInstallments() {
        return new ManagedBillingInstallments();
    }

    /**
     * Create an instance of {@link AVSData }
     * 
     */
    public AVSData createAVSData() {
        return new AVSData();
    }

    /**
     * Create an instance of {@link Capture }
     * 
     */
    public Capture createCapture() {
        return new Capture();
    }

    /**
     * Create an instance of {@link CardData }
     * 
     */
    public CardData createCardData() {
        return new CardData();
    }

    /**
     * Create an instance of {@link ManagedBilling }
     * 
     */
    public ManagedBilling createManagedBilling() {
        return new ManagedBilling();
    }

    /**
     * Create an instance of {@link Verify }
     * 
     */
    public Verify createVerify() {
        return new Verify();
    }

    /**
     * Create an instance of {@link TransactionSummaryData }
     * 
     */
    public TransactionSummaryData createTransactionSummaryData() {
        return new TransactionSummaryData();
    }

    /**
     * Create an instance of {@link Tax }
     * 
     */
    public Tax createTax() {
        return new Tax();
    }

    /**
     * Create an instance of {@link CaptureSelectiveResponse }
     * 
     */
    public CaptureSelectiveResponse createCaptureSelectiveResponse() {
        return new CaptureSelectiveResponse();
    }

    /**
     * Create an instance of {@link BankcardTransaction }
     * 
     */
    public BankcardTransaction createBankcardTransaction() {
        return new BankcardTransaction();
    }

    /**
     * Create an instance of {@link BankcardUndo }
     * 
     */
    public BankcardUndo createBankcardUndo() {
        return new BankcardUndo();
    }

    /**
     * Create an instance of {@link Level2Data }
     * 
     */
    public Level2Data createLevel2Data() {
        return new Level2Data();
    }

    /**
     * Create an instance of {@link CaptureAll }
     * 
     */
    public CaptureAll createCaptureAll() {
        return new CaptureAll();
    }

    /**
     * Create an instance of {@link Adjust }
     * 
     */
    public Adjust createAdjust() {
        return new Adjust();
    }

    /**
     * Create an instance of {@link ArrayOfLineItemDetail }
     * 
     */
    public ArrayOfLineItemDetail createArrayOfLineItemDetail() {
        return new ArrayOfLineItemDetail();
    }

    /**
     * Create an instance of {@link TaxExempt }
     * 
     */
    public TaxExempt createTaxExempt() {
        return new TaxExempt();
    }

    /**
     * Create an instance of {@link CaptureAllResponse }
     * 
     */
    public CaptureAllResponse createCaptureAllResponse() {
        return new CaptureAllResponse();
    }

    /**
     * Create an instance of {@link ReturnUnlinked }
     * 
     */
    public ReturnUnlinked createReturnUnlinked() {
        return new ReturnUnlinked();
    }

    /**
     * Create an instance of {@link CardSecurityData }
     * 
     */
    public CardSecurityData createCardSecurityData() {
        return new CardSecurityData();
    }

    /**
     * Create an instance of {@link ReturnById }
     * 
     */
    public ReturnById createReturnById() {
        return new ReturnById();
    }

    /**
     * Create an instance of {@link BankcardTransactionResponse }
     * 
     */
    public BankcardTransactionResponse createBankcardTransactionResponse() {
        return new BankcardTransactionResponse();
    }

    /**
     * Create an instance of {@link ReturnUnlinkedResponse }
     * 
     */
    public ReturnUnlinkedResponse createReturnUnlinkedResponse() {
        return new ReturnUnlinkedResponse();
    }

    /**
     * Create an instance of {@link UndoResponse }
     * 
     */
    public UndoResponse createUndoResponse() {
        return new UndoResponse();
    }

    /**
     * Create an instance of {@link BankcardCapture }
     * 
     */
    public BankcardCapture createBankcardCapture() {
        return new BankcardCapture();
    }

    /**
     * Create an instance of {@link ItemizedTax }
     * 
     */
    public ItemizedTax createItemizedTax() {
        return new ItemizedTax();
    }

    /**
     * Create an instance of {@link CaptureResponse }
     * 
     */
    public CaptureResponse createCaptureResponse() {
        return new CaptureResponse();
    }

    /**
     * Create an instance of {@link BankcardCaptureResponse }
     * 
     */
    public BankcardCaptureResponse createBankcardCaptureResponse() {
        return new BankcardCaptureResponse();
    }

    /**
     * Create an instance of {@link InternetTransactionData }
     * 
     */
    public InternetTransactionData createInternetTransactionData() {
        return new InternetTransactionData();
    }

    /**
     * Create an instance of {@link BankcardTenderData }
     * 
     */
    public BankcardTenderData createBankcardTenderData() {
        return new BankcardTenderData();
    }

    /**
     * Create an instance of {@link QueryAccount }
     * 
     */
    public QueryAccount createQueryAccount() {
        return new QueryAccount();
    }

    /**
     * Create an instance of {@link Totals }
     * 
     */
    public Totals createTotals() {
        return new Totals();
    }

    /**
     * Create an instance of {@link VerifyResponse }
     * 
     */
    public VerifyResponse createVerifyResponse() {
        return new VerifyResponse();
    }

    /**
     * Create an instance of {@link ReturnByIdResponse }
     * 
     */
    public ReturnByIdResponse createReturnByIdResponse() {
        return new ReturnByIdResponse();
    }

    /**
     * Create an instance of {@link LineItemDetail }
     * 
     */
    public LineItemDetail createLineItemDetail() {
        return new LineItemDetail();
    }

    /**
     * Create an instance of {@link AdjustResponse }
     * 
     */
    public AdjustResponse createAdjustResponse() {
        return new AdjustResponse();
    }

    /**
     * Create an instance of {@link QueryAccountResponse }
     * 
     */
    public QueryAccountResponse createQueryAccountResponse() {
        return new QueryAccountResponse();
    }

    /**
     * Create an instance of {@link BankcardTransactionData }
     * 
     */
    public BankcardTransactionData createBankcardTransactionData() {
        return new BankcardTransactionData();
    }

    /**
     * Create an instance of {@link AuthorizeAndCapture }
     * 
     */
    public AuthorizeAndCapture createAuthorizeAndCapture() {
        return new AuthorizeAndCapture();
    }

    /**
     * Create an instance of {@link CaptureSelective }
     * 
     */
    public CaptureSelective createCaptureSelective() {
        return new CaptureSelective();
    }

    /**
     * Create an instance of {@link AuthorizeResponse }
     * 
     */
    public AuthorizeResponse createAuthorizeResponse() {
        return new AuthorizeResponse();
    }

    /**
     * Create an instance of {@link AVSResult }
     * 
     */
    public AVSResult createAVSResult() {
        return new AVSResult();
    }

    /**
     * Create an instance of {@link AuthorizeAndCaptureResponse }
     * 
     */
    public AuthorizeAndCaptureResponse createAuthorizeAndCaptureResponse() {
        return new AuthorizeAndCaptureResponse();
    }

    /**
     * Create an instance of {@link Authorize }
     * 
     */
    public Authorize createAuthorize() {
        return new Authorize();
    }

    /**
     * Create an instance of {@link ArrayOfItemizedTax }
     * 
     */
    public ArrayOfItemizedTax createArrayOfItemizedTax() {
        return new ArrayOfItemizedTax();
    }

    /**
     * Create an instance of {@link BankcardReturn }
     * 
     */
    public BankcardReturn createBankcardReturn() {
        return new BankcardReturn();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxExempt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "TaxExempt")
    public JAXBElement<TaxExempt> createTaxExempt(TaxExempt value) {
        return new JAXBElement<TaxExempt>(_TaxExempt_QNAME, TaxExempt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfItemizedTax }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "ArrayOfItemizedTax")
    public JAXBElement<ArrayOfItemizedTax> createArrayOfItemizedTax(ArrayOfItemizedTax value) {
        return new JAXBElement<ArrayOfItemizedTax>(_ArrayOfItemizedTax_QNAME, ArrayOfItemizedTax.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdviceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "AdviceResponse")
    public JAXBElement<AdviceResponse> createAdviceResponse(AdviceResponse value) {
        return new JAXBElement<AdviceResponse>(_AdviceResponse_QNAME, AdviceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddressResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "AddressResult")
    public JAXBElement<AddressResult> createAddressResult(AddressResult value) {
        return new JAXBElement<AddressResult>(_AddressResult_QNAME, AddressResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionSummaryData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "TransactionSummaryData")
    public JAXBElement<TransactionSummaryData> createTransactionSummaryData(TransactionSummaryData value) {
        return new JAXBElement<TransactionSummaryData>(_TransactionSummaryData_QNAME, TransactionSummaryData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "AccountType")
    public JAXBElement<AccountType> createAccountType(AccountType value) {
        return new JAXBElement<AccountType>(_AccountType_QNAME, AccountType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardTenderData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BankcardTenderData")
    public JAXBElement<BankcardTenderData> createBankcardTenderData(BankcardTenderData value) {
        return new JAXBElement<BankcardTenderData>(_BankcardTenderData_QNAME, BankcardTenderData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IndustryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "IndustryType")
    public JAXBElement<IndustryType> createIndustryType(IndustryType value) {
        return new JAXBElement<IndustryType>(_IndustryType_QNAME, IndustryType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardCaptureResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BankcardCaptureResponse")
    public JAXBElement<BankcardCaptureResponse> createBankcardCaptureResponse(BankcardCaptureResponse value) {
        return new JAXBElement<BankcardCaptureResponse>(_BankcardCaptureResponse_QNAME, BankcardCaptureResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Level2Data }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "Level2Data")
    public JAXBElement<Level2Data> createLevel2Data(Level2Data value) {
        return new JAXBElement<Level2Data>(_Level2Data_QNAME, Level2Data.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostalCodeResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "PostalCodeResult")
    public JAXBElement<PostalCodeResult> createPostalCodeResult(PostalCodeResult value) {
        return new JAXBElement<PostalCodeResult>(_PostalCodeResult_QNAME, PostalCodeResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineItemDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "LineItemDetail")
    public JAXBElement<LineItemDetail> createLineItemDetail(LineItemDetail value) {
        return new JAXBElement<LineItemDetail>(_LineItemDetail_QNAME, LineItemDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnedACI }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "ReturnedACI")
    public JAXBElement<ReturnedACI> createReturnedACI(ReturnedACI value) {
        return new JAXBElement<ReturnedACI>(_ReturnedACI_QNAME, ReturnedACI.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntryMode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "EntryMode")
    public JAXBElement<EntryMode> createEntryMode(EntryMode value) {
        return new JAXBElement<EntryMode>(_EntryMode_QNAME, EntryMode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardTransactionData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BankcardTransactionData")
    public JAXBElement<BankcardTransactionData> createBankcardTransactionData(BankcardTransactionData value) {
        return new JAXBElement<BankcardTransactionData>(_BankcardTransactionData_QNAME, BankcardTransactionData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BillPayment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BillPayment")
    public JAXBElement<BillPayment> createBillPayment(BillPayment value) {
        return new JAXBElement<BillPayment>(_BillPayment_QNAME, BillPayment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Totals }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "Totals")
    public JAXBElement<Totals> createTotals(Totals value) {
        return new JAXBElement<Totals>(_Totals_QNAME, Totals.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GoodsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "GoodsType")
    public JAXBElement<GoodsType> createGoodsType(GoodsType value) {
        return new JAXBElement<GoodsType>(_GoodsType_QNAME, GoodsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InternetTransactionData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "InternetTransactionData")
    public JAXBElement<InternetTransactionData> createInternetTransactionData(InternetTransactionData value) {
        return new JAXBElement<InternetTransactionData>(_InternetTransactionData_QNAME, InternetTransactionData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeTaxType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "TypeTaxType")
    public JAXBElement<TypeTaxType> createTypeTaxType(TypeTaxType value) {
        return new JAXBElement<TypeTaxType>(_TypeTaxType_QNAME, TypeTaxType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommercialCardResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "CommercialCardResponse")
    public JAXBElement<CommercialCardResponse> createCommercialCardResponse(CommercialCardResponse value) {
        return new JAXBElement<CommercialCardResponse>(_CommercialCardResponse_QNAME, CommercialCardResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Resubmit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "Resubmit")
    public JAXBElement<Resubmit> createResubmit(Resubmit value) {
        return new JAXBElement<Resubmit>(_Resubmit_QNAME, Resubmit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CVResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "CVResult")
    public JAXBElement<CVResult> createCVResult(CVResult value) {
        return new JAXBElement<CVResult>(_CVResult_QNAME, CVResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeCardType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "TypeCardType")
    public JAXBElement<TypeCardType> createTypeCardType(TypeCardType value) {
        return new JAXBElement<TypeCardType>(_TypeCardType_QNAME, TypeCardType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemizedTax }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "ItemizedTax")
    public JAXBElement<ItemizedTax> createItemizedTax(ItemizedTax value) {
        return new JAXBElement<ItemizedTax>(_ItemizedTax_QNAME, ItemizedTax.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CVDataProvided }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "CVDataProvided")
    public JAXBElement<CVDataProvided> createCVDataProvided(CVDataProvided value) {
        return new JAXBElement<CVDataProvided>(_CVDataProvided_QNAME, CVDataProvided.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestAdvice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "RequestAdvice")
    public JAXBElement<RequestAdvice> createRequestAdvice(RequestAdvice value) {
        return new JAXBElement<RequestAdvice>(_RequestAdvice_QNAME, RequestAdvice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardReturn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BankcardReturn")
    public JAXBElement<BankcardReturn> createBankcardReturn(BankcardReturn value) {
        return new JAXBElement<BankcardReturn>(_BankcardReturn_QNAME, BankcardReturn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardTransaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BankcardTransaction")
    public JAXBElement<BankcardTransaction> createBankcardTransaction(BankcardTransaction value) {
        return new JAXBElement<BankcardTransaction>(_BankcardTransaction_QNAME, BankcardTransaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExistingDebt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "ExistingDebt")
    public JAXBElement<ExistingDebt> createExistingDebt(ExistingDebt value) {
        return new JAXBElement<ExistingDebt>(_ExistingDebt_QNAME, ExistingDebt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManagedBillingInstallments }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "ManagedBillingInstallments")
    public JAXBElement<ManagedBillingInstallments> createManagedBillingInstallments(ManagedBillingInstallments value) {
        return new JAXBElement<ManagedBillingInstallments>(_ManagedBillingInstallments_QNAME, ManagedBillingInstallments.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerPresent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "CustomerPresent")
    public JAXBElement<CustomerPresent> createCustomerPresent(CustomerPresent value) {
        return new JAXBElement<CustomerPresent>(_CustomerPresent_QNAME, CustomerPresent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestACI }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "RequestACI")
    public JAXBElement<RequestACI> createRequestACI(RequestACI value) {
        return new JAXBElement<RequestACI>(_RequestACI_QNAME, RequestACI.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardCapture }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BankcardCapture")
    public JAXBElement<BankcardCapture> createBankcardCapture(BankcardCapture value) {
        return new JAXBElement<BankcardCapture>(_BankcardCapture_QNAME, BankcardCapture.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfLineItemDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "ArrayOfLineItemDetail")
    public JAXBElement<ArrayOfLineItemDetail> createArrayOfLineItemDetail(ArrayOfLineItemDetail value) {
        return new JAXBElement<ArrayOfLineItemDetail>(_ArrayOfLineItemDetail_QNAME, ArrayOfLineItemDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tax }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "Tax")
    public JAXBElement<Tax> createTax(Tax value) {
        return new JAXBElement<Tax>(_Tax_QNAME, Tax.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AVSData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "AVSData")
    public JAXBElement<AVSData> createAVSData(AVSData value) {
        return new JAXBElement<AVSData>(_AVSData_QNAME, AVSData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsTaxExempt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "IsTaxExempt")
    public JAXBElement<IsTaxExempt> createIsTaxExempt(IsTaxExempt value) {
        return new JAXBElement<IsTaxExempt>(_IsTaxExempt_QNAME, IsTaxExempt.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChargeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "ChargeType")
    public JAXBElement<ChargeType> createChargeType(ChargeType value) {
        return new JAXBElement<ChargeType>(_ChargeType_QNAME, ChargeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AVSResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "AVSResult")
    public JAXBElement<AVSResult> createAVSResult(AVSResult value) {
        return new JAXBElement<AVSResult>(_AVSResult_QNAME, AVSResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "CardData")
    public JAXBElement<CardData> createCardData(CardData value) {
        return new JAXBElement<CardData>(_CardData_QNAME, CardData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardUndo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BankcardUndo")
    public JAXBElement<BankcardUndo> createBankcardUndo(BankcardUndo value) {
        return new JAXBElement<BankcardUndo>(_BankcardUndo_QNAME, BankcardUndo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestCommercialCard }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "RequestCommercialCard")
    public JAXBElement<RequestCommercialCard> createRequestCommercialCard(RequestCommercialCard value) {
        return new JAXBElement<RequestCommercialCard>(_RequestCommercialCard_QNAME, RequestCommercialCard.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankcardTransactionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "BankcardTransactionResponse")
    public JAXBElement<BankcardTransactionResponse> createBankcardTransactionResponse(BankcardTransactionResponse value) {
        return new JAXBElement<BankcardTransactionResponse>(_BankcardTransactionResponse_QNAME, BankcardTransactionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CardSecurityData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "CardSecurityData")
    public JAXBElement<CardSecurityData> createCardSecurityData(CardSecurityData value) {
        return new JAXBElement<CardSecurityData>(_CardSecurityData_QNAME, CardSecurityData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PINDebitUndoReason }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "PINDebitUndoReason")
    public JAXBElement<PINDebitUndoReason> createPINDebitUndoReason(PINDebitUndoReason value) {
        return new JAXBElement<PINDebitUndoReason>(_PINDebitUndoReason_QNAME, PINDebitUndoReason.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Interval }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "Interval")
    public JAXBElement<Interval> createInterval(Interval value) {
        return new JAXBElement<Interval>(_Interval_QNAME, Interval.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ManagedBilling }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "ManagedBilling")
    public JAXBElement<ManagedBilling> createManagedBilling(ManagedBilling value) {
        return new JAXBElement<ManagedBilling>(_ManagedBilling_QNAME, ManagedBilling.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeUnitOfMeasure }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", name = "TypeUnitOfMeasure")
    public JAXBElement<TypeUnitOfMeasure> createTypeUnitOfMeasure(TypeUnitOfMeasure value) {
        return new JAXBElement<TypeUnitOfMeasure>(_TypeUnitOfMeasure_QNAME, TypeUnitOfMeasure.class, null, value);
    }

}
