
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypeUnitOfMeasure.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypeUnitOfMeasure">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NotSet"/>
 *     &lt;enumeration value="Acre"/>
 *     &lt;enumeration value="AmpereHour"/>
 *     &lt;enumeration value="Ampere"/>
 *     &lt;enumeration value="Year"/>
 *     &lt;enumeration value="TroyOunceOrApothecariesOunce"/>
 *     &lt;enumeration value="Are"/>
 *     &lt;enumeration value="AlcoholicStrengthByMass"/>
 *     &lt;enumeration value="AlcoholicStrengthByVolume"/>
 *     &lt;enumeration value="StandardAtmosphere"/>
 *     &lt;enumeration value="TechnicalAtmosphere"/>
 *     &lt;enumeration value="Bar"/>
 *     &lt;enumeration value="BoardFoot"/>
 *     &lt;enumeration value="BrakeHorsePower"/>
 *     &lt;enumeration value="BillionEURTrillionUS"/>
 *     &lt;enumeration value="DryBarrelUS"/>
 *     &lt;enumeration value="BarrelUSPetroleumEtc"/>
 *     &lt;enumeration value="Becquerel"/>
 *     &lt;enumeration value="BritishThermalUnit"/>
 *     &lt;enumeration value="BushelUS"/>
 *     &lt;enumeration value="BushelUK"/>
 *     &lt;enumeration value="CarryingCapacityInMetricTon"/>
 *     &lt;enumeration value="Candela"/>
 *     &lt;enumeration value="DegreeCelsius"/>
 *     &lt;enumeration value="Hundred"/>
 *     &lt;enumeration value="Centigram"/>
 *     &lt;enumeration value="CoulombPerKilogram"/>
 *     &lt;enumeration value="HundredLeave"/>
 *     &lt;enumeration value="Centilitre"/>
 *     &lt;enumeration value="SquareCentimetre"/>
 *     &lt;enumeration value="CubicCentimetre"/>
 *     &lt;enumeration value="Centimetre"/>
 *     &lt;enumeration value="HundredPack"/>
 *     &lt;enumeration value="CentalUK"/>
 *     &lt;enumeration value="Coulomb"/>
 *     &lt;enumeration value="MetricCarat"/>
 *     &lt;enumeration value="Curie"/>
 *     &lt;enumeration value="HundredPoundsCWTHundredWeightUS"/>
 *     &lt;enumeration value="HundredWeightUK"/>
 *     &lt;enumeration value="Decare"/>
 *     &lt;enumeration value="TenDay"/>
 *     &lt;enumeration value="Day"/>
 *     &lt;enumeration value="Decade"/>
 *     &lt;enumeration value="Decilitre"/>
 *     &lt;enumeration value="SquareDecimetre"/>
 *     &lt;enumeration value="CubicDecimetre"/>
 *     &lt;enumeration value="Decimetre"/>
 *     &lt;enumeration value="DozenPiece"/>
 *     &lt;enumeration value="DozenPair"/>
 *     &lt;enumeration value="DisplacementTonnage"/>
 *     &lt;enumeration value="DramUS"/>
 *     &lt;enumeration value="DramUK"/>
 *     &lt;enumeration value="DozenRoll"/>
 *     &lt;enumeration value="DrachmUK"/>
 *     &lt;enumeration value="DecitonneCentnerMetric100KgQuintalMetric100Kg"/>
 *     &lt;enumeration value="Pennyweight"/>
 *     &lt;enumeration value="Dozen"/>
 *     &lt;enumeration value="DozenPack"/>
 *     &lt;enumeration value="DegreeFahrenheit"/>
 *     &lt;enumeration value="Farad"/>
 *     &lt;enumeration value="Foot"/>
 *     &lt;enumeration value="SquareFoot"/>
 *     &lt;enumeration value="CubicFoot"/>
 *     &lt;enumeration value="Gigabecquerel"/>
 *     &lt;enumeration value="GramOfFissileIsotope"/>
 *     &lt;enumeration value="GreatGross"/>
 *     &lt;enumeration value="GillUS"/>
 *     &lt;enumeration value="GillUK"/>
 *     &lt;enumeration value="DryGallonUS"/>
 *     &lt;enumeration value="GallonUK"/>
 *     &lt;enumeration value="GallonUS"/>
 *     &lt;enumeration value="Gram"/>
 *     &lt;enumeration value="Grain"/>
 *     &lt;enumeration value="Gross"/>
 *     &lt;enumeration value="GrossRegisterTon"/>
 *     &lt;enumeration value="GigawattHour"/>
 *     &lt;enumeration value="Hectare"/>
 *     &lt;enumeration value="Hectobar"/>
 *     &lt;enumeration value="HundredBox"/>
 *     &lt;enumeration value="Hectogram"/>
 *     &lt;enumeration value="HundredInternationalUnit"/>
 *     &lt;enumeration value="Hectolitre"/>
 *     &lt;enumeration value="MillionCubicMetre"/>
 *     &lt;enumeration value="Hectometre"/>
 *     &lt;enumeration value="HectolitreOfPureAlcohol"/>
 *     &lt;enumeration value="Hertz"/>
 *     &lt;enumeration value="Hour"/>
 *     &lt;enumeration value="Inch"/>
 *     &lt;enumeration value="SquareInch"/>
 *     &lt;enumeration value="CubicInch"/>
 *     &lt;enumeration value="Joule"/>
 *     &lt;enumeration value="Kilobar"/>
 *     &lt;enumeration value="Kelvin"/>
 *     &lt;enumeration value="Kilogram"/>
 *     &lt;enumeration value="KilogramPerSecond"/>
 *     &lt;enumeration value="Kilohertz"/>
 *     &lt;enumeration value="Kilojoule"/>
 *     &lt;enumeration value="KilometrePerHour"/>
 *     &lt;enumeration value="SquareKilometre"/>
 *     &lt;enumeration value="KilogramPerCubicMetre"/>
 *     &lt;enumeration value="Kilometre"/>
 *     &lt;enumeration value="KilogramOfNitrogen"/>
 *     &lt;enumeration value="KilogramNamedSubstance"/>
 *     &lt;enumeration value="Knot"/>
 *     &lt;enumeration value="Kilopascal"/>
 *     &lt;enumeration value="KilogramOfPotassiumHydroxideCausticPotash"/>
 *     &lt;enumeration value="KilogramOfPotassiumOxide"/>
 *     &lt;enumeration value="KilogramOfPhosphorusPentoxidePhosphoricAnhydride"/>
 *     &lt;enumeration value="KilogramOfSubstance90PercentDry"/>
 *     &lt;enumeration value="KilogramOfSodiumHydroxideCausticSoda"/>
 *     &lt;enumeration value="Kilotonne"/>
 *     &lt;enumeration value="KilogramOfUranium"/>
 *     &lt;enumeration value="KilovoltAmpere"/>
 *     &lt;enumeration value="Kilovar"/>
 *     &lt;enumeration value="Kilovolt"/>
 *     &lt;enumeration value="KilowattHour"/>
 *     &lt;enumeration value="Kilowatt"/>
 *     &lt;enumeration value="Pound"/>
 *     &lt;enumeration value="TroyPoundUS"/>
 *     &lt;enumeration value="Leaf"/>
 *     &lt;enumeration value="LitreOfPureAlcohol"/>
 *     &lt;enumeration value="TonUKorLongTonUS"/>
 *     &lt;enumeration value="Litre"/>
 *     &lt;enumeration value="Lumen"/>
 *     &lt;enumeration value="Lux"/>
 *     &lt;enumeration value="MegaLitre"/>
 *     &lt;enumeration value="Megametre"/>
 *     &lt;enumeration value="Megawatt"/>
 *     &lt;enumeration value="ThousandStandardBrickEquivalent"/>
 *     &lt;enumeration value="ThousandBoardFeet"/>
 *     &lt;enumeration value="Millibar"/>
 *     &lt;enumeration value="Millicurie"/>
 *     &lt;enumeration value="Milligram"/>
 *     &lt;enumeration value="Megahertz"/>
 *     &lt;enumeration value="SquareMile"/>
 *     &lt;enumeration value="Thousand"/>
 *     &lt;enumeration value="Minute"/>
 *     &lt;enumeration value="Million"/>
 *     &lt;enumeration value="MillionInternationalUnit"/>
 *     &lt;enumeration value="MilliardBillionUS"/>
 *     &lt;enumeration value="Millilitre"/>
 *     &lt;enumeration value="SquareMillimetre"/>
 *     &lt;enumeration value="CubicMillimetre"/>
 *     &lt;enumeration value="Millimetre"/>
 *     &lt;enumeration value="Month"/>
 *     &lt;enumeration value="Megapascal"/>
 *     &lt;enumeration value="CubicMetrePerHour"/>
 *     &lt;enumeration value="CubicMetrePerSecond"/>
 *     &lt;enumeration value="MetrePerSecondSquared"/>
 *     &lt;enumeration value="SquareMetre"/>
 *     &lt;enumeration value="CubicMetre"/>
 *     &lt;enumeration value="Metre"/>
 *     &lt;enumeration value="MetrePerSecond"/>
 *     &lt;enumeration value="MegavoltAmpere"/>
 *     &lt;enumeration value="MegawattHour1000KWH"/>
 *     &lt;enumeration value="NumberOfArticles"/>
 *     &lt;enumeration value="NumberOfBobbins"/>
 *     &lt;enumeration value="NumberOfCells"/>
 *     &lt;enumeration value="Newton"/>
 *     &lt;enumeration value="NumberOfInternationalUnits"/>
 *     &lt;enumeration value="NauticalMile"/>
 *     &lt;enumeration value="NumberOfPacks"/>
 *     &lt;enumeration value="NumberOfParcels"/>
 *     &lt;enumeration value="NumberOfPairs"/>
 *     &lt;enumeration value="NumberOfParts"/>
 *     &lt;enumeration value="NumberOfRolls"/>
 *     &lt;enumeration value="NetRegisterTon"/>
 *     &lt;enumeration value="Ohm"/>
 *     &lt;enumeration value="Ounce"/>
 *     &lt;enumeration value="FluidOunceUS"/>
 *     &lt;enumeration value="FluidOunceUK"/>
 *     &lt;enumeration value="Pascal"/>
 *     &lt;enumeration value="Piece"/>
 *     &lt;enumeration value="ProofGallon"/>
 *     &lt;enumeration value="DryPintUS"/>
 *     &lt;enumeration value="PintUK"/>
 *     &lt;enumeration value="LiquidPintUS"/>
 *     &lt;enumeration value="QuarterOfAYear"/>
 *     &lt;enumeration value="DryQuartUS"/>
 *     &lt;enumeration value="QuartUK"/>
 *     &lt;enumeration value="LiquidQuartUS"/>
 *     &lt;enumeration value="QuarterUK"/>
 *     &lt;enumeration value="RevolutionsPerMinute"/>
 *     &lt;enumeration value="RevolutionsPerSecond"/>
 *     &lt;enumeration value="HalfYear6Months"/>
 *     &lt;enumeration value="Score"/>
 *     &lt;enumeration value="Scruple"/>
 *     &lt;enumeration value="Second"/>
 *     &lt;enumeration value="Set"/>
 *     &lt;enumeration value="ShippingTon"/>
 *     &lt;enumeration value="Siemens"/>
 *     &lt;enumeration value="MileStatuteMile"/>
 *     &lt;enumeration value="ShortStandard7200Matches"/>
 *     &lt;enumeration value="StoneUK"/>
 *     &lt;enumeration value="TonUSOrShortTonUKUS"/>
 *     &lt;enumeration value="KiloampereHourThousandAmpereHour"/>
 *     &lt;enumeration value="TonneMetricTon"/>
 *     &lt;enumeration value="TenPair"/>
 *     &lt;enumeration value="ThousandCubicMetrePerDay"/>
 *     &lt;enumeration value="TrillionEUR"/>
 *     &lt;enumeration value="TonneOfSubstance90PercentDry"/>
 *     &lt;enumeration value="TonOfSteamPerHour"/>
 *     &lt;enumeration value="Volt"/>
 *     &lt;enumeration value="Cord"/>
 *     &lt;enumeration value="Weber"/>
 *     &lt;enumeration value="Week"/>
 *     &lt;enumeration value="WattHour"/>
 *     &lt;enumeration value="Standard"/>
 *     &lt;enumeration value="Watt"/>
 *     &lt;enumeration value="SquareYard"/>
 *     &lt;enumeration value="CubicYard"/>
 *     &lt;enumeration value="Yard"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypeUnitOfMeasure")
@XmlEnum
public enum TypeUnitOfMeasure {

    @XmlEnumValue("NotSet")
    NOT_SET("NotSet"),
    @XmlEnumValue("Acre")
    ACRE("Acre"),
    @XmlEnumValue("AmpereHour")
    AMPERE_HOUR("AmpereHour"),
    @XmlEnumValue("Ampere")
    AMPERE("Ampere"),
    @XmlEnumValue("Year")
    YEAR("Year"),
    @XmlEnumValue("TroyOunceOrApothecariesOunce")
    TROY_OUNCE_OR_APOTHECARIES_OUNCE("TroyOunceOrApothecariesOunce"),
    @XmlEnumValue("Are")
    ARE("Are"),
    @XmlEnumValue("AlcoholicStrengthByMass")
    ALCOHOLIC_STRENGTH_BY_MASS("AlcoholicStrengthByMass"),
    @XmlEnumValue("AlcoholicStrengthByVolume")
    ALCOHOLIC_STRENGTH_BY_VOLUME("AlcoholicStrengthByVolume"),
    @XmlEnumValue("StandardAtmosphere")
    STANDARD_ATMOSPHERE("StandardAtmosphere"),
    @XmlEnumValue("TechnicalAtmosphere")
    TECHNICAL_ATMOSPHERE("TechnicalAtmosphere"),
    @XmlEnumValue("Bar")
    BAR("Bar"),
    @XmlEnumValue("BoardFoot")
    BOARD_FOOT("BoardFoot"),
    @XmlEnumValue("BrakeHorsePower")
    BRAKE_HORSE_POWER("BrakeHorsePower"),
    @XmlEnumValue("BillionEURTrillionUS")
    BILLION_EUR_TRILLION_US("BillionEURTrillionUS"),
    @XmlEnumValue("DryBarrelUS")
    DRY_BARREL_US("DryBarrelUS"),
    @XmlEnumValue("BarrelUSPetroleumEtc")
    BARREL_US_PETROLEUM_ETC("BarrelUSPetroleumEtc"),
    @XmlEnumValue("Becquerel")
    BECQUEREL("Becquerel"),
    @XmlEnumValue("BritishThermalUnit")
    BRITISH_THERMAL_UNIT("BritishThermalUnit"),
    @XmlEnumValue("BushelUS")
    BUSHEL_US("BushelUS"),
    @XmlEnumValue("BushelUK")
    BUSHEL_UK("BushelUK"),
    @XmlEnumValue("CarryingCapacityInMetricTon")
    CARRYING_CAPACITY_IN_METRIC_TON("CarryingCapacityInMetricTon"),
    @XmlEnumValue("Candela")
    CANDELA("Candela"),
    @XmlEnumValue("DegreeCelsius")
    DEGREE_CELSIUS("DegreeCelsius"),
    @XmlEnumValue("Hundred")
    HUNDRED("Hundred"),
    @XmlEnumValue("Centigram")
    CENTIGRAM("Centigram"),
    @XmlEnumValue("CoulombPerKilogram")
    COULOMB_PER_KILOGRAM("CoulombPerKilogram"),
    @XmlEnumValue("HundredLeave")
    HUNDRED_LEAVE("HundredLeave"),
    @XmlEnumValue("Centilitre")
    CENTILITRE("Centilitre"),
    @XmlEnumValue("SquareCentimetre")
    SQUARE_CENTIMETRE("SquareCentimetre"),
    @XmlEnumValue("CubicCentimetre")
    CUBIC_CENTIMETRE("CubicCentimetre"),
    @XmlEnumValue("Centimetre")
    CENTIMETRE("Centimetre"),
    @XmlEnumValue("HundredPack")
    HUNDRED_PACK("HundredPack"),
    @XmlEnumValue("CentalUK")
    CENTAL_UK("CentalUK"),
    @XmlEnumValue("Coulomb")
    COULOMB("Coulomb"),
    @XmlEnumValue("MetricCarat")
    METRIC_CARAT("MetricCarat"),
    @XmlEnumValue("Curie")
    CURIE("Curie"),
    @XmlEnumValue("HundredPoundsCWTHundredWeightUS")
    HUNDRED_POUNDS_CWT_HUNDRED_WEIGHT_US("HundredPoundsCWTHundredWeightUS"),
    @XmlEnumValue("HundredWeightUK")
    HUNDRED_WEIGHT_UK("HundredWeightUK"),
    @XmlEnumValue("Decare")
    DECARE("Decare"),
    @XmlEnumValue("TenDay")
    TEN_DAY("TenDay"),
    @XmlEnumValue("Day")
    DAY("Day"),
    @XmlEnumValue("Decade")
    DECADE("Decade"),
    @XmlEnumValue("Decilitre")
    DECILITRE("Decilitre"),
    @XmlEnumValue("SquareDecimetre")
    SQUARE_DECIMETRE("SquareDecimetre"),
    @XmlEnumValue("CubicDecimetre")
    CUBIC_DECIMETRE("CubicDecimetre"),
    @XmlEnumValue("Decimetre")
    DECIMETRE("Decimetre"),
    @XmlEnumValue("DozenPiece")
    DOZEN_PIECE("DozenPiece"),
    @XmlEnumValue("DozenPair")
    DOZEN_PAIR("DozenPair"),
    @XmlEnumValue("DisplacementTonnage")
    DISPLACEMENT_TONNAGE("DisplacementTonnage"),
    @XmlEnumValue("DramUS")
    DRAM_US("DramUS"),
    @XmlEnumValue("DramUK")
    DRAM_UK("DramUK"),
    @XmlEnumValue("DozenRoll")
    DOZEN_ROLL("DozenRoll"),
    @XmlEnumValue("DrachmUK")
    DRACHM_UK("DrachmUK"),
    @XmlEnumValue("DecitonneCentnerMetric100KgQuintalMetric100Kg")
    DECITONNE_CENTNER_METRIC_100_KG_QUINTAL_METRIC_100_KG("DecitonneCentnerMetric100KgQuintalMetric100Kg"),
    @XmlEnumValue("Pennyweight")
    PENNYWEIGHT("Pennyweight"),
    @XmlEnumValue("Dozen")
    DOZEN("Dozen"),
    @XmlEnumValue("DozenPack")
    DOZEN_PACK("DozenPack"),
    @XmlEnumValue("DegreeFahrenheit")
    DEGREE_FAHRENHEIT("DegreeFahrenheit"),
    @XmlEnumValue("Farad")
    FARAD("Farad"),
    @XmlEnumValue("Foot")
    FOOT("Foot"),
    @XmlEnumValue("SquareFoot")
    SQUARE_FOOT("SquareFoot"),
    @XmlEnumValue("CubicFoot")
    CUBIC_FOOT("CubicFoot"),
    @XmlEnumValue("Gigabecquerel")
    GIGABECQUEREL("Gigabecquerel"),
    @XmlEnumValue("GramOfFissileIsotope")
    GRAM_OF_FISSILE_ISOTOPE("GramOfFissileIsotope"),
    @XmlEnumValue("GreatGross")
    GREAT_GROSS("GreatGross"),
    @XmlEnumValue("GillUS")
    GILL_US("GillUS"),
    @XmlEnumValue("GillUK")
    GILL_UK("GillUK"),
    @XmlEnumValue("DryGallonUS")
    DRY_GALLON_US("DryGallonUS"),
    @XmlEnumValue("GallonUK")
    GALLON_UK("GallonUK"),
    @XmlEnumValue("GallonUS")
    GALLON_US("GallonUS"),
    @XmlEnumValue("Gram")
    GRAM("Gram"),
    @XmlEnumValue("Grain")
    GRAIN("Grain"),
    @XmlEnumValue("Gross")
    GROSS("Gross"),
    @XmlEnumValue("GrossRegisterTon")
    GROSS_REGISTER_TON("GrossRegisterTon"),
    @XmlEnumValue("GigawattHour")
    GIGAWATT_HOUR("GigawattHour"),
    @XmlEnumValue("Hectare")
    HECTARE("Hectare"),
    @XmlEnumValue("Hectobar")
    HECTOBAR("Hectobar"),
    @XmlEnumValue("HundredBox")
    HUNDRED_BOX("HundredBox"),
    @XmlEnumValue("Hectogram")
    HECTOGRAM("Hectogram"),
    @XmlEnumValue("HundredInternationalUnit")
    HUNDRED_INTERNATIONAL_UNIT("HundredInternationalUnit"),
    @XmlEnumValue("Hectolitre")
    HECTOLITRE("Hectolitre"),
    @XmlEnumValue("MillionCubicMetre")
    MILLION_CUBIC_METRE("MillionCubicMetre"),
    @XmlEnumValue("Hectometre")
    HECTOMETRE("Hectometre"),
    @XmlEnumValue("HectolitreOfPureAlcohol")
    HECTOLITRE_OF_PURE_ALCOHOL("HectolitreOfPureAlcohol"),
    @XmlEnumValue("Hertz")
    HERTZ("Hertz"),
    @XmlEnumValue("Hour")
    HOUR("Hour"),
    @XmlEnumValue("Inch")
    INCH("Inch"),
    @XmlEnumValue("SquareInch")
    SQUARE_INCH("SquareInch"),
    @XmlEnumValue("CubicInch")
    CUBIC_INCH("CubicInch"),
    @XmlEnumValue("Joule")
    JOULE("Joule"),
    @XmlEnumValue("Kilobar")
    KILOBAR("Kilobar"),
    @XmlEnumValue("Kelvin")
    KELVIN("Kelvin"),
    @XmlEnumValue("Kilogram")
    KILOGRAM("Kilogram"),
    @XmlEnumValue("KilogramPerSecond")
    KILOGRAM_PER_SECOND("KilogramPerSecond"),
    @XmlEnumValue("Kilohertz")
    KILOHERTZ("Kilohertz"),
    @XmlEnumValue("Kilojoule")
    KILOJOULE("Kilojoule"),
    @XmlEnumValue("KilometrePerHour")
    KILOMETRE_PER_HOUR("KilometrePerHour"),
    @XmlEnumValue("SquareKilometre")
    SQUARE_KILOMETRE("SquareKilometre"),
    @XmlEnumValue("KilogramPerCubicMetre")
    KILOGRAM_PER_CUBIC_METRE("KilogramPerCubicMetre"),
    @XmlEnumValue("Kilometre")
    KILOMETRE("Kilometre"),
    @XmlEnumValue("KilogramOfNitrogen")
    KILOGRAM_OF_NITROGEN("KilogramOfNitrogen"),
    @XmlEnumValue("KilogramNamedSubstance")
    KILOGRAM_NAMED_SUBSTANCE("KilogramNamedSubstance"),
    @XmlEnumValue("Knot")
    KNOT("Knot"),
    @XmlEnumValue("Kilopascal")
    KILOPASCAL("Kilopascal"),
    @XmlEnumValue("KilogramOfPotassiumHydroxideCausticPotash")
    KILOGRAM_OF_POTASSIUM_HYDROXIDE_CAUSTIC_POTASH("KilogramOfPotassiumHydroxideCausticPotash"),
    @XmlEnumValue("KilogramOfPotassiumOxide")
    KILOGRAM_OF_POTASSIUM_OXIDE("KilogramOfPotassiumOxide"),
    @XmlEnumValue("KilogramOfPhosphorusPentoxidePhosphoricAnhydride")
    KILOGRAM_OF_PHOSPHORUS_PENTOXIDE_PHOSPHORIC_ANHYDRIDE("KilogramOfPhosphorusPentoxidePhosphoricAnhydride"),
    @XmlEnumValue("KilogramOfSubstance90PercentDry")
    KILOGRAM_OF_SUBSTANCE_90_PERCENT_DRY("KilogramOfSubstance90PercentDry"),
    @XmlEnumValue("KilogramOfSodiumHydroxideCausticSoda")
    KILOGRAM_OF_SODIUM_HYDROXIDE_CAUSTIC_SODA("KilogramOfSodiumHydroxideCausticSoda"),
    @XmlEnumValue("Kilotonne")
    KILOTONNE("Kilotonne"),
    @XmlEnumValue("KilogramOfUranium")
    KILOGRAM_OF_URANIUM("KilogramOfUranium"),
    @XmlEnumValue("KilovoltAmpere")
    KILOVOLT_AMPERE("KilovoltAmpere"),
    @XmlEnumValue("Kilovar")
    KILOVAR("Kilovar"),
    @XmlEnumValue("Kilovolt")
    KILOVOLT("Kilovolt"),
    @XmlEnumValue("KilowattHour")
    KILOWATT_HOUR("KilowattHour"),
    @XmlEnumValue("Kilowatt")
    KILOWATT("Kilowatt"),
    @XmlEnumValue("Pound")
    POUND("Pound"),
    @XmlEnumValue("TroyPoundUS")
    TROY_POUND_US("TroyPoundUS"),
    @XmlEnumValue("Leaf")
    LEAF("Leaf"),
    @XmlEnumValue("LitreOfPureAlcohol")
    LITRE_OF_PURE_ALCOHOL("LitreOfPureAlcohol"),
    @XmlEnumValue("TonUKorLongTonUS")
    TON_U_KOR_LONG_TON_US("TonUKorLongTonUS"),
    @XmlEnumValue("Litre")
    LITRE("Litre"),
    @XmlEnumValue("Lumen")
    LUMEN("Lumen"),
    @XmlEnumValue("Lux")
    LUX("Lux"),
    @XmlEnumValue("MegaLitre")
    MEGA_LITRE("MegaLitre"),
    @XmlEnumValue("Megametre")
    MEGAMETRE("Megametre"),
    @XmlEnumValue("Megawatt")
    MEGAWATT("Megawatt"),
    @XmlEnumValue("ThousandStandardBrickEquivalent")
    THOUSAND_STANDARD_BRICK_EQUIVALENT("ThousandStandardBrickEquivalent"),
    @XmlEnumValue("ThousandBoardFeet")
    THOUSAND_BOARD_FEET("ThousandBoardFeet"),
    @XmlEnumValue("Millibar")
    MILLIBAR("Millibar"),
    @XmlEnumValue("Millicurie")
    MILLICURIE("Millicurie"),
    @XmlEnumValue("Milligram")
    MILLIGRAM("Milligram"),
    @XmlEnumValue("Megahertz")
    MEGAHERTZ("Megahertz"),
    @XmlEnumValue("SquareMile")
    SQUARE_MILE("SquareMile"),
    @XmlEnumValue("Thousand")
    THOUSAND("Thousand"),
    @XmlEnumValue("Minute")
    MINUTE("Minute"),
    @XmlEnumValue("Million")
    MILLION("Million"),
    @XmlEnumValue("MillionInternationalUnit")
    MILLION_INTERNATIONAL_UNIT("MillionInternationalUnit"),
    @XmlEnumValue("MilliardBillionUS")
    MILLIARD_BILLION_US("MilliardBillionUS"),
    @XmlEnumValue("Millilitre")
    MILLILITRE("Millilitre"),
    @XmlEnumValue("SquareMillimetre")
    SQUARE_MILLIMETRE("SquareMillimetre"),
    @XmlEnumValue("CubicMillimetre")
    CUBIC_MILLIMETRE("CubicMillimetre"),
    @XmlEnumValue("Millimetre")
    MILLIMETRE("Millimetre"),
    @XmlEnumValue("Month")
    MONTH("Month"),
    @XmlEnumValue("Megapascal")
    MEGAPASCAL("Megapascal"),
    @XmlEnumValue("CubicMetrePerHour")
    CUBIC_METRE_PER_HOUR("CubicMetrePerHour"),
    @XmlEnumValue("CubicMetrePerSecond")
    CUBIC_METRE_PER_SECOND("CubicMetrePerSecond"),
    @XmlEnumValue("MetrePerSecondSquared")
    METRE_PER_SECOND_SQUARED("MetrePerSecondSquared"),
    @XmlEnumValue("SquareMetre")
    SQUARE_METRE("SquareMetre"),
    @XmlEnumValue("CubicMetre")
    CUBIC_METRE("CubicMetre"),
    @XmlEnumValue("Metre")
    METRE("Metre"),
    @XmlEnumValue("MetrePerSecond")
    METRE_PER_SECOND("MetrePerSecond"),
    @XmlEnumValue("MegavoltAmpere")
    MEGAVOLT_AMPERE("MegavoltAmpere"),
    @XmlEnumValue("MegawattHour1000KWH")
    MEGAWATT_HOUR_1000_KWH("MegawattHour1000KWH"),
    @XmlEnumValue("NumberOfArticles")
    NUMBER_OF_ARTICLES("NumberOfArticles"),
    @XmlEnumValue("NumberOfBobbins")
    NUMBER_OF_BOBBINS("NumberOfBobbins"),
    @XmlEnumValue("NumberOfCells")
    NUMBER_OF_CELLS("NumberOfCells"),
    @XmlEnumValue("Newton")
    NEWTON("Newton"),
    @XmlEnumValue("NumberOfInternationalUnits")
    NUMBER_OF_INTERNATIONAL_UNITS("NumberOfInternationalUnits"),
    @XmlEnumValue("NauticalMile")
    NAUTICAL_MILE("NauticalMile"),
    @XmlEnumValue("NumberOfPacks")
    NUMBER_OF_PACKS("NumberOfPacks"),
    @XmlEnumValue("NumberOfParcels")
    NUMBER_OF_PARCELS("NumberOfParcels"),
    @XmlEnumValue("NumberOfPairs")
    NUMBER_OF_PAIRS("NumberOfPairs"),
    @XmlEnumValue("NumberOfParts")
    NUMBER_OF_PARTS("NumberOfParts"),
    @XmlEnumValue("NumberOfRolls")
    NUMBER_OF_ROLLS("NumberOfRolls"),
    @XmlEnumValue("NetRegisterTon")
    NET_REGISTER_TON("NetRegisterTon"),
    @XmlEnumValue("Ohm")
    OHM("Ohm"),
    @XmlEnumValue("Ounce")
    OUNCE("Ounce"),
    @XmlEnumValue("FluidOunceUS")
    FLUID_OUNCE_US("FluidOunceUS"),
    @XmlEnumValue("FluidOunceUK")
    FLUID_OUNCE_UK("FluidOunceUK"),
    @XmlEnumValue("Pascal")
    PASCAL("Pascal"),
    @XmlEnumValue("Piece")
    PIECE("Piece"),
    @XmlEnumValue("ProofGallon")
    PROOF_GALLON("ProofGallon"),
    @XmlEnumValue("DryPintUS")
    DRY_PINT_US("DryPintUS"),
    @XmlEnumValue("PintUK")
    PINT_UK("PintUK"),
    @XmlEnumValue("LiquidPintUS")
    LIQUID_PINT_US("LiquidPintUS"),
    @XmlEnumValue("QuarterOfAYear")
    QUARTER_OF_A_YEAR("QuarterOfAYear"),
    @XmlEnumValue("DryQuartUS")
    DRY_QUART_US("DryQuartUS"),
    @XmlEnumValue("QuartUK")
    QUART_UK("QuartUK"),
    @XmlEnumValue("LiquidQuartUS")
    LIQUID_QUART_US("LiquidQuartUS"),
    @XmlEnumValue("QuarterUK")
    QUARTER_UK("QuarterUK"),
    @XmlEnumValue("RevolutionsPerMinute")
    REVOLUTIONS_PER_MINUTE("RevolutionsPerMinute"),
    @XmlEnumValue("RevolutionsPerSecond")
    REVOLUTIONS_PER_SECOND("RevolutionsPerSecond"),
    @XmlEnumValue("HalfYear6Months")
    HALF_YEAR_6_MONTHS("HalfYear6Months"),
    @XmlEnumValue("Score")
    SCORE("Score"),
    @XmlEnumValue("Scruple")
    SCRUPLE("Scruple"),
    @XmlEnumValue("Second")
    SECOND("Second"),
    @XmlEnumValue("Set")
    SET("Set"),
    @XmlEnumValue("ShippingTon")
    SHIPPING_TON("ShippingTon"),
    @XmlEnumValue("Siemens")
    SIEMENS("Siemens"),
    @XmlEnumValue("MileStatuteMile")
    MILE_STATUTE_MILE("MileStatuteMile"),
    @XmlEnumValue("ShortStandard7200Matches")
    SHORT_STANDARD_7200_MATCHES("ShortStandard7200Matches"),
    @XmlEnumValue("StoneUK")
    STONE_UK("StoneUK"),
    @XmlEnumValue("TonUSOrShortTonUKUS")
    TON_US_OR_SHORT_TON_UKUS("TonUSOrShortTonUKUS"),
    @XmlEnumValue("KiloampereHourThousandAmpereHour")
    KILOAMPERE_HOUR_THOUSAND_AMPERE_HOUR("KiloampereHourThousandAmpereHour"),
    @XmlEnumValue("TonneMetricTon")
    TONNE_METRIC_TON("TonneMetricTon"),
    @XmlEnumValue("TenPair")
    TEN_PAIR("TenPair"),
    @XmlEnumValue("ThousandCubicMetrePerDay")
    THOUSAND_CUBIC_METRE_PER_DAY("ThousandCubicMetrePerDay"),
    @XmlEnumValue("TrillionEUR")
    TRILLION_EUR("TrillionEUR"),
    @XmlEnumValue("TonneOfSubstance90PercentDry")
    TONNE_OF_SUBSTANCE_90_PERCENT_DRY("TonneOfSubstance90PercentDry"),
    @XmlEnumValue("TonOfSteamPerHour")
    TON_OF_STEAM_PER_HOUR("TonOfSteamPerHour"),
    @XmlEnumValue("Volt")
    VOLT("Volt"),
    @XmlEnumValue("Cord")
    CORD("Cord"),
    @XmlEnumValue("Weber")
    WEBER("Weber"),
    @XmlEnumValue("Week")
    WEEK("Week"),
    @XmlEnumValue("WattHour")
    WATT_HOUR("WattHour"),
    @XmlEnumValue("Standard")
    STANDARD("Standard"),
    @XmlEnumValue("Watt")
    WATT("Watt"),
    @XmlEnumValue("SquareYard")
    SQUARE_YARD("SquareYard"),
    @XmlEnumValue("CubicYard")
    CUBIC_YARD("CubicYard"),
    @XmlEnumValue("Yard")
    YARD("Yard");
    private final String value;

    TypeUnitOfMeasure(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypeUnitOfMeasure fromValue(String v) {
        for (TypeUnitOfMeasure c: TypeUnitOfMeasure.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
