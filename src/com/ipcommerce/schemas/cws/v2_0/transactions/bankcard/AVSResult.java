
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AVSResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AVSResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ActualResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AddressResult" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}AddressResult" minOccurs="0"/>
 *         &lt;element name="PostalCodeResult" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}PostalCodeResult" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AVSResult", propOrder = {
    "actualResult",
    "addressResult",
    "postalCodeResult"
})
public class AVSResult {

    @XmlElement(name = "ActualResult", nillable = true)
    protected String actualResult;
    @XmlElement(name = "AddressResult")
    protected AddressResult addressResult;
    @XmlElement(name = "PostalCodeResult")
    protected PostalCodeResult postalCodeResult;

    /**
     * Gets the value of the actualResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualResult() {
        return actualResult;
    }

    /**
     * Sets the value of the actualResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualResult(String value) {
        this.actualResult = value;
    }

    /**
     * Gets the value of the addressResult property.
     * 
     * @return
     *     possible object is
     *     {@link AddressResult }
     *     
     */
    public AddressResult getAddressResult() {
        return addressResult;
    }

    /**
     * Sets the value of the addressResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressResult }
     *     
     */
    public void setAddressResult(AddressResult value) {
        this.addressResult = value;
    }

    /**
     * Gets the value of the postalCodeResult property.
     * 
     * @return
     *     possible object is
     *     {@link PostalCodeResult }
     *     
     */
    public PostalCodeResult getPostalCodeResult() {
        return postalCodeResult;
    }

    /**
     * Sets the value of the postalCodeResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link PostalCodeResult }
     *     
     */
    public void setPostalCodeResult(PostalCodeResult value) {
        this.postalCodeResult = value;
    }

}
