
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.ipcommerce.schemas.cws.v2_0.transactions.Response;


/**
 * <p>Java class for BankcardCaptureResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankcardCaptureResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions}Response">
 *       &lt;sequence>
 *         &lt;element name="BatchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionSummaryData" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}TransactionSummaryData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankcardCaptureResponse", propOrder = {
    "batchId",
    "transactionSummaryData"
})
public class BankcardCaptureResponse
    extends Response
{

    @XmlElement(name = "BatchId", nillable = true)
    protected String batchId;
    @XmlElement(name = "TransactionSummaryData", nillable = true)
    protected TransactionSummaryData transactionSummaryData;

    /**
     * Gets the value of the batchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * Sets the value of the batchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBatchId(String value) {
        this.batchId = value;
    }

    /**
     * Gets the value of the transactionSummaryData property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionSummaryData }
     *     
     */
    public TransactionSummaryData getTransactionSummaryData() {
        return transactionSummaryData;
    }

    /**
     * Sets the value of the transactionSummaryData property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionSummaryData }
     *     
     */
    public void setTransactionSummaryData(TransactionSummaryData value) {
        this.transactionSummaryData = value;
    }

}
