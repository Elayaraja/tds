
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.ipcommerce.schemas.cws.v2_0.transactions.Undo;


/**
 * <p>Java class for BankcardUndo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankcardUndo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions}Undo">
 *       &lt;sequence>
 *         &lt;element name="PINDebitReason" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}PINDebitUndoReason" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankcardUndo", propOrder = {
    "pinDebitReason"
})
public class BankcardUndo
    extends Undo
{

    @XmlElement(name = "PINDebitReason")
    protected PINDebitUndoReason pinDebitReason;

    /**
     * Gets the value of the pinDebitReason property.
     * 
     * @return
     *     possible object is
     *     {@link PINDebitUndoReason }
     *     
     */
    public PINDebitUndoReason getPINDebitReason() {
        return pinDebitReason;
    }

    /**
     * Sets the value of the pinDebitReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link PINDebitUndoReason }
     *     
     */
    public void setPINDebitReason(PINDebitUndoReason value) {
        this.pinDebitReason = value;
    }

}
