
package com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.pro;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ArrayOfLineItemDetail;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTransactionData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Level2Data;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ManagedBilling;


/**
 * <p>Java class for BankcardTransactionDataPro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BankcardTransactionDataPro">
 *   &lt;complexContent>
 *     &lt;extension base="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}BankcardTransactionData">
 *       &lt;sequence>
 *         &lt;element name="ManagedBilling" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}ManagedBilling" minOccurs="0"/>
 *         &lt;element name="Level2Data" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}Level2Data" minOccurs="0"/>
 *         &lt;element name="LineItemDetails" type="{http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard}ArrayOfLineItemDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BankcardTransactionDataPro", propOrder = {
    "managedBilling",
    "level2Data",
    "lineItemDetails"
})
public class BankcardTransactionDataPro
    extends BankcardTransactionData
{

    @XmlElement(name = "ManagedBilling", nillable = true)
    protected ManagedBilling managedBilling;
    @XmlElement(name = "Level2Data", nillable = true)
    protected Level2Data level2Data;
    @XmlElement(name = "LineItemDetails", nillable = true)
    protected ArrayOfLineItemDetail lineItemDetails;

    /**
     * Gets the value of the managedBilling property.
     * 
     * @return
     *     possible object is
     *     {@link ManagedBilling }
     *     
     */
    public ManagedBilling getManagedBilling() {
        return managedBilling;
    }

    /**
     * Sets the value of the managedBilling property.
     * 
     * @param value
     *     allowed object is
     *     {@link ManagedBilling }
     *     
     */
    public void setManagedBilling(ManagedBilling value) {
        this.managedBilling = value;
    }

    /**
     * Gets the value of the level2Data property.
     * 
     * @return
     *     possible object is
     *     {@link Level2Data }
     *     
     */
    public Level2Data getLevel2Data() {
        return level2Data;
    }

    /**
     * Sets the value of the level2Data property.
     * 
     * @param value
     *     allowed object is
     *     {@link Level2Data }
     *     
     */
    public void setLevel2Data(Level2Data value) {
        this.level2Data = value;
    }

    /**
     * Gets the value of the lineItemDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfLineItemDetail }
     *     
     */
    public ArrayOfLineItemDetail getLineItemDetails() {
        return lineItemDetails;
    }

    /**
     * Sets the value of the lineItemDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfLineItemDetail }
     *     
     */
    public void setLineItemDetails(ArrayOfLineItemDetail value) {
        this.lineItemDetails = value;
    }

}
