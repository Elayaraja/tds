
package com.ipcommerce.schemas.cws.v2_0.transactions;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ipcommerce.schemas.cws.v2_0.transactions package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Addendum_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Addendum");
    private final static QName _TypeStateProvince_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "TypeStateProvince");
    private final static QName _Capture_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Capture");
    private final static QName _Return_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Return");
    private final static QName _TypeISOCurrencyCodeA3_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "TypeISOCurrencyCodeA3");
    private final static QName _ArrayOfResponse_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "ArrayOfResponse");
    private final static QName _NameInfo_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "NameInfo");
    private final static QName _Unmanaged_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Unmanaged");
    private final static QName _Undo_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Undo");
    private final static QName _Transaction_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Transaction");
    private final static QName _TypeISOCountryCodeA3_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "TypeISOCountryCodeA3");
    private final static QName _Status_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Status");
    private final static QName _Adjust_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Adjust");
    private final static QName _ServiceTransactionDateTime_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "ServiceTransactionDateTime");
    private final static QName _TransactionCustomerData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "TransactionCustomerData");
    private final static QName _TransactionData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "TransactionData");
    private final static QName _ArrayOfCapture_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "ArrayOfCapture");
    private final static QName _TransactionReportingData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "TransactionReportingData");
    private final static QName _AddressInfo_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "AddressInfo");
    private final static QName _TransactionTenderData_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "TransactionTenderData");
    private final static QName _CustomerInfo_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "CustomerInfo");
    private final static QName _Response_QNAME = new QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions", "Response");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ipcommerce.schemas.cws.v2_0.transactions
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransactionTenderData }
     * 
     */
    public TransactionTenderData createTransactionTenderData() {
        return new TransactionTenderData();
    }

    /**
     * Create an instance of {@link Undo }
     * 
     */
    public Undo createUndo() {
        return new Undo();
    }

    /**
     * Create an instance of {@link TransactionReportingData }
     * 
     */
    public TransactionReportingData createTransactionReportingData() {
        return new TransactionReportingData();
    }

    /**
     * Create an instance of {@link NameInfo }
     * 
     */
    public NameInfo createNameInfo() {
        return new NameInfo();
    }

    /**
     * Create an instance of {@link ServiceTransactionDateTime }
     * 
     */
    public ServiceTransactionDateTime createServiceTransactionDateTime() {
        return new ServiceTransactionDateTime();
    }

    /**
     * Create an instance of {@link Adjust }
     * 
     */
    public Adjust createAdjust() {
        return new Adjust();
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link AddressInfo }
     * 
     */
    public AddressInfo createAddressInfo() {
        return new AddressInfo();
    }

    /**
     * Create an instance of {@link TransactionCustomerData }
     * 
     */
    public TransactionCustomerData createTransactionCustomerData() {
        return new TransactionCustomerData();
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link Return }
     * 
     */
    public Return createReturn() {
        return new Return();
    }

    /**
     * Create an instance of {@link Addendum }
     * 
     */
    public Addendum createAddendum() {
        return new Addendum();
    }

    /**
     * Create an instance of {@link ArrayOfResponse }
     * 
     */
    public ArrayOfResponse createArrayOfResponse() {
        return new ArrayOfResponse();
    }

    /**
     * Create an instance of {@link ArrayOfCapture }
     * 
     */
    public ArrayOfCapture createArrayOfCapture() {
        return new ArrayOfCapture();
    }

    /**
     * Create an instance of {@link Unmanaged }
     * 
     */
    public Unmanaged createUnmanaged() {
        return new Unmanaged();
    }

    /**
     * Create an instance of {@link TransactionData }
     * 
     */
    public TransactionData createTransactionData() {
        return new TransactionData();
    }

    /**
     * Create an instance of {@link Capture }
     * 
     */
    public Capture createCapture() {
        return new Capture();
    }

    /**
     * Create an instance of {@link CustomerInfo }
     * 
     */
    public CustomerInfo createCustomerInfo() {
        return new CustomerInfo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Addendum }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Addendum")
    public JAXBElement<Addendum> createAddendum(Addendum value) {
        return new JAXBElement<Addendum>(_Addendum_QNAME, Addendum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeStateProvince }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "TypeStateProvince")
    public JAXBElement<TypeStateProvince> createTypeStateProvince(TypeStateProvince value) {
        return new JAXBElement<TypeStateProvince>(_TypeStateProvince_QNAME, TypeStateProvince.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Capture }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Capture")
    public JAXBElement<Capture> createCapture(Capture value) {
        return new JAXBElement<Capture>(_Capture_QNAME, Capture.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Return }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Return")
    public JAXBElement<Return> createReturn(Return value) {
        return new JAXBElement<Return>(_Return_QNAME, Return.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeISOCurrencyCodeA3 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "TypeISOCurrencyCodeA3")
    public JAXBElement<TypeISOCurrencyCodeA3> createTypeISOCurrencyCodeA3(TypeISOCurrencyCodeA3 value) {
        return new JAXBElement<TypeISOCurrencyCodeA3>(_TypeISOCurrencyCodeA3_QNAME, TypeISOCurrencyCodeA3 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "ArrayOfResponse")
    public JAXBElement<ArrayOfResponse> createArrayOfResponse(ArrayOfResponse value) {
        return new JAXBElement<ArrayOfResponse>(_ArrayOfResponse_QNAME, ArrayOfResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NameInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "NameInfo")
    public JAXBElement<NameInfo> createNameInfo(NameInfo value) {
        return new JAXBElement<NameInfo>(_NameInfo_QNAME, NameInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Unmanaged }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Unmanaged")
    public JAXBElement<Unmanaged> createUnmanaged(Unmanaged value) {
        return new JAXBElement<Unmanaged>(_Unmanaged_QNAME, Unmanaged.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Undo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Undo")
    public JAXBElement<Undo> createUndo(Undo value) {
        return new JAXBElement<Undo>(_Undo_QNAME, Undo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Transaction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Transaction")
    public JAXBElement<Transaction> createTransaction(Transaction value) {
        return new JAXBElement<Transaction>(_Transaction_QNAME, Transaction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TypeISOCountryCodeA3 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "TypeISOCountryCodeA3")
    public JAXBElement<TypeISOCountryCodeA3> createTypeISOCountryCodeA3(TypeISOCountryCodeA3 value) {
        return new JAXBElement<TypeISOCountryCodeA3>(_TypeISOCountryCodeA3_QNAME, TypeISOCountryCodeA3 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Status }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Status")
    public JAXBElement<Status> createStatus(Status value) {
        return new JAXBElement<Status>(_Status_QNAME, Status.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Adjust }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Adjust")
    public JAXBElement<Adjust> createAdjust(Adjust value) {
        return new JAXBElement<Adjust>(_Adjust_QNAME, Adjust.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceTransactionDateTime }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "ServiceTransactionDateTime")
    public JAXBElement<ServiceTransactionDateTime> createServiceTransactionDateTime(ServiceTransactionDateTime value) {
        return new JAXBElement<ServiceTransactionDateTime>(_ServiceTransactionDateTime_QNAME, ServiceTransactionDateTime.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionCustomerData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "TransactionCustomerData")
    public JAXBElement<TransactionCustomerData> createTransactionCustomerData(TransactionCustomerData value) {
        return new JAXBElement<TransactionCustomerData>(_TransactionCustomerData_QNAME, TransactionCustomerData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "TransactionData")
    public JAXBElement<TransactionData> createTransactionData(TransactionData value) {
        return new JAXBElement<TransactionData>(_TransactionData_QNAME, TransactionData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCapture }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "ArrayOfCapture")
    public JAXBElement<ArrayOfCapture> createArrayOfCapture(ArrayOfCapture value) {
        return new JAXBElement<ArrayOfCapture>(_ArrayOfCapture_QNAME, ArrayOfCapture.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionReportingData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "TransactionReportingData")
    public JAXBElement<TransactionReportingData> createTransactionReportingData(TransactionReportingData value) {
        return new JAXBElement<TransactionReportingData>(_TransactionReportingData_QNAME, TransactionReportingData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddressInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "AddressInfo")
    public JAXBElement<AddressInfo> createAddressInfo(AddressInfo value) {
        return new JAXBElement<AddressInfo>(_AddressInfo_QNAME, AddressInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransactionTenderData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "TransactionTenderData")
    public JAXBElement<TransactionTenderData> createTransactionTenderData(TransactionTenderData value) {
        return new JAXBElement<TransactionTenderData>(_TransactionTenderData_QNAME, TransactionTenderData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomerInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "CustomerInfo")
    public JAXBElement<CustomerInfo> createCustomerInfo(CustomerInfo value) {
        return new JAXBElement<CustomerInfo>(_CustomerInfo_QNAME, CustomerInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.ipcommerce.com/CWS/v2.0/Transactions", name = "Response")
    public JAXBElement<Response> createResponse(Response value) {
        return new JAXBElement<Response>(_Response_QNAME, Response.class, null, value);
    }

}
