
/**
 * CWSServiceInformationStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */
        package com.ipcommerce.schemas.cws.serviceinformation;

        

        /*
        *  CWSServiceInformationStub java implementation
        */

        
        public class CWSServiceInformationStub extends org.apache.axis2.client.Stub
        {
        protected org.apache.axis2.description.AxisOperation[] _operations;

        //hashmaps to keep the fault mapping
        private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
        private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
        private java.util.HashMap faultMessageMap = new java.util.HashMap();

        private static int counter = 0;

        private static synchronized java.lang.String getUniqueSuffix(){
            // reset the counter if it is greater than 99999
            if (counter > 99999){
                counter = 0;
            }
            counter = counter + 1; 
            return java.lang.Long.toString(System.currentTimeMillis()) + "_" + counter;
        }

    
    private void populateAxisService() throws org.apache.axis2.AxisFault {

     //creating the Service with a unique name
     _service = new org.apache.axis2.description.AxisService("CWSServiceInformation" + getUniqueSuffix());
     addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[11];
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "saveMerchantProfiles"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[0]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "getMerchantProfiles"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[1]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "deleteApplicationData"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[2]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "getApplicationData"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[3]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "saveApplicationData"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[4]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "signOnWithToken"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[5]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "deleteMerchantProfile"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[6]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "getMerchantProfile"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[7]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "getServiceInformation"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[8]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "isMerchantProfileInitialized"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[9]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation", "ping"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"ServiceInfoProcessing_ICWSServiceInformation_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[10]=__operation;
            
        
        }

    //populates the faults
    private void populateFaults(){
         
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
           


    }

    /**
      *Constructor that takes in a configContext
      */

    public CWSServiceInformationStub(org.apache.axis2.context.ConfigurationContext configurationContext,
       java.lang.String targetEndpoint)
       throws org.apache.axis2.AxisFault {
         this(configurationContext,targetEndpoint,false);
   }


   /**
     * Constructor that takes in a configContext  and useseperate listner
     */
   public CWSServiceInformationStub(org.apache.axis2.context.ConfigurationContext configurationContext,
        java.lang.String targetEndpoint, boolean useSeparateListener)
        throws org.apache.axis2.AxisFault {
         //To populate AxisService
         populateAxisService();
         populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext,_service);
        
        _service.applyPolicy();
        
	
        _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(
                targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);
        
    
    }

    /**
     * Default Constructor
     */
    public CWSServiceInformationStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {
        
                    this(configurationContext,"https://cws-01.ipcommerce.com/2.0/SvcInfo/B30B89F5E6D00001" );
                
    }

    /**
     * Default Constructor
     */
    public CWSServiceInformationStub() throws org.apache.axis2.AxisFault {
        
                    this("https://cws-01.ipcommerce.com/2.0/SvcInfo/B30B89F5E6D00001" );
                
    }

    /**
     * Constructor taking the target endpoint
     */
    public CWSServiceInformationStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        this(null,targetEndpoint);
    }



        
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#saveMerchantProfiles
                     * @param saveMerchantProfiles0
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse saveMerchantProfiles(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles saveMerchantProfiles0)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/SaveMerchantProfiles");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    saveMerchantProfiles0,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "saveMerchantProfiles")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startsaveMerchantProfiles
                    * @param saveMerchantProfiles0
                
                */
                public  void startsaveMerchantProfiles(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles saveMerchantProfiles0,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/SaveMerchantProfiles");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    saveMerchantProfiles0,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "saveMerchantProfiles")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultsaveMerchantProfiles(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorsaveMerchantProfiles(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFaultFault_FaultMessage){
														callback.receiveErrorsaveMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorsaveMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorsaveMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorsaveMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorsaveMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorsaveMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorsaveMerchantProfiles(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveMerchantProfiles(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveMerchantProfiles(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveMerchantProfiles(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveMerchantProfiles(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveMerchantProfiles(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveMerchantProfiles(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveMerchantProfiles(f);
                                            }
									    } else {
										    callback.receiveErrorsaveMerchantProfiles(f);
									    }
									} else {
									    callback.receiveErrorsaveMerchantProfiles(f);
									}
								} else {
								    callback.receiveErrorsaveMerchantProfiles(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorsaveMerchantProfiles(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[0].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[0].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#getMerchantProfiles
                     * @param getMerchantProfiles2
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse getMerchantProfiles(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfiles getMerchantProfiles2)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/GetMerchantProfiles");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getMerchantProfiles2,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "getMerchantProfiles")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startgetMerchantProfiles
                    * @param getMerchantProfiles2
                
                */
                public  void startgetMerchantProfiles(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfiles getMerchantProfiles2,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/GetMerchantProfiles");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getMerchantProfiles2,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "getMerchantProfiles")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultgetMerchantProfiles(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorgetMerchantProfiles(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfiles((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfiles_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorgetMerchantProfiles(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfiles(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfiles(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfiles(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfiles(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfiles(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfiles(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfiles(f);
                                            }
									    } else {
										    callback.receiveErrorgetMerchantProfiles(f);
									    }
									} else {
									    callback.receiveErrorgetMerchantProfiles(f);
									}
								} else {
								    callback.receiveErrorgetMerchantProfiles(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorgetMerchantProfiles(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[1].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[1].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#deleteApplicationData
                     * @param deleteApplicationData4
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse deleteApplicationData(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationData deleteApplicationData4)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/DeleteApplicationData");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    deleteApplicationData4,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "deleteApplicationData")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startdeleteApplicationData
                    * @param deleteApplicationData4
                
                */
                public  void startdeleteApplicationData(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationData deleteApplicationData4,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/DeleteApplicationData");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    deleteApplicationData4,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "deleteApplicationData")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultdeleteApplicationData(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrordeleteApplicationData(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFaultFault_FaultMessage){
														callback.receiveErrordeleteApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrordeleteApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrordeleteApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrordeleteApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrordeleteApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrordeleteApplicationData(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteApplicationData(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteApplicationData(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteApplicationData(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteApplicationData(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteApplicationData(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteApplicationData(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteApplicationData(f);
                                            }
									    } else {
										    callback.receiveErrordeleteApplicationData(f);
									    }
									} else {
									    callback.receiveErrordeleteApplicationData(f);
									}
								} else {
								    callback.receiveErrordeleteApplicationData(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrordeleteApplicationData(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[2].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[2].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#getApplicationData
                     * @param getApplicationData6
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse getApplicationData(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationData getApplicationData6)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[3].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/GetApplicationData");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getApplicationData6,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "getApplicationData")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startgetApplicationData
                    * @param getApplicationData6
                
                */
                public  void startgetApplicationData(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationData getApplicationData6,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[3].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/GetApplicationData");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getApplicationData6,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "getApplicationData")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultgetApplicationData(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorgetApplicationData(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFaultFault_FaultMessage){
														callback.receiveErrorgetApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorgetApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorgetApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorgetApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorgetApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorgetApplicationData(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetApplicationData(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetApplicationData(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetApplicationData(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetApplicationData(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetApplicationData(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetApplicationData(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetApplicationData(f);
                                            }
									    } else {
										    callback.receiveErrorgetApplicationData(f);
									    }
									} else {
									    callback.receiveErrorgetApplicationData(f);
									}
								} else {
								    callback.receiveErrorgetApplicationData(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorgetApplicationData(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[3].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[3].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#saveApplicationData
                     * @param saveApplicationData8
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse saveApplicationData(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData saveApplicationData8)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[4].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/SaveApplicationData");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    saveApplicationData8,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "saveApplicationData")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startsaveApplicationData
                    * @param saveApplicationData8
                
                */
                public  void startsaveApplicationData(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData saveApplicationData8,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[4].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/SaveApplicationData");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    saveApplicationData8,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "saveApplicationData")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultsaveApplicationData(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorsaveApplicationData(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFaultFault_FaultMessage){
														callback.receiveErrorsaveApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorsaveApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorsaveApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorsaveApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorsaveApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorsaveApplicationData((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorsaveApplicationData(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveApplicationData(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveApplicationData(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveApplicationData(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveApplicationData(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveApplicationData(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveApplicationData(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsaveApplicationData(f);
                                            }
									    } else {
										    callback.receiveErrorsaveApplicationData(f);
									    }
									} else {
									    callback.receiveErrorsaveApplicationData(f);
									}
								} else {
								    callback.receiveErrorsaveApplicationData(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorsaveApplicationData(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[4].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[4].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#signOnWithToken
                     * @param signOnWithToken10
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse signOnWithToken(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken signOnWithToken10)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[5].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/SignOnWithToken");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    signOnWithToken10,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "signOnWithToken")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startsignOnWithToken
                    * @param signOnWithToken10
                
                */
                public  void startsignOnWithToken(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken signOnWithToken10,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[5].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/SignOnWithToken");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    signOnWithToken10,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "signOnWithToken")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultsignOnWithToken(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorsignOnWithToken(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorsignOnWithToken((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorsignOnWithToken((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorsignOnWithToken((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorsignOnWithToken((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorsignOnWithToken(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsignOnWithToken(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsignOnWithToken(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsignOnWithToken(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsignOnWithToken(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsignOnWithToken(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsignOnWithToken(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorsignOnWithToken(f);
                                            }
									    } else {
										    callback.receiveErrorsignOnWithToken(f);
									    }
									} else {
									    callback.receiveErrorsignOnWithToken(f);
									}
								} else {
								    callback.receiveErrorsignOnWithToken(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorsignOnWithToken(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[5].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[5].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#deleteMerchantProfile
                     * @param deleteMerchantProfile12
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse deleteMerchantProfile(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfile deleteMerchantProfile12)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[6].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/DeleteMerchantProfile");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    deleteMerchantProfile12,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "deleteMerchantProfile")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startdeleteMerchantProfile
                    * @param deleteMerchantProfile12
                
                */
                public  void startdeleteMerchantProfile(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfile deleteMerchantProfile12,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[6].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/DeleteMerchantProfile");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    deleteMerchantProfile12,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "deleteMerchantProfile")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultdeleteMerchantProfile(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrordeleteMerchantProfile(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFaultFault_FaultMessage){
														callback.receiveErrordeleteMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrordeleteMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrordeleteMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrordeleteMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrordeleteMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrordeleteMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_DeleteMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrordeleteMerchantProfile(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteMerchantProfile(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteMerchantProfile(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteMerchantProfile(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteMerchantProfile(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteMerchantProfile(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteMerchantProfile(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrordeleteMerchantProfile(f);
                                            }
									    } else {
										    callback.receiveErrordeleteMerchantProfile(f);
									    }
									} else {
									    callback.receiveErrordeleteMerchantProfile(f);
									}
								} else {
								    callback.receiveErrordeleteMerchantProfile(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrordeleteMerchantProfile(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[6].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[6].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#getMerchantProfile
                     * @param getMerchantProfile14
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse getMerchantProfile(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfile getMerchantProfile14)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[7].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/GetMerchantProfile");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getMerchantProfile14,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "getMerchantProfile")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startgetMerchantProfile
                    * @param getMerchantProfile14
                
                */
                public  void startgetMerchantProfile(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfile getMerchantProfile14,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[7].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/GetMerchantProfile");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getMerchantProfile14,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "getMerchantProfile")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultgetMerchantProfile(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorgetMerchantProfile(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorgetMerchantProfile((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetMerchantProfile_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorgetMerchantProfile(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfile(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfile(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfile(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfile(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfile(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfile(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetMerchantProfile(f);
                                            }
									    } else {
										    callback.receiveErrorgetMerchantProfile(f);
									    }
									} else {
									    callback.receiveErrorgetMerchantProfile(f);
									}
								} else {
								    callback.receiveErrorgetMerchantProfile(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorgetMerchantProfile(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[7].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[7].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#getServiceInformation
                     * @param getServiceInformation16
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse getServiceInformation(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation getServiceInformation16)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[8].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/GetServiceInformation");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getServiceInformation16,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "getServiceInformation")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startgetServiceInformation
                    * @param getServiceInformation16
                
                */
                public  void startgetServiceInformation(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation getServiceInformation16,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[8].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/GetServiceInformation");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getServiceInformation16,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "getServiceInformation")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultgetServiceInformation(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorgetServiceInformation(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFaultFault_FaultMessage){
														callback.receiveErrorgetServiceInformation((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorgetServiceInformation((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorgetServiceInformation((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorgetServiceInformation((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorgetServiceInformation((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorgetServiceInformation(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetServiceInformation(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetServiceInformation(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetServiceInformation(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetServiceInformation(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetServiceInformation(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetServiceInformation(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetServiceInformation(f);
                                            }
									    } else {
										    callback.receiveErrorgetServiceInformation(f);
									    }
									} else {
									    callback.receiveErrorgetServiceInformation(f);
									}
								} else {
								    callback.receiveErrorgetServiceInformation(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorgetServiceInformation(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[8].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[8].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#isMerchantProfileInitialized
                     * @param isMerchantProfileInitialized18
                    
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse isMerchantProfileInitialized(

                            com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized isMerchantProfileInitialized18)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[9].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/IsMerchantProfileInitialized");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    isMerchantProfileInitialized18,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "isMerchantProfileInitialized")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startisMerchantProfileInitialized
                    * @param isMerchantProfileInitialized18
                
                */
                public  void startisMerchantProfileInitialized(

                 com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized isMerchantProfileInitialized18,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[9].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/ICWSServiceInformation/IsMerchantProfileInitialized");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    isMerchantProfileInitialized18,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "isMerchantProfileInitialized")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultisMerchantProfileInitialized(
                                        (com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorisMerchantProfileInitialized(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFaultFault_FaultMessage){
														callback.receiveErrorisMerchantProfileInitialized((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorisMerchantProfileInitialized((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorisMerchantProfileInitialized((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorisMerchantProfileInitialized((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorisMerchantProfileInitialized((com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorisMerchantProfileInitialized(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorisMerchantProfileInitialized(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorisMerchantProfileInitialized(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorisMerchantProfileInitialized(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorisMerchantProfileInitialized(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorisMerchantProfileInitialized(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorisMerchantProfileInitialized(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorisMerchantProfileInitialized(f);
                                            }
									    } else {
										    callback.receiveErrorisMerchantProfileInitialized(f);
									    }
									} else {
									    callback.receiveErrorisMerchantProfileInitialized(f);
									}
								} else {
								    callback.receiveErrorisMerchantProfileInitialized(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorisMerchantProfileInitialized(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[9].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[9].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#ping
                     * @param ping20
                    
                     */

                    

                            public  com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse ping(

                            com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping ping20)
                        

                    throws java.rmi.RemoteException
                    
                    {
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[10].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common/IExternallyFacingStandardOperations/Ping");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    ping20,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "ping")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformation#startping
                    * @param ping20
                
                */
                public  void startping(

                 com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping ping20,

                  final com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[10].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common/IExternallyFacingStandardOperations/Ping");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    ping20,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                    "ping")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultping(
                                        (com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorping(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
					
										            callback.receiveErrorping(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            }
									    } else {
										    callback.receiveErrorping(f);
									    }
									} else {
									    callback.receiveErrorping(f);
									}
								} else {
								    callback.receiveErrorping(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorping(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[10].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[10].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                


       /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
       private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
       return returnMap;
    }

    
    ////////////////////////////////////////////////////////////////////////
    
    private static org.apache.neethi.Policy getPolicy (java.lang.String policyString) {
    	java.io.ByteArrayInputStream bais = new java.io.ByteArrayInputStream(policyString.getBytes());
    	return org.apache.neethi.PolicyEngine.getPolicy(bais);
    }
    
    /////////////////////////////////////////////////////////////////////////

    
    
    private javax.xml.namespace.QName[] opNameArray = null;
    private boolean optimizeContent(javax.xml.namespace.QName opName) {
        

        if (opNameArray == null) {
            return false;
        }
        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;   
            }
        }
        return false;
    }
     //https://cws-01.ipcommerce.com/2.0/SvcInfo/B30B89F5E6D00001
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveMerchantProfiles;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveMerchantProfilesResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSFailedAuthenticationFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSExpiredSecurityTokenFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSValidationResultFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSInvalidSecurityTokenFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSSignOnServiceUnavailableFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfiles;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfilesResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteApplicationData;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteApplicationDataResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetApplicationData;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetApplicationDataResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveApplicationData;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveApplicationDataResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SignOnWithToken;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SignOnWithTokenResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteMerchantProfile;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteMerchantProfileResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfile;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfileResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetServiceInformation;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetServiceInformationResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_IsMerchantProfileInitialized;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_serviceinformation_IsMerchantProfileInitializedResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_ipc_general_wcf_contracts_Ping;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_ipc_general_wcf_contracts_PingResponse;
            

        private static final java.util.HashMap<Class,javax.xml.bind.JAXBContext> classContextMap = new java.util.HashMap<Class,javax.xml.bind.JAXBContext>();

        static {
            javax.xml.bind.JAXBContext jc;
            
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveMerchantProfiles = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveMerchantProfilesResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSFailedAuthenticationFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSExpiredSecurityTokenFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSValidationResultFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSInvalidSecurityTokenFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSSignOnServiceUnavailableFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfiles.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfiles");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfiles = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfiles.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfilesResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationData.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationData");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteApplicationData = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationData.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteApplicationDataResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationData.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationData");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetApplicationData = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationData.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetApplicationDataResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveApplicationData = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveApplicationDataResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SignOnWithToken = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SignOnWithTokenResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfile.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfile");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteMerchantProfile = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfile.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteMerchantProfileResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfile.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfile");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfile = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfile.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfileResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetServiceInformation = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetServiceInformationResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_IsMerchantProfileInitialized = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_serviceinformation_IsMerchantProfileInitializedResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_ipc_general_wcf_contracts_Ping = jc;
                        classContextMap.put(com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_ipc_general_wcf_contracts_PingResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class, jc);
                    }
                
        }

        

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveMerchantProfiles;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "SaveMerchantProfiles");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "SaveMerchantProfiles", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveMerchantProfilesResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "SaveMerchantProfilesResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "SaveMerchantProfilesResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                        "CWSFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSFailedAuthenticationFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                        "CWSFailedAuthenticationFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSFailedAuthenticationFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSExpiredSecurityTokenFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                        "CWSExpiredSecurityTokenFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSExpiredSecurityTokenFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSExpiredSecurityTokenFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSValidationResultFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                        "CWSValidationResultFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSValidationResultFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSInvalidSecurityTokenFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                        "CWSInvalidSecurityTokenFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSInvalidSecurityTokenFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_faults_CWSSignOnServiceUnavailableFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                        "CWSSignOnServiceUnavailableFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSSignOnServiceUnavailableFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfiles param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfiles;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfiles.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "GetMerchantProfiles");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "GetMerchantProfiles", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfiles param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfilesResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "GetMerchantProfilesResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "GetMerchantProfilesResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationData param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteApplicationData;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationData.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "DeleteApplicationData");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "DeleteApplicationData", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationData param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteApplicationDataResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "DeleteApplicationDataResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "DeleteApplicationDataResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationData param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetApplicationData;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationData.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "GetApplicationData");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "GetApplicationData", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationData param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetApplicationDataResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "GetApplicationDataResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "GetApplicationDataResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveApplicationData;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "SaveApplicationData");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "SaveApplicationData", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SaveApplicationDataResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "SaveApplicationDataResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "SaveApplicationDataResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SignOnWithToken;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "SignOnWithToken");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "SignOnWithToken", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_SignOnWithTokenResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "SignOnWithTokenResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "SignOnWithTokenResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfile param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteMerchantProfile;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfile.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "DeleteMerchantProfile");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "DeleteMerchantProfile", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfile param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_DeleteMerchantProfileResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "DeleteMerchantProfileResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "DeleteMerchantProfileResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfile param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfile;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfile.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "GetMerchantProfile");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "GetMerchantProfile", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfile param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetMerchantProfileResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "GetMerchantProfileResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "GetMerchantProfileResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetServiceInformation;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "GetServiceInformation");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "GetServiceInformation", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_GetServiceInformationResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "GetServiceInformationResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "GetServiceInformationResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_IsMerchantProfileInitialized;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "IsMerchantProfileInitialized");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "IsMerchantProfileInitialized", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_serviceinformation_IsMerchantProfileInitializedResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                        "IsMerchantProfileInitializedResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/ServiceInformation",
                                                                           null);
                        return factory.createOMElement(source, "IsMerchantProfileInitializedResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_ipc_general_wcf_contracts_Ping;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common",
                                                                        "Ping");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common",
                                                                           null);
                        return factory.createOMElement(source, "Ping", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_ipc_general_wcf_contracts_PingResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common",
                                                                        "PingResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common",
                                                                           null);
                        return factory.createOMElement(source, "PingResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
            return factory.getDefaultEnvelope();
        }

        private java.lang.Object fromOM (
            org.apache.axiom.om.OMElement param,
            java.lang.Class type,
            java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{
            try {
                javax.xml.bind.JAXBContext context = classContextMap.get(type);
                javax.xml.bind.Unmarshaller unmarshaller = context.createUnmarshaller();

                return unmarshaller.unmarshal(param.getXMLStreamReaderWithoutCaching(), type).getValue();
            } catch (javax.xml.bind.JAXBException bex){
                throw org.apache.axis2.AxisFault.makeFault(bex);
            }
        }

        class JaxbRIDataSource implements org.apache.axiom.om.OMDataSource {
            /**
             * Bound object for output.
             */
            private final Object outObject;

            /**
             * Bound class for output.
             */
            private final Class outClazz;

            /**
             * Marshaller.
             */
            private final javax.xml.bind.Marshaller marshaller;

            /**
             * Namespace
             */
            private String nsuri;

            /**
             * Local name
             */
            private String name;

            /**
             * Constructor from object and marshaller.
             *
             * @param obj
             * @param marshaller
             */
            public JaxbRIDataSource(Class clazz, Object obj, javax.xml.bind.Marshaller marshaller, String nsuri, String name) {
                this.outClazz = clazz;
                this.outObject = obj;
                this.marshaller = marshaller;
                this.nsuri = nsuri;
                this.name = name;
            }

            public void serialize(java.io.OutputStream output, org.apache.axiom.om.OMOutputFormat format) throws javax.xml.stream.XMLStreamException {
                try {
                    marshaller.marshal(new javax.xml.bind.JAXBElement(
                            new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), output);
                } catch (javax.xml.bind.JAXBException e) {
                    throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
                }
            }

            public void serialize(java.io.Writer writer, org.apache.axiom.om.OMOutputFormat format) throws javax.xml.stream.XMLStreamException {
                try {
                    marshaller.marshal(new javax.xml.bind.JAXBElement(
                            new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), writer);
                } catch (javax.xml.bind.JAXBException e) {
                    throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
                }
            }

            public void serialize(javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                try {
                    marshaller.marshal(new javax.xml.bind.JAXBElement(
                            new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), xmlWriter);
                } catch (javax.xml.bind.JAXBException e) {
                    throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
                }
            }

            public javax.xml.stream.XMLStreamReader getReader() throws javax.xml.stream.XMLStreamException {
                try {
                    javax.xml.bind.JAXBContext context = classContextMap.get(outClazz);
                    org.apache.axiom.om.impl.builder.SAXOMBuilder builder = new org.apache.axiom.om.impl.builder.SAXOMBuilder();
                    javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                    marshaller.marshal(new javax.xml.bind.JAXBElement(
                            new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), builder);

                    return builder.getRootElement().getXMLStreamReader();
                } catch (javax.xml.bind.JAXBException e) {
                    throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
                }
            }
        }
        
    
   }
   