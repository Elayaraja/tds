
/**
 * ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

package com.ipcommerce.schemas.cws.serviceinformation;

public class ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage extends java.lang.Exception{
    
    private com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault faultMessage;

    
        public ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage() {
            super("ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage");
        }

        public ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ICWSServiceInformation_GetServiceInformation_CWSFailedAuthenticationFaultFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault msg){
       faultMessage = msg;
    }
    
    public com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSFailedAuthenticationFault getFaultMessage(){
       return faultMessage;
    }
}
    