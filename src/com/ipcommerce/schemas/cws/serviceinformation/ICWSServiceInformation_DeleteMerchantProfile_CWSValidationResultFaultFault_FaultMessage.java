
/**
 * ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

package com.ipcommerce.schemas.cws.serviceinformation;

public class ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage extends java.lang.Exception{
    
    private com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault faultMessage;

    
        public ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage() {
            super("ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage");
        }

        public ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ICWSServiceInformation_DeleteMerchantProfile_CWSValidationResultFaultFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault msg){
       faultMessage = msg;
    }
    
    public com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault getFaultMessage(){
       return faultMessage;
    }
}
    