
/**
 * CWSServiceInformationCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

    package com.ipcommerce.schemas.cws.serviceinformation;

    /**
     *  CWSServiceInformationCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CWSServiceInformationCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CWSServiceInformationCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CWSServiceInformationCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for saveMerchantProfiles method
            * override this method for handling normal response from saveMerchantProfiles operation
            */
           public void receiveResultsaveMerchantProfiles(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from saveMerchantProfiles operation
           */
            public void receiveErrorsaveMerchantProfiles(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getMerchantProfiles method
            * override this method for handling normal response from getMerchantProfiles operation
            */
           public void receiveResultgetMerchantProfiles(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfilesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getMerchantProfiles operation
           */
            public void receiveErrorgetMerchantProfiles(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteApplicationData method
            * override this method for handling normal response from deleteApplicationData operation
            */
           public void receiveResultdeleteApplicationData(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteApplicationDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteApplicationData operation
           */
            public void receiveErrordeleteApplicationData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationData method
            * override this method for handling normal response from getApplicationData operation
            */
           public void receiveResultgetApplicationData(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetApplicationDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationData operation
           */
            public void receiveErrorgetApplicationData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for saveApplicationData method
            * override this method for handling normal response from saveApplicationData operation
            */
           public void receiveResultsaveApplicationData(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from saveApplicationData operation
           */
            public void receiveErrorsaveApplicationData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for signOnWithToken method
            * override this method for handling normal response from signOnWithToken operation
            */
           public void receiveResultsignOnWithToken(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from signOnWithToken operation
           */
            public void receiveErrorsignOnWithToken(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteMerchantProfile method
            * override this method for handling normal response from deleteMerchantProfile operation
            */
           public void receiveResultdeleteMerchantProfile(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.DeleteMerchantProfileResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteMerchantProfile operation
           */
            public void receiveErrordeleteMerchantProfile(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getMerchantProfile method
            * override this method for handling normal response from getMerchantProfile operation
            */
           public void receiveResultgetMerchantProfile(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetMerchantProfileResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getMerchantProfile operation
           */
            public void receiveErrorgetMerchantProfile(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getServiceInformation method
            * override this method for handling normal response from getServiceInformation operation
            */
           public void receiveResultgetServiceInformation(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getServiceInformation operation
           */
            public void receiveErrorgetServiceInformation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for isMerchantProfileInitialized method
            * override this method for handling normal response from isMerchantProfileInitialized operation
            */
           public void receiveResultisMerchantProfileInitialized(
                    com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from isMerchantProfileInitialized operation
           */
            public void receiveErrorisMerchantProfileInitialized(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for ping method
            * override this method for handling normal response from ping operation
            */
           public void receiveResultping(
                    com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ping operation
           */
            public void receiveErrorping(java.lang.Exception e) {
            }
                


    }
    