
/**
 * ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

package com.ipcommerce.schemas.cws.serviceinformation;

public class ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage extends java.lang.Exception{
    
    private com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault faultMessage;

    
        public ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage() {
            super("ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage");
        }

        public ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ICWSServiceInformation_SaveApplicationData_CWSInvalidSecurityTokenFaultFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault msg){
       faultMessage = msg;
    }
    
    public com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSInvalidSecurityTokenFault getFaultMessage(){
       return faultMessage;
    }
}
    