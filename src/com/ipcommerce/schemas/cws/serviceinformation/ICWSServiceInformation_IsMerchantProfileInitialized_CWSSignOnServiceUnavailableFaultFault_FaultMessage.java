
/**
 * ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

package com.ipcommerce.schemas.cws.serviceinformation;

public class ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage extends java.lang.Exception{
    
    private com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault faultMessage;

    
        public ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage() {
            super("ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
        }

        public ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ICWSServiceInformation_IsMerchantProfileInitialized_CWSSignOnServiceUnavailableFaultFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault msg){
       faultMessage = msg;
    }
    
    public com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSSignOnServiceUnavailableFault getFaultMessage(){
       return faultMessage;
    }
}
    