
/**
 * ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

package com.ipcommerce.schemas.cws.transactions;

public class ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage extends java.lang.Exception{
    
    private com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault faultMessage;

    
        public ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage() {
            super("ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
        }

        public ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault msg){
       faultMessage = msg;
    }
    
    public com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault getFaultMessage(){
       return faultMessage;
    }
}
    