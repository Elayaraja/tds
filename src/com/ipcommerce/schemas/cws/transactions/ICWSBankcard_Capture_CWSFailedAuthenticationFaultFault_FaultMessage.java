
/**
 * ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

package com.ipcommerce.schemas.cws.transactions;

public class ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage extends java.lang.Exception{
    
    private com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault faultMessage;

    
        public ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage() {
            super("ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage");
        }

        public ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault msg){
       faultMessage = msg;
    }
    
    public com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault getFaultMessage(){
       return faultMessage;
    }
}
    