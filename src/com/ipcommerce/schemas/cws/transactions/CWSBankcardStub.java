
/**
 * CWSBankcardStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */
        package com.ipcommerce.schemas.cws.transactions;

        

        /*
        *  CWSBankcardStub java implementation
        */

        
        public class CWSBankcardStub extends org.apache.axis2.client.Stub
        {
        protected org.apache.axis2.description.AxisOperation[] _operations;

        //hashmaps to keep the fault mapping
        private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
        private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
        private java.util.HashMap faultMessageMap = new java.util.HashMap();

        private static int counter = 0;

        private static synchronized java.lang.String getUniqueSuffix(){
            // reset the counter if it is greater than 99999
            if (counter > 99999){
                counter = 0;
            }
            counter = counter + 1; 
            return java.lang.Long.toString(System.currentTimeMillis()) + "_" + counter;
        }

    
    private void populateAxisService() throws org.apache.axis2.AxisFault {

     //creating the Service with a unique name
     _service = new org.apache.axis2.description.AxisService("CWSBankcard" + getUniqueSuffix());
     addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[12];
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "ping"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[0]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "returnUnlinked"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[1]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "queryAccount"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[2]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "adjust"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[3]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "captureAll"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[4]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "authorizeAndCapture"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[5]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "undo"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[6]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "captureSelective"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[7]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "verify"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[8]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "authorize"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[9]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "capture"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[10]=__operation;
            
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard", "returnById"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"TransactionProcessing_ICWSBankcard_policy\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Strict /></wsp:Policy></sp:Layout></wsp:Policy></sp:TransportBinding></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[11]=__operation;
            
        
        }

    //populates the faults
    private void populateFaults(){
         
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionAlreadySettledFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionAlreadySettledFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionAlreadySettledFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionAlreadySettledFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionAlreadySettledFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionAlreadySettledFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExpiredSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidMessageFormatFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidMessageFormatFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSExtendedDataNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSOperationNotSupportedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSOperationNotSupportedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidSecurityTokenFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidSecurityTokenFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSValidationResultFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFailedAuthenticationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSFailedAuthenticationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidServiceInformationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidServiceInformationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionFailedFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionFailedFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSTransactionFailedFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidOperationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidOperationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSInvalidOperationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSSignOnServiceUnavailableFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSSignOnServiceUnavailableFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
           
              faultExceptionNameMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSDeserializationFaultFault_FaultMessage");
              faultExceptionClassNameMap.put(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSDeserializationFaultFault_FaultMessage");
              faultMessageMap.put( new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults","CWSDeserializationFault"),"com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
           


    }

    /**
      *Constructor that takes in a configContext
      */

    public CWSBankcardStub(org.apache.axis2.context.ConfigurationContext configurationContext,
       java.lang.String targetEndpoint)
       throws org.apache.axis2.AxisFault {
         this(configurationContext,targetEndpoint,false);
   }


   /**
     * Constructor that takes in a configContext  and useseperate listner
     */
   public CWSBankcardStub(org.apache.axis2.context.ConfigurationContext configurationContext,
        java.lang.String targetEndpoint, boolean useSeparateListener)
        throws org.apache.axis2.AxisFault {
         //To populate AxisService
         populateAxisService();
         populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext,_service);
        
        _service.applyPolicy();
        
	
        _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(
                targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);
        
    
    }

    /**
     * Default Constructor
     */
    public CWSBankcardStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {
        
                    this(configurationContext,"https://cws-01.ipcommerce.com/2.0/Txn/B30B89F5E6D00001" );
                
    }

    /**
     * Default Constructor
     */
    public CWSBankcardStub() throws org.apache.axis2.AxisFault {
        
                    this("https://cws-01.ipcommerce.com/2.0/Txn/B30B89F5E6D00001" );
                
    }

    /**
     * Constructor taking the target endpoint
     */
    public CWSBankcardStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        this(null,targetEndpoint);
    }



        
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#ping
                     * @param ping0
                    
                     */

                    

                            public  com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse ping(

                            com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping ping0)
                        

                    throws java.rmi.RemoteException
                    
                    {
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common/IExternallyFacingStandardOperations/Ping");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    ping0,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "ping")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startping
                    * @param ping0
                
                */
                public  void startping(

                 com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping ping0,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common/IExternallyFacingStandardOperations/Ping");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    ping0,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "ping")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultping(
                                        (com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorping(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
					
										            callback.receiveErrorping(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorping(f);
                                            }
									    } else {
										    callback.receiveErrorping(f);
									    }
									} else {
									    callback.receiveErrorping(f);
									}
								} else {
								    callback.receiveErrorping(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorping(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[0].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[0].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#returnUnlinked
                     * @param returnUnlinked2
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse returnUnlinked(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked returnUnlinked2)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/ReturnUnlinked");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    returnUnlinked2,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "returnUnlinked")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startreturnUnlinked
                    * @param returnUnlinked2
                
                */
                public  void startreturnUnlinked(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked returnUnlinked2,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/ReturnUnlinked");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    returnUnlinked2,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "returnUnlinked")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultreturnUnlinked(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorreturnUnlinked(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorreturnUnlinked((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorreturnUnlinked(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnUnlinked(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnUnlinked(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnUnlinked(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnUnlinked(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnUnlinked(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnUnlinked(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnUnlinked(f);
                                            }
									    } else {
										    callback.receiveErrorreturnUnlinked(f);
									    }
									} else {
									    callback.receiveErrorreturnUnlinked(f);
									}
								} else {
								    callback.receiveErrorreturnUnlinked(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorreturnUnlinked(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[1].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[1].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#queryAccount
                     * @param queryAccount4
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse queryAccount(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccount queryAccount4)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/QueryAccount");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    queryAccount4,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "queryAccount")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startqueryAccount
                    * @param queryAccount4
                
                */
                public  void startqueryAccount(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccount queryAccount4,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/QueryAccount");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    queryAccount4,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "queryAccount")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultqueryAccount(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorqueryAccount(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorqueryAccount((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_QueryAccount_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorqueryAccount(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorqueryAccount(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorqueryAccount(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorqueryAccount(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorqueryAccount(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorqueryAccount(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorqueryAccount(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorqueryAccount(f);
                                            }
									    } else {
										    callback.receiveErrorqueryAccount(f);
									    }
									} else {
									    callback.receiveErrorqueryAccount(f);
									}
								} else {
								    callback.receiveErrorqueryAccount(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorqueryAccount(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[2].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[2].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#adjust
                     * @param adjust6
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse adjust(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust adjust6)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[3].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Adjust");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    adjust6,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "adjust")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startadjust
                    * @param adjust6
                
                */
                public  void startadjust(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust adjust6,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[3].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Adjust");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    adjust6,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "adjust")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultadjust(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErroradjust(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErroradjust((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErroradjust(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErroradjust(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErroradjust(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErroradjust(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErroradjust(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErroradjust(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErroradjust(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErroradjust(f);
                                            }
									    } else {
										    callback.receiveErroradjust(f);
									    }
									} else {
									    callback.receiveErroradjust(f);
									}
								} else {
								    callback.receiveErroradjust(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErroradjust(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[3].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[3].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#captureAll
                     * @param captureAll8
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse captureAll(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll captureAll8)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[4].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/CaptureAll");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    captureAll8,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "captureAll")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startcaptureAll
                    * @param captureAll8
                
                */
                public  void startcaptureAll(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll captureAll8,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[4].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/CaptureAll");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    captureAll8,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "captureAll")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultcaptureAll(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorcaptureAll(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorcaptureAll((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureAll_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorcaptureAll(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureAll(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureAll(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureAll(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureAll(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureAll(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureAll(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureAll(f);
                                            }
									    } else {
										    callback.receiveErrorcaptureAll(f);
									    }
									} else {
									    callback.receiveErrorcaptureAll(f);
									}
								} else {
								    callback.receiveErrorcaptureAll(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorcaptureAll(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[4].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[4].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#authorizeAndCapture
                     * @param authorizeAndCapture10
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse authorizeAndCapture(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture authorizeAndCapture10)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[5].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/AuthorizeAndCapture");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    authorizeAndCapture10,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "authorizeAndCapture")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startauthorizeAndCapture
                    * @param authorizeAndCapture10
                
                */
                public  void startauthorizeAndCapture(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture authorizeAndCapture10,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[5].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/AuthorizeAndCapture");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    authorizeAndCapture10,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "authorizeAndCapture")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultauthorizeAndCapture(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorauthorizeAndCapture(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorauthorizeAndCapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorauthorizeAndCapture(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorizeAndCapture(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorizeAndCapture(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorizeAndCapture(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorizeAndCapture(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorizeAndCapture(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorizeAndCapture(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorizeAndCapture(f);
                                            }
									    } else {
										    callback.receiveErrorauthorizeAndCapture(f);
									    }
									} else {
									    callback.receiveErrorauthorizeAndCapture(f);
									}
								} else {
								    callback.receiveErrorauthorizeAndCapture(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorauthorizeAndCapture(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[5].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[5].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#undo
                     * @param undo12
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse undo(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo undo12)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[6].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Undo");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    undo12,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "undo")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startundo
                    * @param undo12
                
                */
                public  void startundo(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo undo12,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[6].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Undo");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    undo12,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "undo")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultundo(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorundo(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorundo((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorundo(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorundo(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorundo(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorundo(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorundo(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorundo(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorundo(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorundo(f);
                                            }
									    } else {
										    callback.receiveErrorundo(f);
									    }
									} else {
									    callback.receiveErrorundo(f);
									}
								} else {
								    callback.receiveErrorundo(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorundo(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[6].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[6].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#captureSelective
                     * @param captureSelective14
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionAlreadySettledFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSDeserializationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse captureSelective(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelective captureSelective14)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionAlreadySettledFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSDeserializationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSSignOnServiceUnavailableFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[7].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/CaptureSelective");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    captureSelective14,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "captureSelective")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionAlreadySettledFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionAlreadySettledFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startcaptureSelective
                    * @param captureSelective14
                
                */
                public  void startcaptureSelective(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelective captureSelective14,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[7].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/CaptureSelective");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    captureSelective14,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "captureSelective")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultcaptureSelective(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorcaptureSelective(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionAlreadySettledFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionAlreadySettledFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorcaptureSelective((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_CaptureSelective_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorcaptureSelective(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureSelective(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureSelective(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureSelective(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureSelective(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureSelective(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureSelective(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcaptureSelective(f);
                                            }
									    } else {
										    callback.receiveErrorcaptureSelective(f);
									    }
									} else {
									    callback.receiveErrorcaptureSelective(f);
									}
								} else {
								    callback.receiveErrorcaptureSelective(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorcaptureSelective(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[7].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[7].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#verify
                     * @param verify16
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse verify(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Verify verify16)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[8].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Verify");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    verify16,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "verify")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startverify
                    * @param verify16
                
                */
                public  void startverify(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Verify verify16,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[8].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Verify");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    verify16,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "verify")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultverify(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorverify(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorverify((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Verify_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorverify(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorverify(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorverify(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorverify(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorverify(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorverify(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorverify(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorverify(f);
                                            }
									    } else {
										    callback.receiveErrorverify(f);
									    }
									} else {
									    callback.receiveErrorverify(f);
									}
								} else {
								    callback.receiveErrorverify(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorverify(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[8].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[8].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#authorize
                     * @param authorize18
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse authorize(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize authorize18)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[9].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Authorize");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    authorize18,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "authorize")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startauthorize
                    * @param authorize18
                
                */
                public  void startauthorize(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize authorize18,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[9].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Authorize");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    authorize18,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "authorize")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultauthorize(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorauthorize(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorauthorize((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorauthorize(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorize(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorize(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorize(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorize(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorize(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorize(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorauthorize(f);
                                            }
									    } else {
										    callback.receiveErrorauthorize(f);
									    }
									} else {
									    callback.receiveErrorauthorize(f);
									}
								} else {
								    callback.receiveErrorauthorize(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorauthorize(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[9].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[9].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#capture
                     * @param capture20
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse capture(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture capture20)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[10].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Capture");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    capture20,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "capture")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startcapture
                    * @param capture20
                
                */
                public  void startcapture(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture capture20,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[10].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/Capture");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    capture20,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "capture")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultcapture(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorcapture(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorcapture((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorcapture(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcapture(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcapture(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcapture(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcapture(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcapture(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcapture(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorcapture(f);
                                            }
									    } else {
										    callback.receiveErrorcapture(f);
									    }
									} else {
									    callback.receiveErrorcapture(f);
									}
								} else {
								    callback.receiveErrorcapture(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorcapture(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[10].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[10].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                
                    /**
                     * Auto generated method signature
                     * 
                     * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#returnById
                     * @param returnById22
                    
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidMessageFormatFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExtendedDataNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSOperationNotSupportedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidSecurityTokenFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFailedAuthenticationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidServiceInformationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionFailedFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidOperationFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSSignOnServiceUnavailableFaultFault_FaultMessage : 
                     * @throws com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSDeserializationFaultFault_FaultMessage : 
                     */

                    

                            public  com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse returnById(

                            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById returnById22)
                        

                    throws java.rmi.RemoteException
                    
                    
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidMessageFormatFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExtendedDataNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSOperationNotSupportedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidSecurityTokenFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFailedAuthenticationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidServiceInformationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionFailedFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidOperationFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSSignOnServiceUnavailableFaultFault_FaultMessage
                        ,com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSDeserializationFaultFault_FaultMessage{
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[11].getName());
              _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/ReturnById");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    returnById22,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "returnById")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidMessageFormatFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidMessageFormatFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSOperationNotSupportedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSOperationNotSupportedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidSecurityTokenFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFailedAuthenticationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFailedAuthenticationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidServiceInformationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidServiceInformationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionFailedFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionFailedFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidOperationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidOperationFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex;
                        }
                        
                        if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSDeserializationFaultFault_FaultMessage){
                          throw (com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSDeserializationFaultFault_FaultMessage)ex;
                        }
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see com.ipcommerce.schemas.cws.transactions.CWSBankcard#startreturnById
                    * @param returnById22
                
                */
                public  void startreturnById(

                 com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById returnById22,

                  final com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[11].getName());
             _operationClient.getOptions().setAction("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard/ICWSBankcard/ReturnById");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    returnById22,
                                                    optimizeContent(new javax.xml.namespace.QName("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                    "returnById")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultreturnById(
                                        (com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorreturnById(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidMessageFormatFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidMessageFormatFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExtendedDataNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExtendedDataNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSOperationNotSupportedFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSOperationNotSupportedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidSecurityTokenFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidSecurityTokenFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFailedAuthenticationFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSFailedAuthenticationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidServiceInformationFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidServiceInformationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionFailedFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSTransactionFailedFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidOperationFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSInvalidOperationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSSignOnServiceUnavailableFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSSignOnServiceUnavailableFaultFault_FaultMessage)ex);
											            return;
										            }
										            
													if (ex instanceof com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSDeserializationFaultFault_FaultMessage){
														callback.receiveErrorreturnById((com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSDeserializationFaultFault_FaultMessage)ex);
											            return;
										            }
										            
					
										            callback.receiveErrorreturnById(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnById(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnById(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnById(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnById(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnById(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnById(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorreturnById(f);
                                            }
									    } else {
										    callback.receiveErrorreturnById(f);
									    }
									} else {
									    callback.receiveErrorreturnById(f);
									}
								} else {
								    callback.receiveErrorreturnById(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorreturnById(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[11].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[11].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                


       /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
       private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
       return returnMap;
    }

    
    ////////////////////////////////////////////////////////////////////////
    
    private static org.apache.neethi.Policy getPolicy (java.lang.String policyString) {
    	java.io.ByteArrayInputStream bais = new java.io.ByteArrayInputStream(policyString.getBytes());
    	return org.apache.neethi.PolicyEngine.getPolicy(bais);
    }
    
    /////////////////////////////////////////////////////////////////////////

    
    
    private javax.xml.namespace.QName[] opNameArray = null;
    private boolean optimizeContent(javax.xml.namespace.QName opName) {
        

        if (opNameArray == null) {
            return false;
        }
        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;   
            }
        }
        return false;
    }
     //https://cws-01.ipcommerce.com/2.0/Txn/B30B89F5E6D00001
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_ipc_general_wcf_contracts_Ping;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_ipc_general_wcf_contracts_PingResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnUnlinked;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnUnlinkedResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSExpiredSecurityTokenFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidMessageFormatFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSExtendedDataNotSupportedFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSOperationNotSupportedFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidSecurityTokenFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSValidationResultFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSFailedAuthenticationFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionServiceUnavailableFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidServiceInformationFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionFailedFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidOperationFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSSignOnServiceUnavailableFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSDeserializationFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_QueryAccount;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_QueryAccountResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Adjust;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AdjustResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureAll;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureAllResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeAndCapture;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeAndCaptureResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Undo;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_UndoResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureSelective;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureSelectiveResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionAlreadySettledFault;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Verify;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_VerifyResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Authorize;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Capture;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureResponse;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnById;
            
                private static javax.xml.bind.JAXBContext _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnByIdResponse;
            

        private static final java.util.HashMap<Class,javax.xml.bind.JAXBContext> classContextMap = new java.util.HashMap<Class,javax.xml.bind.JAXBContext>();

        static {
            javax.xml.bind.JAXBContext jc;
            
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_ipc_general_wcf_contracts_Ping = jc;
                        classContextMap.put(com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_ipc_general_wcf_contracts_PingResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnUnlinked = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnUnlinkedResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSExpiredSecurityTokenFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidMessageFormatFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSExtendedDataNotSupportedFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSOperationNotSupportedFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidSecurityTokenFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSValidationResultFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSFailedAuthenticationFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionServiceUnavailableFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidServiceInformationFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionFailedFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidOperationFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSSignOnServiceUnavailableFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSDeserializationFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccount.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccount");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_QueryAccount = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccount.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_QueryAccountResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Adjust = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AdjustResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureAll = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureAllResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeAndCapture = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeAndCaptureResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Undo = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_UndoResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelective.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelective");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureSelective = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelective.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureSelectiveResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionAlreadySettledFault.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionAlreadySettledFault");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionAlreadySettledFault = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionAlreadySettledFault.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Verify.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Verify");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Verify = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Verify.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_VerifyResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Authorize = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Capture = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnById = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById.class, jc);
                    }
                
                    jc = null;
                    try {
                        jc = javax.xml.bind.JAXBContext.newInstance(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse.class);
                    }
                    catch ( javax.xml.bind.JAXBException ex ) {
                        System.err.println("Unable to create JAXBContext for class: com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse");
                        Runtime.getRuntime().exit(-1);
                    }
                    finally {
                        _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnByIdResponse = jc;
                        classContextMap.put(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse.class, jc);
                    }
                
        }

        

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_ipc_general_wcf_contracts_Ping;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common",
                                                                        "Ping");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common",
                                                                           null);
                        return factory.createOMElement(source, "Ping", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.ipc_general_wcf_contracts.Ping param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_ipc_general_wcf_contracts_PingResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common",
                                                                        "PingResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/Ipc.General.WCF.Contracts.Common",
                                                                           null);
                        return factory.createOMElement(source, "PingResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnUnlinked;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "ReturnUnlinked");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "ReturnUnlinked", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnUnlinkedResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "ReturnUnlinkedResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "ReturnUnlinkedResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSExpiredSecurityTokenFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSExpiredSecurityTokenFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSExpiredSecurityTokenFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExpiredSecurityTokenFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidMessageFormatFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSInvalidMessageFormatFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSInvalidMessageFormatFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidMessageFormatFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSExtendedDataNotSupportedFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSExtendedDataNotSupportedFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSExtendedDataNotSupportedFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSOperationNotSupportedFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSOperationNotSupportedFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSOperationNotSupportedFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidSecurityTokenFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSInvalidSecurityTokenFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSInvalidSecurityTokenFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidSecurityTokenFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSValidationResultFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSValidationResultFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSValidationResultFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSFailedAuthenticationFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSFailedAuthenticationFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSFailedAuthenticationFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSFailedAuthenticationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionServiceUnavailableFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSTransactionServiceUnavailableFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSTransactionServiceUnavailableFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionServiceUnavailableFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidServiceInformationFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSInvalidServiceInformationFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSInvalidServiceInformationFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidServiceInformationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionFailedFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSTransactionFailedFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSTransactionFailedFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionFailedFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSInvalidOperationFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSInvalidOperationFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSInvalidOperationFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSInvalidOperationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSSignOnServiceUnavailableFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSSignOnServiceUnavailableFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSSignOnServiceUnavailableFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSSignOnServiceUnavailableFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSDeserializationFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSDeserializationFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSDeserializationFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSDeserializationFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccount param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_QueryAccount;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccount.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "QueryAccount");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "QueryAccount", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccount param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_QueryAccountResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "QueryAccountResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "QueryAccountResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Adjust;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "Adjust");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "Adjust", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AdjustResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "AdjustResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "AdjustResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureAll;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "CaptureAll");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "CaptureAll", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureAllResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "CaptureAllResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "CaptureAllResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeAndCapture;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "AuthorizeAndCapture");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "AuthorizeAndCapture", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeAndCaptureResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "AuthorizeAndCaptureResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "AuthorizeAndCaptureResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Undo;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "Undo");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "Undo", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_UndoResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "UndoResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "UndoResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelective param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureSelective;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelective.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "CaptureSelective");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "CaptureSelective", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelective param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureSelectiveResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "CaptureSelectiveResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "CaptureSelectiveResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionAlreadySettledFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_faults_CWSTransactionAlreadySettledFault;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionAlreadySettledFault.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                        "CWSTransactionAlreadySettledFault");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Faults",
                                                                           null);
                        return factory.createOMElement(source, "CWSTransactionAlreadySettledFault", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSTransactionAlreadySettledFault param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Verify param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Verify;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Verify.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "Verify");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "Verify", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Verify param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_VerifyResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "VerifyResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "VerifyResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Authorize;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "Authorize");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "Authorize", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_AuthorizeResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "AuthorizeResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "AuthorizeResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_Capture;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "Capture");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "Capture", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_CaptureResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "CaptureResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "CaptureResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnById;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "ReturnById");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "ReturnById", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

                private org.apache.axiom.om.OMElement toOM(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    try {
                        javax.xml.bind.JAXBContext context = _com_ipcommerce_schemas_cws_v2_0_transactions_bankcard_ReturnByIdResponse;
                        javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                        marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FRAGMENT, Boolean.TRUE);

                        org.apache.axiom.om.OMFactory factory = org.apache.axiom.om.OMAbstractFactory.getOMFactory();

                        JaxbRIDataSource source = new JaxbRIDataSource( com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse.class,
                                                                        param,
                                                                        marshaller,
                                                                        "http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                        "ReturnByIdResponse");
                        org.apache.axiom.om.OMNamespace namespace = factory.createOMNamespace("http://schemas.ipcommerce.com/CWS/v2.0/Transactions/Bankcard",
                                                                           null);
                        return factory.createOMElement(source, "ReturnByIdResponse", namespace);
                    } catch (javax.xml.bind.JAXBException bex){
                        throw org.apache.axis2.AxisFault.makeFault(bex);
                    }
                }

                private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse param, boolean optimizeContent)
                throws org.apache.axis2.AxisFault {
                    org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
                    envelope.getBody().addChild(toOM(param, optimizeContent));
                    return envelope;
                }

                

        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
            return factory.getDefaultEnvelope();
        }

        private java.lang.Object fromOM (
            org.apache.axiom.om.OMElement param,
            java.lang.Class type,
            java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{
            try {
                javax.xml.bind.JAXBContext context = classContextMap.get(type);
                javax.xml.bind.Unmarshaller unmarshaller = context.createUnmarshaller();

                return unmarshaller.unmarshal(param.getXMLStreamReaderWithoutCaching(), type).getValue();
            } catch (javax.xml.bind.JAXBException bex){
                throw org.apache.axis2.AxisFault.makeFault(bex);
            }
        }

        class JaxbRIDataSource implements org.apache.axiom.om.OMDataSource {
            /**
             * Bound object for output.
             */
            private final Object outObject;

            /**
             * Bound class for output.
             */
            private final Class outClazz;

            /**
             * Marshaller.
             */
            private final javax.xml.bind.Marshaller marshaller;

            /**
             * Namespace
             */
            private String nsuri;

            /**
             * Local name
             */
            private String name;

            /**
             * Constructor from object and marshaller.
             *
             * @param obj
             * @param marshaller
             */
            public JaxbRIDataSource(Class clazz, Object obj, javax.xml.bind.Marshaller marshaller, String nsuri, String name) {
                this.outClazz = clazz;
                this.outObject = obj;
                this.marshaller = marshaller;
                this.nsuri = nsuri;
                this.name = name;
            }

            public void serialize(java.io.OutputStream output, org.apache.axiom.om.OMOutputFormat format) throws javax.xml.stream.XMLStreamException {
                try {
                    marshaller.marshal(new javax.xml.bind.JAXBElement(
                            new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), output);
                } catch (javax.xml.bind.JAXBException e) {
                    throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
                }
            }

            public void serialize(java.io.Writer writer, org.apache.axiom.om.OMOutputFormat format) throws javax.xml.stream.XMLStreamException {
                try {
                    marshaller.marshal(new javax.xml.bind.JAXBElement(
                            new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), writer);
                } catch (javax.xml.bind.JAXBException e) {
                    throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
                }
            }

            public void serialize(javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                try {
                    marshaller.marshal(new javax.xml.bind.JAXBElement(
                            new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), xmlWriter);
                } catch (javax.xml.bind.JAXBException e) {
                    throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
                }
            }

            public javax.xml.stream.XMLStreamReader getReader() throws javax.xml.stream.XMLStreamException {
                try {
                    javax.xml.bind.JAXBContext context = classContextMap.get(outClazz);
                    org.apache.axiom.om.impl.builder.SAXOMBuilder builder = new org.apache.axiom.om.impl.builder.SAXOMBuilder();
                    javax.xml.bind.Marshaller marshaller = context.createMarshaller();
                    marshaller.marshal(new javax.xml.bind.JAXBElement(
                            new javax.xml.namespace.QName(nsuri, name), outObject.getClass(), outObject), builder);

                    return builder.getRootElement().getXMLStreamReader();
                } catch (javax.xml.bind.JAXBException e) {
                    throw new javax.xml.stream.XMLStreamException("Error in JAXB marshalling", e);
                }
            }
        }
        
    
   }
   