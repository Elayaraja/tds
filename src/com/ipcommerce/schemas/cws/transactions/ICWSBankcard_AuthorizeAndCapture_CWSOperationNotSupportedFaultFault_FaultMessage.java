
/**
 * ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

package com.ipcommerce.schemas.cws.transactions;

public class ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage extends java.lang.Exception{
    
    private com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault faultMessage;

    
        public ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage() {
            super("ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage");
        }

        public ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ICWSBankcard_AuthorizeAndCapture_CWSOperationNotSupportedFaultFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault msg){
       faultMessage = msg;
    }
    
    public com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSOperationNotSupportedFault getFaultMessage(){
       return faultMessage;
    }
}
    