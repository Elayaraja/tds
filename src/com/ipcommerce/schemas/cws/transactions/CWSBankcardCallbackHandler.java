
/**
 * CWSBankcardCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

    package com.ipcommerce.schemas.cws.transactions;

    /**
     *  CWSBankcardCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CWSBankcardCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CWSBankcardCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CWSBankcardCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for ping method
            * override this method for handling normal response from ping operation
            */
           public void receiveResultping(
                    com.ipcommerce.schemas.ipc_general_wcf_contracts.PingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from ping operation
           */
            public void receiveErrorping(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for returnUnlinked method
            * override this method for handling normal response from returnUnlinked operation
            */
           public void receiveResultreturnUnlinked(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from returnUnlinked operation
           */
            public void receiveErrorreturnUnlinked(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for queryAccount method
            * override this method for handling normal response from queryAccount operation
            */
           public void receiveResultqueryAccount(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.QueryAccountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from queryAccount operation
           */
            public void receiveErrorqueryAccount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for adjust method
            * override this method for handling normal response from adjust operation
            */
           public void receiveResultadjust(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from adjust operation
           */
            public void receiveErroradjust(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for captureAll method
            * override this method for handling normal response from captureAll operation
            */
           public void receiveResultcaptureAll(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAllResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from captureAll operation
           */
            public void receiveErrorcaptureAll(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for authorizeAndCapture method
            * override this method for handling normal response from authorizeAndCapture operation
            */
           public void receiveResultauthorizeAndCapture(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from authorizeAndCapture operation
           */
            public void receiveErrorauthorizeAndCapture(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for undo method
            * override this method for handling normal response from undo operation
            */
           public void receiveResultundo(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from undo operation
           */
            public void receiveErrorundo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for captureSelective method
            * override this method for handling normal response from captureSelective operation
            */
           public void receiveResultcaptureSelective(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureSelectiveResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from captureSelective operation
           */
            public void receiveErrorcaptureSelective(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for verify method
            * override this method for handling normal response from verify operation
            */
           public void receiveResultverify(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.VerifyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from verify operation
           */
            public void receiveErrorverify(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for authorize method
            * override this method for handling normal response from authorize operation
            */
           public void receiveResultauthorize(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from authorize operation
           */
            public void receiveErrorauthorize(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for capture method
            * override this method for handling normal response from capture operation
            */
           public void receiveResultcapture(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from capture operation
           */
            public void receiveErrorcapture(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for returnById method
            * override this method for handling normal response from returnById operation
            */
           public void receiveResultreturnById(
                    com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from returnById operation
           */
            public void receiveErrorreturnById(java.lang.Exception e) {
            }
                


    }
    