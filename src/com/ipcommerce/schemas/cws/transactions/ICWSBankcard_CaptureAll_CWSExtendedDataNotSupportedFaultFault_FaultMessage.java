
/**
 * ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5  Built on : Apr 30, 2009 (06:07:24 EDT)
 */

package com.ipcommerce.schemas.cws.transactions;

public class ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage extends java.lang.Exception{
    
    private com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault faultMessage;

    
        public ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage() {
            super("ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage");
        }

        public ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ICWSBankcard_CaptureAll_CWSExtendedDataNotSupportedFaultFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault msg){
       faultMessage = msg;
    }
    
    public com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSExtendedDataNotSupportedFault getFaultMessage(){
       return faultMessage;
    }
}
    