package com.tds.wrapperDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.CustomerProfile;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;
import com.tds.wrapper.WrapperBean;
public class WrapperDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(WrapperDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+WrapperDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}

	public static String getAssocode1(String driverID)
	{
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			String SQL = "Select DR_ASSOCODE From TDS_DRIVER where DR_SNO = '"+driverID+"'";
			csp_pst = csp_conn.prepareStatement(SQL);
			cat.info(SQL);
			rs = csp_pst.executeQuery();
			if(rs.next()) {
				 return rs.getString(1);
			}
			 return "";
		} catch (Exception sqex) {
			cat.error("TDSException WrapperDAO-->"+ sqex.getMessage());
			sqex.printStackTrace();
			 return "";
		} finally {
			dbcon.closeConnection();
		}
		 
	}
	
	public static String getTripIDForDriver(String driverID) {
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		
		dbcon = new TDSConnection();
		csp_conn = dbcon.getConnection();
		 
		try {
			String SQL = "SELECT OR_TRIP_ID FROM TDS_OPENREQUEST WHERE OR_TRIP_STATUS IN ('"+TDSConstants.jobAllocated+"','"+TDSConstants.performingDispatchProcesses+"') and OR_DRIVERID= '"+driverID+"' ORDER BY OR_SERVICEDATE desc";
			csp_pst = csp_conn.prepareStatement(SQL);
			//System.out.println(SQL);
			cat.info(SQL);
			rs = csp_pst.executeQuery();
			if(rs.next()) {
				 return rs.getString(1);
			}
			 return "";
		} catch (Exception sqex) {
			cat.error("TDSException WrapperDAO-->"+ sqex.getMessage());
			sqex.printStackTrace();
			 return "";
		} finally {
			dbcon.closeConnection();
		}
	}
	
	public static ArrayList<OpenRequestBO> getAllAccepetedTrip(WrapperBean driverData,int Status) {
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1=null;
		ResultSet rs = null,rs1=null;
		ArrayList<OpenRequestBO> al_all = new ArrayList<OpenRequestBO>();
		 
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query = ""; 
			String query1="";
			pst = con.prepareStatement("Select * from TDS_OPENREQUEST WHERE OR_ASSOCCODE=? and OR_TRIP_STATUS=? and OR_DRIVERID=?");
			pst.setString(1,driverData.getAssocode());
			pst.setInt(2,Status);
			pst.setString(3, driverData.getDriverid());
			//System.out.println("sql:"+pst.toString());
			
			rs = pst.executeQuery();
			while(rs.next()) {
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(rs.getString("OR_STADD2"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setSstate(rs.getString("OR_STSTATE"));
				openRequestBO.setSzip(rs.getString("OR_STZIP"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBO.setEzip(rs.getString("OR_EDZIP"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("OR_SERVICEDATE")));
				if(!TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME")).equalsIgnoreCase("")){
					openRequestBO.setShrs(rs.getString("OR_SERVICETIME").substring(0,2));
					openRequestBO.setSmin(rs.getString("OR_SERVICETIME").substring(2,4));
				}
				else {
					openRequestBO.setShrs("");
					openRequestBO.setSmin("");
				}
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME")));
//				openRequestBO.setSaircode(rs.getString("OR_SAIRCODE"));
//				openRequestBO.setEaircode(rs.getString("OR_EAIRCODE"));
				openRequestBO.setPaymentMeter(rs.getInt("OR_METER_TYPE"));
				if(rs.getInt("OR_METER_TYPE")==0){
					query1 = " AND MD_DEFAULT=1";
				} else {
					query1 = " AND MD_KEY='"+rs.getInt("OR_METER_TYPE")+"'";
				}
				pst1=con.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+driverData.getAssocode()+"'"+query1);
				rs1=pst1.executeQuery();
				if(rs1.next()){
					openRequestBO.setRatePerMile(Double.parseDouble(rs1.getString("MD_RATE_PER_MILE")));
					openRequestBO.setRatePerMin(Double.parseDouble(rs1.getString("MD_RATE_PER_MIN")));
					openRequestBO.setStartAmt(Double.parseDouble(rs1.getString("MD_START_AMOUNT")));
					openRequestBO.setMinSpeed(Integer.parseInt(rs1.getString("MD_MINIMUM_SPEED")));
				}
				al_all.add(openRequestBO);
				
			}
			return al_all;
		} catch (Exception sqex) {
			cat.error("TDSException WrapperDAO-->"+ sqex.getMessage());
			sqex.printStackTrace();
			 return al_all;
		} finally {
			dbcon.closeConnection();
		}
		
	}
	
	/*public static ArrayList<DriverAndJobs> getDriverAndJobSummary(String driverid) {
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		
		ArrayList<DriverAndJobs> list=new ArrayList<DriverAndJobs>();
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			//String SQL = "select * from (Select count(*) as numOfJob , OR_QUEUENO, QD_DESCRIPTION from TDS_OPENREQUEST OR, TDS_QUEUE_DETAILS QD WHERE OR.QUEUENO = QD.QUEUENAME AND OR_QUEUENO <> '' GROUP BY OR_QUEUENO AND OR_ASSOCCODE='"+assocode+"') A LEFT OUTER JOIN (SELECT COUNT(*) as numOfDrivers , QU_NAME from TDS_QUEUE GROUP BY QU_NAME AND QU_ASSOCCODE='"+assocode+"') B ON A.OR_QUEUENO = B.QU_NAME" 
			//				+ " UNION "
			//				+ "select * from (Select count(*) as numOfJob , OR_QUEUENO from TDS_OPENREQUEST OR_QUEUENO <> ''  GROUP BY OR_QUEUENO AND OR_ASSOCCODE='"+assocode+"') A RIGHT OUTER JOIN (SELECT COUNT(*) as numOfDrivers , QU_NAME, QD_DESCRIPTION from TDS_QUEUE Q, TDS_QUEUE_DETAILS QD WHERE Q.QU_NAME = QD.QUEUENAME GROUP BY QU_NAME AND QU_ASSOCCODE='"+assocode+"') B ON A.OR_QUEUENO = B.QU_NAME";
			String SQL = "select " +
					"	* from (" +
					"             Select" +
					"	OR_ASSOCCODE,count(*) as numOfJob , OR_QUEUENO, QD_DESCRIPTION " +
					" from " +
					"	TDS_OPENREQUEST, TDS_QUEUE_DETAILS,TDS_DRIVER" +
					" WHERE" +
					"	OR_QUEUENO = QD_QUEUENAME  AND OR_ASSOCCODE = QD_ASSOCCODE AND DR_ASSOCODE = OR_ASSOCCODE and DR_SNO='"+driverid+"' GROUP BY OR_QUEUENO ) A" +
					" LEFT OUTER JOIN" +
					"( " +
					"SELECT" +
					"	QU_ASSOCCODE,COUNT(*) as numOfDrivers , QU_NAME from TDS_QUEUE GROUP BY QU_NAME ) B ON A.OR_QUEUENO = B.QU_NAME AND B.QU_ASSOCCODE = A.OR_ASSOCCODE" +
					"					UNION" +
					"select * from (" +
					"Select" +
					"	OR_ASSOCCODE,count(*) as numOfJob , OR_QUEUENO from TDS_OPENREQUEST,TDS_DRIVER WHERE OR_QUEUENO <> '' AND DR_ASSOCODE = OR_ASSOCCODE and DR_SNO='"+driverid+"'" +
					"  GROUP BY OR_QUEUENO ) A" +
					"RIGHT OUTER JOIN" +
					"(" +
					"SELECT" +
					"		QD_DESCRIPTION,QU_ASSOCCODE, COUNT(*) as numOfDrivers , QU_NAME" +
					"	from" +
					"		TDS_QUEUE, TDS_QUEUE_DETAILS WHERE QU_NAME = QD_QUEUENAME AND QU_ASSOCCODE = QD_ASSOCCODE" +
					"	GROUP BY QU_NAME ) B" +
					" ON A.OR_QUEUENO = B.QU_NAME AND B.QU_ASSOCCODE = A.OR_ASSOCCODE AND B.QU_ASSOCCODE = A.OR_ASSOCCODE ) as Tb1";
			
			csp_pst = csp_conn.prepareStatement(SQL);
			cat.info(SQL);
			rs = csp_pst.executeQuery();
			while(rs.next()) {
				DriverAndJobs Jobs = new DriverAndJobs();
				if(rs.getString("OR_QUEUENO")!=null){
					Jobs.setZoneNumber(rs.getString("OR_QUEUENO"));
				} else {
					Jobs.setZoneNumber(rs.getString("QU_NAME"));
				}
				Jobs.setNumberOfJobs(rs.getString("numOfJob")==null?"0":rs.getString("numOfJob"));
				Jobs.setNumberOfDrivers(rs.getString("numOfDrivers")==null?"0":rs.getString("numOfDrivers"));
				Jobs.setZoneDesc(rs.getString("QD_DESCRIPTION"));
				list.add(Jobs);
			}
		} catch (Exception sqex) {
			cat.error("TDSException RegistrationDAO.getDriverAndJobSummary-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		return list;
	}*/

	

	public static WrapperBean getLoginDetails(String phoneno){
		cat.info("TDS INFO AddressDAO.Customerprofile");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		WrapperBean driver = null;

		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query = "select * from TDS_WRAPPER_XREF where WX_PHONE_NO = ?";
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, phoneno);

			ResultSet rs = m_pst.executeQuery();

			if(rs.next()){

				driver = new WrapperBean();
				driver.setDriverid(rs.getString("WX_DRIVERID"));
				driver.setPhoneNo(phoneno);
				driver.setSessionID(rs.getString("WX_SESSION_ID"));
				driver.setCabNo(rs.getString("WX_CABNO"));
				driver.setPassWord(rs.getString("WX_PASSWORD"));
				driver.setAssocode(rs.getString("WX_ASSOCIATION_CODE"));
				driver.setEmailAddress(rs.getString("WX_EMAIL_ID"));
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException WrapperDAO-->"+sqex.getMessage());
			//System.out.println("TDSException WrapperDAO-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			m_dbConn.closeConnection();
		}
		return driver;
	}

	public static int udpateSessionIDAndEmail(WrapperBean driverData){
		cat.info("TDS INFO AddressDAO.Customerprofile");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int rows =0;

		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query = "UPDATE TDS_WRAPPER_XREF SET WX_SESSION_ID = ?, WX_EMAIL_ID = ? where WX_DRIVERID = ?";
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, driverData.getSessionID());
			m_pst.setString(2, driverData.getEmailAddress());
			m_pst.setString(3, driverData.getDriverid());

			rows = m_pst.executeUpdate();
			
		}catch (Exception sqex)
		{
			cat.error("TDSException WrapperDAO-->"+sqex.getMessage());
			//System.out.println("TDSException WrapperDAO-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			m_dbConn.closeConnection();
		}
		return rows;
	}
	public static int logoutDriver(WrapperBean driverData){
		cat.info("TDS INFO AddressDAO.Customerprofile");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int rows =0;

		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query = "UPDATE TDS_WRAPPER_XREF SET WX_SESSION_ID = '' where WX_DRIVERID = ?";
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, driverData.getDriverid());

			rows = m_pst.executeUpdate();
			
		}catch (Exception sqex)
		{
			cat.error("TDSException AddressDAO.Customerprofile-->"+sqex.getMessage());
			//System.out.println("TDSException WrapperDAO-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			m_dbConn.closeConnection();
		}
		return rows;
	}

	public static WrapperBean checkAnotherCab(String cabNo, String associationCode){
		cat.info("TDS INFO AddressDAO.Customerprofile");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		WrapperBean driver = null;
		ArrayList<CustomerProfile> profileHistory = new ArrayList<CustomerProfile>();
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query = "SELECT * from TDS_WRAPPER_XREF where WX_CABNO = ? AND WX_SESSION_ID <> '' AND WX_ASSOCIATION_CODE=?";
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, cabNo);
			m_pst.setString(2, associationCode);
			//System.out.println("checking another cab"+m_pst.toString());
			ResultSet rs = m_pst.executeQuery();

			if(rs.next())
			{

				driver = new WrapperBean();
				driver.setDriverid(rs.getString("WX_DRIVERID"));
				driver.setPhoneNo(rs.getString("WX_PHONE_NO"));
				driver.setSessionID(rs.getString("WX_SESSION_ID"));
				driver.setCabNo(cabNo);
				driver.setPassWord(rs.getString("WX_PASSWORD"));
				driver.setEmailAddress(rs.getString("WX_EMAIL_ID"));
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException WrapperDAO-->"+sqex.getMessage());
			//System.out.println("TDSException WrapperDAO-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			m_dbConn.closeConnection();
		}
		return driver;
	}

}


