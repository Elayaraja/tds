package com.tds.tdsBO;

import java.io.Serializable;

public class passengerBO  implements Serializable {
	private static final long serialVersionUID = 1L;
	private String sno = "";
	private String uid = "";
	private String password = "";
	private String repassword ="";
	private String fname = "";
 	private String lname = "";
	private String add1 = "";
	private String add2 = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String phone = "";
	private char payment_type;
	private String payment_ac = "";
	private String start="";
	private String end="";
	private String pass_key="";
	
	
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getPass_key() {
		return pass_key;
	}
	public void setPass_key(String passKey) {
		pass_key = passKey;
	}
	public String getRepassword() {
		return repassword;
	}
	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}
	public String getSno() {
		return sno;
	}
	public void setSno(String sno) {
		this.sno = sno;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getAdd1() {
		return add1;
	}
	public void setAdd1(String add1) {
		this.add1 = add1;
	}
	public String getAdd2() {
		return add2;
	}
	public void setAdd2(String add2) {
		this.add2 = add2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	 
	public String getPayment_ac() {
		return payment_ac;
	}
	public void setPayment_ac(String paymentAc) {
		payment_ac = paymentAc;
	}
	public char getPayment_type() {
		return payment_type;
	}
	public void setPayment_type(char paymentType) {
		payment_type = paymentType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
