package com.tds.tdsBO;

import java.io.Serializable;

public class CompanyMasterBO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String cname = "";
	private String assccode = "";
	private String cdesc = "";
	private String add1 = "";
	private String add2 = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String bankacno = "";
	private String bankroutingno = "";
	private String bankname = "";
	private String phoneno = "";
	private char dipatchStatus;
	private String fileid;
	private String filename;
	private String Email="";
	private boolean show_flg=true;
	private String ccUsername="";
	private String ccPassword="";
	private int ccProvider;
	private int ccVendor;
	private String siteId="";
	private String priceId="";
	private String key="";

	
	
	
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getPriceId() {
		return priceId;
	}
	public void setPriceId(String priceId) {
		this.priceId = priceId;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getCcUsername() {
		return ccUsername;
	}
	public void setCcUsername(String ccUsername) {
		this.ccUsername = ccUsername;
	}
	public String getCcPassword() {
		return ccPassword;
	}
	public void setCcPassword(String ccPassword) {
		this.ccPassword = ccPassword;
	}
	public int getCcProvider() {
		return ccProvider;
	}
	public void setCcProvider(int ccProvider) {
		this.ccProvider = ccProvider;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFileid() {
		return fileid;
	}
	public void setFileid(String fileid) {
		this.fileid = fileid;
	}
	/**
	 * @return the dipatchStatus
	 */
	public char getDipatchStatus() {
		return dipatchStatus;
	}
	/**
	 * @param dipatchStatus the dipatchStatus to set
	 */
	public void setDipatchStatus(char dipatchStatus) {
		this.dipatchStatus = dipatchStatus;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getAdd1() {
		return add1;
	}
	public void setAdd1(String add1) {
		this.add1 = add1;
	}
	public String getAdd2() {
		return add2;
	}
	public void setAdd2(String add2) {
		this.add2 = add2;
	}
	public String getAssccode() {
		return assccode;
	}
	public void setAssccode(String assccode) {
		this.assccode = assccode;
	}
	public String getBankacno() {
		return bankacno;
	}
	public void setBankacno(String bankacno) {
		this.bankacno = bankacno;
	}
	public String getBankname() {
		return bankname;
	}
	public void setBankname(String bankname) {
		this.bankname = bankname;
	}
	public String getBankroutingno() {
		return bankroutingno;
	}
	public void setBankroutingno(String bankroutingno) {
		this.bankroutingno = bankroutingno;
	}
	public String getCdesc() {
		return cdesc;
	}
	public void setCdesc(String cdesc) {
		this.cdesc = cdesc;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public boolean isShow_flg() {
		return show_flg;
	}
	public void setShow_flg(boolean show_flg) {
		this.show_flg = show_flg;
	}
	public void setCcVendor(int ccVendor) {
		this.ccVendor = ccVendor;
	}
	public int getCcVendor() {
		return ccVendor;
	}
}
