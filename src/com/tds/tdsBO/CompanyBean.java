package com.tds.tdsBO;

import java.util.HashMap;

import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion.Static;

public class CompanyBean {

	private static HashMap<String, String> companyProperties;

	public static HashMap<String, String> getCompanyProperties() {
		return companyProperties;
	}

	public static void setCompanyProperties(HashMap<String, String> companyProperties) {
		CompanyBean.companyProperties = companyProperties;
	}
}
