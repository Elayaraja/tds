package com.tds.tdsBO;

import java.io.Serializable;

public class CabMaintenanceBO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String maintenanceKey;
	private String driverKey;
	private String assocCode;
	private String serviceDesc;
	private String serviceDate;
	private String serviceCode;
	private String sparePart;
	private String amount;
	private String driverName;
	
	/**
	 * @return the driverName
	 */
	public String getDriverName() {
		return driverName;
	}
	/**
	 * @param driverName the driverName to set
	 */
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}
	/**
	 * @return the maintenanceKey
	 */
	public String getMaintenanceKey() {
		return maintenanceKey;
	}
	/**
	 * @param maintenanceKey the maintenanceKey to set
	 */
	public void setMaintenanceKey(String maintenanceKey) {
		this.maintenanceKey = maintenanceKey;
	}
	/**
	 * @return the driverKey
	 */
	public String getDriverKey() {
		return driverKey;
	}
	/**
	 * @param driverKey the driverKey to set
	 */
	public void setDriverKey(String driverKey) {
		this.driverKey = driverKey;
	}
	/**
	 * @return the assocCode
	 */
	public String getAssocCode() {
		return assocCode;
	}
	/**
	 * @param assocCode the assocCode to set
	 */
	public void setAssocCode(String assocCode) {
		this.assocCode = assocCode;
	}
	/**
	 * @return the serviceDesc
	 */
	public String getServiceDesc() {
		return serviceDesc;
	}
	/**
	 * @param serviceDesc the serviceDesc to set
	 */
	public void setServiceDesc(String serviceDesc) {
		this.serviceDesc = serviceDesc;
	}
	/**
	 * @return the serviceDate
	 */
	public String getServiceDate() {
		return serviceDate;
	}
	/**
	 * @param serviceDate the serviceDate to set
	 */
	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}
	/**
	 * @return the serviceCode
	 */
	public String getServiceCode() {
		return serviceCode;
	}
	/**
	 * @param serviceCode the serviceCode to set
	 */
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	/**
	 * @return the sparePart
	 */
	public String getSparePart() {
		return sparePart;
	}
	/**
	 * @param sparePart the sparePart to set
	 */
	public void setSparePart(String sparePart) {
		this.sparePart = sparePart;
	}
}
