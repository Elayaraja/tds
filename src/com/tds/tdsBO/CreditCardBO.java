package com.tds.tdsBO;

import java.io.Serializable;

public class CreditCardBO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public String masterKey="";
	public String customerKey="";
	public String userName="";
	public String nameOnCard="";
	public String expiryDate="";
	public String cardNumber="";
	public String assoccode="";
	public String ccInfoKey="";
	public String getMasterKey() {
		return masterKey;
	}
	public void setMasterKey(String masterKey) {
		this.masterKey = masterKey;
	}
	public String getCustomerKey() {
		return customerKey;
	}
	public void setCustomerKey(String customerKey) {
		this.customerKey = customerKey;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getNameOnCard() {
		return nameOnCard;
	}
	public void setNameOnCard(String nameOnCard) {
		this.nameOnCard = nameOnCard;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getAssoccode() {
		return assoccode;
	}
	public void setAssoccode(String assoccode) {
		this.assoccode = assoccode;
	}
	public String getCcInfoKey() {
		return ccInfoKey;
	}
	public void setCcInfoKey(String ccInfoKey) {
		this.ccInfoKey = ccInfoKey;
	}
	
}
