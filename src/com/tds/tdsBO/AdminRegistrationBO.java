package com.tds.tdsBO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import com.tds.cmp.bean.CompanySystemProperties;

public class AdminRegistrationBO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String start = "";
	private String end = "";
	private String pcompany = "";
	private String uname = "";
	private String drid;
	private String password = "";
	private String repassword = "";
	private String usertype = "";
	private String usertypeDesc = "";
	private String paymentType = "";
	private String associateCode = "";
	private int isAvailable = 0;
	private String active = "";
	private String openreqentry="";
	private String driverreg="";
	private String driverupdate="";
	private String createvoucher="";
	private String acceptopenreq="";
	private String changeopenreq="";
	private String userreg="";
	private String openreqxml="";
	private String acceptreqxml="";
	private String tripsummaryrep="";
	private String userlist="";
	private String driversummary="";
	private String companymaster = "";
	private String Fname="";
	private String Lname = "";
	private String changeVoucher = "";
	private String voucherReport = "";
	private String uid = "";
	private String behavior = "";
	private String behaviorSummary;
	private String maintenance;
	private String maintenanceSummary;
	private String lostandfound;
	private String lostandfoundSummary;
	private String queuecoordinate;
	private String queuecoordinateSummary;
	private String dispatchMethod;
	private int dispatchBasedOnVehicleOrDriver;
	private int checkDocuments;
	private int shiftMode;
	private int addressFill=1;
	private String startAmount="";
	private String windowTime = "";
	private String loginLimitTime ="";
	private int driverAlarm=0;
	private String timeZone="";
	private int driverListMobile=0;
	private int driverRating=1;
	private int fareByDistance=0;
	private int zoneLogoutTime=0;
	private double onsiteDist=0.0;
	private String phonePrefix;
	private String smsPrefix;
	private int dispatchBroadcast=0;
	private String paymentUserID="";
	private String payPassword="";
	private int settingsAccess=1;
	private int callerIdType =0;
	private static HashMap<String, String> companyProperties;
	private int job_No_Show_Property=0;
	private int fleetDispatch = 0;
	private ArrayList<String> companyList = new ArrayList<String>();
	private String sessionId="";
	private String changeCompanyCode="";
	private String androidId="";
	private int checkDriverMobile = 0;
	private int sendPushToCustomer = 0;
	private String DefaultLanguage = "en";
	
	private String json_details = "";
	private String wasl_reference_id = "";
	
	public int getDriverListMobile() {
		return driverListMobile;
	}
	public void setDriverListMobile(int driverListMobile) {
		this.driverListMobile = driverListMobile;
	}
	private int tagDeviceCab=0;
	
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public int getTagDeviceCab() {
		return tagDeviceCab;
	}
	public void setTagDeviceCab(int tagDeviceCab) {
		this.tagDeviceCab = tagDeviceCab;
	}
	public int getDriverAlarm() {
		return driverAlarm;
	}
	public void setDriverAlarm(int driverAlarm) {
		this.driverAlarm = driverAlarm;
	}
	public int getShiftMode() {
		return shiftMode;
	}
	public void setShiftMode(int shiftMode) {
		this.shiftMode = shiftMode;
	}
	private String driverPaymentReport;
	private String companyPaymentReport;
	private String driverAvailability;
	private String Email;
	private String googRegKey;
	
	private double defaultLati = 0;
	private double defaultLogi = 0;
	
	private String vehicleNo= "";
	
	private String serviceKey;
	private String merchid;
	private String serviceid;  
	private String userNameDisplay=""; 
	private String driver_flag="";
	private int cabdrivermap=0;
	private String country="";
	private int callerId;
	private String ORTime="";
	private int ORFormat;
	
	private double ratePerMile;
	private boolean calculateZone;
	private String masterAssociateCode="";
	
	private int call_onRoute;
	private int call_onAccept;
	private int call_onSite;
	private int message_onRoute;
	private int message_onAccept;
	private int enable_stadd_flag = 0;
	private int message_onSite;
	private int currencyPrefix;
	private int useMaster0rNot=0;
	private int distanceBasedOn =0;
	private boolean meterMandatory =false;

	private String messageAccept="";
	private String messageRoute="";
	private String messageSite="";
	private String defaultMeterRate="";
	private boolean meterHidden=false;

	
	public String getMessageAccept() {
		return messageAccept;
	}
	public void setMessageAccept(String messageAccept) {
		this.messageAccept = messageAccept;
	}
	public String getMessageRoute() {
		return messageRoute;
	}
	public void setMessageRoute(String messageRoute) {
		this.messageRoute = messageRoute;
	}
	public String getMessageSite() {
		return messageSite;
	}
	public void setMessageSite(String messageSite) {
		this.messageSite = messageSite;
	}
	public int getUseMaster0rNot() {
		return useMaster0rNot;
	}
	public void setUseMaster0rNot(int useMaster0rNot) {
		this.useMaster0rNot = useMaster0rNot;
	}
	public int getCurrencyPrefix() {
		return currencyPrefix;
	}
	public void setCurrencyPrefix(int currencyPrefix) {
		this.currencyPrefix = currencyPrefix;
	}
	public double getRatePerMile() {
		return ratePerMile;
	}
	public void setRatePerMile(double ratePerMile) {
		this.ratePerMile = ratePerMile;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	private String state="";
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public double getTimeZoneOffet() {
		return timeZoneOffet;
	}
	public void setTimeZoneOffet(double timeZoneOffet) {
		this.timeZoneOffet = timeZoneOffet;
	}
	private double timeZoneOffet= 0;

	
//	private final CompanySystemProperties cmpSystemProperties = new CompanySystemProperties();
//	
//	public CompanySystemProperties getCmpSystemProperties() {
//		return cmpSystemProperties;
//	}
	
	public int getCabdrivermap() {
		return cabdrivermap;
	}
	public void setCabdrivermap(int cabdrivermap) {
		this.cabdrivermap = cabdrivermap;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getChargeTypeDesc() {
		return chargeTypeDesc;
	}
	public void setChargeTypeDesc(String chargeTypeDesc) {
		this.chargeTypeDesc = chargeTypeDesc;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getDriver_flag() {
		return driver_flag;
	}
	public void setDriver_flag(String driverFlag) {
		driver_flag = driverFlag;
	}
	public String getUserNameDisplay() {
		return userNameDisplay;
	}
	public void setUserNameDisplay(String userNameDisplay) {
		this.userNameDisplay = userNameDisplay;
	}
	//field for mobile login details add extra
	private String provider;
	private String phnumber;
	
	//for multi driver charge
	private String effectiveDate="";
	private String description="";
	private String chargeTypeDesc="";
	private String amt="";
	

	
	
	
	private ArrayList roles = new ArrayList();
	
	public String getServiceKey() {
		return serviceKey;
	}
	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}
	/**
	 * @return the driverPaymentReport
	 */
	public String getDriverPaymentReport() {
		return driverPaymentReport;
	}
	/**
	 * @param driverPaymentReport the driverPaymentReport to set
	 */
	public void setDriverPaymentReport(String driverPaymentReport) {
		this.driverPaymentReport = driverPaymentReport;
	}
	/**
	 * @return the companyPaymentReport
	 */
	public String getCompanyPaymentReport() {
		return companyPaymentReport;
	}
	/**
	 * @param companyPaymentReport the companyPaymentReport to set
	 */
	public void setCompanyPaymentReport(String companyPaymentReport) {
		this.companyPaymentReport = companyPaymentReport;
	}
	/**
	 * @return the driverAvailability
	 */
	public String getDriverAvailability() {
		return driverAvailability;
	}
	/**
	 * @param driverAvailability the driverAvailability to set
	 */
	public void setDriverAvailability(String driverAvailability) {
		this.driverAvailability = driverAvailability;
	}
	/**
	 * @return the dispatchMethod
	 */
	public String getDispatchMethod() {
		return dispatchMethod;
	}
	/**
	 * @param dispatchMethod the dispatchMethod to set
	 */
	public void setDispatchMethod(String dispatchMethod) {
		this.dispatchMethod = dispatchMethod;
	}
	public int getDispatchBasedOnVehicleOrDriver() {
		return dispatchBasedOnVehicleOrDriver;
	}
	public void setDispatchBasedOnVehicleOrDriver(int dispatchBasedOnVehicleOrDriver) {
		this.dispatchBasedOnVehicleOrDriver = dispatchBasedOnVehicleOrDriver;
	}
	/**
	 * @return the behavior
	 */
	public String getBehavior() {
		return behavior;
	}
	/**
	 * @param behavior the behavior to set
	 */
	public void setBehavior(String behavior) {
		this.behavior = behavior;
	}
	/**
	 * @return the behaviorSummary
	 */
	public String getBehaviorSummary() {
		return behaviorSummary;
	}
	/**
	 * @param behaviorSummary the behaviorSummary to set
	 */
	public void setBehaviorSummary(String behaviorSummary) {
		this.behaviorSummary = behaviorSummary;
	}
	/**
	 * @return the maintenance
	 */
	public String getMaintenance() {
		return maintenance;
	}
	/**
	 * @param maintenance the maintenance to set
	 */
	public void setMaintenance(String maintenance) {
		this.maintenance = maintenance;
	}
	/**
	 * @return the maintenanceSummary
	 */
	public String getMaintenanceSummary() {
		return maintenanceSummary;
	}
	/**
	 * @param maintenanceSummary the maintenanceSummary to set
	 */
	public void setMaintenanceSummary(String maintenanceSummary) {
		this.maintenanceSummary = maintenanceSummary;
	}
	/**
	 * @return the lost and found
	 */
	public String getLostandfound() {
		return lostandfound;
	}
	/**
	 * @param lostandfound the lost and found to set
	 */
	public void setLostandfound(String lostandfound) {
		this.lostandfound = lostandfound;
	}
	/**
	 * @return the lostandfoundSummary
	 */
	public String getLostandfoundSummary() {
		return lostandfoundSummary;
	}
	/**
	 * @param lostandfoundSummary the lostandfoundSummary to set
	 */
	public void setLostandfoundSummary(String lostandfoundSummary) {
		this.lostandfoundSummary = lostandfoundSummary;
	}
	/**
	 * @return the queue coordinate
	 */
	public String getQueuecoordinate() {
		return queuecoordinate;
	}
	/**
	 * @param queuecoordinate the queue coordinate to set
	 */
	public void setQueuecoordinate(String queuecoordinate) {
		this.queuecoordinate = queuecoordinate;
	}
	/**
	 * @return the queuecoordinateSummary
	 */
	public String getQueuecoordinateSummary() {
		return queuecoordinateSummary;
	}
	/**
	 * @param queuecoordinateSummary the queuecoordinateSummary to set
	 */
	public void setQueuecoordinateSummary(String queuecoordinateSummary) {
		this.queuecoordinateSummary = queuecoordinateSummary;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getVoucherReport() {
		return voucherReport;
	}
	public void setVoucherReport(String voucherReport) {
		this.voucherReport = voucherReport;
	}
	public String getChangeVoucher() {
		return changeVoucher;
	}
	public void setChangeVoucher(String changeVoucher) {
		this.changeVoucher = changeVoucher;
	}
	public String getCompanymaster() {
		return companymaster;
	}
	public void setCompanymaster(String companymaster) {
		this.companymaster = companymaster;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public int getIsAvailable() {
		return isAvailable;
	}
	public void setIsAvailable(int isAvailable) {
		this.isAvailable = isAvailable;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getPcompany() {
		return pcompany;
	}
	public void setPcompany(String pcompany) {
		this.pcompany = pcompany;
	}
	public String getRepassword() {
		return repassword;
	}
	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getAssociateCode() {
		return associateCode;
	}
	public void setAssociateCode(String associateCode) {
		this.associateCode = associateCode;
	}
	
	public String getUsertypeDesc() {
		return usertypeDesc;
	}
	public void setUsertypeDesc(String usertypeDesc) {
		this.usertypeDesc = usertypeDesc;
	}
	public String getAcceptopenreq() {
		return acceptopenreq;
	}
	public void setAcceptopenreq(String acceptopenreq) {
		this.acceptopenreq = acceptopenreq;
	}
	public String getAcceptreqxml() {
		return acceptreqxml;
	}
	public void setAcceptreqxml(String acceptreqxml) {
		this.acceptreqxml = acceptreqxml;
	}
	public String getChangeopenreq() {
		return changeopenreq;
	}
	public void setChangeopenreq(String changeopenreq) {
		this.changeopenreq = changeopenreq;
	}
	public String getCreatevoucher() {
		return createvoucher;
	}
	public void setCreatevoucher(String createvoucher) {
		this.createvoucher = createvoucher;
	}
	public String getDriverreg() {
		return driverreg;
	}
	public void setDriverreg(String driverreg) {
		this.driverreg = driverreg;
	}
	public String getDriversummary() {
		return driversummary;
	}
	public void setDriversummary(String driversummary) {
		this.driversummary = driversummary;
	}
	public String getDriverupdate() {
		return driverupdate;
	}
	public void setDriverupdate(String driverupdate) {
		this.driverupdate = driverupdate;
	}
	public String getOpenreqentry() {
		return openreqentry;
	}
	public void setOpenreqentry(String openreqentry) {
		this.openreqentry = openreqentry;
	}
	public String getOpenreqxml() {
		return openreqxml;
	}
	public void setOpenreqxml(String openreqxml) {
		this.openreqxml = openreqxml;
	}
	public String getTripsummaryrep() {
		return tripsummaryrep;
	}
	public void setTripsummaryrep(String tripsummaryrep) {
		this.tripsummaryrep = tripsummaryrep;
	}
	public String getUserlist() {
		return userlist;
	}
	public void setUserlist(String userlist) {
		this.userlist = userlist;
	}
	public String getUserreg() {
		return userreg;
	}
	public void setUserreg(String userreg) {
		this.userreg = userreg;
	}
	public ArrayList getRoles() {
		return roles;
	}
	public void setRoles(ArrayList roles) {
		this.roles = roles;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getFname() {
		return Fname;
	}
	public void setFname(String fname) {
		Fname = fname;
	}
	public String getLname() {
		return Lname;
	}
	public void setLname(String lname) {
		Lname = lname;
	}
	public String getDrid() {
		return drid;
	}
	public void setDrid(String drid) {
		this.drid = drid;
	}
	public String getMerchid() {
		return merchid;
	}
	public void setMerchid(String merchid) {
		this.merchid = merchid;
	}
	public String getServiceid() {
		return serviceid;
	}
	public void setServiceid(String serviceid) {
		this.serviceid = serviceid;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getPhnumber() {
		return phnumber;
	}
	public void setPhnumber(String phnumber) {
		this.phnumber = phnumber;
	}
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}
	 
	public double getDefaultLati() {
		return defaultLati;
	}
	public void setDefaultLati(double defaultLati) {
		this.defaultLati = defaultLati;
	}
	public double getDefaultLogi() {
		return defaultLogi;
	}
	public void setDefaultLogi(double defaultLogi) {
		this.defaultLogi = defaultLogi;
	}
	public String getGoogRegKey() {
		return googRegKey;
	}
	public void setGoogRegKey(String googRegKey) {
		this.googRegKey = googRegKey;
	}
	public int getCallerId() {
		return callerId;
	}
	public void setCallerId(int callerId) {
		this.callerId = callerId;
	}
	public String getORTime() {
		return ORTime;
	}
	public void setORTime(String oRTime) {
		ORTime = oRTime;
	}
	public int getORFormat() {
		return ORFormat;
	}
	public void setORFormat(int oRFormat) {
		ORFormat = oRFormat;
	}
	public void setVehicleNo(String cabNo) {
		this.vehicleNo = cabNo;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setCalculateZone(boolean calculateZone) {
		this.calculateZone = calculateZone;
	}
	public boolean isCalculateZone() {
		return calculateZone;
	}
	public void setMasterAssociateCode(String masterAssociateCode) {
		this.masterAssociateCode = masterAssociateCode;
	}
	public String getMasterAssociateCode() {
		return masterAssociateCode;
	}
	public void setCheckDocuments(int checkDocuments) {
		this.checkDocuments = checkDocuments;
	}
	public int getCheckDocuments() {
		return checkDocuments;
	}
	public int getCall_onRoute() {
		return call_onRoute;
	}
	public void setCall_onRoute(int call_onRoute) {
		this.call_onRoute = call_onRoute;
	}
	public int getCall_onAccept() {
		return call_onAccept;
	}
	public void setCall_onAccept(int call_onAccept) {
		this.call_onAccept = call_onAccept;
	}
	public int getCall_onSite() {
		return call_onSite;
	}
	public void setCall_onSite(int call_onSite) {
		this.call_onSite = call_onSite;
	}
	public int getMessage_onRoute() {
		return message_onRoute;
	}
	public void setMessage_onRoute(int message_onRoute) {
		this.message_onRoute = message_onRoute;
	}
	public int getMessage_onAccept() {
		return message_onAccept;
	}
	public void setMessage_onAccept(int message_onAccept) {
		this.message_onAccept = message_onAccept;
	}
	public int getMessage_onSite() {
		return message_onSite;
	}
	public void setMessage_onSite(int message_onSite) {
		this.message_onSite = message_onSite;
	}
	public void setAddressFill(int addressFill) {
		this.addressFill = addressFill;
	}
	public int getAddressFill() {
		return addressFill;
	}
	public void setStartAmount(String startAmount) {
		this.startAmount = startAmount;
	}
	public String getStartAmount() {
		return startAmount;
	}
	public void setDistanceBasedOn(int distanceBasedOn) {
		this.distanceBasedOn = distanceBasedOn;
	}
	public int getDistanceBasedOn() {
		return distanceBasedOn;
	}
	public void setMeterMandatory(boolean meterMandatory) {
		this.meterMandatory = meterMandatory;
	}
	public boolean isMeterMandatory() {
		return meterMandatory;
	}
	public void setWindowTime(String windowTime) {
		this.windowTime = windowTime;
	}
	public String getWindowTime() {
		return windowTime;
	}
	public void setLoginLimitTime(String loginLimitTime) {
		this.loginLimitTime = loginLimitTime;
	}
	public String getLoginLimitTime() {
		return loginLimitTime;
	}
	public void setDriverRating(int driverRating) {
		this.driverRating = driverRating;
	}
	public int getDriverRating() {
		return driverRating;
	}
	public void setFareByDistance(int fareByDistance) {
		this.fareByDistance = fareByDistance;
	}
	public int getFareByDistance() {
		return fareByDistance;
	}
	public void setZoneLogoutTime(int zoneLogoutTime) {
		this.zoneLogoutTime = zoneLogoutTime;
	}
	public int getZoneLogoutTime() {
		return zoneLogoutTime;
	}
	public double getOnsiteDist() {
		return onsiteDist;
	}
	public void setOnsiteDist(double onsiteDist) {
		this.onsiteDist = onsiteDist;
	}
	public String getPhonePrefix() {
		return phonePrefix;
	}
	public void setPhonePrefix(String phonePrefix) {
		this.phonePrefix = phonePrefix;
	}
	public static HashMap<String, String> getCompanyProperties() {
		return companyProperties;
	}
	public static void setCompanyProperties(HashMap<String, String> companyProperties) {
		AdminRegistrationBO.companyProperties = companyProperties;
	}
	public String getSmsPrefix() {
		return smsPrefix;
	}
	public void setSmsPrefix(String smsPrefix) {
		this.smsPrefix = smsPrefix;
	}
	public int getDispatchBroadcast() {
		return dispatchBroadcast;
	}
	public void setDispatchBroadcast(int dispatchBroadcast) {
		this.dispatchBroadcast = dispatchBroadcast;
	}
	public String getDefaultMeterRate() {
		return defaultMeterRate;
	}
	public void setDefaultMeterRate(String defaultMeterRate) {
		this.defaultMeterRate = defaultMeterRate;
	}
	public String getPaymentUserID() {
		return paymentUserID;
	}
	public void setPaymentUserID(String paymentUserID) {
		this.paymentUserID = paymentUserID;
	}
	public boolean isMeterHidden() {
		return meterHidden;
	}
	public void setMeterHidden(boolean meterHidden) {
		this.meterHidden = meterHidden;
	}
	public int getSettingsAccess() {
		return settingsAccess;
	}
	public void setSettingsAccess(int settingsAccess) {
		this.settingsAccess = settingsAccess;
	}
	public String getPayPassword() {
		return payPassword;
	}
	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}
	public int getCallerIdType() {
		return callerIdType;
	}
	public void setCallerIdType(int callerIdType) {
		this.callerIdType = callerIdType;
	}
	public int getJob_No_Show_Property() {
		return job_No_Show_Property;
	}
	public void setJob_No_Show_Property(int job_No_Show_Property) {
		this.job_No_Show_Property = job_No_Show_Property;
	}
	public int getFleetDispatch() {
		return fleetDispatch;
	}
	public void setFleetDispatch(int fleetDispatch) {
		this.fleetDispatch = fleetDispatch;
	}
	public ArrayList<String> getCompanyList() {
		return companyList;
	}
	public void setCompanyList(ArrayList<String> companyList) {
		this.companyList = companyList;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getChangeCompanyCode() {
		return changeCompanyCode;
	}
	public void setChangeCompanyCode(String changeCompanyCode) {
		this.changeCompanyCode = changeCompanyCode;
	}
	public String getAndroidId() {
		return androidId;
	}
	public void setAndroidId(String androidId) {
		this.androidId = androidId;
	}
	public int getCheckDriverMobile() {
		return checkDriverMobile;
	}
	public void setCheckDriverMobile(int checkDriverMobile) {
		this.checkDriverMobile = checkDriverMobile;
	}
	public int getSendPushToCustomer() {
		return sendPushToCustomer;
	}
	public void setSendPushToCustomer(int sendPushToCustomer) {
		this.sendPushToCustomer = sendPushToCustomer;
	}
	public String getDefaultLanguage() {
		return DefaultLanguage;
	}
	public void setDefaultLanguage(String defaultLanguage) {
		DefaultLanguage = defaultLanguage;
	}
	public String getJson_details() {
		return json_details;
	}
	public void setJson_details(String json_details) {
		this.json_details = json_details;
	}
	public String getWasl_reference_id() {
		return wasl_reference_id;
	}
	public void setWasl_reference_id(String wasl_reference_id) {
		this.wasl_reference_id = wasl_reference_id;
	}
	public int getEnable_stadd_flag() {
		return enable_stadd_flag;
	}
	public void setEnable_stadd_flag(int enable_stadd_flag) {
		this.enable_stadd_flag = enable_stadd_flag;
	}

}
