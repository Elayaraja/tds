package com.tds.tdsBO;

public class CompanyccsetupDO {
	

	String CCC_CARD_TYPE="";
	String chk="";
	int CCC_TEMPLATE_TYPE=0;
	String CCC_CARD_ISSUER="";
	String CCC_TXN_AMT_PERCENTAGE="";
	String CCC_TXN_AMOUNT="";
	String CCC_COMPANY_ID="";
	String CCC_SWIPE_TYPE="";

	public int getCCC_TEMPLATE_TYPE() {
		return CCC_TEMPLATE_TYPE;
	}
	public void setCCC_TEMPLATE_TYPE(int cCC_TEMPLATE_TYPE) {
		CCC_TEMPLATE_TYPE = cCC_TEMPLATE_TYPE;
	}
	public String getChk() {
		return chk;
	}
	public void setChk(String chk) {
		this.chk = chk;
	}
	public String getCCC_CARD_TYPE() {
		return CCC_CARD_TYPE;
	}
	public void setCCC_CARD_TYPE(String cCCCARDTYPE) {
		CCC_CARD_TYPE = cCCCARDTYPE;
	}
	public String getCCC_CARD_ISSUER() {
		return CCC_CARD_ISSUER;
	}
	public void setCCC_CARD_ISSUER(String cCCCARDISSUER) {
		CCC_CARD_ISSUER = cCCCARDISSUER;
	}
	public String getCCC_TXN_AMT_PERCENTAGE() {
		return CCC_TXN_AMT_PERCENTAGE;
	}
	public void setCCC_TXN_AMT_PERCENTAGE(String cCCTXNAMTPERCENTAGE) {
		CCC_TXN_AMT_PERCENTAGE = cCCTXNAMTPERCENTAGE;
	}
	public String getCCC_TXN_AMOUNT() {
		return CCC_TXN_AMOUNT;
	}
	public void setCCC_TXN_AMOUNT(String cCCTXNAMOUNT) {
		CCC_TXN_AMOUNT = cCCTXNAMOUNT;
	}
	public String getCCC_COMPANY_ID() {
		return CCC_COMPANY_ID;
	}
	public void setCCC_COMPANY_ID(String cCCCOMPANYID) {
		CCC_COMPANY_ID = cCCCOMPANYID;
	}
	public String getCCC_SWIPE_TYPE() {
		return CCC_SWIPE_TYPE;
	}
	public void setCCC_SWIPE_TYPE(String cCCSWIPETYPE) {
		CCC_SWIPE_TYPE = cCCSWIPETYPE;
	}

}
