package com.tds.tdsBO;

import java.util.ArrayList;

public class CustomerMobileBO {
	public static final long serialVersionUID = 1L;
	ArrayList<Integer> driverImage = new ArrayList<Integer>();
	public String userName="";
	public String password="";
	public String userId="";
	public String phoneNumber="";
	public String emailId="";
	public String paymentType="";
	public String cardNumber="";
	public String cardExpiryDate="";
	public String voucherNumber="";
	public String voucherExpiryDate="";
	public String serialNo="";
	public String tripId="";
	public String address="";
	public String city="";
	public String name="";
	public String stLatitude="";
	public String stLongitude="";
	public String assoccode="";
	public String tagAddress="";
	public String status="";
	public String addressKey="";
	public String latitude="";
	public String longitude="";
	public String amount="";
	public String chargesTypeCode="";
	public String driver="";
	public String vehicleNo="";
	public String fileName="";
	public String vehRegNum="";
	public String vType="";
	public String vMake="";
	public String customerUniqueId="";
	public boolean hasCcAccount=false;
	public String cCAuthorizeAmount="0";
	public int ccmandatory=0;
	public int paymentMandatory=0;
	public String tripAMount = "";
	public int tripPaymentStatus=0;
	public String app_Source="";
	public String edAddress;
	public String edLatitude="";
	public String edLongitude="";
	public String serviceDate="";
	public int ETA_calculate = 0;
	public String edcity="";
	public boolean isVerified = true;
	public boolean isBlocked = true;
	
	public boolean isBlocked() {
		return isBlocked;
	}
	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	public boolean isVerified() {
		return isVerified;
	}
	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}
	public String getEdcity() {
		return edcity;
	}
	public void setEdcity(String edcity) {
		this.edcity = edcity;
	}
	public String getServiceDate() {
		return serviceDate;
	}
	public int getETA_calculate() {
		return ETA_calculate;
	}
	public void setETA_calculate(int eTA_calculate) {
		ETA_calculate = eTA_calculate;
	}
	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}
	public String getEdAddress() {
		return edAddress;
	}
	public void setEdAddress(String edAddress) {
		this.edAddress = edAddress;
	}
	public String getEdLatitude() {
		return edLatitude;
	}
	public void setEdLatitude(String edLatitude) {
		this.edLatitude = edLatitude;
	}
	public String getEdLongitude() {
		return edLongitude;
	}
	public void setEdLongitude(String edLongitude) {
		this.edLongitude = edLongitude;
	}
	public int getPaymentMandatory() {
		return paymentMandatory;
	}
	public void setPaymentMandatory(int paymentMandatory) {
		this.paymentMandatory = paymentMandatory;
	}
	
	public String getApp_Source() {
		return app_Source;
	}
	public void setApp_Source(String app_Source) {
		this.app_Source = app_Source;
	}
	public int getTripPaymentStatus() {
		return tripPaymentStatus;
	}
	public void setTripPaymentStatus(int tripPaymentStatus) {
		this.tripPaymentStatus = tripPaymentStatus;
	}
	public String getTripAMount() {
		return tripAMount;
	}
	public void setTripAMount(String tripAMount) {
		this.tripAMount = tripAMount;
	}
	public int getCcmandatory() {
		return ccmandatory;
	}
	public void setCcmandatory(int ccmandatory) {
		this.ccmandatory = ccmandatory;
	}
	public String getcCAuthorizeAmount() {
		return cCAuthorizeAmount;
	}
	public void setcCAuthorizeAmount(String cCAuthorizeAmount) {
		this.cCAuthorizeAmount = cCAuthorizeAmount;
	}
	public boolean isHasCcAccount() {
		return hasCcAccount;
	}
	public void setHasCcAccount(boolean hasCcAccount) {
		this.hasCcAccount = hasCcAccount;
	}
	public String getCustomerUniqueId() {
		return customerUniqueId;
	}
	public void setCustomerUniqueId(String customerUniqueId) {
		this.customerUniqueId = customerUniqueId;
	}
	public String getVehRegNum() {
		return vehRegNum;
	}
	public void setVehRegNum(String vehRegNum) {
		this.vehRegNum = vehRegNum;
	}
	public String getvType() {
		return vType;
	}
	public void setvType(String vType) {
		this.vType = vType;
	}
	public String getvMake() {
		return vMake;
	}
	public void setvMake(String vMake) {
		this.vMake = vMake;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getChargesTypeCode() {
		return chargesTypeCode;
	}
	public void setChargesTypeCode(String chargesTypeCode) {
		this.chargesTypeCode = chargesTypeCode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddressKey() {
		return addressKey;
	}
	public void setAddressKey(String addressKey) {
		this.addressKey = addressKey;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTagAddress() {
		return tagAddress;
	}
	public void setTagAddress(String tagAddress) {
		this.tagAddress = tagAddress;
	}

	public String getAssoccode() {
		return assoccode;
	}
	public void setAssoccode(String assoccode) {
		this.assoccode = assoccode;
	}
	public String getStLatitude() {
		return stLatitude;
	}
	public void setStLatitude(String stLatitude) {
		this.stLatitude = stLatitude;
	}
	public String getStLongitude() {
		return stLongitude;
	}
	public void setStLongitude(String stLongitude) {
		this.stLongitude = stLongitude;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getCardExpiryDate() {
		return cardExpiryDate;
	}
	public void setCardExpiryDate(String cardExpiryDate) {
		this.cardExpiryDate = cardExpiryDate;
	}
	public String getVoucherNumber() {
		return voucherNumber;
	}
	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}
	public String getVoucherExpiryDate() {
		return voucherExpiryDate;
	}
	public void setVoucherExpiryDate(String voucherExpiryDate) {
		this.voucherExpiryDate = voucherExpiryDate;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public ArrayList<Integer> getDriverImage() {
		//System.out.println("getter image-->" + driverImage);
		return driverImage;
	}
	public void setDriverImage(ArrayList<Integer> driverimage) {
		this.driverImage = driverimage;
		//System.out.println("Setter image-->" + driverimage);
	}
	public void setFileName(String fileName) {
		// TODO Auto-generated method stub
		this.fileName=fileName;
		
	}
	public String getFileName()
	{
		return this.fileName;
	}

	
}
