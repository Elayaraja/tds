//package com.tds.tdsBO;
package com.tds.tdsBO;
import java.io.Serializable;
public class ZoneCoordinatesBo implements Serializable  {
	/*	private String queueId ="";
	private String queueDescription ="";
	private double latitude =0;
	public String getQueueDescription() {
		return queueDescription;
	}

	public void setQueueDescription(String queueDescription) {
		this.queueDescription = queueDescription;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	private double longitude =0;

	public void setQueueId(String queueId) {
		this.queueId = queueId;
	}

	public String getQueueId() {
		return queueId;
	}
}
*/




//public class QueueCoordinatesBO implements Serializable {
	public static final long serialVersionUID = 1L;

	private String queueId;
	private String qAssocCode;
	/*private String eLongitude;
	private String wLongitude;
	private String nLatitude;
	private String sLatitude;*/
	private String qDelayTime;
	private String qDescription;
	private String qLoginSwitch;
	private String qDispatchType;
	private int qSequence;
	private String qTypeofQueue;
	private String qBroadcastDelay;
	private String qStartTime;
	private String qEndTime;
	private String qDayofWeek;
	private String qRequestBefore;
	private String qDispatchDistance;
	private String qOrder;
	private String smsResponseTime;
	private String zoneNumber;
	private String adjacentZones;
	private String orderNumber;
	private int versionNumber;
	private String phoneCallTime;
	private String adjacentZoneProperties;
	private int staleCallTime=0;
	private int staleCallbasedOn=0;
	private double latitude =0;
	private double longitude =0;

	public int getStaleCallbasedOn() {
		return staleCallbasedOn;
	}
	public void setStaleCallbasedOn(int staleCallbasedOn) {
		this.staleCallbasedOn = staleCallbasedOn;
	}

	public int getStaleCallTime() {
		return staleCallTime;
	}
	public void setStaleCallTime(int staleCallTime) {
		this.staleCallTime = staleCallTime;
	}

	/**
	 * @return the qDescription
	 */
	public String getQDescription() {
		return qDescription;
	}
	/**
	 * @param description the qDescription to set
	 */
	public void setQDescription(String qDescription) {
		this.qDescription = qDescription;
	}
	private boolean updateStaus;

	/**
	 * @return the updateStaus
	 */
	public boolean isUpdateStaus() {
		return updateStaus;
	}
	/**
	 * @param updateStaus the updateStaus to set
	 */
	public void setUpdateStaus(boolean updateStaus) {
		this.updateStaus = updateStaus;
	}
	/**
	 * @return the queueId
	 */
	public String getQueueId() {
		return queueId;
	}
	/**
	 * @param queueId the queueId to set
	 */
	public void setQueueId(String queueId) {
		this.queueId = queueId;
	}
	/**
	 * @return the qAssocCode
	 */
	public String getQAssocCode() {
		return qAssocCode;
	}
	/**
	 * @param assocCode the qAssocCode to set
	 */
	public void setQAssocCode(String assocCode) {
		qAssocCode = assocCode;
	}
	/**
	 * @return the eLongitude
	 */
	/*public String getELongitude() {
		return eLongitude;
	}
	/**
	 * @param longitude the eLongitude to set
	 */
	/*public void setELongitude(String longitude) {
		eLongitude = longitude;
	}
	/**
	 * @return the wLongitude
	 */
	/*public String getWLongitude() {
		return wLongitude;
	}
	/**
	 * @param longitude the wLongitude to set
	 */
	/*public void setWLongitude(String longitude) {
		wLongitude = longitude;
	}
	/**
	 * @return the nLatitude
	 */
	/*public String getNLatitude() {
		return nLatitude;
	}
	/**
	 * @param latitude the nLatitude to set
	 */
	/*public void setNLatitude(String latitude) {
		nLatitude = latitude;
	}
	/**
	 * @return the sLatitude
	 */
	/*public String getSLatitude() {
		return sLatitude;
	}
	/**
	 * @param latitude the sLatitude to set
	 */
	/*public void setSLatitude(String latitude) {
		sLatitude = latitude;
	}*/
	/**
	 * @return the qDelayTime
	 */
	public String getQDelayTime() {
		return qDelayTime;
	}
	/**
	 * @param delayTime the qDelayTime to set
	 */
	public void setQDelayTime(String delayTime) {
		qDelayTime = delayTime;
	}
	public String getQLoginSwitch() {
		return qLoginSwitch;
	}

	public void setQLoginSwitch(String loginswitch) {
		qLoginSwitch = loginswitch;
	}
	public String getQDispatchType() {
		return qDispatchType;
	}
	public void setQDispatchType(String dispatchtype) {
		qDispatchType = dispatchtype;
	}
	public String getQTypeofQueue() {
		return qTypeofQueue;
	}

	public void setQTypeofQueue(String queuetype) {
		qTypeofQueue = queuetype;
	}
	public String getQBroadcastDelay() {
		return qBroadcastDelay;
	}

	public void setQBroadcastDelay(String broadcastdelay) {
		qBroadcastDelay = broadcastdelay;
	}
	public String getQStartTime() {
		return qStartTime;
	}

	public void setQStartTime(String starttime) {
		qStartTime = starttime;
	}
	public String getQEndTime() {
		return qEndTime;
	}

	public void setQEndTime(String endtime) {
		qEndTime = endtime;
	}
	public String getQDayofWeek() {
		return qDayofWeek;
	}

	public void setQDayofWeek(String dayofweek) {
		qDayofWeek = dayofweek;
	}
	public String getQRequestBefore() {
		return qRequestBefore;
	}

	public void setQRequestBefore(String requestbefore) {
		qRequestBefore = requestbefore;
	}
	public String getQDispatchDistance() {
		return qDispatchDistance;
	}

	public void setQDispatchDistance(String dispatchdist) {
		qDispatchDistance = dispatchdist;
	}

	/**
	 * @return the smsResponseTime
	 */
	public String getSmsResponseTime() {
		return smsResponseTime;
	}
	/**
	 * @param smsResponseTime the smsResponseTime to set
	 */
	public void setSmsResponseTime(String smsResponseTime) {
		this.smsResponseTime = smsResponseTime;
	}

	public String getZoneNumber() {
		return zoneNumber;
	}
	public void setZoneNumber(String zoneNumber) {
		this.zoneNumber = zoneNumber;
	}
	public String getAdjacentZones() {
		return adjacentZones;
	}
	public void setAdjacentZones(String adjacentZones) {
		this.adjacentZones = adjacentZones;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}
	public int getVersionNumber() {
		return versionNumber;
	}
	public String getPhoneCallTime() {
		return phoneCallTime;
	}
	public void setPhoneCallTime(String phoneCallTime) {
		this.phoneCallTime = phoneCallTime;
	}
	public String getAdjacentZoneProperties() {
		return adjacentZoneProperties;
	}
	public void setAdjacentZoneProperties(String adjacentZoneProperties) {
		this.adjacentZoneProperties = adjacentZoneProperties;
	}
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getqSequence() {
		return qSequence;
	}
	public void setqSequence(int qSequence) {
		this.qSequence = qSequence;
	}


}
