package com.tds.tdsBO;
import java.io.Serializable;

public class FleetBO implements Serializable {
	public static final long serialVersionUID = 1L;

	private String fleetNumber;
	private String FleetName;
	private String MasterAssoccode;
	private String userId;
	private String fcolor;
	

	public String getFcolor() {
		return fcolor;
	}
	public void setFcolor(String fcolor) {
		this.fcolor = fcolor;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFleetNumber() {
		return fleetNumber;
	}
	public void setFleetNumber(String fleetNumber) {
		this.fleetNumber = fleetNumber;
	}
	public String getFleetName() {
		return FleetName;
	}
	public void setFleetName(String fleetName) {
		FleetName = fleetName;
	}
	public String getMasterAssoccode() {
		return MasterAssoccode;
	}
	public void setMasterAssoccode(String masterAssoccode) {
		MasterAssoccode = masterAssoccode;
	}



}
