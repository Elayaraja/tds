package com.tds.tdsBO;


public class OpenRequestFieldOrderBO {
	private static final long serialVersionUID = 1L;

	private String tripid = "";
	private String phone = "";
	private String sadd1 = "";
	private String sadd2 = "";
	private String scity = "";
	private String eadd1 = "";
	private String eadd2 = "";
	private String ecity = "";
	private String shrs = "";
	private String sdate = "";
	private String specialIns="";
	private String paytype="";
	private String acct="";
	private String amt;
	private String slong;
	private String slat;
	private String edlatitude = "";
	private String edlongitude = "";
	private String associateCode = "";
	private String name = "";
	private String sharedRide = "";
	private String numberOfPassengers="";
	private int dontUpload=0; 
	private String dispatchComments="";
	private String operatorComments="";
	private String separatedBy="";
	private String callerPhone="";
	private String refNumber="";
	private String refNumber1="";
	private String refNumber2="";
	private String refNumber3="";
	private String pickUpOrder="";
	private String dropOffOrder="";
	private String customField="";

	public String getPickUpOrder() {
		return pickUpOrder;
	}
	public void setPickUpOrder(String pickUpOrder) {
		this.pickUpOrder = pickUpOrder;
	}
	public String getDropOffOrder() {
		return dropOffOrder;
	}
	public void setDropOffOrder(String dropOffOrder) {
		this.dropOffOrder = dropOffOrder;
	}
	public String getCallerPhone() {
		return callerPhone;
	}
	public void setCallerPhone(String callerPhone) {
		this.callerPhone = callerPhone;
	}
	public String getRefNumber() {
		return refNumber;
	}
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	public String getRefNumber1() {
		return refNumber1;
	}
	public void setRefNumber1(String refNumber1) {
		this.refNumber1 = refNumber1;
	}
	public String getRefNumber2() {
		return refNumber2;
	}
	public void setRefNumber2(String refNumber2) {
		this.refNumber2 = refNumber2;
	}
	public String getRefNumber3() {
		return refNumber3;
	}
	public void setRefNumber3(String refNumber3) {
		this.refNumber3 = refNumber3;
	}
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	private String routeNumber="";
	private String dateFormat="";
	
	public String getRouteNumber() {
		return routeNumber;
	}
	public void setRouteNumber(String routeNumber) {
		this.routeNumber = routeNumber;
	}
	public String getSeparatedBy() {
		return separatedBy;
	}
	public void setSeparatedBy(String separatedBy) {
		this.separatedBy = separatedBy;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	private String  vendor="";
	private String vendorName="";




	public String getDispatchComments() {
		return dispatchComments;
	}
	public void setDispatchComments(String dispatchComments) {
		this.dispatchComments = dispatchComments;
	}
	public String getOperatorComments() {
		return operatorComments;
	}
	public void setOperatorComments(String operatorComments) {
		this.operatorComments = operatorComments;
	}
	public int getDontUpload() {
		return dontUpload;
	}
	public void setDontUpload(int dontUpload) {
		this.dontUpload = dontUpload;
	}
	public String getNumberOfPassengers() {
		return numberOfPassengers;
	}
	public void setNumberOfPassengers(String numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}
	public String getSharedRide() {
		return sharedRide;
	}
	public void setSharedRide(String sharedRide) {
		this.sharedRide = sharedRide;
	}

	public String getSpecialIns() {
		return specialIns;
	}
	public void setSpecialIns(String specialIns) {
		this.specialIns = specialIns;
	}



	public String getEadd1() {
		return eadd1;
	}
	public void setEadd1(String eadd1) {
		this.eadd1 = eadd1;
	}
	public String getEadd2() {
		return eadd2;
	}
	public void setEadd2(String eadd2) {
		this.eadd2 = eadd2;
	}
	public String getEcity() {
		return ecity;
	}
	public void setEcity(String ecity) {
		this.ecity = ecity;
	}

	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSadd1() {
		return sadd1;
	}
	public void setSadd1(String sadd1) {
		this.sadd1 = sadd1;
	}
	public String getSadd2() {
		return sadd2;
	}
	public void setSadd2(String sadd2) {
		this.sadd2 = sadd2;
	}
	public String getScity() {
		return scity;
	}
	public void setScity(String scity) {
		this.scity = scity;
	}
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}

	public String getTripid() {
		return tripid;
	}
	public void setTripid(String tripid) {
		this.tripid = tripid;
	}
	public String getShrs() {
		return shrs;
	}
	public void setShrs(String shrs) {
		this.shrs = shrs;
	}

	public String getEdlatitude() {
		return edlatitude;
	}
	public void setEdlatitude(String edlatitude) {
		this.edlatitude = edlatitude;
	}
	public String getEdlongitude() {
		return edlongitude;
	}
	public void setEdlongitude(String edlongitude) {
		this.edlongitude = edlongitude;
	}

	public String getAssociateCode() {
		return associateCode;
	}
	public void setAssociateCode(String associateCode) {
		this.associateCode = associateCode;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getPaytype() {
		return paytype;
	}
	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String i) {
		this.amt = i;
	}
	public String getSlong() {
		return slong;
	}
	public void setSlong(String slong) {
		this.slong = slong;
	}
	public String getSlat() {
		return slat;
	}
	public void setSlat(String slat) {
		this.slat = slat;
	}
	public String getCustomField() {
		return customField;
	}
	public void setCustomField(String customField) {
		this.customField = customField;
	}



}
