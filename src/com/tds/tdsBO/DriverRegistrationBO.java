package com.tds.tdsBO;

import java.io.Serializable;
import java.util.ArrayList;

public class DriverRegistrationBO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String sno = "";
	private String uid = "";
	private String password = "";
	private String fname = "";
	private String driExten = "";
	private String lname = "";
	private String add1 = "";
	private String add2 = "";
	private String city = "";
	private String state = "";
	private String zip = "";
	private String route = "";
	private String acct = "";
	private String phone = "";
	private String phoneExt = "";
	private String cmake = "";
	private String cyear = "";
	private String cmodel = "";
	private String tempExt = "";
	private String fdeposit = "";
	
	private String drLicense="";
	private String hkLicense="";
	private String drLicenseExpiry="";
	private String hkLicenseExpiry="";
	private String socialSecurityNo="";
	private int template;
	private int driverRatings=1;
	private int settingsAccess=0;
	
	private String emailId = "";
	private String wasl_reference_No = "";
	private String dr_dob = "";
	private String dr_details_json = "";
	
	public int getDriverRatings() {
		return driverRatings;
	}
	public void setDriverRatings(int driverRatings) {
		this.driverRatings = driverRatings;
	}
	public int getTemplate() {
		return template;
	}
	public void setTemplate(int template) {
		this.template = template;
	}
	public String getSocialSecurityNo() {
		return socialSecurityNo;
	}
	public void setSocialSecurityNo(String socialSecurityNo) {
		this.socialSecurityNo = socialSecurityNo;
	}
	public String getDrLicense() {
		return drLicense;
	}
	public void setDrLicense(String drLicense) {
		this.drLicense = drLicense;
	}
	public String getHkLicense() {
		return hkLicense;
	}
	public void setHkLicense(String hkLicense) {
		this.hkLicense = hkLicense;
	}
	public String getDrLicenseExpiry() {
		return drLicenseExpiry;
	}
	public void setDrLicenseExpiry(String drLicenseExpiry) {
		this.drLicenseExpiry = drLicenseExpiry;
	}
	public String getHkLicenseExpiry() {
		return hkLicenseExpiry;
	}
	public void setHkLicenseExpiry(String hkLicenseExpiry) {
		this.hkLicenseExpiry = hkLicenseExpiry;
	}

	public String getDrProfile() {
		return drProfile;
	}
	public void setDrProfile(String drProfile) {
		this.drProfile = drProfile;
	}
	public ArrayList getAl_list() {
		return al_list;
	}
	public void setAl_list(ArrayList alList) {
		al_list = alList;
	}
	private String status = "";
	private String start = "";
	private String end = "";
	private String associateCode = "";
	private int isDataAvailable = 0; 
	private String ehostid = "messaging.sprintpcs.com";
	private String repassword = "";
	private String cabno = "";
	private String passno = "";
	private char registration;
	
	private String drProfile="";
	private ArrayList al_list = new ArrayList();
	
	//for merchant
	
	private String payUserId="";
	private String payPassword="";
	private String payRePassword="";
	private String payMerchant="";
	
	
	
	
	
	
	public String getPayRePassword() {
		return payRePassword;
	}
	public void setPayRePassword(String payRePassword) {
		this.payRePassword = payRePassword;
	}
	public String getPayUserId() {
		return payUserId;
	}
	public void setPayUserId(String payUserId) {
		this.payUserId = payUserId;
	}
	public String getPayPassword() {
		return payPassword;
	}
	public void setPayPassword(String payPassword) {
		this.payPassword = payPassword;
	}
	public String getPayMerchant() {
		return payMerchant;
	}
	public void setPayMerchant(String payMerchant) {
		this.payMerchant = payMerchant;
	}
	public char getRegistration() {
		return registration;
	}
	public void setRegistration(char registration) {
		this.registration = registration;
	}
	/**
	 * @return the ehostid
	 */
	public String getEhostid() {
		return ehostid;
	}
	/**
	 * @param ehostid the ehostid to set
	 */
	public void setEhostid(String ehostid) {
		this.ehostid = ehostid;
	}
	public int getIsDataAvailable() {
		return isDataAvailable;
	}
	public void setIsDataAvailable(int isDataAvailable) {
		this.isDataAvailable = isDataAvailable;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public String getAdd1() {
		return add1;
	}
	public void setAdd1(String add1) {
		this.add1 = add1;
	}
	public String getAdd2() {
		return add2;
	}
	public void setAdd2(String add2) {
		this.add2 = add2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCmake() {
		return cmake;
	}
	public void setCmake(String cmake) {
		this.cmake = cmake;
	}
	public String getCmodel() {
		return cmodel;
	}
	public void setCmodel(String cmodel) {
		this.cmodel = cmodel;
	}
	public String getCyear() {
		return cyear;
	}
	public void setCyear(String cyear) {
		this.cyear = cyear;
	}
	public String getFdeposit() {
		return fdeposit;
	}
	public void setFdeposit(String fdeposit) {
		this.fdeposit = fdeposit;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPhoneExt() {
		return phoneExt;
	}
	public void setPhoneExt(String phoneExt) {
		this.phoneExt = phoneExt;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTempExt() {
		return tempExt;
	}
	public void setTempExt(String tempExt) {
		this.tempExt = tempExt;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getSno() {
		return sno;
	}
	public void setSno(String sno) {
		this.sno = sno;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getAssociateCode() {
		return associateCode;
	}
	public void setAssociateCode(String associateCode) {
		this.associateCode = associateCode;
	}
	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}
	public String getRepassword() {
		return repassword;
	}
	public String getCabno() {
		return cabno;
	}
	public void setCabno(String cabno) {
		this.cabno = cabno;
	}
	public String getPassno() {
		return passno;
	}
	public void setPassno(String passno) {
		this.passno = passno;
	}
	public String getDriExten() {
		return driExten;
	}
	public void setDriExten(String driExten) {
		this.driExten = driExten;
	}
	public int getSettingsAccess() {
		return settingsAccess;
	}
	public void setSettingsAccess(int settingsAccess) {
		this.settingsAccess = settingsAccess;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getWasl_reference_No() {
		return wasl_reference_No;
	}
	public void setWasl_reference_No(String wasl_reference_No) {
		this.wasl_reference_No = wasl_reference_No;
	}
	public String getDr_dob() {
		return dr_dob;
	}
	public void setDr_dob(String dr_dob) {
		this.dr_dob = dr_dob;
	}
	public String getDr_details_json() {
		return dr_details_json;
	}
	public void setDr_details_json(String dr_details_json) {
		this.dr_details_json = dr_details_json;
	}
	
}
