package com.tds.tdsBO;

public class PaymentSettledSummary {
	private String id ="";
	private String voucherNo = "";
	private String riderName = "";
	private String costCenter = "";
	private String associationCode = "";
	private String amount = "";
	private String expDate ="";
	private String description ="";
	private String creationDate = "";
	private String user ="";
	private String tripId = "";
	private String companyCode ="";
	private String contact ="";
	private String delayDays ="";
	private String freq ="";
	private String serviceDate ="";
	private String transId ="";
	private String driverId = "";
	private String riderSign ="";
	private String retAmount ="";
	private String tip ="";
	private String total ="";
	private String transactionPercentAmount = "";
	private String trtansactionAmount ="";
	private String totalAmountWithTransaction ="";
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVoucherNo() {
		return voucherNo;
	}
	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}
	public String getRiderName() {
		return riderName;
	}
	public void setRiderName(String riderName) {
		this.riderName = riderName;
	}
	public String getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}
	public String getAssociationCode() {
		return associationCode;
	}
	public void setAssociationCode(String associationCode) {
		this.associationCode = associationCode;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getTripId() {
		return tripId;
	}
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getDelayDays() {
		return delayDays;
	}
	public void setDelayDays(String delayDays) {
		this.delayDays = delayDays;
	}
	public String getFreq() {
		return freq;
	}
	public void setFreq(String freq) {
		this.freq = freq;
	}
	public String getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(String serviceDate) {
		this.serviceDate = serviceDate;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getRiderSign() {
		return riderSign;
	}
	public void setRiderSign(String riderSign) {
		this.riderSign = riderSign;
	}
	public String getRetAmount() {
		return retAmount;
	}
	public void setRetAmount(String retAmount) {
		this.retAmount = retAmount;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTransactionPercentAmount() {
		return transactionPercentAmount;
	}
	public void setTransactionPercentAmount(String transactionPercentAmount) {
		this.transactionPercentAmount = transactionPercentAmount;
	}
	public String getTrtansactionAmount() {
		return trtansactionAmount;
	}
	public void setTrtansactionAmount(String trtansactionAmount) {
		this.trtansactionAmount = trtansactionAmount;
	}
	public String getTotalAmountWithTransaction() {
		return totalAmountWithTransaction;
	}
	public void setTotalAmountWithTransaction(String totalAmountWithTransaction) {
		this.totalAmountWithTransaction = totalAmountWithTransaction;
	}
	
}
