package com.tds.tdsBO;

public class ZoneRateBean {
	
	private String assocode="";
	private String zoneid;
	private String startzone="";
	private String endzone="";
	private String amount="";
	public String getAssocode() {
		return assocode;
	}
	public void setAssocode(String assocode) {
		this.assocode = assocode;
	}
	public String getStartzone() {
		return startzone;
	}
	public void setStartzone(String startzone) {
		this.startzone = startzone;
	}
	public String getEndzone() {
		return endzone;
	}
	public void setEndzone(String endzone) {
		this.endzone = endzone;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getZoneid() {
		return zoneid;
	}
	public void setZoneid(String zoneid) {
		this.zoneid = zoneid;
	}
	
}