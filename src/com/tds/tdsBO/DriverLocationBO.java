package com.tds.tdsBO;

import java.io.Serializable;

public class DriverLocationBO implements Serializable {
	private static final long serialVersionUID = 1;
	
	private String driverid = "";
	private String drName  = "";
	private String latitude = "0";
	private String longitude = "0";
	private String status = "";
	private String updatedTime = "";
	private String assocCode = "";
	private String phone="";
	private String smsid="";
	private String uid="";
	private String provider = "";
	private String profile = "";
	private String pushKey = "";
	private String vehicleNo = "";
	private int getJobsFromAdjacentZones =0;
	private int counter=0;
	private int lastUpdatedMilliSeconds=0;
	private String age="";
	private String appVersion ="";
	private String lastLoginTime ="";
	private int driverActivateStatus=0;
	private int loginTime = 0;
	private int driverRatings=1;
	private String distance="";
	
	private String json_details = "";
	private boolean in_job = false;
	
	public int getDriverRatings() {
		return driverRatings;
	}
	public void setDriverRatings(int driverRatings) {
		this.driverRatings = driverRatings;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSmsid() {
		return smsid;
	}
	public void setSmsid(String smsid) {
		this.smsid = smsid;
	}
	public String getAssocCode() {
		return assocCode;
	}
	public void setAssocCode(String assocCode) {
		this.assocCode = assocCode;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}
	public String getPushKey() {
		return pushKey;
	}
	public void setPushKey(String pushKey) {
		this.pushKey = pushKey;
	}
	public int getGetJobsFromAdjacentZones() {
		return getJobsFromAdjacentZones;
	}
	public void setGetJobsFromAdjacentZones(int getJobsFromAdjacentZones) {
		this.getJobsFromAdjacentZones = getJobsFromAdjacentZones;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public int getCounter() {
		return counter;
	}
	public void setLastUpdatedMilliSeconds(int lastUpdatedMilliSeconds) {
		this.lastUpdatedMilliSeconds = lastUpdatedMilliSeconds;
	}
	public int getLastUpdatedMilliSeconds() {
		return lastUpdatedMilliSeconds;
	}
	public void setDrName(String drName) {
		this.drName = drName;
	}
	public String getDrName() {
		return drName;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getAge() {
		return age;
	}
	public void setLoginTime(int loginTime) {
		this.loginTime = loginTime;
	}
	public int getLoginTime() {
		return loginTime;
	}
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}
	public String getAppVersion() {
		return appVersion;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public int getDriverActivateStatus() {
		return driverActivateStatus;
	}
	public void setDriverActivateStatus(int driverActivateStatus) {
		this.driverActivateStatus = driverActivateStatus;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getJson_details() {
		return json_details;
	}
	public void setJson_details(String json_details) {
		this.json_details = json_details;
	}
	public boolean isIn_job() {
		return in_job;
	}
	public void setIn_job(boolean in_job) {
		this.in_job = in_job;
	}

}
