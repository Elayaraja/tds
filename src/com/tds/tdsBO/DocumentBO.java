package com.tds.tdsBO;
import java.io.Serializable;

public class DocumentBO implements Serializable {
	public static final long serialVersionUID = 1L;

	private String documentType;
	private String serverityType;
	private String ShortName;
	private String serialNumber;
	private String documentBelongsTo;
	private int checkForExpiry;
	
	public int getCheckForExpiry() {
		return checkForExpiry;
	}
	public void setCheckForExpiry(int checkForExpiry) {
		this.checkForExpiry = checkForExpiry;
	}
	
	public String getDocumentBelongsTo() {
		return documentBelongsTo;
	}
	public void setDocumentBelongsTo(String documentBelongsTo) {
		this.documentBelongsTo = documentBelongsTo;
	}

	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getServerityType() {
		return serverityType;
	}
	public void setServerityType(String serverityType) {
		this.serverityType = serverityType;
	}
	public String getShortName() {
		return ShortName;
	}
	public void setShortName(String shortName) {
		ShortName = shortName;
	}




	
}
