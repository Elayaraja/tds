package com.tds.tdsBO;

import java.io.Serializable;

public class DispatchPropertiesBO implements Serializable {
	private static final long serialVersionUID = 1;
	
	private int sAddress1_onOffer=0;
	private int sAddress2_onOffer=0;
	private int name_onOffer=0;
	private int eAddress1_onOffer=0;
	private int eAddress2_onOffer=0;
	private int phoneNumber_onOffer=0;
	private int sCity_onOffer=0;
	private int eCity_onOffer=0;
	private int paymentType_onOffer=0;
	private int fareAmt_onOffer=0;
	private int zone_onOffer=0;
	private int acceptanceTimeOnOffer = 0;
	private int jobAttributeOnOffer = 0;

	private int sAddress1_onAccept=0;
	private int sAddress2_onAccept=0;
	private int name_onAccept=0;
	private int eAddress1_onAccept=0;
	private int eAddress2_onAccept=0;
	private int phoneNumber_onAccept=0;
	private int sCity_onAccept=0;
	private int eCity_onAccept=0;
	private int paymentType_onAccept=0;
	private int fareAmt_onAccept=0;
	private int acceptanceTimeOnAccept = 0;
	private int jobAttributeOnAccept=0;

	private int zone_onAccept=0;
	private int paymentType_onSite=0;
	private int sAddress1_onSite=0;
	private int sAddress2_onSite=0;
	private int name_onSite=0;
	private int eAddress1_onSite=0;
	private int eAddress2_onSite=0;
	private int phoneNumber_onSite=0;
	private int sCity_onSite=0;
	private int eCity_onSite=0;
	private int zone_onSite=0;
	private int endZone_onAccept=0;
	private int endZone_onOffer=0;
	private int endZone_onSite=0;
	private int acceptanceTimeOnOnSite = 0;
	private int jobAttributeOnSite=0;

	
	private int call_onRoute;
	private int call_onAccept;
	private int call_onSite;
	private int message_onRoute;
	private int message_onAccept;
	private int message_onSite;

	private String messageAccept="";
	private String messageRoute="";
	private String messageSite="";
	
	public String getMessageAccept() {
		return messageAccept;
	}
	public void setMessageAccept(String messageAccept) {
		this.messageAccept = messageAccept;
	}
	public String getMessageRoute() {
		return messageRoute;
	}
	public void setMessageRoute(String messageRoute) {
		this.messageRoute = messageRoute;
	}
	public String getMessageSite() {
		return messageSite;
	}
	public void setMessageSite(String messageSite) {
		this.messageSite = messageSite;
	}
	public int getEndZone_onSite() {
		return endZone_onSite;
	}
	public void setEndZone_onSite(int endZone_onSite) {
		this.endZone_onSite = endZone_onSite;
	}
	public int getEndZone_onAccept() {
		return endZone_onAccept;
	}
	public void setEndZone_onAccept(int endZone_onAccept) {
		this.endZone_onAccept = endZone_onAccept;
	}
	public int getEndZone_onOffer() {
		return endZone_onOffer;
	}
	public void setEndZone_onOffer(int endZone_onOffer) {
		this.endZone_onOffer = endZone_onOffer;
	}

	public int getPaymentType_onSite() {
		return paymentType_onSite;
	}
	public void setPaymentType_onSite(int paymentType_onSite) {
		this.paymentType_onSite = paymentType_onSite;
	}
	public int getsAddress1_onSite() {
		return sAddress1_onSite;
	}
	public void setsAddress1_onSite(int sAddress1_onSite) {
		this.sAddress1_onSite = sAddress1_onSite;
	}
	public int getsAddress2_onSite() {
		return sAddress2_onSite;
	}
	public void setsAddress2_onSite(int sAddress2_onSite) {
		this.sAddress2_onSite = sAddress2_onSite;
	}
	public int getName_onSite() {
		return name_onSite;
	}
	public void setName_onSite(int name_onSite) {
		this.name_onSite = name_onSite;
	}
	public int geteAddress1_onSite() {
		return eAddress1_onSite;
	}
	public void seteAddress1_onSite(int eAddress1_onSite) {
		this.eAddress1_onSite = eAddress1_onSite;
	}
	public int geteAddress2_onSite() {
		return eAddress2_onSite;
	}
	public void seteAddress2_onSite(int eAddress2_onSite) {
		this.eAddress2_onSite = eAddress2_onSite;
	}
	public int getPhoneNumber_onSite() {
		return phoneNumber_onSite;
	}
	public void setPhoneNumber_onSite(int phoneNumber_onSite) {
		this.phoneNumber_onSite = phoneNumber_onSite;
	}
	public int getsCity_onSite() {
		return sCity_onSite;
	}
	public void setsCity_onSite(int sCity_onSite) {
		this.sCity_onSite = sCity_onSite;
	}
	public int geteCity_onSite() {
		return eCity_onSite;
	}
	public void seteCity_onSite(int eCity_onSite) {
		this.eCity_onSite = eCity_onSite;
	}
	public int getZone_onSite() {
		return zone_onSite;
	}
	public void setZone_onSite(int zone_onSite) {
		this.zone_onSite = zone_onSite;
	}
	

	public int getZone_onOffer() {
		return zone_onOffer;
	}
	public void setZone_onOffer(int zone_onOffer) {
		this.zone_onOffer = zone_onOffer;
	}
	public int getZone_onAccept() {
		return zone_onAccept;
	}
	public void setZone_onAccept(int zone_onAccept) {
		this.zone_onAccept = zone_onAccept;
	}
	public int getsAddress1_onOffer() {
		return sAddress1_onOffer;
	}
	public void setsAddress1_onOffer(int sAddress1_onOffer) {
		this.sAddress1_onOffer = sAddress1_onOffer;
	}
	public int getsAddress2_onOffer() {
		return sAddress2_onOffer;
	}
	public void setsAddress2_onOffer(int sAddress2_onOffer) {
		this.sAddress2_onOffer = sAddress2_onOffer;
	}
	public int getName_onOffer() {
		return name_onOffer;
	}
	public void setName_onOffer(int name_onOffer) {
		this.name_onOffer = name_onOffer;
	}
	public int geteAddress1_onOffer() {
		return eAddress1_onOffer;
	}
	public void seteAddress1_onOffer(int eAddress1_onOffer) {
		this.eAddress1_onOffer = eAddress1_onOffer;
	}
	public int geteAddress2_onOffer() {
		return eAddress2_onOffer;
	}
	public void seteAddress2_onOffer(int eAddress2_onOffer) {
		this.eAddress2_onOffer = eAddress2_onOffer;
	}
	public int getPhoneNumber_onOffer() {
		return phoneNumber_onOffer;
	}
	public void setPhoneNumber_onOffer(int phoneNumber_onOffer) {
		this.phoneNumber_onOffer = phoneNumber_onOffer;
	}
	public int getsCity_onOffer() {
		return sCity_onOffer;
	}
	public void setsCity_onOffer(int sCity_onOffer) {
		this.sCity_onOffer = sCity_onOffer;
	}
	public int geteCity_onOffer() {
		return eCity_onOffer;
	}
	public void seteCity_onOffer(int eCity_onOffer) {
		this.eCity_onOffer = eCity_onOffer;
	}
	public int getPaymentType_onOffer() {
		return paymentType_onOffer;
	}
	public void setPaymentType_onOffer(int paymentType_onOffer) {
		this.paymentType_onOffer = paymentType_onOffer;
	}
	public int getsAddress1_onAccept() {
		return sAddress1_onAccept;
	}
	public void setsAddress1_onAccept(int sAddress1_onAccept) {
		this.sAddress1_onAccept = sAddress1_onAccept;
	}
	public int getsAddress2_onAccept() {
		return sAddress2_onAccept;
	}
	public void setsAddress2_onAccept(int sAddress2_onAccept) {
		this.sAddress2_onAccept = sAddress2_onAccept;
	}
	public int getName_onAccept() {
		return name_onAccept;
	}
	public void setName_onAccept(int name_onAccept) {
		this.name_onAccept = name_onAccept;
	}
	public int geteAddress1_onAccept() {
		return eAddress1_onAccept;
	}
	public void seteAddress1_onAccept(int eAddress1_onAccept) {
		this.eAddress1_onAccept = eAddress1_onAccept;
	}
	public int geteAddress2_onAccept() {
		return eAddress2_onAccept;
	}
	public void seteAddress2_onAccept(int eAddress2_onAccept) {
		this.eAddress2_onAccept = eAddress2_onAccept;
	}
	public int getPhoneNumber_onAccept() {
		return phoneNumber_onAccept;
	}
	public void setPhoneNumber_onAccept(int phoneNumber_onAccept) {
		this.phoneNumber_onAccept = phoneNumber_onAccept;
	}
	public int getsCity_onAccept() {
		return sCity_onAccept;
	}
	public void setsCity_onAccept(int sCity_onAccept) {
		this.sCity_onAccept = sCity_onAccept;
	}
	public int geteCity_onAccept() {
		return eCity_onAccept;
	}
	public void seteCity_onAccept(int eCity_onAccept) {
		this.eCity_onAccept = eCity_onAccept;
	}
	public int getPaymentType_onAccept() {
		return paymentType_onAccept;
	}
	public void setPaymentType_onAccept(int paymentType_onAccept) {
		this.paymentType_onAccept = paymentType_onAccept;
	}
	public int getCall_onRoute() {
		return call_onRoute;
	}
	public void setCall_onRoute(int call_onRoute) {
		this.call_onRoute = call_onRoute;
	}
	public int getCall_onAccept() {
		return call_onAccept;
	}
	public void setCall_onAccept(int call_onAccept) {
		this.call_onAccept = call_onAccept;
	}
	public int getCall_onSite() {
		return call_onSite;
	}
	public void setCall_onSite(int call_onSite) {
		this.call_onSite = call_onSite;
	}
	public int getMessage_onRoute() {
		return message_onRoute;
	}
	public void setMessage_onRoute(int message_onRoute) {
		this.message_onRoute = message_onRoute;
	}
	public int getMessage_onAccept() {
		return message_onAccept;
	}
	public void setMessage_onAccept(int message_onAccept) {
		this.message_onAccept = message_onAccept;
	}
	public int getMessage_onSite() {
		return message_onSite;
	}
	public void setMessage_onSite(int message_onSite) {
		this.message_onSite = message_onSite;
	}
	public void setAcceptanceTimeOnAccept(int acceptanceTimeOnAccept) {
		this.acceptanceTimeOnAccept = acceptanceTimeOnAccept;
	}
	public int getAcceptanceTimeOnAccept() {
		return acceptanceTimeOnAccept;
	}
	public void setAcceptanceTimeOnOffer(int acceptanceTimeOnOffer) {
		this.acceptanceTimeOnOffer = acceptanceTimeOnOffer;
	}
	public int getAcceptanceTimeOnOffer() {
		return acceptanceTimeOnOffer;
	}
	public void setAcceptanceTimeOnOnSite(int acceptanceTimeOnOnSite) {
		this.acceptanceTimeOnOnSite = acceptanceTimeOnOnSite;
	}
	public int getAcceptanceTimeOnOnSite() {
		return acceptanceTimeOnOnSite;
	}
	public void setJobAttributeOnAccept(int jobAttributeOnAccept) {
		this.jobAttributeOnAccept = jobAttributeOnAccept;
	}
	public int getJobAttributeOnAccept() {
		return jobAttributeOnAccept;
	}
	public void setJobAttributeOnOffer(int jobAttributeOnOffer) {
		this.jobAttributeOnOffer = jobAttributeOnOffer;
	}
	public int getJobAttributeOnOffer() {
		return jobAttributeOnOffer;
	}
	public void setJobAttributeOnSite(int jobAttributeOnSite) {
		this.jobAttributeOnSite = jobAttributeOnSite;
	}
	public int getJobAttributeOnSite() {
		return jobAttributeOnSite;
	}
	private int fareAmt_onSite=0;
	public int getFareAmt_onOffer() {
		return fareAmt_onOffer;
	}
	public void setFareAmt_onOffer(int fareAmt_onOffer) {
		this.fareAmt_onOffer = fareAmt_onOffer;
	}
	public int getFareAmt_onAccept() {
		return fareAmt_onAccept;
	}
	public void setFareAmt_onAccept(int fareAmt_onAccept) {
		this.fareAmt_onAccept = fareAmt_onAccept;
	}
	public int getFareAmt_onSite() {
		return fareAmt_onSite;
	}
	public void setFareAmt_onSite(int fareAmt_onSite) {
		this.fareAmt_onSite = fareAmt_onSite;
	}

	
	}
