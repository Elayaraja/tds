package com.tds.tdsBO;

import java.io.Serializable;

public class VoucherBO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String vno = "";
	private String vname = "";
	private String vstatus = "";
	private String vcostcenter = "";
	private String vassoccode = "";
	private String vamount = "";
	private String vexpdate = "";
	private String vdesc = "";
	private String vtripid = "";
	private String from_date = "";
	
	private String companyName="";
	private String companyAddress="";
	private String billingAddress="";
	private String contactName="";
	private String contactTitle="";
	private String telephoneNumber="";
	private String faxNumber="";
	private String emailAddress="";
	private int emailStatus=0;
	private int noTip;
	private int activeOrInactive=0;
	private String startAmount="";
	private String ratePerMile="";
	private String driverComments="";
	private String operatorComments="";
	private boolean dontExpire=false;
	private boolean onlyForSR=false;
	private String allocateOnlyTo="";
	private String notAllocateTo="";

	private String ccNum ="";
	private String name="";
	private String cvv="";
	private String expiryDate="";
	private int declineStatus=0;
	private String zip="";
	private int processMethod=0;
	private String infokey;
	private int[] inVoiceNo; 
	private int ccProcessPercentage=0;
	private int cc_Percent_processed =0;
	private String cc_Prcnt_Amt="";
	
	public int getCc_Percent_processed() {
		return cc_Percent_processed;
	}
	public void setCc_Percent_processed(int cc_Percent_processed) {
		this.cc_Percent_processed = cc_Percent_processed;
	}
	public String getCc_Prcnt_Amt() {
		return cc_Prcnt_Amt;
	}
	public void setCc_Prcnt_Amt(String cc_Prcnt_Amt) {
		this.cc_Prcnt_Amt = cc_Prcnt_Amt;
	}
	
	public String getCcNum() {
		return ccNum;
	}
	public void setCcNum(String ccNum) {
		this.ccNum = ccNum;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCvv() {
		return cvv;
	}
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public int getDeclineStatus() {
		return declineStatus;
	}
	public void setDeclineStatus(int declineStatus) {
		this.declineStatus = declineStatus;
	}
	private String specialFlags="";
	
	public String getDriverComments() {
		return driverComments;
	}
	public void setDriverComments(String driverComments) {
		this.driverComments = driverComments;
	}
	public String getOperatorComments() {
		return operatorComments;
	}
	public void setOperatorComments(String operatorComments) {
		this.operatorComments = operatorComments;
	}
	public String getStartAmount() {
		return startAmount;
	}
	public void setStartAmount(String startAmount) {
		this.startAmount = startAmount;
	}
	public String getRatePerMile() {
		return ratePerMile;
	}
	public void setRatePerMile(String ratePerMile) {
		this.ratePerMile = ratePerMile;
	}
	public int getActiveOrInactive() {
		return activeOrInactive;
	}
	public void setActiveOrInactive(int activeOrInactive) {
		this.activeOrInactive = activeOrInactive;
	}
	public String getVdriver() {
		return vdriver;
	}
	public void setVdriver(String vdriver) {
		this.vdriver = vdriver;
	}
	private String to_date = "";
	private String vuser;
	private String vcode="";
	private String vcontact="";
	private String vdelay="";
	private String vfreq="";
	private String vreason;
	private boolean v_status;
	private String vtrans;
	private String vdriver;
	private String tip;
	private String voucherFormat;
	private String fixedAmount="";

	
	public String getFixedAmount() {
		return fixedAmount;
	}
	public void setFixedAmount(String fixedAmount) {
		this.fixedAmount = fixedAmount;
	}
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	public String getVtrans() {
		return vtrans;
	}
	public void setVtrans(String vtrans) {
		this.vtrans = vtrans;
	}
	public boolean isV_status() {
		return v_status;
	}
	public void setV_status(boolean vStatus) {
		v_status = vStatus;
	}
	public String getVreason() {
		return vreason;
	}
	public void setVreason(String vreason) {
		this.vreason = vreason;
	}
	public String getVfreq() {
		return vfreq;
	}
	public void setVfreq(String vfreq) {
		this.vfreq = vfreq;
	}
	public String getVcode() {
		return vcode;
	}
	public void setVcode(String vcode) {
		this.vcode = vcode;
	}
	public String getVcontact() {
		return vcontact;
	}
	public void setVcontact(String vcontact) {
		this.vcontact = vcontact;
	}
	public String getVdelay() {
		return vdelay;
	}
	public void setVdelay(String vdelay) {
		this.vdelay = vdelay;
	}
	public String getVuser() {
		return vuser;
	}
	public void setVuser(String vuser) {
		this.vuser = vuser;
	}
	public String getFrom_date() {
		return from_date;
	}
	public void setFrom_date(String fromDate) {
		from_date = fromDate;
	}
	public String getTo_date() {
		return to_date;
	}
	public void setTo_date(String toDate) {
		to_date = toDate;
	}
	
	public String getVtripid() {
		return vtripid;
	}
	public void setVtripid(String vtripid) {
		this.vtripid = vtripid;
	}
	public String getVamount() {
		return vamount;
	}
	public void setVamount(String vamount) {
		this.vamount = vamount;
	}
	public String getVexpdate() {
		return vexpdate;
	}
	public void setVexpdate(String vexpdate) {
		this.vexpdate = vexpdate;
	}
	public String getVdesc() {
		return vdesc;
	}
	public void setVdesc(String vdesc) {
		this.vdesc = vdesc;
	}
	public String getVassoccode() {
		return vassoccode;
	}
	public void setVassoccode(String vassoccode) {
		this.vassoccode = vassoccode;
	}
	public String getVcostcenter() {
		return vcostcenter;
	}
	public void setVcostcenter(String vcostcenter) {
		this.vcostcenter = vcostcenter;
	}
	public String getVname() {
		return vname;
	}
	public void setVname(String vname) {
		this.vname = vname;
	}
	public String getVno() {
		return vno;
	}
	public void setVno(String vno) {
		this.vno = vno;
	}
	public String getVstatus() {
		return vstatus;
	}
	public void setVstatus(String vstatus) {
		this.vstatus = vstatus;
	}
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyAddress() {
		return companyAddress;
	}
	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}
	public String getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactTitle() {
		return contactTitle;
	}
	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public String getFaxNumber() {
		return faxNumber;
	}
	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public void setVoucherFormat(String voucherFormat) {
		this.voucherFormat = voucherFormat;
	}
	public String getVoucherFormat() {
		return voucherFormat;
	}
	public void setNoTip(int noTip) {
		this.noTip = noTip;
	}
	public int getNoTip() {
		return noTip;
	}
	public void setDontExpire(boolean dontExpire) {
		this.dontExpire = dontExpire;
	}
	public boolean isDontExpire() {
		return dontExpire;
	}
	public void setOnlyForSR(boolean onlyForSR) {
		this.onlyForSR = onlyForSR;
	}
	public boolean isOnlyForSR() {
		return onlyForSR;
	}
	public String getAllocateOnlyTo() {
		return allocateOnlyTo;
	}
	public void setAllocateOnlyTo(String allocateOnlyTo) {
		this.allocateOnlyTo = allocateOnlyTo;
	}
	public String getNotAllocateTo() {
		return notAllocateTo;
	}
	public void setNotAllocateTo(String notAllocateTo) {
		this.notAllocateTo = notAllocateTo;
	}
	public String getSpecialFlags() {
		return specialFlags;
	}
	public void setSpecialFlags(String specialFlags) {
		this.specialFlags = specialFlags;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public int getProcessMethod() {
		return processMethod;
	}
	public void setProcessMethod(int processMethod) {
		this.processMethod = processMethod;
	}
	public String getInfokey() {
		return infokey;
	}
	public void setInfokey(String infokey) {
		this.infokey = infokey;
	}
	public int[] getInVoiceNo() {
		return inVoiceNo;
	}
	public void setInVoiceNo(int[] inVoiceNo) {
		this.inVoiceNo = inVoiceNo;
	}
	public int getEmailStatus() {
		return emailStatus;
	}
	public void setEmailStatus(int emailStatus) {
		this.emailStatus = emailStatus;
	}
	public int getCcProcessPercentage() {
		return ccProcessPercentage;
	}
	public void setCcProcessPercentage(int ccProcessPercentage) {
		this.ccProcessPercentage = ccProcessPercentage;
	}
	
}
