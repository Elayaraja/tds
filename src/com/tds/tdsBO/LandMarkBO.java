package com.tds.tdsBO;

import java.math.BigDecimal;
import java.util.ArrayList;

public class LandMarkBO {
	private static final long serialVersionUID = 1L;
	private int serialNo = 0;

	public int getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
}
