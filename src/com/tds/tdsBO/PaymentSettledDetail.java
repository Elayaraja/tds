package com.tds.tdsBO;

public class PaymentSettledDetail {
		private String id ="";
		private String voucherNo = "";
		private String riderName = "";
		private String costCenter = "";
		private String associationCode = "";
		private String amount = "";
		private String expDate ="";
		private String description ="";
		private String creationDate = "";
		private String user ="";
		private String tripId = "";
		private String companyCode ="";
		private String contact ="";
		private String delayDays ="";
		private String freq ="";
		private String serviceDate ="";
		private String transId ="";
		private String driverId = "";
		private String riderSign ="";
		private String retAmount ="";
		private String tip ="";
		private String total ="";
		private String transactionPercentAmount = "";
		private String trtansactionAmount ="";
		private String totalAmountWithTransaction ="";
		private int invoiceNumber = 0;
		private String processDate = "";
		private String paymentNo = "";
		private String chequeNumber = "";
		private String paymentRecievedDate = "";
		private String totalcount = "0";
		private int paymentReceivedStatus=0;
		private int verified =0;
		private int driverPayedStatus =0;
		private String accountName ="";
		private String voucherFormat ="";
		private String emailAddress="";
		private String cc_Percentage = "";
		private int cc_Percent_processed =0;
		private String cc_Prcnt_Amt="";
		
		public int getInvoiceNumber() {
			return invoiceNumber;
		}
		public void setInvoiceNumber(int invoiceNumber) {
			this.invoiceNumber = invoiceNumber;
		}
		public String getProcessDate() {
			return processDate;
		}
		public void setProcessDate(String processDate) {
			this.processDate = processDate;
		}
		public String getPaymentNo() {
			return paymentNo;
		}
		public void setPaymentNo(String paymentNo) {
			this.paymentNo = paymentNo;
		}
		public String getChequeNumber() {
			return chequeNumber;
		}
		public void setChequeNumber(String chequeNumber) {
			this.chequeNumber = chequeNumber;
		}
		public String getPaymentRecievedDate() {
			return paymentRecievedDate;
		}
		public void setPaymentRecievedDate(String paymentRecievedDate) {
			this.paymentRecievedDate = paymentRecievedDate;
		}
		public int getTotalNoOfVoucher() {
			return totalNoOfVoucher;
		}
		public void setTotalNoOfVoucher(int totalNoOfVoucher) {
			this.totalNoOfVoucher = totalNoOfVoucher;
		}
		public double getTotalAmount() {
			return totalAmount;
		}
		public void setTotalAmount(double totalAmount) {
			this.totalAmount = totalAmount;
		}
		private int totalNoOfVoucher = 0;
		private double totalAmount =0;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getVoucherNo() {
			return voucherNo;
		}
		public void setVoucherNo(String voucherNo) {
			this.voucherNo = voucherNo;
		}
		public String getRiderName() {
			return riderName;
		}
		public void setRiderName(String riderName) {
			this.riderName = riderName;
		}
		public String getCostCenter() {
			return costCenter;
		}
		public void setCostCenter(String costCenter) {
			this.costCenter = costCenter;
		}
		public String getAssociationCode() {
			return associationCode;
		}
		public void setAssociationCode(String associationCode) {
			this.associationCode = associationCode;
		}
		public String getAmount() {
			return amount;
		}
		public void setAmount(String amount) {
			this.amount = amount;
		}
		public String getExpDate() {
			return expDate;
		}
		public void setExpDate(String expDate) {
			this.expDate = expDate;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getCreationDate() {
			return creationDate;
		}
		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}
		public String getUser() {
			return user;
		}
		public void setUser(String user) {
			this.user = user;
		}
		public String getTripId() {
			return tripId;
		}
		public void setTripId(String tripId) {
			this.tripId = tripId;
		}
		public String getCompanyCode() {
			return companyCode;
		}
		public void setCompanyCode(String companyCode) {
			this.companyCode = companyCode;
		}
		public String getContact() {
			return contact;
		}
		public void setContact(String contact) {
			this.contact = contact;
		}
		public String getDelayDays() {
			return delayDays;
		}
		public void setDelayDays(String delayDays) {
			this.delayDays = delayDays;
		}
		public String getFreq() {
			return freq;
		}
		public void setFreq(String freq) {
			this.freq = freq;
		}
		public String getServiceDate() {
			return serviceDate;
		}
		public void setServiceDate(String serviceDate) {
			this.serviceDate = serviceDate;
		}
		public String getTransId() {
			return transId;
		}
		public void setTransId(String transId) {
			this.transId = transId;
		}
		public String getDriverId() {
			return driverId;
		}
		public void setDriverId(String driverId) {
			this.driverId = driverId;
		}
		public String getRiderSign() {
			return riderSign;
		}
		public void setRiderSign(String riderSign) {
			this.riderSign = riderSign;
		}
		public String getRetAmount() {
			return retAmount;
		}
		public void setRetAmount(String retAmount) {
			this.retAmount = retAmount;
		}
		public String getTip() {
			return tip;
		}
		public void setTip(String tip) {
			this.tip = tip;
		}
		public String getTotal() {
			return total;
		}
		public void setTotal(String total) {
			this.total = total;
		}
		public String getTransactionPercentAmount() {
			return transactionPercentAmount;
		}
		public void setTransactionPercentAmount(String transactionPercentAmount) {
			this.transactionPercentAmount = transactionPercentAmount;
		}
		public String getTrtansactionAmount() {
			return trtansactionAmount;
		}
		public void setTrtansactionAmount(String trtansactionAmount) {
			this.trtansactionAmount = trtansactionAmount;
		}
		public String getTotalAmountWithTransaction() {
			return totalAmountWithTransaction;
		}
		public void setTotalAmountWithTransaction(String totalAmountWithTransaction) {
			this.totalAmountWithTransaction = totalAmountWithTransaction;
		}
		public void setTotalcount(String totalcount) {
			this.totalcount = totalcount;
		}
		public String getTotalcount() {
			return totalcount;
		}
		public void setPaymentReceivedStatus(int paymentReceivedStatus) {
			this.paymentReceivedStatus = paymentReceivedStatus;
		}
		public int getPaymentReceivedStatus() {
			return paymentReceivedStatus;
		}
		public void setVerified(int verified) {
			this.verified = verified;
		}
		public int getVerified() {
			return verified;
		}
		public void setDriverPayedStatus(int driverPayedStatus) {
			this.driverPayedStatus = driverPayedStatus;
		}
		public int getDriverPayedStatus() {
			return driverPayedStatus;
		}
		public void setAccountName(String accountName) {
			this.accountName = accountName;
		}
		public String getAccountName() {
			return accountName;
		}
		public void setVoucherFormat(String voucherFormat) {
			this.voucherFormat = voucherFormat;
		}
		public String getVoucherFormat() {
			return voucherFormat;
		}
		public String getEmailAddress() {
			return emailAddress;
		}
		public void setEmailAddress(String emailAddress) {
			this.emailAddress = emailAddress;
		}
		public String getCc_Percentage() {
			return cc_Percentage;
		}
		public void setCc_Percentage(String cc_Percentage) {
			this.cc_Percentage = cc_Percentage;
		}
		public int getCc_Percent_processed() {
			return cc_Percent_processed;
		}
		public void setCc_Percent_processed(int cc_Percent_processed) {
			this.cc_Percent_processed = cc_Percent_processed;
		}
		public String getCc_Prcnt_Amt() {
			return cc_Prcnt_Amt;
		}
		public void setCc_Prcnt_Amt(String cc_Prcnt_Amt) {
			this.cc_Prcnt_Amt = cc_Prcnt_Amt;
		}
		
	}
