package com.tds.tdsBO;

import java.io.Serializable;

public class DriverBehaviorBO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String driverId;
	private String driverName;
	private String assocCode;
	private String comment;
	private String amount;
	private String behaviorDate;
	private String behaviorType;
	private String behaviorKey;
	/**
	 * @return the driverName
	 */
	public String getDriverName() {
		return driverName;
	}
	/**
	 * @param driverName the driverName to set
	 */
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	/**
	 * @return the assocCode
	 */
	public String getAssocCode() {
		return assocCode;
	}
	/**
	 * @param assocCode the assocCode to set
	 */
	public void setAssocCode(String assocCode) {
		this.assocCode = assocCode;
	}
	
	
	public String getDriverId() {
		return driverId;
	}
	public String getBehaviorType() {
		return behaviorType;
	}
	public void setBehaviorType(String behaviorType) {
		this.behaviorType = behaviorType;
	}
	public String getBehaviorKey() {
		return behaviorKey;
	}
	public void setBehaviorKey(String behaviorKey) {
		this.behaviorKey = behaviorKey;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getBehaviorDate() {
		return behaviorDate;
	}
	public void setBehaviorDate(String behaviorDate) {
		this.behaviorDate = behaviorDate;
	}
	
	
}
