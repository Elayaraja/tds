package com.tds.tdsBO;

public class ApplicationPoolBO {
	
	private String _primaryServiceEndpoint;
	private String _secondaryServiceEndpoint;
	private String _primaryTxnEndpoint;
	private String _secondaryTxnEndpoint;
	private String _IdentityToken;
	private String _PTLSSocketId;
	//private String _ServiceId;
	
	
	private String SMTP_HOST_NAME;
	private String SMTP_AUTH_USER;
	private String SMTP_AUTH_PWD;
	private String SMTP_PORT;
	private String emailFromAddress;
	private String protocol;
	private String did;
	
	public String getDid() {
		return did;
	}
	public void setDid(String did) {
		this.did = did;
	}
	private String _ApplicationProfileId;
	//private String _MerchantProfileId;
	
	private Long _SessionTokenDateTime;//Temp
	private String _SessionToken;//Temp
	private boolean status;//Temp
	
	public String getSMTP_HOST_NAME() {
		return SMTP_HOST_NAME;
	}
	public void setSMTP_HOST_NAME(String sMTPHOSTNAME) {
		SMTP_HOST_NAME = sMTPHOSTNAME;
	}
	public String getSMTP_AUTH_USER() {
		return SMTP_AUTH_USER;
	}
	public void setSMTP_AUTH_USER(String sMTPAUTHUSER) {
		SMTP_AUTH_USER = sMTPAUTHUSER;
	}
	public String getSMTP_AUTH_PWD() {
		return SMTP_AUTH_PWD;
	}
	public void setSMTP_AUTH_PWD(String sMTPAUTHPWD) {
		SMTP_AUTH_PWD = sMTPAUTHPWD;
	}
	public String getEmailFromAddress() {
		return emailFromAddress;
	}
	public void setEmailFromAddress(String emailFromAddress) {
		this.emailFromAddress = emailFromAddress;
	}
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String get_SessionToken() {
		return _SessionToken;
	}
	public void set_SessionToken(String sessionToken) {
		_SessionToken = sessionToken;
	}
	 
	public Long get_SessionTokenDateTime() {
		return _SessionTokenDateTime;
	}
	public void set_SessionTokenDateTime(Long sessionTokenDateTime) {
		_SessionTokenDateTime = sessionTokenDateTime;
	}
	 
	public String get_primaryServiceEndpoint() {
		return _primaryServiceEndpoint;
	}
	public void set_primaryServiceEndpoint(String primaryServiceEndpoint) {
		_primaryServiceEndpoint = primaryServiceEndpoint;
	}
	public String get_secondaryServiceEndpoint() {
		return _secondaryServiceEndpoint;
	}
	public void set_secondaryServiceEndpoint(String secondaryServiceEndpoint) {
		_secondaryServiceEndpoint = secondaryServiceEndpoint;
	}
	public String get_primaryTxnEndpoint() {
		return _primaryTxnEndpoint;
	}
	public void set_primaryTxnEndpoint(String primaryTxnEndpoint) {
		_primaryTxnEndpoint = primaryTxnEndpoint;
	}
	public String get_secondaryTxnEndpoint() {
		return _secondaryTxnEndpoint;
	}
	public void set_secondaryTxnEndpoint(String secondaryTxnEndpoint) {
		_secondaryTxnEndpoint = secondaryTxnEndpoint;
	}
	public String get_IdentityToken() {
		return _IdentityToken;
	}
	public void set_IdentityToken(String identityToken) {
		_IdentityToken = identityToken;
	}
	public String get_PTLSSocketId() {
		return _PTLSSocketId;
	}
	public void set_PTLSSocketId(String ptlsSocketId) {
		_PTLSSocketId = ptlsSocketId;
	}
	/*public String get_ServiceId() {
		return _ServiceId;
	}
	public void set_ServiceId(String serviceId) {
		_ServiceId = serviceId;
	}*/
	public String get_ApplicationProfileId() {
		return _ApplicationProfileId;
	}
	public void set_ApplicationProfileId(String applicationProfileId) {
		_ApplicationProfileId = applicationProfileId;
	}
	/*public String get_MerchantProfileId() {
		return _MerchantProfileId;
	}
	public void set_MerchantProfileId(String merchantProfileId) {
		_MerchantProfileId = merchantProfileId;
	}*/
	public String getSMTP_PORT() {
		return SMTP_PORT;
	}
	public void setSMTP_PORT(String smtp_port) {
		SMTP_PORT = smtp_port;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	
	
}
