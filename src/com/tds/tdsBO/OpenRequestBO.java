package com.tds.tdsBO;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.charges.bean.DriverChargesBean;

public class OpenRequestBO {
	private static final long serialVersionUID = 1L;

	private String tripid = "";
	private String phone = "";
	private String startTimeStamp ="";
	private String sadd1 = "";
	private String sadd2 = "";
	private String scity = "";
	private String sstate = "";
	private String szip = "";
	private String eadd1 = "";
	private String eadd2 = "";
	private String ecity = "";
	private String estate = "";
	private String ezip = "";
	private String shrs = "";
	private String smin = "";
	private String sdate = "";
	//private String sstatus = "";
	private int QC_DELAYTIME= 0;
	private int timeToStartTripInSeconds= 0;
	private String elandmark ="";
	private String keyvalue="";
	private String addressVerified="";
	private int dontDispatch=0; 
	private String zoneNumber="";
	private String historyOfDrivers = "";
	private String masterAddressKey = "";
	private String comments;
	private String typeOfRide;
	private String advanceTime;
	private String passengerPhoneServiceProvider;
	private String tripStatus;
	private String days;
	private int serialNo=0;
	private int tripSource;
	private int SMS_TIME_DIFF= 0;
	private int broadCastDelay= 0;
	private String createdBy="";
	private boolean dontMarkRequestsAsPublic = false;
	private long multiJobTag=0;
	private String repeatGroup = "";
	private double dispatchDistance = 0;
	private int dispatchIterationNumber = 0;
	private String pendingTime ="";
	private String dispatchMethod = "";
	private String sintersection="";
	private String slandmark="";
	private String specialIns="";
	private String timeZone="";
	private String drProfile="";
	private String vecProfile="";
	private String dispatchStatus="";
	private String paytype="";
	private String acct="";
	private String acctName="";
	private BigDecimal amt;
	private String slong;
	private String slat;
	private String landMarkKey = "";
	private String userAddressKey = "";
	private final String passgerKey = "";
	private String edlatitude = "";
	private String edlongitude = "";
	private String driverid = "";
	private String driverUName = "";
	private String associateCode = "";
	private String requestTime = "";
	private String acceptedTime = "";
	private String inprogressTime = "";
	private String completedTime = "";
	private String name = "";
	private String queueno = "";
	private String status_msg;
	private String landMarkZone = "";
	private String vehicleNo="";
	private String userZone="";
	private String status="";
	private String sttime = "";
	private String edtime = "";
	private String setTotalJobsPerDriver="";
	private ArrayList al_drList = new ArrayList();
	private ArrayList al_vecList = new ArrayList();
	private String roundTrip="";
	private int numOfPassengers=1;
	private String dropTime;
	private String routeNumber="";
	private String routeDesc="";
	private int phoneCallDelayTime=0;
	private int adjacentZoneOrderType = 1;
	private int premiumCustomer = 0;
	private String pickupOrder = "";
	private String sequenceNumber = "";
	private String createdTime = "";
	private String total = "";
	private String createdDate = "";
	private String shRideStatus ="";
	private String sharedLogReason ="";
	private String email="";
	private int numOfCabs=1;
	private int paymentStatus;
	private int jobRating=1;
	private String callerPhone="";
	private String callerName="";
	private String refNumber="";
	private String refNumber1="";
	private String refNumber2="";
	private String refNumber3="";
	private String customField="";
	private int pickupOrderSwitch =0;
	private String dropOffOrder ="";
	private int staleCallTime = 0;
	private int StaleCallTimeForZone=0;
	private int dispatchLeadTime=0;
	private int sharedRidePaymentType = 0;
	private int paymentMeter=0;
	private int minSpeed=0;
	private double ratePerMin=0.00;
	private double ratePerMile=0.00;
	private double startAmt=0.00;
	private String airName="";
	private String airNo="";
	private String airFrom="";
	private String airTo="";
	private String materPayment="";
	private boolean driverLogStatus=false;
	/*	private String zip="";
	private String saircode = "";
	private String eaircode = "";
	private String saircodeid = "";
	private String stlatitude2 = "";
	private String stlongitude2= "";
	private String edlatitude2 = "";
	private String edlongitude2 = "";
	private String stlatitude3 = "";
	private String stlongitude3 = "";
	private String edlatitude3 = "";
	private String edlongitude3 = "";
	private String stlatitude4 = "";
	private String stlongitude4 = "";
	private String edlatitude4= "";
	private String edlongitude4 = "";
	private String stlatitude = "0";
	private String stlongitude = "0";
	private String add1="";
	private String add2="";
	private String state="";
	private String city="";
	private String Latitude="";
	private String Longitude="";
	private String eaircodeid = "";

	 */

	
	public int getMinSpeed() {
		return minSpeed;
	}
	public void setMinSpeed(int minSpeed) {
		this.minSpeed = minSpeed;
	}
	public double getRatePerMin() {
		return ratePerMin;
	}
	public void setRatePerMin(double ratePerMin) {
		this.ratePerMin = ratePerMin;
	}
	public double getRatePerMile() {
		return ratePerMile;
	}
	public void setRatePerMile(double ratePerMile) {
		this.ratePerMile = ratePerMile;
	}
	public double getStartAmt() {
		return startAmt;
	}
	public void setStartAmt(double startAmt) {
		this.startAmt = startAmt;
	}
	public int getDispatchLeadTime() {
		return dispatchLeadTime;
	}
	public void setDispatchLeadTime(int dispatchLeadTime) {
		this.dispatchLeadTime = dispatchLeadTime;
	}
	private String endQueueno="";
	private String landmarkKeyName="";
	
	
	public String getLandmarkKeyName() {
		return landmarkKeyName;
	}
	public void setLandmarkKeyName(String landmarkKeyName) {
		this.landmarkKeyName = landmarkKeyName;
	}
	public int getStaleCallTimeForZone() {
		return StaleCallTimeForZone;
	}
	public void setStaleCallTimeForZone(int staleCallTimeForZone) {
		StaleCallTimeForZone = staleCallTimeForZone;
	}
	private ArrayList<DriverChargesBean> al_drivercharges; 
	private double total_amount;
	private String programKey;


	public ArrayList<DriverChargesBean> getAl_drivercharges() {
		return al_drivercharges;
	}
	public void setAl_drivercharges(ArrayList<DriverChargesBean> al_drivercharges) {
		this.al_drivercharges = al_drivercharges;
	}
	public double getTotal_amount() {
		return total_amount;
	}
	public void setTotal_amount(double total_amount) {
		this.total_amount = total_amount;
	}
	public String getProgramKey() {
		return programKey;
	}
	public void setProgramKey(String programKey) {
		this.programKey = programKey;
	}
	public String getEndQueueno() {
		return endQueueno;
	}
	public void setEndQueueno(String endQueueno) {
		this.endQueueno = endQueueno;
	}

	public String getCallerPhone() {
		return callerPhone;
	}
	public void setCallerPhone(String callerPhone) {
		this.callerPhone = callerPhone;
	}
	public String getRefNumber() {
		return refNumber;
	}
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}
	public String getRefNumber1() {
		return refNumber1;
	}
	public void setRefNumber1(String refNumber1) {
		this.refNumber1 = refNumber1;
	}
	public String getRefNumber2() {
		return refNumber2;
	}
	public void setRefNumber2(String refNumber2) {
		this.refNumber2 = refNumber2;
	}
	public String getRefNumber3() {
		return refNumber3;
	}
	public void setRefNumber3(String refNumber3) {
		this.refNumber3 = refNumber3;
	}
	public int getTripSource() {
		return tripSource;
	}
	public void setTripSource(int tripSoruce) {
		this.tripSource = tripSoruce;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getTypeOfRide() {
		return typeOfRide;
	}
	public void setTypeOfRide(String typeOfRide) {
		this.typeOfRide = typeOfRide;
	}
	public String getMasterAddressKey() {
		return masterAddressKey;
	}
	public void setMasterAddressKey(String masterAddressKey) {
		this.masterAddressKey = masterAddressKey;
	}
	public String getKeyvalue() {
		return keyvalue;
	}
	public void setKeyvalue(String keyvalue) {
		this.keyvalue = keyvalue;
	}
	public String getAddressVerified() {
		return addressVerified;
	}
	public void setAddressVerified(String addressVerified) {
		this.addressVerified = addressVerified;
	}
	public int getDontDispatch() {
		return dontDispatch;
	}
	public void setDontDispatch(int dontDispatch) {
		this.dontDispatch = dontDispatch;
	}
	public String getZoneNumber() {
		return zoneNumber;
	}
	public void setZoneNumber(String zoneNumber) {
		this.zoneNumber = zoneNumber;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getPassgerKey() {
		return passgerKey;
	}
	public String getUserZone() {
		return userZone;
	}
	public void setUserZone(String userZone) {
		this.userZone = userZone;
	}
	public String getDispatchStatus() {
		return dispatchStatus;
	}
	public void setDispatchStatus(String dispatchStatus) {
		this.dispatchStatus = dispatchStatus;
	}
	public int getSMS_TIME_DIFF() {
		return SMS_TIME_DIFF;
	}
	public void setSMS_TIME_DIFF(int sMSTIMEDIFF) {
		SMS_TIME_DIFF = sMSTIMEDIFF;
	}
	public int getBroadCastDelay() {
		return broadCastDelay;
	}
	public void setBroadCastDelay(int broadCastDelay) {
		this.broadCastDelay = broadCastDelay;
	}
	private String OR_SMS_SENT="";
	private int  QC_SMS_RES_TIME= 0;

	public String getOR_SMS_SENT() {
		return OR_SMS_SENT;
	}
	public void setOR_SMS_SENT(String oRSMSSENT) {
		OR_SMS_SENT = oRSMSSENT;
	}
	public int getQC_SMS_RES_TIME() {
		return QC_SMS_RES_TIME;
	}
	public void setQC_SMS_RES_TIME(int qCSMSRESTIME) {
		QC_SMS_RES_TIME = qCSMSRESTIME;
	}
	public String getDrProfile() {
		return drProfile;
	}
	public int getQC_DELAYTIME() {
		return QC_DELAYTIME;
	}
	public void setQC_DELAYTIME(int qCDELAYTIME) {
		QC_DELAYTIME = qCDELAYTIME;
	}
	public int getTimeToStartTripInSeconds() {
		return timeToStartTripInSeconds;
	}
	public void setTimeToStartTripInSeconds(int timeToStartTripInSeconds) {
		this.timeToStartTripInSeconds = timeToStartTripInSeconds;
	}
	public void setDrProfile(String drProfile) {
		this.drProfile = drProfile;
	}
	public String getVecProfile() {
		return vecProfile;
	}
	public void setVecProfile(String vecProfile) {
		this.vecProfile = vecProfile;
	}
	public ArrayList getAl_drList() {
		return al_drList;
	}
	public void setAl_drList(ArrayList alDrList) {
		al_drList = alDrList;
	}
	public ArrayList getAl_vecList() {
		return al_vecList;
	}
	public void setAl_vecList(ArrayList alVecList) {
		al_vecList = alVecList;
	}
	public String getSpecialIns() {
		return specialIns;
	}
	public void setSpecialIns(String specialIns) {
		this.specialIns = specialIns;
	}
	public String getSintersection() {
		return sintersection;
	}
	public void setSintersection(String sintersection) {
		this.sintersection = sintersection;
	}
	public String getSlandmark() {
		return slandmark;
	}
	public void setSlandmark(String slandmark) {
		this.slandmark = slandmark;
	}
	private int chckTripStatus=0;

	public int getChckTripStatus() {
		return chckTripStatus;
	}
	public void setChckTripStatus(int chckTripStatus) {
		this.chckTripStatus = chckTripStatus;
	}
	/**
	 * @return the dispatchMethod
	 */
	public String getDispatchMethod() {
		return dispatchMethod;
	}
	/**
	 * @param dispatchMethod the dispatchMethod to set
	 */
	public void setDispatchMethod(String dispatchMethod) {
		this.dispatchMethod = dispatchMethod;
	}
	/**
	 * @return the saircodeid
	 */
	public String getStatus_msg() {
		return status_msg;
	}
	public void setStatus_msg(String statusMsg) {
		status_msg = statusMsg;
	}
	private int isBeandata = 0;
	/**
	 * @return the driverUName
	 */
	public String getDriverUName() {
		return driverUName;
	}
	/**
	 * @param driverUName the driverUName to set
	 */
	public void setDriverUName(String driverUName) {
		this.driverUName = driverUName;
	}
	public String getEadd1() {
		return eadd1;
	}
	public void setEadd1(String eadd1) {
		this.eadd1 = eadd1;
	}
	public String getEadd2() {
		return eadd2;
	}
	public void setEadd2(String eadd2) {
		this.eadd2 = eadd2;
	}
	public String getEcity() {
		return ecity;
	}
	public void setEcity(String ecity) {
		this.ecity = ecity;
	}
	public String getEstate() {
		return estate;
	}
	public void setEstate(String estate) {
		this.estate = estate;
	}
	public String getEzip() {
		return ezip;
	}
	public void setEzip(String ezip) {
		this.ezip = ezip;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getSadd1() {
		return sadd1;
	}
	public void setSadd1(String sadd1) {
		this.sadd1 = sadd1;
	}
	public String getSadd2() {
		return sadd2;
	}
	public void setSadd2(String sadd2) {
		this.sadd2 = sadd2;
	}
	public String getScity() {
		return scity;
	}
	public void setScity(String scity) {
		this.scity = scity;
	}
	public String getSdate() {
		return sdate;
	}
	public void setSdate(String sdate) {
		this.sdate = sdate;
	}
	public String getSstate() {
		return sstate;
	}
	public void setSstate(String sstate) {
		this.sstate = sstate;
	}
//	public String getSstatus() {
//		return sstatus;
//	}
//	public void setSstatus(String sstatus) {
//		this.sstatus = sstatus;
//	}
	public String getSzip() {
		return szip;
	}
	public void setSzip(String szip) {
		this.szip = szip;
	}
	public String getTripid() {
		return tripid;
	}
	public void setTripid(String tripid) {
		this.tripid = tripid;
	}
	public String getShrs() {
		return shrs;
	}
	public void setShrs(String shrs) {
		this.shrs = shrs;
	}
	public String getSmin() {
		return smin;
	}
	public void setSmin(String smin) {
		this.smin = smin;
	}
	public int getIsBeandata() {
		return isBeandata;
	}
	public void setIsBeandata(int isBeandata) {
		this.isBeandata = isBeandata;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getEdlatitude() {
		return edlatitude;
	}
	public void setEdlatitude(String edlatitude) {
		this.edlatitude = edlatitude;
	}
	public String getEdlongitude() {
		return edlongitude;
	}
	public void setEdlongitude(String edlongitude) {
		this.edlongitude = edlongitude;
	}
	public String getSttime() {
		return sttime;
	}
	public void setSttime(String sttime) {
		this.sttime = sttime;
	}
	public String getEdtime() {
		return edtime;
	}
	public void setEdtime(String edtime) {
		this.edtime = edtime;
	}
	public String getAssociateCode() {
		return associateCode;
	}
	public void setAssociateCode(String associateCode) {
		this.associateCode = associateCode;
	}
	public String getAcceptedTime() {
		return acceptedTime;
	}
	public void setAcceptedTime(String acceptedTime) {
		this.acceptedTime = acceptedTime;
	}
	public String getCompletedTime() {
		return completedTime;
	}
	public void setCompletedTime(String completedTime) {
		this.completedTime = completedTime;
	}
	public String getInprogressTime() {
		return inprogressTime;
	}
	public void setInprogressTime(String inprogressTime) {
		this.inprogressTime = inprogressTime;
	}
	public String getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(String requestTime) {
		this.requestTime = requestTime;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQueueno() {
		return queueno;
	}
	public void setQueueno(String queueno) {
		this.queueno = queueno;
	}
	public String getPaytype() {
		return paytype;
	}
	public void setPaytype(String paytype) {
		this.paytype = paytype;
	}
	public String getAcct() {
		return acct;
	}
	public void setAcct(String acct) {
		this.acct = acct;
	}
	public BigDecimal getAmt() {
		return amt;
	}
	public void setAmt(BigDecimal amt) {
		this.amt = amt;
	}
	public String getSlong() {
		return slong;
	}
	public void setSlong(String slong) {
		this.slong = slong;
	}
	public String getSlat() {
		return slat;
	}
	public void setSlat(String slat) {
		this.slat = slat;
	}
	public double getDispatchDistance() {
		return dispatchDistance;
	}
	public void setDispatchDistance(double dispatchDistance) {
		this.dispatchDistance = dispatchDistance;
	}
	public int getDispatchIterationNumber() {
		return dispatchIterationNumber;
	}
	public void setDispatchIterationNumber(int disIterationNum) {
		dispatchIterationNumber = disIterationNum;
	}
	public String getLandMarkKey() {
		return landMarkKey;
	}
	public void setLandMarkKey(String landMarkKey) {
		this.landMarkKey = landMarkKey;
	}
	public String getUserAddressKey() {
		return userAddressKey;
	}
	public void setUserAddressKey(String userAddressKey) {
		this.userAddressKey = userAddressKey;
	}
	public String getLandMarkZone() {
		return landMarkZone;
	}
	public void setLandMarkZone(String landMarkZone) {
		this.landMarkZone = landMarkZone;
	}
	public String getHistoryOfDrivers() {
		return historyOfDrivers;
	}
	public void setHistoryOfDrivers(String historyOfDrivers) {
		this.historyOfDrivers = historyOfDrivers;
	}
	public String getAdvanceTime() {
		return advanceTime;
	}
	public void setAdvanceTime(String advanceTime) {
		this.advanceTime = advanceTime;
	}
	public String getPassengerPhoneServiceProvider() {
		return passengerPhoneServiceProvider;
	}
	public void setPassengerPhoneServiceProvider(
			String passengerPhoneServiceProvider) {
		this.passengerPhoneServiceProvider = passengerPhoneServiceProvider;
	}
	public String getTripStatus() {
		return tripStatus;
	}
	public void setTripStatus(String tripStatus) {
		this.tripStatus = tripStatus;
	}
	public String getDays() {
		return days;
	}
	public void setDays(String days) {
		this.days = days;
	}
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getStartTimeStamp() {
		return startTimeStamp;
	}
	public void setStartTimeStamp(String startTimeStamp) {
		this.startTimeStamp = startTimeStamp;
	}
	public String getAcctName() {
		return acctName;
	}
	public void setAcctName(String acctName) {
		this.acctName = acctName;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public String getPendingTime() {
		return pendingTime;
	}
	public void setPendingTime(String pendingTime) {
		this.pendingTime = pendingTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isDontMarkRequestsAsPublic() {
		return dontMarkRequestsAsPublic;
	}
	public void setDontMarkRequestsAsPublic(boolean dontMarkRequestsAsPublic) {
		this.dontMarkRequestsAsPublic = dontMarkRequestsAsPublic;
	}
	public void setMultiJobTag(long multiJobTag) {
		this.multiJobTag = multiJobTag;
	}
	public long getMultiJobTag() {
		return multiJobTag;
	}
	public void setRepeatGroup(String repeatGroup) {
		this.repeatGroup = repeatGroup;
	}
	public String getRepeatGroup() {
		return repeatGroup;
	}
	public void setElandmark(String elandmark) {
		this.elandmark = elandmark;
	}
	public String getElandmark() {
		return elandmark;
	}
	/*	private ArrayList<OpenRequestBO> arrayList = new ArrayList<OpenRequestBO>();

	public ArrayList<OpenRequestBO> getArrayList() {
		return arrayList;
	}
	public void setArrayList(ArrayList<OpenRequestBO> arrayList) {
		this.arrayList = arrayList;
	}
	public String getLandmark() {
		return landmark;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getAdd1() {
		return add1;
	}
	public void setAdd1(String add1) {
		this.add1 = add1;
	}
	public String getAdd2() {
		return add2;
	}
	public void setAdd2(String add2) {
		this.add2 = add2;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLatitude() {
		return Latitude;
	}
	public void setLatitude(String latitude) {
		Latitude = latitude;
	}
	public String getLongitude() {
		return Longitude;
	}
	public void setLongitude(String longitude) {
		Longitude = longitude;
	}
	public String getStlatitude2() {
		return stlatitude2;
	}
	public void setStlatitude2(String stlatitude2) {
		this.stlatitude2 = stlatitude2;
	}
	public String getStlongitude2() {
		return stlongitude2;
	}
	public void setStlongitude2(String stlongitude2) {
		this.stlongitude2 = stlongitude2;
	}
	public String getEdlatitude2() {
		return edlatitude2;
	}
	public void setEdlatitude2(String edlatitude2) {
		this.edlatitude2 = edlatitude2;
	}
	public String getEdlongitude2() {
		return edlongitude2;
	}
	public void setEdlongitude2(String edlongitude2) {
		this.edlongitude2 = edlongitude2;
	}
	public String getStlatitude3() {
		return stlatitude3;
	}
	public void setStlatitude3(String stlatitude3) {
		this.stlatitude3 = stlatitude3;
	}
	public String getStlongitude3() {
		return stlongitude3;
	}
	public void setStlongitude3(String stlongitude3) {
		this.stlongitude3 = stlongitude3;
	}
	public String getEdlatitude3() {
		return edlatitude3;
	}
	public void setEdlatitude3(String edlatitude3) {
		this.edlatitude3 = edlatitude3;
	}
	public String getEdlongitude3() {
		return edlongitude3;
	}
	public void setEdlongitude3(String edlongitude3) {
		this.edlongitude3 = edlongitude3;
	}
	public String getStlatitude4() {
		return stlatitude4;
	}
	public void setStlatitude4(String stlatitude4) {
		this.stlatitude4 = stlatitude4;
	}
	public String getStlongitude4() {
		return stlongitude4;
	}
	public void setStlongitude4(String stlongitude4) {
		this.stlongitude4 = stlongitude4;
	}
	public String getEdlatitude4() {
		return edlatitude4;
	}
	public void setEdlatitude4(String edlatitude4) {
		this.edlatitude4 = edlatitude4;
	}
	public String getEdlongitude4() {
		return edlongitude4;
	}
	public void setEdlongitude4(String edlongitude4) {
		this.edlongitude4 = edlongitude4;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getZip() {
		return zip;
	}
	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
	public String getLandmark() {
		return landmark;
	}
	public String getStlatitude() {
		return stlatitude;
	}
	public void setStlatitude(String stlatitude) {
		this.stlatitude = stlatitude;
	}
	public String getStlongitude() {
		return stlongitude;
	}
	public void setStlongitude(String stlongitude) {
		this.stlongitude = stlongitude;
	}
	public String getEaircode() {
		return eaircode;
	}
	public void setEaircode(String eaircode) {
		this.eaircode = eaircode;
	}
	public String getSaircode() {
		return saircode;
	}
	public void setSaircode(String saircode) {
		this.saircode = saircode;
	}
	ublic String getSaircodeid() {
		return saircodeid;
	}
	 *//**
	 * @param saircodeid the saircodeid to set
	 *//*
	public void setSaircodeid(String saircodeid) {
		this.saircodeid = saircodeid;
	}
	  *//**
	  * @return the eaircodeid
	  *//*
	public String getEaircodeid() {
		return eaircodeid;
	}
	   *//**
	   * @param eaircodeid the eaircodeid to set
	   *//*
	public void setEaircodeid(String eaircodeid) {
		this.eaircodeid = eaircodeid;
	}
	    */
	public void setSetTotalJobsPerDriver(String setTotalJobsPerDriver) {
		this.setTotalJobsPerDriver = setTotalJobsPerDriver;
	}
	public String getSetTotalJobsPerDriver() {
		return setTotalJobsPerDriver;
	}
	public void setRoundTrip(String roundTrip) {
		this.roundTrip = roundTrip;
	}
	public String getRoundTrip() {
		return roundTrip;
	}
	public void setDropTime(String dropTime) {
		this.dropTime = dropTime;
	}
	public String getDropTime() {
		return dropTime;
	}
	public void setNumOfPassengers(int numOfPassengers) {
		this.numOfPassengers = numOfPassengers;
	}
	public int getNumOfPassengers() {
		return numOfPassengers;
	}
	public void setRouteNumber(String routeNumber) {
		this.routeNumber = routeNumber;
	}
	public String getRouteNumber() {
		return routeNumber;
	}
	public void setPhoneCallDelayTime(int phoneCallDelayTime) {
		this.phoneCallDelayTime = phoneCallDelayTime;
	}
	public int getPhoneCallDelayTime() {
		return phoneCallDelayTime;
	}
	public void setAdjacentZoneOrderType(int adjacentZoneOrderType) {
		this.adjacentZoneOrderType = adjacentZoneOrderType;
	}
	public int getAdjacentZoneOrderType() {
		return adjacentZoneOrderType;
	}
	public void setPremiumCustomer(int premiumCustomer) {
		this.premiumCustomer = premiumCustomer;
	}
	public int getPremiumCustomer() {
		return premiumCustomer;
	}
	public void setPickupOrder(String pickupOrder) {
		this.pickupOrder = pickupOrder;
	}
	public String getPickupOrder() {
		return pickupOrder;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getTotal() {
		return total;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setShRideStatus(String shRideStatus) {
		this.shRideStatus = shRideStatus;
	}
	public String getShRideStatus() {
		return shRideStatus;
	}
	public void setSharedLogReason(String sharedLogReason) {
		this.sharedLogReason = sharedLogReason;
	}
	public String getSharedLogReason() {
		return sharedLogReason;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setNumOfCabs(int numOfCabs) {
		this.numOfCabs = numOfCabs;
	}
	public int getNumOfCabs() {
		return numOfCabs;
	}
	public void setPaymentStatus(int paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public int getPaymentStatus() {
		return paymentStatus;
	}
	public void setJobRating(int jobRating) {
		this.jobRating = jobRating;
	}
	public int getJobRating() {
		return jobRating;
	}
	public void setPickupOrderSwitch(int pickupOrderSwitch) {
		this.pickupOrderSwitch = pickupOrderSwitch;
	}
	public int getPickupOrderSwitch() {
		return pickupOrderSwitch;
	}
	public void setDropOffOrder(String dropOffOrder) {
		this.dropOffOrder = dropOffOrder;
	}
	public String getDropOffOrder() {
		return dropOffOrder;
	}
	public void setStaleCallTime(int staleCallTime) {
		this.staleCallTime = staleCallTime;
	}
	public int getStaleCallTime() {
		return staleCallTime;
	}
	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}
	public String getRouteDesc() {
		return routeDesc;
	}
	public void setSharedRidePaymentType(int sharedRidePaymentType) {
		this.sharedRidePaymentType = sharedRidePaymentType;
	}
	public int getSharedRidePaymentType() {
		return sharedRidePaymentType;
	}
	public void setCallerName(String callerName) {
		this.callerName = callerName;
	}
	public String getCallerName() {
		return callerName;
	}
	public void setPaymentMeter(int paymentMeter) {
		this.paymentMeter = paymentMeter;
	}
	public int getPaymentMeter() {
		return paymentMeter;
	}
	public String getCustomField() {
		return customField;
	}
	public void setCustomField(String customField) {
		this.customField = customField;
	}
	public String getAirName() {
		return airName;
	}
	public void setAirName(String airName) {
		this.airName = airName;
	}
	public String getAirTo() {
		return airTo;
	}
	public void setAirTo(String airTo) {
		this.airTo = airTo;
	}
	public String getAirFrom() {
		return airFrom;
	}
	public void setAirFrom(String airFrom) {
		this.airFrom = airFrom;
	}
	public String getAirNo() {
		return airNo;
	}
	public void setAirNo(String airNo) {
		this.airNo = airNo;
	}
	public String getMaterPayment() {
		return materPayment;
	}
	public void setMaterPayment(String materPayment) {
		this.materPayment = materPayment;
	}
	public boolean isDriverLogStatus() {
		return driverLogStatus;
	}
	public void setDriverLogStatus(boolean driverLogStatus) {
		this.driverLogStatus = driverLogStatus;
	}
	
}
