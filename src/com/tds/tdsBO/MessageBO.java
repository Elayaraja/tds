package com.tds.tdsBO;

import java.io.Serializable;
import java.util.ArrayList;

public class MessageBO implements Serializable{
 private static final long serialVersionUID = 1L;
 
 private String messagecode="";
 private String driverid="";
 private String desc="";
 private String enteredBy="";
 private String time="";
 private String loginSwitch="";
 private String message="";
 private String msgDate="";
 private String msgKey="";
 private String category ="";
 private int emergencyCheck=0;
 
public String getMessagecode() {
	return messagecode;
}
public void setMessagecode(String messagecode) {
	this.messagecode = messagecode;
}
public String getDriverid() {
	return driverid;
}
public void setDriverid(String driverid) {
	this.driverid = driverid;
}
public String getDesc() {
	return desc;
}
public void setDesc(String desc) {
	this.desc = desc;
}
public void setEnteredBy(String enteredBy) {
	this.enteredBy = enteredBy;
}
public String getEnteredBy() {
	return enteredBy;
}
public void setTime(String time) {
	this.time = time;
}
public String getTime() {
	return time;
}
public void setLoginSwitch(String loginSwitch) {
	this.loginSwitch = loginSwitch;
}
public String getLoginSwitch() {
	return loginSwitch;
}
public void setMessage(String message) {
	this.message = message;
}
public String getMessage() {
	return message;
}
public void setMsgDate(String msgDate) {
	this.msgDate = msgDate;
}
public String getMsgDate() {
	return msgDate;
}
public void setMsgKey(String msgKey) {
	this.msgKey = msgKey;
}
public String getMsgKey() {
	return msgKey;
}
public void setCategory(String category) {
	this.category = category;
}
public String getCategory() {
	return category;
}
public void setEmergencyCheck(int emergencyCheck) {
	this.emergencyCheck = emergencyCheck;
}
public int getEmergencyCheck() {
	return emergencyCheck;
}
 
}
