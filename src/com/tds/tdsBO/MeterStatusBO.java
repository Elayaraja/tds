package com.tds.tdsBO;
import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement


public class MeterStatusBO implements Serializable {
	public static final long serialVersionUID = 1L;

	private String latitude;
	private String longitude;
	private String driverId;
	private String cabNo;
	private String distanceTravelled;
	private String currentSpeed;
	private String fare="0.00";
	private String extra="0.00";
	private String amt="0.00";
	private String companyCode;
	private String pass;
	private String op;
	private String txn;
	private String meMeterStatus;
	private String meRate;

	private String ccNum;
	private String ccStatus;
	private String ccApproval;
	private String ccTxnCode;
	private String ccName="";
	private String ccExpiry="9999";
	
	private String respCode;
	private String statusCode;
	private String message;
	
	
	public String getRespCode() {
		return respCode;
	}
	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getCcNum() {
		return ccNum;
	}
	public void setCcNum(String ccNum) {
		this.ccNum = ccNum;
	}
	public String getCcStatus() {
		return ccStatus;
	}
	public void setCcStatus(String ccStatus) {
		this.ccStatus = ccStatus;
	}
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getFare() {
		return fare;
	}
	public void setFare(String fare) {
		this.fare = fare;
	}
	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public String getDistanceTravelled() {
		return distanceTravelled;
	}
	public void setDistanceTravelled(String distanceTravelled) {
		this.distanceTravelled = distanceTravelled;
	}
	public String getCurrentSpeed() {
		return currentSpeed;
	}
	public void setCurrentSpeed(String currentSpeed) {
		this.currentSpeed = currentSpeed;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getCabNo() {
		return cabNo;
	}
	public void setCabNo(String cabNo) {
		this.cabNo = cabNo;
	}
	public String getTxn() {
		return txn;
	}
	public void setTxn(String txn) {
		this.txn = txn;
	}
	public String getMeMeterStatus() {
		return meMeterStatus;
	}
	public void setMeMeterStatus(String meMeterStatus) {
		this.meMeterStatus = meMeterStatus;
	}
	public String getMeRate() {
		return meRate;
	}
	public void setMeRate(String meRate) {
		this.meRate = meRate;
	}
	public String getCcApproval() {
		return ccApproval;
	}
	public void setCcApproval(String ccApproval) {
		this.ccApproval = ccApproval;
	}
	public String getCcTxnCode() {
		return ccTxnCode;
	}
	public void setCcTxnCode(String ccTxnCode) {
		this.ccTxnCode = ccTxnCode;
	}
	public String getCcName() {
		return ccName;
	}
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}
	public String getCcExpiry() {
		return ccExpiry;
	}
	public void setCcExpiry(String ccExpiry) {
		this.ccExpiry = ccExpiry;
	}
	

}
