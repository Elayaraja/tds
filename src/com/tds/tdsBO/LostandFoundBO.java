package com.tds.tdsBO;

import java.io.Serializable;

public class LostandFoundBO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String lfKey;
	private String tripId;
	private String itemName;
	private String itemDesc;
	private String lfdate;
	private String lftime;
	private String driverid;
	private String assocCode;
	private String driverName;
	private String phoneNumber;
	private String riderName;
	private String mode;
	private String status;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getRiderName() {
		return riderName;
	}
	public void setRiderName(String riderName) {
		this.riderName = riderName;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	/**
	 * @return the driverName
	 */
	public String getDriverName() {
		return driverName;
	}
	/**
	 * @param driverName the driverName to set
	 */
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	/**
	 * @return the lfKey
	 */
	public String getLfKey() {
		return lfKey;
	}
	/**
	 * @param lfKey the lfKey to set
	 */
	public void setLfKey(String lfKey) {
		this.lfKey = lfKey;
	}
	/**
	 * @return the tripId
	 */
	public String getTripId() {
		return tripId;
	}
	/**
	 * @param tripId the tripId to set
	 */
	public void setTripId(String tripId) {
		this.tripId = tripId;
	}
	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}
	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	/**
	 * @return the itemDesc
	 */
	public String getItemDesc() {
		return itemDesc;
	}
	/**
	 * @param itemDesc the itemDesc to set
	 */
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	/**
	 * @return the lfdate
	 */
	public String getLfdate() {
		return lfdate;
	}
	/**
	 * @param lfdate the lfdate to set
	 */
	public void setLfdate(String lfdate) {
		this.lfdate = lfdate;
	}
	/**
	 * @return the lftime
	 */
	public String getLftime() {
		return lftime;
	}
	/**
	 * @param lftime the lftime to set
	 */
	public void setLftime(String lftime) {
		this.lftime = lftime;
	}
	/**
	 * @return the driverid
	 */
	public String getDriverid() {
		return driverid;
	}
	/**
	 * @param driverid the driverid to set
	 */
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	/**
	 * @return the assocCode
	 */
	public String getAssocCode() {
		return assocCode;
	}
	/**
	 * @param assocCode the assocCode to set
	 */
	public void setAssocCode(String assocCode) {
		this.assocCode = assocCode;
	}

}
