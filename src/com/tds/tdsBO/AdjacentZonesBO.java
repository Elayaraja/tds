package com.tds.tdsBO;
import java.io.Serializable;

public class AdjacentZonesBO implements Serializable {
	public static final long serialVersionUID = 1L;

	private String zoneNumber;
	private String adjacentZones;
	private String orderNumber;
	private int serialNo;




	public String getZoneNumber() {
		return zoneNumber;
	}
	public void setZoneNumber(String zoneNumber) {
		this.zoneNumber = zoneNumber;
	}
	public String getAdjacentZones() {
		return adjacentZones;
	}
	public void setAdjacentZones(String adjacentZones) {
		this.adjacentZones = adjacentZones;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
}
