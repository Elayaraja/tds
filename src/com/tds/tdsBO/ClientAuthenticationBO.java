package com.tds.tdsBO;

public class ClientAuthenticationBO {
	private String userId="";
	private String userName="";
	private String phone="";
	private String emailId="";
	private String assoccode="";
	private String timeZone ="";
	private String customerUniqueId="";
	private String cCAuthorizeAmount="0";
	private int cCMandatory=0;
	private String AppSrc="";

	public int getcCMandatory() {
		return cCMandatory;
	}

	public void setcCMandatory(int cCMandatory) {
		this.cCMandatory = cCMandatory;
	}

	public String getAssoccode() {
		return assoccode;
	}

	public void setAssoccode(String assoccode) {
		this.assoccode = assoccode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public String getCustomerUniqueId() {
		return customerUniqueId;
	}

	public void setCustomerUniqueId(String customerUniqueId) {
		this.customerUniqueId = customerUniqueId;
	}

	public String getcCAuthorizeAmount() {
		return cCAuthorizeAmount;
	}

	public void setcCAuthorizeAmount(String cCAuthorizeAmount) {
		this.cCAuthorizeAmount = cCAuthorizeAmount;
	}

	public String getAppSrc() {
		return AppSrc;
	}

	public void setAppSrc(String appSrc) {
		AppSrc = appSrc;
	}

}
