package com.tds.tdsBO;

public class QueueBean {
	private String driverid = "";
	private String lastLoginTime ="";
	private int loginTime = 0;

	private String tripid = "";
	private String startTimeStamp ="";
	private String QU_NAME;
	private int QU_WAITING;
	private int QU_JOBWAITING;
	private int flg;
	public String queueDescription;
	public String vehicleNo ="";
	private String queue_login_flag="";
	private String queue_name="";
	private String queue_desc="";
	
	private String QU_DRIVERNAME;
	private String QU_LOGINTIME;
	private String QU_ASSOCCODE ;
	private String QU_QUEUEPOSITION;
	private String driverProfile;
	private String QU_DRIVERID="";
	private String driverId="";
	private String reason="";
	
	private String dq_LoginTime="";
	private String dq_LogoutTime="";
	private String status ="";
	private String Dq_Reason ="";
	private String ORH_REASON ="";
	private String DZH_ZONEID ="";
	private String DZH_LOGINTIME ="";
	private String DZH_LOGOUTTIME ="";
	private String ORH_JOB_ACCEPT_TIME ="";
	private String ORH_JOB_COMPLETE_TIME ="";
	private String ORH_JOB_PICKUP_TIME ="";
	private String ORH_TRIP_ID ="";
	private String ORH_TIME_DIFF ="";

	private String DZH_reason ="";
	private String LO_Logintime="";
	private String LO_LogoutTime="";
	private String LO_REASON="";
	private String querystatus="";

	public String getQuerystatus() {
		return querystatus;
	}
	public void setQuerystatus(String querystatus) {
		this.querystatus = querystatus;
	}
	public String getLO_Logintime() {
		return LO_Logintime;
	}
	public void setLO_Logintime(String lO_Logintime) {
		LO_Logintime = lO_Logintime;
	}
	public String getLO_LogoutTime() {
		return LO_LogoutTime;
	}
	public void setLO_LogoutTime(String lO_LogoutTime) {
		LO_LogoutTime = lO_LogoutTime;
	}
	public String getLO_REASON() {
		return LO_REASON;
	}
	public void setLO_REASON(String lO_REASON) {
		LO_REASON = lO_REASON;
	}
	


	
	
	public String getDZH_reason() {
		return DZH_reason;
	}
	public void setDZH_reason(String dZH_reason) {
		DZH_reason = dZH_reason;
	}
	public String getORH_JOB_ACCEPT_TIME() {
		return ORH_JOB_ACCEPT_TIME;
	}
	public void setORH_JOB_ACCEPT_TIME(String oRH_JOB_ACCEPT_TIME) {
		this.ORH_JOB_ACCEPT_TIME = oRH_JOB_ACCEPT_TIME;
	}
	public String getORH_JOB_COMPLETE_TIME() {
		return ORH_JOB_COMPLETE_TIME;
	}
	public void setORH_JOB_COMPLETE_TIME(String oRH_JOB_COMPLETE_TIME) {
		this.ORH_JOB_COMPLETE_TIME = oRH_JOB_COMPLETE_TIME;
	}
	
	
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(String lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}
	public void setLoginTime(int loginTime) {
		this.loginTime = loginTime;
	}
	public int getLoginTime() {
		return loginTime;
	}
	
	
	
	
	
	public String getDq_LoginTime() {
		return dq_LoginTime;
	}
	public void setDq_LoginTime(String dq_LoginTime) {
		this.dq_LoginTime = dq_LoginTime;
	}
	public String getDq_LogoutTime() {
		return dq_LogoutTime;
	}
	public void setDq_LogoutTime(String dq_LogoutTime) {
		this.dq_LogoutTime = dq_LogoutTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getQueueDescription() {
		return queueDescription;
	}
	public void setQueueDescription(String qD) {
		queueDescription = qD;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	public int getFlg() {
		return flg;
	}
	public void setFlg(int flg) {
		this.flg = flg;
	}
	public int getQU_JOBWAITING() {
		return QU_JOBWAITING;
	}
	public void setQU_JOBWAITING(int qU_JOBWAITING) {
		QU_JOBWAITING = qU_JOBWAITING;
	}
	public int getQU_WAITING() {
		return QU_WAITING;
	}
	public void setQU_WAITING(int qU_WAITING) {
		QU_WAITING = qU_WAITING;
	}
	
	public String getQU_DRIVERNAME() {
		return QU_DRIVERNAME;
	}
	public void setQU_DRIVERNAME(String qUDRIVERNAME) {
		QU_DRIVERNAME = qUDRIVERNAME;
	}
	
	public String getDriverProfile() {
		return driverProfile;
	}
	public void setDriverProfile(String driverProfile) {
		this.driverProfile = driverProfile;
	}
	public String getQU_NAME() {
		return QU_NAME;
	}

	public String getDriverId() {
		return driverId;
	}
	public void setQU_NAME(String qUNAME) {
		QU_NAME = qUNAME;
	}
	public String getQU_DRIVERID() {
		return QU_DRIVERID;
	}
	public void setQU_DRIVERID(String qUDRIVERID) {
		QU_DRIVERID = qUDRIVERID;
	}
	public String getQU_LOGINTIME() {
		return QU_LOGINTIME;
	}
	public void setQU_LOGINTIME(String qULOGINTIME) {
		QU_LOGINTIME = qULOGINTIME;
	}
	public String getQU_ASSOCCODE() {
		return QU_ASSOCCODE;
	}
	public void setQU_ASSOCCODE(String qUASSOCCODE) {
		QU_ASSOCCODE = qUASSOCCODE;
	}
	public String getQU_QUEUEPOSITION() {
		return QU_QUEUEPOSITION;
	}
	public void setQU_QUEUEPOSITION(String qUQUEUEPOSITION) {
		QU_QUEUEPOSITION = qUQUEUEPOSITION;
	}
	public void setQueue_login_flag(String queue_login_flag) {
		this.queue_login_flag = queue_login_flag;
	}
	public String getQueue_login_flag() {
		return queue_login_flag;
	}
	public void setQueue_name(String queue_name) {
		this.queue_name = queue_name;
	}
	public String getQueue_name() {
		return queue_name;
	}
	public void setQueue_desc(String queue_desc) {
		this.queue_desc = queue_desc;
	}
	public String getQueue_desc() {
		return queue_desc;
	}
	public String getTripid() {
		return tripid;
	}
	public void setTripid(String tripid) {
		this.tripid = tripid;
	}
	public String getStartTimeStamp() {
		return startTimeStamp;
	}
	public void setStartTimeStamp(String startTimeStamp) {
		this.startTimeStamp = startTimeStamp;
	}

	public String getDZH_LOGOUTTIME() {
		return DZH_LOGOUTTIME;
	}
	public void setDZH_LOGOUTTIME(String DZH_LOGOUTTIME) {
		this.DZH_LOGOUTTIME = DZH_LOGOUTTIME;
	}
	public String getDZH_LOGINTIME() {
		return DZH_LOGINTIME;
	}
	public void setDZH_LOGINTIME(String DZH_LOGINTIME ) {
		this.DZH_LOGINTIME = DZH_LOGINTIME;
	}
	public String getDZH_ZONEID() {
		return DZH_ZONEID;
	}
	public void setDZH_ZONEID(String DZH_ZONEID) {
		this.DZH_ZONEID = DZH_ZONEID;
	}
	public String getORH_REASON() {
		return ORH_REASON;
	}
	public void setORH_REASON(String oRH_REASON) {
		ORH_REASON = oRH_REASON;
	}
	public String getDq_Reason() {
		return Dq_Reason;
	}
	public void setDq_Reason(String dq_Reason) {
		Dq_Reason = dq_Reason;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getORH_JOB_PICKUP_TIME() {
		return ORH_JOB_PICKUP_TIME;
	}
	public void setORH_JOB_PICKUP_TIME(String oRH_JOB_PICKUP_TIME) {
		ORH_JOB_PICKUP_TIME = oRH_JOB_PICKUP_TIME;
	}
	public String getORH_TRIP_ID() {
		return ORH_TRIP_ID;
	}
	public void setORH_TRIP_ID(String oRH_TRIP_ID) {
		ORH_TRIP_ID = oRH_TRIP_ID;
	}
	public String getORH_TIME_DIFF() {
		return ORH_TIME_DIFF;
	}
	public void setORH_TIME_DIFF(String oRH_TIME_DIFF) {
		ORH_TIME_DIFF = oRH_TIME_DIFF;
	}
	
	
}
