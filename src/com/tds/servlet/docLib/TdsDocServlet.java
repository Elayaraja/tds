package com.tds.servlet.docLib;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.tds.constants.docLib.DocumentConstant;
import com.tds.controller.docLib.DocController;
import com.tds.dao.docLib.DocumentDAO;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;


public class TdsDocServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("do");
		
		HttpSession m_session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
		request.setAttribute("compId", adminBO.getAssociateCode()); 
		request.setAttribute("userId", adminBO.getUid());
		
		if (action != null && action.equals("disp")) {
			showDocumentList(request, response);
		} else if (action != null && action.equals("dispAdmin")) {
			showAdminDocumentList(request, response);
		} else if (action != null && action.equals("rejectFile")) {
			rejectDocument(request, response);
		} else if (action != null && action.equals("approveFile")) {
			approveFile(request, response);
		} else if (action != null && action.equals("uploadNew")) {
			displayUpload(request, response);
		} else if (action != null && action.equals("uploadOne")) {
			displaySingleUpload(request, response);
		}else if (action != null && action.equals("uploadAlternate")) {
			uploadDocuments(request, response,"/jsp/uploadSingle.jsp");
		}else if (action != null && action.equals("upload")) {
			uploadDocuments(request, response,"/jsp/upload.jsp");
		}else if (action != null && action.equals("viewDoc")) {
			renderDocument(request, response);
		}else if (action != null && action.equals("uploadOthers")) {
			displayUploadOthers(request, response);
		}
		
		
		else{
			request.setAttribute("screen","/jsp/error.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}

	}
	
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		

		String action = request.getParameter("do");
		if (action != null && action.equals("disp")) {
			showDocumentList(request, response);
		} else if (action != null && action.equals("dispAdmin")) {
			showAdminDocumentList(request, response);
		} else if (action != null && action.equals("rejectFile")) {
			rejectDocument(request, response);
		} else if (action != null && action.equals("approveFile")) {
			approveFile(request, response);
		} else if (action != null && action.equals("uploadNew")) {
			displayUpload(request, response);
		} else if (action != null && action.equals("uploadOne")) {
			displaySingleUpload(request, response);
		}else if (action != null && action.equals("uploadAlternate")) {
			uploadDocuments(request, response,"/jsp/uploadSingle.jsp");
		}else if (action != null && action.equals("upload")) {
			uploadDocuments(request, response,"/jsp/upload.jsp");
		}else if (action != null && action.equals("viewDoc")) {
			renderDocument(request, response);
		}else if (action != null && action.equals("uploadOthers")) {
			displayUploadOthers(request, response);
		}
		else{
			request.setAttribute("screen","/jsp/error.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}

	}
	
	
	private void displayUploadOthers(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
				
		
		HttpSession m_session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
		request.setAttribute("compId", adminBO.getAssociateCode()); 
	
		request.setAttribute("userId", "");
		request.setAttribute("screen","/jsp/uploadsingle.jsp");
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}
	
	
	
	
	private void displayUpload(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
				
		String companyId = (String)request.getParameter("compId");
		String userId = (String)request.getParameter("userId");
		
		request.setAttribute("compId", companyId);
		request.setAttribute("userId", userId);
		request.setAttribute("screen","/jsp/upload.jsp");
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}
	
	
	private void showDocumentList(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		DocController dc = new DocController();
		dc.showListDocController(request);
		request.setAttribute("screen","/jsp/documentListUsers.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,response);
		
	}
	
	
	private void showAdminDocumentList(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("userId", request.getParameter("userId"));
		DocController.loadAdminPage(request);
		request.setAttribute("screen","/jsp/documentListApprove.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,response);
		
	}
	
	
	private void rejectDocument(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		DocController dc = new DocController();
		dc.rejectDocController(request);
		DocController.loadAdminPage(request);
		request.setAttribute("screen","/jsp/documentListApprove.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,response);

	}
	
	
	
	private void approveFile(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		try {
			DocController dc = new DocController();
			dc.approveDocController(request);
			DocController.loadAdminPage(request);
			request.setAttribute("screen","/jsp/documentListApprove.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,response);
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void displaySingleUpload(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		request.setAttribute("docId", request.getParameter("hDocId"));
		request.setAttribute("compId", request.getParameter("compId"));
		request.setAttribute("docNum", request.getParameter("hDocNum"));
		request.setAttribute("docType", request.getParameter("hDocType"));
		request.setAttribute("userId", request.getParameter("userId"));
		
		request.setAttribute("screen","/jsp/uploadSingle.jsp");
		
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}
	
	
	
	private void uploadDocuments(HttpServletRequest request,
			HttpServletResponse response,String fwdPath) throws ServletException, IOException {

		MultipartRequest multipartRequest = new MultipartRequest(request,"/usr/local/",1024 * 1024 * 10, new DefaultFileRenamePolicy());
		String compId = multipartRequest.getParameter("compId");
		String userId = multipartRequest.getParameter("userId");
		String docId = multipartRequest.getParameter("docId");
		
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;

		File tmpFile = multipartRequest.getFile("Filedata");
		InputStream fis = new FileInputStream(tmpFile);
		int len = (int) tmpFile.length();
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			
			String query = "insert into TDS_DOCUMENTS (DOCUMENT_NAME,TDS_COMP_CODE,DOC_DATA,DOC_STATUS,DOC_UPLOADED_BY) "
					+ " values (?,?,?,?,?)";
			
			pst = con.prepareStatement(query);
			pst.setString(1, tmpFile.getName());
			pst.setString(2, compId);
			pst.setBinaryStream(3, fis, len);
			pst.setString(4, DocumentConstant.STATUS_PENDING);
			pst.setString(5, userId);
			pst.execute();
			dbcon.closeConnection();
			if(docId!=null && !docId.equals("")){
				DocumentDAO docDAO = new DocumentDAO();
				docDAO.updateDocStatus(docId, DocumentConstant.STATUS_RENEWED, userId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			tmpFile.delete();
			dbcon.closeConnection();
		}
		request.setAttribute("userId", userId);
		request.setAttribute("compId", compId);
		
		request.setAttribute("screen",fwdPath);
	
		RequestDispatcher dispatcher = getServletContext()
				.getRequestDispatcher(TDSConstants.getMainNewJSP);
		
		dispatcher.forward(request, response);
	}
	
	private void renderDocument(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Blob blobData = null;
		String fileName = "";
		try {

			String documentId = request.getParameter("docId") != null ? request.getParameter("docId") : "";

			String query = "";

			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if (!documentId.equals("")) {
				query = "select DOC_DATA,DOCUMENT_NAME from TDS_DOCUMENTS where DOCUMENT_ID = ?";
				pst = con.prepareStatement(query);
				pst.setInt(1, Integer.parseInt(documentId));
				rs = pst.executeQuery();
				while (rs.next()) {
					blobData = rs.getBlob(1);
					fileName = rs.getString(2);
				}

			}

			response.setContentType(getServletContext().getMimeType(fileName));
			OutputStream outStream = response.getOutputStream();
			InputStream in = blobData.getBinaryStream();
			int nextByte = in.read();
			while (nextByte != -1) {
				outStream.write(nextByte);
				nextByte = in.read();
			}
			outStream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
	}
	
}
