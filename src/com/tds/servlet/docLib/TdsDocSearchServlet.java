package com.tds.servlet.docLib;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tds.controller.docLib.DocController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;


public class TdsDocSearchServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("do");
		
		HttpSession m_session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
		request.setAttribute("compId", adminBO.getAssociateCode()); 
		
		
		
		if (action != null && action.equals("searchOption")) {
			showDocumentList(request, response);
		} else if (action != null && action.equals("searchFile")) {
			searchDocumentList(request, response);
		} else if (action != null && action.equals("viewDoc")) {
			renderDocument(request, response);
		}else{
			request.setAttribute("screen","/jsp/error.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,response);
		}

	}
	
	
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("do");
		HttpSession m_session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
		request.setAttribute("compId", adminBO.getAssociateCode()); 
		
		
		
		
		if (action != null && action.equals("searchOption")) {
			showDocumentList(request, response);
		} else if (action != null && action.equals("searchFile")) {
			searchDocumentList(request, response);
		} else if (action != null && action.equals("viewDoc")) {
			renderDocument(request, response);

		}else{
			request.setAttribute("screen","/jsp/error.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,response);
		}

	}
	
	

	
	

	
	private void showDocumentList(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		DocController dc = new DocController();
		dc.showSearchDocuments(request);
			
			request.setAttribute("screen","/jsp/searchDocument.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,response);
		
	}
	
	
	private void searchDocumentList(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("userId", request.getParameter("userId"));
		DocController dc = new DocController();
		dc.searchDocumentList(request);
		request.setAttribute("screen","/jsp/searchDocument.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,response);
		
	}
	
	
	
	
	
	
	private void renderDocument(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Blob blobData = null;
		String fileName = "";
		try {

			String documentId = request.getParameter("docId") != null ? request.getParameter("docId") : "";

			String query = "";

			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if (!documentId.equals("")) {
				query = "select DOC_DATA,DOCUMENT_NAME from TDS_DOCUMENTS where DOCUMENT_ID = ?";
				pst = con.prepareStatement(query);
				pst.setInt(1, Integer.parseInt(documentId));
				rs = pst.executeQuery();
				while (rs.next()) {
					blobData = rs.getBlob(1);
					fileName = rs.getString(2);
				}

			}

			response.setContentType(getServletContext().getMimeType(fileName));
			OutputStream outStream = response.getOutputStream();
			InputStream in = blobData.getBinaryStream();
			int nextByte = in.read();
			while (nextByte != -1) {
				outStream.write(nextByte);
				nextByte = in.read();
			}
			outStream.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
	}
	
}
