package com.tds.ivr;

import org.asteriskjava.fastagi.AgiChannel;
import org.asteriskjava.fastagi.AgiException;
import org.asteriskjava.fastagi.AgiRequest;
import org.asteriskjava.fastagi.BaseAgiScript;


public class IVRMenuHadler extends BaseAgiScript{
	
	public void service(AgiRequest arg0, AgiChannel channel) throws AgiException {
		String event=channel.getVariable("event")!=null?channel.getVariable("event"):"";
		if(event.equals("") || event.equalsIgnoreCase("incomingBound")){
			IVRIncoming.handleCall(arg0, channel);
		}else if(event.equalsIgnoreCase("driverCaller")){
			IVRDriverMenuHandler.handleCall(arg0, channel);
		}else if(event.equalsIgnoreCase("passengerCaller")){
			IVRPassengerMenuHadlerOutBound.handleCall(arg0, channel);
		}else if(event.equalsIgnoreCase("callPassenger")){
			IVRPassengerCallerJobOutbound.handleCall(arg0, channel);
		}

	}
}
