package com.tds.ivr;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tds.cmp.bean.AsteriskBean;
import com.tds.cmp.bean.SMSBean;
import com.tds.dao.PhoneDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class PhoneIVR
 */
public class PhoneIVR extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PhoneIVR() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String eventParam = "";

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if((AdminRegistrationBO)request.getSession().getAttribute("user")!=null)
		{
			if(eventParam.equalsIgnoreCase("insertAsterisk")) {
				insertAsterisk(request,response); 
			}
		}
	}
	public void insertAsterisk(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		if(request.getParameter("submit")==null && request.getParameter("submitSMS")==null){
			AsteriskBean asteriskBean=PhoneDAO.getAsteriskParameter(adminBO.getMasterAssociateCode());
			request.setAttribute("asteriskValues", asteriskBean);
			SMSBean smsBean=PhoneDAO.getSMSParameter(adminBO.getMasterAssociateCode(),1);
			request.setAttribute("smsValues", smsBean);
			request.setAttribute("screen", "/SystemSetup/PhoneAsterisk.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		} else if(request.getParameter("submit")!=null){
			AsteriskBean asterisk=new AsteriskBean();
			asterisk.setIpAddress(request.getParameter("IPAddress"));
			asterisk.setName(request.getParameter("name"));
			asterisk.setUserName(request.getParameter("userName"));
			asterisk.setPassword(request.getParameter("password"));
			asterisk.setPhoneNumber(request.getParameter("phone"));
			asterisk.setLocation(request.getParameter("location"));
			int status=PhoneDAO.updateAsteriskParameter(adminBO.getMasterAssociateCode(),asterisk);
			if(status==1){
				request.setAttribute("errors", "Asterisk details updated successfully");
			} else {
				request.setAttribute("errors", "Failed to update Asterisk details");
			}
			AsteriskBean asteriskBean=PhoneDAO.getAsteriskParameter(adminBO.getMasterAssociateCode());
			SMSBean smsBean=PhoneDAO.getSMSParameter(adminBO.getMasterAssociateCode(),1);
			request.setAttribute("asteriskValues", asteriskBean);
			request.setAttribute("smsValues", smsBean);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}else if(request.getParameter("submitSMS")!=null){
			SMSBean smsBean=new SMSBean();
			smsBean.setUserName(request.getParameter("userId"));
			smsBean.setPassword(request.getParameter("passwordSMS"));
			smsBean.setPhone(request.getParameter("phoneSMS"));
			smsBean.setType(Integer.parseInt(request.getParameter("credentials")));
			int status=PhoneDAO.updateSMSParameter(adminBO.getMasterAssociateCode(),smsBean);
			if(status==1){
				request.setAttribute("errors", "SMS details updated successfully");
			} else {
				request.setAttribute("errors", "Failed to update SMS details");
			}
			AsteriskBean asteriskBean=PhoneDAO.getAsteriskParameter(adminBO.getMasterAssociateCode());
			SMSBean sms=PhoneDAO.getSMSParameter(adminBO.getMasterAssociateCode(),1);
			request.setAttribute("asteriskValues", asteriskBean);
			request.setAttribute("smsValues", sms);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}
	}

}
