package com.tds.ivr;

import java.io.IOException;

import org.asteriskjava.manager.ManagerConnection;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.asteriskjava.manager.ManagerEventListener;
import org.asteriskjava.manager.action.OriginateAction;
import org.asteriskjava.manager.event.DialEvent;
import org.asteriskjava.manager.event.HangupEvent;
import org.asteriskjava.manager.event.ManagerEvent;
import org.asteriskjava.manager.response.ManagerResponse;

import com.tds.cmp.bean.AsteriskBean;
import com.tds.dao.PhoneDAO;
import com.common.util.TDSProperties;

public class IVRPassengerCallerJob extends Thread implements ManagerEventListener {
	String phoneNo = "123456";
	String tripId = "";
	String companyCode="";
	String companyName="";
	String companyPhone="";

	private ManagerConnection managerConnection;
	
	public IVRPassengerCallerJob(String _tripId, String _phoneNo,String assoccode) throws IOException {
		AsteriskBean asterisk = PhoneDAO.getAsteriskParameter(assoccode);
		ManagerConnectionFactory factory = new ManagerConnectionFactory(asterisk.getIpAddress(),asterisk.getUserName(),asterisk.getPassword());
		this.managerConnection = factory.createManagerConnection();
		phoneNo = _phoneNo;
		tripId = _tripId;
		companyCode=assoccode;
		companyName=asterisk.getName();
		companyPhone=asterisk.getPhoneNumber();
	}

	public void run(){
        OriginateAction originateAction;
        ManagerResponse originateResponse = null;
        originateAction = new OriginateAction();
        //originateAction.setChannel("SIP/106");
        originateAction.setChannel("Local/"+phoneNo+"@from-internal");
        if(!companyPhone.equals("")){
        	originateAction.setCallerId(companyName +"<" +companyPhone+">");
        }
        originateAction.setExten(TDSProperties.getValue("IVRExtension"));
        originateAction.setContext("from-internal");
        originateAction.setPriority(new Integer(1));
        originateAction.setTimeout(new Integer(30000));
        originateAction.setVariable("tripID", tripId);
        originateAction.setVariable("phoneNo", phoneNo);
        originateAction.setVariable("cmpnyPh", companyPhone);
        originateAction.setVariable("assoccode",companyCode);
        originateAction.setVariable("event", "callPassenger");
        
        try{
	        // connect to Asterisk and log in
	        managerConnection.addEventListener(this);
	        managerConnection.login();
	     // register for events
	        
	        // send the originate action and wait for a maximum of 30 seconds for Asterisk
	        // to send a reply
	        originateResponse = managerConnection.sendAction(originateAction, 30000);
	        
        } catch (Exception e){
        	System.out.println(e.toString());
        }
        managerConnection.logoff();
	}

	
	@Override
	public void onManagerEvent(ManagerEvent event) {
		// TODO Auto-generated method stub
		//System.out.println("getting event"+ event.toString());
		// As new events 
		if(event instanceof DialEvent) {
		// code to handle DialEvent	
		}
		if(event instanceof HangupEvent) {
		// code to handle HangupEvent
		}
		
	}

}
