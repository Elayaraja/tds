package com.tds.ivr;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;

import org.apache.log4j.Category;
import org.asteriskjava.manager.ManagerConnection;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.asteriskjava.manager.ManagerEventListener;
import org.asteriskjava.manager.action.OriginateAction;
import org.asteriskjava.manager.event.DialEvent;
import org.asteriskjava.manager.event.HangupEvent;
import org.asteriskjava.manager.event.ManagerEvent;
import org.asteriskjava.manager.response.ManagerResponse;

import com.tds.cmp.bean.AsteriskBean;
import com.tds.controller.TDSController;
import com.tds.dao.DispatchDAO;
import com.tds.dao.PhoneDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.TDSProperties;

public class IVRDriverCaller extends Thread implements ManagerEventListener{
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(IVRDriverCaller.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given Class is "+IVRDriverCaller.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
    
	String phoneNo = "123456";
	String tripID = "";
	String driverID = "";
	String provider = "";
	String companyPhone = "";
	String companyName = "";
	String vehicle = "";
	String associationCode = "";
	String masterAssoccode="";
	String timeZoneOffset ="";
	public static ServletConfig config;

	private ManagerConnection managerConnection;
	
	public IVRDriverCaller(String _driverID, String _tripID, String _phoneNo, String _provider, String _companyPhone, String _companyName, String _vehicle, String _associationCode, String _timeZoneOffset, String _masterAssoccode,ServletConfig config) throws IOException {

		AsteriskBean asterisk = PhoneDAO.getAsteriskParameter(_masterAssoccode);
		ManagerConnectionFactory factory = new ManagerConnectionFactory(asterisk.getIpAddress(),asterisk.getUserName(),asterisk.getPassword());
		this.managerConnection = factory.createManagerConnection();
		phoneNo = _phoneNo;
		tripID = _tripID;
		driverID = _driverID;
		provider = _provider;
		companyPhone = asterisk.getPhoneNumber();
		companyName = asterisk.getName();
		vehicle = _vehicle;
		associationCode = _associationCode;
		masterAssoccode = _masterAssoccode;
		timeZoneOffset = _timeZoneOffset;
		this.config = config;
	}
	
	@Override
	public void run(){
		OpenRequestBO orBO = new OpenRequestBO();
		AdminRegistrationBO adminBO = new AdminRegistrationBO();
		adminBO.setAssociateCode(associationCode);
		adminBO.setMasterAssociateCode(masterAssoccode);
		adminBO.setTimeZone(timeZoneOffset);
		//adminBO.setTimeZoneOffet(Double.parseDouble(timeZoneOffset));
		orBO.setTripid(tripID);
		//System.out.println("Inside ivr tripid"+tripID);
		adminBO.setUid(driverID);
		adminBO.setCompanyList(SecurityDAO.getAllAssoccode(masterAssoccode));
		String jobsCode = DispatchDAO.getCompCode(orBO.getTripid(), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
			orBO = RequestDAO.getOpenRequestBean(orBO, "", "0", adminBO,adminBO.getCompanyList());
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			orBO = RequestDAO.getOpenRequestBean(orBO, "", "0", adminBO, myString);
		}
//		orBO = RequestDAO.getOpenRequestBean(orBO,"","0", adminBO);
		if(orBO==null){
			//System.out.println("ORBO is null");

			return;
		}
		cat.info("About to check driver update time");
		int time=DispatchDAO.getDriverUpdateTime(driverID,associationCode);
		cat.info("After check driver update time "+time);
		//if(time < 30){
		//	return;
		//}
        OriginateAction originateAction;
        ManagerResponse originateResponse = null;
        //String phoneNo = request.getParameter("phoneNo");
        originateAction = new OriginateAction();
        //originateAction.setChannel("SIP/106");
         originateAction.setChannel("Local/"+phoneNo+"@from-internal");
        if(!companyPhone.equals("")){
        	originateAction.setCallerId(companyName +"<" +companyPhone+">");
        }
        originateAction.setExten(TDSProperties.getValue("IVRExtension"));
        originateAction.setContext("from-internal");
        originateAction.setPriority(new Integer(1));
        originateAction.setTimeout(new Integer(30000));
        originateAction.setVariable("tripID", tripID);
        originateAction.setVariable("driverID", driverID);
        originateAction.setVariable("phoneNo", phoneNo);
        originateAction.setVariable("provider", provider);
        originateAction.setVariable("vehicle", vehicle);
        originateAction.setVariable("cmpnyPh", companyPhone);
        originateAction.setVariable("event", "driverCaller");
        originateAction.setVariable("associationCode", associationCode);
        originateAction.setVariable("masterAssoccode",masterAssoccode);
        originateAction.setVariable("timeZoneOffset", timeZoneOffset);
        
        
        try{
	        // connect to Asterisk and log in
	        managerConnection.addEventListener(this);
	        managerConnection.login();
	     // register for events
	        
	        // send the originate action and wait for a maximum of 30 seconds for Asterisk
	        // to send a reply
	        originateResponse = managerConnection.sendAction(originateAction, 30000);
	        
        } catch (Exception e){
        	System.out.println(e.toString());
        }
        managerConnection.logoff();
		
	}

	
	@Override
	public void onManagerEvent(ManagerEvent event) {
		// TODO Auto-generated method stub
		//System.out.println("getting event"+ event.toString());
		// As new events 
		if(event instanceof DialEvent) {
		// code to handle DialEvent	
		}
		if(event instanceof HangupEvent) {
		// code to handle HangupEvent
		}
		
	}
	

}
