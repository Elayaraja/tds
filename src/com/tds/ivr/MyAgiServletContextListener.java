package com.tds.ivr;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.asteriskjava.fastagi.DefaultAgiServer;

import com.common.util.TDSProperties;

public class MyAgiServletContextListener implements ServletContextListener {

	private static final IVRMenuHadler service=new IVRMenuHadler();
	private static final DefaultAgiServer AGIserver=new DefaultAgiServer(service);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		AGIserver.shutdown();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	      new Thread(new Runnable(){
	    	  //This code must be inside a thread
	    	  //because it stops the owner thread to listen on a specific port
	    	  //and this cause your application lock on startup/deploy
	    	   @Override
	    	  public void run() {
	    	  try {
//	    		  System.out.println("Hello world from agi");
	    		  AGIserver.setPort(Integer.parseInt(TDSProperties.getValue("IVRExtension")));
	    		  AGIserver.startup();
	    	  } catch (IllegalStateException e) {
	    		  e.printStackTrace();
	    	  } catch (Exception e) {
	    		  System.out.println(e);
	    		  e.printStackTrace();
	    	  }
	    	   }}).start();
	}
}

