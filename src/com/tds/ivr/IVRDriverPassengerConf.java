package com.tds.ivr;

import java.io.IOException;

import org.apache.log4j.Category;
import org.asteriskjava.manager.ManagerConnection;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.asteriskjava.manager.ManagerEventListener;
import org.asteriskjava.manager.action.OriginateAction;
import org.asteriskjava.manager.event.DialEvent;
import org.asteriskjava.manager.event.HangupEvent;
import org.asteriskjava.manager.event.ManagerEvent;
import org.asteriskjava.manager.response.ManagerResponse;

import com.tds.cmp.bean.AsteriskBean;
import com.tds.controller.TDSController;
import com.tds.dao.AuditDAO;
import com.tds.dao.IVRDAO;
import com.tds.dao.PhoneDAO;
import com.common.util.TDSProperties;

public class IVRDriverPassengerConf extends Thread implements ManagerEventListener{
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(IVRDriverPassengerConf.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given Class is "+IVRDriverPassengerConf.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
    
	String phoneNo = "123456";
	String companyPhone = "";
	String companyName = "";
	String masterAssoccode="";
	String driverPhoneNumber="";
	String tripId="";
	String driverId="";
	
	private ManagerConnection managerConnection;
	
	public IVRDriverPassengerConf(String _customerPhone,String _associationCode,String _driverPhone,String _tripId, String _driverId) throws IOException {

		AsteriskBean asterisk = PhoneDAO.getAsteriskParameter(_associationCode);
		ManagerConnectionFactory factory = new ManagerConnectionFactory(asterisk.getIpAddress(),asterisk.getUserName(),asterisk.getPassword());
		this.managerConnection = factory.createManagerConnection();

		phoneNo = _customerPhone;
		companyName=asterisk.getName();
		companyPhone=asterisk.getPhoneNumber();
		masterAssoccode=_associationCode;
		driverPhoneNumber=_driverPhone;
		tripId=_tripId;
		driverId=_driverId;
		
	}
	
	@Override
	public void run(){
        OriginateAction originateAction;
        ManagerResponse originateResponse = null;
        //String phoneNo = request.getParameter("phoneNo");
        originateAction = new OriginateAction();
        //originateAction.setChannel("SIP/106");
        originateAction.setChannel("Local/"+driverPhoneNumber+"@from-internal");
        if(!companyPhone.equals("")){
        	originateAction.setCallerId(companyName+"<" +companyPhone+ ">");
        }
        originateAction.setExten(TDSProperties.getValue("IVRExtensionConference"));
        originateAction.setContext(TDSProperties.getValue("IVRExtensionContext"));
        originateAction.setPriority(new Integer(1));
        originateAction.setTimeout(new Integer(30000));
        originateAction.setVariable("CustNo", "Local/"+phoneNo+"@from-internal");
        originateAction.setVariable("cmpPhone",companyPhone);
        originateAction.setVariable("cmpName",companyName);
		if(!driverPhoneNumber.contains("0000000")){
			IVRDAO.insertIVREntry(masterAssoccode, companyPhone, driverPhoneNumber);
		}
        AuditDAO.insertJobLogs("Driver-Passenger Conference Call", tripId, masterAssoccode, 40, driverId, "0.000", "0.000", masterAssoccode);
		if(!phoneNo.contains("0000000")){
			IVRDAO.insertIVREntry(masterAssoccode, driverPhoneNumber, phoneNo);
		}
        try{
	        // connect to Asterisk and log in
	        managerConnection.addEventListener(this);
	        managerConnection.login();
	     // register for events
	        
	        // send the originate action and wait for a maximum of 30 seconds for Asterisk
	        // to send a reply
	        originateResponse = managerConnection.sendAction(originateAction, 30000);
	        
        } catch (Exception e){
//        	System.out.println(e.toString());
        }
        managerConnection.logoff();
		
	}

	
	@Override
	public void onManagerEvent(ManagerEvent event) {
		// TODO Auto-generated method stub
	//	System.out.println("getting event"+ event.toString());

		if(event instanceof DialEvent) {
			int j = 0;
		}
		if(event instanceof HangupEvent) {
		// code to handle HangupEvent
			int j = 0;
		}
		
	}
	

}
