package com.tds.ivr;

import org.asteriskjava.fastagi.AgiChannel;
import org.asteriskjava.fastagi.AgiException;
import org.asteriskjava.fastagi.AgiRequest;

import com.tds.dao.DispatchDAO;
import com.tds.dao.IVRDAO;


public class IVRPassengerCallerJobOutbound {
	
	public static void handleCall(AgiRequest arg0, AgiChannel channel) throws AgiException {
		// TODO Auto-generated method stub
		//answer
		//System.out.println("About to call passenger for stale time");
		String tripId = channel.getVariable("tripID");
		String assoccode = channel.getVariable("assoccode");
		//Getting data from the channel that was sent by the manager
		String fromNumber = channel.getVariable("cmpnyPh");
		String phoneNumber = channel.getVariable("phoneNo");
		if(!phoneNumber.contains("0000000")){
			IVRDAO.insertIVREntry(assoccode, fromNumber, phoneNumber);
		}
		channel.answer();
		channel.exec("flite","Cannot create job.Do you want to continue?");
		char charValuePressedIs = channel.getOption("/var/lib/asterisk/sounds/followme/options","12",3000);
		if(charValuePressedIs== 0x0 ){
			charValuePressedIs = channel.getOption("/var/lib/asterisk/sounds/followme/options","12",3000);			
			if(charValuePressedIs== 0x0){
				DispatchDAO.updateJobByPassenger(assoccode, tripId, true);
				channel.exec("flite","Sorry Time Out. System will hangup the phone. Goodbye");
				channel.hangup();
				return;
			}
		}
		String stringValuePressedIs = Character.toString(charValuePressedIs);
		if(stringValuePressedIs.equals("1")){
			DispatchDAO.updateJobByPassenger(assoccode, tripId, true);
			channel.hangup();
			return;
		} else if(stringValuePressedIs.equals("2")){
			DispatchDAO.updateJobByPassenger(assoccode, tripId, false);
			channel.hangup();
			return;			
		}
	}
}
