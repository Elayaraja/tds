package com.tds.ivr;

import org.asteriskjava.fastagi.AgiChannel;
import org.asteriskjava.fastagi.AgiException;
import org.asteriskjava.fastagi.AgiRequest;

import com.tds.dao.IVRDAO;


public class IVRPassengerMenuHadlerOutBound {
	
	public static void handleCall(AgiRequest arg0, AgiChannel channel) throws AgiException {
		// TODO Auto-generated method stub
		//answer
		
		//Getting data from the channel that was sent by the manager
		String phoneNo = channel.getVariable("phoneNo");
		String message = channel.getVariable("msgID");
		String minutes = channel.getVariable("time");
		String fromNumber = channel.getVariable("cmpnyNum");
		String companyCode= channel.getVariable("assoccode");
		//System.out.println("Company Code In Call Before Answer--->"+companyCode+" Message Id--->"+message);
		if(!phoneNo.contains("0000000")){
			IVRDAO.insertIVREntry(companyCode, fromNumber, phoneNo);
		}
		channel.answer();
//		if(companyCode.equals("105")){
		channel.streamFile("/var/lib/asterisk/sounds/getacab/ThisIsACallFrom"+companyCode,"",0) ;
//		} else if(companyCode.equals("103")){
//			channel.streamFile("/var/lib/asterisk/sounds/getacab/ThisIsACallFromTaxiTime","",0) ;
//		} else if(companyCode.equals("113")){
//			channel.exec("flite","This is the call from Yellow Cab") ;
//		}
		if(message.equals("1")){
			channel.streamFile("/var/lib/asterisk/sounds/getacab/YourDriverWillArriveIn5Minutes","",0) ;
		} else if(message.equals("2")){
			channel.streamFile("/var/lib/asterisk/sounds/getacab/YourDriverWillArriveIn10Minutes","",0) ;
		} else if(message.equals("3")){
			channel.streamFile("/var/lib/asterisk/sounds/getacab/YourDriverWillArriveIn15Minutes","",0) ;
		} else if(message.equals("4")){
			channel.streamFile("/var/lib/asterisk/sounds/getacab/YourDriverWillArriveIn20Minutes","",0) ;
		} else if(message.equals("5")){
			channel.streamFile("/var/lib/asterisk/sounds/getacab/YourDriverHasArrived","",0) ;
		} else if(message.equals("6")){
//			channel.streamFile("/var/lib/asterisk/sounds/getacab/ErrorRetrivingJob","",0) ;
			channel.exec("flite","Driver can't find you there");
		} else if(message.equals("7")){
			channel.exec("flite","Your trip is allocated to a driver");
		} else if(message.equals("8")){
			channel.exec("flite","The driver is on route to pick you up");
		}else if(message.equals("9")){
			channel.exec("flite","Your driver is waiting outside");
		}
//		if(companyCode.equals("105")){
		channel.streamFile("/var/lib/asterisk/sounds/getacab/ThankYouForUsing"+companyCode,"",0) ;
//		} else if(companyCode.equals("103")){
//			channel.streamFile("/var/lib/asterisk/sounds/getacab/ThankYouForUsingTaxiTime","",0) ;
//		} else if(companyCode.equals("113")){
//			channel.exec("flite","Thank you for using Yellow Cab") ;
//		}
		channel.hangup();
		return;
	}
}
