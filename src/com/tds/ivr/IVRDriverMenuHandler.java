package com.tds.ivr;

import java.util.ArrayList;


import org.asteriskjava.fastagi.AgiChannel;
import org.asteriskjava.fastagi.AgiException;
import org.asteriskjava.fastagi.AgiRequest;

import com.tds.dao.AuditDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.IVRDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.TDSConstants;
import com.common.util.sendSMS;
import com.twilio.sdk.TwilioRestException;


public class IVRDriverMenuHandler {

	public static void handleCall(AgiRequest arg0, AgiChannel channel) throws AgiException {
		// TODO Auto-generated method stub
		//answer
		
		//Getting data from the channel that was sent by the manager
		//System.out.println("In DriverMenuHandler Function");
	//	System.out.println("Venki Testing In Number--->"+arg0.getCallerIdNumber());
		
		String phoneNo = channel.getVariable("phoneNo");
		String tripID = channel.getVariable("tripID");
		String driverID = channel.getVariable("driverID");
		String provider = channel.getVariable("provider");
		String vehicle = channel.getVariable("vehicle");
		String associationCode = channel.getVariable("associationCode");
		String fromNumber = channel.getVariable("cmpnyPh");
		String masterAssociationCode= channel.getVariable("masterAssoccode")==null?"":channel.getVariable("masterAssoccode");
		String timeZoneOffset = channel.getVariable("timeZoneOffset");
		OpenRequestBO orBO = new OpenRequestBO();
		AdminRegistrationBO adminBO = new AdminRegistrationBO();
		adminBO.setAssociateCode(associationCode);
		adminBO.setTimeZone(timeZoneOffset);
		orBO.setTripid(tripID);
		adminBO.setUid(driverID);
		String jobsCode = DispatchDAO.getCompCode(orBO.getTripid(), adminBO.getMasterAssociateCode());
		if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
			ArrayList<String> allFleets = SecurityDAO.getAllAssoccode(masterAssociationCode);
			orBO = RequestDAO.getOpenRequestBean(orBO, "", "0", adminBO,allFleets);
		} else {
			ArrayList<String> myString = new ArrayList<String>();
			orBO = RequestDAO.getOpenRequestBean(orBO, "", "0", adminBO, myString);
		}
//		orBO = RequestDAO.getOpenRequestBean(orBO,"","0", adminBO);
		//System.out.println("About to make IVR Entry");
		if(!phoneNo.contains("0000000")){
			IVRDAO.insertIVREntry(associationCode, fromNumber, phoneNo);
		}
		channel.answer();
		if(orBO==null){
			channel.streamFile("/var/lib/asterisk/sounds/getacab/ErrorRetrivingJob","",0);
			channel.streamFile("/var/lib/asterisk/sounds/getacab/ContactDispatcher","",0);
			channel.hangup();
			return;
		} else {
			//String time=(orBO.getShrs().equals("00")?12:orBO.getShrs())+orBO.getSmin();
			channel.streamFile("/var/lib/asterisk/sounds/getacab/YouHaveAJob","",3000);
		}
		char charValuePressedIs = channel.getOption("/var/lib/asterisk/sounds/followme/options","12",3000);
		if(charValuePressedIs== 0x0 ){
			charValuePressedIs = channel.getOption("/var/lib/asterisk/sounds/followme/options","12",3000);			
			if(charValuePressedIs== 0x0){
				//IVRDAO.updateOpenRequestOnDriverReject(associationCode, tripID, driverID);
				channel.exec("flite","Sorry Time Out. System will hangup the phone. Goodbye");
				channel.hangup();
				return;
			}
		}
//		if(charValuePressedIs== 0x0 ){
//		charValuePressedIs = getData("/var/lib/asterisk/sounds/followme/options",3000,1) ;			
//		if(charValuePressedIs== 0x0){
//			IVRDAO.updateOpenRequestOnDriverReject(associationCode, tripID, driverID);
//			channel.exec("flite","Sorry Time Out. System will hangup the phone. Goodbye");
//			hangup();
//			return;
//		}
//	}

		String stringValuePressedIs = Character.toString(charValuePressedIs);
		if(stringValuePressedIs.equals("1")){
			int numberOfRowsChanged = IVRDAO.updateOpenRequestOnDriverAccept(associationCode, tripID, driverID, vehicle);
			if(numberOfRowsChanged>0){
				channel.streamFile("/var/lib/asterisk/sounds/getacab/PleaseHangUpAndAcceptTheJobFromYourDevice","",0);
				ServiceRequestDAO.updateDriverAvailabilty("N", driverID, associationCode);
				AuditDAO.insertJobLogs("Trip Accepted By IVR", tripID,associationCode,TDSConstants.jobAccepted, "System","","",masterAssociationCode);

				OpenRequestBO orBo=RequestDAO.getOpenRequestByTripID(tripID, associationCode, "");
				String phoneNumber = adminBO.getSmsPrefix()+""+ServiceRequestDAO.getDriverPhoneNumber(driverID);
				try {
					sendSMS.main("Address:"+orBo.getSadd1()+","+orBo.getSadd2()+","+orBo.getScity()+","+orBo.getSstate(), phoneNumber, masterAssociationCode);
				} catch (TwilioRestException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				channel.hangup();
				return;
			} else {
				channel.exec("flite","Sorry job is not available. Please hangup phone. Goodbye");
				AuditDAO.insertJobLogs("Trip Timeout By IVR", tripID,associationCode,TDSConstants.jobRejected, "System","","",masterAssociationCode);
				channel.hangup();
				return;
			}
			
		} else if(stringValuePressedIs.equals("2")){
			IVRDAO.updateOpenRequestOnDriverReject(associationCode, tripID, driverID);
			AuditDAO.insertJobLogs("Trip Rejected By IVR", tripID,associationCode,TDSConstants.jobRejected, "System","","",masterAssociationCode);
			channel.streamFile("/var/lib/asterisk/sounds/getacab/YouHaveRejectedTheJob","",0);
			channel.hangup();
			return;			
		} else {
			
		}
			
	}

}
