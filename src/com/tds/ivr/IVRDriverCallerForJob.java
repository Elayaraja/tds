package com.tds.ivr;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Category;
import org.asteriskjava.manager.ManagerConnection;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.asteriskjava.manager.ManagerEventListener;
import org.asteriskjava.manager.action.OriginateAction;
import org.asteriskjava.manager.event.DialEvent;
import org.asteriskjava.manager.event.HangupEvent;
import org.asteriskjava.manager.event.ManagerEvent;
import org.asteriskjava.manager.response.ManagerResponse;

import com.tds.cmp.bean.AsteriskBean;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.controller.TDSController;
import com.tds.dao.IVRDAO;
import com.tds.dao.PhoneDAO;

public class IVRDriverCallerForJob extends Thread implements ManagerEventListener{
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(IVRDriverCallerForJob.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given Class is "+IVRDriverCallerForJob.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
    
	String phoneNo = "5618293323";
	String driverID = "";
	String companyPhone = "";
	String companyName = "";
	String associationCode = "";
	ArrayList<DriverCabQueueBean> driverList = new ArrayList<DriverCabQueueBean>();
	private ManagerConnection managerConnection;
	
	public IVRDriverCallerForJob(ArrayList<DriverCabQueueBean> dlList,String _associationCode) throws IOException {

		AsteriskBean asterisk = PhoneDAO.getAsteriskParameter(_associationCode);
		ManagerConnectionFactory factory = new ManagerConnectionFactory(asterisk.getIpAddress(),asterisk.getUserName(),asterisk.getPassword());
		this.managerConnection = factory.createManagerConnection();
		companyPhone = asterisk.getPhoneNumber();
		companyName = asterisk.getName();
		associationCode = _associationCode;
		driverList=dlList;
	}
	
	@Override
	public void run(){
        OriginateAction originateAction;
        ManagerResponse originateResponse = null;
        originateAction = new OriginateAction();
        if(!companyPhone.equals("")){
        	originateAction.setCallerId(companyName+"<" +companyPhone+ ">");
        }
        originateAction.setExten("7004");
        originateAction.setContext("custom-gaccalldriver");
        originateAction.setPriority(new Integer(1));
        originateAction.setTimeout(new Integer(20000));
        originateAction.setVariable("DriverMessage", "Attention..Attention...Lots of jobs available for pickup");
        for(int i=0;i<driverList.size();i++){
        	if(!driverList.get(i).getPhoneNo().contains("00000")){
        		IVRDAO.insertIVREntry(associationCode, companyPhone, driverList.get(i).getPhoneNo());
        	}
        	originateAction.setChannel("Local/91"+driverList.get(i).getPhoneNo()+"@from-internal");
	        try{
	            managerConnection.addEventListener(this);
	        	managerConnection.login();
		        originateResponse = managerConnection.sendAction(originateAction, 20000);
	        } catch (Exception e){
	        	//System.out.println(e.toString());
	        }
	        managerConnection.logoff();
        }
	}

	@Override
	public void onManagerEvent(ManagerEvent event) {
		// TODO Auto-generated method stub
		//System.out.println("getting event"+ event.toString());

		if(event instanceof DialEvent) {
			int j = 0;
		}
		if(event instanceof HangupEvent) {
		// code to handle HangupEvent
			int j = 0;
		}
	}
}
