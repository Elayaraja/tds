package com.tds.wrapper;

public class TripBean {
	private String tripId = "";
	private String startLatitude = "";
	private String endLatitude = "";
	private String startLongitude = "";
	private String endLongitude = "";
	private String startAddress1 = "";
	private String startAddress2 = "";
	private String startCity = "";
	private String startZip = "";
	private String startState = "";
	private String sharedRide = "";
	private long startTimeInMilliSeconds = 0;

	private String endAddress1 = "";

	private String endAddress2 = "";
	private String endCity = "";
	private String endState = "";
	private String endZip = "";

	private Double amt = 0.00;
	private Double tip = 0.00;
	private String serviceStartDate = "";
	private String serviceStartTime = "";
	private String serviceStartDateTime = "";
	private int tripSource = 0;

	public String getServiceStartDateTime() {
		return serviceStartDateTime;
	}

	public void setServiceStartDateTime(String serviceStartDateTime) {
		this.serviceStartDateTime = serviceStartDateTime;
	}

	private String paymentType = "";
	private String acctNum = "";
	private String acctName = "";

	private String customerPhoneNum = "";
	private String custName = "";
	private String specialInstructions = "";
	private String specialFlags = "";
	private String tripStatus = "";
	private String serviceEndDateTime = "";

	private double distance = 0.00;

	public String getCustomerPhoneNum() {
		return customerPhoneNum;
	}

	public void setCustomerPhoneNum(String customerPhoneNum1) {
		this.customerPhoneNum = customerPhoneNum1;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getSpecialInstructions() {
		return specialInstructions;
	}

	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}

	public String getSpecialFlags() {
		return specialFlags;
	}

	public void setSpecialFlags(String specialFlags) {
		this.specialFlags = specialFlags;
	}

	public String getTripId() {
		return tripId;
	}

	public void setTripId(String tripId1) {
		tripId = tripId1;
	}

	public String getStartLatitude() {
		return startLatitude;
	}

	public void setStartLatitude(String startLatitude1) {
		startLatitude = startLatitude1;
	}

	public String getEndLatitude() {
		return endLatitude;
	}

	public void setEndLatitude(String endLatitude1) {
		endLatitude = endLatitude1;
	}

	public String getStartLongitude() {
		return startLongitude;
	}

	public void setStartLongitude(String startLongitude1) {
		startLongitude = startLongitude1;
	}

	public String getEndLongitude() {
		return endLongitude;
	}

	public void setEndLongitude(String endLongitude1) {
		endLongitude = endLongitude1;
	}

	public String getStartAddress1() {
		return startAddress1;
	}

	public void setStartAddress1(String startAddress11) {
		startAddress1 = startAddress11;
	}

	public String getStartAddress2() {
		return startAddress2;
	}

	public void setStartAddress2(String startAddress21) {
		startAddress2 = startAddress21;
	}

	public String getStartCity() {
		return startCity;
	}

	public void setStartCity(String startCity1) {
		startCity = startCity1;
	}

	public String getStartZip() {
		return startZip;
	}

	public void setStartZip(String startZip1) {
		startZip = startZip1;
	}

	public String getStartState() {
		return startState;
	}

	public void setStartState(String startState1) {
		startState = startState1;
	}

	public Double getAmt() {
		return amt;
	}

	public void setAmt(Double amt1) {
		amt = amt1;
	}

	public Double getTip() {
		return tip;
	}

	public void setTip(Double tip1) {
		tip = tip1;
	}

	public String getServiceStartDate() {
		return serviceStartDate;
	}

	public void setServiceStartDate(String serviceDate1) {
		serviceStartDate = serviceDate1;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType1) {
		paymentType = paymentType1;
	}

	public String getAcctNum() {
		return acctNum;
	}

	public void setAcctNum(String acctNum1) {
		acctNum = acctNum1;
	}

	public String getAcctName() {
		return acctName;
	}

	public void setAcctName(String acctName1) {
		acctName = acctName1;
	}

	public String getServiceStartTime() {
		return serviceStartTime;
	}

	public void setServiceStartTime(String serviceStartTime1) {
		serviceStartTime = serviceStartTime1;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getSharedRide() {
		return sharedRide;
	}

	public void setSharedRide(String sharedRide) {
		this.sharedRide = sharedRide;
	}

	public String getEndAddress1() {
		return endAddress1;
	}

	public void setEndAddress1(String endAddress1) {
		this.endAddress1 = endAddress1;
	}

	public String getEndAddress2() {
		return endAddress2;
	}

	public void setEndAddress2(String endAddress2) {
		this.endAddress2 = endAddress2;
	}

	public String getEndCity() {
		return endCity;
	}

	public void setEndCity(String endCity) {
		this.endCity = endCity;
	}

	public String getEndState() {
		return endState;
	}

	public void setEndState(String endState) {
		this.endState = endState;
	}

	public String getEndZip() {
		return endZip;
	}

	public void setEndZip(String endZip) {
		this.endZip = endZip;
	}

	public String getTripStatus() {
		return tripStatus;
	}

	public void setTripStatus(String tripStatus) {
		this.tripStatus = tripStatus;
	}

	public void initilizeTripBean() {

		tripId = "";
		startLatitude = "";
		endLatitude = "";
		startLongitude = "";
		endLongitude = "";
		startAddress1 = "";
		startAddress2 = "";
		amt = 0.00;
		tip = 0.00;
		serviceStartDate = "";
		paymentType = "";
		acctNum = "";
		acctName = "";
		serviceStartTime = "";
		distance = 0.0;
	}

	public long getStartTimeInMilliSeconds() {
		return startTimeInMilliSeconds;
	}

	public void setStartTimeInMilliSeconds(long startTimeInMilliSeconds) {
		this.startTimeInMilliSeconds = startTimeInMilliSeconds;
	}

	public String getServiceEndDateTime() {
		return serviceEndDateTime;
	}

	public void setServiceEndDateTime(String serviceEndDateTime) {
		this.serviceEndDateTime = serviceEndDateTime;
	}

	public void setTripSource(int tripSource) {
		this.tripSource = tripSource;
	}

	public int getTripSource() {
		return tripSource;
	}

}
