package com.tds.wrapper;

public class WrapperInternetResponseBean {

	
	private String queue;
	private String assocode;
	private boolean loggedIn;
	private String sessionID;
	private String message;
	private String zoneID;
	private String zoneDescription;
	private String jobDetails;
	private boolean jobAvailable;
	public String getJobDetails() {
		return jobDetails;
	}
	public void setJobDetails(String jobDetails) {
		this.jobDetails = jobDetails;
	}
	public String getZoneDescription() {
		return zoneDescription;
	}
	public void setZoneDescription(String zoneDescription) {
		this.zoneDescription = zoneDescription;
	}


	private int zonePos;
	private boolean zoneStatus;
	private String tripData;
	
	

	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getZoneID() {
		return zoneID;
	}
	public void setZoneID(String zoneID) {
		this.zoneID = zoneID;
	}
	public int getZonePos() {
		return zonePos;
	}
	public void setZonePos(int zonePos) {
		this.zonePos = zonePos;
	}
	public boolean getZoneStatus() {
		return zoneStatus;
	}
	public void setZoneStatus(boolean zoneStatus) {
		this.zoneStatus = zoneStatus;
	}
	public String getTripData() {
		return tripData;
	}
	public void setTripData(String tripData) {
		this.tripData = tripData;
	}
	public String getAllocatedTripID() {
		return allocatedTripID;
	}
	public void setAllocatedTripID(String allocatedTripID) {
		this.allocatedTripID = allocatedTripID;
	}
	public WrapperInternetResponseBean() {
		
	}
	
	
	private String driverid;
	private String drFlag;
	private String cabNo;
	private String provider;
	private String phoneNo;
	private String passWord;
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}


	private String emailAddress;
	private String allocatedTripID;
	

	public String getSessionID() {
		return sessionID;
	}
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getDrFlag() {
		return drFlag;
	}
	public void setDrFlag(String drFlag) {
		this.drFlag = drFlag;
	}
	public String getCabNo() {
		return cabNo;
	}
	public void setCabNo(String cabNo) {
		this.cabNo = cabNo;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	public String getAssocode() {
		return assocode;
	}
	public void setAssocode(String assocode) {
		this.assocode = assocode;
	}
	public boolean isJobAvailable() {
		return jobAvailable;
	}
	public void setJobAvailable(boolean jobAvailable) {
		this.jobAvailable = jobAvailable;
	}

	
}
