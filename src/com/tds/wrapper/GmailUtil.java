package com.tds.wrapper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;
import java.util.TimerTask;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.gac.mobile.dao.MobileDAO;
import com.sun.mail.pop3.POP3SSLStore;
import com.tds.cmp.bean.DriverAndJobs;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.MessageGenerateJSON;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.tds.wrapperDAO.WrapperDAO;

public class GmailUtil extends TimerTask{
    //private String invalidCommand = "";
    private String activatFirst = "NOT ACTIVATED---- 0=ACTIVATE----";
    private String invalidCommand = "invalid-request- "+
    							    "0=activate--- "+
    							    "1=check-in " +
    							    "2=check-off " +
    							    "3=stand-pos " +
    							    "4=resend-job " +
    							    "6=std-query " +
    							    "7=accept-job " +
    							    "8=open-jobs " +
    							    "9=deactivate ";
    private String activated = "2222 ACTIVATED ";
    private String deActivated = "9999 DEACTIVATED";
    private String logoutString = "CHECKED OFF";
    private String errorActivating = "Error Activating. Please contact administrator";
    
    private Session session = null;
    private Store store = null;
    private String username, password;
    private Folder folder;
    private Session sessionSend = null;
    private Transport transport = null;
    private boolean jobIsRunning = false;
    private boolean continueProcess = true;
    private boolean resetAndReconnect = false;
    public GmailUtil() {
        
    }
    
    public void setUserPass(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
    public void connect() throws Exception {
        
        String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        
        Properties pop3Props = new Properties();
        
        pop3Props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
        pop3Props.setProperty("mail.pop3.socketFactory.fallback", "false");
        pop3Props.setProperty("mail.pop3.port",  "995");
        pop3Props.setProperty("mail.pop3.socketFactory.port", "995");
        pop3Props.put("mail.debug", "true");

        
        URLName url = new URLName("pop3", TDSProperties.getValue("PopEmailAddress"), 995, "",
                TDSProperties.getValue("PopEmailUser"), TDSProperties.getValue("PopEmailPassword"));
        
        session = Session.getInstance(pop3Props, null);
        //session.setDebug(true);
        store = new POP3SSLStore(session, url);
        store.connect();
        
        Properties smtpProp = new Properties();
        smtpProp.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        smtpProp.put("mail.smtp.starttls.enable", "true");
        smtpProp.put("mail.smtp.socketFactory.fallback","false");
        smtpProp.put("mail.smtp.host", "secure.emailsrvr.com");
        smtpProp.put("mail.smtp.port", "465"); // 587 is the port number of yahoo mail
        smtpProp.put("mail.smtp.auth", "true");
        smtpProp.put("mail.smtp.socketFactory.port", "465");
        smtpProp.put("mail.debug", "true");

        Authenticator sendAuth = new Authenticator() {
        	protected PasswordAuthentication getPasswordAuthentication() {
        		return new PasswordAuthentication(TDSProperties.getValue("PopEmailUser"), TDSProperties.getValue("PopEmailPassword"));
        		}		
        	};

        sessionSend = Session.getDefaultInstance(smtpProp, sendAuth);
        transport = sessionSend.getTransport("smtp");
        //System.out.println("About to connect with userid and passwrod"+TDSProperties.getValue("PopEmailUser")+"-"+TDSProperties.getValue("PopEmailPassword"));
        transport.connect("secure.emailsrvr.com", TDSProperties.getValue("PopEmailUser"), TDSProperties.getValue("PopEmailPassword"));
      
    }
    
    public void openFolder(String folderName) throws Exception {
        // Open the Folder
        folder = store.getDefaultFolder();
        
        folder = folder.getFolder(folderName);
        
    }
    
    
    public void run() {
    	continueProcess = true;
        if(jobIsRunning){
        	return;
        }
        jobIsRunning = true;
        Internet.setWebAddress("http://localhost/");
    	Message[] msgs = null;
    	try{
            if(resetAndReconnect==true){
            	store.close();
            	//System.out.println("Reconnecting to server......");
            	connect();
            	openFolder("INBOX");
            	if(folder!=null){
            		resetAndReconnect=false;
            	}
            }
            folder.open(Folder.READ_WRITE);
            msgs = folder.getMessages();
            FetchProfile fp = new FetchProfile();
            fp.add(FetchProfile.Item.CONTENT_INFO);        
            folder.fetch(msgs, fp);
            //System.out.println("Checking Messages. There are " +msgs.length + " messages" );
    	} catch (Exception e){
    		System.out.println("error processing " + e.toString());
    		msgs = new Message[0];
    		continueProcess = false;
    		resetAndReconnect = true;

    		//return;
    	}
    	if(continueProcess){
        for (int i = 0; i < msgs.length; i++) {
            try{
            //System.out.println(msgs[i].getContentType());
            //if(msgs[i].getContentType().contains("Multi")||msgs[i].getContentType().contains("multi")){ 
            //    msgs[i].setFlag(Flags.Flag.DELETED, true);
            	//continue;
            //}
            //System.out.println(msgs[i].getFrom()[0]+";"+(String)msgs[i].getContent());
            String [] parsedData = parseEmail(msgs[i].getFrom()[0].toString());
            WrapperBean driverData = WrapperDAO.getLoginDetails(parsedData[0]);
            driverData.setEmailAddress(msgs[i].getFrom()[0].toString());
            
            WrapperBeanCommand commandBean = null;
            String response = "";
            String emailContent = "";
            //System.out.println("d");
            if(driverData == null){
            	response = "Not Registered";
            } else {
            	if(msgs[i].getContentType().contains("Multi")||msgs[i].getContentType().contains("multi")){
            		Multipart multipart = (Multipart) msgs[i].getContent();
            		 for (int x = 0; x < multipart.getCount(); x++) {
            			 //emailContent = multipart.getBodyPart(x).toString().trim();
            			 BodyPart bodyPart = multipart.getBodyPart(x);
            			 //System.out.println(bodyPart.getContentType());
            			 if(bodyPart.getContentType().contains("text")){
            				 emailContent = emailContent + bodyPart.getContent().toString();
            			 }

            		 }
            		 //("EmailContent"+emailContent.toString());
            	} else {
            		emailContent = (String)msgs[i].getContent();
            	}
            	commandBean = parseCommand(emailContent);	
            	driverData.setProvider(parsedData[1]);
            	
            }
            	

            if(commandBean != null){ 
            	switch (commandBean.getCommand()) {
				case 0:
					response = login0(driverData);
					break;
				case 1:
					response = bookToZone1(driverData, commandBean);
					break;
				case 2:
					response = logOffZone2(driverData);
					response = "CHECKED OFF";
					break;
				case 3:
					response = getStandPosition3(driverData);
					break;
				case 4:
					response = resendJob(driverData);
					break;
				case 5:
					response = resendJob(driverData);
					break;
				case 6:
					response = standQuery6(driverData, commandBean);
					break;
				case 7:
					response = acceptAJob(driverData);
					break;
				case 8:
					response = getOpenJobs8(driverData);
					break;
				case 9:
					response = logoff9(driverData);
					response = deActivated;
					break;
					
				default:
					response = invalidCommand;
					break;
				}
            } else if(driverData != null){
            	response = invalidCommand;
            }
            
            //System.out.println("1");
            MimeMessage respondMessage = new MimeMessage(sessionSend);
            respondMessage.setContent(response, "text/plain");

            respondMessage.setRecipient(Message.RecipientType.TO, msgs[i].getFrom()[0]);
            respondMessage.setFrom(new InternetAddress(TDSProperties.getValue("PopEmailUser")));
            transport.send(respondMessage);
            //System.out.println(msgs[i].getFrom()[0]+";"+response);

            //respondMessage.sen
            //httpProcess(parsedData[0], parsedData[1]);
            msgs[i].setFlag(Flags.Flag.DELETED, true);
            
        	}catch (Exception e) {
        		
    			//System.out.println("Exception Reading and processing emails"+e.toString());
    			try{
                    msgs[i].setFlag(Flags.Flag.DELETED, true);
    			} catch (Exception e1){
    				
    			}
    		} 
            
        }
    	}
        try{
        folder.close(true);
        } catch(Exception e){
        	System.out.println("output"+e.toString());
        }
    	
        jobIsRunning = false;

        
    }
    
    
    public String[] parseEmail(String email){
    	String[] array = new String[3];
    	String[] split1 = email.split("@");
    	if(split1.length>1){
    		array[0] = split1[0];
    		if(split1[1].equals("messaging.sprintpcs.com")){
    			array[1] = "Sprint";
    		}
    		if(split1[1].equals("txt.att.net")){
    			array[1] = "ATT";
    		}
    		if(split1[1].equals("mms.att.net")){
    			array[1] = "ATT";
    		}
    		if(split1[1].equals("mymetropcs.com")){
    			array[1] = "MetroPCS";
    		}    		
    	}
    	
    	
    	return array;
    }
    
    public void httpProcess(String phoneNo, String provider){
		HttpClient client = new HttpClient();
		BufferedReader br = null;
		String googleRegKey = "";

		PostMethod method = new PostMethod("http://www.getacabdemo.com/TDS/control?action=mobilerequest&event=mlogin");
		method.addParameter("loginName", "10300");
		method.addParameter("password", "1234");
		method.addParameter("cabNo", "123");
		method.addParameter("phoneno", phoneNo);
		method.addParameter("provider", provider);
		method.addParameter("version", "Wrapper");
		method.addParameter("googRegKey", "");
		
	    try{
	        int returnCode = client.executeMethod(method);

	        if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
	          System.err.println("The Post method is not implemented by this URI");
	          method.getResponseBodyAsString();
	        } else {
	          br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
	          String readLine;
	          while(((readLine = br.readLine()) != null)) {
	            System.err.println(readLine);
	          }

	        }
	      } catch (Exception e) {
	        System.err.println(e);
	      } finally {
	        method.releaseConnection();
	        if(br != null) try { br.close(); } catch (Exception fe) {}
	      }


    }
    public WrapperBeanCommand parseCommand(String commands){
    	commands = commands.trim();
    	WrapperBeanCommand commandBean = null;
    	int commandInt;
    	String command = commands.substring(0,1);
    	try{
    		commandInt = Integer.parseInt(command);
    	} catch (Exception e){
    		return null;
    	}
    	switch (commandInt) {
		case 0:
		case 2:
		case 3:
		case 4:
		case 5:
		case 7:
		case 8:
		case 9:
			commandBean = new WrapperBeanCommand();
			commandBean.setCommand(commandInt);
			break;
		case 1:
		case 6:
			commandBean = new WrapperBeanCommand();
			commandBean.setCommand(commandInt);
			
			//String[] words = commands.split(" ");
			if(commands.length()>3){
			commandBean.setZone(commands.substring(1,4));
			}
			break;
		default:
			break;
		}
    	
    	return commandBean;
    	
    }
    public String login0(WrapperBean driverData){
    	String response = "";
    	String responseFromInternet;
    	if(!driverData.getSessionID().equals("")){
    		response ="Already Logged In";
        	return response;
    	} else {
    		WrapperBean anotherDriver = WrapperDAO.checkAnotherCab(driverData.getCabNo(), driverData.getAssocode());
    		if(anotherDriver != null){
    			logoff9(anotherDriver);
                MimeMessage anotherMessage = new MimeMessage(sessionSend);
                try{
	                anotherMessage.setContent(logoutString + "BY DRIVER " + driverData.getDriverid(), "text/plain");
	
	                anotherMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(anotherDriver.getEmailAddress()));
	                transport.send(anotherMessage);
                } catch(Exception e){
                	
                }

    		}
    		responseFromInternet = Internet.login(driverData);
    		WrapperInternetResponseBean intRes = WrapperParser.loginParser(responseFromInternet);
    		if(intRes.isLoggedIn()){
    			driverData.setSessionID(intRes.getSessionID());
    			Internet.driverAvailability(driverData);
    			if(WrapperDAO.udpateSessionIDAndEmail(driverData)>0){
    				response = activated;
    		    	ArrayList<DriverAndJobs> zoneDriverAndJobs = RegistrationDAO.getOpenJobSummary(driverData.getAssocode());
    		    	if(zoneDriverAndJobs == null || zoneDriverAndJobs.isEmpty())
    		    		return response;
    				for(int i=0;i<zoneDriverAndJobs.size();i++){
    					response += "S"+zoneDriverAndJobs.get(i).getZoneNumber()+"-"+zoneDriverAndJobs.get(i).getNumberOfJobs()+".. ";
    				}

    			} else{
        			response = errorActivating;    				
    			}
    		} else{
    			response = errorActivating;
    		}
    			
    	}
    	return response;
    	
    }
    public String bookToZone1(WrapperBean driverData, WrapperBeanCommand commandBean){
    	String response = "";
    	String responseFromInternet="";
    	if(driverData.getSessionID().equals("")){
    		return activatFirst;
    	}
    	responseFromInternet = Internet.zoneLogin(driverData, commandBean);
		WrapperInternetResponseBean intRes = WrapperParser.zoneLogin(responseFromInternet);
		if(intRes.isLoggedIn()==false) {
			return activatFirst;
		} else if (intRes.getZoneStatus()){
			response = "STAND " + intRes.getZoneID() + " - "+ intRes.getZonePos()+"...";
	    	ArrayList<DriverAndJobs> zoneDriverAndJobs = RegistrationDAO.getOpenJobSummary(driverData.getAssocode());
	    	if(zoneDriverAndJobs == null || zoneDriverAndJobs.isEmpty())
	    		return response;
			for(int i=0;i<zoneDriverAndJobs.size();i++){
				response += "s"+zoneDriverAndJobs.get(i).getZoneNumber()+"-"+zoneDriverAndJobs.get(i).getNumberOfJobs()+".. ";
			}

		} else {
			response = "Error Booking into Zone";
		}
			
    	return response;
    }
    public String logOffZone2(WrapperBean driverData){
    	String response = "";
    	if(driverData.getSessionID().equals("")){
    		return activatFirst;
    	}
    	String responseFromInternet = Internet.zoneLogOut(driverData);
		WrapperInternetResponseBean intRes = WrapperParser.zoneLogout(responseFromInternet);
		if(intRes.isLoggedIn()==false){
			return activatFirst;
		}
		response = "CHECKED OFF";
    	return response;
    	
    }
    public String standQuery6(WrapperBean driverData, WrapperBeanCommand commandBean){
    	
    	
    	String response = "";
    	if(driverData.getSessionID().equals("")){
    		return activatFirst;
    	}
    	if(Internet.ping(driverData).contains("Alive")){
    		int numOfDrivers = MobileDAO.getDriversInZone(driverData.getAssocode(), commandBean.getZone());
	    	ArrayList<DriverAndJobs> zoneDriverAndJobs = RegistrationDAO.getOpenJobSummary(driverData.getAssocode());
	    	response = "STAND " + commandBean.getZone() + " " + numOfDrivers+ " ";
	    	if(zoneDriverAndJobs == null || zoneDriverAndJobs.isEmpty()){
	    		return response;
	    	}
			for(int i=0;i<zoneDriverAndJobs.size();i++){
				response += "s"+zoneDriverAndJobs.get(i).getZoneNumber()+"-"+zoneDriverAndJobs.get(i).getNumberOfJobs()+".. ";
			}
    	} else {
    		response = "Session Expired";
           	WrapperDAO.logoutDriver(driverData);

    	}
		
    	
    	return response;
    

    }
    public String getStandPosition3(WrapperBean driverData){
    	String response = "";
    	if(driverData.getSessionID().equals("")){
    		return activatFirst;
    	}
    	if(Internet.ping(driverData).contains("Alive")){
			ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(driverData.getDriverid());
			if(currentQueue!=null && currentQueue.size()>0) {
				int m_position = ServiceRequestDAO.getQueuePosition(currentQueue.get(0),driverData.getDriverid(),driverData.getAssocode());
				response =  "STAND "+currentQueue + " - " +m_position;
			} else {
				return "ZoneStatus=Logged out";
	
			}
	    	ArrayList<DriverAndJobs> zoneDriverAndJobs = RegistrationDAO.getOpenJobSummary(driverData.getAssocode());
	    	if(zoneDriverAndJobs == null || zoneDriverAndJobs.isEmpty())
	    		return response;
			for(int i=0;i<zoneDriverAndJobs.size();i++){
				response += "S"+zoneDriverAndJobs.get(i).getZoneNumber()+"-"+zoneDriverAndJobs.get(i).getNumberOfJobs()+".. ";
			}

    	}else {
           	WrapperDAO.logoutDriver(driverData);
    		return "Session Expired";
    	}
    	return response;
    }
    public String getOpenJobs8(WrapperBean driverData){
    	String response = "";
    	if(driverData.getSessionID().equals("")){
    		return activatFirst;
    	}
    	if(Internet.ping(driverData).contains("Alive")){
	    	ArrayList<DriverAndJobs> zoneDriverAndJobs = RegistrationDAO.getOpenJobSummary(driverData.getAssocode());
	    	if(zoneDriverAndJobs == null || zoneDriverAndJobs.isEmpty())
	    		return response;
			for(int i=0;i<zoneDriverAndJobs.size();i++){
				response += "S"+zoneDriverAndJobs.get(i).getZoneNumber()+"-"+zoneDriverAndJobs.get(i).getNumberOfJobs()+".. ";
			}

    	}else {
           	WrapperDAO.logoutDriver(driverData);
    		return "Session Expired";
    	}
    	return response;
    }
    public String logoff9(WrapperBean driverData){
    	String response = "";
    	String responseFromInternet = Internet.logout(driverData);
    	WrapperDAO.logoutDriver(driverData);
    	return response;
    	
    }
    public String resendJob(WrapperBean driverData){
    	String response = "";
    	if(driverData.getSessionID().equals("")){
    		return activatFirst;
    	}
    	if(Internet.ping(driverData).contains("Alive")){
    		ArrayList<OpenRequestBO> al =  WrapperDAO.getAllAccepetedTrip(driverData, TDSConstants.jobAllocated);
	    	if(al == null || al.isEmpty())
	    		return "Job is empty";
	    	    	
	    	for(int i=0;i<al.size();i++)
	    	{
	    		response += MessageGenerateJSON.generateMessageAfterAcceptance(al.get(i), "", 0,"102");
	    	}
    	} else {
    		response = "Session Expired";
        	WrapperDAO.logoutDriver(driverData);
    	}
    	return response;

    }
    public String completeJob(WrapperBean driverData){
    	String response = "";
    	if(driverData.getSessionID().equals("")){
    		return activatFirst;
    	}
    	if(Internet.ping(driverData).contains("Alive")){
    		ArrayList<OpenRequestBO> al =  WrapperDAO.getAllAccepetedTrip(driverData, TDSConstants.jobAllocated);
	    	if(al == null || al.isEmpty())
	    		return "Job is empty";
	    	    	
	    	for(int i=0;i<al.size();i++)
	    	{
	    		response += MessageGenerateJSON.generateMessageAfterAcceptance(al.get(i), "", 0, "102");
	    	}
    	} else {
    		response = "Session Expired";
        	WrapperDAO.logoutDriver(driverData);
    	}
    	return response;

    }
    public String acceptAJob(WrapperBean driverData){
    	String response = "";
     	if(driverData.getSessionID().equals("")){
    		return activatFirst;
    	}
    	if(Internet.ping(driverData).contains("Alive")){
    		String acceptResponse = Internet.acceptATrip(WrapperDAO.getTripIDForDriver(driverData.getDriverid()),driverData);
    		TripBean tripBean = WrapperParser.acceptedTripBean(acceptResponse);
    		if(tripBean.getTripStatus().equals("40")){
    			response = tripBean.getStartAddress1() + " " + tripBean.getStartAddress2() + " " + tripBean.getCustomerPhoneNum() + " " + tripBean.getCustName(); 
    			return acceptResponse;
    		} else {
    			return "--JOB TIME-OUT--";
    		}    			
    		//return "Accepted";
    	} else {
           	WrapperDAO.logoutDriver(driverData);
           	return "Session Expired";
    	}
    		
    }
}