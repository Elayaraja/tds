package com.tds.wrapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class WrapperParser {

	public static WrapperInternetResponseBean loginParser(String loginResponse) {
		WrapperInternetResponseBean intRespBean = new WrapperInternetResponseBean();
		
		if (loginResponse.contains("ResVer=1.1")) {
			String[] response = loginResponse.split(";");
			if (response.length > 1) {

				for (int i = 0; i < response.length; i++) {
					String[] nameValPar = response[i].split("=");
					if (nameValPar.length == 2) {
						if (nameValPar[0].equalsIgnoreCase("Login")) {
							if (nameValPar[1].equalsIgnoreCase("Valid")) {
								intRespBean.setLoggedIn(true);
							} else {
								intRespBean.setLoggedIn(false);
								intRespBean.setMessage("Couldn't Login");
								break;
							}
						} else if (nameValPar[0].equalsIgnoreCase("SessionID")) {
							intRespBean.setSessionID(nameValPar[1]);
						} 
					}
				}
			}
		} 		
		
		return intRespBean;
	}

	public static WrapperInternetResponseBean zoneLogin(String zoneLoginResponse) {
		boolean success = false;
		String zoneStatus = "";
		String zonePosition = "";
		String zoneName = "";
		boolean successLoggingIn = false;
		WrapperInternetResponseBean intRespBean = new WrapperInternetResponseBean();

		if (sessionTimedOut(zoneLoginResponse)) {
			intRespBean.setLoggedIn(false);
			intRespBean.setMessage("System has timed-out");
			return intRespBean;
		}
		String[] firstLevelSplit = zoneLoginResponse.split(";");
		if (firstLevelSplit.length < 1) {
			return intRespBean;
		}
		intRespBean.setZoneStatus(true);
		for (int i = 0; i < firstLevelSplit.length; i++) {
			String[] nameValue = firstLevelSplit[i].split("=");
			if (nameValue[0].equalsIgnoreCase("ZoneStatus")) {
				if (nameValue[1].equalsIgnoreCase("LoggedIn") || nameValue[1].equalsIgnoreCase("AlreadyLoggedIn") || nameValue[1].equalsIgnoreCase("QueueSwitched")) {
					successLoggingIn = true;
					
					intRespBean.setLoggedIn(true);
					success = true;
				} else {
					successLoggingIn = false;
					intRespBean.setZoneStatus(false);
					intRespBean.setZonePos(0);
					intRespBean.setZoneID("");
					intRespBean.setMessage(nameValue[1]);
					break;
				}
			} else if (nameValue[0].equalsIgnoreCase("ZonePosition")) {
				intRespBean.setZonePos(Integer.parseInt(nameValue[1]));
			} else if (nameValue[0].equalsIgnoreCase("ZoneName")) {
				intRespBean.setZoneID(nameValue[1]);
			} else if (nameValue[0].equalsIgnoreCase("ZoneDesc")) {
				intRespBean.setZoneDescription(nameValue[1]);
			}

		}

		return intRespBean;

	}

	public static WrapperInternetResponseBean zoneLogout(String zoneLoginResponse) {
		boolean success = false;
		String zoneStatus = "";
		String zonePosition = "";
		String zoneName = "";
		WrapperInternetResponseBean intRespBean = new WrapperInternetResponseBean();

		boolean successLoggingIn = false;
		if (sessionTimedOut(zoneLoginResponse)) {
			intRespBean.setLoggedIn(false);
			intRespBean.setMessage("System has timed-out");
			return intRespBean;
		}
		String[] firstLevelSplit = zoneLoginResponse.split(";");
		if (firstLevelSplit.length < 1) {
			return intRespBean;
		}
		for (int i = 0; i < firstLevelSplit.length; i++) {
			String[] nameValue = firstLevelSplit[i].split("=");
			if (nameValue[0].equalsIgnoreCase("ZoneStatus")) {
				if (nameValue[1].equalsIgnoreCase("LoggedOut")) {
					intRespBean.setZoneStatus(false);
					intRespBean.setZoneID("");
					intRespBean.setZoneDescription("");
					intRespBean.setZonePos(0);
					success = true;
				}
			}
		}

		return intRespBean;

	}

	public static WrapperInternetResponseBean acceptedTrip(String acceptTripResponse) {
		WrapperInternetResponseBean intRespBean = new WrapperInternetResponseBean();

		if (acceptTripResponse.contains("ResVer=1.1")) {
			String[] response = acceptTripResponse.split(";");
			if (response.length > 1) {
				for (int i = 0; i < response.length; i++) {
					String[] nameValPar = response[i].split("=");
					if (nameValPar.length == 2) {
						if (nameValPar[0].equalsIgnoreCase("Status")) {
							if (nameValPar[1].equalsIgnoreCase("SUCCESS")) {
								intRespBean.setJobAvailable(true);
							} else {
								intRespBean.setJobAvailable(false);
							}
						} else if (nameValPar[0].equalsIgnoreCase("TI")) {
							
						} else if (nameValPar[0].equalsIgnoreCase("SLA")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SLO")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SD")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("ST")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SI")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SA1")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SA2")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("C")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("Z")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("CN")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("CP")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						}

					}
				}
			}
		}

		return intRespBean;
	}

	public static WrapperInternetResponseBean getTripDetails(String acceptTripResponse) {
		WrapperInternetResponseBean intRespBean = new WrapperInternetResponseBean();

		if (acceptTripResponse.contains("ResVer=1.1")) {
			String[] response = acceptTripResponse.split(";");
			if (response.length > 1) {
				for (int i = 0; i < response.length; i++) {
					String[] nameValPar = response[i].split("=");
					if (nameValPar.length == 2) {
						if (nameValPar[0].equalsIgnoreCase("Status")) {
							if (nameValPar[1].equalsIgnoreCase("SUCCESS")) {
								intRespBean.setJobAvailable(true);
							} else {
								intRespBean.setJobAvailable(false);
							}
						} else if (nameValPar[0].equalsIgnoreCase("TI")) {
							
						} else if (nameValPar[0].equalsIgnoreCase("SLA")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SLO")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SD")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("ST")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SI")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SA1")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SA2")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("C")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("Z")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("CN")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("CP")) {
							intRespBean.setJobDetails(intRespBean.getJobDetails()+ nameValPar[1]);
						}

					}
				}
			}
		}

		return intRespBean;

	}

	public static boolean sessionTimedOut(String response) {
		if (response.contains("Your Session Expired")) {
			return true;
		} else
			return false;
	}

	public static boolean isNumeric(String inputData) {
		return inputData.matches("[-+]?\\d+(\\.\\d+)?");
	}

	public static void publicRequests(String publicRequests) {

	}

	public static void genericMessage(String publicRequests) {

	}
	public static TripBean acceptedTripBean(String acceptTripResponse) {
		TripBean tripData = null;
		if (acceptTripResponse.contains("ResVer=1.1")) {
			String[] response = acceptTripResponse.split(";");
			tripData = new TripBean();
			if (response.length > 1) {
				for (int i = 0; i < response.length; i++) {
					String[] nameValPar = response[i].split("=");
					if (nameValPar.length == 2) {
						if (nameValPar[0].equalsIgnoreCase("Status")) {
							if (nameValPar[1].equalsIgnoreCase("SUCCESS")) {
								tripData.setTripStatus("40");
							} else {
								tripData.setTripStatus("99");
							}
						} else if (nameValPar[0].equalsIgnoreCase("TI")) {
							tripData.setTripId(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SLA")) {
							tripData.setStartLatitude(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SLO")) {
							tripData.setStartLongitude(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SD")) {
							// newTripBean.setT(nameValue[1]);
						} else if (nameValPar[0].equalsIgnoreCase("ST")) {
							tripData.setServiceStartDateTime(nameValPar[1]);
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

							try {
								Date tmpDateFormat = dateFormat.parse(nameValPar[1]);
								tripData.setServiceStartDate(tmpDateFormat.getMonth() + "/" + tmpDateFormat.getDay() + "/" + tmpDateFormat.getYear());
								tripData.setServiceStartTime(DateFormat.getTimeInstance(DateFormat.SHORT).format(dateFormat.parse(nameValPar[1])));
							} catch (Exception e) {
							}
						} else if (nameValPar[0].equalsIgnoreCase("SI")) {
							// newTripBean.sets
						} else if (nameValPar[0].equalsIgnoreCase("SA1")) {
							tripData.setStartAddress1(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SA2")) {
							tripData.setStartAddress2(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SC")) {
							tripData.setStartCity(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SZ")) {
							tripData.setStartZip(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("CN")) {
							tripData.setCustName(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("CP")) {
							tripData.setCustomerPhoneNum(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("PT")) {
							tripData.setPaymentType(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("PA")) {
							tripData.setAcctNum(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("PN")) {
							tripData.setAcctName(nameValPar[1]);
						} else if (nameValPar[0].equalsIgnoreCase("SPI")) {
							tripData.setSpecialInstructions(nameValPar[1]);
						}

					}
				}
			}
		}

		return tripData;
	}
}
