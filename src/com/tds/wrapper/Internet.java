package com.tds.wrapper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class Internet {

	private static String web = "http://localhost";

	public static void setWebAddress(String address) {
		web = address;
	}

	public static String getWebAddress() {
		return web;
	}

	public static String login(WrapperBean driverData) {

		String response = "";
		String encodedData = ""; // user-supplied

		try {
			// Perform action on click
			String postData = URLEncoder.encode("loginName", "UTF-8") + "=" + URLEncoder.encode(driverData.getDriverid(), "UTF-8");
			postData += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(driverData.getPassWord(), "UTF-8");
			postData += "&" + URLEncoder.encode("cabNo", "UTF-8") + "=" + URLEncoder.encode(driverData.getCabNo(), "UTF-8");
			postData += "&" + URLEncoder.encode("phoneno", "UTF-8") + "=" + URLEncoder.encode(driverData.getPhoneNo(), "UTF-8");
			postData += "&" + URLEncoder.encode("provider", "UTF-8") + "=" + URLEncoder.encode(driverData.getProvider(), "UTF-8");
			postData += "&" + URLEncoder.encode("version", "UTF-8") + "=" + URLEncoder.encode("Wrapper", "UTF-8");
			postData += "&" + URLEncoder.encode("googRegKey", "UTF-8") + "=" + URLEncoder.encode("", "UTF-8");

			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=mlogin");
			// Get the response
			response = connectToInternet(url1, postData, driverData);
		} catch (Exception e) {
			response = e.getMessage();
		}
		return response;

	}

	public static String tripDetail(String tripid, WrapperBean driverData) {
		String response = "";
		try {
			URL url1 = new URL("http://" + web + "/TDS/control?action=mobilerequest&event=&tripid=" + tripid);
			String encodedData = ""; // user-supplied
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = e.getMessage();
		}
		return response;

	}

	public static String logout(WrapperBean driverData) {
		String encodedData = ""; // user-supplied
		String response;
		try {

			// Perform action on click
			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=mlogout");
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = e.getMessage();
		}
		return response;

	}

	public static String driverAvailability(WrapperBean driverData) {
		String response = "";
		try {

			String encodedData = ""; // user-supplied

			URL url1 = new URL(web + "/TDS/mobilerequest?action=mobilerequest&event=mdriverlocation&latitude=1.0&longitude=1.0&availability=Y");
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;

	}

	public static String ping(WrapperBean driverData) {
		String response = "";
		try {

			String encodedData = ""; // user-supplied

			URL url1 = new URL(web + "/TDS/ping");
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;

	}


	public static String zoneLogin(WrapperBean driverData, WrapperBeanCommand commandBean) {

		String response = "";
		try {
			String postData = URLEncoder.encode("queueName", "UTF-8") + "=" + URLEncoder.encode(commandBean.getZone(), "UTF-8");
			postData += "&" + URLEncoder.encode("latitude", "UTF-8") + "=1.0" + URLEncoder.encode("1.0", "UTF-8");
			postData += "&" + URLEncoder.encode("longitude", "UTF-8") + "=" + URLEncoder.encode("1.0", "UTF-8");

			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=loginToQueue");
			// Get the response
			response = connectToInternet(url1, postData, driverData);

		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;
	}

	public static String zonePosition(WrapperBean driverData) {
		String response = "";
		try {
			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=mobileGetZonePosition");
			// Get the response
			response = connectToInternet(url1, "", driverData);

		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;
	}

	public static String zoneLogOut(WrapperBean driverData) {
		String response = "";
		try {
			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=zoneLogout");
			// Get the response
			response = connectToInternet(url1, "", driverData);

		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;
	}

	public static String getQueueTripDetails(String tripID, WrapperBean driverData) {
		String response = "";
		try {

			// Perform action on click
			String encodedData = "tripid=" + URLEncoder.encode(tripID, "UTF-8");
			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=qsetup");
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;
	}

	public static String getAcceptedTripDetails(String tripID, WrapperBean driverData) {
		String response = "";
		try {

			String encodedData = "tripid=" + URLEncoder.encode(tripID, "UTF-8");

			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=qsetup");
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;
	}

	public static String acceptATrip(String tripID,WrapperBean driverData) {
		String response = "";
		try {

			// Perform action on click
			String encodedData = "tripid=" + URLEncoder.encode(tripID, "UTF-8");

			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=maccepttrip");
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;
	}

	public static String delete_180114_rejectATrip(String tripID,WrapperBean driverData) {
		String response = "";
		try {

			// Perform action on click
			String encodedData = "tripid=" + URLEncoder.encode(tripID, "UTF-8");

			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=mrejecttrip");
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = "TDSE" + e.getMessage();
		}
		return response;
	}

	public static String delete_180114_checkConnectivity(WrapperBean driverData) {
		String response = "";
		try {

			// Perform action on click
			String encodedData = ""; // user-supplied

			URL url1 = new URL(web + "/TDS/control?action=mobilerequest&event=qsetup");
			// Get the response
			response = connectToInternet(url1, encodedData, driverData);
		} catch (Exception e) {
			response = e.getMessage();
		}
		return response;

	}


public static String connectToInternet(URL url1, String postData,WrapperBean driverData) {

		// String encodedData = ""; // user-supplied
		String response = "";
		System.setProperty("http.keepAlive", "false");
		try {
			HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("User-Agent", "GetACab");
			conn.setRequestProperty("cookie", "JSESSIONID="+driverData.getSessionID());
			// conn.setRequestProperty("connection","Keep-Alive");
			PrintStream ps = new PrintStream(conn.getOutputStream());
			ps.write(postData.getBytes());
			ps.flush();
			ps.close();

			conn.connect();

			if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
				InputStream is = conn.getInputStream();
				// read reply
				StringBuffer b = new StringBuffer();
				BufferedReader r = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = r.readLine()) != null)
					b.append(line);
				// System.out.println("Reply From Site:" + b.toString());
				is.close();
				response = b.toString();
				conn.disconnect();

			}

		}

		catch (Exception e) {
			response = "GRACIERROR;" + e.getMessage();

		}

		return response;
	}

}
