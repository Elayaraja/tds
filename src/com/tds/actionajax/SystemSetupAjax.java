package com.tds.actionajax;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Timer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tds.bean.docLib.DocBean;
import com.tds.cmp.bean.CallerIDBean;
import com.tds.cmp.bean.CompanySystemProperties;
import com.tds.cmp.bean.DriverVehicleBean;
import com.tds.cmp.bean.MessageBeen;
import com.tds.cmp.bean.MeterType;
import com.tds.cmp.bean.OperatorShift;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.MailReport;
import com.tds.dao.CallerIDDAO;
import com.tds.dao.EmailSMSDAO;
import com.tds.dao.MessageDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.ZoneDAO;
import com.tds.dao.docLib.DocumentDAO;
import com.tds.dao.docLib.DocumentMaterDAO;
import com.common.util.CheckZone;
import com.tds.process.Email;
import com.tds.process.QueueProcess;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.DocumentBO;
import com.tds.tdsBO.FleetBO;
import com.tds.tdsBO.OpenRequestFieldOrderBO;
import com.tds.tdsBO.QueueBean;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.tds.tdsBO.VoucherBO;
import com.tds.tdsBO.ZoneFlagBean;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
/**
 * Servlet simplementation class DashBoard
 */
public class SystemSetupAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SystemSetupAjax() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String eventParam = "";
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if((AdminRegistrationBO)request.getSession().getAttribute("user")!=null)
		{
			if(eventParam.equalsIgnoreCase("StartOrStopTimer")) {
				StartOrStopTimer(request, response);
			} else if(eventParam.equalsIgnoreCase("reloadZones")) {
				reloadZones(request, response);
			} else if(eventParam.equalsIgnoreCase("reloadCab")) {
				reloadCabs(request, response);
			} else if(eventParam.equalsIgnoreCase("deleteAdjacentZones")) {
				deleteAdjacentZones(request, response);
			} else if(eventParam.equalsIgnoreCase("zoneDetails")) {
				zoneDetails(request,response);
			} else if(eventParam.equalsIgnoreCase("deleteWrapper")) {
				deleteWrapper(request,response);
			} else if(eventParam.equalsIgnoreCase("deleteIP")){
				deleteIP(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteDocumentType")){
				deleteDocumentType(request,response);
			}else if(eventParam.equalsIgnoreCase("getseverity")){
				documentSeverity(request,response);
			}else if(eventParam.equalsIgnoreCase("changeAssociationCode")){
				changeAssociationCode(request,response);
			}else if(eventParam.equalsIgnoreCase("sendMessage")){
				sendMessage(request,response);
			} else if(eventParam.equalsIgnoreCase("getDocumentId")){
				getDocumentId(request,response);
			}else if(eventParam.equalsIgnoreCase("archiveDocument")){
				archiveDocument(request,response);
			} else if(eventParam.equalsIgnoreCase("deleteDocument")){
				deleteDocument(request,response);
			}  else if(eventParam.equalsIgnoreCase("giveFleetAccess")){
				giveFleetAccess(request,response);
			}  else if(eventParam.equalsIgnoreCase("changeFleet")){
				changeFleet(request,response);
			}  else if(eventParam.equalsIgnoreCase("deleteFleet")){
				deleteFleet(request,response);
			}else if(eventParam.equalsIgnoreCase("editDocumentType")){
				editDocumentType(request,response);
			}else if(eventParam.equalsIgnoreCase("changeDriverFleet")){
				changeDriverFleet(request,response);
			} else if(eventParam.equalsIgnoreCase("changeCabFleet")){
				changeCabFleet(request,response);
			}else if(eventParam.equalsIgnoreCase("driverShiftDetails")){
				driverShiftDetails(request,response);
			}else if(eventParam.equalsIgnoreCase("getMeterTypes")){
				getMeterTypes(request,response);
			}else if(eventParam.equalsIgnoreCase("getFlagsForCompany")){
				getFlagsForCompany(request,response);
			}else if(eventParam.equalsIgnoreCase("getFlagsForZone")){
				getFlagsForZone(request,response);
			}else if(eventParam.equalsIgnoreCase("reviewuploadfilemapping")){
				reviewuploadfilemapping(request,response);
			}else if(eventParam.equalsIgnoreCase("calleriggenflags")){
				calleriggenflags(request,response);
			}else if(eventParam.equalsIgnoreCase("meterriggenflags")){
				meterriggenflags(request,response);
			}else if(eventParam.equalsIgnoreCase("deletereviewuploadfilemapping")){
				deletereViewUploadFileMapping(request,response);
			}else if(eventParam.equalsIgnoreCase("getMessageList")){
				getMessagelist(request,response);
			}else if(eventParam.equalsIgnoreCase("getDriverList")){
				getDriverList(request,response);
			}else if(eventParam.equalsIgnoreCase("getCCDetails")){
				getCCDetails(request,response);
			}else if(eventParam.equalsIgnoreCase("getAdvanceTime")){
				getAdvanceTime(request,response);
			}else if(eventParam.equalsIgnoreCase("getZoneCharges")){
				getZoneCharges(request,response,getServletConfig());
			}else if(eventParam.equalsIgnoreCase("sendMail")){
				sendMail(request,response);
			}else if(eventParam.equalsIgnoreCase("checkVoucher")){
				checkVoucher(request,response);
			}else if(eventParam.equalsIgnoreCase("driverZoneHistory")){
				driverZoneHistory(request,response);
			}

		}
	}
	private void getAdvanceTime(HttpServletRequest request, HttpServletResponse response) {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String assocode=adminBO.getAssociateCode();
		String mAssocode=adminBO.getMasterAssociateCode();
		//System.out.println("Assocode :" + assocode);
		//System.out.println("mAssocode : " + mAssocode);
	}

	private void getDriverList(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String assocode=adminBO.getAssociateCode();
		//System.out.println("Current user is-->"  + assocode );
		String driverId=request.getParameter("driverId")!=null?request.getParameter("driverId"):null;
		ArrayList<MessageBeen> driverList=MessageDAO.getDriverList(adminBO.getAssociateCode(),request.getParameter("msgId"),driverId);
		JSONArray array=new JSONArray();
		try{
			for(int i=0;i<driverList.size();i++)
			{
				JSONObject obj=new JSONObject();
				obj.put("driverId", driverList.get(i).getDriverId());
				obj.put("msgId", driverList.get(i).getMessageId());
				obj.put("sendTime", driverList.get(i).getSentDate());
				obj.put("resTime", driverList.get(i).getResDate());
				array.put(obj);
			}
		}catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	private void sendMail(HttpServletRequest request,HttpServletResponse response) throws SecurityException,IOException{
		//System.out.println("Mail Id & URL--->"+request.getParameter("mailId")+" "+request.getRequestURL().toString());
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("mailId")!=null && !request.getParameter("mailId").equals("") && request.getParameter("tripId")!=null && !request.getParameter("tripId").equals("")){
			String mailId=request.getParameter("mailId");
			ArrayList<String> emails=EmailSMSDAO.emailUsers(adminBO.getMasterAssociateCode());
			String url1="http://www.getacabdemo.com";
			if(request.getServerName().contains("getacabdemo.com")){
				url1="http://www.getacabdemo.com";
			} else {
				url1="https://www.gacdispatch.com";
			}
			MailReport mailPass = new MailReport(url1, request.getParameter("tripId"), mailId, adminBO.getTimeZone(), emails, "jobMail" , adminBO.getMasterAssociateCode(),1,"");
			mailPass.start();
		}
	}
	private void getMessagelist(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String fromDate=request.getParameter("fromDate")!=null?request.getParameter("fromDate"):null;
		String toDate=request.getParameter("toDate")!=null?request.getParameter("toDate"):null;
		ArrayList<MessageBeen> msgList=MessageDAO.getMessagelist(adminBO.getAssociateCode(),adminBO.getUname(),fromDate,toDate,adminBO.getTimeZone());
		JSONArray array=new JSONArray();
		try {
			for (int i = 0; i < msgList.size(); i++) {
				JSONObject obj = new JSONObject();
				obj.put("msgId", msgList.get(i).getMessageId());
				obj.put("sent", msgList.get(i).getSentDate());
				obj.put("uName", msgList.get(i).getUserName());
				obj.put("msg", msgList.get(i).getMessage());
				obj.put("drId", msgList.get(i).getDriverId());
				array.put(obj);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	private void deletereViewUploadFileMapping(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException,Exception  {
		String vendor=request.getParameter("vendorid")==null?"":request.getParameter("vendorid");
		if(vendor.equalsIgnoreCase("")){
			response.getWriter().write("error");
			return;
		}else{
			int result = SystemPropertiesDAO.deleteORFieldsPosition(vendor);
			if(result>0){
				response.getWriter().write("Successfully Deleted");
			}else{
				response.getWriter().write("Not Successfully deleted");
			}
		}
	}

	private void calleriggenflags(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException,Exception {
		AdminRegistrationBO adminBO=(AdminRegistrationBO)request.getSession().getAttribute("user");
		CallerIDBean callerId=new CallerIDBean();
		callerId=CallerIDDAO.getUserIdPassword(adminBO);
		JSONArray flagList = new JSONArray();
		JSONObject flagDetails = new JSONObject();
		flagDetails.put("userid", callerId.getUserID());
		flagDetails.put("password",callerId.getPassword());
		flagList.put(flagDetails);
		response.getWriter().write(flagList.toString());

	}

	private void meterriggenflags(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException,Exception {
		AdminRegistrationBO adminBO=(AdminRegistrationBO)request.getSession().getAttribute("user");
		String callerId="";
		callerId=CallerIDDAO.getPasswordId(adminBO.getMasterAssociateCode());
		JSONArray flagList = new JSONArray();
		JSONObject flagDetails = new JSONObject();
		flagDetails.put("password",callerId);
		flagList.put(flagDetails);
		response.getWriter().write(flagList.toString());

	}


	private void reviewuploadfilemapping(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException,Exception {
		// TODO Auto-generated method stub
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user"); 
		ArrayList<OpenRequestFieldOrderBO> fieldList=SystemPropertiesDAO.getORFieldsOrderForjsp(adminBO.getAssociateCode(),request.getParameter("vendor")==null?"":request.getParameter("vendor"));

		//System.out.println(fieldList.get(1).getVendorName().toString());
		JSONArray flagList = new JSONArray();
		for(int i=0;i<fieldList.size();i++){
			JSONObject flagDetails = new JSONObject();
			try {
				flagDetails.put("vname",(fieldList.get(i).getVendorName()!=null)?fieldList.get(i).getVendorName():"null");
				flagDetails.put("name",(fieldList.get(i).getName()!="")?fieldList.get(i).getName():"null");
				flagDetails.put("ph",(fieldList.get(i).getPhone()!="")?fieldList.get(i).getPhone():"null");
				flagDetails.put("gsa1",(fieldList.get(i).getSadd1()!="")?fieldList.get(i).getSadd1():"null");
				flagDetails.put("gsa2",(fieldList.get(i).getSadd2()!="")?fieldList.get(i).getSadd2():"null");
				flagDetails.put("gea1",(fieldList.get(i).getEadd1()!="")?fieldList.get(i).getEadd1():"null");
				flagDetails.put("gea2",(fieldList.get(i).getEadd2()!="")?fieldList.get(i).getEadd2():"null");
				flagDetails.put("gsc",(fieldList.get(i).getScity()!="")?fieldList.get(i).getScity():"null");
				flagDetails.put("gec",(fieldList.get(i).getEcity()!="")?fieldList.get(i).getEcity():"null");
				flagDetails.put("gsl",(fieldList.get(i).getSlat()!="")?fieldList.get(i).getSlat():"null");

				flagDetails.put("gslo",(fieldList.get(i).getSlong()!=null)?fieldList.get(i).getSlong():"null");
				flagDetails.put("gedl",(fieldList.get(i).getEdlatitude()!=null)?fieldList.get(i).getEdlatitude():"null");
				flagDetails.put("gedlo",(fieldList.get(i).getEdlongitude()!=null)?fieldList.get(i).getEdlongitude():"null");
				flagDetails.put("gs",(fieldList.get(i).getShrs()!=null)?fieldList.get(i).getShrs():"null");
				flagDetails.put("gsd",(fieldList.get(i).getSdate()!=null)?fieldList.get(i).getSdate():"null");
				flagDetails.put("gpt",(fieldList.get(i).getPaytype()!=null)?fieldList.get(i).getPaytype():"null");
				flagDetails.put("gac",(fieldList.get(i).getAcct()!=null)?fieldList.get(i).getAcct():"null");
				flagDetails.put("ga",(fieldList.get(i).getAmt()!=null)?fieldList.get(i).getAmt():"null");
				flagDetails.put("goc",(fieldList.get(i).getOperatorComments()!=null)?fieldList.get(i).getOperatorComments():"null");
				flagDetails.put("god2",(fieldList.get(i).getDispatchComments()!=null)?fieldList.get(i).getDispatchComments():"null");

				flagDetails.put("vendor",(fieldList.get(i).getVendor()!=null)?fieldList.get(i).getVendor():"null");
				flagDetails.put("getrout",(fieldList.get(i).getRouteNumber()!=null)?fieldList.get(i).getRouteNumber():"null");
				flagDetails.put("getshare",(fieldList.get(i).getSharedRide()!=null)?fieldList.get(i).getSharedRide():"null");
				flagList.put(flagDetails);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//request.setAttribute("fieldsList",orfList);
		//response.sendRedirect("mainNew.jsp?screen=/SystemSetup/reviewUploadFileMapping.jsp&module=systemsetupView");
		response.getWriter().write(flagList.toString());


	}

	public void StartOrStopTimer(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		long queueInterval = 10 * 1000  ;

		if(request.getParameter("btnTimer")!=null ){
			Timer jobTimer = (Timer)this.getServletContext().getAttribute(adminBO.getAssociateCode()+"Timer");
			QueueProcess queueProcess = (QueueProcess)this.getServletContext().getAttribute(adminBO.getAssociateCode()+"Process");
			if(queueProcess!= null && queueProcess.isDispatchingJobs()){
				jobTimer.cancel();
				queueProcess = null;
				jobTimer = null;

				this.getServletConfig().getServletContext().removeAttribute(adminBO.getAssociateCode()+"Timer");
				this.getServletConfig().getServletContext().removeAttribute(adminBO.getAssociateCode()+"Process");
				this.getServletContext().removeAttribute(adminBO.getAssociateCode()+"ManualStop");

				QueueProcess performQueue = new QueueProcess(adminBO.getAssociateCode(),adminBO.getMasterAssociateCode(),getServletConfig());
				performQueue.setDispatchingJobs(false);
				Timer q_timer = new Timer();	 
				q_timer.schedule(performQueue,(1*1000) , queueInterval); 		
				//System.out.println("Job stopped for "+adminBO.getAssociateCode() + " with dispatch switch = " + performQueue.isDispatchingJobs());

				this.getServletContext().setAttribute(adminBO.getAssociateCode()+"Timer",null);
				this.getServletContext().setAttribute(adminBO.getAssociateCode()+"Process",null);
				this.getServletContext().setAttribute(adminBO.getAssociateCode()+"ManualStop","Yes");
				response.getWriter().write("Timer Stopped Successfully");

			}else if(queueProcess!= null && !queueProcess.isDispatchingJobs()){
				if(jobTimer!=null){
					jobTimer.cancel();
				}
				jobTimer = null;
				queueProcess = null;

				this.getServletConfig().getServletContext().removeAttribute(adminBO.getAssociateCode()+"Timer");
				this.getServletConfig().getServletContext().removeAttribute(adminBO.getAssociateCode()+"Process");
				this.getServletContext().removeAttribute(adminBO.getAssociateCode()+"ManualStop");

				QueueProcess performQueue = new QueueProcess(adminBO.getAssociateCode(),adminBO.getMasterAssociateCode(),getServletConfig());
				performQueue.setDispatchingJobs(true);

				Timer q_timer = new Timer();	 
				q_timer.schedule(performQueue,(1*1000) , queueInterval); 		
				System.out.println("Job started for "+adminBO.getAssociateCode() + " with dispatch switch = " + performQueue.isDispatchingJobs());

				this.getServletContext().setAttribute(adminBO.getAssociateCode()+"Timer",q_timer);
				this.getServletContext().setAttribute(adminBO.getAssociateCode()+"Process",performQueue);
				response.getWriter().write("Timer Started Successfully");
			}else if(queueProcess== null){
				//System.out.println("Queue process Null");
				Timer timerOld = (Timer)this.getServletConfig().getServletContext().getAttribute(adminBO.getAssociateCode()+"Timer");
				if(timerOld!=null){
					timerOld.cancel();
				}
				timerOld = null;

				this.getServletConfig().getServletContext().removeAttribute(adminBO.getAssociateCode()+"Timer");
				this.getServletConfig().getServletContext().removeAttribute(adminBO.getAssociateCode()+"Process");
				this.getServletContext().removeAttribute(adminBO.getAssociateCode()+"ManualStop");

				String dispatchType = ServiceRequestDAO.getDispatchType(adminBO.getAssociateCode());
				if(!dispatchType.equals("")){
					QueueProcess queueProcessNew = new QueueProcess(adminBO.getAssociateCode(),adminBO.getMasterAssociateCode(), this);
					if(dispatchType.equalsIgnoreCase("a")|| dispatchType.equalsIgnoreCase("q")|| dispatchType.equalsIgnoreCase("m")){
						queueProcessNew.setDispatchingJobs(true);
						//System.out.println("Job started for "+adminBO.getAssociateCode() + "with dispatch switch = " + queueProcessNew.isDispatchingJobs());
						Timer timer = new Timer();
						timer.schedule(queueProcessNew,0, 10000);
						this.getServletContext().setAttribute(adminBO.getAssociateCode()+"Timer",timer);
						this.getServletContext().setAttribute(adminBO.getAssociateCode()+"Process",queueProcessNew);
						response.getWriter().write("Timer Started Successfully");
						return;
					}else if(!dispatchType.equalsIgnoreCase("x")){
						queueProcessNew.setDispatchingJobs(false);
						//System.out.println("Not starting job process - Dispatch type:"+dispatchType);
					} else {
						//System.out.println("Not starting job process - Dispatch type:"+dispatchType);
					}
					response.getWriter().write("Timer starting/stopping is Unsuccessful");
				} else {
					response.getWriter().write("Timer starting/stopping is Unsuccessful");	            		
				}
			} else {
				response.getWriter().write("Timer starting/stopping is Unsuccessful");
			}
		}
	}


	public void reloadZones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("btnreloadZones")!=null){
			SystemUtils.reloadZones(this, adminBO.getAssociateCode());
			response.getWriter().write("Reloaded Successfully");
		}
	}
	public void reloadCabs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("btnreloadCab")!=null){
			SystemUtils.reloadCompanyFlags(this, adminBO.getMasterAssociateCode());
			response.getWriter().write("Reloaded Successfully");
		}
	}
	public void deleteAdjacentZones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if (request.getParameter("delete")!=null){
			String serialNum=request.getParameter("serialNum");
			int delete=SystemPropertiesDAO.deleteAdjacentZones(serialNum,adminBO);
			if(delete==1){
				response.getWriter().write("Deleted Successfully");
			} else {
				response.getWriter().write("Delete Unsuccessful");
			}

		}
	}

	public void zoneDetails(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<QueueCoordinatesBO> zone_list =SystemPropertiesDAO.zoneDetails(adminBO);
		StringBuffer buffer = new StringBuffer();
		for(int i=0;i<zone_list.size();i++){
			buffer.append(""+zone_list.get(i).getQueueId()+"^^^");
		}
		response.getWriter().write(buffer.toString());
	}
	public void deleteWrapper(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		if(request.getParameter("delete")!=null){
			String driverId=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
			int result=SystemPropertiesDAO.deleteWrapperbyDriver(adminBO,driverId);
			if(result==1){
				response.getWriter().write("Deleted Successfully");
			} else {
				response.getWriter().write("Delete Unsuccessful");
			}

		}
	}
	public void deleteIP(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("delete")!=null){
			String ipAddress=(request.getParameter("ipAddress")==null?"":request.getParameter("ipAddress"));
			int result=SecurityDAO.deleteIPAddress(ipAddress,adminBO);
			if(result==1){
				response.getWriter().write("Deleted Successfully");
			} else {
				response.getWriter().write("Delete Unsuccessful");
			}
		}


	}


	public void deleteDocumentType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("delete")!=null){
			String serialNumber=(request.getParameter("serialNumber")==null?"":request.getParameter("serialNumber"));
			int result=SecurityDAO.deleteDocumentType(serialNumber,adminBO);
			if(result==1){
				response.getWriter().write("Deleted Successfully");
			} else {
				response.getWriter().write("Delete Unsuccessful");
			}
		}

	}
	public void documentSeverity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		DocumentMaterDAO dmDao = new DocumentMaterDAO();
		String docType=(request.getParameter("docType")==null?"" : request.getParameter("docType"));
		String severity=dmDao.getServerity(adminBO.getAssociateCode(), docType);

		if(severity!=null && !severity.equals("")){
			response.getWriter().write(severity);
		}else {
			response.getWriter().write("");
		}
	}


	public void changeAssociationCode(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String assoccode=request.getParameter("assoccode");
		int result=SecurityDAO.getFleetAccess(adminBO,assoccode);
		if(result>0){
			adminBO.setAssociateCode(assoccode);
			HttpSession session = null;
			session.setAttribute("user", adminBO);
		}
	}

	public void sendMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String message=request.getParameter("message");
		String subject=request.getParameter("subject");
		String fromAddress="";
		String emailAddress="";
		String smtpAddress="";
		ArrayList<String> emails=new ArrayList<String>();
		emails=EmailSMSDAO.emailUsers(adminBO.getAssociateCode());
		String username=emails.get(0);
		String password=emails.get(1);
		String assoccode=emails.get(2);
		//String host=emails.get(3);
		String port=emails.get(4);
		smtpAddress=emails.get(5);

		//EmailSMSDAO emailDAO =new EmailSMSDAO();
		if(request.getParameter("drivers")!=null && !request.getParameter("drivers").equals("")){
			String drivers=request.getParameter("drivers");
			String[] driver=drivers.split(";");
			for(int i=0;i<driver.length;i++){
				emailAddress=EmailSMSDAO.getEmailAddresses(driver[i],assoccode);
				//fromAddress=emailAddress;
				//System.out.println("fromAddress-->"+fromAddress);
				if(!fromAddress.equals("")){
					fromAddress=fromAddress+";"+emailAddress;
				}else{
					fromAddress=emailAddress;
				}
			}
		}
		Email otherProviderSMS = new Email(smtpAddress,username,password,username,port,"smtp");

		try{
			//String[] emails = new String[""];
			otherProviderSMS.sendMail(fromAddress,subject, message);
		} catch (Exception e ){
			System.out.println(e.toString());
		}

	}

	public void getDocumentId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		//   HttpSession session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<DocBean> documentIds= new ArrayList<DocBean>();
		String driverId=request.getParameter("driverId")==null?"":request.getParameter("driverId");
		String cabNumber=request.getParameter("cabNumber")==null?"":request.getParameter("cabNumber");
		if(!driverId.equals("")){
			documentIds=DocumentDAO.getDocumentId(adminBO.getAssociateCode(),driverId,1);
		}else if(!cabNumber.equals("")){
			documentIds=DocumentDAO.getDocumentId(adminBO.getAssociateCode(),cabNumber,2);
		}
		String docId="";
		if(documentIds.size()==0 ){
			response.getWriter().write("<font color=\"white\">No Uploaded Files Found</font>");
		} else {
			response.getWriter().write("<table>");
			response.getWriter().write("<tr><td>");
			response.getWriter().write("<table id=\"logs\" width=\"440\"><tr><td><font color=\"white\">DocumentID</font></td><td><font color=\"white\">Document Name</font></td><td><font color=\"white\">View</font></td>"+"</td></tr>");
			for(int i=0;i<documentIds.size();i++){
				response.getWriter().write("<tr><td  ><font color=\"white\">"+documentIds.get(i).getDocId()+"</font></td><td  ><font color=\"white\">"+documentIds.get(i).getDocName()+"</font></td><td  ><input type=\"button\" name=\"button\" value=\"View\" onclick=\"loadFile('"+documentIds.get(i).getDocId()+"');\"</input></td></tr>");
			}
			response.getWriter().write("</td>");
			response.getWriter().write("</table>");
			response.getWriter().write("<td> <div> <iframe src=\"\" id=\"docLoaderImage\" name=\"docLoaderImage\" width=\"100%\" height=\"100px\"></iframe></div>");
			response.getWriter().write("</td></tr>");
			response.getWriter().write("</table>");

		}

		response.getWriter().write(docId);
	}

	public void archiveDocument(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String docId=request.getParameter("docId");
		int result=DocumentDAO.archiveFile(adminBO.getAssociateCode(), docId);
		String result1=Integer.toString(result);
		response.getWriter().write(result1);
	}
	public void deleteDocument(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String docId=request.getParameter("docId");
		int result=DocumentDAO.deleteFile(adminBO.getAssociateCode(), docId);
		String result1=Integer.toString(result);
		response.getWriter().write(result1);
	}
	public void changeFleet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		String fleet=request.getParameter("fleet");
		HttpSession session = null;
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String masterAssociationCode=adminBO.getMasterAssociateCode();
		int result=SecurityDAO.getFleetAccess(adminBO,fleet);
		if(result==1){
			session=request.getSession();
			//System.out.println("User session "+session.getId());
			adminBO=SecurityDAO.getFleet(adminBO.getUid());
			//   adminBO.setMasterAssociateCode(associationCode);

			adminBO=RegistrationDAO.getUserAvailable(adminBO.getUid(), adminBO.getPassword(), "", "", "");
			if(adminBO.getIsAvailable() == 1) {
				CompanySystemProperties cspBO=SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());
				adminBO.setDefaultLanguage(cspBO.getDefaultLanguage());
				adminBO.setCountry(cspBO.getCountry()); 
				adminBO.setDispatchBasedOnVehicleOrDriver(cspBO.getDispatchBasedOnDriverOrVehicle());
				adminBO.setCallerId((cspBO.getCallerId()));
				adminBO.setTimeZoneOffet(cspBO.getTimezone());
				adminBO.setORTime(cspBO.getORTime());
				adminBO.setORFormat(cspBO.getORFormat());
				adminBO.setRatePerMile(cspBO.getRatePerMile());
				adminBO.setAssociateCode(fleet);
				adminBO.setMasterAssociateCode(masterAssociationCode);
				adminBO.setCurrencyPrefix(cspBO.getCurrencyPrefix());
				adminBO.setTimeZone(cspBO.getTimeZoneArea());
				adminBO.setMeterHidden(cspBO.isStartmeterHidden());
				adminBO.setFleetDispatch(cspBO.getFleetDispatchSwitch());
				double[] ztb ={0.00,0.00};
				if(adminBO.getUseMaster0rNot()==0){
					ztb = ZoneDAO.getDefaultLocation(adminBO.getMasterAssociateCode());
				}else{
					ztb = ZoneDAO.getDefaultLocation(adminBO.getAssociateCode());

				}
				if(ztb !=null){
					adminBO.setDefaultLati(ztb[0]);
					adminBO.setDefaultLogi(ztb[1]);
				}
				session.setAttribute("user", adminBO);
			}else{
				response.getWriter().write("Changing Fleet is not Successful");
				return;
			}
			//request.setAttribute("screen", screen);
			String value = SystemPropertiesDAO.getParameter(adminBO.getAssociateCode(), "TimeOutOperator");
			if(!value.equals("") && !value.equals("0")){
				session.setMaxInactiveInterval(Integer.parseInt(value)*60);
			}
			if(getServletConfig().getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "ZonesLoaded") == null){
				//System.out.println("Loading zone for the first time for company" + adminBO.getMasterAssociateCode());
				SystemUtils.reloadZones(this, adminBO.getMasterAssociateCode());
			}

			response.getWriter().write("Changed Fleet Successfully");

		}else{
			response.getWriter().write("Doesn't have the access"); 
		}
	}

	public void giveFleetAccess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		HttpSession session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>();
		ArrayList<FleetBO> fleetsForOperator=new ArrayList<FleetBO>();
		String fleetName[]=null;
		String driverId=request.getParameter("driverId");
		if(session.getAttribute("fleetList")!=null) {
			fleetList=(ArrayList)session.getAttribute("fleetList");
		} 
		for(int i=0;i<fleetList.size();i++){
			FleetBO fleetBO=new FleetBO();
			fleetBO.setFleetNumber(request.getParameter("fleetNumber"+i));
			fleetBO.setFleetName(request.getParameter("fleetName"+i));
			fleetsForOperator.add(fleetBO);
		}
		int result=RequestDAO.insertFleet(adminBO,fleetsForOperator,driverId);
		if(result==1){
			response.getWriter().write("1");
		}else{
			response.getWriter().write("0");
		}
	}
	public void deleteFleet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String assoccode=request.getParameter("assoccode");
		int result=SystemPropertiesDAO.deleteFleet(adminBO.getMasterAssociateCode(), assoccode);
		response.getWriter().write(result+"");
	}
	public void editDocumentType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		DocumentBO docBO=new DocumentBO();
		docBO.setSerialNumber(request.getParameter("serialNumber"));
		docBO.setDocumentType(request.getParameter("docType"));
		docBO.setShortName(request.getParameter("shortName"));
		docBO.setServerityType(request.getParameter("severity"));
		docBO.setDocumentBelongsTo(request.getParameter("belongs"));
		docBO.setCheckForExpiry(request.getParameter("severity").equals("Critical")?1:Integer.parseInt(request.getParameter("checkForExpiry")));
		int result=SystemPropertiesDAO.updateDocumentTypes(docBO, adminBO);
		response.getWriter().write(result+"");
	}public void changeDriverFleet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String fleet=request.getParameter("fleet");
		String driverId=request.getParameter("driverId");
		int result=SecurityDAO.updateDriverFleet(driverId,fleet,adminBO);
		response.getWriter().write(result+"");
	}
	public void changeCabFleet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String fleet=request.getParameter("fleet");
		String cabNumber=request.getParameter("cabNumber");
		int result=SecurityDAO.updateCabFleet(cabNumber,fleet,adminBO);
		response.getWriter().write(result+"");
	}
	public void driverShiftDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<OperatorShift> shiftList= new ArrayList<OperatorShift>();
		String driverId=request.getParameter("dId")==null?"":request.getParameter("dId");
		String fromDate=request.getParameter("fDate")==null?"":request.getParameter("fDate");
		String toDate=request.getParameter("tDate")==null?"":request.getParameter("tDate");
		shiftList=SystemPropertiesDAO.driverShiftDetails(adminBO,driverId,fromDate,toDate);
		if(shiftList.size()>0){
			JSONArray driverShiftList = new JSONArray();
			for(int i=0;i<shiftList.size();i++){
				JSONObject shiftDetails = new JSONObject();
				try {
					shiftDetails.put("dId", shiftList.get(i).getDriverId());
					shiftDetails.put("key", shiftList.get(i).getKey());
					shiftDetails.put("getOpenTime",shiftList.get(i).getOpenTime());
					shiftDetails.put("getCloseTime", shiftList.get(i).getCloseTime());
					driverShiftList.put(shiftDetails);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			response.getWriter().write(driverShiftList.toString());
		}else{
			response.getWriter().write("");
		}

	}
	public void getMeterTypes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<MeterType> meters= SystemPropertiesDAO.getMeterValues(adminBO.getMasterAssociateCode());
		if(meters.size()>0){
			JSONArray metertList = new JSONArray();
			for(int i=0;i<meters.size();i++){
				JSONObject meterDetails = new JSONObject();
				try {
					meterDetails.put("MN", meters.get(i).getMeterName());
					meterDetails.put("RMl", meters.get(i).getRatePerMile());
					meterDetails.put("RMn",meters.get(i).getRatePerMin());
					meterDetails.put("SA", meters.get(i).getStartAmt());
					meterDetails.put("MS", meters.get(i).getMinSpeed());
					meterDetails.put("MK", meters.get(i).getKey());
					meterDetails.put("DF", meters.get(i).getDefaultMeter());
					meterDetails.put("D1", meters.get(i).getDis1());
					meterDetails.put("D2", meters.get(i).getDis2());
					meterDetails.put("D3", meters.get(i).getDis3());
					meterDetails.put("R1", meters.get(i).getRate1());
					meterDetails.put("R2", meters.get(i).getRate2());
					meterDetails.put("R3", meters.get(i).getRate3());

					metertList.put(meterDetails);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			response.getWriter().write(metertList.toString());
		}else{
			response.getWriter().write("");
		}
	}

	public void getFlagsForCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<DriverVehicleBean> dvList=new ArrayList<DriverVehicleBean>();
		HashMap<String, DriverVehicleBean> companyFlags =UtilityDAO.getCmpyVehicleFlag(adminBO.getMasterAssociateCode());
		Set hashMapEntries = companyFlags.entrySet();
		Iterator it = hashMapEntries.iterator();
		while(it.hasNext()){
			Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
			dvList.add(companyFlag.getValue());
		}
		JSONArray flagList = new JSONArray();
		for(int i=0;i<dvList.size();i++){
			JSONObject flagDetails = new JSONObject();
			try {
				flagDetails.put("LD", dvList.get(i).getLongDesc());
				flagDetails.put("SW", dvList.get(i).getDriverVehicleSW());
				flagDetails.put("K",dvList.get(i).getKey());
				flagDetails.put("SD", dvList.get(i).getShortDesc());
				flagDetails.put("GI", dvList.get(i).getGroupId()==0?"":dvList.get(i).getGroupId());
				flagList.put(flagDetails);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.getWriter().write(flagList.toString());
	}

	public void getFlagsForZone(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<ZoneFlagBean> znList=new ArrayList<ZoneFlagBean>();
		HashMap<String,ZoneFlagBean> zoneFlags =UtilityDAO.getZoneFlag(adminBO.getAssociateCode());
		//al_list =  RegistrationDAO.getZoneFlag(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode());

		Set hashMapEntries = zoneFlags.entrySet();
		Iterator it = hashMapEntries.iterator();
		while(it.hasNext()){
			Map.Entry<String, ZoneFlagBean> zoneFlag=(Map.Entry<String, ZoneFlagBean>)it.next();
			znList.add(zoneFlag.getValue());
		}
		JSONArray flagList = new JSONArray();
		for(int i=0;i<znList.size();i++){
			JSONObject flagDetails = new JSONObject();
			try {
				flagDetails.put("FL", znList.get(i).getFlag1());
				flagDetails.put("SD", znList.get(i).getFlag1_value());
				//System.out.println("SD"+znList.get(i).getFlag1());
				flagDetails.put("LD",znList.get(i).getFlag1_lng_desc());
				flagList.put(flagDetails);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.getWriter().write(flagList.toString());
	}

	public void getCCDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		CompanyMasterBO cmpBean = SystemPropertiesDAO.getCCProperties(adminBO.getMasterAssociateCode());
		JSONArray creditDetail = new JSONArray();
		JSONObject ccDetails = new JSONObject();
		if(cmpBean!=null && !cmpBean.getCcUsername().equals("")){
			try{
				ccDetails.put("UN", cmpBean.getCcUsername());
				ccDetails.put("PW", cmpBean.getCcPassword());
				ccDetails.put("EM", cmpBean.getEmail());
				ccDetails.put("CN", cmpBean.getCname());
				ccDetails.put("PR", cmpBean.getCcProvider());
				ccDetails.put("SI", cmpBean.getSiteId());
				ccDetails.put("PI", cmpBean.getPriceId());
				ccDetails.put("K", cmpBean.getKey());
				creditDetail.put(ccDetails);
			} catch(Exception e){
				e.printStackTrace();
			}
		}
		response.getWriter().write(creditDetail.toString());
	}
	public void getZoneCharges(HttpServletRequest request, HttpServletResponse response,ServletConfig servletConf) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		double lat=Double.parseDouble(request.getParameter("sLat"));
		double lon=Double.parseDouble(request.getParameter("sLon"));
		double eLat=Double.parseDouble(request.getParameter("eLat"));
		double eLon=Double.parseDouble(request.getParameter("eLon"));
		String amount="";
		ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) servletConf.getServletContext().getAttribute((adminBO.getMasterAssociateCode() +"Zones"));
		String queueId = CheckZone.checkZone(allZonesForCompany, null, lat,lon);
		String eQueueId = CheckZone.checkZone(allZonesForCompany, null, eLat,eLon);
		JSONArray array = new JSONArray();
		if(!queueId.equals("") && !eQueueId.equals("")){
			amount=ZoneDAO.getZoneCharges(adminBO.getMasterAssociateCode(),queueId,eQueueId);
		}
		JSONObject address = new JSONObject();
		try {
			address.put("AM", amount.equals("")?"0.00":amount);
			address.put("SZ", queueId.equals("")?"No Zone":queueId);
			address.put("EZ", eQueueId.equals("")?"No Zone":eQueueId);
			array.put(address);			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}


	private void checkVoucher(HttpServletRequest request,HttpServletResponse response) throws SecurityException,IOException{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String voucherNumber= (request.getParameter("voucherNumber")!=null && !request.getParameter("voucherNumber").equals(""))?request.getParameter("voucherNumber"):"";
		ArrayList<VoucherBO> al_list = RequestDAO.getComments(adminBO,voucherNumber);
		String resposeString="";
		/*int result = 0;
		if(!voucherNumber.equals("")){
			 result = RegistrationDAO.checkVoucherNo(voucherNumber);
		}*/
		//System.out.println("result:"+result);
		if(al_list.size()>0){
			try {
				if(al_list.get(0).getTo_date().startsWith("-") || al_list.get(0).getVstatus().equals("0")){
					resposeString = "Voucher:"+voucherNumber+" was expired or not active";
				} else {	
					resposeString = "ok";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			resposeString = "Sorry, Voucher:"+voucherNumber+" is not Found";
		}
		//System.out.println("response:"+resposeString);
		response.getWriter().write(resposeString);
	}
	private void driverZoneHistory(HttpServletRequest request, HttpServletResponse response) throws IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String zoneId = request.getParameter("zoneId");
		String date = request.getParameter("hisDate");
		String time = request.getParameter("hisTime");
		ArrayList<QueueBean> resultantList = ZoneDAO.getDriverPositionByTime(zoneId,date,time,adminBO);
		JSONArray array = new JSONArray();
		JSONObject address = new JSONObject();
		if(resultantList!=null && resultantList.size()>0){
			for(int i=0;i<resultantList.size();i++){
				try {
					address.put("ZI", resultantList.get(i).getQueueDescription());
					address.put("DId", resultantList.get(i).getDriverId());
					address.put("CNo", resultantList.get(i).getVehicleNo());
					address.put("T", resultantList.get(i).getDq_LoginTime());
					address.put("P", resultantList.get(i).getFlg());
					array.put(address);
				}catch (JSONException e) {
					e.printStackTrace();
				}
			} 
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

}

