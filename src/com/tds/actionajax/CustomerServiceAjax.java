package com.tds.actionajax;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Category;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.charges.dao.ChargesDAO;
import com.tds.controller.TDSController;
import com.tds.dao.CreditCardDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.ZoneDAO;
import com.tds.payment.PaymentCatgories;
import com.tds.payment.PaymentGatewaySlimCD;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class OpenRequestAjax
 */
public class CustomerServiceAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;


	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ CustomerServiceAjax.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerServiceAjax() {

		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String eventParam = "";

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if((AdminRegistrationBO)request.getSession(false).getAttribute("user")!=null)
		{
			if(eventParam.equalsIgnoreCase("creditCardRegistration")) {
				creditCardRegistration(request,response);
			}else if(eventParam.equalsIgnoreCase("creditCardDetails")) {
				creditCardDetails(request,response);
			}else if(eventParam.equalsIgnoreCase("paymentFromCreditCard")) {
				paymentFromCreditCard(request,response);
			}else if(eventParam.equalsIgnoreCase("checkCustomerKey")) {
				checkCustomerKey(request,response);
			}else if(eventParam.equalsIgnoreCase("creditCardUpdate")) {
				creditCardUpdate(request,response);
			}else if(eventParam.equalsIgnoreCase("creditCardDelete")) {
				creditCardDelete(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteChargeType")) {
				deleteChargeType(request,response);
			}


		}

	}

	public void creditCardDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(adminBO.getMasterAssociateCode());
		String infoKey="";
		int result=0;
		StringBuffer buffer=new StringBuffer();
		String customerKey=request.getParameter("customerKey");
		String customerName=request.getParameter("customerName");
		String creditCardNumber=request.getParameter("cardNumber");
		String nameOnCard=request.getParameter("nameOnCard");
		String expiryDate=request.getParameter("expiryDate");
		String cvvCode=request.getParameter("cvvCode");
		String cardNumberToStore="";
		CompanyMasterBO companyBean =new CompanyMasterBO();
		companyBean=SystemPropertiesDAO.getCCProperties(((AdminRegistrationBO)request.getSession().getAttribute("user")).getMasterAssociateCode());
		if(creditCardNumber.length()>4){
			cardNumberToStore=creditCardNumber.substring(creditCardNumber.length()-4);
			cardNumberToStore="************"+cardNumberToStore;
		}
		//System.out.println("CmpBean.getccProvider -----> " + cmpBean.getCcProvider());
		if(cmpBean.getCcProvider()==2){
			PaymentProcessBean processBean = new PaymentProcessBean();
			processBean.setB_card_full_no(creditCardNumber);
			processBean.setB_cardHolderName(nameOnCard);
			processBean.setCvv(cvvCode);
			processBean.setB_amount("0.00");
			processBean.setB_tip_amt("0.00");
//			System.out.println("New Code");
			processBean.setB_cardExpiryDate(expiryDate);
			PaymentGatewaySlimCD.makeProcess(processBean, companyBean, PaymentCatgories.Load);
			if(processBean.isAuth_status()){
				infoKey = processBean.getB_trans_id();
			}
		} else {
			String responseFromXML = "";
			HttpURLConnection conn;
			PrintStream ps;
			InputStream is;
			StringBuffer b;
			BufferedReader r;

			// check response for status
			//while (response.indexOf("<Status StatusCode = \"Retry\"/>") > 0)
			//{

			System.setProperty("http.keepAlive", "true");
			try {
				URL url1 = new URL("https://pmn.payment-gate.net/paygate/ws/recurring.asmx/ManageCreditCardInfo");
				conn = (HttpURLConnection) url1.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Host", "pmn.payment-gate.net");

				conn.setRequestProperty("Connection","Keep-Alive");
				conn.setRequestProperty("Cache-Control","no-cache");
				conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

				//xml_body ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> " ;
				String encodedData = "UserName=" + URLEncoder.encode(companyBean.getCcUsername(), "UTF-8")+
						"&Password="+companyBean.getCcPassword()+
						"&TransType=ADD"+
						"&Vendor=6273"+
						"&CustomerKey="+customerKey+
						"&CardInfoKey="+
						"&CcAccountNum="+creditCardNumber+
						"&CcExpDate="+expiryDate+
						"&CcNameOnCard="+nameOnCard+
						"&CcStreet="+
						"&CCZip="+
						"&ExtData=";
				//System.out.println("encoded date--->"+encodedData);
				conn.setRequestProperty("Content-Length",""+encodedData.length());

				ps = new PrintStream(conn.getOutputStream());
				ps.write(encodedData.getBytes());
				ps.flush();
				ps.close();
				conn.connect();
				if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
					is = (InputStream) conn.getInputStream();
					// read reply
					b = new StringBuffer();
					r = new BufferedReader(new InputStreamReader(is));
					String line;
					while ((line = r.readLine()) != null)
						b.append(line);
					// System.out.println("Reply From Site:" + b.toString());
					is.close();
					responseFromXML = b.toString();
					//System.out.println("REsponse for creditCard registration:"+ responseFromXML);
					conn.disconnect();
				}
			}

			catch (Exception e) {
				responseFromXML = "GRACIERROR;" + e.getMessage();

			}

			//System.out.println(responseFromXML);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			try{
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				//Document doc = dBuilder.parse(response);
				Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(responseFromXML.getBytes("utf-8"))));
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("RecurringResult");
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						customerKey=getTagValue("CustomerKey",eElement);
						infoKey=getTagValue("CcInfoKey", eElement);
						System.out.println("Info Key is : " + infoKey);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if(infoKey!=null && !infoKey.equals("")){
			result=CreditCardDAO.insertCreditCardDetails(adminBO.getAssociateCode(),customerName,cardNumberToStore,nameOnCard,expiryDate,customerKey,infoKey);
			CreditCardDAO.updateInfoKey(adminBO.getMasterAssociateCode(),customerKey,infoKey);
		}
		buffer.append(infoKey);
		response.getWriter().write(buffer.toString());
	}
	public void creditCardRegistration(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		// String encodedData = ""; // user-supplied
		String responseFromXML = "";
		HttpURLConnection conn;
		PrintStream ps;
		InputStream is;
		StringBuffer b;
		BufferedReader r;
		String customerKey="";
		StringBuffer buffer=new StringBuffer();
		String masterKey=request.getParameter("masterKey");
		// check response for status
		//while (response.indexOf("<Status StatusCode = \"Retry\"/>") > 0)
		//{
		CompanyMasterBO companyBean =new CompanyMasterBO();
		companyBean=SystemPropertiesDAO.getCCProperties(((AdminRegistrationBO)request.getSession().getAttribute("user")).getMasterAssociateCode());
		System.setProperty("http.keepAlive", "true");
		try {
			URL url1 = new URL("https://pmn.payment-gate.net/paygate/ws/recurring.asmx/ManageCustomer");
			conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Host", "pmn.payment-gate.net");

			conn.setRequestProperty("Connection","Keep-Alive");
			conn.setRequestProperty("Cache-Control","no-cache");
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");


			String encodedData = "UserName=" + URLEncoder.encode(companyBean.getCcUsername(), "UTF-8")+
			"&Password="+companyBean.getCcPassword()+
			"&TransType=ADD"+
			"&Vendor=6273"+
			"&CustomerKey="+
			"&CustomerID="+
			"&CustomerName="+URLEncoder.encode(request.getParameter("customerName"), "UTF-8")+
			"&FirstName="+
			"&LastName="+
			"&Title=" +
			"&Department="+
			"&Street1="+
			"&Street2="+
			"&Street3="+
			"&City="+
			"&StateID="+
			"&Province="+
			"&Zip="+
			"&CountryID="+
			"&Email="+
			"&DayPhone="+
			"&NightPhone="+
			"&Fax="+
			"&Mobile="+
			"&Status="+
			"&ExtData=";
			conn.setRequestProperty("Content-Length",""+encodedData.length());

			ps = new PrintStream(conn.getOutputStream());
			ps.write(encodedData.getBytes());
			ps.flush();
			ps.close();
			conn.connect();
			if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
				is = (InputStream) conn.getInputStream();
				// read reply
				b = new StringBuffer();
				r = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = r.readLine()) != null)
					b.append(line);
				// System.out.println("Reply From Site:" + b.toString());
				is.close();
				responseFromXML = b.toString();
				//System.out.println("REsponse:"+ responseFromXML);
				conn.disconnect();
			}
		}

		catch (Exception e) {
			responseFromXML = "GRACIERROR;" + e.getMessage();

		}

		//System.out.println(responseFromXML);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try{
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			//Document doc = dBuilder.parse(response);
			Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(responseFromXML.getBytes("utf-8"))));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("RecurringResult");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					customerKey=getTagValue("CustomerKey",eElement);
					System.out.println("key--->"+customerKey);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}			

		if(customerKey!=null && !customerKey.equals("")){
			CreditCardDAO.updateClientUser(((AdminRegistrationBO)request.getSession().getAttribute("user")).getMasterAssociateCode(), masterKey, customerKey);
		}
		buffer.append(customerKey);
		response.getWriter().write(buffer.toString());
	}
	public void creditCardUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		// String encodedData = ""; // user-supplied
		String responseFromXML = "";
		HttpURLConnection conn;
		//String xml_body;
		PrintStream ps;
		InputStream is;
		StringBuffer b;
		BufferedReader r;
		int result=0;
		CompanyMasterBO companyBean =new CompanyMasterBO();
		companyBean=SystemPropertiesDAO.getCCProperties(((AdminRegistrationBO)request.getSession().getAttribute("user")).getMasterAssociateCode());
		StringBuffer buffer=new StringBuffer();
		String customerKey=request.getParameter("customerKey");
		String customerName=request.getParameter("customerName");
		String creditCardNumber=request.getParameter("cardNumber");
		String cardNumberToStore="";
		if(creditCardNumber.length()>0){
			cardNumberToStore=creditCardNumber.substring(creditCardNumber.length()-4);
			cardNumberToStore="************"+cardNumberToStore;
		}
		String nameOnCard=request.getParameter("nameOnCard");
		String expiryDate=request.getParameter("expiryDate");
		String infoKey=request.getParameter("ccInfoKey");
		System.setProperty("http.keepAlive", "true");
		try {
			URL url1 = new URL("https://pmn.payment-gate.net/paygate/ws/recurring.asmx/ManageCreditCardInfo");
			conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Host", "pmn.payment-gate.net");

			conn.setRequestProperty("Connection","Keep-Alive");
			conn.setRequestProperty("Cache-Control","no-cache");
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

			//xml_body ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> " ;
			String encodedData = "UserName=" + URLEncoder.encode(companyBean.getCcUsername(), "UTF-8")+
			"&Password="+companyBean.getCcPassword()+
			"&TransType=UPDATE"+
			"&Vendor=6273"+
			"&CustomerKey="+customerKey+
			"&CardInfoKey="+infoKey+
			"&CcAccountNum="+creditCardNumber+
			"&CcExpDate="+expiryDate+
			"&CcNameOnCard="+nameOnCard+
			"&CcStreet="+
			"&CCZip="+
			"&ExtData=";
			System.out.println("encoded date--->"+encodedData);
			infoKey="";
			conn.setRequestProperty("Content-Length",""+encodedData.length());

			ps = new PrintStream(conn.getOutputStream());
			ps.write(encodedData.getBytes());
			ps.flush();
			ps.close();
			conn.connect();
			if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
				is = (InputStream) conn.getInputStream();
				// read reply
				b = new StringBuffer();
				r = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = r.readLine()) != null)
					b.append(line);
				// System.out.println("Reply From Site:" + b.toString());
				is.close();
				responseFromXML = b.toString();
				//System.out.println("REsponse for creditCard registration:"+ responseFromXML);
				conn.disconnect();
			}
		}

		catch (Exception e) {
			responseFromXML = "GRACIERROR;" + e.getMessage();

		}

		//System.out.println(responseFromXML);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try{
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			//Document doc = dBuilder.parse(response);
			Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(responseFromXML.getBytes("utf-8"))));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("RecurringResult");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					customerKey=getTagValue("CustomerKey",eElement);
					infoKey=getTagValue("CcInfoKey", eElement);
					//System.out.println("infoKey--->"+infoKey);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}			
		if(infoKey!=null && !infoKey.equals("")){
			result=CreditCardDAO.updateCreditCardDetails(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode(),customerName,cardNumberToStore,nameOnCard,expiryDate,customerKey,infoKey);
		}
		buffer.append(infoKey);
		response.getWriter().write(buffer.toString());
	}
	public void creditCardDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		String responseFromXML = "";
		HttpURLConnection conn;
		//String xml_body;
		PrintStream ps;
		InputStream is;
		StringBuffer b;
		BufferedReader r;
		CompanyMasterBO companyBean =new CompanyMasterBO();
		companyBean=SystemPropertiesDAO.getCCProperties(((AdminRegistrationBO)request.getSession().getAttribute("user")).getMasterAssociateCode());
		int result=0;
		StringBuffer buffer=new StringBuffer();
		String customerKey=request.getParameter("customerKey");
		//String customerName=request.getParameter("customerName");
		String creditCardNumber=request.getParameter("cardNumber");
		/*String cardNumberToStore="";
			if(creditCardNumber.length()>0){
				cardNumberToStore=creditCardNumber.substring(creditCardNumber.length()-4);
			}*/
		String nameOnCard=request.getParameter("nameOnCard");
		String expiryDate=request.getParameter("expiryDate");
		String infoKey=request.getParameter("ccInfoKey");
		System.setProperty("http.keepAlive", "true");
		try {
			URL url1 = new URL("https://pmn.payment-gate.net/paygate/ws/recurring.asmx/ManageCreditCardInfo");
			conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Host", "pmn.payment-gate.net");

			conn.setRequestProperty("Connection","Keep-Alive");
			conn.setRequestProperty("Cache-Control","no-cache");
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

			//xml_body ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> " ;
			String encodedData = "UserName=" + URLEncoder.encode(companyBean.getCcUsername(), "UTF-8")+
			"&Password="+companyBean.getCcPassword()+
			"&TransType=DELETE"+
			"&Vendor=6273"+
			"&CustomerKey="+customerKey+
			"&CardInfoKey="+infoKey+
			"&CcAccountNum="+creditCardNumber+
			"&CcExpDate="+expiryDate+
			"&CcNameOnCard="+nameOnCard+
			"&CcStreet="+
			"&CCZip="+
			"&ExtData=";
			infoKey="";
			conn.setRequestProperty("Content-Length",""+encodedData.length());

			ps = new PrintStream(conn.getOutputStream());
			ps.write(encodedData.getBytes());
			ps.flush();
			ps.close();
			conn.connect();
			if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
				is = (InputStream) conn.getInputStream();
				// read reply
				b = new StringBuffer();
				r = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = r.readLine()) != null)
					b.append(line);
				// System.out.println("Reply From Site:" + b.toString());
				is.close();
				responseFromXML = b.toString();
				//System.out.println("REsponse for creditCard registration:"+ responseFromXML);
				conn.disconnect();
			}
		}

		catch (Exception e) {
			responseFromXML = "GRACIERROR;" + e.getMessage();

		}

		//System.out.println(responseFromXML);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try{
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			//Document doc = dBuilder.parse(response);
			Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(responseFromXML.getBytes("utf-8"))));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("RecurringResult");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					customerKey=getTagValue("CustomerKey",eElement);
					infoKey=getTagValue("CcInfoKey", eElement);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}			
		if(infoKey!=null && !infoKey.equals("")){
			result=CreditCardDAO.deleteCreditCardDetails(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode(),customerKey,infoKey);
		}
		buffer.append(infoKey);
		response.getWriter().write(buffer.toString());
	}
	public void paymentFromCreditCard(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		String responseFromXML = "";
		HttpURLConnection conn;
		String xml_body;
		PrintStream ps;
		InputStream is;
		StringBuffer b;
		BufferedReader r;
		StringBuffer buffer=new StringBuffer();
		String amount=request.getParameter("amount");
		CompanyMasterBO companyBean =new CompanyMasterBO();
		companyBean=SystemPropertiesDAO.getCCProperties(((AdminRegistrationBO)request.getSession().getAttribute("user")).getMasterAssociateCode());
		
		// check response for status
		//while (response.indexOf("<Status StatusCode = \"Retry\"/>") > 0)
		//{

		System.setProperty("http.keepAlive", "true");
		try {
			URL url1 = new URL("https://pmn.payment-gate.net/paygate/ws/recurring.asmx/ProcessCreditCard");
			conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Host", "pmn.payment-gate.net");

			conn.setRequestProperty("Connection","Keep-Alive");
			conn.setRequestProperty("Cache-Control","no-cache");
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

			//xml_body ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> " ;
			String encodedData = "UserName=" + URLEncoder.encode(companyBean.getCcUsername(), "UTF-8")+
			"&Password="+companyBean.getCcPassword()+
			"&Vendor=6273"+
			"&CCInfoKey=2024924"+
			"&Amount="+amount+
			"&InvNum="+
			"&ExtData=";
			System.out.println("encoded date--->"+encodedData);
			conn.setRequestProperty("Content-Length",""+encodedData.length());

			ps = new PrintStream(conn.getOutputStream());
			ps.write(encodedData.getBytes());
			ps.flush();
			ps.close();
			conn.connect();
			if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
				is = (InputStream) conn.getInputStream();
				// read reply
				b = new StringBuffer();
				r = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = r.readLine()) != null)
					b.append(line);
				// System.out.println("Reply From Site:" + b.toString());
				is.close();
				responseFromXML = b.toString();
				//System.out.println("REsponse for creditCard registration:"+ responseFromXML);
				conn.disconnect();
			}
		}

		catch (Exception e) {
			responseFromXML = "GRACIERROR;" + e.getMessage();

		}

		//System.out.println(responseFromXML);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		try{
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			//Document doc = dBuilder.parse(response);
			Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(responseFromXML.getBytes("utf-8"))));
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("RecurringResult");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}			

		buffer.append("1");
		response.getWriter().write(buffer.toString());
	}

	private static String getTagValue(String sTag, Element eElement) {
		NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

		Node nValue = (Node) nlList.item(0);

		return nValue.getNodeValue();
	}

	public void checkCustomerKey(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String masterKey=request.getParameter("masterKey");
		String customerKey=CreditCardDAO.checkCustomerKey(adminBO, masterKey);
		response.getWriter().write(customerKey==null?"":customerKey);
	}
	public void deleteChargeType(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String key=request.getParameter("serialNumber");
		int result=ChargesDAO.deleteChargeTypes(adminBO.getAssociateCode(), key);
		if(result==1){
			ZoneDAO.updateVersion(adminBO.getMasterAssociateCode(), 2);
			response.getWriter().write("001");
		} else {
			response.getWriter().write("002");
		}
	}


}
