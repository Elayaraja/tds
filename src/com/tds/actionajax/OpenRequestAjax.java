package com.tds.actionajax;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.charges.constant.ICCConstant;
import com.charges.dao.ChargesDAO;
import com.tds.cmp.bean.CustomerProfile;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.OpenRequestHistory;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.MailReport;
import com.tds.controller.TDSController;
import com.tds.dao.AddressDAO;
import com.tds.dao.AdministrationDAO;
import com.tds.dao.AuditDAO;
import com.tds.dao.CallerIDDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.EmailSMSDAO;
import com.tds.dao.FinanceDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.Request2DAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SharedRideDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.ZoneDAO;
import com.tds.db.TDSConnection;
import com.tds.debug.RequestResponseIterator;
import com.tds.payment.PaymentCatgories;
import com.tds.payment.PaymentGateway;
import com.tds.payment.PaymentGatewaySlimCD;
import com.tds.pp.bean.PaymentProcessBean;
import com.common.util.CheckZone;
import com.tds.process.Email;
import com.common.util.Messaging;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.AssistantOperatorBo;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.tds.tdsBO.VoucherBO;
import com.common.util.HandleMemoryMessage;
import com.common.util.MessageGenerateJSON;
import com.common.util.OpenRequestUtil;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.common.util.TDSValidation;
import com.tds.util.TripIdUtil;

/**
 * Servlet implementation class OpenRequestAjax
 */
public class OpenRequestAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;


	@Override
	public void init(ServletConfig config) throws ServletException {
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//cat.info("Class Name "+ OpenRequestAjax.class.getName());
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public OpenRequestAjax() {

		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        
		String eventParam = "";
		
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
		if((AdminRegistrationBO)request.getSession().getAttribute("user")!=null || (clientBO!=null))
		{
			if(eventParam.equalsIgnoreCase("getComments")) {
				getComments(request,response); 
			} else if(eventParam.equalsIgnoreCase("callerIdAddress")){
				getAddress(request,response);
			}else if(eventParam.equalsIgnoreCase("acceptCall")) {
				acceptCall(request,response);
			} else if(eventParam.equalsIgnoreCase("saveOpenRequest")) {
				saveOpenRequest(request,response);
			} else if(eventParam.equalsIgnoreCase("getOpenRequest")) {
				getOpenRequest(request,response);
			} else if(eventParam.equalsIgnoreCase("logsDetail")) {
				logsDetail(request,response);
			}else if(eventParam.equalsIgnoreCase("getLatLongi")) {
				getLatLongi(request,response);
			}else if(eventParam.equalsIgnoreCase("driverDetails")) {
				driverDetails(request,response);
			}else if(eventParam.equalsIgnoreCase("updateDispatchType")) {
				updateDispatchType(request,response);
			}else if(eventParam.equalsIgnoreCase("insertSharedRide")) {
				insertSharedRide(request,response);
			}else if(eventParam.equalsIgnoreCase("getCompletedJobs")) {
				getCompletedJobs(request,response);
			}else if(eventParam.equalsIgnoreCase("getCharges")) {
				getCharges(request,response);
			}else if(eventParam.equalsIgnoreCase("getAutoTripId")) {
				getAutoTripId(request,response);
			}else if(eventParam.equalsIgnoreCase("enterCCAuth")) {
				enterCC(request,response);
			}else if(eventParam.equalsIgnoreCase("getShiftKey")) {
				getShiftKey(request,response);
			}else if(eventParam.equalsIgnoreCase("reverseDisbursement")) {
				reverseDisbursement(request,response);
			}else if(eventParam.equalsIgnoreCase("addVouchers")) {
				addVC(request,response);
			}else if(eventParam.equalsIgnoreCase("chargesForVoucher")) {
				chargesForVoucher(request,response);
			}else if(eventParam.equalsIgnoreCase("getVoucherCharges")) {
				getVoucherCharges(request,response);
			}else if(eventParam.equalsIgnoreCase("getChargesForVoucher")) {
				getChargesForVoucher(request,response);
			}else if(eventParam.equalsIgnoreCase("jobsForThisSplFlag")){
				jobsForThisSplFlag(request,response);
			}else if(eventParam.equalsIgnoreCase("chargesFromHistory")){
				chargesFromHistory(request,response);
			}else if (eventParam.equalsIgnoreCase("getSharedSummary")) {
				getSharedSummary(request, response);
			}else if (eventParam.equalsIgnoreCase("sharedRideIndividualScreen")) {
				sharedRideIndividualScreen(request, response);
			}else if(eventParam.equalsIgnoreCase("readJobStatus")){
				readJobStatus(request,response);
			}else if (eventParam.equalsIgnoreCase("callDriver")) {
				callDriver(request, response);
			}else if (eventParam.equalsIgnoreCase("getDistFMQA")) {
				getDistanceFromMapQuestApi(request, response);
			}
		}if(eventParam.equalsIgnoreCase("saveOpenRequestHTML")) {
			saveOpenRequestHTML(request,response,getServletConfig());
		}else if(eventParam.equalsIgnoreCase("sendMail")) {
			sendMail(request,response,getServletConfig());
		}else if (eventParam.equalsIgnoreCase("driverList")) {
			driverList(request, response);
		} else if(eventParam.equalsIgnoreCase("getDriverDocument")){
			getDriverDocument(request,response);
		}else if (eventParam.equalsIgnoreCase("getSharedRideSummary")) {
			getSharedRideSummary(request, response);
		}else if (eventParam.equalsIgnoreCase("getCurrentZone")) {
			getCurrentZone(request, response);
		}
	}

	private void getDistanceFromMapQuestApi(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//var url = 'https://open.mapquestapi.com/directions/v1/route?key=ZX3FD32ZdJ8LfqzB2cKPMD6WDiuSbwTE&outFormat=json
			//&routeType=shortest&timeType=1&enhancedNarrative=false&locale=en_US&unit=m
			//&from='+origin+'&to='+destination+'&drivingStyle=2&highwayEfficiency=21.0';
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		
		String from = request.getParameter("from");
		String to = request.getParameter("to");
		String keyParam = "MapQuestApiKey"+adminBO.getMasterAssociateCode();
		String key = TDSProperties.getValue(keyParam);
		if(key==null || key.equals("")){
			System.out.println("key is null for ccode:"+adminBO.getMasterAssociateCode());
			key = TDSProperties.getValue("MapQuestApiKey");
			if(key==null || key.equals("")){
				System.out.println("Default api key too null for ccode:"+adminBO.getMasterAssociateCode());
				//ZX3FD32ZdJ8LfqzB2cKPMD6WDiuSbwTE - ram key
				key = "zWvd3uIdpAEhiSecZloa25pMuY4mHUvC";
			}
		}
		
		//AUTH_FAILED
		String jsonCCode = TDSProperties.getValue("AllowedCCode");
		System.out.println("jsonccode:"+jsonCCode);
		jsonCCode = jsonCCode.replace(" ", "");
		boolean isCCodeAllowed = false;
		if(jsonCCode!=null && !jsonCCode.equalsIgnoreCase("")){
			try {
				JSONArray array1 = new JSONArray(jsonCCode);
				JSONObject jobj = array1.getJSONObject(0);
				if(jobj.has("CCode")){
					String ccodeStr = jobj.getString("CCode");
					String[]ccodeList = ccodeStr.split(",");
					if(ccodeList!=null && ccodeList.length>0){
						for(int i=0;i<ccodeList.length;i++){
							if(ccodeList[i].equalsIgnoreCase(adminBO.getMasterAssociateCode())){
								isCCodeAllowed = true;
								break;
							}
						}
					}
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(!isCCodeAllowed){
			System.out.println("Distance asked for "+adminBO.getMasterAssociateCode()+" and AUTH_FAILED");
			response.getWriter().write("AUTH_FAILED");
			return;
		}
		String routeType = (request.getParameter("routeType")!=null && !request.getParameter("routeType").equals(""))?request.getParameter("routeType"):"shortest";
		String locale = (request.getParameter("locale")!=null && !request.getParameter("locale").equals(""))?request.getParameter("locale"):"en_US";
		String drivingStyle = (request.getParameter("drivingStyle")!=null && !request.getParameter("drivingStyle").equals(""))?request.getParameter("drivingStyle"):"2";
		String highwayEfficiency = (request.getParameter("highwayEfficiency")!=null && !request.getParameter("highwayEfficiency").equals(""))?request.getParameter("highwayEfficiency"):"21.0";
		String extraData = "&outFormat=json&timeType=1&enhancedNarrative=false&unit=m";
		String encodedData = "key="+key+"&routeType="+routeType+"&locale="+locale+"&from="+from+"&to="+to+"&drivingStyle="+drivingStyle+"&highwayEfficiency="+highwayEfficiency+extraData;
		
		String responseData = "GRACIERROR";
		HttpURLConnection conn;
		//System.out.println("encodedData --> "+encodedData);
		System.setProperty("http.keepAlive", "true");
		//System.setProperty("https.protocols", "TLSv1.2");
		try {
			URL url1 = new URL("http://open.mapquestapi.com/directions/v1/route?"+encodedData);
			conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("GET");
			conn.setRequestProperty("User-Agent", "www.gacdispatch.com");
			
			System.out.println("\nSending 'GET' request to URL : " + url1);
			int resposeCode = conn.getResponseCode();
			System.out.println("Distance asked for "+adminBO.getMasterAssociateCode()+" and Response code is "+resposeCode);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputline;
			StringBuffer responseBuffer = new StringBuffer();
			while((inputline = in.readLine())!=null){
				responseBuffer.append(inputline);
			}
			in.close();
			
			responseData = responseBuffer.toString();
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Distance asked for "+adminBO.getMasterAssociateCode()+" and Exception due to : "+e.getMessage());
			//System.out.println("message:"+e.getMessage());
			responseData = "GRACIERROR";
		}
		//System.out.println("Response--->"+responseData);
		response.getWriter().write(responseData);
	}
	
	public void sendMail(HttpServletRequest request, HttpServletResponse response,ServletConfig srvletConfig) throws ServletException, IOException { 
		String username="";
		String password="";
		String host="";
		ArrayList<String> emails=new ArrayList<String>();
		emails=EmailSMSDAO.emailUsers(request.getParameter("associationCode"));
		if(emails.size()>0){
			username=emails.get(0);
			password=emails.get(1);
			host=emails.get(3);
			Email otherProviderSMS = new Email(host,username,password,username,"465","smtp");
			String details="";
			details =request.getParameter("fName")+" "+request.getParameter("lName")+" "+request.getParameter("eMail")+" "+request.getParameter("organization")+
					" "+request.getParameter("inquiry")+" "+request.getParameter("state")+" "+request.getParameter("country")+" "+request.getParameter("desc");
			try{
				otherProviderSMS.sendMail("ram@888getacab.com","GetACab Website-New Client", details);
				response.getWriter().write("001###");
			} catch (Exception e ){
				//System.out.println(e.toString());
				response.getWriter().write("002###");
			}
		}
	}

	public void getAutoTripId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String tripID=TripIdUtil.tripIdAction(adminBO.getAssociateCode(),adminBO.getMasterAssociateCode());
		response.getWriter().write(tripID);
	}
	public void chargesForVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("size")!=null){
			int numberOfRows = Integer.parseInt(request.getParameter("size"));
			String voucher=request.getParameter("vNo");
			String[] amount=new String[numberOfRows+1];
			String[] key=new String[numberOfRows+1];
			for (int rowIterator = 0; rowIterator < numberOfRows; rowIterator++) {
				amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
				key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
			}
			ChargesDAO.matchVoucherCharges(adminBO.getAssociateCode(),amount,key,numberOfRows,voucher);
		}
	}

	public void getVoucherCharges(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String account=request.getParameter("account");
		ArrayList<ChargesBO> chargesValues = ChargesDAO.getVoucherCharge(adminBO.getAssociateCode(),account);
		JSONArray array = new JSONArray();
		if(chargesValues!=null && chargesValues.size()>0){
			for(int j=0;j<chargesValues.size();j++){
				JSONObject address = new JSONObject();
				try {
					address.put("CK", chargesValues.get(j).getPayTypeKey());
					address.put("CA", chargesValues.get(j).getPayTypeAmount());
					array.put(address);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}
	public void getChargesForVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String voucherNo=request.getParameter("voucherNo");
		ArrayList<ChargesBO> al_list = ChargesDAO.getVoucherCharges(adminBO.getAssociateCode(),voucherNo);
		JSONArray array = new JSONArray();
		if(al_list.size()>0){
			for(int i=0;i<al_list.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("T", al_list.get(i).getPayTypeKey());
					address.put("A", al_list.get(i).getAmount());
					array.put(address);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	public void addVC(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(adminBO.getMasterAssociateCode());
		String type=request.getParameter("payType");
		boolean result=false;
		if(type.equals("vc")){
			String tripID=request.getParameter("tripId");
			String vcNum=request.getParameter("vcNum");
			String driverId=request.getParameter("driverId");
			String amount=request.getParameter("amount");
			result=ChargesDAO.insertVouchers(tripID,vcNum,adminBO.getAssociateCode(),driverId,amount);
		} else {
			PaymentProcessBean processBean = new PaymentProcessBean();
			processBean.setB_trip_id(request.getParameter("tripId"));
			processBean.setCvv(request.getParameter("cvvCode"));
			processBean.setB_card_full_no(request.getParameter("vcNum"));
			processBean.setB_amount(request.getParameter("amount").equals("")?"0":request.getParameter("amount"));
			processBean.setB_cardExpiryDate(request.getParameter("expDate"));
			processBean.setZip(request.getParameter("zipCode")==null?"":request.getParameter("zipCode"));
			processBean.setB_cardHolderName(request.getParameter("name"));
			processBean.setB_driverid(request.getParameter("driverId"));
			processBean.setB_tip_amt("0");
			if(cmpBean.getCcProvider()==2){
				processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Auth);
			} else {
				processBean=PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Auth);
			}
			if(processBean.isAuth_status()){
				ChargesDAO.insertPaymentIntoCC(processBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBO, ICCConstant.PREAUTH,ICCConstant.GAC);
				result=true;
			}
		}
		if(result){
			response.getWriter().write("001");
		} else {
			response.getWriter().write("002");
		}
	}



	public void getShiftKey(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		int shiftId=RequestDAO.getShiftKey(adminBO);
		response.getWriter().write(shiftId+"");
	}


	public void enterCC(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		PaymentProcessBean processBean = new PaymentProcessBean();
		CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(adminBO.getMasterAssociateCode());
		String category=request.getParameter("category");
		if(request.getParameter("transId")==null){
			processBean.setB_trip_id(request.getParameter("tripId"));
			processBean.setCvv(request.getParameter("cvvCode"));
			processBean.setB_card_full_no(request.getParameter("ccNumber"));
			processBean.setB_amount(request.getParameter("amount").equals("")?"0":request.getParameter("amount"));
			processBean.setB_cardExpiryDate(request.getParameter("expDate"));
			processBean.setZip(request.getParameter("zipCode"));
			processBean.setB_cardHolderName(request.getParameter("name"));
			processBean.setB_tip_amt("0");
			processBean.setB_driverid(adminBO.getUid());
			if(cmpBean.getCcProvider()==2){
				processBean= PaymentGatewaySlimCD.makeProcess(processBean, cmpBean, category);
			} else {
				processBean=PaymentGateway.makeProcess(processBean,cmpBean, category);
			}
			if(processBean.isAuth_status()){
				int transId=ChargesDAO.insertPaymentIntoCC(processBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBO, ICCConstant.PREAUTH,ICCConstant.GAC);
				response.getWriter().write("001;"+transId+"###");
			} else {
				response.getWriter().write("002;"+processBean.getReason()+"###");
			}
		} else {
			processBean=ChargesDAO.getCCDetails(adminBO.getAssociateCode(),request.getParameter("transId"),1);
			processBean.setB_driverid(adminBO.getUid());
			processBean.setB_cap_trans("");
			if(cmpBean.getCcProvider()==2){
				processBean= PaymentGatewaySlimCD.makeProcess(processBean, cmpBean, category);
			} else {
				processBean=PaymentGateway.makeProcess(processBean,cmpBean, category);
			}
			if(processBean.isAuth_status()){
				RequestDAO.deleteOpenRequest(request.getParameter("transId"), adminBO.getAssociateCode());
				response.getWriter().write("Success###");
			} else {
				response.getWriter().write("Failed###");
			}
		}
	}


	public void getCharges(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String tripId=request.getParameter("tripId");
		ArrayList<ChargesBO> al_list = ChargesDAO.getORCharges(adminBO.getAssociateCode(),tripId,adminBO.getMasterAssociateCode());
		JSONArray array = new JSONArray();
		if(al_list.size()>0){
			for(int i=0;i<al_list.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("T", al_list.get(i).getPayTypeKey());
					address.put("A", al_list.get(i).getAmount());
					array.put(address);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}
	public void getComments(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		StringBuffer buffer=new StringBuffer();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String account=request.getParameter("account");
		ArrayList<VoucherBO> al_list = RequestDAO.getComments(adminBO,account);
		JSONArray array = new JSONArray();
		if(al_list.size()>0){
			for(int i=0;i<al_list.size();i++){
				JSONObject address = new JSONObject();
				try {
					if(al_list.get(i).getTo_date().startsWith("-") || al_list.get(i).getVstatus().equals("0")){
						address.put("Description", "Failed");
						address.put("Name", "Failed");
						address.put("FixedRate", "");
						address.put("CN", "");
						address.put("SA", "");
						address.put("RPM", "");
						address.put("OC", "");
						address.put("DC", "");
						address.put("SF", "");
						array.put(address);			
					} else {	
						address.put("Description", al_list.get(i).getVdesc()==null?"":al_list.get(i).getVdesc());
						address.put("Name", al_list.get(i).getVcontact()==null?"":al_list.get(i).getVcontact());
						address.put("FixedRate", al_list.get(i).getVamount()==null?"":al_list.get(i).getVamount());
						address.put("CN", al_list.get(i).getCompanyName()==null?"":al_list.get(i).getCompanyName());
						address.put("SA", al_list.get(i).getStartAmount()==null?"":al_list.get(i).getStartAmount());
						address.put("RPM", al_list.get(i).getRatePerMile()==null?"":al_list.get(i).getRatePerMile());
						address.put("DC", al_list.get(i).getDriverComments()==null?"":al_list.get(i).getDriverComments());
						address.put("OC", al_list.get(i).getOperatorComments()==null?"":al_list.get(i).getOperatorComments());
						address.put("SF", al_list.get(i).getSpecialFlags()==null?"":al_list.get(i).getSpecialFlags());
						array.put(address);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*			if(al_list.get(i).getTo_date().startsWith("-")){
					buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"	xmlns:v=\"urn:schemas-microsoft-com:vml\">");
					buffer.append("<h3><center>Voucher Expired</center></h3></br>###");
				}else{
					if(al_list.get(i).getVamount()!=null && !al_list.get(i).getVamount().equals("")){
						text="Fixed Rate="+al_list.get(i).getVamount()+";";
					}
					buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"	xmlns:v=\"urn:schemas-microsoft-com:vml\">");
					buffer.append("<h3><center>Account Details</center></h3></br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+text+"Description="+al_list.get(i).getVdesc()+";Name="+al_list.get(i).getVcontact()+"###");
				}*/
			}
		} else {
			JSONObject address = new JSONObject();
			try {
				address.put("Description", "Failed");
				address.put("Name", "Failed");
				address.put("FixedRate", "");
				address.put("CN", "");
				array.put(address);			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	public void getAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		StringBuffer buffer=new StringBuffer();
		JSONArray array = new JSONArray();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String phoneNumber=request.getParameter("phoneNumber");
		int lineNumber =Integer.parseInt(request.getParameter("lineNumber"));
		boolean success=RequestDAO.setCallerIdAccept(adminBO,lineNumber,phoneNumber);
		ArrayList<OpenRequestBO> Address = RequestDAO.getAddress(adminBO.getMasterAssociateCode(),phoneNumber);
		for(int i=0;i<Address.size();i++){
			JSONObject address = new JSONObject();
			try {
				address.put("A1", Address.get(i).getSadd1()==null?"":Address.get(i).getSadd1());
				address.put("A2", Address.get(i).getSadd2()==null?"":Address.get(i).getSadd2());
				address.put("C", Address.get(i).getScity()==null?"":Address.get(i).getScity());
				address.put("S", Address.get(i).getSstate()==null?"":Address.get(i).getSstate());
				address.put("Z", Address.get(i).getSzip()==null?"":Address.get(i).getSzip());
				address.put("P", Address.get(i).getPaytype()==null?"":Address.get(i).getPaytype());
				address.put("A", Address.get(i).getAcct()==null?"":Address.get(i).getAcct());
				address.put("La", Address.get(i).getSlat()==null?"":Address.get(i).getSlat());
				address.put("Lo", Address.get(i).getSlong()==null?"":Address.get(i).getSlong());
				address.put("M", Address.get(i).getMasterAddressKey()==null?"":Address.get(i).getMasterAddressKey());
				//			address.put("Name", Address.get(i).getName()==null?"":Address.get(i).getName());
				array.put(address);			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.setContentType("application/json");
		response.getWriter().write(responseString.toString());

	}

	public void acceptCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String buffer="notUpdate";
		int lineNum=Integer.parseInt(request.getParameter("lineNumber"));
		boolean success=RequestDAO.setCallerIdAccept(adminBO,lineNum,request.getParameter("phNum"));
		if(success){
			buffer="update";
		}
		response.getWriter().write(buffer.toString());
	}
	public void getOpenRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		StringBuffer buffer=new StringBuffer();
		OpenRequestBO openRequestBO = new OpenRequestBO();
		JSONArray array = new JSONArray();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String tripId=request.getParameter("tripId");
		if(request.getParameter("fromHistory")!=null){
			openRequestBO = ServiceRequestDAO.getOpenRequestHistoryByTripID(tripId, adminBO);
		}else{
			openRequestBO = ServiceRequestDAO.getOpenRequestByTripID(tripId, adminBO, new ArrayList<String>());
		}
		JSONObject address = new JSONObject();
		try {
			address.put("Add1", openRequestBO.getSadd1()==null?"":openRequestBO.getSadd1());
			address.put("Add2", openRequestBO.getSadd2()==null?"":openRequestBO.getSadd2());
			address.put("City", openRequestBO.getScity()==null?"":openRequestBO.getScity());
			address.put("State", openRequestBO.getSstate()==null?"":openRequestBO.getSstate());
			address.put("Zip", openRequestBO.getSzip()==null?"":openRequestBO.getSzip());
			address.put("PayType", openRequestBO.getPaytype()==null?"":openRequestBO.getPaytype());
			address.put("Acct", openRequestBO.getAcct()==null?"":openRequestBO.getAcct());
			address.put("Lat", openRequestBO.getSlat()==null?"":openRequestBO.getSlat());
			address.put("Lon", openRequestBO.getSlong()==null?"":openRequestBO.getSlong());
			address.put("Name", openRequestBO.getName()==null?"":openRequestBO.getName());
			address.put("EAdd1", openRequestBO.getEadd1()==null?"":openRequestBO.getEadd1());
			address.put("EAdd2", openRequestBO.getEadd2()==null?"":openRequestBO.getEadd2());
			address.put("ECity", openRequestBO.getEcity()==null?"":openRequestBO.getEcity());
			address.put("EState", openRequestBO.getEstate()==null?"":openRequestBO.getEstate());
			address.put("EZip", openRequestBO.getEzip()==null?"":openRequestBO.getEzip());
			address.put("ELat", openRequestBO.getEdlatitude()==null?"":openRequestBO.getEdlatitude());
			address.put("ELon", openRequestBO.getEdlongitude()==null?"":openRequestBO.getEdlongitude());
			address.put("Zone", openRequestBO.getQueueno()==null?"":openRequestBO.getQueueno());
			address.put("LandMark", openRequestBO.getSlandmark()==null?"":openRequestBO.getSlandmark());
			address.put("ELandMark", openRequestBO.getElandmark()==null?"":openRequestBO.getElandmark());
			address.put("Date", openRequestBO.getSdate()==null?"":openRequestBO.getSdate());
			address.put("Time", openRequestBO.getShrs()==null?"":openRequestBO.getShrs());
			address.put("Phone", openRequestBO.getPhone()==null?"":openRequestBO.getPhone());
			address.put("SplIns", openRequestBO.getSpecialIns()==null?"":openRequestBO.getSpecialIns());
			address.put("Comments", openRequestBO.getComments()==null?"":openRequestBO.getComments());
			address.put("Driver", openRequestBO.getDriverid()==null?"":openRequestBO.getDriverid());
			address.put("Vehicle", openRequestBO.getVehicleNo()==null?"":openRequestBO.getVehicleNo());
			address.put("Flag", openRequestBO.getDrProfile()==null?"":openRequestBO.getDrProfile());
			String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getMasterAssociateCode(),  openRequestBO.getDrProfile()==null?"":openRequestBO.getDrProfile(), ";", 0);
			address.put("SFL", driverFlagValue);
			address.put("tripId", openRequestBO.getTripid()==null?"":openRequestBO.getTripid());
			address.put("sharedRide", openRequestBO.getTypeOfRide()==null?"":openRequestBO.getTypeOfRide());
			address.put("multiJobTag", openRequestBO.getRepeatGroup()==null?"":openRequestBO.getRepeatGroup());
			address.put("numberOfPassengers", openRequestBO.getNumOfPassengers()==0?"":openRequestBO.getNumOfPassengers());
			address.put("dropTime", openRequestBO.getDropTime()==null?"":openRequestBO.getDropTime());
			address.put("status",openRequestBO.getTripStatus()==null?"":openRequestBO.getTripStatus());
			address.put("dontDispatch",openRequestBO.getDontDispatch());
			address.put("premiumCustomer",openRequestBO.getPremiumCustomer());
			address.put("AT",openRequestBO.getAdvanceTime()==null?"-1":openRequestBO.getAdvanceTime());
			address.put("EM",openRequestBO.getEmail()==null?"":openRequestBO.getEmail());
			address.put("TS",openRequestBO.getChckTripStatus());
			address.put("JR",openRequestBO.getJobRating());
			address.put("FL",openRequestBO.getAssociateCode());
			address.put("CPh",openRequestBO.getCallerPhone()==null?"":openRequestBO.getCallerPhone());
			address.put("CNa",openRequestBO.getCallerName()==null?"":openRequestBO.getCallerName());
			address.put("MTy",openRequestBO.getPaymentMeter());
			address.put("ANAM", openRequestBO.getAirName()==null?"":openRequestBO.getAirName());
			address.put("AN", openRequestBO.getAirNo()==null?"":openRequestBO.getAirNo());
			address.put("AFROM", openRequestBO.getAirFrom()==null?"":openRequestBO.getAirFrom());
			address.put("ATO", openRequestBO.getAirTo()==null?"":openRequestBO.getAirTo());
			
			address.put("Rf",openRequestBO.getRefNumber()==null?"":openRequestBO.getRefNumber());
			address.put("Rf1",openRequestBO.getRefNumber1()==null?"":openRequestBO.getRefNumber1());
			address.put("Rf2",openRequestBO.getRefNumber2()==null?"":openRequestBO.getRefNumber2());
			address.put("Rf3",openRequestBO.getRefNumber3()==null?"":openRequestBO.getRefNumber3());
			array.put(address);			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String responseString = array.toString();
		response.setContentType("application/json");
		response.getWriter().write(responseString.toString());
	}	

	public void saveOpenRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		HttpSession session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		if(adminBO.getRoles().contains("1003")){
			StringBuffer buffer=new StringBuffer();
			String errors = "";
			int result=0;
			String error="";
			String m_queueId = "";
			String m_queueIdRoundTrip="";
			OpenRequestBO openRequestBO = new OpenRequestBO();
			ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
			String tripId = request.getParameter("tripId");
			if(request.getParameter("delete") != null && request.getParameter("delete").equalsIgnoreCase("delete")) {
				openRequestBO.setTripid(request.getParameter("tripId"));
				openRequestBO.setRepeatGroup(request.getParameter("multiJobUpdate"));
				if(openRequestBO.getTripid().length() > 0 || !openRequestBO.getTripid().equals("") || !openRequestBO.getRepeatGroup().equals("")) {
					String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
					if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
						result = DispatchDAO.moveToHistory(openRequestBO.getTripid(), request.getParameter("assoCode")==null?adminBO.getAssociateCode():request.getParameter("assoCode"), openRequestBO.getRepeatGroup(),adminBO.getCompanyList());
					} else {
						ArrayList<String> myString = new ArrayList<String>();
						result = DispatchDAO.moveToHistory(openRequestBO.getTripid(), request.getParameter("assoCode")==null?adminBO.getAssociateCode():request.getParameter("assoCode"), openRequestBO.getRepeatGroup(),myString);
					}
//					result=DispatchDAO.moveToHistory(openRequestBO.getTripid(), request.getParameter("assoCode")==null?adminBO.getAssociateCode():request.getParameter("assoCode"),openRequestBO.getRepeatGroup());
					if(result >0 ) {
						error="001;Trip "+openRequestBO.getTripid()+" is deleted succesfully";
					} else {
						error="002;Error in deleting Trip "+openRequestBO.getTripid();
					}
				} 
			} else {
				openRequestBO = setOpenRequest(openRequestBO, request);
				request.setAttribute("openrequestBO", openRequestBO);
				error=OpenRequestUtil.saveOpenRequest(request, response, getServletConfig());
				RequestResponseIterator.writeRequestIntoDatabase(request);
				if(tripId.equals("")){
					if(openRequestBO.getMasterAddressKey().equals("")){
						ArrayList<CustomerProfile> custProf = AddressDAO.getCustomerProfile(openRequestBO.getPhone(), "",adminBO, 0, "");
						if(custProf.size()>0){
							openRequestBO.setMasterAddressKey(custProf.get(0).getMasterKey());
							if(openRequestBO.getLandMarkKey().equals("") && openRequestBO.getUserAddressKey().equals("")){
								RequestDAO.insertClientUser(openRequestBO, 0,adminBO.getMasterAssociateCode());
								if(!openRequestBO.getEadd1().equals("") && !openRequestBO.getEdlatitude().equals("") && !openRequestBO.getEdlongitude().equals("")){
									RequestDAO.insertClientUser(openRequestBO, 1,adminBO.getMasterAssociateCode());
								}
							} else if(!openRequestBO.getLandMarkKey().equals("")){
								RequestDAO.insertClientLandMark(openRequestBO);
							}
						} else {
							String dispatchComments=request.getParameter("updateStaticComment").replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", "");
							String driverComments=request.getParameter("updateDriverComment").replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", "");
							AddressDAO.insertUserAddressMaster(openRequestBO,dispatchComments,driverComments,adminBO.getMasterAssociateCode());
						}
					} else if(!openRequestBO.getMasterAddressKey().equals("")){
						if(openRequestBO.getUserAddressKey().equals("") && request.getParameter("slandmark").equals("") && !openRequestBO.getSadd1().equals("") && request.getParameter("addPickUp").equals("")){
							RequestDAO.insertClientUser(openRequestBO, 0,adminBO.getMasterAssociateCode());
						}if(request.getParameter("userAddressTo").equals("") && request.getParameter("elandmark").equals("") && !openRequestBO.getEadd1().equals("") && request.getParameter("addDropOff").equals("")){
							RequestDAO.insertClientUser(openRequestBO, 1,adminBO.getMasterAssociateCode());
						}if(!openRequestBO.getLandMarkKey().equals("")){
							RequestDAO.insertClientLandMark(openRequestBO);
						}
					}
					if(!openRequestBO.getMasterAddressKey().equals("") && !openRequestBO.getUserAddressKey().equals("")){
						RequestDAO.updateClientAddress(adminBO.getMasterAssociateCode(), openRequestBO.getUserAddressKey(), openRequestBO.getMasterAddressKey());
					}	
					if(!request.getParameter("updateStaticComment").equals("")){
						String dispatchComments=request.getParameter("updateStaticComment");
						AddressDAO.updateUserAddressMasterComments(dispatchComments.replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", ""),openRequestBO.getPhone(),adminBO.getMasterAssociateCode(),0,1);
					}if(!request.getParameter("updateDriverComment").equals("")){
						String driverComments=request.getParameter("updateDriverComment");
						AddressDAO.updateUserAddressMasterComments(driverComments.replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", ""),openRequestBO.getPhone(),adminBO.getMasterAssociateCode(),0,2);
					}
					CallerIDDAO.checkStatus(openRequestBO.getPhone(),adminBO.getAssociateCode(),adminBO.getUid());
				} else {
					if((error.length() <= 6 || error.contains("OK"))) {
						OpenRequestBO openBo = RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());
						try{
							//Check, If dispatch start time is today.
							//System.out.println("openBo.getDriverid() ->> "+openBo.getDriverid());
							//System.out.println("satrttime : "+openBo.getStartTimeStamp());
							//System.out.println("time to start: "+openBo.getTimeToStartTripInSeconds());
							if(!openBo.getDriverid().equals("")){
								if(openBo.getTimeToStartTripInSeconds()<=0){
									System.out.println("No reservation job -> tripId:"+tripId+" --> driverid:"+openBo.getDriverid());
									DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getMasterAssociateCode(), openBo.getDriverid(), 1);
									String[] message = MessageGenerateJSON.generateMessageAfterAcceptance(openBo, "AA", System.currentTimeMillis(), adminBO.getAssociateCode());
									Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, adminBO.getAssociateCode());
									oneSMS.start();
								}else{
									System.out.println("A reservation job -> tripId:"+tripId+" --> driverid:"+openBo.getDriverid());
								}
							}
							
						} catch(Exception e){
							System.out.println("Error sending mesage to driver"+e.toString());
						}
					}
				}
				if(!request.getParameter("eMail").equals("")){
					String driverComments=request.getParameter("eMail");
					AddressDAO.updateUserAddressMasterComments(driverComments.replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", ""),openRequestBO.getPhone(),adminBO.getMasterAssociateCode(),0,4);
				}
				//System.out.println("Error--->"+error);
				if(error.length() <= 6 || error.contains("OK")) { 
					if(openRequestBO.getAcct()!=null && !openRequestBO.getAcct().equals("")){
						String[] allocationArray=FinanceDAO.readVoucherAllocation(openRequestBO.getAcct(),adminBO.getMasterAssociateCode());
						String[] firstSplit = error.split(";");
						String[] tripIdFinal = firstSplit[0].split("=");
						DispatchDAO.enterRestrictionForVoucher(allocationArray,tripIdFinal[1]);
					}
					error="001;Job Created/Updated for "+openRequestBO.getName()+"  at:"+(openRequestBO.getShrs().equals("2525")?" Now":openRequestBO.getShrs()+" Hrs")+""+error;
				} else {
					error="002;Fail To Create/Update Open Request<br> "+error;
				}
			}
			if(request.getParameter("nextScreen")!=null && !request.getParameter("nextScreen").equals("") && request.getParameter("nextScreen").equalsIgnoreCase("DashBoard")){
				buffer.append(error+"###"+"003");
			}else{
				buffer.append(error+"###");
			}
			JSONArray array = new JSONArray();
			JSONObject tripIdResp = new JSONObject();
			if(request.getParameter("responseFormat")!=null && request.getParameter("responseFormat").equals("json")){
				try {
					tripIdResp.put("TI", error);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				array.put(tripIdResp);			
				response.getWriter().write(array.toString());
			} else {
				response.getWriter().write(buffer.toString());
			}
		} else {
			response.getWriter().write("002;No Access");
		}
	}

	public OpenRequestBO setOpenRequest(OpenRequestBO requestBO,HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		requestBO.setUserAddressKey(request.getParameter("userAddress"));
		requestBO.setMasterAddressKey(request.getParameter("masterAddress"));
		requestBO.setEadd1(request.getParameter("eadd1"));
		requestBO.setEadd2(request.getParameter("eadd2"));
		requestBO.setEcity(request.getParameter("ecity"));
		requestBO.setEstate(request.getParameter("estate"));
		requestBO.setEzip(request.getParameter("ezip"));
		requestBO.setPhone(request.getParameter("phone").replace("-", ""));
		requestBO.setSadd1(request.getParameter("sadd1"));
		requestBO.setSadd2(request.getParameter("sadd2"));
		requestBO.setScity(request.getParameter("scity"));
		requestBO.setSdate(request.getParameter("sdate"));
		requestBO.setSstate(request.getParameter("sstate"));
		requestBO.setShrs(request.getParameter("shrs"));
		requestBO.setSzip(request.getParameter("szip"));
		requestBO.setSlandmark(request.getParameter("slandmark"));
		requestBO.setElandmark(request.getParameter("elandmark"));
		requestBO.setLandMarkKey(request.getParameter("landMarkKey"));
		requestBO.setSpecialIns(request.getParameter("specialIns"));
		requestBO.setSintersection(request.getParameter("sintersection"));
		requestBO.setRepeatGroup(request.getParameter("multiJobUpdate"));
		if(request.getParameter("tripSource")!=null&&!request.getParameter("tripSource").equalsIgnoreCase("")){
			requestBO.setTripSource(Integer.parseInt(request.getParameter("tripSource")));
			//			requestBO.setTripSource(TDSConstants.web);
		}else{
			requestBO.setTripSource(TDSConstants.web);
		}
		requestBO.setPaymentMeter(Integer.parseInt((request.getParameter("meterType")==null||request.getParameter("meterType").equals(""))?"0":request.getParameter("meterType")));
		requestBO.setEmail(request.getParameter("eMail"));
		requestBO.setCallerPhone(request.getParameter("callerPhone")==null?"":request.getParameter("callerPhone"));
		requestBO.setCallerName(request.getParameter("callerName")==null?"":request.getParameter("callerName"));
		requestBO.setRefNumber(request.getParameter("custRefNum")==null?"":request.getParameter("custRefNum"));
		requestBO.setRefNumber1(request.getParameter("custRefNum1")==null?"":request.getParameter("custRefNum1"));
		requestBO.setRefNumber2(request.getParameter("custRefNum2")==null?"":request.getParameter("custRefNum2"));
		requestBO.setRefNumber3(request.getParameter("custRefNum3")==null?"":request.getParameter("custRefNum3"));
		//Getting latitude and longitue from the html screen
		if(request.getParameter("sLongitude").equals("")){
			requestBO.setSlong("0");		
		} else {
			requestBO.setSlong(request.getParameter("sLongitude"));
		}
		if(request.getParameter("sLatitude").equals("")){
			requestBO.setSlat("0");
		} else {
			requestBO.setSlat(request.getParameter("sLatitude"));
		}
		if(!request.getParameter("numberOfPassengers").equals("")){
			requestBO.setNumOfPassengers(Integer.parseInt(request.getParameter("numberOfPassengers")));
		} else {
			requestBO.setNumOfPassengers(1);
		}
		if(request.getParameter("dropTime")!=null && !request.getParameter("dropTime").equals("")){
			requestBO.setDropTime(request.getParameter("dropTime"));
		} else {
			requestBO.setDropTime(request.getParameter("0000"));
		}
		if(request.getParameter("numOfCabs")!=null && request.getParameter("numOfCabs")!=""){
			requestBO.setNumOfCabs(Integer.parseInt(request.getParameter("numOfCabs")));
		} else {
			requestBO.setNumOfCabs(1);
		}
		requestBO.setTypeOfRide(request.getParameter("sharedRide"));
		requestBO.setPremiumCustomer(Integer.parseInt(request.getParameter("premiumCustomer")==null?"0":request.getParameter("premiumCustomer")));
		requestBO.setComments(request.getParameter("Comments"));
		requestBO.setDriverid(request.getParameter("driver"));
		requestBO.setVehicleNo(request.getParameter("cabNo"));
		requestBO.setTripid(request.getParameter("tripId"));
		requestBO.setAdvanceTime(request.getParameter("advanceTime"));
		requestBO.setPaytype((request.getParameter("payType")!=null && !request.getParameter("payType").equals(""))?request.getParameter("payType"):"Cash");
		//requestBO.setPaytype(request.getParameter("payType"));
		requestBO.setAcct(request.getParameter("acct"));
		requestBO.setAirName(request.getParameter("airName"));
		requestBO.setAirNo(request.getParameter("airNo"));
		requestBO.setAirFrom(request.getParameter("airFrom"));
		requestBO.setAirTo(request.getParameter("airTo"));
		requestBO.setJobRating(Integer.parseInt(request.getParameter("jobRating")==null?"1":request.getParameter("jobRating")));
		if (request.getParameter("amt") != null){
			if(!request.getParameter("amt").trim().equals("")){
				requestBO.setAmt(new BigDecimal(request.getParameter("amt").toString()));
			}else{
				requestBO.setAmt(new BigDecimal("0.00"));
			}
		}if(request.getParameter("ccAuthCode")!=null && !request.getParameter("ccAuthCode").equals("")){
			requestBO.setPaytype("CC");
			requestBO.setAcct(request.getParameter("ccAuthCode"));
			requestBO.setPaymentStatus(1);
		} 
		//		if(request.getParameter("driverid") != null) {
		//			requestBO.setDriverid(request.getParameter("driverid"));
		//			requestBO.setVehicleNo("");
		//		}
		if(request.getParameter("tripid") != null) {
			requestBO.setTripid(request.getParameter("tripid"));
		} 
		if(request.getParameter("associateCode") != null) {
			requestBO.setAssociateCode(request.getParameter("associateCode"));
		}
		//		if(request.getParameter("Slat") != null ) {
		//			requestBO.setSlat(request.getParameter("Slat"));
		//		}
		//		if(request.getParameter("Slong") != null) {
		//			requestBO.setSlong(request.getParameter("Slong"));
		//		}
		if(request.getParameter("edlongitude").equals("")||request.getParameter("edlongitude").equals("0.000000")){
			requestBO.setEdlongitude("0");		
		} else {
			requestBO.setEdlongitude(request.getParameter("edlongitude"));
		}
		if(request.getParameter("edlatitude").equals("")||request.getParameter("edlatitude").equals("0.000000")){
			requestBO.setEdlatitude("0");
		} else {
			requestBO.setEdlatitude(request.getParameter("edlatitude"));
		}
		if(request.getParameter("name") != null) {
			requestBO.setName(request.getParameter("name"));
		}
		if(request.getParameter("createdBy")!=null){
			requestBO.setCreatedBy(request.getParameter("createdBy"));
		}else{
			requestBO.setCreatedBy(adminBO.getUid()==null?"106001":adminBO.getUid());
		}
		if(request.getParameter("queueno") != null && !request.getParameter("queueno").equals("")) {
			requestBO.setQueueno(request.getParameter("queueno"));
		}if(request.getParameter("dispatchStatus") !=null && !request.getParameter("dispatchStatus").equals("")) {
			requestBO.setDontDispatch(1);
		}else {
			requestBO.setDontDispatch(0);
		}
		//Populate the phone provider of the passenger.
		if(request.getParameter("toSms") != null) {
			requestBO.setPassengerPhoneServiceProvider(request.getParameter("toSms"));
		}
		try{
			if(request.getParameter("drprofileSize")!=null && request.getParameter("drprofileSize")!=""&& Integer.parseInt(request.getParameter("drprofileSize")) > 0)
			{
				ArrayList al_l = new ArrayList();
				String drProfile="";
				for(int i=0;i<Integer.parseInt(request.getParameter("drprofileSize"));i++)
				{
					if(request.getParameter("dchk"+i)!=null){
						al_l.add(request.getParameter("dchk"+i));
						drProfile = drProfile + request.getParameter("dchk"+i);
					}
				}
				requestBO.setAl_drList(al_l);
				requestBO.setDrProfile(drProfile);
			}
		}catch (NumberFormatException e) {
			// TODO: handle exception
		}
		try{
			if(request.getParameter("vprofileSize")!=null && request.getParameter("vprofileSize")!=""&& Integer.parseInt(request.getParameter("vprofileSize")) > 0)
			{
				ArrayList al_l = new ArrayList();
				String drProfile="";
				for(int i=0;i<Integer.parseInt(request.getParameter("vprofileSize"));i++)
				{
					if(request.getParameter("vchk"+i)!=null){
						al_l.add(request.getParameter("vchk"+i));
						drProfile = drProfile + request.getParameter("vchk"+i);
					}
				}
				requestBO.setAl_vecList(al_l);
				requestBO.setVecProfile(drProfile);
			}
		}catch (NumberFormatException e) {
			// TODO: handle exception
		}
		if (request.getParameter("Sun")==null){
			requestBO.setDays("0");
		}else {
			requestBO.setDays("1");
		}
		if(request.getParameter("Mon")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else{
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Tue")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else{
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Wed")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else {
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Thu")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else{
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Fri")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else {
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Sat")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else {
			requestBO.setDays(requestBO.getDays()+"1");
		}

		return requestBO;
	}	
	public OpenRequestBO setOpenRequestRoundTrip(OpenRequestBO requestBO,HttpServletRequest request) {
		requestBO.setEadd1(request.getParameter("sadd1"));
		requestBO.setEadd2(request.getParameter("sadd2"));
		requestBO.setEcity(request.getParameter("scity"));
		requestBO.setEstate(request.getParameter("sstate"));
		requestBO.setEzip(request.getParameter("szip"));
		requestBO.setSadd1(request.getParameter("eadd1"));
		requestBO.setSadd2(request.getParameter("eadd2"));
		requestBO.setScity(request.getParameter("ecity"));
		requestBO.setSstate(request.getParameter("estate"));
		requestBO.setSzip(request.getParameter("ezip"));
		if(request.getParameter("fromDate")==null || request.getParameter("fromDate").equals("")){
			requestBO.setSdate(request.getParameter("sdateTrip"));
		}else{
			requestBO.setSdate(request.getParameter("fromDate"));
		}
		requestBO.setShrs(request.getParameter("shrsTrip"));
		if(request.getParameter("edlatitude") != null) {
			requestBO.setSlat(request.getParameter("edlatitude"));
		} else {
			requestBO.setSlat("0");
		}
		if(request.getParameter("edlongitude") != null) {
			requestBO.setSlong(request.getParameter("edlongitude"));
		} else {
			requestBO.setSlong("0");
		}
		if(request.getParameter("sLongitude").equals("")){
			requestBO.setEdlongitude("0");		
		} else {
			requestBO.setEdlongitude(request.getParameter("sLongitude"));
		}
		if(request.getParameter("sLatitude").equals("")){
			requestBO.setEdlatitude("0");
		} else {
			requestBO.setEdlatitude(request.getParameter("sLatitude"));
		}
		return requestBO;
	}


	public void logsDetail(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String tripId=(request.getParameter("tripId")==null?"":request.getParameter("tripId"));
		ArrayList<OpenRequestHistory> ORLogs= new ArrayList<OpenRequestHistory>();
		ORLogs=RegistrationDAO.getJobLogs(tripId,adminBO);
		String jobNo;
		int n;
		if(request.getParameter("number")!=null){
			jobNo=request.getParameter("number");
			n=Integer.parseInt(jobNo)+1;
			response.getWriter().write("<font color=\"#461B7E\" size=\"2\"   style=\"\">"+n+". Trip ID:"+tripId);
		}else{
			response.getWriter().write("<font color=\"#461B7E\" size=\"2\"   style=\"\" >Trip ID:"+tripId);
		}
		if(ORLogs.size()==0 ){
			response.getWriter().write("\t No Records Found");
		} else {
			//response.getWriter().write("<table id=\"bodypage\" width=\"970\">");
			//response.getWriter().write("<div class='ScrollFormGrid'>");
			response.getWriter().write("<table id=\"logs\" width=\"100%\" style=\"background-color:rgba(175, 226, 170, 0.4);\" align=\"center\" > <tr style=\"color: rgb(248, 128, 23)\" style=\"font-weight:bold\"> <td>Action</td> <td>Created By</td> <td>Time</td>"+"</tr>");
			for(int i=0;i<ORLogs.size();i++){
				response.getWriter().write("<tr><td align=\"center\" style=\"background-color: white;white-space:pre-wrap\" ><font color=\"black\">"+ORLogs.get(i).getReason()+"</font></td><td align=\"center\" style=\"background-color: white\"><font color=\"black\">"+ORLogs.get(i).getUserId()+"</font></td><td align=\"center\" style=\"background-color: white\" ><font color=\"black\">"+ORLogs.get(i).getTime()+"</font></td></tr>");
			}
			response.getWriter().write("</table>");
			//response.getWriter().write("</div>");
			//response.getWriter().write("</table>");
		}
		response.getWriter().write("</font>");
	}
	public void driverDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<DriverLocationBO> drivers = new ArrayList<DriverLocationBO>();
		StringBuffer buffer=new StringBuffer();
		drivers = UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 1,adminBO.getTimeZone(),"");

		for(int i=0;i<drivers.size();i++){
			buffer.append("D="+drivers.get(i).getDriverid()+';'+"S="+drivers.get(i).getStatus()+"^");
		}

		response.getWriter().write(buffer.toString());
	}



	public void getLatLongi(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String tripId=(request.getParameter("tripId")==null?"":request.getParameter("tripId"));
		ArrayList<OpenRequestBO> driverLocation= new ArrayList<OpenRequestBO>();
		driverLocation=RegistrationDAO.getLatLongiForTripId(adminBO.getMasterAssociateCode(),tripId);
		if(driverLocation.size()>0){
			request.setAttribute("driverLocation",driverLocation);
			request.setAttribute("screen", "/jsp/driverLocationforaTrip.jsp");
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/driverLocationforaTrip.jsp");
		requestDispatcher.forward(request, response);
	}
	public void updateDispatchType(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String zoneNumber=(request.getParameter("zoneNumber")==null?"":request.getParameter("zoneNumber"));
		String dispatchType=(request.getParameter("dispatchType")==null?"":request.getParameter("dispatchType"));
		int result=ZoneDAO.updateZoneDispatch(adminBO.getAssociateCode(),zoneNumber,dispatchType);
		if(result==1){
			response.getWriter().write("Dispatch Type Updated Successfully");
		} else {
			response.getWriter().write("Fail To Update Dispatch Type");
		}
	}
	public void insertSharedRide(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String tripId=(request.getParameter("tripId")==null?"":request.getParameter("tripId"));
		String driverId=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
		String routeNumber=(request.getParameter("routeNumber")==null?"":request.getParameter("routeNumber"));
		String[] finalTrip=null;
		ArrayList<String> tripIds=new ArrayList<String>();
		if(!tripId.equals("")){
			finalTrip=tripId.split(";");
			for(int i=0;i<finalTrip.length;i++){
				tripIds.add(finalTrip[i]);
			}
		}
		int jobAllocated=RequestDAO.updateJobDriver(adminBO.getAssociateCode(),driverId,tripIds,routeNumber,1);
		if(jobAllocated==0){
			response.getWriter().write("002;");
		} else {
			response.getWriter().write("001;");
		}
	}
	public void getCompletedJobs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{ 
		String phoneNum = request.getParameter("phone");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		ArrayList<OpenRequestBO> al_list  = RegistrationDAO.getCompletedTrips(TDSValidation.getDBPhoneFormat(phoneNum), adminBO); 
		request.setAttribute("prevopenrequests", al_list);
		request.setAttribute("screen", "/jsp/OpenRequestCompleted.jsp");
		getServletContext().getRequestDispatcher("/jsp/OpenRequestCompleted.jsp").forward(request, response);
	}


	public void reverseDisbursement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		String disbursementId=request.getParameter("disbursementKey");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		int result=ChargesDAO.reverseDisbursement(disbursementId,adminBO);
		//System.out.println(result);
		if(result>=1){
			response.getWriter().write("001");
		} else {
			response.getWriter().write("002");
		}
	}
	public void saveOpenRequestHTML(HttpServletRequest request, HttpServletResponse response,ServletConfig srvletConfig) throws ServletException, IOException { 
		OpenRequestBO openRequestBO = new OpenRequestBO();
		AdminRegistrationBO adminBo= new AdminRegistrationBO();
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");

		adminBo.setAssociateCode(request.getParameter("associationCode"));
		if(request.getParameter("timeZone")!=null){
			adminBo.setTimeZone(request.getParameter("timeZone"));
		}
		else if(request.getParameter("associationCode").equals("113")){
			adminBo.setTimeZone("US/Pacific");
		}else{
			adminBo.setTimeZone("US/Eastern");
		}
		adminBo.setTimeZoneOffet(-7);
		adminBo.setUid("web"); String queueId="";
		adminBo.setMasterAssociateCode(request.getParameter("masterAssociationCode")==null?request.getParameter("associationCode"):request.getParameter("masterAssociationCode"));
		openRequestBO.setAssociateCode(adminBo.getAssociateCode());
		openRequestBO = setOpenRequest(openRequestBO, request);
		openRequestBO.setChckTripStatus(TDSConstants.newRequestDispatchProcessesNotStarted);
		request.setAttribute("openrequestBO", openRequestBO);
		if(adminBo.getAssociateCode().equals("127")){
			int time=CallerIDDAO.checkTime(openRequestBO.getSdate(),openRequestBO.getShrs(),adminBo.getTimeZone());
			if(time < 16){
				response.getWriter().write("Response=002;Reason=For Limo Service, Time Should Be Ateast 16 Hours From Now.");
				return;
			}
		}
		if(request.getParameter("tripSource")!=null&&!request.getParameter("tripSource").equalsIgnoreCase("")){
			openRequestBO.setTripSource(Integer.parseInt(request.getParameter("tripSource")));
		}else{
			openRequestBO.setTripSource(TDSConstants.web);
		}
		//System.out.println("Trip Source--->"+openRequestBO.getTripSource());
		int trip=0;
		int dispatchAdvaceTime=15*60;
		if(!openRequestBO.getPhone().equals("") && !openRequestBO.getName().equals("") && !openRequestBO.getSadd1().equals("")) {
			if(getServletConfig().getServletContext().getAttribute(adminBo.getMasterAssociateCode() + "ZonesLoaded") == null){
				//System.out.println("Loading zone for the first time for company by user for html" + adminBo.getMasterAssociateCode());
				SystemUtils.reloadZones(this, adminBo.getMasterAssociateCode());
			}
			if(openRequestBO.getSlat()!=null && !openRequestBO.getSlat().equals("0") && !openRequestBO.getSlong().equals("0")  ) {
				ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) srvletConfig.getServletContext().getAttribute((adminBo.getMasterAssociateCode() +"Zones"));
				queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(openRequestBO.getSlat()),Double.parseDouble(openRequestBO.getSlong()));
				if(queueId.equals("")){
					ZoneTableBeanSP zonesId = ZoneDAO.getDefaultZone(adminBo.getMasterAssociateCode());
					queueId = zonesId.getZoneKey();
					dispatchAdvaceTime = zonesId.getAdvance();
					openRequestBO.setDrProfile(zonesId.getZnProfile()!=null?zonesId.getZnProfile():openRequestBO.getDrProfile());
				} else {
					List<QueueCoordinatesBO> queueProp = SystemPropertiesDAO.getQueueCoOrdinateList(queueId, adminBo.getMasterAssociateCode(), "");
					dispatchAdvaceTime = Integer.parseInt(queueProp.get(0).getQDelayTime())*60;
					openRequestBO.setDrProfile(queueProp.get(0).getZnprofile()!=null?queueProp.get(0).getZnprofile():openRequestBO.getDrProfile());
				}
				openRequestBO.setQueueno(queueId);
			}
			if(openRequestBO.getEdlatitude()!=null && openRequestBO.getEdlatitude()!=""){
				ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) srvletConfig.getServletContext().getAttribute((adminBo.getAssociateCode() +"Zones"));
				queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(openRequestBO.getEdlatitude()),Double.parseDouble(openRequestBO.getEdlongitude()));
				if(queueId.equals("")){
					ZoneTableBeanSP zonesId = ZoneDAO.getDefaultZone(adminBo.getMasterAssociateCode());
					queueId = zonesId.getZoneKey();
				} 
				openRequestBO.setEndQueueno(queueId);
			}
			if(!openRequestBO.getVehicleNo().equals("")||!openRequestBO.getDriverid().equals("")){
				openRequestBO.setChckTripStatus(TDSConstants.srAllocatedToCab);
			}
			openRequestBO.setAirName(request.getParameter("airname")==null?"":request.getParameter("airname"));
			openRequestBO.setAirNo(request.getParameter("airno")==null?"":request.getParameter("airno"));
			openRequestBO.setAirFrom(request.getParameter("from")==null?"":request.getParameter("from"));
			openRequestBO.setAirTo(request.getParameter("to")==null?"":request.getParameter("to"));
			trip=RequestDAO.saveOpenRequest(openRequestBO,adminBo,1);
			openRequestBO.setTripid(trip+"");
			DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTime, adminBo.getMasterAssociateCode());
			if(openRequestBO.getMasterAddressKey().equals("")){
				ArrayList<CustomerProfile> custProf = AddressDAO.getCustomerProfile(openRequestBO.getPhone(), "",adminBo, 0, "");
				if(custProf.size()>0){
					openRequestBO.setMasterAddressKey(custProf.get(0).getMasterKey());
					if(openRequestBO.getLandMarkKey().equals("") && openRequestBO.getUserAddressKey().equals("")){
						RequestDAO.insertClientUser(openRequestBO, 0,adminBo.getAssociateCode());
					} else if(!openRequestBO.getLandMarkKey().equals("")){
						RequestDAO.insertClientLandMark(openRequestBO);
					}
				} else if(request.getParameter("updateStaticComment")!=null && request.getParameter("updateDriverComment")!=null){
					String dispatchComments=request.getParameter("updateStaticComment").replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", "");
					String driverComments=request.getParameter("updateDriverComment").replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", "");
					AddressDAO.insertUserAddressMaster(openRequestBO,dispatchComments,driverComments,request.getParameter("associationCode"));
				}
			} else if(!openRequestBO.getMasterAddressKey().equals("")){
				if(openRequestBO.getUserAddressKey().equals("") && request.getParameter("slandmark").equals("") && !openRequestBO.getSadd1().equals("")){
					RequestDAO.insertClientUser(openRequestBO, 0,adminBo.getAssociateCode());
				}if(request.getParameter("userAddressTo").equals("") && request.getParameter("elandmark").equals("") && !openRequestBO.getEadd1().equals("")){
					RequestDAO.insertClientUser(openRequestBO, 1,adminBo.getAssociateCode());
				}if(!openRequestBO.getLandMarkKey().equals("")){
					RequestDAO.insertClientLandMark(openRequestBO);
				}
			}
			if(!openRequestBO.getMasterAddressKey().equals("") && !openRequestBO.getUserAddressKey().equals("")){
				RequestDAO.updateClientAddress(request.getParameter("associationCode"), openRequestBO.getUserAddressKey(), openRequestBO.getMasterAddressKey());
			}	
			if(request.getParameter("updateStaticComment")!=null && !request.getParameter("updateStaticComment").equals("")){
				String dispatchComments=request.getParameter("updateStaticComment");
				AddressDAO.updateUserAddressMasterComments(dispatchComments.replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", ""),openRequestBO.getPhone(),request.getParameter("associationCode"),0,1);
			}if(request.getParameter("updateDriverComment")!=null && !request.getParameter("updateDriverComment").equals("")){
				String driverComments=request.getParameter("updateDriverComment");
				AddressDAO.updateUserAddressMasterComments(driverComments.replace("'", "").replace("P/U:", "").replace("D/O:", "").replace("Permanent Comments", "").replace("Temporary Comments", ""),openRequestBO.getPhone(),request.getParameter("associationCode"),0,2);
			}
			if(!openRequestBO.getEmail().equals("") && !openRequestBO.getAssociateCode().equals("124")){
				ArrayList<String> emails=EmailSMSDAO.emailUsers(adminBo.getMasterAssociateCode());
				if(openRequestBO.getEmail()!=null && !openRequestBO.getEmail().equals("") && emails!=null && emails.size()>0){
					String url1="http://www.getacabdemo.com";
					if(request.getServerName().contains("getacabdemo.com")){
						url1="http://www.getacabdemo.com";
					} else {
						url1="https://www.gacdispatch.com";
					}
					try {
						MailReport mailPass = new MailReport(url1, trip+"", openRequestBO.getEmail(), adminBo.getTimeZone(), emails, "jobMail" , adminBo.getMasterAssociateCode(),1,"Your trip has been booked");
						mailPass.start();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			//web booking for preAuth(cc)
			if(request.getParameter("payType")!=null && request.getParameter("payType").equals("CC")){
				PaymentProcessBean processBean = new PaymentProcessBean();
				CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(adminBo.getMasterAssociateCode());
				if(request.getParameter("transId")==null){
					processBean.setB_trip_id(openRequestBO.getTripid());
					processBean.setCvv(request.getParameter("cvvNumber"));
					processBean.setB_card_full_no(request.getParameter("ccNumber"));
					processBean.setB_amount(request.getParameter("ccAmount").equals("")?"0":request.getParameter("ccAmount"));
					processBean.setB_cardExpiryDate(request.getParameter("expDate"));
					processBean.setZip(request.getParameter("billingAddress"));
					processBean.setB_cardHolderName(request.getParameter("ccName"));
					processBean.setB_tip_amt("0");
					processBean.setB_driverid(adminBo.getUid());//Integer.parseInt
					if(cmpBean.getCcProvider()==2){
						processBean= PaymentGatewaySlimCD.makeProcess(processBean, cmpBean,ICCConstant.PREAUTH+"");
					} else {
						processBean=PaymentGateway.makeProcess(processBean,cmpBean, ICCConstant.PREAUTH+"");
					}
					if(processBean.isAuth_status()){
						int transId=ChargesDAO.insertPaymentIntoCC(processBean, request.getRealPath("/images/nosign.gif"), poolBO, adminBo, ICCConstant.PREAUTH,ICCConstant.GAC);
						response.getWriter().write("001;"+transId+"###");
					} else {
						response.getWriter().write("002;"+processBean.getReason()+"###");
					}
				} else {
					processBean=ChargesDAO.getCCDetails(adminBo.getAssociateCode(),request.getParameter("transId"),1);
					processBean.setB_driverid(adminBo.getUid());
					processBean.setB_cap_trans("");
					if(cmpBean.getCcProvider()==2){
						processBean= PaymentGatewaySlimCD.makeProcess(processBean, cmpBean, ICCConstant.PREAUTH+"");
					} else {
						processBean=PaymentGateway.makeProcess(processBean,cmpBean, ICCConstant.PREAUTH+"");
					}
					if(processBean.isAuth_status()){
						RequestDAO.deleteOpenRequest(request.getParameter("transId"), adminBo.getAssociateCode());
						response.getWriter().write("Success###");
					} else {
						response.getWriter().write("Failed###");
					}
				}
			}
			//System.out.println("trip id is for valeue" +trip);
			response.getWriter().write("Response=001;TripId="+trip);
		}

	}

	public void jobsForThisSplFlag(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		String splFlag=request.getParameter("splFlags")==null?"":request.getParameter("splFlags");
		String date=request.getParameter("date")==null?"":request.getParameter("date");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<OpenRequestBO> orList=Request2DAO.getTripForSplFlag(splFlag,date,adminBO);
		if(orList.size()>0){
			JSONArray orListForSplFlag = new JSONArray();
			for(int i=0;i<orList.size();i++){
				JSONObject jobDetails = new JSONObject();
				try {
					jobDetails.put("TI", orList.get(i).getTripid()==null?"":orList.get(i).getTripid());
					jobDetails.put("SA", orList.get(i).getSadd1()==null?"":orList.get(i).getSadd1()+" "+orList.get(i).getSadd2()+" "+orList.get(i).getScity());
					jobDetails.put("EA", orList.get(i).getEadd1()==null?"":orList.get(i).getEadd1()+" "+orList.get(i).getEadd2()+" "+orList.get(i).getEcity());
					jobDetails.put("ST", orList.get(i).getSdate());
					orListForSplFlag.put(jobDetails);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			response.setContentType("application/json");
			response.getWriter().write(orListForSplFlag.toString());
		}
	}
	public void chargesFromHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("chargesLength")!=null){
			int tripId=Integer.parseInt(request.getParameter("tripId"));
			int numberOfRows = Integer.parseInt(request.getParameter("chargesLength"));
			BigDecimal tripAmt=new BigDecimal("0");
			String[] amount=new String[numberOfRows+1];
			String[] key=new String[numberOfRows+1];
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				tripAmt=tripAmt.add(new BigDecimal(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator)));
				amount[rowIterator]=(request.getParameter("chargeAmount"+rowIterator).equals("")?"0":request.getParameter("chargeAmount"+rowIterator));
				key[rowIterator]=request.getParameter("chargeKey"+rowIterator);
			}
			int result=ChargesDAO.insertDriverCharge(adminBO.getAssociateCode(),amount,key,tripId,numberOfRows,"",adminBO.getMasterAssociateCode());
			if(result==1){
				RequestDAO.updateTripAmt(request.getParameter("tripId"), adminBO.getAssociateCode(), tripAmt,"no",adminBO.getMasterAssociateCode());
			}
		}
	}
	public void getSharedSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String date = request.getParameter("date")!=null? request.getParameter("date"):"";
		ArrayList<OpenRequestBO> al_list = SharedRideDAO.getSharedRideGroups(adminBo.getAssociateCode(), adminBo.getTimeZone(),date);
		JSONArray array = new JSONArray();
		for (int i = 0; i < al_list.size(); i++) {
			JSONObject address = new JSONObject();
			try {
				address.put("RT", al_list.get(i).getRouteNumber()!=null?al_list.get(i).getRouteNumber():"");
				address.put("CB", al_list.get(i).getCreatedBy()!=null? al_list.get(i).getCreatedBy():"");
				address.put("T", al_list.get(i).getCreatedTime()!=null?al_list.get(i).getCreatedTime():"");
				address.put("D", al_list.get(i).getCreatedDate()!=null?al_list.get(i).getCreatedDate():"");
				address.put("TT", al_list.get(i).getTotal()!=null?al_list.get(i).getTotal():"");
				address.put("DRI", al_list.get(i).getDriverid()!=null?al_list.get(i).getDriverid():"");
				address.put("STA", al_list.get(i).getShRideStatus()!=null?al_list.get(i).getShRideStatus():"");
				address.put("V", al_list.get(i).getVehicleNo()!=null?al_list.get(i).getVehicleNo():"");
				array.put(address);
			}catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		String responseString = array.toString();
		response.setContentType("application/json");
		response.getWriter().write(responseString.toString());
	}
	public void sharedRideIndividualScreen(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		JSONArray array = new JSONArray();
		String date = request.getParameter("date");
		ArrayList<OpenRequestBO> Address = AdministrationDAO.getOpnReqDashForShared(adminBO.getAssociateCode(), 0,adminBO.getTimeZone(), 0, date);
		for (int i = 0; i < Address.size(); i++) {
			if(Address.get(i).getTypeOfRide()!=null&&Address.get(i).getTypeOfRide().equalsIgnoreCase("1")){
				JSONObject address = new JSONObject();
				try {
					address.put("D", Address.get(i).getDriverid() == null ? "" : Address.get(i).getDriverid());
					address.put("V", Address.get(i).getVehicleNo() == null ? "" : Address.get(i).getVehicleNo());
					address.put("N", Address.get(i).getName() == null ? "" : Address.get(i).getName());
					address.put("T", Address.get(i).getTripid() == null ? "" : Address.get(i).getTripid());
					address.put("Q", Address.get(i).getQueueno() == null ? "" : Address.get(i).getQueueno());
					address.put("SAD",Address.get(i).getSadd1() == null ? "" : Address.get(i).getSadd1());
					address.put("EAD",Address.get(i).getEadd1() == null ? "" : Address.get(i).getEadd1());
					address.put("SCTY", Address.get(i).getScity() == null ? "" : Address.get(i).getScity());
					address.put("ECTY",Address.get(i).getEcity() == null ? "" : Address.get(i).getEcity());
					address.put("PT", Address.get(i).getPendingTime() == null ? "" : Address.get(i).getPendingTime());
					address.put("P", Address.get(i).getPhone() == null ? "" : Address.get(i).getPhone());
					address.put("S", Address.get(i).getTripStatus() == null ? "" : Address.get(i).getTripStatus());
					address.put("ST", Address.get(i).getRequestTime() == null ? "" : Address.get(i).getRequestTime());
					address.put("SD", Address.get(i).getSdate() == null ? "" : Address.get(i).getSdate());
					address.put("LA", Address.get(i).getSlat() == null ? "" : Address.get(i).getSlat());
					address.put("LO", Address.get(i).getSlong() == null ? "" : Address.get(i).getSlong());
					address.put("DD", Address.get(i).getDontDispatch());
					address.put("SR", Address.get(i).getTypeOfRide() == null ? "" : Address.get(i).getTypeOfRide());
					address.put("AC", Address.get(i).getPaytype() == null ? "" : Address.get(i).getPaytype());
					address.put("PC", Address.get(i).getPremiumCustomer() == 0 ? "" : Address.get(i).getPremiumCustomer());
					address.put("RT", Address.get(i).getRouteNumber() == null ? "" : Address.get(i).getRouteNumber());

					address.put("EDQ", Address.get(i).getEndQueueno() == null ? "" : Address.get(i).getEndQueueno());
					address.put("LMN", Address.get(i).getSlandmark() == null ? "" : Address.get(i).getSlandmark());
					address.put("ELMN", Address.get(i).getElandmark() == null ? "" : Address.get(i).getElandmark());
					address.put("COMM", Address.get(i).getComments() == null ? "" : Address.get(i).getComments());

					address.put("REF", Address.get(i).getRefNumber());
					address.put("REF1", Address.get(i).getRefNumber1());
					address.put("REF2", Address.get(i).getRefNumber2());
					address.put("REF3", Address.get(i).getRefNumber3());

					address.put("CF", Address.get(i).getCustomField());

					address.put("DRLAT", Address.get(i).getEdlatitude());
					address.put("DRLON", Address.get(i).getEdlongitude());

					String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getMasterAssociateCode(), Address.get(i).getDrProfile(), ";", 0);

					address.put("DP", driverFlagValue);
					array.put(address);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}}
		String responseString = array.toString();
		response.setContentType("application/json");
		response.getWriter().write(responseString.toString());
	}

	public void readJobStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String statusForJob="";
		if(request.getParameter("tripId")!=null && !request.getParameter("tripId").equals("")){
			statusForJob=AuditDAO.getJobLogs(adminBO.getMasterAssociateCode(),request.getParameter("tripId"));
		}
		response.getWriter().write(statusForJob+"###");
	}
	public void callDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		OpenRequestBO openBO=RequestDAO.getOpenRequestByTripID(request.getParameter("tripId"), adminBO.getMasterAssociateCode(), adminBO.getTimeZone());
		if(!openBO.getDriverid().equals("")){
			String[] message = MessageGenerateJSON.generateMessage(openBO, "TCB", System.currentTimeMillis(), adminBO.getMasterAssociateCode());
			HandleMemoryMessage.sendMessage(getServletContext(), adminBO.getAssociateCode(), openBO.getDriverid(), message[0]);
			ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
			DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getMasterAssociateCode(), openBO.getDriverid(), 1);
			Messaging oneSMS = new Messaging(getServletContext(),cabQueueBean, message,poolBO,adminBO.getAssociateCode());
			oneSMS.start();
		}
	}
	public void driverList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<DriverCabQueueBean> driverBO = new ArrayList<DriverCabQueueBean>();
		if(request.getParameter("tripId")!=null && !request.getParameter("tripId").equals("")){
			//System.out.println("Having Trip Id--->"+request.getParameter("tripId"));
			driverBO = DispatchDAO.getJobLocation(request.getParameter("assoccode"), request.getParameter("tripId"));
		} else {
			driverBO = DispatchDAO.allDrivers(request.getParameter("assoccode"), 0.00, 0.00, 0.00, null, "");
		}
		JSONArray customerAccountRegistration= new JSONArray();
		for (int i = 0; i < driverBO.size(); i++) {
			JSONObject registrationStatus = new JSONObject();
			try {
				registrationStatus.put("getLatitude",driverBO.get(i).getCurrentLatitude());
				registrationStatus.put("getLongitude",driverBO.get(i).getCurrentLongitude());
				registrationStatus.put("getCabNo", driverBO.get(i).getVehicleNo());
				customerAccountRegistration.put(registrationStatus);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.setContentType("application/json");
		response.getWriter().write(customerAccountRegistration.toString());	
	}
	private void getDriverDocument(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Blob blobData = null;
		String fileName = "";
		String query = "";
		String driverId = request.getParameter("driverId") != null ? request.getParameter("driverId") : "";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if (!driverId.equals("")) {
				query = "SELECT DOC_DATA,DOCUMENT_NAME FROM TDS_DOCUMENTS WHERE DOC_TYPE = 'Driver Photo' AND DOC_UPLOADED_BY='"+driverId+"'";
				pst = con.prepareStatement(query);
				rs = pst.executeQuery();
				while (rs.next()) {
					blobData = rs.getBlob(1);
					fileName = rs.getString(2);
				}
				response.setContentType(getServletContext().getMimeType(fileName));
				OutputStream outStream = response.getOutputStream();
				InputStream in = blobData.getBinaryStream();
				int nextByte = in.read();
				while (nextByte != -1) {
					outStream.write(nextByte);
					nextByte = in.read();
				}
				outStream.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
	}

	public void getSharedRideSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBo = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<OpenRequestBO> al_list = SharedRideDAO.getSharedRide(adminBo);
		request.setAttribute("Jobs", al_list);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/AllocateSharedRide.jsp");
		requestDispatcher.forward(request, response);
	}

	public void getCurrentZone(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String assoccode = request.getParameter("ccode");
		String latti = request.getParameter("latti");
		String longi = request.getParameter("longi");
		if(assoccode==null || latti==null || longi==null ){
			System.out.println("skipped for null values");
			response.getWriter().write("Failed");
			return;
		}
		
		if(getServletConfig().getServletContext().getAttribute(assoccode + "ZonesLoaded") == null){
			SystemUtils.reloadZones(this, assoccode);
		}
		ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((assoccode +"Zones"));
		String queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(latti),Double.parseDouble(longi));
		String queueName = "";
		if(queueId.equals("")){
			ZoneTableBeanSP zonesId = ZoneDAO.getDefaultZone(assoccode);
			queueId = zonesId.getZoneKey();
			queueName = zonesId.getZoneDesc();
		}else {
			System.out.println("Loading defalut zone");
			List<QueueCoordinatesBO> queueProp = SystemPropertiesDAO.getQueueCoOrdinateList(queueId, assoccode, "");
			queueName = queueProp.get(0).getQDescription();
		}

		JSONArray array = new JSONArray();
		
		try{
			JSONObject jobj = new JSONObject();
			
			jobj.put("QN", queueId);
			jobj.put("QD", queueName);
			
			array.put(jobj);
		}catch(JSONException e){
			e.printStackTrace();
		}
		
		System.out.println("currentZoneID res : "+array.toString());
		response.getWriter().write(array.toString());
	}
}
