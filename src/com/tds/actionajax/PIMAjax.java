package com.tds.actionajax;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.dao.DispatchDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.DriverLocationBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class PIMAjax
 */
public class PIMAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PIMAjax() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			doProcess(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String eventParam = "";

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if(request.getParameter("compCode")!=null){
			if(request.getParameter("pass")!=null && request.getParameter("pass").equalsIgnoreCase("PaulDavid")){
				if(eventParam.equals("getDriver")){
					getAllDrivers(request,response);
				} else if (eventParam.equals("jobStatus")) {
					getJobStatus(request,response);
				}
			} else {
				response.getWriter().write("400:Password Missing/Mismatch");
			}
		}else {
			response.getWriter().write("401:Confirm Your Company");
		}
	}

	public void getAllDrivers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException { 

		JSONArray array = new JSONArray();
		String driverId = request.getParameter("dId")==null?"":request.getParameter("dId");
		int dStatus = Integer.parseInt(request.getParameter("status")==null?"0":request.getParameter("status"));
		ArrayList<DriverLocationBO> dlBean = new ArrayList<DriverLocationBO>();
		if(!driverId.equals("")){
			dlBean = UtilityDAO.getDriverLocationListByStatus(request.getParameter("compCode"), 0 , "", driverId);
		} else if (dStatus!=0){
			dlBean = UtilityDAO.getDriverLocationListByStatus(request.getParameter("compCode"), dStatus , "", "");
		}
		if(dlBean!=null && dlBean.size()>0){
			for(int i=0;i<dlBean.size();i++){
				JSONObject address = new JSONObject();
				address.put("DID", dlBean.get(i).getDriverid());
				address.put("CAB", dlBean.get(i).getVehicleNo());
				address.put("LT", dlBean.get(i).getLatitude());
				address.put("LO", dlBean.get(i).getLongitude());
				address.put("NM", dlBean.get(i).getDrName());
				address.put("PH", dlBean.get(i).getPhone());
				address.put("ST", dlBean.get(i).getStatus());
				array.put(address);
			}
		}
		response.getWriter().write(array.toString());
	}

	public void getJobStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException { 

		JSONArray array = new JSONArray();
		String tripId = request.getParameter("trip")==null?"":request.getParameter("trip");
		ArrayList<DriverCabQueueBean> dlBean = new ArrayList<DriverCabQueueBean>();
		if(!tripId.equals("")){
			dlBean = DispatchDAO.getJobLocation(request.getParameter("compCode"), tripId);
		}
		if(dlBean!=null && dlBean.size()>0){
			for(int i=0;i<dlBean.size();i++){
				JSONObject address = new JSONObject();
				if(dlBean.get(i).getDriverid()!=null && !dlBean.get(i).getDriverid().equals("")){
					address.put("ST", "Job allocated to driver");
					address.put("TI", tripId);
					address.put("DID", dlBean.get(i).getDriverid());
					address.put("CAB", dlBean.get(i).getVehicleNo());
					address.put("VT", dlBean.get(i).getvMake()+"-"+dlBean.get(i).getvType());
					address.put("LT", dlBean.get(i).getCurrentLatitude());
					address.put("LO", dlBean.get(i).getCurrentLongitude());
					array.put(address);
				}else {
					address.put("ST", "Driver is not allocated yet");
					address.put("TI", tripId);
					array.put(address);
				}
			}
		} 
		response.getWriter().write(array.toString());
	}

}
