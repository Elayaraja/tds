package com.tds.actionajax;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.charges.dao.ChargesDAO;
import com.gac.mobile.dao.MobileDAO;
import com.ibm.icu.text.SimpleDateFormat;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.OpenRequestHistory;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.AddressDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.EmailSMSDAO;
import com.tds.dao.FinanceDAO;
import com.tds.dao.Registration2DAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.SharedRideDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.ZoneDAO;
import com.tds.ivr.IVRDriverCallerForJob;
import com.tds.ivr.IVRDriverPassengerConf;
import com.common.util.CheckZone;
import com.tds.process.Email;
import com.common.util.Messaging;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.tds.tdsBO.VoucherBO;
import com.common.util.HandleMemoryMessage;
import com.common.util.MessageGenerateJSON;
import com.common.util.OpenRequestUtil;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;
import com.common.util.sendSMS;

/**
 * Servlet implementation class OpenRequestAjax
 */
public class RegistrationAjax extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistrationAjax() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws Exception {

		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		String eventParam = "";
		
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if((AdminRegistrationBO)request.getSession().getAttribute("user")!=null)
		{
			if(eventParam.equalsIgnoreCase("getmapdetails")) {
				getMapDetails(request,response);	
			}else if(eventParam.equalsIgnoreCase("getLandMarkDetails")) {
				getLandMarkDetails(request,response);
			} else if(eventParam.equalsIgnoreCase("deleteLandmark")) {
				deleteLandmark(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteLandmarkPending")) {
				deleteLandmarkPending(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteCab")) {
				deleteCab(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteDriver")) {
				deleteDriver(request,response);
			}else if(eventParam.equalsIgnoreCase("tripDetails")) {
				tripDetails(request,response);
			}else if(eventParam.equalsIgnoreCase("checkDriverOrCab")) {
				checkDriverOrCab(request,response);
			}else if(eventParam.equalsIgnoreCase("getDriverLogin")) {
				getDriverLogin(request,response);
			}else if(eventParam.equalsIgnoreCase("updateOpenRequestDriverIdForShared")) {
				updateOpenRequestDriverIdForShared(request,response);
			}else if(eventParam.equalsIgnoreCase("updateOpenRequestDriverId")) {
				updateOpenRequestDriverId(request,response);
			}else if(eventParam.equalsIgnoreCase("contactUs")) {
				contactUs(request,response);
			}else if(eventParam.equalsIgnoreCase("getPaymentDetails")) {
				getPaymentDetails(request,response);
			}else if(eventParam.equalsIgnoreCase("getVoucherCharges")) {
				getVoucherCharges(request,response);
			}else if(eventParam.equalsIgnoreCase("getDriverRoute")) {
				getDriverRoute(request,response);
			}else if(eventParam.equalsIgnoreCase("unlinkVoucher")) {
				unlinkVoucher(request,response);
			}else if(eventParam.equalsIgnoreCase("getLast3Ids")) {
				getLast3Ids(request,response);
			}else if(eventParam.equalsIgnoreCase("getDrivers")) {
				getDrivers(request,response);
			}else if (eventParam.equalsIgnoreCase("callActiveDrivers")) {
				callActiveDrivers(request, response);
			}else if (eventParam.equalsIgnoreCase("driverConference")) {
				driverConference(request, response);
			}else if (eventParam.equalsIgnoreCase("getHistoryJobs")) {
				getHistoryJobs(request, response);
			}else if (eventParam.equalsIgnoreCase("reDispatchJob")) {
				reDispatchJob(request, response);
			}else if (eventParam.equalsIgnoreCase("deleteFlag")) {
				deleteFlag(request, response);
			}else if (eventParam.equalsIgnoreCase("getExtraPhones")) {
				getExtraPhones(request, response);
			}else if (eventParam.equalsIgnoreCase("enterExtraPhones")) {
				enterExtraPhones(request, response);
			}else if (eventParam.equalsIgnoreCase("updateFormula")) {
				updateFormula(request, response);
			}else if (eventParam.equalsIgnoreCase("checkPayPercent")) {
				checkPayPercent(request, response);
			}else if (eventParam.equalsIgnoreCase("checkZone")) {
				checkZone(request, response);
			}else if (eventParam.equalsIgnoreCase("checkDynamicCharges")) {
				checkDynamicCharges(request, response);
			}else if (eventParam.equalsIgnoreCase("drZoneActivity")) {
				drZoneActivity(request, response);
			}else if (eventParam.equalsIgnoreCase("searchJobs")) {
				searchJobs(request, response);
			}else if (eventParam.equalsIgnoreCase("driverRatingDetails")) {
				driverRatingDetails(request, response);
			}else if(eventParam.equalsIgnoreCase("resetDriver")) {
				resetDriver(request,response);
			}
		}if(eventParam.equalsIgnoreCase("contactUs")) {
			contactUs(request,response);
		}else if (eventParam.equalsIgnoreCase("lmList")) {
			lmList(request, response);
		}
	}
	public void getMapDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		StringBuffer buffer = new StringBuffer();
		//HttpSession session = request.getSession();
		//PrintWriter out = response.getWriter();
		OpenRequestBO openRequestBean=new OpenRequestBO();
		double latitudeAbove = 0;
		double latitudeBelow = 0;
		double longitudeAbove = 0;
		double longitudeBelow = 0;
		//double values=0.0001;

		if(request.getParameter("address")!=null && !request.getParameter("address").equals("")){
			openRequestBean.setSadd1(request.getParameter("address"));
		}
		//	if(!request.getParameter("latitude").equals("")){
		//		latitudeAbove=Double.parseDouble(request.getParameter("latitude"))+values;
		//		latitudeBelow=Double.parseDouble(request.getParameter("latitude"))-values;
		//	}
		//	if(!request.getParameter("longitude").equals("")){
		//		longitudeAbove=Double.parseDouble(request.getParameter("longitude"))+values;
		//		longitudeBelow=Double.parseDouble(request.getParameter("longitude"))-values;
		//	}
		ArrayList arr_list = RegistrationDAO.landMarkSummary(adminBO, openRequestBean,latitudeAbove,latitudeBelow,longitudeAbove,longitudeBelow);

		request.setAttribute("zonedetails", arr_list);
		if(arr_list.size()>0){
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getOpenMap);
			requestDispatcher.forward(request, response);
		} else {
			buffer.append("002;###");
			response.getWriter().write(buffer.toString());
		}

	}

	public void updateFormula(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String key=request.getParameter("key");
		String desc = request.getParameter("desc");
		String amount = request.getParameter("amt");
		String formula=request.getParameter("formula");
		ChargesDAO.updateChargeFormula(key,adminBO.getMasterAssociateCode(),formula,desc,amount);
		response.getWriter().write("Success");
	}

	public void checkPayPercent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String key=request.getParameter("keyValue");
		ArrayList<ChargesBO> charges=ChargesDAO.checkDynamicCharges(key,adminBO.getMasterAssociateCode());
		JSONArray array = new JSONArray();
		if(charges.size()>0){
			for(int i=0;i<charges.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("K", charges.get(i).getKey());
					address.put("F", charges.get(i).getDynamicFormula());
					array.put(address);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	public void checkDynamicCharges(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String key=request.getParameter("keyValue");
		ArrayList<ChargesBO> charges=ChargesDAO.getDynamicCharges(key,adminBO.getMasterAssociateCode());
		JSONArray array = new JSONArray();
		if(charges.size()>0){
			for(int i=0;i<charges.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("K", charges.get(i).getKey());
					address.put("F", charges.get(i).getDynamicFormula());
					address.put("A", charges.get(i).getAmount());
					array.put(address);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	public void getDriverRoute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		DriverLocationBO dlBo=new DriverLocationBO();
		dlBo.setDriverid(request.getParameter("driverId"));
		ArrayList<DriverLocationBO> al_list = UtilityDAO.searchDrivers(adminBO.getAssociateCode(),dlBo);
		JSONArray array = new JSONArray();
		if(al_list.size()>0){
			for(int i=0;i<al_list.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("LA", al_list.get(i).getLatitude());
					address.put("LO", al_list.get(i).getLongitude());
					array.put(address);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	public void getExtraPhones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String primaryPhone=request.getParameter("primaryNumber");
		ArrayList al_list = AddressDAO.searchPhones(adminBO.getMasterAssociateCode(),primaryPhone);
		JSONArray array = new JSONArray();
		if(al_list.size()>0){
			for(int i=0;i<al_list.size();i++){
				JSONObject extraNumbers = new JSONObject();
				try {
					extraNumbers.put("Ph", al_list.get(i));
					array.put(extraNumbers);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		response.getWriter().write(array.toString());
	}
	public void enterExtraPhones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String custNum=request.getParameter("primaryNumber");
		//System.out.println("Cust Num--->"+custNum);
		int totalNumbers=Integer.parseInt(request.getParameter("totalNum"));
		String[] phones = new String[totalNumbers];
		for(int i=0;i<totalNumbers;i++){
			phones[i] = request.getParameter("number"+i);
		}
		AddressDAO.submitPhones(adminBO.getMasterAssociateCode(),custNum,phones);
	}

	public void getPaymentDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		int tripId=Integer.parseInt(request.getParameter("tripId"));
		ArrayList<VoucherBO> al_list = FinanceDAO.getPaymentByTrip(adminBO.getAssociateCode(),tripId,adminBO.getMasterAssociateCode());
		JSONArray array = new JSONArray();
		if(al_list!=null && al_list.size()>0){
			for(int i=0;i<al_list.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("D", al_list.get(i).getVdesc()==null?"":al_list.get(i).getVdesc());
					address.put("A", al_list.get(i).getVamount()==null?"":al_list.get(i).getVamount());
					address.put("N", al_list.get(i).getVno()==null?"":al_list.get(i).getVno());
					address.put("S", al_list.get(i).getVstatus()==null?"":al_list.get(i).getVstatus());
					address.put("T", al_list.get(i).getTip()==null?"":al_list.get(i).getTip());
					address.put("PV", al_list.get(i).getVcode()==null?"":al_list.get(i).getVcode());
					address.put("TXN",al_list.get(i).getVtrans()==null?"":al_list.get(i).getVtrans());
					array.put(address);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} else {
			JSONObject address = new JSONObject();
			try {
				address.put("D", "Failed");
				address.put("N", "Failed");
				address.put("A", "Failed");
				address.put("S", "Failed");
				address.put("T", "Failed");
				address.put("PV", "Failed");
				address.put("TXN","Failed");
				array.put(address);			
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	public void getLandMarkDetails(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String screen = ""; 
		//HttpSession session = request.getSession();
		//OpenRequestBO openRequestBO = new OpenRequestBO();
		String land="";
		String select=request.getParameter("type");
		if(request.getParameter("landMark") != null && request.getParameter("landMark") != "") {
			land= request.getParameter("landMark");
		}

		ArrayList al_list = new ArrayList(); 
		al_list = RequestDAO.getlandMarkDetails(land,adminBO.getMasterAssociateCode()); 
		request.setAttribute("type", select);
		request.setAttribute("landmarkDetails", al_list);
		if(!request.getParameter("Screen").equalsIgnoreCase("OR")){
			request.setAttribute("type", select);
			screen ="/jsp/landmarkDash.jsp";
		} else {
			screen = TDSConstants.viewlandmarkJSP; 
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(screen);
		requestDispatcher.forward(request, response);
	}
	public void deleteLandmark(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {

		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("delete")!=null){
			String LMKey=(request.getParameter("LMKey")==null?"":request.getParameter("LMKey"));
			int result=RegistrationDAO.deleteLandMark(adminBO,LMKey);
			if(result==1){
				response.getWriter().write("Deleted Successfully");
			} else {
				response.getWriter().write("Delete Unsuccessful");
			}

		}

	}
	public void deleteLandmarkPending(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<String> lmKeys=new ArrayList<String>();
		if(request.getParameter("delete")!=null){
			String LMKey=request.getParameter("SerialNumber");
			String[] pendingLM=null;
			if(!LMKey.equals("")){
				pendingLM=LMKey.split(";");
				for(int i=0;i<pendingLM.length;i++){
					lmKeys.add(pendingLM[i]);
				}
			}
			int result=RegistrationDAO.deleteLandMarkPending(adminBO,lmKeys);
			if(result==1){
				response.getWriter().write("Deleted Successfully");
			} else {
				response.getWriter().write("Delete Unsuccessful");
			}
		}
	}

	public void deleteCab(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String cabNum=(request.getParameter("cabNum")==null?"":request.getParameter("cabNum"));
		int result=RegistrationDAO.deleteCab(adminBO,cabNum);
		if(result==1){
			response.getWriter().write("1");
		} else {
			response.getWriter().write("0");
		}
	}

	public void deleteFlag(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String flag=(request.getParameter("flagValue")==null?"":request.getParameter("flagValue"));
		boolean result=RegistrationDAO.deleteFlag(flag, adminBO.getMasterAssociateCode());
		SystemUtils.reloadCompanyFlags(getServletConfig(), adminBO.getMasterAssociateCode());
		if(result){
			response.getWriter().write("1");
		} else {
			response.getWriter().write("0");
		}
	}

	public void tripDetails(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		int mode=Integer.parseInt((request.getParameter("mode")==null?"":request.getParameter("mode")));
		if(mode==1){
			String phoneNumber=(request.getParameter("phoneNumber")==null?"":request.getParameter("phoneNumber"));
			String fromDate=(request.getParameter("fromDate")==null?"":request.getParameter("fromDate"));
			String toDate=(request.getParameter("toDate")==null?"":request.getParameter("toDate"));
			ArrayList<OpenRequestHistory> ORDetails=RegistrationDAO.getDetailsForJobs(phoneNumber,adminBO,fromDate,toDate,mode);
			if(ORDetails.size()==0 ){
				response.getWriter().write("No Records Found");
			} else {
				response.getWriter().write("<table id=\"logs\" width=\"440\"><tr><td></td><td class=\"firstCol\">TripID</td>	<td class=\"firstCol\">Time</td><td class=\"firstCol\">DriverID</td>"+"</td><td class=\"firstCol\">CabNo</td>"+"</td></tr>");
				for(int i=0;i<ORDetails.size();i++){
					response.getWriter().write("<tr><td align=\"center\" style=\"background-color: lightgrey\"><input type=\"button\" name=\"button\" value=\"select\" onclick=\"fixValues('"+ORDetails.get(i).getTripid()+"','"+ORDetails.get(i).getDriverId()+"','"+ORDetails.get(i).getVehicleNumber()+"','"+ORDetails.get(i).getTime()+"','"+ORDetails.get(i).getDate()+"',1);\"/><input type=\"hidden\" name=\"tripid_"+ORDetails.get(i).getTripid()+"\" value="+ORDetails.get(i).getTripid()+"/></td><input type=\"hidden\" name=\"driverId_"+ORDetails.get(i).getTripid()+"\" id=\"driverId_"+ORDetails.get(i).getTripid()+"\" value="+ORDetails.get(i).getDriverId()+"/></td><input type=\"hidden\" name=\"cabNo_"+ORDetails.get(i).getTripid()+"\" id=\"cabNo_"+ORDetails.get(i).getTripid()+"\" value="+ORDetails.get(i).getVehicleNumber()+"/></td><input type=\"hidden\" name=\"serviceTime_"+ORDetails.get(i).getTripid()+"\" id=\"serviceTime_"+ORDetails.get(i).getTripid()+"\" value="+ORDetails.get(i).getTime()+"/></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">"+ORDetails.get(i).getTripid()+"</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">"+ORDetails.get(i).getTime()+"</font></td><td align=\"center\" style=\"background-color: lightgrey\" ><font color=\"black\">"+ORDetails.get(i).getDriverId()+"</font></td><td align=\"center\" style=\"background-color: lightgrey\" ><font color=\"black\">"+ORDetails.get(i).getVehicleNumber()+"</font></td></tr>");
				}
				response.getWriter().write("</table>");
			}
		}else if(mode==2){
			String fromDate=(request.getParameter("fromDate")==null?"":request.getParameter("fromDate"));
			String toDate=(request.getParameter("toDate")==null?"":request.getParameter("toDate"));
			String driverId=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));

			ArrayList<OpenRequestHistory> ORDetails=RegistrationDAO.getDetailsForJobs(driverId,adminBO,fromDate,toDate,mode);
			if(ORDetails.size()==0 ){
				response.getWriter().write("No Records Found");
			} else {
				response.getWriter().write("<table id=\"logs\" width=\"440\"><tr><td></td><td class=\"firstCol\">TripID</td>	<td class=\"firstCol\">Time</td><td class=\"firstCol\">RiderName</td>"+"</td><td class=\"firstCol\">PhoneNumber</td>"+"</td></tr>");
				for(int i=0;i<ORDetails.size();i++){
					response.getWriter().write("<tr><td align=\"center\" style=\"background-color: lightgrey\"><input type=\"button\" name=\"button\" value=\"select\" onclick=\"fixValues('"+ORDetails.get(i).getTripid()+"','"+ORDetails.get(i).getName()+"','"+ORDetails.get(i).getPhoneNumber()+"','"+ORDetails.get(i).getTime()+"','"+ORDetails.get(i).getDate()+"',2);\"/><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">"+ORDetails.get(i).getTripid()+"</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">"+ORDetails.get(i).getTime()+"</font></td><td align=\"center\" style=\"background-color: lightgrey\" ><font color=\"black\">"+ORDetails.get(i).getName()+"</font></td><td align=\"center\" style=\"background-color: lightgrey\" ><font color=\"black\">"+ORDetails.get(i).getPhoneNumber()+"</font></td></tr>");
				}
				response.getWriter().write("</table>");
			}
		}
	}



	public void deleteDriver(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String driverNum=(request.getParameter("driverNum")==null?"":request.getParameter("driverNum"));
		int driverOrOperator=Integer.parseInt((request.getParameter("driverOrOperator")==null?"":request.getParameter("driverOrOperator")));
		int result=RegistrationDAO.deleteDriver(adminBO,driverNum,driverOrOperator);
		if(result==1){
			response.getWriter().write("1");
		} else {
			response.getWriter().write("0");
		}
	}
	public void checkDriverOrCab(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		StringBuffer buffer=new StringBuffer();
		String driverId=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
		String cabNo=(request.getParameter("cabNo")==null?"":request.getParameter("cabNo"));
		cabNo=cabNo.replace("F", "").replace("f", "");
		driverId=driverId.replace("F", "").replace("f", "");
		ArrayList driverList=RegistrationDAO.getdriverlogdetails(driverId, adminBO.getAssociateCode(),cabNo,1,adminBO.getTimeZone());
		if(driverList!=null && driverList.size()>0){
			buffer.append("001;Driver "+driverList.get(0)+" With Cab "+driverList.get(4)+" allocated to this job###");
		} else {
			buffer.append("002;Warning:The Driver/Cab entered is logged out.Will allocate to the driver on login###");
		}
		response.getWriter().write(buffer.toString());
	}
	public void getDriverLogin(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String driverId=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
		String cabNumber=(request.getParameter("cabNum")==null?"":request.getParameter("cabNum"));
		String result="";
		StringBuffer buffer=new StringBuffer();
		if(!driverId.equals("")||!cabNumber.equals("")){
			if(driverId!=null && !driverId.equals("")){
				driverId=driverId.replace("F","").replace("f","");
			}
			if(cabNumber!=null && !cabNumber.equals("")){
				cabNumber=cabNumber.replace("F", "").replace("f","");
			}
			result=RegistrationDAO.getCabNumber(driverId,adminBO.getAssociateCode(),cabNumber);
		}
		buffer.append(result==null?"":result+"###");
		response.getWriter().write(buffer.toString());
	}

	public void updateOpenRequestDriverId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String driverId=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
		String cabNumber=(request.getParameter("cabNumber")==null?"":request.getParameter("cabNumber"));
		String tripId=(request.getParameter("tripId")==null?"":request.getParameter("tripId"));
		StringBuffer buffer=new StringBuffer();
		int result=0;
		if((driverId!=null && !driverId.equals("")) || (cabNumber!=null && !cabNumber.equals(""))){
			if(driverId.contains("F") || driverId.contains("f")|| (cabNumber!=null && (cabNumber.contains("F") || cabNumber.contains("f") )) ){
				driverId=driverId.replace("F","");
				driverId=driverId.replace("f","");
				cabNumber=cabNumber.replace("F", "");
				cabNumber=cabNumber.replace("f", "");
				result=RegistrationDAO.updateJobDriver(adminBO.getAssociateCode(),driverId,tripId,cabNumber,1);
				int status=RequestDAO.getDriverLoginStatus(driverId, adminBO.getAssociateCode());
				if(status>=1){
					OpenRequestBO openBO=RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());
					String[] message = MessageGenerateJSON.generateMessage(openBO, "AA", System.currentTimeMillis(), adminBO.getAssociateCode());
					HandleMemoryMessage.sendMessage(getServletContext(), adminBO.getAssociateCode(), driverId, message[0]);
				}
				String driverPhone=MobileDAO.getPhoneNum(driverId, adminBO.getMasterAssociateCode());
				sendSMS.main("Reservation Job "+tripId+" Is Allocated To You", adminBO.getSmsPrefix()+driverPhone, adminBO.getMasterAssociateCode());
			}else{
				result=RegistrationDAO.updateJobDriver(adminBO.getAssociateCode(),driverId,tripId,cabNumber,0);
			}
		}
		buffer.append(result+"###");
		response.getWriter().write(buffer.toString());

	}
	public void updateOpenRequestDriverIdForShared(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String driverId=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
		String cabNumber=(request.getParameter("cabNumber")==null?"":request.getParameter("cabNumber"));
		String routeNo=(request.getParameter("routeNo")==null?"":request.getParameter("routeNo"));
		StringBuffer buffer=new StringBuffer();
		int result=0;

		if((driverId!=null && !driverId.equals("")) || (cabNumber!=null && !cabNumber.equals(""))){
			if(driverId.contains("F") || driverId.contains("f")|| (cabNumber!=null && (cabNumber.contains("F") || cabNumber.contains("f") )) ){
				driverId=driverId.replace("F","");
				driverId=driverId.replace("f","");
				cabNumber=cabNumber.replace("F", "");
				cabNumber=cabNumber.replace("f", "");
				result=RegistrationDAO.updateJobDriverForShared(adminBO.getAssociateCode(),driverId,routeNo,cabNumber,1);
				if(driverId.equals("")&&!cabNumber.equals("")){
					driverId = RegistrationDAO.getCabNumber(driverId, adminBO.getAssociateCode(), cabNumber);
				}
				int status=RequestDAO.getDriverLoginStatus(driverId, adminBO.getAssociateCode());
				if(status>=1){
					ArrayList<OpenRequestBO> routeTrips  = SharedRideDAO.getOpenRequestForRouteByTripID (routeNo, adminBO,"1");
					//OpenRequestBO openBO=RequestDAO.getOpenRequestByTripID(tripId, adminBO.getAssociateCode(), adminBO.getTimeZone());
					String[] message = MessageGenerateJSON.generateMessage(routeTrips.get(0), "AA", System.currentTimeMillis(), adminBO.getAssociateCode());
					HandleMemoryMessage.sendMessage(getServletContext(), adminBO.getAssociateCode(), driverId, message[0]);
					DriverCabQueueBean cabQueueBean = null;
					ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
					cabQueueBean = DispatchDAO.getDriverByDriverID(adminBO.getMasterAssociateCode(), driverId, 1);
					Messaging oneSMS = new Messaging(getServletContext(),cabQueueBean, message,poolBO,adminBO.getAssociateCode());
					oneSMS.start();
				}
			}else{
				result=RegistrationDAO.updateJobDriverForShared(adminBO.getAssociateCode(),driverId,routeNo,cabNumber,0);
			}
		}


		buffer.append(result+"###");
		response.getWriter().write(buffer.toString());

	}

	public void contactUs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = new AdminRegistrationBO();
		if(request.getSession().getAttribute("user")!=null){
			adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		} else {
			adminBO.setAssociateCode("106");
		}
		if(request.getParameter("content")!=null && !request.getParameter("content").equals("")){
			ArrayList<String> emails=new ArrayList<String>();
			emails=EmailSMSDAO.emailUsers(adminBO.getAssociateCode());
			String username=emails.get(0);
			String password=emails.get(1);
			String assoccode=emails.get(2);
			String host=emails.get(3);
			Email otherProviderSMS = new Email("smtp.emailsrvr.com",username,password,username,"465","smtp");
			try{
				otherProviderSMS.sendMail("venki@getacab.com","Query/Suggestion", request.getParameter("content"));
			} catch (Exception e ){
				System.out.println(e.toString());
			}
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/mainNew.jsp");
			requestDispatcher.forward(request, response);
		} else {
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/Email.jsp");
			requestDispatcher.forward(request, response);
		}
	}
	public void getVoucherCharges(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<ChargesBO> charges =  FinanceDAO.getChargesForVoucher(adminBO.getAssociateCode(), request.getParameter("tripId"),adminBO.getMasterAssociateCode()) ;
		JSONArray array = new JSONArray();
		if(charges.size()>0){
			for(int i=0;i<charges.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("T", charges.get(i).getPayTypeKey());
					address.put("A", charges.get(i).getAmount());
					array.put(address);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}
	public void unlinkVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		int result=FinanceDAO.unLinkVoucherFromInvoice(adminBO,request.getParameter("transId")==null?"":request.getParameter("transId"),request.getParameter("invoiceNo")==null?"":request.getParameter("invoiceNo"),request.getParameter("voucherNumber")==null?"":request.getParameter("voucherNumber"));
		response.getWriter().write(result+"");
	}
	public void getLast3Ids(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<String> last3ids= Registration2DAO.getLast3Ids(adminBO);
		response.getWriter().write("<table id=\"logs\" width=\"500px\" border=\"1\"><tr><td align=\"center\" style=\"background-color:#0000A0\" width=\"25%\"><font color=\"white\" style=\"bold\">Driver ID</font></td>");
		for(int i=0;i<last3ids.size();i++){
			response.getWriter().write("<td align=\"center\" style=\"background-color: lightgrey\" width=\"25%\"><font color=\"black\">"+last3ids.get(i)+"</font></td>");
		}
		response.getWriter().write("</table>");
	}
	public void getDrivers(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList al_List=UtilityDAO.getDriverList(adminBO.getAssociateCode(), request.getParameter("driver").trim(), 0);
		request.setAttribute("position", request.getParameter("position"));
		request.setAttribute("driverDetails", al_List);
		String screen ="/Company/driverSummary.jsp";
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(screen);
		requestDispatcher.forward(request, response);
	}
	public void callActiveDrivers(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
		ArrayList<DriverCabQueueBean> driverDetails=MobileDAO.getActiveDrivers(m_adminBO.getMasterAssociateCode());
		if(driverDetails!=null && driverDetails.size()>0){
			IVRDriverCallerForJob ivr = new IVRDriverCallerForJob(driverDetails, m_adminBO.getMasterAssociateCode());
			ivr.start();
		}
	}
	public void driverConference(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		HttpSession m_session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
		String passengerPhone=request.getParameter("phoneNumber");
		String driverPhone="";
		if(passengerPhone!="" && !passengerPhone.equals("0000000000")){
			driverPhone=MobileDAO.getDriverPhone(passengerPhone,adminBO.getAssociateCode());
		}
		if(!driverPhone.equals("")){
			try {
				Thread.sleep(20000);
				IVRDriverPassengerConf ivr = new IVRDriverPassengerConf(passengerPhone, adminBO.getMasterAssociateCode(), driverPhone,"",adminBO.getUid());
				ivr.start();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			response.getWriter().write("001");
		} else {
			response.getWriter().write("002");
		}
	}
	public void getHistoryJobs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		OpenRequestBO openRequestHistory =new OpenRequestBO();
		JSONArray array = new JSONArray();
		StringBuffer outData = new StringBuffer("");
		String value =request.getParameter("value")==null?"":request.getParameter("value");
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		String fromDateFormatted=TDSValidation.getTimeStampDateFormat(fromDate);
		String toDateFormatted=TDSValidation.getTimeStampDateFormat(toDate);
		String driverId=request.getParameter("driverid")==null?"":request.getParameter("driverid");
		int status = Integer.parseInt((request.getParameter("status")==null||request.getParameter("status").equals(""))?"0":request.getParameter("status"));
		
		String trip_src = (request.getParameter("trip_Src")!=null && !request.getParameter("trip_Src").equals("")?request.getParameter("trip_Src"):"");
		//System.out.println("source:"+trip_src);
		String drProfile="";
		String assocCode = request.getParameter("fleetNo")!=null?request.getParameter("fleetNo"):"";
		//System.out.println("Assoccode for history:"+assocCode);
		if(request.getParameter("splFlag")!=null && !request.getParameter("splFlag").equals(""))
		{
			for(int i=0;i<Integer.parseInt(request.getParameter("splFlag"));i++)
			{
				if(request.getParameter("dchk"+i)!=null){
					drProfile = drProfile + request.getParameter("dchk"+i);
				}
			}
		}
		ArrayList<OpenRequestBO>openReqHistory=RequestDAO.openRequestHistory(openRequestHistory,adminBO,value,fromDateFormatted,toDateFormatted,drProfile,status,driverId,assocCode,trip_src);
		if(openReqHistory!=null && openReqHistory.size()>0){
			for(int i=0;i<openReqHistory.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("TI", openReqHistory.get(i).getTripid()==null?"":openReqHistory.get(i).getTripid());
					address.put("TD", openReqHistory.get(i).getSdate()==null?"":openReqHistory.get(i).getSdate());
					address.put("NA", openReqHistory.get(i).getName()==null?"":openReqHistory.get(i).getName());
					address.put("DR", openReqHistory.get(i).getDriverid()==null?"":openReqHistory.get(i).getDriverid());
					address.put("PH", openReqHistory.get(i).getPhone()==null?"":openReqHistory.get(i).getPhone());
					address.put("SA", openReqHistory.get(i).getSadd1()==null?"":openReqHistory.get(i).getSadd1());
					address.put("SA2", openReqHistory.get(i).getSadd2()==null?"":openReqHistory.get(i).getSadd2());
					address.put("SC", openReqHistory.get(i).getScity()==null?"":openReqHistory.get(i).getScity());
					address.put("EA", openReqHistory.get(i).getEadd1()==null?"":openReqHistory.get(i).getEadd1());
					address.put("EA2", openReqHistory.get(i).getEadd2()==null?"":openReqHistory.get(i).getEadd2());
					address.put("EC", openReqHistory.get(i).getEcity()==null?"":openReqHistory.get(i).getEcity());
					address.put("TS", openReqHistory.get(i).getTripStatus()==null?"":openReqHistory.get(i).getTripStatus());
					address.put("AM", openReqHistory.get(i).getAmt()==null?"0.00":openReqHistory.get(i).getAmt().toString());
					address.put("FL", openReqHistory.get(i).getAssociateCode()==null?"":openReqHistory.get(i).getAssociateCode());
					address.put("VN", openReqHistory.get(i).getVehicleNo()==null?"":openReqHistory.get(i).getVehicleNo());
					address.put("TSO", openReqHistory.get(i).getTripSource()==0?"":openReqHistory.get(i).getTripSource());
					address.put("CB", openReqHistory.get(i).getCreatedBy()==null?"":openReqHistory.get(i).getCreatedBy());
					address.put("DP", openReqHistory.get(i).getDrProfile()==null?"":openReqHistory.get(i).getDrProfile());
					address.put("VP", openReqHistory.get(i).getVecProfile()==null?"":openReqHistory.get(i).getVecProfile());
					address.put("CN", openReqHistory.get(i).getCallerName()==null?"":openReqHistory.get(i).getCallerName());
					address.put("CP", openReqHistory.get(i).getCallerPhone()==null?"":openReqHistory.get(i).getCallerPhone());
					address.put("NP", openReqHistory.get(i).getNumOfPassengers()<1?"1":openReqHistory.get(i).getNumOfPassengers());
					address.put("RF", openReqHistory.get(i).getRefNumber()==null?"":openReqHistory.get(i).getRefNumber());
					address.put("RN", openReqHistory.get(i).getRefNumber1()==null?"":openReqHistory.get(i).getRefNumber1());
					address.put("RF2", openReqHistory.get(i).getRefNumber2()==null?"":openReqHistory.get(i).getRefNumber2());
					address.put("RF3", openReqHistory.get(i).getRefNumber3()==null?"":openReqHistory.get(i).getRefNumber3());
					address.put("CM", (openReqHistory.get(i).getComments()==null||openReqHistory.get(i).getComments().equals(""))?"No Comments":openReqHistory.get(i).getComments());
					address.put("SI", (openReqHistory.get(i).getSpecialIns()==null||openReqHistory.get(i).getSpecialIns().equals(""))?"No Comments":openReqHistory.get(i).getSpecialIns());
					address.put("DRC", openReqHistory.get(i).getDrProfile()==null?"":openReqHistory.get(i).getDrProfile());
					address.put("SLA",openReqHistory.get(i).getSlat()==null?"":openReqHistory.get(i).getSlat());
					address.put("SLO",openReqHistory.get(i).getSlong()==null?"":openReqHistory.get(i).getSlong());
					address.put("ELA",openReqHistory.get(i).getEdlatitude()==null?"":openReqHistory.get(i).getEdlatitude());
					address.put("ELO",openReqHistory.get(i).getEdlongitude()==null?"":openReqHistory.get(i).getEdlongitude());
					address.put("RNO",openReqHistory.get(i).getRouteNumber()==null?"":openReqHistory.get(i).getRouteNumber());
					address.put("SITIME",openReqHistory.get(i).getOR_SMS_SENT()==null?"":openReqHistory.get(i).getOR_SMS_SENT());
					address.put("CRTIME",openReqHistory.get(i).getCreatedTime()==null?"":openReqHistory.get(i).getCreatedTime());
					address.put("COTIME",openReqHistory.get(i).getCompletedTime()==null?"":openReqHistory.get(i).getCompletedTime());
					address.put("TRS",openReqHistory.get(i).getTripStatus()==null?"":openReqHistory.get(i).getTripStatus());
					address.put("PT",openReqHistory.get(i).getPaytype()==null?"":openReqHistory.get(i).getPaytype());
					address.put("ACCT",openReqHistory.get(i).getAcct()==null?"":openReqHistory.get(i).getAcct());
					address.put("CRB",openReqHistory.get(i).getCreatedBy()==null?"":openReqHistory.get(i).getCreatedBy());
					address.put("SO",openReqHistory.get(i).getShrs()==null?"0":openReqHistory.get(i).getShrs());
					address.put("EO",openReqHistory.get(i).getSmin()==null?"0":openReqHistory.get(i).getSmin());

					if(!openReqHistory.get(i).getMaterPayment().contains("empty")){
						JSONArray meterArray = new JSONArray(openReqHistory.get(i).getMaterPayment());
						JSONArray headerArray = meterArray.getJSONArray(0);
						JSONObject meterObj = headerArray.getJSONObject(0);
						address.put("GPS", "Yes");
						if(meterObj.has("meterAmt")){
							address.put("MP", meterObj.getString("meterAmt"));
						}else{
							address.put("MP", "0.00");
						}
						if(meterObj.has("isHidden")){
							address.put("GHidden", meterObj.getString("isHidden"));
						}else{
							address.put("GHidden", "");
						}
						if(meterObj.has("tripID")){
							address.put("GTID", meterObj.getString("tripID"));
						}else{
							address.put("GTID", "");
						}
						if(meterObj.has("TimeAccuredInSecs")){
							address.put("GTAIS", meterObj.getString("TimeAccuredInSecs"));
						}else{
							address.put("GTAIS", "");
						}
						if(meterObj.has("DistanceAccuredInMlies")){
							address.put("GDAIM", meterObj.getString("DistanceAccuredInMlies"));
						}else{
							address.put("GDAIM", "");
						}
						if(meterObj.has("TotalTimeCalculated")){
							address.put("GTC", meterObj.getString("TotalTimeCalculated"));
						}else{
							address.put("GTC", "");
						}
						if(meterObj.has("TotalDistanceCalculated")){
							address.put("GDC", meterObj.getString("TotalDistanceCalculated"));
						}else{
							address.put("GDC", "");
						}
						if(meterObj.has("StartAmount")){
							address.put("GST", meterObj.getString("StartAmount"));
						}else{
							address.put("GST", "");
						}
						if(meterObj.has("MinimumSpeed")){
							address.put("GMS", meterObj.getString("MinimumSpeed"));
						}else{
							address.put("GMS", "");
						}
					}else{
						address.put("GPS", "No");
						address.put("MP", "0.00");
					}


					array.put(address);			
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			outData.append(array);
			response.setContentType("application/json");
			response.getWriter().write(outData.toString());
		}
	}


	public void reDispatchJob(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		OpenRequestBO openRequestHistory =null;
		String errorReturn="";
		if(request.getParameter("reDispatch").equalsIgnoreCase("YES")){
			String tripId = request.getParameter("tripIdHistory");
			String time= request.getParameter("time");
			OpenRequestBO openrequestBOfromAction=new OpenRequestBO();
			openrequestBOfromAction=RequestDAO.reDispatch(openRequestHistory,adminBO,tripId,time);
			Calendar currentDate = Calendar.getInstance();
			SimpleDateFormat formatter=   new SimpleDateFormat("MM/dd/yyyy");
			String dateNow = formatter.format(currentDate.getTime());
			if(openrequestBOfromAction.getEadd1()!=null && openrequestBOfromAction.getEadd1().contains("Couldnt Determine")){
				openrequestBOfromAction.setEadd1("");
				openrequestBOfromAction.setEadd2("");
			}
			openrequestBOfromAction.setSdate(dateNow);
			openrequestBOfromAction.setShrs("2525");
			openrequestBOfromAction.setDays("0000000");
			request.setAttribute("openrequestBOfromAction", openrequestBOfromAction);
			errorReturn=OpenRequestUtil.saveOpenRequest(request, response, getServletConfig());
			if(errorReturn.length() <= 6 || errorReturn.contains("OK")) { 
				errorReturn="Job Redispatched for "+openrequestBOfromAction.getName()+"  at:"+(openrequestBOfromAction.getShrs().equals("2525")?" Now":openrequestBOfromAction.getShrs()+" Hrs")+" with "+errorReturn;
			} else {
				errorReturn="Failed To Redispatch Job<br> "+errorReturn;
			}
		}
		response.getWriter().write(errorReturn);
	}

	public void checkZone(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String lati=request.getParameter("latitude");
		String longi=request.getParameter("longitude");
		ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getMasterAssociateCode() +"Zones"));
		String queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(lati),Double.parseDouble(longi));
		response.getWriter().write(queueId==null?"":queueId);
	}
	public void lmList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = new AdminRegistrationBO();
		adminBO.setAssociateCode(request.getParameter("assoccode"));
		OpenRequestBO openRequestBean = new OpenRequestBO();
		ArrayList<OpenRequestBO> al_list = RegistrationDAO.landMarkSummary(adminBO,openRequestBean,0,0,0,0); 
		JSONArray lmArray= new JSONArray();
		for (int i = 0; i < al_list.size(); i++) {
			JSONObject landMark = new JSONObject();
			try {
				landMark.put("key", al_list.get(i).getLandMarkKey());
				landMark.put("lm", al_list.get(i).getSlandmark());
				landMark.put("a1", al_list.get(i).getSadd1());
				landMark.put("a2", al_list.get(i).getSadd2());
				landMark.put("cy", al_list.get(i).getScity());
				landMark.put("st", al_list.get(i).getSstate());
				landMark.put("zi", al_list.get(i).getSzip());
				landMark.put("lt", al_list.get(i).getSlat());
				landMark.put("lo", al_list.get(i).getSlong());
				lmArray.put(landMark);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.setContentType("application/json");
		response.getWriter().write(lmArray.toString());	
	}
	public void searchJobs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String val = request.getParameter("value");
		String fromDate=TDSValidation.getTimeStampDateFormat(request.getParameter("fD"));
		String toDate=TDSValidation.getTimeStampDateFormat(request.getParameter("tD"));
		JSONArray totalArray = new JSONArray();
		JSONObject totalObject = new JSONObject();
		JSONArray summArrary = new JSONArray();
		JSONArray histArray = new JSONArray();
		ArrayList<OpenRequestBO> summJobs = DispatchDAO.getAllTrips(adminBO.getMasterAssociateCode(),val,adminBO.getTimeZone());
		ArrayList<OpenRequestBO> openReqHistory=RequestDAO.openRequestHistory(null,adminBO,val,fromDate,toDate,"",0,"","","");
		for (int i = 0; i < summJobs.size(); i++) {
			JSONObject jobs = new JSONObject();
			try {
				jobs.put("DID", summJobs.get(i).getDriverid() == null ? "" : summJobs.get(i).getDriverid());
				jobs.put("VNO", summJobs.get(i).getVehicleNo() == null ? "" : summJobs.get(i).getVehicleNo());
				jobs.put("N", summJobs.get(i).getName() == null ? "" : summJobs.get(i).getName());
				jobs.put("TI", summJobs.get(i).getTripid() == null ? "" : summJobs.get(i).getTripid());
				jobs.put("SA", summJobs.get(i).getSadd1() == null ? "" : summJobs.get(i).getSadd1());
				jobs.put("EA", summJobs.get(i).getEadd1() == null ? "" : summJobs.get(i).getEadd1());
				jobs.put("D", summJobs.get(i).getSdate() == null ? "" : summJobs.get(i).getSdate());
				jobs.put("S", summJobs.get(i).getStatus() == null ? "" : summJobs.get(i).getStatus());
				jobs.put("RN", summJobs.get(i).getRouteNumber() == null ? "" : summJobs.get(i).getRouteNumber());
				summArrary.put(jobs);
			}catch (JSONException e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < openReqHistory.size(); i++) {
			JSONObject jobs = new JSONObject();
			try {
				jobs.put("DID", openReqHistory.get(i).getDriverid() == null ? "" : openReqHistory.get(i).getDriverid());
				jobs.put("VNO", openReqHistory.get(i).getVehicleNo() == null ? "" : openReqHistory.get(i).getVehicleNo());
				jobs.put("N", openReqHistory.get(i).getName() == null ? "" : openReqHistory.get(i).getName());
				jobs.put("TI", openReqHistory.get(i).getTripid() == null ? "" : openReqHistory.get(i).getTripid());
				jobs.put("SA", openReqHistory.get(i).getSadd1() == null ? "" : openReqHistory.get(i).getSadd1());
				jobs.put("EA", openReqHistory.get(i).getEadd1() == null ? "" : openReqHistory.get(i).getEadd1());
				jobs.put("D", openReqHistory.get(i).getSdate() == null ? "" : openReqHistory.get(i).getSdate());
				jobs.put("S", openReqHistory.get(i).getStatus() == null ? "" : openReqHistory.get(i).getStatus());
				jobs.put("RN", openReqHistory.get(i).getRouteNumber() == null ? "" : openReqHistory.get(i).getRouteNumber());
				histArray.put(jobs);
			}catch (JSONException e) {
				e.printStackTrace();
			}
		}
		try {
			totalObject.put("summ", summArrary);
			totalObject.put("histy", histArray);
			totalArray.put(totalObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		response.setContentType("application/json");
		response.getWriter().write(totalArray.toString());
	}
	public void drZoneActivity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String drId=request.getParameter("drNum");
		ArrayList<QueueBean> dzaList = ZoneDAO.getDriverZoneActivity(adminBO, "", drId,"","");
		JSONArray array = new JSONArray();
		if(dzaList.size()>0){
			for(int i=0;i<dzaList.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("D", dzaList.get(i).getDriverId());
					address.put("C", dzaList.get(i).getVehicleNo());
					address.put("Z", dzaList.get(i).getQU_NAME());
					address.put("L", dzaList.get(i).getDq_LoginTime());
					address.put("S", dzaList.get(i).getStatus());
					address.put("R", dzaList.get(i).getReason());
					address.put("Lo", (dzaList.get(i).getDq_LogoutTime()!=null && !dzaList.get(i).getDq_LogoutTime().equals(""))?dzaList.get(i).getDq_LogoutTime():"-" );
					array.put(address);			
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	public void driverRatingDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		JSONArray array=new JSONArray();
		String driverId = (request.getParameter("dId") != null && !request.getParameter("dId").equals("")) ? request.getParameter("dId") : "";
		ArrayList<String> companiesToLook = new ArrayList<String>();
		companiesToLook = SecurityDAO.getAllAssoccode(adminBO.getMasterAssociateCode());
		if(companiesToLook==null || companiesToLook.size()<1){
			companiesToLook.add(adminBO.getMasterAssociateCode());
		}
		if(!driverId.equals("")){
			ArrayList<HashMap<String, String>> list = RegistrationDAO.getDriverRatingDetails(companiesToLook, driverId,adminBO.getTimeZone(),adminBO.getMasterAssociateCode());
			try{
				for(HashMap<String, String> map:list){
					JSONObject obj=new JSONObject();
					obj.put("TI", map.get("tripId"));
//					obj.put("CI", map.get("custId"));
					obj.put("RT", map.get("rating"));
					obj.put("CMT", map.get("cmt"));
					obj.put("NM", map.get("name"));
					obj.put("DT", map.get("date"));
					obj.put("CN", map.get("custName"));
					array.put(obj);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		//System.out.println(array);
		//System.out.println("Done");
		response.getWriter().write(array.toString());
	}
	
	public void resetDriver(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String driverNum=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
		boolean result=RegistrationDAO.updateAndroidIdFirstTimeforDriver("", driverNum, adminBO.getAssociateCode());
		if(result){
			response.getWriter().write("1");
		} else {
			response.getWriter().write("0");
		}
	}
}


