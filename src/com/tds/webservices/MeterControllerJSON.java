package com.tds.webservices;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBElement;

import com.tds.tdsBO.MeterStatusBO;
import com.tds.util.MeterControlUtil;
import com.tds.webservices.bean.Command;

@Path("/feed")

public class MeterControllerJSON {
	@Context ServletConfig serveletConfig;
	@Context HttpServletRequest req;
	@Context HttpServletResponse resp;
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public MeterStatusBO getXML(JAXBElement<MeterStatusBO> jCommand) throws UnsupportedEncodingException {
		MeterStatusBO todo = jCommand.getValue();
//		System.out.println("From JSON--->"+todo.getCompanyCode()+" "+todo.getDriverId());
		todo=MeterControlUtil.enterMeterValues(req,resp,todo, this.serveletConfig);
		return todo;
	}

}
