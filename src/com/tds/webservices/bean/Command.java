package com.tds.webservices.bean;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement

public class Command {

	// JAX-RS supports an automatic mapping from JAXB annotated class to XML and JSON
	// Isn't that cool?
	  private String summary;
	  private String description;
	  
	  public Command (String id, String summary){
		    this.summary = id;
		    this.description = summary;
	}
	  public Command (){
		    this.summary = "";
		    this.description = "";
	}
		  
	  public String getSummary() {
		    return summary;
		  }
		  public void setSummary(String summary) {
		    this.summary = summary;
		  }
		  public String getDescription() {
		    return description;
		  }
		  public void setDescription(String description) {
		    this.description = description;
		  }


}
