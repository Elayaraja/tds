package com.tds.cmp.bean;

import com.tds.util.SphericalPolygon;

import nsidc.spheres.Point;
//import nsidc.spheres.SphericalPolygon;

 

public class ZoneTableBeanSP {

	   private String ZoneKey;
	    private String ZoneDesc;
	    private String orderOfZone;
	    private SphericalPolygon zoneCoord;
		private Integer advance;
	    private Integer waitTime;
	    private String dispatchType;
	    private String zoneType;
	    private Integer broadCastTime;
	    private double distance;
	    private Integer phoneCallWaitTime;
		private double northLatitude; 
	    private double eastLongitude;
	    private double southLatitude; 
	    private double westLongitude;
	    private Double distanceTest;
	    private String zoneSwitch;
	    private String maxDistance;
	    private String znProfile;
	    
	    public void setZoneKey(String s) { ZoneKey = s; }
	    public String getZoneKey() { return ZoneKey; }
	    public void setZoneDesc(String s) { ZoneDesc = s; }
	    public String getZoneDesc() { return ZoneDesc; }
	    public void setOrderOfZone(String s) { orderOfZone = s; }
	    public String getOrderOfZone() { return orderOfZone; }
	    public void setAdvance(Integer s) { advance = s; }
	    public Integer getAdvance() { return advance; }
	    public void setWaitTime(Integer s) { waitTime = s; }
	    public Integer getWaitTime() { return waitTime; }
	    public SphericalPolygon getZoneCoord() {
			return zoneCoord;
		}
		public void setZoneCoord(SphericalPolygon zoneCoord) {
			this.zoneCoord = zoneCoord;
		}
		public void setZoneCoord(double[] latitude, double[] longitude){
			zoneCoord = new SphericalPolygon(latitude,longitude);
			zoneCoord.setRadius(6367.435d);
		}
		public String getDispatchType() {
			return dispatchType;
		}
		public void setDispatchType(String dispatchType) {
			this.dispatchType = dispatchType;
		}

		public void setExternalPoint(double latitude, double longitude) {
			zoneCoord.setExternalPoint(new Point(latitude, longitude));
		}
		public String getZoneType() {
			return zoneType;
		}
		public void setZoneType(String zoneType) {
			this.zoneType = zoneType;
		}
		public Integer getBroadCastTime() {
			return broadCastTime;
		}
		public void setBroadCastTime(Integer broadCastTime) {
			this.broadCastTime = broadCastTime;
		}
		

		public double getDistance() {
			return distance;
		}
		public void setDistance(double distance) {
			this.distance = distance;
		}
	    public double getNorthLatitude() {
			return northLatitude;
		}
		public void setNorthLatitude(double northLatitude) {
			this.northLatitude = northLatitude;
		}
		public double getEastLongitude() {
			return eastLongitude;
		}
		public void setEastLongitude(double eastLongitude) {
			this.eastLongitude = eastLongitude;
		}
		public double getSouthLatitude() {
			return southLatitude;
		}
		public void setSouthLatitude(double southLatitude) {
			this.southLatitude = southLatitude;
		}
		public double getWestLongitude() {
			return westLongitude;
		}
		public void setWestLongitude(double westLongitude) {
			this.westLongitude = westLongitude;
		}
		public void setDistanceTest(Double distanceTest) {
			this.distanceTest = distanceTest;
		}
		public Double getDistanceTest() {
			return distanceTest;
		}
		public void setPhoneCallWaitTime(Integer phoneCallWaitTime) {
			this.phoneCallWaitTime = phoneCallWaitTime;
		}
		public Integer getPhoneCallWaitTime() {
			return phoneCallWaitTime;
		}
		public String getZoneSwitch() {
			return zoneSwitch;
		}
		public void setZoneSwitch(String zoneSwitch) {
			this.zoneSwitch = zoneSwitch;
		}
		public String getMaxDistance() {
			return maxDistance;
		}
		public void setMaxDistance(String maxDistance) {
			this.maxDistance = maxDistance;
		}
		public String getZnProfile() {
			return znProfile;
		}
		public void setZnProfile(String znProfile) {
			this.znProfile = znProfile;
		}
}
