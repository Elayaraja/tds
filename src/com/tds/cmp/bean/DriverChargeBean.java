package com.tds.cmp.bean;

public class DriverChargeBean {
	
	private String DC_ASSOCODE = "";
	private String DC_CHARGE_NO = ""; 
	private String DC_DESC = "";   
	private String DC_FREQUECY =""; 
	private String DC_AMOUNT = ""; 
	private String DC_STATUS = ""; 
	private String DC_LAST_UPDATE; 
	private String DC_NEXT_SCH;
	private String DC_LDAY;
	private int msgId;
	
	public String getDC_LDAY() {
		return DC_LDAY;
	}
	public void setDC_LDAY(String dCLDAY) {
		DC_LDAY = dCLDAY;
	}
	public String getDC_ASSOCODE() {
		return DC_ASSOCODE;
	}
	public void setDC_ASSOCODE(String dCASSOCODE) {
		DC_ASSOCODE = dCASSOCODE;
	}
	public String getDC_CHARGE_NO() {
		return DC_CHARGE_NO;
	}
	public void setDC_CHARGE_NO(String dCCHARGENO) {
		DC_CHARGE_NO = dCCHARGENO;
	}
	public String getDC_DESC() {
		return DC_DESC;
	}
	public void setDC_DESC(String dCDESC) {
		DC_DESC = dCDESC;
	}
	public String getDC_FREQUECY() {
		return DC_FREQUECY;
	}
	public void setDC_FREQUECY(String dCFREQUECY) {
		DC_FREQUECY = dCFREQUECY;
	}
	public String getDC_AMOUNT() {
		return DC_AMOUNT;
	}
	public void setDC_AMOUNT(String dCAMOUNT) {
		DC_AMOUNT = dCAMOUNT;
	}
	public String getDC_STATUS() {
		return DC_STATUS;
	}
	public void setDC_STATUS(String dCSTATUS) {
		DC_STATUS = dCSTATUS;
	}
	public String getDC_LAST_UPDATE() {
		return DC_LAST_UPDATE;
	}
	public void setDC_LAST_UPDATE(String dCLASTUPDATE) {
		DC_LAST_UPDATE = dCLASTUPDATE;
	}
	public String getDC_NEXT_SCH() {
		return DC_NEXT_SCH;
	}
	public void setDC_NEXT_SCH(String dCNEXTSCH) {
		DC_NEXT_SCH = dCNEXTSCH;
	}
	public int getMsgId() {
		return msgId;
	}
	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}  
	
	
	
}
