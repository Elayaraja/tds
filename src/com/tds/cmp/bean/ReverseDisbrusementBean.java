package com.tds.cmp.bean;
import java.io.Serializable;

public class ReverseDisbrusementBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	

	private String driverIdDis = "";
	private String paymentDateDis = "";
	private String amountDis = "";
	private String chequeORCashDis = "";
	private String operatorID="";	
	private String firstName = "";
	private String lastName = "";
	private String userName = "";



	
	public String getDriverIdDis() {
		return driverIdDis;
	}
	public void setDriverIdDis(String driverIdDis) {
		this.driverIdDis = driverIdDis;
	}
	
	public String getPaymentDateDis() {
		return paymentDateDis;
	}
	public void setPaymentDateDis(String paymentDateDis) {
		this.paymentDateDis = paymentDateDis;
	}
	public String getAmountDis() {
		return amountDis;
	}
	public void setAmountDis(String amountDis) {
		this.amountDis = amountDis;
	}
	public String getChequeORCashDis() {
		return chequeORCashDis;
	}
	public void setChequeORCashDis(String chequeORCashDis) {
		this.chequeORCashDis = chequeORCashDis;
	}
	public String getOperatorID() {
		return operatorID;
	}
	public void setOperatorID(String operatorID) {
		this.operatorID = operatorID;
	}
	
	private String checkNO = "";

	public String getCheckNO() {
		return checkNO;
	}
	public void setCheckNO(String checkNO) {
		this.checkNO = checkNO;
	}
	

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String fname) {
		this.firstName = fname;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lname) {
		this.lastName = lname;
	}
  

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
    
	private String payId = "";


	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	
	
	private int reverseIndi =0;


	public int getReverseIndi() {
		return reverseIndi;
	}
	public void setReverseIndi(int reverseIndi) {
		this.reverseIndi = reverseIndi;
	}
    
	private String reverseDateTime="";


	public String getReverseDateTime() {
		return reverseDateTime;
	}
	public void setReverseDateTime(String reverseDateTime) {
		this.reverseDateTime = reverseDateTime;
	}
	
	

}
