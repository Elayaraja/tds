package com.tds.cmp.bean;

public class TransactionSummaryBean {
	private String t_processDate;
	private String t_card_no;
	private String t_amout;
	private String t_tip;
	private String t_trans_id;
	private String t_alter_flg;
	private String t_trip;
	private String t_total;
	private String t_authcapflg;
	private boolean t_voidflg = false;
	private String t_voiddate;
	private boolean t_voidedflg = false;
	private String t_driver;
	private String t_approval_code;
	private String t_approval_status;
	private String t_fleetno;
	
	
	
	
	
	public String getT_fleetno() {
		return t_fleetno;
	}
	public void setT_fleetno(String t_fleetno) {
		this.t_fleetno = t_fleetno;
	}
	public String getT_approval_status() {
		return t_approval_status;
	}
	public void setT_approval_status(String t_approval_status) {
		this.t_approval_status = t_approval_status;
	}
	public String getT_approval_code() {
		return t_approval_code;
	}
	public void setT_approval_code(String t_approval_code) {
		this.t_approval_code = t_approval_code;
	}
	public String getT_driver() {
		return t_driver;
	}
	public void setT_driver(String t_driver) {
		this.t_driver = t_driver;
	}
	public boolean isT_voidflg() {
		return t_voidflg;
	}
	public void setT_voidflg(boolean t_voidflg) {
		this.t_voidflg = t_voidflg;
	}
	public String getT_voiddate() {
		return t_voiddate;
	}
	public void setT_voiddate(String t_voiddate) {
		this.t_voiddate = t_voiddate;
	}
	public boolean isT_voidedflg() {
		return t_voidedflg;
	}
	public void setT_voidedflg(boolean t_voidedflg) {
		this.t_voidedflg = t_voidedflg;
	}
	public String getT_authcapflg() {
		return t_authcapflg;
	}
	public void setT_authcapflg(String t_authcapflg) {
		this.t_authcapflg = t_authcapflg;
	}
	public String getT_processDate() {
		return t_processDate;
	}
	public void setT_processDate(String tProcessDate) {
		t_processDate = tProcessDate;
	}
	public String getT_card_no() {
		return t_card_no;
	}
	public void setT_card_no(String tCardNo) {
		t_card_no = tCardNo;
	}
	public String getT_amout() {
		return t_amout;
	}
	public void setT_amout(String tAmout) {
		t_amout = tAmout;
	}
	public String getT_tip() {
		return t_tip;
	}
	public void setT_tip(String tTip) {
		t_tip = tTip;
	}
	public String getT_trans_id() {
		return t_trans_id;
	}
	public void setT_trans_id(String tTransId) {
		t_trans_id = tTransId;
	}
	public String getT_alter_flg() {
		return t_alter_flg;
	}
	public void setT_alter_flg(String tAlterFlg) {
		t_alter_flg = tAlterFlg;
	}
	public String getT_trip() {
		return t_trip;
	}
	public void setT_trip(String tTrip) {
		t_trip = tTrip;
	}
	public String getT_total() {
		return t_total;
	}
	public void setT_total(String tTotal) {
		t_total = tTotal;
	}
	
	
}
