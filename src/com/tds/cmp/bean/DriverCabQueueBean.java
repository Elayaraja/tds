package com.tds.cmp.bean;

public class DriverCabQueueBean {

	
	private String queue;
	private String assocode;
	

	public DriverCabQueueBean() {
		
	}
	
	private String uid;
	private String tripID;
	private String uname;
	private String gkey;
	private String driverid;
	private String vehicleNo;
	private String drFlag;
	private String cabFlag;
	private String provider;
	private String phoneNo;
	private String pushKey;
	private String emailAddress;
	private double currentLatitude;
	private double currentLongitude;
	private long lastReportedTimeInMilliseconds;
	private int driverRating;
	
	private String drivername="";
	private String cabno="";
	private String availability="";
	private String logintime="";
	private String latitude="0.00";
	private String longitude="0.00";
	private String zonepos="";
     
    public String vType="";
 	public String vMake="";
	
	public String getvType() {
		return vType;
	}
	public void setvType(String vType) {
		this.vType = vType;
	}
	public String getvMake() {
		return vMake;
	}
	public void setvMake(String vMake) {
		this.vMake = vMake;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getTripID() {
		return tripID;
	}
	public void setTripID(String tripID) {
		this.tripID = tripID;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getGkey() {
		return gkey;
	}
	public void setGkey(String gkey) {
		this.gkey = gkey;
	}

	
	public String getProvider() {
		return provider;
	}
	public void setProvider(String provider) {
		this.provider = provider;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getDrFlag() {
		return drFlag;
	}
	public void setDrFlag(String drFlag) {
		this.drFlag = drFlag;
	}
	public String getCabFlag() {
		return cabFlag;
	}
	public void setCabFlag(String cabFlag) {
		this.cabFlag = cabFlag;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public double getCurrentLatitude() {
		return currentLatitude;
	}
	public void setCurrentLatitude(double currentLatitude) {
		this.currentLatitude = currentLatitude;
	}
	public double getCurrentLongitude() {
		return currentLongitude;
	}
	public void setCurrentLongitude(double currentLongitude) {
		this.currentLongitude = currentLongitude;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	public String getAssocode() {
		return assocode;
	}
	public void setAssocode(String assocode) {
		this.assocode = assocode;
	}
	public String getPushKey() {
		return pushKey;
	}
	public void setPushKey(String pushKey) {
		this.pushKey = pushKey;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public void setLastReportedTimeInMilliseconds(
			long lastReportedTimeInMilliseconds) {
		this.lastReportedTimeInMilliseconds = lastReportedTimeInMilliseconds;
	}
	public long getLastReportedTimeInMilliseconds() {
		return lastReportedTimeInMilliseconds;
	}
	public void setDriverRating(int driverRating) {
		this.driverRating = driverRating;
	}
	public int getDriverRating() {
		return driverRating;
	}
	public String getDrivername() {
		return drivername;
	}
	public void setDrivername(String drivername) {
		this.drivername = drivername;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	public String getCabno() {
		return cabno;
	}
	public void setCabno(String cabno) {
		this.cabno = cabno;
	}
	public String getLogintime() {
		return logintime;
	}
	public void setLogintime(String logintime) {
		this.logintime = logintime;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getZonepos() {
		return zonepos;
	}
	public void setZonepos(String zonepos) {
		this.zonepos = zonepos;
	}
}
