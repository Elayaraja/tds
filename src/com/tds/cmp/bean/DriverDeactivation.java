package com.tds.cmp.bean;

public class DriverDeactivation {

	private String driver_id;
	private String driver_name;
	private String from_date;
	private String to_date;
	private String from_time;
	private String to_time; 
	private String desc;
	private String user;
	private String status;
	private String entered_date;
	private String assoccode;
	private String hour;
	
	 
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	 
	
	 
	public String getFrom_time() {
		return from_time;
	}
	public void setFrom_time(String fromTime) {
		from_time = fromTime;
	}
	public String getTo_time() {
		return to_time;
	}
	public void setTo_time(String toTime) {
		to_time = toTime;
	}
	public String getDriver_name() {
		return driver_name;
	}
	public void setDriver_name(String driverName) {
		driver_name = driverName;
	}	
	
	public String getAssoccode() {
		return assoccode;
	}
	public void setAssoccode(String assoccode) {
		this.assoccode = assoccode;
	}
	public String getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(String driverId) {
		driver_id = driverId;
	}
	public String getFrom_date() {
		return from_date;
	}
	public void setFrom_date(String fromDate) {
		from_date = fromDate;
	}
	public String getTo_date() {
		return to_date;
	}
	public void setTo_date(String toDate) {
		to_date = toDate;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEntered_date() {
		return entered_date;
	}
	public void setEntered_date(String enteredDate) {
		entered_date = enteredDate;
	}
	
	
		
}
