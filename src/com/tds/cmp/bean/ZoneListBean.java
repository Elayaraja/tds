package com.tds.cmp.bean;

import java.util.ArrayList;

import nsidc.spheres.*;

public class ZoneListBean {

	private String zoneNum;
    private String zoneName;
    private int zoneOrder;
    private ArrayList<ZoneTableBeanSP> otherZonesSP;

	
	public String getZoneNum() {
		return zoneNum;
	}
	public void setZoneNum(String zN) {
		zoneNum = zN;
	}
	public String getZoneName() {
		return zoneName;
	}
	public void setZoneName(String zN) {
		zoneName = zN;
	}
	public int getZoneOrder() {
		return zoneOrder;
	}
	public void setZoneOrder(int zO) {
		zoneOrder = zO;
	}

    public ArrayList<ZoneTableBeanSP> getOtherZonesSP() {
		return otherZonesSP;
	}
	public void setOtherZonesSP(ArrayList<ZoneTableBeanSP> oZSP) {
		otherZonesSP = oZSP;
	}

	public void initializeZoneData(){
		otherZonesSP = new ArrayList();
		temporaryDataPopulate();
	}
	

		
	public void temporaryDataPopulate(){
		double[] lats1 = {25.700198,25.701049,25.69579,25.690762,25.685657,25.681712,25.682022,25.683414};
		double[] longs1 = {-80.152752,-80.168202,-80.173094,-80.175755,-80.179188,-80.173781,-80.165284,-80.154297};
		ZoneTableBeanSP spoly = new ZoneTableBeanSP();
		spoly.setZoneDesc("Florida");
		spoly.getZoneCoord().setRadius(6367.435d);

		spoly.setZoneCoord(lats1, longs1);
		otherZonesSP.add(spoly);
		
		double[] lats2 = {39.942849,39.942733,39.942249,39.942899,39.945416,39.946329,39.945087,39.944411};
		double [] longs2 = {-104.960251,-104.958251,-104.956518,-104.955305,-104.953857,-104.955466,-104.958857,-104.959323};
		ZoneTableBeanSP spoly2 = new ZoneTableBeanSP();
		spoly2.setZoneCoord(lats2, longs2);
		spoly2.getZoneCoord().setRadius(6367.435d);
		spoly2.setZoneDesc("Near Home");
		otherZonesSP.add(spoly2);
		
		
		double[] lats3 = {39.942635,39.940693,39.937798,39.936975,39.936975,39.934902,39.933981,39.932632,39.931875,39.931348,39.930723,39.930163,39.929439,39.928814,39.928419,39.928288,39.929012,39.92888,39.928189,39.927718,39.927169,39.92707,39.927817,39.928123,39.928452,39.928288,39.930196,39.932006,39.933921,39.93397,39.933773,39.93352,39.932829,39.932138,39.932539,39.933164,39.934267,39.935165,39.936257,39.937041,39.937952,39.938456,39.938719,39.939235,39.939772,39.940502,39.941352,39.942701,39.942733,39.942766,39.942766};
		double [] longs3 = {-104.959195,-104.95928,-104.959238,-104.958894,-104.958894,-104.957864,-104.957736,-104.957006,-104.956877,-104.957263,-104.958293,-104.958894,-104.959023,-104.959023,-104.959023,-104.955676,-104.952157,-104.949839,-104.94868,-104.948219,-104.947608,-104.945977,-104.944528,-104.943187,-104.941299,-104.94014,-104.94014,-104.94014,-104.940258,-104.941117,-104.941911,-104.942801,-104.944389,-104.945891,-104.947704,-104.948348,-104.948519,-104.948423,-104.948541,-104.948552,-104.948648,-104.948895,-104.94971,-104.950472,-104.950869,-104.951288,-104.951341,-104.951169,-104.952586,-104.954774,-104.957135};
		//spoly2.getZoneCoord().
		ZoneTableBeanSP spoly3 = new ZoneTableBeanSP();
		spoly3.getZoneCoord().setRadius(6367.435d);
		spoly3.setZoneCoord(lats3, longs3);
		spoly3.setZoneDesc("Sanjay Home");
		otherZonesSP.add(spoly3);

	}

	public int determineZone(double currentLatitude, double currentLongitude){
		int changeZone = 2;

		for(int i=0;i<otherZonesSP.size();i++){
			if(otherZonesSP.get(i).getZoneCoord().contains(new Point(currentLatitude, currentLongitude))){
				changeZone = 1;
				return changeZone;
			}
		}
		return changeZone;
	}
	
	public void add(ZoneTableBeanSP zTB){
		otherZonesSP.add(zTB);
	}
}
