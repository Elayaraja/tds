package com.tds.cmp.bean;

import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import com.tds.cmp.bean.Coordinates;

public class YahooXmlReader    {
	private YahooXmlReader() {
		// no instantiation
	}

	public static ArrayList  readConfig(InputStream in)  throws Exception {
	
	
		ArrayList al_list = new ArrayList();
		
		try {
			// First create a new XMLInputFactory
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			// Setup a new eventReader
			// InputStream in = new FileInputStream(configFile);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			// Read the XML document
	 
			
			
			while (eventReader.hasNext()) { 
				
				
				XMLEvent event = eventReader.nextEvent();
				  
				if (event.isStartElement()) {
					Coordinates coordinates = new Coordinates();
					//System.out.println("FIELD::::::::"+event.asStartElement().getName().getLocalPart());
					//System.out.println("DATA::::::::"+event.asCharacters().getData());
					 
					if (event.asStartElement().getName().getLocalPart() == ("Latitude")) {
						event = eventReader.nextEvent();
						al_list.add(event.asCharacters().getData());
						//coordinates.setLatitude(event.asCharacters().getData());
						
						//System.out.println("latitude::"+coordinates.getLatitude());
						 
					} else 
					if (event.asStartElement().getName().	getLocalPart() == ("Longitude")) {
						event = eventReader.nextEvent();
						coordinates .setLongitude(event.asCharacters().getData()); 
						//System.out.println("langitutde::"+coordinates.getLongitude());
						al_list.add(event.asCharacters().getData());
						 
					}else
						if (event.asStartElement().getName().getLocalPart() == ("Address")) {
							event = eventReader.nextEvent();
							coordinates.setCity(event.asCharacters().getData()); 
							//System.out.println("Address Street:"+coordinates.getCity());
							al_list.add(event.asCharacters().getData());
						}
					else
					if (event.asStartElement().getName().getLocalPart() == ("City")) {
						event = eventReader.nextEvent();
						coordinates.setCity(event.asCharacters().getData()); 
						//System.out.println("City:"+coordinates.getCity());
						al_list.add(event.asCharacters().getData());
						 
					}else 
					if (event.asStartElement().getName().getLocalPart() == ("State")) {
						event = eventReader.nextEvent();
						coordinates.setState(event.asCharacters().getData()); 
						//System.out.println("State::"+coordinates.getState());
						al_list.add(event.asCharacters().getData());
						 
					}else
					if (event.asStartElement().getName().getLocalPart() == ("Zip")) {
						event = eventReader.nextEvent();
						coordinates.setZip(event.asCharacters().getData()); 
						//System.out.println("Zip::"+coordinates.getZip());
						al_list.add(event.asCharacters().getData());
						 
					}
					else
						if (event.asStartElement().getName().getLocalPart() == ("Country")) {
							event = eventReader.nextEvent();
							coordinates.setZip(event.asCharacters().getData()); 
							//System.out.println("Country::"+coordinates.getZip());
							al_list.add(event.asCharacters().getData());
							 
						}
					 
				} 
			}
			for(int i=0;i<al_list.size();i++) {
				//System.out.println("data"+al_list.get(i));
			}
			//System.out.println("SIZE OF LOTITUDE:::>>>>>"+al_list.size());
			
 		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (ClassCastException t) {
			t.printStackTrace();
		}
		
		return al_list;
	}


}
