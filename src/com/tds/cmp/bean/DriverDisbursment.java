package com.tds.cmp.bean;

import java.util.ArrayList;

public class DriverDisbursment {

	private String driverid;
	private String drivername;
	private String amount;
	private String set_amount;
	private String process_date;
	private String disbur_date;
	private String trans_id;
	private String trip_id;
	private String reason;
	private String opr_id;
	private String set;
	private String payment_id;
	private String descr;
	private String checkno;
	private String type;
	private String check_cash;
	private ArrayList al_list;
	private String receivedCheque;
	// for mapping
	private String queue_name="";
//	private String queue_east="";
//	private String queue_west="";
//	private String queue_north="";
//	private String queue_south="";
	private String queue_desc="";
	private String queue_login_flag="";
//	private String button_flag="";
	private String checked="";


	private String chequeStatus;
	private String received_Date;
	private String date;
	
	private String chequeReceived;
	private String chequeBounced;
		
	public String getQueue_name() {
		return queue_name;
	}
	public void setQueue_name(String queue_name) {
		this.queue_name = queue_name;
	}
	/*public String getQueue_east() {
		return queue_east;
	}
	public void setQueue_east(String queue_east) {
		this.queue_east = queue_east;
	}
	public String getQueue_west() {
		return queue_west;
	}
	public void setQueue_west(String queue_west) {
		this.queue_west = queue_west;
	}
	public String getQueue_north() {
		return queue_north;
	}
	public void setQueue_north(String queue_north) {
		this.queue_north = queue_north;
	}
	public String getQueue_south() {
		return queue_south;
	}
	public void setQueue_south(String queue_south) {
		this.queue_south = queue_south;
	}*/
	public String getQueue_desc() {
		return queue_desc;
	}
	public void setQueue_desc(String queue_desc) {
		this.queue_desc = queue_desc;
	}
	/*public String getButton_flag() {
		return button_flag;
	}
	public void setButton_flag(String button_flag) {
		this.button_flag = button_flag;
	}*/
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	public ArrayList getAl_list() {
		return al_list;
	}
	public void setAl_list(ArrayList al_list) {
		this.al_list = al_list;
	}
	public String getCheckno() {
		return checkno;
	}
	public void setCheckno(String checkno) {
		this.checkno = checkno;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getPayment_id() {
		return payment_id;
	}
	public void setPayment_id(String payment_id) {
		this.payment_id = payment_id;
	}
	public String getSet() {
		return set;
	}
	public void setSet(String set) {
		this.set = set;
	}
	public String getDriverid() {
		return driverid;
	}
	public void setDriverid(String driverid) {
		this.driverid = driverid;
	}
	public String getDrivername() {
		return drivername;
	}
	public void setDrivername(String drivername) {
		this.drivername = drivername;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSet_amount() {
		return set_amount;
	}
	public void setSet_amount(String set_amount) {
		this.set_amount = set_amount;
	}
	public String getProcess_date() {
		return process_date;
	}
	public void setProcess_date(String process_date) {
		this.process_date = process_date;
	}
	public String getDisbur_date() {
		return disbur_date;
	}
	public void setDisbur_date(String disbur_date) {
		this.disbur_date = disbur_date;
	}
	public String getTrans_id() {
		return trans_id;
	}
	public void setTrans_id(String trans_id) {
		this.trans_id = trans_id;
	}
	public String getTrip_id() {
		return trip_id;
	}
	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getOpr_id() {
		return opr_id;
	}
	public void setOpr_id(String opr_id) {
		this.opr_id = opr_id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCheck_cash() {
		return check_cash;
	}
	public void setCheck_cash(String check_cash) {
		this.check_cash = check_cash;
	}
	
	public String getReceivedCheque() {
		return receivedCheque;
	}
	public void setReceivedCheque(String receivedCheque) {
		this.receivedCheque = receivedCheque;
	}
	
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getReceived_Date() {
		return received_Date;
	}
	public void setReceived_Date(String received_Date) {
		this.received_Date = received_Date;
	}
	public String getChequeStatus() {
		return chequeStatus;
	}
	public void setChequeStatus(String chequeStatus) {
		this.chequeStatus = chequeStatus;
	}

	public String getChequeReceived() {
		return chequeReceived;
	}
	public void setChequeReceived(String chequeReceived) {
		this.chequeReceived = chequeReceived;
	}
	public String getChequeBounced() {
		return chequeBounced;
	}
	public void setChequeBounced(String chequeBounced) {
		this.chequeBounced = chequeBounced;
	}
	
	public String getQueue_login_flag() {
		return queue_login_flag;
	}
	public void setQueue_login_flag(String queue_login_flag) {
		this.queue_login_flag = queue_login_flag;
	}
	
}
