package com.tds.cmp.bean;

public class CashSettlement
{

private String operatorId="";
private String associateCode="";
private int openCash;
private int intialChequeNo;
private int openTime;
private String openedBy="";
private int closingCash;
private int closeTime;
private String closedBy="";
private int closingCheckNo;
private boolean csBean;
private String driverId;
private String description;
private String disbursementType;
private String chequeNumber;
private String amount;
private String payId;
private String receivedCheques;


public String getReceivedCheques() {
	return receivedCheques;
}
public void setReceivedCheques(String receivedCheques) {
	this.receivedCheques = receivedCheques;
}

public String getDriverId() {
	return driverId;
}
public void setDriverId(String driverId) {
	this.driverId = driverId;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getDisbursementType() {
	return disbursementType;
}
public void setDisbursementType(String disbursementType) {
	this.disbursementType = disbursementType;
}
public String getChequeNumber() {
	return chequeNumber;
}
public void setChequeNumber(String chequeNumber) {
	this.chequeNumber = chequeNumber;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getPayId() {
	return payId;
}
public void setPayId(String payId) {
	this.payId = payId;
}
public boolean isCsBean() {
	return csBean;
}
public void setCsBean(boolean csBean) {
	this.csBean = csBean;
}
public int getClosingCheckNo() {
	return closingCheckNo;
}
public void setClosingCheckNo(int closingCheckNo) {
	this.closingCheckNo = closingCheckNo;
}
private boolean isRegisterOpen;

public boolean isRegisterOpen() {
	return isRegisterOpen;
}
public void setRegisterOpen(boolean isRegisterOpen) {
	this.isRegisterOpen = isRegisterOpen;
}
public String getOperatorId() {
	return operatorId;
}
public void setOperatorId(String operatorId) {
	this.operatorId = operatorId;
}
public String getAssociateCode() {
	return associateCode;
}
public void setAssociateCode(String associateCode) {
	this.associateCode = associateCode;
}
public int getOpenCash() {
	return openCash;
}
public void setOpenCash(int openCash) {
	this.openCash = openCash;
}
public int getIntialChequeNo() {
	return intialChequeNo;
}
public void setIntialChequeNo(int intialChequeNo) {
	this.intialChequeNo = intialChequeNo;
}
public int getOpenTime() {
	return openTime;
}
public void setOpenTime(int openTime) {
	this.openTime = openTime;
}
public String getOpenedBy() {
	return openedBy;
}
public void setOpenedBy(String openedBy) {
	this.openedBy = openedBy;
}
public int getClosingCash() {
	return closingCash;
}
public void setClosingCash(int closingCash) {
	this.closingCash = closingCash;
}
public int getCloseTime() {
	return closeTime;
}
public void setCloseTime(int closeTime) {
	this.closeTime = closeTime;
}
public String getClosedBy() {
	return closedBy;
}
public void setClosedBy(String closedBy) {
	this.closedBy = closedBy;
}


}

