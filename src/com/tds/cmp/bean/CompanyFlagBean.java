package com.tds.cmp.bean;

public class CompanyFlagBean {
	
	private String assocode="";
	public String getAssocode() {
		return assocode;
	}
	public void setAssocode(String assocode) {
		this.assocode = assocode;
	}
	
	 private String flag1_lng_desc="";
	
	 private String flag2_lng_desc="";
	
	 private String flag3_lng_desc="";
	
	 private String flag4_lng_desc="";
	
	 private String flag5_lng_desc="";
	
	 private String flag6_lng_desc="";
	
	
	 private String flag7_lng_desc="";
	
	 
	private String flag8_lng_desc="";
	
	
	public String getFlag1_lng_desc() {
		return flag1_lng_desc;
	}
	public void setFlag1_lng_desc(String flag1LngDesc) {
		flag1_lng_desc = flag1LngDesc;
	}
	public String getFlag2_lng_desc() {
		return flag2_lng_desc;
	}
	public void setFlag2_lng_desc(String flag2LngDesc) {
		flag2_lng_desc = flag2LngDesc;
	}
	public String getFlag3_lng_desc() {
		return flag3_lng_desc;
	}
	public void setFlag3_lng_desc(String flag3LngDesc) {
		flag3_lng_desc = flag3LngDesc;
	}
	public String getFlag4_lng_desc() {
		return flag4_lng_desc;
	}
	public void setFlag4_lng_desc(String flag4LngDesc) {
		flag4_lng_desc = flag4LngDesc;
	}
	public String getFlag5_lng_desc() {
		return flag5_lng_desc;
	}
	public void setFlag5_lng_desc(String flag5LngDesc) {
		flag5_lng_desc = flag5LngDesc;
	}
	public String getFlag6_lng_desc() {
		return flag6_lng_desc;
	}
	public void setFlag6_lng_desc(String flag6LngDesc) {
		flag6_lng_desc = flag6LngDesc;
	}
	public String getFlag7_lng_desc() {
		return flag7_lng_desc;
	}
	public void setFlag7_lng_desc(String flag7LngDesc) {
		flag7_lng_desc = flag7LngDesc;
	}
	public String getFlag8_lng_desc() {
		return flag8_lng_desc;
	}
	public void setFlag8_lng_desc(String flag8LngDesc) {
		flag8_lng_desc = flag8LngDesc;
	}

	private String flag1="";
	private String flag1_value="";
	private String flag1_sw="1";
	private String flag1_cusap="";
	private String flag1_group = null;
	
	private String flag2="";
	private String flag2_value="";
	private String flag2_sw="1";
	
	private String flag3="";
	private String flag3_sw="1";
	public String getFlag1_value() {
		return flag1_value;
	}
	public void setFlag1_value(String flag1Value) {
		flag1_value = flag1Value;
	}
	public String getFlag2_value() {
		return flag2_value;
	}
	public void setFlag2_value(String flag2Value) {
		flag2_value = flag2Value;
	}
	public String getFlag3_value() {
		return flag3_value;
	}
	public void setFlag3_value(String flag3Value) {
		flag3_value = flag3Value;
	}
	public String getFlag4_value() {
		return flag4_value;
	}
	public void setFlag4_value(String flag4Value) {
		flag4_value = flag4Value;
	}
	public String getFlag5_value() {
		return flag5_value;
	}
	public void setFlag5_value(String flag5Value) {
		flag5_value = flag5Value;
	}
	public String getFlag6_value() {
		return flag6_value;
	}
	public void setFlag6_value(String flag6Value) {
		flag6_value = flag6Value;
	}
	public String getFlag7_value() {
		return flag7_value;
	}
	public void setFlag7_value(String flag7Value) {
		flag7_value = flag7Value;
	}
	public String getFlag8_value() {
		return flag8_value;
	}
	public void setFlag8_value(String flag8Value) {
		flag8_value = flag8Value;
	}
	private String flag3_value="";
	
	private String flag4="";
	private String flag4_sw="1";
	private String flag4_value="";
	
	private String flag5="";
	private String flag5_sw="1";
	private String flag5_value="";
	
	private String flag6="";
	private String flag6_sw="1";
	private String flag6_value="";
	
	private String flag7="";
	private String flag7_sw="1";
	private String flag7_value="";
	
	private String flag8="";
	private String flag8_sw="1";
	private String flag8_value="";
	
	public String getFlag1_sw() {
		return flag1_sw;
	}
	public void setFlag1_sw(String flag1Sw) {
		flag1_sw = flag1Sw;
	}
	public String getFlag2_sw() {
		return flag2_sw;
	}
	public void setFlag2_sw(String flag2Sw) {
		flag2_sw = flag2Sw;
	}
	public String getFlag3_sw() {
		return flag3_sw;
	}
	public void setFlag3_sw(String flag3Sw) {
		flag3_sw = flag3Sw;
	}
	public String getFlag4_sw() {
		return flag4_sw;
	}
	public void setFlag4_sw(String flag4Sw) {
		flag4_sw = flag4Sw;
	}
	public String getFlag5_sw() {
		return flag5_sw;
	}
	public void setFlag5_sw(String flag5Sw) {
		flag5_sw = flag5Sw;
	}
	public String getFlag6_sw() {
		return flag6_sw;
	}
	public void setFlag6_sw(String flag6Sw) {
		flag6_sw = flag6Sw;
	}
	public String getFlag7_sw() {
		return flag7_sw;
	}
	public void setFlag7_sw(String flag7Sw) {
		flag7_sw = flag7Sw;
	}
	public String getFlag8_sw() {
		return flag8_sw;
	}
	public void setFlag8_sw(String flag8Sw) {
		flag8_sw = flag8Sw;
	}
	public String getFlag1() {
		return flag1;
	}
	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}
	public String getFlag2() {
		return flag2;
	}
	public void setFlag2(String flag2) {
		this.flag2 = flag2;
	}
	public String getFlag3() {
		return flag3;
	}
	public void setFlag3(String flag3) {
		this.flag3 = flag3;
	}
	public String getFlag4() {
		return flag4;
	}
	public void setFlag4(String flag4) {
		this.flag4 = flag4;
	}
	public String getFlag5() {
		return flag5;
	}
	public void setFlag5(String flag5) {
		this.flag5 = flag5;
	}
	public String getFlag6() {
		return flag6;
	}
	public void setFlag6(String flag6) {
		this.flag6 = flag6;
	}
	public String getFlag7() {
		return flag7;
	}
	public void setFlag7(String flag7) {
		this.flag7 = flag7;
	}
	public String getFlag8() {
		return flag8;
	}
	public void setFlag8(String flag8) {
		this.flag8 = flag8;
	}
	public String getFlag1_group() {
		return flag1_group;
	}
	public void setFlag1_group(String flag1_group) {
		this.flag1_group = flag1_group;
	}
	public String getFlag1_cusap() {
		return flag1_cusap;
	}
	public void setFlag1_cusap(String flag1_cusap) {
		this.flag1_cusap = flag1_cusap;
	}
	 
	
	
	
}
