package com.tds.cmp.bean;

public class DriverAndJobs {
	
private String zoneNumber;
private String zoneDesc;
private String numberOfDrivers;
private String numberOfJobs;
private String future1HourJobs;

public String getFuture1HourJobs() {
	return future1HourJobs;
}
public void setFuture1HourJobs(String future1HourJobs) {
	this.future1HourJobs = future1HourJobs;
}
public String getZoneNumber() {
	return zoneNumber;
}
public void setZoneNumber(String zoneNumber) {
	this.zoneNumber = zoneNumber;
}
public String getZoneDesc() {
	return zoneDesc;
}
public void setZoneDesc(String zoneDesc) {
	this.zoneDesc = zoneDesc;
}
public String getNumberOfDrivers() {
	return numberOfDrivers;
}
public void setNumberOfDrivers(String numberOfDrivers) {
	this.numberOfDrivers = numberOfDrivers;
}
public String getNumberOfJobs() {
	return numberOfJobs;
}
public void setNumberOfJobs(String numberOfJobs) {
	this.numberOfJobs = numberOfJobs;
}
}
