package com.tds.cmp.bean;

public class DisbursementBean {
private String startDate;
private String endDate;
private String shortDesc;
private String longDesc;
private String associationCode;
private int disbursementNumber;
public String getStartDate() {
	return startDate;
}
public void setStartDate(String startDate) {
	this.startDate = startDate;
}
public String getEndDate() {
	return endDate;
}
public void setEndDate(String endDate) {
	this.endDate = endDate;
}
public String getShortDesc() {
	return shortDesc;
}
public void setShortDesc(String shortDesc) {
	this.shortDesc = shortDesc;
}
public String getLongDesc() {
	return longDesc;
}
public void setLongDesc(String longDesc) {
	this.longDesc = longDesc;
}
public String getAssociationCode() {
	return associationCode;
}
public void setAssoCode(String assoCode) {
	this.associationCode = assoCode;
}
public int getDisbursementNumber() {
	return disbursementNumber;
}
public void setDisNum(int x) {
	this.disbursementNumber = x;
}

}