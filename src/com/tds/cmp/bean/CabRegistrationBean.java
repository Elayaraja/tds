package com.tds.cmp.bean;

import java.util.ArrayList;

public class CabRegistrationBean {
	
	
	private String cab_cabkey="";
	private String cab_cabno="";
	private String cab_assocode="";
	private String cab_cabmake="";
	private String cab_cabYear="";
	private String cab_rcno="";
	private String status = "";
	private String cab_vinno = "";
	private String cab_vtype= "";
	private String cab_model = "";
	private String odoMeterValue="";
	
	private String cab_registrationDate="";
	private String cab_expiryDate="";
	private String cab_meterRate="";
	private ArrayList cab_meterTypes;
	private String payId = "";
	private String payPass = "";
	
	private String cab_plate_number = "";
	private String cab_plate_name_left = "";
	private String cab_plate_name_right = "";
	private String cab_plate_name_middle = "";
	private String cab_detials_json = "";
	private String cab_reference_id = "";
	
	public String getCab_plate_number() {
		return cab_plate_number;
	}
	public void setCab_plate_number(String cab_plate_number) {
		this.cab_plate_number = cab_plate_number;
	}
	public String getCab_plate_name_left() {
		return cab_plate_name_left;
	}
	public void setCab_plate_name_left(String cab_plate_name_left) {
		this.cab_plate_name_left = cab_plate_name_left;
	}
	public String getCab_plate_name_right() {
		return cab_plate_name_right;
	}
	public void setCab_plate_name_right(String cab_plate_name_right) {
		this.cab_plate_name_right = cab_plate_name_right;
	}
	public String getCab_plate_name_middle() {
		return cab_plate_name_middle;
	}
	public void setCab_plate_name_middle(String cab_plate_name_middle) {
		this.cab_plate_name_middle = cab_plate_name_middle;
	}
	public String getPayPass() {
		return payPass;
	}
	public void setPayPass(String payPass) {
		this.payPass = payPass;
	}
	public String getCab_registrationDate() {
		return cab_registrationDate;
	}
	public void setCab_registrationDate(String cab_registrationDate) {
		this.cab_registrationDate = cab_registrationDate;
	}
	public String getCab_expiryDate() {
		return cab_expiryDate;
	}
	public void setCab_expiryDate(String cab_exipryDate) {
		this.cab_expiryDate = cab_exipryDate;
	}

	
	
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	private String vehicleProfile="";
	private ArrayList al_list = new ArrayList();
	
	
	public String getVehicleProfile() {
		return vehicleProfile;
	}
	public void setVehicleProfile(String vehicleProfile) {
		this.vehicleProfile = vehicleProfile;
	}
	public ArrayList getAl_list() {
		return al_list;
	}
	public void setAl_list(ArrayList alList) {
		al_list = alList;
	}
	public String getCab_cabkey() {
		return cab_cabkey;
	}
	public void setCab_cabkey(String cab_cabkey) {
		this.cab_cabkey = cab_cabkey;
	}
	public String getCab_cabno() {
		return cab_cabno;
	}
	public void setCab_cabno(String cab_cabno) {
		this.cab_cabno = cab_cabno;
	}
	public String getCab_assocode() {
		return cab_assocode;
	}
	public void setCab_assocode(String cab_assocode) {
		this.cab_assocode = cab_assocode;
	}
	public String getCab_cabmake() {
		return cab_cabmake;
	}
	public void setCab_cabmake(String cab_cabmake) {
		this.cab_cabmake = cab_cabmake;
	}
	public String getCab_cabYear() {
		return cab_cabYear;
	}
	public void setCab_cabYear(String cab_cabYear) {
		this.cab_cabYear = cab_cabYear;
	}
	public String getCab_rcno() {
		return cab_rcno;
	}
	public void setCab_rcno(String cab_rcno) {
		this.cab_rcno = cab_rcno;
	}
	
	public String getCab_vinno() {
		return cab_vinno;
	}
	public void setCab_vinno(String cab_vinno) {
		this.cab_vinno = cab_vinno;
	}
	public String getCab_vtype() {
		return cab_vtype;
	}
	public void setCab_vtype(String cab_vtype) {
		this.cab_vtype = cab_vtype;
	}

	public String getCab_model() {
		return cab_model;
	}
	public void setCab_model(String cab_model) {
		this.cab_model = cab_model;
	}
	public void setOdoMeterValue(String odoMeterValue) {
		this.odoMeterValue = odoMeterValue;
	}
	public String getOdoMeterValue() {
		return odoMeterValue;
	}
	public String getCab_meterRate() {
		return cab_meterRate;
	}
	public void setCab_meterRate(String cab_meterRate) {
		this.cab_meterRate = cab_meterRate;
	}
	public ArrayList getCab_meterTypes() {
		return cab_meterTypes;
	}
	public void setCab_meterTypes(ArrayList cab_meterTypes) {
		this.cab_meterTypes = cab_meterTypes;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public String getCab_detials_json() {
		return cab_detials_json;
	}
	public void setCab_detials_json(String cab_detials_json) {
		this.cab_detials_json = cab_detials_json;
	}
	public String getCab_reference_id() {
		return cab_reference_id;
	}
	public void setCab_reference_id(String cab_reference_id) {
		this.cab_reference_id = cab_reference_id;
	}
	

	
	
	
}
