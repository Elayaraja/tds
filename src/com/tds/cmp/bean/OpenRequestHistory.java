package com.tds.cmp.bean;



public class OpenRequestHistory  {
	public String name="";
	public String phoneNumber="";
	public String tripid="";
	public String startAddress1="";
	public String startAddress2="";
	public String startState="";
	public String startZip="";
	public String endAddress1="";
	public String endAddress2="";
	public String endState="";
	public String endZip="";
	public String status="";
	public String driverId="";
	public String startCity="";
	public String endCity="";
	public String startLatitude="";
	public String endLatitude="";
	public String startLongitude="";
	public String endLongitude="";
	public String tripStatus="";
	public String reason="";
	public String time="";
	public String userId="";
	public String date="";
	public String vehicleNumber="";
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getStartCity() {
		return startCity;
	}
	public void setStartCity(String startCity) {
		this.startCity = startCity;
	}
	public String getEndCity() {
		return endCity;
	}
	public void setEndCity(String endCity) {
		this.endCity = endCity;
	}
	public String getStartLatitude() {
		return startLatitude;
	}
	public void setStartLatitude(String startLatitude) {
		this.startLatitude = startLatitude;
	}
	public String getEndLatitude() {
		return endLatitude;
	}
	public void setEndLatitude(String endLatitude) {
		this.endLatitude = endLatitude;
	}
	public String getStartLongitude() {
		return startLongitude;
	}
	public void setStartLongitude(String startLongitude) {
		this.startLongitude = startLongitude;
	}
	public String getEndLongitude() {
		return endLongitude;
	}
	public void setEndLongitude(String endLongitude) {
		this.endLongitude = endLongitude;
	}
	public String getTripStatus() {
		return tripStatus;
	}
	public void setTripStatus(String tripStatus) {
		this.tripStatus = tripStatus;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getTripid() {
		return tripid;
	}
	public void setTripid(String tripid) {
		this.tripid = tripid;
	}
	public String getStartAddress1() {
		return startAddress1;
	}
	public void setStartAddress1(String startAddress1) {
		this.startAddress1 = startAddress1;
	}
	public String getStartAddress2() {
		return startAddress2;
	}
	public void setStartAddress2(String startAddress2) {
		this.startAddress2 = startAddress2;
	}
	public String getStartState() {
		return startState;
	}
	public void setStartState(String startState) {
		this.startState = startState;
	}
	public String getStartZip() {
		return startZip;
	}
	public void setStartZip(String startZip) {
		this.startZip = startZip;
	}
	public String getEndAddress1() {
		return endAddress1;
	}
	public void setEndAddress1(String endAddress1) {
		this.endAddress1 = endAddress1;
	}
	public String getEndAddress2() {
		return endAddress2;
	}
	public void setEndAddress2(String endAddress2) {
		this.endAddress2 = endAddress2;
	}
	public String getEndState() {
		return endState;
	}
	public void setEndState(String endState) {
		this.endState = endState;
	}
	public String getEndZip() {
		return endZip;
	}
	public void setEndZip(String endZip) {
		this.endZip = endZip;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDriverId() {
		return driverId;
	}
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
}
