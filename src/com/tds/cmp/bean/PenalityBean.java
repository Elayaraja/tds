package com.tds.cmp.bean;

public class PenalityBean {
	private String b_driver_id = "";
	private String b_ass_code = "";
	private String b_desc ="";
	private String b_amount="";
	private String b_p_date="";
	private String btype="";
	public String getBtype() {
		return btype;
	}
	public void setBtype(String btype) {
		this.btype = btype;
	}
	private String b_entered_date;
	private String b_trans="";
	public String getB_driver_id() {
		return b_driver_id;
	}
	public void setB_driver_id(String bDriverId) {
		b_driver_id = bDriverId;
	}
	public String getB_ass_code() {
		return b_ass_code;
	}
	public void setB_ass_code(String bAssCode) {
		b_ass_code = bAssCode;
	}
	public String getB_desc() {
		return b_desc;
	}
	public void setB_desc(String bDesc) {
		b_desc = bDesc;
	}
	public String getB_amount() {
		return b_amount;
	}
	public void setB_amount(String bAmount) {
		b_amount = bAmount;
	}
	public String getB_p_date() {
		return b_p_date;
	}
	public void setB_p_date(String bPDate) {
		b_p_date = bPDate;
	}
	public String getB_entered_date() {
		return b_entered_date;
	}
	public void setB_entered_date(String bEnteredDate) {
		b_entered_date = bEnteredDate;
	}
	public String getB_trans() {
		return b_trans;
	}
	public void setB_trans(String bTrans) {
		b_trans = bTrans;
	}
	
	
	
}
