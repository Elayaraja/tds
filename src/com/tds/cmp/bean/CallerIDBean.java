package com.tds.cmp.bean;

public class CallerIDBean {
	
	private String lineNo;
	private String associationCode;
	private String phoneNo;
	private String name;
	private String password;
	private String userID;
	private String time;
	private String date;
	private String acceptedBy;
	private int status;
	private String acceptedDate;
	private String acceptedTime;
	private String diffence;
	private boolean ringing;


	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLineNo() {
		return lineNo;
	}
	public void setLineNo(String lineNo) {
		this.lineNo = lineNo;
	}
	public String getAssociationCode() {
		return associationCode;
	}
	public void setAssociationCode(String associationCode) {
		this.associationCode = associationCode;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getAcceptedBy() {
		return acceptedBy;
	}
	public void setAcceptedBy(String acceptedBy) {
		this.acceptedBy = acceptedBy;
	}
	 public String getAcceptedDate() {
	 return acceptedDate;
	 }
	 public void setAcceptedDate(String acceptedDate) {
	 this.acceptedDate = acceptedDate;
	 }
	 public String getAcceptedTime() {
	 return acceptedTime;
	 }
	 public void setAcceptedTime(String acceptedTime) {
	 this.acceptedTime = acceptedTime;
	 }
	 public String getDiffence() {
	 return diffence;
	 }
	 public void setDiffence(String diffence) {
	 this.diffence = diffence;
	 }
	 public int getStatus() {
	 return status;
	 }
	 public void setStatus(int status) {
	 this.status = status;
	 }
	public boolean isRinging() {
		return ringing;
	}
	public void setRinging(boolean ringing) {
		this.ringing = ringing;
	}
	}
