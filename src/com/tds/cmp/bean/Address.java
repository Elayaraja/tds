package com.tds.cmp.bean;

public class Address {
	
	private String street;
	private String number;
	private String postalCode;
	private String city;
	private String country;
	private String state;
	private String zip;
	private String addressKey;
	private String masterKey;
	private String landmarkKey;
	private String landmarkName;
	public String getLandmarkKey() {
		return landmarkKey;
	}

	public void setLandmarkKey(String landmarkKey) {
		this.landmarkKey = landmarkKey;
	}

	public String getLandmarkName() {
		return landmarkName;
	}

	public void setLandmarkName(String landmarkName) {
		this.landmarkName = landmarkName;
	}

	private String name = "";
	private String add1 = "";
	private String add2 = "";
	private String queueNo;
	private int addverify = 0;
	public int getAddverify() {
		return addverify;
	}

	public void setAddverify(int addverify) {
		this.addverify = addverify;
	}
	
	

	public String getName() {
		return name;
	}

	

	public void setName(String name) {
		this.name = name;
	}

	public String getAdd1() {
		return add1;
	}

	public void setAdd1(String add1) {
		this.add1 = add1;
	}

	public String getAdd2() {
		return add2;
	}

	public void setAdd2(String add2) {
		this.add2 = add2;
	}
	
	 private String longitude = "";
	 private String latitude = "";
	    
	 
	 
	 
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	

	public Address() {
	}

	public Address(String street, String city, String state, String zip) {
		this.postalCode = zip; 
		this.street = street;
		this.city = city;
		this.state = state;
		
		
	}

	
	
	
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		String s = "";
		s += street != null ? street + " " : "";
		s += number != null ? number + " " : "";
		s += postalCode != null ? postalCode + " " : "";
		s += city != null ? city + " " : "";
		s += country != null ? country + " " : "";

		return s;
	}


	public void setAssociateCode(String associateCode) {
		// TODO Auto-generated method stub
		
	
		// TODO Auto-generated method stub
		
	}
	public String getAddressKey() {
		return addressKey;
	}

	public void setAddressKey(String addressKey) {
		this.addressKey = addressKey;
	}

  
	

	public String getMasterKey() {
		return masterKey;
	}

	public void setMasterKey(String masterKey) {
		this.masterKey = masterKey;
	}

	public String getQueueNo() {
		return queueNo;
	}

	public void setQueueNo(String queueNo) {
		this.queueNo = queueNo;
	}	
	

}
