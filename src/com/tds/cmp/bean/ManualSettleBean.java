package com.tds.cmp.bean;

public class ManualSettleBean {
	private String description="";
	private String date="";
	private String amount="";
	private String txnID="";
	private String txnType="";
	private String txnLocation = "";
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getamt() {
		return amount;
	}
	public void setAmount(String amt) {
		this.amount = amt;
	}
	public String getTxnID() {
		return txnID;
	}
	public void settxnID(String txnID) {
		this.txnID = txnID;
	}
	public String gettxnType() {
		return txnType;
	}
	public void settxnType (String txnType) {
		this.txnType = txnType;
	}
	public String getTxnLocation() {
		return txnLocation;
	}
	public void setTxnLocation (String txnLocation) {
		this.txnLocation = txnLocation;
	}
	
}
