package com.tds.cmp.bean;

public class MessageInMemory {

	String command = "";
	long msgID = 0;
	String message = "";
	String tripID = "";
	
	public MessageInMemory(String command, long msgID, String message,
			String tripID) {
		super();
		this.command = command;
		this.msgID = msgID;
		this.message = message;
		this.tripID = tripID;
		//System.sy
	}
	
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public long getMsgID() {
		return msgID;
	}
	public void setMsgID(long msgID) {
		this.msgID = msgID;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTripID() {
		return tripID;
	}
	public void setTripID(String tripID) {
		this.tripID = tripID;
	}
}
