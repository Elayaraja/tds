package com.tds.cmp.bean;

import java.math.BigDecimal;

public class MeterType {
	private String meterName = "";
	private BigDecimal ratePerMile;
	private BigDecimal ratePerMin;
	private BigDecimal startAmt;
	private int minSpeed=0;
	private int key=0;
	private int defaultMeter=0;
	private BigDecimal rate1;
	private BigDecimal rate2;
	private BigDecimal rate3;
	private int dis1=0;
	private int dis2=0;
	private int dis3=0;
	
	
	
	public BigDecimal getRate1() {
		return rate1;
	}
	public void setRate1(BigDecimal rate1) {
		this.rate1 = rate1;
	}
	public BigDecimal getRate2() {
		return rate2;
	}
	public void setRate2(BigDecimal rate2) {
		this.rate2 = rate2;
	}
	public BigDecimal getRate3() {
		return rate3;
	}
	public void setRate3(BigDecimal rate3) {
		this.rate3 = rate3;
	}
	public int getDis1() {
		return dis1;
	}
	public void setDis1(int dis1) {
		this.dis1 = dis1;
	}
	public int getDis2() {
		return dis2;
	}
	public void setDis2(int dis2) {
		this.dis2 = dis2;
	}
	public int getDis3() {
		return dis3;
	}
	public void setDis3(int dis3) {
		this.dis3 = dis3;
	}
	public String getMeterName() {
		return meterName;
	}
	public void setMeterName(String meterName) {
		this.meterName = meterName;
	}
	public BigDecimal getRatePerMile() {
		return ratePerMile;
	}
	public void setRatePerMile(BigDecimal ratePerMile) {
		this.ratePerMile = ratePerMile;
	}
	public BigDecimal getRatePerMin() {
		return ratePerMin;
	}
	public void setRatePerMin(BigDecimal ratePerMin) {
		this.ratePerMin = ratePerMin;
	}
	public BigDecimal getStartAmt() {
		return startAmt;
	}
	public void setStartAmt(BigDecimal startAmt) {
		this.startAmt = startAmt;
	}
	public int getMinSpeed() {
		return minSpeed;
	}
	public void setMinSpeed(int minSpeed) {
		this.minSpeed = minSpeed;
	}
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public int getDefaultMeter() {
		return defaultMeter;
	}
	public void setDefaultMeter(int defaultMeter) {
		this.defaultMeter = defaultMeter;
	}

}
