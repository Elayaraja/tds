package com.tds.cmp.bean;

import java.util.ArrayList;

public class ComplaintsBean
{

public ArrayList<ComplaintDetailBean> getComplaintDetails() {
		return complaintDetails;
	}
	public void setComplaintDetails(ArrayList<ComplaintDetailBean> complaintDetails) {
		this.complaintDetails = complaintDetails;
	}
public String getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}
private String driverId = "";
private String complaintNo= "";
private String passengerName= "";
private String assoCode= "";
private String passengerId= "";
private String passengerPhone= "";
private String Status= "";
private String Desc= "";
private String dateTime="";
private final String status="";
private String CreatedBy="";
private String tripId="";
private ArrayList<ComplaintDetailBean> complaintDetails;


public String getDateTime() {
	return dateTime;
}
public void setDateTime(String dateTime) {
	this.dateTime = dateTime;
}
private boolean updateStatus= false;
private String Subject= "";
public String getSubject() {
	return Subject;
}
public void setSubject(String subject) {
	Subject = subject;
}
public boolean isUpdateStatus() {
	return updateStatus;
}
public void setUpdateStatus(boolean updateStatus) {
	this.updateStatus = updateStatus;
}
public String getDriverId() {
	return driverId;
}
public void setDriverId(String driverId) {
	this.driverId = driverId;
}
public String getComplaintNo() {
	return complaintNo;
}
public void setComplaintNo(String complaintNo) {
	this.complaintNo = complaintNo;
}
public String getPassengerName() {
	return passengerName;
}
public void setPassengerName(String passengerName) {
	this.passengerName = passengerName;
}
public String getAssoCode() {
	return assoCode;
}
public void setAssoCode(String assoCode) {
	this.assoCode = assoCode;
}
public String getPassengerId() {
	return passengerId;
}
public void setPassengerId(String passengerId) {
	this.passengerId = passengerId;
}
public String getPassengerPhone() {
	return passengerPhone;
}
public void setPassengerPhone(String passengerPhone) {
	this.passengerPhone = passengerPhone;
}
public String getStatus() {
	return Status;
}
public void setStatus(String status) {
	Status = status;
}
public String getDesc() {
	return Desc;
}
public void setDesc(String desc) {
	Desc = desc;
}
public void setTripId(String tripId) {
	this.tripId = tripId;
}
public String getTripId() {
	return tripId;
}


}