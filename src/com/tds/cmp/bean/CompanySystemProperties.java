package com.tds.cmp.bean;

public class CompanySystemProperties {
	
	
	private int timeOutOperator = 0;
	private int timeOutDriver = 0 ;
	private String state="";
	private String country="";
	private int dispatchBasedOnDriverOrVehicle;
	private int callerId;

	private int driveralarm;
	private int providePhoneNum;
	
	private int provideEndAddress;
	private String ORTime="";
	private int ORFormat;
	private double RatePerMile;
	private boolean calculateZones;
	private boolean bookAutomatically;
	private int checkDocuments;
	private String totalDigitsForPh;
	private int currencyPrefix=1;
	private String minimumSpeed;
	private double ratePerMinute;
	private String startAmount;
	private int simultaneousLogin;
	private int addressFill=1;
	private String mobilePassword="";
	private int driverListMobile=1;
	private int distanceBasedOn =0;
	private boolean meterMandatory =false;
	private boolean startMeterAutomatically =false;
	private boolean startmeterHidden=false;
	private String windowTime = "";
	private String loginLimitTime ="";
	private int noProgress=0;
	private int meterOn=0;
	private int remoteNoTrip=0;
	private int noTripTooSoon=0;
	private double distanceNoProgress=0.0;
	private double distanceMeterOn=0.0;
	private double distanceRemoteNoTrip=0.0;
	private double distanceNoTripTooSoon=0.0;
	private double appRatePerMile=0.0;
	private double appRatePerMinute=0.0;
	private double appStartRate=0.0;
	private String timeZoneArea="";
	private int tagDeviceCab=0;
	private int distanceValueToFare=0;
	private int zoneLogoutTime=0;
	private int meterTypes=0;
	private int futureCounts=0;
	private double onsiteDist=0.0;
	private int frequencyOfPosting=0;
	private boolean checkBalance;
	private double driverBalanceAmount=0.00;
	private String mobilePrefix="";
	private String smsPrefix="";
	private int jobAction;
	private int ccForCustomerApp=0;
	private int paymentForCustomerApp=0;
	private int ETA_Calculate=0;
	private String ccDefaultAmt="";
	private int batchForSR=0;
	private int sendBCJ=0;
	private int ZonesStepaway=0;
	private String paymentMandatory="0";
	private int seeZoneStatus;
	private double creditCharge=0.00;
	private int callerIdType=0;
	private int no_show=0;
	private int driverDirection = 1;
	private int zone_login=1;
	private int fleetDispatchSwitch = 0; 
	private int storeGPSHistory = 0;
	private int checkDriverMobile = 0;
	private int checkDr_Mob_Version = 0;
	private int sendPushToCustomer = 0;
	private String DefaultLanguage="en";
	private int enable_stAdd_flagTrip = 0;
	private int power_mode = 0;
	private int enable_power_save = 1;
	
	public int getEnable_power_save() {
		return enable_power_save;
	}
	public void setEnable_power_save(int enable_power_save) {
		this.enable_power_save = enable_power_save;
	}
	public int getPower_mode() {
		return power_mode;
	}
	public void setPower_mode(int power_mode) {
		this.power_mode = power_mode;
	}
	public int getETA_Calculate() {
		return ETA_Calculate;
	}
	public void setETA_Calculate(int eTA_Calculate) {
		ETA_Calculate = eTA_Calculate;
	}
	public String getPaymentMandatory() {
		return paymentMandatory;
	}
	public void setPaymentMandatory(String paymentMandatory) {
		this.paymentMandatory = paymentMandatory;
	}
	public String getTimeZoneArea() {
		return timeZoneArea;
	}
	public int getCcForCustomerApp() {
		return ccForCustomerApp;
	}
	public void setCcForCustomerApp(int ccForCustomerApp) {
		this.ccForCustomerApp = ccForCustomerApp;
	}
	public int getPaymentForCustomerApp() {
		return paymentForCustomerApp;
	}
	public void setPaymentForCustomerApp(int paymentForCustomerApp) {
		this.paymentForCustomerApp = paymentForCustomerApp;
	}
	public void setTimeZoneArea(String timeZoneArea) {
		this.timeZoneArea = timeZoneArea;
	}
	public int getTagDeviceCab() {
		return tagDeviceCab;
	}
	public void setTagDeviceCab(int tagDeviceCab) {
		this.tagDeviceCab = tagDeviceCab;
	}
	public double getAppRatePerMile() {
		return appRatePerMile;
	}
	public void setAppRatePerMile(double appRatePerMile) {
		this.appRatePerMile = appRatePerMile;
	}
	public double getAppRatePerMinute() {
		return appRatePerMinute;
	}
	public void setAppRatePerMinute(double appRatePerMinute) {
		this.appRatePerMinute = appRatePerMinute;
	}
	public double getAppStartRate() {
		return appStartRate;
	}
	public void setAppStartRate(double appStartRate) {
		this.appStartRate = appStartRate;
	}

	public double getDistanceNoProgress() {
		return distanceNoProgress;
	}
	public void setDistanceNoProgress(double distanceNoProgress) {
		this.distanceNoProgress = distanceNoProgress;
	}
	public double getDistanceMeterOn() {
		return distanceMeterOn;
	}
	public void setDistanceMeterOn(double distanceMeterOn) {
		this.distanceMeterOn = distanceMeterOn;
	}
	public double getDistanceRemoteNoTrip() {
		return distanceRemoteNoTrip;
	}
	public void setDistanceRemoteNoTrip(double distanceRemoteNoTrip) {
		this.distanceRemoteNoTrip = distanceRemoteNoTrip;
	}
	public double getDistanceNoTripTooSoon() {
		return distanceNoTripTooSoon;
	}
	public void setDistanceNoTripTooSoon(double distanceNoTripTooSoon) {
		this.distanceNoTripTooSoon = distanceNoTripTooSoon;
	}

	public int getNoProgress() {
		return noProgress;
	}
	public void setNoProgress(int noProgress) {
		this.noProgress = noProgress;
	}
	public int getMeterOn() {
		return meterOn;
	}
	public void setMeterOn(int meterOn) {
		this.meterOn = meterOn;
	}
	public int getRemoteNoTrip() {
		return remoteNoTrip;
	}
	public void setRemoteNoTrip(int remoteNoTrip) {
		this.remoteNoTrip = remoteNoTrip;
	}
	public int getNoTripTooSoon() {
		return noTripTooSoon;
	}
	public void setNoTripTooSoon(int noTripTooSoon) {
		this.noTripTooSoon = noTripTooSoon;
	}

	public int getAddressFill() {
		return addressFill;
	}
	public void setAddressFill(int addressFill) {
		this.addressFill = addressFill;
	}
	public int getSimultaneousLogin() {
		return simultaneousLogin;
	}
	public void setSimultaneousLogin(int simultaneousLogin) {
		this.simultaneousLogin = simultaneousLogin;
	}

	public String getStartAmount() {
		return startAmount;
	}
	public void setStartAmount(String startAmount) {
		this.startAmount = startAmount;
	}

	public String getMinimumSpeed() {
		return minimumSpeed;
	}
	public void setMinimumSpeed(String minimumSpeed) {
		this.minimumSpeed = minimumSpeed;
	}
	public double getRatePerMinute() {
		return ratePerMinute;
	}
	public void setRatePerMinute(double ratePerMinute) {
		this.ratePerMinute = ratePerMinute;
	}

	public boolean getCalculateZones() {
		return calculateZones;
	}
	public void setCalculateZones(boolean calculateZones) {
		this.calculateZones = calculateZones;
	}	

	
	public double getRatePerMile() {
		return RatePerMile;
	}
	public void setRatePerMile(double RatePerMile) {
		this.RatePerMile = RatePerMile;
	}
	
	public int getProvideEndAddress() {
		return provideEndAddress;
	}
	public void setProvideEndAddress(int provideEndAddress) {
		this.provideEndAddress = provideEndAddress;
	}
	public int getProvidePhoneNum() {
		return providePhoneNum;
	}
	public void setProvidePhoneNum(int providePhoneNum) {
		this.providePhoneNum = providePhoneNum;
	}
	public int getDriveralarm() {
		return driveralarm;
	}
	public void setDriveralarm(int driveralarm) {
		this.driveralarm = driveralarm;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	/*public boolean isAllowCreditCardReturns() {
		return allowCreditCardReturns;
	}
	public void setAllowCreditCardReturns(boolean allowCreditCardReturns) {
		this.allowCreditCardReturns = allowCreditCardReturns;
	}
	public boolean isCvvCodeManadatory() {
		return cvvCodeManadatory;
	}
	public void setCvvCodeManadatory(boolean cvvCodeManadatory) {
		this.cvvCodeManadatory = cvvCodeManadatory;
	}
	public boolean isCheckCabNoOnLogin() {
		return checkCabNoOnLogin;
	}
	public void setCheckCabNoOnLogin(boolean checkCabNoOnLogin) {
		this.checkCabNoOnLogin = checkCabNoOnLogin;
	}*/
	public int getTimeOutOperator() {
		return timeOutOperator;
	}
	public void setTimeOutOperator(int timeOutOperator) {
		this.timeOutOperator = timeOutOperator;
	}
	public int getTimeOutDriver() {
		return timeOutDriver;
	}
	public int getTimeOutDriverInSec() {
		return timeOutDriver*60;
	}
	public void setTimeOutDriver(int timeOutDriver) {
		this.timeOutDriver = timeOutDriver;
	}
	private String associateCode = "";
	
	public String getAssociateCode() {
		return associateCode;
	}
	public void setAssociateCode(String associateCode) {
		this.associateCode = associateCode;
	}
	private String preline1 = "";
	private String preline2 = "";
	private String preline3 = "";
	private String preline4 = "";
	private String preline5 = "";
	private String postline1 = "";
	private String postline2 = "";
	private String postline3 = "";
	private String postline4 = "";
	private String postline5 = "";
	private String postline6 = "";
	private String postline7 = "";
	
	
	
	public String getPreline1() {
		return preline1;
	}
	public void setPreline1(String preline1) {
		this.preline1 = preline1;
	}
	public String getPreline2() {
		return preline2;
	}
	public void setPreline2(String preline2) {
		this.preline2 = preline2;
	}
	public String getPreline3() {
		return preline3;
	}
	public void setPreline3(String preline3) {
		this.preline3 = preline3;
	}
	public String getPreline4() {
		return preline4;
	}
	public void setPreline4(String preline4) {
		this.preline4 = preline4;
	}
	public String getPreline5() {
		return preline5;
	}
	public void setPreline5(String preline5) {
		this.preline5 = preline5;
	}
	public String getPostline1() {
		return postline1;
	}
	public void setPostline1(String postline1) {
		this.postline1 = postline1;
	}
	public String getPostline2() {
		return postline2;
	}
	public void setPostline2(String postline2) {
		this.postline2 = postline2;
	}
	public String getPostline3() {
		return postline3;
	}
	public void setPostline3(String postline3) {
		this.postline3 = postline3;
	}
	public String getPostline4() {
		return postline4;
	}
	public void setPostline4(String postline4) {
		this.postline4 = postline4;
	}
	public String getPostline5() {
		return postline5;
	}
	public void setPostline5(String postline5) {
		this.postline5 = postline5;
	}
	public String getPostline6() {
		return postline6;
	}
	public void setPostline6(String postline6) {
		this.postline6 = postline6;
	}
	public String getPostline7() {
		return postline7;
	}
	public void setPostline7(String postline7) {
		this.postline7 = postline7;
	}
	
    private double timezone;
    private double timezoneMin;
    
	public double getTimezoneMin() {
		return timezoneMin;
	}
	public void setTimezoneMin(Double timezoneMin) {
		this.timezoneMin = timezoneMin;
	}
	public double getTimezone() {
		return timezone;
	}
	public void setTimezone(Double timezone) {
		this.timezone = timezone;
	}
	
	private String allowCreditCardReturns = "";
	private String cvvCodeManadatory = "";
	private String checkCabNoOnLogin = "";


	public String getAllowCreditCardReturns() {
		return allowCreditCardReturns;
	}
	public void setAllowCreditCardReturns(String allowCreditCardReturns) {
		this.allowCreditCardReturns = allowCreditCardReturns;
	}
	public String getCvvCodeManadatory() {
		return cvvCodeManadatory;
	}
	public void setCvvCodeManadatory(String cvvCodeManadatory) {
		this.cvvCodeManadatory = cvvCodeManadatory;
	}
	public String getCheckCabNoOnLogin() {
		return checkCabNoOnLogin;
	}
	public void setCheckCabNoOnLogin(String checkCabNoOnLogin) {
		this.checkCabNoOnLogin = checkCabNoOnLogin;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public int getDispatchBasedOnDriverOrVehicle() {
		return dispatchBasedOnDriverOrVehicle;
	}
	public void setDispatchBasedOnDriverOrVehicle(int dispatch) {
		this.dispatchBasedOnDriverOrVehicle = dispatch;
	}

	public int getCallerId() {
		return callerId;
	}
	public void setCallerId(int callerId) {
		this.callerId = callerId;
	}
	
	public String getORTime() {
		return ORTime;
	}
	public void setORTime(String oRTime) {
		ORTime = oRTime;
	}
	
	public int getORFormat() {
		return ORFormat;
	}
	public void setORFormat(int oRFormat) {
		ORFormat = oRFormat;
	}
	public void setCheckDocuments(int checkDocuments) {
		this.checkDocuments = checkDocuments;
	}
	public int getCheckDocuments() {
		return checkDocuments;
	}
	public void setTotalDigitsForPh(String totalDigitsForPh) {
		this.totalDigitsForPh = totalDigitsForPh;
	}
	public String getTotalDigitsForPh() {
		return totalDigitsForPh;
	}
	public void setCurrencyPrefix(int currencyPrefix) {
		this.currencyPrefix = currencyPrefix;
	}
	public int getCurrencyPrefix() {
		return currencyPrefix;
	}
	public void setMobilePassword(String mobilePassword) {
		this.mobilePassword = mobilePassword;
	}
	public String getMobilePassword() {
		return mobilePassword;
	}
	public void setDriverListMobile(int driverListMobile) {
		this.driverListMobile = driverListMobile;
	}
	public int getDriverListMobile() {
		return driverListMobile;
	}
	public void setDistanceBasedOn(int distanceBasedOn) {
		this.distanceBasedOn = distanceBasedOn;
	}
	public int getDistanceBasedOn() {
		return distanceBasedOn;
	}
	public void setMeterMandatory(boolean meterMandatory) {
		this.meterMandatory = meterMandatory;
	}
	public boolean isMeterMandatory() {
		return meterMandatory;
	}
	public void setWindowTime(String windowTime) {
		this.windowTime = windowTime;
	}
	public String getWindowTime() {
		return windowTime;
	}
	public void setLoginLimitTime(String loginLimitTime) {
		this.loginLimitTime = loginLimitTime;
	}
	public String getLoginLimitTime() {
		return loginLimitTime;
	}
	public void setDistanceValueToFare(int distanceValueToFare) {
		this.distanceValueToFare = distanceValueToFare;
	}
	public int getDistanceValueToFare() {
		return distanceValueToFare;
	}
	public void setZoneLogoutTime(int zoneLogoutTime) {
		this.zoneLogoutTime = zoneLogoutTime;
	}
	public int getZoneLogoutTime() {
		return zoneLogoutTime;
	}
	public int getMeterTypes() {
		return meterTypes;
	}
	public void setMeterTypes(int meterTypes) {
		this.meterTypes = meterTypes;
	}

	public int getFutureCounts() {
		return futureCounts;
	}
	public void setFutureCounts(int FutureCounts) {
		this.futureCounts =FutureCounts;
	}
	public double getOnsiteDist() {
		return onsiteDist;
	}
	public void setOnsiteDist(Double onsiteDist) {
		this.onsiteDist = onsiteDist;
	}
	public int getFrequencyOfPosting() {
		return frequencyOfPosting;
	}
	public void setFrequencyOfPosting(int frequencyOfPosting) {
		this.frequencyOfPosting = frequencyOfPosting;
	}
	public boolean isBookAutomatically() {
		return bookAutomatically;
	}
	public void setBookAutomatically(boolean bookAutomatically) {
		this.bookAutomatically = bookAutomatically;
	}
	public boolean isCheckBalance() {
		return checkBalance;
	}
	public void setCheckBalance(boolean checkBalance) {
		this.checkBalance = checkBalance;
	}
	
	public double getDriverBalanceAmount() {
		return driverBalanceAmount;
	}
	public void setDriverBalanceAmount(double driverBalanceAmount) {
		this.driverBalanceAmount = driverBalanceAmount;
	}
	
	public String getMobilePrefix() {
		return mobilePrefix;
	}
	public void setMobilePrefix(String mobilePrefix) {
		this.mobilePrefix = mobilePrefix;
	}
	public int getJobAction() {
		return jobAction;
	}
	public void setJobAction(int jobAction) {
		this.jobAction = jobAction;
	}
	public String getCcDefaultAmt() {
		return ccDefaultAmt;
	}
	public void setCcDefaultAmt(String ccDefaultAmt) {
		this.ccDefaultAmt = ccDefaultAmt;
	}
	public int getBatchForSR() {
		return batchForSR;
	}
	public void setBatchForSR(int batchForSR) {
		this.batchForSR = batchForSR;
	}
	public String getSmsPrefix() {
		return smsPrefix;
	}
	public void setSmsPrefix(String smsPrefix) {
		this.smsPrefix = smsPrefix;
	}
	public boolean isStartMeterAutomatically() {
		return startMeterAutomatically;
	}
	public void setStartMeterAutomatically(boolean startMeterAutomatically) {
		this.startMeterAutomatically = startMeterAutomatically;
	}
	public int getSendBCJ() {
		return sendBCJ;
	}
	public void setSendBCJ(int sendBCJ) {
		this.sendBCJ = sendBCJ;
	}
	public boolean isStartmeterHidden() {
		return startmeterHidden;
	}
	public void setStartmeterHidden(boolean startmeterHidden) {
		this.startmeterHidden = startmeterHidden;
	}
	public int getZonesStepaway() {
		return ZonesStepaway;
	}
	public void setZonesStepaway(int zonesStepaway) {
		ZonesStepaway = zonesStepaway;
	}
	public int getSeeZoneStatus() {
		return seeZoneStatus;
	}
	public void setSeeZoneStatus(int seeZoneStatus) {
		this.seeZoneStatus = seeZoneStatus;
	}
	public double getCreditCharge() {
		return creditCharge;
	}
	public void setCreditCharge(double creditCharge) {
		this.creditCharge = creditCharge;
	}
	public int getCallerIdType() {
		return callerIdType;
	}
	public void setCallerIdType(int callerIdType) {
		this.callerIdType = callerIdType;
	}
	public int getNo_show() {
		return no_show;
	}
	public void setNo_show(int no_show) {
		this.no_show = no_show;
	}
	public int getDriverDirection() {
		return driverDirection;
	}
	public void setDriverDirection(int driverDirection) {
		this.driverDirection = driverDirection;
	}
	public int getZone_login() {
		return zone_login;
	}
	public void setZone_login(int zone_login) {
		this.zone_login = zone_login;
	}
	public int getFleetDispatchSwitch() {
		return fleetDispatchSwitch;
	}
	public void setFleetDispatchSwitch(int fleetDispatchSwitch) {
		this.fleetDispatchSwitch = fleetDispatchSwitch;
	}
	public int getStoreGPSHistory() {
		return storeGPSHistory;
	}
	public void setStoreGPSHistory(int storeGPSHistory) {
		this.storeGPSHistory = storeGPSHistory;
	}
	public int getCheckDriverMobile() {
		return checkDriverMobile;
	}
	public void setCheckDriverMobile(int checkDriverMobile) {
		this.checkDriverMobile = checkDriverMobile;
	}
	public int getCheckDr_Mob_Version() {
		return checkDr_Mob_Version;
	}
	public void setCheckDr_Mob_Version(int checkDr_Mob_Version) {
		this.checkDr_Mob_Version = checkDr_Mob_Version;
	}
	public int getSendPushToCustomer() {
		return sendPushToCustomer;
	}
	public void setSendPushToCustomer(int sendPushToCustomer) {
		this.sendPushToCustomer = sendPushToCustomer;
	}
	public String getDefaultLanguage() {
		return DefaultLanguage;
	}
	public void setDefaultLanguage(String defaultLanguage) {
		DefaultLanguage = defaultLanguage;
	}
	public int getEnable_stAdd_flagTrip() {
		return enable_stAdd_flagTrip;
	}
	public void setEnable_stAdd_flagTrip(int enable_stAdd_flagTrip) {
		this.enable_stAdd_flagTrip = enable_stAdd_flagTrip;
	}
	
	
}
