package com.tds.cmp.bean;

public class ComplaintDetailBean {
	public String createdBy;
	public int complaintNo;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String timeStamp;
	public String description;
	public String status="";

	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public int getComplaintNo() {
		return complaintNo;
	}
	public void setComplaintNo(int complaintNo) {
		this.complaintNo = complaintNo;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

}
