package com.tds.cmp.bean;

public class DriverVehicleBean {

private String key="";
private String shortDesc="";
private String longDesc="";
private int driverVehicleSW;
private int groupId;



public int getDriverVehicleSW() {
	return driverVehicleSW;
}
public void setDriverVehicleSW(int driverVehicleSW) {
	this.driverVehicleSW = driverVehicleSW;
}
public String getKey() {
	return key;
}
public void setKey(String key) {
	this.key = key;
}
public String getShortDesc() {
	return shortDesc;
}
public void setShortDesc(String shortDesc) {
	this.shortDesc = shortDesc;
}
public String getLongDesc() {
	return longDesc;
}
public void setLongDesc(String longDesc) {
	this.longDesc = longDesc;
}
public int getGroupId() {
	return groupId;
}
public void setGroupId(int groupId) {
	this.groupId = groupId;
}
	
}
