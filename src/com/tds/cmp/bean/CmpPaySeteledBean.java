package com.tds.cmp.bean;

public class CmpPaySeteledBean {
		
	private String c_assocode;
	private String c_sch_day;
	private String c_sch_time;
	private String c_sch_status;
	private String c_sch_day_name;
	private String c_AMPM;
	private String Dchargesrunstatus;
	 
	public String getDchargesrunstatus() {
		return Dchargesrunstatus;
	}
	public void setDchargesrunstatus(String dchargesrunstatus) {
		Dchargesrunstatus = dchargesrunstatus;
	}
	public String getC_AMPM() {
		return c_AMPM;
	}
	public void setC_AMPM(String cAMPM) {
		c_AMPM = cAMPM;
	}
	public String getC_sch_day_name() {
		return c_sch_day_name;
	}
	public void setC_sch_day_name(String cSchDayName) {
		c_sch_day_name = cSchDayName;
	}
	private String bstatus;
	
	
	
	public String getBstatus() {
		return bstatus;
	}
	public void setBstatus(String bstatus) {
		this.bstatus = bstatus;
	}
	 
	public String getC_assocode() {
		return c_assocode;
	}
	public void setC_assocode(String cAssocode) {
		c_assocode = cAssocode;
	}
	public String getC_sch_day() {
		return c_sch_day;
	}
	public void setC_sch_day(String cSchDay) {
		c_sch_day = cSchDay;
	}
	public String getC_sch_time() {
		return c_sch_time;
	}
	public void setC_sch_time(String cSchTime) {
		c_sch_time = cSchTime;
	}
	public String getC_sch_status() {
		return c_sch_status;
	}
	public void setC_sch_status(String cSchStatus) {
		c_sch_status = cSchStatus;
	}
 
	
}
