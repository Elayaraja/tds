package com.tds.cmp.bean;

public class CabDriverMappingBean {

	private String from_date;
	private String to_date;
	private String driver_id;
	private String assoccode;
	private String cab_no;
	private String cab_dr_key;
	private String opr_id;
	private String status;
	private String userKey="";
		
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFrom_date() {
		return from_date;
	}
	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}
	public String getTo_date() {
		return to_date;
	}
	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}
	public String getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}
	public String getAssoccode() {
		return assoccode;
	}
	public void setAssoccode(String assoccode) {
		this.assoccode = assoccode;
	}
	public String getCab_no() {
		return cab_no;
	}
	public void setCab_no(String cab_no) {
		this.cab_no = cab_no;
	}
	public String getCab_dr_key() {
		return cab_dr_key;
	}
	public void setCab_dr_key(String cab_dr_key) {
		this.cab_dr_key = cab_dr_key;
	}
	public String getOpr_id() {
		return opr_id;
	}
	public void setOpr_id(String opr_id) {
		this.opr_id = opr_id;
	}
	
	
}
