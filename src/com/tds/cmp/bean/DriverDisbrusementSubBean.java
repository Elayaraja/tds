package com.tds.cmp.bean;

public class DriverDisbrusementSubBean {

	private String edit;
	private String amount;
	private String pss_id;
	private String descr;
	private String type;
	
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getEdit() {
		return edit;
	}
	public void setEdit(String edit) {
		this.edit = edit;
	}
	public String getPss_id() {
		return pss_id;
	}
	public void setPss_id(String pss_id) {
		this.pss_id = pss_id;
	}
	
	
	
	
}
