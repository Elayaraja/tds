package com.tds.cmp.bean;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.tds.tdsBO.OpenRequestBO;

public class Geocoder {

	
	private String applicationId;
	private final static String YAHOOURL = "http://local.yahooapis.com/MapsService/V1/geocode";

	/**
	 * @return the applicationId
	 */
	public String getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId
	 *            the applicationId to set
	 */
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public ArrayList<OpenRequestBO>  geocode(Address address) throws  Exception {
		ArrayList ar_list = new ArrayList();
		ArrayList<OpenRequestBO> ar_list_or = new ArrayList<OpenRequestBO>();
		OpenRequestBO openBO =new OpenRequestBO();
		Coordinates geocoordinates = null;
		String web = YAHOOURL + "?appid=" + applicationId + "&street="
				+ createLocation(address);
		URL url;
		try {
			url = new URL(web);
			System.out.println("url"+url);
			// BufferedReader in = new BufferedReader(new );
			InputStream in = url.openStream();
			  
			ar_list = YahooXmlReader.readConfig(in);
			for(int i=0;i<ar_list.size();i=i+7){
			openBO.setSadd1((String)ar_list.get(i+2));
			openBO.setScity((String)ar_list.get(i+3));	
			openBO.setSstate((String)ar_list.get(i+4));	
			openBO.setSzip((String)ar_list.get(i+5));	
			openBO.setSlat((String)ar_list.get(i));	
			openBO.setSlong((String)ar_list.get(i+1));
			ar_list_or.add(openBO);
			}in.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ar_list_or;
	}

	private String createLocation(Address address) {
		String s = "";

		s += address.getStreet() != null ? address.getStreet() + "+" : "";
		s +=address.getState() !=null ? address.getState() + "+" : "";
		s += address.getCity() != null ? address.getCity() + "+" : "";
		s += address.getPostalCode() != null ? address.getPostalCode() + "+"
				: "";
		s += address.getCountry() != null ? address.getCountry() + "+" : "";
		if (s.endsWith("+")) {
			s = s.substring(0, s.length() - 1);
		}
		s = s.replace(" ", "+");
		return s;
	}
}