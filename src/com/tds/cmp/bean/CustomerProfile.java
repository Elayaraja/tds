package com.tds.cmp.bean;

import java.util.ArrayList;

public class CustomerProfile {
	
	private String CCprofile="";
	private String CClastfourdigits="";
	private String CCtype="";
	private String Premiumcustomer="";
	private String number = "";
	private String name = "";
	private String Add1 = "";
	private String Add2 = "";
	private ArrayList al_drList = new ArrayList();
	private ArrayList al_vecList = new ArrayList();
	private String drProfile="";
	private String vecProfile="";
	private String city = "";
	private String state = "";
	private String zip="";
	private String masterKey="";
	private String paymentType="";
	private String account="";
	private String description="";
	private String customerNo="";
	private String corporateCode="";
	private String Latitude = "";
	private String Longitude = "";
	private String comments = "";
	private String operComments="";
	private String eMail="";

	private ArrayList<Address> addresshistory = new ArrayList<Address>();
	private ArrayList<CustomerProfile> profileHistory = new ArrayList<CustomerProfile>();

	
	
	
	
	public ArrayList getAl_drList() {
		return al_drList;
	}

	public void setAl_drList(ArrayList al_drList) {
		this.al_drList = al_drList;
	}

	public ArrayList getAl_vecList() {
		return al_vecList;
	}

	public void setAl_vecList(ArrayList al_vecList) {
		this.al_vecList = al_vecList;
	}

	public String getDrProfile() {
		return drProfile;
	}

	public void setDrProfile(String drProfile) {
		this.drProfile = drProfile;
	}

	public String getVecProfile() {
		return vecProfile;
	}

	public void setVecProfile(String vecProfile) {
		this.vecProfile = vecProfile;
	}

	public String getMasterKey() {
		return masterKey;
	}

	public void setMasterKey(String masterKey) {
		this.masterKey = masterKey;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCorporateCode() {
		return corporateCode;
	}

	public void setCorporateCode(String corporateCode) {
		this.corporateCode = corporateCode;
	}

	public ArrayList<Address> getAddresshistory() {
		return addresshistory;
	}

	public void setAddresshistory(ArrayList<Address> addresshistory) {
		this.addresshistory = addresshistory;
	}

	public ArrayList<CustomerProfile> getProfileHistory() {
		return profileHistory;
	}

	public void setProfileHistory(ArrayList<CustomerProfile> profileHistory) {
		this.profileHistory = profileHistory;
	}

	public String getPremiumcustomer() {
		return Premiumcustomer;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public ArrayList<Address> getAddressHistory() {
		return addresshistory;
	}

	public void setAddressHistory(ArrayList<Address> addressHistory) {
		this.addresshistory = addressHistory;
	}

	public String getAdd1() {
		return Add1;
	}

	public void setAdd1(String add1) {
		Add1 = add1;
	}

	public String getAdd2() {
		return Add2;
	}

	public void setAdd2(String add2) {
		Add2 = add2;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPremiumcustomer(String premiumcustomer) {
		Premiumcustomer = premiumcustomer;
	}

	public String getCClastfourdigits() {
		return CClastfourdigits;
	}

	public void setCClastfourdigits(String cClastfourdigits) {
		CClastfourdigits = cClastfourdigits;
	}

	public String getCCtype() {
		return CCtype;
	}

	public void setCCtype(String cCtype) {
		CCtype = cCtype;
	}

	public String getCCprofile() {
		return CCprofile;
	}

	public void setCCprofile(String cCprofile) {
		CCprofile = cCprofile;
	}

	public void setAssociateCode(String associateCode) {
		// TODO Auto-generated method stub
		
	
		// TODO Auto-generated method stub
		
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String geteMail() {
		return eMail;
	}

	public String getOperComments() {
		return operComments;
	}

	public void setOperComments(String operComments) {
		this.operComments = operComments;
	}
	


}
