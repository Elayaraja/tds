package com.tds.cmp.bean;

public class OperatorShift
{

private String operatorId="";
private String associateCode="";
private String openTime;
private String openedBy="";
private String closeTime;
private String closedBy="";
private boolean csBean;
private String driverId;
private String description;
private String disbursementType;
private String chequeNumber;
private String amount;
private String payId;
private String status;
private int key=0;
private double odometerValue=0;
private String vehicleNo ="";

public double getOdometerValue() {
	return odometerValue;
}
public void setOdometerValue(double odometerValue) {
	this.odometerValue = odometerValue;
}
public int getKey() {
	return key;
}
public void setKey(int key) {
	this.key = key;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getDriverId() {
	return driverId;
}
public void setDriverId(String driverId) {
	this.driverId = driverId;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public String getDisbursementType() {
	return disbursementType;
}
public void setDisbursementType(String disbursementType) {
	this.disbursementType = disbursementType;
}
public String getChequeNumber() {
	return chequeNumber;
}
public void setChequeNumber(String chequeNumber) {
	this.chequeNumber = chequeNumber;
}
public String getAmount() {
	return amount;
}
public void setAmount(String amount) {
	this.amount = amount;
}
public String getPayId() {
	return payId;
}
public void setPayId(String payId) {
	this.payId = payId;
}
public boolean isCsBean() {
	return csBean;
}
public void setCsBean(boolean csBean) {
	this.csBean = csBean;
}

private boolean isRegisterOpen;

public boolean isRegisterOpen() {
	return isRegisterOpen;
}
public void setRegisterOpen(boolean isRegisterOpen) {
	this.isRegisterOpen = isRegisterOpen;
}
public String getOperatorId() {
	return operatorId;
}
public void setOperatorId(String operatorId) {
	this.operatorId = operatorId;
}
public String getAssociateCode() {
	return associateCode;
}
public void setAssociateCode(String associateCode) {
	this.associateCode = associateCode;
}

public String getOpenTime() {
	return openTime;
}
public void setOpenTime(String openTime) {
	this.openTime = openTime;
}
public String getOpenedBy() {
	return openedBy;
}
public void setOpenedBy(String openedBy) {
	this.openedBy = openedBy;
}
public String getCloseTime() {
	return closeTime;
}
public void setCloseTime(String closeTime) {
	this.closeTime = closeTime;
}
public String getClosedBy() {
	return closedBy;
}
public void setClosedBy(String closedBy) {
	this.closedBy = closedBy;
}
public void setVehicleNo(String vehicleNo) {
	this.vehicleNo = vehicleNo;
}
public String getVehicleNo() {
	return vehicleNo;
}


}

