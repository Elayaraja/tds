package com.tds.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.common.util.TDSConstants;

public class TDSConnection {
	
	/*private static Category cat =TDSController.cat;
	
	static {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+TDSConnection.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}*/
	
	Context ctx;
    Connection con ;
    DataSource ds;
    
    /** Creates a new instance of DBConnection */
    public TDSConnection() { 
        try
        {
        	/*
        	 *   Code for Weblogic Server
        	 *  
        	 */
        	/*Hashtable ht = new Hashtable();
        	ht.put(Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory");
        	ht.put(Context.PROVIDER_URL, "t3://localhost:7001");
        	ctx = new InitialContext(ht);
        	ds = (DataSource) ctx.lookup("tds");*/
        	/*
        	 *   
        	 *  End of Weblogic Server
        	 */
        	
        	 /* 
        	 *   For Web Sphere 
        	 */
        	/* ctx = new InitialContext();
         	ds  = (DataSource)ctx.lookup("java:comp/env/jdbc/tds2");*/
         	
        	/*
        	 *  End of Web Shpere
        	 * 
        	 */
           ctx = new InitialContext();
            Context envContext  = (Context)ctx.lookup("java:/comp/env");
            
            //System.out.println("Context is got"+envContext.toString());
            ds  = (DataSource)envContext.lookup(TDSConstants.JNDI_CONNN);//CustomerSql.JNDI_NAME);//java:/comp/env/jdbc/royalty");	           
            
           // System.out.println("DataSource is got"+ds.toString());
        }
        catch(NamingException nex){
        	//System.out.println("Not able to find the DataSource" + nex);
        	//cat.info("Not able to find the DataSource" + nex);
        }
    }
     
    public Connection getConnection(){
        try{ 
            if(con == null){
                con = ds.getConnection();
                //System.out.println("===================Opening Connection:-" );
            }  
        }        
        catch(SQLException sex){
        	//cat.info("Error in getConnection "+sex.getMessage());
        	System.out.println("Error in openConnection "+sex.getMessage());
        }
        return con;
    }	    
    
    public void closeConnection(){	    
        try{
         	if(con!=null && !con.isClosed()){
         		//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$Closing Connection:-");
        		con.close();
        		
        	}
        		
        }
        catch(SQLException sex){
        	//cat.info("Not able to close Connection "+ sex.getErrorCode() + " " + sex.getMessage());
        	//System.out.println("Not able to close Connection "+ sex.getErrorCode() + " " + sex.getMessage());
        	sex.printStackTrace();
        }
    }
}
