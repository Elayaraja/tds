package com.tds.cron.docLib;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.tds.constants.docLib.DocumentConstant;
import com.tds.db.TDSConnection;

public class DocumentExpDtVerify implements Job {

	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		TDSConnection dbcon = null;
		Connection con = null;
		Statement pst = null;
		try {
			//System.out.println("DocumentExpDtVerify Job Started>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ new Date());
			String query = "";
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			query = "update TDS_DOCUMENTS  set DOC_STATUS='"+DocumentConstant.STATUS_EXPIRED +"' where DOC_STATUS='"+DocumentConstant.STATUS_APPROVED +"' and STR_TO_DATE(DOC_EXP_DATE, '%m/%d/%Y') < STR_TO_DATE('"+getDefaultDate()+"', '%m/%d/%Y')";
			pst = con.createStatement();
			pst.execute(query);
			//System.out.println("DocumentExpDtVerify Job Completed>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+ new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try{
			pst.close();
			con.close();
			}catch(Exception e){
				
			}
			dbcon.closeConnection();
		}

	}
	
	 public static String getDefaultDate()
	    {
	      String strSysDate;
	      java.util.Date currentDate = new java.util.Date();

	      java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
	      strSysDate = formatter.format(currentDate);

	      return strSysDate;
	    }
}
