package com.tds.cron.docLib;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;

public class CronScheluder {

	public CronScheluder() throws Exception {

		SchedulerFactory sf = new StdSchedulerFactory();
		Scheduler sche = sf.getScheduler();
		sche.start();
		JobDetail jDetail = new JobDetailImpl("TDS", "documentExpriryVerify",DocumentExpDtVerify.class);
		// "0 0 0 * * ?" Fires at 12am every day
		CronTrigger crTrigger = new CronTriggerImpl("cronTriggerTDS", "NJob12",	"0 0 0 * * ?");
		sche.scheduleJob(jDetail, crTrigger);
	}
}
