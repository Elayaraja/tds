package com.tds.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.HashMap;



import org.apache.log4j.Category;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.common.util.TDSProperties;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import com.tds.controller.TDSController;

public class CheckUserInformation {
	
	private static Category cat =TDSController.cat;
	static {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given util class "+CheckUserInformation.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	
	public static HashMap checkPhoneNo(String p_phone) {
		int result = 1;
		String dataContent = "";
		String xmldata ="";
		HashMap hmp_phonelist = new HashMap();
		hmp_phonelist.put("address", "");
		hmp_phonelist.put("city", "");
		hmp_phonelist.put("state", "");
		hmp_phonelist.put("zip", "");
		hmp_phonelist.put("latitude", "");
		hmp_phonelist.put("longitude", "");
		try {
			String url = TDSProperties.getValue("phoneURL")+p_phone+TDSProperties.getValue("phoneLicene");
			cat.info("Given URL is "+url);
			URL serviceURL = new URL(url);
			BufferedReader bufferedStream = new BufferedReader( new InputStreamReader(serviceURL.openStream()));
			xmldata = bufferedStream.readLine();
			while (dataContent != null){
				xmldata += dataContent;
				dataContent = bufferedStream.readLine();
			}
			//System.out.println(xmldata);
			DOMParser parser = new DOMParser();
			parser.parse(new InputSource(new StringReader(xmldata)));
			Document document = parser.getDocument();
			Element element = document.getDocumentElement();
			NodeList nodeList = element.getChildNodes();
			NodeList contactNodeList;
			Node dataNode ;
			Node contactNode;
			int contactChild;
			for(int child = 0; child < nodeList.getLength(); child ++) {
				dataNode = nodeList.item(child);
				if(dataNode.getNodeName().equalsIgnoreCase("ERROR")) {
					cat.info("Error "+dataNode.getTextContent());
					result = 0;
				} 
				if(dataNode.getNodeName().equalsIgnoreCase("Contacts")) {
					contactNodeList = dataNode.getChildNodes();
					for(contactChild = 0; contactChild < contactNodeList.getLength(); contactChild++) {
						contactNode = contactNodeList.item(contactChild);
						if(contactNode.getNodeName().equalsIgnoreCase("contact")) {
				//			System.out.println("Address "+contactNode.getTextContent());
							contactNodeList = contactNode.getChildNodes();
							break;
						}
					}
					
					for(contactChild = 0; contactChild < contactNodeList.getLength();contactChild++) {
						contactNode = contactNodeList.item(contactChild);
						if(contactNode.getNodeName().equalsIgnoreCase("address")) {
							hmp_phonelist.put("address", contactNode.getTextContent());
					//		System.out.println("Address "+contactNode.getTextContent());
						} 
						if(contactNode.getNodeName().equalsIgnoreCase("city")) {
							hmp_phonelist.put("city", contactNode.getTextContent());
						}
						if(contactNode.getNodeName().equalsIgnoreCase("state")) {
							hmp_phonelist.put("state", contactNode.getTextContent());
						}
						if(contactNode.getNodeName().equalsIgnoreCase("zip")) {
							hmp_phonelist.put("zip", contactNode.getTextContent());
						}
					}
					
				}
			}
			hmp_phonelist.put("result", result+"");
			//System.out.println("Data in XML "+hmp_phonelist);
			cat.info(" Result 0 - invalid phone no 1 - valid phone");
			cat.info("Result "+result);
		} catch(IOException exp) {
			cat.info("Expention in while reading URL "+exp.getMessage());
		}  catch (SAXException sax) {
			cat.info("Error in Reading Xml "+sax.getMessage());
		}
		return hmp_phonelist;
	}
	
	public static int checkAddress(String p_address,String p_city,String p_state,String p_postalcode) {
		int result = 1;
		String dataContent = "";
		String xmldata ="";
		try {
			String url = TDSProperties.getValue("addressURL")+"Address="+p_address+"&City="+p_city+"&State="+p_state+"&PostalCode="+p_postalcode+"&LicenseKey="+TDSProperties.getValue("addressLicence");
			url = url.replaceAll(" ", "+");
			cat.info("Given URL is "+url);
			//System.out.println("URL for Address Checking "+url);
			URL serviceURL = new URL(url);
			
			BufferedReader bufferedStream = new BufferedReader( new InputStreamReader(serviceURL.openStream()));
			xmldata = bufferedStream.readLine();
			while (dataContent != null){
				xmldata += dataContent;
				dataContent = bufferedStream.readLine();
			}
			DOMParser parser = new DOMParser();
			parser.parse(new InputSource(new StringReader(xmldata)));
			Document document = parser.getDocument();
			Element element = document.getDocumentElement();
			NodeList nodeListist = element.getChildNodes();
			Node dataNode ;
			for(int child = 0; child < nodeListist.getLength(); child ++) {
				dataNode = nodeListist.item(child);
				if(dataNode.getNodeName().equalsIgnoreCase("ERROR")) {
					cat.info("Error "+dataNode.getTextContent());
					result = 0;
				} 
			}
			cat.info(" Result 0 - invalid phone no 1 - valid phone");
			cat.info("Result "+result);
		} catch(IOException exp) {
			cat.info("Expention in while reading URL "+exp.getMessage());
		}  catch (SAXException sax) {
			cat.info("Error in Reading Xml "+sax.getMessage());
		}
		return result;
	}
	
	public static HashMap checkPhoneByAPI (String phone) {
		HashMap hmp_result = new HashMap();
		String result = "0";
		String dataContent = "";
		String xmlConent = "";
		hmp_result.put("address", "");
		hmp_result.put("city", "");
		hmp_result.put("state", "");
		hmp_result.put("zip", "");
		hmp_result.put("latitude", "");
		hmp_result.put("longitude", "");
		try {
			String url = TDSProperties.getValue("phoneAPI")+phone+TDSProperties.getValue("phoneAPIKEY");
			//String url = "http://api.whitepages.com/reverse_phone/1.0/?phone="+phone+";api_key=b5c596c4010bebd0f483c53f18663b6c";
			cat.info("URL =" + url);
			//System.out.println("URL =" + url);
			URL serviceURL = new URL(url);
			BufferedReader bufferedStream = new BufferedReader(new InputStreamReader (serviceURL.openStream()));
			xmlConent = bufferedStream.readLine();
			while (xmlConent != null) {
				dataContent += xmlConent;
				xmlConent = bufferedStream.readLine();
			}
			//System.out.println("Data Content "+dataContent);
			DOMParser domParser = new DOMParser();
			domParser.parse(new InputSource (new StringReader(dataContent)));
			Document document =  domParser.getDocument();
			Element element = document.getDocumentElement();
			NodeList nodeList;
			nodeList = element.getElementsByTagName("wp:address");
			//System.out.println("Node List "+nodeList.getLength());
			if(nodeList.getLength() >0 ) {
				NodeList datanodeList = nodeList.item(0).getChildNodes();
				Node dataNode;
				for(int count = 0;count <datanodeList.getLength(); count++) {
					dataNode = datanodeList.item(count);
					if(dataNode.getNodeName().equalsIgnoreCase("wp:fullstreet")) {
						hmp_result.put("address", dataNode.getTextContent());
					}
					if(dataNode.getNodeName().equalsIgnoreCase("wp:city")) {
						hmp_result.put("city", dataNode.getTextContent());
					}
					if(dataNode.getNodeName().equalsIgnoreCase("wp:state")) {
						hmp_result.put("state", dataNode.getTextContent());
					}
					if(dataNode.getNodeName().equalsIgnoreCase("wp:zip")) {
						hmp_result.put("zip", dataNode.getTextContent());
					}
					result = "1";
				}
			}
			nodeList = element.getElementsByTagName("wp:geodata");
			if(nodeList.getLength() > 0) {
				NodeList geoNodeList = nodeList.item(0).getChildNodes();
				for(int count = 0;count < geoNodeList.getLength();count ++) {
					Node geoNode = geoNodeList.item(count);
					if(geoNode.getNodeName().equalsIgnoreCase("wp:latitude")) {
						hmp_result.put("latitude", geoNode.getTextContent());
					}
					if(geoNode.getNodeName().equalsIgnoreCase("wp:longitude")) {
						hmp_result.put("longitude", geoNode.getTextContent());
					}
				}
			}
		}catch(IOException exp) {
			//System.out.println("Expention in while reading URL "+exp.getMessage());
			cat.info("Expention in while reading URL "+exp.getMessage());
		}  catch (SAXException sax) {
			//System.out.println("Error in Reading Xml "+sax.getMessage());
			cat.info("Error in Reading Xml "+sax.getMessage());
		}
		hmp_result.put("result", result);
		return hmp_result;
	}
	
	
}
