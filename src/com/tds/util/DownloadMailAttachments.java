package com.tds.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TimerTask;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.dao.EmailSMSDAO;
import com.tds.tdsBO.DocumentBO;
public class DownloadMailAttachments  extends TimerTask{
	private static Category cat =TDSController.cat;


	public void handleMultipart(Multipart multipart) throws MessagingException, IOException {
		for (int i=0; i<multipart.getCount(); i++){
			handlePart(multipart.getBodyPart(i));
		}
	}

	public void handlePart(Part part)  throws MessagingException, IOException {
		String dposition = part.getDisposition();
		String cType = part.getContentType();
		if (dposition == null) { 
	//		System.out.println("Null: "  + cType);
			if ((cType.length() >= 10) && (cType.toLowerCase().substring(0, 10).equals("text/plain"))) {
				part.writeTo(System.out);
			}
			else {
		//		System.out.println("Other body: " + cType);
				part.writeTo(System.out);
			}
		} 
		else if (dposition.equalsIgnoreCase(Part.ATTACHMENT)) {
			//System.out.println("Attachment: " + part.getFileName() + " : " + cType);
			saveFile(part.getFileName(), part.getInputStream());
		} 
		else if (dposition.equalsIgnoreCase(Part.INLINE)) {
			//System.out.println("Inline: " + part.getFileName() +  " : " + cType);
			saveFile(part.getFileName(), part.getInputStream());
		}
		else {  
			System.out.println("Other: " + dposition);
		}
	}

	public void saveFile(String filename,InputStream input) throws IOException {
		if (filename == null) {
			filename = File.createTempFile("MailAttacheFile", ".out").getName();
		}
	//	System.out.println("downloading attachment...");
		File file = new File(filename);
		for (int i=0; file.exists(); i++) {
			file = new File(filename+i);
		}
		FileOutputStream fos = new FileOutputStream(file);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		BufferedInputStream bis = new BufferedInputStream(input);
		int fByte;
		while ((fByte = bis.read()) != -1) {
			bos.write(fByte);
		}
		bos.flush();
		bos.close();
		bis.close();
		//System.out.println("done attachment...");
	}

	@Override
	public void run() {
		if(true){
			return;
		}
		String content="";
		String host = "";
		Message message[]=null;
		String username="";
		String password="";
		String assoccode="";
		String fromAddress="";
		String serverHost="";
		String port="";
		ArrayList<String> emails=new ArrayList<String>();
		//EmailSMSDAO emailDAO =new EmailSMSDAO();
		emails=EmailSMSDAO.emailUsers("");
	//	System.out.println("size--->"+emails.size());
		for(int k=0;k<emails.size();k=k+6){
			username=emails.get(k);
			password=emails.get(k+1);
			assoccode=emails.get(k+2);
			host=emails.get(k+3);
			port=emails.get(k+4);
			serverHost=emails.get(k+5);
			
			try{
			Session session = Session.getInstance(new Properties(), null);
			Store store = session.getStore("pop3s");
			store.connect(host, username, password);
			Folder folder = store.getFolder("INBOX");
			folder.open(Folder.READ_WRITE);
			message = folder.getMessages();
			if(message.length<1){
				store.close();

			}
			for(int i = 0; i<message.length; i++){
				if(message[i].getContentType().contains("multipart/mixed")) {
					List<File> attachments = new ArrayList<File>();
					for (Message message1 : message) {
						Multipart multipart = (Multipart) message1.getContent();
						for (int i1 = 0; i1 < multipart.getCount(); i1++) {
							BodyPart bodyPart = multipart.getBodyPart(i1);
							if(!Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
								continue; // dealing with attachments only
							} 
							InputStream is = bodyPart.getInputStream();
							File f = new File("/tmp/" + bodyPart.getFileName());
							FileOutputStream fos = new FileOutputStream(f);
							byte[] buf = new byte[4096];
							int bytesRead;
							while((bytesRead = is.read(buf))!=-1) {
								fos.write(buf, 0, bytesRead);
							}
							fos.close();
							attachments.add(f);
							String driverId[]=null;
							String driver="";
							if( message[i].getSubject().contains(":")){
								driverId=message[i].getSubject().split(": ");
								driver=driverId[1];
								
							}else{
								driver= message[i].getSubject();
							}
							File tmpFile = new File("/tmp/"+bodyPart.getFileName());
							InputStream fis = new FileInputStream(tmpFile);
							int len = (int) tmpFile.length();
							int result=0;
							String fileName=tmpFile.getName();
							String tmpFileName="";
							String from= message[i].getFrom()[0].toString();
							if(tmpFile.getName().contains(".")){
								tmpFileName=tmpFile.getName().split("\\.")[0];
							}
							String type="";
							ArrayList<DocumentBO> documentTypes=new ArrayList<DocumentBO>();
							documentTypes=EmailSMSDAO.getDocumentTypes(assoccode,tmpFileName);
							if(documentTypes.size()>0){
							for(int j=0;j<documentTypes.size();j++){
								if(tmpFileName.equalsIgnoreCase(documentTypes.get(j).getShortName())){
									type=documentTypes.get(j).getDocumentType();
									result=EmailSMSDAO.insertDocument(driver, assoccode, tmpFileName, fileName, from, fis, len,type);
								}
								type="";
							}
							}else{
								//content=content+"\n Provide the correct File Name ";
								result=EmailSMSDAO.insertDocument(driver, assoccode, tmpFileName, fileName, from, fis, len,"");

							}
							//fromAddress=message[i].getFrom()[0].toString();
							message[i].setFlag(Flags.Flag.DELETED, true);
						/*	if(result==1){
								content=content+"\n"+ i1+". DOCUMENT "+bodyPart.getFileName()+" uploaded successfully. \n";
							}else if(result==0){
								content=content+"\n"+ i1+". DOCUMENT "+bodyPart.getFileName()+" not uploaded.Having Some Fault.Check and resend. \n";
							}else{
								content=content+"\n Failed to update.Give the Correct Driver Id in Subject of the Mail and Resend \n";
							}
*/
						}
					}
				}
				//folder.close(true);
				store.close();
				/*if(fromAddress!=null && !(fromAddress).equals("")){
					GmailSmtpSSL emailNotify = new GmailSmtpSSL(username, password);
					try{
		    			//String[] emails = new String[""];
						emailNotify.sendMailTo(fromAddress,"Result Of Document Upload", content);
						} catch (Exception e ){
		    			System.out.println(e.toString());
		    		}
					GmailSmtpSSL emailNotify = new GmailSmtpSSL(username, password);
					emailNotify.sendMailTo(fromAddress,"Result Of Document Upload", content);
				}
				fromAddress="";
*/			}
			//folder.close(true);
			}catch (Exception e){
		//	System.out.println("DownloadMailAttachments error--->"+e.getMessage());	
			e.printStackTrace();
			}
		}
	}
}
