package com.tds.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;

public class MapParser {
	public static String getLatLongForAddr(String dcodeXML)
	{
		int result = 0;
		String latlong="";
		
		String xmldata ="",dataContent="";
		try {
			
				DOMParser parser = new DOMParser();
				
				//parser.parse(new InputSource(new StringReader(dcodeXML.trim())));
				parser.parse(new InputSource(new StringReader("<?xml version=\"1.0\" encoding=\"UTF-8\"?><GeocodeResponse><status>OK</status><result><type>street_address</type><formatted_address>1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA</formatted_address><address_component><long_name>1600</long_name><short_name>1600</short_name><type>street_number</type></address_component><address_component><long_name>Amphitheatre Pkwy</long_name><short_name>Amphitheatre Pkwy</short_name><type>route</type></address_component>" +
						"<address_component><long_name>Mountain View</long_name><short_name>Mountain View</short_name><type>locality</type><type>political</type></address_component><address_component><long_name>San Jose</long_name><short_name>San Jose</short_name><type>administrative_area_level_3</type><type>political</type></address_component><address_component><long_name>Santa Clara</long_name><short_name>Santa Clara</short_name><type>administrative_area_level_2</type><type>political</type>" +
						"</address_component><address_component><long_name>California</long_name> <short_name>CA</short_name><type>administrative_area_level_1</type><type>political</type></address_component><address_component><long_name>United States</long_name><short_name>US</short_name>" +
						"<type>country</type> <type>political</type></address_component><address_component><long_name>94043</long_name><short_name>94043</short_name><type>postal_code</type></address_component><geometry> <location>" +
						"<lat>37.4217550</lat><lng>-122.0846330</lng></location><location_type>ROOFTOP</location_type><viewport><southwest><lat>37.4188514</lat><lng>-122.0874526</lng>" +
						"</southwest><northeast><lat>37.4251466</lat><lng>-122.0811574</lng></northeast> </viewport></geometry></result></GeocodeResponse>")));
				
	/*	
		 	parser.parse(new InputSource(new StringReader("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
						"<ResultSet xsi:schemaLocation=urn:yahoo:maps http://local.yahooapis.com/MapsService/V1/GeocodeResponse.xsd>" +
							"<Result precision>address" +
								"<Latitude>39.945646"+"</Latitude>"+
								"<Longitude>-104.954288"+"</Longitude>"+
								"<Address>2667 E 138th Ave"+"</Address>"+
								"<City>Thornton"+"</City>"+
								"<State>CO"+"</State>"+
								"<Zip>80602"+"</Zip>"+
								"<Country>US"+"</Country>"+ 
							"</Result>" +
							"</ResultSet>")));*/ 
				
				Document document = parser.getDocument();
				Element element = document.getDocumentElement();
				NodeList nodeList = element.getChildNodes();
				NodeList resultNodeList,geoNodeList,locNodeList;
				Node dataNode,resultNode,geoNode,locNode;
				//System.out.println(nodeList.getLength());
				for(int child = 0; child < nodeList.getLength(); child ++) {
					dataNode = nodeList.item(child);
					if(dataNode.getNodeName().equalsIgnoreCase("status")) {
					//	System.out.println("Status" + dataNode.getTextContent());
						result = 0;
					}
					if(dataNode.getTextContent().equalsIgnoreCase("OK"))
					if(dataNode.getNodeName().equalsIgnoreCase("result")) {
	 
						resultNodeList = dataNode.getChildNodes();
						for(int resultChild = 0; resultChild < resultNodeList.getLength(); resultChild++) {
							resultNode = resultNodeList.item(resultChild);
							if(resultNode.getNodeName().equalsIgnoreCase("geometry")) {
								 
								geoNodeList = resultNode.getChildNodes();
								for(int geoChild = 0; geoChild < geoNodeList.getLength(); geoChild++) {
									geoNode = geoNodeList.item(geoChild);
									if(geoNode.getNodeName().equalsIgnoreCase("location")) {
										 
										locNodeList = geoNode.getChildNodes();
										for(int locChild = 0; locChild < locNodeList.getLength(); locChild++) {
											locNode = locNodeList.item(locChild);
										 
											if(locNode.getNodeName().equalsIgnoreCase("lat"))
											{
												latlong = latlong + locNode.getTextContent()+"^";
						//						System.out.println("111111111111111111111111111111:"+latlong);
											}
											if(locNode.getNodeName().equalsIgnoreCase("lng"))
											{
												latlong = latlong + locNode.getTextContent();
						//						System.out.println("11111111111221111111111111111111:"+latlong);
												
											}
										}
									}
								}
							}
						}
					}
				}
			
	        
		} catch(IOException exp) {
			 exp.printStackTrace();
		}  catch (SAXException sax) {
			 sax.printStackTrace();
		}
		
		return latlong;
		
	}

	/*public static String getLatLongForAddr(String Add1,String Add2,String city,String state,String zip)
	{
		int result = 0;
		String latlong="";
		
		String xmldata ="",dataContent="";
		try {
			URL serviceURL = new URL(TDSProperties.getValue("latLongURL"));
			
			HttpURLConnection hConnection = (HttpURLConnection)serviceURL.openConnection();
	        HttpURLConnection.setFollowRedirects( true );
	        hConnection.setDoOutput( true );
	         
	        hConnection.setRequestMethod("POST");	
	        PrintStream ps = new PrintStream(hConnection.getOutputStream() );
	        //address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=true_or_false
	        String add = "address="+(!Add1.equals("")?(Add1.replace(' ', '+')+","):"")+(!Add2.equals("")?(Add2.replace(' ', '+')+","):"")+(!city.equals("")?(city.replace(' ', '+')+","):"")+(!state.equals("")?(state.replace(' ', '+')+","):"")+(!zip.equals("")?zip.replace(' ', '+'):"");
	        if(add.endsWith(","))
	        	add = add.substring(0,add.length()-1);
	        
	        ps.print(add+"&sensor=true");
	        System.out.println(TDSProperties.getValue("latLongURL")+add+"&sensor=true");
	        ps.close(); 
	        hConnection.connect();
	        if( HttpURLConnection.HTTP_OK == hConnection.getResponseCode() )
	        {
	        	InputStream is = hConnection.getInputStream();
				BufferedReader bufferedStream = new BufferedReader( new InputStreamReader(is));
				while((xmldata = bufferedStream.readLine())!=null)
				{
					dataContent += xmldata;
				}
				System.out.println("Data Content:"+dataContent);
				DOMParser parser = new DOMParser();
				parser.parse(new InputSource(new StringReader(dataContent)));
				parser.parse(new InputSource(new StringReader("<?xml version=\"1.0\" encoding=\"UTF-8\"?><GeocodeResponse><status>OK</status><result><type>street_address</type><formatted_address>1600 Amphitheatre Pkwy, Mountain View, CA 94043, USA</formatted_address><address_component><long_name>1600</long_name><short_name>1600</short_name><type>street_number</type></address_component><address_component><long_name>Amphitheatre Pkwy</long_name><short_name>Amphitheatre Pkwy</short_name><type>route</type></address_component>" +
						"<address_component><long_name>Mountain View</long_name><short_name>Mountain View</short_name><type>locality</type><type>political</type></address_component><address_component><long_name>San Jose</long_name><short_name>San Jose</short_name><type>administrative_area_level_3</type><type>political</type></address_component><address_component><long_name>Santa Clara</long_name><short_name>Santa Clara</short_name><type>administrative_area_level_2</type><type>political</type>" +
						"</address_component><address_component><long_name>California</long_name> <short_name>CA</short_name><type>administrative_area_level_1</type><type>political</type></address_component><address_component><long_name>United States</long_name><short_name>US</short_name>" +
						"<type>country</type> <type>political</type></address_component><address_component><long_name>94043</long_name><short_name>94043</short_name><type>postal_code</type></address_component><geometry> <location>" +
						"<lat>37.4217550</lat><lng>-122.0846330</lng></location><location_type>ROOFTOP</location_type><viewport><southwest><lat>37.4188514</lat><lng>-122.0874526</lng>" +
						"</southwest><northeast><lat>37.4251466</lat><lng>-122.0811574</lng></northeast> </viewport></geometry></result></GeocodeResponse>")));
				
		
				Document document = parser.getDocument();
				Element element = document.getDocumentElement();
				NodeList nodeList = element.getChildNodes();
				NodeList resultNodeList,geoNodeList,locNodeList;
				Node dataNode,resultNode,geoNode,locNode;
				
				for(int child = 0; child < nodeList.getLength(); child ++) {
					dataNode = nodeList.item(child);
					if(dataNode.getNodeName().equalsIgnoreCase("status")) {
						System.out.println("Status" + dataNode.getTextContent());
						result = 0;
					}
					if(dataNode.getTextContent().equalsIgnoreCase("OK"))
					if(dataNode.getNodeName().equalsIgnoreCase("result")) {
	 
						resultNodeList = dataNode.getChildNodes();
						for(int resultChild = 0; resultChild < resultNodeList.getLength(); resultChild++) {
							resultNode = resultNodeList.item(resultChild);
							if(resultNode.getNodeName().equalsIgnoreCase("geometry")) {
								 
								geoNodeList = resultNode.getChildNodes();
								for(int geoChild = 0; geoChild < geoNodeList.getLength(); geoChild++) {
									geoNode = geoNodeList.item(geoChild);
									if(geoNode.getNodeName().equalsIgnoreCase("location")) {
										 
										locNodeList = geoNode.getChildNodes();
										for(int locChild = 0; locChild < locNodeList.getLength(); locChild++) {
											locNode = locNodeList.item(locChild);
										 
											if(locNode.getNodeName().equalsIgnoreCase("lat"))
											{
												latlong = latlong + locNode.getTextContent()+"^";
											}
											if(locNode.getNodeName().equalsIgnoreCase("lng"))
											{
												latlong = latlong + locNode.getTextContent();
											}
										}
									}
								}
							}
						}
					}
				}
			
	        }
		} catch(IOException exp) {
			 exp.printStackTrace();
		}  catch (SAXException sax) {
			 sax.printStackTrace();
		}
		
		return latlong;
		
	}*/
	
	/*public static void main(String a[])
	{
		MapParser m = new MapParser();
		m.getLatLongForAddr("", "", "", "", "");
	}*/
}
