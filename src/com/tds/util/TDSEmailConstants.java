package com.tds.util;

public interface TDSEmailConstants {
	

	/*
	* Basic controller fields
	*/
	
	public static final String ATT= "@xt.att.net"; 
	public static final String BoostMobile= "@myboostmobile.com";	 
	public static final String Cricket= "@sms.mycricket.com"; 
	public static final String MetroPCS= "@mymetropcs.com"; 
	public static final String SimpleMobile= "@vtex.com"; 
	public static final String Sprint= "@messaging.sprintpcs.com"; 
	public static final String TMobile = "@tmomail.net";
	public static final String Verizon= "@vtext.com"; 
	public static final String VirginMobile= "@vmobl.com";
}
	 