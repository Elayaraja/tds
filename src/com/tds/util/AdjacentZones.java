package com.tds.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.common.util.DistanceCalculation;
import com.tds.cmp.bean.ZoneTableBeanSP;

public class AdjacentZones {
	public static void calculateDistance( ArrayList<ZoneTableBeanSP> zones,double currentLat,double currentLon){
		for(int i=0;i<zones.size();i++){
			double latCenter=(zones.get(i).getNorthLatitude()+zones.get(i).getSouthLatitude())/2;
			double lonCenter=(zones.get(i).getEastLongitude()+zones.get(i).getWestLongitude())/2;
			zones.get(i).setDistanceTest(DistanceCalculation.distance(currentLat,latCenter,currentLon,lonCenter));
		}
	}
	public static void sortAdjacentZones(ArrayList<ZoneTableBeanSP> zoneList){
		Collections.sort(zoneList,new Comparator<ZoneTableBeanSP>(){
			public int compare(ZoneTableBeanSP firstObject, ZoneTableBeanSP secondObject) {
				return firstObject.getDistanceTest().compareTo(secondObject.getDistanceTest());
			}
		});
	}
}
