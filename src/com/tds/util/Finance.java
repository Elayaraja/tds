package com.tds.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import com.charges.bean.DisbursementDetaillBean;
import com.charges.bean.DriverChargesBean;


public class Finance {
	public static void calculateChargesOnCharges(ArrayList<DisbursementDetaillBean> chargeCalcPctData , ArrayList<DriverChargesBean> listOfCharges){
		for(int i =0; i<chargeCalcPctData.size();i++){
			for(int j =0; j<listOfCharges.size();j++){
				if(chargeCalcPctData.get(i).getCTD_TYPE_CODE()==listOfCharges.get(j).getDC_TYPE_CODE()){
					if(chargeCalcPctData.get(i).getCTD_PAYMENT_SWITCH()==1){
						//Amount Calculation
						listOfCharges.get(j).setDC_DRIVER_PMT_TAX1_AMT(chargeCalcPctData.get(i).getCTD_TAX1_AMOUNT().setScale(2, RoundingMode.CEILING));
						listOfCharges.get(j).setDC_DRIVER_PMT_TAX2_AMT(chargeCalcPctData.get(i).getCTD_TAX2_AMOUNT().setScale(2, RoundingMode.CEILING));
						listOfCharges.get(j).setDC_DRIVER_PMT_TAX3_AMT(chargeCalcPctData.get(i).getCTD_TAX3_AMOUNT().setScale(2, RoundingMode.CEILING));
						//????
						BigDecimal total = listOfCharges.get(j).getDC_DRIVER_PMT_TAX1_AMT().add(listOfCharges.get(j).getDC_DRIVER_PMT_TAX2_AMT()).add(listOfCharges.get(j).getDC_DRIVER_PMT_TAX3_AMT());
						total.setScale(2, RoundingMode.CEILING);
						//System.out.println("Total:chargeCalcPctData.CTD_AMOUNT-"+total+":"+chargeCalcPctData.get(i).getCTD_AMOUNT());
						listOfCharges.get(j).setDC_DRIVER_PMT_AMT(total);
						//listOfCharges.get(j).setDC_DRIVER_PMT_AMT(chargeCalcPctData.get(i).getCTD_AMOUNT());
						
					} else {
						// Pct Calculation 
						listOfCharges.get(j).setDC_DRIVER_PMT_TAX1_AMT(listOfCharges.get(j).getTotalamount().multiply(chargeCalcPctData.get(i).getCTD_TAX1_AMOUNT().divide(new BigDecimal("100.00"))).setScale(2, RoundingMode.CEILING));
						listOfCharges.get(j).setDC_DRIVER_PMT_TAX2_AMT(listOfCharges.get(j).getTotalamount().multiply(chargeCalcPctData.get(i).getCTD_TAX2_AMOUNT().divide(new BigDecimal("100.00"))).setScale(2, RoundingMode.CEILING));
						listOfCharges.get(j).setDC_DRIVER_PMT_TAX3_AMT(listOfCharges.get(j).getTotalamount().multiply(chargeCalcPctData.get(i).getCTD_TAX3_AMOUNT().divide(new BigDecimal("100.00"))).setScale(2, RoundingMode.CEILING));
						///???
						BigDecimal total = listOfCharges.get(j).getDC_DRIVER_PMT_TAX1_AMT().add(listOfCharges.get(j).getDC_DRIVER_PMT_TAX2_AMT()).add(listOfCharges.get(j).getDC_DRIVER_PMT_TAX3_AMT());
						total.setScale(2, RoundingMode.CEILING);
						//System.out.println("Total:chargeCalcPctData.CTD_AMOUNT-"+total+":"+chargeCalcPctData.get(i).getCTD_AMOUNT());
						
						listOfCharges.get(j).setDC_DRIVER_PMT_AMT(total);
						//listOfCharges.get(j).setDC_DRIVER_PMT_AMT(listOfCharges.get(j).getTotalamount().multiply(chargeCalcPctData.get(i).getCTD_AMOUNT().divide(new BigDecimal("100.00"))));
					}
					//listOfCharges.get(j).setDC_TYPE_DESC(chargeCalcPctData.get(i).getCT_DESCRIPRION());

				}
			}
		}
	}
	public static void calculateChargesOnPayment(ArrayList<DisbursementDetaillBean> chargeCalcPctData , ArrayList<DriverChargesBean> listOfPayments){
		for(int i =0; i<chargeCalcPctData.size();i++){
			for(int j =0; j<listOfPayments.size();j++){
				if(chargeCalcPctData.get(i).getCTD_TYPE_CODE()==listOfPayments.get(j).getDC_TYPE_CODE()){
					listOfPayments.get(j).setDC_DRIVER_PMT_AMT(listOfPayments.get(j).getTotalamount().multiply(new BigDecimal("1").subtract(chargeCalcPctData.get(i).getCTD_PERCENT().divide(new BigDecimal("100.00")))));
					listOfPayments.get(j).setDC_TYPE_DESC(chargeCalcPctData.get(i).getCT_DESCRIPRION());
				}
			}
		}
	}



}
