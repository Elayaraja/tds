package com.tds.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.dao.ChargesDAO;
import com.common.util.CheckZone;
import com.common.util.MessageGenerateJSON;
import com.common.util.Messaging;
import com.common.util.OpenRequestUtil;
import com.common.util.TDSConstants;
import com.gac.mobile.dao.MobileDAO;
import com.tds.cmp.bean.CallerIDBean;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.AuditDAO;
import com.tds.dao.CallerIDDAO;
import com.tds.dao.CreditCardDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.EmailSMSDAO;
import com.tds.dao.MeterDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.ZoneDAO;
import com.tds.payment.PaymentCatgories;
import com.tds.payment.PaymentGatewaySlimCD;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.process.Email;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.ClientAuthenticationBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.MeterStatusBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueCoordinatesBO;

public class MeterControlUtil {

	public static MeterStatusBO enterMeterValues(HttpServletRequest request,HttpServletResponse response,MeterStatusBO meterBean,ServletConfig srvletConfig) throws UnsupportedEncodingException{
		OpenRequestBO openRequestBo = new OpenRequestBO();
		AdminRegistrationBO adminBo = new AdminRegistrationBO();
		MeterStatusBO meterBo=new MeterStatusBO();
		ArrayList<String> companyList = new ArrayList<String>();
		JSONObject jasonObject = new JSONObject();
		JSONObject header = new JSONObject();
		JSONArray array = new JSONArray();
		JSONArray subArray = new JSONArray();
		ApplicationPoolBO poolBO = (ApplicationPoolBO) srvletConfig.getServletContext().getAttribute("poolBO");
		if(meterBean.getTxn().equals("ME")){
			if(meterBean.getDriverId()!=null && !meterBean.getDriverId().equals("")){
				String password=CallerIDDAO.getPasswordId(meterBean.getCompanyCode()==null?"":meterBean.getCompanyCode());
				if(meterBean.getPass()!=null && meterBean.getPass().equals(password) && meterBean.getDriverId()!=null && !meterBean.getDriverId().equals("")){
					if(meterBean.getCabNo()!=null && !meterBean.getCabNo().equals("")){
						if(meterBean.getCompanyCode()!=null && !meterBean.getCompanyCode().equals("")){
							companyList.add(meterBean.getCompanyCode());
							meterBean.setOp(meterBean.getMeMeterStatus());
							int status=0;
							String errorReturn = "";
							if(meterBean.getOp().equals("1")){
								openRequestBo = DispatchDAO.getAllTripsForDriver(companyList,meterBean.getDriverId(),meterBean.getCabNo(),SystemPropertiesDAO.getParameter(meterBean.getCompanyCode(), "driverAlarm"),0);
								int logStatus = MobileDAO.checkCabAllocation(meterBean.getCompanyCode(), meterBean.getDriverId(),meterBean.getCabNo());
								if(openRequestBo.getTripid()!=null && !openRequestBo.getTripid().equals("")){
									if(ServiceRequestDAO.setStartTrip(openRequestBo,companyList)>0){
										AuditDAO.insertJobLogs("Trip Started By CMT Meter", openRequestBo.getTripid(), meterBean.getCompanyCode(), TDSConstants.startedTrip, "CMT Unit", meterBean.getLatitude(), meterBean.getLongitude(), meterBean.getCompanyCode());
										DispatchDAO.updateAcceptTime(meterBean.getCompanyCode(), openRequestBo.getTripid(),1,companyList);
										try {
											header.put("rV", 1.2);
											header.put("Act", "GMAC");
											header.put("rC", 200);
											header.put("Command","STP"); 
											header.put("TI",openRequestBo.getTripid()); 
											header.put("msgID",System.currentTimeMillis());

											jasonObject.put("rV", 1.2);
											jasonObject.put("Act", "GMAC");
											jasonObject.put("rC", 200);
											jasonObject.put("Command","STP"); 
											jasonObject.put("TI",openRequestBo.getTripid()); 
											jasonObject.put("msgID",System.currentTimeMillis());
											jasonObject.put("rV", 1.2);
											jasonObject.put("Act", "ST");
											jasonObject.put("rC", 200);
											jasonObject.put("STATUS", "SUCCESS");
											jasonObject.put("MESSAGE", "Trip Started Successfully");

											array.put(0, jasonObject);
											array.put(1, jasonObject);
											subArray.put(header);
										} catch (JSONException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										status=1;
										errorReturn = "Trip Started Successfully";
										DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(meterBean.getCompanyCode(), meterBean.getDriverId(), 1);
										String[] message=new String[2];
										message[0] = array.toString();
										message[1] = subArray.toString();
										if(cabQueueBean!=null && cabQueueBean.getDriverid()!=null){
											Messaging oneSMS = new Messaging(srvletConfig.getServletContext(),cabQueueBean, message,poolBO,meterBean.getCompanyCode());
											oneSMS.start();
										}
									}
								} else if(logStatus==0){
									openRequestBo = DispatchDAO.getAllTripsForDriver(companyList,meterBean.getDriverId(),meterBean.getCabNo(),SystemPropertiesDAO.getParameter(meterBean.getCompanyCode(), "driverAlarm"),2);
									if(openRequestBo.getTripid()==null || openRequestBo.getTripid().equals("")){
										openRequestBo.setSlat(meterBean.getLatitude());
										openRequestBo.setSlong(meterBean.getLongitude());
										openRequestBo.setDriverid(meterBean.getDriverId());
										openRequestBo.setAssociateCode(meterBean.getCompanyCode());
										openRequestBo.setVehicleNo(meterBean.getCabNo());
										Calendar currentDate = Calendar.getInstance();
										SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
										String dateNow = sdf.format(currentDate.getTime());
										openRequestBo.setShrs("2525");
										openRequestBo.setPhone("0000000000");
										openRequestBo.setName("Flag Trip CMT");
										openRequestBo.setSdate(dateNow);
										openRequestBo.setDays("0000000");
										openRequestBo.setTypeOfRide("0");
										openRequestBo.setTripSource(TDSConstants.flagTrip);
										openRequestBo.setSadd1("Flag Trip");
										openRequestBo.setAmt(new BigDecimal(meterBean.getAmt()).add(new BigDecimal(meterBean.getExtra())));
										openRequestBo.setChckTripStatus(TDSConstants.tripStarted);
										openRequestBo.setAdvanceTime("-1");

										adminBo.setAssociateCode(meterBean.getCompanyCode());
										adminBo.setMasterAssociateCode(meterBean.getCompanyCode());
										adminBo.setTimeZone(SystemPropertiesDAO.getParameter(adminBo.getMasterAssociateCode(), "timeZoneArea"));
										adminBo.setUid("CMT Unit");

										request.setAttribute("openrequestBOfromAction", openRequestBo);
										request.setAttribute("adminBoFromAction", adminBo);
										errorReturn = OpenRequestUtil.saveOpenRequest(request, response,srvletConfig);
										if(errorReturn.contains("OK")){
											String[] parseTI = errorReturn.split(";");
											String[] tripId = parseTI[0].split("=");
											status=MeterDAO.meterStatusInsert(meterBean);
											if(status==1){
												openRequestBo.setTripid(tripId[1]);
												String[] acceptedStatus = new String[2];
												acceptedStatus = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBo, "FA", 0, meterBean.getCompanyCode());
												DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(meterBean.getCompanyCode(), meterBean.getDriverId(), 1);
												if(cabQueueBean!=null && cabQueueBean.getDriverid()!=null){
													Messaging oneSMS = new Messaging(srvletConfig.getServletContext(),cabQueueBean, acceptedStatus,poolBO,meterBean.getCompanyCode());
													oneSMS.start();
												}
											}
										}
									}
								}
							} else if(meterBean.getOp().equals("2")) {
								errorReturn=MeterDAO.getFlagTripByDriver(meterBean.getDriverId(), meterBean.getCabNo(), meterBean.getCompanyCode());
								if(!errorReturn.equals("")){
									ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) srvletConfig.getServletContext().getAttribute((meterBean.getCompanyCode() +"Zones"));
									String m_queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(meterBean.getLatitude()),Double.parseDouble(meterBean.getLongitude()));
									if(m_queueId.equals("")){
										ZoneTableBeanSP zonesId = ZoneDAO.getDefaultZone(meterBean.getCompanyCode());
										m_queueId=zonesId.getZoneKey();
									} else {
										List<QueueCoordinatesBO> queueProp = SystemPropertiesDAO.getQueueCoOrdinateList(m_queueId, meterBean.getCompanyCode(), "");
									}

									BigDecimal fare = new BigDecimal(meterBean.getFare()==null?"0.00":meterBean.getFare());
									BigDecimal extras = new BigDecimal(meterBean.getExtra()==null?"0.00":meterBean.getExtra());
									openRequestBo.setAmt(fare.add(extras));

									openRequestBo.setEndQueueno(m_queueId);
									openRequestBo.setTripid(errorReturn);
									openRequestBo.setAssociateCode(meterBean.getCompanyCode());
									openRequestBo.setDriverid(meterBean.getDriverId());
									int endResult=ServiceRequestDAO.setEndTrip(openRequestBo, false,companyList);
									DispatchDAO.moveToHistory(errorReturn, meterBean.getCompanyCode(),"",companyList);
									AuditDAO.insertJobLogs("Trip Ended By CMT Meter", openRequestBo.getTripid(), meterBean.getCompanyCode(), TDSConstants.tripEnded, "CMT Unit", meterBean.getLatitude(), meterBean.getLongitude(), meterBean.getCompanyCode());
									status=MeterDAO.meterStatusInsert(meterBean);
									errorReturn = "Successfully Trip Completed";
									if(endResult>0){
										DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(meterBean.getCompanyCode(), meterBean.getDriverId(), 1);
										String[] message = MessageGenerateJSON.generateMessage(openRequestBo, "DC", System.currentTimeMillis(), meterBean.getCompanyCode());
										if (cabQueueBean.getDriverid() != null && !cabQueueBean.getDriverid().equals("")) {
											Messaging oneSMS = new Messaging(srvletConfig.getServletContext(),cabQueueBean, message,poolBO,meterBean.getCompanyCode());
											oneSMS.start();
										}
									}
								}
							} else {
								status=MeterDAO.meterStatusInsert(meterBean);
							}
							meterBo.setRespCode(meterBean.getTxn());
							meterBo.setStatusCode(status==1?"100":status+"");
							meterBo.setMessage(errorReturn);

						}else{
							meterBo.setRespCode("200");
							meterBo.setStatusCode("");
							meterBo.setMessage("CompanyCode is mandatory");
						}
					} else {
						meterBo.setRespCode("301");
						meterBo.setStatusCode("");
						meterBo.setMessage("Cab Number is mandatory");
					}
				}else{
					meterBo.setRespCode("400");
					meterBo.setStatusCode("");
					meterBo.setMessage("Username & Password doesnt match");
				}
			} else {
				meterBo.setRespCode("300");
				meterBo.setStatusCode("");
				meterBo.setMessage("Driver Id is mandatory");
			}
		} else {
			if(meterBean.getCompanyCode()!=null && !meterBean.getCompanyCode().equals("")){
				if(meterBean.getCcNum()!=null && meterBean.getCcNum().length()==4){
					String ccNumber="************"+meterBean.getCcNum();
					long infoKey=System.currentTimeMillis();
					int result = CreditCardDAO.insertCreditCardDetails(meterBean.getCompanyCode(),"CMT",meterBean.getCcNum(),ccNumber,meterBean.getCcExpiry(),"CMT",infoKey+"");
					if(result==1){
						meterBo.setRespCode(result==1?"100":result+"");
						meterBo.setStatusCode(infoKey+"");
						meterBo.setMessage("Your Card Successfully Added");
					} else {
						meterBo.setRespCode("CC");
						meterBo.setStatusCode("101");
						meterBo.setMessage("Something went wrong please check all your data");
					}
				} else {
					meterBo.setRespCode("301");
					meterBo.setStatusCode("");
					meterBo.setMessage("Card Number must be 4 digits");
				}
			} else {
				meterBo.setRespCode("200");
				meterBo.setStatusCode("");
				meterBo.setMessage("CompanyCode is mandatory");
			}
		}
		return meterBo; 
	}

	private static void put(JSONObject jasonObject) {
		// TODO Auto-generated method stub
	}

}
