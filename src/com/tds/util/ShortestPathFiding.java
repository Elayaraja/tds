package com.tds.util;

import java.util.ArrayList;

import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;

public class ShortestPathFiding {
	
	//Formula 1
	
	/*
	 * var R = 6371; // km
		var dLat = (lat2-lat1).toRad();
		var dLon = (lon2-lon1).toRad(); 
		var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		        Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
		        Math.sin(dLon/2) * Math.sin(dLon/2); 
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		var d = R * c;
	 */
	//Formula 2	
	/*
	 * 
	 * Math.acos(Math.sin(lat1)*Math.sin(lat2) + 
                  Math.cos(lat1)*Math.cos(lat2) *
                  Math.cos(lon2-lon1)) * R;
	 * 
	 * */
	
	public static OpenRequestBO getShortestDriver(ArrayList al_list,OpenRequestBO requestBO)
	{
		double R = 6371; // km 
		//ArrayList al_short = new ArrayList();
		double temp = 0, kms=0;
		//int postion = 0;
		double p_lat = Double.parseDouble(requestBO.getSlat());
		double p_long = Double.parseDouble(requestBO.getSlong());
		String driver="",drivername="";
		for(int i=0;i<al_list.size();i++)
		{
			DriverLocationBO driverLocationBO = (DriverLocationBO)al_list.get(i);
			
			if(Double.parseDouble(driverLocationBO.getLatitude()) == 0 && Double.parseDouble(driverLocationBO.getLongitude()) == 0)
			{
				driverLocationBO.setLatitude(requestBO.getSlat());
				driverLocationBO.setLongitude(requestBO.getSlong());
			}
			
			//System.out.println("Lat1(From Driver Location):" + driverLocationBO.getLatitude());
			//System.out.println("Lat2(Pickup Time):" + p_lat);
			//System.out.println("Long1(From Driver Location):" + driverLocationBO.getLongitude());
			//System.out.println("Long2(Pickup Time):" + p_long);
			
			//double dLat = Math.toRadians((Double.parseDouble(driverLocationBO.getLatitude()) -p_lat));
			double dLat = Math.toRadians(p_lat-(Double.parseDouble(driverLocationBO.getLatitude())));
			//double dLon = Math.toRadians((Double.parseDouble(driverLocationBO.getLongitude()) -p_long));
			double dLon = Math.toRadians(p_long - (Double.parseDouble(driverLocationBO.getLongitude())));
			double ave = Math.sin(dLat/2) * Math.sin(dLat/2)+
					     Math.cos(Math.toRadians(Double.parseDouble(driverLocationBO.getLatitude()))) * Math.cos(Math.toRadians(p_lat))*
					     Math.sin(dLon/2)*Math.sin(dLon/2);
			kms = R*(2*Math.atan2(Math.sqrt(ave),Math.sqrt(1-ave)));
			
			//System.out.println("Driver:- " + driverLocationBO.getDriverid() + "Kms:- " + kms);
			
			/*kms = Math.acos(Math.sin((Double.parseDouble(driverLocationBO.getLatitude()))))*Math.sin(p_lat) + 
					Math.cos(p_lat)*Math.cos((Double.parseDouble(driverLocationBO.getLatitude()))) *
		 					Math.cos((p_long-Double.parseDouble(driverLocationBO.getLongitude()))) * R;
		   */
			
			if(i == 0)
			{
				temp = kms;
				driver = driverLocationBO.getUid();
				drivername = driverLocationBO.getDriverid();
				//al_short.add(kms);
				//al_short.add(driverLocationBO.getUid());
				//al_short.add(driverLocationBO.getDriverid());
			
			} else {
				
				if(kms < temp)
				{
					temp = kms;
					driver = driverLocationBO.getUid();
					drivername = driverLocationBO.getDriverid();
				}
				
			}
		}
		
		/*
		for(int i=0;i<al_short.size();i=i+3)
		{
			if(i == 0)
			{
				postion = i;
				temp = Double.parseDouble(al_short.get(i).toString());
			} else {
				if(Double.parseDouble(al_short.get(i).toString()) < temp)
				{
					temp = Double.parseDouble(al_short.get(i).toString());
					postion = i;
				}
			} 
		} 
		
		 
		
		requestBO.setDriverid(al_short.get(postion+1).toString());
		requestBO.setDriverUName(al_short.get(postion+2).toString());
		al_short.clear(); */
		
		requestBO.setDriverid(driver);
		requestBO.setDriverUName(drivername);
		
		return requestBO;
	}
	
}
