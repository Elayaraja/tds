package com.tds.util;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.woden.wsdl20.Description;

import com.tds.cmp.bean.DriverCabQueueBean;
/**
 * @author vimal
 * @category Class
 * @see Description
 * This is class is used to send a Mail Using Java Mail API
 * 
 */
public class MailUtil {
	 
	 
	public void sendMail(String host,String user, String pwd,String emailFromAddress,ArrayList toAddress,String p_message) {
		final String SMTP_HOST_NAME = host;
		final String SMTP_AUTH_USER = user;
		final String SMTP_AUTH_PWD = pwd;
		//final String SMTP_PORT = "2;
		boolean debug = false;
		try{
		Properties props = new Properties();
	    props.put("mail.smtp.host", SMTP_HOST_NAME);
	    
	    Session session = Session.getDefaultInstance(props, null);
	    session.setDebug(debug);
	    
	    // create a message
	    MimeMessage msg = new MimeMessage(session);
	    InternetAddress addressFrom = new InternetAddress(emailFromAddress);
	    msg.setFrom(addressFrom);
	    
		Address[] m_internetToAddress = new InternetAddress[toAddress.size()];
		for(int m_counter=0;m_counter < toAddress.size(); m_counter ++) {
			m_internetToAddress[m_counter] = new InternetAddress(toAddress.get(m_counter).toString());
		}
		msg.setRecipients(MimeMessage.RecipientType.TO, m_internetToAddress);
		msg.setSubject(p_message);
	    //msg.setContent("Yow", "text/plain");
	    Transport.send(msg);
		} catch(Exception a_exception){
			a_exception.printStackTrace();
			 System.out.println("Exception==>>"+a_exception);
		}
		

	}

	public void translateMultipleSMSToMail(ArrayList<DriverCabQueueBean> smsBean) {
		if (smsBean.size()<1){
			return;
		}
		for(int i=0; i< smsBean.size();i++){
			translate(smsBean.get(i));
		}
		
		return;
	}

	public void translateSingleSMSToMail(DriverCabQueueBean smsBean) {
		translate(smsBean);
		return;
	}

	private void translate(DriverCabQueueBean smsBean){
		if(smsBean.getProvider().equalsIgnoreCase("ATT")){
			smsBean.setEmailAddress(smsBean.getPhoneNo()+"@txt.att.net");
		} else if (smsBean.getProvider().equalsIgnoreCase("BoostMobile")){
			smsBean.setEmailAddress(smsBean.getPhoneNo()+"@myboostmobile.com");
		} else if (smsBean.getProvider().equalsIgnoreCase("Cricket")){
			smsBean.setEmailAddress(smsBean.getPhoneNo()+"@sms.mycricket.com");
		} else if (smsBean.getProvider().equalsIgnoreCase("MetroPCS")){
			smsBean.setEmailAddress(smsBean.getPhoneNo()+"@mymetropcs.com");
		} else if (smsBean.getProvider().equalsIgnoreCase("SimpleMobile")){
			smsBean.setEmailAddress(smsBean.getPhoneNo()+"@smtext.com");
		} else if (smsBean.getProvider().equalsIgnoreCase("TMobile")){
			smsBean.setEmailAddress("1"+smsBean.getPhoneNo()+"@tmomail.net");
		} else if (smsBean.getProvider().equalsIgnoreCase("Verizon")){
			smsBean.setEmailAddress(smsBean.getPhoneNo()+"@vtext.com");
		} else if (smsBean.getProvider().equalsIgnoreCase("VirginMobile")){
			smsBean.setEmailAddress(smsBean.getPhoneNo()+"@vmobl.com");
		} else if (smsBean.getProvider().equalsIgnoreCase("Sprint")){
			smsBean.setEmailAddress(smsBean.getPhoneNo()+"@messaging.sprintpcs.com");
		}
		
	}

}
