package com.tds.util;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.asteriskjava.fastagi.DefaultAgiServer;

import com.tds.ivr.IVRHandler;

public class ContextListener implements ServletContextListener{
	public static int sessionCount = 0;
	private static final IVRHandler service=new IVRHandler();
	private static final DefaultAgiServer AGIserver=new DefaultAgiServer(service);
	

    @Override
    public void contextDestroyed(ServletContextEvent event) {
    	AGIserver.shutdown();
    }
	  public void contextInitialized(ServletContextEvent event) {
		  new Thread(new Runnable(){//This code must be inside a thread
		  //because it stops the owner thread to listen on a specific port
		  //and this cause your application lock on startup/deploy 
		  @Override
		  public void run() {
		  try {
			  AGIserver.startup();
		  } catch (IllegalStateException e) {
			  e.printStackTrace();
		  } catch (IOException e) {
			  e.printStackTrace();
		  }
		  }}).start();
	 
	  }}


