package com.tds.util;

import java.util.ArrayList;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import com.common.util.HandleMemoryMessage;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.tdsBO.AdminRegistrationBO;

public class SessionListener implements HttpSessionListener{
	public static int sessionCount = 0;

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		synchronized (this) {
			sessionCount++;
		}

//		System.out.println("Session Created: " + event.getSession().getId());
//		System.out.println("Total Sessions: " + sessionCount);
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		synchronized (this) {
			if(sessionCount > 0)
				sessionCount--;
		}
		
		if(event.getSession().getAttribute("user")!= null){
			AdminRegistrationBO adminBO = (AdminRegistrationBO) event.getSession().getAttribute("user"); 
			String associationCode = ((AdminRegistrationBO)(event.getSession().getAttribute("user"))).getAssociateCode();
			RegistrationDAO.InsertLoginDetail(associationCode,"", event.getSession().getId(),"0",1,event.getSession().getLastAccessedTime(),"","","","","","","",adminBO.getMasterAssociateCode(),"");
	//		System.out.println("SessionListenerOnDestroyUserType:"+ adminBO.getUsertype() );
	//		System.out.println("SessionListenerOnDestroyUserName:"+ adminBO.getUname());
			if(adminBO.getUsertype().equalsIgnoreCase("Driver")){
		//		System.out.println("Logging The Driver <<<"+adminBO.getUid()+">>> Out By Session Timeout");
				ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(adminBO.getUid());
				if(currentQueue!=null && currentQueue.size()>0){
					ServiceRequestDAO.Update_DriverLogOutfromQueue(adminBO.getUid(), adminBO.getVehicleNo(), currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+adminBO.getUid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". Due to session destroyed");
				}
				ServiceRequestDAO.deleteDriverFromQueue(adminBO.getUname(),adminBO.getMasterAssociateCode());
				ServiceRequestDAO.updateDriverAvailabilty("L", adminBO.getUname(),associationCode);
				HandleMemoryMessage.removeMessage(event.getSession().getServletContext(),adminBO.getAssociateCode(), adminBO.getUid());
			 	event.getSession().getServletContext().removeAttribute(event.getSession().getId()+"LO");
			}
		}
		//System.out.println("Session Destroyed: " + event.getSession().getId());
		//System.out.println("Total Sessions: " + sessionCount);
	}
	
	/*public static int getSessionCount()
	{
		return sessionCount;
	}*/
}
