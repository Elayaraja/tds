package com.tds.util;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import com.tds.dao.UtilityDAO;

public class AutoComplete extends HttpServlet {
	
	public void service(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

	PrintWriter out = response.getWriter();
	String targetId = request.getParameter("id");
//	System.out.println("In AutoComplete, Id:" + targetId);
	ArrayList list = UtilityDAO.getDriverList("AS103", 1);
	
	//list.add("sanjeev tuli"); list.add("sumit ranjan"); list.add("sam"); list.add("somesh"); list.add("mahesh"); list.add("madhu");
	//list.add("venkat"); list.add("veena"); list.add("veera"); list.add("veerat");
	
	if (targetId != null) targetId = targetId.trim().toLowerCase();
	StringBuffer sb = new StringBuffer();
	Iterator it = list.iterator();
		while (it.hasNext()) {
			String id = (String) it.next();
			if (targetId != null && id.startsWith(targetId)) {
				sb.append("<driver><id>"+ id +"</id><name>"+(String)it.next()+"</name></driver>");
				
			}
		}
	response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
	//System.out.println(sb.toString());
	response.getWriter().write("<drivers>"+sb.toString()+"</drivers>");
	out.close();
	}
}