package com.tds.util;

public class main {
	
	public static void main(String args[]) throws Exception {
		
		SampleApp SA = new SampleApp();
		
//		System.out.println("Hello First program");
		//GetACabTest test = new GetACabTest();
		
		//test.doStuff();
	///Preparing the application to transact
		///Note : The following steps should be performed during the initial setup of the application.
		/// The results of the following steps will be an ApplicationProfileId, ServiceId and MerchantProfileId.
		/// These values should be persisted/cached and use for Transaction Processing. The persistence/caching 
		/// should be treated similar user credentials. 
			
		/*Step 1 - Let's first get a Security Token from the Service Key and Identity token provided  
		 * in the Integration Recommendation.
		*/
		//System.exit(0);
		ConfigurationValues CV = new ConfigurationValues();
		 
		if(!SA.signOnWithToken(CV))return;
	//	System.out.println("Session Token:"+CV._SessionToken);
		/*Step 2 - If this is the first time running the application, you need to save application 
		 * and retrieve an applicationProfileId used in transacting.
		*/
		if(!SA.SaveApplicationConfiguration(CV))return;
		//System.out.println("Application:"+CV._ApplicationProfileId);
		/*Step 3 - Retrieve Service Information. In this scenario we'll use the first ServiceId returned
		*/
		if(!SA.GetFirstServiceId(CV))return;
		//System.out.println("Service:"+CV._ServiceId);
		/*Step 4 - The final step for preparing your application to transact is to save a merchant profile
		 * the application should store the name as this values will be used in all transactions. 
		*/
		if(!SA.SaveMerchantProfile(CV))return;
		//CV._MerchantProfileId="";
		//System.out.println("Merchant:" + CV._MerchantProfileId);
	///Transaction Processing
		/*Step 5 -  Authorizing. The user has two options for authorizing a card. The first "AuthorizeAndCapture()"
		 * is an immediate capture of funds. The second "Authorize()" is a pre-capture where funds are 
		 * reserved however not marked for capture. The "Authorize()" requires a follow-up "Capture" to mark the 
		 * transaction for settlement.  
		*/
		//if(!SA.AuthorizeAndCapture(CV))return;
		
		if(!SA.Authorize(CV))return;
		
		/*Step 6 - Adjusting and Voiding
		 * 
		*/
		if(!SA.Adjust(CV))return;
		if(!SA.Undo(CV))return;
		/*Step 7 - Capturing for Settlement
		 * If a transaction has not be marked for settlement then a capture is required. Such is the case for an Authorize transaction
		 * http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Capturing_Transactions_for_Settlement.aspx
		*/
		if(!SA.Capture(CV))return;
		
		/*Step 8 - Refunding
		 * 
		*/
		if(!SA.ReturnById(CV))return;
		if(!SA.ReturnUnlinked(CV))return;
		
		/*Step 9 - Optional Operations
		 * 
		*/
		
	}
}
