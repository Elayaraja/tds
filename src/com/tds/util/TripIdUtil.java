package com.tds.util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.common.util.TDSConstants;
import com.tds.dao.RequestDAO;

public class TripIdUtil {
	
/*	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	*//**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 *//*
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String eventParam = "";

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if((AdminRegistrationBO)request.getSession().getAttribute("user")!=null)
		{
			if(eventParam.equalsIgnoreCase("getTripId")) {
				getTrip(request,response); 
			}
		}
	}
	public void getTrip(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String tripId=RequestDAO.getTripId(adminBO.getAssociateCode());
		response.getWriter().write(tripId);
	}*/
	public synchronized static String tripIdAction(String assoccode,String masterAssoccode) throws ServletException, IOException {
		String tripId=RequestDAO.getTripId(assoccode,TDSConstants.tripEmpty,masterAssoccode);
		return tripId;
	}


}
