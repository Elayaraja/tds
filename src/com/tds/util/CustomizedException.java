package com.tds.util;

public class CustomizedException extends Exception {

	public CustomizedException(String msg)
	{
		super(msg);
	}
}
