package com.tds.util;

import java.util.ArrayList;
import java.util.TimerTask;

import javax.servlet.ServletConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.common.util.DistanceCalculation;
import com.common.util.MessageGenerateJSON;
import com.common.util.Messaging;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.common.util.TDSValidation;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.AuditDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.ZoneDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.DispatchPropertiesBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;

public class CustomerUtil extends TimerTask{
	String asscocode = "";
	String tripId = "";
	String timeOffset = "";
	AdminRegistrationBO admBo = null;
	OpenRequestBO openbo = null;
	public String jobName = "System-A";
	private ServletConfig config = null;
	
	public CustomerUtil (AdminRegistrationBO admBo, OpenRequestBO openBo, ServletConfig config){
		System.out.println("Constructor");
		this.admBo = admBo;
		this.openbo = openBo;
		this.tripId = openBo.getTripid();
		this.asscocode = admBo.getAssociateCode();
		this.timeOffset = admBo.getTimeZone();
		this.config = config;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		String wasl_ccode = TDSProperties.getValue("WASL_Company");
		if(asscocode.equals(wasl_ccode) && !tripId.equals("")){
			JSONObject obj = new JSONObject();
			try {
				obj.put("apiKey", "D42D418A-4FEE-42C9-9005-18C06F497DC6");
				
				String captainId = "";
				String vehicle_id = "";
				DriverLocationBO driverBo = DispatchDAO.getPhoneForDriverID(openbo.getDriverid(), asscocode);
				if(!driverBo.getJson_details().equals("")){
					JSONArray drievrArray = new JSONArray(driverBo.getJson_details());
					if(drievrArray!=null && drievrArray.length()>0){
						JSONObject driverObj = drievrArray.getJSONObject(0);
						captainId = driverObj.has("captainReferenceNumber")?driverObj.getString("captainReferenceNumber"):"";
						vehicle_id = driverObj.has("vehicleReferenceNumber")?driverObj.getString("vehicleReferenceNumber"):"";
					}
				}
				obj.put("vehicleReferenceNumber", vehicle_id);
				obj.put("captainReferenceNumber", captainId);
				
				double st_lat = Double.parseDouble(openbo.getSlat());
				double st_lon = Double.parseDouble(openbo.getSlong());
				double ed_lat = Double.parseDouble(openbo.getEdlatitude());
				double ed_lon = Double.parseDouble(openbo.getEdlongitude());
				
				obj.put("originLatitude", ""+st_lat);
				obj.put("originLongitude", ""+st_lon);
				obj.put("destinationLatitude", ""+ed_lat);
				obj.put("destinationLongitude", ""+ed_lon);
				obj.put("originCityNameInArabic", openbo.getScity());
				obj.put("destinationCityNameInArabic", openbo.getEcity());
				
				if(ed_lat!=0.00 && ed_lon!=0.00){
					double distanceInMiles = DistanceCalculation.distance(st_lat, ed_lat, st_lon, ed_lon);
					double distanceInMetres = distanceInMiles * 1609.34;
					obj.put("distanceInMeters", distanceInMetres);
				}else{
					obj.put("distanceInMeters", 0);
				}
				
				ArrayList<String> time_list = UtilityDAO.getFormattedTimeDetailsForWasl(asscocode, timeOffset, tripId);
				if(time_list!=null){
					obj.put("customerWaitingTimeInSeconds", time_list.get(0));
					obj.put("durationInSeconds", time_list.get(1));
					obj.put("pickupTimestamp", time_list.get(2));
					obj.put("dropoffTimestamp", time_list.get(3));
				}else{
					obj.put("durationInSeconds", "");
					obj.put("customerWaitingTimeInSeconds", "");
					obj.put("pickupTimestamp", "");
					obj.put("dropoffTimestamp", "");
				}
				
				//Rating has to be done later
				//obj.put("customerRating", "");
				
				System.out.println("Final obj:"+obj.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//Insert into Wasl openrequest table
			int result = UtilityDAO.insertIntoWaslOpenrequest(tripId, obj.toString(), asscocode);
			if(result<1){
				System.out.println("Problem insert into WASL table for tripId->"+tripId+": "+obj.toString()+" assocde;"+asscocode);
			}
		}
	}

}
