package com.tds.util;

public class ConfigurationValues {
	public String _SessionToken;
	public Long _SessionTokenDateTime;
	public String _ServiceId;
	public String _ApplicationProfileId;
	public String _MerchantProfileId;
	public boolean _Status;
}
