package com.tds.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.text.html.HTMLDocument.Iterator;
import javax.wsdl.Output;
import javax.xml.ws.WebServiceException;

import org.apache.axis2.databinding.types.soapencoding.DateTime;
import org.apache.axis2.extensions.spring.receivers.ApplicationContextHolder;
import org.apache.woden.tool.converter.Convert;
import org.omg.CORBA.ExceptionList;

import com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationStub;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.CWSBankcardCallbackHandler;
import com.ipcommerce.schemas.cws.transactions.CWSBankcardStub;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.AddressInfo;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ApplicationData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ApplicationLocation;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ArrayOfMerchantProfile;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.BankcardMerchantData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.BankcardService;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.BankcardTransactionDataDefaults;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.CustomerPresent;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.HardwareType;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.MerchantProfile;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.MerchantProfileMerchantData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.MerchantProfileTransactionData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.Operations;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.PINCapability;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ReadCapability;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfilesResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ServiceInformation;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.TenderType;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeISOCountryCodeA3;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeISOCurrencyCodeA3;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeISOLanguageCodeA3;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince;
import com.ipcommerce.schemas.cws.v2_0.transactions.ArrayOfCapture;
import com.ipcommerce.schemas.cws.v2_0.transactions.ArrayOfResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.Response;
import com.ipcommerce.schemas.cws.v2_0.transactions.Status;
import com.ipcommerce.schemas.cws.v2_0.transactions.TransactionData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AVSData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AdjustResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCaptureResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardCapture;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardCaptureResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardReturn;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTenderData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTransaction;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTransactionData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTransactionResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardUndo;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CVDataProvided;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CardData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CardSecurityData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.EntryMode;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.IndustryType;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnByIdResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinkedResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.TypeCardType;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.UndoResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault;
import com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault;
import com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import com.sun.xml.bind.v2.schemagen.xmlschema.List;

public class SampleApp {
	//BEGIN Configuration Values. The following values should be placed in a Configuration File. Please pay  
	//special attention towards security around the "Identity Token" similar to how you would protect user credentials
	public String _primaryServiceEndpoint = "https://cws-01.ipcommerce.com:443/2.0/SvcInfo/C53FB920CF600001";
	public String _secondaryServiceEndpoint = "https://cws-02.ipcommerce.com:443/2.0/SvcInfo/C53FB920CF600001";
	public String _primaryTxnEndpoint = "https://cws-01.ipcommerce.com:443/2.0/Txn/C53FB920CF600001";
	public String _secondaryTxnEndpoint = "https://cws-02.ipcommerce.com:443/2.0/Txn/C53FB920CF600001";
	public String _IdentityToken = "PHNhbWw6QXNzZXJ0aW9uIE1ham9yVmVyc2lvbj0iMSIgTWlub3JWZXJzaW9uPSIxIiBBc3NlcnRpb25JRD0iXzJiZGRmYjZmLWQzOGItNDQyYS1hMDdkLTFhODA4NTFlOWQ5OSIgSXNzdWVyPSJJcGNBdXRoZW50aWNhdGlvbiIgSXNzdWVJbnN0YW50PSIyMDEwLTA0LTI2VDE1OjI1OjI5LjQzM1oiIHhtbG5zOnNhbWw9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMDphc3NlcnRpb24iPjxzYW1sOkNvbmRpdGlvbnMgTm90QmVmb3JlPSIyMDEwLTA0LTI2VDE1OjI1OjI5LjQzM1oiIE5vdE9uT3JBZnRlcj0iMjAxMy0wNC0yNlQxNToyNToyOS40MzNaIj48L3NhbWw6Q29uZGl0aW9ucz48c2FtbDpBZHZpY2U+PC9zYW1sOkFkdmljZT48c2FtbDpBdHRyaWJ1dGVTdGF0ZW1lbnQ+PHNhbWw6U3ViamVjdD48c2FtbDpOYW1lSWRlbnRpZmllcj5DNTNGQjkyMENGNjAwMDAxPC9zYW1sOk5hbWVJZGVudGlmaWVyPjwvc2FtbDpTdWJqZWN0PjxzYW1sOkF0dHJpYnV0ZSBBdHRyaWJ1dGVOYW1lPSJTQUsiIEF0dHJpYnV0ZU5hbWVzcGFjZT0iaHR0cDovL3NjaGVtYXMuaXBjb21tZXJjZS5jb20vSWRlbnRpdHkiPjxzYW1sOkF0dHJpYnV0ZVZhbHVlPkM1M0ZCOTIwQ0Y2MDAwMDE8L3NhbWw6QXR0cmlidXRlVmFsdWU+PC9zYW1sOkF0dHJpYnV0ZT48c2FtbDpBdHRyaWJ1dGUgQXR0cmlidXRlTmFtZT0iU2VyaWFsIiBBdHRyaWJ1dGVOYW1lc3BhY2U9Imh0dHA6Ly9zY2hlbWFzLmlwY29tbWVyY2UuY29tL0lkZW50aXR5Ij48c2FtbDpBdHRyaWJ1dGVWYWx1ZT5iMDM5YzhiMi00NzQ3LTQ4ODYtOTE1NS03ZGYwOWNjNWRjNjI8L3NhbWw6QXR0cmlidXRlVmFsdWU+PC9zYW1sOkF0dHJpYnV0ZT48c2FtbDpBdHRyaWJ1dGUgQXR0cmlidXRlTmFtZT0ibmFtZSIgQXR0cmlidXRlTmFtZXNwYWNlPSJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcyI+PHNhbWw6QXR0cmlidXRlVmFsdWU+QzUzRkI5MjBDRjYwMDAwMTwvc2FtbDpBdHRyaWJ1dGVWYWx1ZT48L3NhbWw6QXR0cmlidXRlPjwvc2FtbDpBdHRyaWJ1dGVTdGF0ZW1lbnQ+PFNpZ25hdHVyZSB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PFNpZ25lZEluZm8+PENhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiPjwvQ2Fub25pY2FsaXphdGlvbk1ldGhvZD48U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIj48L1NpZ25hdHVyZU1ldGhvZD48UmVmZXJlbmNlIFVSST0iI18yYmRkZmI2Zi1kMzhiLTQ0MmEtYTA3ZC0xYTgwODUxZTlkOTkiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyI+PC9UcmFuc2Zvcm0+PC9UcmFuc2Zvcm1zPjxEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjc2hhMSI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPnhMWWtVeVBSam51dzFDZGJZaC9HL1VOYkwzWT08L0RpZ2VzdFZhbHVlPjwvUmVmZXJlbmNlPjwvU2lnbmVkSW5mbz48U2lnbmF0dXJlVmFsdWU+YUtnUXBwU1JKSmZTNWZjUkZadys0NkhvYnVkR0NYcUR5Ym8xZmlEaFByUDJKYjhkcTg2NHBxM3gyZ0RjOUxUa0RleTdoUGR0NEdYNTV4R09BTnVOUDZjWHN6UmRwbkNES0gyZmorcjF6YmJvdDBHdjJmK25tdEFQL05kZkNtdE56a1hyaGIvalN3TThSbUxkR1BKR2I5Vm9PYUpESDRoZ3ltdEFBWnFybWdrPTwvU2lnbmF0dXJlVmFsdWU+PEtleUluZm8+PG86U2VjdXJpdHlUb2tlblJlZmVyZW5jZSB4bWxuczpvPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3MvMjAwNC8wMS9vYXNpcy0yMDA0MDEtd3NzLXdzc2VjdXJpdHktc2VjZXh0LTEuMC54c2QiPjxvOktleUlkZW50aWZpZXIgVmFsdWVUeXBlPSJodHRwOi8vZG9jcy5vYXNpcy1vcGVuLm9yZy93c3Mvb2FzaXMtd3NzLXNvYXAtbWVzc2FnZS1zZWN1cml0eS0xLjEjVGh1bWJwcmludFNIQTEiPllmN3ZZeEZWSHo2VnFHQ3R0b1lUSVo4VTJCND08L286S2V5SWRlbnRpZmllcj48L286U2VjdXJpdHlUb2tlblJlZmVyZW5jZT48L0tleUluZm8+PC9TaWduYXR1cmU+PC9zYW1sOkFzc2VydGlvbj4=";
	//The PTLSSocketId changes from Sandbox to Production however is tied to your certified application. 
	public String _PTLSSocketId = "MIIEwjCCA6qgAwIBAgIBEjANBgkqhkiG9w0BAQUFADCBsTE0MDIGA1UEAxMrSVAgUGF5bWVudHMgRnJhbWV3b3JrIENlcnRpZmljYXRlIEF1dGhvcml0eTELMAkGA1UEBhMCVVMxETAPBgNVBAgTCENvbG9yYWRvMQ8wDQYDVQQHEwZEZW52ZXIxGjAYBgNVBAoTEUlQIENvbW1lcmNlLCBJbmMuMSwwKgYJKoZIhvcNAQkBFh1hZG1pbkBpcHBheW1lbnRzZnJhbWV3b3JrLmNvbTAeFw0wNjEyMTUxNzQyNDVaFw0xNjEyMTIxNzQyNDVaMIHAMQswCQYDVQQGEwJVUzERMA8GA1UECBMIQ29sb3JhZG8xDzANBgNVBAcTBkRlbnZlcjEeMBwGA1UEChMVSVAgUGF5bWVudHMgRnJhbWV3b3JrMT0wOwYDVQQDEzRFcWJwR0crZi8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL0E9MS4wLAYJKoZIhvcNAQkBFh9zdXBwb3J0QGlwcGF5bWVudHNmcmFtZXdvcmsuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQD7BTLqXah9t6g4W2pJUfFKxJj/R+c1Dt5MCMYGKeJCMvimAJOoFQx6Cg/OO12gSSipAy1eumAqClxxpR6QRqO3iv9HUoREq+xIvORxm5FMVLcOv/oV53JctN2fwU2xMLqnconD0+7LJYZ+JT4z3hY0mn+4SFQ3tB753nqc5ZRuqQIDAQABo4IBVjCCAVIwCQYDVR0TBAIwADAdBgNVHQ4EFgQUk7zYAajw24mLvtPv7KnMOzdsJuEwgeYGA1UdIwSB3jCB24AU3+ASnJQimuunAZqQDgNcnO2HuHShgbekgbQwgbExNDAyBgNVBAMTK0lQIFBheW1lbnRzIEZyYW1ld29yayBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkxCzAJBgNVBAYTAlVTMREwDwYDVQQIEwhDb2xvcmFkbzEPMA0GA1UEBxMGRGVudmVyMRowGAYDVQQKExFJUCBDb21tZXJjZSwgSW5jLjEsMCoGCSqGSIb3DQEJARYdYWRtaW5AaXBwYXltZW50c2ZyYW1ld29yay5jb22CCQD/yDY5hYVsVzA9BglghkgBhvhCAQQEMBYuaHR0cHM6Ly93d3cuaXBwYXltZW50c2ZyYW1ld29yay5jb20vY2EtY3JsLnBlbTANBgkqhkiG9w0BAQUFAAOCAQEAFk/WbEleeGurR+FE4p2TiSYHMau+e2Tgi+L/oNgIDyvAatgosk0TdSndvtf9YKjCZEaDdvWmWyEMfirb5mtlNnbZz6hNpYoha4Y4ThrEcCsVhfHLLhGZZ1YaBD+ZzCQA7vtb0v5aQb25jX262yPVshO+62DPxnMiJevSGFUTjnNisVniX23NVouUwR3n12GO8wvzXF8IYb5yogaUcVzsTIxEFQXEo1PhQF7JavEnDksVnLoRf897HwBqcdSs0o2Fpc/GN1dgANkfIBfm8E9xpy7k1O4MuaDRqq5XR/4EomD8BWQepfJY0fg8zkCfkuPeGjKkDCitVd3bhjfLSgTvDg==";
	//END Configuration Values 
	
	public CWSServiceInformationStub CWSSIC;
	public CWSBankcardStub CWSBC;
	public Operations SupportedTxnTypes = new Operations();
	public ServiceInformation SI;
	
 
//The following is used for Service Information Setup
	public boolean signOnWithToken(ConfigurationValues _CV){
		try {
			checkTokenExpire(_CV);
		} 
		catch (WebServiceException WSE){
			return false;
		}
        catch (ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
		return true;
	}

	public boolean SaveApplicationConfiguration(ConfigurationValues _CV) {

		try {
			checkTokenExpire(_CV);//Always verify that the session token has not expired.
						
			SaveApplicationData SAD = new SaveApplicationData();
			SaveApplicationDataResponse SADR;

			ApplicationData AD = new ApplicationData();
			AD.setApplicationAttended(true);
			AD.setApplicationLocation(ApplicationLocation.HOME_INTERNET);
			AD.setApplicationName("MyTestApp");
			AD.setDeveloperId("TPP123");// Only used for First Data
			AD.setHardwareType(HardwareType.PC);
			AD.setPINCapability(PINCapability.PIN_NOT_SUPPORTED);
			AD.setReadCapability(ReadCapability.HAS_MSR);
			AD.setSerialNumber("Ser123");
			AD.setPTLSSocketId(_PTLSSocketId.trim());
		 	AD.setSoftwareVersion("2.1.0");
			Calendar cal = Calendar.getInstance();
			cal.set(2010, 03, 25);
			AD.setSoftwareVersionDate(cal);

			SAD.setApplicationData(AD);
			SAD.setSessionToken(_CV._SessionToken);
			// Now Let's save The ApplicationData
			SADR = CWSSIC.saveApplicationData(SAD);
			_CV._ApplicationProfileId = SADR.getSaveApplicationDataResult();

		} 
        catch (ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage e) { 
            
        	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationErrorFault error = errors.next();
//                System.out.println(error.getRuleLocationKey() + ": " +  error.getRuleMessage());
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
		return true;
	}

	public boolean GetFirstServiceId(ConfigurationValues _CV) {

		try {
			checkTokenExpire(_CV);//Always verify that the session token has not expired.
			
			// ServiceInformation SI = new ServiceInformation();
			GetServiceInformation GSI = new GetServiceInformation();
			GetServiceInformationResponse GSIR = new GetServiceInformationResponse();

			GSI.setSessionToken(_CV._SessionToken);
			GSIR = CWSSIC.getServiceInformation(GSI);
			SI = GSIR.getGetServiceInformationResult();
			//NOTE: At this point you can either iterate through the services in SI or in the case of our example just use the first one.	
			_CV._ServiceId = SI.getBankcardServices().getBankcardService().get(0).getServiceId();
			//Set the supported transaction types. This will be used later on when processing transactions.
			SupportedTxnTypes = SI.getBankcardServices().getBankcardService().get(0).getOperations();
			
			
			if (_CV._ServiceId.length() < 1)
				return false;
		} 
        catch (ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
		return true;
	}

	public boolean SaveMerchantProfile(ConfigurationValues _CV) {

		try {
			checkTokenExpire(_CV);//Always verify that the session token has not expired.
			
			SaveMerchantProfiles SMP = new SaveMerchantProfiles();
			//SaveMerchantProfilesResponse SMPR = new SaveMerchantProfilesResponse();
			MerchantProfileMerchantData MPMD = new MerchantProfileMerchantData();
			MerchantProfile MP = new MerchantProfile();
			ArrayOfMerchantProfile AOMP = new ArrayOfMerchantProfile();

			//Set Merchant Data
			AddressInfo AI = new AddressInfo();
			AI.setStreet1("1234 HappyWay");
			AI.setCity("Pleasanton");
			AI.setStateProvince(TypeStateProvince.CA);
			AI.setPostalCode("94566");
			AI.setCountryCode(TypeISOCountryCodeA3.USA);
			MPMD.setAddress(AI);
			MPMD.setCustomerServicePhone("513 3334444");
			MPMD.setMerchantId("123456789012");
			MPMD.setName("MerchA");
			MPMD.setLanguage(TypeISOLanguageCodeA3.ENG);
			MPMD.setPhone("513 8884444");
			MPMD.setTaxId("");			
			MerchantProfileTransactionData MPTD = new MerchantProfileTransactionData();
			BankcardTransactionDataDefaults BTDD = new BankcardTransactionDataDefaults();
			BTDD.setCurrencyCode(TypeISOCurrencyCodeA3.USD);
			BTDD.setCustomerPresent(CustomerPresent.PRESENT);
			BTDD.setRequestACI(com.ipcommerce.schemas.cws.v2_0.serviceinformation.RequestACI.IS_CPS_MERIT_CAPABLE);
			MPTD.setBankcardTransactionDataDefaults(BTDD);
			BankcardMerchantData BCMD = new BankcardMerchantData();
			BCMD.setClientNumber("2323");
			BCMD.setSIC("0022");
			BCMD.setTerminalId("001");						
			MPMD.setBankcardMerchantData(BCMD);
			//MP.setProfileId("Merch_FDR_Prof");
			MP.setProfileId("Merch_A");
			MP.setMerchantData(MPMD);
			MP.setTransactionData(MPTD);			
			AOMP.getMerchantProfile().add(MP);

			SMP.setMerchantProfiles(AOMP);
			SMP.setServiceId(_CV._ServiceId);
			//SMP.setServiceId("A656D00001");
			SMP.setSessionToken(_CV._SessionToken);
			SMP.setTenderType(TenderType.CREDIT);
			//SMPR = CWSSIC.saveMerchantProfiles(SMP);
			CWSSIC.saveMerchantProfiles(SMP);
			
			//Now Verify that the new profile saved above was actually saved successfully. 
			IsMerchantProfileInitialized IMPI = new IsMerchantProfileInitialized();
			IsMerchantProfileInitializedResponse IMPIR = new IsMerchantProfileInitializedResponse();			
			IMPI.setMerchantProfileId(MP.getProfileId());
			IMPI.setServiceId(_CV._ServiceId);
			IMPI.setSessionToken(_CV._SessionToken);
			IMPI.setTenderType(TenderType.CREDIT);
			IMPIR = CWSSIC.isMerchantProfileInitialized(IMPI);
			if(IMPIR.isIsMerchantProfileInitializedResult()){
				_CV._MerchantProfileId = MP.getProfileId();
				return true;
			}
			else{
				_CV._MerchantProfileId = "";
				return false;
			}
		} 
        catch (ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage e) { 
            
        	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}

//The following is used for Transaction Processing
	public boolean AuthorizeAndCapture(ConfigurationValues _CV){
		//The AuthorizeAndCapture() operation is used to authorize and capture a transaction in a single invocation.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Authorizing_Transactions/AuthorizeAndCapture.aspx
      
        try
        {
    		checkTokenExpire(_CV);//Always verify that the session token has not expired.
            if (!SupportedTxnTypes.isAuthAndCapture()) return true;//Check to see if this transaction type is supported
    		
            BankcardTransaction BCtransaction = setBankCardTxnData();
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture AAC = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture();
        	AAC.setApplicationProfileId(_CV._ApplicationProfileId);
        	AAC.setMerchantProfileId(_CV._MerchantProfileId);
        	AAC.setServiceId(_CV._ServiceId);
        	AAC.setSessionToken(_CV._SessionToken);
        	AAC.setTransaction(BCtransaction);
        	CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	BCTR = (BankcardTransactionResponse) CWSBC.authorizeAndCapture(AAC).getAuthorizeAndCaptureResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	
        	return true;
        }
        catch (ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}
	
	public boolean Authorize(ConfigurationValues _CV){
		//The Authorize() operation is used to precapture or reserve funds
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Authorizing_Transactions/Authorize.aspx
        
        try
        {
    		checkTokenExpire(_CV);//Always verify that the session token has not expired.
            if (!SupportedTxnTypes.isAuthorize()) return true;//Check to see if this transaction type is supported
    		
            BankcardTransaction BCtransaction = setBankCardTxnData();
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            
            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize A = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize();
        	A.setApplicationProfileId(_CV._ApplicationProfileId);
        	A.setMerchantProfileId(_CV._MerchantProfileId);
        	A.setServiceId(_CV._ServiceId);
        	A.setSessionToken(_CV._SessionToken);
        	A.setTransaction(BCtransaction);
        	CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	BCTR = (BankcardTransactionResponse) CWSBC.authorize(A).getAuthorizeResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	    
        	System.out.println("Approval Code"+BCTR.getApprovalCode());
        	System.out.println("Approval Status msg"+BCTR.getStatusMessage());
        	System.out.println("TransAction ID:"+BCTR.getTransactionId());
        	
        	return true;
        }
        catch (ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}
	
	public boolean Adjust(ConfigurationValues _CV){
		//The Adjust() operation is used to make adjustments to a previously authorized amount (incremental or reversal) prior to capture and settlement.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Adjusting_and_Voiding_Transactions/Adjust.aspx
        try
        {
    		checkTokenExpire(_CV);//Always verify that the session token has not expired.
            if (!SupportedTxnTypes.isAdjust()) return true;//Check to see if this transaction type is supported

            CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	//First Let's perform an Authorize and Capture. Well use this later on for the Adjust
        	BankcardTransaction BCtransaction = setBankCardTxnData();
        	BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture AAC = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture();
        	AAC.setApplicationProfileId(_CV._ApplicationProfileId);
        	AAC.setMerchantProfileId(_CV._MerchantProfileId);
        	AAC.setServiceId(_CV._ServiceId);
        	AAC.setSessionToken(_CV._SessionToken);
        	AAC.setTransaction(BCtransaction);
        	BCTR = (BankcardTransactionResponse) CWSBC.authorizeAndCapture(AAC).getAuthorizeAndCaptureResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	
        	//Now Let's adjust the previously approved AuthorizeAndCapture
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust aTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust();
        	com.ipcommerce.schemas.cws.v2_0.transactions.Adjust aDifference = new com.ipcommerce.schemas.cws.v2_0.transactions.Adjust();
        	
        	BigDecimal Amt = new BigDecimal("40.00").setScale(2, RoundingMode.UNNECESSARY); //Previous Value was 42.00
        	aDifference.setAmount(Amt);
        	aDifference.setTransactionId(BCTR.getTransactionId()); //Simply use the TransactionId from the AuthorizeAndCapture
        	aTransaction.setApplicationProfileId(_CV._ApplicationProfileId);
        	aTransaction.setMerchantProfileId(_CV._MerchantProfileId);
        	aTransaction.setServiceId(_CV._ServiceId);
        	aTransaction.setSessionToken(_CV._SessionToken);
        	aTransaction.setDifferenceData(aDifference);
        	BCTR = (BankcardTransactionResponse) CWSBC.adjust(aTransaction).getAdjustResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);        	

        	return true;
        }
        catch (ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}

	public boolean Undo(ConfigurationValues _CV){
		//The Undo() operation is used to void (Credit Card) or reverse (PIN Debit) a transaction that has been previously authorized, but not yet captured (flagged) for settlement.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Adjusting_and_Voiding_Transactions/Undo.aspx
        try
        {
    		checkTokenExpire(_CV);//Always verify that the session token has not expired.
            if (!SupportedTxnTypes.isUndo()) return true;//Check to see if this transaction type is supported

            CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	
            BankcardTransaction BCtransaction = setBankCardTxnData();
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize A = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize();
        	A.setApplicationProfileId(_CV._ApplicationProfileId);
        	A.setMerchantProfileId(_CV._MerchantProfileId);
        	A.setServiceId(_CV._ServiceId);
        	A.setSessionToken(_CV._SessionToken);
        	A.setTransaction(BCtransaction);
        	BCTR = (BankcardTransactionResponse) CWSBC.authorize(A).getAuthorizeResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	      
        	//Now Let's Undo the previously approved Authorize
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo uTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo();
        	BankcardUndo uDifference = new BankcardUndo();
        	
        	uDifference.setTransactionId(BCTR.getTransactionId()); //Simply use the TransactionId from the Authorize
        	uTransaction.setApplicationProfileId(_CV._ApplicationProfileId);
        	uTransaction.setMerchantProfileId(_CV._MerchantProfileId);
        	uTransaction.setServiceId(_CV._ServiceId);
        	uTransaction.setSessionToken(_CV._SessionToken);
        	uTransaction.setDifferenceData(uDifference);
        	BCTR = (BankcardTransactionResponse) CWSBC.undo(uTransaction).getUndoResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	
        	return true;
        }
        catch (ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}
	
	public boolean Capture(ConfigurationValues _CV){
		//The Capture() operation is used to capture a single transaction for settlement after it has been successfully authorized by the Authorize() operation.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Capturing_Transactions_for_Settlement/Capture.aspx
        try
        {
    		checkTokenExpire(_CV);//Always verify that the session token has not expired.
            if (!SupportedTxnTypes.isCapture()) return true;//Check to see if this transaction type is supported

        	CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	
            BankcardTransaction BCtransaction = setBankCardTxnData();
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            BankcardCaptureResponse BCCR = new BankcardCaptureResponse();
            
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize A = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize();
        	A.setApplicationProfileId(_CV._ApplicationProfileId);
        	A.setMerchantProfileId(_CV._MerchantProfileId);
        	A.setServiceId(_CV._ServiceId);
        	A.setSessionToken(_CV._SessionToken);
        	A.setTransaction(BCtransaction);
        	BCTR = (BankcardTransactionResponse) CWSBC.authorize(A).getAuthorizeResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	      
        	//Now Let's Capture the previously approved Authorize
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture cTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture();
        	BankcardCapture cDifference = new BankcardCapture();
        	
        	cDifference.setTransactionId(BCTR.getTransactionId()); //Simply use the TransactionId from the Authorize
        	//Conditional values. The following scenario would be used in a Restaurant industry type where tip is added.
        	cDifference.setAmount(new BigDecimal("48.00").setScale(2, RoundingMode.UNNECESSARY)); //Only necessary if this value changed such as tip
        	cDifference.setTipAmount(new BigDecimal("6.00").setScale(2, RoundingMode.UNNECESSARY));
        	
        	cTransaction.setApplicationProfileId(_CV._ApplicationProfileId);
        	cTransaction.setMerchantProfileId(_CV._MerchantProfileId);
        	cTransaction.setServiceId(_CV._ServiceId);
        	cTransaction.setSessionToken(_CV._SessionToken);
        	cTransaction.setDifferenceData(cDifference);
        	BCCR = (BankcardCaptureResponse) CWSBC.capture(cTransaction).getCaptureResult();
        	ProcessBankcardCaptureResponse(BCCR, _CV);

        	return true;
        }
        catch (ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}
	
	public boolean CaptureAll(ConfigurationValues _CV){
		//The Capture() operation is used to capture a single transaction for settlement after it has been successfully authorized by the Authorize() operation.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Capturing_Transactions_for_Settlement/Capture.aspx
        try
        {
    		checkTokenExpire(_CV);//Always verify that the session token has not expired.
            if (!SupportedTxnTypes.isCapture()) return true;//Check to see if this transaction type is supported

        	CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	
            BankcardTransaction BCtransaction = setBankCardTxnData();
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            ArrayOfResponse AOR = new ArrayOfResponse();
            
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize A = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize();
        	A.setApplicationProfileId(_CV._ApplicationProfileId);
        	A.setMerchantProfileId(_CV._MerchantProfileId);
        	A.setServiceId(_CV._ServiceId);
        	A.setSessionToken(_CV._SessionToken);
        	A.setTransaction(BCtransaction);
        	BCTR = (BankcardTransactionResponse) CWSBC.authorize(A).getAuthorizeResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	      
        	//Now Let's Capture All the previously approved Authorize
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll cATransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CaptureAll();
        	ArrayOfCapture cDArrayOfCapture = new ArrayOfCapture();
        	
        	//BankcardCapture cDifference = new BankcardCapture();
        	
        	
        	//cDifference.setTransactionId(BCTR.getTransactionId()); //Simply use the TransactionId from the Authorize
        	//Conditional values. The following scenario would be used in a Restaurant industry type where tip is added.
        	//cDifference.setAmount(new BigDecimal("48.00").setScale(2, RoundingMode.UNNECESSARY)); //Only necessary if this value changed such as tip
        	//cDifference.setTipAmount(new BigDecimal("6.00").setScale(2, RoundingMode.UNNECESSARY));
        	
        	cATransaction.setApplicationProfileId(_CV._ApplicationProfileId);
        	cATransaction.setMerchantProfileId(_CV._MerchantProfileId);
        	cATransaction.setServiceId(_CV._ServiceId);
        	//cATransaction.setSessionToken(_CV._SessionToken);
        	//cATransaction.setDifferenceData(cDifference);
        	cATransaction.setDifferenceData(cDArrayOfCapture);
        	AOR = (ArrayOfResponse) CWSBC.captureAll(cATransaction).getCaptureAllResult();
        	 
        	List l = (List)AOR.getResponse();
        	return true;
        }
        catch (ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}

	public boolean ReturnById(ConfigurationValues _CV){
		//The ReturnById() operation is used to return funds to a payment account based on a transaction that has been previously settled.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Refunding_Transactions/ReturnById.aspx
        try
        {
    		checkTokenExpire(_CV);//Always verify that the session token has not expired.
            if (!SupportedTxnTypes.isReturnById()) return true;//Check to see if this transaction type is supported

        	CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	
            BankcardTransaction BCtransaction = setBankCardTxnData();
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture AAC = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture();
        	AAC.setApplicationProfileId(_CV._ApplicationProfileId);
        	AAC.setMerchantProfileId(_CV._MerchantProfileId);
        	AAC.setServiceId(_CV._ServiceId);
        	AAC.setSessionToken(_CV._SessionToken);
        	AAC.setTransaction(BCtransaction);
        	CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	BCTR = (BankcardTransactionResponse) CWSBC.authorizeAndCapture(AAC).getAuthorizeAndCaptureResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	      
        	//Now Let's ReturnById the previously approved AuthorizeAndCapture
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById rTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById();
        	BankcardReturn rDifference = new BankcardReturn();
        	
        	rDifference.setTransactionId(BCTR.getTransactionId()); //Simply use the TransactionId from the AuthorizeAndCapture
        	//Conditional values. The following scenario would be used in a partial return.
        	//rDifference.setAmount(new BigDecimal("25.00").setScale(2, RoundingMode.UNNECESSARY)); //Only necessary if this value changed such as tip
        	        	
        	rTransaction.setApplicationProfileId(_CV._ApplicationProfileId);
        	rTransaction.setMerchantProfileId(_CV._MerchantProfileId);
        	rTransaction.setServiceId(_CV._ServiceId);
        	rTransaction.setSessionToken(_CV._SessionToken);
        	rTransaction.setDifferenceData(rDifference);
        	BCTR = (BankcardTransactionResponse) CWSBC.returnById(rTransaction).getReturnByIdResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);

        	return true;
        }
        catch (ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
          		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}
	
	public boolean ReturnUnlinked(ConfigurationValues _CV){

		//The ReturnUnlinked() operation is used to return funds to a payment account without associating the return with a specific transaction (�unlinked�) that has been previously settled.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Refunding_Transactions/ReturnUnlinked.aspx
        try
        {
    		checkTokenExpire(_CV);//Always verify that the session token has not expired.
            if (!SupportedTxnTypes.isReturnUnlinked()) return true;//Check to see if this transaction type is supported

        	CWSBC = new CWSBankcardStub(_primaryTxnEndpoint);
        	      
        	//Let's Return an unlinked transaction
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked rTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnUnlinked();
        	BankcardTransaction BCR = setBankCardTxnData();
        	BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
        	
        	rTransaction.setApplicationProfileId(_CV._ApplicationProfileId);
        	rTransaction.setMerchantProfileId(_CV._MerchantProfileId);
        	rTransaction.setServiceId(_CV._ServiceId);
        	rTransaction.setSessionToken(_CV._SessionToken);
        	rTransaction.setTransaction(BCR);
        	BCTR = (BankcardTransactionResponse) CWSBC.returnUnlinked(rTransaction).getReturnUnlinkedResult();
        	ProcessBankcardTransactionResponse(BCTR, _CV);
        	
        	return true;
        }
        catch (ICWSBankcard_ReturnUnlinked_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	return false;
        } 
        catch (ICWSBankcard_ReturnUnlinked_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            return false;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
          		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.        		
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            return false;
        }
	}
	
//The following are helper methods
	public void checkTokenExpire(ConfigurationValues _CV) throws Exception{
		try {
				if(_CV._SessionTokenDateTime == null){
					//First Time so get a new Session Token
					CWSSIC = new CWSServiceInformationStub(_primaryServiceEndpoint);
					SignOnWithToken signOnWithToken = new SignOnWithToken();
					signOnWithToken.setIdentityToken(_IdentityToken);
					SignOnWithTokenResponse response = CWSSIC.signOnWithToken(signOnWithToken);
		
					_CV._SessionToken = response.getSignOnWithTokenResult().toString();
					
					GregorianCalendar gc1 = new GregorianCalendar();
					Date d1 = gc1.getTime();
					_CV._SessionTokenDateTime = d1.getTime();;				
				}
				else
				{
					//Let's check to see if the token has expired or is about to expire. 
					GregorianCalendar gc = new GregorianCalendar();
					Date d = gc.getTime();
					long l = d.getTime();
					long difference = l - _CV._SessionTokenDateTime;
					difference = difference/60000;
					if(difference>25){
						//In this case the session token has either expired or will expire in less than 5 min. So get a new one.
						CWSSIC = new CWSServiceInformationStub(_primaryServiceEndpoint);
						SignOnWithToken signOnWithToken = new SignOnWithToken();
						signOnWithToken.setIdentityToken(_IdentityToken);
						SignOnWithTokenResponse response = CWSSIC.signOnWithToken(signOnWithToken);
			
						_CV._SessionToken = response.getSignOnWithTokenResult().toString();
						
						GregorianCalendar gc1 = new GregorianCalendar();
						Date d1 = gc1.getTime();
						_CV._SessionTokenDateTime = d1.getTime();;
					}
				}
				

		} catch (Exception ex) {
			throw ex;
		}
	}

	private BankcardTransaction setBankCardTxnData()
    {
        BankcardTransaction BCtransaction = new BankcardTransaction();

        BankcardTransactionData BCTD = new BankcardTransactionData();
        BigDecimal Amt = new BigDecimal("42.00").setScale(2, RoundingMode.UNNECESSARY);
        BCTD.setAmount(Amt);
        BCTD.setCurrencyCode(com.ipcommerce.schemas.cws.v2_0.transactions.TypeISOCurrencyCodeA3.USD);
        //BCTD.setEntryMode(EntryMode.KEYED); 
        BCTD.setEntryMode(EntryMode.TRACK_DATA_FROM_MSR);
        BCTD.setIndustryType(IndustryType.RETAIL);
        BCTD.setCustomerPresent(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CustomerPresent.PRESENT);
        BCTD.setSignatureCaptured(false);

        //Used for Retail
        BCTD.setEmployeeId("123h");

        //Used for Ecommerce
        BCTD.setOrderNumber("123");

        BCtransaction.setTransactionData(BCTD);

        BankcardTenderData BCTen = new BankcardTenderData();
        CardData CD = new CardData();
        CD.setCardType(TypeCardType.MASTER_CARD);
       // CD.setTrack1Data("B5454545454545454^IPCOMMERCE/TESTCARD^1312101013490000000001000880000");
        CD.setTrack2Data("5454545454545454=13121010134988000010");
        CD.setExpire("1210"); // exactly 4 digits � MMYY where as a swipe will be YYMM
        CD.setPAN("5454545454545454");
        BCTen.setCardData(CD);
        
        CardSecurityData CSD = new CardSecurityData();
        //AVSData
        AVSData AVS = new AVSData();
        AVS.setPostalCode("45040");
        CSD.setAVSData(AVS);
        //CVData
        CSD.setCVDataProvided(CVDataProvided.PROVIDED);
        CSD.setCVData("111");
        
        BCTen.setCardSecurityData(CSD);
        BCtransaction.setTenderData(BCTen);
        
        return BCtransaction;
    }

	private void ProcessBankcardTransactionResponse(BankcardTransactionResponse _BCResponse, ConfigurationValues _CV){
		/*At this point, you'll need to store the response information into your database. Please refer to the functional requirement
		 * as to what values are required, conditionally required and optional.	The values are broken out logically below and assigned
		 * to a string as reference only. 	
		*/
		if (_BCResponse.getStatus() == Status.SUCCESSFUL){//The transaction was approved
			String strSuccessfulResponseValues = "";
			strSuccessfulResponseValues = strSuccessfulResponseValues + "Your transaction was APPROVED"
			 //Note Highly recommended to save
			+ "\r\nTransactionId : " + _BCResponse.getTransactionId()
            + "\r\nProfileId : " + _CV._MerchantProfileId //Must be stored with the TransactionId in order to identify which merchant sent which transaction. Required to support multi-merchant.
            //Note Highly recommended to save if Tokenization will be used
            + "\r\nPaymentAccountDataToken : " + _BCResponse.getPaymentAccountDataToken() //If tokenization purchased this field represents the actual token returned in the transaction for future use.
            //Note Optional but recommended to save
            + "\r\nStatus Code : " + _BCResponse.getStatusCode() //Status code generated by the Service Provider. This code should be displayed to the user as verification of the transaction.
            + "\r\nStatus Message : " + _BCResponse.getStatusMessage() //Explains the StatusCode which is generated by the Service Provider. This message should be displayed to the user as verification of the transaction.
            + "\r\nApprovalCode : " + _BCResponse.getApprovalCode() //A value returned when a transaction is approved. This value should be printed on the receipt, and also recorded for every off-line transaction, such as a voice authorization. This same data element and value must be provided during settlement. Required.
            + "\r\nAmount : " + _BCResponse.getAmount() //Specifies the authorization amount of the transaction. This is the actual amount authorized.
            ////Note Optional but recommended if AVS is supported
            //+ "\r\nAVSResult ActualResult : " + _BCResponse.getAVSResult().getActualResult() //Specifies the actual result of AVS from the Service Provider.
            //+ "\r\nAVSResult AddressResult : " + _BCResponse.getAVSResult().getAddressResult() //Specifies the result of AVS as it pertains to Address matching
            //+ "\r\nAVSResult PostalCodeResult : " + _BCResponse.getAVSResult().getPostalCodeResult() //Specifies the result of AVS as it pertains to Postal Code matching
            ////Note Optional but recommended if CV data is supported
            + "\r\nCVResult : " + _BCResponse.getCVResult() //Response code returned by the card issuer indicating the result of Card Verification (CVV2/CVC2/CID).
            //Note Optional
            + "\r\nBatchId : " + _BCResponse.getBatchId() //A unique ID used to identify a specific batch settlement                
            + "\r\nDowngradeCode : " + _BCResponse.getDowngradeCode() //Indicates downgrade reason.
            + "\r\nFeeAmount : " + _BCResponse.getFeeAmount() //Fee amount charged for the transaction. 
            + "\r\nResubmit : " + _BCResponse.getResubmit() //Specifies whether resubmission is supported for PIN Debit transactions.
            + "\r\nServiceTransactionDateTime : " + _BCResponse.getServiceTransactionDateTime()
            + "\r\nServiceTransactionId : " + _BCResponse.getServiceTransactionId()
            + "\r\nSettlementDate : " + _BCResponse.getSettlementDate() //Settlement date. Conditional, if present in the authorization response, this same data element and value must be provided during settlement
            ;		
		}
		if (_BCResponse.getStatus() == Status.FAILURE){//The transaction was declined
			String strDeclinedResponseValues = "";
			strDeclinedResponseValues = strDeclinedResponseValues + "Your transaction was DECLINED"
			//NOTE : Please reference the developers guide for a more complete explination of the return fields
            //Note Highly recommended to save
            + "\r\nTransactionId : " + _BCResponse.getTransactionId() //The unique id of the transaction. TransactionId is required for all subsequent transactions such as Return, Undo, etc.
            + "\r\nProfileId : " + _CV._MerchantProfileId //Must be stored with the TransactionId in order to identify which merchant sent which transaction. Required to support multi-merchant.
            //Note Optional but recommended to save
            + "\r\nStatus Code : " + _BCResponse.getStatusCode() //Status code generated by the Service Provider. This code should be displayed to the user as verification of the transaction.
            + "\r\nStatus Message : " + _BCResponse.getStatusMessage() //Explains the StatusCode which is generated by the Service Provider. This message should be displayed to the user as verification of the transaction.
            //Note Optional but recommended if CV data is supported
            + "\r\nCVResult : " + _BCResponse.getCVResult() //Response code returned by the card issuer indicating the result of Card Verification (CVV2/CVC2/CID).
            //Note Optional
            + "\r\nServiceTransactionId : " + _BCResponse.getServiceTransactionId()
            ;
		}
	}
	
	private void ProcessBankcardCaptureResponse(BankcardCaptureResponse _BCResponse, ConfigurationValues _CV){
		/*At this point, you'll need to store the response information into your database. Please refer to the functional requirement
		 * as to what values are required, conditionally required and optional.	The values are broken out logically below and assigned
		 * to a string as reference only. 	
		*/
		if (_BCResponse.getStatus() == Status.SUCCESSFUL){//The transaction was approved
			String strSuccessfulResponseValues = "";
			strSuccessfulResponseValues = strSuccessfulResponseValues + "Your transaction was APPROVED"
            //Note Highly recommended to save
            + "\r\nTransactionId : " + _BCResponse.getTransactionId() //The unique id of the transaction. TransactionId is required for all subsequent transactions such as Return, Undo, etc.
            + "\r\nProfileId : " + _CV._MerchantProfileId //Must be stored with the TransactionId in order to identify which merchant sent which transaction. Required to support multi-merchant.
            //Note Optional but recommended to save
            + "\r\nStatus Code : " + _BCResponse.getStatusCode() //Status code generated by the Service Provider. This code should be displayed to the user as verification of the transaction.
            + " : Status Message : " + _BCResponse.getStatusMessage() //Explains the StatusCode which is generated by the Service Provider. This message should be displayed to the user as verification of the transaction.
            //Note Optional
            + "\r\nBatchId : " + _BCResponse.getBatchId() //A unique ID used to identify a specific batch settlement                
            + "\r\nServiceTransactionDateTime : " + _BCResponse.getServiceTransactionDateTime()
            + "\r\nServiceTransactionId : " + _BCResponse.getServiceTransactionId()
            ;		
		}
		if (_BCResponse.getStatus() == Status.FAILURE){//The transaction was declined
			String strDeclinedResponseValues = "";
			strDeclinedResponseValues = strDeclinedResponseValues + "Your transaction was DECLINED"
			//NOTE : Please reference the developers guide for a more complete explination of the return fields
			//Note Highly recommended to save
            + "\r\nTransactionId : " + _BCResponse.getTransactionId() //The unique id of the transaction. TransactionId is required for all subsequent transactions such as Return, Undo, etc.
            + "\r\nProfileId : " + _CV._MerchantProfileId //Must be stored with the TransactionId in order to identify which merchant sent which transaction. Required to support multi-merchant.
            //Note Optional but recommended to save
            + "\r\nStatus Code : " + _BCResponse.getStatusCode() //Status code generated by the Service Provider. This code should be displayed to the user as verification of the transaction.
            + " : Status Message : " + _BCResponse.getStatusMessage() //Explains the StatusCode which is generated by the Service Provider. This message should be displayed to the user as verification of the transaction.
            //Note Optional
            + "\r\nServiceTransactionId : " + _BCResponse.getServiceTransactionId()
            ;
		}
		
	}
}
