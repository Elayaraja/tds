package com.tds.util;

import java.util.ArrayList;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import nsidc.spheres.Point;

public class SphericalPolygon {

    private ArrayList<Double> latPolys;
    private ArrayList<Double> lonPolys;
    private double radius;
    private Point externalPoint;
    
    
    
	public ArrayList<Double> getLatPolys() {
		return latPolys;
	}
	public void setLatPolys(ArrayList<Double> latPolys) {
		this.latPolys = latPolys;
	}
	public ArrayList<Double> getLonPolys() {
		return lonPolys;
	}
	public void setLonPolys(ArrayList<Double> lonPolys) {
		this.lonPolys = lonPolys;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public Point getExternalPoint() {
		return externalPoint;
	}
	public void setExternalPoint(Point externalPoint) {
		this.externalPoint = externalPoint;
	}

	public SphericalPolygon(double[] lats,double[] longis){
		ArrayList<Double> latitudes = new ArrayList<Double>();
		ArrayList<Double> longitudes = new ArrayList<Double>();
		for(int i=0;i<lats.length;i++){
			latitudes.add(lats[i]);
			longitudes.add(longis[i]);
		}
		setLatPolys(latitudes);
		setLonPolys(longitudes);
	}
	public boolean contains(Point point) {
		int numPoints = latPolys.size();
		int j = numPoints-1;
		boolean inPoly=false;
		for(int k=0; k < numPoints; k++) { 
			double lati1 = latPolys.get(k);
			double longi1 = lonPolys.get(k);
			double lati2 = latPolys.get(j);
			double longi2 = lonPolys.get(j);
			if (longi1 < point.lon && longi2 >= point.lon || longi2 < point.lon && longi1 >= point.lon)  {
				if (lati1 + (point.lon - longi1) / (longi2 - longi1) * (lati2 - lati1) < point.lat) {
					inPoly = !inPoly;
				}
			}
			j = k;
		}
		return inPoly;
	}
}
