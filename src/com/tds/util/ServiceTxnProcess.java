package com.tds.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.ws.WebServiceException;

import com.ipcommerce.schemas.cws.serviceinformation.CWSServiceInformationStub;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.serviceinformation.ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.CWSBankcardStub;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.transactions.ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.AddressInfo;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ApplicationData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ApplicationLocation;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ArrayOfMerchantProfile;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.BankcardMerchantData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.BankcardTransactionDataDefaults;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.CustomerPresent;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformation;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.GetServiceInformationResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.HardwareType;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitialized;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.IsMerchantProfileInitializedResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.MerchantProfile;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.MerchantProfileMerchantData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.MerchantProfileTransactionData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.Operations;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.PINCapability;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ReadCapability;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationData;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveApplicationDataResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SaveMerchantProfiles;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.ServiceInformation;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithToken;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.SignOnWithTokenResponse;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.TenderType;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeISOCurrencyCodeA3;
import com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeISOLanguageCodeA3;
import com.ipcommerce.schemas.cws.v2_0.transactions.TypeStateProvince;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AVSData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardCapture;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardCaptureResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardReturn;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTenderData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTransaction;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTransactionData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardTransactionResponse;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.BankcardUndo;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CVDataProvided;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CardData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CardSecurityData;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.EntryMode;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.IndustryType;
import com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.TypeCardType;
import com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault;
import com.tds.dao.ProcessingDAO;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.process.OneToOneSMS;
import com.tds.tdsBO.ApplicationPoolBO;

public class ServiceTxnProcess {
 	
	public ConfigurationValues signOnWithToken(ApplicationPoolBO poolBO){
		ConfigurationValues _CV = new ConfigurationValues();
		try {
			
			_CV = checkTokenExpire(poolBO,_CV);
			_CV._Status = true;
		} 
		catch (WebServiceException WSE){
			_CV._Status = false;
			 
		}
        catch (ICWSServiceInformation_SignOnWithToken_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		_CV = checkTokenExpire(poolBO,_CV);
        		_CV._Status = true;
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        		_CV._Status = false;
        		
  //      		System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	
        } 
        catch (Exception e) { 
         	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
    //        System.out.println("Error From IpCom:" + FV._strErrorMessage);
            _CV._Status = false;
        }
		return _CV;
	}
	
	public ConfigurationValues gettingServiceIdAndSavingMerchantProfile(ApplicationPoolBO poolBO,com.tds.cmp.bean.MerchantProfile mProfile,String serviceID)
	{ 
		ConfigurationValues _CV = new ConfigurationValues();
		
		//_CV = SaveApplicationConfiguration(poolBO, _CV);
		//_CV = GetFirstServiceId(poolBO, _CV);
		_CV = SaveMerchantProfile(poolBO, mProfile, _CV,serviceID);
		 
		//System.out.println("Application id:"+_CV._ApplicationProfileId);
		//System.out.println("Service id:"+_CV._ServiceId);
	//	System.out.println("Merchant Profile :" + _CV._MerchantProfileId);
		
		return _CV;
 	}

	public ConfigurationValues SaveApplicationConfiguration(ApplicationPoolBO poolBO, ConfigurationValues _CV) {
		
		CWSServiceInformationStub CWSSIC = null; 
 
		try {
			//checkTokenExpire(_CV);//Always verify that the session token has not expired.
					
			CWSSIC = new CWSServiceInformationStub(poolBO.get_primaryServiceEndpoint());
			
			SaveApplicationData SAD = new SaveApplicationData();
			SaveApplicationDataResponse SADR;

			ApplicationData AD = new ApplicationData();
			AD.setApplicationAttended(true);
			AD.setApplicationLocation(ApplicationLocation.HOME_INTERNET);
			AD.setApplicationName("GetACab");
			AD.setDeveloperId("TPP123");// Only used for First Data
			AD.setHardwareType(HardwareType.PC);
			AD.setPINCapability(PINCapability.PIN_NOT_SUPPORTED);
			AD.setReadCapability(ReadCapability.HAS_MSR);
			AD.setSerialNumber("Ser123");
			AD.setPTLSSocketId(poolBO.get_PTLSSocketId().trim());
		 	AD.setSoftwareVersion("2.1.0");
			Calendar cal = Calendar.getInstance();
			cal.set(2010, 03, 25);
			AD.setSoftwareVersionDate(cal);

			SAD.setApplicationData(AD);
			SAD.setSessionToken(poolBO.get_SessionToken());
			// Now Let's save The ApplicationData
			SADR = CWSSIC.saveApplicationData(SAD);
			_CV._ApplicationProfileId = SADR.getSaveApplicationDataResult();
		 	 
        	return _CV;

		} 
        catch (ICWSServiceInformation_SaveApplicationData_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		//checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
      //  		System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	
        	_CV._ApplicationProfileId="";
        	return _CV;
        } 
        catch (ICWSServiceInformation_SaveApplicationData_CWSValidationResultFaultFault_FaultMessage e) { 
            
        	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationErrorFault error = errors.next();
                //System.out.println(error.getRuleLocationKey() + ": " +  error.getRuleMessage());
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
        //		System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            _CV._ApplicationProfileId="";
        	return _CV;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
        		
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            //System.out.println("Error From IpCom:" + FV._strErrorMessage);
            _CV._ApplicationProfileId="";
        	return _CV;
        }
		
	}

	
	public ConfigurationValues GetFirstServiceId(ApplicationPoolBO poolBO,ConfigurationValues _CV) {
		
		CWSServiceInformationStub CWSSIC = null;
		
		Operations SupportedTxnTypes = new Operations();
		ServiceInformation SI;
		
		try {
			CWSSIC = new CWSServiceInformationStub(poolBO.get_primaryServiceEndpoint());
			//checkTokenExpire(_CV);//Always verify that the session token has not expired.
			
			// ServiceInformation SI = new ServiceInformation();
			GetServiceInformation GSI = new GetServiceInformation();
			GetServiceInformationResponse GSIR = new GetServiceInformationResponse();

			GSI.setSessionToken(poolBO.get_SessionToken());
			GSIR = CWSSIC.getServiceInformation(GSI);
			SI = GSIR.getGetServiceInformationResult();
			//NOTE: At this point you can either iterate through the services in SI or in the case of our example just use the first one.	
			_CV._ServiceId = SI.getBankcardServices().getBankcardService().get(0).getServiceId();
			//Set the supported transaction types. This will be used later on when processing transactions.
			SupportedTxnTypes = SI.getBankcardServices().getBankcardService().get(0).getOperations();
			
			
			//System.out.println("Getting Service id"+ _CV._ServiceId);
			
			if (_CV._ServiceId.length() < 1)
				return _CV;
		} 
        catch (ICWSServiceInformation_GetServiceInformation_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		//checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	return _CV;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            //System.out.println("Error From IpCom:" + FV._strErrorMessage);
            return _CV;
        }
		return _CV;
	}

	public ConfigurationValues SaveMerchantProfile(ApplicationPoolBO poolBO,com.tds.cmp.bean.MerchantProfile mProfile,ConfigurationValues _CV,String ServiceId) {
	
		CWSServiceInformationStub CWSSIC = null;
		
		try {
			//checkTokenExpire(_CV);//Always verify that the session token has not expired.
			CWSSIC = new CWSServiceInformationStub(poolBO.get_primaryServiceEndpoint());
			SaveMerchantProfiles SMP = new SaveMerchantProfiles();
			//SaveMerchantProfilesResponse SMPR = new SaveMerchantProfilesResponse();
			MerchantProfileMerchantData MPMD = new MerchantProfileMerchantData();
			MerchantProfile MP = new MerchantProfile();
			ArrayOfMerchantProfile AOMP = new ArrayOfMerchantProfile();

			//Set Merchant Data
			AddressInfo AI = new AddressInfo();
			AI.setStreet1(mProfile.getStreet());
			AI.setCity(mProfile.getCity());
			AI.setStateProvince(getstateService(mProfile.getState()));			 
			AI.setPostalCode(mProfile.getZipcode());
			AI.setCountryCode(com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeISOCountryCodeA3.USA);
			MPMD.setAddress(AI);
			MPMD.setCustomerServicePhone(mProfile.getPhone_no());
			MPMD.setMerchantId(mProfile.getMerch_id());
			MPMD.setName(mProfile.getMerch_name());
			MPMD.setLanguage(TypeISOLanguageCodeA3.ENG);
			MPMD.setPhone(mProfile.getPhone_no());
			MPMD.setTaxId("");			
			MerchantProfileTransactionData MPTD = new MerchantProfileTransactionData();
			BankcardTransactionDataDefaults BTDD = new BankcardTransactionDataDefaults();
			BTDD.setCurrencyCode(TypeISOCurrencyCodeA3.USD);
			BTDD.setCustomerPresent(CustomerPresent.PRESENT);
			BTDD.setRequestACI(com.ipcommerce.schemas.cws.v2_0.serviceinformation.RequestACI.IS_CPS_MERIT_CAPABLE);
			MPTD.setBankcardTransactionDataDefaults(BTDD);
			BankcardMerchantData BCMD = new BankcardMerchantData();
			BCMD.setClientNumber(mProfile.getClient_no());
			BCMD.setSIC(mProfile.getSic());
			BCMD.setTerminalId(mProfile.getTerminal_id());						
			MPMD.setBankcardMerchantData(BCMD);
			MP.setProfileId(mProfile.getProfile_id());
			MP.setMerchantData(MPMD);
			MP.setTransactionData(MPTD);			
			AOMP.getMerchantProfile().add(MP);

			SMP.setMerchantProfiles(AOMP);
			//SMP.setServiceId(_CV._ServiceId);
			SMP.setServiceId(mProfile.getService_id());
			SMP.setSessionToken(poolBO.get_SessionToken());
			SMP.setTenderType(TenderType.CREDIT);
			//SMPR = CWSSIC.saveMerchantProfiles(SMP);
			CWSSIC.saveMerchantProfiles(SMP);
			
			//Now Verify that the new profile saved above was actually saved successfully. 
			IsMerchantProfileInitialized IMPI = new IsMerchantProfileInitialized();
			IsMerchantProfileInitializedResponse IMPIR = new IsMerchantProfileInitializedResponse();			
			IMPI.setMerchantProfileId(MP.getProfileId());
			IMPI.setServiceId(ServiceId);
			IMPI.setSessionToken(poolBO.get_SessionToken());
			IMPI.setTenderType(TenderType.CREDIT);
			IMPIR = CWSSIC.isMerchantProfileInitialized(IMPI);
			if(IMPIR.isIsMerchantProfileInitializedResult()){
				_CV._MerchantProfileId = MP.getProfileId();
				return _CV;
			}
			else{
				_CV._MerchantProfileId = "";
				return _CV;
			}
		} 
        catch (ICWSServiceInformation_SaveMerchantProfiles_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		//checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	
        	return _CV;
        } 
        catch (ICWSServiceInformation_SaveMerchantProfiles_CWSValidationResultFaultFault_FaultMessage e) { 
            
        	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            return _CV;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.serviceinformation.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
        		 
            }
            catch (Exception e2) {// Not a IP Commerce fault.
        		FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            	 
            }
            //System.out.println("Error From IpCom:" + FV._strErrorMessage);
            return _CV;
        }
	}
	
	
	
	//Transaction process Starts Here............
		
	public PaymentProcessBean AuthorizeAndCapture(ApplicationPoolBO poolBO,PaymentProcessBean processBean){
		//The AuthorizeAndCapture() operation is used to authorize and capture a transaction in a single invocation.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Authorizing_Transactions/AuthorizeAndCapture.aspx
		CWSBankcardStub CWSBC;
		boolean flg = true;
        try
        {
    		//checkTokenExpire(poolBO,_CV);//Always verify that the session token has not expired.
            //if (!SupportedTxnTypes.isAuthAndCapture()) return true;//Check to see if this transaction type is supported
    		
            BankcardTransaction BCtransaction = setBankCardTxnData(processBean);
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture AAC = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.AuthorizeAndCapture();
        	AAC.setApplicationProfileId(poolBO.get_ApplicationProfileId());
        	AAC.setMerchantProfileId(processBean.getMerchid());
        	AAC.setServiceId(processBean.getServiceid());
        	AAC.setSessionToken(poolBO.get_SessionToken());
        	AAC.setTransaction(BCtransaction);
        	CWSBC = new CWSBankcardStub(poolBO.get_primaryTxnEndpoint());
        	BCTR = (BankcardTransactionResponse) CWSBC.authorizeAndCapture(AAC).getAuthorizeAndCaptureResult();
        	
        	processBean.setB_approval_code(BCTR.getApprovalCode());
        	processBean.setB_trans_id(BCTR.getTransactionId());
        	for(int i=0;i<BCTR.getStatusCode().length();i++)
        	{
        		if(BCTR.getStatusCode().charAt(i) != '0')
        		{
        			flg = false;
        			break;
        		} 
        	}
        	processBean.setB_approval_status(flg?"1":"2");
        	//processBean.setB_approval_status(BCTR.getStatus() == Status.SUCCESSFUL?"1":"2");
        	processBean.setAuth_status(true);
        	processBean.setB_approval(BCTR.getStatusMessage());
        	processBean.setStatus_code(BCTR.getStatusCode());
        	//System.out.println("AUTHCAP;STCODE=" + BCTR.getStatusCode()+";APVCDE="+ BCTR.getApprovalCode()+";ApprovalStatusMsg="+BCTR.getStatusMessage()+";TXNID="+BCTR.getTransactionId());
        	//System.out.println(";APVCDE="+ BCTR.getApprovalCode());
        	//System.out.println(";ApprovalStatusMsg="+BCTR.getStatusMessage());
        	//System.out.println(";TXNID="+BCTR.getTransactionId());
        	//ProcessBankcardTransactionResponse(BCTR, poolBO);
        	
        	processBean.setAuth_status(true);
        	return processBean;
        }
        catch (ICWSBankcard_AuthorizeAndCapture_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	ProcessingDAO.insertManualCCErrorLog(processBean);
        	e.printStackTrace();
        	ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
        	try{
        		//checkTokenExpire(poolBO,_CV);
        	}
        	catch(Exception e2) {
        		e2.printStackTrace();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	processBean.setAuth_status(false);
        	return processBean;
        } 
        catch (ICWSBankcard_AuthorizeAndCapture_CWSValidationResultFaultFault_FaultMessage e) { 
        	e.printStackTrace();
        	ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
        	
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            processBean.setAuth_status(false);
        	return processBean;
        } 
        catch (Exception e) {
        	e.printStackTrace();
        	ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	e2.printStackTrace();
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            //System.out.println("Error From IpCom:" + FV._strErrorMessage);
            processBean.setAuth_status(false);
        	return processBean;
        }
	}
	
	public PaymentProcessBean Authorize(ApplicationPoolBO poolBO,PaymentProcessBean processBean){
		//The Authorize() operation is used to precapture or reserve funds
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Authorizing_Transactions/Authorize.aspx
		CWSBankcardStub CWSBC;
		boolean  flg = true;
        try
        {
    		//checkTokenExpire(_CV);//Always verify that the session token has not expired.
           // if (!SupportedTxnTypes.isAuthorize()) return true;//Check to see if this transaction type is supported
    		
            BankcardTransaction BCtransaction = setBankCardTxnData(processBean);
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            
            com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize A = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Authorize();
        	
            A.setApplicationProfileId(poolBO.get_ApplicationProfileId());
        	A.setMerchantProfileId(processBean.getMerchid());
        	A.setServiceId(processBean.getServiceid());
        	A.setSessionToken(poolBO.get_SessionToken());
        	             
        	A.setTransaction(BCtransaction);
        	CWSBC = new CWSBankcardStub(poolBO.get_primaryTxnEndpoint());
        	BCTR = (BankcardTransactionResponse) CWSBC.authorize(A).getAuthorizeResult();
        	//ProcessBankcardTransactionResponse(BCTR, _CV);
        	processBean.setB_approval_code(BCTR.getApprovalCode());
        	processBean.setB_trans_id(BCTR.getTransactionId());
        	
        	
        	for(int i=0;i<BCTR.getStatusCode().length();i++)
        	{
        		if(BCTR.getStatusCode().charAt(i) != '0')
        		{
        			flg = false;
        			break;
        		} 
        	}
        	
        	processBean.setB_approval_status(flg?"1":"2");
        	//processBean.setB_approval_status(BCTR.getStatus() == Status.SUCCESSFUL?"1":"2");
        	processBean.setStatus_code(BCTR.getStatusCode());
        	processBean.setB_approval(BCTR.getStatusMessage());
        	processBean.setAuth_status(true);
        	
        	//System.out.println("AUTH;APVCDE="+BCTR.getApprovalCode()+";ApprovalStatusMSG="+BCTR.getStatusMessage()+";TXNID="+BCTR.getTransactionId());
        	//System.out.println("Approval Status msg"+BCTR.getStatusMessage());
        	//System.out.println("TransAction ID:"+BCTR.getTransactionId());
        	
        	processBean.setAuth_status(true);
        	return processBean;
        }
        catch (ICWSBankcard_Authorize_CWSExpiredSecurityTokenFaultFault_FaultMessage e) {
        	
        	
        	System.out.println((processBean.getTrack1()!=null?"Track1 :"+processBean.getTrack1():"")+(processBean.getTrack2()!=null?" Track2 :"+processBean.getTrack2():"")+(" Pan No:"+processBean.getB_card_full_no()));
         	
        	ProcessingDAO.insertManualCCErrorLog(processBean);
        	e.printStackTrace();
        	ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
        	try{
        		//checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		//System.out.println((processBean.getTrack1()!=null?"Track1 :"+processBean.getTrack1():"")+(processBean.getTrack2()!=null?" Track2 :"+processBean.getTrack2():"")+(" Pan No:"+processBean.getB_card_full_no()));
        		e2.printStackTrace();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	processBean.setAuth_status(false);
        	return processBean;
        } 
        catch (ICWSBankcard_Authorize_CWSValidationResultFaultFault_FaultMessage e) {
        	//System.out.println((processBean.getTrack1()!=null?"Track1 :"+processBean.getTrack1():"")+(processBean.getTrack2()!=null?" Track2 :"+processBean.getTrack2():"")+(" Pan No:"+processBean.getB_card_full_no()));
        	e.printStackTrace();
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            
            ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
		 	
            processBean.setAuth_status(false);
        	return processBean;
        } 
        catch (Exception e) { 
        	//System.out.println((processBean.getTrack1()!=null?"Track1 :"+processBean.getTrack1():"")+(processBean.getTrack2()!=null?" Track2 :"+processBean.getTrack2():"")+(" Pan No:"+processBean.getB_card_full_no()));
        	e.printStackTrace();
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            
            ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
            
            //System.out.println("Error From IpCom:" + FV._strErrorMessage);
            processBean.setAuth_status(false);
        	return processBean;
        }
	}
		
	public PaymentProcessBean Adjust(ApplicationPoolBO poolBO,PaymentProcessBean processBean,String merchId,String serviceId){
		//The Adjust() operation is used to make adjustments to a previously authorized amount (incremental or reversal) prior to capture and settlement.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Adjusting_and_Voiding_Transactions/Adjust.aspx
		CWSBankcardStub CWSBC ;
		BankcardTransactionResponse BCTR;
        try
        {
    		//checkTokenExpire(_CV);//Always verify that the session token has not expired.
           // if (!SupportedTxnTypes.isAdjust()) return true;//Check to see if this transaction type is supported
   	
         
        	BCTR = new BankcardTransactionResponse();
                    	 
        	//Now Let's adjust the previously approved AuthorizeAndCapture
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust aTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Adjust();
        	com.ipcommerce.schemas.cws.v2_0.transactions.Adjust aDifference = new com.ipcommerce.schemas.cws.v2_0.transactions.Adjust();
        	
        	BigDecimal Amt = new BigDecimal(processBean.getB_amount()).setScale(2, RoundingMode.UNNECESSARY); //Previous Value was 42.00
        	aDifference.setAmount(Amt);
        	aDifference.setTransactionId(processBean.getB_trans_id()); //Simply use the TransactionId from the AuthorizeAndCapture
        	aTransaction.setApplicationProfileId(poolBO.get_ApplicationProfileId());
        	aTransaction.setMerchantProfileId(merchId);
        	aTransaction.setServiceId(serviceId);
        	aTransaction.setSessionToken(poolBO.get_SessionToken());
        	aTransaction.setDifferenceData(aDifference);
        	CWSBC = new CWSBankcardStub(poolBO.get_primaryTxnEndpoint());
        	BCTR = (BankcardTransactionResponse) CWSBC.adjust(aTransaction).getAdjustResult();
        	
        	processBean.setB_approval_code(BCTR.getApprovalCode());
        	processBean.setB_trans_id(BCTR.getTransactionId());
        	processBean.setB_approval_status(BCTR.getStatusMessage().equalsIgnoreCase("APPROVED")?"1":"2");
        	processBean.setAuth_status(true);
        	
        	//System.out.println("ADJST;APVCDE="+BCTR.getApprovalCode()+";ApprovalStatusMsg="+BCTR.getStatusMessage()+";TXNID="+BCTR.getTransactionId());
        	//System.out.println(";ApprovalStatusMsg="+BCTR.getStatusMessage());
        	//System.out.println(";TXNID="+BCTR.getTransactionId());
        	
        	return processBean;
        }
        catch (ICWSBankcard_Adjust_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	try{
        		//checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		
        		//System.out.println(""+BCTR.getStatusCode());
        		
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        	}
        	processBean.setAuth_status(false);
        	return processBean;
        } 
        catch (ICWSBankcard_Adjust_CWSValidationResultFaultFault_FaultMessage e) { 
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
            }
            processBean.setAuth_status(false);
        	return processBean;
        } 
        catch (Exception e) { 
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            }
            //System.out.println("Error From IpCom:" + FV._strErrorMessage);
            processBean.setAuth_status(false);
        	return processBean;
        }
	}
	
	public PaymentProcessBean Undo(ApplicationPoolBO poolBO,PaymentProcessBean processBean){
		//The Undo() operation is used to void (Credit Card) or reverse (PIN Debit) a transaction that has been previously authorized, but not yet captured (flagged) for settlement.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Adjusting_and_Voiding_Transactions/Undo.aspx
		CWSBankcardStub CWSBC ;
		 
        try
        {
    		//checkTokenExpire(_CV);//Always verify that the session token has not expired.
            //if (!SupportedTxnTypes.isUndo()) return true;//Check to see if this transaction type is supported
            
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
            
        	//Now Let's Undo the previously approved Authorize
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo uTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Undo();
        	BankcardUndo uDifference = new BankcardUndo();
        	
        	uDifference.setTransactionId(processBean.getB_trans_id()); //Simply use the TransactionId from the Authorize
        	
        	uTransaction.setApplicationProfileId(poolBO.get_ApplicationProfileId());
        	uTransaction.setMerchantProfileId(processBean.getMerchid());
        	uTransaction.setServiceId(processBean.getServiceid());
        	uTransaction.setSessionToken(poolBO.get_SessionToken());
        	uTransaction.setDifferenceData(uDifference);
        	
        	CWSBC = new CWSBankcardStub(poolBO.get_primaryTxnEndpoint());
        	BCTR = (BankcardTransactionResponse) CWSBC.undo(uTransaction).getUndoResult();
       	 
        	processBean.setB_approval_code(BCTR.getApprovalCode());
        	processBean.setB_trans_id(BCTR.getTransactionId());
        	processBean.setB_approval_status(BCTR.getStatusMessage().equalsIgnoreCase("APPROVED")?"1":"2");
        	processBean.setAuth_status(true);
        	
        	//System.out.println("REVERSE;APVCDE="+BCTR.getApprovalCode()+";ApprovalStatusMsg="+BCTR.getStatusMessage()+";TXNID="+BCTR.getTransactionId());
        	//System.out.println(";ApprovalStatusMsg="+BCTR.getStatusMessage());
        	//System.out.println(";TXNID="+BCTR.getTransactionId());
        	
        	processBean.setAuth_status(true);
         	return processBean;
        }
        catch (ICWSBankcard_Undo_CWSExpiredSecurityTokenFaultFault_FaultMessage e) {
        	e.printStackTrace();
        	try{
        		//checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	processBean.setAuth_status(false);
         	return processBean;
        } 
        catch (ICWSBankcard_Undo_CWSValidationResultFaultFault_FaultMessage e) {
        	e.printStackTrace();
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            processBean.setAuth_status(false);
        	return processBean;
        } 
        catch (Exception e) { 
        	e.printStackTrace();
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	e2.printStackTrace();
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            	//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            processBean.setAuth_status(false);
        	return processBean;
        }
	}
	
	public PaymentProcessBean Capture(ApplicationPoolBO poolBO,PaymentProcessBean processBean){
		//The Capture() operation is used to capture a single transaction for settlement after it has been successfully authorized by the Authorize() operation.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Capturing_Transactions_for_Settlement/Capture.aspx\
		CWSBankcardStub CWSBC;
		boolean flg = true;
        try
        {
    		//checkTokenExpire(_CV);//Always verify that the session token has not expired.
           // if (!SupportedTxnTypes.isCapture()) return true;//Check to see if this transaction type is supported

        	CWSBC = new CWSBankcardStub(poolBO.get_primaryTxnEndpoint());        	
             
             
            BankcardCaptureResponse BCCR = new BankcardCaptureResponse();
                    	 
        	      
        	//Now Let's Capture the previously approved Authorize
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture cTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.Capture();
        	BankcardCapture cDifference = new BankcardCapture();
        	
        	cDifference.setTransactionId(processBean.getB_trans_id()); //Simply use the TransactionId from the Authorize
        	//Conditional values. The following scenario would be used in a Restaurant industry type where tip is added.
        	
        	 double amount = Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt());
        	 double tip = Double.parseDouble(processBean.getB_tip_amt());
             NumberFormat formatter = new DecimalFormat("#0.00");
             
             //System.out.println("Amount" + formatter.format(amount));
             //BigDecimal Amt = new BigDecimal(formatter.format(amount)).setScale(2, RoundingMode.UNNECESSARY);
            // BigDecimal tipAmt = new BigDecimal(processBean.getB_tip_amt()).setScale(2, RoundingMode.UNNECESSARY);
        	
        	cDifference.setAmount(new BigDecimal(formatter.format(amount)).setScale(2, RoundingMode.UNNECESSARY)); //Only necessary if this value changed such as tip
        	cDifference.setTipAmount(new BigDecimal(formatter.format(tip)).setScale(2, RoundingMode.UNNECESSARY));
        	
        	cTransaction.setApplicationProfileId(poolBO.get_ApplicationProfileId());
        	cTransaction.setMerchantProfileId(processBean.getMerchid());
        	cTransaction.setServiceId(processBean.getServiceid());
        	cTransaction.setSessionToken(poolBO.get_SessionToken());
        	cTransaction.setDifferenceData(cDifference);
        	
        	BCCR = (BankcardCaptureResponse) CWSBC.capture(cTransaction).getCaptureResult();
        	
        	processBean.setB_cap_trans(BCCR.getTransactionId());
        	
        	for(int i=0;i<BCCR.getStatusCode().length();i++)
        	{
        		if(BCCR.getStatusCode().charAt(i) != '0')
        		{
        			flg = false;
        			break;
        		} 
        	}
        	
        	processBean.setB_approval_status(flg?"1":"2");
        	//processBean.setB_approval_status(BCCR.getStatus() == Status.SUCCESSFUL?"1":"2");	
        	
          	//System.out.println("CAPTURE;TXNID="+BCCR.getTransactionId()+";Amount=" + formatter.format(amount)+";ApprovaLStatusMsg="+BCCR.getStatusMessage()+";Status=" + BCCR.getStatusCode());
        	//System.out.println(";Status=" + BCCR.getStatusCode()) ;
          	
        	processBean.setAuth_status(true);
         	return processBean;
        }
        catch (ICWSBankcard_Capture_CWSExpiredSecurityTokenFaultFault_FaultMessage e) {
        	e.printStackTrace();
        	
        	ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
		 	
        	try{
        		//checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		e2.printStackTrace();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	processBean.setAuth_status(false);
         	return processBean;
        } 
        catch (ICWSBankcard_Capture_CWSValidationResultFaultFault_FaultMessage e) {
        	e.printStackTrace();
        	
        	ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
        	
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            processBean.setAuth_status(false);
         	return processBean;
        } 
        catch (Exception e) { 
        	e.printStackTrace();
        	ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,e.getMessage(),poolBO,1);
		 	oneDirect.start();
			
		 	
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
        		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	e2.printStackTrace();
            	FV._strErrorID = "";
            	FV._strErrorMessage = e.getMessage();
            	//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            processBean.setAuth_status(false);
         	return processBean;
        }
	}
	
	 
	
	public PaymentProcessBean ReturnById(ApplicationPoolBO poolBO,PaymentProcessBean processBean){
		//The ReturnById() operation is used to return funds to a payment account based on a transaction that has been previously settled.
		//http://docs.commercelab.ipcommerce.com/CWS_Developer_Guide/Implementing_Commerce_Web_Services/Transaction_Processing/Refunding_Transactions/ReturnById.aspx
		CWSBankcardStub CWSBC;
        try
        {
    		//checkTokenExpire(_CV);//Always verify that the session token has not expired.
            //if (!SupportedTxnTypes.isReturnById()) return true;//Check to see if this transaction type is supported

        	CWSBC = new CWSBankcardStub(poolBO.get_primaryTxnEndpoint());
            BankcardTransactionResponse BCTR = new BankcardTransactionResponse();
        	 
        	      
        	//Now Let's ReturnById the previously approved AuthorizeAndCapture
        	com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById rTransaction = new com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.ReturnById();
        	BankcardReturn rDifference = new BankcardReturn();
        	
        	rDifference.setTransactionId(processBean.getB_trans_id()); //Simply use the TransactionId from the AuthorizeAndCapture
        	
        	//Conditional values. The following scenario would be used in a partial return.
        	//rDifference.setAmount(new BigDecimal("25.00").setScale(2, RoundingMode.UNNECESSARY)); //Only necessary if this value changed such as tip
         	//double amt = Double.parseDouble(processBean.getB_amount())+ Double.parseDouble(processBean.getB_tip_amt());
        	double amt = Double.parseDouble(processBean.getTxn_total());
         	NumberFormat format = new DecimalFormat("#0.00");
         	//System.out.println("Amount:" + format.format(amt));
         	
        	rDifference.setAmount(new BigDecimal(format.format(amt)).setScale(2, RoundingMode.UNNECESSARY));
        	
        	rTransaction.setApplicationProfileId(poolBO.get_ApplicationProfileId());
        	rTransaction.setMerchantProfileId(processBean.getMerchid());
        	rTransaction.setServiceId(processBean.getServiceid());
        	rTransaction.setSessionToken(poolBO.get_SessionToken());
        	rTransaction.setDifferenceData(rDifference);
        	BCTR = (BankcardTransactionResponse) CWSBC.returnById(rTransaction).getReturnByIdResult();
        	
        	
        	//System.out.println("RETURN;APVCDE"+BCTR.getApprovalCode()+";ApprovalStatusMsg"+BCTR.getStatusMessage()+";TXNID:"+BCTR.getTransactionId());
        	//System.out.println(";ApprovalStatusMsg"+BCTR.getStatusMessage());
        	//System.out.println(";TXNID:"+BCTR.getTransactionId());
        	
        	
        	processBean.setB_approval_code(BCTR.getApprovalCode());
        	processBean.setB_ret_transid(BCTR.getTransactionId());
        	//processBean.setB_approval_status(BCTR.getStatusMessage().equalsIgnoreCase("APPROVED")?"1":"2");
        	processBean.setB_approval_status("1");
        	 
         	processBean.setAuth_status(true);
         	return processBean;
        }
        catch (ICWSBankcard_ReturnById_CWSExpiredSecurityTokenFaultFault_FaultMessage e) { 
        	e.printStackTrace();
        	try{
        		//checkTokenExpire(_CV);
        	}
        	catch(Exception e2) {
        		e2.printStackTrace();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
        		
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
        	}
        	processBean.setB_approval_status("2");
        	processBean.setAuth_status(false);
         	return processBean;
        } 
        catch (ICWSBankcard_ReturnById_CWSValidationResultFaultFault_FaultMessage e) { 
        	e.printStackTrace();
        	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationResultFault detail = e.getFaultMessage();
            java.util.Iterator<CWSValidationErrorFault> errors = detail.getErrors().getCWSValidationErrorFault().iterator();
            while (errors.hasNext())
            {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSValidationErrorFault error = errors.next();
        		FaultValues FV = new FaultValues();
        		FV._strErrorID = error.getRuleLocationKey().toString();
        		FV._strErrorMessage = error.getRuleMessage();
        		
        		//System.out.println("Error From IpCom:" + FV._strErrorMessage);
            }
            processBean.setB_approval_status("2");
        	processBean.setAuth_status(false);
         	return processBean;
        } 
        catch (Exception e) { 
        	//e.printStackTrace();
        	FaultValues FV = new FaultValues();
        	try {
            	com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault faultInfo = (com.ipcommerce.schemas.cws.v2_0.transactions.faults.CWSBaseFault) e.getClass().getMethod("getFaultInfo", null).invoke(e, null);
          		FV._strErrorID = faultInfo.getErrorID().toString();
        		FV._strErrorMessage = faultInfo.getProblemType();
        		
            }
            catch (Exception e2) {// Not a IP Commerce fault.
            	e2.printStackTrace();
            	FV._strErrorID = "";
        		FV._strErrorMessage = e.getMessage();
            }
            //System.out.println("Error From IpCom:" + FV._strErrorMessage);
            
            processBean.setAuth_status(false);
            processBean.setB_approval_status("2");
          	return processBean;
        }
	}
	
	
	private BankcardTransaction setBankCardTxnData(PaymentProcessBean processBean)
    {
		String card_no="";
		
		BankcardTransaction BCtransaction = new BankcardTransaction();

        BankcardTransactionData BCTD = new BankcardTransactionData();
        //BigDecimal Amt = new BigDecimal("42.00").setScale(2, RoundingMode.UNNECESSARY);
         
         
        double amount = Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt());
        double tip = Double.parseDouble(processBean.getB_tip_amt());
        NumberFormat formatter = new DecimalFormat("#0.00");
        
        //System.out.println("Amount" + formatter.format(amount));
        BigDecimal Amt = new BigDecimal(formatter.format(amount)).setScale(2, RoundingMode.UNNECESSARY);
        BigDecimal tipAmt = new BigDecimal(formatter.format(tip)).setScale(2, RoundingMode.UNNECESSARY);
        
        //System.out.println("After amount:" + Amt);
        
        BCTD.setAmount(Amt);
        BCTD.setTipAmount(tipAmt);
        
        BCTD.setCurrencyCode(com.ipcommerce.schemas.cws.v2_0.transactions.TypeISOCurrencyCodeA3.USD);
        BCTD.setIndustryType(IndustryType.RETAIL);
        BCTD.setSignatureCaptured(false);

        BankcardTenderData BCTen = new BankcardTenderData();
        CardData CD = new CardData();
        
      //AVSData
        AVSData AVS = new AVSData();
        CardSecurityData CSD = new CardSecurityData();
        BCTD.setCustomerPresent(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CustomerPresent.PRESENT);
        if(!processBean.getTrack1().equals(""))
        {
        	BCTD.setEntryMode(EntryMode.TRACK_DATA_FROM_MSR);
            
        	CD.setTrack1Data(processBean.getTrack1());
        	//CD.setTrack1Data("5454545454545454XXX^XXXXXXXXXXXXXXXXXXXXXXXXXX^1012XXXXXX");
        	
        	    
        } else if(processBean.getB_paymentType().equals("M"))
        {
        	BCTD.setEntryMode(EntryMode.KEYED);
            //BCTD.setCustomerPresent(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CustomerPresent.ECOMMERCE);
            if(!processBean.getZip().equals("") || !processBean.getCvv().equals(""))
           {
        	   if(!processBean.getZip().equals(""))
        	   {
        		   // AVS DATA
        		   AVS.setStreet(processBean.getAdd1()+","+processBean.getAdd2());
        		   AVS.setCity(processBean.getCity());
        		   AVS.setStateProvince(getstate(processBean.getState()));
        		   AVS.setPostalCode(processBean.getZip());
        	   }           
               //CVData
               CSD.setAVSData(AVS);
               if(!processBean.getCvv().equals(""))
        	   {
            	   CSD.setCVDataProvided(CVDataProvided.PROVIDED);
            	   CSD.setCVData(processBean.getCvv());
        	   } else {
        		   CSD.setCVDataProvided(CVDataProvided.NOT_SET);
        	   }
           }
            
        	        	 
        } else {
        	BCTD.setEntryMode(EntryMode.TRACK_DATA_FROM_MSR);
            //BCTD.setCustomerPresent(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CustomerPresent.PRESENT);
        	CD.setTrack2Data(processBean.getTrack2());
        	//CD.setTrack2Data("5454545454545454=13121010134988000010");
        }
               
        if(processBean.getB_card_issuer().equals("5"))
    	{
    		CD.setCardType(TypeCardType.MASTER_CARD);
    	} else if(processBean.getB_card_issuer().equals("3"))
    	{
    		CD.setCardType(TypeCardType.AMERICAN_EXPRESS);
    	} else if(processBean.getB_card_issuer().equals("4"))
    	{
    		CD.setCardType(TypeCardType.VISA);
    	} else if(processBean.getB_card_issuer().equalsIgnoreCase("6"))
    	{
    		CD.setCardType(TypeCardType.DISCOVER);
    	}
    	
        CD.setExpire(processBean.getB_cardExpiryDate());
        
        card_no = processBean.getB_card_full_no().replaceAll(" ", "");
        
       // System.out.println("Card No in Service TXN Action :"+card_no);
        
        CD.setPAN(card_no);
        //CD.setExpire("1210"); // exactly 4 digits � MMYY where as a swipe will be YYMM
        //CD.setPAN("5454545454545454");
        //Used for Retail
        BCTD.setEmployeeId("123h");

        //Used for Ecommerce
        //BCTD.setOrderNumber("123");
        
      //  System.out.println("==========================trip:" + processBean.getB_trip_id());
        BCTD.setOrderNumber(processBean.getB_trip_id());
      //  BCTD.setInvoiceNumber(processBean.getB_trip_id());
       // BCTD.setTerminalId(processBean.getB_driverid());

        BCtransaction.setTransactionData(BCTD);      
        
        BCTen.setCardData(CD);  
                
        BCTen.setCardSecurityData(CSD);
        BCtransaction.setTenderData(BCTen);
        
        return BCtransaction;
    }
	

	/*private BankcardTransaction setBankCardTxnData(PaymentProcessBean processBean)
    {
		
		BankcardTransaction BCtransaction = new BankcardTransaction();

        BankcardTransactionData BCTD = new BankcardTransactionData();
        //BigDecimal Amt = new BigDecimal("42.00").setScale(2, RoundingMode.UNNECESSARY);
         
        BigDecimal Amt = new BigDecimal(processBean.getB_amount()).setScale(2, RoundingMode.UNNECESSARY);
        BigDecimal tipAmt = new BigDecimal(processBean.getB_tip_amt()).setScale(2, RoundingMode.UNNECESSARY);
        
        BCTD.setAmount(Amt);
        BCTD.setTipAmount(tipAmt);
        
        BCTD.setCurrencyCode(com.ipcommerce.schemas.cws.v2_0.transactions.TypeISOCurrencyCodeA3.USD);
        BCTD.setIndustryType(IndustryType.RETAIL);
        BCTD.setSignatureCaptured(false);

        BankcardTenderData BCTen = new BankcardTenderData();
        CardData CD = new CardData();
        
        if(!processBean.getTrack1().equals(""))
        {
        	BCTD.setEntryMode(EntryMode.TRACK_DATA_FROM_MSR);
            BCTD.setCustomerPresent(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CustomerPresent.PRESENT);
        	CD.setTrack1Data(processBean.getTrack1());
        	//CD.setTrack1Data("5454545454545454XXX^XXXXXXXXXXXXXXXXXXXXXXXXXX^1012XXXXXX");
        } else if(processBean.getB_paymentType().equals("M"))
        {
        	BCTD.setEntryMode(EntryMode.KEYED);
            BCTD.setCustomerPresent(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CustomerPresent.ECOMMERCE);
        	//CD.setExpire("1210"); // exactly 4 digits � MMYY where as a swipe will be YYMM
            //CD.setPAN("5454545454545454");        	 
        } else {
        	BCTD.setEntryMode(EntryMode.TRACK_DATA_FROM_MSR);
            BCTD.setCustomerPresent(com.ipcommerce.schemas.cws.v2_0.transactions.bankcard.CustomerPresent.PRESENT);
        	CD.setTrack2Data(processBean.getTrack2());
        	//CD.setTrack2Data("5454545454545454=13121010134988000010");
        }
               
        if(processBean.getB_card_issuer().equals("5"))
    	{
    		CD.setCardType(TypeCardType.MASTER_CARD);
    	} else if(processBean.getB_card_issuer().equals("3"))
    	{
    		CD.setCardType(TypeCardType.AMERICAN_EXPRESS);
    	} else if(processBean.getB_card_issuer().equals("4"))
    	{
    		CD.setCardType(TypeCardType.VISA);
    	} else if(processBean.getB_card_issuer().equalsIgnoreCase("6"))
    	{
    		CD.setCardType(TypeCardType.DISCOVER);
    	}
    	
        CD.setExpire("1210");
        CD.setPAN(processBean.getB_card_full_no());
             

        //Used for Retail
        BCTD.setEmployeeId("123h");

        //Used for Ecommerce
        //BCTD.setOrderNumber("123");
        
      //  System.out.println("==========================trip:" + processBean.getB_trip_id());
        BCTD.setOrderNumber(processBean.getB_trip_id());
        //BCTD.setInvoiceNumber(processBean.getB_trip_id());
        //BCTD.setTerminalId(processBean.getB_driverid());

        BCtransaction.setTransactionData(BCTD);
        
        
        BCTen.setCardData(CD);
               
        CardSecurityData CSD = new CardSecurityData();
        //AVSData
        AVSData AVS = new AVSData();
        AVS.setPostalCode("45040");
        CSD.setAVSData(AVS);
        //CVData
        CSD.setCVDataProvided(CVDataProvided.PROVIDED);
        CSD.setCVData("111");
        
        BCTen.setCardSecurityData(CSD);
        BCtransaction.setTenderData(BCTen);
        
        return BCtransaction;
    }
*/
	
	public static TypeStateProvince getstate(String state){
		if(state.equals("AK"))
			return TypeStateProvince.AK;
		else if(state.equals("AL"))
			return TypeStateProvince.AL;
		else if(state.equals("AR"))
			return TypeStateProvince.AR;
		else if(state.equals("AZ"))
			return TypeStateProvince.AZ;
		else if(state.equals("CA"))
			return TypeStateProvince.CA;
		else if(state.equals("CO"))
			return TypeStateProvince.CO;
		else if(state.equals("CT"))
			return TypeStateProvince.CT;
		else if(state.equals("DC"))
			return TypeStateProvince.DC;
		else if(state.equals("DE"))
			return TypeStateProvince.DE;
		else if(state.equals(""))
			return TypeStateProvince.AL;
		else if(state.equals("FL"))
			return TypeStateProvince.FL;
		else if(state.equals("GA"))
			return TypeStateProvince.GA;
		else if(state.equals("HI"))
			return TypeStateProvince.HI;
		else if(state.equals("IA"))
			return TypeStateProvince.IA;
		else if(state.equals("ID"))
			return TypeStateProvince.ID;
		else if(state.equals("IL"))
			return TypeStateProvince.IL;
		else if(state.equals("IN"))
			return TypeStateProvince.IN;
		else if(state.equals("KS"))
			return TypeStateProvince.KS;
		else if(state.equals("KY"))
			return TypeStateProvince.KY;
		else if(state.equals("LA"))
			return TypeStateProvince.LA;
		else if(state.equals("MA"))
			return TypeStateProvince.MA;
		else if(state.equals("MD"))
			return TypeStateProvince.MD;
		else if(state.equals("ME"))
			return TypeStateProvince.MI;
		else if(state.equals("MN"))
			return TypeStateProvince.MN;
		else if(state.equals("MO"))
			return TypeStateProvince.MO;
		else if(state.equals("MP"))
			return TypeStateProvince.MP;
		else if(state.equals("MS"))
			return TypeStateProvince.MS;
		else if(state.equals("MT"))
			return TypeStateProvince.MT;
		else if(state.equals("NC"))
			return TypeStateProvince.NC;
		else if(state.equals("ND"))
			return TypeStateProvince.ND;
		else if(state.equals("NE"))
			return TypeStateProvince.NE;
		else if(state.equals("NH"))
			return TypeStateProvince.NH;
		else if(state.equals("NJ"))
			return TypeStateProvince.NJ;
		else if(state.equals("NM"))
			return TypeStateProvince.NM;
		else if(state.equals("NV"))
			return TypeStateProvince.NV;
		else if(state.equals("NY"))
			return TypeStateProvince.NY;
		else if(state.equals("OH"))
			return TypeStateProvince.OH;
		else if(state.equals("OK"))
			return TypeStateProvince.OK;
		else if(state.equals("OR"))
			return TypeStateProvince.OR;
		else if(state.equals("PA"))
			return TypeStateProvince.PA;
		else if(state.equals("RI"))
			return TypeStateProvince.RI;
		else if(state.equals("SC"))
			return TypeStateProvince.SC;
		else if(state.equals("SD"))
			return TypeStateProvince.SD;
		else if(state.equals("TN"))
			return TypeStateProvince.TN;
		else if(state.equals("TX"))
			return TypeStateProvince.TX;
		else if(state.equals("UT"))
			return TypeStateProvince.UT;
		else if(state.equals("VA"))
			return TypeStateProvince.VA;
		else if(state.equals("VT"))
			return TypeStateProvince.VT;
		else if(state.equals("WA"))
			return TypeStateProvince.WA;
		else if(state.equals("WI"))
			return TypeStateProvince.WI;
		else if(state.equals("WV"))
			return TypeStateProvince.WV;
		else
			return TypeStateProvince.WY;
		
	}
	
	public static com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince getstateService(String state){
		if(state.equals("AK"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.AK;
		else if(state.equals("AL"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.AL;
		else if(state.equals("AR"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.AR;
		else if(state.equals("AZ"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.AZ;
		else if(state.equals("CA"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.CA;
		else if(state.equals("CO"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.CO;
		else if(state.equals("CT"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.CT;
		else if(state.equals("DC"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.DC;
		else if(state.equals("DE"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.DE;
		else if(state.equals(""))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.AL;
		else if(state.equals("FL"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.FL;
		else if(state.equals("GA"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.GA;
		else if(state.equals("HI"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.HI;
		else if(state.equals("IA"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.IA;
		else if(state.equals("ID"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.ID;
		else if(state.equals("IL"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.IL;
		else if(state.equals("IN"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.IN;
		else if(state.equals("KS"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.KS;
		else if(state.equals("KY"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.KY;
		else if(state.equals("LA"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.LA;
		else if(state.equals("MA"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.MA;
		else if(state.equals("MD"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.MD;
		else if(state.equals("ME"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.MI;
		else if(state.equals("MN"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.MN;
		else if(state.equals("MO"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.MO;
		else if(state.equals("MP"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.MP;
		else if(state.equals("MS"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.MS;
		else if(state.equals("MT"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.MT;
		else if(state.equals("NC"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.NC;
		else if(state.equals("ND"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.ND;
		else if(state.equals("NE"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.NE;
		else if(state.equals("NH"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.NH;
		else if(state.equals("NJ"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.NJ;
		else if(state.equals("NM"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.NM;
		else if(state.equals("NV"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.NV;
		else if(state.equals("NY"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.NY;
		else if(state.equals("OH"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.OH;
		else if(state.equals("OK"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.OK;
		else if(state.equals("OR"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.OR;
		else if(state.equals("PA"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.PA;
		else if(state.equals("RI"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.RI;
		else if(state.equals("SC"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.SC;
		else if(state.equals("SD"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.SD;
		else if(state.equals("TN"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.TN;
		else if(state.equals("TX"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.TX;
		else if(state.equals("UT"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.UT;
		else if(state.equals("VA"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.VA;
		else if(state.equals("VT"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.VT;
		else if(state.equals("WA"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.WA;
		else if(state.equals("WI"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.WI;
		else if(state.equals("WV"))
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.WV;
		else
			return com.ipcommerce.schemas.cws.v2_0.serviceinformation.TypeStateProvince.WY;
		
	}
	
	public ConfigurationValues checkTokenExpire(ApplicationPoolBO poolBO,ConfigurationValues _CV) throws Exception{
		try {
				
				CWSServiceInformationStub CWSSIC;
				if(_CV._SessionTokenDateTime == null){
					//First Time so get a new Session Token
					CWSSIC = new CWSServiceInformationStub(poolBO.get_primaryServiceEndpoint());
					SignOnWithToken signOnWithToken = new SignOnWithToken();
					signOnWithToken.setIdentityToken(poolBO.get_IdentityToken());
					SignOnWithTokenResponse response = CWSSIC.signOnWithToken(signOnWithToken);
		
					_CV._SessionToken =response.getSignOnWithTokenResult().toString();
					
					GregorianCalendar gc1 = new GregorianCalendar();
					Date d1 = gc1.getTime();
					_CV._SessionTokenDateTime = d1.getTime()+(30*60*1000);
					
					//System.out.println("SessionTokenTime"+d1.getTime()+";SessionTokenExpTime"+_CV._SessionTokenDateTime);
					//System.out.println("Session Token EXP time"+_CV._SessionTokenDateTime);
					
				}
				else
				{
					//Let's check to see if the token has expired or is about to expire. 
					GregorianCalendar gc = new GregorianCalendar();
					Date d = gc.getTime();
					long l = d.getTime();
					//long difference = l - _CV._SessionTokenDateTime;
					//difference = difference/60000;
					//if(difference>25){
					if(l > _CV._SessionTokenDateTime){
						//In this case the session token has either expired or will expire in less than 5 min. So get a new one.
						CWSSIC = new CWSServiceInformationStub(poolBO.get_primaryServiceEndpoint());
						SignOnWithToken signOnWithToken = new SignOnWithToken();
						signOnWithToken.setIdentityToken(poolBO.get_IdentityToken());
						SignOnWithTokenResponse response = CWSSIC.signOnWithToken(signOnWithToken);
			
						_CV._SessionToken =response.getSignOnWithTokenResult().toString();
						
						GregorianCalendar gc1 = new GregorianCalendar();
						Date d1 = gc1.getTime();
						_CV._SessionTokenDateTime = d1.getTime();		
					}
				}
				
				
		} catch (Exception ex) {
			_CV._Status = false;
			ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,"Session Token did not get",poolBO,1);
		 	oneDirect.start();
			throw ex;
		}
		return _CV;
	}
	
}
