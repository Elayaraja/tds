package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;

public class EditHistoryJobsDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(RequestDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+RequestDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	
	public static ArrayList<OpenRequestBO> getHistotyJobsByVoucherandDate(AdminRegistrationBO adminBO, String VoucherNo, String fdate, String tdate){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> orList=new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query ="SELECT ORH_TRIP_ID,ORH_PHONE,ORH_STADD1,ORH_STADD2,ORH_STCITY,ORH_STSTATE,ORH_STZIP,ORH_EDADD1,ORH_EDADD2,ORH_EDCITY,ORH_EDSTATE,ORH_EDZIP,ORH_SERVICEDATE,ORH_ENTEREDTIME,ORH_DISPATCH_START_TIME, ORH_JOB_ACCEPT_TIME,ORH_JOB_PICKUP_TIME,ORH_JOB_COMPLETE_TIME,ORH_TRIP_STATUS,case when ORH_TRIP_STATUS=61 THEN 'Job Completed' when ORH_TRIP_STATUS=70 THEN 'Job Payment Settled' end as TRIPSTATUS,"
				+ "DATE_FORMAT(CONVERT_TZ(ORH_JOB_PICKUP_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d %H:%i:%s') AS TIMESTART, DATE_FORMAT(CONVERT_TZ(ORH_JOB_COMPLETE_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d %H:%i:%s') AS TIMEEND,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d %H:%i:%s') AS ORH_SERVICEDATE_FORMATTED,"
				+ "DATE_FORMAT(CONVERT_TZ(TEST.ORL_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d %H:%i:%s') AS TIMEONSITE,DATE_FORMAT(CONVERT_TZ(TEST1.ORL_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d %H:%i:%s') AS TIMELSTART, DATE_FORMAT(CONVERT_TZ(TEST2.ORL_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d %H:%i:%s') AS TIMELEND,ORH_PAYACC,ORH_PAYTYPE,ORH_CUST_REF_NUM FROM TDS_OPENREQUEST_HISTORY "
				+ "LEFT JOIN TDS_OPENREQUEST_LOGS AS TEST ON ORH_MASTER_ASSOCCODE=TEST.ORL_MASTER_ASSOCCODE AND ORH_TRIP_ID=TEST.ORL_TRIPID AND TEST.ORL_REASON ='DRIVER ONSITE' LEFT JOIN TDS_OPENREQUEST_LOGS AS TEST1 ON ORH_MASTER_ASSOCCODE=TEST1.ORL_MASTER_ASSOCCODE AND ORH_TRIP_ID=TEST1.ORL_TRIPID AND TEST1.ORL_REASON ='TRIP STARTED' "
				+ "LEFT JOIN TDS_OPENREQUEST_LOGS AS TEST2 ON ORH_MASTER_ASSOCCODE=TEST2.ORL_MASTER_ASSOCCODE AND ORH_TRIP_ID=TEST2.ORL_TRIPID  AND TEST2.ORL_REASON ='TRIP ENDED'"
				+ " WHERE ORH_MASTER_ASSOCCODE='103' AND ORH_SERVICEDATE <=CONVERT_TZ(STR_TO_DATE('"+tdate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC')  AND ORH_SERVICEDATE >=CONVERT_TZ(STR_TO_DATE('"+fdate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') " ;
		if(VoucherNo!=null && !VoucherNo.equals("")){
			query = query+"AND ORH_PAYACC='"+VoucherNo+"'";
		}
		
		query=query+"AND (ORH_TRIP_STATUS='61' OR ORH_TRIP_STATUS='70') GROUP BY ORH_TRIP_ID ORDER BY ORH_SERVICEDATE";
		try{
			pst =  con.prepareStatement(query);
			System.out.println("getalljobs:"+pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				OpenRequestBO openRequestBO=new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("ORH_TRIP_ID"));
				openRequestBO.setPhone(rs.getString("ORH_PHONE"));
				
				String stAddress = rs.getString("ORH_STADD1") + " " +rs.getString("ORH_STADD2") + " " +rs.getString("ORH_STCITY") + " " +rs.getString("ORH_STZIP");
				openRequestBO.setSadd1(stAddress);
				
				String edAddress = rs.getString("ORH_EDADD1") + " " +rs.getString("ORH_EDADD2") + " " +rs.getString("ORH_EDCITY") + " " +rs.getString("ORH_EDZIP");
				openRequestBO.setEadd1(edAddress);
				
				openRequestBO.setAcct(rs.getString("ORH_PAYACC"));
				openRequestBO.setPaytype(rs.getString("ORH_PAYTYPE"));
				openRequestBO.setTripStatus(rs.getString("ORH_TRIP_STATUS"));
				openRequestBO.setOR_SMS_SENT(rs.getString("TIMEONSITE"));
				if(rs.getString("TIMESTART")!=null && !rs.getString("TIMESTART").equals("")){
					openRequestBO.setCreatedTime(rs.getString("TIMESTART"));
				}else{
					openRequestBO.setCreatedTime(rs.getString("TIMELSTART"));
				}
				if(rs.getString("TIMEEND")!=null && !rs.getString("TIMEEND").equals("")){
					openRequestBO.setCompletedTime(rs.getString("TIMEEND"));
				}else{
					openRequestBO.setCompletedTime(rs.getString("TIMELEND"));
				}
				openRequestBO.setRefNumber(rs.getString("ORH_CUST_REF_NUM"));
				orList.add(openRequestBO); 
			}
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.getHistotyJobsByVoucherandDate-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.println("TDSException RequestDAO.getHistotyJobsByVoucherandDate-->" + sqex.getMessage());
			//sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return orList;
	}

	public static int updateHistoryTimings(AdminRegistrationBO adminBO,String tripid,String stTime, String edTime){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateHistoryTimings");
		int status = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			String queryForHistory = "UPDATE TDS_OPENREQUEST_HISTORY SET ORH_JOB_PICKUP_TIME=CONVERT_TZ(STR_TO_DATE('"+stTime+"', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC'),ORH_JOB_COMPLETE_TIME=CONVERT_TZ(STR_TO_DATE('"+edTime+"', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC'),ORH_SERVICEDATE=CONVERT_TZ(STR_TO_DATE('"+stTime+"', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') WHERE (ORH_ASSOCCODE=? OR ORH_MASTER_ASSOCCODE=?) AND ORH_TRIP_ID=? ";
			String queryForLogsStartTime = "UPDATE TDS_OPENREQUEST_LOGS SET ORL_TIME=CONVERT_TZ(STR_TO_DATE('"+stTime+"', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') WHERE (ORL_ASSOCCODE=? OR ORL_MASTER_ASSOCCODE=?) AND ORL_TRIPID=? AND ORL_STATUS='35' ORDER BY ORL_SNO DESC";
			String queryForLogsEndTime 	 = "UPDATE TDS_OPENREQUEST_LOGS SET ORL_TIME=CONVERT_TZ(STR_TO_DATE('"+edTime+"', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') WHERE (ORL_ASSOCCODE=? OR ORL_MASTER_ASSOCCODE=?) AND ORL_TRIPID=? AND ORL_STATUS='45'";
			
			pst=con.prepareStatement(queryForHistory);
			pst.setString(1,adminBO.getAssociateCode());
			pst.setString(2, adminBO.getMasterAssociateCode());
			pst.setString(3,tripid);
			System.out.println("update Hist:"+pst.toString());
			status=pst.executeUpdate();
			
			if(status>0){
				pst=con.prepareStatement(queryForLogsStartTime);
				pst.setString(1,adminBO.getAssociateCode());
				pst.setString(2, adminBO.getMasterAssociateCode());
				pst.setString(3,tripid);
				System.out.println("update st:"+pst.toString());
				pst.executeUpdate();
				
				pst=con.prepareStatement(queryForLogsEndTime);
				pst.setString(1,adminBO.getAssociateCode());
				pst.setString(2, adminBO.getMasterAssociateCode());
				pst.setString(3,tripid);
				System.out.println("update ed:"+pst.toString());
				pst.executeUpdate();
			}
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateHistoryTimings-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.println("TDSException RequestDAO.updateHistoryTimings-->"+sqex.getMessage());
			//sqex.printStackTrace();
		}

		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	
	public static int updateHistoryEA(AdminRegistrationBO adminBO,String tripid,String eadd1, String eadd2, String eCity, String epost, String elati, String elong){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateHistoryTimings");
		int status = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			String queryForHistory = "UPDATE TDS_OPENREQUEST_HISTORY SET ORH_EDADD1='"+eadd1+"', ORH_EDADD2='"+eadd2+"', ORH_EDCITY='"+eCity+"', ORH_EDZIP='"+epost+"', ORH_EDLATITUDE='"+elati+"', ORH_EDLONGITUDE='"+elong+"' WHERE (ORH_ASSOCCODE=? OR ORH_MASTER_ASSOCCODE=?) AND ORH_TRIP_ID=? ";
			//String queryForLogsStartTime = "UPDATE TDS_OPENREQUEST_LOGS SET ORL_TIME=CONVERT_TZ(STR_TO_DATE('"+stTime+"', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') WHERE (ORL_ASSOCCODE=? OR ORL_MASTER_ASSOCCODE=?) AND ORL_TRIPID=? AND ORL_STATUS='35' ORDER BY ORL_SNO DESC";
			//String queryForLogsEndTime 	 = "UPDATE TDS_OPENREQUEST_LOGS SET ORL_TIME=CONVERT_TZ(STR_TO_DATE('"+edTime+"', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') WHERE (ORL_ASSOCCODE=? OR ORL_MASTER_ASSOCCODE=?) AND ORL_TRIPID=? AND ORL_STATUS='45'";
			
			pst=con.prepareStatement(queryForHistory);
			pst.setString(1,adminBO.getAssociateCode());
			pst.setString(2, adminBO.getMasterAssociateCode());
			pst.setString(3,tripid);
			//System.out.println("update Hist:"+pst.toString());
			status=pst.executeUpdate();
			
			/*if(status>0){
				pst=con.prepareStatement(queryForLogsStartTime);
				pst.setString(1,adminBO.getAssociateCode());
				pst.setString(2, adminBO.getMasterAssociateCode());
				pst.setString(3,tripid);
				//System.out.println("update st:"+pst.toString());
				pst.executeUpdate();
				
				pst=con.prepareStatement(queryForLogsEndTime);
				pst.setString(1,adminBO.getAssociateCode());
				pst.setString(2, adminBO.getMasterAssociateCode());
				pst.setString(3,tripid);
				//System.out.println("update ed:"+pst.toString());
				pst.executeUpdate();
			}*/
			
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateHistoryTimings-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.println("TDSException RequestDAO.updateHistoryTimings-->"+sqex.getMessage());
			//sqex.printStackTrace();
		}

		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
}
