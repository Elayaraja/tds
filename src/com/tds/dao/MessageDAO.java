package com.tds.dao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Category;
import org.codehaus.jettison.json.JSONException;

import com.tds.cmp.bean.MessageBeen;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverChargeBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.DriverLocationBO;
public class MessageDAO {

	private static Category cat =TDSController.cat;
	static {

		cat = Category.getInstance(MessageDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+MessageDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static String getMessageAndUpdateForDriver(DriverLocationBO driverLocBO,int typeOfUpdate,String assoccode,String dummyCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		TDSConnection dbcon = null;
		Connection con = null;
		CallableStatement cst = null;
		ResultSet rs = null;
		String message="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(typeOfUpdate==0){
				cst = con.prepareCall("{call TDS_MESSAGE_HANDLER(?,?,?,?,?,?,?)}");
			} else {
				cst = con.prepareCall("{call TDS_MESSAGE_HANDLER_SWITCH(?,?,?,?,?,?,?)}");
			}
			cst.setString(1, driverLocBO.getDriverid());
			cst.setString(2, driverLocBO.getAssocCode());
			cst.setString(3, driverLocBO.getStatus());
			cst.setString(4, driverLocBO.getLatitude());
			cst.setString(5, driverLocBO.getLongitude());
			cst.setString(6, assoccode);
			cst.setString(7, dummyCode);
			cst.execute();
			rs = cst.getResultSet();
			if(rs.next()){
				message =  rs.getString("O_MSGTEXT");
			}
		} catch (Exception sqex) {
			cat.error("TDSException MessageDAO.getMessageAndUpdateForDriver--->"+sqex.getMessage());
			//System.out.println("TDSException MessageDAO.getMessageAndUpdateForDriver--->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return message;		
	}
	public static void storeMessageToDBForADriver(String assocCode, String driverID,String messageText)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		ResultSet rs = null;
		PreparedStatement m_pst= null;
		if(driverID!=null && ! driverID.equals("")){
			try{
				messageText=messageText.replace("'", "");
				m_pst = m_conn.prepareStatement("SELECT MSG_MSGTEXT FROM TDS_DRIVER_MESSAGES WHERE MSG_ASSOCODE='"+assocCode+"' AND MSG_DRIVER_ID='"+driverID+"'");
				rs = m_pst.executeQuery();
				if(rs.next()){
					String finalMessageText =rs.getString("MSG_MSGTEXT")+"^"+messageText;
					m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER_MESSAGES SET MSG_MSGTEXT='"+finalMessageText+"' WHERE MSG_ASSOCODE='"+assocCode+"' AND MSG_DRIVER_ID='"+driverID+"'");
					m_pst.executeUpdate();
				} else {
					m_pst = m_conn.prepareStatement("INSERT INTO TDS_DRIVER_MESSAGES (MSG_ASSOCODE, MSG_DRIVER_ID,MSG_MSGTEXT) values ('" + assocCode + "','" +  driverID+ "','" + messageText + "')");
					m_pst.execute();
				}
			} catch (Exception sqex) {
				cat.error("TDSException MessageDAO.storeMessageToDBForADriver--->"+sqex.getMessage());
				//System.out.println("TDSException MessageDAO.storeMessageToDBForADriver--->"+sqex.getMessage());
				sqex.printStackTrace();
			} finally{
				m_dbConn.closeConnection();
			}
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static void storeMessageToDB(String assocCode, ArrayList<DriverCabQueueBean> driverIDs,String messageText)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		int count = 0;
		ResultSet rs = null;
		PreparedStatement m_pst= null;
		DriverChargeBean db=new DriverChargeBean();
		AdminRegistrationBO adbo=new AdminRegistrationBO();
		String[] timeZone = new String[2];
		String[] textMessage = new String[2];
		String[] delayDate = new String[2];
		String[] delayTime = new String[2];
		String query;
		String timeNow=" ";
		long days=0;
		if(driverIDs!=null && driverIDs.size()>0){
			TDSConnection m_dbConn  = new TDSConnection();
			Connection m_conn = m_dbConn.getConnection();
			try
			{
				if(timeNow!=null && !timeNow.equals(" ")){
					days=calculateDays(timeZone[1],timeNow);
				}
				if(days<2)
				{
					messageText=messageText.replace("'", "");
					for(int i=0;i<driverIDs.size();i++){
						m_pst = m_conn.prepareStatement("SELECT MSG_MSGTEXT FROM TDS_DRIVER_MESSAGES WHERE MSG_ASSOCODE='"+assocCode+"' AND MSG_DRIVER_ID='"+driverIDs.get(i).getDriverid()+"'");
						rs = m_pst.executeQuery();
						if(rs.next()){
							String finalMessageText =rs.getString("MSG_MSGTEXT")+"^"+messageText;
							m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER_MESSAGES SET MSG_MSGTEXT='"+finalMessageText+"' WHERE MSG_ASSOCODE='"+assocCode+"' AND MSG_DRIVER_ID='"+driverIDs.get(i).getDriverid()+"'");
							m_pst.executeUpdate();
						} else {
							m_pst = m_conn.prepareStatement("INSERT INTO TDS_DRIVER_MESSAGES (MSG_ASSOCODE, MSG_DRIVER_ID,MSG_MSGTEXT,MSG_TIME) values ('" + assocCode + "','" +  driverIDs.get(i).getDriverid() + "','" + messageText + "',CONVERT_TZ(NOW(),'UTC','" + timeZone[1] + "'))");
							m_pst.execute();
						}
					}
				}
				//message History
			} catch (Exception sqex){
				System.out.println("Query For Null--->"+assocCode+":"+driverIDs.size()+":"+messageText);
				cat.error("TDSException MessageDAO.storeMessageToDB--->"+sqex.getMessage());
				//System.out.println("TDSException MessageDAO.storeMessageToDB--->"+sqex.getMessage());
				sqex.printStackTrace();
			} finally{
				m_dbConn.closeConnection();
			}
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	private static long calculateDays(String timeZone,String timeNow) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
		Date date1;
		long days=0;
		try {
			date1 = sdf.parse(sdf.format(date));
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date2=sdf.parse(timeNow);
			long diff=date2.getTime() - date1.getTime();
			days=diff/(24 * 60 * 60 * 1000);
			//System.out.println("The Given Time is ---->" + timeNow);
			//System.out.println("the Current time of"+ timeZone +"zone is -----> " + sdf.format(date));
			//System.out.println("No.of days -----> " + days);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return days;
	}
	public static void removeDriverMessage(String driverId,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("DELETE FROM TDS_DRIVER_MESSAGES WHERE MSG_DRIVER_ID='"+driverId+"' AND MSG_ASSOCODE='"+assoccode+"'");
			pst.execute();
		}catch(Exception sqex){
			cat.error("TDSException MessageDAO.removeDriverMessage--->"+sqex.getMessage());
			//System.out.println("TDSException MessageDAO.removeDriverMessage--->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}


	public static ArrayList<MessageBeen> getMessagelist(String assocCode,String userName,String fromDate,String toDate,String timeZone){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		ResultSet rs = null;
		PreparedStatement m_pst= null;
		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		String query;

		ArrayList<MessageBeen> array=new ArrayList<MessageBeen>();
		try{
			if(fromDate!=null && toDate!=null)
			{
				String fDate[] = fromDate.split("/");
				String tDate[] = toDate.split("/");
				fromDate =fDate[2]+"-"+fDate[0]+"-"+fDate[1]+" "+"00:00:00";
				toDate =tDate[2]+"-"+tDate[0]+"-"+tDate[1]+" "+"00:00:00";
				query="SELECT *,DATE_FORMAT(CONVERT_TZ(TM_TIME,'UTC','"+timeZone+"') ,'%m/%d/%Y %h:%i %p') AS DATETIME FROM TDS_TEXT_MESSAGE WHERE TM_ASSOCIATION_CODE='"+assocCode+"' and CAST(TM_TIME as Date) >= CONVERT_TZ('"+fromDate+"','"+timeZone+"','UTC') and CAST(TM_TIME as Date) <= CONVERT_TZ('"+toDate+"','"+timeZone+"','UTC') ORDER BY TM_TIME DESC";
			}else
			{
				query="SELECT *,DATE_FORMAT(CONVERT_TZ(TM_TIME,'UTC','"+timeZone+"') ,'%m/%d/%Y %h:%i %p') AS DATETIME FROM TDS_TEXT_MESSAGE WHERE TM_ASSOCIATION_CODE='"+assocCode+"' ORDER BY TM_TIME DESC";
			}
			m_pst = m_conn.prepareStatement(query);
			rs = m_pst.executeQuery();
			while(rs.next())
			{
				MessageBeen obj=new MessageBeen();
				obj.setMessageId(rs.getInt("TM_KEY"));
				obj.setSentDate(rs.getString("DATETIME"));
				obj.setUserName(rs.getString("TM_ENTERED_BY"));
				obj.setMessage(rs.getString("TM_MESSAGE"));
				obj.setDriverId(rs.getString("TM_DRIVER_ID"));
				array.add(obj);
			}
		}catch(Exception sqex){
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return array;
	} 

	public static ArrayList<MessageBeen> getDriverList(String assocCode,String msgId,String driverId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		ResultSet rs = null;
		PreparedStatement m_pst= null;
		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		String query;
		ArrayList<MessageBeen> array =new ArrayList<MessageBeen>();
		try{
			if(driverId!=null)
			{
				query="SELECT * FROM TDS_MESSAGE_DRIVER_LIST WHERE MD_ASSOCODE='"+assocCode+"'and MD_MSGID='"+msgId+"' and MD_DRIVERID='"+driverId+"'";
			}else
			{
				query="SELECT * FROM TDS_MESSAGE_DRIVER_LIST WHERE MD_ASSOCODE='"+assocCode+"'and MD_MSGID='"+msgId+"'";
			}
			m_pst = m_conn.prepareStatement(query);
			rs = m_pst.executeQuery();
			while(rs.next())
			{
				MessageBeen obj=new MessageBeen();
				obj.setDriverId(rs.getString("MD_DRIVERID"));
				obj.setMsgId(rs.getString("MD_MSGID"));
				obj.setSentDate(rs.getString("MD_SENTTIME"));
				obj.setResDate(rs.getString("MD_RESPONSETIME"));
				array.add(obj);
			}
		}catch(Exception sqex){
			sqex.printStackTrace();
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return array;
	}
	public static void setMessageAck(String driverId, String msgId,String timeZone) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		PreparedStatement m_pst= null;
		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("UPDATE TDS_MESSAGE_DRIVER_LIST SET MD_RESPONSETIME=CONVERT_TZ(NOW(),'"+timeZone+"','UTC') WHERE MD_DRIVERID='"+driverId+"' AND MD_MSGID='"+msgId+"'");
			m_pst.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static ArrayList<MessageBeen> getDelayMessage(String userId, String timeZone) throws JSONException {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		ResultSet rs = null;
		PreparedStatement m_pst= null;
		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		//		JSONArray array=new JSONArray();
		ArrayList<MessageBeen> array=new ArrayList<MessageBeen>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String query="SELECT * FROM TDS_MESSAGE_HISTORY WHERE MH_MSGID IN (SELECT MD_MSGID FROM TDS_MESSAGE_DRIVER_LIST WHERE MD_DRIVERID='"+userId+"') AND (CAST(MH_SENTDATE as Date) BETWEEN CONVERT_TZ(NOW(),'"+timeZone+"','UTC') AND (SELECT DATE_ADD(CONVERT_TZ(NOW(),'"+timeZone+"','UTC'), INTERVAL 48/1 HOUR_MINUTE)))";

		try {
			m_pst = m_conn.prepareStatement(query);
			rs = m_pst.executeQuery();
			while(rs.next())
			{
				/*JSONObject obj=new JSONObject();
				obj.put("msg",rs.getString("MH_MESSAGE"));	
				obj.put("msgId", rs.getString("MH_MSGID"));*/
				MessageBeen obj=new MessageBeen();
				obj.setMessage(rs.getString("MH_MESSAGE"));
				obj.setMsgId(rs.getString("MH_MSGID"));
				Date date;
				try {
					date = sdf.parse(rs.getString("MH_SENTDATE"));
					sdf.setTimeZone(TimeZone.getTimeZone(timeZone));
					String deliverTime=sdf.format(date);
					//					obj.put("deliverTime", deliverTime);
					obj.setDeliverTime(deliverTime);
					array.add(obj);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return array;
	}
}

