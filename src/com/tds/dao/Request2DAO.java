package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;

public class Request2DAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(Request2DAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+Request2DAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static ArrayList<OpenRequestBO> getTripForSplFlag(String splFlag,String date,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getTripForSplFlag");
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<OpenRequestBO> orList = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String query="SELECT *,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i') AS OR_SERVICEDATE_FORMATTED FROM TDS_OPENREQUEST WHERE  OR_ASSOCCODE="+adminBO.getAssociateCode()+" AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') ='"+date+"' AND OR_DRCABFLAG like '%"+splFlag+"%'";
		/*for (int i = 0;i < splFlag.length(); i++){
		    System.out.println(splFlag.charAt(i));
		    query=query+" OR_DRCABFLAG like '%"+splFlag.charAt(i)+"%' ";
		    if(i!=splFlag.length()-1){
		    	query=query+" OR ";
		    }
		}
		query=query+")";
*/		try {
			pst = con.prepareStatement(query);
			cat.info("query--->"+pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				OpenRequestBO orBo  = new OpenRequestBO();
				orBo.setTripid(rs.getString("OR_TRIP_ID"));
				orBo.setSadd1(rs.getString("OR_STADD1"));
				orBo.setSadd2(rs.getString("OR_STADD2"));
				orBo.setScity(rs.getString("OR_STCITY"));
				orBo.setEadd1(rs.getString("OR_EDADD1"));
				orBo.setEadd2(rs.getString("OR_EDADD2"));
				orBo.setEcity(rs.getString("OR_EDCITY"));
				orBo.setSdate(rs.getString("OR_SERVICEDATE_FORMATTED"));
				orList.add(orBo);
			}
		} catch(SQLException sqex) {
			cat.error("TDSException Request2DAO.getTripForSplFlag-->"+sqex.getMessage());
			//System.out.println("TDSException Request2DAO.getTripForSplFlag-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
        cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return orList;
	}



}
