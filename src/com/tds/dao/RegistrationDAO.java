package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Category;
import org.apache.woden.wsdl20.Description;
import org.w3c.flute.parser.selectors.PseudoClassConditionImpl;

import com.tds.cmp.bean.Address;
import com.tds.cmp.bean.CabDriverMappingBean;
import com.tds.cmp.bean.CabRegistrationBean;
import com.tds.cmp.bean.CmpPaySeteledBean;
import com.tds.cmp.bean.CompanyFlagBean;
import com.tds.cmp.bean.CustomerProfile;
import com.tds.cmp.bean.DisbursementBean;
import com.tds.cmp.bean.DriverAndJobs;
import com.tds.cmp.bean.DriverChargeBean;
import com.tds.cmp.bean.DriverDisbrusementSubBean;
import com.tds.cmp.bean.DriverDisbursment;
import com.tds.cmp.bean.FutureJobs;
import com.tds.cmp.bean.LoginAttempts;
import com.tds.cmp.bean.OpenRequestHistory;
import com.tds.cmp.bean.PenalityBean;
import com.tds.controller.SystemUnavailableException;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.process.OnetoOneDirect;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.CabMaintenanceBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.DriverBehaviorBO;
import com.tds.tdsBO.DriverLocationHistoryBO;
import com.tds.tdsBO.DriverRegistrationBO;
import com.tds.tdsBO.LostandFoundBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.tds.tdsBO.VoucherBO;
import com.tds.tdsBO.ZoneFlagBean;
import com.tds.tdsBO.passengerBO;
import com.common.util.TDSConstants;
import com.tds.util.TDSSQLConstants;
import com.common.util.TDSValidation;
@SuppressWarnings("deprecation")
public class RegistrationDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(RegistrationDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+RegistrationDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static ArrayList getCmpyVehicleFlag(String assocode,String vehicle)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RegistrationDAO.getCmpyVehicleFlag");
		//CompanyFlagBean cmFlagBean = new CompanyFlagBean();
		//TDS_CMPY_FLAG_SETUP
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ArrayList al_list = new ArrayList();
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();


		try {


			m_pst = m_conn.prepareStatement("select * from TDS_CMPY_FLAG_SETUP where CS_FLAG_SW='"+vehicle+"' and CS_ASSOCODE='"+assocode+"'");
			cat.info(m_pst.toString());

			ResultSet rs = m_pst.executeQuery();
			//cmFlagBean.setAssocode(assocode);
			while(rs.next()){
				if(!rs.getString("CS_FLAG").equals("")){
					al_list.add(rs.getString("CS_FLAG_LNG_DESC"));
					al_list.add(rs.getString("CS_FLAG_VALUE"));
					al_list.add(rs.getString("CS_GRP"));
				}
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getCmpyVehicleFlag-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getCmpyVehicleFlag-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static ArrayList<OpenRequestBO> getpreviousDetails(String phoneno,AdminRegistrationBO adminBo)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RegistrationDAO.getpreviousDetails");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>(); 
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			m_pst = m_conn.prepareStatement("select OR_CALLER_PHONE,OR_CALLER_NAME,OR_TRIP_ID,OR_RATINGS,OR_ASSOCCODE,OR_VEHICLE_NO,OR_TRIP_STATUS,OR_QUEUENO,OR_DRCABFLAG,OR_SMS_SENT,OR_DRIVERID,OR_DISPATCH_HISTORY_DRIVERS,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_SPLINS,OR_PAYACC,OR_PAYTYPE,"+
					"OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EMAIL,OR_TRIP_STATUS,OR_DISPATCH_LEAD_TIME,OR_ELANDMARK,OR_PREMIUM_CUSTOMER,OR_SHARED_RIDE,OR_NUMBER_OF_PASSENGERS,OR_DROP_TIME,OR_EDSTATE,OR_PAYACC,OR_QUEUENO,OR_REPEAT_GROUP,OR_PAYTYPE,OR_DRCABFLAG,OR_NAME,OR_COMMENTS,OR_DRIVERID,OR_LANDMARK,OR_METER_TYPE,OR_EDZIP,OR_STLATITUDE,OR_STLONGITUDE,OR_EDLATITUDE,OR_EDLONGITUDE,"+"DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%H%i') "+"as TIME, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"') AS OFFSET_DATETIME,OR_AIRLINE_NAME,OR_AIRLINE_NO,OR_AIRLINE_FROM,OR_AIRLINE_TO,OR_CUST_REF_NUM,OR_CUST_REF_NUM1,OR_CUST_REF_NUM2,OR_CUST_REF_NUM3 from TDS_OPENREQUEST where OR_PHONE='"+phoneno+"' AND OR_MASTER_ASSOCCODE='"+adminBo.getMasterAssociateCode()+"' ORDER BY OR_SERVICEDATE DESC");
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{
				OpenRequestBO orBO= new OpenRequestBO();
				orBO.setTripid(rs.getString("OR_TRIP_ID"));
				orBO.setDriverid(rs.getString("OR_DRIVERID"));
				orBO.setPhone(rs.getString("OR_PHONE")); 
				orBO.setSadd1(rs.getString("OR_STADD1"));
				orBO.setSadd2(rs.getString("OR_STADD2"));
				orBO.setScity(rs.getString("OR_STCITY"));
				orBO.setSstate(rs.getString("OR_STSTATE"));
				orBO.setSzip(rs.getString("OR_STZIP"));
				orBO.setSpecialIns(rs.getString("OR_SPLINS")); 
				orBO.setEadd1(rs.getString("OR_EDADD1"));
				orBO.setEadd2(rs.getString("OR_EDADD2"));
				orBO.setEcity(rs.getString("OR_EDCITY"));
				orBO.setEstate(rs.getString("OR_EDSTATE"));
				orBO.setName(rs.getString("OR_NAME"));
				orBO.setEzip(rs.getString("OR_EDZIP"));
				orBO.setSlat(rs.getString("OR_STLATITUDE"));
				orBO.setSlong(rs.getString("OR_STLONGITUDE"));
				orBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				orBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				orBO.setComments(rs.getString("OR_COMMENTS"));
				orBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("DATE")));
				orBO.setSttime((rs.getString("TIME")));
				orBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				orBO.setSlandmark(rs.getString("OR_LANDMARK"));
				orBO.setPaytype((rs.getString("OR_PAYTYPE")));
				orBO.setAcct(rs.getString("OR_PAYACC"));
				orBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				orBO.setQueueno(rs.getString("OR_QUEUENO"));
				orBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
				orBO.setRepeatGroup(rs.getString("OR_REPEAT_GROUP"));
				orBO.setElandmark(rs.getString("OR_ELANDMARK"));
				orBO.setNumOfPassengers(rs.getInt("OR_NUMBER_OF_PASSENGERS"));
				orBO.setDropTime(rs.getString("OR_DROP_TIME"));
				orBO.setPremiumCustomer(rs.getInt("OR_PREMIUM_CUSTOMER"));
				orBO.setAdvanceTime(rs.getString("OR_DISPATCH_LEAD_TIME"));
				orBO.setEmail(rs.getString("OR_EMAIL"));
				orBO.setChckTripStatus(rs.getInt("OR_TRIP_STATUS"));
				orBO.setAssociateCode(rs.getString("OR_ASSOCCODE"));
				orBO.setJobRating(rs.getInt("OR_RATINGS"));
				orBO.setCallerPhone(rs.getString("OR_CALLER_PHONE"));
				orBO.setCallerName(rs.getString("OR_CALLER_NAME"));
				orBO.setPaymentMeter(rs.getInt("OR_METER_TYPE"));
				orBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
				orBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
				orBO.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
				orBO.setAirTo(rs.getString("OR_AIRLINE_TO"));
				orBO.setRefNumber(rs.getString("OR_CUST_REF_NUM"));
				orBO.setRefNumber1(rs.getString("OR_CUST_REF_NUM1"));
				orBO.setRefNumber2(rs.getString("OR_CUST_REF_NUM2"));
				orBO.setRefNumber3(rs.getString("OR_CUST_REF_NUM3"));
				al_list.add(orBO);
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getpreviousDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getpreviousDetails-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static boolean deleteFlag(String  flagKey,String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		TDSConnection dbCon = new TDSConnection();
		Connection con = dbCon.getConnection();
		boolean result=false;
		try{
			m_pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST WHERE OR_ASSOCCODE='"+assocode+"' AND OR_DRCABFLAG LIKE '%"+flagKey+"%'");
			rs=m_pst.executeQuery();
			if(rs.next()){
				return result;
			}
			m_pst=con.prepareStatement("SELECT * FROM TDS_DRIVER WHERE DR_ASSOCODE='"+assocode+"' AND DR_FLAG LIKE '%"+flagKey+"%'");
			rs=m_pst.executeQuery();
			if(rs.next()){
				return result;
			}
			m_pst=con.prepareStatement("SELECT * FROM TDS_VEHICLE WHERE V_ASSOCCODE='"+assocode+"' AND V_FLAG LIKE '%"+flagKey+"%'");
			rs=m_pst.executeQuery();
			if(rs.next()){
				return result;
			}
			m_pst =  con.prepareStatement("DELETE FROM TDS_CMPY_FLAG_SETUP WHERE CS_ASSOCODE='"+assocode+"' AND CS_FLAG_VALUE='"+flagKey+"'");
			m_pst.execute();
			result=true;
		} catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.deleteFlag-->"+sqex.getMessage());
			//System.out.println("TDSException RegistrationDAO.deleteFlag-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbCon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static boolean insertCmpyFlagSetup(ArrayList al_list,String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertCmpyFlagSetup");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		int result=0;
		boolean response = false;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();

		try {

			m_conn.setAutoCommit(false);
			m_pst = m_conn.prepareStatement("select * from TDS_CMPY_FLAG_SETUP where CS_ASSOCODE='"+assocode+"'");
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();

			if(rs.first())
			{
				m_pst =  m_conn.prepareStatement("delete from TDS_CMPY_FLAG_SETUP where CS_ASSOCODE='"+assocode+"'");
				cat.info(m_pst.toString());
				m_pst.execute();
			}

			m_pst = m_conn.prepareStatement("insert into TDS_CMPY_FLAG_SETUP (CS_ASSOCODE,CS_FLAG_VALUE,CS_FLAG,CS_FLAG_LNG_DESC,CS_FLAG_SW,CS_ENTERED_TIME, CS_GRP,CS_CUSAPP)values(?,?,?,?,?,now(),?,?)");

			for(int i=0;i<al_list.size();i++)
			{
				CompanyFlagBean cmpFlagBean = (CompanyFlagBean)al_list.get(i);
				if(!cmpFlagBean.getFlag1_value().equals(""))
				{
					m_pst.setString(1, cmpFlagBean.getAssocode());
					m_pst.setString(2, cmpFlagBean.getFlag1_value());
					m_pst.setString(3, cmpFlagBean.getFlag1());
					m_pst.setString(4, cmpFlagBean.getFlag1_lng_desc());
					m_pst.setString(5, cmpFlagBean.getFlag1_sw());
					m_pst.setString(6, cmpFlagBean.getFlag1_group());
					m_pst.setString(7,cmpFlagBean.getFlag1_cusap());
					cat.info(m_pst.toString());
					result=m_pst.executeUpdate();
				}
			}

			m_conn.commit();
			m_conn.setAutoCommit(true);
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.insertCmpyFlagSetup-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertCmpyFlagSetup-->"+sqex.getMessage());
			sqex.printStackTrace();

			try
			{
				m_conn.rollback();
			} catch (Exception e1) {
				// TODO: handle exception
			}
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		if(result>0){
			response = true;
		}
		return response;
	}

	public static ArrayList getCmpyFlag(String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCmpyFlag");
		ArrayList al_list = new ArrayList();
		//TDS_CMPY_FLAG_SETUP
		//TDSConnection m_dbConn = null;
		//Connection m_conn = null;
		PreparedStatement m_pst = null;
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();

		try {

			m_pst = m_conn.prepareStatement("select * from TDS_CMPY_FLAG_SETUP where CS_ASSOCODE='"+assocode+"' order by CS_FLAG_VALUE,CS_GRP");
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();

			while(rs.next())
			{
				CompanyFlagBean cmFlagBean = new CompanyFlagBean();	
				cmFlagBean.setFlag1(rs.getString("CS_FLAG"));
				cmFlagBean.setFlag1_sw(rs.getString("CS_FLAG_SW"));
				cmFlagBean.setFlag1_value(rs.getString("CS_FLAG_VALUE"));
				cmFlagBean.setFlag1_lng_desc(rs.getString("CS_FLAG_LNG_DESC"));
				cmFlagBean.setFlag1_group(rs.getString("CS_GRP"));
				cmFlagBean.setFlag1_cusap(rs.getString("CS_CUSAPP"));
				al_list.add(cmFlagBean);
			}


		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getCmpyFlag-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getCmpyFlag-->"+sqex.getMessage());
			sqex.printStackTrace();


		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}


	public static ArrayList getFromandToAddress(String phoneno, String associationCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getFromandToAddress");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs=null;
		ArrayList al_list = new ArrayList();
		if(phoneno.equals("")){
			return al_list;
		}
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		
		try {
			
			m_pst = m_conn.prepareStatement("SELECT * FROM TDS_USERADDRESS as a,TDS_USERADDRESSMASTER as b,TDS_USER_PHONE as c where b.CU_MASTERASSOCIATIONCODE = '"+associationCode+"' and a.CU_MASTERASSOCIATIONCODE=b.CU_MASTERASSOCIATIONCODE and a.cu_master_key=b.cu_master_key and a.cu_master_key=c.UP_CUSTOMER_NUMBER AND c.UP_PHONE_NUMBER='"+phoneno+"' order by cu_updatedtime desc LIMIT 0,10");
			//System.out.println("query1:"+m_pst.toString());
			rs = m_pst.executeQuery();
			while(rs.next())
			{
				OpenRequestBO orBO= new OpenRequestBO();
				orBO.setMasterAddressKey(rs.getString("CU_MASTER_KEY"));
				orBO.setAcct(rs.getString("CU_ACCOUNT"));
				orBO.setUserAddressKey(rs.getString("CU_ADDRESS_KEY"));
				orBO.setSadd1(rs.getString("a.CU_ADD1"));
				orBO.setSadd2(rs.getString("a.CU_ADD2"));
				orBO.setScity(rs.getString("a.CU_CITY"));
				orBO.setSstate(rs.getString("a.CU_STATE"));
				orBO.setSzip(rs.getString("CU_ZIP"));
				orBO.setSlat(rs.getString("CU_LATITUDE"));
				orBO.setSlong(rs.getString("CU_LONGITUDE"));
				orBO.setPaytype(rs.getString("CU_PMTTYPE"));
				orBO.setComments(rs.getString("CU_COMMENTS"));
				orBO.setName(rs.getString("b.CU_NAME"));
				orBO.setSpecialIns(rs.getString("b.CU_OPERATOR_COMMENTS"));
				orBO.setPremiumCustomer(rs.getInt("b.PREMIUMCUSTOMER"));
				orBO.setEmail(rs.getString("b.CU_EMAIL"));
				orBO.setAdvanceTime(rs.getString("b.CU_ADVANCE_TIME"));
				orBO.setPhone(phoneno);
				orBO.setDrProfile(rs.getString("b.CU_FLAG"));
				al_list.add(orBO);
			}
			
			if(al_list.size()<=0){
				m_pst = m_conn.prepareStatement("SELECT * FROM TDS_USERADDRESSMASTER AS a,TDS_USER_PHONE AS c WHERE a.CU_MASTERASSOCIATIONCODE = '"+associationCode+"' AND a.cu_master_key=c.UP_CUSTOMER_NUMBER AND c.UP_PHONE_NUMBER='"+phoneno+"'");
				//System.out.println("query2:"+m_pst.toString());
				rs = m_pst.executeQuery();
				while(rs.next())
				{
					OpenRequestBO orBO= new OpenRequestBO();
					orBO.setMasterAddressKey(rs.getString("CU_MASTER_KEY"));
					orBO.setAcct(rs.getString("CU_ACCOUNT"));
					orBO.setSadd1(rs.getString("CU_ADD1"));
					orBO.setSadd2(rs.getString("CU_ADD2"));
					orBO.setScity(rs.getString("CU_CITY"));
					orBO.setSstate(rs.getString("CU_STATE"));
					orBO.setSzip(rs.getString("CU_ZIP"));
					orBO.setSlat(rs.getString("CU_LATITUDE"));
					orBO.setSlong(rs.getString("CU_LONGITUDE"));
					orBO.setPaytype(rs.getString("CU_PMTTYPE"));
					orBO.setComments(rs.getString("CU_COMMENTS"));
					orBO.setName(rs.getString("CU_NAME"));
					orBO.setSpecialIns(rs.getString("CU_OPERATOR_COMMENTS"));
					orBO.setPremiumCustomer(rs.getInt("PREMIUMCUSTOMER"));
					orBO.setEmail(rs.getString("CU_EMAIL"));
					orBO.setAdvanceTime(rs.getString("CU_ADVANCE_TIME"));
					orBO.setPhone(phoneno);
					orBO.setDrProfile(rs.getString("CU_FLAG"));
					al_list.add(orBO);
				}
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getFromandToAddress-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getFromandToAddress-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static int checkAsscode(String id,String code)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.checkAsscode");
		int result =0;

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			m_pst = m_conn.prepareStatement("SELECT * FROM TDS_DRIVER WHERE DR_USERID=? AND DR_ASSOCODE=?");
			m_pst.setString(1, id);
			m_pst.setString(2, code);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();

			if(rs.next()) {
				result =1; 
			} 

		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.checkAsscode-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.checkAsscode-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static String doUpdateVoucherStatus(String assocode,String costcenter)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.doUpdateVoucherStatus");
		String date="";

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();

		try {

			m_pst = m_conn.prepareStatement("select now() as date");
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();

			if(rs.first())
				date = rs.getString("date");

			m_pst = m_conn.prepareStatement("update TDS_VOUCHER_DETAIL set VD_PAY_SEND_DATE=? ,VD_PAYMENT_RECEIVED_STATUS=2 where VD_PAYMENT_RECEIVED_STATUS=0 and VD_ASSOC_CODE=? and VD_COSTCENTER=?");
			m_pst.setString(1, date);
			m_pst.setString(2, assocode);
			m_pst.setString(3, costcenter);
			cat.info(m_pst.toString());
			m_pst.execute();

		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.doUpdateVoucherStatus-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.doUpdateVoucherStatus-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return date;
	}
	public static void destorySession()
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.destorySession");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("update TDS_LOGINDETAILS set LO_LOGOUT = now() where LO_LOGOUT = '1970-01-01 00:00:00'");
			cat.info(m_pst.toString());
			m_pst.execute();
		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.destorySession-->"+sqex.getMessage());
			//System.out.println("TDSException RegistrationDAO.destorySession-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static int getActiveUser()
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getActiveUser");
		int activeCount = 0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("select count(*) from TDS_LOGINDETAILS where LO_LOGOUT = '1970-01-01 00:00:00'");
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();
			if(rs.first())
				activeCount = rs.getInt(1);

		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getActiveUser-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getActiveUser-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return activeCount;
	}


	public static void updateCabRegistration(CabRegistrationBean registrationBean ){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateCabRegistration");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int status =0;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		try {
			m_pst = m_conn.prepareStatement("update TDS_VEHICLE set V_VNO = ?,V_VMAKE=?,V_VYEAR=?,V_RCNO=?,V_FLAG=?,V_STATUS=?,V_VINNO=?,V_MODEL=?,V_VTYPE=?,V_REGDATE=?,V_EXPDATE=?,V_VMETER_RATE=?,V_PAY_ID=?,V_PAY_PASS=? where V_VKEY = ? and V_ASSOCCODE=?");
			m_pst.setString(1, registrationBean.getCab_cabno());
			m_pst.setString(2, registrationBean.getCab_cabmake());
			m_pst.setString(3, registrationBean.getCab_cabYear());
			m_pst.setString(4, registrationBean.getCab_rcno());
			m_pst.setString(5, registrationBean.getVehicleProfile());
			m_pst.setString(6, registrationBean.getStatus());
			m_pst.setString(7, registrationBean.getCab_vinno());
			m_pst.setString(8, registrationBean.getCab_model());
			m_pst.setString(9, registrationBean.getCab_vtype());
			m_pst.setString(10, registrationBean.getCab_registrationDate());
			m_pst.setString(11, registrationBean.getCab_expiryDate());
			m_pst.setString(12, registrationBean.getCab_meterRate());
			m_pst.setString(13, registrationBean.getPayId()); 
			m_pst.setString(14, registrationBean.getPayPass());
			m_pst.setString(15, registrationBean.getCab_cabkey()); 
			m_pst.setString(16, registrationBean.getCab_assocode());
			cat.info(m_pst.toString());
			m_pst.execute();

			m_pst =  m_conn.prepareStatement("DELETE FROM TDS_ODOMETER WHERE OD_CAB_NO ='"+registrationBean.getCab_cabno()+"' AND OD_ASSOCCODE ='"+registrationBean.getCab_assocode()+"'");
			m_pst.execute();

			m_pst = m_conn.prepareStatement("INSERT INTO  TDS_ODOMETER(OD_VALUE,OD_UPDATE_TIME,OD_TRIP_ID,OD_DRIVERID,OD_CAB_NO,OD_EVENT,OD_ASSOCCODE) VALUES(?,NOW(),'','',?,'REGISTER',?)");
			m_pst.setString(1, registrationBean.getOdoMeterValue());
			m_pst.setString(2, registrationBean.getCab_cabno());
			m_pst.setString(3, registrationBean.getCab_assocode());
			cat.info(m_pst.toString());
			m_pst.execute();

		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.updateCabRegistration-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.updateCabRegistration-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));


	}



	public static int cabMapRecordLength(String Assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.cabMapRecordLength");
		int trl = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("select count(*) as cnt from TDS_CABDRIVERMAPPING where CM_ASSOCCODE=?  group by CM_ASSOCCODE");
			pst.setString(1, Assocode);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.first()){
				trl= rs.getInt(1);
			}
		}
		catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.cabMapRecordLength-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.cabMapRecordLength-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return trl;
	}

	public static ArrayList getCabMappingDetails(String driverId,String cabNo,String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCabMappingDetails");
		ArrayList al_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			String query ="SELECT *,GET_DNAME(CM_DRIVERID) as Driver_name,GET_DNAME(CM_OPRID) as opr  from TDS_CABDRIVERMAPPING where CM_ASSOCCODE = '"+assocode+"' ";

			if(!driverId.equals("")) {
				query  = query+"and   CM_DRIVERID=  '"+driverId+"' ";
			}
			if(!cabNo.equals("")) {
				query  = query+"and   CM_CABNO=  '"+cabNo+"' ";

			}    
			m_pst = m_conn.prepareStatement(query); 
			rs = m_pst.executeQuery();

			while(rs.next())
			{
				CabDriverMappingBean driverMappingBean = new CabDriverMappingBean();
				driverMappingBean.setAssoccode(rs.getString("CM_ASSOCCODE"));
				driverMappingBean.setCab_no(rs.getString("CM_CABNO"));
				driverMappingBean.setFrom_date(TDSValidation.getUserDateFormat(rs.getString("CM_FROMDATE")));
				driverMappingBean.setTo_date(rs.getString("CM_TODATE").equals("")?"":TDSValidation.getUserDateFormat(rs.getString("CM_TODATE")));
				driverMappingBean.setOpr_id(rs.getString("opr"));
				driverMappingBean.setDriver_id(rs.getString("Driver_name"));
				driverMappingBean.setUserKey(rs.getString("CM_UID"));
				al_list.add(driverMappingBean);
			}

		} catch (Exception sqex) {
			cat.error("TDSException RegistrationDAO.getCabMappingDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getCabMappingDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static ArrayList getCabDriverMapping(String start,String end,String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCabDriverMapping");
		ArrayList al_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			m_pst = m_conn.prepareStatement("select *,GET_DNAME(CM_DRIVERID) as Driver_name,GET_DNAME(CM_OPRID) as opr  from TDS_CABDRIVERMAPPING where CM_ASSOCCODE = '"+assocode+"' order by CM_CURRENT desc limit ?,? ");
			m_pst.setInt(1, Integer.parseInt(start));
			m_pst.setInt(2, Integer.parseInt(end));
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();

			while(rs.next())
			{
				CabDriverMappingBean driverMappingBean = new CabDriverMappingBean();
				driverMappingBean.setAssoccode(rs.getString("CM_ASSOCCODE"));
				driverMappingBean.setCab_no(rs.getString("CM_CABNO"));
				driverMappingBean.setFrom_date(TDSValidation.getUserDateFormat(rs.getString("CM_FROMDATE")));
				driverMappingBean.setTo_date(rs.getString("CM_TODATE").equals("")?"":TDSValidation.getUserDateFormat(rs.getString("CM_TODATE")));
				driverMappingBean.setOpr_id(rs.getString("opr"));
				driverMappingBean.setDriver_id(rs.getString("Driver_name"));
				al_list.add(driverMappingBean);
			}

		} catch (Exception sqex) {
			cat.error("TDSException RegistrationDAO.getCabDriverMapping-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getCabDriverMapping-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static void insertCabDriverMapping(ArrayList al_list,String masterAssoccode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertCabDriverMapping");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			m_conn.setAutoCommit(false);

			for(int i=0;i<al_list.size();i++)
			{
				CabDriverMappingBean driverMappingBean = (CabDriverMappingBean)al_list.get(i);

				/*m_pst = m_conn.prepareStatement("select * from TDS_CABDRIVERMAPPING where CM_DRIVERID = ? and CM_ASSOCCODE = ? and CM_STATUS =1 " );
				m_pst.setString(1, driverMappingBean.getDriver_id());
				m_pst.setString(2, driverMappingBean.getAssoccode());
				rs = m_pst.executeQuery();

				if(rs.first())
				{
					m_pst = m_conn.prepareStatement("update TDS_CABDRIVERMAPPING set CM_STATUS = 2 ,CM_TODATE = ? where CM_DRIVERID = ? and CM_ASSOCCODE = ? ");
					m_pst.setString(1, TDSValidation.getDBdateFormat(driverMappingBean.getFrom_date()));
					m_pst.setString(2, driverMappingBean.getDriver_id());
					m_pst.setString(3, driverMappingBean.getAssoccode());
					m_pst.execute();
				}*/

				m_pst = m_conn.prepareStatement("insert into TDS_CABDRIVERMAPPING (CM_DRIVERID,CM_ASSOCCODE,CM_CABNO,CM_FROMDATE,CM_TODATE,CM_CURRENT,CM_OPRID,CM_MASTERASSOCCODE) values (?,?,?,?,?,now(),?,?)");
				m_pst.setString(1, driverMappingBean.getDriver_id());
				m_pst.setString(2, driverMappingBean.getAssoccode());
				m_pst.setString(3, driverMappingBean.getCab_no());
				m_pst.setString(4, TDSValidation.getDBdateFormat(driverMappingBean.getFrom_date()));
				m_pst.setString(5, driverMappingBean.getTo_date().equals("")?"":TDSValidation.getDBdateFormat(driverMappingBean.getTo_date()));
				m_pst.setString(6, driverMappingBean.getOpr_id());
				m_pst.setString(7, masterAssoccode);
				cat.info(m_pst.toString());
				m_pst.execute();

			}
			m_conn.commit();
			m_conn.setAutoCommit(true);
		} catch (Exception sqex)
		{
			try
			{
				m_conn.rollback();
			} catch (Exception e1) {
				// TODO: handle exception
			}
			cat.error("TDSException RegistrationDAO.insertCabDriverMapping-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.insertCabDriverMapping-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}


	public static int updatePassenger(passengerBO passenger )
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updatePassenger");
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		int result =0;

		try {
			dbConn = new TDSConnection();
			conn = dbConn.getConnection();

			pst = conn.prepareStatement("UPDATE TDS_PASSENGER SET PAS_FNAME=?,PAS_LNAME=?,PAS_PHNO=?,PAS_LOGIN_ID=?,PAS_PWD=?,PAS_ADD1=?,PAS_ADD2=?,PAS_CITY=?,PAS_STATE=?,PAS_PAYMENT_TYPE=?,PAS_PAYMENT_AC=?, PAS_ZIP=? WHERE PAS_KEY=?");

			pst.setString(1, passenger.getFname());
			pst.setString(2, passenger.getLname());
			pst.setString(3, passenger.getPhone());
			pst.setString(4, passenger.getUid());
			pst.setString(5, passenger.getPassword());
			pst.setString(6, passenger.getAdd1());
			pst.setString(7, passenger.getAdd2());
			pst.setString(8, passenger.getCity());
			pst.setString(9, passenger.getState()); 
			pst.setString(10, Character.toString(passenger.getPayment_type()));
			pst.setString(11, passenger.getPayment_ac()); 
			pst.setString(12, passenger.getZip());
			pst.setString(13, ""+passenger.getPass_key());
			cat.info(pst.toString());
			result = pst.executeUpdate();

		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.updatePassenger-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.updatePassenger-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;

	}
	public static int insertPassenger(passengerBO passenger )
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertPassenger");
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		int result =0;

		try {
			dbConn = new TDSConnection();
			conn = dbConn.getConnection();

			pst = conn.prepareStatement("insert TDS_PASSENGER(PAS_KEY,PAS_FNAME,PAS_LNAME,PAS_PHNO,PAS_LOGIN_ID,PAS_PWD,PAS_ADD1,PAS_ADD2,PAS_CITY,PAS_STATE,PAS_PAYMENT_TYPE,PAS_PAYMENT_AC,PAS_ZIP) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");
			pst.setInt(1, ConfigDAO.tdsKeyGen("TDS_PASSENGER_KEY", conn));
			pst.setString(2, passenger.getFname());
			pst.setString(3, passenger.getLname());
			pst.setString(4, passenger.getPhone());
			pst.setString(5, passenger.getUid());
			pst.setString(6, passenger.getPassword());
			pst.setString(7, passenger.getAdd1());
			pst.setString(8, passenger.getAdd2());
			pst.setString(9, passenger.getCity());
			pst.setString(10, passenger.getState());
			pst.setString(11, Character.toString(passenger.getPayment_type()));
			pst.setString(12, passenger.getPayment_ac());
			pst.setString(13, passenger.getZip());
			cat.info(pst.toString());
			result = pst.executeUpdate();

		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.insertPassenger-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.insertPassenger-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;

	}


	public static void updatedisMessage(String stdate,String endDate,String shortdesc,String longDesc,String asscode )
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updatedisMessage");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null; 
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			m_pst = m_conn.prepareStatement("insert TDS_DISBURSHMENT_MESSAGE(DM_ASSCCODE,DM_STARTDATE,DM_ENDDATE,DM_SHORTDESC,DM_LONGDESC ) values (?,?,?,?,?)");
			m_pst.setString(1, asscode);
			m_pst.setString(2, TDSValidation.getDBdateFormat(stdate));
			m_pst.setString(3, TDSValidation.getDBdateFormat(endDate));
			m_pst.setString(4, shortdesc);
			m_pst.setString(5, longDesc); 
			cat.info(m_pst.toString());
			m_pst.execute(); 
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.updatedisMessage-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.updatedisMessage-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	} 


	public static int updateLogoutDriver(String driver_id,String asscode  )
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateLogoutDriver");
		ArrayList ar_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs,rs2 = null;
		int rsult1=0;
		int result=0;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 

			m_pst = m_conn.prepareStatement("UPDATE  TDS_LOGINDETAILS  SET LO_Logout=NOW() WHERE  LO_DRIVERID= ? and LO_ASSCODE=  ?");
			m_pst.setString(1,  driver_id);
			m_pst.setString(2,  asscode);  
			cat.info(m_pst.toString());
			m_pst.executeUpdate();

			m_pst = m_conn.prepareStatement("INSERT INTO TDS_LOGIN_HISTORY (SELECT * FROM TDS_LOGINDETAILS WHERE  LO_DRIVERID= ? and LO_ASSCODE=  ?)");
			m_pst.setString(1,  driver_id);
			m_pst.setString(2,  asscode);  
			cat.info(m_pst.toString());
			m_pst.executeUpdate();


			m_pst = m_conn.prepareStatement("DELETE FROM TDS_LOGINDETAILS  WHERE  LO_DRIVERID= ? and LO_ASSCODE=  ?");
			m_pst.setString(1,  driver_id);
			m_pst.setString(2,  asscode);
			cat.info(m_pst.toString());
			result = m_pst.executeUpdate();

			//result = 1;



		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.updateLogoutDriver-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.updateLogoutDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}


	public static int deleteMoveToHistory(String driver_id,String asscode  )
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.deleteMoveToHistory");
		ArrayList ar_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs,rs2 = null;
		int rsult1=0;
		int result=0;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			m_pst = m_conn.prepareStatement("SELECT * FROM TDS_LOGINDETAILS  WHERE  LO_DRIVERID= ? and LO_ASSCODE=  ?");
			m_pst.setString(1,  driver_id);
			m_pst.setString(2,  asscode); 
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();
			if(rs.next()) {  

				m_pst = m_conn.prepareStatement("insert into TDS_LOGIN_HISTORY (LH_DRIVERID,LH_ASSCODE,LH_SESSIONID,LH_LOGINTIME,LH_LOGOUT,LH_DPHONENO,LH_PROVIDER,LH_DRPROFILE) values(?,?,?,?,now(),?,?,?)");
				m_pst.setString(1, rs.getString("LO_DriverID"));
				m_pst.setString(2, rs.getString("LO_ASSCODE"));
				m_pst.setString(3, rs.getString("LO_SESSIONID"));
				m_pst.setString(4, rs.getString("LO_LOGINTIME"));
				m_pst.setString(5, rs.getString("LO_DPHONENO"));
				m_pst.setString(6, rs.getString("LO_PROVIDER"));
				m_pst.setString(7, rs.getString("LO_DRPROFILE"));
				cat.info(m_pst.toString());
				result = m_pst.executeUpdate(); 
			}

			m_pst = m_conn.prepareStatement("DELETE FROM TDS_LOGINDETAILS  WHERE  LO_DRIVERID= ? and LO_ASSCODE=  ?");
			m_pst.setString(1,  driver_id);
			m_pst.setString(2,  asscode);
			cat.info(m_pst.toString());
			rsult1 = m_pst.executeUpdate();

			if(rsult1>0) {
			}



		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.deleteMoveToHistory-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.deleteMoveToHistory-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}


	public static ArrayList getdriverlogdetails(String driver_id,String asscode,String cabno,int oprType,String timeZone)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getdriverlogdetails");
		ArrayList ar_list = new ArrayList();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		ResultSet rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			String query = "SELECT LO_DriverID,DATE_FORMAT(CONVERT_TZ(LO_LOGINTIME,'UTC','"+timeZone+"'),'%Y-%m-%d %h:%i %p') AS LO_LOGINTIME,case LO_Logout when '1970-01-01 00:00:00' then '' else LO_Logout end as LO_Logout,LO_PROVIDER,LO_DRPROFILE,LO_CABNO,DL_DRNAME FROM TDS_LOGINDETAILS  LEFT JOIN  TDS_DRIVERLOCATION ON LO_DriverID=DL_DRIVERID ";
			if( !driver_id.equals("") ) {
				query = query+"WHERE  LO_DriverID='"+driver_id+"' AND LO_ASSCODE='"+asscode+"' ";
			}else if(!cabno.equals("")) {
				query = query+"WHERE  LO_CABNO='"+cabno+"' AND LO_ASSCODE='"+asscode+"' "; 
			} 
			else {
				query = query+"WHERE LO_ASSCODE='"+asscode+"'"; 
			}
			if(oprType==0){
				query=query+" AND LO_VERSION IS NOT NULL AND LO_VERSION <> ''";
			}
			m_pst = m_conn.prepareStatement(query); 
			//System.out.println("Query-->"+m_pst);
			rs = m_pst.executeQuery();
			while(rs.next()) {
				ar_list.add(rs.getString("LO_DriverID"));
				ar_list.add(rs.getString("LO_LOGINTIME"));
				ar_list.add(rs.getString("LO_DRPROFILE"));
				ar_list.add(rs.getString("DL_DRNAME"));
				ar_list.add(rs.getString("LO_CABNO"));
			} 
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getdriverlogdetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getdriverlogdetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ar_list;
	}

	public static ArrayList getdriverlogHistorydetails(String driver_id,String asscode,String cabno,int oprType,String timeZone,int logoutDriver,String fromDate,String toDate,int type)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getdriverlogdetails");
		ArrayList ar_list = new ArrayList();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		ResultSet rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			String query = "SELECT LH_DRIVERID,DATE_FORMAT(CONVERT_TZ(LH_LOGINTIME,'UTC','"+timeZone+"'),'%Y-%m-%d %h:%i %p') AS LH_LOGINTIME,LH_DRPROFILE,LH_CABNO,DL_DRNAME,DATE_FORMAT(CONVERT_TZ(LH_LOGOUT,'UTC','"+timeZone+"'),'%Y-%m-%d %h:%i %p') AS LH_LOGOUT FROM TDS_LOGIN_HISTORY  LEFT JOIN  TDS_DRIVERLOCATION ON LH_DRIVERID=DL_DRIVERID ";
			if( !driver_id.equals("") ) {
				query = query+"WHERE  LH_DRIVERID='"+driver_id+"' AND LH_ASSCODE='"+asscode+"'";
			}else if(!cabno.equals("")) {
				query = query+"WHERE  LH_CABNO='"+cabno+"' AND LH_ASSCODE='"+asscode+"' "; 
			} 
			else {
				query = query+"WHERE LH_ASSCODE='"+asscode+"' "; 
			}

			if(oprType==0){
				//System.out.println("in type 0");
				query = query+"AND LH_CABNO!='' AND LH_CABNO IS NOT NULL ";
			}
			if(!fromDate.equals("") && !toDate.equals("")){

				if(type==1){
					//System.out.println("from date type 1");
					String fromDateFormated[] = fromDate.split("/");
					String toDateFormated[] = toDate.split("/");
					query=query+" AND DATE(LH_LOGINTIME) >= '"+fromDateFormated[2]+"-"+fromDateFormated[0]+"-"+fromDateFormated[1]+"' AND DATE(LH_LOGINTIME) <= '"+toDateFormated[2]+"-"+toDateFormated[0]+"-"+toDateFormated[1]+"' ";
				} else {
					//System.out.println("from date type 0");
					query= query+"AND LH_LOGINTIME >= '"+fromDate+"' AND LH_LOGINTIME <= '"+toDate+"' ";
				}
			}

			m_pst = m_conn.prepareStatement(query+" ORDER BY LH_LOGINTIME DESC limit 0,50"); 
			System.out.println("Query-->"+m_pst);
			rs = m_pst.executeQuery();
			while(rs.next()) {
				ar_list.add(rs.getString("LH_DRIVERID"));
				ar_list.add(rs.getString("LH_LOGINTIME"));
				ar_list.add(rs.getString("LH_DRPROFILE"));
				ar_list.add(rs.getString("DL_DRNAME"));
				ar_list.add(rs.getString("LH_CABNO"));
				ar_list.add(rs.getString("LH_LOGOUT"));

			} 
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getdriverlogdetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getdriverlogdetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ar_list;
	}

	public static ArrayList<OpenRequestBO> landMarkSummary(AdminRegistrationBO adminBO, OpenRequestBO openRequestBean,double latAbove,double latBelow,double lonAbove,double lonBelow){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.LandMarkSummary");
		ArrayList<OpenRequestBO> ar_list = new ArrayList<OpenRequestBO>();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		String query ="SELECT * FROM TDS_LANDMARK WHERE LM_ASSCODE='"+adminBO.getAssociateCode()+"'";

		if(!openRequestBean.getSlandmark().equals("")){
			query = query + " AND LM_NAME like '%"+openRequestBean.getSlandmark()+"%'"; 
		}
		if(!openRequestBean.getLandMarkKey().equals(""))
		{
			query = query + " AND LM_KEY='"+openRequestBean.getLandMarkKey()+"'"; 
		}
		if(!openRequestBean.getSadd1().equals(""))
		{
			query = query + " AND LM_ADD1 LIKE '"+"%"+"' "+openRequestBean.getSadd1()+" '"+"%"+"' ";
		}
		if(latAbove!=0)
		{
			query = query + " AND LM_LATITUDE BETWEEN '"+latBelow+"' AND '"+latAbove+"' ";
		}
		if(lonAbove!=0)
		{
			query = query + " AND LM_LONGITUDE BETWEEN '"+lonAbove+"' AND '"+lonBelow+"' ";
		}
		query=query+" ORDER BY LM_NAME";
		try {

			m_pst = m_conn.prepareStatement(query); 
			rs = m_pst.executeQuery();
			while(rs.next()) {
				OpenRequestBO openRequestBo = new OpenRequestBO();
				openRequestBo.setLandMarkKey(rs.getString("LM_KEY"));
				openRequestBo.setSlandmark(rs.getString("LM_NAME"));
				openRequestBo.setSadd1(rs.getString("LM_ADD1"));
				openRequestBo.setSadd2(rs.getString("LM_ADD2"));
				openRequestBo.setScity(rs.getString("LM_CITY"));
				openRequestBo.setSstate(rs.getString("LM_STATE"));
				openRequestBo.setSzip(rs.getString("LM_ZIP"));
				openRequestBo.setDontDispatch(rs.getInt("LM_DONTDISPATCH"));
				openRequestBo.setZoneNumber(rs.getString("LM_Zone"));
				openRequestBo.setSlat(rs.getString("LM_LATITUDE"));
				openRequestBo.setSlong(rs.getString("LM_LONGITUDE"));
				openRequestBo.setPhone(rs.getString("LM_PHONE"));
				ar_list.add(openRequestBo);
			} 

		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.LandMarkSummary-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.LandMarkSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ar_list;
	}



	public static int deleteLandMarkPending(AdminRegistrationBO adminBO,ArrayList<String> lmKeys){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.deleteLandMarkPending");
		int result=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String keys="";
		for (int i=0;i<lmKeys.size()-1;i++){
			keys= keys+"'"+lmKeys.get(i)+"'"+",";
		} 
		keys=keys+"'"+lmKeys.get(lmKeys.size()-1)+"'";
		String Query=" AND LP_SNO IN ("+keys+")";
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		try {
			if(lmKeys!=null && lmKeys.size()>0){
				m_pst=m_conn.prepareStatement("DELETE FROM TDS_LANDMARK_PENDING WHERE LP_ASSCODE="+adminBO.getAssociateCode()+Query);
				m_pst.executeUpdate();
				result=1;
			} 
		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.deleteLandMarkPending-->"+sqex.getMessage());
			//	System.out.println("TDSException RegistrationDAO.deleteLandMarkPending-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;

	}


	public static OpenRequestBO editLandMark(OpenRequestBO openRequestBean)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.editLandMark");
		OpenRequestBO openRequestBo = new OpenRequestBO();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			String query ="SELECT * FROM TDS_LANDMARK";

			if(!openRequestBean.getLandMarkKey().equals(""))
			{
				query = query + " where LM_KEY='"+openRequestBean.getLandMarkKey()+"'"; 
			}
			m_pst = m_conn.prepareStatement(query); 
			rs = m_pst.executeQuery();
			while(rs.next()) {

				openRequestBo.setLandMarkKey(rs.getString("LM_KEY"));
				openRequestBo.setSlandmark(rs.getString("LM_NAME"));
				openRequestBo.setSadd1(rs.getString("LM_ADD1"));
				openRequestBo.setSadd2(rs.getString("LM_ADD2"));
				openRequestBo.setScity(rs.getString("LM_CITY"));
				openRequestBo.setSstate(rs.getString("LM_STATE"));
				openRequestBo.setSzip(rs.getString("LM_ZIP"));
				openRequestBo.setDontDispatch(rs.getInt("LM_DONTDISPATCH"));
				openRequestBo.setZoneNumber(rs.getString("LM_Zone"));
				openRequestBo.setSlat(rs.getString("LM_LATITUDE"));
				openRequestBo.setSlong(rs.getString("LM_LONGITUDE"));
				openRequestBo.setPhone(rs.getString("LM_PHONE"));
				openRequestBo.setLandmarkKeyName(rs.getString("LM_KEY_NAME"));
				openRequestBo.setComments(rs.getString("LM_COMMENTS"));

			} 

		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.editLandMark-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.editLandMark-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openRequestBo;
	}

	public static void updateLandMark(OpenRequestBO openRequestBean, AdminRegistrationBO adminBO){    
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateLandMark");
		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		PreparedStatement m_pst = null; 
		int result =0;
		try {

			m_pst = m_conn.prepareStatement("UPDATE TDS_LANDMARK set LM_NAME =?,LM_ADD1 =?,LM_ADD2 =?,LM_CITY =?,LM_STATE =?,LM_ZIP =?,LM_LATITUDE =?,LM_LONGITUDE =?,LM_PHONE=?,LM_DONTDISPATCH=?,LM_KEY_NAME=?,LM_COMMENTS=? where LM_KEY =? and LM_ASSCODE =?");
			m_pst.setString(1, openRequestBean.getSlandmark());
			m_pst.setString(2, openRequestBean.getSadd1());
			m_pst.setString(3, openRequestBean.getSadd2());
			m_pst.setString(4, openRequestBean.getScity());
			m_pst.setString(5, openRequestBean.getSstate());
			m_pst.setString(6, openRequestBean.getSzip()); 
			m_pst.setString(7, openRequestBean.getSlat());
			m_pst.setString(8, openRequestBean.getSlong());
			m_pst.setString(9, openRequestBean.getPhone());
			m_pst.setInt(10, openRequestBean.getDontDispatch());
			m_pst.setString(11, openRequestBean.getLandmarkKeyName());
			m_pst.setString(12, openRequestBean.getComments());
			m_pst.setString(13, openRequestBean.getLandMarkKey());
			m_pst.setString(14, adminBO.getMasterAssociateCode());
			result = m_pst.executeUpdate();
			if(result == 1) {
			}
		} catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.updateLandMark-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.updateLandMark-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}



	public static int insertLandMark(String asscode,OpenRequestBO openRequestBean ){	
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertLandMark");
		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		PreparedStatement m_pst = null; 
		int result=0;
		try {
			m_pst = m_conn.prepareStatement("insert TDS_LANDMARK(LM_ASSCODE,LM_NAME,LM_ADD1,LM_ADD2,LM_CITY,LM_STATE,LM_ZIP,LM_LATITUDE,LM_LONGITUDE,LM_ADDVERIFIED,LM_DONTDISPATCH,LM_Zone,LM_PHONE,LM_KEY_NAME,LM_COMMENTS) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			//	m_pst.setInt(1, ConfigDAO.tdsKeyGen("TDS_LANDMARK_KEY", m_conn)); 
			m_pst.setString(1, asscode);
			m_pst.setString(2, openRequestBean.getSlandmark()); 
			m_pst.setString(3, openRequestBean.getSadd1());
			m_pst.setString(4, openRequestBean.getSadd2());
			m_pst.setString(5, openRequestBean.getScity());
			m_pst.setString(6, openRequestBean.getSstate());
			m_pst.setString(7, openRequestBean.getSzip()); 
			m_pst.setString(8, openRequestBean.getSlat());
			m_pst.setString(9, openRequestBean.getSlong()); 
			m_pst.setString(10, openRequestBean.getAddressVerified());
			m_pst.setInt(11,openRequestBean.getDontDispatch()); 
			m_pst.setString(12, openRequestBean.getZoneNumber());
			m_pst.setString(13, openRequestBean.getPhone());
			m_pst.setString(14, openRequestBean.getLandmarkKeyName());
			m_pst.setString(15, openRequestBean.getComments());
			result=m_pst.executeUpdate(); 

		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.insertLandMark-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.insertLandMark-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}


	public static void insertDisbursementMessage(String stdate,String endDate,String shortdesc,String longDesc,String asscode,String masterAssociationCode )
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertDisbursementMessage");

		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		PreparedStatement m_pst = null; 
		try {
			m_pst = m_conn.prepareStatement("insert TDS_DISBURSEMENT_MESSAGE(DM_ASSCCODE,DM_STARTDATE,DM_ENDDATE,DM_SHORTDESC,DM_LONGDESC,DM_MASTERASSOCCODE ) values (?,?,?,?,?,?)");
			m_pst.setString(1, asscode);
			m_pst.setString(2, TDSValidation.getDBdateFormat(stdate));
			m_pst.setString(3, TDSValidation.getDBdateFormat(endDate));
			m_pst.setString(4, shortdesc);
			m_pst.setString(5, longDesc);
			m_pst.setString(6, masterAssociationCode);
			cat.info(m_pst.toString());
			m_pst.execute(); 
		} catch (Exception e)
		{
			cat.error("TDSException RegistrationDAO.insertDisbursementMessage-->"+e.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.insertDisbursementMessage-->"+e.getMessage());
			e.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}	




	public static void updateDisMessage(String stdate,String endDate,String shortdesc,String longDesc,String asscode,String Num)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateDisMessage");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null; 
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			m_pst = m_conn.prepareStatement("update TDS_DISBURSEMENT_MESSAGE SET DM_STARTDATE=?,DM_ENDDATE=?,DM_SHORTDESC=?,DM_LONGDESC=? WHERE DM_MASTERASSCCODE=? AND DM_DIS_NUM=?");
			m_pst.setString(1, TDSValidation.getDBdateFormat(stdate));
			m_pst.setString(2, TDSValidation.getDBdateFormat(endDate));
			m_pst.setString(3, shortdesc);
			m_pst.setString(4, longDesc);
			m_pst.setString(5, asscode);
			m_pst.setString(6,Num);
			m_pst.execute(); 
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.updateDisMessage-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException RegistrationDAO.updateDisMessage-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	public static void updateDisbursementMessage(String stdate,String endDate,String shortdesc,String longDesc,String asscode,String disbursemtNumber)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateDisbursementMessage");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement pst = null; 

		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			pst=m_conn.prepareStatement("UPDATE TDS_DISBURSEMENT_MESSAGE SET DM_STARTDATE =?,DM_ENDDATE =?,DM_SHORTDESC =?,DM_LONGDESC =? where DM_DIS_NUM =? AND DM_ASSCCODE = ?");  
			pst.setString(1, TDSValidation.getDBdateFormat(stdate));
			pst.setString(2, TDSValidation.getDBdateFormat(endDate));
			pst.setString(3, shortdesc);
			pst.setString(4, longDesc); 
			pst.setString(5, disbursemtNumber);
			pst.setString(6, asscode); 
			cat.info(pst.toString());
			pst.executeUpdate(); 
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.updateDisbursementMessage-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.updateDisbursementMessage-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}


	public static ArrayList<DisbursementBean> getDisbursementMessages(DisbursementBean disbursementBean,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO TDSException.RegistrationDAO.editDisbursementMessage");
		ArrayList<DisbursementBean> disbursementSummary= new ArrayList<DisbursementBean>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String searchCrietria = "";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		if (!disbursementBean.getStartDate().equals("")) {
			searchCrietria = " AND  DM_STARTDATE > '" + TDSValidation.getDBdateFormat(disbursementBean.getStartDate()) + "'";
		}
		if (!disbursementBean.getEndDate().equals("")) {
			searchCrietria += " AND DM_ENDDATE < '" + TDSValidation.getDBdateFormat(disbursementBean.getEndDate())+ "'";
		}
		try {
			pst = con.prepareStatement("select DM_ASSCCODE,date_format(DM_STARTDATE,'%m/%d/%Y') as DM_STARTDATE,date_format(DM_ENDDATE,'%m/%d/%Y') as DM_ENDDATE,DM_SHORTDESC,DM_LONGDESC,DM_DIS_NUM  FROM TDS_DISBURSEMENT_MESSAGE WHERE DM_MASTERASSCCODE=? "+searchCrietria);
			pst.setString(1, adminBO.getMasterAssociateCode());
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				DisbursementBean dsBean = new DisbursementBean();
				dsBean.setAssoCode(rs.getString("DM_ASSCCODE"));
				dsBean.setShortDesc(rs.getString("DM_SHORTDESC"));
				dsBean.setStartDate(rs.getString("DM_STARTDATE"));
				dsBean.setEndDate(rs.getString("DM_ENDDATE"));
				dsBean.setDisNum(rs.getInt("DM_DIS_NUM"));
				dsBean.setLongDesc(rs.getString("DM_LONGDESC"));
				disbursementSummary.add(dsBean);
			}

		}catch (Exception sqex) {
			cat.error(("TDSException.RegistrationDAO.editDisbursementMessage-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException.RegistrationDAO.editDisbursementMessage-->"+ sqex.getMessage());
			sqex.printStackTrace();		
		} 
		finally {
			dbcon.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return disbursementSummary;
	}


	/*	public static void updateDisMessage(String stdate,String endDate,String shortdesc,String longDesc,String asscode,String Num)
	{
		cat.info("TDS INFO RegistrationDAO.updateDisMessage");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null; 
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			m_pst = m_conn.prepareStatement("update TDS_DISBURSEMENT_MESSAGE SET DM_STARTDATE=?,DM_ENDDATE=?,DM_SHORTDESC=?,DM_LONGDESC=? WHERE DM_ASSCCODE=? AND DM_DIS_NUM=?");
			m_pst.setString(1, TDSValidation.getDBdateFormat(stdate));
			m_pst.setString(2, TDSValidation.getDBdateFormat(endDate));
			m_pst.setString(3, shortdesc);
			m_pst.setString(4, longDesc);
			m_pst.setString(5, asscode);
			m_pst.setString(6,Num);
			m_pst.execute(); 
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.updateDisMessage-->"+sqex.getMessage());
			System.out.println("TDSException RegistrationDAO.updateDisMessage-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
	}
	 */

	public static int insertCabRegistration(CabRegistrationBean registrationBean,String masterAssoccode ){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO TDSException.RegistrationDAO.insertCabRegistration");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		int result = 0;
		try {
			m_pst = m_conn.prepareStatement("INSERT TDS_VEHICLE (V_VKEY,V_VNO,V_VMAKE,V_VYEAR,V_ASSOCCODE,V_RCNO,V_FLAG,V_STATUS,V_MODEL,V_VINNO,V_VTYPE,V_REGDATE,V_EXPDATE,V_MASTER_ASSOCCODE,V_VMETER_RATE,V_PAY_ID,V_PAY_PASS,V_PLATE_NO) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"); 
			m_pst.setInt(1, ConfigDAO.tdsKeyGen("CAB_SEQ_KEY", m_conn));
			m_pst.setString(2, registrationBean.getCab_cabno());
			m_pst.setString(3, registrationBean.getCab_cabmake());
			m_pst.setString(4, registrationBean.getCab_cabYear());
			m_pst.setString(5, registrationBean.getCab_assocode());
			m_pst.setString(6, registrationBean.getCab_rcno());
			m_pst.setString(7, registrationBean.getVehicleProfile());
			m_pst.setString(8, registrationBean.getStatus());
			m_pst.setString(9, registrationBean.getCab_model()); 
			m_pst.setString(10, registrationBean.getCab_vinno()); 
			m_pst.setString(11, registrationBean.getCab_vtype()); 
			m_pst.setString(12, registrationBean.getCab_registrationDate());
			m_pst.setString(13, registrationBean.getCab_expiryDate());
			m_pst.setString(14, masterAssoccode);
			m_pst.setString(15, registrationBean.getCab_meterRate());
			m_pst.setString(16, registrationBean.getPayId());
			m_pst.setString(17, registrationBean.getPayPass());
			m_pst.setString(18, registrationBean.getCab_plate_number());
			cat.info(m_pst.toString());
			result = m_pst.executeUpdate();

			m_pst = m_conn.prepareStatement("INSERT INTO TDS_ODOMETER(OD_VALUE,OD_UPDATE_TIME,OD_TRIP_ID,OD_DRIVERID,OD_CAB_NO,OD_EVENT,OD_ASSOCCODE) VALUES(?,NOW(),'','',?,'REGISTER',?)");
			m_pst.setString(1, registrationBean.getOdoMeterValue());
			m_pst.setString(2, registrationBean.getCab_cabno());
			m_pst.setString(3, masterAssoccode);
			cat.info(m_pst.toString());
			m_pst.execute();

		} catch (Exception sqex){
			cat.error(("TDSException.RegistrationDAO.insertCabRegistration-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException.RegistrationDAO.insertCabRegistration-->"+ sqex.getMessage());
			sqex.printStackTrace();	
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		
		return result;
	}

	public static ArrayList getSetteledTransSummmary(String pss_key, AdminRegistrationBO adminBO)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getSetteledTransSummmary");
		ArrayList al_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;


		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query = "";

			//String query = "select PSD_DRIVER_ID,GET_DNAME(PSD_DRIVER_ID) as DNAME,PSD_TRIP_ID,PSD_AMOUNT,PSD_TIP_AMOUNT,PSD_TOTAL_AMOUNT,PSD_TRANS_ID,date_format(PSD_PROCESSING_DATE_TIME,'%m/%d/%Y') as date,case PSD_APPROVAL_STATUS when 1 then 'Approved' else 'Declined' end as status,PSD_APPROVALCODE,PSD_TXN_PERCENT_AMT,PSD_TXN_AMT,PSD_TOTAL_AMOUNT_WITH_TXN,case PSD_VOID_FLG when true then 'Txn Voided' else '' end as void_flg,case PSD_VOIDED_FLG when true then 'Txn from void' end as voided_flg from TDS_PAYMENT_SETTLED_DETAIL where PSD_SET_ID in (select PSS_SET_ID from TDS_PAYMENT_SETTLED_SUMMARY where PAY_ID = '"+pss_key+"')";



			query = "select PSS_SET_ID,PSS_DRIVER_ID,GET_DNAME(PSS_DRIVER_ID) as DNAME,PSS_AMOUNT,PSS_TIP_AMOUNT,PSS_TOTAL_AMOUNT,date_format(PSS_PROCESSING_DATE_TIME,'%m/%d/%Y') as date,PSS_TXN_PERCENT_AMT,PSS_TXN_AMT,PSS_TOTAL_AMOUNT_WITH_TXN,PSS_ADJ_AMOUNT,PSS_DESCR from TDS_PAYMENT_SETTLED_SUMMARY where PAY_ID = '"+pss_key+"' and PSS_COMPANY_ID = ?";

			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, adminBO.getAssociateCode());
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();

			while(rs.next())
			{
				PaymentProcessBean pBean = new PaymentProcessBean();
				pBean.setB_driverid(rs.getString("PSS_DRIVER_ID")+"-"+rs.getString("DNAME"));
				pBean.setB_amount(rs.getString("PSS_AMOUNT"));
				pBean.setB_tip_amt(rs.getString("PSS_TIP_AMOUNT"));
				pBean.setB_trans_id(rs.getString("PSS_SET_ID"));
				pBean.setProcess_date(rs.getString("date"));
				pBean.setTxn_amt(rs.getString("PSS_TXN_AMT"));
				pBean.setTxn_prec(rs.getString("PSS_TXN_PERCENT_AMT"));
				pBean.setTxn_total(rs.getString("PSS_TOTAL_AMOUNT_WITH_TXN"));
				pBean.setAdj_amount(rs.getString("PSS_ADJ_AMOUNT"));
				pBean.setReason(rs.getString("PSS_DESCR"));
				pBean.setType("1");
				al_list.add(pBean);
			}

			query = "select CSS_SET_ID,CSS_DRIVER_ID,GET_DNAME(CSS_DRIVER_ID) as DNAME,CSS_TOTAL_AMOUNT,date_format(CSS_PROCESSING_DATE,'%m/%d/%Y') as date,CSS_TOTAL_AMOUNT,CSS_ADJ_AMOUNT,CSS_DESCR from TDS_DRIVER_SETELED_SUMMARY where CSS_PAY_KEY = '"+pss_key+"' and CSS_COMPANY_ID = ?";
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, adminBO.getAssociateCode());

			rs = m_pst.executeQuery();

			while(rs.next())
			{
				PaymentProcessBean pBean = new PaymentProcessBean();
				pBean.setB_driverid(rs.getString("CSS_DRIVER_ID")+"-"+rs.getString("DNAME"));
				pBean.setB_amount("0");
				pBean.setB_tip_amt("0");
				pBean.setB_trans_id(rs.getString("CSS_SET_ID"));
				pBean.setProcess_date(rs.getString("date"));
				pBean.setTxn_amt("0");
				pBean.setTxn_prec("0");
				pBean.setTxn_total("-"+rs.getString("CSS_TOTAL_AMOUNT"));
				pBean.setAdj_amount(rs.getString("CSS_ADJ_AMOUNT"));
				pBean.setReason(rs.getString("CSS_DESCR"));
				pBean.setType("2");
				al_list.add(pBean);
			}


			query = "select DPS_SET_ID,DPS_DRIVER_ID,GET_DNAME(DPS_DRIVER_ID) as DNAME,DPS_TOTAL_AMOUNT,date_format(DPS_PROCESSING_DATE,'%m/%d/%Y') as date,DPS_TOTAL_AMOUNT,DPS_ADJ_AMOUNT,DPS_DESCR from TDS_DRIVER_PENALITY_SETELED_SUMMARY where DPS_PAY_KEY = '"+pss_key+"'  and DPS_COMPANY_ID = ?";
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, adminBO.getAssociateCode());
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();

			while(rs.next())
			{
				PaymentProcessBean pBean = new PaymentProcessBean();
				pBean.setB_driverid(rs.getString("DPS_DRIVER_ID")+"-"+rs.getString("DNAME"));
				pBean.setB_amount("0");
				pBean.setB_tip_amt("0");
				pBean.setB_trans_id(rs.getString("DPS_SET_ID"));
				pBean.setProcess_date(rs.getString("date"));
				pBean.setTxn_amt("0");
				pBean.setTxn_prec("0");
				pBean.setTxn_total(rs.getString("DPS_TOTAL_AMOUNT"));
				pBean.setAdj_amount(rs.getString("DPS_ADJ_AMOUNT"));
				pBean.setReason(rs.getString("DPS_DESCR"));
				pBean.setType("3");
				al_list.add(pBean);
			}

			query = "select VSS_ID,VSS_DRIVER_ID,GET_DNAME(VSS_DRIVER_ID) as DNAME,VSS_RET_AMOUNT,VSS_TIP,VSS_TOTAL,date_format(VSS_PROCESSING_DATE,'%m/%d/%Y') as date,VSS_TXN_PERCENT_AMT,VSS_TXN_AMT,VSS_TOTAL_AMOUNT_WITH_TXN,VSS_ADJ_AMOUNT,VSS_DESCR from TDS_VOUCHER_SETELED_SUMMARY where VSS_PAY_KEY = '"+pss_key+"' and VSS_ASSOCCODE = ?";
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, adminBO.getAssociateCode());
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();

			while(rs.next())
			{
				PaymentProcessBean pBean = new PaymentProcessBean();
				pBean.setB_driverid(rs.getString("VSS_DRIVER_ID")+"-"+rs.getString("DNAME"));
				pBean.setB_amount(rs.getString("VSS_RET_AMOUNT"));
				pBean.setB_tip_amt(rs.getString("VSS_TIP"));
				pBean.setB_trans_id(rs.getString("VSS_ID"));
				pBean.setProcess_date(rs.getString("date"));
				pBean.setTxn_amt(rs.getString("VSS_TXN_AMT"));
				pBean.setTxn_prec(rs.getString("VSS_TXN_PERCENT_AMT"));
				pBean.setTxn_total(rs.getString("VSS_TOTAL_AMOUNT_WITH_TXN"));
				pBean.setAdj_amount(rs.getString("VSS_ADJ_AMOUNT"));
				pBean.setReason(rs.getString("VSS_DESCR"));
				pBean.setType("4");
				al_list.add(pBean);
			}



		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getSetteledTransSummmary-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException PaymentDAO.getSetteledTransSummmary-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}



	public static ArrayList getSetteledTransDetail(String pss_key,String type, String associationCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getSetteledTransDetail");
		ArrayList al_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;


		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query = "";

			if(type.equals("1"))
				query = "select PSD_DRIVER_ID as did,PSD_CARD_NO as card_no,GET_DNAME(PSD_DRIVER_ID) as DNAME,PSD_TRIP_ID as trip,PSD_AMOUNT as amount,PSD_TIP_AMOUNT as tip,PSD_TOTAL_AMOUNT as total,PSD_TRANS_ID as transid,date_format(PSD_PROCESSING_DATE_TIME,'%m/%d/%Y') as date,case PSD_APPROVAL_STATUS when 1 then 'Approved' else 'Declined' end as status,PSD_APPROVALCODE as appcode,PSD_TXN_PERCENT_AMT as txnper,PSD_TXN_AMT as txnamt,PSD_TOTAL_AMOUNT_WITH_TXN as totaltxn,case PSD_VOID_FLG when true then 'Txn Voided' else '' end as void_flg,case PSD_VOIDED_FLG when true then 'Txn from void' end as voided_flg,'' as descr from TDS_PAYMENT_SETTLED_DETAIL where PSD_SET_ID='"+pss_key+"'  and PSD_COMPANY_ID ='"+ associationCode+"'";
			else if(type.equals("2"))
				query = "select CSD_DRIVER_ID as did,'' as card_no,GET_DNAME(CSD_DRIVER_ID) as DNAME,0 as trip, 0 as amount,0 as tip,0 as total,CSD_TRAN_ID as transid,date_format(CSD_PROCESSING_DATE,'%m/%d/%Y') as date,'Approved' as status,'' as appcode,0 as txnper,0 as txnamt,CSD_AMOUNT as totaltxn,'' as void_flg,'' as voided_flg,'' as descr from TDS_DRIVER_SETELED_DETAIL where CSD_SET_ID='"+pss_key+"' and CSD_COMPANY_ID ='"+ associationCode+"'";
			else if(type.equals("3"))
				query = "select DPD_DRIVER_ID as did,'' as card_no,GET_DNAME(DPD_DRIVER_ID) as DNAME,0 as trip, 0 as amount,0 as tip,0 as total,DPD_TRAN_ID as transid,date_format(DPD_PROCESSING_DATE,'%m/%d/%Y') as date,'Approved' as status,'' as appcode,0 as txnper,0 as txnamt,DPD_AMOUNT as totaltxn,'' as void_flg,'' as voided_flg, DPD_DESC as descr from TDS_DRIVER_PENALITY_SETELED_DETAIL where DPD_SET_ID='"+pss_key+"'  and DPD_COMPANY_ID ='"+ associationCode+"'";
			else
				query = "select VSD_DRIVER_ID as did,'' as card_no,GET_DNAME(VSD_DRIVER_ID) as DNAME,VSD_TRIP_ID as trip, VSD_RET_AMOUNT as amount,VSD_TIP as tip,VSD_TOTAL as total,VSD_TRANS_ID as transid,date_format(VSD_SERVICE_DATE,'%m/%d/%Y') as date,'Approved' as status,'' as appcode,VSD_TXN_PERCENT_AMT as txnper,VSD_TXN_AMT as txnamt,VSD_TOTAL_AMOUNT_WITH_TXN as totaltxn,'' as void_flg,'' as voided_flg,'' as descr from TDS_VOUCHER_SETELED_DETAIL where VSD_ID='"+pss_key+"'  and VSD_ASSOCCODE ='"+ associationCode+"'";

			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();

			while(rs.next())
			{
				PaymentProcessBean pBean = new PaymentProcessBean();
				pBean.setB_driverid(rs.getString("did")+"-"+rs.getString("DNAME"));
				pBean.setB_trip_id(rs.getString("trip"));
				pBean.setB_amount(rs.getString("amount"));
				pBean.setB_tip_amt(rs.getString("tip"));
				pBean.setB_trans_id(rs.getString("transid"));
				pBean.setProcess_date(rs.getString("date"));
				pBean.setB_approval_code(rs.getString("appcode"));
				pBean.setB_approval_status(rs.getString("status"));
				pBean.setTxn_amt(rs.getString("txnamt"));
				pBean.setTxn_prec(rs.getString("txnper"));
				pBean.setTxn_total(rs.getString("totaltxn"));
				pBean.setB_desc(rs.getString("descr"));
				pBean.setB_card_no(rs.getString("card_no"));
				pBean.setType(type);
				al_list.add(pBean);
			}


		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getSetteledTransDetail-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getSetteledTransDetail-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}
	/*
	public static int insertDriverMapping(String assocode, String driverId,ArrayList al_list)
	{ 
		cat.info("TDS INFO RegistrationDAO.getSetteledTransDetail");
		int result =0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst,m_pst1 = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst1 =m_conn.prepareStatement("INSERT INTO TDS_QUEUE_DRIVER_MAPPING (QUE_NAME,DRIVER_ID,ASS_CODE,LOGIN_FLAG) VALUES(?,?,?,?)");

			for(int i=0;i<al_list.size();i++) {

				m_pst1.setString(1,""+al_list.get(i) );
				m_pst1.setString(2, driverId);
				m_pst1.setString(3, assocode);
				m_pst1.setString(4, "R"); 
				result = m_pst1.executeUpdate();


			}  	
		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getSetteledTransDetail-->"+sqex.getMessage());
			System.out.println("TDSException RegistrationDAO.getSetteledTransDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}

		return result;
	}
	 */
	public static int updateDriverMapping(String assocode, String driverId,ArrayList name_list )
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateDriverMapping");
		ArrayList al_list = new ArrayList();
		int result = 0,result1 =0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		ResultSet rs=null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst1 = m_conn.prepareStatement("DELETE FROM TDS_QUEUE_DRIVER_MAPPING WHERE DRIVER_ID=?  AND ASS_CODE=?" );
			m_pst1.setString(1, driverId);
			m_pst1.setString(2, assocode);
			cat.info(m_pst1.toString());
			result1 = m_pst1.executeUpdate();
			for(int i=0;i<name_list.size();i=i+2) {
				m_pst1 =m_conn.prepareStatement("INSERT INTO   TDS_QUEUE_DRIVER_MAPPING (QUE_NAME,DRIVER_ID,ASS_CODE,LOGIN_FLAG) VALUES(?,?,?,?)");
				m_pst1.setString(1, ""+name_list.get(i));
				m_pst1.setString(2, driverId);
				m_pst1.setString(3, assocode);
				m_pst1.setString(4, ""+name_list.get(i+1));
				result = m_pst1.executeUpdate();

				if(result == 1) {

				} else {

				}
			}


		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.updateDriverMapping-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.updateDriverMapping-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int detailsexcistOrnot(String assocode, String driverId)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.detailsexcistOrnot");
		ArrayList al_list = new ArrayList();
		int result =0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		ResultSet rs,rs1 = null; 
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			m_pst1 =m_conn.prepareStatement("SELECT DRIVER_ID,QUE_NAME,LOGIN_FLAG  FROM  TDS_QUEUE_DRIVER_MAPPING WHERE DRIVER_ID=? AND ASS_CODE=?  ");
			m_pst1.setString(1, driverId);
			m_pst1.setString(2, assocode);
			cat.info(m_pst1.toString());
			rs1 = m_pst1.executeQuery();

			while (rs1.next()) {
				result =1;
			}

		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.detailsexcistOrnot-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.detailsexcistOrnot-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {

			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static ArrayList loginList(String masterAssocode, String driverId,String assocode)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RegistrationDAO.loginList");
		ArrayList al_list = null;

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		ResultSet rs,rs1 = null; 
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			m_pst = m_conn.prepareStatement("SELECT DR_USERID FROM TDS_DRIVER  where DR_ASSOCODE= ? AND DR_USERID = ?");
			m_pst.setString(1,assocode);
			m_pst.setString(2,driverId);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();
			if(rs.next()){
				al_list = new ArrayList();
				m_pst1 = m_conn.prepareStatement(TDSSQLConstants.getmappingList);
				m_pst1.setString(1, driverId);
				m_pst1.setString(2, masterAssocode);
				m_pst1.setString(3, masterAssocode);
				System.out.println(m_pst1.toString());
				rs = m_pst1.executeQuery();
				// System.out.println("DRIVER MAPP::"+m_pst1.toString());
				while (rs.next()) {
					DriverDisbursment driverdi = new DriverDisbursment();
					driverdi.setQueue_desc(rs.getString("QD_DESCRIPTION"));
					driverdi.setQueue_name(rs.getString("QD_QUEUENAME"));
					if (rs.getString("QD_LOGIN_SWITCH").equalsIgnoreCase("A")) {
						driverdi.setQueue_login_flag("trues");
					} else if (rs.getString("LOGIN").equalsIgnoreCase("R")) {
						driverdi.setQueue_login_flag("true");
					}  else {
						driverdi.setQueue_login_flag("false");
					}if (rs.getString("LOGIN").equalsIgnoreCase("NA")) {
						driverdi.setQueue_login_flag("noAccess");
					}
					al_list.add(driverdi);
				}
			}			
		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.loginList-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.loginList-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static ArrayList zonelist(String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.zonelist");
		ArrayList al_list = new ArrayList();



		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null; 
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query = "select QD_QUEUENAME,QD_DESCRIPTION FROM TDS_QUEUE_DETAILS  where QD_ASSOCCODE =? ";
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1, assocode);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery(); 
			while(rs.next())
			{

				al_list.add(rs.getString("QD_QUEUENAME"));
				al_list.add(rs.getString("QD_DESCRIPTION"));



			} 

		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.zonelist-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.zonelist-->"+sqex.getMessage());
			sqex.printStackTrace();


		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}
	public static int insertzonedetails(String driver,String assocode,String zone,String masterAssociation)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationnDAO.insertzonedetails");
		int returnValue = 0;

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs =null;
		boolean status=false;
		String driverflag="";
		String phoneNO = "";
		String provider = "";
		String cabNo ="";
		String googleRegKey="";
		String driverId="";
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			m_pst = m_conn.prepareStatement("SELECT LO_DRPROFILE, LO_DPHONENO, LO_PROVIDER,LO_CABNO,LO_GOOG_REG_KEY,LO_DriverID  FROM TDS_LOGINDETAILS  where LO_ASSCODE= ? AND (LO_DRIVERID = '"+driver+"' OR LO_CABNO='"+driver+"') ORDER BY LO_LOGINTIME DESC");
			m_pst.setString(1,assocode);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();
			if(rs.next())
			{
				driverflag = rs.getString("LO_DRPROFILE");
				phoneNO = rs.getString("LO_DPHONENO");
				provider = rs.getString("LO_PROVIDER");
				cabNo= rs.getString("LO_CABNO");
				googleRegKey=rs.getString("LO_GOOG_REG_KEY");
				driverId=rs.getString("LO_DriverID");
				returnValue = 1;
			} else {
				returnValue = 2;
			}


			if(returnValue == 1){ 
				m_pst = m_conn.prepareStatement("insert into TDS_QUEUE (QU_NAME,QU_DRIVERID,QU_LOGINTIME,QU_ASSOCCODE,QU_QUEUEPOSITION,QU_DRPROFILE,QU_PHONENO,QU_PROVIDER,QU_VEHICLE_NO,QU_GOOG_REG_KEY,QU_MASTER_ASSOCCODE) values(?,?,now(),?,?,?,?,?,?,?,?)"); 
				m_pst.setString(1, zone);
				m_pst.setString(2, driverId);
				m_pst.setString(3, assocode);
				m_pst.setString(4, "0");
				m_pst.setString(5, driverflag); 
				m_pst.setString(6, phoneNO);
				m_pst.setString(7, provider);
				m_pst.setString(8,cabNo);
				m_pst.setString(9,googleRegKey);
				m_pst.setString(10,masterAssociation);
				cat.info(m_pst.toString());
				m_pst.executeUpdate();  


				m_pst=m_conn.prepareStatement(TDSSQLConstants.insertDriverZoneLogin);
				m_pst.setString(1, driverId);
				m_pst.setString(2, zone);
				m_pst.setString(3, assocode);
				m_pst.executeUpdate();


			}
		}  catch (SQLException sqex) {
			returnValue = 0;
			cat.error("TDSException RegistrationDAO.insertzonedetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationnDAO.insertzonedetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return returnValue;

	}
	public static int deleteDriverFromQueueWithoutQueueID(String driverid,String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			/*pst = con.prepareStatement(TDSSQLConstants.DeleteQueuePosition);//delete from TDS_DRIVERLOCATION where DL_DRIVERID=?
			pst.setString(1, driverid);
			pst.execute();
			pst.close();*/

			pst = con.prepareStatement("DELETE FROM TDS_QUEUE WHERE QU_ASSOCCODE='"+assoCode+"' AND (QU_DRIVERID='"+driverid+"' OR QU_VEHICLE_NO='"+driverid+"')");//DELETE FROM TDS_QUEUE WHERE QU_DRIVERID= ? AND QU_NAME = ? 
			cat.info(pst.toString());
			result = pst.executeUpdate();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException RegistrationDAO.deleteDriverFromQueue-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.deleteDriverFromQueue-->"+sqex.getMessage());
			sqex.printStackTrace();		
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static String checkDriverCurrentlyLogggedInQueue(String driverID,String assoCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String queueName = "";
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("SELECT Q.QU_NAME FROM TDS_QUEUE Q WHERE Q.QU_ASSOCCODE='"+assoCode+"' AND (Q.QU_DRIVERID ='"+driverID+"' OR Q.QU_VEHICLE_NO='"+driverID+"')");
			m_rs = m_pst.executeQuery();
			while (m_rs.next()) {
				queueName = m_rs.getString("QU_NAME");
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {

			cat.error("TDSException RegistrationDAO.checkDriverCurrentlyLogggedInQueue-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.checkDriverCurrentlyLogggedInQueue-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return queueName;
	}


	@SuppressWarnings({ "rawtypes", "rawtypes" })
	public static int insertDriverDisbrush(DriverDisbursment disbursment,String assocode,String oprid)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertDriverDisbrush");

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;

		int key = 0;

		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			key = ConfigDAO.tdsKeyGen("DRIVER_DISBURSEMENT_SEQ", m_conn);

			m_conn.setAutoCommit(false);



			m_pst = m_conn.prepareStatement("insert into TDS_DISBURSEMENT (DM_DRIVERID,DM_ASSOCCODE,DM_PAYMENTDATE,DM_AMOUNT,DM_CHECKNO,DM_DESC,DM_PAYID,DM_OPR_ID,DM_CHK_CASH)values(?,?,now(),?,?,?,?,?,?)");

			m_pst.setString(1, disbursment.getDriverid());
			m_pst.setString(2, assocode);
			m_pst.setString(3, disbursment.getSet_amount());
			m_pst.setString(4, disbursment.getCheckno());
			m_pst.setString(5, disbursment.getDescr());
			m_pst.setString(6, ""+key);
			m_pst.setString(7, oprid);
			m_pst.setString(8, disbursment.getCheck_cash());
			cat.info(m_pst.toString());
			m_pst.execute();

			for(int i=0;i<disbursment.getAl_list().size();i++)
			{
				DriverDisbrusementSubBean subBean =  (DriverDisbrusementSubBean)disbursment.getAl_list().get(i);

				if(subBean.getType().equals("1"))//Driver Settlement
					m_pst1 = m_conn.prepareStatement("update TDS_PAYMENT_SETTLED_SUMMARY set PSS_DIS_FLG = true,PAY_ID=?,PSS_ADJ_AMOUNT =?,PSS_DESCR=? where PSS_SET_ID = ?");
				else if(subBean.getType().equals("2"))//Driver Charges
					m_pst1 = m_conn.prepareStatement("update TDS_DRIVER_SETELED_SUMMARY set CSS_DIS_FLG = true,CSS_PAY_KEY=?,CSS_ADJ_AMOUNT =?,CSS_DESCR=? where CSS_SET_ID = ?");
				else if(subBean.getType().equals("3")) // Driver Penality/Bonus
					m_pst1 = m_conn.prepareStatement("update TDS_DRIVER_PENALITY_SETELED_SUMMARY set DPS_DIS_FLG = true,DPS_PAY_KEY=?,DPS_ADJ_AMOUNT =?,DPS_DESCR=? where DPS_SET_ID = ?");
				else
					m_pst1 = m_conn.prepareStatement("update TDS_VOUCHER_SETELED_SUMMARY set VSS_DIS_FLG = true,VSS_PAY_KEY=?,VSS_ADJ_AMOUNT =?,VSS_DESCR=? where VSS_ID = ?");

				m_pst1.setString(1, ""+key);
				m_pst1.setString(2, subBean.getAmount().split(" ")[0].equals("$")?subBean.getAmount().split(" ")[1]:subBean.getAmount().split(" ")[0]);
				m_pst1.setString(3, subBean.getDescr());
				m_pst1.setString(4, subBean.getPss_id());
				cat.info(m_pst1.toString());
				m_pst1.execute();

				m_pst1.close();
			}

			m_pst.close();


			m_conn.commit();
			m_conn.setAutoCommit(true);

		}  catch (SQLException sqex) {
			if(m_conn!=null)
			{
				try{
					m_conn.rollback();
				} catch (SQLException e) {
					// TODO: handle exception
				}
			}
			cat.error("TDSException RegistrationDAO.insertDriverDisbrush-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertDriverDisbrush-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return key;

	}

	public static ArrayList getDriverSetelment(String from_date,String to_date, String assocode,String driverid)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverSetelment");
		ArrayList al_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;


		try {

			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			String query = "select PSS_SET_ID,PSS_DRIVER_ID,PSS_TOTAL_AMOUNT_WITH_TXN as total," +
					" date_format(PSS_PROCESSING_DATE_TIME,'%m/%d/%Y') as p_date,DR_FNAME" +
					" from TDS_PAYMENT_SETTLED_SUMMARY, TDS_DRIVER" +
					" Where PSS_DRIVER_ID = DR_SNO and PSS_DIS_FLG!=true and PSS_COMPANY_ID='"+assocode+"' ";

			if(!from_date.equals(""))
				query = query + " and date_format(PSS_PROCESSING_DATE_TIME,'%Y%m%d') >= '"+from_date+"'";
			if(!to_date.equals(""))
				query = query + " and date_format(PSS_PROCESSING_DATE_TIME,'%Y%m%d') <= '"+to_date+"'";
			if(!driverid.equals(""))
				query = query + " and PSS_DRIVER_ID = '"+driverid+"'" ;


			query = query + " order by PSS_DRIVER_ID ";
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString()); 		
			rs = m_pst.executeQuery();

			while(rs.next())
			{
				DriverDisbursment disbursment = new DriverDisbursment();
				disbursment.setAmount(rs.getString("total"));
				disbursment.setDriverid(rs.getString("PSS_DRIVER_ID"));
				disbursment.setDrivername(rs.getString("DR_FNAME"));
				disbursment.setProcess_date(rs.getString("p_date"));
				disbursment.setTrans_id(rs.getString("PSS_SET_ID"));
				disbursment.setType("1");
				/*disbursment.setTrip_id(rs.getString("PSD_TRIP_ID"));
					disbursment.setPayment_id(rs.getString("PSD_TRANS_ID"));*/
				al_list.add(disbursment);

			}

			query = "select " +
					" CSS_SET_ID,CSS_DRIVER_ID,-CSS_TOTAL_AMOUNT as total," +
					" date_format(CSS_PROCESSING_DATE,'%m/%d/%Y') as p_date,DR_FNAME" +
					" from" +
					" TDS_DRIVER_SETELED_SUMMARY, TDS_DRIVER" +
					" Where " +
					" CSS_DRIVER_ID = DR_SNO and CSS_DIS_FLG!=true and CSS_COMPANY_ID='"+assocode+"'";

			if(!from_date.equals(""))
				query = query + " and date_format(CSS_PROCESSING_DATE,'%Y%m%d') >= '"+from_date+"'";
			if(!to_date.equals(""))
				query = query + " and date_format(CSS_PROCESSING_DATE,'%Y%m%d') <= '"+to_date+"'";
			if(!driverid.equals(""))
				query = query + " and CSS_DRIVER_ID = '"+driverid+"'" ;


			query = query + " order by CSS_DRIVER_ID ";
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();

			while(rs.next())
			{
				DriverDisbursment disbursment = new DriverDisbursment();
				disbursment.setAmount(rs.getString("total"));
				disbursment.setDriverid(rs.getString("CSS_DRIVER_ID"));
				disbursment.setDrivername(rs.getString("DR_FNAME"));
				disbursment.setProcess_date(rs.getString("p_date"));
				disbursment.setTrans_id(rs.getString("CSS_SET_ID"));
				disbursment.setType("2");
				/*disbursment.setTrip_id(rs.getString("PSD_TRIP_ID"));
					disbursment.setPayment_id(rs.getString("PSD_TRANS_ID"));*/
				al_list.add(disbursment);

			}

			query = "select" +
					"	DPS_SET_ID,DPS_DRIVER_ID,DPS_TOTAL_AMOUNT as total," +
					"   date_format(DPS_PROCESSING_DATE,'%m/%d/%Y') as p_date,DR_FNAME" +
					" from " +
					"  TDS_DRIVER_PENALITY_SETELED_SUMMARY, TDS_DRIVER " +
					" Where " +
					" DPS_DRIVER_ID = DR_SNO and DPS_DIS_FLG!=true and DPS_COMPANY_ID='"+assocode+"'";




			if(!from_date.equals(""))
				query = query + " and date_format(DPS_PROCESSING_DATE,'%Y%m%d') >= '"+from_date+"'";
			if(!to_date.equals(""))
				query = query + " and date_format(DPS_PROCESSING_DATE,'%Y%m%d') <= '"+to_date+"'";
			if(!driverid.equals(""))
				query = query + " and DPS_DRIVER_ID = '"+driverid+"'" ;


			query = query + " order by DPS_DRIVER_ID ";
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();

			while(rs.next())
			{
				DriverDisbursment disbursment = new DriverDisbursment();
				disbursment.setAmount(rs.getString("total"));
				disbursment.setDriverid(rs.getString("DPS_DRIVER_ID"));
				disbursment.setDrivername(rs.getString("DR_FNAME"));
				disbursment.setProcess_date(rs.getString("p_date"));
				disbursment.setTrans_id(rs.getString("DPS_SET_ID"));
				disbursment.setType("3");
				/*disbursment.setTrip_id(rs.getString("PSD_TRIP_ID"));
					disbursment.setPayment_id(rs.getString("PSD_TRANS_ID"));*/
				al_list.add(disbursment);

			}


			query = "select" +
					"	VSS_ID,VSS_DRIVER_ID,VSS_TOTAL_AMOUNT_WITH_TXN as total," +
					"   date_format(VSS_PROCESSING_DATE,'%m/%d/%Y') as p_date,DR_FNAME" +
					" from " +
					"  TDS_VOUCHER_SETELED_SUMMARY, TDS_DRIVER" +
					" Where " +
					" VSS_DRIVER_ID=DR_SNO and VSS_DIS_FLG!=true and VSS_ASSOCCODE='"+assocode+"'";

			if(!from_date.equals(""))
				query = query + " and date_format(VSS_PROCESSING_DATE,'%Y%m%d') >= '"+from_date+"'";
			if(!to_date.equals(""))
				query = query + " and date_format(VSS_PROCESSING_DATE,'%Y%m%d') <= '"+to_date+"'";
			if(!driverid.equals(""))
				query = query + " and VSS_DRIVER_ID = '"+driverid+"'" ;


			query = query + " order by VSS_DRIVER_ID ";
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();

			while(rs.next())
			{
				DriverDisbursment disbursment = new DriverDisbursment();
				disbursment.setAmount(rs.getString("total"));
				disbursment.setDriverid(rs.getString("VSS_DRIVER_ID"));
				disbursment.setDrivername(rs.getString("DR_FNAME"));
				disbursment.setProcess_date(rs.getString("p_date"));
				disbursment.setTrans_id(rs.getString("VSS_ID"));
				disbursment.setType("4");
				/*disbursment.setTrip_id(rs.getString("PSD_TRIP_ID"));
					disbursment.setPayment_id(rs.getString("PSD_TRANS_ID"));*/
				al_list.add(disbursment);

			}



		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getDriverSetelment-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverSetelment-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static ArrayList getDriverDisbrushmentSummary(String from_date,String to_date, String assocode,String driver)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverDisbrushmentSummary");
		ArrayList al_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;


		try {
			String query = "select" +
					" DM_DRIVERID,DR_FNAME,date_format(DM_PAYMENTDATE, '%m/%d/%Y') as p_date,DM_AMOUNT,DM_CHECKNO,DM_DESC,DM_PAYID,DM_OPR_ID" +
					" from TDS_DISBURSEMENT, TDS_DRIVER " +
					" where " +
					" DM_DRIVERID=DR_SNO and DM_ASSOCCODE= ? ";

			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			if(!from_date.equals(""))
			{
				query = query + " and date_format(DM_PAYMENTDATE,'%Y%m%d') >= '"+from_date+"'";
			}

			if(!to_date.equals(""))
			{
				query = query + " and date_format(DM_PAYMENTDATE,'%Y%m%d') <= '"+to_date+"'";
			}

			if(!driver.equals(""))
			{
				query =query + " and DM_DRIVERID = '"+driver+"'";
			}

			query = query + " order by p_date desc ";

			m_pst = m_conn.prepareStatement(query);

			m_pst.setString(1, assocode);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();

			while(rs.next())
			{
				DriverDisbursment disbursment = new DriverDisbursment();
				disbursment.setAmount(rs.getString("DM_AMOUNT"));
				disbursment.setDriverid(rs.getString("DM_DRIVERID"));
				disbursment.setDrivername(rs.getString("DR_FNAME"));
				disbursment.setProcess_date(rs.getString("p_date"));

				disbursment.setDescr(rs.getString("DM_DESC"));
				disbursment.setCheckno(rs.getString("DM_CHECKNO"));
				//disbursment.setTrip_id(rs.getString("PSD_TRIP_ID"));
				disbursment.setPayment_id(rs.getString("DM_PAYID"));
				al_list.add(disbursment);


			}


		}  catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getDriverDisbrushmentSummary-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverDisbrushmentSummary-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}
	public static int forgotpasswordDAO(String username,ApplicationPoolBO pool) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.CompanyMasterview");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1;
		StringBuffer sb=null;
		ResultSet rs = null;

		int ssa=0;
		try {

			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
			sb=new StringBuffer();
			Random r = new Random();
			int te=0;
			for(int i=1;i<=8;i++){
				te=r.nextInt(str.length()-1);
				sb.append(str.charAt(te));
			}

			m_pst = m_conn.prepareStatement("select AU_ASSOCCODE from TDS_ADMINUSER  where AU_USERNAME = ?");
			m_pst.setString(1,username);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();

			if(rs.first())
			{	
				m_pst1 = m_conn.prepareStatement("update TDS_ADMINUSER set AU_PASSWORD = ? where AU_USERNAME = ?");
				m_pst1.setString(1,sb.toString());
				m_pst1.setString(2,username);
				ssa = m_pst1.executeUpdate();

				if(ssa==1)
				{
					AuditDAO.insertauditRequest(rs.getString("AU_ASSOCCODE"), username, "PasswordChanged", "", "", "105","","");

					ArrayList al = new ArrayList(1);
					al.add(UtilityDAO.returnEmail(username));					

					if(!al.isEmpty() && al.get(0).toString()!=null)
					{
						OnetoOneDirect oneDirect = new OnetoOneDirect(al.get(0).toString(), "TDSTDS;F;"+System.currentTimeMillis()+";Your new Password is"+sb.toString(),pool);
						oneDirect.start();
					}
					//MailingUtiil.sendMail(poolBO.getSMTP_HOST_NAME(), poolBO.getSMTP_AUTH_USER(), poolBO.getSMTP_AUTH_PWD(), poolBO.getEmailFromAddress(), al, sb.toString(),"");

				} 
			} 

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.forgotpasswordDAO-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.CompanyMasterview-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ssa;
	}
	public static ArrayList CompanyMasterview(String Assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.CompanyMasterview");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		ResultSet rs=null;
		ArrayList array_list=new ArrayList();

		PreparedStatement pst = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("select CD_ASSOCCODE,CD_COMPANYNAME,CD_COMPANYDESC,CD_ADDRESS1,CD_ADDRESS2,CD_CITY," +
					"CD_STATE,CD_PHONE,CD_BANKACNO,CD_BANKROUTINGNO,CD_BANKNAME,CD_ZIPCODE,CD_DISPATCH_SWITH,CD_FILEID,CD_EMAIL," +
					"case CD_MERCHANTPROFILE when '' then 0  else 1 end as flg," +
					"(select FILENAME from UPLOADS where UPLOADID = CD_FILEID) as FNAME" +
					" from TDS_COMPANYDETAIL where CD_ASSOCCODE=?");
			pst.setString(1, Assocode);
			cat.info(pst.toString());

			rs = pst.executeQuery();
			while(rs.next()){
				CompanyMasterBO CompanyBO=new CompanyMasterBO();
				CompanyBO.setAssccode(rs.getString("CD_ASSOCCODE"));
				CompanyBO.setCname(rs.getString("CD_COMPANYNAME"));
				CompanyBO.setCdesc(rs.getString("CD_COMPANYDESC"));
				CompanyBO.setAdd1(rs.getString("CD_ADDRESS1"));
				CompanyBO.setAdd2(rs.getString("CD_ADDRESS2"));
				CompanyBO.setCity(rs.getString("CD_CITY"));   			
				CompanyBO.setState(rs.getString("CD_STATE"));
				CompanyBO.setPhoneno(rs.getString("CD_PHONE"));
				CompanyBO.setBankacno(rs.getString("CD_BANKACNO"));
				CompanyBO.setBankroutingno(rs.getString("CD_BANKROUTINGNO"));
				CompanyBO.setBankname(rs.getString("CD_BANKNAME"));
				CompanyBO.setZip(rs.getString("CD_ZIPCODE"));
				CompanyBO.setFileid(rs.getString("CD_FILEID"));
				CompanyBO.setFilename(rs.getString("FNAME"));
				CompanyBO.setDipatchStatus(rs.getString("CD_DISPATCH_SWITH").trim().toCharArray()[0]);
				CompanyBO.setEmail(rs.getString("CD_EMAIL"));
				CompanyBO.setShow_flg(rs.getInt("flg")==0?false:true);
				array_list.add(CompanyBO);
			}
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.CompanyMasterview-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.CompanyMasterview-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return array_list;	
	}



	public static void uncapturedamtcapturedflag(String tripid) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.uncapturedamtcapturedflag");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String check[]=tripid.split(";");
			for(int i=0;i<check.length;i++){
				pst = con.prepareStatement("update TDS_VOUCHER_DETAIL  set VD_PAYMENT_RECEIVED_STATUS = 'true' WHERE VD_TRIP_ID=?");
				pst.setString(1,check[i]);
				cat.info(pst.toString());
				pst.executeUpdate();
			}
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.uncapturedamtcapturedflag-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.uncapturedamtcapturedflag-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));


	}

	public static ArrayList uncapturedamtdetails(String assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.uncapturedamtdetails");
		ArrayList Voucher_data=new ArrayList();
		VoucherBO obj;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		try {

			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement("SELECT format(VD_RET_AMOUNT,2) as VD_RET_AMOUNT ,format(VD_TIP,2) as VD_TIP,VD_TRIP_ID,VD_DRIVER_ID,VD_DESCR,VD_VOUCHERNO,VD_RIDER_NAME,DATE_FORMAT(VD_PROCESSING_DATE_TIME,'%m/%d/%Y %h:%m') as VD_SERVICE_DATE,format((VD_RET_AMOUNT+VD_TIP),2) as sumamt FROM TDS_VOUCHER_DETAIL WHERE VD_assoc_code='"+assocode+"' and VD_PAYMENT_RECEIVED_STATUS='0'");
			//pst = con.prepareStatement("SELECT *,(VW_RET_AMOUNT+VW_TIP) as sumamt FROM TDS_VOUCHER_WORK WHERE vw_assoccode='"+assocode+"' and VW_PAYMENT_RECEIVEDFLG='0'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				obj=new VoucherBO();
				obj.setVno(rs.getString("VD_VOUCHERNO"));
				obj.setVuser(rs.getString("VD_RIDER_NAME"));
				obj.setVdesc(rs.getString("VD_DESCR"));
				obj.setFrom_date(rs.getString("VD_SERVICE_DATE"));
				obj.setVtripid(rs.getString("VD_TRIP_ID"));
				obj.setVdriver(rs.getString("VD_DRIVER_ID"));
				obj.setVamount(rs.getString("sumamt"));
				obj.setVcode(rs.getString("VD_RET_AMOUNT"));
				obj.setTip(rs.getString("VD_TIP"));
				Voucher_data.add(obj);
			}
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.uncapturedamtdetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.uncapturedamtdetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return Voucher_data;
	}


	public static boolean delDriverPenality(String checkval, AdminRegistrationBO adminBO) {		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RegistrationDAO.delDriverPenality");
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();

			m_pst = m_conn.prepareStatement("delete from  TDS_DRIVER_PENALITY where DP_TRANS_ID=? and DP_ASSOCCODE =?");
			String check[]=checkval.split(";");
			for(int i=0;i<check.length;i++) {
				m_pst.setString(1,check[i]);
				m_pst.setString(2, adminBO.getAssociateCode());
				if(check[i]!=null && check[i]!="")
					cat.info(m_pst.toString());
				m_pst.executeUpdate();

			}

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.delDriverPenality-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.delDriverPenality-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}

	public static int VoucherSavefromcompany(VoucherBO p_voucherBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.VoucherSavefromcompany");
		int result = 0;
		int result1 = 0;
		cat.info("In VoucherSavefromcompany Voucher Entry ");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.insertVoucherEntry);
			pst.setString(1, p_voucherBO.getVno());
			pst.setString(2, p_voucherBO.getVname());
			pst.setString(3, "P");
			pst.setString(4, " ");

			pst.setString(5, " ");
			String ss=p_voucherBO.getVamount().split(" ")[0].equals("$")?p_voucherBO.getVamount().split(" ")[1]:p_voucherBO.getVamount().split(" ")[0];
			pst.setString(6, ss);
			pst.setString(7, TDSValidation.getDBdateFormat(p_voucherBO.getVexpdate()));
			pst.setString(8, p_voucherBO.getVdesc());
			pst.setString(9, p_voucherBO.getVuser());
			pst.setString(10,p_voucherBO.getVassoccode());
			pst.setString(11, p_voucherBO.getVcontact());
			pst.setString(12, p_voucherBO.getVdelay());
			pst.setString(13, p_voucherBO.getVfreq());
			cat.info(pst.toString());

			result = pst.executeUpdate();
			String check[]=p_voucherBO.getVcode().split(";");
			for(int i=0;i<check.length;i++) {
				pst = con.prepareStatement(TDSSQLConstants.insertVoucherEntrytocorparate);
				pst.setString(1,p_voucherBO.getVassoccode());
				pst.setString(2,check[i]);
				pst.setString(3,p_voucherBO.getVno());
				pst.setString(4,"p");
				pst.setString(5,p_voucherBO.getVexpdate());
				cat.info(pst.toString());

				result1 =pst.executeUpdate();
			}
			if(result >0 && result1 >0) {
				cat.info("Success fully updated Voucher Entry");
			} else {
				cat.info("Failure to insert voucher Entry");
			}
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.VoucherSavefromcompany-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.VoucherSavefromcompany-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static String getCopanyName(String code,String assoccode)
	{
		String company="";
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCopanyName");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		int userkey = 0;
		try {
			dbcon = new TDSConnection();
			con  = dbcon.getConnection();
			pst = con.prepareStatement("select VC_COSTCENTER from TDS_GOVT_VOUCHER where VC_VOUCHERNO = '"+code+"' and  VC_ASSOCCODE='"+assoccode+"'");
			cat.info(pst.toString());

			rs= pst.executeQuery();
			if(rs.first())
			{
				company= rs.getString("VC_COSTCENTER");
			} else
				company= "";

		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getCopanyName-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getCopanyName-->"+sqex.getMessage());
			sqex.printStackTrace();

		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return company;
	}

	public static String getCompanyEmailAddress(String code)
	{
		String company="";
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCopanyName");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		int userkey = 0;
		try {
			dbcon = new TDSConnection();
			con  = dbcon.getConnection();
			pst = con.prepareStatement("select CD_EMAIL from TDS_COMPANYDETAIL where CD_ASSOCCODE = '"+code+"'");
			cat.info(pst.toString());

			rs= pst.executeQuery();
			if(rs.first())
			{
				company= rs.getString("CD_EMAIL");
			} else{
				company= "";
			}

		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getCopanyName-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getCopanyName-->"+sqex.getMessage());
			sqex.printStackTrace();

		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return company;
	}


	public static ArrayList CompanyDetailsview() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.CompanyDetailsview");
		ArrayList Company_data=new ArrayList();
		CompanyMasterBO obj;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		try {

			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT CD_ASSOCCODE,CD_COMPANYNAME,CD_COMPANYDESC FROM TDS_COMPANYDETAIL");
			cat.info(pst.toString());

			rs = pst.executeQuery();
			while (rs.next()) {
				obj=new CompanyMasterBO();
				obj.setAssccode(rs.getString("CD_ASSOCCODE"));
				obj.setCname(rs.getString("CD_COMPANYNAME"));
				obj.setCdesc(rs.getString("CD_COMPANYDESC"));
				Company_data.add(obj);
			}
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.CompanyDetailsview-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.CompanyDetailsview-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return Company_data;
	}
	public static int insertDriverManager(DriverRegistrationBO p_driverBO,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertDriverManager");
		int result = 0;
		int result2= 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;

		try {
			dbcon = new TDSConnection();
			con  = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.insertParetDetails);
			int userkey = ConfigDAO.tdsKeyGen("ADMINID", con);
			//pst.setString(1, p_driverBO.getAssociateCode()+""+userkey);
			pst.setString(1, p_driverBO.getUid());
			pst.setString(2, p_driverBO.getUid());
			pst.setString(3, p_driverBO.getPassword());
			pst.setString(4, p_driverBO.getRegistration() =='D'?"Driver":"Operator");
			pst.setString(5, p_driverBO.getAssociateCode());
			pst.setString(6, p_driverBO.getPayUserId());
			pst.setString(7, p_driverBO.getPayPassword());
			pst.setString(8, p_driverBO.getPayMerchant());
			pst.setString(9, "1");
			pst.setString(10, p_driverBO.getFname());
			pst.setString(11, p_driverBO.getLname());
			pst.setString(12, p_driverBO.getSocialSecurityNo());
			pst.setString(13, adminBO.getMasterAssociateCode());
			pst.setString(14, p_driverBO.getEmailId());

			/*pst = con.prepareStatement(TDSSQLConstants.selectDriverParent);
			pst.setString(1,p_driverBO.getUid());
			pst.setString(2, p_driverBO.getPassword());*/

			cat.info(pst.toString());

			result = pst.executeUpdate(); 

			if(p_driverBO.getRegistration() == 'D') {

				pst1 = con.prepareStatement(TDSSQLConstants.insertDriverManager);
				//pst1.setString(1, p_driverBO.getAssociateCode()+""+userkey);
				pst1.setString(1, p_driverBO.getUid());
				pst1.setString(2, p_driverBO.getUid());
				pst1.setString(3, p_driverBO.getPassword());
				pst1.setString(4, p_driverBO.getFname());
				pst1.setString(5, p_driverBO.getLname());
				pst1.setString(6,p_driverBO.getAdd1());
				pst1.setString(7, p_driverBO.getAdd2());
				pst1.setString(8, p_driverBO.getCity());
				pst1.setString(9, p_driverBO.getState());
				pst1.setString(10, p_driverBO.getZip());
				pst1.setString(11, p_driverBO.getRoute());
				pst1.setString(12, p_driverBO.getAcct());
				pst1.setString(13, p_driverBO.getPhone());
				pst1.setString(14, "");
				pst1.setString(15, p_driverBO.getCmake());
				pst1.setString(16, p_driverBO.getCyear());
				pst1.setString(17, p_driverBO.getCmodel());
				pst1.setString(18, p_driverBO.getDriExten().equals("")?"1-888":p_driverBO.getDriExten());
				pst1.setString(19, p_driverBO.getFdeposit());
				pst1.setString(20, p_driverBO.getStatus());
				pst1.setString(21, p_driverBO.getEhostid());
				pst1.setString(22, p_driverBO.getCabno());
				pst1.setString(23, p_driverBO.getPassno());
				pst1.setString(24, p_driverBO.getAssociateCode());
				pst1.setString(25, p_driverBO.getDrProfile());
				pst1.setString(26, p_driverBO.getDrLicense());
				pst1.setString(27, p_driverBO.getDrLicenseExpiry());
				pst1.setString(28, p_driverBO.getHkLicense());
				pst1.setString(29,p_driverBO.getHkLicenseExpiry());
				pst1.setString(30, p_driverBO.getTemplate()+"");
				pst1.setString(31, adminBO.getMasterAssociateCode());
				pst1.setInt(32, p_driverBO.getDriverRatings());
				pst1.setInt(33, p_driverBO.getSettingsAccess());
				pst1.setString(34, p_driverBO.getEmailId());
				
				cat.info(pst1.toString());
				result2 = pst1.executeUpdate();

			}
			pst.close();
			con.close();
			if(result2 == 1 ) {

				cat.info("Successfully inserted into TDS_DRIVER");
			} else {
				cat.info("Failure to insert into TDS_DRIVER");
			}
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.insertDriverManager-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertDriverManager-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}



	public static ArrayList getDriverDetailSummary() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverDetailSummary");
		ArrayList al_driverList = new ArrayList ();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		DriverRegistrationBO driverBO;
		try {
			dbcon = new TDSConnection();
			con =dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getDriverSummary);
			cat.info(pst.toString());

			rs = pst.executeQuery();
			while (rs.next()) {
				driverBO = new DriverRegistrationBO();
				driverBO.setSno(rs.getString("DR_SNO"));
				driverBO.setUid(rs.getString("DR_USERID"));
				driverBO.setFname(rs.getString("DR_FNAME"));
				driverBO.setLname(rs.getString("DR_LNAME"));
				driverBO.setRoute(rs.getString("DR_BANKROUTING"));
				driverBO.setPhone(rs.getString("DR_PHONE"));
				al_driverList.add(driverBO);
			}
			rs.close();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getDriverDetailSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverDetailSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_driverList;
	}
	public static ArrayList getAllCompanyVoucher(VoucherBO voucherBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getAllCompanyVoucher");
		ArrayList voucher_data=new ArrayList();
		cat.info("In getAllVoucher Entry ");

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs=null,rs1=null;
		String st="",st1="";
		String query="";
		int i = 1;
		try{
			if(voucherBO.getVno()!=""){
				query+=" AND VC_VOUCHERNO ='"+voucherBO.getVno()+"'";
			}
			if(voucherBO.getVname()!=""){
				query+=" AND VC_RIDERNAME ='"+voucherBO.getVname()+"'";
			}
			if(voucherBO.getFrom_date()!="" ){
				query+=" AND date_format(VC_CREATION_DATE,'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(voucherBO.getFrom_date())+"'";
			}
			if(voucherBO.getTo_date()!=""){
				query+=" AND date_format(VC_CREATION_DATE,'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(voucherBO.getTo_date())+"'";
			}
			String st2[]=voucherBO.getVcode().split(";");
			for(int h=0;h<=st2.length;h++) {
				if(st2.length == h) {
					st1+="'";
				}else {
					st1+=st2[h]+"','";
				}             	
			}
			if(voucherBO.getVcode() != ""){
				query+=" AND CV_ASSOCODE in ('"+st1+")";
			}
			query+=")group by VC_VOUCHERNO";

			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getAllCompanyVoucherEntry+query);
			cat.info(pst.toString()); 
			rs=pst.executeQuery();
			while(rs.next()){
				VoucherBO vdataBO = new VoucherBO();
				vdataBO.setVreason(rs.getString("asso_code")+";");
				vdataBO.setVname(rs.getString("VC_RIDERNAME"));
				vdataBO.setVno(rs.getString("VC_VOUCHERNO"));
				vdataBO.setVstatus(rs.getString("VC_SWITH"));
				vdataBO.setVassoccode(rs.getString("VC_ASSOCCODE"));
				vdataBO.setVamount(rs.getString("VC_AMOUNT"));
				vdataBO.setVexpdate(rs.getString("expiredate"));
				vdataBO.setVdesc(rs.getString("VC_DESCR"));
				vdataBO.setFrom_date(rs.getString("date"));
				vdataBO.setVcode(rs.getString("SSOCODE"));
				vdataBO.setVcontact(rs.getString("VC_CONTACT"));
				vdataBO.setVdelay(rs.getString("VC_DELAYDAYS"));
				vdataBO.setVfreq(rs.getString("VC_FREQ"));
				voucher_data.add(vdataBO);
			}
			rs.close();
			pst.close();
			con.close();
		} 
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getAllCompanyVoucher-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getAllCompanyVoucher-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucher_data;
	}
	public static int updatecompanyVoucherEntry(VoucherBO p_voucherBO,String action,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updatecompanyVoucherEntry");
		int result = 0;
		cat.info("In VoucherSavefromcompany Voucher Entry ");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.VoucherEntryUpdatecompany);

			cat.info(pst.toString());

			pst.setString(1, p_voucherBO.getVno());
			pst.setString(2, p_voucherBO.getVname());
			pst.setString(3, "P");
			pst.setString(4, " ");
			pst.setString(5, " ");
			String ss=p_voucherBO.getVamount().split(" ")[0].equals("$")?p_voucherBO.getVamount().split(" ")[1]:p_voucherBO.getVamount().split(" ")[0];
			pst.setString(6, ss);
			pst.setString(7, TDSValidation.getDBdateFormat(p_voucherBO.getVexpdate()));
			pst.setString(8, p_voucherBO.getVdesc());
			pst.setString(9, p_voucherBO.getVuser());
			pst.setString(10," ");
			pst.setString(11,p_voucherBO.getVassoccode());
			pst.setString(12, p_voucherBO.getVcontact());
			pst.setString(13, p_voucherBO.getVdelay());
			pst.setString(14, p_voucherBO.getVfreq());
			pst.setString(15, p_voucherBO.getVno());
			try {

				result = pst.executeUpdate();
			}catch(Exception w){
				cat.error("TDSException RegistrationDAO.updatecompanyVoucherEntry-->" + w.toString());
			}
			String check[]=p_voucherBO.getVcode().split(";");

			pst = con.prepareStatement("delete FROM TDS_CORPARATE_VOUCHER where CV_VNO=? and CV_ASSOCODE=?");
			pst.setString(1,p_voucherBO.getVno());
			pst.setString(2,adminBO.getAssociateCode());
			try {
				cat.info(pst.toString());
				pst.executeUpdate();
			}catch(Exception w) {
				//System.out.println(w);
				cat.error("TDSException RegistrationDAO.updatecompanyVoucherEntry-->" + w.toString());
			}
			for(int i=0;i<check.length;i++) {
				pst = con.prepareStatement(TDSSQLConstants.insertVoucherEntrytocorparate);
				pst.setString(1,p_voucherBO.getVassoccode());
				pst.setString(2,check[i]);
				pst.setString(3,p_voucherBO.getVno());
				pst.setString(4,"p");
				pst.setString(5,p_voucherBO.getVexpdate());
				cat.info(pst.toString());

				pst.executeUpdate();
			}
			if(result >0) {
				cat.info("Success fully updated Voucher Entry");
			} else {
				cat.info("Failure to insert voucher Entry");
			}
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.updatecompanyVoucherEntry-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.updatecompanyVoucherEntry-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int updateDriverDetails(HashMap hmp_driver) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateDriverDetails");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.updateDriverAvailability);
			pst.setString(1, hmp_driver.get("latitude").toString());
			pst.setString(2, hmp_driver.get("logitude").toString());
			pst.setString(3, hmp_driver.get("status").toString());
			pst.setString(4, hmp_driver.get("driverid").toString());
			cat.info(pst.toString());


			result = pst.executeUpdate();
			if(result >0) {
				cat.info("Successfully updated into TDS_DRiver table");
			} else {
				cat.info("Failure to update into TDS_Driver table");
			}
			pst.close();
			con.close();

		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.updateDriverDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.updateDriverDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int saveAdminRegistration(AdminRegistrationBO p_adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.saveAdminRegistration");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		String userType[] = p_adminBO.getUsertype().split("/");
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.insertAdminDetail);
			pst.setString(1, ""+ConfigDAO.tdsKeyGen("ADMINID", con));		
			pst.setString(2, p_adminBO.getUname());
			pst.setString(3, p_adminBO.getPassword());
			pst.setString(4, userType[0]);
			pst.setString(5, userType[1]);
			pst.setString(6, p_adminBO.getPcompany());
			pst.setString(7, p_adminBO.getPaymentType());
			pst.setString(8, p_adminBO.getAssociateCode());
			cat.info(pst.toString());

			result = pst.executeUpdate();
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.saveAdminRegistration-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.saveAdminRegistration-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int saveVoucherEntry(VoucherBO p_voucherBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.saveVoucherEntry");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.insertVoucherEntry);
			pst.setString(1, p_voucherBO.getVno());
			pst.setString(2, p_voucherBO.getVname());
			pst.setString(3, "P");
			pst.setString(4, p_voucherBO.getVcostcenter());
			pst.setString(5, p_voucherBO.getVassoccode());
			String ss=p_voucherBO.getVamount().split(" ")[0].equals("$")?p_voucherBO.getVamount().split(" ")[1] : p_voucherBO.getVamount().split(" ")[0];
			pst.setString(6, ss);
			pst.setString(7, p_voucherBO.getVexpdate().equals("")?"99991212":TDSValidation.getDBdateFormat(p_voucherBO.getVexpdate()));
			pst.setString(8, p_voucherBO.getVdesc());
			pst.setString(9, p_voucherBO.getVuser());
			pst.setString(10, p_voucherBO.getVcode());
			pst.setString(11, p_voucherBO.getVcontact());
			pst.setString(12, p_voucherBO.getVdelay());
			pst.setString(13, p_voucherBO.getVfreq());
			pst.setString(14, p_voucherBO.getCompanyName());
			pst.setString(15, p_voucherBO.getCompanyAddress());
			pst.setString(16, p_voucherBO.getBillingAddress());
			pst.setString(17, p_voucherBO.getContactName());
			pst.setString(18, p_voucherBO.getContactTitle());
			pst.setString(19, p_voucherBO.getTelephoneNumber());
			pst.setString(20, p_voucherBO.getFaxNumber());
			pst.setString(21, p_voucherBO.getEmailAddress());
			pst.setInt(22, p_voucherBO.getNoTip());
			pst.setString(23, p_voucherBO.getFixedAmount());
			pst.setInt(24, p_voucherBO.getActiveOrInactive());
			pst.setString(25,p_voucherBO.getStartAmount());
			pst.setString(26,p_voucherBO.getRatePerMile());
			pst.setString(27, p_voucherBO.getDriverComments());
			pst.setString(28, p_voucherBO.getOperatorComments());
			pst.setBoolean(29,p_voucherBO.isOnlyForSR());
			pst.setString(30,p_voucherBO.getAllocateOnlyTo());
			pst.setString(31,p_voucherBO.getNotAllocateTo());
			pst.setString(32,p_voucherBO.getSpecialFlags());
			pst.setString(33, p_voucherBO.getInfokey());
			pst.setInt(34, p_voucherBO.getEmailStatus());
			pst.setInt(35, p_voucherBO.getCcProcessPercentage());
			cat.info(pst.toString());
			result = pst.executeUpdate();
			if(result >0 ) {
				cat.info("Success fully updated Voucher Entry");
			} else {
				cat.info("Failure to insert voucher Entry");
			}
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.saveVoucherEntry-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.saveVoucherEntry-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int UpdateVoucherEntry(VoucherBO voucherBO,String action,AdminRegistrationBO adminBO,String voucherNumber){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.UpdateVoucherEntry");
		int status=0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			if(action.equalsIgnoreCase("update")){
				pst = con.prepareStatement(TDSSQLConstants.updateVoucherEntry);
				pst.setString(1, voucherBO.getVname());
				pst.setString(2, "P");
				pst.setString(3, voucherBO.getVcostcenter());
				String ss=voucherBO.getVamount().split(" ")[0].equals("$")?voucherBO.getVamount().split(" ")[1] :voucherBO.getVamount().split(" ")[0];
				pst.setString(4, ss);
				pst.setString(5, TDSValidation.getDBdateFormat(voucherBO.getVexpdate()));
				pst.setString(6, voucherBO.getVdesc());
				pst.setString(7, voucherBO.getVuser());
				pst.setString(8, voucherBO.getVcode());
				pst.setString(9, voucherBO.getVcontact());
				pst.setString(10, voucherBO.getVdelay());
				pst.setString(11, voucherBO.getVfreq());
				pst.setString(12, voucherBO.getCompanyName());
				pst.setString(13, voucherBO.getCompanyAddress());
				pst.setString(14, voucherBO.getBillingAddress());
				pst.setString(15, voucherBO.getContactName());
				pst.setString(16, voucherBO.getContactTitle());
				pst.setString(17, voucherBO.getTelephoneNumber());
				pst.setString(18, voucherBO.getFaxNumber());
				pst.setString(19, voucherBO.getEmailAddress());
				pst.setString(20, voucherBO.getVoucherFormat());
				pst.setInt(21, voucherBO.getNoTip());
				pst.setString(22, voucherBO.getFixedAmount());
				pst.setInt(23, voucherBO.getActiveOrInactive());
				pst.setString(24,voucherBO.getStartAmount());
				pst.setString(25, voucherBO.getRatePerMile());
				pst.setString(26,voucherBO.getDriverComments());
				pst.setString(27, voucherBO.getOperatorComments());
				pst.setString(28, voucherBO.isOnlyForSR()?"0":"1");
				pst.setString(29, voucherBO.getAllocateOnlyTo());
				pst.setString(30, voucherBO.getNotAllocateTo());
				pst.setString(31, voucherBO.getSpecialFlags());
				pst.setString(32,voucherBO.getInfokey());
				pst.setInt(33, voucherBO.getEmailStatus());
				pst.setInt(34, voucherBO.getCcProcessPercentage());
				pst.setString(35, voucherBO.getVno());
				pst.setString(36,adminBO.getMasterAssociateCode());

				cat.info(pst.toString());
				//System.out.println("UPDATE QUERY::"+pst.toString());

				status=pst.executeUpdate();
			}
			if(action.equalsIgnoreCase("delete")){
				pst=con.prepareStatement(TDSSQLConstants.deleteVoucherEntry);
				pst.setString(1, voucherBO.getVno());
				pst.setString(2,adminBO.getAssociateCode());
				cat.info(pst.toString());

				//System.out.println("delete query::"+pst.toString());
				status=pst.executeUpdate();
			}
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.UpdateVoucherEntry-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.UpdateVoucherEntry-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}




	public static ArrayList getAllVoucher(VoucherBO voucherBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getAllVoucher");
		ArrayList voucher_data=new ArrayList();
		//TDSConnection dbcon = null;
		//Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		String query="";
		if(voucherBO.getVno()!=""){
			query+=" AND VC_VOUCHERNO ='"+voucherBO.getVno()+"'";
		}
		if(voucherBO.getVname()!=""){
			query+=" AND VC_RIDERNAME like '%"+voucherBO.getVname()+"%'";
		}
		if(voucherBO.getVcostcenter()!=""){
			query+=" AND VC_COSTCENTER = '"+voucherBO.getVcostcenter()+"'";
		}
		if(voucherBO.getFrom_date()!="" ){
			query+=" AND date_format(VC_CREATION_DATE,'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(voucherBO.getFrom_date())+"'";
		}
		if(voucherBO.getTo_date()!=""){
			query+=" AND date_format(VC_CREATION_DATE,'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(voucherBO.getTo_date())+"'";
		}
		if(voucherBO.getVcostcenter()!="" ){
			query+=" AND VC_COSTCENTER = '"+voucherBO.getVcostcenter()+"'";
		}


		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			pst = con.prepareStatement(TDSSQLConstants.getAllVoucherEntry+query);
			pst.setString(1, voucherBO.getVassoccode());
			cat.info(pst.toString());

			rs=pst.executeQuery();
			while(rs.next()){
				VoucherBO vdataBO = new VoucherBO();
				vdataBO.setVcostcenter(rs.getString("VC_COSTCENTER"));
				vdataBO.setVname(rs.getString("VC_RIDERNAME"));
				vdataBO.setVno(rs.getString("VC_VOUCHERNO"));
				vdataBO.setVstatus(rs.getString("VC_STATUS"));
				vdataBO.setVassoccode(voucherBO.getVassoccode());
				vdataBO.setVamount(rs.getString("VC_AMOUNT"));
				//vdataBO.setVexpdate(rs.getString("VC_EXPIRYDATE"));
				vdataBO.setVdesc(rs.getString("VC_DESCR"));
				vdataBO.setFrom_date(rs.getString("date"));
				vdataBO.setVcode(rs.getString("VC_CCODE"));
				vdataBO.setVcontact(rs.getString("VC_CONTACT"));
				vdataBO.setVdelay(rs.getString("VC_DELAYDAYS"));
				vdataBO.setVfreq(rs.getString("VC_FREQ"));
				vdataBO.setCompanyName(rs.getString("VC_COMPANYNAME"));
				vdataBO.setBillingAddress(rs.getString("VC_BILLING_ADDRESS"));
				vdataBO.setTelephoneNumber(rs.getString("VC_TELEPHONE_NUMBER"));
				vdataBO.setVexpdate(rs.getString("expiryDate"));
				vdataBO.setAllocateOnlyTo(rs.getString("VC_ALLOCATE_ONLY_TO"));
				vdataBO.setNotAllocateTo(rs.getString("VC_NOT_ALLOCATE_TO"));
				vdataBO.setEmailStatus((rs.getString("VC_EMAIL_STATUS")!=null && !rs.getString("VC_EMAIL_STATUS").equals(""))?rs.getInt("VC_EMAIL_STATUS"):0);
				vdataBO.setCcProcessPercentage((rs.getString("VC_CC_PERCENTAGE")!=null && !rs.getString("VC_CC_PERCENTAGE").equals(""))?rs.getInt("VC_CC_PERCENTAGE"):0);
				voucher_data.add(vdataBO);
			}
			rs.close();
			pst.close();
			con.close();
		} 
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getAllVoucher-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getAllVoucher-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucher_data;
	}


	public static VoucherBO getRecForVoucherNo(String p_id){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getRecForVoucherNo");
		VoucherBO voucherBo = new VoucherBO();
		PreparedStatement pst = null;
		ResultSet rs=null;
		String query="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		if(p_id!=null ){
			query = " WHERE VC_VOUCHERNO ='"+p_id+"'";
		}
		try{
			pst=con.prepareStatement(TDSSQLConstants.getVoucherEntry+query);
			cat.info(pst.toString());

			rs=pst.executeQuery();
			if(rs.next()){
				voucherBo.setVcostcenter(rs.getString("VC_COSTCENTER"));
				voucherBo.setVname(rs.getString("VC_RIDERNAME"));
				voucherBo.setVno(rs.getString("VC_VOUCHERNO"));
				voucherBo.setVstatus(rs.getString("VC_SWITH"));
				voucherBo.setVassoccode(rs.getString("VC_ASSOCCODE"));
				voucherBo.setVamount(rs.getString("VC_AMOUNT"));
				voucherBo.setVexpdate(TDSValidation.getUserDateFormat(rs.getString("VC_EXPIRYDATE")));
				voucherBo.setVdesc(rs.getString("VC_DESCR"));
				voucherBo.setFrom_date(rs.getString("date"));
				voucherBo.setVcode(rs.getString("VC_CCODE"));
				voucherBo.setVcontact(rs.getString("VC_CONTACT"));
				voucherBo.setVdelay(rs.getString("VC_DELAYDAYS"));
				voucherBo.setVfreq(rs.getString("VC_FREQ"));
				voucherBo.setCompanyName(rs.getString("VC_COMPANYNAME"));
				voucherBo.setCompanyAddress(rs.getString("VC_COMPANY_ADDRESS"));
				voucherBo.setBillingAddress(rs.getString("VC_BILLING_ADDRESS"));
				voucherBo.setContactName(rs.getString("VC_CONTACT_PERSON"));
				voucherBo.setContactTitle(rs.getString("VC_CONTACT_TITLE"));
				voucherBo.setTelephoneNumber(rs.getString("VC_TELEPHONE_NUMBER"));
				voucherBo.setFaxNumber(rs.getString("VC_FAX_NUMBER"));
				voucherBo.setEmailAddress(rs.getString("VC_EMAIL_ADDRESS"));
				voucherBo.setVoucherFormat(rs.getString("VC_FORMAT"));
				voucherBo.setNoTip(rs.getInt("VC_TIP_ALLOWED"));
				voucherBo.setFixedAmount(rs.getString("VC_FIXED_RATE"));
				voucherBo.setActiveOrInactive(rs.getInt("VC_STATUS"));
				voucherBo.setStartAmount(rs.getString("VC_START_AMOUNT"));
				voucherBo.setRatePerMile(rs.getString("VC_RATE_PER_MILE"));
				voucherBo.setDriverComments(rs.getString("VC_DRIVER_COMMENTS"));
				voucherBo.setOperatorComments(rs.getString("VC_OPERATOR_COMMENTS"));
				voucherBo.setOnlyForSR(rs.getBoolean("VC_SHARED_RIDE"));
				voucherBo.setAllocateOnlyTo(rs.getString("VC_ALLOCATE_ONLY_TO"));
				voucherBo.setNotAllocateTo(rs.getString("VC_NOT_ALLOCATE_TO"));
				voucherBo.setSpecialFlags(rs.getString("VC_SPECIAL_FLAGS"));
				voucherBo.setEmailStatus(rs.getInt("VC_EMAIL_STATUS"));
				voucherBo.setCcProcessPercentage((rs.getString("VC_CC_PERCENTAGE")!=null && !rs.getString("VC_CC_PERCENTAGE").equals(""))?rs.getInt("VC_CC_PERCENTAGE"):0);
			}

		}catch(SQLException sqex) {

			cat.error("TDSException RegistrationDAO.getRecForVoucherNo-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getRecForVoucherNo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucherBo;
	}


	public static int checkVoucherNo(String p_voucherno) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.checkVoucherNo");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.checkVoucherNo);
			pst.setString(1, p_voucherno);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.next()) {
				result = 1;
			}
			pst.close();
			con.close();
		} catch(SQLException sqex) {

			cat.error("TDSException RegistrationDAO.checkVoucherNo-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.checkVoucherNo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static String Tdsdriverkey() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.Tdsdriverkey");
		int result = 0;
		TDSConnection dbcon = null;
		String driverid="";
		Connection con = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			driverid=""+ConfigDAO.tdsKeyGen("DRIVERID", con);
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.Tdsdriverkey>"+sqex.getMessage());
			//	System.out.println("TDSException RegistrationDAO.Tdsdriverkey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverid;
	}
	public static ArrayList getpassengerDetails(passengerBO passenger,String f_name,String l_name,String passId,String city) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getpassengerDetails");
		ArrayList al_driverList = new ArrayList ();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = TDSSQLConstants.getPassengerDetails;
		try { 

			if(!f_name.equals("")) {
				f_name= f_name.concat("%");
				if(query.endsWith("TDS_PASSENGER")) {
					query =query+" WHERE PAS_FNAME LIKE'"+f_name+"' ";
				} else {
					query = query+" AND PAS_FNAME  LIKE'"+f_name+"' ";
				}
			}
			if(!l_name.equals("")){
				l_name =l_name.concat("%");
				if(query.endsWith("TDS_PASSENGER")) {
					query =query+" WHERE PAS_LNAME LIKE'"+l_name+"' ";
				} else {
					query = query+" AND PAS_LNAME LIKE'"+l_name+"' ";
				}
			}
			if(!passId.equals("")) {
				passId = passId.concat("%");
				if(query.endsWith("TDS_PASSENGER")) {
					query =query+" WHERE PAS_LOGIN_ID LIKE'"+passId+"' ";
				} else {
					query = query+" AND PAS_LOGIN_ID LIKE'"+passId+"' ";
				}
			}
			if(!city.equals("")) {
				city = city.concat("%");
				if(query.endsWith("TDS_PASSENGER")) {
					query =query+" WHERE PAS_CITY LIKE'"+city+"' ";
				} else {
					query = query+" AND PAS_CITY LIKE'"+city+"' ";
				}
			} 
			query = query+" LIMIT "+Integer.parseInt(passenger.getStart())+","+Integer.parseInt(passenger.getEnd());
			//System.out.println("query limit"+query);
			dbcon = new TDSConnection();
			con =dbcon.getConnection();
			pst = con.prepareStatement(query); 
			cat.info(pst.toString()); 
			rs = pst.executeQuery();
			while (rs.next()) {
				passenger = new passengerBO();
				passenger.setPass_key(rs.getString("PAS_KEY"));
				passenger.setFname(rs.getString("PAS_FNAME"));
				passenger.setLname(rs.getString("PAS_LNAME"));
				passenger.setAdd1(rs.getString("PAS_ADD1"));
				passenger.setAdd2(rs.getString("PAS_ADD2"));
				passenger.setCity(rs.getString("PAS_CITY"));
				passenger.setPassword(rs.getString("PAS_PWD"));
				passenger.setPayment_ac(rs.getString("PAS_PAYMENT_AC"));
				passenger.setPayment_type(rs.getString("PAS_PAYMENT_TYPE").toString().toCharArray()[0]);
				passenger.setPhone(rs.getString("PAS_PHNO"));
				passenger.setRepassword(rs.getString("PAS_PWD"));
				passenger.setUid(rs.getString("PAS_LOGIN_ID"));
				passenger.setZip(rs.getString("PAS_ZIP"));
				passenger.setState(rs.getString("PAS_STATE"));

				al_driverList.add(passenger);
			}
			rs.close();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getpassengerDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getpassengerDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_driverList;
	}


	public static passengerBO getPassengerDetails(String seq){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getpassengerDetails");
		passengerBO passenger = null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			passenger = new passengerBO();
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getPassengerEditDetails);
			pst.setInt(1, Integer.parseInt(seq));
			cat.info(pst.toString());
			rs = pst.executeQuery();	

			if(rs.next()){
				passenger.setPass_key(rs.getString("PAS_KEY"));
				passenger.setFname(rs.getString("PAS_FNAME"));
				passenger.setLname(rs.getString("PAS_LNAME"));
				passenger.setAdd1(rs.getString("PAS_ADD1"));
				passenger.setAdd2(rs.getString("PAS_ADD2"));
				passenger.setCity(rs.getString("PAS_CITY"));
				passenger.setPassword(rs.getString("PAS_PWD"));
				passenger.setPayment_ac(rs.getString("PAS_PAYMENT_AC"));
				passenger.setPayment_type(rs.getString("PAS_PAYMENT_TYPE").trim().toCharArray()[0]);
				passenger.setPhone(rs.getString("PAS_PHNO"));
				passenger.setRepassword(rs.getString("PAS_PWD"));
				passenger.setUid(rs.getString("PAS_LOGIN_ID"));
				passenger.setZip(rs.getString("PAS_ZIP"));
				passenger.setState(rs.getString("PAS_STATE"));

			}
			pst.close();
			con.close();
		} 
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getPassengerDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getpassengerDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return passenger;
	}


	public static ArrayList getDriverDetailSearch(DriverRegistrationBO driverBO, String assocode, String f_name,String l_name,String driverId,String cabNo,String status) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverDetailSearch");
		ArrayList al_driverList = new ArrayList ();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = TDSSQLConstants.getDriverSearch;

		if(!f_name.equals("")) {
			f_name= f_name.concat("%");
			query = query+" AND DR_FNAME  LIKE'"+f_name+"' ";
		}
		if(!l_name.equals("")){
			l_name =l_name.concat("%");
			query = query+" AND DR_LNAME LIKE'"+l_name+"' ";
		}
		if(!driverId.equals("")) {
			driverId = driverId.concat("%");
			query = query+" AND DR_USERID LIKE'"+driverId+"' ";
		}
		if(!cabNo.equals("")) {
			cabNo = cabNo.concat("%");
			query = query+" AND DR_CABNO LIKE'"+cabNo+"' ";
		} 
		if(!status.equals("")&& !status.equals("2")) { 
			query = query+" AND DR_ACTIVESTATUS='"+status+"' ";  
		} 
		else { 
			query = query+" ORDER BY ABS(DR_SNO)";  
		} 
		//query = query+"ORDER BY DR_SNO LIMIT "+Integer.parseInt(driverBO.getStart())+","+Integer.parseInt(driverBO.getEnd());

		dbcon = new TDSConnection();
		con =dbcon.getConnection();

		try { 
			pst = con.prepareStatement(query);
			pst.setString(1, assocode);
			//System.out.println("Query"+pst.toString());
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				driverBO = new DriverRegistrationBO();
				driverBO.setSno(rs.getString("DR_SNO"));
				driverBO.setUid(rs.getString("DR_USERID"));
				driverBO.setAdd1(rs.getString("DR_ADD1"));
				driverBO.setAdd2(rs.getString("DR_ADD2"));
				driverBO.setCity(rs.getString("DR_CITY"));
				driverBO.setState(rs.getString("DR_STATE"));
				driverBO.setZip(rs.getString("DR_ZIPCODE"));
				driverBO.setCmake(rs.getString("DR_CARMAKE"));
				driverBO.setCmodel(rs.getString("DR_CARMODEL"));
				driverBO.setCyear(rs.getString("DR_CARYEAR"));
				driverBO.setFname(rs.getString("DR_FNAME"));
				driverBO.setLname(rs.getString("DR_LNAME"));
				driverBO.setRoute(rs.getString("DR_BANKROUTING"));
				driverBO.setPhone(rs.getString("DR_PHONE"));
				driverBO.setCabno(rs.getString("DR_CABNO"));
				driverBO.setPassno(rs.getString("DR_PASSNO"));
				driverBO.setPhone(rs.getString("DR_PHONE"));
				driverBO.setStatus(rs.getString("DR_ACTIVESTATUS"));
				al_driverList.add(driverBO);
			}
			rs.close();
			pst.close();
		} catch(SQLException sqex) {

			cat.error("TDSException RegistrationDAO.getDriverDetailSearch-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException RegistrationDAO.getDriverDetailSearch-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_driverList;
	}

	public static ArrayList getDriverDetailSummary(DriverRegistrationBO driverBO, String assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverDetailSummary");
		ArrayList al_driverList = new ArrayList ();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con =dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getDriverSummary);
			pst.setString(1, assocode);
			pst.setInt(2, Integer.parseInt(driverBO.getStart()));
			pst.setInt(3,Integer.parseInt(driverBO.getEnd()));
			cat.info(pst.toString());

			rs = pst.executeQuery();
			while (rs.next()) {
				driverBO = new DriverRegistrationBO();
				driverBO.setSno(rs.getString("DR_SNO"));
				driverBO.setUid(rs.getString("DR_USERID"));
				driverBO.setAdd1(rs.getString("DR_ADD1"));
				driverBO.setAdd2(rs.getString("DR_ADD2"));
				driverBO.setCity(rs.getString("DR_CITY"));
				driverBO.setState(rs.getString("DR_STATE"));
				driverBO.setZip(rs.getString("DR_ZIPCODE"));
				driverBO.setCmake(rs.getString("DR_CARMAKE"));
				driverBO.setCmodel(rs.getString("DR_CARMODEL"));
				driverBO.setCyear(rs.getString("DR_CARYEAR"));
				driverBO.setFname(rs.getString("DR_FNAME"));
				driverBO.setLname(rs.getString("DR_LNAME"));
				driverBO.setRoute(rs.getString("DR_BANKROUTING"));
				driverBO.setPhone(rs.getString("DR_PHONE"));
				driverBO.setCabno(rs.getString("DR_CABNO"));
				driverBO.setPassno(rs.getString("DR_PASSNO"));
				driverBO.setPhone(rs.getString("DR_PHONE"));
				al_driverList.add(driverBO);
			}
			rs.close();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getDriverDetailSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverDetailSummary-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_driverList;
	}

	public static int totalRecordLength(String Assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.totalRecordLength");
		int trl = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getTotalRecordLengthDR);
			pst.setString(1, Assocode);
			cat.info(pst.toString());

			rs=pst.executeQuery();
			if(rs.next()){
				trl= rs.getInt(1);
				cat.info("the total recored length = "+trl);
			}
		}
		catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.totalRecordLength-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.totalRecordLength-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return trl;
	}

	public static int totalpasengerRecord(){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.totalpasengerRecord");
		int trl = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getTotalRecordPassenger);
			rs=pst.executeQuery();
			if(rs.next()){
				trl= rs.getInt(1);
				cat.info("the total recored length = "+trl);
			}
		}
		catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.totalpasengerRecord-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.totalpasengerRecord-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return trl;
	}

	public static DriverRegistrationBO getDriverDetailBySeq(String seq,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverDetailBySeq");
		DriverRegistrationBO driverBO = null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			driverBO = new DriverRegistrationBO();
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getDriverRecForEdit);
			pst.setInt(1, Integer.parseInt(seq));
			pst.setString(2, assocode);
			cat.info(pst.toString());

			rs = pst.executeQuery();			
			if(rs.next()){
				driverBO.setIsDataAvailable(1);
				driverBO.setRegistration('D');
				driverBO.setSno(rs.getString("DR_SNO"));
				driverBO.setUid(rs.getString("DR_USERID"));
				driverBO.setAdd1(rs.getString("DR_ADD1"));
				driverBO.setAdd2(rs.getString("DR_ADD2"));
				driverBO.setCity(rs.getString("DR_CITY"));
				driverBO.setState(rs.getString("DR_STATE"));
				driverBO.setZip(rs.getString("DR_ZIPCODE"));
				driverBO.setCmake(rs.getString("DR_CARMAKE"));
				driverBO.setCmodel(rs.getString("DR_CARMODEL"));
				driverBO.setCyear(rs.getString("DR_CARYEAR"));
				driverBO.setFname(rs.getString("DR_FNAME"));
				driverBO.setLname(rs.getString("DR_LNAME"));
				driverBO.setRoute(rs.getString("DR_BANKROUTING"));
				driverBO.setPhone(rs.getString("DR_PHONE"));
				driverBO.setPhoneExt(rs.getString("DR_PHONEXT"));
				driverBO.setDriExten(rs.getString("DR_DRIVEREXT"));
				driverBO.setFdeposit(rs.getString("DR_FREQ_OF_DEPOSIT"));
				driverBO.setAcct(rs.getString("DR_BANKACCT"));
				driverBO.setStatus(rs.getString("DR_ACTIVESTATUS"));
				driverBO.setEhostid(rs.getString("DR_SMSID"));
				driverBO.setCabno(rs.getString("DR_CABNO"));
				driverBO.setPassno(rs.getString("DR_PASSNO"));
				driverBO.setPassword(rs.getString("DR_PASSWORD"));
				driverBO.setRepassword(rs.getString("DR_PASSWORD"));
				driverBO.setPayUserId(rs.getString("DRIVER_PAY_ID"));
				driverBO.setPayPassword(rs.getString("DRIVER_PAY_PASSWORD"));
				driverBO.setPayMerchant(rs.getString("DRIVER_PAY_MERCHAND"));
				driverBO.setPayRePassword(rs.getString("DRIVER_PAY_PASSWORD"));
				driverBO.setDrLicense(rs.getString("DR_DRIVER_LICENSE"));
				driverBO.setDrLicenseExpiry(rs.getString("DR_DRIVER_LICENSE_EXPIRYDATE"));
				driverBO.setHkLicense(rs.getString("DR_HACK_LICENSE"));
				driverBO.setHkLicenseExpiry(rs.getString("DR_HACK_LICENSE_EXPIRYDATE"));
				driverBO.setSocialSecurityNo(rs.getString("AU_SOCIALSECURITY_NO"));
				driverBO.setTemplate(rs.getInt("DR_PROGRAM_TMPLT"));
				driverBO.setDrProfile(rs.getString("DR_FLAG"));
				driverBO.setDriverRatings(rs.getInt("DR_RATING"));
				driverBO.setSettingsAccess(rs.getInt("DR_SETTINGS_ACCESS"));
				String drProfile =  rs.getString("DR_FLAG");
				driverBO.setDr_details_json((rs.getString("DR_DETAILS_JSON")!=null)?rs.getString("DR_DETAILS_JSON"):"");
				driverBO.setEmailId((rs.getString("DR_EMAIL_ID")!=null)?rs.getString("DR_EMAIL_ID"):"");
				ArrayList al_list = new ArrayList();
				for(int i=0;i< drProfile.toCharArray().length;i++)
				{
					al_list.add(drProfile.toCharArray()[i]);
				}

				driverBO.setAl_list(al_list); 


			}
			pst.close();
			con.close();
		} 
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.gRegistrationDAO.getDriverDetailBySeqetDriverDetailBySeq-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverDetailBySeq-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} 
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverBO;
	}


	public static int editUpdateDriverDetail(DriverRegistrationBO e_driverBO,AdminRegistrationBO adminBO, int pass){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.editUpdateDriverDetail");
		int status = 0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		try{
			if(pass==1){
				pst = con.prepareStatement(TDSSQLConstants.updateDriverManager);
				pst.setString(30, e_driverBO.getSno());
				pst.setString(31,adminBO.getAssociateCode());
			}
			else{
				pst = con.prepareStatement(TDSSQLConstants.updateDriverManagerwithPassword); 
				pst.setString(30, e_driverBO.getPassword());
				pst.setString(31, e_driverBO.getSno());
				pst.setString(32,adminBO.getAssociateCode());
			}
			pst.setString(1, e_driverBO.getFname());
			pst.setString(2, e_driverBO.getLname());
			pst.setString(3, e_driverBO.getAdd1());
			pst.setString(4, e_driverBO.getAdd2());
			pst.setString(5, e_driverBO.getCity());
			pst.setString(6, e_driverBO.getState());
			pst.setString(7, e_driverBO.getZip());
			pst.setString(8, "");
			pst.setString(9, e_driverBO.getAcct());
			pst.setString(10, e_driverBO.getPhone());
			pst.setString(11, "");
			pst.setString(12, e_driverBO.getCmake());
			pst.setString(13, e_driverBO.getCyear());
			pst.setString(14, e_driverBO.getCmodel());
			pst.setString(15, e_driverBO.getDriExten().equals("")?"1-888":e_driverBO.getDriExten());
			pst.setString(16, e_driverBO.getFdeposit());
			pst.setString(17, e_driverBO.getStatus());
			pst.setString(18, e_driverBO.getEhostid());
			pst.setString(19, e_driverBO.getCabno());
			pst.setString(20, e_driverBO.getPassno());
			pst.setString(21, e_driverBO.getDrProfile());
			pst.setString(22, e_driverBO.getDrLicense());
			pst.setString(23, e_driverBO.getDrLicenseExpiry());
			pst.setString(24, e_driverBO.getHkLicense());
			pst.setString(25, e_driverBO.getHkLicenseExpiry());
			pst.setString(26, e_driverBO.getTemplate()+"");
			pst.setInt(27, e_driverBO.getDriverRatings());
			pst.setInt(28, e_driverBO.getSettingsAccess());
			pst.setString(29, e_driverBO.getEmailId());
			status=pst.executeUpdate();
			pst.close();
			if(pass==1){
				pst = con.prepareStatement(TDSSQLConstants.UpdateAdminDetailForDriver);
				pst.setString(8, e_driverBO.getSno());
				pst.setString(9,adminBO.getAssociateCode());
			}
			else{
				pst = con.prepareStatement(TDSSQLConstants.UpdateAdminDetailForDriverWithPassword);
				pst.setString(8, e_driverBO.getPassword());
				pst.setString(9, e_driverBO.getSno());
				pst.setString(10,adminBO.getAssociateCode());
			}
			pst.setString(1, e_driverBO.getStatus());
			pst.setString(2, e_driverBO.getFname());
			pst.setString(3, e_driverBO.getLname());
			pst.setString(4, e_driverBO.getPayUserId());
			pst.setString(5, e_driverBO.getPayPassword());
			pst.setString(6, e_driverBO.getPayMerchant()); 
			pst.setString(7, e_driverBO.getSocialSecurityNo());
			status=pst.executeUpdate();
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.editUpdateDriverDetail-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.editUpdateDriverDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}




	public static void InsertLoginDetail(String assocode,String uname,String sessionid,String insertOrupdate,int auto,long lastAccessed,String provider,String phno,String Version,String driverFlag,String cabNo, String googRegKey,String ipAddress,String masterAssoccode,String mobileVersion) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.InsertLoginDetail");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		int result,rsult1=0;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(insertOrupdate.equals("1")){
				pst = con.prepareStatement(TDSSQLConstants.insertLogindetails);
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//				Date resultdate = new Date(lastsessionTime);
				pst.setString(1,uname);
				pst.setString(2,sessionid);
				pst.setString(3,assocode);
				pst.setString(4,phno);
				pst.setString(5,provider);
				pst.setString(6,Version);
				pst.setString(7,driverFlag); 
				pst.setString(8, cabNo);
				pst.setString(9, googRegKey);
				pst.setString(10,ipAddress);
				pst.setString(11,masterAssoccode);
				pst.setString(12, mobileVersion);
				cat.info(pst.toString());

				pst.executeUpdate();
			} else {
				if(auto == 0) {
					pst = con.prepareStatement(TDSSQLConstants.UpdateLoginDetails);
					pst.setString(1, Version);
					pst.setString(2,sessionid);
				} else {
					pst = con.prepareStatement(TDSSQLConstants.UpdateAutoLoginDetails);
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date resultdate = new Date(lastAccessed);
					pst.setString(1,sdf.format(resultdate));
					pst.setString(2, Version);
					pst.setString(3,sessionid);
				} 
				cat.info(pst.toString());

				pst.executeUpdate();

				//Moving data fromLogin table to login history
				pst = con.prepareStatement("insert into TDS_LOGIN_HISTORY (SELECT * FROM TDS_LOGINDETAILS WHERE LO_SESSIONID = ? and LO_ASSCODE=  ? )");
				pst.setString(1,  sessionid);
				pst.setString(2,  assocode); 
				cat.info(pst.toString());
				pst.executeUpdate();

				//Deleting data from login table	
				pst = con.prepareStatement("DELETE FROM TDS_LOGINDETAILS  WHERE  LO_SESSIONID= ? and LO_ASSCODE=  ?");
				pst.setString(1,  sessionid);
				pst.setString(2,  assocode);
				cat.info(pst.toString());

				rsult1 = pst.executeUpdate();
				//System.out.println("delete  query::"+pst.toString()); 
			} 

			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.InsertLoginDetail-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.InsertLoginDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}



	/*public static int getDriverActive(AdminRegistrationBO adminbo) {
		int result=0;
		cat.info("In Check getDriverStatus Method");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst,pst1 =null;
		ResultSet rs,rs1=null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection(); 
			pst = con.prepareStatement("SELECT * FROM TDS_DRIVER_DEACTIVATION where DATE_FORMAT(DD_TO_DATE,'%m-%d-%y') <= DATE_FORMAT(now(),'%m-%d-%y') and  TIME_FORMAT(DD_TO_TIME,'%H:%i') <= TIME_FORMAT(NOW(), '%H:%i')  and DD_DRIVER_ID=? and   DD_ASSOCCODE=?");
			pst.setString(1, adminbo.getUid());
			pst.setString(2, adminbo.getAssociateCode());
			rs = pst.executeQuery();
			if(rs.first()) {
				result =1; 
			}  else {
				pst1 = con.prepareStatement("DELETE from TDS_DRIVER_DEACTIVATION where DD_DRIVER_ID=? and DD_ASSOCCODE=?");
				pst1.setString(1,rs.getString("DD_DRIVER_ID"));
				pst1.setString(2,rs.getString("DD_ASSOCCODE"));
				pst1.execute();

			}
		}catch (SQLException sqlex) {
			System.out.println("Error in getdriverstatus"+sqlex.getMessage()); 
		} finally {
			dbcon.closeConnection();
		}
		return  result;
	}
	 */
	public static AdminRegistrationBO getUserAvailable(String p_userid,String p_password,String provider,String phno,String cabNo) throws SystemUnavailableException {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getUserAvailable");
		AdminRegistrationBO adminBO = new AdminRegistrationBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs = null,rs1 = null,rs2=null;
		boolean adminUserIsNotEmpty = false;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try{
			pst = con.prepareStatement(TDSSQLConstants.isUserAvailable);
			pst.setString(1, p_userid);
			pst.setString(2, p_password);
			//	pst.setString(2, com.tds.controller.PasswordService.getInstance().encrypt(p_password));
			//System.out.println("Query in mLoging "+pst);
			cat.info("Query in mLoging "+pst);
			rs = pst.executeQuery();
			if(rs.next()) {  
				adminBO.setUname(rs.getString("AU_USERNAME"));
				adminBO.setUserNameDisplay(rs.getString("AU_FNAME").concat(" "+rs.getString("AU_LNAME"))); 
				adminBO.setUid(rs.getString("AU_SNO"));
				adminBO.setPhnumber(phno);
				adminBO.setProvider(provider);
				adminBO.setPaymentType(rs.getString("AU_PAYMENTTYPE"));
				adminBO.setPcompany(rs.getString("AU_PARENTCOMPANY"));
				adminBO.setUsertype(rs.getString("AU_USERTYPE"));
				adminBO.setUsertypeDesc(rs.getString("AU_USERTYPE"));
				adminBO.setAssociateCode(rs.getString("AU_ASSOCCODE"));
				adminBO.setMasterAssociateCode(rs.getString("AU_MASTER_ASSOCCODE"));
				adminBO.setPaymentUserID(rs.getString("DRIVER_PAY_ID")!=null&&rs.getString("DRIVER_PAY_ID")!=""?rs.getString("DRIVER_PAY_ID"):"");
				adminBO.setPayPassword(rs.getString("DRIVER_PAY_PASSWORD")!=null&&rs.getString("DRIVER_PAY_PASSWORD")!=""?rs.getString("DRIVER_PAY_PASSWORD"):"");
				adminBO.setServiceKey(rs.getString("CD_SERVICE_KEY"));
				adminBO.setPassword(rs.getString("AU_PASSWORD"));
				adminBO.setMerchid(rs.getString("CD_MERCHANTPROFILE"));
				adminBO.setServiceid(rs.getString("CD_SERVICEID"));
				adminBO.setPhnumber(rs.getString("DR_PHONE")==null?phno:rs.getString("DR_PHONE"));
				adminBO.setDriverRating(rs.getInt("DR_RATING"));
				adminBO.setDriver_flag(rs.getString("DR_FLAG")==null?"":rs.getString("DR_FLAG"));
				adminBO.setSettingsAccess(rs.getInt("DR_SETTINGS_ACCESS"));
				adminBO.setIsAvailable(1);
				adminBO.setAndroidId(rs.getString("DR_AID")==null?"":rs.getString("DR_AID"));
				adminBO.setJson_details(rs.getString("DR_DETAILS_JSON")==null?"":rs.getString("DR_DETAILS_JSON"));
				adminBO.setWasl_reference_id(rs.getString("DR_REFERENCE_NO")==null?"":rs.getString("DR_REFERENCE_NO"));
				//Following line picks up the default state where the company operates to enable doing address
				//verification in the OpenRequest/Job screen
				String place=SystemPropertiesDAO.getParameter(adminBO.getAssociateCode(),"State" );
				adminBO.setState(place);
				double timeZoneOffset = 0.0;
				adminBO.setTimeZone(SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(),"timeZoneArea"));
				try {
					timeZoneOffset = Double.parseDouble(SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(),"TimeZone" ));
				} catch (Exception e){
					e.printStackTrace();
				}
				adminBO.setTimeZoneOffet(timeZoneOffset);
				adminUserIsNotEmpty = true;
			}
			if(adminUserIsNotEmpty){
				if(rs.getString("AU_USERTYPE").equalsIgnoreCase("Driver")) {
					cat.info("driver flag condition:>>>>>>::");
					pst1 = con.prepareStatement("SELECT DR_FLAG FROM TDS_DRIVER  where DR_ASSOCODE= ? AND DR_USERID= ? AND DR_PASSWORD= ?");
					pst1.setString(1,rs.getString("AU_MASTER_ASSOCCODE"));
					pst1.setString(2, p_userid);
					pst1.setString(3, p_password);
					cat.info(pst1.toString());

					rs1 =pst1.executeQuery();
					if(rs1.next()) { 
						adminBO.setDriver_flag(rs1.getString("DR_FLAG")); 
					} 
				} 
				pst = con.prepareStatement("select * from TDS_SECURITY_ACCESS where S_UID= ? and S_ACESS=1");
				pst.setString(1, rs.getString("AU_SNO"));
				cat.info(pst.toString());

				rs = pst.executeQuery(); 
				if(rs.first()) {
					ArrayList roles= new ArrayList(); 
					do
					{
						roles.add(rs.getString("S_MODULE_NO"));
					}while(rs.next());
					adminBO.setRoles(roles);
				}
			}
			if(adminBO.getAssociateCode() != null && adminBO.getAssociateCode().length() > 0) {
				pst = con.prepareStatement(TDSSQLConstants.getDispatchStatus);
				pst.setString(1, adminBO.getAssociateCode());
				rs = pst.executeQuery();
				if(rs.next()) {
					adminBO.setDispatchMethod(rs.getString("CD_DISPATCH_SWITH"));
				}
			}

		}
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getUserAvailable method -->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getUserAvailable method--> "+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			try {
				if(rs != null) rs.close();
				if(pst != null) pst.close();
				if(con != null)  con.close();
				if(dbcon != null) dbcon.closeConnection();
			} catch (Exception sqex) {}
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return adminBO;
	}

	public static int saveCompanyMaster(CompanyMasterBO companyBO,String Updateorinsert,String assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.saveCompanyMaster");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(Updateorinsert.equalsIgnoreCase("insert")){
				pst = con.prepareStatement(TDSSQLConstants.insertCompanyMaster);
				pst.setString(15, companyBO.getEmail());
				pst.setString(14,""+ConfigDAO.tdsKeyGen("ASSOCIATION_CODE", con));
			} else {
				pst = con.prepareStatement(TDSSQLConstants.UPdateCompanyMaster);
				pst.setString(15,assocode);
				pst.setString(14, companyBO.getEmail());
			}
			pst.setString(1, companyBO.getCname());
			pst.setString(2, companyBO.getCdesc());
			pst.setString(3, companyBO.getAdd1());
			pst.setString(4, companyBO.getAdd2());
			pst.setString(5, companyBO.getCity());
			pst.setString(6, companyBO.getState());
			pst.setString(7, companyBO.getZip());
			pst.setString(8, companyBO.getPhoneno());
			pst.setString(9, companyBO.getBankacno());
			pst.setString(10, companyBO.getBankroutingno());
			pst.setString(11, companyBO.getBankname());
			pst.setString(12, companyBO.getDipatchStatus()+"");
			pst.setString(13, companyBO.getFileid());
			cat.info(pst.toString());

			result = pst.executeUpdate();

			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.saveCompanyMaster-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSEXception RegistrationDAO.saveCompanyMaster method--> "+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;	
	}
	/*public static int saveAdminRegistration(AdminRegistrationBO p_adminBO,String updateorinsert) {
		cat.info("In Admin Registration DAO method insert Admin detail");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		//String userType[] = p_adminBO.getUsertype().split("/");
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(updateorinsert.equalsIgnoreCase("insert")) {
				pst = con.prepareStatement(TDSSQLConstants.insertAdminDetail);
				if(p_adminBO.getUsertype().equals("Driver")){
				 pst.setString(1,p_adminBO.getAssociateCode()+""+p_adminBO.getDrid());
				 pst.setString(2, p_adminBO.getUname());
					pst.setString(3, p_adminBO.getPassword());
					pst.setString(4, p_adminBO.getUsertype());
					pst.setString(5, "1");
					pst.setString(6, " ");
					pst.setString(7, " ");
					pst.setString(8, p_adminBO.getAssociateCode());
					pst.setString(9, p_adminBO.getEmail());
					pst.setString(10,p_adminBO.getFname());
					pst.setString(11,p_adminBO.getLname());
					//pst.setString(12,p_adminBO.getUid());
				} else {
					pst.setString(1,p_adminBO.getAssociateCode());
				}

			} else {
				pst = con.prepareStatement(TDSSQLConstants.UpdateAdminDetail);
				pst.setString(1, p_adminBO.getUname());
				pst.setString(2, p_adminBO.getPassword());
				pst.setString(3, p_adminBO.getUsertype());
				pst.setString(4, "1");
				pst.setString(5, " ");
				pst.setString(6, " ");
				pst.setString(7, p_adminBO.getAssociateCode());
				pst.setString(8, p_adminBO.getEmail());
				pst.setString(9,p_adminBO.getFname());
				pst.setString(10,p_adminBO.getLname());
				pst.setString(11,p_adminBO.getUid());
			}

			result = pst.executeUpdate();
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			sqex.printStackTrace();
			cat.info("Error in update driver details "+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		return result;
	}*/
	public static int saveAdminRegistration(AdminRegistrationBO p_adminBO,String updateorinsert,int value) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.saveAdminRegistration");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		//String userType[] = p_adminBO.getUsertype().split("/");
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(updateorinsert.equalsIgnoreCase("insert")) {
				pst = con.prepareStatement(TDSSQLConstants.insertAdminDetail);
				/*if(p_adminBO.getUsertype().equals("Driver")){*/

				//pst.setString(1,p_adminBO.getAssociateCode()+""+p_adminBO.getDrid());
				pst.setString(1, p_adminBO.getUname());    
				pst.setString(2, p_adminBO.getUname());
				pst.setString(3, p_adminBO.getPassword());
				pst.setString(4, p_adminBO.getUsertype());
				pst.setString(5, "1");
				pst.setString(6, p_adminBO.getActive());
				pst.setString(7, " ");
				pst.setString(8, " ");
				pst.setString(9, p_adminBO.getAssociateCode());
				pst.setString(10, p_adminBO.getEmail());
				pst.setString(11,p_adminBO.getFname());
				pst.setString(12,p_adminBO.getLname());
				pst.setString(13,p_adminBO.getMasterAssociateCode());
				p_adminBO.setUid(p_adminBO.getAssociateCode()+""+p_adminBO.getDrid());

				//pst.setString(12,p_adminBO.getUid());
				/*	} else {
					pst.setString(1,p_adminBO.getAssociateCode());
				}
				 */	
			} else if(updateorinsert.equalsIgnoreCase("update")&&value==2) {
				pst = con.prepareStatement(TDSSQLConstants.UpdateAdminDetail);

				pst.setString(1, p_adminBO.getUname());
				pst.setString(2, p_adminBO.getPassword());
				pst.setString(3, p_adminBO.getUsertype());
				pst.setString(4,p_adminBO.getActive());
				pst.setString(5, "1");
				pst.setString(6, " ");
				pst.setString(7, " ");
				pst.setString(8, p_adminBO.getAssociateCode());
				pst.setString(9, p_adminBO.getEmail());
				pst.setString(10,p_adminBO.getFname());
				pst.setString(11,p_adminBO.getLname());
				pst.setString(12,p_adminBO.getUid());
			} else if(updateorinsert.equalsIgnoreCase("update")&&value==1) {
				pst = con.prepareStatement("update TDS_ADMINUSER set AU_USERNAME=?,AU_USERTYPE=?,AU_ACTIVE=?,AU_ACCESS=?,AU_PARENTCOMPANY=?,AU_PAYMENTTYPE=?,AU_ASSOCCODE=?,AU_EMAIL=?,AU_FNAME=?,AU_LNAME=? where AU_SNO=?");

				pst.setString(1, p_adminBO.getUname());
				pst.setString(2, p_adminBO.getUsertype());
				pst.setString(3,p_adminBO.getActive());
				pst.setString(4, "1");
				pst.setString(5, " ");
				pst.setString(6, " ");
				pst.setString(7, p_adminBO.getAssociateCode());
				pst.setString(8, p_adminBO.getEmail());
				pst.setString(9,p_adminBO.getFname());
				pst.setString(10,p_adminBO.getLname());
				pst.setString(11,p_adminBO.getUid());
			} 
			cat.info(pst.toString());
			result = pst.executeUpdate();
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.saveAdminRegistration-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.saveAdminRegistration method--> "+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}


	public static ArrayList AdminMasterview(String Assocode,String uid,String listorone,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.AdminMasterview");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		ResultSet rs=null;
		ArrayList array_list=new ArrayList();
		String query="";
		PreparedStatement pst = null;


		if(listorone.equalsIgnoreCase("list")){
			if(!(adminBO.getFname()).equals("")){
				query = " AND AU_FNAME like '%"+adminBO.getFname()+"%'";
			}
			if(!(adminBO.getLname()).equals("")){
				query = query + " AND AU_LNAME like '%"+adminBO.getLname()+"%'";
			}
			if(!(adminBO.getUname()).equals("")){
				query = query + " AND AU_USERNAME like '%"+adminBO.getUname()+"%'";
			}
		}
		query=query+" ORDER BY AU_SNO";

		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(listorone.equalsIgnoreCase("list")){

				pst = con.prepareStatement("select * from TDS_ADMINUSER where AU_ASSOCCODE=? and AU_USERTYPE !='Driver'"+query);
				pst.setString(1, Assocode);
			} else if(listorone.equalsIgnoreCase("one")){
				pst = con.prepareStatement("select * from TDS_ADMINUSER where AU_SNO=?");
				pst.setString(1, uid);
			}
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				AdminRegistrationBO adminBo=new AdminRegistrationBO();
				adminBo.setUid(rs.getString("AU_SNO").trim());
				adminBo.setUname(rs.getString("AU_USERNAME").trim());
				adminBo.setPassword(rs.getString("AU_PASSWORD").trim());
				adminBo.setUsertype(rs.getString("AU_USERTYPE").trim());
				adminBo.setAssociateCode(rs.getString("AU_ASSOCCODE").trim());
				adminBo.setPcompany(rs.getString("AU_PARENTCOMPANY"));
				adminBo.setPaymentType(rs.getString("AU_PAYMENTTYPE"));
				adminBo.setFname(rs.getString("AU_FNAME").trim());
				adminBo.setLname(rs.getString("AU_LNAME").trim());
				adminBo.setActive(rs.getString("AU_ACTIVE").trim());
				adminBo.setEmail(rs.getString("AU_EMAIL").trim());
				adminBo.setDriverreg(rs.getString("AU_DRIVERREGENTRY").trim());
				adminBo.setEffectiveDate(rs.getString("AU_DRIVERAVAILABILITY").trim());
				adminBo.setMerchid(rs.getString("DRIVER_PAY_MERCHAND"));
				array_list.add(adminBo);
			}
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("Error in RegistrationDAO.AdminMasterview"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("Error in RegistrationDAO.AdminMasterview method "+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return array_list;	
	}


	/**
	 * @param p_penaltyBO
	 * @return boolean true or false. true for insert successfully. false means failed to insert
	 * @author vimal
	 * @see Description
	 * This method is used to update the driver penalty.
	 */
	public static boolean insertDriverBehavior(DriverBehaviorBO p_beahaviorBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertDriverBehavior");
		boolean m_insertStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.insertBehavior);
			m_pst.setString(1, p_beahaviorBO.getBehaviorType()+ConfigDAO.tdsKeyGen("PERFORMANCENO", m_conn));
			m_pst.setString(2, p_beahaviorBO.getDriverId());
			m_pst.setString(3, p_beahaviorBO.getBehaviorType());
			m_pst.setString(4, p_beahaviorBO.getComment());
			m_pst.setString(5, p_beahaviorBO.getAmount());
			m_pst.setString(6, TDSValidation.getDBdateFormat(p_beahaviorBO.getBehaviorDate()));
			m_pst.setString(7, p_beahaviorBO.getAssocCode());
			//System.out.println("Query in insert Driver Behavior Method "+m_pst);
			cat.info(m_pst.toString());
			if(m_pst.executeUpdate() > 0) {
				m_insertStatus = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error("Error in RegistrationDAO.insertDriverBehavior-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("Error in RegistrationDAO.insertDriverBehavior--> "+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_insertStatus;
	}



	/**
	 * @param p_driverId
	 * @return List all the Behavior List.
	 * @author vimal
	 * @see Description 
	 * This method is used to retrieve all the penalty.
	 */
	public static List getBehaviorList (Map p_criteriaMap) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getBehaviorList");
		List m_behaviorList = null;
		DriverBehaviorBO m_behaviorBO = null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String m_queryAppend = "";
		if(p_criteriaMap != null ) {
			if(p_criteriaMap.containsKey("type")) {
				m_queryAppend += " AND DB_BEHAVIORTYPE LIKE '" + p_criteriaMap.get("type") +"'";
			}
			if(p_criteriaMap.containsKey("date")) {
				m_queryAppend += " AND DB_BEHAVIORDATE LIKE '" + TDSValidation.getDBdateFormat(p_criteriaMap.get("date").toString()) +"'";
			}
		}
		ResultSet m_rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getBehavior + m_queryAppend);
			cat.info("Query in Behavior "+m_pst);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_behaviorList = new ArrayList();
				do {
					m_behaviorBO = new DriverBehaviorBO();
					m_behaviorBO.setBehaviorKey(m_rs.getString("DB_BEHAVIORNO"));
					m_behaviorBO.setComment(m_rs.getString("DB_COMMENT"));
					m_behaviorBO.setBehaviorType(m_rs.getString("DB_BEHAVIORTYPE"));
					m_behaviorBO.setAmount(m_rs.getString("DB_AMOUNT"));
					m_behaviorBO.setDriverId(m_rs.getString("DR_USERID"));
					m_behaviorBO.setBehaviorDate(TDSValidation.getUserDateFormat(m_rs.getString("DB_BEHAVIORDATE")));
					m_behaviorList.add(m_behaviorBO);
				} while (m_rs.next());
			}

		} catch (SQLException sqex) {
			cat.error("Error in RegistrationDAO.getBehaviorList-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("Error in RegistrationDAO.getBehaviorList--> "+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_behaviorList;
	}

	public static DriverBehaviorBO getBehaviorByNo (String p_behaviorNo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getBehaviorByNo");
		DriverBehaviorBO m_behaviorBO = null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getBehaviorByNo);
			m_pst.setString(1, p_behaviorNo);
			cat.info("Query in Behavior "+m_pst);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_behaviorBO = new DriverBehaviorBO();
				m_behaviorBO.setBehaviorKey(m_rs.getString("DB_BEHAVIORNO"));
				m_behaviorBO.setComment(m_rs.getString("DB_COMMENT"));
				m_behaviorBO.setBehaviorType(m_rs.getString("DB_BEHAVIORTYPE"));
				m_behaviorBO.setAmount(m_rs.getString("DB_AMOUNT"));
				m_behaviorBO.setBehaviorDate(TDSValidation.getUserDateFormat(m_rs.getString("DB_BEHAVIORDATE")));

			}

		} catch (SQLException sqex) {
			cat.error("Error in RegistrationDAO.getBehaviorByNo->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("Error in RegistrationDAO.getBehaviorByNo-> "+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_behaviorBO;
	}

	public static boolean updateDriverBehavior(DriverBehaviorBO p_behaviorBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateDriverBehavior");
		boolean m_updateStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateBehavior);
			m_pst.setString(1, p_behaviorBO.getAmount());
			m_pst.setString(2, p_behaviorBO.getComment());
			m_pst.setString(3, p_behaviorBO.getBehaviorKey());
			cat.info("Query in Behavior "+m_pst);
			if(m_pst.executeUpdate() > 0 ) {
				m_updateStatus = true;
			}
		} catch (SQLException sqex) {
			cat.error("Error in RegistrationDAO.updateDriverBehavior->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("Error in RegistrationDAO.updateDriverBehavior-> "+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_updateStatus;
	}

	public static boolean insertCabMaintenance(CabMaintenanceBO p_maintenanceBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertCabMaintenance");
		boolean m_insertStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.insertMaintenance);
			m_pst.setString(1, ConfigDAO.tdsKeyGen("CAB_MAINTENANCE", m_conn)+"");
			m_pst.setString(2, p_maintenanceBO.getDriverKey());
			m_pst.setString(3, p_maintenanceBO.getAssocCode());
			m_pst.setString(4, p_maintenanceBO.getServiceCode());
			m_pst.setString(5, p_maintenanceBO.getAmount());
			m_pst.setString(6, TDSValidation.getDBdateFormat(p_maintenanceBO.getServiceDate()));
			m_pst.setString(7, p_maintenanceBO.getServiceDesc());
			cat.info("Query in insertCabMaintenance Method "+m_pst);
			if(m_pst.executeUpdate() > 0) {
				m_insertStatus = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			//System.out.println("TDSException RegistrationDAO.insertCabMaintenance--> "+sqex.getMessage());
			cat.error(m_pst.toString());
			cat.error("TDSException RegistrationDAO.insertCabMaintenance-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_insertStatus;
	}

	public static List getCabMaintenanceList (Map p_criteriaMap,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCabMaintenanceList");
		List m_cabMaintenanceList = null;
		CabMaintenanceBO m_cabMaintenanceBO = null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String m_queryAppend = "";
		m_cabMaintenanceList = new ArrayList();
		if(p_criteriaMap != null ) {
			if(p_criteriaMap.containsKey("scode")) {
				m_queryAppend += " and CM_SERVICECODE LIKE '" + p_criteriaMap.get("scode") +"'";
			}
			if(p_criteriaMap.containsKey("date")) {
				m_queryAppend += " and CM_SERVICEDATE LIKE '" + TDSValidation.getDBdateFormat(p_criteriaMap.get("date").toString()) +"'";
			}
		}
		ResultSet m_rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getCabMaintenance +m_queryAppend );
			cat.info("Query in Maintenance "+m_pst);
			m_pst.setString(1,adminBO.getAssociateCode());
			m_rs = m_pst.executeQuery();
			while (m_rs.next()) {
				m_cabMaintenanceBO = new CabMaintenanceBO();
				m_cabMaintenanceBO.setDriverName(m_rs.getString("CM_DRIVERNO"));
				m_cabMaintenanceBO.setMaintenanceKey(m_rs.getString("CM_MAINTENANCENO"));
				m_cabMaintenanceBO.setServiceCode(m_rs.getString("CM_SERVICECODE"));
				m_cabMaintenanceBO.setServiceDate(TDSValidation.getUserDateFormat(m_rs.getString("CM_SERVICEDATE")));
				m_cabMaintenanceBO.setAssocCode(m_rs.getString("CM_ASSOCCODE"));
				m_cabMaintenanceBO.setServiceDesc(m_rs.getString("CM_SERVICEDESC"));
				m_cabMaintenanceBO.setAmount(m_rs.getString("CM_SERVICECOST"));
				m_cabMaintenanceList.add(m_cabMaintenanceBO);
			} 

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getCabMaintenanceList-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getCabMaintenanceList-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_cabMaintenanceList;
	}


	public static CabMaintenanceBO getCabMaintenanceByNo (String p_maintenanceNo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCabMaintenanceList");
		CabMaintenanceBO m_cabMaintenanceBO = null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getCabMaintenaceByNo);
			m_pst.setString(1, p_maintenanceNo);
			cat.info("Query in Behavior "+m_pst);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_cabMaintenanceBO = new CabMaintenanceBO();
				m_cabMaintenanceBO.setDriverKey(m_rs.getString("CM_DRIVERNO"));
				m_cabMaintenanceBO.setMaintenanceKey(m_rs.getString("CM_MAINTENANCENO"));
				m_cabMaintenanceBO.setServiceCode(m_rs.getString("CM_SERVICECODE"));
				m_cabMaintenanceBO.setServiceDate(TDSValidation.getUserDateFormat(m_rs.getString("CM_SERVICEDATE")));
				m_cabMaintenanceBO.setAssocCode(m_rs.getString("CM_ASSOCCODE"));
				m_cabMaintenanceBO.setServiceDesc(m_rs.getString("CM_SERVICEDESC"));
				m_cabMaintenanceBO.setAmount(m_rs.getString("CM_SERVICECOST"));
			}

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getCabMaintenanceList-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getCabMaintenanceList-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_cabMaintenanceBO;
	}

	public static boolean updateCabMaintenance(CabMaintenanceBO p_cabMaintenance,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateCabMaintenance");
		boolean m_updateStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateMaintenace);
			m_pst.setString(1, p_cabMaintenance.getServiceCode());
			m_pst.setString(2, TDSValidation.getDBdateFormat(p_cabMaintenance.getServiceDate()));
			m_pst.setString(3, p_cabMaintenance.getServiceDesc());
			m_pst.setString(4, p_cabMaintenance.getAmount());
			m_pst.setString(5, p_cabMaintenance.getMaintenanceKey());
			m_pst.setString(6,adminBO.getAssociateCode());

			cat.info("Query in Maintenance "+m_pst);
			if(m_pst.executeUpdate() > 0 ) {
				m_updateStatus = true;
			}
		} catch (SQLException sqex) {
			cat.info("TDSException RegistrationDAO.updateCabMaintenance-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.updateCabMaintenance-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_updateStatus;
	}


	public static boolean insertLostandFound(LostandFoundBO p_lostandFoundBO,String mode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertLostandFound");
		boolean m_insertStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement(TDSSQLConstants.insertLostandFound);
			m_pst.setString(1, ConfigDAO.tdsKeyGen("LOST_AND_FOUND", m_conn)+"");
			m_pst.setString(2, p_lostandFoundBO.getTripId());
			m_pst.setString(3, p_lostandFoundBO.getItemName());
			m_pst.setString(4, p_lostandFoundBO.getItemDesc());
			m_pst.setString(5, TDSValidation.getDBdateFormat(p_lostandFoundBO.getLfdate()));
			m_pst.setString(6, p_lostandFoundBO.getLftime());
			m_pst.setString(7, p_lostandFoundBO.getDriverid());
			m_pst.setString(8, p_lostandFoundBO.getAssocCode());
			m_pst.setString(9, mode);
			m_pst.setString(10, p_lostandFoundBO.getRiderName());
			m_pst.setString(11, p_lostandFoundBO.getPhoneNumber());
			m_pst.setString(12,p_lostandFoundBO.getStatus());
			cat.info("Query in insertLostandFound Method "+m_pst);
			if(m_pst.executeUpdate() > 0) {
				m_insertStatus = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.info("TDSException RegistrationDAO.insertLostandFound->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertLostandFound-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_insertStatus;
	}



	public static List getlostAndFoundList (Map p_criteriaMap, AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getLostandFoundList");
		List m_lostandFoundList = null;
		LostandFoundBO m_lostandFoundBO = null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String m_queryAppend = "";
		if(p_criteriaMap != null) {
			if(p_criteriaMap.containsKey("itemName")) {
				m_queryAppend += " AND LF_ITEMNAME LIKE '"+p_criteriaMap.get("itemName")+"'";
			}
			if(p_criteriaMap.containsKey("date")) {
				m_queryAppend += " AND LF_DATE LIKE '"+ TDSValidation.getDBdateFormat(p_criteriaMap.get("date").toString()) +"'";
			}
		}

		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getLostandFoundList + m_queryAppend);
			cat.info("Query in LostandFound "+m_pst);
			m_pst.setString(1,adminBO.getAssociateCode());
			m_rs = m_pst.executeQuery();

			if(m_rs.first()) {
				m_lostandFoundList = new ArrayList();
				do {
					m_lostandFoundBO = new LostandFoundBO();
					//DR_USERID is not used
					//m_lostandFoundBO.setDriverName(m_rs.getString("DR_USERID"));
					m_lostandFoundBO.setLfKey(m_rs.getString("LF_KEY"));
					m_lostandFoundBO.setTripId(m_rs.getString("LF_TRIPID"));
					m_lostandFoundBO.setItemName(m_rs.getString("LF_ITEMNAME"));
					m_lostandFoundBO.setLfdate(TDSValidation.getUserDateFormat(m_rs.getString("LF_DATE")));
					m_lostandFoundBO.setAssocCode(m_rs.getString("LF_ASSOCCODE"));
					m_lostandFoundBO.setItemDesc(m_rs.getString("LF_ITEMDESC"));
					m_lostandFoundBO.setLftime(TDSValidation.getUserTimeFormat(m_rs.getString("LF_TIME")));
					m_lostandFoundList.add(m_lostandFoundBO);
				} while (m_rs.next());
			}

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getLostandFoundList "+ sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getLostandFoundList "+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_lostandFoundList;
	}

	public static LostandFoundBO getLostandFoundByNo (String p_lostandFoundNo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getLostandFoundByNo");
		LostandFoundBO m_lostandFoundBO = null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getLostandFoundByNo);
			m_pst.setString(1, p_lostandFoundNo);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_lostandFoundBO = new LostandFoundBO();
				m_lostandFoundBO.setRiderName(m_rs.getString("LF_RIDERNAME"));
				m_lostandFoundBO.setPhoneNumber(m_rs.getString("LF_PHONENUMBER"));
				m_lostandFoundBO.setDriverid(m_rs.getString("LF_DRIVERID"));
				m_lostandFoundBO.setMode(m_rs.getString("LF_MODE"));
				m_lostandFoundBO.setLfKey(m_rs.getString("LF_KEY"));
				m_lostandFoundBO.setTripId(m_rs.getString("LF_TRIPID"));
				m_lostandFoundBO.setItemName(m_rs.getString("LF_ITEMNAME"));
				m_lostandFoundBO.setItemDesc(m_rs.getString("LF_ITEMDESC"));
				m_lostandFoundBO.setLfdate(TDSValidation.getUserDateFormat(m_rs.getString("LF_DATE")));
				m_lostandFoundBO.setLftime(m_rs.getString("LF_TIME"));
				m_lostandFoundBO.setStatus(m_rs.getString("LF_STATUS"));
			}

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getLostandFoundByNo "+ sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getLostandFoundByNo"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_lostandFoundBO;
	}


	public static boolean updateLostandFound (LostandFoundBO p_lostandFoundBO,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateLostandFound");
		boolean m_updateStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateLostandFound);
			m_pst.setString(1, p_lostandFoundBO.getTripId());
			m_pst.setString(2, p_lostandFoundBO.getItemName());
			m_pst.setString(3, p_lostandFoundBO.getItemDesc());
			m_pst.setString(4, TDSValidation.getDBdateFormat(p_lostandFoundBO.getLfdate()));
			m_pst.setString(5, p_lostandFoundBO.getLftime());
			m_pst.setString(6, p_lostandFoundBO.getDriverid());
			m_pst.setString(7,p_lostandFoundBO.getRiderName());
			m_pst.setString(8,p_lostandFoundBO.getPhoneNumber());
			m_pst.setString(9, p_lostandFoundBO.getStatus());
			m_pst.setString(10, p_lostandFoundBO.getLfKey());
			m_pst.setString(11,adminBO.getAssociateCode());

			cat.info(m_pst.toString());
			if(m_pst.executeUpdate() > 0 ) {
				m_updateStatus = true;
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.updateLostandFound-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.updateLostandFound-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_updateStatus;
	}






	public static Map getDriverIDandCode(String p_driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverIDandCode");
		Map m_airPortCodeMap = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.getDriverIDandCode);
			m_pst.setString(1, p_driverId);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			if(m_rs.next()) {
				m_airPortCodeMap = new HashMap();
				m_airPortCodeMap.put("driverId",m_rs.getString("AU_SNO"));
				m_airPortCodeMap.put("assoccode", m_rs.getString("AU_ASSOCCODE") == null ? "" : m_rs.getString("AU_ASSOCCODE"));
			}
			cat.info("Data in DAO "+m_airPortCodeMap);
		} catch (SQLException sqex ) {
			cat.error("TDSException RegistrationDAO.getDriverIDandCode-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverIDandCode-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_airPortCodeMap;
	}


	public static boolean insertCmpPaySetSch(ArrayList al_list,String ins_flg,String p_assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RegistrationDAO.insertCmpPaySetSch");

		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			if(ins_flg.equalsIgnoreCase("insert")) {

				m_pst = m_conn.prepareStatement("delete from TDS_SCHEDULED_PROCESSING  where SCH_ASSOCCODE = ? ");
				m_pst.setString(1, p_assocode);
				cat.info(m_pst.toString());
				m_pst.execute();
				m_pst.close();

				m_pst = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING(SCH_DAY,SCH_TIME,SCH_STATUS,SCH_UP_TO_DATE,SCH_ASSOCCODE,SCH_DRIVER_CHARGES) values(?,?,?,now(),?,?)");
			} else {

				m_pst = m_conn.prepareStatement("delete from TDS_SCHEDULED_PROCESSING  where SCH_ASSOCCODE = ? ");
				m_pst.setString(1, p_assocode);
				cat.info(m_pst.toString());
				m_pst.execute();
				m_pst.close();

				m_pst = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING(SCH_DAY,SCH_TIME,SCH_STATUS,SCH_UP_TO_DATE,SCH_ASSOCCODE,SCH_DRIVER_CHARGES) values(?,?,?,now(),?,?)");
			}

			for(int i=0;i<al_list.size();i++)
			{
				CmpPaySeteledBean seteledBean = (CmpPaySeteledBean)al_list.get(i);

				m_pst.setString(1, seteledBean.getC_sch_day());
				if(seteledBean.getC_sch_status().equalsIgnoreCase("1"))
					m_pst.setString(2, TDSValidation.get24HrsFormat(seteledBean.getC_sch_time(), seteledBean.getC_AMPM()));
				else
					m_pst.setString(2, "00:00:00");
				m_pst.setString(3, seteledBean.getC_sch_status());
				m_pst.setString(4, seteledBean.getC_assocode());
				m_pst.setString(5, seteledBean.getDchargesrunstatus());

				m_pst.execute();	


			}


		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.insertCmpPaySetSch-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistRegistrationDAO.insertCmpPaySetSchrationDAO.insertCmpPaySetSch-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}




	public static boolean insertDriverCharge(ArrayList al_list,String ins_flg,String p_assocode) {		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertDriverCharge");
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();


			for(int i=0;i<al_list.size();i++)
			{

				DriverChargeBean chargeBean = (DriverChargeBean)al_list.get(i);
				//System.out.println("sfsaf==="+chargeBean.getDC_CHARGE_NO()+"::"+al_list.size()+"::"+i);
				System.out.println(chargeBean.getDC_CHARGE_NO().equalsIgnoreCase(""));
				if(chargeBean.getDC_CHARGE_NO().equalsIgnoreCase(""))
					m_pst = m_conn.prepareStatement("insert into TDS_DRIVER_CHARGES(DC_ASSOCODE,DC_DESC,DC_FREQUECY,DC_AMOUNT,DC_STATUS,DC_ENTERED_DATE,DC_LDAY,DC_CHARGE_NO) values(?,?,?,?,?,now(),?,?)");
				else 
					m_pst = m_conn.prepareStatement("update TDS_DRIVER_CHARGES set DC_ASSOCODE = ?,DC_DESC = ? ,DC_FREQUECY = ?,DC_AMOUNT=?,DC_STATUS =?,DC_ENTERED_DATE = now(),DC_LDAY=? where DC_CHARGE_NO = ?");

				m_pst.setString(1, p_assocode);
				m_pst.setString(2, chargeBean.getDC_DESC());
				m_pst.setString(3, chargeBean.getDC_FREQUECY());

				if(chargeBean.getDC_STATUS().equalsIgnoreCase("1")){
					String s[]=chargeBean.getDC_AMOUNT().split(" ");
					m_pst.setString(4,s[0]);
				}
				else
					m_pst.setString(4, "0");

				m_pst.setString(5, chargeBean.getDC_STATUS());
				m_pst.setString(6, chargeBean.getDC_LDAY());


				if(chargeBean.getDC_CHARGE_NO().equalsIgnoreCase(""))
					m_pst.setInt(7, ConfigDAO.tdsKeyGen("TDS_DRIVER_CHARGE_SEQ", m_conn));
				else
					m_pst.setString(7, chargeBean.getDC_CHARGE_NO());
				cat.info(m_pst.toString());
				m_pst.execute();				
			}


		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.insertDriverCharge-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertDriverCharge-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}



	public static ArrayList getCmpPaySetSch(String p_asscode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCmpPaySetSch");

		ArrayList al_list = null;
		CmpPaySeteledBean seteledBean = null ;

		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("select SCH_ASSOCCODE,SCH_DRIVER_CHARGES,SCH_DAY,TIME_FORMAT(SCH_TIME,'%h:%i') as SCH_TIME,TIME_FORMAT(SCH_TIME,'%p') as AMPM,SCH_STATUS,case SCH_DAY when 'MON' then 'Monday' when 'TUE' then 'Tuesday' when 'WED' then 'Wednesday' when 'THU' then 'Thursday' " +
					" when 'FRI' then 'Friday' when 'SAT' then 'Saturday' when 'SUN' then 'Sunday' end as DAY_NAME, case SCH_DAY when 'MON' then 1 when 'TUE' then 2 when 'WED' then 3 when 'THU' then 4 " +
					" when 'FRI' then 5 when 'SAT' then 6 when 'SUN' then 7 end as ord from TDS_SCHEDULED_PROCESSING where  SCH_ASSOCCODE = ? order by ord  ");
			m_pst.setString(1, p_asscode);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();

			if(rs.first())
			{
				al_list = new ArrayList();
				do
				{
					seteledBean = new CmpPaySeteledBean();
					seteledBean.setC_assocode(rs.getString("SCH_ASSOCCODE"));
					seteledBean.setC_sch_day(rs.getString("SCH_DAY"));
					seteledBean.setC_sch_time(rs.getString("SCH_TIME"));
					seteledBean.setC_sch_status(rs.getString("SCH_STATUS"));
					seteledBean.setC_sch_day_name(rs.getString("DAY_NAME"));
					seteledBean.setC_AMPM(rs.getString("AMPM"));
					seteledBean.setDchargesrunstatus(rs.getString("SCH_DRIVER_CHARGES"));
					al_list.add(seteledBean);
				} while(rs.next());

			}  

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getCmpPaySetSch-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getCmpPaySetSch-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;

	}
	public static ArrayList getDriverCharges(String p_asscode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverCharges");

		ArrayList al_list = null;
		DriverChargeBean chargeBean = null ;

		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("select * from TDS_DRIVER_CHARGES where  DC_ASSOCODE = ?");
			m_pst.setString(1, p_asscode);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();

			if(rs.first())
			{
				al_list = new ArrayList();
				do
				{
					chargeBean = new DriverChargeBean();
					chargeBean.setDC_ASSOCODE(rs.getString("DC_ASSOCODE"));
					chargeBean.setDC_CHARGE_NO(rs.getString("DC_CHARGE_NO"));
					chargeBean.setDC_DESC(rs.getString("DC_DESC"));
					chargeBean.setDC_FREQUECY(rs.getString("DC_FREQUECY"));
					chargeBean.setDC_AMOUNT(rs.getString("DC_AMOUNT"));
					chargeBean.setDC_STATUS(rs.getString("DC_STATUS"));
					chargeBean.setDC_LDAY(rs.getString("DC_LDAY"));

					al_list.add(chargeBean);
				} while(rs.next());

			}  

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getDriverCharges-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverCharges-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;

	}


	public static ArrayList getDriverPenality(String p_asscode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverPenality");

		ArrayList al_list = null;
		DriverChargeBean chargeBean = null ;

		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("select *,date_format(DP_PENALITY_DATE,'%m/%d/%Y') as PENALITY_DATE from TDS_DRIVER_PENALITY where  DP_ASSOCCODE = ?");
			m_pst.setString(1, p_asscode);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();
			PenalityBean penalityBean = null;
			if(rs.first())
			{
				al_list = new ArrayList();
				do
				{

					penalityBean = new PenalityBean();

					penalityBean.setB_amount(rs.getString("DP_TYPE").equals("2")?(new BigDecimal(-rs.getDouble("DP_AMOUNT")).setScale(2, 5)).toString():(new BigDecimal(rs.getDouble("DP_AMOUNT")).setScale(2, 5)).toString());
					penalityBean.setB_ass_code(p_asscode);
					penalityBean.setB_desc(rs.getString("DP_DESC"));
					penalityBean.setB_driver_id(rs.getString("DP_DRIVER_ID"));
					penalityBean.setB_p_date(rs.getString("PENALITY_DATE"));
					penalityBean.setB_trans(rs.getString("DP_TRANS_ID"));
					penalityBean.setBtype(rs.getString("DP_TYPE"));

					al_list.add(penalityBean);
				} while(rs.next());

			}  

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrRegistrationDAO.getDriverPenalityationDAO.getDriverPenality-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverPenality-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;

	}

	public static boolean insertDriverPenalityError(ArrayList al_list,String ins_flg,String p_assocode) {		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertDriverPenalityError");

		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();

			for(int i=0;i<al_list.size();i++)
			{


				PenalityBean penalityBean = (PenalityBean)al_list.get(i);
				m_pst = m_conn.prepareStatement("insert into TDS_DRIVER_PENALITY_ERROR(DP_DRIVER_ID,DP_ASSOCCODE,DP_TYPE,DP_AMOUNT,DP_ENTERED_DATE,DP_PENALITY_DATE,DP_DESC) values(?,?,?,?,?,now(),?)");

				m_pst.setString(1, penalityBean.getB_driver_id());
				m_pst.setString(2, p_assocode);
				m_pst.setString(3, penalityBean.getBtype());
				m_pst.setString(4, penalityBean.getB_amount());
				m_pst.setString(5, penalityBean.getB_p_date());
				m_pst.setString(6, penalityBean.getB_desc());
				cat.info(m_pst.toString());

				m_pst.executeUpdate();

			}


		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.insertDriverPenalityError-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertDriverPenalityError-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}


	public static ArrayList  getqueueDetails(String queuename) {		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getqueueDetails");

		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		ResultSet rs =null;
		boolean  result = false; 
		PreparedStatement m_pst = null;
		ArrayList ar_list = new ArrayList();
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("SELECT QB_LONGITUDE,QB_LATITUDE FROM  TDS_QUEUE_BOUNDRIES where QB_QUEUENAME=?");
			m_pst.setString(1, queuename);
			cat.info(m_pst.toString());

			rs = m_pst.executeQuery();
			while(rs.next()) {
				ar_list.add(rs.getString("QB_LONGITUDE"));
				ar_list.add(rs.getString("QB_LATITUDE"));

			}  
		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getqueueDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getqueueDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ar_list;
	}


	public static boolean queueNameCheck(String queuuName) {		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.queueNameCheck");

		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		ResultSet rs =null;
		boolean  result = false; 
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("SELECT * FROM  TDS_QUEUE_MASTER WHERE QM_QUEUENAME =?");
			m_pst.setString(1, queuuName); 
			rs = m_pst.executeQuery();
			if(rs.next()) {
				result =true;
			} 
			cat.info("TDS QUEUE MASTER::"+m_pst.toString());  

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.queueNameCheck-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.queueNameCheck-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}


	public static boolean insertQueueMapping(String queueName,String description,String asscode,ArrayList ar_list,String size,String Order,String date,String smstime,String loginswitch,String Sttime,String Endtime,String dayweek,String Dispipe,String brdreqbefr) {		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertQueueMapping");

		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		int result =0;
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("insert into TDS_QUEUE_MASTER (QM_QUEUENAME,QM_ASSOCCODE,QM_DESCRIPTION,QM_ORDER,QM_DELAYTIME,QM_SMS_RES_TIME,QM_LOGIN_SWITCH,QM_STTIME,QM_ETTIME,QM_DAYWEEK,QM_DS_PIPE,QM_REQUEST_BEFORE) values(?,?,?,?,?,?,?,?,?,?,?,?)");
			m_pst.setString(1, queueName);
			m_pst.setString(2, asscode);
			m_pst.setString(3, description);
			m_pst.setString(4, Order);
			m_pst.setString(5, date);
			m_pst.setString(6, smstime);
			m_pst.setString(7, loginswitch); 
			m_pst.setString(8, Sttime);
			m_pst.setString(9, Endtime);
			m_pst.setString(10, dayweek);
			m_pst.setString(11, Dispipe);
			m_pst.setString(12, brdreqbefr);
			cat.info("TDS QUEUE MASTER::"+m_pst.toString()); 
			result = m_pst.executeUpdate();

			m_pst = m_conn.prepareStatement("insert into  TDS_QUEUE_DETAILS (QD_QUEUENAME,QD_ASSOCCODE,QD_DESCRIPTION,QD_ORDER,QD_DELAYTIME,QD_SMS_RES_TIME,QD_LOGIN_SWITCH,QD_STTIME,QD_ETTIME,QD_DAYWEEK,QD_DS_PIPE,QD_REQUEST_BEFORE) values(?,?,?,?,?,?,?,?,?,?,?,?)");
			m_pst.setString(1, queueName);
			m_pst.setString(2, asscode);
			m_pst.setString(3, description);
			m_pst.setString(4, Order);
			m_pst.setString(5, date);
			m_pst.setString(6, smstime);
			m_pst.setString(7, loginswitch); 
			m_pst.setString(8, Sttime);
			m_pst.setString(9, Endtime);
			m_pst.setString(10, dayweek);
			m_pst.setString(11, Dispipe);
			m_pst.setString(12, brdreqbefr);
			cat.info("TDS QUEUE COORDINATE::"+m_pst.toString());

			result = m_pst.executeUpdate();
			for(int i=0;i<ar_list.size();)
			{
				m_pst = m_conn.prepareStatement("insert into TDS_QUEUE_BOUNDRIES (QB_ASSOCCODE, QB_QUEUENAME,QB_LONGITUDE,QB_LATITUDE) values(?,?,?,?)"); 
				m_pst.setString(1, asscode);
				m_pst.setString(2, queueName);
				m_pst.setString(3, ""+ar_list.get(i));
				m_pst.setString(4,  ""+ar_list.get(i+1));
				i=i+2;

				m_pst.executeUpdate();
				cat.info("tds bounderies::"+m_pst.toString());
			}

		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.insertQueueMapping-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertQueueMapping-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}

	public static HashMap insertDriverPenality(ArrayList al_list,String ins_flg,String p_assocode) {	 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertDriverPenality");

		HashMap hm_sql_container = new HashMap();			
		ArrayList al_sql_error = new ArrayList();
		ArrayList al_sql_valid = new ArrayList();							
		StringBuffer error = new StringBuffer();

		boolean m_result;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();					 
			for(int i=0;i<al_list.size();i++)
			{
				PenalityBean penalityBean = (PenalityBean)al_list.get(i);
				try{	
					//if(penalityBean.getB_trans().equalsIgnoreCase(""))?
					m_pst = m_conn.prepareStatement("insert into TDS_DRIVER_PENALITY(DP_TRANS_ID,DP_DRIVER_ID,DP_ASSOCCODE,DP_TYPE,DP_AMOUNT,DP_ENTERED_DATE,DP_PENALITY_DATE,DP_DESC) values(?,?,?,?,?,now(),?,?)");
					//else 
					//m_pst = m_conn.prepareStatement("update TDS_DRIVER_PENALITY set DP_DRIVER_ID = ? ,DP_ASSOCCODE = ?,DP_DESC = ?,DP_AMOUNT = ?,DP_ENTERED_DATE = now(),DP_PENALITY_DATE = ? where DP_TRANS_ID = ?");								
					m_pst.setInt(1, ConfigDAO.tdsKeyGen("TDS_DRIVER_PENALITY_SEQ", m_conn));
					m_pst.setString(2, penalityBean.getB_driver_id());
					m_pst.setString(3, p_assocode);
					m_pst.setString(4, penalityBean.getBtype());
					m_pst.setDouble(5, (penalityBean.getBtype().equals("2")?-Double.parseDouble(penalityBean.getB_amount()) : Double.parseDouble(penalityBean.getB_amount())));
					m_pst.setString(6, TDSValidation.getDBdateFormat(penalityBean.getB_p_date()));
					m_pst.setString(7, penalityBean.getB_desc());
					//if(penalityBean.getB_trans().equalsIgnoreCase(""))						
					//else
					//m_pst.setString(6, penalityBean.getB_trans());
					cat.info(m_pst);
					m_pst.executeUpdate();
					al_sql_valid.add(penalityBean);
				}catch (SQLException sql_ex){	
					//Capturing SQLException and return to view
					int j=i+1;
					al_sql_error.add(penalityBean);							
					error.append("Error occured while Inserting Row: " +j+ " in Data Insertion; ");
					sql_ex.getStackTrace();
				}
			}
		} catch (Exception sqex) {
			//Run time Exception handling
			cat.error("TDSException RegistrationDAO.insertDriverPenality-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertDriverPenality-->"+sqex.getMessage());
			sqex.printStackTrace();
			throw new RuntimeException(sqex);
		} 
		finally {
			m_tdsConn.closeConnection();
		}

		hm_sql_container.put("al_sql_error", al_sql_error);
		hm_sql_container.put("al_sql_valid", al_sql_valid);
		hm_sql_container.put("sql_errBuff", error.toString());
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return hm_sql_container;
	}

	public static VoucherBO Vouchersequence(String seq,String assocode) {		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		VoucherBO voucherBO =  new VoucherBO();
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();

		if(seq.equals("COV")){
			int vno=(ConfigDAO.tdsKeyGen("COMPANY_VOUCHER", m_conn));
			voucherBO.setVno(assocode+vno);
		} else if(seq.equals("CRV")){
			int vno=(ConfigDAO.tdsKeyGen("CORPARATE_VOUCHER", m_conn));
			voucherBO.setVno(assocode+vno);
		}
		cat.info("What is this??? No Execute statement");
		m_tdsConn.closeConnection();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucherBO;
	}

	public static int deleteCabMappingDetails(String U_id,String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.deleteCabMappingDetails");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int returnCode=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement("INSERT INTO TDS_HISTORY(SELECT * FROM TDS_CABDRIVERMAPPING WHERE CM_UID= ? AND CM_ASSOCCODE= ?)");	
			pst.setString(1,U_id);
			pst.setString(2,associationCode);
			cat.info(pst.toString());

			pst.execute();

			pst = con.prepareStatement("DELETE FROM TDS_CABDRIVERMAPPING WHERE CM_UID= ? AND CM_ASSOCCODE= ? ");	
			pst.setString(1,U_id);
			pst.setString(2,associationCode);
			cat.info(pst.toString());
			pst.execute();
			returnCode=1;
		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.deleteCabMappingDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.deleteCabMappingDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return returnCode;
	}	
	public static ArrayList getDriverDisbrushmentSummaryByLimit(String from_date,String to_date, String assocode,String driver,int startValue,int endValue)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateDisMessage");
		ArrayList al_list = new ArrayList();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;


		try {
			String query = "select" +
					" DM_DRIVERID,DR_FNAME,date_format(DM_PAYMENTDATE, '%m/%d/%Y') as p_date,DM_AMOUNT,DM_CHECKNO,DM_DESC,DM_PAYID,DM_OPR_ID" +
					" from TDS_DISBURSEMENT, TDS_DRIVER " +
					" where " +
					" DM_DRIVERID=DR_SNO and DM_ASSOCCODE= ?";
			String query1="  LIMIT ?, ?";
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			if(!from_date.equals(""))
			{
				query = query + " and date_format(DM_PAYMENTDATE,'%Y%m%d') >= '"+from_date+"'";
			}

			if(!to_date.equals(""))
			{
				query = query + " and date_format(DM_PAYMENTDATE,'%Y%m%d') <= '"+to_date+"'";
			}

			if(!driver.equals(""))
			{
				query =query + " and DM_DRIVERID = '"+driver+"'";
			}

			query = query + " order by p_date desc "+query1;

			m_pst = m_conn.prepareStatement(query);

			m_pst.setString(1, assocode);
			m_pst.setInt(2,startValue);
			m_pst.setInt(3,endValue);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();
			while(rs.next())
			{
				DriverDisbursment disbursment = new DriverDisbursment();
				disbursment.setAmount(rs.getString("DM_AMOUNT"));
				disbursment.setDriverid(rs.getString("DM_DRIVERID"));
				disbursment.setDrivername(rs.getString("DR_FNAME"));
				disbursment.setProcess_date(rs.getString("p_date"));

				disbursment.setDescr(rs.getString("DM_DESC"));
				disbursment.setCheckno(rs.getString("DM_CHECKNO"));
				disbursment.setPayment_id(rs.getString("DM_PAYID"));
				al_list.add(disbursment);
			}
		}  catch (SQLException sqex) {
			//System.out.println("TDSException RegistrationDAO.getDriverDisbrushmentSummaryByLimit"+sqex.getMessage());
			cat.error(m_pst.toString());
			cat.error("TDSException RegistrationDAO.getDriverDisbrushmentSummaryByLimit-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}
	public static boolean deleteAddress(String assocode,String userKey, String masterKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs=null;
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement("DELETE FROM TDS_USERADDRESS WHERE CU_ADDRESS_KEY='"+userKey+"' AND CU_MASTERASSOCIATIONCODE='"+assocode+"'"); 
			cat.info(csp_pst.toString());
			csp_pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.info("TDSException RegistrationDAO.deleteAddress-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException RegistrationDAO.deleteAddress" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}
	public static ArrayList<DriverAndJobs> getOpenJobSummary(String assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;

		ArrayList<DriverAndJobs> list=new ArrayList<DriverAndJobs>();
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			String SQL = "select count(*) as numOfJob , OR_QUEUENO from TDS_OPENREQUEST WHERE OR_ASSOCCODE='"+assocode+"' AND (OR_TRIP_STATUS = '"+TDSConstants.broadcastRequest+"' OR (OR_TRIP_STATUS = '"+TDSConstants.performingDispatchProcesses+"' AND OR_DRIVERID = ''))GROUP BY OR_QUEUENO"; 

			csp_pst = csp_conn.prepareStatement(SQL);
			cat.info(SQL);
			rs = csp_pst.executeQuery();
			while(rs.next()) {
				DriverAndJobs Jobs = new DriverAndJobs();
				if(rs.getString("OR_QUEUENO")!=null){
					Jobs.setZoneNumber(rs.getString("OR_QUEUENO"));
				} else {
					Jobs.setZoneNumber(rs.getString("QU_NAME"));
				}
				Jobs.setNumberOfJobs(rs.getString("numOfJob")==null?"0":rs.getString("numOfJob"));
				//Jobs.setNumberOfDrivers(rs.getString("numOfDrivers")==null?"0":rs.getString("numOfDrivers"));
				//Jobs.setZoneDesc(rs.getString("QD_DESCRIPTION"));
				list.add(Jobs);
			}
		} catch (Exception sqex) {
			cat.error("TDSException RegistrationDAO.getDriverAndJobSummary-->"+ sqex.getMessage());
			cat.error(csp_pst.toString());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return list;
	}
	public static ArrayList<DriverAndJobs> getDriverAndJobSummary(String masterAssocode,String assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null,pst2=null;
		ResultSet rs = null,rs1=null;

		ArrayList<DriverAndJobs> list=new ArrayList<DriverAndJobs>();

		ArrayList<FutureJobs> flist1=new ArrayList<FutureJobs>();


		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			//String SQL = "select * from (Select count(*) as numOfJob , OR_QUEUENO, QD_DESCRIPTION from TDS_OPENREQUEST OR, TDS_QUEUE_DETAILS QD WHERE OR.QUEUENO = QD.QUEUENAME AND OR_QUEUENO <> '' GROUP BY OR_QUEUENO AND OR_ASSOCCODE='"+assocode+"') A LEFT OUTER JOIN (SELECT COUNT(*) as numOfDrivers , QU_NAME from TDS_QUEUE GROUP BY QU_NAME AND QU_ASSOCCODE='"+assocode+"') B ON A.OR_QUEUENO = B.QU_NAME" 
			//				+ " UNION "
			//				+ "select * from (Select count(*) as numOfJob , OR_QUEUENO from TDS_OPENREQUEST OR_QUEUENO <> ''  GROUP BY OR_QUEUENO AND OR_ASSOCCODE='"+assocode+"') A RIGHT OUTER JOIN (SELECT COUNT(*) as numOfDrivers , QU_NAME, QD_DESCRIPTION from TDS_QUEUE Q, TDS_QUEUE_DETAILS QD WHERE Q.QU_NAME = QD.QUEUENAME GROUP BY QU_NAME AND QU_ASSOCCODE='"+assocode+"') B ON A.OR_QUEUENO = B.QU_NAME";
			String SQL = "select * from (Select count(*) as numOfJob , OR_QUEUENO, QD_DESCRIPTION from TDS_OPENREQUEST, TDS_QUEUE_DETAILS WHERE OR_QUEUENO = QD_QUEUENAME AND (OR_ASSOCCODE='"+assocode+"' OR OR_ASSOCCODE='"+masterAssocode+"') AND OR_MASTER_ASSOCCODE='"+masterAssocode+"' AND (OR_TRIP_STATUS='3' OR OR_TRIP_STATUS='4') AND OR_MASTER_ASSOCCODE = QD_ASSOCCODE GROUP BY OR_QUEUENO ) A LEFT OUTER JOIN (SELECT COUNT(*) as numOfDrivers , QU_NAME from TDS_QUEUE WHERE QU_MASTER_ASSOCCODE='"+masterAssocode+"' GROUP BY QU_NAME) B ON A.OR_QUEUENO = B.QU_NAME " +
					"UNION " +
					"select * from (Select count(*) as numOfJob , OR_QUEUENO from TDS_OPENREQUEST WHERE OR_QUEUENO <> '' AND OR_ASSOCCODE='"+assocode+"' AND OR_MASTER_ASSOCCODE='"+masterAssocode+"' AND (OR_TRIP_STATUS='3' OR OR_TRIP_STATUS='4') AND OR_DISPATCH_START_TIME <= NOW() GROUP BY OR_QUEUENO) A RIGHT OUTER JOIN (SELECT QD_DESCRIPTION, COUNT(*) as numOfDrivers , QU_NAME from TDS_QUEUE, TDS_QUEUE_DETAILS WHERE QU_NAME = QD_QUEUENAME AND QU_MASTER_ASSOCCODE='"+masterAssocode+"' AND QU_MASTER_ASSOCCODE = QD_ASSOCCODE GROUP BY QU_NAME ) B ON A.OR_QUEUENO = B.QU_NAME ";
			String sql1 = "SELECT COUNT(*) AS N_1HourJob,OR_QUEUENO FROM TDS_OPENREQUEST WHERE OR_DISPATCH_START_TIME > NOW() AND OR_DISPATCH_START_TIME <= (NOW()+INTERVAL 1 HOUR) AND (OR_ASSOCCODE='"+assocode+"' OR OR_ASSOCCODE='"+masterAssocode+"') AND OR_MASTER_ASSOCCODE='"+masterAssocode+"' GROUP BY OR_QUEUENO ORDER BY OR_QUEUENO";
			pst2 = csp_conn.prepareStatement(sql1);
			rs1 = pst2.executeQuery();
			int n=0;
			while(rs1.next()){
				FutureJobs fjobs = new FutureJobs();
				fjobs.setStr1(rs1.getString("OR_QUEUENO"));
				fjobs.setStr2(rs1.getString("N_1HourJob"));
				flist1.add(fjobs);
				n++;
			}
			flist1=n==0?null:flist1;


			csp_pst = csp_conn.prepareStatement(SQL);
			//System.out.println(SQL);
			rs = csp_pst.executeQuery();
			while(rs.next()) {
				DriverAndJobs Jobs = new DriverAndJobs();
				if(rs.getString("OR_QUEUENO")!=null){
					Jobs.setZoneNumber(rs.getString("OR_QUEUENO"));
				} else {
					Jobs.setZoneNumber(rs.getString("QU_NAME"));
				}
				Jobs.setNumberOfJobs(rs.getString("numOfJob")==null?"0":rs.getString("numOfJob"));
				Jobs.setNumberOfDrivers(rs.getString("numOfDrivers")==null?"0":rs.getString("numOfDrivers"));
				Jobs.setZoneDesc(rs.getString("QD_DESCRIPTION"));
				int j=0;
				String fjobs = null,fjobs2;
				if(flist1!=null){
					while(j<n){											
						if(flist1.get(j).getStr1().equals(rs.getString("OR_QUEUENO"))){						
							fjobs=flist1.get(j).getStr2();
							break;
						}
						j++;
					}
					fjobs2=(fjobs==null)?"0":fjobs;
					Jobs.setFuture1HourJobs(fjobs2);											
				}else{
					Jobs.setFuture1HourJobs("0");
				}				
				list.add(Jobs);
			}
		} catch (Exception sqex) {
			cat.error("TDSException RegistrationDAO.getDriverAndJobSummary-->"+ sqex.getMessage());
			cat.error(csp_pst.toString());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return list;
	}
	public static ArrayList getLandMark(String phoneno,String assoccode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RegistrationDAO.getFromandToAddress");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ArrayList al_list = new ArrayList();
		if(!phoneno.equals("")){
			try {
				m_dbConn = new TDSConnection();
				m_conn = m_dbConn.getConnection();
				m_pst = m_conn.prepareStatement("SELECT * FROM TDS_LANDMARK WHERE LM_PHONE='"+phoneno+"' AND LM_ASSCODE='"+assoccode+"'");
				//System.out.println("lm:"+m_pst.toString());
				cat.info(m_pst.toString());
				ResultSet rs = m_pst.executeQuery();
				while(rs.next()){
					OpenRequestBO orBO= new OpenRequestBO();
					orBO.setSlandmark(rs.getString("LM_NAME"));
					orBO.setSadd1(rs.getString("LM_ADD1"));
					orBO.setSadd2(rs.getString("LM_ADD2"));
					orBO.setScity(rs.getString("LM_CITY"));
					orBO.setSstate(rs.getString("LM_STATE"));
					orBO.setSzip(rs.getString("LM_ZIP"));
					orBO.setPhone(rs.getString("LM_PHONE"));
					orBO.setSlat(rs.getString("LM_LATITUDE"));
					orBO.setSlong(rs.getString("LM_LONGITUDE"));
					orBO.setLandMarkKey(rs.getString("LM_KEY"));
					orBO.setComments(rs.getString("LM_COMMENTS"));
					al_list.add(orBO);
				}
			}catch (Exception sqex)
			{
				cat.error("TDSException RegistrationDAO.getLandMark-->"+sqex.getMessage());
				cat.error(m_pst.toString());
				//System.out.println("TDSException RegistrationDAO.getLandMark-->"+sqex.getMessage());
				sqex.printStackTrace();

			} finally {
				m_dbConn.closeConnection();
			}
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static int deleteCab(AdminRegistrationBO adminBO,String cabNum){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.deleteCab");
		int result=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		try {
			m_pst=m_conn.prepareStatement("DELETE FROM TDS_VEHICLE WHERE V_VKEY="+cabNum+" AND V_ASSOCCODE="+adminBO.getAssociateCode());
			cat.info("Query---->"+m_pst.toString());
			result=m_pst.executeUpdate();
		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.deleteCab-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.deleteCab-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int deleteDriver(AdminRegistrationBO adminBO,String driverNum,int driverOrOperator){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.deleteDriver");
		int result=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		try {
			if(driverOrOperator==0){
				m_pst=m_conn.prepareStatement("DELETE FROM TDS_DRIVER WHERE DR_SNO='"+driverNum+"' AND DR_ASSOCODE='"+adminBO.getAssociateCode()+"'");
				cat.info("Query---->"+m_pst.toString());
				result=m_pst.executeUpdate();
			}
			if(result==1 || driverOrOperator==1){
				result=0;
				m_pst=m_conn.prepareStatement("DELETE FROM TDS_ADMINUSER WHERE AU_SNO='"+driverNum+"' AND AU_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
				cat.info("Query---->"+m_pst.toString());
				result=m_pst.executeUpdate();
			}
		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.deleteDriver-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.deleteDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}


	public static ArrayList<String> getAllDates(String days,String fromDate,String toDate){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null; 
		ArrayList<String> dates=new ArrayList<String>();
		String day="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String whereQuery = "";
		if(days.substring(0,1).equals("1")){
			whereQuery = "AND (DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 1  ";
		}if(days.substring(1,2).equals("1")){
			if(whereQuery.equals("")){
				whereQuery = whereQuery+"AND (DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 2   ";
			} else {
				whereQuery = whereQuery+"OR DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 2  ";
			}
		}if(days.substring(2,3).equals("1")){
			if(whereQuery.equals("")){
				whereQuery = whereQuery+"AND (DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 3   ";
			} else {
				whereQuery = whereQuery+"OR DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 3  ";
			}
		}if(days.substring(3,4).equals("1")){
			if(whereQuery.equals("")){
				whereQuery = whereQuery+"AND (DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 4   ";
			} else {
				whereQuery = whereQuery+"OR DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 4  ";
			}
		}if(days.substring(4,5).equals("1")){
			if(whereQuery.equals("")){
				whereQuery = whereQuery+"AND (DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 5   ";
			} else {
				whereQuery = whereQuery+"OR DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 5  ";
			}
		}if(days.substring(5,6).equals("1")){
			if(whereQuery.equals("")){
				whereQuery = whereQuery+"AND (DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 6   ";
			} else {
				whereQuery = whereQuery+"OR DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 6  ";
			}
		}if(days.substring(6,7).equals("1")){
			if(whereQuery.equals("")){
				whereQuery = whereQuery+"AND (DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 7   ";
			} else {
				whereQuery = whereQuery+"OR DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 7  ";
			}
		}if(!whereQuery.equals("")){
			whereQuery=whereQuery+")";
		}
		try {
			pst = con.prepareStatement("SELECT DATE_FORMAT(DATE_ADD(CURDATE(), INTERVAL row DAY),'%m/%d/%Y') as genDate from (SELECT @row:=@row + 1 as row FROM	(select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t,(select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t2,(SELECT @row:=-1) t5) m where DATE_ADD(CURDATE(), INTERVAL row DAY) between ? and ? "+whereQuery);
			pst.setString(1, TDSValidation.getDBdateFormat(fromDate));
			pst.setString(2, TDSValidation.getDBdateFormat(toDate));
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				day=rs.getString("genDate");
				dates.add(day);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException CallerIdDAO.getAllDates-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CallerIdDAO.getAllDates-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return dates;
	}
	public static void moveDriverLocationToHistory(int status)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getDriverLocationHistory");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		String query = "INSERT INTO TDS_DRIVERLOCATIONHISTORY(DLH_DRIVERID,DLH_LATITUDE,DLH_LONGITUDE,DLH_SWITCH,DLH_ASSOCCODE,DLH_LASTUPDATEDTIME)(SELECT DL_DRIVERID,DL_LATITUDE,DL_LONGITUDE,DL_SWITCH,DL_ASSOCCODE,now() FROM TDS_DRIVERLOCATION";
		if(status==1){
			query = query +" WHERE DL_SWITCH ='Y')";
		}else{
			query = query +" WHERE DL_SWITCH ='N')";
		}
		try {
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			m_pst.execute();
		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getDriverLocationHistory-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverLocationHistory-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}	
	public static ArrayList<DriverLocationHistoryBO> s(String associationCode,String lastUpdatedDate,String driverId,String fromDate,String toDate,int type){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DriverLocationHistoryBO> driverLocation = new ArrayList<DriverLocationHistoryBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst= null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="";
		if(!fromDate.equals("") && !toDate.equals("")){
			if(type==1){
				String fromDateFormated[] = fromDate.split("/");
				String toDateFormated[] = toDate.split("/");
				query=" AND DATE(DLH_LASTUPDATEDTIME) >= '"+fromDateFormated[2]+"-"+fromDateFormated[0]+"-"+fromDateFormated[1]+"' AND DATE(DLH_LASTUPDATEDTIME) <= '"+toDateFormated[2]+"-"+toDateFormated[0]+"-"+toDateFormated[1]+"'";
			} else {
				query=" AND DLH_LASTUPDATEDTIME >= '"+fromDate+"' AND DLH_LASTUPDATEDTIME <= '"+toDate+"'";
			}
		}
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_DRIVERLOCATIONHISTORY WHERE DLH_ASSOCCODE='"+associationCode+"' AND DLH_DRIVERID='"+driverId+"'"+query);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				DriverLocationHistoryBO driverLocationBO = new DriverLocationHistoryBO();
				driverLocationBO.setLatitude(rs.getString("DLH_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("DLH_LONGITUDE"));
				driverLocationBO.setStatus(rs.getString("DLH_SWITCH"));
				driverLocationBO.setUpdatedTime(rs.getString("DLH_LASTUPDATEDTIME"));
				driverLocationBO.setDriverid(rs.getString("DLH_DRIVERID"));
				driverLocationBO.setsNo(rs.getString("DLH_SNO"));
				driverLocation.add(driverLocationBO);
			}
		}catch (Exception sqex){
			cat.error("TDSException RequestDAO.getDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverLocation;

	}



	public static double distance(double lat1, double lon1, double lat2, double lon2) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		dist = dist * 0.8684;
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return (dist);
	}

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts decimal degrees to radians             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts radians to decimal degrees             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}
	public static int deleteLandMark(AdminRegistrationBO adminBO,String key){

		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.deleteLandMark");
		int result=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 

		try {
			m_pst=m_conn.prepareStatement("DELETE FROM TDS_LANDMARK WHERE LM_KEY="+key+" AND LM_ASSCODE="+adminBO.getAssociateCode());
			cat.info(m_pst.toString());

			result=m_pst.executeUpdate();
		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.deleteLandMark-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.deleteLandMark-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;

	}
	public static ArrayList<OpenRequestHistory> getJobLogs(String tripId,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		ArrayList<OpenRequestHistory> ORLogs = new ArrayList<OpenRequestHistory>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs,rs1 = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		String firstValue="";
		String lastValue="";
		int count=0;
		int timeDifference;
		try {
			pst=con.prepareStatement("SELECT ORL_REASON,CONVERT_TZ(ORL_TIME,'UTC','"+adminBO.getTimeZone()+"') as TIME,ORL_CHANGED_BY,ORL_STATUS,case when ORL_STATUS='1' then 'INSERT JOB' when ORL_STATUS='5'then 'UPDATE JOB' when ORL_STATUS='10'then 'RESTART JOB' when ORL_STATUS='15'then 'PERFORMING DISPATCH' when ORL_STATUS='20'then 'BROADCAST JOB' when ORL_STATUS='25'then 'JOB ACCEPTED' when ORL_STATUS='30'then 'JOB REJECTED' when ORL_STATUS='35'then 'TRIP STARTED' when ORL_STATUS='40'then 'DRIVER ONROUTE' when ORL_STATUS='45'then 'JOB COMPLETED' when ORL_STATUS='55'then 'CUSTOMER CANCELLED' when ORL_STATUS='60'then 'OPERATOR NO SHOW' when ORL_STATUS='65'then 'DRIVER NO SHOW' when ORL_STATUS='70'then 'TRIP ON HOLD' when ORL_STATUS='0'then 'NO STATUS' when ORL_STATUS='50'then 'JOB COMPLETED' end as ORL_STATUSDESC FROM TDS_OPENREQUEST_LOGS WHERE ORL_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND ORL_TRIPID='"+tripId+"' ORDER BY ORL_TIME DESC,ORL_SNO DESC");
			rs = pst.executeQuery();
			while (rs.next()) {
				OpenRequestHistory ORHistory = new OpenRequestHistory();
				ORHistory.setReason(rs.getString("ORL_REASON"));
				ORHistory.setTime(rs.getString("TIME"));
				ORHistory.setUserId(rs.getString("ORL_CHANGED_BY"));
				ORHistory.setStatus(rs.getString("ORL_STATUSDESC"));
				ORLogs.add(ORHistory);
			}

		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.getJobLogs-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getJobLogs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ORLogs;

	}

	public static ArrayList<OpenRequestHistory> insertJobLogs(String associationCode,String tripId,String reason){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		ArrayList<OpenRequestHistory> ORLogs = new ArrayList<OpenRequestHistory>();
		OpenRequestBO ORBO=new OpenRequestBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs,rs1 = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		String firstValue="";
		String lastValue="";
		int count=0;
		int timeDifference;
		try {
			pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE) VALUES (?,NOW(),?,?,?)");
			pst.setString(1, ORBO.getTripid());
			pst.setString(2, reason);
			pst.setString(3, ORBO.getCreatedBy());
			pst.setString(5, associationCode);
			rs = pst.executeQuery();


		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.insertJobLogs-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertJobLogs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ORLogs;

	}
	public static ArrayList<OpenRequestBO> getLatLongiForTripId(String associationCode,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> driverLocation = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs,rs1 = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		String firstValue="";
		String lastValue="";
		int count=0;
		int timeDifference;
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_LOGS WHERE ORL_MASTER_ASSOCCODE='"+associationCode+"' AND ORL_TRIPID='"+tripId+"'");
			rs = pst.executeQuery();
			while (rs.next()) {
				OpenRequestBO ORBO = new OpenRequestBO();
				ORBO.setSlat(rs.getString("ORL_LATITUDE"));
				ORBO.setSlong(rs.getString("ORL_LONGITUDE"));
				ORBO.setStatus(rs.getString("ORL_STATUS"));
				driverLocation.add(ORBO);
			}

		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.getLatLongiForTripId-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getLatLongiForTripId-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverLocation;

	}

	public static ArrayList<OpenRequestHistory> getDetailsForJobs(String value,AdminRegistrationBO adminBO,String fromDate, String toDate,int mode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		ArrayList<OpenRequestHistory> ORDetails = new ArrayList<OpenRequestHistory>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst= null;
		ResultSet rs,rs1 = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		String firstValue="";
		String lastValue="";
		int count=0;
		int timeDifference;
		try {
			if(mode==1){
				pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_HISTORY WHERE ORH_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND ORH_PHONE='"+value+"' AND DATE_FORMAT(ORH_SERVICEDATE,'%m/%d/%Y')<='"+toDate+"' AND DATE_FORMAT(ORH_SERVICEDATE,'%m/%d/%Y')>='"+fromDate+"' order by ORH_SERVICEDATE DESC");
			}else if(mode==2){
				pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_HISTORY WHERE ORH_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND ORH_DRIVERID='"+value+"' AND DATE_FORMAT(ORH_SERVICEDATE,'%m/%d/%Y')<='"+toDate+"' AND DATE_FORMAT(ORH_SERVICEDATE,'%m/%d/%Y')>='"+fromDate+"' order by ORH_SERVICEDATE DESC");
			}
			rs = pst.executeQuery();
			while (rs.next()) {
				OpenRequestHistory ORHistory = new OpenRequestHistory();
				ORHistory.setTripid(rs.getString("ORH_TRIP_ID"));
				ORHistory.setDate(TDSValidation.getUserDateFormatMDY(rs.getString("ORH_SERVICEDATE")));
				ORHistory.setTime(rs.getString("ORH_SERVICETIME"));
				ORHistory.setDriverId(rs.getString("ORH_DRIVERID"));
				ORHistory.setVehicleNumber(rs.getString("ORH_VEHICLE_NO"));
				ORHistory.setName(rs.getString("ORH_NAME"));
				ORHistory.setPhoneNumber(rs.getString("ORH_PHONE"));
				ORDetails.add(ORHistory);
			}

		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.getDetailsForJobs-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDetailsForJobs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return ORDetails;

	}
	public static ArrayList<OpenRequestBO> getCompletedTrips(String phoneno,AdminRegistrationBO adminBo)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RegistrationDAO.getpreviousDetails");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		try {
			m_pst = m_conn.prepareStatement("select ORH_TRIP_ID,ORH_TRIP_STATUS,ORH_QUEUENO,ORH_DRCABFLAG,ORH_SMS_SENT,ORH_DRIVERID,ORH_DISPATCH_HISTORY_DRIVERS,ORH_PHONE,ORH_STADD1,ORH_STADD2,ORH_STCITY,ORH_STSTATE,ORH_STZIP,ORH_SPLINS,ORH_PAYACC,ORH_PAYTYPE,"+
					"ORH_EDADD1,ORH_EDADD2,ORH_EDCITY,ORH_ELANDMARK,ORH_SHARED_RIDE,ORH_NUMBER_OF_PASSENGERS,ORH_DROP_TIME,ORH_EDSTATE,ORH_PAYACC,ORH_QUEUENO,ORH_REPEAT_GROUP,ORH_PAYTYPE,ORH_DRCABFLAG,ORH_NAME,ORH_OPERATOR_COMMENTS,ORH_DRIVERID,ORH_LANDMARK,ORH_EDZIP,ORH_STLATITUDE,ORH_STLONGITUDE,ORH_EDLATITUDE,ORH_EDLONGITUDE,"+"DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%H%i') "+"as TIME, CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"') AS OFFSET_DATETIME from TDS_OPENREQUEST_HISTORY where ORH_PHONE='"+phoneno+"' AND ORH_ASSOCCODE='"+adminBo.getAssociateCode()+"'  AND ORH_TRIP_STATUS='61'");
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{
				OpenRequestBO orBO= new OpenRequestBO();
				orBO.setTripid(rs.getString("ORH_TRIP_ID"));
				orBO.setDriverid(rs.getString("ORH_DRIVERID"));
				orBO.setPhone(rs.getString("ORH_PHONE")); 
				orBO.setSadd1(rs.getString("ORH_STADD1"));
				orBO.setSadd2(rs.getString("ORH_STADD2"));
				orBO.setScity(rs.getString("ORH_STCITY"));
				orBO.setSstate(rs.getString("ORH_STSTATE"));
				orBO.setSzip(rs.getString("ORH_STZIP"));
				orBO.setSpecialIns(rs.getString("ORH_SPLINS")); 
				orBO.setEadd1(rs.getString("ORH_EDADD1"));
				orBO.setEadd2(rs.getString("ORH_EDADD2"));
				orBO.setEcity(rs.getString("ORH_EDCITY"));
				orBO.setEstate(rs.getString("ORH_EDSTATE"));
				orBO.setName(rs.getString("ORH_NAME"));
				orBO.setEzip(rs.getString("ORH_EDZIP"));
				orBO.setSlat(rs.getString("ORH_STLATITUDE"));
				orBO.setSlong(rs.getString("ORH_STLONGITUDE"));
				orBO.setEdlatitude(rs.getString("ORH_EDLATITUDE"));
				orBO.setEdlongitude(rs.getString("ORH_EDLONGITUDE"));
				orBO.setComments(rs.getString("ORH_OPERATOR_COMMENTS"));
				orBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("DATE")));
				orBO.setSttime((rs.getString("TIME")));
				orBO.setDriverid(rs.getString("ORH_DRIVERID"));
				orBO.setSlandmark(rs.getString("ORH_LANDMARK"));
				orBO.setPaytype((rs.getString("ORH_PAYTYPE")));
				orBO.setAcct(rs.getString("ORH_PAYACC"));
				orBO.setDrProfile(rs.getString("ORH_DRCABFLAG"));
				orBO.setQueueno(rs.getString("ORH_QUEUENO"));
				orBO.setTypeOfRide(rs.getString("ORH_SHARED_RIDE"));
				orBO.setRepeatGroup(rs.getString("ORH_REPEAT_GROUP"));
				orBO.setElandmark(rs.getString("ORH_ELANDMARK"));
				orBO.setNumOfPassengers(rs.getInt("ORH_NUMBER_OF_PASSENGERS"));
				orBO.setDropTime(rs.getString("ORH_DROP_TIME"));
				al_list.add(orBO);
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getCompletedTrips-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getCompletedTrips-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static void InsertLoginAttempts(String uName,String password,String ipAddress,String reason) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.InsertLoginAttempts");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		int result,rsult1=0;
		String assoccode="";
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			//Moving data  to login Attempts
			pst = con.prepareStatement("SELECT AU_ASSOCCODE FROM TDS_ADMINUSER WHERE AU_SNO='"+uName+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				assoccode=rs.getString("AU_ASSOCCODE");
			}
			pst = con.prepareStatement("insert into TDS_LOGIN_ATTEMPTS (LA_USERID,LA_PASSWORD,LA_IPADDRESS,LA_REASON,LA_TIME,LA_ASSOCCODE) VALUES (?,?,?,?,NOW(),?)");
			pst.setString(1,  uName);
			pst.setString(2,  password); 
			pst.setString(3,  ipAddress); 
			pst.setString(4,  reason); 
			pst.setString(5, assoccode);
			cat.info(pst.toString());
			pst.executeUpdate();
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.InsertLoginAttempts-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.InsertLoginAttempts-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	public static void setDefaultQueue(String Assoccode,String zoneNo,String driver) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.setDefaultQueue");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con  = dbcon.getConnection();
		try {
			pst = con.prepareStatement("INSERT INTO TDS_DRIVER_DEFAULTZONE (DD_ASSOCIATIONCODE,DD_DRIVERID,DD_ZONE) VALUES (?,?,?)");
			pst.setString(1, Assoccode);
			pst.setString(2, driver);
			pst.setString(3, zoneNo);
			pst.execute();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.setDefaultQueue-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.setDefaultQueue-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static void updateDriverZone(String zoneNo,String Assoccode,String driver) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.setDefaultQueue");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con  = dbcon.getConnection();
		try {
			pst = con.prepareStatement("UPDATE TDS_DRIVER_DEFAULTZONE SET DD_ZONE='"+zoneNo+"' WHERE DD_ASSOCIATIONCODE='"+Assoccode+"' AND DD_DRIVERID='"+driver+"'");
			pst.executeUpdate();
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.updateDriverZone-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.updateDriverZonee-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static String getZoneNo(String driver,String assoCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getZoneNo");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con  = dbcon.getConnection();
		String zoneNo="";
		try {
			pst = con.prepareStatement("SELECT DD_ZONE FROM TDS_DRIVER_DEFAULTZONE WHERE DD_DRIVERID='"+driver+"' AND DD_ASSOCIATIONCODE='"+assoCode+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				zoneNo=rs.getString("DD_ZONE");
			}
		} catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getZoneNo-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RegistrationDAO.getZoneNo-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return zoneNo;
	}
	public static ArrayList<LoginAttempts> loginAttempts(AdminRegistrationBO adminBO,String associationCode,String driverId,String fromDate,String toDate){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<LoginAttempts> loginAttempts = new ArrayList<LoginAttempts>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst= null;
		ResultSet rs = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		String query="SELECT *,DATE_FORMAT(CONVERT_TZ(LA_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i') AS LA_TIME_FORMATTED FROM TDS_LOGIN_ATTEMPTS  WHERE LA_ASSOCCODE='"+associationCode+"' ";
		if(driverId!=null && !driverId.equals("")){
			query=query+"AND LA_USERID='"+driverId+"' ";
		}
		if(fromDate!=null && !fromDate.equals("")){
			query=query+"AND LA_TIME >='"+fromDate+"' ";
		}
		if(toDate!=null && !toDate.equals("")){
			query=query+"AND LA_TIME <='"+toDate+"' ";
		}

		try {
			pst=con.prepareStatement(query);
			cat.info("query--->"+pst.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				LoginAttempts loginAttemptsBean = new LoginAttempts();
				loginAttemptsBean.setUserId(rs.getString("LA_USERID"));
				loginAttemptsBean.setIpAddress(rs.getString("LA_IPADDRESS"));
				loginAttemptsBean.setReason(rs.getString("LA_REASON"));
				loginAttemptsBean.setTime(rs.getString("LA_TIME_FORMATTED"));
				loginAttempts.add(loginAttemptsBean);
			}

		}catch (Exception sqex){
			cat.error("TDSException RequestDAO.loginAttempts-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.loginAttempts-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return loginAttempts;

	}

	public static String getCabNumber(String driverId,String associationCode,String cabNumber){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String result="";
		TDSConnection dbcon =  new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst= null;
		ResultSet rs = null;
		String query="";
		if(!driverId.equals("")){
			query="SELECT DL_VEHICLE_NO FROM TDS_DRIVERLOCATION WHERE DL_ASSOCCODE='"+associationCode+"' AND DL_DriverID= '"+driverId+"' ORDER BY DL_LASTUPDATE Desc";
		} else {
			query="SELECT DL_DriverID FROM TDS_DRIVERLOCATION WHERE DL_ASSOCCODE='"+associationCode+"' AND DL_VEHICLE_NO= '"+cabNumber+"' ORDER BY DL_LASTUPDATE Desc";
		}
		try {
			pst=con.prepareStatement(query);
			rs = pst.executeQuery();
			if (rs.next()) {
				if(!driverId.equals("")){
					result=rs.getString("DL_VEHICLE_NO");
				} else {
					result=rs.getString("DL_DriverID");			
				}
			}
		}catch (Exception sqex){
			cat.error("TDSException RequestDAO.getCabNumber-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.println("TDSException RequestDAO.getCabNumber-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}


	public static int updateJobDriver(String assocode,String driverId, String tripId,String cabNumber,int force){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con =dbcon.getConnection();
		PreparedStatement pst = null;
		int driverUpdated=0;
		String query="";
		query=" UPDATE TDS_OPENREQUEST ";
		if(driverId!=null && !driverId.equals("")){
			query=query+" SET OR_DRIVERID= '"+driverId+"'";
			if(cabNumber!=null && !cabNumber.equals("") && !cabNumber.equals("-1")){
				query=query+" , OR_VEHICLE_NO= '"+cabNumber+"'";
			}
		}else{
			if(cabNumber!=null && !cabNumber.equals("") && !cabNumber.equals("-1")){
				query=query+" SET OR_VEHICLE_NO= '"+cabNumber+"'";
			}
		}
		if(force==1){
			query=query+", OR_TRIP_STATUS='40' ";
		}else{
			query=query+", OR_TRIP_STATUS='37' ";
		}
		query=query+" WHERE OR_ASSOCCODE='"+assocode+"' AND OR_TRIP_ID ='"+tripId+"'";
		try{
			pst=con.prepareStatement(query);
			driverUpdated=pst.executeUpdate();
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.updateJobDriver-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateJobDriver-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverUpdated;
	}

	public static int updateJobDriverForShared(ArrayList<String> assocode,String associationCode,String driverId, String routeNo,String cabNumber,int force) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+associationCode+"')";
			}
		} else {
			fleets = "('"+associationCode+"')";
		}
		int orBo = updateJobDriverForShared(fleets,driverId,routeNo,cabNumber,force);
		return orBo;
	}


	public static int updateJobDriverForShared(String assocode,String driverId, String routeNo,String cabNumber,int force){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con =dbcon.getConnection();
		PreparedStatement pst = null;
		int driverUpdated=0;
		String query="";
		String sql ="";
		sql ="UPDATE TDS_SHARED_RIDE";

		query=" UPDATE TDS_OPENREQUEST ";
		if(driverId!=null){
			query=query+" SET OR_DRIVERID= '"+driverId+"'";
			sql=sql+" SET SR_DRIVERID='"+driverId+"'";
			if(cabNumber!=null && !cabNumber.equals("-1")){
				query=query+" , OR_VEHICLE_NO= '"+cabNumber+"'";
				sql=sql+" , SR_VEHICLE_NO='"+cabNumber+"'";
			}
		}else{
			if(cabNumber!=null && !cabNumber.equals("") && !cabNumber.equals("-1")){
				query=query+" SET OR_VEHICLE_NO= '"+cabNumber+"'";
				sql=sql+" SET SR_VEHICLE_NO='"+cabNumber+"'";
			}
		}
		if(force==1){
			query=query+", OR_TRIP_STATUS='40' ";
		}else{
			query=query+", OR_TRIP_STATUS='4' ";
		}
		query=query+" WHERE OR_ASSOCCODE IN "+assocode+" AND OR_ROUTE_NUMBER ='"+routeNo+"'";
		sql=sql+" WHERE SR_ASSOCCODE IN "+assocode+" AND SR_ROUTE_ID ='"+routeNo+"'";
		try{
			pst=con.prepareStatement(query);
			//System.out.println("query-->"+pst.toString());
			driverUpdated=pst.executeUpdate();

			pst=con.prepareStatement(sql);
			//System.out.println("query-->"+pst.toString());
			pst.executeUpdate();

		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.updateJobDriverForShared-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateJobDriverForShared-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverUpdated;
	}

	public static ArrayList<CustomerProfile> getCustomerProfile(String phoneno,String name,AdminRegistrationBO adminBO,int value,String masterKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO AddressDAO.Customerprofile");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ArrayList<CustomerProfile> profileHistory = new ArrayList<CustomerProfile>();
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query="";
			if(value==1){
				query = "select * from TDS_USERADDRESSMASTER where CU_MASTERASSOCIATIONCODE = '"+adminBO.getMasterAssociateCode()+"'AND CU_MASTER_KEY='"+masterKey+"'";
			}else{
				query = "select * from TDS_USERADDRESSMASTER where CU_MASTERASSOCIATIONCODE = '"+ adminBO.getMasterAssociateCode()+"'";


				if(!phoneno.equals("") && !name.equals("")){
					query = query + " and CU_PHONENO='"+phoneno+"'and CU_Name LIKE'"+name+"'";
				} else if (!phoneno.equals("") && name.equals("")){
					query = query + " and CU_PHONENO='"+phoneno+"'";					
				} else {
					query = query + " and CU_Name LIKE '"+name+"'";
				}
			}
			cat.info("Query pppp : "+query);

			m_pst = m_conn.prepareStatement(query);


			ResultSet rs = m_pst.executeQuery();

			while(rs.next())
			{
				CustomerProfile orBO = new CustomerProfile();

				orBO.setName(rs.getString("CU_NAME"));
				orBO.setAdd1(rs.getString("CU_ADD1"));
				orBO.setAdd2(rs.getString("CU_ADD2"));
				orBO.setNumber(rs.getString("CU_PHONENO"));
				orBO.setCity(rs.getString("CU_CITY"));
				orBO.setState(rs.getString("CU_STATE"));
				orBO.setZip(rs.getString("CU_ZIP"));
				orBO.setAccount(rs.getString("CU_ACCOUNT"));
				orBO.setMasterKey(rs.getString("CU_MASTER_KEY"));


				orBO.setCCprofile(rs.getString("CU_CCPROFILE"));
				orBO.setCCtype(rs.getString("CU_CCTYPE"));
				orBO.setLongitude(rs.getString("CU_LONGITUDE"));
				orBO.setLatitude(rs.getString("CU_LATITUDE"));

				profileHistory.add(orBO);
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException AddressDAO.Customerprofile-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.Customerprofile-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return profileHistory;
	}

	public static ArrayList<Address> getFromAndToAddress(String phoneno,int value,String masterKey,String addressKey,AdminRegistrationBO adminBO)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO AddressDAO.getFromandToAddress");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		ArrayList<Address> addressHistory = new ArrayList<Address>();

		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query1,query2="";
			ResultSet rs2=null;
			if(value==2){
				query1 = "select * from TDS_USERADDRESS where CU_ADDRESS_KEY='"+addressKey+"'AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'";
			}else if(value==1){
				query1="select * from TDS_USERADDRESS where CU_MASTER_KEY='"+masterKey+"'AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'";
				query2="SELECT CUL_LANDMARK_KEY,LM_NAME FROM TDS_USERLANDMARK,TDS_LANDMARK where CUL_MASTERKEY='"+masterKey+"' and CUL_LANDMARK_KEY=LM_KEY AND CUL_ASSOCIATIONCODE='"+adminBO.getAssociateCode()+"'";
			}else{
				query1 = "select * from TDS_USERADDRESS where CU_PHONENO='"+phoneno+"'AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'";
			}
			m_pst = m_conn.prepareStatement(query1);
			ResultSet rs1 = m_pst.executeQuery();
			if(!query2.equals("") && query2!=null){
				m_pst1=m_conn.prepareStatement(query2);
				rs2=m_pst1.executeQuery();
			}
			while(rs1.next())
			{
				Address orBO= new Address();
				orBO.setAdd1(rs1.getString("CU_ADD1"));
				orBO.setAdd2(rs1.getString("CU_ADD2"));
				orBO.setNumber(rs1.getString("CU_PHONENO"));
				orBO.setCity(rs1.getString("CU_CITY"));
				orBO.setState(rs1.getString("CU_STATE"));
				orBO.setZip(rs1.getString("CU_ZIP"));
				orBO.setAddressKey(rs1.getString("CU_ADDRESS_KEY"));
				if(rs2!=null){
					while(rs2.next()){
						orBO.setLandmarkKey(rs2.getString("CUL_LANDMARK_KEY"));
						orBO.setLandmarkName(rs2.getString("LM_NAME"));	
					}
				}
				addressHistory.add(orBO);

			}

		}catch (Exception sqex)
		{
			cat.error("TDSException AddressDAO.getFromandToAddress-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.getFromandToAddress-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 

		finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return addressHistory;
	}



	public static int deleteUserAddress(String addressKey,AdminRegistrationBO adminBO,String landmarkKey,String masterKey,int mode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO AddressDAO.deleteUserAddress");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		int delete=0;
		int result=0;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 

			if(mode==1){
				m_pst = m_conn.prepareStatement("DELETE FROM TDS_USERADDRESS where CU_ADDRESS_KEY=? AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'");
				m_pst.setString(1,  addressKey);
			}else if(mode==2){
				m_pst = m_conn.prepareStatement("DELETE FROM TDS_USERLANDMARK where CUL_MASTERKEY=? AND CUL_LANDMARK_KEY=? AND CUL_ASSOCIATIONCODE='"+adminBO.getAssociateCode()+"'");
				m_pst.setString(1,  masterKey);
				m_pst.setString(2,  landmarkKey);
			}
			cat.info("delete  query::"+m_pst.toString());
			delete = m_pst.executeUpdate();
		} catch (Exception sqex){
			cat.error("TDSException AddressDAO.deleteUserAddress-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.deleteUserAddress-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static ArrayList<DriverLocationHistoryBO> getDriverLocation(String associationCode,String lastUpdatedDate,String driverId,String fromDate,String toDate,int type){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DriverLocationHistoryBO> driverLocation = new ArrayList<DriverLocationHistoryBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst= null;
		ResultSet rs= null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="";
		if(!fromDate.equals("") && !toDate.equals("")){
			if(type==1){
				String fromDateFormated[] = fromDate.split("/");
				String toDateFormated[] = toDate.split("/");
				query=" AND DATE(DLH_LASTUPDATEDTIME) >= '"+fromDateFormated[2]+"-"+fromDateFormated[0]+"-"+fromDateFormated[1]+"' AND DATE(DLH_LASTUPDATEDTIME) <= '"+toDateFormated[2]+"-"+toDateFormated[0]+"-"+toDateFormated[1]+"'";
			} else {
				query=" AND DLH_LASTUPDATEDTIME >= '"+fromDate+"' AND DLH_LASTUPDATEDTIME <= '"+toDate+"'";
			}
		}
		if(driverId!=null && !driverId.equals("")){
			query = query+" AND DLH_DRIVERID='"+driverId+"'";
		}
		try {
			pst=con.prepareStatement("SELECT *,DATE_FORMAT(CONVERT_TZ(DLH_LASTUPDATEDTIME,'UTC','US/EASTERN'),'%Y/%m/%d %h:%i %p') as DT FROM TDS_DRIVERLOCATIONHISTORY WHERE DLH_ASSOCCODE='"+associationCode+"' AND DLH_LATITUDE > '0.000000' "+query+" GROUP BY DT");
			rs = pst.executeQuery();
			while (rs.next()) {
				DriverLocationHistoryBO driverLocationBO = new DriverLocationHistoryBO();
				driverLocationBO.setLatitude(rs.getString("DLH_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("DLH_LONGITUDE"));
				driverLocationBO.setStatus(rs.getString("DLH_SWITCH"));
				driverLocationBO.setUpdatedTime(rs.getString("DT"));
				driverLocationBO.setDriverid(rs.getString("DLH_DRIVERID"));
				driverLocationBO.setsNo(rs.getString("DLH_SNO"));
				driverLocation.add(driverLocationBO);
			}

		}catch (Exception sqex){
			cat.error("TDSException RequestDAO.getDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverLocation;

	}

	public static void insertZoneFlagSetup(ArrayList al_list,String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertCmpyFlagSetup");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();

		try {

			m_conn.setAutoCommit(false);


			m_pst = m_conn.prepareStatement("select * from TDS_ZONE_FLAG_SETUP where ZN_ASSOCODE='"+assocode+"'");
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();

			if(rs.first())
			{
				m_pst =  m_conn.prepareStatement("delete from TDS_ZONE_FLAG_SETUP where ZN_ASSOCODE='"+assocode+"'");
				cat.info(m_pst.toString());
				m_pst.execute();
			}

			m_pst = m_conn.prepareStatement("insert into TDS_ZONE_FLAG_SETUP (ZN_ASSOCODE,ZN_FLAG_VALUE,ZN_FLAG,ZN_FLAG_LNG_DESC,ZN_ENTERED_TIME)values(?,?,?,?,now())");

			for(int i=0;i<al_list.size();i++)
			{
				ZoneFlagBean znFlagBean = (ZoneFlagBean)al_list.get(i);
				if(!znFlagBean.getFlag1_value().equals(""))
				{
					m_pst.setString(1, znFlagBean.getAssocode());
					m_pst.setString(2, znFlagBean.getFlag1_value());
					m_pst.setString(3, znFlagBean.getFlag1());
					m_pst.setString(4, znFlagBean.getFlag1_lng_desc());
					cat.info(m_pst.toString());
					m_pst.execute();
				}



			}

			m_conn.commit();
			m_conn.setAutoCommit(true);
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.insertZoneFlagSetup-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertZoneFlagSetup-->"+sqex.getMessage());
			sqex.printStackTrace();

			try
			{
				m_conn.rollback();
			} catch (Exception e1) {
				// TODO: handle exception
			}
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}


	public static ArrayList getZoneFlag(String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getCmpyFlag");
		ArrayList al_list = new ArrayList();
		PreparedStatement m_pst = null;
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();

		try {

			m_pst = m_conn.prepareStatement("select * from TDS_ZONE_FLAG_SETUP where ZN_ASSOCODE='"+assocode+"' order by ZN_FLAG_VALUE");
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();

			while(rs.next())
			{
				ZoneFlagBean znFlagBean = new ZoneFlagBean();	
				znFlagBean.setFlag1(rs.getString("ZN_FLAG"));
				znFlagBean.setFlag1_value(rs.getString("ZN_FLAG_VALUE"));
				znFlagBean.setFlag1_lng_desc(rs.getString("ZN_FLAG_LNG_DESC"));
				al_list.add(znFlagBean);
			}


		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getZoneFlag-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getZoneFlag-->"+sqex.getMessage());
			sqex.printStackTrace();


		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}


	public static boolean deletezoneFlag(String  flagKey,String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		TDSConnection dbCon = new TDSConnection();
		Connection con = dbCon.getConnection();
		boolean result=false;
		try{
			m_pst =  con.prepareStatement("DELETE FROM TDS_ZONE_FLAG_SETUP WHERE ZN_ASSOCODE='"+assocode+"' AND ZN_FLAG_VALUE='"+flagKey+"'");
			m_pst.execute();
			//System.out.println("m-pst"+m_pst);
			result=true;
		} catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.deletezoneFlag-->"+sqex.getMessage());
			//System.out.println("TDSException RegistrationDAO.deletezoneFlag-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbCon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<HashMap<String, String>> getDriverRatingDetails(ArrayList<String> assocode,String driverId,String timeOffset,String masterAssoccode){
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+masterAssoccode+"')";
			}
		} else {
			fleets = "('"+masterAssoccode+"')";
		}
		ArrayList<HashMap<String, String>> orBo = getDriverRatingDetails(fleets,driverId,timeOffset);
		return orBo;
	}

	
	public static ArrayList<HashMap<String, String>> getDriverRatingDetails(String assoCode,String driverId,String timeOffset){
		long startTime = System.currentTimeMillis();
		ArrayList list=new ArrayList();
		PreparedStatement pst = null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		StringBuffer str=new StringBuffer();
		String query="SELECT A.CDR_TRIP_ID,A.CDR_CUSTOMER_ID,A.CDR_RATING,A.CDR_COMMENTS,B.DR_FNAME,B.DR_LNAME,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"'),'%m/%d/%Y %h:%i %p') AS FORMATEDDATE,CCU_USERNAME FROM TDS_CUSTOMER_DRIVER_RATING A INNER JOIN TDS_DRIVER B ON A.CDR_DRIVERID = B.DR_USERID INNER JOIN TDS_OPENREQUEST_HISTORY ON ORH_TRIP_ID = CDR_TRIP_ID INNER JOIN CLNT_CLIENTUSER ON CCU_USERID = CDR_CUSTOMER_ID WHERE A.CDR_DRIVERID=? AND A.CDR_ASSOCCODE IN "+assoCode+" GROUP BY CDR_TRIP_ID";
		try{
			pst = con.prepareStatement(query);
			pst.setString(1, driverId);
			rs=pst.executeQuery();
			while(rs.next()){
				HashMap<String, String> map =new HashMap<String, String>();
				map.put("tripId", rs.getString("CDR_TRIP_ID")!=null?rs.getString("CDR_TRIP_ID"):"");
				map.put("rating",rs.getString("CDR_RATING")!=null?rs.getString("CDR_RATING"):"");
				map.put("cmt", rs.getString("CDR_COMMENTS")!=null?rs.getString("CDR_COMMENTS"):"");
				map.put("name", (rs.getString("DR_FNAME")!=null?rs.getString("DR_FNAME"):"")+" "+(rs.getString("DR_LNAME")!=null?rs.getString("DR_LNAME"):""));
				map.put("date", rs.getString("FORMATEDDATE")!=null?rs.getString("FORMATEDDATE"):"");
				map.put("custName", rs.getString("CCU_USERNAME")!=null?rs.getString("CCU_USERNAME"):"");
				list.add(map);
			}
			rs.close();
			pst.close();
			con.close();
		} 
		catch(SQLException sqex) {
			cat.error("TDSException RegistrationDAO.getDriverRatingDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return list;	
	}
	
	public static ArrayList<DriverLocationHistoryBO> getDriverLocationGPS(AdminRegistrationBO adminBo,String driverId,String fromDate,String toDate){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DriverLocationHistoryBO> driverLocation = new ArrayList<DriverLocationHistoryBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst= null;
		ResultSet rs= null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="";
		/*if(!fromDate.equals("") && !toDate.equals("")){
			if(type==1){
				String fromDateFormated[] = fromDate.split("/");
				String toDateFormated[] = toDate.split("/");
				query=" AND DATE(DLH_LASTUPDATEDTIME) >= '"+fromDateFormated[2]+"-"+fromDateFormated[0]+"-"+fromDateFormated[1]+"' AND DATE(DLH_LASTUPDATEDTIME) <= '"+toDateFormated[2]+"-"+toDateFormated[0]+"-"+toDateFormated[1]+"'";
			} else {
				query=" AND DLH_LASTUPDATEDTIME >= '"+fromDate+"' AND DLH_LASTUPDATEDTIME <= '"+toDate+"'";
			}
		}
		if(driverId!=null && !driverId.equals("")){
			query = query+" AND DLH_DRIVERID='"+driverId+"'";
		}*/
		try {
			pst=con.prepareStatement("SELECT *,DATE_FORMAT(CONVERT_TZ(GPS_UPDATEDTIME,'UTC','"+adminBo.getTimeZone()+"'),'%Y/%m/%d %H:%i:%s') as DT,DATE_FORMAT(CONVERT_TZ(GPS_UPDATEDTIME,'UTC','"+adminBo.getTimeZone()+"'),'%H:%i %p') as Time FROM TDS_DRIVERGPSHISTORY WHERE GPS_ASSOCCODE='"+adminBo.getMasterAssociateCode()+"' AND GPS_DRIVERID='"+driverId+"'"
					+" AND CONVERT_TZ(GPS_UPDATEDTIME,'UTC','"+adminBo.getTimeZone()+"')<='"+toDate+"' AND CONVERT_TZ(GPS_UPDATEDTIME,'UTC','"+adminBo.getTimeZone()+"')>='"+fromDate+"'");
			
			//System.out.println("query:"+pst.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				DriverLocationHistoryBO driverLocationBO = new DriverLocationHistoryBO();
				driverLocationBO.setLatitude(rs.getString("GPS_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("GPS_LONGITUDE"));
				driverLocationBO.setStatus(rs.getString("GPS_SWITCH"));
				driverLocationBO.setUpdatedTime(rs.getString("DT"));
				driverLocationBO.setDriverid(rs.getString("GPS_DRIVERID"));
				driverLocationBO.setsNo(rs.getString("GPS_TRIPID"));
				driverLocationBO.setTimeDifference(rs.getString("Time"));
				driverLocation.add(driverLocationBO);
			}

		}catch (Exception sqex){
			cat.error("TDSException RequestDAO.getDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverLocation;

	}
	
	public static boolean updateAndroidIdFirstTimeforDriver(String androidId, String driverID, String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateAndroidIdforDriver");
		boolean m_insertStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER SET DR_AID = ? WHERE DR_SNO=? AND DR_ASSOCODE=?");
			m_pst.setString(1, androidId);
			m_pst.setString(2, driverID);
			m_pst.setString(3, assoccode);
			
			//System.out.println("update:"+m_pst.toString());
			if(m_pst.executeUpdate() > 0) {
				m_insertStatus = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.info("TDSException RegistrationDAO.updateAndroidIdforDriver->"+sqex.getMessage());
			cat.error(m_pst.toString());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_insertStatus;
	}
	
	public static boolean updateWaslReferenceForDriver(String reference_id, String json_str, String assoccode, String driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateWaslReferenceForDriver");
		boolean m_insertStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER SET DR_REFERENCE_NO = ?, DR_DETAILS_JSON = ? WHERE DR_SNO=? AND DR_ASSOCODE=?");
			m_pst.setString(1, reference_id);
			m_pst.setString(2, json_str);
			m_pst.setString(3, driverId);
			m_pst.setString(4, assoccode);
			
			//System.out.println("Driver WASL update : "+m_pst.toString());
			if(m_pst.executeUpdate() > 0) {
				m_insertStatus = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.info("TDSException RegistrationDAO.updateWaslReferenceForDriver->"+sqex.getMessage());
			cat.error(m_pst.toString());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_insertStatus;
	}
	
	public static boolean updateWaslReferenceForVehicle(String reference_id, String json_str, String assoccode, String cab, String v_no) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateWaslReferenceForVehicle");
		boolean m_insertStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("UPDATE TDS_VEHICLE SET V_REFERENCE_NO = ?, V_DETAILS_JSON = ? WHERE V_VNO=? AND V_ASSOCCODE=? AND V_RCNO=?");
			m_pst.setString(1, reference_id);
			m_pst.setString(2, json_str);
			m_pst.setString(3, cab);
			m_pst.setString(4, assoccode);
			m_pst.setString(5, v_no);
			
			//System.out.println("Vehicle WASL update : "+m_pst.toString());
			if(m_pst.executeUpdate() > 0) {
				m_insertStatus = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.info("TDSException RegistrationDAO.updateWaslReferenceForVehicle->"+sqex.getMessage());
			cat.error(m_pst.toString());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_insertStatus;
	}
	
	public static int checkCabAlreadyRegistered(CabRegistrationBean cab_bo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.checkCabAlreadyRegistered");
		int result=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		try {
			m_pst=m_conn.prepareStatement("SELECT * FROM TDS_VEHICLE WHERE V_ASSOCCODE='"+cab_bo.getCab_assocode()+"' AND (V_RCNO='"+cab_bo.getCab_rcno()+"' OR V_VNO='"+cab_bo.getCab_cabno()+"')");
			//System.out.println("query check : "+m_pst.toString());
			cat.info("Query---->"+m_pst.toString());
			rs=m_pst.executeQuery();
			if(rs.next()){
				result = 1;
			}
		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.checkCabAlreadyRegistered-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.deleteCab-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	
	public static int deleteCabByBean(CabRegistrationBean cabBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.deleteCabByBean");
		int result=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		try {
			m_pst=m_conn.prepareStatement("DELETE FROM TDS_VEHICLE WHERE V_RCNO='"+cabBO.getCab_rcno()+"' AND V_ASSOCCODE='"+cabBO.getCab_assocode()+"'");
			//System.out.println("Query---->"+m_pst.toString());
			result=m_pst.executeUpdate();
		}catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.deleteCabByBean-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	
	public static boolean updateWaslReferenceInLocation(String json_str, String assoccode, String driverId, String cabNo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateWaslReferenceInLocation");
		boolean m_insertStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_JSON_REFERENCE = ? WHERE DL_ASSOCCODE=? AND DL_DRIVERID=?  AND DL_VEHICLE_NO=?");
			m_pst.setString(1, json_str);
			m_pst.setString(2, assoccode);
			m_pst.setString(3, driverId);
			m_pst.setString(4, cabNo);
			
			//System.out.println("Location WASL update : "+m_pst.toString());
			if(m_pst.executeUpdate() > 0) {
				m_insertStatus = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.info("TDSException RegistrationDAO.updateWaslReferenceInLocation->"+sqex.getMessage());
			cat.error(m_pst.toString());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_insertStatus;
	}
	
	public static boolean updateWaslJSONforDriver(String json_str, String assoccode, String driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.updateWaslReferenceForDriver");
		boolean m_insertStatus = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER SET DR_DETAILS_JSON = ? WHERE DR_SNO=? AND DR_ASSOCODE=?");
			m_pst.setString(1, json_str);
			m_pst.setString(2, driverId);
			m_pst.setString(3, assoccode);
			
			//System.out.println("Driver WASL update : "+m_pst.toString());
			if(m_pst.executeUpdate() > 0) {
				m_insertStatus = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.info("TDSException RegistrationDAO.updateWaslReferenceForDriver->"+sqex.getMessage());
			cat.error(m_pst.toString());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_insertStatus;
	}
	
	public static ArrayList<QueueBean> getDriverReport(AdminRegistrationBO adminBo,String driverId,String fromDate,String toDate){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		
		ArrayList<QueueBean> driverList = new ArrayList<QueueBean>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst= null;
		ResultSet rs= null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="";
		try {
			
			// STR_TO_DATE('"+tdate+" 23:59:59', '%Y-%m-%d %H:%i:%s')
			// STR_TO_DATE('"+fdate+" 00:00:01', '%Y-%m-%d %H:%i:%s')
			
			String select_query = "SELECT *, TIMESTAMPDIFF(SECOND, SRH_OPEN_TIME, SRH_CLOSING_TIME) AS Sec, TIMESTAMPDIFF(MINUTE, SRH_OPEN_TIME,"
					+ " SRH_CLOSING_TIME) AS Min, TIMESTAMPDIFF(HOUR, SRH_OPEN_TIME, SRH_CLOSING_TIME) AS Hr,"
					+ " DATE_FORMAT(CONVERT_TZ(SRH_OPEN_TIME,'UTC','"+adminBo.getTimeZone()+"'),'%Y-%m-%d %h:%i %p') AS START,"
					+ " DATE_FORMAT(CONVERT_TZ(SRH_CLOSING_TIME,'UTC','"+adminBo.getTimeZone()+"'),'%Y-%m-%d %h:%i %p') AS END,"
					+ " CONCAT('',TIMEDIFF(SRH_CLOSING_TIME, SRH_OPEN_TIME)) AS Tot FROM TDS_SHIFT_REGISTER_MASTER_HISTORY WHERE"
					+ " SRH_OPEN_TIME BETWEEN CONVERT_TZ(STR_TO_DATE('"+fromDate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBo.getTimeZone()+"','UTC')"
					+ " AND CONVERT_TZ(STR_TO_DATE('"+toDate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBo.getTimeZone()+"','UTC')"
					+ " AND SRH_ASSOCCODE='"+adminBo.getMasterAssociateCode()+"'";
			
			if(driverId!=null && !driverId.equals("")){
				select_query = select_query + " AND SRH_OPERATORID='"+driverId+"' ORDER BY SRH_DATE";
			}else{
				select_query = select_query + " ORDER BY SRH_OPERATORID, SRH_DATE ";
			}
			pst=con.prepareStatement(select_query);
			//System.out.println("query:"+pst.toString());
			
			rs = pst.executeQuery();
			while (rs.next()) {
				QueueBean driver = new QueueBean();				
				driver.setDriverid(rs.getString("SRH_OPERATORID"));
				driver.setLO_Logintime(rs.getString("START"));
				driver.setLO_LogoutTime(rs.getString("END"));
				driver.setStartTimeStamp(rs.getString("Tot"));
				driver.setLastLoginTime(rs.getString("SRH_DATE"));
				driver.setQU_NAME(rs.getString("Sec"));
				driver.setFlg(0);
				driver.setVehicleNo(rs.getString("SRH_VEHICLE_NO"));
				//System.out.println("id:"+driver.getDriverid()+"--tot:"+rs.getString("Tot")+"--sec:"+rs.getString("Sec")+"--totttl:"+driver.getStartTimeStamp());
				driverList.add(driver);
			}

		}catch (Exception sqex){
			cat.error("TDSException RequestDAO.getDriverReport-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.println("TDSException RequestDAO.getDriverReport-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return driverList;
		
	}
	
}
