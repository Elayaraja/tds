package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.log4j.Category;
import org.apache.woden.wsdl20.Description;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.common.util.SendingSMS;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.CustomerMobileBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.MessageBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.common.util.TDSConstants;
import com.tds.util.TDSSQLConstants;
import com.common.util.TDSValidation;

public class ServiceRequestDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(ServiceRequestDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ServiceRequestDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
       public static ArrayList<OpenRequestBO> getOpenRequestDetailOnTime(String assocode, String masterAssocCode, String timeOffset,int sharedRideStatus) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		OpenRequestBO requestBO = null;
		PreparedStatement m_pst=null,m_pst1 = null;
		ResultSet m_rs,m_rs1 = null;
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String query="";
		TDSConnection m_tdsConn = new TDSConnection();
		Connection m_conn = m_tdsConn.getConnection();
		if(sharedRideStatus>0){
			query = " AND OR_SHARED_RIDE <> 1";
		}
		try {
			//OR_TRIP_STATUS (FOR TRIP STATUS CHECK TDSCONSTANT)
			m_pst = m_conn.prepareStatement("SELECT " +
					"  OR_TRIP_ID,OR_ASSOCCODE,OR_PHONE,OR_QUEUENO,OR_METER_TYPE,OR_TRIP_STATUS,OR_PAYTYPE,OR_DRCABFLAG, QD_DISPATCH_TYPE, QD_DISPATCH_DISTANCE,QD_STALE_CALL_TIME, OR_STLATITUDE, OR_STLONGITUDE, OR_DISPATCH_HISTORY_DRIVERS,OR_DONT_DISPATCH, OR_SHARED_RIDE," +
					"  case QD_DELAYTIME IS NULL WHEN true THEN  (OR_DISPATCH_LEAD_TIME * 60) ELSE CASE OR_DISPATCH_LEAD_TIME = -1 when true THEN QD_DELAYTIME*60 else (OR_DISPATCH_LEAD_TIME * 60) END END  AS QD_DELAYTIME," + 
					" OR_SMS_SENT,OR_TRIP_SOURCE,QD_PHONE_CALL_WAIT_TIME, QD_SMS_RES_TIME, QD_ADJACENT_ZONE_SORT_TYPE, OR_DRIVERID, OR_VEHICLE_NO, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"') AS OR_OFFSET_SERVICEDATE, "+
					" date_format(OR_SERVICEDATE,'%r') as st_time," +
					" time_to_sec(timediff(OR_SERVICEDATE,now())) as st_time_diff," +
					" time_to_sec(timediff(now(),OR_DISPATCH_START_TIME)) as stale_time," +
					//" case OR_DRCABFLAG when '|' then 2 else 1 end as sortOrder,"+
					"time_to_sec(timediff(now(),OR_SMS_SENT)) as  SMS_TIME_DIFF,QD.QD_BROADCAST_DELAY,"+
					/*					"  case time_to_sec(timediff(now(),OR_SMS_SENT)) >= QD_SMS_RES_TIME is null when true then 0 " +
					"  else case (time_to_sec(timediff(now(),OR_SMS_SENT)) >= QD_SMS_RES_TIME) when true then 1 else 2 end end as flg, " +
					 */
					"  OR_TRIP_STATUS,OR_RATINGS," +
					"  SRD_PICKUP_ORDER, OR_ROUTE_NUMBER  " +
					//					"  case OR_TRIP_STATUS when '' then 4 when 'P' then 2 when 'A' then 10 when 'B' then 15 else 40 end as chckStatus " +
					" FROM" +
					" TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD " +
					" ON  OR_QUEUENO = QD_QUEUENAME AND QD_ASSOCCODE = ? " +
					" LEFT JOIN TDS_SHARED_RIDE_DETAILS SRD "+
					" ON  SRD_TRIP_ID = OR_TRIP_ID AND SRD_ASSOCCODE = OPR.OR_ASSOCCODE "+
					" WHERE OPR.OR_TRIP_STATUS IN ('"+ TDSConstants.performingDispatchProcesses +"','"+ TDSConstants.performingDispatchProcessesCantFindDrivers +"','"+ TDSConstants.newRequestDispatchProcessesNotStarted +"','"+ TDSConstants.dummyTripId +"','"+ TDSConstants.srAllocatedToCab +"','"+ TDSConstants.broadcastRequest+"','"+TDSConstants.tripCompleted+"') " +
					" AND OPR.OR_ASSOCCODE = ? " +
					" AND OR_DISPATCH_START_TIME < NOW()"+query+
			" ORDER BY OR_QUEUENO,OR_DRCABFLAG,OR_TRIP_STATUS,OR_SERVICEDATE");

			m_pst.setString(1, masterAssocCode);
			m_pst.setString(2, assocode);
			cat.info(m_pst.toString());
			//System.out.println("query1 : "+m_pst);
			m_rs = m_pst.executeQuery();

			while(m_rs.next()) {

				requestBO = new OpenRequestBO();

				requestBO.setTripid(m_rs.getString("OR_TRIP_ID"));
				requestBO.setChckTripStatus(m_rs.getInt("OR_TRIP_STATUS"));
				requestBO.setTripStatus(m_rs.getString("OR_TRIP_STATUS"));
				requestBO.setSttime(m_rs.getString("st_time"));
				requestBO.setStaleCallTime(m_rs.getInt("stale_time"));
				requestBO.setStartTimeStamp (m_rs.getString("OR_OFFSET_SERVICEDATE"));
				requestBO.setQueueno(m_rs.getString("OR_QUEUENO"));
				requestBO.setDrProfile(m_rs.getString("OR_DRCABFLAG"));
				requestBO.setTripStatus(m_rs.getString("OR_TRIP_STATUS"));
				requestBO.setQC_DELAYTIME(m_rs.getInt("QD_DELAYTIME"));
				requestBO.setOR_SMS_SENT(m_rs.getString("OR_SMS_SENT"));
				requestBO.setQC_SMS_RES_TIME(m_rs.getInt("QD_SMS_RES_TIME"));
				requestBO.setSMS_TIME_DIFF(m_rs.getInt("SMS_TIME_DIFF"));
				if(m_rs.getString("QD_BROADCAST_DELAY") != null){
					requestBO.setBroadCastDelay(m_rs.getInt("QD_BROADCAST_DELAY"));
					requestBO.setDontMarkRequestsAsPublic(false);
				} else {
					requestBO.setDontMarkRequestsAsPublic(true);
				}
				requestBO.setTripSource(m_rs.getInt("OR_TRIP_SOURCE"));
				requestBO.setAssociateCode(m_rs.getString("OR_ASSOCCODE"));
				requestBO.setStaleCallTimeForZone(m_rs.getInt("QD_STALE_CALL_TIME"));
				requestBO.setPhoneCallDelayTime(m_rs.getInt("QD_PHONE_CALL_WAIT_TIME"));
				requestBO.setTimeToStartTripInSeconds(m_rs.getInt("st_time_diff"));
				requestBO.setPhone(m_rs.getString("OR_PHONE"));
				requestBO.setDriverid(m_rs.getString("OR_DRIVERID"));
				requestBO.setDispatchMethod(m_rs.getString("QD_DISPATCH_TYPE"));
				requestBO.setDispatchDistance(m_rs.getInt("QD_DISPATCH_DISTANCE"));
				requestBO.setSlat(m_rs.getString("OR_STLATITUDE"));
				requestBO.setSlong(m_rs.getString("OR_STLONGITUDE"));
				requestBO.setHistoryOfDrivers(m_rs.getString("OR_DISPATCH_HISTORY_DRIVERS"));
				requestBO.setPaytype(m_rs.getString("OR_PAYTYPE"));
				requestBO.setPaytype(m_rs.getString("OR_PAYTYPE"));
				requestBO.setDontDispatch(m_rs.getInt("OR_DONT_DISPATCH"));
				requestBO.setAdjacentZoneOrderType(m_rs.getInt("QD_ADJACENT_ZONE_SORT_TYPE"));
				requestBO.setVehicleNo(m_rs.getString("OR_VEHICLE_NO"));
				requestBO.setTypeOfRide(m_rs.getString("OR_SHARED_RIDE"));
				requestBO.setPickupOrder(m_rs.getString("SRD_PICKUP_ORDER"));
				requestBO.setRouteNumber(m_rs.getString("OR_ROUTE_NUMBER"));
				requestBO.setJobRating(m_rs.getInt("OR_RATINGS"));
				requestBO.setPaymentMeter(m_rs.getInt("OR_METER_TYPE"));
				if(m_rs.getInt("OR_METER_TYPE")==0){
					query = " AND MD_DEFAULT=1";
				} else {
					query = " AND MD_KEY='"+m_rs.getInt("OR_METER_TYPE")+"'";
				}
				m_pst1=m_conn.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+assocode+"'"+query);
				m_rs1=m_pst1.executeQuery();
				if(m_rs1.next()){
					requestBO.setRatePerMile(Double.parseDouble(m_rs1.getString("MD_RATE_PER_MILE")));
					requestBO.setRatePerMin(Double.parseDouble(m_rs1.getString("MD_RATE_PER_MIN")));
					requestBO.setStartAmt(Double.parseDouble(m_rs1.getString("MD_START_AMOUNT")));
					requestBO.setMinSpeed(Integer.parseInt(m_rs1.getString("MD_MINIMUM_SPEED")));
				}
				al_list.add(requestBO);
			}

		} catch (Exception sqex) {
			System.out.println("getOpenRequestDetailOnTime for Asscoocede: "+assocode);
			cat.error("TDSException ServiceRequestDAO.getOpenRequestDetailOnTime-->"+sqex.getMessage());
			//System.out.println("TDSException ServiceRequestDAO.getOpenRequestDetailOnTime-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}


	public static OpenRequestBO getDriverIdByTripId(String associationCode, String tripid,int routeOrTrip) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String phoneNumber="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query="";
		if(routeOrTrip==1){
			query=" AND OR_ROUTE_NUMBER = '"+tripid+"'";
		} else {
			query=" AND OR_TRIP_ID = '"+tripid+"'";
		}
		OpenRequestBO orBo = new OpenRequestBO(); 
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST WHERE OR_ASSOCCODE ='"+associationCode+"'"+query);
			cat.info(pst.toString());
			rs =pst.executeQuery();
			if (rs.first()) {
				orBo.setDriverid(rs.getString("OR_DRIVERID"));
				orBo.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				orBo.setTripid(rs.getString("OR_TRIP_ID"));
				orBo.setSadd1(rs.getString("OR_STADD1"));
				orBo.setSadd2(rs.getString("OR_STADD2"));
				orBo.setScity(rs.getString("OR_STCITY"));
				orBo.setSstate(rs.getString("OR_STSTATE"));
				orBo.setEadd1(rs.getString("OR_EDADD1"));
				orBo.setEadd2(rs.getString("OR_EDADD2"));
				orBo.setEcity(rs.getString("OR_EDCITY"));
				orBo.setEstate(rs.getString("OR_EDSTATE"));
				orBo.setPhone(rs.getString("OR_PHONE"));
				orBo.setSttime(TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME")));
				orBo.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getDriverIdByTripId-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getDriverIdByTripId-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return orBo;
	}




	public static int mlogoutupdatelocation(String driverid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("update TDS_DRIVERLOCATION  set DL_SWITCH='L' where DL_DRIVERID=?");
			pst.setString(1, driverid);
			cat.info(pst.toString());

			pst.executeUpdate();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.mlogoutupdatelocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.mlogoutupdatelocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static OpenRequestBO checkTripinAcceptedRequest(OpenRequestBO openRequestBO ) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In checkTripinOpenRequest Method");

		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = con.prepareStatement(TDSSQLConstants.checkTripIdinAcceptedRequest);
			pst.setString(1, openRequestBO.getTripid());
			pst.setString(2, openRequestBO.getAssociateCode());
			cat.info(pst.toString());

			rs =pst.executeQuery();
			if (rs.first()) {
				openRequestBO.setTripid(rs.getString("OR_TRIPID"));
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkTripinAcceptedRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkTripinAcceptedRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return openRequestBO;
	}

	public static String getPhoneNoByTripID(String associationCode, String driverID, String tripID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In checkTripinOpenRequest Method");
		String phoneNumber="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = ? AND OR_MASTER_ASSOCCODE = ? AND OR_DRIVERID = ?");
			pst.setString(1, tripID);
			pst.setString(2, associationCode);
			pst.setString(3, driverID);
			cat.info(pst.toString());
			rs =pst.executeQuery();
			if (rs.first()) {
				phoneNumber = rs.getString("OR_PHONE");
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getPhoneNoByTripID-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getPhoneNoByTripID-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return phoneNumber;
	}

	/*public static int setStartTrip(Map hm_startTrip,OpenRequestBO openRequestBO) {
		//cat.info("In serviceRequest setStart Trip ");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.updateInprogress);
			pst.setString(1, hm_startTrip.get("tripid").toString());
			pst.setString(2, hm_startTrip.get("driverid").toString());
			pst.setString(3, openRequestBO.getPhone());
			pst.setString(4,hm_startTrip.get("Slat").toString());
			pst.setString(5, hm_startTrip.get("Slong").toString());
			pst.setString(6,openRequestBO.getSadd1());
			pst.setString(7, openRequestBO.getSadd2());
			pst.setString(8, openRequestBO.getScity());
			pst.setString(9, openRequestBO.getSstate());
			pst.setString(10, openRequestBO.getSzip());
			pst.setString(11, openRequestBO.getEadd1());
			pst.setString(12, openRequestBO.getEadd2());
			pst.setString(13, openRequestBO.getEcity());
			pst.setString(14, openRequestBO.getEstate());
			pst.setString(15, openRequestBO.getEzip());

			pst.setString(16, openRequestBO.getSaircode());
			pst.setString(17, openRequestBO.getEaircode());
			pst.setString(18,openRequestBO.getShrs()+openRequestBO.getSmin());
			pst.setString(19, openRequestBO.getSdate());

			pst.setString(20, openRequestBO.getAssociateCode());
			pst.setString(21, openRequestBO.getEdlatitude());

			pst.setString(22, openRequestBO.getEdlongitude());

			pst.setString(23, openRequestBO.getRequestTime());
			pst.setString(24, openRequestBO.getAcceptedTime());
			pst.setString(25, openRequestBO.getName());
			System.out.println("Inprogress Query "+pst);
			result = pst.executeUpdate();
			if(result >0) {
				result = 0;
				pst = con.prepareStatement(TDSSQLConstants.deleteAcceptRequest);
				pst.setString(1, hm_startTrip.get("tripid").toString());
				result = pst.executeUpdate();
				//cat.info("Successfully inserted into Start Strip");
			} else {
				//cat.info("Failure to insert into Start Strip");
			}
			con.close();
			pst.close();
		} catch(SQLException sqex) {
			System.out.println("Error in Sql "+sqex.getMessage());
			//cat.info("Error in serviceRequest "+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}

		return result;
	}*/
	public static int mlogoutdeletelocation(String driverid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result =0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(TDSSQLConstants.deleteDriverFromlocation );
			pst.setString(1, driverid);
			cat.info(pst.toString());

			pst.executeUpdate();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//CAT is not defined
			cat.error("TDSException ServiceRequestDAO.mlogoutdeletelocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.mlogoutdeletelocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static int setStartTrip(OpenRequestBO openRequestBO,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+openRequestBO.getAssociateCode()+"')";
			}
		} else {
			fleets = "('"+openRequestBO.getAssociateCode()+"')";
		}
		int orBo = setStartTrip(openRequestBO,fleets);
		return orBo;
	}

	public static int setStartTrip(OpenRequestBO openRequestBO,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In serviceRequest setStart Trip ");
		int result = 0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		//		ResultSet rs =null;
		//		String phone="";
		try {
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST set OR_JOB_PICKUP_TIME = NOW(), OR_TRIP_STATUS = '"+TDSConstants.tripStarted+"' WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets+" and (OR_TRIP_STATUS = "+TDSConstants.onRotueToPickup+" OR OR_TRIP_STATUS = "+TDSConstants.jobAllocated+ " OR OR_TRIP_STATUS = "+TDSConstants.driverReportedNoShow+"  OR OR_TRIP_STATUS = "+TDSConstants.onSite+") and OR_DRIVERID = ?");
			pst.setString(1, openRequestBO.getTripid());
//			pst.setString(2, openRequestBO.getAssociateCode());
			pst.setString(2, openRequestBO.getDriverid());
			cat.info(pst.toString());

			result = pst.executeUpdate();
			con.close();
			pst.close();
		} catch(SQLException sqex) {
			//cat nat defined
			cat.error("TDSException ServiceRequestDAO.setStartTrip-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.setStartTrip-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static int setEndTrip(OpenRequestBO openRequestBO, boolean updateEndAddress,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+openRequestBO.getAssociateCode()+"')";
			}
		} else {
			fleets = "('"+openRequestBO.getAssociateCode()+"')";
		}
		int result = setEndTrip(openRequestBO, updateEndAddress,fleets);
		return result;
	}

	public static int setEndTrip(OpenRequestBO openRequestBO, boolean updateEndAddress,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In serviceRequest setStart Trip ");
		int result = 0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;

		try {
			if(updateEndAddress){
				pst = con.prepareStatement("update TDS_OPENREQUEST set OR_JOB_COMPLETE_TIME = NOW(), OR_EDADD1='"+ openRequestBO.getEadd1()+"', OR_EDADD2='"+ openRequestBO.getEadd2()+"',OR_END_QUEUENO='"+openRequestBO.getEndQueueno()+"', OR_TRIP_STATUS = '"+TDSConstants.tripCompleted+"', OR_EDCITY='"+openRequestBO.getEcity()+"',OR_EDSTATE='"+openRequestBO.getEstate()+"',OR_AMT='"+openRequestBO.getAmt()+"' WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets+" and (OR_TRIP_STATUS = "+TDSConstants.onRotueToPickup+" OR OR_TRIP_STATUS = "+TDSConstants.jobAllocated + " OR OR_TRIP_STATUS = "+TDSConstants.jobSTC + " OR OR_TRIP_STATUS = "+TDSConstants.tripStarted+  ") and OR_DRIVERID = ?");
			} else {
				pst = con.prepareStatement("update TDS_OPENREQUEST set OR_JOB_COMPLETE_TIME = NOW(), OR_TRIP_STATUS = '"+TDSConstants.tripCompleted+"',OR_END_QUEUENO='"+openRequestBO.getEndQueueno()+"',OR_AMT='"+openRequestBO.getAmt()+"' WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets+" and (OR_TRIP_STATUS = "+TDSConstants.onRotueToPickup+" OR OR_TRIP_STATUS = "+TDSConstants.jobAllocated + " OR OR_TRIP_STATUS = "+TDSConstants.jobSTC+ " OR OR_TRIP_STATUS = "+TDSConstants.tripStarted+  ") and OR_DRIVERID = ?");
			}
			pst.setString(1, openRequestBO.getTripid());
			pst.setString(2, openRequestBO.getDriverid());
			result = pst.executeUpdate();
			con.close();
			pst.close();
		} catch(SQLException sqex) {
			//cat nat defined
			cat.error("TDSException ServiceRequestDAO.setEndTrip-->"+sqex.getMessage());
			System.out.println("TDSException ServiceRequestDAO.setEndTrip-->"+pst);
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int setCustomerNoShow(OpenRequestBO openRequestBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In serviceRequest setStart Trip ");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("update TDS_OPENREQUEST set OR_TRIP_STATUS = '"+TDSConstants.noShowRequest+"' WHERE OR_TRIP_ID = ? and OR_ASSOCCODE=? and OR_DRIVERID = ?");
			pst.setString(1, openRequestBO.getTripid());
			pst.setString(2, openRequestBO.getAssociateCode());
			pst.setString(3, openRequestBO.getDriverid());
			cat.info(pst.toString());

			result = pst.executeUpdate();

			con.close();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.setStartTrip-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.setStartTrip-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int insertNewStartTrip(OpenRequestBO openRequestBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In serviceRequest setStart Trip ");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("insert into TDS_ACCEPTEDREQUEST (AR_TRIPID,AR_DRIVERKEY,AR_DRIVERID,AR_Slat,AR_Slong,AR_ASSOCCODE,AR_ACCEPTEDTIME,AR_SERVICEDATE,AR_REQUESTTIME,AR_TRIP_STATUS)values(?,?,?,?,?,?,now(),now(),now(),'I')");
			pst.setString(1, openRequestBO.getTripid());
			pst.setString(2, openRequestBO.getDriverid());
			pst.setString(3, openRequestBO.getDriverUName());
			pst.setString(4, openRequestBO.getSlat());
			pst.setString(5, openRequestBO.getSlong());
			pst.setString(6, openRequestBO.getAssociateCode());
			cat.info(pst.toString());

			result = pst.executeUpdate();	 			
			con.close();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.insertNewStartTrip-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.insertNewStartTrip-->"+sqex.getMessage());
			sqex.printStackTrace();
			//cat.info("Error in serviceRequest "+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int setNoShow(OpenRequestBO openRequestBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In setNoShow Trip ");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.updateInprogress);
			pst.setString(1, openRequestBO.getTripid());
			pst.setString(2, openRequestBO.getDriverid());
			pst.setString(3, openRequestBO.getPhone());
			pst.setString(4, openRequestBO.getSlat());
			pst.setString(5, openRequestBO.getSlong());
			pst.setString(6,openRequestBO.getSadd1());
			pst.setString(7, openRequestBO.getSadd2());
			pst.setString(8, openRequestBO.getScity());
			pst.setString(9, openRequestBO.getSstate());
			pst.setString(10, openRequestBO.getSzip());
			pst.setString(11, openRequestBO.getEadd1());
			pst.setString(12, openRequestBO.getEadd2());
			pst.setString(13, openRequestBO.getEcity());
			pst.setString(14, openRequestBO.getEstate());
			pst.setString(15, openRequestBO.getEzip());
			/*			pst.setString(16, openRequestBO.getSaircode());
			pst.setString(17, openRequestBO.getEaircode());
			 */			pst.setString(16,openRequestBO.getShrs()+openRequestBO.getSmin());
			 pst.setString(17, openRequestBO.getSdate());

			 pst.setString(18, openRequestBO.getAssociateCode());
			 pst.setString(19, openRequestBO.getEdlatitude());

			 pst.setString(20, openRequestBO.getEdlongitude());

			 pst.setString(21, openRequestBO.getRequestTime());
			 pst.setString(22, openRequestBO.getAcceptedTime());
			 pst.setString(23, openRequestBO.getName());
			 cat.info(pst.toString());

			 result = pst.executeUpdate();
			 //cat.info("Successfully inserted into No Show");

			 con.close();
			 pst.close();
		} catch(SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.setNoShow-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.setNoShow-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	//	public static OpenRequestBO checkDriverandTrip(OpenRequestBO openRequestBO) {
	//		//cat.info("In serviceRequest checkDriverandTrip ");
	//		 
	//		TDSConnection dbcon = null;
	//		Connection con = null;
	//		PreparedStatement pst = null;
	//		ResultSet rs = null;
	//		try {
	//			dbcon = new TDSConnection();
	//			con = dbcon.getConnection();
	//			//pst = con.prepareStatement(TDSSQLConstants.checkDriverandTrip);
	//			pst = con.prepareStatement("SELECT AR_DRIVERKEY,AR_TRIPID,AR_NAME,AR_ASSOCCODE,AR_QUEUENO,AR_CUSTOMERPHONE,AR_Slat,AR_Slong,AR_STADD1,AR_STADD2,AR_STCITY,AR_STSTATE,AR_STZIP,AR_EDLATITUDE,AR_EDLONGITUDE,AR_EDADD1,AR_EDADD2,AR_EDCITY,AR_EDSTATE,AR_EDZIP,AR_ASSOCCODE,AR_SAIRCODE,AR_EAIRCODE,AR_SERVICETIME,AR_SERVICEDATE,AR_REQUESTTIME,AR_ACCEPTEDTIME," +
	//					" case AR_TRIP_STATUS WHEN 'I' THEN 1 else 0 end as TRIP_STATUS FROM TDS_ACCEPTEDREQUEST WHERE AR_TRIPID = ?");
	//			pst.setString(1,openRequestBO.getTripid());
	//			cat.info(pst.toString());
	//			
	//			rs = pst.executeQuery();
	//			if(rs.next()) {
	//				openRequestBO.setTripid(rs.getString("AR_TRIPID")!=null?rs.getString("AR_TRIPID"):"");
	//				//openRequestBO.setDriverUName(rs.getString("AR_DRIVERID")!=null?rs.getString("AR_DRIVERID"):"");
	//				openRequestBO.setDriverid(rs.getString("AR_DRIVERKEY")!=null?rs.getString("AR_DRIVERKEY"):"");
	//				openRequestBO.setEadd1(rs.getString("AR_EDADD1")!=null?rs.getString("AR_EDADD1"):"");
	//				openRequestBO.setEadd2(rs.getString("AR_EDADD2")!=null?rs.getString("AR_EDADD2"):"");
	//				openRequestBO.setEcity(rs.getString("AR_EDCITY")!=null?rs.getString("AR_EDCITY"):"");
	//				openRequestBO.setEstate(rs.getString("AR_EDSTATE")!=null?rs.getString("AR_EDSTATE"):"");
	//				openRequestBO.setEzip(rs.getString("AR_EDZIP")!=null?rs.getString("AR_EDZIP"):"");
	//				openRequestBO.setIsBeandata(1);
	//				openRequestBO.setPhone(rs.getString("AR_CUSTOMERPHONE")!=null?rs.getString("AR_CUSTOMERPHONE"):"");
	//				openRequestBO.setSadd1(rs.getString("AR_STADD1")!=null?rs.getString("AR_STADD1"):"");
	//				openRequestBO.setSadd2(rs.getString("AR_STADD2")!=null?rs.getString("AR_STADD2"):"");
	//				openRequestBO.setScity(rs.getString("AR_STCITY")!=null?rs.getString("AR_STCITY"):"");
	//				openRequestBO.setSstate(rs.getString("AR_STSTATE")!=null?rs.getString("AR_STSTATE"):"");
	//				openRequestBO.setSzip(rs.getString("AR_STZIP")!=null?rs.getString("AR_STZIP"):"");
	//				openRequestBO.setAssociateCode(rs.getString("AR_ASSOCCODE")!=null?rs.getString("AR_ASSOCCODE"):"");
	//				openRequestBO.setSaircode(rs.getString("AR_SAIRCODE")!=null?rs.getString("AR_SAIRCODE"):"");
	//				openRequestBO.setEaircode(rs.getString("AR_EAIRCODE")!=null?rs.getString("AR_EAIRCODE"):"");
	//				openRequestBO.setSlong(rs.getString("AR_Slong")!=null?rs.getString("AR_Slong"):"");
	//				openRequestBO.setSlat(rs.getString("AR_Slat")!=null?rs.getString("AR_Slat"):"");
	//				openRequestBO.setEdlongitude(rs.getString("AR_EDLONGITUDE")!=null?rs.getString("AR_EDLONGITUDE"):"");
	//				openRequestBO.setEdlatitude(rs.getString("AR_EDLATITUDE")!=null?rs.getString("AR_EDLATITUDE"):"");
	//				openRequestBO.setSdate(rs.getString("AR_SERVICEDATE")!=null?rs.getString("AR_SERVICEDATE"):"");
	//				openRequestBO.setShrs(rs.getString("AR_SERVICETIME")!=null?rs.getString("AR_SERVICETIME").substring(0, 2):"");
	//				openRequestBO.setSmin(rs.getString("AR_SERVICETIME")!=null?rs.getString("AR_SERVICETIME").substring(2,4):"");
	//				openRequestBO.setAcceptedTime(rs.getString("AR_ACCEPTEDTIME"));
	//				openRequestBO.setRequestTime(rs.getString("AR_REQUESTTIME"));
	//				openRequestBO.setAssociateCode(rs.getString("AR_ASSOCCODE"));
	//				openRequestBO.setName(rs.getString("AR_NAME"));
	//				openRequestBO.setQueueno(rs.getString("AR_QUEUENO"));
	//				openRequestBO.setChckTripStatus(rs.getInt("TRIP_STATUS"));
	//			}
	//			
	//			con.close();
	//			pst.close();
	//		} catch(SQLException sqex) {
	//			cat.error("TDSException ServiceRequestDAO.checkDriverandTrip-->"+sqex.getMessage());
	//			System.out.println("TDSException ServiceRequestDAO.checkDriverandTrip-->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//			} finally {
	//			dbcon.closeConnection();
	//		}
	//		return openRequestBO;
	//	}

	public static int checkVoucherNo(String p_voucherno,String p_voucherName) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In serviceRequest checkVoucherNo ");
		//cat.info("Voucher no "+p_voucherno);
		//cat.info("Voucher Name "+p_voucherName);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		ResultSet rs = null;
		ResultSet rs1 = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst1=con.prepareStatement(TDSSQLConstants.checkVoucherNoExits);
			pst1.setString(1,p_voucherno);
			rs1=pst1.executeQuery();
			if(rs1.next()){
				pst = con.prepareStatement(TDSSQLConstants.checkVoucherAvailable);
				pst.setString(1, p_voucherno);
				pst.setString(2, p_voucherName);
				cat.info(pst.toString());

				rs = pst.executeQuery();
				if(rs.next()) {
					//cat.info("Given voucher  is availbale "+p_voucherno);
					result = 1;
				}	
				rs.close();
				pst.close();
			}
			else{
				result=2;
				//cat.info("Given voucher No is not present  "+p_voucherno);
			}
			rs1.close();
			pst1.close();						
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkVoucherNo-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkVoucherNo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static int updateCompletedRequest(HashMap hmp_completedRequest,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In checkCreditCard method ");
		int status = 0,result1=0,result2=0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			con.setAutoCommit(false);
			pst = con.prepareStatement(TDSSQLConstants.updateCompletedRequest);
			pst.setString(1, hmp_completedRequest.get("amount").toString());
			pst.setString(2, hmp_completedRequest.get("paymentType").toString());
			pst.setString(3, hmp_completedRequest.get("paymentNo").toString());
			pst.setString(4, hmp_completedRequest.get("tripid").toString());
			pst.setString(5, hmp_completedRequest.get("driverid").toString());
			pst.setString(6,adminBO.getAssociateCode());
			cat.info(pst.toString());

			result1 = pst.executeUpdate();
			//System.out.println("Result of completed Request "+ result1);
			pst1 = con.prepareStatement(TDSSQLConstants.insertPaymentApproval);
			pst1.setString(1,hmp_completedRequest.get("driverid").toString());
			pst1.setString(2, "Y");
			pst1.setString(3, hmp_completedRequest.get("paymentType").toString());
			pst1.setString(4, hmp_completedRequest.get("paymentNo").toString());
			pst1.setString(5, hmp_completedRequest.get("amount").toString());
			pst1.setString(6, hmp_completedRequest.get("tripid").toString());
			pst1.setString(7, hmp_completedRequest.get("cardType") == null ? "" :hmp_completedRequest.get("cardType").toString());
			pst1.setString(8, hmp_completedRequest.get("securityCode") == null ? "" :hmp_completedRequest.get("securityCode").toString());
			pst1.setString(9, hmp_completedRequest.get("assocCode").toString());
			cat.info(pst1.toString());

			//pst1.setBlob(6, null);
			result2 =  pst1.executeUpdate();

			//System.out.println("Result of Payment Approval "+ result2);
			if(result1 > 0 && result2 > 0 && !hmp_completedRequest.get("paymentType").toString().equalsIgnoreCase("CC")) {
				changeVoucherStatus(con,hmp_completedRequest.get("paymentNo").toString(),adminBO);
				//System.out.println("Change Voucher Status");
			}
			con.setAutoCommit(true);
			if(result1 > 0 && result2 > 0) {
				status = 1;
				//cat.info("Successfully Updated in TDS_COMPLETEDREQUEST");
			} else {
				//cat.info("Failure to update in TDS_COMPLETEDREQUEST");
			}
		} catch(SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateCompletedRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateCompletedRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	/*public static int insertOpenRequest(HashMap hmp_openRequestData) {
		//cat.info("In insertOpenRequest method ");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		String tripid = "";
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			tripid = "CC"+ConfigDAO.tdsKeyGen("TRIPID", con);
			//cat.info("New Trip ID "+tripid + hmp_openRequestData.get("driverid").toString());
			pst = con.prepareStatement(TDSSQLConstants.createOpenRequsest);
			pst.setString(1, tripid);
			pst.setString(2, hmp_openRequestData.get("driverid").toString());
			pst.setString(3, hmp_openRequestData.get("Slat").toString());
			pst.setString(4, hmp_openRequestData.get("Slong").toString());
			result = pst.executeUpdate();
			//cat.info("Accepted id result "+result);
			if(result > 0) {
				result = 0;
				pst = con.prepareStatement(TDSSQLConstants.updateInprogress);
				//cat.info("Successfully created OpenRequest in AcceptedRequest table");
				pst.setString(1, tripid);
				pst.setString(2, hmp_openRequestData.get("driverid").toString());
				pst.setString(3, " ");
				pst.setString(4, hmp_openRequestData.get("Slat").toString());
				pst.setString(5, hmp_openRequestData.get("Slong").toString());
				pst.setString(6," ");
				pst.setString(7, " ");
				pst.setString(8, " ");
				pst.setString(9, " ");
				pst.setString(10, " ");
				pst.setString(11, " ");
				pst.setString(12, " ");
				pst.setString(13, " ");
				pst.setString(14, " ");
				pst.setString(15, " ");
				pst.setString(16, " ");
				pst.setString(17, " ");
				pst.setString(18, " ");
				pst.setString(19, " ");
				pst.setString(20, " ");
				pst.setString(21, " ");
				pst.setString(22, " ");
				pst.setString(23, " ");
				pst.setString(24, " ");
				pst.setString(25, " ");

				result = pst.executeUpdate();
				if(result >0) {
					//cat.info("Successfully inserted into Start Strip");
				} else {
					//cat.info("Failure to insert into Start Strip");
				}
			} else {
				//cat.info("Failed to creat OpenRequest in AcceptedRequest table");
			}
			pst.close();
			con.close();

		}catch(SQLException sqex) {
			//cat.info("Error in serviceRequest "+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		return result;
	}*/


	public static ArrayList getProgressDataXML(OpenRequestBO openBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In getProgressDataXML method ");
		ArrayList al_PRData = null;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			al_PRData=new ArrayList();
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getInProgressData);
			cat.info(pst.toString());

			rs=pst.executeQuery();
			while(rs.next()){

				openBO.setTripid(rs.getString("IN_TRIPID"));
				openBO.setPhone(rs.getString("IN_PHONE"));
				openBO.setSadd1(rs.getString("IN_STADD1"));
				openBO.setSadd2(rs.getString("IN_STADD2"));
				openBO.setScity(rs.getString("IN_STCITY"));
				openBO.setSstate(rs.getString("IN_STSTATE"));
				openBO.setSzip(rs.getString("IN_STZIP"));
				openBO.setEadd1(rs.getString("IN_EDADD1"));
				openBO.setEadd2(rs.getString("IN_EDADD2"));
				openBO.setEcity(rs.getString("IN_EDCITY"));
				openBO.setEstate(rs.getString("IN_EDSTATE"));
				openBO.setEzip(rs.getString("IN_EDZIP"));
				openBO.setSlat(rs.getString("IN_Slat"));
				openBO.setSlong(rs.getString("IN_Slong"));

				openBO.setSttime(""+rs.getString("IN_STTIME"));				
				openBO.setDriverid(rs.getString("IN_DRIVERID"));
				al_PRData.add(openBO);
			}
			pst.close();
			rs.close();
			con.close();
		}
		catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getProgressDataXML-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getProgressDataXML-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_PRData;
	}
	public static ArrayList getAcceptedRequestDataXML(String userid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In getAcceptedRequestDataXML method ");
		ArrayList al_ARData = null;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		OpenRequestBO openRequestBO;
		try{
			al_ARData=new ArrayList();
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(userid != "" || userid.length()>0) {
				pst = con.prepareStatement(TDSSQLConstants.getAcceptRequestData+" WHERE AR_DRIVERID = '"+userid+"'");
				//System.out.println("in if statement "+TDSSQLConstants.getAcceptRequestData+" WHERE AR_DRIVERID = '"+userid+"'");
			}
			else {
				//System.out.println("in else statement "+TDSSQLConstants.getAcceptRequestData);
				pst = con.prepareStatement(TDSSQLConstants.getAcceptRequestData);
			}
			cat.info(pst.toString());

			rs=pst.executeQuery();
			while(rs.next()){
				openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("AR_TRIPID"));
				openRequestBO.setPhone(rs.getString("AR_CUSTOMERPHONE"));
				openRequestBO.setSadd1(rs.getString("AR_STADD1"));
				openRequestBO.setSadd2(rs.getString("AR_STADD2"));
				openRequestBO.setScity(rs.getString("AR_STCITY"));
				openRequestBO.setSstate(rs.getString("AR_STSTATE"));
				openRequestBO.setSzip(rs.getString("AR_STZIP"));
				openRequestBO.setEadd1(rs.getString("AR_EDADD1"));
				openRequestBO.setEadd2(rs.getString("AR_EDADD2"));
				openRequestBO.setEcity(rs.getString("AR_EDCITY"));
				openRequestBO.setEstate(rs.getString("AR_EDSTATE"));
				openRequestBO.setEzip(rs.getString("AR_EDZIP"));				

				openRequestBO.setDriverid(rs.getString("AR_DRIVERID"));
				openRequestBO.setSlat(rs.getString("AR_Slat"));
				openRequestBO.setSlong(rs.getString("AR_Slong"));
				al_ARData.add(openRequestBO);
			}
			pst.close();
			rs.close();
			con.close();
		}
		catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getAcceptedRequestDataXML-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getAcceptedRequestDataXML-->"+sqex.getMessage());
			sqex.printStackTrace();			}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_ARData;
	}

	public static int insertAcceptedRequest(OpenRequestBO openRequestBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In insertAcceptedRequest method ");
		int result = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		//ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.insertAccetptedRequest);
			pst.setString(1, openRequestBO.getTripid());
			pst.setString(2, openRequestBO.getDriverid());
			pst.setString(3, openRequestBO.getDriverUName());
			pst.setString(4, TDSValidation.getDBPhoneFormat(openRequestBO.getPhone()));
			pst.setString(5, openRequestBO.getSadd1());
			pst.setString(6, openRequestBO.getSadd2());
			pst.setString(7, openRequestBO.getScity());
			pst.setString(8, openRequestBO.getSstate());
			pst.setString(9, openRequestBO.getSzip());
			pst.setString(10, openRequestBO.getEadd1());
			pst.setString(11, openRequestBO.getEadd2());
			pst.setString(12, openRequestBO.getEcity());
			pst.setString(13, openRequestBO.getEstate());
			pst.setString(14, openRequestBO.getEzip());
			/*			pst.setString(15, openRequestBO.getSaircode());
			pst.setString(16, openRequestBO.getEaircode());
			 */			pst.setString(15,  openRequestBO.getShrs());
			 pst.setString(16, openRequestBO.getSdate());
			 pst.setString(17, openRequestBO.getAssociateCode());
			 pst.setString(18, "0000-00-00 00:00:00");
			 pst.setString(19, openRequestBO.getSlat());
			 pst.setString(20, openRequestBO.getEdlatitude());
			 pst.setString(21, openRequestBO.getSlong());
			 pst.setString(22, openRequestBO.getEdlongitude());
			 pst.setString(23, openRequestBO.getName());
			 pst.setString(24, openRequestBO.getQueueno());
			 pst.setString(25, openRequestBO.getSlandmark());
			 pst.setString(26, openRequestBO.getSintersection());
			 cat.info(pst.toString());

			 result = pst.executeUpdate();
			 if(result > 0) {
				 result = 0;

				 pst = con.prepareStatement(TDSSQLConstants.deleteOpenRequestData);
				 pst.setString(1, openRequestBO.getTripid());
				 result = pst.executeUpdate();
			 } 
			 pst.close();
			 //rs.close();
			 con.close();
		}
		catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.insertAcceptedRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.insertAcceptedRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int changeAcceptedRequest(OpenRequestBO openRequestBO,String action,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int status=0;
		//cat.info("In updateAcceptedRequest method ");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(action.equalsIgnoreCase("update")){
				pst=con.prepareStatement(TDSSQLConstants.updateAcceptRequest);
				pst.setString(1, openRequestBO.getDriverid());
				//cat.info("openRequestBO.getDriverid()"+openRequestBO.getDriverid());
				pst.setString(2, TDSValidation.getDBPhoneFormat(openRequestBO.getPhone()));
				//cat.info("openRequestBO.getPhone()"+TDSValidation.getDBPhoneFormat(openRequestBO.getPhone()));
				pst.setString(3, openRequestBO.getSlat());
				pst.setString(4,adminBO.getAssociateCode());
				//cat.info("openRequestBO.getSlat()"+openRequestBO.getSlat()!="" ?openRequestBO.getSlat():"0");
				//cat.info("openRequestBO.getSlong()"+openRequestBO.getSlong()!="" ?openRequestBO.getSlong():"0");
				//cat.info(" openRequestBO.getSadd1()"+ openRequestBO.getSadd1());
				//cat.info("openRequestBO.getSadd2()"+openRequestBO.getSadd2());

				//cat.info("openRequestBO.getScity()"+openRequestBO.getScity());
				//cat.info("openRequestBO.getSstate()"+openRequestBO.getSstate());
				//cat.info("openRequestBO.getSzip()"+openRequestBO.getSzip());
				//cat.info(" openRequestBO.getEdlatitude()"+ openRequestBO.getEdlatitude()!="" ?openRequestBO.getEdlatitude():"0");

				//cat.info("openRequestBO.getEdlongitude()"+openRequestBO.getEdlongitude()!="" ?openRequestBO.getEdlongitude():"0");
				//cat.info("openRequestBO.getEadd1()"+openRequestBO.getEadd1());

				//cat.info("openRequestBO.getEadd2()"+openRequestBO.getEadd2());
				//cat.info("openRequestBO.getEcity()"+openRequestBO.getEcity());
				//cat.info(" openRequestBO.getEstate()"+ openRequestBO.getEstate());
				//cat.info("openRequestBO.getEzip()"+openRequestBO.getEzip());
				//cat.info("openRequestBO.getSaircode()"+openRequestBO.getSaircode());
				//cat.info("openRequestBO.getEaircode()"+openRequestBO.getEaircode());
				//cat.info("TDSValidation.getDBdateFormat(openRequestBO.getSdate())"+TDSValidation.getDBdateFormat(openRequestBO.getSdate()));
				pst.setString(4, openRequestBO.getSlong());
				pst.setString(5, openRequestBO.getSadd1());
				pst.setString(6, openRequestBO.getSadd2());
				pst.setString(7, openRequestBO.getScity());
				pst.setString(8, openRequestBO.getSstate());
				pst.setString(9, openRequestBO.getSzip());
				pst.setString(10, openRequestBO.getEdlatitude());
				pst.setString(11, openRequestBO.getEdlongitude());
				pst.setString(12, openRequestBO.getEadd1());
				pst.setString(13, openRequestBO.getEadd2());
				pst.setString(14, openRequestBO.getEcity());
				pst.setString(15, openRequestBO.getEstate());
				pst.setString(16, openRequestBO.getEzip());
				/*				pst.setString(17, openRequestBO.getSaircode());
				pst.setString(18, openRequestBO.getEaircode());
				 */				pst.setString(17, TDSValidation.getDBdateFormat(openRequestBO.getSdate()));
				 //cat.info("openRequestBO.getShrs()+openRequestBO.getSmin()"+openRequestBO.getShrs()+openRequestBO.getSmin());
				 pst.setString(18, openRequestBO.getShrs()+openRequestBO.getSmin());
				 pst.setString(19, openRequestBO.getName());
				 pst.setString(20, openRequestBO.getQueueno());
				 pst.setString(21, openRequestBO.getSlandmark());
				 pst.setString(22, openRequestBO.getSintersection());
				 //cat.info("openRequestBO.getTripid()"+openRequestBO.getTripid());
				 pst.setString(25, openRequestBO.getTripid());
				 cat.info(pst.toString());

				 status=pst.executeUpdate();
				 //cat.info("The status for update is =="+status);
			}
			if(action.equalsIgnoreCase("delete")){
				pst=con.prepareStatement(TDSSQLConstants.deleteAcceptRequest);
				pst.setString(1, openRequestBO.getTripid());
				pst.setString(2,adminBO.getAssociateCode());
				status=pst.executeUpdate();
			}
		}
		catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.changeAcceptedRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.changeAcceptedRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static int insertDriverLocation (DriverLocationBO driverLocBO,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.insertDriverLocation);
			//System.out.println("Query "+pst);
			pst.setString(1, driverLocBO.getDriverid());
			pst.setString(2, driverLocBO.getLatitude());
			pst.setString(3, driverLocBO.getLongitude()) ;
			pst.setString(4, driverLocBO.getStatus());
			pst.setString(5, driverLocBO.getAssocCode());
			pst.setString(6, driverLocBO.getPhone());
			pst.setString(7, driverLocBO.getSmsid());
			pst.setString(8, driverLocBO.getVehicleNo());
			pst.setString(9, driverLocBO.getProvider());
			pst.setString(10, driverLocBO.getPushKey());
			pst.setString(11, driverLocBO.getProfile());
			pst.setString(12, masterAssoccode);
			cat.info(pst.toString());

			result = pst.executeUpdate();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.insertDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.insertDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int checkDriverExist(String driverid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In checkDriverExist");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs =  null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.checkDriverExist);
			pst.setString(1, driverid);
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.next()){
				result = 1;
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkDriverExist-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkDriverExist-->"+sqex.getMessage());
			sqex.printStackTrace();		
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int changeDriverRequest(String driverid,String newdriverid,String tripid,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In changeDriverRequest DAO");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;

		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.changeDriverRequest);
			pst.setString(1, driverid);
			cat.info(pst.toString());

			result = pst.executeUpdate();
			if(result > 0){
				//cat.info("Change Accepted "+result);
				int seq = ConfigDAO.tdsKeyGen("MESSAGEID",con);
				//cat.info("Message Key :"+"MS"+seq);
				pst =  con.prepareStatement(TDSSQLConstants.updateMessageTable);
				pst.setString(1,"MS"+seq);
				pst.setString(2, newdriverid);
				pst.setString(3, tripid);
				result +=pst.executeUpdate();
				pst1 = con.prepareStatement(TDSSQLConstants.updateAcceptedRequest);
				pst1.setString(1, newdriverid);
				pst1.setString(2, tripid);
				pst1.setString(3,adminBO.getAssociateCode());
				cat.info(pst1.toString());
				result += pst1.executeUpdate();
				if(result > 1){
					//cat.info("Change Accepted "+result);
				}
				else{
					result = 0;
				}
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.changeDriverRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.changeDriverRequest-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static ArrayList getMessage(String userid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In getMessage"+userid);
		ArrayList message_data =  new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs= null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getMessage);
			pst.setString(1,userid);
			cat.info(pst.toString());

			rs= pst.executeQuery();
			while(rs.next()){
				MessageBO messBo = new MessageBO();
				messBo.setDesc(rs.getString("MG_DESC")!=null?rs.getString("MG_DESC"):"");
				//messBo.setDriverid(rs.getString("MG_DRIVERID")!=null?rs.getString("MG_DRIVERID"):"");
				messBo.setMessagecode(rs.getString("MG_MESCODE")!=null?rs.getString("MG_MESCODE"):"");
				message_data.add(messBo);
			}
		}
		catch(SQLException sqex) {

			cat.error("TDSException ServiceRequestDAO.getMessage-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getMessage-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return message_data;
	}
	public static int updateDriverLocation(DriverLocationBO driverLocBO,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		try {
			if(!driverLocBO.getStatus().equals("")){
				pst = con.prepareStatement(TDSSQLConstants.updateDriverLocation);
				pst.setString(1, driverLocBO.getLatitude());
				pst.setString(2, driverLocBO.getLongitude()) ;
				pst.setString(3, driverLocBO.getStatus());
				pst.setString(4, masterAssoccode);
				pst.setString(5, driverLocBO.getDriverid());
				pst.setString(6, driverLocBO.getAssocCode());
			}else{
				pst=con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_LATITUDE = ?,DL_LONGITUDE = ?,DL_LASTUPDATE = NOW(),DL_MASTER_ASSOCCODE=? WHERE DL_DRIVERID = ? AND DL_ASSOCCODE = ?");
				pst.setString(1, driverLocBO.getLatitude());
				pst.setString(2, driverLocBO.getLongitude()) ;
				pst.setString(3, masterAssoccode);
				pst.setString(4, driverLocBO.getDriverid());
				pst.setString(5, driverLocBO.getAssocCode());
			
			}

			cat.info("UpdateDriverLocation-->"+pst);
			result = pst.executeUpdate();
			if(result<1){
				pst = con.prepareStatement(TDSSQLConstants.insertDriverLocationWithoutLoginTime);
				//System.out.println("Query "+pst);
				pst.setString(1, driverLocBO.getDriverid());
				pst.setString(2, driverLocBO.getLatitude());
				pst.setString(3, driverLocBO.getLongitude()) ;
				pst.setString(4, driverLocBO.getStatus());
				pst.setString(5, driverLocBO.getAssocCode());
				pst.setString(6, driverLocBO.getPhone());
				pst.setString(7, driverLocBO.getSmsid());
				pst.setString(8, driverLocBO.getVehicleNo());
				pst.setString(9, driverLocBO.getProvider());
				pst.setString(10, driverLocBO.getPushKey());
				pst.setString(11, driverLocBO.getProfile());
				pst.setString(12, driverLocBO.getDriverRatings()+"");
				pst.setString(13, driverLocBO.getAppVersion()+"");
				pst.setString(14, driverLocBO.getDrName());
				pst.setString(15, "");
				pst.setString(16, masterAssoccode);

				cat.info("InsertDriverLocation-->"+pst.toString());

				result = pst.executeUpdate();

			}
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());

			//System.out.println("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static int updateDriverDistance(DriverLocationBO driverLocBO,String distance) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		try {
			pst=con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_DISTANCE=? WHERE DL_DRIVERID = ? AND DL_ASSOCCODE = ?");
			pst.setString(1, distance);
			pst.setString(2, driverLocBO.getDriverid());
			pst.setString(3, driverLocBO.getAssocCode());
			
			System.out.println("UpdateDriverLocation-->"+pst.toString());
			result = pst.executeUpdate();
			
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateDriverDistance-->"+sqex.getMessage());
			cat.error(pst.toString());

			//System.out.println("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static boolean updateDriverAvailability(String availability,String driverId,String assoCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		boolean returnValue = false;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_SWITCH = ?,DL_LASTUPDATE = NOW() WHERE DL_DRIVERID = ? AND DL_ASSOCCODE = ?");
			pst.setString(1, availability);
			pst.setString(2, driverId);
			pst.setString(3, assoCode);
			cat.info("UpdateDriverLocation-->"+pst);
			result = pst.executeUpdate();
			if(result>0){
				returnValue = true;
			}
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());

			//System.out.println("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnValue;
	}
	public static int updateDriverLocationSP(DriverLocationBO driverLocBO,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareCall("call LocationUpdate('0.90','0.90','y', '105000', '105',@tmp)");

			pst.setString(1, driverLocBO.getLatitude());
			pst.setString(2, driverLocBO.getLongitude()) ;
			pst.setString(3, driverLocBO.getStatus());
			pst.setString(4, driverLocBO.getDriverid());
			cat.info("UpdateDriverLocation-->"+pst);
			result = pst.executeUpdate();
			if(result<1){
				pst = con.prepareStatement(TDSSQLConstants.insertDriverLocation);
				//System.out.println("Query "+pst);
				pst.setString(1, driverLocBO.getDriverid());
				pst.setString(2, driverLocBO.getLatitude());
				pst.setString(3, driverLocBO.getLongitude()) ;
				pst.setString(4, driverLocBO.getStatus());
				pst.setString(5, driverLocBO.getAssocCode());
				pst.setString(6, driverLocBO.getPhone());
				pst.setString(7, driverLocBO.getSmsid());
				pst.setString(8, driverLocBO.getVehicleNo());
				pst.setString(9, driverLocBO.getProvider());
				pst.setString(10, driverLocBO.getPushKey());
				pst.setString(11, driverLocBO.getProfile());
				pst.setString(12, masterAssoccode);

				cat.info("InsertDriverLocation-->"+pst.toString());

				result = pst.executeUpdate();

			}
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());

			//System.out.println("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static String getLoginTime(String associationCode, String windowTime, String driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In getLoginTime Method");
		String loginTime="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query ="SELECT DATE_SUB(NOW(),INTERVAL SUM(TIME_TO_SEC(TIMEDIFF(SRH_CLOSING_TIME,OPENTIME))) SECOND) AS LOGINTIME FROM (SELECT SRH_CLOSING_TIME,SRH_OPERATORID,CASE   WHEN SRH_OPEN_TIME < DATE_SUB(NOW(), INTERVAL '"+windowTime+"'  HOUR) THEN DATE_SUB(NOW(), INTERVAL '"+windowTime+"'  HOUR) WHEN SRH_OPEN_TIME >= DATE_SUB(NOW(), INTERVAL '"+windowTime+"'  HOUR)THEN SRH_OPEN_TIME END AS OPENTIME FROM TDS_SHIFT_REGISTER_MASTER_HISTORY WHERE SRH_CLOSING_TIME>DATE_SUB(NOW(), INTERVAL '"+windowTime+"' HOUR) AND SRH_ASSOCCODE ='"+associationCode+"' AND SRH_OPERATORID ='"+driverId+"' ) AS A GROUP BY SRH_OPERATORID";
		try {
			pst = con.prepareStatement(query);
			cat.info(pst.toString());
			rs =pst.executeQuery();
			if (rs.first()) {
				loginTime = rs.getString("LOGINTIME");
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getLoginTime-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getLoginTime-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return loginTime;
	}

	public static int updateDriverPhoneAndProvider(DriverLocationBO driverLocBO,String loginTime,String timeZone,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon =  new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		String sql = "SELECT MAX(CONVERT_TZ(LH_LOGOUT,'UTC','"+timeZone+"')) AS LASTLOGINTIME    from TDS_LOGIN_HISTORY  WHERE LH_DRIVERID ='"+driverLocBO.getDriverid()+"' AND LH_ASSCODE ='"+driverLocBO.getAssocCode()+"'";
		ResultSet rs = null;
		PreparedStatement m_pst = null;
		String lastLoginTime = "";
		try {
			m_pst = con.prepareStatement(sql);
			//System.out.println(" checking Query:"+m_pst.toString());
			rs =  m_pst.executeQuery();
			if(rs.next()){
				lastLoginTime = rs.getString("LASTLOGINTIME");
			}
			if(loginTime!=null&&loginTime!=""){
				pst = con.prepareStatement(TDSSQLConstants.updateDriverPhoneAndProvider);
				pst.setString(10, (loginTime!=null&&!loginTime.equals(""))?loginTime:"0000-00-00 00:00:00");
				pst.setString(11, driverLocBO.getDrName());
				pst.setString(12, lastLoginTime);
//				pst.setString(13, phoneVersion);
				pst.setString(13, driverLocBO.getDriverid());
			}else{
				pst = con.prepareStatement(TDSSQLConstants.updateDriverPhoneAndProviderWithoutLoginTime);
				pst.setString(10, driverLocBO.getDrName());
				pst.setString(11, lastLoginTime);
//				pst.setString(12, phoneVersion);
				pst.setString(12, driverLocBO.getDriverid());
			}
			pst.setString(1, driverLocBO.getPhone());
			pst.setString(2, driverLocBO.getProvider());
			pst.setString(3, driverLocBO.getPushKey());
			pst.setString(4, driverLocBO.getProfile());
			pst.setString(5, driverLocBO.getVehicleNo());
			pst.setString(6, driverLocBO.getAssocCode());
			pst.setInt(7, driverLocBO.getDriverRatings());
			pst.setString(8, driverLocBO.getAppVersion());
			pst.setInt(9, driverLocBO.getDriverRatings());
			
			cat.info("Query in Update Driver Location "+pst);
			result = pst.executeUpdate();
			if(result<1){
				
				if(loginTime!=null&&loginTime!=""){
					pst = con.prepareStatement(TDSSQLConstants.insertDriverLocation);
					pst.setString(14,  loginTime!=null&&loginTime!=""?loginTime:"0000-00-00 00:00:00");
					pst.setString(15, driverLocBO.getDrName());
					pst.setString(16, lastLoginTime);
					pst.setString(17, masterAssoccode);
//					pst.setString(17, phoneVersion);
				}else{
					pst = con.prepareStatement(TDSSQLConstants.insertDriverLocationWithoutLoginTime);
					pst.setString(14, driverLocBO.getDrName());
					pst.setString(15, lastLoginTime);
					pst.setString(16, masterAssoccode);
//					pst.setString(16, phoneVersion);
				}
				//System.out.println("Query "+pst);
				pst.setString(1, driverLocBO.getDriverid());
				pst.setString(2, driverLocBO.getLatitude());
				pst.setString(3, driverLocBO.getLongitude()) ;
				pst.setString(4, driverLocBO.getStatus());
				pst.setString(5, driverLocBO.getAssocCode());
				pst.setString(6, driverLocBO.getPhone());
				pst.setString(7, driverLocBO.getSmsid());
				pst.setString(8, driverLocBO.getVehicleNo());
				pst.setString(9, driverLocBO.getProvider());
				pst.setString(10, driverLocBO.getPushKey());
				pst.setString(11, driverLocBO.getProfile());
				pst.setInt(12, driverLocBO.getDriverRatings());
				pst.setString(13, driverLocBO.getAppVersion());
				cat.info("InsertDriverLocation-->"+pst.toString());
				//System.out.println("Checking query "+pst.toString());
				result = pst.executeUpdate();
			}
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateDriverPhoneAndProvider-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static int checkDriverLocation (String driverid) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null; 
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.checkDriverLocation );
			pst.setString(1, driverid);
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.next()) {
				result = 1;
			}
			rs.close();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			cat.error("Error in checkDriverLocation "+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int setDriverQueue (String driverid,String queuename,String assoccode,int p_qpostion, String drProfile, String phoneNo, String provider, String googRegKey, int getJobsFromAdjacentZones,String vechicleNo,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet m_rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement(TDSSQLConstants.insertDriverQueue );
			pst.setString(1, queuename);
			pst.setString(2, driverid);
			pst.setString(3, assoccode);
			pst.setInt(4, p_qpostion);
			pst.setString(5, drProfile);
			pst.setString(6, phoneNo);
			pst.setString(7, provider);
			pst.setString(8, googRegKey);
			pst.setInt(9, getJobsFromAdjacentZones);
			pst.setString(10, vechicleNo);
			pst.setString(11,masterAssoccode);
			cat.info(pst.toString());

			result = pst.executeUpdate();

			pst.close();
			
			pst=con.prepareStatement(TDSSQLConstants.insertDriverZoneLogin);
			pst.setString(1, driverid);
			pst.setString(2, queuename);
			pst.setString(3, assoccode);
			result=pst.executeUpdate();
			pst.close();
			
			con.close();
			//System.out.println("Result in setDriver Queue "+result);
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.setDriverQueue-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.setDriverQueue-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static int getNumberOfDriversInQueue (String queuename,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet m_rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement(TDSSQLConstants.countOfDriversInAQueue);
			pst.setString(1, queuename);
			pst.setString(2, assoccode);
			cat.info(pst.toString());

			m_rs = pst.executeQuery();
			if(m_rs.next()){
				result = m_rs.getInt("NumberOfDrivers");
			} else {
				result = -1;
			}

			pst.close();
			con.close();
			//System.out.println("Result in setDriver Queue "+result);
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.setDriverQueue-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.setDriverQueue-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	/**
	 * @param p_keyword
	 * @author vimal
	 * @see Description
	 * This method is used to generate queue postion of a driver
	 * @return
	 */
	public static int getQueuePosition(String p_queueId, String driverID, String associationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In DAO KeyGen Method");
		TDSConnection m_dbConn = null;
		Connection m_con = null;
		PreparedStatement pst = null;
		ResultSet m_rs = null;
		int driverPosition = 0;
		try{
			m_dbConn = new TDSConnection();
			m_con = m_dbConn.getConnection();
			pst = m_con.prepareStatement(TDSSQLConstants.getQueueNumForDriverID);
			pst.setString(1, p_queueId);
			pst.setString(2, associationCode);
			pst.setString(3, driverID);
			cat.info(pst.toString());
			boolean foundDriver = false;
			m_rs = pst.executeQuery();
			if(m_rs.next()) {
				driverPosition = m_rs.getInt("DRIVERORDER");
			}
			pst.close();
			m_rs.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getQueuePosition-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getQueuePosition-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return driverPosition;
	}
	
	
	
	public static  ArrayList<DriverCabQueueBean> getZoneDetails(String zone, String masterAssocode, String associationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_dbConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<DriverCabQueueBean> driverzone=new ArrayList<DriverCabQueueBean> ();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst=con.prepareStatement("SELECT QU_DRIVERID,QU_VEHICLE_NO FROM TDS_QUEUE WHERE QU_NAME ='"+zone+"' AND QU_ASSOCCODE='"+associationCode+"' AND QU_MASTER_ASSOCCODE='"+masterAssocode+"' ORDER BY QU_QUEUEPOSITION");	
			rs=pst.executeQuery();
			while(rs.next()){
				DriverCabQueueBean driver=new DriverCabQueueBean();
				driver.setDriverid(rs.getString("QU_DRIVERID"));
				driver.setCabno(rs.getString("QU_VEHICLE_NO"));
				driverzone.add(driver);
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException ServiceRequestDAO.getZoneDetails-->"+sqex.getMessage());
			//System.out.println("TDSException ServiceRequestDAO.getZoneDetails" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return driverzone;
	}
	
	public static  ArrayList<QueueBean> getZoneHistoryDetails(String zone, String masterAssocode, String associationCode,String driverId,String timezone) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_dbConn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<QueueBean> driverzone=new ArrayList<QueueBean> ();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection(); 
		try {
			pst=con.prepareStatement("SELECT DZH_DRIVERID,DZH_CABNO,CONVERT_TZ(DZH_LOGOUTTIME,'UTC','"+timezone+"') AS DZH_LOGOUTTIME,DZH_REASON FROM TDS_DRIVER_ZONE_HISTORY WHERE DZH_ZONEID ='"+zone+"'  AND DZH_DRIVERID='"+driverId+"'  AND DZH_SWITCH='2' AND DZH_ASSOCCODE='"+associationCode+"' AND DZH_MASTER_ASSOCCODE='"+masterAssocode+"' ORDER BY DZH_LOGOUTTIME DESC LIMIT 5");	
			rs=pst.executeQuery();
			while(rs.next()){
				QueueBean driver=new QueueBean();
				driver.setDriverId(rs.getString("DZH_DRIVERID"));
				driver.setVehicleNo(rs.getString("DZH_CABNO"));
				driver.setDq_LogoutTime(rs.getString("DZH_LOGOUTTIME"));
				driver.setReason(rs.getString("DZH_REASON"));
				driverzone.add(driver);
			}
			pst.close();
			rs.close();
		} catch (SQLException  sqex) {
			cat.error("TDSException ServiceRequestDAO.getZoneHistoryDetails-->"+sqex.getMessage());
			//System.out.println("TDSException ServiceRequestDAO.getZoneDetails" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return driverzone;
	}

	

	public static boolean checkQueueNo (String p_queueId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_status = false;
		TDSConnection m_dbcon = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_resultSet = null;
		try {
			m_dbcon = new TDSConnection();
			m_con = m_dbcon.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.checkQueueIdPresent);
			cat.info(m_pst.toString());

			m_resultSet = m_pst.executeQuery();
			if(m_resultSet.next()) {
				m_status = true;
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkQueueNo-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkQueueNo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_status;
	}

	/**
	 * @param p_queueId
	 * @return position of the particular Queue.
	 * @author vimal
	 * @see Description
	 * This method is used to insert the QueueId into the Sequence table.
	 */
	public static int insertQueuePostition (String p_queueId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int m_postion = -1;
		TDSConnection m_dbcon = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		try {
			m_dbcon = new TDSConnection();
			m_con = m_dbcon.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.insertQueuePosition);
			m_pst.setString(1, p_queueId);
			cat.info(m_pst.toString());

			if(m_pst.executeUpdate() > 0) {
				m_postion = 1;
			}
			m_pst.close();
			m_con.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.insertQueuePostition-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.insertQueuePostition-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_postion;
	}


	public static String getQueueID (String latitude,String longitude) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String result = "0";
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null; 
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getQueueID );
			pst.setString(1, longitude);
			pst.setString(2, latitude);
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.next()) {
				result = rs.getString("QD_QUEUENAME");
			}
			rs.close();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getQueueID-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getQueueID-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList getQueueName(String latitude,String longitude,AdminRegistrationBO aBo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String result = "0";
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList al_list = new ArrayList();
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(latitude.equals("0") && longitude.equalsIgnoreCase("0"))
			{
				pst = con.prepareStatement("select QD_QUEUENAME,QD_DESCRIPTION from TDS_QUEUE_DETAILS "+(aBo!=null?" WHERE QD_ASSOCCODE='"+aBo.getAssociateCode()+"'":"")+"");

				//System.out.println("if zone"+pst.toString());
			}else{	
				pst = con.prepareStatement("select QD_QUEUENAME,QD_DESCRIPTION from TDS_QUEUE_DETAILS where ? BETWEEN QD_LONGITUDEWEST AND QD_LONGITUDEEAST AND ? BETWEEN QD_LATITUDESOUTH AND QD_LATITUDENORTH "+(aBo!=null?" AND QD_ASSOCCODE='"+aBo.getAssociateCode()+"'":"")+"");
				pst.setString(1, longitude);
				pst.setString(2, latitude);
				//System.out.println("else zone"+pst.toString());
			}

			//System.out.println(""+pst.toString());
			//cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				al_list.add(rs.getString("QD_QUEUENAME"));
				al_list.add(rs.getString("QD_DESCRIPTION"));
			}
			rs.close();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getQueueName-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getQueueName-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	/**
	 * @param p_driverId
	 * @return Map with queue name and queue position
	 */
	public static Map getDriverQueueOrder(String p_driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		Map m_queueDetail = null;
		TDSConnection tdsConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			tdsConn = new TDSConnection();
			conn = tdsConn.getConnection();
			pst = conn.prepareStatement(TDSSQLConstants.getDriverQueueDetail);
			pst.setString(1, p_driverId);
			cat.info(pst.toString());

			//System.out.println("Query in prepare statement "+pst);
			rs = pst.executeQuery();
			if(rs.next()) {
				m_queueDetail = new HashMap();
				m_queueDetail.put("queueId",rs.getString("QU_NAME"));
				m_queueDetail.put("position", rs.getString("QU_QUEUEPOSITION"));
			}
			pst.close();
			conn.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getDriverQueueOrder-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getDriverQueueOrder-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_queueDetail;
	}

	/**
	 * @param p_userId, p_availStatus
	 * @return boolean true for update Driver is not available and false means if there is an error.
	 * @see Description This method is used to update the driver availability based on the available status and the user id.
	 */
	public static boolean updateDriverAvailabilty(String p_availStatus, String p_userId, String associationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_availabilityStatus = false;
		TDSConnection m_dbCon = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbCon = new TDSConnection();
			m_conn = m_dbCon.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateDriverAvailabilityOnly);
			m_pst.setString(1, p_availStatus);
			m_pst.setString(2, p_userId);
			m_pst.setString(3, associationCode);
			cat.info(m_pst.toString());
			if(m_pst.executeUpdate() > 0 ) {
				m_availabilityStatus = true;
			}
			m_pst.close();
			m_conn.close();

		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateDriverAvailabilty-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateDriverAvailabilty-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbCon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_availabilityStatus;
	}


	/**
	 * @see descrition 
	 * Method is used to get a priority driverid from a Queue table based on the QueueID
	 * @param p_queueId
	 * @return m_driverId
	 */
	/*public static String getDriverid(String p_queueId, String associationCode) {
		String m_driverId = "";
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getQueueOrder);
			//FROM TDS_QUEUE WHERE QU_NAME LIKE ? ORDER BY QU_LOGINTIME LIMIT 1
			m_pst.setString(1, p_queueId);
			m_pst.setString(2, associationCode);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			if (m_rs.next()) {
				m_driverId = m_rs.getString("QU_DRIVERID");
			}


			/*m_pst = m_conn.prepareStatement("select * from TDS_WORK limit 1");
			m_rs = m_pst.executeQuery();

			if(m_rs.first())
			{
				try{
				 byte[] bytearray = m_rs.getBytes("TW_RIDERSIGN");
				 File f=new File("/root/Desktop/Sample_"+System.currentTimeMillis()+".gif");
			     FileOutputStream fop=new FileOutputStream(f);
			     fop.write(bytearray);
			     fop.close();
				}catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
			}

			m_pst.close();
			m_conn.close();


		} catch (SQLException sqex) {
			//cat.info("Error in getDriverid method "+sqex.getMessage());
		} finally {
			m_tdsConn.closeConnection();
		}
		return m_driverId;
	}
	 */


	/**
	 * @see 	Following Method is used to get the phone number of a Driver.
	 * @author vimal
	 * @exception SQLException Error in getDriverPhoneNumber method
	 * @param p_driverId
	 * @return m_phoneNo , m_smsID
	 */
	public static Map getDriverPhoneNumber(String p_driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		Map m_driverMap = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getDriverPhoneNumber);
			m_pst.setString(1, p_driverId);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			if (m_rs.next()) {
				m_driverMap = new HashMap();
				m_driverMap.put("phoneNo", m_rs.getString("DR_PHONE"));
				m_driverMap.put("smsId", "@"+m_rs.getString("DR_SMSID").toString());
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getDriverPhoneNumber-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getDriverPhoneNumber-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_driverMap;
	}


	public static String getDriverPhoneNumberForQueue(String p_driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String result="";
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			//m_pst = m_conn.prepareStatement("SELECT concat(DR_PHONE,'@',DR_SMSID) as sms_msg FROM TDS_DRIVER WHERE DR_SNO LIKE ?");
			m_pst = m_conn.prepareStatement("SELECT DR_SMSID as sms_msg FROM TDS_DRIVER WHERE DR_SNO LIKE ?");
			m_pst.setString(1, p_driverId);
			cat.info(m_pst.toString());

			//System.out.println(m_pst);
			m_rs = m_pst.executeQuery();
			if (m_rs.first()) {
				result= m_rs.getString("sms_msg");
			} 
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getDriverPhoneNumberForQueue-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getDriverPhoneNumberForQueue-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static String checkORByQueueID(String p_queueId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String m_tripId = "";
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getOpenRequestByQueueID);
			//FROM TDS_OPENREQUEST,TDS_QUEUE_DETAILS WHERE OR_QUEUENO = ? AND OR_TRIP_STATUS != 'P' AND OR_QUEUENO = QD_QUEUENAME AND TIMESTAMPDIFF(MINUTE,SYSDATE(),TIMESTAMP(CONCAT(CONCAT(OR_SERVICEDATE,OR_SERVICETIME),'00'))) > QD_DELAYTIME ORDER BY OR_TRIP_ID LIMIT 1
			//FROM TDS_QUEUE Q, TDS_QUEUE_DETAILS QC , TDS_OPENREQUEST OPR  WHERE  Q.QU_NAME LIKE ? AND Q.QU_NAME =  QC.QD_QUEUENAME AND OPR.OR_TRIP_STATUS != 'P' AND OR_QUEUENO = QD_QUEUENAME AND TIMESTAMPDIFF(MINUTE,SYSDATE(),TIMESTAMP(CONCAT(CONCAT(OR_SERVICEDATE,OR_SERVICETIME),'00'))) > QD_DELAYTIME ORDER BY TIMESTAMP(CONCAT(CONCAT(OR_SERVICEDATE,OR_SERVICETIME),'00')) LIMIT 1
			m_pst.setString(1, p_queueId);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				m_tripId = m_rs.getString("OR_TRIP_ID");
			}
			m_pst.close();
			m_conn.close();
		} catch(SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkORByQueueID-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkORByQueueID-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_tripId;
	}


	/**
	 * @param p_driverId
	 * @author vimal
	 * @return ArrayList which contains set of OpenRequest which is based on QueueId
	 * @see method
	 * This method is use to get the OpenRequest of a particular Area(QueueID)
	 */
	public static ArrayList checkOROFDriver(String p_driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList m_openRequestList = new ArrayList();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.driverQueueID);
			//FROM TDS_QUEUE Q, TDS_QUEUE_DETAILS QC ,TDS_OPENREQUEST OPR WHERE  Q.QU_DRIVERID LIKE ? AND Q.QU_NAME = QC.QD_QUEUENAME AND OPR.OR_TRIP_STATUS != 'P' AND OR_QUEUENO = QD_QUEUENAME LIMIT 1
			m_pst.setString(1, p_driverId);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(m_rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(m_rs.getString("QU_NAME"));
				openRequestBO.setSadd1(m_rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(m_rs.getString("OR_STADD2"));
				openRequestBO.setScity(m_rs.getString("OR_STCITY"));
				openRequestBO.setSstate(m_rs.getString("OR_STSTATE"));
				openRequestBO.setEadd1(m_rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(m_rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(m_rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(m_rs.getString("OR_EDSTATE"));
				openRequestBO.setPhone(m_rs.getString("OR_PHONE"));
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(m_rs.getString("OR_SERVICETIME")));
				m_openRequestList.add(openRequestBO);
			}
			m_pst.close();
			m_conn.close();
		} catch(SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkOROFDriver-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkOROFDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_openRequestList;
	}

	public static ArrayList checkOROFDriverWTime(String p_driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList m_openRequestList = new ArrayList();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.driverQueueIDWithTimeConstraint);
			//FROM TDS_QUEUE Q, TDS_QUEUE_DETAILS QC ,  TDS_OPENREQUEST OPR WHERE  Q.QU_DRIVERID LIKE ? AND Q.QU_NAME = QC.QD_QUEUENAME AND OPR.OR_TRIP_STATUS != 'P' AND OR_QUEUENO = QD_QUEUENAME AND TIMESTAMPDIFF(MINUTE,SYSDATE(),TIMESTAMP(CONCAT(CONCAT(OR_SERVICEDATE,OR_SERVICETIME),'00'))) > QD_DELAYTIME ORDER BY TIMESTAMP(CONCAT(CONCAT(OR_SERVICEDATE,OR_SERVICETIME),'00')) LIMIT 1
			m_pst.setString(1, p_driverId);
			//System.out.println("SQL Query in "+m_pst);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(m_rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(m_rs.getString("QU_NAME"));
				openRequestBO.setSadd1(m_rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(m_rs.getString("OR_STADD2"));
				openRequestBO.setScity(m_rs.getString("OR_STCITY"));
				openRequestBO.setSstate(m_rs.getString("OR_STSTATE"));
				openRequestBO.setEadd1(m_rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(m_rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(m_rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(m_rs.getString("OR_EDSTATE"));
				openRequestBO.setPhone(m_rs.getString("OR_PHONE"));
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(m_rs.getString("OR_SERVICETIME")));
				m_openRequestList.add(openRequestBO);
			}
			m_pst.close();
			m_conn.close();
		} catch(SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkOROFDriverWTime-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkOROFDriverWTime-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_openRequestList;
	}

	/**
	 * @param p_tripid
	 * @param p_tripStatus P for public request, D for Driver request.
	 * @return true -updated false - not updated
	 * @author vimal
	 * @see Description
	 * This method is changed the Trip status of a OpenRequest. The Trip Status which contains whether 
	 * the Openrequest is either public Request or Driver Request.
	 * 
	 */
	public static int updateOpenRequestStatus(String assoCode,String p_tripid, String p_tripStatus, String p_driverId, String historyOfDrivers, String vehicleNO, String historyOfVehicles) {

		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int returnCode = 0;
		ResultSet rs = null;
		String routeNo ="";
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		//cat.info("ServiceRequestDAO.updateOpenRequestStatus Route-->"+routeNo);
		try {
			m_pst = m_conn.prepareStatement("SELECT OR_ROUTE_NUMBER FROM TDS_OPENREQUEST WHERE OR_ASSOCCODE='"+assoCode+"' AND OR_TRIP_ID='"+p_tripid+"'");
			rs=m_pst.executeQuery();
			if(rs.next()){
				routeNo = rs.getString("OR_ROUTE_NUMBER");
				if(routeNo!=null && !routeNo.equals("") && !routeNo.equals("0")){
					m_pst = m_conn.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? , OR_DRIVERID = ?, OR_VEHICLE_NO = ?, OR_DISPATCH_HISTORY_DRIVERS = ?, OR_SMS_SENT = now(), OR_DISPATCH_ITERATION_NO = OR_DISPATCH_ITERATION_NO + 1  WHERE  OR_ROUTE_NUMBER =? AND OR_ASSOCCODE = ?");
					m_pst.setString(1, p_tripStatus);
					m_pst.setString(2, p_driverId);
					m_pst.setString(3, vehicleNO);
					m_pst.setString(4, historyOfDrivers);
					m_pst.setString(5, routeNo);
					m_pst.setString(6, assoCode);
					//System.out.println("Changing job status" +m_pst.toString());
					cat.info(m_pst.toString());
					returnCode = m_pst.executeUpdate();
					m_pst =  m_conn.prepareStatement("UPDATE TDS_SHARED_RIDE SET SR_STATUS = ? , SR_DRIVERID = ?, SR_VEHICLE_NO = ? WHERE  SR_ROUTE_ID =? AND SR_ASSOCCODE =?");
					m_pst.setString(1, p_tripStatus);
					m_pst.setString(2, p_driverId);
					m_pst.setString(3, vehicleNO);
					m_pst.setString(4, routeNo);
					m_pst.setString(5, assoCode);
					m_pst.executeUpdate();
					if(returnCode > 0) {
						m_result = true;
						//System.out.println("Successfully updated");
					}
				}else{
					m_pst = m_conn.prepareStatement(TDSSQLConstants.updateOpenRequestTripStatus);
					//UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? , OR_DRIVERID = ? WHERE OR_TRIP_ID LIKE ?
					m_pst.setString(1, p_tripStatus);
					m_pst.setString(2, p_driverId);
					m_pst.setString(3, vehicleNO);
					m_pst.setString(4, historyOfDrivers);
					m_pst.setString(5, p_tripid);
					//System.out.println("Changing job status" +m_pst.toString());
					cat.info(m_pst.toString());
					returnCode = m_pst.executeUpdate();
					//m_pst.setString(4, p_driverId);
					if(returnCode > 0) {
						m_result = true;
						//System.out.println("Successfully updated");
					}
				}
			}

			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateOpenRequestStatus-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateOpenRequestStatus-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnCode;
	}
	public static int updateOpenRequestStatusAndDrivers(String p_tripid, String p_tripStatus, String p_driverId, String vehicleNO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int returnCode = 0;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateOpenRequestTripStatusAndDrivers);
			//UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? , OR_DRIVERID = ? WHERE OR_TRIP_ID LIKE ?
			m_pst.setString(1, p_tripStatus);
			m_pst.setString(2, p_driverId);
			m_pst.setString(3, vehicleNO);
			m_pst.setString(4, p_tripid);
			//System.out.println("Chaning job status" +m_pst.toString());
			cat.info(m_pst.toString());
			returnCode = m_pst.executeUpdate();
			//m_pst.setString(4, p_driverId);
			if(returnCode > 0) {
				m_result = true;
				//System.out.println("Successfully updated");
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateOpenRequestStatus-->"+sqex.getMessage());
			//System.out.println("TDSException ServiceRequestDAO.updateOpenRequestStatus-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnCode;
	}

	public static int updateOpenRequestStatusAlone(String p_tripid, String p_tripStatus) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int returnCode = 0;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateOpenRequestTripStatusAlone);
			//UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? , OR_DRIVERID = ? WHERE OR_TRIP_ID LIKE ?
			m_pst.setString(1, p_tripStatus);
			m_pst.setString(2, p_tripid);
			//System.out.println("Changing job status alone" +m_pst.toString());
			cat.info(m_pst.toString());
			returnCode = m_pst.executeUpdate();
			//m_pst.setString(4, p_driverId);
			if(returnCode > 0) {
				m_result = true;
				//System.out.println("Successfully updated");
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateOpenRequestStatusAlone-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateOpenRequestStatusAlone-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnCode;
	}

	public static int updateOpenRequestHisStatusAlone(String p_tripid, String p_tripStatus) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int returnCode = 0;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateOpenRequestTripHistoryStatusAlone);
			m_pst.setString(1, p_tripStatus);
			m_pst.setString(2, p_tripid);
			//System.out.println("Changing job status alone" +m_pst.toString());
			cat.info(m_pst.toString());
			returnCode = m_pst.executeUpdate();
			//m_pst.setString(4, p_driverId);
			if(returnCode > 0) {
				m_result = true;
				//System.out.println("Successfully updated");
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateOpenRequestStatusAlone-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateOpenRequestStatusAlone-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnCode;
	}

	public static int updateOpenRequestZone(String p_tripid, String zone) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int returnCode = 0;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateOpenRequestTripZone);
			//UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? , OR_DRIVERID = ? WHERE OR_TRIP_ID LIKE ?
			m_pst.setString(1, zone);
			m_pst.setString(2, p_tripid);
			cat.info(m_pst.toString());
			returnCode = m_pst.executeUpdate();
			//m_pst.setString(4, p_driverId);
			if(returnCode > 0) {
				m_result = true;
				//System.out.println("Successfully updated");
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateOpenRequestStatus-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateOpenRequestStatus-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnCode;
	}

	/**
	 * @return List of Tripid's
	 * @author vimal 
	 * @see Description 
	 * This method is used to get only the public open Request data. It contains a Map that contains another Map.
	 * The outermost Map contain the QueueID where the inner Map contain Tripid and Service Time of a Open Request.
	 * Example A Tripid is CC102 and the Start Time is 10:50, then QueueId is C101
	 * Map queueMap = new HashMap();
	 * Map tripMap = new HashMap();
	 * tripMap.put('CC102','10:50');
	 * testMap.put ('C101',tripMap);
	 * 
	 * If  p_status is Thread Request means it will return only the Unique id
	 * If p_status is Public Request means it will return all the Open Request
	 * @param p_status 0 -- public Request, 1 -- Thread Request
	 */
	public static List getPublicTripId (int p_status) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		Map m_tripIdMap = null;
		List m_queueIdList =  new ArrayList();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			if(p_status == 0) { //This is for public id
				m_pst = m_conn.prepareStatement(TDSSQLConstants.getPublicOpenRequestTripid);
				//TDS_OPENREQUEST WHERE OR_TRIP_STATUS IS NULL OR OR_TRIP_STATUS = \"\"
			} else if(p_status == 1) { //This is for open request id 
				m_pst = m_conn.prepareStatement(TDSSQLConstants.getOpenRequestUniqueQueueID);
				//FROM TDS_OPENREQUEST WHERE OR_TRIP_STATUS != 'P' GROUP BY OR_QUEUENO ORDER BY TIMESTAMP(CONCAT(CONCAT(OR_SERVICEDATE,OR_SERVICETIME),'00'))
			}
			//System.out.println("Querey "+m_pst);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();

			while (m_rs.next()) {
				m_queueIdList.add(m_rs.getString("OR_QUEUENO").toString());
				if(p_status == 0) { //Condition for checking the thread request or not
					m_tripIdMap = new HashMap();
					m_tripIdMap.put("tripId",m_rs.getString("OR_TRIP_ID").toString());
					m_tripIdMap.put("startTime",TDSValidation.getUserTimeFormat(m_rs.getString("OR_SERVICETIME").toString()));
					m_queueIdList.add(m_tripIdMap);
				}

			}
			//System.out.println("Data in List "+m_queueIdList);
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getPublicTripId-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getPublicTripId-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_queueIdList;
	}

	public static List getQueueIdAsThread() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_queueList = null;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getQueueIDAsThread);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_queueList = new ArrayList();
				do {
					m_queueList.add(m_rs.getString("QD_QUEUENAME"));
				} while (m_rs.next());
			}

		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getQueueIdAsThread-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getQueueIdAsThread-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_queueList;
	}


	/*public static OpenRequestBO getOpenRequestTripDetail(String p_driverId) {
		OpenRequestBO m_openRequestDetail = null;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("");
		}  catch (SQLException sqex) {
			//cat.info("Error in getOpenRequestTripDetail method "+sqex.getMessage());
		} finally {
			m_tdsConn.closeConnection();
		}
		return m_openRequestDetail;
	}*/

	/**
	 * @return
	 * 
	 */
	public static List getOpenRequestOnTimeConstraints(String p_queueId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_tripList = null;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getOpenRequuestTimeConstraints);
			//FROM TDS_QUEUE Q, TDS_QUEUE_DETAILS QC , TDS_OPENREQUEST OPR  WHERE  Q.QU_NAME LIKE ? AND Q.QU_NAME =  QC.QD_QUEUENAME AND OPR.OR_TRIP_STATUS != 'P' AND OR_QUEUENO = QD_QUEUENAME AND TIMESTAMPDIFF(MINUTE,SYSDATE(),TIMESTAMP(CONCAT(CONCAT(OR_SERVICEDATE,OR_SERVICETIME),'00'))) > QD_DELAYTIME ORDER BY TIMESTAMP(CONCAT(CONCAT(OR_SERVICEDATE,OR_SERVICETIME),'00')) LIMIT 1
			m_pst.setString(1, p_queueId);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			//System.out.println("getOpenRequestOnTimeConstraints query "+m_pst);
			m_tripList = new ArrayList();
			while (m_rs.next()) {
				m_tripList.add(m_rs.getString("OR_TRIP_ID").toString());
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getOpenRequestOnTimeConstraints-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getOpenRequestOnTimeConstraints-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_tripList;
	}

	/**
	 * @return List of Driverid from the Queue Table
	 * @see Description 
	 * This method is used to get all the driver from Queue Table.
	 */
	public static List getAllDriverFromQueue() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_driverQueueList = null;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getDriverFromQueue);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			m_driverQueueList = new ArrayList();
			while(m_rs.next()) {
				m_driverQueueList.add(m_rs.getString("QU_DRIVERID"));
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getAllDriverFromQueue-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getAllDriverFromQueue-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_driverQueueList;
	}

	/**
	 * @param p_queueId
	 * @param p_driverId
	 * @return
	 * @author vimal
	 * @see Description
	 * This method is used to get the driver id  and position of the driver from Queue Table but we have one condition
	 * Whether you give the queue id and driver id then it will return only particular driver position otherwise
	 * you give queue id and driver id as empty or blank space then it will return all the drivers belongs to this queue id.
	 * 
	 */
	public static List getQueueList (String p_queueId, int p_position) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_queueList = null;
		Map m_queueMap =  null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			if(p_position != 0) {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.checkQueueIdPresent + "  AND QU_QUEUEPOSITION > "+p_position);
			} else {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.checkQueueIdPresent);
			}
			m_pst.setString(1, p_queueId);
			cat.info("In getQueueList Method "+m_pst);
			//cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_queueList = new ArrayList();
				do {
					m_queueMap = new HashMap();
					m_queueMap.put("driverid", m_rs.getString("QU_DRIVERID"));
					m_queueMap.put("position", m_rs.getString("QU_QUEUEPOSITION"));
					m_queueList.add(m_queueMap);
				} while(m_rs.next());
			}
			m_rs.close();
			m_pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getQueueList-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getQueueList-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_queueList;
	}

	public static int getDriverPosition(String p_driverId, String p_queueId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int m_position = 0;
		TDSConnection m_dbcon = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_dbcon = new TDSConnection();
			m_con = m_dbcon.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.checkQueueIdPresent +"  AND QU_DRIVERID = \'" + p_driverId + "\'");
			m_pst.setString(1, p_queueId);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_position = m_rs.getInt("QU_QUEUEPOSITION");
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getDriverPosition-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getDriverPosition-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_position;
	}

	public static boolean updateQueueDetail(List p_queueList, String p_queueId,ApplicationPoolBO poolBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_status = false;
		TDSConnection m_dbConn =  null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		SendingSMS m_sendingSMS = null;
		int m_queuePosition = 0;
		Map m_queueMap = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			cat.info("QueueList data "+p_queueList);
			if(p_queueList != null) {
				for (Iterator iterator = p_queueList.iterator(); iterator
				.hasNext();) {
					m_queueMap = (HashMap) iterator.next();
					m_pst = m_conn
					.prepareStatement(TDSSQLConstants.updateQueuePosition);
					m_queuePosition = Integer.parseInt(m_queueMap.get("position")
							.toString()) - 1;
					m_pst.setInt(1, m_queuePosition);
					m_pst.setString(2, m_queueMap.get("driverid").toString());
					m_pst.setString(3, p_queueId);
					if (m_pst.executeUpdate() > 0) {
						m_status = true;
					} else {
						m_status = false;
						break;
					}
					if(m_status) {
						m_pst = m_conn.prepareStatement(TDSSQLConstants.getPhoneNo);
						m_pst.setString(1, m_queueMap.get("driverid").toString());
						m_rs = m_pst.executeQuery();
						if(m_rs.next()) {
							m_sendingSMS = new SendingSMS(poolBO.getSMTP_HOST_NAME(),poolBO.getSMTP_AUTH_USER(),poolBO.getSMTP_AUTH_PWD(),poolBO.getEmailFromAddress(),poolBO.getSMTP_PORT(),poolBO.getProtocol());
							try {
								m_sendingSMS.sendMail(m_rs.getString("ID"), "Your Queue position of "+m_queueMap.get("driverid").toString()+" is "+m_queuePosition );
							} catch (MessagingException mex) {
								//System.out.println("Error in Sending SMS "+mex.getMessage());
							}
						}
					}
					// m_pst.clearParameters();
				}
			}
			if(m_status) {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.updateSequence);
				m_pst.setString(1, p_queueId);
				m_pst.executeUpdate();
			} else  {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.deleteSequence);
				m_pst.setString(1, p_queueId);
				m_pst.executeUpdate();
			}


			m_pst.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateQueueDetail-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateQueueDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_sendingSMS = null;
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_status;
	}


	public static int deleteDriverFromQueue(String driverid, String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs= null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement(TDSSQLConstants.deleteDriverFromQueue);//DELETE FROM TDS_QUEUE WHERE QU_DRIVERID= ? AND QU_NAME = ? 
			pst.setString(1, driverid);
			pst.setString(2, associationCode);
			cat.info(pst.toString());
			result = pst.executeUpdate();
			pst = con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_SWITCH = 'Y',DL_LASTUPDATE = NOW() WHERE DL_DRIVERID ='"+driverid+"' AND DL_ASSOCCODE ='"+associationCode+"' AND DL_SWITCH='Z'");
			pst.execute();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.deleteDriverFromQueue-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.deleteDriverFromQueue-->"+sqex.getMessage());
			sqex.printStackTrace();		
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int deleteDriverFromQueueWithoutQueueIDDelete(String driverid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			/*pst = con.prepareStatement(TDSSQLConstants.DeleteQueuePosition);//delete from TDS_DRIVERLOCATION where DL_DRIVERID=?
			pst.setString(1, driverid);
			pst.execute();
			pst.close();*/

			pst = con.prepareStatement(TDSSQLConstants.deleteDriverFromQueueWithoutQueueID );//DELETE FROM TDS_QUEUE WHERE QU_DRIVERID= ? AND QU_NAME = ? 
			pst.setString(1, driverid);
			cat.info(pst.toString());

			result = pst.executeUpdate();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.deleteDriverFromQueue-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.deleteDriverFromQueue-->"+sqex.getMessage());
			sqex.printStackTrace();		
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	//For Voucher
	public static int changeVoucherStatus(Connection con,String Vno,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result =0;
		PreparedStatement pst = null;
		try{
			pst = con.prepareStatement(TDSSQLConstants.updateVoucherStatus);
			pst.setString(1, Vno);
			pst.setString(2,adminBO.getAssociateCode());
			cat.info(pst.toString());
			result = pst.executeUpdate();
		}
		catch(SQLException sqex){
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.changeVoucherStatus-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.changeVoucherStatus-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int checkOpenRequest(String tripid) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In checkOpenRequest Method");
		//System.out.println("In checkOpenRequest Method");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int result = 0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(tripid != "" || tripid.length() != 0 ){
				pst = con.prepareStatement(TDSSQLConstants.getTripAvailability);
				//FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = ? AND OR_SERVICESTATUS = 0
				pst.setString(1, tripid);
			} 
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.next()) {
				result = 1;
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkOpenRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}



	/**
	 * @param p_queueId
	 * @author vimal
	 * @see Method
	 * This method is used to check, whether the given queueId is present in the Queue table or not.
	 * @return 0 - record is not available and 1 - record is available 
	 */
	public static ArrayList<String> checkDriverCurrentlyLogggedInQueue(String driverID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<String> queueName = new ArrayList<String>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getDriverIdPresentOrNot);
			m_pst.setString(1, driverID);
			m_rs = m_pst.executeQuery();
			if (m_rs.next()) {
				queueName.add(m_rs.getString("QU_NAME"));
				queueName.add(m_rs.getString("QD_DESCRIPTION"));
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {

			cat.error("TDSException ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue-->"+sqex.getMessage());
			sqex.printStackTrace();	
		} finally {
			m_tdsConn.closeConnection();
		}
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!In Check QueueID "+queueName);
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return queueName;
	}

	/*public static String getQueueIdByDriverid(String p_driverId) {
		String m_queueId = "";
		TDSConnection m_dbConn = null;
		Connection m_con = null;

		return m_queueId;
	}*/

	public static boolean updateSignature(String p_dataByte, String p_driverid, String p_tripid) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.updateSignature);
			m_pst.setString(1, p_dataByte);
			m_pst.setString(2, p_tripid);
			m_pst.setString(3, p_driverid);
			cat.info(m_pst.toString());

			if (m_pst.executeUpdate() > 0) {
				m_result = true;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateSignature-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateSignature-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!In updateSignature "+m_result);
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_result;
	}

	/**
	 * @param p_queueId
	 * @param p_driverId
	 * @return boolean true driver is avail and false driver is not avail.
	 * @see Description This method is used to check whether the driver is available in the queue or not. 
	 */


	public static boolean  checkQueueIsAvailableForDriver(String associationCode, String p_queueId, String driverID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_result = true;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("SELECT QD_QUEUENAME,QD_LOGIN_SWITCH FROM TDS_QUEUE_DETAILS WHERE QD_QUEUENAME ='"+p_queueId+"' AND QD_ASSOCCODE ='"+associationCode+"'");
			m_rs = m_pst.executeQuery();
			if (m_rs.next()) {
				String newQueue=m_rs.getString("QD_QUEUENAME");
				if(m_rs.getString("QD_LOGIN_SWITCH").equalsIgnoreCase("A")){
					m_pst=m_conn.prepareStatement("SELECT QUE_NAME FROM TDS_QUEUE_DRIVER_MAPPING WHERE QUE_NAME = '"+newQueue+"' AND DRIVER_ID = '"+driverID+"' AND LOGIN_FLAG = 'NA' AND ASS_CODE = '"+associationCode+"'");
					m_rs=m_pst.executeQuery();
					if(m_rs.next()){
						m_result=false;
					}
				} else if (m_rs.getString("QD_LOGIN_SWITCH").equalsIgnoreCase("R")){
					m_pst=m_conn.prepareStatement("SELECT QUE_NAME FROM TDS_QUEUE_DRIVER_MAPPING WHERE QUE_NAME = '"+newQueue+"' AND DRIVER_ID = '"+driverID+"' AND LOGIN_FLAG = 'R' AND ASS_CODE = '"+associationCode+"'");
					m_rs=m_pst.executeQuery();
					if(!m_rs.next()){
						m_result=false;
					}
				}
			} else {
				m_result=false;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.checkQueueIsAvailableForDriver-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceDAO.checkQueueIsAvailableForDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!In Check QueueID "+m_result);
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_result;
	}


	public static boolean checkDriverQueue(String p_queueId, String p_driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_availableStatus = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.checkDriverQueue);
			m_pst.setString(1, p_queueId);
			m_pst.setString(2, p_driverId);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			if(m_rs.next()) {
				m_availableStatus = true;
			}
			//System.out.println("Query in checkDriverQueue "+m_pst);
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.checkDriverQueue-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.checkDriverQueue-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!In updateSignature "+m_availableStatus);
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_availableStatus; 
	}

	public static String noOfDriverinQueueID(String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String m_driverCount = "";
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			//m_pst = m_conn.prepareStatement(TDSSQLConstants.getNoOfDriverbasedonQueueID);
			m_pst = m_conn.prepareStatement("select QU_NAME,QU_ASSOCCODE,QD_DESCRIPTION,count(*) as coun from TDS_QUEUE c,TDS_QUEUE_DETAILS qc where c. QU_ASSOCCODE = '"+assoccode+"' and c.QU_NAME=qc.QD_QUEUENAME group by QU_ASSOCCODE");
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				if(m_rs.isLast())
					m_driverCount = m_driverCount+m_rs.getString("QU_NAME")+"^"+m_rs.getString("QD_DESCRIPTION")+"^"+m_rs.getString("coun");
				else
					m_driverCount = m_driverCount+m_rs.getString("QU_NAME")+"^"+m_rs.getString("QD_DESCRIPTION")+"^"+m_rs.getString("coun")+";";
			}

			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RegistrationDAO.noOfDriverinQueueID-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.noOfDriverinQueueID-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_driverCount;
	}


	public static List getAirPortCode(String p_codeValue) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_airPortCodeList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.getAirPortCode);
			m_pst.setString(1, p_codeValue+"%");
			//System.out.println("Query in getAirPortCode "+m_pst);
			m_rs = m_pst.executeQuery();
			if(m_rs.next()) {
				m_airPortCodeList = new ArrayList();
				do {
					m_airPortCodeList.add(m_rs.getString("AR_AIRCODE"));
					m_airPortCodeList.add(m_rs.getString("AR_PORTDESC"));
				} while(m_rs.next() );
			}
			//System.out.println("Data in DAO "+m_airPortCodeList);
		} catch (SQLException sqex ) {
			cat.error("TDSException RegistrationDAO.getAirPortCode->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getAirPortCode-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_airPortCodeList;
	}

	public static List getVCompany(String vcostcenter,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List al_cmp = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement("select * from TDS_GOVT_VOUCHER where VC_ASSOCCODE='"+assoccode+"' and VC_COSTCENTER like ?");
			m_pst.setString(1, vcostcenter+"%");
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			if(m_rs.next()) {
				al_cmp = new ArrayList();
				do {
					al_cmp.add(m_rs.getString("VC_COSTCENTER"));
				} while(m_rs.next() );
			}
			//System.out.println("Data in DAO "+al_cmp);
		} catch (SQLException sqex ) {
			cat.error("TDSException RegistrationDAO.getVCompany->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getVCompany-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_cmp;
	}

	public static List getDriverName(String p_driverId,String asscode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_airPortCodeList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.getDriverName);
			m_pst.setString(1, p_driverId+"%");
			m_pst.setString(2, asscode);
			cat.info(m_pst.toString());

			//System.out.println("Query in getAirPortCode "+m_pst);
			m_rs = m_pst.executeQuery();
			if(m_rs.next()) {
				m_airPortCodeList = new ArrayList();
				do {
					m_airPortCodeList.add(m_rs.getString("DR_USERID"));
				} while(m_rs.next() );
			}
			//System.out.println("Data in DAO "+m_airPortCodeList);
		} catch (SQLException sqex ) {
			cat.error("TDSException RegistrationDAO.getDriverName->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getDriverName-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_airPortCodeList;
	}

	//Purpose : To get the SMS Response Time for the given Queue name
	//Parameter : Queue Name
	//Return Value : Sms Response Time(int) 

	public static long getSMSResponseTime(String p_queueName){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		long m_smsResponseTime = 0;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.getSMSResponseTime);
			m_pst.setString(1, p_queueName);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();

			if(m_rs.next()) {
				do {
					m_smsResponseTime = m_rs.getLong("QD_SMS_RES_TIME");
				} while(m_rs.next() );
			}

			//System.out.println("SMS RESPONSE TIME in DAO : "+ m_smsResponseTime);			

		} catch (SQLException p_sqex ) {
			//System.out.println("Error in get Air port code "+p_sqex.getMessage());
			cat.error(m_pst.toString());
			p_sqex.getStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_smsResponseTime;
	}

	/**
	 * @param p_condition
	 * @param p_constraint
	 * @return Description
	 * This method is user to retrieve the user information based on the the constraints.
	 * Their constraints are defined as 
	 * 0 --  Address1
	 * 1 --  Address2
	 * 2 --  City
	 * 3 --  State
	 * 4 --  Zip Code
	 * 
	 */
	public static List getUserInformation(String p_condition,String p_phone, int p_constraint) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_conditionList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			if(p_constraint == 0) {
				m_pst = m_con.prepareStatement(TDSSQLConstants.getAddress1List);
			} else if(p_constraint == 1) {
				m_pst = m_con.prepareStatement(TDSSQLConstants.getAddress2List);
			} else if(p_constraint == 2) {
				m_pst = m_con.prepareStatement(TDSSQLConstants.getCityList);
			} else if(p_constraint == 3) {
				m_pst = m_con.prepareStatement(TDSSQLConstants.getStateList);
			} else if(p_constraint == 4) {
				m_pst = m_con.prepareStatement(TDSSQLConstants.getZipList);
			}
			m_pst.setString(1, p_phone);
			m_pst.setString(2, p_condition+"%");
			//cat.info("Query in retrieve User information "+m_pst);
			cat.info("Query in retrieve User information "+m_pst);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_conditionList = new ArrayList();
				do {
					m_conditionList.add(m_rs.getString("CU_DATA"));
				} while (m_rs.next());
			}
		} catch (SQLException p_sqex ) {
			//System.out.println("Error in getUserInformation "+p_sqex.getMessage());
			cat.error(m_pst.toString());
			//cat.info("Error in getUserInformation "+p_sqex.getMessage());
			p_sqex.getStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_conditionList;
	}

	public static ArrayList getAssoccodeForAutomatic()
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList m_conditionList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_conditionList = new ArrayList();
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement("select CD_ASSOCCODE from TDS_COMPANYDETAIL where CD_DISPATCH_SWITH = 'a' ");
			//System.out.println(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				m_conditionList.add(m_rs.getString("CD_ASSOCCODE"));
			}



		} catch (SQLException p_sqex ) {
			cat.error(m_pst.toString());
			p_sqex.getStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_conditionList;
	}

	public static ArrayList getAssoccodeForQueue()
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList m_conditionList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_conditionList = new ArrayList();
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement("select CD_ASSOCCODE, CD_DISPATCH_SWITH,CD_MASTER_ASSOCCODE from TDS_COMPANYDETAIL");
			//System.out.println(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				m_conditionList.add(m_rs.getString("CD_ASSOCCODE"));
				m_conditionList.add(m_rs.getString("CD_DISPATCH_SWITH"));
				m_conditionList.add(m_rs.getString("CD_MASTER_ASSOCCODE"));
			}
		} catch (SQLException p_sqex ) {
			p_sqex.getStackTrace();
			cat.error(m_pst.toString());
		} finally {
			m_tdsConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_conditionList;
	}
	public static String getDispatchType(String associationCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList m_conditionList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String returnAssociationCode = "";
		try {
			m_conditionList = new ArrayList();
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement("select CD_ASSOCCODE, CD_DISPATCH_SWITH from TDS_COMPANYDETAIL WHERE CD_ASSOCCODE=?");
			m_pst.setString(1, associationCode);
			//System.out.println(m_pst.toString());
			m_rs = m_pst.executeQuery();
			if(m_rs.next())
			{
				returnAssociationCode = m_rs.getString("CD_DISPATCH_SWITH");
			}



		} catch (SQLException p_sqex ) {
			p_sqex.getStackTrace();
			cat.error(m_pst.toString());
		} finally {
			m_tdsConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnAssociationCode;
	}

	public static List getQueueForAssoccode1(String Assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String trace = "j-" +System.currentTimeMillis();
		List m_conditionList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_conditionList = new ArrayList();
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			//System.out.println(trace);
			m_pst = m_con.prepareStatement("SELECT QD_QUEUENAME FROM TDS_QUEUE_DETAILS,TDS_COMPANYDETAIL where CD_ASSOCCODE= QD_ASSOCCODE and QD_ASSOCCODE = ? and CD_DISPATCH_SWITH = 'Q'");
			m_pst.setString(1, Assocode);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				m_conditionList.add(m_rs.getString("QD_QUEUENAME"));
			}

		} catch (SQLException p_sqex ) {
			//System.out.println("Error in getUserInformation "+p_sqex.getMessage());
			cat.error(m_pst.toString());
			//cat.info("Error in getUserInformation "+p_sqex.getMessage());
			p_sqex.getStackTrace();
		} finally {
			m_tdsConn.closeConnection();
			//System.out.println(trace);
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_conditionList;
	}

	public static OpenRequestBO getOpenRequestOnTime(String p_queueId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		OpenRequestBO requestBO = null;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			requestBO = new OpenRequestBO();
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("SELECT OR_TRIP_ID,OR_QUEUENO,date_format(OR_SERVICEDATE,'%r') as st_time,case time_to_sec(timediff(now(),OR_SMS_SENT)) >= QD_SMS_RES_TIME is null when true then 0 else case (time_to_sec(timediff(now(),OR_SMS_SENT)) >= QD_SMS_RES_TIME) when true then 1 else 2 end end as flg,case OR_TRIP_STATUS when 'P' then 1 else 2 end as ord FROM TDS_QUEUE Q, TDS_QUEUE_DETAILS QC , TDS_OPENREQUEST OPR  WHERE  Q.QU_NAME LIKE ? AND Q.QU_NAME =  QC.QD_QUEUENAME AND (OPR.OR_TRIP_STATUS = 'P' OR OPR.OR_TRIP_STATUS= '')  AND OR_QUEUENO = QD_QUEUENAME AND TIMESTAMPDIFF(MINUTE,SYSDATE(),OR_SERVICEDATE) <= QD_DELAYTIME ORDER BY OR_SERVICEDATE,ord LIMIT 1");
			m_pst.setString(1, p_queueId);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			//System.out.println("query1 "+m_pst);

			if (m_rs.first() && (m_rs.getString("flg").equals("0"))) {
				requestBO.setTripid(m_rs.getString("OR_TRIP_ID"));
				requestBO.setChckTripStatus(1);
				requestBO.setSttime(m_rs.getString("st_time"));
				requestBO.setQueueno(m_rs.getString("OR_QUEUENO"));

			} else if(m_rs.first() && (m_rs.getString("flg").equals("1"))){
				requestBO.setTripid(m_rs.getString("OR_TRIP_ID"));
				requestBO.setChckTripStatus(2);
				requestBO.setSttime(m_rs.getString("st_time"));
				requestBO.setQueueno(m_rs.getString("OR_QUEUENO"));
			} else {
				requestBO.setTripid("");
				requestBO.setChckTripStatus(0);
				requestBO.setSttime("00:00:00");
				requestBO.setQueueno("");
			}

		} catch (SQLException sqex) {
			//System.out.println("Error in getOpenRequestOnTimeConstraints method "+sqex.getMessage());
			cat.error(m_pst.toString());
			//cat.info("Error in getOpenRequestOnTimeConstraints method "+sqex.getMessage());
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return requestBO;
	}


	public static OpenRequestBO checkOROFDriverWTimeQueue(String quid) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		OpenRequestBO openRequestBO = null;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			//m_pst = m_conn.prepareStatement(TDSSQLConstants.driverQueueIDWithTimeConstraint);
			m_pst = m_conn.prepareStatement("SELECT OPR.* FROM TDS_QUEUE Q, TDS_QUEUE_DETAILS QC , TDS_OPENREQUEST OPR  WHERE  Q.QU_NAME LIKE ? AND Q.QU_NAME =  QC.QD_QUEUENAME AND OPR.OR_TRIP_STATUS = 'P'  AND OR_QUEUENO = QD_QUEUENAME AND TIMESTAMPDIFF(MINUTE,SYSDATE(),OR_SERVICEDATE) <= QD_DELAYTIME ORDER BY OR_SERVICEDATE LIMIT 1");
			m_pst.setString(1, quid);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(m_rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(m_rs.getString("QU_NAME"));
				openRequestBO.setSadd1(m_rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(m_rs.getString("OR_STADD2"));
				openRequestBO.setScity(m_rs.getString("OR_STCITY"));
				openRequestBO.setSstate(m_rs.getString("OR_STSTATE"));
				openRequestBO.setEadd1(m_rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(m_rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(m_rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(m_rs.getString("OR_EDSTATE"));
				openRequestBO.setPhone(m_rs.getString("OR_PHONE"));
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(m_rs.getString("OR_SERVICETIME")));

			}
			m_pst.close();
			m_conn.close();
		} catch(SQLException sqex) {
			cat.error(m_pst.toString());
			//cat.info("Error in Driver Queue "+sqex.getMessage());
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return openRequestBO;
	}
	public static int checkOpenRequestForQueue(String qid) {
		//cat.info("In checkOpenRequest Method");
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result=0;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("SELECT OR_TRIP_ID FROM TDS_QUEUE_DETAILS QC , TDS_OPENREQUEST OPR  WHERE  OR_QUEUENO LIKE ? AND OR_QUEUENO =  QC.QD_QUEUENAME AND (OPR.OR_TRIP_STATUS = 'P' OR OPR.OR_TRIP_STATUS= '') AND TIMESTAMPDIFF(MINUTE,SYSDATE(),OR_SERVICEDATE) < QD_DELAYTIME ORDER BY OR_SERVICEDATE");
			m_pst.setString(1, qid);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			//System.out.println("getOpenRequestOnTimeConstraints query "+m_pst);

			if (m_rs.first()) {
				//m_tripList.add(m_rs.getString("OR_TRIP_ID").toString());
				result= 1;
			} else {
				result= 0;
			}

		} catch (SQLException sqex) {
			//System.out.println("Error in getOpenRequestOnTimeConstraints method "+sqex.getMessage());
			cat.error(m_pst.toString());
			//cat.info("Error in getOpenRequestOnTimeConstraints method "+sqex.getMessage());
			result= 0;

		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static void deleteDriver(String p_queueId,String driver_id,String trip) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String m_driverId = "";
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("delete from TDS_QUEUE where QU_DRIVERID = ? and QU_NAME = ?");
			m_pst.setString(1, driver_id);
			m_pst.setString(2, p_queueId);	
			cat.info(m_pst.toString());

			//System.out.println("Delete:" + m_pst.toString());
			m_pst.execute();
			m_pst.close();

			m_pst = m_conn.prepareStatement("update TDS_OPENREQUEST set OR_SMS_SENT = '1970-01-01 00:00:00',OR_DRIVERID='',OR_TRIP_STATUS=''  where OR_TRIP_ID = '"+trip+"'");
			//System.out.println("Update:" + m_pst.toString());
			m_pst.execute();

			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			//cat not defined
			//cat.info("TDSException ServiceRequestDAO.deleteDriver-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.deleteDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}



	public static ArrayList getORForPublicReq(String queueid) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList m_openRequestList = new ArrayList();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("SELECT OPR.*,TIME_FORMAT(OPR.OR_SERVICEDATE,'%h:%i %p') as time1 FROM  TDS_QUEUE_DETAILS QC , TDS_OPENREQUEST OPR  WHERE OR_QUEUENO LIKE ? AND OR_QUEUENO =  QC.QD_QUEUENAME AND (OPR.OR_TRIP_STATUS = 'P' OR OPR.OR_TRIP_STATUS= '') AND TIMESTAMPDIFF(MINUTE,SYSDATE(),OR_SERVICEDATE) <= QD_DELAYTIME ORDER BY OR_SERVICEDATE");

			//FROM TDS_QUEUE Q, TDS_QUEUE_DETAILS QC ,TDS_OPENREQUEST OPR WHERE  Q.QU_DRIVERID LIKE ? AND Q.QU_NAME = QC.QD_QUEUENAME AND OPR.OR_TRIP_STATUS != 'P' AND OR_QUEUENO = QD_QUEUENAME LIMIT 1
			m_pst.setString(1, queueid);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(m_rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(m_rs.getString("OR_QUEUENO"));
				openRequestBO.setSadd1(m_rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(m_rs.getString("OR_STADD2"));
				openRequestBO.setScity(m_rs.getString("OR_STCITY"));
				openRequestBO.setSstate(m_rs.getString("OR_STSTATE"));
				openRequestBO.setEadd1(m_rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(m_rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(m_rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(m_rs.getString("OR_EDSTATE"));
				openRequestBO.setPhone(m_rs.getString("OR_PHONE"));
				openRequestBO.setSttime(m_rs.getString("time1"));
				m_openRequestList.add(openRequestBO);
			}
			m_pst.close();
			m_conn.close();
		} catch(SQLException sqex) {
			sqex.printStackTrace();
			cat.error(m_pst.toString());
			//cat.info("Error in Driver Queue "+sqex.getMessage());
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_openRequestList;
	}
	public static OpenRequestBO getOpenRequestByTripID(String tripID,AdminRegistrationBO adminBo, ArrayList<String> tripStatus) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		OpenRequestBO requestBO = null;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,pst1=null;
		ResultSet m_rs = null,rs1=null;
		String query="";
		String filterQuery = "";
		if(tripStatus!=null && tripStatus.size()>0){
			filterQuery = "AND OR_TRIP_STATUS IN(";
			for(int i =0;i<tripStatus.size()-1;i++){
				filterQuery = filterQuery+"'"+tripStatus.get(i)+"',";
			}
			filterQuery = filterQuery+"'"+tripStatus.get(tripStatus.size()-1)+"') ";
		}
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("select OR_TRIP_SOURCE,OR_CREATED_BY,OR_ASSOCCODE,OR_TRIP_ID,OR_RATINGS,OR_AMT,OR_TRIP_STATUS,OR_QUEUENO,OR_END_QUEUENO,OR_DRCABFLAG,OR_SMS_SENT,OR_DRIVERID,OR_DISPATCH_HISTORY_DRIVERS,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_SPLINS,OR_PAYACC,OR_PAYTYPE,"+
					"OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_CALLER_PHONE,OR_CALLER_NAME,OR_EMAIL,OR_TRIP_STATUS,OR_DISPATCH_LEAD_TIME,OR_DONT_DISPATCH,OR_PREMIUM_CUSTOMER,OR_SHARED_RIDE,OR_ELANDMARK,OR_EDSTATE,OR_NAME,OR_EDZIP,OR_METER_TYPE,OR_REPEAT_GROUP,OR_PHONE,OR_NUMBER_OF_PASSENGERS,OR_DROP_TIME,OR_STLATITUDE,OR_COMMENTS,OR_STLONGITUDE,OR_EDLATITUDE,OR_DRIVERID,OR_VEHICLE_NO,OR_EDLONGITUDE,OR_LANDMARK,OR_DRCABFLAG,OR_QUEUENO," +	"DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%H%i') "+"as TIME, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"') AS OFFSET_DATETIME,OR_AIRLINE_NAME,OR_AIRLINE_NO,OR_AIRLINE_FROM,OR_AIRLINE_TO,VC_COMPANYNAME,OR_CUST_REF_NUM,OR_CUST_REF_NUM1,OR_CUST_REF_NUM2,OR_CUST_REF_NUM3 from TDS_OPENREQUEST LEFT JOIN TDS_GOVT_VOUCHER ON OR_MASTER_ASSOCCODE=VC_ASSOCCODE AND VC_VOUCHERNO=OR_PAYACC WHERE OR_MASTER_ASSOCCODE=? AND OR_TRIP_ID =? "+filterQuery);
			m_pst.setString(1, adminBo.getMasterAssociateCode());
			m_pst.setString(2, tripID);
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				requestBO = new OpenRequestBO();
				requestBO.setTripid(m_rs.getString("OR_TRIP_ID"));
				requestBO.setCallerPhone(m_rs.getString("OR_CALLER_PHONE"));
				requestBO.setCallerName(m_rs.getString("OR_CALLER_NAME"));
				requestBO.setPhone(m_rs.getString("OR_PHONE"));
				requestBO.setName(m_rs.getString("OR_NAME"));
				requestBO.setSdate(TDSValidation.getUserDateFormat(m_rs.getString("DATE")));
				requestBO.setShrs((m_rs.getString("TIME")));
				requestBO.setPaytype((m_rs.getString("OR_PAYTYPE")));
				requestBO.setAcct(m_rs.getString("OR_PAYACC"));
				requestBO.setAcctName(m_rs.getString("VC_COMPANYNAME"));
				requestBO.setSlandmark(m_rs.getString("OR_LANDMARK"));
				requestBO.setSpecialIns(m_rs.getString("OR_SPLINS"));
				requestBO.setComments(m_rs.getString("OR_COMMENTS"));
				requestBO.setVehicleNo(m_rs.getString("OR_VEHICLE_NO"));
				requestBO.setDriverid(m_rs.getString("OR_DRIVERID"));
				requestBO.setQueueno(m_rs.getString("OR_QUEUENO"));
				requestBO.setEndQueueno("OR_END_QUEUENO");
				requestBO.setDrProfile(m_rs.getString("OR_DRCABFLAG"));
				requestBO.setStartTimeStamp(m_rs.getString("OFFSET_DATETIME"));
				requestBO.setTripStatus(m_rs.getString("OR_TRIP_STATUS"));
				requestBO.setOR_SMS_SENT(m_rs.getString("OR_SMS_SENT"));
				requestBO.setHistoryOfDrivers(m_rs.getString("OR_DISPATCH_HISTORY_DRIVERS"));
				requestBO.setSadd1(m_rs.getString("OR_STADD1"));
				requestBO.setSadd2(m_rs.getString("OR_STADD2"));
				requestBO.setScity(m_rs.getString("OR_STCITY"));
				requestBO.setSstate(m_rs.getString("OR_STSTATE"));
				requestBO.setSzip(m_rs.getString("OR_STZIP"));
				requestBO.setSlat(m_rs.getString("OR_STLATITUDE"));
				requestBO.setSlong(m_rs.getString("OR_STLONGITUDE"));
				requestBO.setEadd1(m_rs.getString("OR_EDADD1"));
				requestBO.setEadd2(m_rs.getString("OR_EDADD2"));
				requestBO.setEcity(m_rs.getString("OR_EDCITY"));
				requestBO.setEstate(m_rs.getString("OR_EDSTATE"));
				requestBO.setEzip(m_rs.getString("OR_EDZIP"));
				requestBO.setEdlatitude(m_rs.getString("OR_EDLATITUDE"));
				requestBO.setEdlongitude(m_rs.getString("OR_EDLONGITUDE"));
				requestBO.setTypeOfRide(m_rs.getString("OR_SHARED_RIDE"));
				requestBO.setRepeatGroup(m_rs.getString("OR_REPEAT_GROUP"));
				requestBO.setAmt(new BigDecimal(m_rs.getString("OR_AMT")));
				requestBO.setElandmark(m_rs.getString("OR_ELANDMARK"));
				requestBO.setNumOfPassengers(m_rs.getInt("OR_NUMBER_OF_PASSENGERS"));
				requestBO.setDropTime(m_rs.getString("OR_DROP_TIME"));
				requestBO.setDontDispatch(m_rs.getInt("OR_DONT_DISPATCH"));
				requestBO.setPremiumCustomer(m_rs.getInt("OR_PREMIUM_CUSTOMER"));
				requestBO.setAdvanceTime(m_rs.getString("OR_DISPATCH_LEAD_TIME"));
				requestBO.setEmail(m_rs.getString("OR_EMAIL"));
				requestBO.setChckTripStatus(m_rs.getInt("OR_TRIP_STATUS"));
				requestBO.setTripStatus(m_rs.getString("OR_TRIP_STATUS"));
				requestBO.setJobRating(m_rs.getInt("OR_RATINGS"));
				requestBO.setAssociateCode(m_rs.getString("OR_ASSOCCODE"));
				requestBO.setPaymentMeter(m_rs.getInt("OR_METER_TYPE"));
				requestBO.setCreatedBy(m_rs.getString("OR_CREATED_BY"));
				requestBO.setTripSource(m_rs.getInt("OR_TRIP_SOURCE"));
				requestBO.setAirName(m_rs.getString("OR_AIRLINE_NAME"));
				requestBO.setAirNo(m_rs.getString("OR_AIRLINE_NO"));
				requestBO.setAirFrom(m_rs.getString("OR_AIRLINE_FROM"));
				requestBO.setAirTo(m_rs.getString("OR_AIRLINE_TO"));
				requestBO.setRefNumber(m_rs.getString("OR_CUST_REF_NUM"));
				requestBO.setRefNumber1(m_rs.getString("OR_CUST_REF_NUM1"));
				requestBO.setRefNumber2(m_rs.getString("OR_CUST_REF_NUM2"));
				requestBO.setRefNumber3(m_rs.getString("OR_CUST_REF_NUM3"));
				
				if(m_rs.getInt("OR_METER_TYPE")==0){
					query = " AND MD_DEFAULT=1";
				} else {
					query = " AND MD_KEY='"+m_rs.getInt("OR_METER_TYPE")+"'";
				}
				pst1=m_conn.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+adminBo.getMasterAssociateCode()+"'"+query);
				rs1=pst1.executeQuery();
				if(rs1.next()){
					requestBO.setRatePerMile(Double.parseDouble(rs1.getString("MD_RATE_PER_MILE")));
					requestBO.setRatePerMin(Double.parseDouble(rs1.getString("MD_RATE_PER_MIN")));
					requestBO.setStartAmt(Double.parseDouble(rs1.getString("MD_START_AMOUNT")));
					requestBO.setMinSpeed(Integer.parseInt(rs1.getString("MD_MINIMUM_SPEED")));
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceDAO.getOpenRequestByTripID-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceDAO.getOpenRequestByTripID" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return requestBO;
	}

	public static ArrayList getAccountList(String accountNumber,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_cmp = null;
		TDSConnection m_tdsConn =  new TDSConnection();
		Connection m_con =  m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_pst = m_con.prepareStatement("select * from TDS_GOVT_VOUCHER where VC_ASSOCCODE='"+assoccode+"' and (VC_VOUCHERNO like ? OR VC_COMPANYNAME like ?)");
			m_pst.setString(1, accountNumber+"%");
			m_pst.setString(2, "%"+accountNumber+"%");
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			al_cmp = new ArrayList();
			while(m_rs.next()) {
				al_cmp.add(m_rs.getString("VC_VOUCHERNO"));
				String compName=m_rs.getString("VC_COMPANYNAME").replace("&", "&#38;");
				String name=compName.replaceAll("'", "&#39;");
				al_cmp.add(name);
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException ServiceRequestDAO.getAccountList->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getAccountList-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_cmp;
	}


	public static ArrayList getLandMark(String landMarkFirst,String landmarkSecond,String masterAssociationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList m_airPortCodeList = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		TDSConnection m_tdsConn = new TDSConnection(); 
		Connection m_con = m_tdsConn.getConnection();
		if(!landmarkSecond.equals("")){
			landmarkSecond=landmarkSecond+"%";
		}
		try {
			m_pst = m_con.prepareStatement("SELECT LM_NAME,LM_KEY_NAME FROM TDS_LANDMARK WHERE (LM_NAME LIKE ? OR LM_KEY_NAME LIKE ?) AND LM_ASSCODE= '"+masterAssociationCode+"' LIMIT 20");
			m_pst.setString(1, "%"+landMarkFirst+"%"+landmarkSecond);
			m_pst.setString(2, "%"+landMarkFirst+"%"+landmarkSecond);
			m_rs = m_pst.executeQuery();
			m_airPortCodeList = new ArrayList();
			while(m_rs.next()) {
				String lmName=m_rs.getString("LM_NAME").replace("&", "&#38;");
				String name=lmName.replaceAll("'", "&#39;");
				m_airPortCodeList.add(name);
				String keyName=(m_rs.getString("LM_KEY_NAME")==null?"":m_rs.getString("LM_KEY_NAME")).replace("&", "&#38;");
				String kname=keyName.replaceAll("'", "&#39;");
				m_airPortCodeList.add(kname);
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException RegistrationDAO.getLandMark-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getLandMark-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_airPortCodeList;
	}





	public static ArrayList getCabNum(String cabNumber,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_cmp = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement("select * from TDS_VEHICLE where V_ASSOCCODE='"+assoccode+"' and V_VNO like ?");
			m_pst.setString(1, cabNumber+"%");
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			al_cmp = new ArrayList();
			while(m_rs.next()) {
				al_cmp.add(m_rs.getString("V_VNO"));
			}
			//System.out.println("Data in DAO "+al_cmp);
		} catch (SQLException sqex ) {
			cat.error("TDSException ServiceRequestDAO.getCabNum->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getCabNum-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_cmp;
	}

	public static ArrayList getUserName(String name,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_cmp = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();
			m_pst = m_con.prepareStatement("select * from TDS_USERADDRESSMASTER where CU_MASTERASSOCIATIONCODE='"+assoccode+"' and CU_NAME like ?");
			m_pst.setString(1, name+"%");
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			al_cmp = new ArrayList();
			while(m_rs.next() ){
				al_cmp.add(m_rs.getString("CU_NAME"));
			}

		} catch (SQLException sqex ) {
			cat.error("TDSException ServiceRequestDAO.getUserName->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getUserName-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_cmp;
	}
	public static ArrayList getCostCenter(String costCenter,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_cmp = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();

			m_pst = m_con.prepareStatement("select * from TDS_GOVT_VOUCHER where VC_ASSOCCODE='"+assoccode+"' and (VC_COSTCENTER like ? )");
			m_pst.setString(1, costCenter+"%");
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			al_cmp = new ArrayList();
			while(m_rs.next()) {
				al_cmp.add(m_rs.getString("VC_COSTCENTER"));
			}

		} catch (SQLException sqex ) {
			cat.error("TDSException ServiceRequestDAO.getCostCenter->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getCostCenter-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_cmp;
	}
	//	public static int insertJobLogs(String reason,OpenRequestBO orBO,AdminRegistrationBO adminBO,int status, String latitude, String longitude){
	//
	//		int result=0;
	//		TDSConnection dbcon = null;
	//		Connection con = null;
	//		PreparedStatement pst = null,pst1 = null;
	//		dbcon = new TDSConnection();
	//		con = dbcon.getConnection(); 
	//		try {
	//			pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS, ORL_LATITUDE, ORL_LONGITUDE) VALUES (?,NOW(),?,?,?,?,?,?)");
	//			pst.setString(1, orBO.getTripid());
	//			pst.setString(2, reason);
	//			pst.setString(3, adminBO.getUid());
	//			pst.setString(4, adminBO.getAssociateCode());
	//			pst.setInt(5, status);
	//			pst.setString(6, latitude);
	//			pst.setString(7, longitude);
	//
	//			cat.info(pst.toString());
	//
	//			result = pst.executeUpdate();
	//		}catch (Exception sqex){
	//			cat.error("TDSException ServiceRequestDAO.insertJobLogs-->"+sqex.getMessage());
	//			cat.error(pst.toString());
	//			System.out.println("TDSException ServiceRequestDAO.insertJobLogs-->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//		} finally {
	//			dbcon.closeConnection();
	//		} 
	//		return result;
	//	}
	//	public static int insertJobLogs(String reason,String tripID,AdminRegistrationBO adminBO,int status, String latitude, String longitude){
	//
	//		int result=0;
	//		TDSConnection dbcon = null;
	//		Connection con = null;
	//		PreparedStatement pst = null,pst1 = null;
	//		dbcon = new TDSConnection();
	//		con = dbcon.getConnection(); 
	//		try {
	//			pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS, ORL_LATITUDE, ORL_LONGITUDE) VALUES (?,NOW(),?,?,?,?,?,?)");
	//			pst.setString(1, tripID);
	//			pst.setString(2, reason);
	//			pst.setString(3, adminBO.getUid());
	//			pst.setString(4, adminBO.getAssociateCode());
	//			pst.setInt(5, status);
	//			pst.setString(6, latitude);
	//			pst.setString(7, longitude);
	//
	//			cat.info(pst.toString());
	//
	//			result = pst.executeUpdate();
	//		}catch (Exception sqex){
	//			cat.error("TDSException ServiceRequestDAO.insertJobLogs-->"+sqex.getMessage());
	//			cat.error(pst.toString());
	//			System.out.println("TDSException ServiceRequestDAO.insertJobLogs-->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//		} finally {
	//			dbcon.closeConnection();
	//		} 
	//		return result;
	//	}
	public static ArrayList getZoneList(String zoneNumber,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_cmp = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();

			m_pst = m_con.prepareStatement("select * from TDS_QUEUE_DETAILS where QD_ASSOCCODE='"+assoccode+"' and (QD_QUEUENAME like ? OR QD_DESCRIPTION like ?)");
			m_pst.setString(1, zoneNumber+"%");
			m_pst.setString(2, zoneNumber+"%");
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			al_cmp = new ArrayList();
			while(m_rs.next()) {
				al_cmp.add(m_rs.getString("QD_QUEUENAME"));
				al_cmp.add(m_rs.getString("QD_DESCRIPTION"));

			}
		} catch (SQLException sqex ) {
			cat.error("TDSException ServiceRequestDAO.getZoneList->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getZoneList-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_cmp;
	}

	public static int updateGoogleKey(String key,String sessionId,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("UPDATE TDS_LOGINDETAILS SET LO_GOOG_REG_KEY='"+key+"' WHERE LO_SESSIONID='"+sessionId+"'");
			cat.info(pst.toString());
			result=pst.executeUpdate();
			pst=con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_GOOG_REG_KEY='"+key+"' WHERE DL_DRIVERID='"+adminBO.getUid()+"' AND DL_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
			cat.info(pst.toString());
			result=pst.executeUpdate();
		} catch (SQLException sqex ) {
			cat.error("TDSException ServiceRequestDAO.updateGoogleKey->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateGoogleKey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int updateUserPassword(String associationCode, String driverID, String password,String newPassword) {
		//cat.info("In checkTripinOpenRequest Method");
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String phoneNumber="";
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try {
			pst = con.prepareStatement("SELECT * FROM TDS_ADMINUSER WHERE AU_SNO = ? AND AU_PASSWORD=? AND AU_ASSOCCODE = ? ");
			pst.setString(1, driverID);
			pst.setString(2, password);
			pst.setString(3, associationCode);
			cat.info(pst.toString());
			rs =pst.executeQuery();
			if(rs.next()){
				pst = con.prepareStatement("UPDATE TDS_ADMINUSER SET AU_PASSWORD=? WHERE AU_SNO = ? AND AU_PASSWORD=? AND AU_ASSOCCODE = ? ");
				pst.setString(1,newPassword);
				pst.setString(2, driverID);
				pst.setString(3, password);
				pst.setString(4, associationCode);
				cat.info(pst.toString());
				result =pst.executeUpdate();
			}

		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.updateUserPassword-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateUserPassword-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int endRoute(String assoCode, String routeNo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//cat.info("In serviceRequest setStart Trip ");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query1 = "DELETE FROM TDS_SHARED_RIDE WHERE SR_ROUTE_ID =? AND SR_ASSOCCODE =?";
		String query2 = "DELETE FROM TDS_SHARED_RIDE_DETAILS WHERE SRD_ROUTE_NO =? AND SRD_ASSOCCODE =?";
		try {
			pst = con.prepareStatement(query1);
			pst.setString(1, routeNo);
			pst.setString(2, assoCode);
			cat.info(pst.toString());
			result = pst.executeUpdate();
			pst = con.prepareStatement(query2);
			pst.setString(1, routeNo);
			pst.setString(2, assoCode);
			cat.info(pst.toString());
			result = pst.executeUpdate();
			con.close();
			pst.close();
		} catch(SQLException sqex) {
			//cat nat defined
			cat.error("TDSException ServiceRequestDAO.endRoute-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.endRoute-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static OpenRequestBO getOpenRequestHistoryByTripID(String tripID,AdminRegistrationBO adminBo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		OpenRequestBO requestBO = null;
		PreparedStatement m_pst = null,pst1=null;
		ResultSet m_rs = null,rs1=null;
		String query="";
		TDSConnection m_tdsConn = new TDSConnection();
		Connection m_conn = m_tdsConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("select ORH_TRIP_ID,ORH_AMT,ORH_TRIP_STATUS,ORH_QUEUENO,ORH_DRCABFLAG,ORH_SMS_SENT,ORH_DRIVERID,ORH_DISPATCH_HISTORY_DRIVERS,ORH_PHONE,ORH_STADD1,ORH_STADD2,ORH_STCITY,ORH_STSTATE,ORH_STZIP,ORH_SPLINS,ORH_PAYACC,ORH_PAYTYPE,"+
					"ORH_EDADD1,ORH_EDADD2,ORH_EDCITY,ORH_EMAIL,ORH_METER_TYPE,ORH_TRIP_STATUS,ORH_DISPATCH_LEAD_TIME,ORH_DONT_DISPATCH,ORH_PREMIUM_CUSTOMER,ORH_SHARED_RIDE,ORH_ELANDMARK,ORH_EDSTATE,ORH_NAME,ORH_EDZIP,ORH_REPEAT_GROUP,ORH_PHONE,ORH_NUMBER_OF_PASSENGERS,ORH_DROP_TIME,ORH_STLATITUDE,ORH_OPERATOR_COMMENTS,ORH_STLONGITUDE,ORH_EDLATITUDE,ORH_DRIVERID,ORH_VEHICLE_NO,ORH_EDLONGITUDE,ORH_LANDMARK,ORH_DRCABFLAG,ORH_QUEUENO," +	"DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%H%i') "+"as TIME, CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"') AS OFFSET_DATETIME,ORH_AIRLINE_NAME,ORH_AIRLINE_NO,ORH_AIRLINE_FROM,ORH_AIRLINE_TO,ORH_CUST_REF_NUM,ORH_CUST_REF_NUM1,ORH_CUST_REF_NUM2,ORH_CUST_REF_NUM3 from TDS_OPENREQUEST_HISTORY WHERE ORH_MASTER_ASSOCCODE=? AND ORH_TRIP_ID =? ");
			m_pst.setString(1, adminBo.getMasterAssociateCode());
			m_pst.setString(2, tripID);
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				requestBO = new OpenRequestBO();
				requestBO.setTripid(m_rs.getString("ORH_TRIP_ID"));
				requestBO.setPhone(m_rs.getString("ORH_PHONE"));
				requestBO.setName(m_rs.getString("ORH_NAME"));
				requestBO.setSdate(TDSValidation.getUserDateFormat(m_rs.getString("DATE")));
				requestBO.setShrs((m_rs.getString("TIME")));
				requestBO.setPaytype((m_rs.getString("ORH_PAYTYPE")));
				requestBO.setAcct(m_rs.getString("ORH_PAYACC"));
				requestBO.setSlandmark(m_rs.getString("ORH_LANDMARK"));
				requestBO.setSpecialIns(m_rs.getString("ORH_SPLINS"));
				requestBO.setComments(m_rs.getString("ORH_OPERATOR_COMMENTS"));
				requestBO.setVehicleNo(m_rs.getString("ORH_VEHICLE_NO"));
				requestBO.setDriverid(m_rs.getString("ORH_DRIVERID"));
				requestBO.setQueueno(m_rs.getString("ORH_QUEUENO"));
				requestBO.setDrProfile(m_rs.getString("ORH_DRCABFLAG"));
				requestBO.setStartTimeStamp(m_rs.getString("OFFSET_DATETIME"));
				requestBO.setTripStatus(m_rs.getString("ORH_TRIP_STATUS"));
				requestBO.setOR_SMS_SENT(m_rs.getString("ORH_SMS_SENT"));
				requestBO.setHistoryOfDrivers(m_rs.getString("ORH_DISPATCH_HISTORY_DRIVERS"));
				requestBO.setSadd1(m_rs.getString("ORH_STADD1"));
				requestBO.setSadd2(m_rs.getString("ORH_STADD2"));
				requestBO.setScity(m_rs.getString("ORH_STCITY"));
				requestBO.setSstate(m_rs.getString("ORH_STSTATE"));
				requestBO.setSzip(m_rs.getString("ORH_STZIP"));
				requestBO.setSlat(m_rs.getString("ORH_STLATITUDE"));
				requestBO.setSlong(m_rs.getString("ORH_STLONGITUDE"));
				requestBO.setEadd1(m_rs.getString("ORH_EDADD1"));
				requestBO.setEadd2(m_rs.getString("ORH_EDADD2"));
				requestBO.setEcity(m_rs.getString("ORH_EDCITY"));
				requestBO.setEstate(m_rs.getString("ORH_EDSTATE"));
				requestBO.setEzip(m_rs.getString("ORH_EDZIP"));
				requestBO.setEdlatitude(m_rs.getString("ORH_EDLATITUDE"));
				requestBO.setEdlongitude(m_rs.getString("ORH_EDLONGITUDE"));
				requestBO.setTypeOfRide(m_rs.getString("ORH_SHARED_RIDE"));
				requestBO.setRepeatGroup(m_rs.getString("ORH_REPEAT_GROUP"));
				requestBO.setAmt(new BigDecimal(m_rs.getString("ORH_AMT")));
				requestBO.setElandmark(m_rs.getString("ORH_ELANDMARK"));
				requestBO.setNumOfPassengers(m_rs.getInt("ORH_NUMBER_OF_PASSENGERS"));
				requestBO.setDropTime(m_rs.getString("ORH_DROP_TIME"));
				requestBO.setDontDispatch(m_rs.getInt("ORH_DONT_DISPATCH"));
				requestBO.setPremiumCustomer(m_rs.getInt("ORH_PREMIUM_CUSTOMER"));
				requestBO.setAdvanceTime(m_rs.getString("ORH_DISPATCH_LEAD_TIME"));
				requestBO.setEmail(m_rs.getString("ORH_EMAIL"));
				requestBO.setChckTripStatus(m_rs.getInt("ORH_TRIP_STATUS"));
				requestBO.setTripStatus(m_rs.getString("ORH_TRIP_STATUS"));
				requestBO.setPaymentMeter(m_rs.getInt("ORH_METER_TYPE"));
				requestBO.setAirName(m_rs.getString("ORH_AIRLINE_NAME"));
				requestBO.setAirNo(m_rs.getString("ORH_AIRLINE_NO"));
				requestBO.setAirFrom(m_rs.getString("ORH_AIRLINE_FROM"));
				requestBO.setAirTo(m_rs.getString("ORH_AIRLINE_TO"));
				requestBO.setRefNumber(m_rs.getString("ORH_CUST_REF_NUM"));
				requestBO.setRefNumber1(m_rs.getString("ORH_CUST_REF_NUM1"));
				requestBO.setRefNumber2(m_rs.getString("ORH_CUST_REF_NUM2"));
				requestBO.setRefNumber3(m_rs.getString("ORH_CUST_REF_NUM3"));
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getOpenRequestHistoryByTripID-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getOpenRequestHistoryByTripID" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return requestBO;
	}
	
	public static int updateforgetPassword(String driverid, String key, String newpassword){
		cat.info("TDS INFO ServiceRequestDAO.updateforgetPassword");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1=null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_ADMINUSER WHERE AU_USERNAME=? AND  AU_FORGET_KEY=? ");
			pst.setString(1,driverid);
			pst.setString(2,key);
			rs=pst.executeQuery();
			if(rs.next()){
				pst1 = con.prepareStatement("UPDATE TDS_ADMINUSER SET AU_PASSWORD=? WHERE AU_FORGET_KEY=?  AND AU_USERNAME=? ");
				pst1.setString(1,newpassword);
				pst1.setString(2,key);
				pst1.setString(3,driverid);
				result=pst1.executeUpdate();
				
				}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException ServiceRequestDAO.updateforgetPassword-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateforgetPassword-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		return result;
	}
	
	public static CustomerMobileBO customerUserIdVerification(String userID){
		cat.info("TDS INFO CustomerMobileDAO.customerUserIdVerification");
		CustomerMobileBO cmBo =null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_DRIVER WHERE  DR_USERID=? ");
			pst.setString(1, userID);
			rs = pst.executeQuery();
			if(rs.next()){
				cmBo = new CustomerMobileBO();
				cmBo.setUserName(rs.getString("DR_SNO"));
				cmBo.setPhoneNumber(rs.getString("DR_PHONE"));
				cmBo.setAssoccode(rs.getString("DR_ASSOCODE"));
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException ServiceRequestDAO.customerUserIdVerification-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.customerUserIdVerification-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		return cmBo;
	}

	public static int customerRegeneratedPasswordKeyUpdate(String ccode,String userID, String key){
		cat.info("TDS INFO ServiceRequestDAO.customerRegeneratedPasswordKeyUpdate");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			
			pst = con.prepareStatement("UPDATE TDS_ADMINUSER SET AU_FORGET_KEY=? WHERE AU_USERNAME=? AND AU_ASSOCCODE=?");
			pst.setString(1, key);
			pst.setString(2, userID);
			pst.setString(3, ccode);
			result=pst.executeUpdate();
        	pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException ServiceRequestDAO.customerRegeneratedPasswordKeyUpdate-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.customerRegeneratedPasswordKeyUpdate-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		return result;
	}

	
	 public static OpenRequestBO CustomerOpenRequest(ArrayList<String> assocode,String associationCode,  String tripID) {
		 String fleets ="";
		 if(assocode != null){
			 if(assocode.size() >0){
				 fleets = "(";
				 for(int j=0; j<assocode.size()-1;j++){
					 fleets = fleets + "'" + assocode.get(j) + "',";
				 }
				 fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			 } else {
				 fleets = "('"+associationCode+"')";
			 }
		 } else {
			 fleets = "('"+associationCode+"')";
		 }
		 OpenRequestBO orBo = CustomerOpenRequest(fleets,tripID);
		 return orBo;
	 }

	public static OpenRequestBO CustomerOpenRequest(String associationCode,  String tripID) {
		//cat.info("In checkTripinOpenRequest Method");
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		OpenRequestBO openBo = new OpenRequestBO();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE IN "+associationCode);
			pst.setString(1, tripID);
//			pst.setString(2, associationCode);
			cat.info(pst.toString());
			//System.out.println("query"+pst.toString());
			rs =pst.executeQuery();
			if (rs.first()) {
				openBo.setTripid(tripID);
				openBo.setTripSource(rs.getInt("OR_TRIP_SOURCE"));
				openBo.setCreatedBy(rs.getString("OR_CREATED_BY"));
				openBo.setAssociateCode(associationCode);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getPhoneNoByTripID-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getPhoneNoByTripID-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return openBo;
	}
	
	
	public static int insert_BtZ_DriverLogin(String driverid,String cabNo, String queueId,String status, String reason,String assoccode,String master) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet m_rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			//DQB_ZONE_BOOKING_TIME,DQB_ZONE_LOGOUT_TIME,
			pst = con.prepareStatement("INSERT INTO TDS_DRIVER_QUEUEBOOK (DQB_DRIVERID,DQB_CABNO,DQB_QUEUEID,DQB_SWITCH,DQB_SWITCH_UPDATED_TIME,DQB_REASON,DQB_ASSOCCODE,DQB_MASTER_ASSOCCODE,DQB_DRIVER_LOGIN_TIME) VALUES(?,?,?,?,NOW(),?,?,?,NOW())");
			pst.setString(1, driverid);
			pst.setString(2, cabNo);
			pst.setString(3, queueId);
			pst.setString(4, status);
			pst.setString(5, reason);
			pst.setString(6, assoccode);
			pst.setString(7, master);
			
			cat.info(pst.toString());
			//System.out.println("query for insert login:"+pst.toString());
			result = pst.executeUpdate();

			pst.close();
			con.close();
			//System.out.println("Result in setDriver Queue "+result);
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.insertDriver_BooktoZone-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.insertDriver_BooktoZone-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static int Update_BtZ_DriverLogout(String driverid,String cabNo, String queueId,String status, String reason,String assoccode,String master) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst1 = null,pst2 = null;
		ResultSet m_rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			//DQB_ZONE_BOOKING_TIME,DQB_ZONE_LOGOUT_TIME,
			if(!cabNo.equals("")){
				pst2 = con.prepareStatement("UPDATE TDS_DRIVER_QUEUEBOOK SET DQB_QUEUEID=?,DQB_SWITCH=?,DQB_SWITCH_UPDATED_TIME=NOW(),DQB_REASON=?,DQB_DRIVER_LOGOUT_TIME=NOW() WHERE DQB_DRIVERID=? AND DQB_MASTER_ASSOCCODE=? AND DQB_ASSOCCODE=? AND DQB_CABNO=? AND DQB_SWITCH!=?");
				pst2.setString(7, cabNo);
				pst2.setString(8, "4");
			}else{
				pst2 = con.prepareStatement("UPDATE TDS_DRIVER_QUEUEBOOK SET DQB_QUEUEID=?,DQB_SWITCH=?,DQB_SWITCH_UPDATED_TIME=NOW(),DQB_REASON=?,DQB_DRIVER_LOGOUT_TIME=NOW() WHERE DQB_DRIVERID=? AND DQB_MASTER_ASSOCCODE=? AND DQB_ASSOCCODE=? AND DQB_SWITCH!=?");
				pst2.setString(7, "4");
			}
			pst2.setString(1, queueId);
			pst2.setString(2, status);
			pst2.setString(3, reason);
			pst2.setString(4, driverid);
			pst2.setString(5, master);
			pst2.setString(6, assoccode);
			
			cat.info(pst2.toString());
			//System.out.println("query for update logout BTZ:"+pst2.toString());
			result = pst2.executeUpdate();
			pst2.close();
			con.close();
			//System.out.println("Result in setDriver Queue "+result);
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.Update_BtZ_DriverLogout-->"+sqex.getMessage());
			cat.error(pst2.toString());
			//System.out.println("TDSException ServiceRequestDAO.Update_BtZ_DriverLogout-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static int Update_BtZ_ZoneLogin(String driverid,String cabNo, String queueId,String status, String reason,String assoccode,String master) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst1 = null,pst2 = null;
		ResultSet m_rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			//DQB_ZONE_BOOKING_TIME,DQB_ZONE_LOGOUT_TIME,
			if(!cabNo.equals("")){
				pst2 = con.prepareStatement("UPDATE TDS_DRIVER_QUEUEBOOK SET DQB_QUEUEID=?,DQB_SWITCH=?,DQB_SWITCH_UPDATED_TIME=NOW(),DQB_REASON=?,DQB_ZONE_BOOKING_TIME=NOW(),DQB_ZONE_LOGOUT_TIME='1970-01-01 00:00:00' WHERE DQB_DRIVERID=? AND DQB_MASTER_ASSOCCODE=? AND DQB_ASSOCCODE=? AND DQB_CABNO=? AND DQB_SWITCH!=?");
				pst2.setString(7, cabNo);
				pst2.setString(8, "4");
			}else{
				pst2 = con.prepareStatement("UPDATE TDS_DRIVER_QUEUEBOOK SET DQB_QUEUEID=?,DQB_SWITCH=?,DQB_SWITCH_UPDATED_TIME=NOW(),DQB_REASON=?,DQB_ZONE_BOOKING_TIME=NOW(),DQB_ZONE_LOGOUT_TIME='1970-01-01 00:00:00' WHERE DQB_DRIVERID=? AND DQB_MASTER_ASSOCCODE=? AND DQB_ASSOCCODE=? AND DQB_SWITCH!=?");
				pst2.setString(7, "4");
			}
			pst2.setString(1, queueId);
			pst2.setString(2, status);
			pst2.setString(3, reason);
			pst2.setString(4, driverid);
			pst2.setString(5, master);
			pst2.setString(6, assoccode);
			
			cat.info(pst2.toString());
			//System.out.println("query for update BTZ ZONE login:"+pst2.toString());
			result = pst2.executeUpdate();
			pst2.close();
			con.close();
			//System.out.println("Result in setDriver Queue "+result);
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.Update_BtZ_ZoneLogin-->"+sqex.getMessage());
			cat.error(pst2.toString());
			//System.out.println("TDSException ServiceRequestDAO.Update_BtZ_ZoneLogin-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	
	public static int Update_BtZ_ZoneLogout(String driverid,String cabNo, String queueId,String status, String reason,String assoccode,String master) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst1 = null,pst2 = null;
		ResultSet m_rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			//DQB_ZONE_BOOKING_TIME,DQB_ZONE_LOGOUT_TIME,
			if(!cabNo.equals("")){
				pst2 = con.prepareStatement("UPDATE TDS_DRIVER_QUEUEBOOK SET DQB_QUEUEID=?,DQB_SWITCH=?,DQB_SWITCH_UPDATED_TIME=NOW(),DQB_REASON=?,DQB_ZONE_LOGOUT_TIME=NOW() WHERE DQB_DRIVERID=? AND DQB_MASTER_ASSOCCODE=? AND DQB_ASSOCCODE=? AND DQB_CABNO=? AND DQB_SWITCH!=?");
				pst2.setString(7, cabNo);
				pst2.setString(8, "4");
			}else{
				pst2 = con.prepareStatement("UPDATE TDS_DRIVER_QUEUEBOOK SET DQB_QUEUEID=?,DQB_SWITCH=?,DQB_SWITCH_UPDATED_TIME=NOW(),DQB_REASON=?,DQB_ZONE_LOGOUT_TIME=NOW() WHERE DQB_DRIVERID=? AND DQB_MASTER_ASSOCCODE=? AND DQB_ASSOCCODE=? AND DQB_SWITCH!=?");
				pst2.setString(7, "4");
			}
			pst2.setString(1, queueId);
			pst2.setString(2, status);
			pst2.setString(3, reason);
			pst2.setString(4, driverid);
			pst2.setString(5, master);
			pst2.setString(6, assoccode);
			
			cat.info(pst2.toString());
			//System.out.println("query for update BTZ ZONE logout:"+pst2.toString());
			result = pst2.executeUpdate();
			pst2.close();
			con.close();
			//System.out.println("Result in setDriver Queue "+result);
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.Update_BtZ_ZoneLogout-->"+sqex.getMessage());
			cat.error(pst2.toString());
			//System.out.println("TDSException ServiceRequestDAO.Update_BtZ_ZoneLogout-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static int Update_BtZ_DriverStatus(String driverid,String cabNo, String queueId,String status, String reason,String assoccode,String master) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst1 = null,pst2 = null;
		ResultSet m_rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			//DQB_ZONE_BOOKING_TIME,DQB_ZONE_LOGOUT_TIME,
			if(!cabNo.equals("")){
				pst2 = con.prepareStatement("UPDATE TDS_DRIVER_QUEUEBOOK SET DQB_QUEUEID=?,DQB_SWITCH=?,DQB_SWITCH_UPDATED_TIME=NOW(),DQB_REASON=? WHERE DQB_DRIVERID=? AND DQB_MASTER_ASSOCCODE=? AND DQB_ASSOCCODE=? AND DQB_CABNO=? AND DQB_SWITCH!=?");
				pst2.setString(7, cabNo);
				pst2.setString(8, "4");
			}else{
				pst2 = con.prepareStatement("UPDATE TDS_DRIVER_QUEUEBOOK SET DQB_QUEUEID=?,DQB_SWITCH=?,DQB_SWITCH_UPDATED_TIME=NOW(),DQB_REASON=? WHERE DQB_DRIVERID=? AND DQB_MASTER_ASSOCCODE=? AND DQB_ASSOCCODE=? AND DQB_SWITCH!=?");
				pst2.setString(7, "4");
			}
			pst2.setString(1, queueId);
			pst2.setString(2, status);
			pst2.setString(3, reason);
			pst2.setString(4, driverid);
			pst2.setString(5, master);
			pst2.setString(6, assoccode);
			
			cat.info(pst2.toString());
			//System.out.println("query for update BTZ driver details update:"+pst2.toString());
			result = pst2.executeUpdate();
			pst2.close();
			con.close();
			//System.out.println("Result in setDriver Queue "+result);
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.Update_BtZ_DriverStatus-->"+sqex.getMessage());
			cat.error(pst2.toString());
			//System.out.println("TDSException ServiceRequestDAO.Update_BtZ_DriverStatus-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static String getQueueIdByDriverIDandAssoccode(String associationCode,  String driverID) {
		//cat.info("In checkTripinOpenRequest Method");
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result="";
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_QUEUE WHERE QU_DRIVERID= ? AND QU_ASSOCCODE = ? ");
			pst.setString(1, driverID);
			pst.setString(2, associationCode);
			cat.info(pst.toString());
			rs =pst.executeQuery();
			if (rs.next()) {
				result = rs.getString("QU_NAME");
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getPhoneNoByTripID-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getPhoneNoByTripID-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static ArrayList<String> getLastZone(String masterCode,String driverID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<String> resultZone = new ArrayList<String>();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_DRIVER_ZONE_HISTORY LEFT JOIN TDS_QUEUE_DETAILS ON QD_QUEUENAME=DZH_ZONEID AND QD_ASSOCCODE=DZH_MASTER_ASSOCCODE WHERE DZH_DRIVERID='"+driverID+"' AND DZH_MASTER_ASSOCCODE = '"+masterCode+"' AND DZH_ZONEID<>'' ORDER BY DZH_UPDATED_TIME DESC LIMIT 0,1");
			rs =pst.executeQuery();
			if (rs.next()) {
				resultZone.add(rs.getString("DZH_ZONEID"));
				resultZone.add(rs.getString("QD_DESCRIPTION"));
			}else{
				pst = con.prepareStatement("SELECT * FROM TDS_DRIVER_QUEUEBOOK LEFT JOIN TDS_QUEUE_DETAILS ON QD_QUEUENAME=DQB_QUEUEID AND QD_ASSOCCODE=DQB_MASTER_ASSOCCODE WHERE DQB_DRIVERID='"+driverID+"' AND DQB_MASTER_ASSOCCODE = '"+masterCode+"' AND DQB_QUEUEID <> '' ORDER BY DQB_SWITCH_UPDATED_TIME DESC");
				rs =pst.executeQuery();
				if (rs.next()) {
					resultZone.add(rs.getString("DQB_QUEUEID"));
					resultZone.add(rs.getString("QD_DESCRIPTION"));
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getLastZone-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getLastZone-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return resultZone;
	}


	public static int insert_DriverLoginToQueue(String driverid,String cabNo, String queueId,String assoccode,String master,String status, String reason) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet m_rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("INSERT INTO TDS_DRIVER_ZONE_HISTORY (DZH_DRIVERID,DZH_CABNO,DZH_ZONEID,DZH_LOGINTIME,DZH_ASSOCCODE,DZH_MASTER_ASSOCCODE,DZH_UPDATED_TIME,DZH_SWITCH,DZH_REASON) VALUES(?,?,?,NOW(),?,?,NOW(),?,?)");
			pst.setString(1, driverid);
			pst.setString(2, cabNo);
			pst.setString(3, queueId);
			pst.setString(4, assoccode);
			pst.setString(5, master);
			pst.setString(6, status);
			pst.setString(7, reason);
			
			cat.info(pst.toString());
			//System.out.println("insert_DriverLoginToQueue:"+pst.toString());
			result = pst.executeUpdate();

			pst.close();
			con.close();
			//System.out.println("Result in insert_DriverLoginToQueue:"+result);
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.insert_DriverLoginToQueue-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.println("TDSException ServiceRequestDAO.insert_DriverLoginToQueue-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	
	public static int Update_DriverLogOutfromQueue(String driverid,String cabNo, String queueId,String assoccode,String status, String reason) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		
		int result = 0;
		System.out.println("ZoneID:"+queueId+" --- reason:"+reason);
		if(!queueId.equals("")){
			TDSConnection dbcon = null;
			Connection con = null;
			PreparedStatement pst = null;
			ResultSet m_rs = null;
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			try {
				if(!cabNo.equals("")){
					pst = con.prepareStatement("UPDATE TDS_DRIVER_ZONE_HISTORY SET DZH_LOGOUTTIME=NOW(),DZH_UPDATED_TIME=NOW(),DZH_SWITCH=?,DZH_REASON=? WHERE DZH_DRIVERID=? AND DZH_CABNO=? AND (DZH_ASSOCCODE=? OR DZH_MASTER_ASSOCCODE='"+assoccode+"') AND DZH_ZONEID=? AND DZH_SWITCH='1'");
					pst.setString(4, cabNo);
					pst.setString(5, assoccode);
					pst.setString(6, queueId);
				}else{
					pst = con.prepareStatement("UPDATE TDS_DRIVER_ZONE_HISTORY SET DZH_LOGOUTTIME=NOW(),DZH_UPDATED_TIME=NOW(),DZH_SWITCH=?,DZH_REASON=? WHERE DZH_DRIVERID=? AND (DZH_ASSOCCODE=? OR DZH_MASTER_ASSOCCODE='"+assoccode+"') AND DZH_ZONEID=? AND DZH_SWITCH='1'");
					pst.setString(4, assoccode);
					pst.setString(5, queueId);
				}
				pst.setString(1, status);
				pst.setString(2, reason);
				pst.setString(3, driverid);
				
				cat.info(pst.toString());
				result = pst.executeUpdate();
	
				pst.close();
				con.close();
				//System.out.println("Result in Update_DriverLogOutfromQueue:"+result);
			} catch (SQLException sqex) {
				//cat not defined
				cat.error("TDSException ServiceRequestDAO.Update_DriverLogOutfromQueue-->"+sqex.getMessage());
				cat.error(pst.toString());
				System.out.println("TDSException ServiceRequestDAO.Update_DriverLogOutfromQueue-->"+sqex.getMessage());
				sqex.printStackTrace();
			} finally {
				dbcon.closeConnection();
			}
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int insertDriverGPS(String masterAssoccode, String driverID, String lat, String lon, String cab, int type,String tripID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			
			if(lat==null || lat.equals("")){
				lat = "0";
			}
			
			if(lon==null || lon.equals("")){
				lon = "0";
			}
			
			pst = con.prepareStatement("INSERT INTO TDS_DRIVERGPSHISTORY (GPS_DRIVERID,GPS_LATITUDE,GPS_LONGITUDE,GPS_CABNO,GPS_UPDATEDTIME,GPS_ASSOCCODE,GPS_SWITCH,GPS_TRIPID) VALUES (?,?,?,?,NOW(),?,?,?)");
			pst.setString(1, driverID);
			pst.setString(2, lat);
			pst.setString(3, lon) ;
			pst.setString(4, cab);
			pst.setString(5, masterAssoccode);
			pst.setInt(6, type);
			pst.setString(7, tripID);
			cat.info(pst.toString());
			//type 0-login,1-logout,2-accept,3-route,4-site,5-start,6-stc,7-end,8-normal
			//System.out.println("query:"+pst.toString());
			result = pst.executeUpdate();
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.insertDriverGPS-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.insertDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	public static DriverLocationBO getPrevDriverLocationBO(String associationCode,  String driverID) {
		//cat.info("In checkTripinOpenRequest Method");
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		DriverLocationBO driverLocationBo = null;
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_DRIVERLOCATION WHERE DL_DRIVERID=? AND DL_MASTER_ASSOCCODE= ? ");
			pst.setString(1, driverID);
			pst.setString(2, associationCode);
			cat.info(pst.toString());
			rs =pst.executeQuery();
			if (rs.next()) {
				driverLocationBo = new DriverLocationBO();
				driverLocationBo.setLatitude(rs.getString("DL_LATITUDE"));
				driverLocationBo.setLongitude(rs.getString("DL_LONGITUDE"));
				driverLocationBo.setUpdatedTime(rs.getString("DL_LASTUPDATE"));
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ServiceRequestDAO.getPhoneNoByTripID-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getPhoneNoByTripID-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return driverLocationBo;
	}
	
}
