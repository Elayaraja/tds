package com.tds.dao;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

public class SharedRideDAO {
	private static Category cat =TDSController.cat;
	static {
		cat = Category.getInstance(SharedRideDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+SharedRideDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static int insertSharedRide(AdminRegistrationBO adminBo,String total,String driverId,String cabNo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int routeNo = 0;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="INSERT INTO TDS_SHARED_RIDE(SR_CREATED_TIME,SR_CREATED_BY,SR_TOTAL_TRIPS,SR_ASSOCCODE,SR_DRIVERID,SR_STATUS,SR_VEHICLE_NO) VALUES(NOW(),?,?,?,?,?,?)";
		try{
			pst=con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, adminBo.getUid());
			pst.setString(2, total);
			pst.setString(3, adminBo.getAssociateCode());
			pst.setString(4, driverId);
			if(driverId!=null&&driverId!=""){
				pst.setString(5,"40");
			}else{
				pst.setString(5,"8");
			}
			pst.setString(6, cabNo);
			cat.info(pst.toString());
			pst.execute();
			rs = pst.getGeneratedKeys();
			if(rs.next()){
				routeNo = rs.getInt(1);
			}

		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.insertSharedRide-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.insertSharedRide-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return routeNo;
	}
	public static int insertToSharedDetails(String assoCode,String routeNo,ArrayList<OpenRequestBO> alist,int indPay){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="INSERT INTO TDS_SHARED_RIDE_DETAILS(SRD_TRIP_ID,SRD_ASSOCCODE,SRD_ROUTE_NO,SRD_PICKUP_ORDER,SRD_ORDER_SWITCH,SRD_PAY_TYPE) VALUES(?,?,?,?,?,?)";
		try{
			for(int i=0;i<alist.size();i++){
				pst=con.prepareStatement(query);
				pst.setString(1, alist.get(i).getTripid());
				pst.setString(2, assoCode);
				pst.setString(3, routeNo);
				pst.setString(4, alist.get(i).getPickupOrder());
				pst.setInt(5, alist.get(i).getPickupOrderSwitch());
				pst.setInt(6, indPay);
				cat.info(pst.toString());
				result = pst.executeUpdate();
			}
		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.insertToSharedDetails-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.insertToSharedDetails-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int updateOpenrequestForShared(String assoCode,String routeNo,ArrayList<OpenRequestBO> alist,String[] exRoute,String driverId,String cabNo,int indpay){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String tripIdList="";
		String exRouteList="";
		String tripListForShare = "";
		String queryAppend ="";
		if(alist != null){
			if(alist.size() >0){
				tripIdList = " AND OR_TRIP_ID IN(";
				for(int j=0; j<alist.size()-1;j++){
					tripIdList = tripIdList + "'" + alist.get(j).getTripid() + "',";
				}
				tripIdList = tripIdList + "'" + alist.get(alist.size()-1).getTripid() + "')";
			}
		}
		if(alist != null){
			if(alist.size() >0){
				tripListForShare = " AND SRD_TRIP_ID IN(";
				for(int j=0; j<alist.size()-1;j++){
					tripListForShare = tripListForShare + "'" + alist.get(j).getTripid() + "',";
				}
				tripListForShare = tripListForShare + "'" + alist.get(alist.size()-1).getTripid() + "')";
			}
		}
		if(exRoute!=null){
			if(exRoute.length>0){
				exRouteList = " AND SR_ROUTE_ID IN(";
				for(int k=0;k<exRoute.length;k++){
					exRouteList = exRouteList + "'" + exRoute[k] + "',";
				}
				exRouteList = exRouteList + "'" + exRoute[exRoute.length-1] + "')";
			}
		}
		if(!driverId.equalsIgnoreCase("")&&driverId!=null){
			queryAppend ="  OR_TRIP_STATUS = 40 , ";
		}
		String query2 = "DELETE FROM TDS_SHARED_RIDE_DETAILS WHERE SRD_ASSOCCODE='"+assoCode+"'"+tripListForShare;
		String query = "UPDATE TDS_OPENREQUEST SET "+queryAppend+" OR_ROUTE_NUMBER ='"+routeNo+"' , OR_DRIVERID = '"+driverId+"' ,   OR_VEHICLE_NO ='"+cabNo+"' , OR_SHAREDRIDE_PAYTYPE="+indpay+" WHERE OR_ASSOCCODE='"+assoCode+"'"+tripIdList;
		try{
			for(int i=0;i<alist.size();i++){
				pst=con.prepareStatement(query);
				cat.info(pst.toString());
				result = pst.executeUpdate();
			}
			pst=con.prepareStatement(query2);
			cat.info(pst.toString());
			result = pst.executeUpdate();
		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.updateOpenrequestForShared-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.updateOpenrequestForShared-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static ArrayList<OpenRequestBO> getSharedRideGroups(String assoCode,String timeOffset,String date){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String queryForDate ="";
		if(date!=null&&!date.equalsIgnoreCase("")){
			queryForDate = " AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE ,'UTC', '"+timeOffset+"'),'%m/%d/%Y') = '"+date+"'";

		}else{
			queryForDate = " AND DATE(CONVERT_TZ(OR_SERVICEDATE ,'UTC', '"+timeOffset+"'))=DATE(NOW())";
		}


		String sql = "SELECT SR_ROUTE_ID,SR_CREATED_BY,DATE(CONVERT_TZ(SR_CREATED_TIME ,'UTC', '"+timeOffset+"')) AS DATE,DATE_FORMAT(CONVERT_TZ(SR_CREATED_TIME ,'UTC','"+timeOffset+"'),'%h:%i %p') AS TIME,SR_TOTAL_TRIPS,SR_DRIVERID,SR_STATUS,SR_VEHICLE_NO,SR_ROUTE_DESC,OR_SERVICEDATE FROM TDS_SHARED_RIDE LEFT JOIN TDS_OPENREQUEST ON SR_ROUTE_ID = OR_ROUTE_NUMBER AND SR_ASSOCCODE = OR_ASSOCCODE WHERE OR_ASSOCCODE = '"+assoCode+"' "+queryForDate+" GROUP BY SR_ROUTE_ID ";
		String query="DELETE  FROM TDS_SHARED_RIDE_DETAILS WHERE SRD_ROUTE_NO NOT IN (SELECT SR_ROUTE_ID FROM TDS_SHARED_RIDE )";
		//String query1="SELECT *, DATE(CONVERT_TZ(SR_CREATED_TIME ,'UTC', '"+timeOffset+"')) AS DATE,DATE_FORMAT(CONVERT_TZ(SR_CREATED_TIME ,'UTC','"+timeOffset+"'),'%r') AS TIME FROM TDS_SHARED_RIDE WHERE  SR_ASSOCCODE='"+assoCode+"' '"+queryForDate+"'  ORDER BY SR_ROUTE_ID";
		try {
			pst=con.prepareStatement(query);
			pst.execute();
			pst=con.prepareStatement(sql);
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestBO orBo = new OpenRequestBO();
				orBo.setRouteNumber(rs.getString("SR_ROUTE_ID"));
				orBo.setCreatedBy(rs.getString("SR_CREATED_BY"));
				orBo.setCreatedTime(rs.getString("TIME"));
				orBo.setCreatedDate(rs.getString("DATE"));
				orBo.setTotal(rs.getString("SR_TOTAL_TRIPS"));
				orBo.setDriverid(rs.getString("SR_DRIVERID"));
				orBo.setShRideStatus(rs.getString("SR_STATUS"));
				orBo.setVehicleNo(rs.getString("SR_VEHICLE_NO"));
				orBo.setRouteDesc(rs.getString("SR_ROUTE_DESC"));
				al_list.add(orBo);
			}
		} catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.getSharedRideGroups-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.getSharedRideGroups-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	public static ArrayList<OpenRequestBO> getSharedRideDetails(String assoCode,String routeNo,String date,String timeOffset){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query ="";
		if(routeNo!=""){
			query ="SELECT CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER) AS PICKUP_ORDER,SRD_TRIP_ID,SRD_ROUTE_NO,SRD_ORDER_SWITCH, DL_LATITUDE ,DL_LONGITUDE,OR_STLATITUDE,OR_STLONGITUDE,SRD_PICKUP_ORDER,OR_DRIVERID,OR_STADD1,OR_STADD2,OR_STCITY,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_TRIP_STATUS,OR_QUEUENO,OR_END_QUEUENO   FROM TDS_SHARED_RIDE_DETAILS JOIN TDS_OPENREQUEST LEFT JOIN TDS_DRIVERLOCATION ON DL_DRIVERID = OR_DRIVERID  AND SRD_ASSOCCODE = DL_ASSOCCODE WHERE  SRD_ASSOCCODE='"+assoCode+"' AND OR_ASSOCCODE= SRD_ASSOCCODE AND OR_ROUTE_NUMBER='"+routeNo+"' AND SRD_ROUTE_NO = OR_ROUTE_NUMBER AND OR_TRIP_ID=SRD_TRIP_ID  ORDER BY PICKUP_ORDER";
		}else{
			if(date!=""){
				query ="SELECT CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER) AS PICKUP_ORDER,SRD_TRIP_ID,SRD_ROUTE_NO,SRD_ORDER_SWITCH,DL_LATITUDE ,DL_LONGITUDE,OR_STLATITUDE,OR_STLONGITUDE,OR_DRIVERID,SRD_PICKUP_ORDER,OR_STADD1,OR_STADD2,OR_STCITY,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_TRIP_STATUS,OR_QUEUENO,OR_END_QUEUENO   FROM TDS_SHARED_RIDE_DETAILS JOIN TDS_OPENREQUEST LEFT JOIN TDS_DRIVERLOCATION ON DL_DRIVERID = OR_DRIVERID  AND SRD_ASSOCCODE = DL_ASSOCCODE WHERE  SRD_ASSOCCODE='"+assoCode+ "' AND OR_ASSOCCODE= SRD_ASSOCCODE  AND SRD_ROUTE_NO = OR_ROUTE_NUMBER AND OR_TRIP_ID=SRD_TRIP_ID  AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE ,'UTC', '"+timeOffset+"'),'%m/%d/%Y') = '"+date+"' ORDER BY PICKUP_ORDER ";
			}
		}try {
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestBO orBo = new OpenRequestBO();
				//orBo.setRouteNumber(rs.getString("SRD_ROUTE_NO"));
				orBo.setTripid(rs.getString("SRD_TRIP_ID"));
				orBo.setPickupOrder(rs.getString("SRD_PICKUP_ORDER"));
				orBo.setScity(rs.getString("OR_STADD1")+""+rs.getString("OR_STADD2")+","+rs.getString("OR_STCITY"));
				orBo.setEcity(rs.getString("OR_EDADD1")+""+rs.getString("OR_EDADD2")+","+rs.getString("OR_EDCITY"));
				orBo.setStatus(rs.getString("OR_TRIP_STATUS"));
				orBo.setEndQueueno(rs.getString("OR_END_QUEUENO"));
				orBo.setQueueno(rs.getString("OR_QUEUENO"));
				orBo.setRouteNumber(rs.getString("SRD_ROUTE_NO"));
				orBo.setPickupOrderSwitch(rs.getInt("SRD_ORDER_SWITCH"));
				orBo.setSlat(rs.getString("OR_STLATITUDE"));
				orBo.setSlong(rs.getString("OR_STLONGITUDE"));
				orBo.setDriverid(rs.getString("OR_DRIVERID"));
				orBo.setEdlatitude(rs.getString("DL_LATITUDE")!=null?rs.getString("DL_LATITUDE"):"");
				orBo.setEdlongitude(rs.getString("DL_LONGITUDE")!=null?rs.getString("DL_LONGITUDE"):"");
				al_list.add(orBo);
			}
		} catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.getSharedRideDetails-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.getSharedRideDetails-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}




	public static int allocateDriverForShared(String assoCode,String driverId,String routeNo,String cabNo,int status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query = "UPDATE TDS_OPENREQUEST SET OR_DRIVERID = '"+driverId+"' , OR_VEHICLE_NO='"+cabNo+"' , OR_TRIP_STATUS ='"+status+"',OR_SMS_SENT = now() WHERE OR_ASSOCCODE='"+assoCode+"' AND OR_ROUTE_NUMBER='"+routeNo+"'";
		String query1 = "UPDATE TDS_SHARED_RIDE SET SR_STATUS ='"+status+"', SR_DRIVERID ='"+driverId+"' , SR_VEHICLE_NO='"+cabNo+"' WHERE SR_ASSOCCODE='"+assoCode+"' AND SR_ROUTE_ID='"+routeNo+"'";
		try{
			pst=con.prepareStatement(query);
			result = pst.executeUpdate();
			pst = con.prepareStatement(query1);
			result = pst.executeUpdate();
		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.allocateDriverForShared-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.allocateDriverForShared-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}	
	public static int deletegroup(String assoCode,String routeNo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result = 0;

		String query = "UPDATE TDS_OPENREQUEST SET OR_ROUTE_NUMBER ='' , OR_DRIVERID='' , OR_VEHICLE_NO='' , OR_TRIP_STATUS='4' WHERE OR_ASSOCCODE='"+assoCode+"' AND OR_ROUTE_NUMBER="+routeNo;
		String query1 = "DELETE FROM  TDS_SHARED_RIDE  WHERE SR_ASSOCCODE="+assoCode+" AND SR_ROUTE_ID="+routeNo;
		String query2 = "DELETE FROM TDS_SHARED_RIDE_DETAILS  WHERE SRD_ASSOCCODE="+assoCode+" AND SRD_ROUTE_NO="+routeNo;
		try{
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			result = pst.executeUpdate();
			pst = con.prepareStatement(query1);
			cat.info(pst.toString());
			result = pst.executeUpdate();
			pst = con.prepareStatement(query2);
			cat.info(pst.toString());
			result = pst.executeUpdate();
		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.deleteGroup-->" + sqex.getMessage()));
			//System.out.println("TDSException SharedRideDAO.deleteGroup-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int updateSharedStatus(String assoCode,String routeNo, String status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String extraQuery = "";
		String query2="UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS='"+status+"' , OR_VEHICLE_NO='', OR_DRIVERID='', OR_DISPATCH_HISTORY_DRIVERS ='', OR_HISTORY_OF_VEHICLES='' WHERE OR_ASSOCCODE='"+assoCode+"'AND OR_ROUTE_NUMBER="+routeNo;
		if(status.equalsIgnoreCase("30")){
			extraQuery =" , SR_DRIVERID ='' , SR_VEHICLE_NO =''";
		}
		String query = "UPDATE  TDS_SHARED_RIDE SET SR_STATUS ='"+status+"'"+extraQuery+" WHERE SR_ROUTE_ID='"+routeNo+"' AND SR_ASSOCCODE ="+assoCode;
		try{
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			result = pst.executeUpdate();
			if(status.equalsIgnoreCase("30")){
				pst=con.prepareStatement(query2);
				cat.info(pst.toString());
				pst.executeUpdate();
			}
		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.updateSharedStatus-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.updateSharedStatus-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}	
	public static ArrayList<OpenRequestBO> getOpnReqDashForShared(String p_assoccode,int open, double timeOffset,int tripStatus,String date) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String queryAppend="";
		String queryForDate="";
		if(date!=null&&!date.equalsIgnoreCase("")){
			queryForDate = " AND DATE_FORMAT(DATE_SUB(OR_SERVICEDATE, INTERVAL -"+timeOffset+" HOUR),'%m/%d/%Y') = '"+date+"'";
		}else{
			queryForDate = " AND DATE(DATE_SUB(OR_SERVICEDATE, INTERVAL -"+timeOffset+" HOUR))=DATE(NOW())";
		}
		if(tripStatus==0){
			queryAppend = " AND OR_TRIP_STATUS != '"+TDSConstants.jobAllocated+ "'" +" AND OR_TRIP_STATUS != '"+TDSConstants.onRotueToPickup+ "'" +" AND OR_TRIP_STATUS != '"+TDSConstants.tripStarted+ "'" ;
		}
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT " +
					"  OR_TRIP_ID,OR_NAME, OR_STADD1,OR_EDADD1, OR_STCITY,OR_PHONE, OR_QUEUENO,OR_TRIP_STATUS,OR_DRCABFLAG, QD_DISPATCH_TYPE, DATE_SUB(OR_SERVICEDATE ,INTERVAL -"+timeOffset+" HOUR) AS OR_SERVICEDATE, DATE_FORMAT(OR_SERVICEDATE,'%m/%d/%Y') AS SERVICEDATE,QD_DISPATCH_DISTANCE,"+
					" case OR_DISPATCH_START_TIME  < NOW() WHEN TRUE THEN TIMESTAMPDIFF(MINUTE,OR_DISPATCH_START_TIME,NOW()) END AS PENDING ,OR_STLATITUDE, OR_STLONGITUDE, OR_DISPATCH_HISTORY_DRIVERS," +
					"  case QD_DELAYTIME IS NULL WHEN true THEN  (OR_DISPATCH_LEAD_TIME * 60) ELSE CASE OR_DISPATCH_LEAD_TIME = -1 when true THEN QD_DELAYTIME*60 else (OR_DISPATCH_LEAD_TIME * 60) END END  AS QD_DELAYTIME," + 
					" OR_SMS_SENT,QD_SMS_RES_TIME, OR_DRIVERID,OR_VEHICLE_NO,OR_DONT_DISPATCH,OR_SHARED_RIDE, OR_PAYTYPE, OR_ROUTE_NUMBER , " +
					" date_format(OR_SERVICEDATE,'%r') as st_time," +
					" time_to_sec(timediff(OR_SERVICEDATE,now())) as st_time_diff," +
					"time_to_sec(timediff(now(),OR_SMS_SENT)) as  SMS_TIME_DIFF,QD.QD_BROADCAST_DELAY,"+
					"date_format(DATE_SUB(OR_SERVICEDATE ,INTERVAL -"+timeOffset+" HOUR),'%H%i') as SERVICE_TIME, "+
					"  OR_TRIP_STATUS" +
					" FROM" +
					" TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD " +
					" ON  OR_QUEUENO = QD_QUEUENAME AND OR_ASSOCCODE = QD_ASSOCCODE" +
					" WHERE " +
					" OPR.OR_ASSOCCODE = ? " +
					" AND OR_TRIP_STATUS != '"+TDSConstants.newRequestDispatchProcessesNotStarted+ "'" +
					" AND OR_TRIP_STATUS != '"+TDSConstants.tripCompleted+ "'" +
					queryForDate+
					queryAppend +
					" ORDER BY OR_QUEUENO,OR_DRCABFLAG,OR_TRIP_STATUS,OR_SERVICEDATE");
			pst.setString(1, p_assoccode);
			rs = pst.executeQuery();
			while(rs.next()) {
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				openRequestBO.setScity(rs.getString("OR_STADD1"));
				openRequestBO.setPendingTime(rs.getString("PENDING")==null?"":rs.getString("PENDING"));
				openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setRequestTime(rs.getString("SERVICE_TIME"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setSdate(rs.getString("SERVICEDATE"));
				openRequestBO.setDontDispatch(rs.getInt("OR_DONT_DISPATCH"));
				openRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
				openRequestBO.setPaytype(rs.getString("OR_PAYTYPE"));
				openRequestBO.setEcity(rs.getString("OR_EDADD1"));
				openRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				al_list.add(openRequestBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException SharedRideDAO.getOpnReqDashForShared-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.getOpnReqDashForShared-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	public static ArrayList<OpenRequestBO> getSharedRideLogs(String assoCode,String routeNo,String fDate,String tDate,String timeOffset){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String queryAppend1 = "";
		String queryAppend2 = "";
		if(routeNo!=null&&routeNo!=""){
			queryAppend1 = " AND SRL_ROUTE_NO ="+routeNo;
		}
		if(fDate!=null&&tDate!=null&&fDate!=""&&tDate!=""){
			queryAppend2 = " AND DATE_FORMAT(CONVERT_TZ(SRL_CREATED_TIME,'UTC','"+timeOffset+"'),'%Y-%m-%d') BETWEEN '"+fDate+"' AND '"+tDate+"'";
		}
		String query="SELECT *, DATE(SRL_CREATED_TIME) AS DATE,DATE_FORMAT(SRL_CREATED_TIME,'%r') AS TIME FROM TDS_SHARED_RIDE_LOGS WHERE  SRL_ASSOCCODE='"+assoCode+"'"+queryAppend1+queryAppend2+" ORDER BY SRL_CREATED_TIME";
		try {
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestBO orBo = new OpenRequestBO();
				orBo.setRouteNumber(rs.getString("SRL_ROUTE_NO"));
				orBo.setCreatedBy(rs.getString("SRL_CREATED_BY"));
				orBo.setCreatedTime(rs.getString("TIME"));
				orBo.setCreatedDate(rs.getString("DATE"));
				if(rs.getString("SRL_STATUS").equalsIgnoreCase("30")){
					orBo.setShRideStatus("BroadCast");
				}else if(rs.getString("SRL_STATUS").equalsIgnoreCase("40")){
					orBo.setShRideStatus("Allocated");
				}else if(rs.getString("SRL_STATUS").equalsIgnoreCase("8")){
					orBo.setShRideStatus("Dispatch Not Started yet");
				}else if(rs.getString("SRL_STATUS").equalsIgnoreCase("61")){
					orBo.setShRideStatus("Completed");
				}else {
					orBo.setShRideStatus("UnKnown Status");
				}
				orBo.setSharedLogReason(rs.getString("SRL_REASON"));
				al_list.add(orBo);
			}
		} catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.getSharedRideGroups-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.getSharedRideGroups-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	public static int insertSharedLogEntries(AdminRegistrationBO adminBo,String routeNo,String Reason,String status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("INSERT INTO TDS_SHARED_RIDE_LOGS (SRL_CREATED_TIME,SRL_REASON,SRL_CREATED_BY,SRL_ASSOCCODE,SRL_STATUS,SRL_ROUTE_NO) VALUES (NOW(),?,?,?,?,?)");
			pst.setString(1, Reason);
			pst.setString(2, adminBo.getUid());
			pst.setString(3, adminBo.getAssociateCode());
			pst.setString(4, status);
			pst.setString(5, routeNo);
			cat.info(pst.toString());
			result = pst.executeUpdate();
		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.insertSharedLogEntries-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.insertSharedLogEntries-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}	
	public static ArrayList<OpenRequestBO> getOpenRequestForRouteByTripID(String tripId,AdminRegistrationBO adminBo,String byRoute) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1=null;
		ResultSet rs,rs1;
		String query="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		OpenRequestBO opRequestBO= null;
		String routeNo = "";
		try {
			if(byRoute.equalsIgnoreCase("1")){
				pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST, TDS_SHARED_RIDE_DETAILS WHERE OR_TRIP_ID = SRD_TRIP_ID AND SRD_ROUTE_NO=OR_ROUTE_NUMBER AND SRD_ASSOCCODE = OR_ASSOCCODE AND OR_ASSOCCODE =? AND OR_ROUTE_NUMBER = ? ORDER BY OR_DISPATCH_START_TIME");
				pst.setString(1,adminBo.getAssociateCode());
				pst.setString(2,tripId);
				rs=pst.executeQuery();
			}else{
				pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST, TDS_SHARED_RIDE_DETAILS WHERE OR_TRIP_ID = SRD_TRIP_ID AND SRD_ROUTE_NO=OR_ROUTE_NUMBER AND SRD_ASSOCCODE = OR_ASSOCCODE AND OR_ASSOCCODE =? AND OR_ROUTE_NUMBER = (SELECT OR_ROUTE_NUMBER FROM TDS_OPENREQUEST WHERE OR_TRIP_ID =? AND OR_ASSOCCODE =?) ORDER BY OR_DISPATCH_START_TIME");
				pst.setString(1,adminBo.getAssociateCode());
				pst.setString(2,tripId);
				pst.setString(3,adminBo.getAssociateCode());
				rs=pst.executeQuery();
			}
			while(rs.next()){
				opRequestBO = new OpenRequestBO();
				opRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				opRequestBO.setRouteNumber(rs.getString("SRD_ROUTE_NO"));
				opRequestBO.setPhone(rs.getString("OR_PHONE"));
				opRequestBO.setSadd1(rs.getString("OR_STADD1") != null?rs.getString("OR_STADD1"):"");
				opRequestBO.setSadd2(rs.getString("OR_STADD2") != null?rs.getString("OR_STADD2"):"");
				opRequestBO.setScity(rs.getString("OR_STCITY") != null?rs.getString("OR_STCITY"):"");
				opRequestBO.setSstate(rs.getString("OR_STSTATE")!= null?rs.getString("OR_STSTATE"):"");
				opRequestBO.setSzip(rs.getString("OR_STZIP")!= null?rs.getString("OR_STZIP"):"");
				opRequestBO.setEadd1(rs.getString("OR_EDADD1")!= null?rs.getString("OR_EDADD1"):"");
				opRequestBO.setEadd2(rs.getString("OR_EDADD2") != null?rs.getString("OR_EDADD2"):"");
				opRequestBO.setEcity(rs.getString("OR_EDCITY") != null?rs.getString("OR_EDCITY"):"");
				opRequestBO.setEstate(rs.getString("OR_EDSTATE") != null?rs.getString("OR_EDSTATE"):"");
				opRequestBO.setEzip(rs.getString("OR_EDZIP") != null?rs.getString("OR_EDZIP"):"");
				opRequestBO.setAssociateCode(rs.getString("OR_ASSOCCODE")!= null ? rs.getString("OR_ASSOCCODE"):"");
				opRequestBO.setDriverid(rs.getString("OR_DRIVERID")); 
				opRequestBO.setElandmark(rs.getString("OR_ELANDMARK")); 
				opRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				opRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				opRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				opRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				opRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				opRequestBO.setPaytype((rs.getString("OR_PAYTYPE")));
				opRequestBO.setAcct(rs.getString("OR_PAYACC"));
				opRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
				opRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				String reqTime = rs.getString("OR_SERVICETIME");
				opRequestBO.setShrs(reqTime.substring(0,2));
				opRequestBO.setSmin(reqTime.substring(2,4));
				opRequestBO.setIsBeandata(1);
				opRequestBO.setName(rs.getString("OR_NAME"));
				opRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				opRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				opRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				opRequestBO.setPaymentMeter(rs.getInt("OR_METER_TYPE"));
				opRequestBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
				opRequestBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
				opRequestBO.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
				opRequestBO.setAirTo(rs.getString("OR_AIRLINE_TO"));
				
				if(rs.getInt("OR_METER_TYPE")==0){
					query = " AND MD_DEFAULT=1";
				} else {
					query = " AND MD_KEY='"+rs.getInt("OR_METER_TYPE")+"'";
				}
				pst1=con.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+adminBo.getMasterAssociateCode()+"'"+query);
				rs1=pst1.executeQuery();
				if(rs1.next()){
					opRequestBO.setRatePerMile(Double.parseDouble(rs1.getString("MD_RATE_PER_MILE")));
					opRequestBO.setRatePerMin(Double.parseDouble(rs1.getString("MD_RATE_PER_MIN")));
					opRequestBO.setStartAmt(Double.parseDouble(rs1.getString("MD_START_AMOUNT")));
					opRequestBO.setMinSpeed(Integer.parseInt(rs1.getString("MD_MINIMUM_SPEED")));
				}

				al_list.add(opRequestBO);
			}
			cat.info("Query in open RequestHistory "+pst);
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException SharedRideDAO.getOpenRequestByRoute-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.getOpenRequestByRoute-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	public static ArrayList<OpenRequestBO> getOpenRequestTrulyByRoute(String routeNo,AdminRegistrationBO adminBo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ArrayList<OpenRequestBO> jobInARouteForDriver = new ArrayList<OpenRequestBO>();

		try {
			pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_TRIP_STATUS,OR_AMT,OR_TRIP_STATUS,OR_QUEUENO,OR_DRCABFLAG,OR_SMS_SENT,OR_DRIVERID,OR_DISPATCH_HISTORY_DRIVERS,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_SPLINS,OR_PAYACC,OR_PAYTYPE,OR_ROUTE_NUMBER,"+
					"OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_SHARED_RIDE,OR_ELANDMARK,OR_EDSTATE,OR_NAME,OR_EDZIP,OR_REPEAT_GROUP,OR_PHONE,OR_STLATITUDE,OR_COMMENTS,OR_STLONGITUDE,OR_EDLATITUDE,OR_DRIVERID,OR_VEHICLE_NO,OR_EDLONGITUDE,OR_LANDMARK,OR_DRCABFLAG,OR_QUEUENO," +	"DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%H%i') "+"as TIME, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"') AS OFFSET_DATETIME FROM TDS_OPENREQUEST WHERE OR_ROUTE_NUMBER=? AND OR_ASSOCCODE=? AND OR_DRIVERID =?");
			pst.setString(1,routeNo);
			pst.setString(2,adminBo.getAssociateCode());
			pst.setString(3,adminBo.getUid());
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestBO opRequestBO = new OpenRequestBO();
				opRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				opRequestBO.setPhone(rs.getString("OR_PHONE"));
				opRequestBO.setName(rs.getString("OR_NAME"));
				opRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("DATE")));
				opRequestBO.setShrs((rs.getString("TIME")));
				opRequestBO.setPaytype((rs.getString("OR_PAYTYPE")));
				opRequestBO.setAcct(rs.getString("OR_PAYACC"));
				opRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				opRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				opRequestBO.setComments(rs.getString("OR_COMMENTS"));
				opRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				opRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				opRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				opRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				opRequestBO.setStartTimeStamp(rs.getString("OFFSET_DATETIME"));
				opRequestBO.setChckTripStatus(rs.getInt("OR_TRIP_STATUS"));
				opRequestBO.setOR_SMS_SENT(rs.getString("OR_SMS_SENT"));
				opRequestBO.setHistoryOfDrivers(rs.getString("OR_DISPATCH_HISTORY_DRIVERS"));
				opRequestBO.setSadd1(rs.getString("OR_STADD1"));
				opRequestBO.setSadd2(rs.getString("OR_STADD2"));
				opRequestBO.setScity(rs.getString("OR_STCITY"));
				opRequestBO.setSstate(rs.getString("OR_STSTATE"));
				opRequestBO.setSzip(rs.getString("OR_STZIP"));
				opRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				opRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				opRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				opRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				opRequestBO.setEcity(rs.getString("OR_EDCITY"));
				opRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				opRequestBO.setEzip(rs.getString("OR_EDZIP"));
				opRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				opRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				opRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
				opRequestBO.setRepeatGroup(rs.getString("OR_REPEAT_GROUP"));
				opRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
				opRequestBO.setElandmark(rs.getString("OR_ELANDMARK"));
				opRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				opRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				jobInARouteForDriver.add(opRequestBO);

			} 
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.getOpenRequestStatus-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getOpenRequestStatus-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return jobInARouteForDriver;
	}

	public static int checkEndTrip(String assoCode,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int total=0;
		TDSConnection dbcon = null;
		Connection con = null;
		ResultSet rs =null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String routeNo ="";
		try{
			pst=con.prepareStatement("SELECT OR_ROUTE_NUMBER FROM  TDS_OPENREQUEST WHERE OR_TRIP_ID ='"+tripId+"' AND OR_ROUTE_NUMBER!=''  AND OR_ASSOCCODE="+assoCode);
			cat.info(pst.toString());
			rs= pst.executeQuery();
			if(rs.next()){
				routeNo = rs.getString("OR_ROUTE_NUMBER");
				pst=con.prepareStatement("SELECT COUNT(*) AS COUNT FROM TDS_OPENREQUEST WHERE OR_ROUTE_NUMBER= '"+routeNo+"' AND OR_ASSOCCODE="+assoCode);
				cat.info(pst.toString());
				rs= pst.executeQuery();
				if(rs.next()){
					total = rs.getInt("COUNT");
					total = total-1;
				}
			}
		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.insertSharedLogEntries-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.insertSharedLogEntries-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return total;
	}
	public static int createIndividualTrips(String[] trips, String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int result = 0;
		String tripIdList ="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		if(trips.length >0){
			tripIdList = " AND OR_TRIP_ID IN(";
			for(int j=0; j<trips.length-1;j++){
				tripIdList = tripIdList + "'" + trips[j] + "',";
			}
			tripIdList = tripIdList + "'" + trips[trips.length-1] + "')";
		}
		String query1 = "UPDATE TDS_OPENREQUEST SET OR_SHARED_RIDE=? WHERE OR_ASSOCCODE=?"+tripIdList;
		try{
			pst=con.prepareStatement(query1);
			pst.setString(1, "0");
			pst.setString(2, assoCode);
			cat.info(pst.toString());
			result = pst.executeUpdate();
		}catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.createIndividualTrips-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.createIndividualTrips-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static ArrayList<OpenRequestBO> getSharedLogs(String assoCode,String routeNo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query ="SELECT * FROM TDS_OPENREQUEST_HISTORY AS A,TDS_OPENREQUEST_LOGS AS B WHERE B.ORL_TRIPID =A.ORH_TRIP_ID AND B.ORL_ASSOCCODE ='"+assoCode+"' AND A.ORH_ROUTE_NUMBER ='"+routeNo+"' ORDER BY B.ORL_TIME";
		try {

			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestBO orBo = new OpenRequestBO();
				orBo.setTripid(rs.getString("ORH_TRIP_ID"));
				orBo.setCreatedBy(rs.getString("ORL_CHANGED_BY"));
				orBo.setCreatedTime(rs.getString("ORL_TIME"));
				orBo.setStatus(rs.getString("ORL_REASON"));
				al_list.add(orBo);
			}
		} catch (Exception sqex) {
			cat.error(("TDSException SharedRideDAO.getSharedLogs-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.getSharedLogs-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	/*public static int splitTripFromGroup(String assocode,String tripid,String routeNo,String source){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs =null;
		int dropOffOrder = 0;
		int pickupOrder =0;
		int resultValue=0;
		String query = "UPDATE TDS_OPENREQUEST SET OR_DRIVERID='',OR_VEHICLE_NO='',OR_ROUTE_NUMBER ='',OR_TRIP_STATUS ='4' WHERE OR_TRIP_ID ='"+tripid+"' AND OR_ASSOCCODE ='"+assocode+"'";
		String query1 = "DELETE FROM TDS_SHARED_RIDE_DETAILS WHERE SRD_TRIP_ID ='"+tripid+"' AND SRD_ASSOCCODE ='"+assocode+"'";
		String queryFirst ="SELECT SRD_PICKUP_ORDER FROM TDS_SHARED_RIDE_DETAILS where SRD_TRIP_ID ='"+tripid+"' AND SRD_ASSOCCODE ='"+assocode+"' AND SRD_ORDER_SWITCH ='1'";
		String querySecond ="SELECT SRD_PICKUP_ORDER FROM TDS_SHARED_RIDE_DETAILS where SRD_TRIP_ID ='"+tripid+"' AND SRD_ASSOCCODE ='"+assocode+"' AND SRD_ORDER_SWITCH ='0'";
		String query4 ="UPDATE TDS_SHARED_RIDE SET SR_TOTAL_TRIPS=(SR_TOTAL_TRIPS-1) WHERE SR_ASSOCCODE ='"+assocode+"' AND SR_ROUTE_ID ='"+routeNo+"'";
		try {
			m_pst = m_conn.prepareStatement(queryFirst);
			rs = m_pst.executeQuery();
			if(rs.next()){
				dropOffOrder = rs.getInt("SRD_PICKUP_ORDER");
			}
			m_pst = m_conn.prepareStatement(querySecond);
			rs = m_pst.executeQuery();
			if(rs.next()){
				pickupOrder = rs.getInt("SRD_PICKUP_ORDER");
			}
			String query2 = "UPDATE TDS_SHARED_RIDE_DETAILS SET SRD_PICKUP_ORDER = (CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER)-1) WHERE SRD_ROUTE_NO ='"+routeNo+"' AND SRD_PICKUP_ORDER > '"+dropOffOrder+"'  AND SRD_ASSOCCODE ='"+assocode+"'";
			String query3 = "UPDATE TDS_SHARED_RIDE_DETAILS SET SRD_PICKUP_ORDER = (CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER)-1) WHERE SRD_ROUTE_NO ='"+routeNo+"' AND SRD_PICKUP_ORDER > '"+pickupOrder+"'  AND SRD_ASSOCCODE ='"+assocode+"'";
			if(source.equalsIgnoreCase("SH")){
				m_pst = m_conn.prepareStatement(query);
				resultValue = m_pst.executeUpdate();
			}
			m_pst = m_conn.prepareStatement(query1);
			resultValue = m_pst.executeUpdate();

			if(dropOffOrder>0){
				m_pst = m_conn.prepareStatement(query2);
				resultValue = m_pst.executeUpdate();
			}
			if(pickupOrder>0){
				m_pst = m_conn.prepareStatement(query3);
				resultValue = m_pst.executeUpdate();
			}
			m_pst = m_conn.prepareStatement(query4);
			resultValue = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error(("TDSException SharedRideDAO.splitTripFromGroup-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			System.out.println("TDSException SharedRideDAO.splitTripFromGroup-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return resultValue;
	}
*/
	
		public static int splitTripFromGroup(String assocode,String trip,String routeNo,String source){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs =null;
		int dropOffOrder = 0;
		int pickupOrder =0;
		int resultValue=0;
		String query = "UPDATE TDS_OPENREQUEST SET OR_DRIVERID='',OR_VEHICLE_NO='',OR_ROUTE_NUMBER ='',OR_TRIP_STATUS ='4' WHERE OR_TRIP_ID =? AND OR_ASSOCCODE ='"+assocode+"'";
		String query1 = "DELETE FROM TDS_SHARED_RIDE_DETAILS WHERE SRD_TRIP_ID =? AND SRD_ASSOCCODE ='"+assocode+"'";
		String queryFirst ="SELECT SRD_PICKUP_ORDER FROM TDS_SHARED_RIDE_DETAILS where SRD_TRIP_ID =? AND SRD_ASSOCCODE ='"+assocode+"' AND SRD_ORDER_SWITCH ='1'";
		String querySecond ="SELECT SRD_PICKUP_ORDER FROM TDS_SHARED_RIDE_DETAILS where SRD_TRIP_ID =? AND SRD_ASSOCCODE ='"+assocode+"' AND SRD_ORDER_SWITCH ='0'";
		String query4 ="UPDATE TDS_SHARED_RIDE SET SR_TOTAL_TRIPS=(SR_TOTAL_TRIPS-1) WHERE SR_ASSOCCODE ='"+assocode+"' AND SR_ROUTE_ID ='"+routeNo+"'";
		try {
			String tripid[]=trip.split(";");
			
			for(int i=0;i<tripid.length;i++){
				String tripId=tripid[i];

			m_pst = m_conn.prepareStatement(queryFirst);
			m_pst.setString(1, tripId);
			rs = m_pst.executeQuery();
			if(rs.next()){
				dropOffOrder = rs.getInt("SRD_PICKUP_ORDER");
			}
			m_pst = m_conn.prepareStatement(querySecond);
			m_pst.setString(1, tripId);
			rs = m_pst.executeQuery();
			if(rs.next()){
				pickupOrder = rs.getInt("SRD_PICKUP_ORDER");
			}
			String query2 = "UPDATE TDS_SHARED_RIDE_DETAILS SET SRD_PICKUP_ORDER = (CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER)-1) WHERE SRD_ROUTE_NO ='"+routeNo+"' AND SRD_PICKUP_ORDER > '"+dropOffOrder+"'  AND SRD_ASSOCCODE ='"+assocode+"'";
			String query3 = "UPDATE TDS_SHARED_RIDE_DETAILS SET SRD_PICKUP_ORDER = (CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER)-1) WHERE SRD_ROUTE_NO ='"+routeNo+"' AND SRD_PICKUP_ORDER > '"+pickupOrder+"'  AND SRD_ASSOCCODE ='"+assocode+"'";
			if(source.equalsIgnoreCase("SH")){
				m_pst = m_conn.prepareStatement(query);
				m_pst.setString(1, tripId);
				resultValue = m_pst.executeUpdate();
			}
			m_pst = m_conn.prepareStatement(query1);
			m_pst.setString(1, tripId);
			resultValue = m_pst.executeUpdate();

			if(dropOffOrder>0){
				m_pst = m_conn.prepareStatement(query2);
				resultValue = m_pst.executeUpdate();
			}
			if(pickupOrder>0){
				m_pst = m_conn.prepareStatement(query3);
				resultValue = m_pst.executeUpdate();
			}
			m_pst = m_conn.prepareStatement(query4);
			resultValue = m_pst.executeUpdate();
		} }catch (SQLException sqex) {
			cat.error(("TDSException SharedRideDAO.splitTripFromGroup-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException SharedRideDAO.splitTripFromGroup-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return resultValue;
	}

	public static int addTripToGroup(String assocode,String tripid,String routeNo,int pickupOrder,int dropOffOrder){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement m_pst = null;
		ResultSet rs=null;
		int resultValue = 0,maxRoute=0;
		TDSConnection m_tdsConn = new TDSConnection();
		Connection m_conn = m_tdsConn.getConnection();
		try {
			m_pst=m_conn.prepareStatement("SELECT MAX(SRD_PICKUP_ORDER) AS ROUTEORDER FROM TDS_SHARED_RIDE_DETAILS WHERE SRD_ROUTE_NO='"+routeNo+"'");
			rs=m_pst.executeQuery();
			if(rs.next()){
				maxRoute = rs.getInt("ROUTEORDER");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(maxRoute!=0 && (pickupOrder>(maxRoute+1)||dropOffOrder>(maxRoute+2))){
			return resultValue;
		}
		String query =  "UPDATE TDS_OPENREQUEST SET OR_SHARED_RIDE='1',OR_ROUTE_NUMBER ='"+routeNo+"',OR_TRIP_STATUS=?,OR_DRIVERID=?,OR_VEHICLE_NO=? WHERE OR_TRIP_ID ='"+tripid+"' AND OR_ASSOCCODE ='"+assocode+"'";
		String query1 = "INSERT INTO TDS_SHARED_RIDE_DETAILS(SRD_TRIP_ID,SRD_ASSOCCODE,SRD_ROUTE_NO,SRD_PICKUP_ORDER,SRD_ORDER_SWITCH,SRD_PAY_TYPE) VALUES(?,?,?,?,?,?)";
		String query2 = "UPDATE TDS_SHARED_RIDE_DETAILS SET SRD_PICKUP_ORDER = (CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER)+1) WHERE SRD_ROUTE_NO ='"+routeNo+"' AND SRD_PICKUP_ORDER >= '"+dropOffOrder+"'  AND SRD_ASSOCCODE ='"+assocode+"'";
		String query3 = "UPDATE TDS_SHARED_RIDE_DETAILS SET SRD_PICKUP_ORDER = (CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER)+1) WHERE SRD_ROUTE_NO ='"+routeNo+"' AND SRD_PICKUP_ORDER >= '"+pickupOrder+"'  AND SRD_ASSOCCODE ='"+assocode+"'";
		String query4 = "UPDATE TDS_SHARED_RIDE SET SR_TOTAL_TRIPS=(SR_TOTAL_TRIPS+1) WHERE SR_ASSOCCODE ='"+assocode+"' AND SR_ROUTE_ID ='"+routeNo+"'";
		String query5 = "SELECT SR_DRIVERID,SR_STATUS,SR_VEHICLE_NO FROM TDS_SHARED_RIDE WHERE SR_ROUTE_ID='"+routeNo+"' AND SR_ASSOCCODE='"+assocode+"'";
		try {
			m_pst=m_conn.prepareStatement(query5);
			rs=m_pst.executeQuery();
			if(rs.next()){
				if(dropOffOrder>0){
					m_pst = m_conn.prepareStatement(query2);
					resultValue = m_pst.executeUpdate();

					m_pst = m_conn.prepareStatement(query1);
					m_pst.setString(1, tripid);
					m_pst.setString(2, assocode);
					m_pst.setString(3, routeNo);
					m_pst.setInt(4,dropOffOrder);
					m_pst.setString(5, "1");
					m_pst.setString(6, "0");
					resultValue=m_pst.executeUpdate();

					m_pst = m_conn.prepareStatement(query4);
					resultValue = m_pst.executeUpdate();
				}
				if(pickupOrder>0){
					m_pst = m_conn.prepareStatement(query3);
					resultValue = m_pst.executeUpdate();

					m_pst = m_conn.prepareStatement(query1);
					m_pst.setString(1, tripid);
					m_pst.setString(2, assocode);
					m_pst.setString(3, routeNo);
					m_pst.setInt(4,pickupOrder);
					m_pst.setString(5, "0");
					m_pst.setString(6, "0");
					resultValue=m_pst.executeUpdate();

					m_pst = m_conn.prepareStatement(query4);
					resultValue = m_pst.executeUpdate();
				}
				if(pickupOrder==0 && dropOffOrder==0){
					m_pst = m_conn.prepareStatement(query1);
					m_pst.setString(1, tripid);
					m_pst.setString(2, assocode);
					m_pst.setString(3, routeNo);
					m_pst.setInt(4,pickupOrder);
					m_pst.setString(5, "99");
					m_pst.setString(6, "0");
					resultValue=m_pst.executeUpdate();

					m_pst = m_conn.prepareStatement(query4);
					resultValue = m_pst.executeUpdate();
				}
				m_pst = m_conn.prepareStatement(query);
				m_pst.setString(1, rs.getString("SR_STATUS"));
				m_pst.setString(2, rs.getString("SR_DRIVERID"));
				m_pst.setString(3, rs.getString("SR_VEHICLE_NO"));
				resultValue = m_pst.executeUpdate();
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException SharedRideDAO.addTripToGroup-->" + sqex.getMessage()));
			//System.out.println("TDSException SharedRideDAO.addTripToGroup-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return resultValue;
	}


	public static int splitTripFromGroupForIndividual(String assocode,String[] trips,String routeNo,String source){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs =null;
		int dropOffOrder = 0;
		int pickupOrder =0;
		int resultValue=0;

		String tripIdList,tripIdListSh ="";
		tripIdList = "  OR_TRIP_ID IN (";
		tripIdListSh = "  SRD_TRIP_ID IN (";
		for(int j=0; j<trips.length;j++){
			tripIdList = tripIdList + "'" + trips[j] + "',";
			tripIdListSh = tripIdListSh + "'" + trips[j] + "',";

		}
		tripIdList = tripIdList + "'" + trips[(trips.length-1)] + "')";
		tripIdListSh = tripIdListSh + "'" + trips[(trips.length-1)] + "')";

		String query = "UPDATE TDS_OPENREQUEST SET OR_ROUTE_NUMBER ='' WHERE "+tripIdList+" AND OR_ASSOCCODE ='"+assocode+"'";
		String query1 = "DELETE FROM TDS_SHARED_RIDE_DETAILS WHERE "+tripIdListSh+" AND SRD_ASSOCCODE ='"+assocode+"'";
		String queryFirst ="SELECT SRD_PICKUP_ORDER FROM TDS_SHARED_RIDE_DETAILS where "+tripIdListSh+" AND SRD_ASSOCCODE ='"+assocode+"' AND SRD_ORDER_SWITCH ='1'";
		String querySecond ="SELECT SRD_PICKUP_ORDER FROM TDS_SHARED_RIDE_DETAILS where "+tripIdListSh+" AND SRD_ASSOCCODE ='"+assocode+"' AND SRD_ORDER_SWITCH ='0'";
		String query4 ="UPDATE TDS_SHARED_RIDE SET SR_TOTAL_TRIPS=(SR_TOTAL_TRIPS-1) WHERE SR_ASSOCCODE ='"+assocode+"' AND SR_ROUTE_ID ='"+routeNo+"'";
		try {
			m_pst = m_conn.prepareStatement(queryFirst);
			rs = m_pst.executeQuery();
			if(rs.next()){
				dropOffOrder = rs.getInt("SRD_PICKUP_ORDER");
			}
			m_pst = m_conn.prepareStatement(querySecond);

			rs = m_pst.executeQuery();
			if(rs.next()){
				pickupOrder = rs.getInt("SRD_PICKUP_ORDER");
			}
			String query2 = "UPDATE TDS_SHARED_RIDE_DETAILS SET SRD_PICKUP_ORDER = (CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER)-1) WHERE SRD_ROUTE_NO ='"+routeNo+"' AND SRD_PICKUP_ORDER > '"+dropOffOrder+"'  AND SRD_ASSOCCODE ='"+assocode+"'";
			String query3 = "UPDATE TDS_SHARED_RIDE_DETAILS SET SRD_PICKUP_ORDER = (CONVERT(SRD_PICKUP_ORDER ,UNSIGNED INTEGER)-1) WHERE SRD_ROUTE_NO ='"+routeNo+"' AND SRD_PICKUP_ORDER > '"+pickupOrder+"'  AND SRD_ASSOCCODE ='"+assocode+"'";
			if(source.equalsIgnoreCase("SH")){
				m_pst = m_conn.prepareStatement(query);

				resultValue = m_pst.executeUpdate();
			}
			m_pst = m_conn.prepareStatement(query1);

			resultValue = m_pst.executeUpdate();

			if(dropOffOrder>0){
				m_pst = m_conn.prepareStatement(query2);
				resultValue = m_pst.executeUpdate();
			}
			if(pickupOrder>0){
				m_pst = m_conn.prepareStatement(query3);
				resultValue = m_pst.executeUpdate();
			}
			m_pst = m_conn.prepareStatement(query4);
			resultValue = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error(("TDSException SharedRideDAO.splitTripFromGroupForIndividual-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException SharedRideDAO.splitTripFromGroupForIndividual-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return resultValue;
	}

	
	
	public static ArrayList<OpenRequestBO> getSharedRide(AdminRegistrationBO adminBo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		OpenRequestBO opRequestBO= null;
		
		try {
				pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST, TDS_SHARED_RIDE_DETAILS WHERE  SRD_ROUTE_NO=OR_ROUTE_NUMBER AND SRD_ASSOCCODE = OR_ASSOCCODE AND OR_ASSOCCODE =?  ORDER BY SRD_PICKUP_ORDER");
				pst.setString(1,adminBo.getAssociateCode());
		        rs=pst.executeQuery();
			
			while(rs.next()){
				opRequestBO = new OpenRequestBO();
				opRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				opRequestBO.setRouteNumber(rs.getString("SRD_ROUTE_NO"));
				opRequestBO.setSadd1(rs.getString("OR_STADD1"));
				opRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				opRequestBO.setPickupOrder(rs.getString("SRD_PICKUP_ORDER"));
				opRequestBO.setDropOffOrder(rs.getString("SRD_DROPOFF_ORDER"));
				al_list.add(opRequestBO);
			}
			cat.info("Query in  sharedRideDetails "+pst);
			
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException SharedRideDAO.getSharedRide-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException SharedRideDAO.getSharedRide-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	
}
