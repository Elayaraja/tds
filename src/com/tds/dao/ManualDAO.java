package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.ManualSettleBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;

public class ManualDAO {	

	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(ManualDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ManualDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	
	public static int moveToPaymentSetteled(String assocCode, String driverID, ArrayList  transIDs)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO ManualDAO.moveToPaymentSetteled");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst_approv = null,m_pst_pay = null,m_pst_pay_summ = null;
		PreparedStatement m_pst_del=null,m_pst_cmp=null, m_pst_cmp_up = null,m_pst_log=null;
		ResultSet m_rs_approv = null,rs_cmp=null;
		String prev_cc,prev_dri,cc_cc,c_dri,pre_rec_set;
		int status=0;
		String queryTransID = "";
		for (int i=0;i<transIDs.size() -1 ; i++){
			queryTransID = queryTransID + "'" + (String) transIDs.get(i)+ "'" +",";
		}
		queryTransID = queryTransID + "'" + (String) transIDs.get(transIDs.size() -1) + "'";
		try {


			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			int key =0 , counter=0,no_of_rec=0,no_of_accp=0,no_of_dec=0;
			double AMOUNT =0,TIP_AMOUNT=0,TOTAL_AMOUNT=0,TXN_PERCENT_AMT=0,TXN_AMT=0,TOTAL_AMOUNT_WITH_TXN=0;
			m_conn.setAutoCommit(false);


			prev_cc="";prev_dri="";cc_cc="";c_dri="";pre_rec_set="";
			counter = 0;

			status=1;

			m_pst_approv = m_conn.prepareStatement("select * from TDS_APPROVAL where AP_TRANS_ID IN (" + queryTransID + ")");
			m_rs_approv = m_pst_approv.executeQuery();
			m_pst_pay = m_conn.prepareStatement("insert into TDS_PAYMENT_SETTLED_DETAIL (PSD_SET_ID,PSD_DRIVER_ID,PSD_COMPANY_ID,PSD_TRIP_ID,PSD_CARD_ISSUER,PSD_CARD_TYPE,PSD_SWIPETYPE," +
					"PSD_CARD_NO,PSD_AMOUNT,PSD_TIP_AMOUNT,PSD_TOTAL_AMOUNT,PSD_TRANS_ID,PSD_PROCESSING_DATE_TIME," +
					"PSD_APPROVAL_STATUS,PSD_APPROVALCODE  ,PSD_TXN_PERCENT_AMT,PSD_TXN_AMT,PSD_TOTAL_AMOUNT_WITH_TXN,PSD_RIDER_SIGNATURE,PSD_REC_SET,PSD_CAPTUREID,PSD_CAPTUREDATE,PSD_AUTHCAPFLG, " +
					"PSD_VOID_FLG,PSD_VOID_DATE,PSD_VOIDED_FLG,PSD_STATUSCODE)" +
			"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");


			m_pst_pay_summ = m_conn.prepareStatement("insert into TDS_PAYMENT_SETTLED_SUMMARY(PSS_SET_ID,PSS_DRIVER_ID,PSS_COMPANY_ID,PSS_PROCESSING_DATE_TIME,PSS_AMOUNT,PSS_TIP_AMOUNT,PSS_TOTAL_AMOUNT," +
			"PSS_TXN_PERCENT_AMT,PSS_TXN_AMT,PSS_TOTAL_AMOUNT_WITH_TXN,PSS_NO_OF_REC,PSS_REPO_GEN_FLG,PSS_NO_OF_A,PSS_NO_OF_DEC,PSS_REC_SET) values(?,?,?,now(),?,?,?,?,?,?,?,?,?,?,?)");

			key  =  ConfigDAO.tdsKeyGen("TDS_PAYMENT_SETTELED_SEQ", m_conn);

			while(m_rs_approv.next())
			{
				m_pst_pay.setInt(1, key);
				m_pst_pay.setString(2, m_rs_approv.getString("AP_DRIVER_ID"));
				m_pst_pay.setString(3, m_rs_approv.getString("AP_COMPANY_ID"));
				m_pst_pay.setString(4, m_rs_approv.getString("AP_TRIP_ID"));
				m_pst_pay.setString(5, m_rs_approv.getString("AP_CARD_ISSUER"));
				m_pst_pay.setString(6, m_rs_approv.getString("AP_CARD_TYPE"));
				m_pst_pay.setString(7, m_rs_approv.getString("AP_SWIPETYPE"));
				m_pst_pay.setString(8, m_rs_approv.getString("AP_CARD_NO"));
				m_pst_pay.setString(9, m_rs_approv.getString("AP_AMOUNT"));
				m_pst_pay.setString(10, m_rs_approv.getString("AP_TIP_AMOUNT"));
				m_pst_pay.setString(11, m_rs_approv.getString("AP_TOTAL_AMOUNT"));
				m_pst_pay.setString(12, m_rs_approv.getString("AP_TRANS_ID"));
				m_pst_pay.setString(13, m_rs_approv.getString("AP_PROCESSING_DATE_TIME"));
				m_pst_pay.setString(14, m_rs_approv.getString("AP_APPROVAL_STATUS"));
				m_pst_pay.setString(15, m_rs_approv.getString("AP_APPROVALCODE"));
				m_pst_pay.setString(16, m_rs_approv.getString("AP_TXN_PERCENT_AMT"));
				m_pst_pay.setString(17, m_rs_approv.getString("AP_TXN_AMT"));
				m_pst_pay.setString(18, m_rs_approv.getString("AP_TOTAL_AMOUNT_WITH_TXN"));
				m_pst_pay.setBlob(19, m_rs_approv.getBlob("AP_RIDER_SIGNATURE"));
				m_pst_pay.setString(20, m_rs_approv.getString("AP_REC_SET"));
				m_pst_pay.setString(21, m_rs_approv.getString("AP_CAPTUREID"));
				m_pst_pay.setString(22, m_rs_approv.getString("AP_CAPTUREDATE"));
				m_pst_pay.setString(23, m_rs_approv.getString("AP_AUTHCAPFLG"));
				m_pst_pay.setString(24, m_rs_approv.getString("AP_VOID_FLG"));
				m_pst_pay.setString(25, m_rs_approv.getString("AP_VOID_DATE"));
				m_pst_pay.setString(26, m_rs_approv.getString("AP_VOIDED_FLG"));
				m_pst_pay.setString(27, m_rs_approv.getString("AP_STATUSCODE"));
				cat.info(m_pst_pay.toString());
				m_pst_pay.execute();

				if(m_rs_approv.getInt("AP_APPROVAL_STATUS") == 1)
				{
					AMOUNT = AMOUNT  + m_rs_approv.getDouble("AP_AMOUNT");
					TIP_AMOUNT = TIP_AMOUNT + m_rs_approv.getDouble("AP_TIP_AMOUNT");
					TOTAL_AMOUNT = TOTAL_AMOUNT + m_rs_approv.getDouble("AP_TOTAL_AMOUNT");
					no_of_accp = no_of_accp + 1;
				} else {
					no_of_dec = no_of_dec + 1; 
				}

				TXN_AMT = TXN_AMT + m_rs_approv.getDouble("AP_TXN_AMT");
				TXN_PERCENT_AMT = TXN_PERCENT_AMT +  m_rs_approv.getDouble("AP_TXN_PERCENT_AMT");
				TOTAL_AMOUNT_WITH_TXN = TOTAL_AMOUNT_WITH_TXN + m_rs_approv.getDouble("AP_TOTAL_AMOUNT_WITH_TXN");

				no_of_rec = no_of_rec+1;

				counter =1;

				prev_cc=m_rs_approv.getString("AP_COMPANY_ID");
				prev_dri=m_rs_approv.getString("AP_DRIVER_ID");
				pre_rec_set = m_rs_approv.getString("AP_REC_SET");

				m_pst_del = m_conn.prepareStatement("delete from TDS_APPROVAL where AP_TRANS_ID = '" + m_rs_approv.getString("AP_TRANS_ID") + "'");
				m_pst_del.execute();
			}
			m_pst_pay_summ.setInt(1, key);
			m_pst_pay_summ.setString(2,prev_dri);
			m_pst_pay_summ.setString(3,prev_cc);
			m_pst_pay_summ.setString(4,""+AMOUNT);
			m_pst_pay_summ.setString(5,""+TIP_AMOUNT);
			m_pst_pay_summ.setString(6,""+TOTAL_AMOUNT);
			m_pst_pay_summ.setString(7,""+TXN_PERCENT_AMT);
			m_pst_pay_summ.setString(8,""+TXN_AMT);
			m_pst_pay_summ.setString(9,""+TOTAL_AMOUNT_WITH_TXN);
			m_pst_pay_summ.setString(10,""+no_of_rec);
			m_pst_pay_summ.setString(11,"1");
			m_pst_pay_summ.setString(12,""+no_of_accp);
			m_pst_pay_summ.setString(13,""+no_of_dec);
			m_pst_pay_summ.setString(14,pre_rec_set);
			cat.info(m_pst_pay.toString());

			m_pst_pay_summ.execute();	


			m_conn.commit();
			m_conn.setAutoCommit(true);

		} catch (SQLException sqex) {
			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			cat.error("TDSException ManualDAO.moveToPaymentSetteled-->"+sqex.getMessage());
			cat.error(m_pst_cmp.toString());
			//System.out.println("TDSException ManualDAO.moveToPaymentSetteled" + sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));


		return status;
	}



	public static int moveToDriverPenality(String assocCode, String driverID,ArrayList  transIDs)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO ManualDAO.moveToDriverPenality");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst_summ = null,m_pst_pay = null,m_pst_pay_summ = null;
		PreparedStatement m_pst_del=null,m_pst_cmp=null, m_pst_cmp_up = null,m_pst_log=null;
		ResultSet m_rs_summ = null,rs_cmp=null;
		String prev_cc,prev_dri,c_dri;
		int key =0 , counter=0,no_of_rec=0;
		double AMOUNT =0;
		int status=0;
		String queryTransID = "";
		for (int i=0;i<transIDs.size() -1; i++){
			queryTransID = queryTransID + "'" +(String) transIDs.get(i)+ "'" + ",";
		}
		queryTransID = queryTransID + "'" + (String) transIDs.get(transIDs.size() -1)+ "'" ;

		try {

			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_conn.setAutoCommit(false);

			prev_cc="";prev_dri="";c_dri="";

			status=1;

			m_pst_summ = m_conn.prepareStatement("select * from TDS_DRIVER_PENALITY where DP_ASSOCCODE  = '"+ assocCode +"' and DP_TRANS_ID in ("+ queryTransID +")");
			m_rs_summ = m_pst_summ.executeQuery();

			m_pst_pay = m_conn.prepareStatement("insert into TDS_DRIVER_PENALITY_SETELED_DETAIL (DPD_SET_ID,DPD_TRAN_ID,DPD_DRIVER_ID,DPD_COMPANY_ID,DPD_DESC,DPD_AMOUNT,DPD_PENALITY_DATE,DPD_PROCESSING_DATE)" +
			"values(?,?,?,?,?,?,?,now())");


			m_pst_pay_summ = m_conn.prepareStatement("insert into TDS_DRIVER_PENALITY_SETELED_SUMMARY(DPS_SET_ID,DPS_DRIVER_ID,DPS_COMPANY_ID,DPS_TOTAL_AMOUNT,DPS_PROCESSING_DATE,DPS_NO_OF_REC)" +
			"values(?,?,?,?,now(),?)");
			key  =  ConfigDAO.tdsKeyGen("TDS_DRIVER_PENALITY_SETTELED_SEQ", m_conn);

			while(m_rs_summ.next())
			{
				c_dri=m_rs_summ.getString("DP_DRIVER_ID");

				m_pst_pay.setInt(1, key);
				m_pst_pay.setString(2, m_rs_summ.getString("DP_TRANS_ID"));
				m_pst_pay.setString(3, m_rs_summ.getString("DP_DRIVER_ID"));
				m_pst_pay.setString(4, m_rs_summ.getString("DP_ASSOCCODE"));
				m_pst_pay.setString(5, m_rs_summ.getString("DP_DESC"));
				m_pst_pay.setString(6, m_rs_summ.getString("DP_AMOUNT"));
				m_pst_pay.setString(7, m_rs_summ.getString("DP_PENALITY_DATE"));
				cat.info(m_pst_pay.toString());

				m_pst_pay.execute();



				AMOUNT = AMOUNT  + m_rs_summ.getDouble("DP_AMOUNT");

				no_of_rec = no_of_rec+1;

				m_pst_del = m_conn.prepareStatement("delete from TDS_DRIVER_PENALITY where DP_TRANS_ID = '" + m_rs_summ.getString("DP_TRANS_ID")+"'");
				m_pst_del.execute();
			}	
			m_pst_pay_summ.setInt(1, key);
			m_pst_pay_summ.setString(2,driverID);
			m_pst_pay_summ.setString(3,assocCode);
			m_pst_pay_summ.setString(4,""+AMOUNT);
			m_pst_pay_summ.setString(5,""+no_of_rec);
			cat.info(m_pst_pay.toString());

			m_pst_pay_summ.execute();

			m_conn.commit();

			m_conn.setAutoCommit(true);

		} catch (SQLException sqex) {


			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
					m_conn.setAutoCommit(true);
				} catch (SQLException e) {
					//cat.info("TDSException ManualDAO.moveToDriverPenality.SecondTime-->"+sqex.getMessage());
					//System.out.println("TDSException ManualDAO.moveToDriverPenality.SecondTime-->"+sqex.getMessage());
					sqex.printStackTrace();
				}
			}
			cat.error("TDSException ManualDAO.moveToDriverPenality-->"+sqex.getMessage());
			cat.error(m_pst_cmp.toString());
			System.out.println("TDSException ManualDAO.moveToDriverPenality" + sqex.getMessage());
			sqex.printStackTrace();			
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;

	}
	public static ArrayList<ManualSettleBean>  getTxnsToSettle(String driverID,String assocCode, Boolean creditcard, Boolean voucher, Boolean penalty)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO ManualDAO.getTxnsToSettle");
		ArrayList<ManualSettleBean> txnToSettle = new ArrayList<ManualSettleBean>();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		boolean m_result = false;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			if (creditcard)
			{
				m_pst = m_conn.prepareStatement("SELECT AP_TOTAL_AMOUNT_WITH_TXN as amt, date_format(AP_PROCESSING_DATE_TIME,'%m/%d/%Y') as date, AP_TRANS_ID as transid FROM TDS_APPROVAL WHERE AP_DRIVER_ID ='" + driverID + "' and AP_COMPANY_ID = '" + assocCode +"'" );
				cat.info(m_pst.toString());

				m_rs = m_pst.executeQuery();
				while(m_rs.next())
				{
					ManualSettleBean settlebean = new ManualSettleBean();
					settlebean.setDescription("Credit Card Txn");
					settlebean.setAmount(m_rs.getString("amt"));
					settlebean.setDate(m_rs.getString("date"));
					settlebean.settxnID(m_rs.getString("transid"));
					settlebean.setTxnLocation("C");
					txnToSettle.add(settlebean);
				}
			}
			if (penalty)
			{
				m_pst = m_conn.prepareStatement("SELECT DP_AMOUNT as amt, DP_DESC as descrip, date_format(DP_ENTERED_DATE,'%m/%d/%Y') as date, DP_TRANS_ID as transid FROM TDS_DRIVER_PENALITY WHERE DP_DRIVER_ID ='" + driverID + "' and DP_ASSOCCODE = '" + assocCode +"'" );

				m_rs = m_pst.executeQuery();
				while(m_rs.next()){
					ManualSettleBean settlebean = new ManualSettleBean();
					settlebean.setDescription(m_rs.getString("descrip"));
					settlebean.setAmount(m_rs.getString("amt"));
					settlebean.setDate(m_rs.getString("date"));
					settlebean.settxnID(m_rs.getString("transid"));
					settlebean.setTxnLocation("P");
					txnToSettle.add(settlebean);
				}
			}
			if(voucher){
				m_pst = m_conn.prepareStatement("SELECT VD_RET_AMOUNT as amt, VD_DESCR as descrip, date_format(VD_PROCESSING_DATE_TIME,'%m/%d/%Y') as date, VD_TXN_ID as transid FROM TDS_VOUCHER_DETAIL WHERE  VD_DRIVER_ID ='" + driverID + "' and VD_ASSOC_CODE = '" + assocCode +"'" );
				m_rs = m_pst.executeQuery();
				while(m_rs.next()){
					ManualSettleBean settlebean = new ManualSettleBean();
					settlebean.setDescription(m_rs.getString("descrip"));
					settlebean.setAmount(m_rs.getString("amt"));
					settlebean.setDate(m_rs.getString("date"));
					settlebean.settxnID(m_rs.getString("transid"));
					settlebean.setTxnLocation("V");
					txnToSettle.add(settlebean);
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException ManualDAO.getTxnsToSettle-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ManualDAO.getTxnsToSettle" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));


		return txnToSettle;

	}


public static int moveToVoucherSetteled(String assocCode, String driverID, ArrayList  transIDs)
	{
	long startTime = System.currentTimeMillis();
	cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO ManualDAO.moveToPaymentSetteled");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst_approv = null,m_pst_pay = null,m_pst_pay_summ = null;
		PreparedStatement m_pst_del=null,m_pst_cmp=null, m_pst_cmp_up = null,m_pst_log=null;
		ResultSet m_rs_summ = null,rs_cmp=null,rs=null;
		String prev_dri;
		int status=0;
		String queryTransID = "";
		for (int i=0;i<transIDs.size() -1 ; i++){
			queryTransID = queryTransID + "'" + (String) transIDs.get(i)+ "'" +",";
		}
		queryTransID = queryTransID + "'" + (String) transIDs.get(transIDs.size() -1) + "'";
		try {


			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			int key =0 , counter=0,no_of_rec=0,no_of_accp=0,no_of_dec=0;
			double AMOUNT =0,TIP_AMOUNT=0,TOTAL_AMOUNT=0,TXN_PERCENT_AMT=0,TXN_AMT=0,TOTAL_AMOUNT_WITH_TXN=0,CCC_TXN_AMT_PERCENTAGE=0,CCC_TXN_AMOUNT=0;
			m_conn.setAutoCommit(false);


			prev_dri="";
			counter = 0;

			status=1;

			m_pst_cmp = m_conn.prepareStatement("select * from TDS_COMPANY_CC_SETUP where CCC_COMPANY_ID = '"+assocCode+"' and CCC_CARD_TYPE = 'V' ");
			rs = m_pst_cmp.executeQuery();

			if(rs.first())
			{
				CCC_TXN_AMT_PERCENTAGE = rs.getDouble("CCC_TXN_AMT_PERCENTAGE"); 
				CCC_TXN_AMOUNT = rs.getDouble("CCC_TXN_AMOUNT");
			}
			

			m_pst_approv = m_conn.prepareStatement("select * from TDS_VOUCHER_DETAIL where VD_TXN_ID IN (" + queryTransID + ")");
			m_rs_summ = m_pst_approv.executeQuery();
			m_pst_pay = m_conn.prepareStatement("UPDATE TDS_VOUCHER_DETAIL SET(VD_TXN_PERCENT_AMT=?,VD_AMOUNT=?,VD_TOTAL=?,VD_INVOICE_NO=?) WHERE VD_TXN_ID=? AND VD_ASSOC_CODE=?");

			while(m_rs_summ.next())
			{
				m_pst_pay.setDouble(1, ((CCC_TXN_AMT_PERCENTAGE/100)*((m_rs_summ.getDouble("VD_RET_AMOUNT")+m_rs_summ.getDouble("VD_TIP")))));
				m_pst_pay.setDouble(2,CCC_TXN_AMOUNT);
				m_pst_pay.setDouble(3,  ((m_rs_summ.getDouble("VD_RET_AMOUNT")+m_rs_summ.getDouble("VD_TIP"))-CCC_TXN_AMOUNT-((CCC_TXN_AMT_PERCENTAGE/100)*((m_rs_summ.getDouble("VD_RET_AMOUNT")+m_rs_summ.getDouble("VD_TIP"))))));
				m_pst_pay.setString(4, m_rs_summ.getString("VD_INVOICE_NO"));
				m_pst_pay.setString(5, m_rs_summ.getString("VD_TXN_ID"));
				m_pst_pay.setString(6, m_rs_summ.getString("VD_ASSOC_CODE"));
				cat.info(m_pst_pay.toString());
				m_pst_pay.execute();
				AMOUNT = AMOUNT  + m_rs_summ.getDouble("VD_RET_AMOUNT");
				TIP_AMOUNT = TIP_AMOUNT + m_rs_summ.getDouble("VD_TIP");
				TOTAL_AMOUNT = TOTAL_AMOUNT + m_rs_summ.getDouble("VD_RET_AMOUNT") + m_rs_summ.getDouble("VD_TIP");

				TXN_AMT = TXN_AMT + CCC_TXN_AMOUNT;
				TXN_PERCENT_AMT = TXN_PERCENT_AMT +  ((CCC_TXN_AMT_PERCENTAGE/100)*((m_rs_summ.getDouble("VD_RET_AMOUNT")+m_rs_summ.getDouble("VD_TIP"))));
				TOTAL_AMOUNT_WITH_TXN = TOTAL_AMOUNT_WITH_TXN + ((m_rs_summ.getDouble("VD_RET_AMOUNT")+m_rs_summ.getDouble("VD_TIP"))-CCC_TXN_AMOUNT-((CCC_TXN_AMT_PERCENTAGE/100)*((m_rs_summ.getDouble("VD_RET_AMOUNT")+m_rs_summ.getDouble("VD_TIP")))));

				no_of_rec = no_of_rec+1;

				counter =1;

				prev_dri=m_rs_summ.getString("VD_DRIVER_ID");
				 
			
  
			}
			
 
			m_conn.commit();
			m_conn.setAutoCommit(true);

		} catch (SQLException sqex) {
			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			cat.error("TDSException ManualDAO.moveToPaymentSetteled-->"+sqex.getMessage());
			cat.error(m_pst_cmp.toString());
			//System.out.println("TDSException ManualDAO.moveToPaymentSetteled" + sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));


		return status;
	}



}


