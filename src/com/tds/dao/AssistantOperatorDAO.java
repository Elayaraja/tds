package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.OpenRequestHistory;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AssistantOperatorBo;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.PaymentSettledDetail;
import com.tds.tdsBO.VoucherBO;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

public class AssistantOperatorDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(AssistantOperatorDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+AssistantOperatorDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}
	public static ArrayList<OpenRequestBO> getjobsForAssistants( String timeOffset,String createdBy) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String queryAppend="";
		String queryAppend1 ="";
		if(createdBy!=null&createdBy!=""){
			queryAppend1 =" AND OR_CREATED_BY ='"+createdBy+"' AND OR_TRIP_SOURCE = '"+TDSConstants.assistant+"'";
		}
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT " +
					"  OR_TRIP_ID,OR_EDADD1,OR_NAME,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y/%m/%d') as DATE, OR_STADD1,OR_EDADD1,OR_END_QUEUENO,OR_STCITY,OR_PHONE,OR_PREMIUM_CUSTOMER, OR_QUEUENO,OR_TRIP_STATUS,OR_DRCABFLAG, QD_DISPATCH_TYPE, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"') AS OR_SERVICEDATE,QD_DISPATCH_DISTANCE,"+
					" case OR_DISPATCH_START_TIME  < NOW() WHEN TRUE THEN TIMESTAMPDIFF(MINUTE,OR_DISPATCH_START_TIME,NOW()) END AS PENDING ,OR_STLATITUDE, OR_STLONGITUDE, OR_DISPATCH_HISTORY_DRIVERS," +
					"  case QD_DELAYTIME IS NULL WHEN true THEN  (OR_DISPATCH_LEAD_TIME * 60) ELSE CASE OR_DISPATCH_LEAD_TIME = -1 when true THEN QD_DELAYTIME*60 else (OR_DISPATCH_LEAD_TIME * 60) END END  AS QD_DELAYTIME," + 
					" OR_SMS_SENT,QD_SMS_RES_TIME, OR_DRIVERID,OR_VEHICLE_NO,OR_DONT_DISPATCH,OR_SHARED_RIDE, OR_PAYTYPE, OR_ROUTE_NUMBER , " +
					" date_format(OR_SERVICEDATE,'%r') as st_time," +
					" time_to_sec(timediff(OR_SERVICEDATE,now())) as st_time_diff," +
					"time_to_sec(timediff(now(),OR_SMS_SENT)) as  SMS_TIME_DIFF,QD.QD_BROADCAST_DELAY,OR_LANDMARK,"+
					"date_format(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"'),'%H%i') as SERVICE_TIME, "+
					"  OR_TRIP_STATUS" +
					" FROM" +
					" TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD " +
					" ON  OR_QUEUENO = QD_QUEUENAME AND OR_ASSOCCODE = QD_ASSOCCODE" +
					" WHERE " +
					"  OR_TRIP_STATUS != '"+TDSConstants.dummyTripId+ "'" +
					" AND OR_TRIP_STATUS != '"+TDSConstants.srAllocatedToCab+ "'" +
					" AND OR_TRIP_STATUS != '"+TDSConstants.tripCompleted+ "'" +
					queryAppend +
					queryAppend1 +
			" ORDER BY PENDING DESC");
			//System.out.println("checking query:"+pst.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				openRequestBO.setScity(rs.getString("OR_STADD1"));
				openRequestBO.setPendingTime(rs.getString("PENDING")==null?"":rs.getString("PENDING"));
				openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setRequestTime(rs.getString("SERVICE_TIME"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setSdate(rs.getString("DATE"));
				openRequestBO.setDontDispatch(rs.getInt("OR_DONT_DISPATCH"));
				openRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
				openRequestBO.setPaytype(rs.getString("OR_PAYTYPE"));
				openRequestBO.setEcity(rs.getString("OR_EDADD1"));
				openRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				openRequestBO.setPremiumCustomer(rs.getInt("OR_PREMIUM_CUSTOMER"));
				openRequestBO.setEndQueueno(rs.getString("OR_END_QUEUENO"));
				openRequestBO.setEcity(rs.getString("OR_EDADD1"));
				openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				al_list.add(openRequestBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException AssistantOperatorDAO.getjobsForAssistants-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AssistantOperatorDAO.getjobsForAssistants-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList getJobHistoryForAssistant(String  timeOffset,String createdBy,String value,String fromDate,String toDate) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO>openReqHistory = new ArrayList<OpenRequestBO>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query= "SELECT *,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"'),'%m/%d/%Y %H:%i') AS ORH_SERVICEDATE_FORMATTED, case when ORH_TRIP_STATUS=40 THEN 'Job Allocated' when ORH_TRIP_STATUS=43 THEN 'On Route' when ORH_TRIP_STATUS=47 THEN 'Driver NoShow' when ORH_TRIP_STATUS=4 THEN 'Performing Dispatch' when ORH_TRIP_STATUS=8 THEN 'New Request' when ORH_TRIP_STATUS=30 THEN 'Broadcast' when ORH_TRIP_STATUS=50 THEN 'Customer cancelled' when ORH_TRIP_STATUS=51 THEN 'NoShow' when ORH_TRIP_STATUS=52 THEN 'Couldnt service' when ORH_TRIP_STATUS=55 THEN 'Operator Cancelled' when ORH_TRIP_STATUS=61 THEN 'Job Completed' when ORH_TRIP_STATUS=99 THEN 'On Hold' end as TRIPSTATUS FROM TDS_OPENREQUEST_HISTORY WHERE  ORH_CREATED_BY ='"+createdBy+"' AND ORH_TRIP_SOURCE = '"+TDSConstants.assistant+"'";
		if(!value.equals("")){
			query =query+" AND( ORH_DRIVERID like '%"+value+"%' OR ORH_PHONE like'%"+value+"%' OR ORH_NAME like'%"+value+"%' OR ORH_TRIP_ID like'%"+value+"%' OR ORH_VEHICLE_NO like'%"+value+"%' OR ORH_STCITY like'%"+value+"%' OR ORH_STSTATE like'%"+value+"%' OR ORH_STZIP  like'%"+value+"%' OR ORH_VEHICLE_NO like '%"+value+"%')";
		}
		if(!fromDate.equals("")&&!toDate.equals("")){
			query=query+" AND DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y-%m-%d')<='"+toDate+"' AND DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y-%m-%d')>='"+fromDate+"'";	
		}
		query=query+" ORDER BY ORH_SERVICEDATE";
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection(); 
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				OpenRequestBO openRequestBean=new OpenRequestBO();
				openRequestBean.setDriverid(rs.getString("ORH_DRIVERID"));
				openRequestBean.setName(rs.getString("ORH_NAME"));
				openRequestBean.setPhone(rs.getString("ORH_PHONE"));
				openRequestBean.setTripid(rs.getString("ORH_TRIP_ID"));
				openRequestBean.setSadd1(rs.getString("ORH_STADD1"));
				openRequestBean.setSadd2(rs.getString("ORH_STADD2"));
				openRequestBean.setScity(rs.getString("ORH_STCITY"));
				openRequestBean.setSlat(rs.getString("ORH_STLATITUDE"));
				openRequestBean.setSlong(rs.getString("ORH_STLONGITUDE"));
				openRequestBean.setTripStatus(rs.getString("TRIPSTATUS"));
				openRequestBean.setSstate(rs.getString("ORH_STSTATE"));
				openRequestBean.setSzip(rs.getString("ORH_STZIP"));
				openRequestBean.setEadd1(rs.getString("ORH_EDADD1"));
				openRequestBean.setEadd2(rs.getString("ORH_EDADD2"));
				openRequestBean.setEcity(rs.getString("ORH_EDCITY"));
				openRequestBean.setEstate(rs.getString("ORH_EDSTATE"));
				openRequestBean.setEdlatitude(rs.getString("ORH_EDLATITUDE"));
				openRequestBean.setEdlongitude(rs.getString("ORH_EDLONGITUDE"));
				openRequestBean.setEzip(rs.getString("ORH_EDZIP"));
				openRequestBean.setSdate(rs.getString("ORH_SERVICEDATE_FORMATTED"));
				openRequestBean.setPaytype(rs.getString("ORH_PAYTYPE"));
				openRequestBean.setAmt(new BigDecimal(rs.getString("ORH_AMT")));
				openRequestBean.setSpecialIns(rs.getString("ORH_SPLINS"));
				openRequestBean.setComments(rs.getString("ORH_OPERATOR_COMMENTS"));
				openRequestBean.setVehicleNo(rs.getString("ORH_VEHICLE_NO"));
				openRequestBean.setAssociateCode(rs.getString("ORH_ASSOCCODE"));
				openReqHistory.add(openRequestBean);
			}
		}
		catch (Exception sqex)
		{
			cat.error("TDSException AssistantOperatorDAO.getJobHistoryForAssistant-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AssistantOperatorDAO.getJobHistoryForAssistant-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return openReqHistory;
	}	
	public static ArrayList<OpenRequestHistory> getJobLogsForAssistant(String tripId,String timeZoneOffset){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestHistory> ORLogs = new ArrayList<OpenRequestHistory>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null ;
		ResultSet rs = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try {
			pst=con.prepareStatement("SELECT ORL_REASON,CONVERT_TZ(ORL_TIME,'UTC','"+timeZoneOffset+"') as TIME,ORL_CHANGED_BY,ORL_STATUS,case when ORL_STATUS='1' then 'INSERT JOB' when ORL_STATUS='5'then 'UPDATE JOB' when ORL_STATUS='10'then 'RESTART JOB' when ORL_STATUS='15'then 'PERFORMING DISPATCH' when ORL_STATUS='20'then 'BROADCAST JOB' when ORL_STATUS='25'then 'JOB ACCEPTED' when ORL_STATUS='30'then 'JOB REJECTED' when ORL_STATUS='35'then 'TRIP STARTED' when ORL_STATUS='40'then 'DRIVER ONROUTE' when ORL_STATUS='45'then 'JOB COMPLETED' when ORL_STATUS='55'then 'CUSTOMER CANCELLED' when ORL_STATUS='60'then 'OPERATOR NO SHOW' when ORL_STATUS='65'then 'DRIVER NO SHOW' when ORL_STATUS='70'then 'TRIP ON HOLD' when ORL_STATUS='0'then 'NO STATUS' when ORL_STATUS='50'then 'JOB COMPLETED' end as ORL_STATUSDESC FROM TDS_OPENREQUEST_LOGS WHERE  ORL_TRIPID='"+tripId+"'   ORDER BY ORL_TIME DESC");
			rs = pst.executeQuery();
			while (rs.next()) {
				OpenRequestHistory ORHistory = new OpenRequestHistory();
				ORHistory.setReason(rs.getString("ORL_REASON"));
				ORHistory.setTime(rs.getString("TIME"));
				ORHistory.setUserId(rs.getString("ORL_CHANGED_BY"));
				ORHistory.setStatus(rs.getString("ORL_STATUSDESC"));
				ORLogs.add(ORHistory);
			}
		}catch (Exception sqex){
			cat.error("TDSException AssistantOperatorDAO.getJobLogsForAssistant-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AssistantOperatorDAO.getJobLogsForAssistant-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return ORLogs;
	}
	public static ArrayList openRequestSummaryForAssistants(String timeZoneOffset,String createdBy,String value,String fromDate,String toDate) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO>openReqSummary = new ArrayList<OpenRequestBO>();
		TDSConnection dbConn = new TDSConnection();
		Connection conn =  dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query="SELECT *,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeZoneOffset+"'),'%m/%d/%Y %H:%i') AS OR_SERVICEDATE_FORMATTED FROM TDS_OPENREQUEST  where OR_CREATED_BY ='"+createdBy+"' AND (OR_TRIP_STATUS='8' OR OR_TRIP_STATUS='37' )";
		if(!value.equals("")){
			query =query+" AND( OR_DRIVERID like '%"+value+"%' OR OR_PHONE like'%"+value+"%' OR OR_NAME like'%"+value+"%' OR OR_TRIP_ID like'%"+value+"%' OR OR_VEHICLE_NO like'%"+value+"%' OR OR_STCITY like'%"+value+"%' OR OR_STSTATE like'%"+value+"%' OR OR_STZIP  like'%"+value+"%') ";
		}
		if(!fromDate.equals("")&&!fromDate.equals(null) && !toDate.equals("") && ! toDate.equals(null)){
			query=query+" AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeZoneOffset+"'),'%Y-%m-%d')<='"+toDate+"' AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeZoneOffset+"'),'%Y-%m-%d')>='"+fromDate+"'";	
		}
		query=query+" ORDER BY OR_SERVICEDATE";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				OpenRequestBO openRequestBean=new OpenRequestBO();
				openRequestBean.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBean.setName(rs.getString("OR_NAME"));
				openRequestBean.setPhone(rs.getString("OR_PHONE"));
				openRequestBean.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBean.setSadd1(rs.getString("OR_STADD1"));
				openRequestBean.setSadd2(rs.getString("OR_STADD2"));
				openRequestBean.setScity(rs.getString("OR_STCITY"));
				openRequestBean.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBean.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBean.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBean.setStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBean.setSstate(rs.getString("OR_STSTATE"));
				openRequestBean.setSzip(rs.getString("OR_STZIP"));
				openRequestBean.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBean.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBean.setEcity(rs.getString("OR_EDCITY"));
				openRequestBean.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBean.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBean.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBean.setEzip(rs.getString("OR_EDZIP"));
				openRequestBean.setStartTimeStamp(rs.getString("OR_SERVICEDATE_FORMATTED"));
				openRequestBean.setRepeatGroup(rs.getString("OR_REPEAT_GROUP"));
				openRequestBean.setComments(rs.getString("OR_COMMENTS"));
				openRequestBean.setSpecialIns(rs.getString("OR_SPLINS"));
				openRequestBean.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				openRequestBean.setCreatedBy(rs.getString("OR_CREATED_BY"));
				openRequestBean.setAssociateCode(rs.getString("OR_ASSOCCODE"));
				openReqSummary.add(openRequestBean);
			}
		}
		catch (Exception sqex){
			cat.error("TDSException AssistantOperatorDAO.openRequestSummaryForAssistants-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AssistantOperatorDAO.openRequestSummaryForAssistants-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return openReqSummary;
	}
	public static ArrayList<PaymentSettledDetail> getVoucherDetails(PaymentSettledDetail invoiceBo,String fDate,String tDate,String userId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst = null;
		ArrayList<PaymentSettledDetail> voucher =new ArrayList<PaymentSettledDetail>();
		ResultSet rs=null;
		//ArrayList<String> voucherslist = new ArrayList<String>();

		PaymentSettledDetail invoiceBO = null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String queryAppend ="SELECT AOA_ACCOUNT_NO,AOA_EMAILID,CCU_USERID FROM TDS_ASSISTANTOPERATOR_ACCESS,CLNT_CLIENTUSER WHERE AOA_EMAILID = CCU_EMAIL  AND CCU_USERID ='"+userId+"'";
		String query = "SELECT *,date_format(VD_EXPIRY_DATE,'%m/%d/%Y') as VD_EXPIRYDATE,date_format(VD_PROCESSING_DATE_TIME,'%m/%d/%Y') as VD_PROCESSING_DATE_TIME from TDS_VOUCHER_DETAIL WHERE ";
		String qExtend ="";
		try{
			pst=con.prepareStatement(queryAppend);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			query += "VD_VOUCHERNO IN("; 
			while(rs.next()){
				//voucherslist.add(rs.getString("AOA_ACCOUNT_NO"));
				qExtend += rs.getString("AOA_ACCOUNT_NO")+',';
			}
			if(qExtend!=""){
				query += qExtend.substring(0,qExtend.length()-1)+')';
				pst=con.prepareStatement(query);
				cat.info(pst.toString());
				rs=pst.executeQuery();
				while(rs.next()){
					invoiceBO = new PaymentSettledDetail();
					invoiceBO.setInvoiceNumber(rs.getInt("VD_INVOICE_NO"));
					invoiceBO.setTransId(rs.getString("VD_TXN_ID"));
					invoiceBO.setCostCenter(rs.getString("VD_COSTCENTER"));
					invoiceBO.setRiderName(rs.getString("VD_RIDER_NAME"));
					invoiceBO.setVoucherNo(rs.getString("VD_VOUCHERNO"));
					invoiceBO.setAssociationCode(rs.getString("VD_ASSOC_CODE"));
					invoiceBO.setAmount(rs.getString("VD_TOTAL"));
					invoiceBO.setExpDate((rs.getString("VD_EXPIRYDATE")));
					invoiceBO.setDescription(rs.getString("VD_DESCR"));
					invoiceBO.setCompanyCode(rs.getString("VD_COMPANY_CODE"));
					invoiceBO.setContact(rs.getString("VD_CONTACT"));
					invoiceBO.setDelayDays(rs.getString("VD_DELAYDAYS"));
					invoiceBO.setFreq(rs.getString("VD_FREQUENCY"));
					invoiceBO.setTripId(rs.getString("VD_TRIP_ID"));
					invoiceBO.setDriverId(rs.getString("VD_DRIVER_ID"));
					invoiceBO.setPaymentReceivedStatus(rs.getInt("VD_PAYMENT_RECEIVED_STATUS"));
					invoiceBO.setVerified(rs.getInt("VD_VERIFIED"));
					invoiceBO.setDriverPayedStatus(rs.getInt("VD_DRIVER_PMT_STATUS"));
					voucher.add(invoiceBO);
				}
			}

		}catch(SQLException sqex) {
			cat.error("TDSException AssistantOperatorDAO.getVoucherDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AssistantOperatorDAO.getVoucherDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return voucher;
	}
	public static int upDateVoucherAsverified(String[] setID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int status =0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query = "UPDATE TDS_VOUCHER_DETAIL SET VD_VERIFIED ='4',VD_PROCESSING_DATE_TIME=VD_PROCESSING_DATE_TIME WHERE  VD_TXN_ID in (";
		if(setID.length -1 >= 0)
			query += setID[setID.length-1];

		for(int j=0;j<setID.length-1;j++){
			query +=  ","+setID[j] ;
		}
		query += ")";	
		try{
			pst=con.prepareStatement(query);
			status=pst.executeUpdate();
		}catch(SQLException sqex) {
			cat.error("TDSException AssistantOperatorDAO.upDateVoucherAsverified-->"+sqex.getMessage());
			//System.out.println("TDSException AssistantOperatorDAO.upDateVoucherAsverified-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static int createUser(AssistantOperatorBo assistBo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result=0;
		cat.info("TDS INFO AssistantOperatorDAO.createUser");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String sql = "INSERT INTO TDS_ASSISTANT_OPERATOR(AO_NAME,AO_USERID,AO_PASSWORD,AO_EMAIL,AO_ADDRESS,AO_CITY,AO_ZIPCODE,AO_PHONENO) VALUES (?,?,?,?,?,?,?,?)";
		try {
			pst=con.prepareStatement(sql);
			pst.setString(1, assistBo.getName());
			pst.setString(2, assistBo.getUserId());
			pst.setString(3,assistBo.getPassword() );
			pst.setString(4, assistBo.getEmail());
			pst.setString(5, assistBo.getAddress());
			pst.setString(6, assistBo.getCity() );
			pst.setInt(7, assistBo.getZipCode());
			pst.setString(8, assistBo.getPhNo());
			result = pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException AssistantOperatorDAO.createUser-->"+sqex.getMessage());
			//System.out.println("TDSException AssistantOperatorDAO.createUser-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static AssistantOperatorBo customerLogin(AssistantOperatorBo CMBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//int result =0;
		AssistantOperatorBo customerBO = new AssistantOperatorBo();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_ASSISTANT_OPERATOR WHERE AO_USERID=? AND AO_PASSWORD=?");
			pst.setString(1, CMBO.getUserId());
			pst.setString(2, CMBO.getPassword());
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				customerBO.setName(rs.getString("AO_NAME"));
				customerBO.setPhNo(rs.getString("AO_PHONENO"));
				customerBO.setEmail(rs.getString("AO_EMAIL"));
				customerBO.setUserId(rs.getString("AO_USERID"));
				customerBO.setTimeZone(rs.getString("AO_TIMEZONE"));
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException AssistantOperatorDAO.customerLogin-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AssistantOperatorDAO.customerLogin-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return customerBO;
	}
	public static ArrayList<VoucherBO> getVouchers(String mailId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<VoucherBO> voucherList = new ArrayList<VoucherBO>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_GOVT_VOUCHER WHERE VC_EMAIL_ADDRESS LIKE '%;"+mailId+";%'");
			rs=pst.executeQuery();
			while (rs.next()){
				VoucherBO newVoucher = new VoucherBO();
				newVoucher.setVassoccode(rs.getString("VC_ASSOCCODE"));
				newVoucher.setVno(rs.getString("VC_VOUCHERNO"));
				voucherList.add(newVoucher);
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException AssistantOperatorDAO.customerLogin-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AssistantOperatorDAO.customerLogin-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return voucherList;
			
	}

	
}
