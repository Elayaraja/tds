package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.util.TDSSQLConstants;

public class ConfigDAO {

	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(ConfigDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ConfigDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}

	
	public static int tdsKeyGen(String p_keyword, Connection p_conn) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ConfigDAO.tdsKeyGen");
		int keyGenValue = 0;
		int incrementValue = 0;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			pst = p_conn.prepareStatement(TDSSQLConstants.getKeyGen);
			pst.setString(1, p_keyword);
			rs = pst.executeQuery();
			if(rs.first()) {
				keyGenValue = Integer.parseInt(rs.getString("SQ_CURRENTVALUE"));
				incrementValue = Integer.parseInt(rs.getString("SQ_INCREMENTVALUE"));
			}
			pst.close();
			rs.close();
			pst = p_conn.prepareStatement(TDSSQLConstants.updateKeyGen);
			pst.setInt(1, keyGenValue+incrementValue);
			pst.setString(2, p_keyword);
			cat.info(pst.toString());
			int result = pst.executeUpdate();
			if(result <= 0 ){
				keyGenValue = 0;
			}
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException ConfigDAO.tdsKeyGen-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ConfigDAO.tdsKeyGen-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return keyGenValue;
	}

	public static int checkUsedIdExist(String userID,String type) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ConfigDAO.checkUsedIdExist");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = "";
		if(type.equalsIgnoreCase("DRIVER")) {
			query = TDSSQLConstants.checkUseridExist;
		} else {
			query = TDSSQLConstants.checkUseridExist;
		}
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(query);
			pst.setString(1, userID);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.next()) {
				result = 1;
			}
			rs.close();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException ConfigDAO.checkUsedIdExist-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ConfigDAO.checkUsedIdExist-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();

		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int checkAirCode(String aircode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ConfigDAO.checkAirCode");
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.checkAirCode);
			pst.setString(1, aircode);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.next()) {
				result = 1;
			}
			rs.close();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException ConfigDAO.checkAirCode-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CompanySetupDAO.checkAirCode-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
}
