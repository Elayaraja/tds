package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Category;
import org.apache.woden.wsdl20.Description;

import com.tds.cmp.bean.CabRegistrationBean;
import com.tds.cmp.bean.OperatorShift;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.security.bean.TagSystemBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.FleetBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.OpenRequestFieldOrderBO;
import com.tds.tdsBO.VoucherBO;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.tds.util.TDSSQLConstants;
import com.common.util.TDSValidation;

public class RequestDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(RequestDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+RequestDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static ArrayList getAllOpenRequestForAutoAlloc(String assoccode) {
		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getAllOpenRequestForAutoAlloc");
		ArrayList al_openRequest = new ArrayList();
		OpenRequestBO openRequest;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null,rs1=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst.setString(1, assoccode);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			//String time;
			while(rs.next()) {
				openRequest = new OpenRequestBO();
				openRequest.setName(rs.getString("OR_NAME"));
				openRequest.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequest.setTripid(rs.getString("OR_TRIP_ID"));
				openRequest.setSadd1(rs.getString("OR_STADD1"));
				openRequest.setSadd2(rs.getString("OR_STADD2"));
				openRequest.setScity(rs.getString("OR_STCITY"));
				openRequest.setSstate(rs.getString("OR_STSTATE"));
				openRequest.setEcity(rs.getString("OR_EDCITY"));
				openRequest.setEstate(rs.getString("OR_EDSTATE"));
				//time = rs.getString("OR_SERVICETIME");
				//openRequest.setShrs(time.substring(0,2));
				//openRequest.setSmin(time.substring(2,4));
				openRequest.setSdate(rs.getString("Service_Date"));
				openRequest.setSlat(rs.getString("OR_STLATITUDE"));
				openRequest.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequest.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequest.setEdlongitude(rs.getString("OR_EDLONGITUDE"));

				pst = con.prepareStatement(" select ((QD_LATITUDENORTH+QD_LATITUDESOUTH)/2) as LATITUDE,((QD_LONGITUDEEAST+QD_LONGITUDEWEST)/2) as LONGITUDE from TDS_QUEUE_DETAILS where QD_QUEUENAME=?");
				pst.setString(1, rs.getString("OR_QUEUENO"));
				rs1 = pst.executeQuery();

				if(rs1.first())
				{
					openRequest.setSlat(rs1.getString("LATITUDE"));
					openRequest.setSlong(rs1.getString("LONGITUDE"));
				} else {
					openRequest.setSlat("0");
					openRequest.setSlat("0");
				}


				al_openRequest.add(openRequest);
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getAllOpenRequestForAutoAlloc-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestRequestDAO.getAllOpenRequestForAutoAllocDAO.getAllOpenRequestForAutoAlloc-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_openRequest;
	}




	public static CabRegistrationBean getCabDetails(String key,String companyCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getCabDetails");
		CabRegistrationBean registrationBean = new CabRegistrationBean();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;

		PreparedStatement pst1 = null;
		ResultSet rs1 = null;
		String sql = "";

		ArrayList<Character> al_list = new ArrayList<Character>();
		ResultSet rs = null;
		String query = "select * from TDS_VEHICLE WHERE V_ASSOCCODE='"+companyCode+"'"; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		query = query + " AND V_VKEY='"+key+"'";
		try {
			pst = con.prepareStatement(query);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.first())
			{ 
				registrationBean.setCab_assocode(rs.getString("V_ASSOCCODE"));
				registrationBean.setCab_cabkey(rs.getString("V_VKEY"));
				registrationBean.setCab_cabmake(rs.getString("V_VMAKE"));	
				registrationBean.setCab_cabno(rs.getString("V_VNO"));
				registrationBean.setCab_cabYear(rs.getString("V_VYEAR"));
				registrationBean.setCab_rcno(rs.getString("V_RCNO"));
				registrationBean.setVehicleProfile(rs.getString("V_FLAG")); 
				registrationBean.setStatus(rs.getString("V_STATUS"));
				registrationBean.setCab_model(rs.getString("V_MODEL"));
				registrationBean.setCab_vinno(rs.getString("V_VINNO"));
				registrationBean.setCab_vtype(rs.getString("V_VTYPE"));
				registrationBean.setCab_registrationDate(rs.getString("V_REGDATE"));
				registrationBean.setCab_expiryDate(rs.getString("V_EXPDATE"));
				registrationBean.setCab_meterRate(rs.getString("V_VMETER_RATE"));
				registrationBean.setPayId(rs.getString("V_PAY_ID"));
				registrationBean.setPayPass(rs.getString("V_PAY_PASS"));
				registrationBean.setCab_plate_number(rs.getString("V_PLATE_NO"));
				registrationBean.setCab_detials_json(rs.getString("V_DETAILS_JSON")!=null?rs.getString("V_DETAILS_JSON"):"");
				
				String vechileProfile =  rs.getString("V_FLAG");
				pst1 = con.prepareStatement("SELECT MAX(OD_VALUE) AS OD_VALUE  FROM TDS_ODOMETER WHERE OD_ASSOCCODE = '"+rs.getString("V_ASSOCCODE")+"' AND OD_CAB_NO ='"+rs.getString("V_VNO")+"' ");
				//System.out.println("Get Cab Details:"+pst1.toString());
				rs1 = pst1.executeQuery();
				if(rs1.next()) {
					registrationBean.setOdoMeterValue(rs1.getInt("OD_VALUE")+"");				
				}  

				for(int i=0;i< vechileProfile.toCharArray().length;i++)
				{
					al_list.add(vechileProfile.toCharArray()[i]);
				}
				registrationBean.setAl_list(al_list);
			}
		}  catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.getCabDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getCabDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return registrationBean;
	}

	public static ArrayList getlandMarkDetails(String  landMarkName,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getlandMarkDetails");
		ArrayList al_list = new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null ;
		ResultSet rs = null; 
		OpenRequestBO openRequestBO = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try {
			pst = con.prepareStatement(TDSSQLConstants.getlandmarkdetails);
			pst.setString(1,  assoccode); 
			pst.setString(2,  landMarkName+"%"); 
			cat.info("Query in RequestDAO getOpenRequest "+pst);
			rs = pst.executeQuery();
			while(rs.next()) {
				openRequestBO = new OpenRequestBO(); 
				openRequestBO.setSadd1(rs.getString("LM_ADD1"));
				openRequestBO.setSadd2(rs.getString("LM_ADD2"));
				openRequestBO.setScity(rs.getString("LM_CITY"));
				openRequestBO.setSstate(rs.getString("LM_STATE"));
				openRequestBO.setSzip(rs.getString("LM_ZIP")); 
				openRequestBO.setSlat(rs.getString("LM_LATITUDE"));
				openRequestBO.setSlong(rs.getString("LM_LONGITUDE")); 
				openRequestBO.setSlandmark(rs.getString("LM_NAME")); 
				openRequestBO.setLandMarkKey(rs.getString("LM_KEY"));
				openRequestBO.setComments(rs.getString("LM_COMMENTS"));
				al_list.add(openRequestBO);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getlandMarkDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getlandMarkDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static ArrayList getopenrequestView(String assocode,OpenRequestBO p_openRequest) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getopenrequestView");
		ArrayList al_list = new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1,pst2 = null ;
		ResultSet rs,rs1,rs2 = null; 
		OpenRequestBO openRequestBO = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try {
			pst = con.prepareStatement(TDSSQLConstants.getOpenRequestView);
			pst.setString(1, p_openRequest.getTripid());
			pst.setString(2, assocode); 
			cat.info("Query in RequestDAO getOpenRequest "+pst);
			rs = pst.executeQuery();
			while(rs.next()) {
				openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(rs.getString("OR_STADD2"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setSstate(rs.getString("OR_STSTATE"));
				openRequestBO.setSzip(rs.getString("OR_STZIP"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBO.setEzip(rs.getString("OR_EDZIP"));
				//openRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("OR_SERVICEDATE")));
				openRequestBO.setSdate(rs.getString("OR_SERVICEDATE"));
				openRequestBO.setShrs(rs.getString("OR_SERVICETIME"));
				/*openRequestBO.setSmin(rs.getString("OR_SERVICETIME").substring(2,4));*/
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME"))); 
				openRequestBO.setAssociateCode(rs.getString("OR_ASSOCCODE"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setQueueno(rs.getString("QD_DESCRIPTION"));
				openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				openRequestBO.setSintersection(rs.getString("OR_INTERSECTION"));
				/*				pst1 = con.prepareStatement("SELECT   AR_PORTDESC FROM TDS_AIRPORTDETAIL   WHERE AR_AIRCODE=?");
				pst1.setString(1, rs.getString("OR_SAIRCODE"));
				rs1 = pst1.executeQuery();
				if(rs1.next())  {
					openRequestBO.setSaircode(rs1.getString("AR_PORTDESC")); 
				} 
				pst2 = con.prepareStatement("SELECT   AR_PORTDESC FROM TDS_AIRPORTDETAIL   WHERE AR_AIRCODE=?");
				pst2.setString(1, rs.getString("OR_EAIRCODE"));
				rs2 = pst2.executeQuery();
				if(rs2.next())  {	
					openRequestBO.setEaircode(rs2.getString("AR_PORTDESC")); 
				} 
				 */				al_list.add(openRequestBO);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getopenrequestView-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getopenrequestView-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static ArrayList<CabRegistrationBean> getCabSummary(String Assocode,String cabno,String registration,String year,String status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getCabSummary");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ArrayList<CabRegistrationBean> al_list = new ArrayList<CabRegistrationBean>();
		ResultSet rs = null;
		String whereClauseQuery = "";
		String query = "select * from TDS_VEHICLE where V_ASSOCCODE='"+Assocode+"'"; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		if(!cabno.equals("")){
			whereClauseQuery =  " AND V_VNO='"+cabno+"'";
		}
		if (!registration.equals("")){
			whereClauseQuery = whereClauseQuery + " and V_RCNO='"+registration+"'";
		}
		if(!year.equals("")){
			whereClauseQuery = whereClauseQuery + " and V_VYEAR='"+year+"'";
		}
		if(!status.equals("") && !status.equals("2")){
			whereClauseQuery = whereClauseQuery + " and V_STATUS='"+status+"'";
		}
		query = query + whereClauseQuery + " ORDER BY ABS(V_VNO)";	
		try {
			pst = con.prepareStatement(query);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				CabRegistrationBean registrationBean  = new CabRegistrationBean();
				registrationBean.setCab_assocode(rs.getString("V_ASSOCCODE"));
				registrationBean.setCab_cabkey(rs.getString("V_VKEY"));
				registrationBean.setCab_cabmake(rs.getString("V_VMAKE"));
				registrationBean.setCab_cabno(rs.getString("V_VNO"));
				registrationBean.setCab_cabYear(rs.getString("V_VYEAR"));
				registrationBean.setCab_rcno(rs.getString("V_RCNO"));
				registrationBean.setCab_rcno(rs.getString("V_RCNO"));
				registrationBean.setCab_model(rs.getString("V_MODEL"));
				registrationBean.setCab_vinno(rs.getString("V_VINNO"));
				registrationBean.setStatus(rs.getString("V_STATUS"));
				registrationBean.setCab_reference_id((rs.getString("V_REFERENCE_NO")==null)?"":rs.getString("V_REFERENCE_NO"));
				al_list.add(registrationBean);
			}
		}  catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.getCabSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getCabSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}



	public static int updateOpenRequest(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO) { 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateOpenRequest"); 
		int resultValue = 0; 
		TDSConnection dbcon = null; 
		Connection con = null; 
		PreparedStatement pst = null;
		PreparedStatement pst1=null;
		String date[] = p_OpenRequestBO.getSdate().split("/"); 
		Timestamp dateTime=new Timestamp((long) (System.currentTimeMillis())); 
		String splitTime[]=dateTime.toString().split("\\."); 
		String time=(dateTime.getHours()<10?"0"+dateTime.getHours():dateTime.getHours())+""+(dateTime.getMinutes()<10?"0"+dateTime.getMinutes():dateTime.getMinutes()); 
		dbcon = new TDSConnection(); 
		con = dbcon.getConnection(); 
		try { 
			if(!p_OpenRequestBO.getRepeatGroup().equalsIgnoreCase("")){ 
				pst = con.prepareStatement(TDSSQLConstants.updateOpenRequestByGroup);
				pst.setString(49, p_OpenRequestBO.getRepeatGroup()); 
				pst.setString(50, adminBO.getMasterAssociateCode()); 
			} else {
				pst = con.prepareStatement(TDSSQLConstants.updateOpenRequest); 
				if(p_OpenRequestBO.getShrs().equals("2525")){ 
					pst.setString(52, time); 
					pst.setString(53, splitTime[0]);  
					pst.setString(54, "UTC"); 
				} else { 
					pst.setString(52, p_OpenRequestBO.getShrs()); 
					pst.setString(53, (date[2]+"-"+date[0]+"-"+date[1]+" "+p_OpenRequestBO.getShrs().substring(0,2)+":"+p_OpenRequestBO.getShrs().substring(2,4)+":"+"00"));  
					pst.setString(54, adminBO.getTimeZone()); 
				} 
				pst.setInt(49, p_OpenRequestBO.getChckTripStatus()); 
				pst.setString(50, p_OpenRequestBO.getVehicleNo()); 
				pst.setString(51, p_OpenRequestBO.getDriverid()); 
				pst.setString(55, p_OpenRequestBO.getTripid()); 
				pst.setString(56, adminBO.getMasterAssociateCode()); 
			} 
			pst.setString(1, TDSValidation.getDBPhoneFormat(p_OpenRequestBO.getPhone())); 
			pst.setString(2, p_OpenRequestBO.getSadd1()); 
			pst.setString(3, p_OpenRequestBO.getSadd2()); 
			pst.setString(4, p_OpenRequestBO.getScity()); 
			pst.setString(5, p_OpenRequestBO.getSstate()); 
			pst.setString(6, p_OpenRequestBO.getSzip()); 
			pst.setString(7, p_OpenRequestBO.getEadd1()); 
			pst.setString(8, p_OpenRequestBO.getEadd2()); 
			pst.setString(9, p_OpenRequestBO.getEcity()); 
			pst.setString(10, p_OpenRequestBO.getEstate()); 
			pst.setString(11, p_OpenRequestBO.getEzip()); 
			pst.setString(12, p_OpenRequestBO.getSlat() != "" ?p_OpenRequestBO.getSlat():"0"); 
			pst.setString(13, p_OpenRequestBO.getSlong() != "" ?p_OpenRequestBO.getSlong():"0"); 
			pst.setString(14, p_OpenRequestBO.getEdlatitude() != "" ?p_OpenRequestBO.getEdlatitude():"0"); 
			pst.setString(15, p_OpenRequestBO.getEdlongitude() != "" ?p_OpenRequestBO.getEdlongitude():"0"); 
			pst.setString(16, p_OpenRequestBO.getQueueno() !=""?p_OpenRequestBO.getQueueno() :""); 
			pst.setString(17, p_OpenRequestBO.getName() !=""?p_OpenRequestBO.getName():""); 
			pst.setString(18, p_OpenRequestBO.getSlandmark()); 
			pst.setString(19, p_OpenRequestBO.getSintersection()); 
			pst.setString(20, p_OpenRequestBO.getSpecialIns()); 
			pst.setString(21, p_OpenRequestBO.getComments()); 
			pst.setString(22, p_OpenRequestBO.getTypeOfRide()); 
			pst.setString(23, p_OpenRequestBO.getPaytype()); 
			pst.setString(24, p_OpenRequestBO.getAcct()); 
			pst.setBigDecimal(25, p_OpenRequestBO.getAmt()); 
			pst.setString(26, p_OpenRequestBO.getElandmark()); 
			pst.setInt(27, p_OpenRequestBO.getNumOfPassengers()); 
			pst.setString(28, p_OpenRequestBO.getDropTime()); 
			pst.setInt(29, p_OpenRequestBO.getDontDispatch()); 
			pst.setString(30, p_OpenRequestBO.getAdvanceTime()); 
			pst.setInt(31, p_OpenRequestBO.getPremiumCustomer()); 
			pst.setString(32, p_OpenRequestBO.getDrProfile()+p_OpenRequestBO.getVecProfile()); 
			pst.setString(33, p_OpenRequestBO.getEmail()); 
			pst.setString(34, p_OpenRequestBO.getEndQueueno()); 
			pst.setInt(35, p_OpenRequestBO.getPaymentStatus()); 
			pst.setString(36, p_OpenRequestBO.getAssociateCode()); 
			pst.setInt(37, p_OpenRequestBO.getJobRating()); 
			pst.setInt(38, p_OpenRequestBO.getPaymentMeter()); 
			pst.setString(39, p_OpenRequestBO.getCreatedBy());
			pst.setInt(40, p_OpenRequestBO.getTripSource());
			pst.setString(41, p_OpenRequestBO.getAirName()); 
			pst.setString(42, p_OpenRequestBO.getAirNo()); 
			pst.setString(43, p_OpenRequestBO.getAirFrom()); 
			pst.setString(44, p_OpenRequestBO.getAirTo());
			pst.setString(45, p_OpenRequestBO.getRefNumber()); 
			pst.setString(46, p_OpenRequestBO.getRefNumber1()); 
			pst.setString(47, p_OpenRequestBO.getRefNumber2()); 
			pst.setString(48, p_OpenRequestBO.getRefNumber3());
			resultValue = pst.executeUpdate();
			pst.close(); 

			pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS,ORL_MASTER_ASSOCCODE) VALUES (?,NOW(),?,?,?,?,?)");
			pst.setString(1, p_OpenRequestBO.getTripid());
			pst.setString(2, "Job Updated");
			pst.setString(3, adminBO.getUid());
			pst.setString(4, adminBO.getAssociateCode());
			pst.setInt(5, TDSConstants.updateJob);
			pst.setString(6, adminBO.getMasterAssociateCode());
			pst.executeUpdate();


			pst1=con.prepareStatement("INSERT INTO TDS_CONSOLE (C_TRIPID,C_TIME,C_REASON,C_CHANGED_BY,C_ASSOCCODE,C_STATUS,C_MASTER_ASSOCCODE) VALUES (?,NOW(),?,?,?,?,?)");
			pst1.setString(1, p_OpenRequestBO.getTripid());
			pst1.setString(2, "Job Updated");
			pst1.setString(3, adminBO.getUid());
			pst1.setString(4, adminBO.getAssociateCode());
			pst1.setInt(5, TDSConstants.updateJob);
			pst1.setString(6, adminBO.getMasterAssociateCode());
			pst1.executeUpdate();



			if(resultValue > 0) {
				cat.info("Successfully updated TDS_OPENREQUEST");
				//				pst=con.prepareStatement("UPDATE TDS_USERADDRESSMASTER SET CU_COMMENTS=? WHERE CU_PHONENO=? and CU_MASTERASSOCIATIONCODE=?");
				//				pst.setString(1, p_OpenRequestBO.getComments());
				//				pst.setString(2, TDSValidation.getDBPhoneFormat(p_OpenRequestBO.getPhone()));
				//				pst.setString(3, adminBO.getAssociateCode());
				//				pst.executeUpdate();
			} else {
				cat.info("Failed to updat TDS_OPENREQUEST");
			}
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateOpenRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	 public static int updateOpenRequestOnDriverAccept(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,ArrayList<String> assocode) {
		 String fleets ="";
		 if(assocode != null){
			 if(assocode.size() >0){
				 fleets = "(";
				 for(int j=0; j<assocode.size()-1;j++){
					 fleets = fleets + "'" + assocode.get(j) + "',";
				 }
				 fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			 } else {
				 fleets = "('"+adminBO.getAssociateCode()+"')";
			 }
		 } else {
			 fleets = "('"+adminBO.getAssociateCode()+"')";
		 }
		 int orBo = updateOpenRequestOnDriverAccept(p_OpenRequestBO,adminBO,fleets);
		 return orBo;
	 }

	
	public static int updateOpenRequestOnDriverAccept(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateOpenRequestOnDriverAccept");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(p_OpenRequestBO.getRouteNumber().equals("") || p_OpenRequestBO.getRouteNumber().equals("0")){
				pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_JOB_ACCEPT_TIME = NOW(), OR_DRIVERID = ?, OR_VEHICLE_NO = ?, OR_TRIP_STATUS = ?,OR_ASSOCCODE=?,OR_MASTER_ASSOCCODE=? WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets);
				pst.setString(1, p_OpenRequestBO.getDriverid());
				pst.setString(2, p_OpenRequestBO.getVehicleNo());
				pst.setString(3, TDSConstants.jobAllocated + "");
				pst.setString(4, p_OpenRequestBO.getAssociateCode());
				pst.setString(5, adminBO.getMasterAssociateCode());
				pst.setString(6, p_OpenRequestBO.getTripid());
			} else {
				pst = con.prepareStatement("UPDATE TDS_SHARED_RIDE SET SR_STATUS = ? WHERE SR_ROUTE_ID = ? AND SR_ASSOCCODE IN "+fleets);
				pst.setString(1, TDSConstants.jobAllocated + "");
				pst.setString(2, p_OpenRequestBO.getRouteNumber());
//				pst.setString(3, p_OpenRequestBO.getAssociateCode());
				pst.executeUpdate();

				pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_JOB_ACCEPT_TIME = NOW(), OR_DRIVERID = ?, OR_VEHICLE_NO = ?, OR_TRIP_STATUS = ?,OR_ASSOCCODE=?,OR_MASTER_ASSOCCODE=? WHERE OR_ROUTE_NUMBER = ? and OR_ASSOCCODE IN "+fleets);
				pst.setString(1, p_OpenRequestBO.getDriverid());
				pst.setString(2, p_OpenRequestBO.getVehicleNo());
				pst.setString(3, TDSConstants.jobAllocated + "");
				pst.setString(4, p_OpenRequestBO.getAssociateCode());
				pst.setString(5, adminBO.getMasterAssociateCode());
				pst.setString(6, p_OpenRequestBO.getRouteNumber());
			}


			cat.info(pst.toString());
			resultValue = pst.executeUpdate();

			pst.close();

		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateOpenRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}
	
	 public static int updateOpenRequestOnDriverReportingNoShow(OpenRequestBO p_OpenRequestBO, AdminRegistrationBO adminBO,ArrayList<String> assocode) {
		 String fleets ="";
		 if(assocode != null){
			 if(assocode.size() >0){
				 fleets = "(";
				 for(int j=0; j<assocode.size()-1;j++){
					 fleets = fleets + "'" + assocode.get(j) + "',";
				 }
				 fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			 } else {
				 fleets = "('"+adminBO.getAssociateCode()+"')";
			 }
		 } else {
			 fleets = "('"+adminBO.getAssociateCode()+"')";
		 }
		 int orBo = updateOpenRequestOnDriverReportingNoShow(p_OpenRequestBO,adminBO,fleets);
		 return orBo;
	 }

	public static int updateOpenRequestOnDriverReportingNoShow(OpenRequestBO p_OpenRequestBO, AdminRegistrationBO adminBO,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateOpenRequestOnDriverReportingNoShow");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try {
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets+" and OR_TRIP_STATUS IN (?,?,?) and OR_DRIVERID = ?");
			pst.setString(1, TDSConstants.driverReportedNoShow + "");
			pst.setString(2, p_OpenRequestBO.getTripid());
//			pst.setString(3, adminBO.getAssociateCode());
			pst.setString(3, TDSConstants.jobAllocated+"");
			pst.setString(4, TDSConstants.onRotueToPickup+"");			
			pst.setString(5, TDSConstants.onSite+"");			
			pst.setString(6, p_OpenRequestBO.getDriverid());

			//System.out.println(pst.toString());

			cat.info(pst.toString());
			resultValue = pst.executeUpdate();
			//System.out.println(resultValue);

			pst.close();

		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateOpenRequestOnDriverReportingNoShow-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateOpenRequestOnDriverReportingNoShow-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static int updateOpenRequestOnDriverOnRoute(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+adminBO.getAssociateCode()+"')";
			}
		} else {
			fleets = "('"+adminBO.getAssociateCode()+"')";
		}
		int result = updateOpenRequestOnDriverOnRoute(p_OpenRequestBO,adminBO,fleets);
		return result;
	}

	
	public static int updateOpenRequestOnDriverOnRoute(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateOpenRequestOnDriverOnRoute");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		//ResultSet rs= null;
		//String date[] = p_OpenRequestBO.getSdate().split("/");
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try {
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets+" and OR_TRIP_STATUS = ? and OR_DRIVERID = ?");
			pst.setString(1, TDSConstants.onRotueToPickup + "");
			pst.setString(2, p_OpenRequestBO.getTripid());
//			pst.setString(3, adminBO.getAssociateCode());
			pst.setString(3, TDSConstants.jobAllocated + "");
			pst.setString(4, p_OpenRequestBO.getDriverid());
			cat.info(pst.toString());
			resultValue = pst.executeUpdate();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateOpenRequestOnDriverOnRoute-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateOpenRequestOnDriverOnRoute-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static int updateOpenRequestOnDriverSTC(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+adminBO.getAssociateCode()+"')";
			}
		} else {
			fleets = "('"+adminBO.getAssociateCode()+"')";
		}
		int orBo = updateOpenRequestOnDriverSTC(p_OpenRequestBO,adminBO,fleets);
		return orBo;
	}

	public static int updateOpenRequestOnDriverSTC(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateOpenRequestOnDriverSTC");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		//ResultSet rs= null;
		//String date[] = p_OpenRequestBO.getSdate().split("/");
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try {
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets+" and OR_TRIP_STATUS = ? and OR_DRIVERID = ?");
			pst.setString(1, TDSConstants.jobSTC + "");
			pst.setString(2, p_OpenRequestBO.getTripid());
//			pst.setString(3, adminBO.getAssociateCode());
			pst.setString(3, TDSConstants.tripStarted + "");
			pst.setString(4, p_OpenRequestBO.getDriverid());
			cat.info(pst.toString());
			resultValue = pst.executeUpdate();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateOpenRequestOnDriverSTC-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateOpenRequestOnDriverSTC-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static int updateOpenRequestOnDriverReject(OpenRequestBO openRequestBO,AdminRegistrationBO adminBO,String driverId,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+adminBO.getAssociateCode()+"')";
			}
		} else {
			fleets = "('"+adminBO.getAssociateCode()+"')";
		}
		int orBo = updateOpenRequestOnDriverReject(openRequestBO,adminBO,driverId,fleets);
		return orBo;
	}

	
	public static int updateOpenRequestOnDriverReject(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,String driverId,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int resultValue = 0; 
		PreparedStatement pst = null; 
		TDSConnection dbcon = new TDSConnection(); 
		Connection con = dbcon.getConnection(); 
		try { 
			if(p_OpenRequestBO.getRouteNumber().equals("")){ 
				pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_DRIVERID = ?, OR_VEHICLE_NO = ?, OR_SMS_SENT = DATE_SUB(now(), INTERVAL 5 DAY) WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets+" and OR_DRIVERID = ?"); 
				pst.setString(1, p_OpenRequestBO.getDriverid()); 
				pst.setString(2, p_OpenRequestBO.getVehicleNo()); 
				pst.setString(3, p_OpenRequestBO.getTripid()); 
//				pst.setString(4, adminBO.getAssociateCode()); 
				pst.setString(4, driverId); 
			}else { 
				pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_DRIVERID = ?, OR_VEHICLE_NO = ?, OR_SMS_SENT = DATE_SUB(now(), INTERVAL 5 DAY) WHERE OR_ROUTE_NUMBER = ? and OR_ASSOCCODE IN "+fleets+" and OR_DRIVERID = ?"); 
				pst.setString(1, p_OpenRequestBO.getDriverid()); 
				pst.setString(2, p_OpenRequestBO.getVehicleNo()); 
				pst.setString(3, p_OpenRequestBO.getRouteNumber()); 
//				pst.setString(4, adminBO.getAssociateCode()); 
				pst.setString(4, driverId); 
			} 
			resultValue = pst.executeUpdate(); 
			pst.close(); 
		} catch(SQLException sqex) { 
			cat.error("TDSException RequestDAO.updateOpenRequestOnDriverReject-->"+sqex.getMessage()); 
			cat.error(pst.toString()); 
			//System.out.println("TDSException RequestDAO.updateOpenRequestOnDriverReject-->"+sqex.getMessage()); 
			sqex.printStackTrace(); 
		} finally { 

			dbcon.closeConnection(); 
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue; 
	}


	public static int updateOpenRequestOnDriverNoResponse(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int resultValue = 0; 
		PreparedStatement pst = null; 
		TDSConnection dbcon = new TDSConnection(); 
		Connection con = dbcon.getConnection(); 
		try { 
			if(p_OpenRequestBO.getRouteNumber().equals("")){ 
				pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_DRIVERID = ?, OR_VEHICLE_NO = ? WHERE OR_TRIP_ID = ? and OR_ASSOCCODE=?"); 
				pst.setString(1, p_OpenRequestBO.getDriverid()); 
				pst.setString(2, p_OpenRequestBO.getVehicleNo()); 
				pst.setString(3, p_OpenRequestBO.getTripid()); 
				pst.setString(4, adminBO.getAssociateCode()); 
			}else { 
				pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_DRIVERID = ?, OR_VEHICLE_NO = ? WHERE OR_ROUTE_NUMBER = ? and OR_ASSOCCODE=?"); 
				pst.setString(1, p_OpenRequestBO.getDriverid()); 
				pst.setString(2, p_OpenRequestBO.getVehicleNo()); 
				pst.setString(3, p_OpenRequestBO.getRouteNumber()); 
				pst.setString(4, adminBO.getAssociateCode()); 
			} 
			resultValue = pst.executeUpdate(); 
			pst.close(); 
		} catch(SQLException sqex) { 
			cat.error("TDSException RequestDAO.updateOpenRequestOnDriverReject-->"+sqex.getMessage()); 
			cat.error(pst.toString()); 
			//System.out.println("TDSException RequestDAO.updateOpenRequestOnDriverReject-->"+sqex.getMessage()); 
			sqex.printStackTrace(); 
		} finally { 

			dbcon.closeConnection(); 
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue; 
	} 


	public static int saveOpenRequest(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,int tripType) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.saveOpenRequest");
		int resultValue = 0;
		PreparedStatement pst = null;
		PreparedStatement pst1=null;
		String date[] = p_OpenRequestBO.getSdate().split("/");
		Timestamp dateTime=new Timestamp((long) (System.currentTimeMillis()));
		String splitTime[]=dateTime.toString().split("\\.");
		String time=(dateTime.getHours()<10?"0"+dateTime.getHours():dateTime.getHours())+""+(dateTime.getMinutes()<10?"0"+dateTime.getMinutes():dateTime.getMinutes());
		ResultSet rs;
		String processedDate=date[2]+"-"+date[0]+"-"+date[1]+" "+p_OpenRequestBO.getShrs().substring(0,2)+":"+p_OpenRequestBO.getShrs().substring(2,4)+":"+"00";
		String newTimeZone=adminBO.getTimeZone();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement(TDSSQLConstants.insertOpenRequest,Statement.RETURN_GENERATED_KEYS);
			if(p_OpenRequestBO.getShrs().equals("2525")){
				pst.setString(40, time);
				pst.setString(55, splitTime[0]); 
				Calendar now =Calendar.getInstance();
				TimeZone timezone=now.getTimeZone();
				pst.setString(56, timezone.getID()); 
				//pst.setString(56, "UTC"); 
			} else {
				pst.setString(40, p_OpenRequestBO.getShrs());
				pst.setString(55, processedDate); 
				pst.setString(56, newTimeZone); 
			}
			//System.out.println("before insert: "+p_OpenRequestBO.getScity());
			pst.setString(1, TDSValidation.getDBPhoneFormat(p_OpenRequestBO.getPhone()));
			pst.setString(2, p_OpenRequestBO.getSadd1());
			pst.setString(3, p_OpenRequestBO.getSadd2());
			pst.setString(4, p_OpenRequestBO.getScity());
			pst.setString(5, p_OpenRequestBO.getSstate());
			pst.setString(6, p_OpenRequestBO.getSzip());
			pst.setString(7, p_OpenRequestBO.getEadd1()); 
			pst.setString(8, p_OpenRequestBO.getEadd2());
			pst.setString(9, p_OpenRequestBO.getEcity());
			pst.setString(10, p_OpenRequestBO.getEstate());
			pst.setString(11, p_OpenRequestBO.getEzip());
			pst.setString(12, p_OpenRequestBO.getAssociateCode()!="" ?p_OpenRequestBO.getAssociateCode():"");
			pst.setString(13, p_OpenRequestBO.getSlat()!="" ?p_OpenRequestBO.getSlat():"0");
			pst.setString(14, p_OpenRequestBO.getSlong()!="" ?p_OpenRequestBO.getSlong():"0");
			pst.setString(15, p_OpenRequestBO.getEdlatitude()!="" ?p_OpenRequestBO.getEdlatitude():"0");
			pst.setString(16, p_OpenRequestBO.getEdlongitude()!="" ?p_OpenRequestBO.getEdlongitude():"0");
			pst.setString(17, p_OpenRequestBO.getQueueno()!=""?p_OpenRequestBO.getQueueno():"");
			pst.setString(18, p_OpenRequestBO.getName()!=""?p_OpenRequestBO.getName():"");
			pst.setInt(19, p_OpenRequestBO.getChckTripStatus());
			pst.setString(20, p_OpenRequestBO.getSlandmark());
			pst.setString(21, p_OpenRequestBO.getSintersection());
			pst.setString(22, p_OpenRequestBO.getDrProfile()+p_OpenRequestBO.getVecProfile());
			pst.setString(23, p_OpenRequestBO.getSpecialIns());
			pst.setInt(24, p_OpenRequestBO.getDontDispatch());
			pst.setString(25, p_OpenRequestBO.getPaytype());
			pst.setString(26, p_OpenRequestBO.getAcct());
			pst.setBigDecimal(27, p_OpenRequestBO.getAmt());
			pst.setString(28,p_OpenRequestBO.getDriverid());
			pst.setString(29, p_OpenRequestBO.getComments());
			pst.setInt(30, p_OpenRequestBO.getTripSource());
			pst.setString(31, p_OpenRequestBO.getCreatedBy());
			pst.setString(32, p_OpenRequestBO.getVehicleNo());
			pst.setLong(33,p_OpenRequestBO.getMultiJobTag());
			pst.setString(34, p_OpenRequestBO.getElandmark());
			pst.setString(35, p_OpenRequestBO.getTypeOfRide());
			pst.setInt(36, p_OpenRequestBO.getNumOfPassengers());
			pst.setString(37, p_OpenRequestBO.getDropTime());
			pst.setString(38, p_OpenRequestBO.getAdvanceTime());
			pst.setInt(39,p_OpenRequestBO.getPremiumCustomer());
			pst.setString(41, p_OpenRequestBO.getEmail());
			pst.setString(42, p_OpenRequestBO.getEndQueueno());
			pst.setString(43, p_OpenRequestBO.getRouteNumber()==null?"":p_OpenRequestBO.getRouteNumber());
			pst.setString(44, p_OpenRequestBO.getTripid()==null?"":p_OpenRequestBO.getTripid());
			pst.setInt(45, p_OpenRequestBO.getPaymentStatus());
			pst.setString(46, adminBO.getMasterAssociateCode());
			pst.setString(47, p_OpenRequestBO.getCallerPhone());
			pst.setString(48, p_OpenRequestBO.getCallerName());
			pst.setString(49, p_OpenRequestBO.getRefNumber());
			pst.setString(50, p_OpenRequestBO.getRefNumber1());
			pst.setString(51, p_OpenRequestBO.getRefNumber2());
			pst.setString(52, p_OpenRequestBO.getRefNumber3());
			pst.setInt(53, p_OpenRequestBO.getJobRating());
			pst.setInt(54, p_OpenRequestBO.getPaymentMeter());
			pst.setString(57, p_OpenRequestBO.getCustomField());
			pst.setString(58, p_OpenRequestBO.getAirName());
			pst.setString(59, p_OpenRequestBO.getAirNo());
			pst.setString(60, p_OpenRequestBO.getAirFrom());
			pst.setString(61, p_OpenRequestBO.getAirTo());
			//System.out.println("Trip Insert:"+pst.toString());
			
			pst.execute();
			rs = pst.getGeneratedKeys();
			if(rs.next()){
				resultValue = rs.getInt(1);
			}
			//System.out.println("Query"+rs);
			if(resultValue > 0) {
				cat.info("Successfully updated TDS_OPENREQUEST");
			} else {
				cat.info("Failed to updat TDS_OPENREQUEST");
			}

			pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS,ORL_MASTER_ASSOCCODE) VALUES (?,NOW(),?,?,?,?,?)");
			pst.setInt(1, resultValue);
			pst.setString(2, "Job Created");
			pst.setString(3, adminBO.getUid());
			pst.setString(4, adminBO.getAssociateCode());
			pst.setInt(5, TDSConstants.insertJob);
			pst.setString(6, adminBO.getMasterAssociateCode());
			pst.executeUpdate();


			pst1=con.prepareStatement("INSERT INTO TDS_CONSOLE (C_TRIPID,C_TIME,C_REASON,C_CHANGED_BY,C_ASSOCCODE,C_STATUS,C_MASTER_ASSOCCODE) VALUES (?,NOW(),?,?,?,?,?)");
			pst1.setInt(1, resultValue);
			pst1.setString(2, "Job Created");
			pst1.setString(3, adminBO.getUid());
			pst1.setString(4, adminBO.getAssociateCode());
			pst1.setInt(5, TDSConstants.insertJob);
			pst1.setString(6, adminBO.getMasterAssociateCode());
			pst1.executeUpdate();


			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.saveOpenRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.saveOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		
		System.out.println("Trip-Id:"+resultValue);
		return resultValue;
	}


	public static int saveOpenRequestHistory(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.saveOpenRequestHistory");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement pst1=null;
		String keygen;
		int keyValue = 0;
		String date[] = p_OpenRequestBO.getSdate().split("/");

		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement(TDSSQLConstants.insertOpenRequestHistory);
			pst.setString(1, p_OpenRequestBO.getTripid());
			pst.setString(2, TDSValidation.getDBPhoneFormat(p_OpenRequestBO.getPhone()));
			pst.setString(3, p_OpenRequestBO.getSadd1());
			pst.setString(4, p_OpenRequestBO.getSadd2());
			pst.setString(5, p_OpenRequestBO.getScity());
			pst.setString(6, p_OpenRequestBO.getSstate());
			pst.setString(7, p_OpenRequestBO.getSzip());
			pst.setString(8, p_OpenRequestBO.getEadd1()); 
			pst.setString(9, p_OpenRequestBO.getEadd2());
			pst.setString(10, p_OpenRequestBO.getEcity());
			pst.setString(11, p_OpenRequestBO.getEstate());
			pst.setString(12, p_OpenRequestBO.getEzip());
			pst.setString(13, p_OpenRequestBO.getShrs());
			pst.setString(14, (date[2]+"-"+date[0]+"-"+date[1]+" "+p_OpenRequestBO.getShrs().substring(0,2)+":"+p_OpenRequestBO.getShrs().substring(2,4)+":"+"00")); 
			pst.setString(15, adminBO.getTimeZone());
			/*			pst.setString(16, p_OpenRequestBO.getSaircode());
			pst.setString(17, p_OpenRequestBO.getEaircode());
			 */			pst.setString(16, p_OpenRequestBO.getAssociateCode()!="" ?p_OpenRequestBO.getAssociateCode():"");
			 pst.setString(17, p_OpenRequestBO.getSlat()!="" ?p_OpenRequestBO.getSlat():"0");
			 pst.setString(18, p_OpenRequestBO.getSlong()!="" ?p_OpenRequestBO.getSlong():"0");
			 pst.setString(19, p_OpenRequestBO.getEdlatitude()!="" ?p_OpenRequestBO.getEdlatitude():"0");
			 pst.setString(20, p_OpenRequestBO.getEdlongitude()!="" ?p_OpenRequestBO.getEdlongitude():"0");
			 pst.setString(21, p_OpenRequestBO.getQueueno()!=""?p_OpenRequestBO.getQueueno():"");
			 pst.setString(22, p_OpenRequestBO.getName()!=""?p_OpenRequestBO.getName():"");
			 pst.setInt(23, p_OpenRequestBO.getChckTripStatus());
			 pst.setString(24, p_OpenRequestBO.getSlandmark());
			 pst.setString(25, p_OpenRequestBO.getSintersection());
			 pst.setString(26, p_OpenRequestBO.getDrProfile()+p_OpenRequestBO.getVecProfile());
			 pst.setString(27, p_OpenRequestBO.getSpecialIns());
			 pst.setString(28, p_OpenRequestBO.getDispatchStatus());
			 pst.setString(29, p_OpenRequestBO.getPaytype());
			 pst.setString(30, p_OpenRequestBO.getAcct());
			 pst.setBigDecimal(31, p_OpenRequestBO.getAmt());
			 pst.setString(32,p_OpenRequestBO.getDriverid());
			 pst.setString(33, p_OpenRequestBO.getComments());
			 pst.setInt(34, p_OpenRequestBO.getTripSource());
			 pst.setString(35,p_OpenRequestBO.getAirName());
			 pst.setString(36, p_OpenRequestBO.getAirNo());
			 pst.setString(37, p_OpenRequestBO.getAirFrom());
			 pst.setString(38, p_OpenRequestBO.getAirTo());
			 cat.info("Query in open RequestHistory "+pst);
			 resultValue = pst.executeUpdate();

			 pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE) VALUES (?,NOW(),?,?,?)");
			 pst.setString(1, p_OpenRequestBO.getTripid());
			 pst.setString(2, "Job Updated");
			 pst.setString(3, adminBO.getUid());
			 pst.setString(4, adminBO.getAssociateCode());
			 pst.executeUpdate();


			 pst1=con.prepareStatement("INSERT INTO TDS_CONSOLE (C_TRIPID,C_TIME,C_REASON,C_CHANGED_BY,C_ASSOCCODE) VALUES (?,NOW(),?,?,?)");
			 pst1.setString(1, p_OpenRequestBO.getTripid());
			 pst1.setString(2, "Job Updated");
			 pst1.setString(3, adminBO.getUid());
			 pst1.setString(4, adminBO.getAssociateCode());
			 pst1.executeUpdate();



			 if(resultValue > 0) {
				 cat.info("Successfully insert TDS_OPENREQUEST_HISTORY");
			 } else {
				 cat.info("Failed to updat TDS_OPENREQUEST_HISTORY");
			 }

			 con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.saveOpenRequestHistory-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.saveOpenRequestHistory-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}
	public static int updateOpenRequestHistory(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateOpenRequestHistory");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		String keygen;
		int keyValue = 0;
		String date[] = p_OpenRequestBO.getSdate().split("/");

		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement(TDSSQLConstants.updateOpenRequestHistoryOnComplete);
			pst.setString(21, p_OpenRequestBO.getEdlatitude()!="" ?p_OpenRequestBO.getEdlatitude():"0");
			pst.setString(22, p_OpenRequestBO.getEdlongitude()!="" ?p_OpenRequestBO.getEdlongitude():"0");


			pst.setString(1, p_OpenRequestBO.getTripid());
			pst.setString(18, p_OpenRequestBO.getAssociateCode()!="" ?p_OpenRequestBO.getAssociateCode():"");

			cat.info("Query in open RequestHistory "+pst);
			resultValue = pst.executeUpdate();
			if(resultValue > 0) {
				cat.info("Successfully insert TDS_OPENREQUEST_HISTORY");
			} else {
				cat.info("Failed to updat TDS_OPENREQUEST_HISTORY");
			}

			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.saveOpenRequestHistory-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.saveOpenRequestHistory-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static OpenRequestBO getOpenRequestStatus(String tripId,AdminRegistrationBO adminBo,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+adminBo.getAssociateCode()+"')";
			}
		} else {
			fleets = "('"+adminBo.getAssociateCode()+"')";
		}
		OpenRequestBO orBo = getOpenRequestStatus(tripId,adminBo,fleets);
		return orBo;
	}

	
	public static OpenRequestBO getOpenRequestStatus(String tripId,AdminRegistrationBO adminBo,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequestStatus");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null, pst1=null;
		ResultSet rs, rs1 ;
		String query="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		OpenRequestBO opRequestBO= null;
		try {
			pst = con.prepareStatement("SELECT OR_CREATED_BY,OR_TRIP_SOURCE,OR_TRIP_ID,OR_TRIP_STATUS,OR_AMT,OR_TRIP_STATUS,OR_QUEUENO,OR_END_QUEUENO,OR_DRCABFLAG,OR_SMS_SENT,OR_DRIVERID,OR_DISPATCH_HISTORY_DRIVERS,OR_PHONE,OR_STADD1,OR_SHAREDRIDE_PAYTYPE,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_SPLINS,OR_PAYACC,OR_PAYTYPE,OR_ROUTE_NUMBER,OR_DISPATCH_LEAD_TIME,"+
					"OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_SHARED_RIDE,OR_ELANDMARK,OR_EDSTATE,OR_NAME,OR_EDZIP,OR_REPEAT_GROUP,OR_PHONE,OR_STLATITUDE,OR_COMMENTS,OR_STLONGITUDE,OR_EDLATITUDE,OR_DRIVERID,OR_VEHICLE_NO,OR_EDLONGITUDE,OR_LANDMARK,OR_DRCABFLAG,OR_QUEUENO,OR_EMAIL,OR_METER_TYPE," +"DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%H%i') "+"as TIME, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"') AS OFFSET_DATETIME,OR_EMAIL,OR_AIRLINE_NAME,OR_AIRLINE_NO,OR_AIRLINE_FROM,OR_AIRLINE_TO,OR_NUMBER_OF_PASSENGERS,VC_COMPANYNAME FROM TDS_OPENREQUEST LEFT JOIN TDS_GOVT_VOUCHER ON OR_MASTER_ASSOCCODE=VC_ASSOCCODE AND VC_VOUCHERNO=OR_PAYACC WHERE OR_TRIP_ID=? AND OR_ASSOCCODE IN "+fleets+" AND OR_DRIVERID =?");
			pst.setString(1,tripId);
//			pst.setString(2,adminBo.getAssociateCode());
			pst.setString(2,adminBo.getUid());
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				opRequestBO = new OpenRequestBO();
				opRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				opRequestBO.setPhone(rs.getString("OR_PHONE"));
				opRequestBO.setName(rs.getString("OR_NAME"));
				opRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("DATE")));
				opRequestBO.setShrs((rs.getString("TIME")));
				opRequestBO.setPaytype((rs.getString("OR_PAYTYPE")));
				opRequestBO.setAcct(rs.getString("OR_PAYACC"));
				opRequestBO.setAcctName(rs.getString("VC_COMPANYNAME"));
				opRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				opRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				opRequestBO.setComments(rs.getString("OR_COMMENTS"));
				opRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				opRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				opRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				opRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				opRequestBO.setStartTimeStamp(rs.getString("OFFSET_DATETIME"));
				opRequestBO.setChckTripStatus(rs.getInt("OR_TRIP_STATUS"));
				opRequestBO.setOR_SMS_SENT(rs.getString("OR_SMS_SENT"));
				opRequestBO.setHistoryOfDrivers(rs.getString("OR_DISPATCH_HISTORY_DRIVERS"));
				opRequestBO.setSadd1(rs.getString("OR_STADD1"));
				opRequestBO.setSadd2(rs.getString("OR_STADD2"));
				opRequestBO.setScity(rs.getString("OR_STCITY"));
				opRequestBO.setSstate(rs.getString("OR_STSTATE"));
				opRequestBO.setSzip(rs.getString("OR_STZIP"));
				opRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				opRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				opRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				opRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				opRequestBO.setEcity(rs.getString("OR_EDCITY"));
				opRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				opRequestBO.setEzip(rs.getString("OR_EDZIP"));
				opRequestBO.setEmail(rs.getString("OR_EMAIL"));
				opRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				opRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				opRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
				opRequestBO.setRepeatGroup(rs.getString("OR_REPEAT_GROUP"));
				opRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
				opRequestBO.setElandmark(rs.getString("OR_ELANDMARK"));
				opRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				opRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				opRequestBO.setDispatchLeadTime(rs.getInt("OR_DISPATCH_LEAD_TIME"));
				opRequestBO.setPaymentMeter(rs.getInt("OR_METER_TYPE"));
				opRequestBO.setTripSource(rs.getInt("OR_TRIP_SOURCE"));
				opRequestBO.setCreatedBy(rs.getString("OR_CREATED_BY"));
				opRequestBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
				opRequestBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
				opRequestBO.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
				opRequestBO.setSharedRidePaymentType(rs.getInt("OR_SHAREDRIDE_PAYTYPE"));
				opRequestBO.setAirTo(rs.getString("OR_AIRLINE_TO"));
				opRequestBO.setEndQueueno(rs.getString("OR_END_QUEUENO"));
				opRequestBO.setEmail(rs.getString("OR_EMAIL"));
				opRequestBO.setNumOfPassengers(rs.getString("OR_NUMBER_OF_PASSENGERS")!=null&&!rs.getString("OR_NUMBER_OF_PASSENGERS").equals("")?Integer.parseInt(rs.getString("OR_NUMBER_OF_PASSENGERS")):1);
				if(rs.getInt("OR_METER_TYPE")==0){
					query = " AND MD_DEFAULT=1";
				} else {
					query = " AND MD_KEY='"+rs.getInt("OR_METER_TYPE")+"'";
				}
				pst1=con.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+adminBo.getMasterAssociateCode()+"'"+query);
				rs1=pst1.executeQuery();
				if(rs1.next()){
					opRequestBO.setRatePerMile(Double.parseDouble(rs1.getString("MD_RATE_PER_MILE")));
					opRequestBO.setRatePerMin(Double.parseDouble(rs1.getString("MD_RATE_PER_MIN")));
					opRequestBO.setStartAmt(Double.parseDouble(rs1.getString("MD_START_AMOUNT")));
					opRequestBO.setMinSpeed(Integer.parseInt(rs1.getString("MD_MINIMUM_SPEED")));
				}
			} else {
				pst = con.prepareStatement("SELECT ORH_CREATED_BY,ORH_TRIP_SOURCE,ORH_TRIP_ID,ORH_TRIP_STATUS,ORH_AMT,ORH_TRIP_STATUS,ORH_QUEUENO,ORH_DRCABFLAG,ORH_SMS_SENT,ORH_DRIVERID,ORH_DISPATCH_HISTORY_DRIVERS,ORH_PHONE,ORH_STADD1,ORH_STADD2,ORH_STCITY,ORH_STSTATE,ORH_STZIP,ORH_SPLINS,ORH_PAYACC,ORH_PAYTYPE,"+
						"ORH_EDADD1,ORH_EDADD2,ORH_EDCITY,ORH_SHARED_RIDE,ORH_ELANDMARK,ORH_EDSTATE,ORH_NAME,ORH_EDZIP,ORH_REPEAT_GROUP,ORH_PHONE,ORH_STLATITUDE,ORH_OPERATOR_COMMENTS,ORH_STLONGITUDE,ORH_EDLATITUDE,ORH_DRIVERID,ORH_VEHICLE_NO,ORH_EDLONGITUDE,ORH_LANDMARK,ORH_DRCABFLAG,ORH_QUEUENO," +	"DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%H%i') "+"as TIME, CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"') AS OFFSET_DATETIME,ORH_AIRLINE_NAME,ORH_AIRLINE_NO,ORH_AIRLINE_FROM,ORH_AIRLINE_TO  FROM TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_ID=? AND ORH_ASSOCCODE IN "+fleets+" AND ORH_DRIVERID =?");
				pst.setString(1,tripId);
//				pst.setString(2,adminBo.getAssociateCode());
				pst.setString(2,adminBo.getUid());
				cat.info(pst.toString());
				rs=pst.executeQuery();
				if(rs.next()){
					opRequestBO = new OpenRequestBO();
					opRequestBO.setTripid(rs.getString("ORH_TRIP_ID"));
					opRequestBO.setPhone(rs.getString("ORH_PHONE"));
					opRequestBO.setName(rs.getString("ORH_NAME"));
					opRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("DATE")));
					opRequestBO.setShrs((rs.getString("TIME")));
					opRequestBO.setPaytype((rs.getString("ORH_PAYTYPE")));
					opRequestBO.setAcct(rs.getString("ORH_PAYACC"));
					opRequestBO.setSlandmark(rs.getString("ORH_LANDMARK"));
					opRequestBO.setSpecialIns(rs.getString("ORH_SPLINS"));
					opRequestBO.setComments(rs.getString("ORH_OPERATOR_COMMENTS"));
					opRequestBO.setVehicleNo(rs.getString("ORH_VEHICLE_NO"));
					opRequestBO.setDriverid(rs.getString("ORH_DRIVERID"));
					opRequestBO.setQueueno(rs.getString("ORH_QUEUENO"));
					opRequestBO.setDrProfile(rs.getString("ORH_DRCABFLAG"));
					opRequestBO.setStartTimeStamp(rs.getString("OFFSET_DATETIME"));
					opRequestBO.setChckTripStatus(rs.getInt("ORH_TRIP_STATUS"));
					opRequestBO.setOR_SMS_SENT(rs.getString("ORH_SMS_SENT"));
					opRequestBO.setHistoryOfDrivers(rs.getString("ORH_DISPATCH_HISTORY_DRIVERS"));
					opRequestBO.setSadd1(rs.getString("ORH_STADD1"));
					opRequestBO.setSadd2(rs.getString("ORH_STADD2"));
					opRequestBO.setScity(rs.getString("ORH_STCITY"));
					opRequestBO.setSstate(rs.getString("ORH_STSTATE"));
					opRequestBO.setSzip(rs.getString("ORH_STZIP"));
					opRequestBO.setSlat(rs.getString("ORH_STLATITUDE"));
					opRequestBO.setSlong(rs.getString("ORH_STLONGITUDE"));
					opRequestBO.setEadd1(rs.getString("ORH_EDADD1"));
					opRequestBO.setEadd2(rs.getString("ORH_EDADD2"));
					opRequestBO.setEcity(rs.getString("ORH_EDCITY"));
					opRequestBO.setEstate(rs.getString("ORH_EDSTATE"));
					opRequestBO.setEzip(rs.getString("ORH_EDZIP"));
					opRequestBO.setEdlatitude(rs.getString("ORH_EDLATITUDE"));
					opRequestBO.setEdlongitude(rs.getString("ORH_EDLONGITUDE"));
					opRequestBO.setTypeOfRide(rs.getString("ORH_SHARED_RIDE"));
					opRequestBO.setRepeatGroup(rs.getString("ORH_REPEAT_GROUP"));
					opRequestBO.setAmt(new BigDecimal(rs.getString("ORH_AMT")));
					opRequestBO.setElandmark(rs.getString("ORH_ELANDMARK"));
					opRequestBO.setTripStatus(rs.getString("ORH_TRIP_STATUS"));
					opRequestBO.setTripSource(rs.getInt("ORH_TRIP_SOURCE"));
					opRequestBO.setCreatedBy(rs.getString("ORH_CREATED_BY"));
					opRequestBO.setAirName(rs.getString("ORH_AIRLINE_NAME"));
					opRequestBO.setAirNo(rs.getString("ORH_AIRLINE_NO"));
					opRequestBO.setAirFrom(rs.getString("ORH_AIRLINE_FROM"));
					opRequestBO.setAirTo(rs.getString("ORH_AIRLINE_TO"));

				}	
			}
			cat.info("Query in open RequestHistory "+pst);
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.getOpenRequestStatus-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getOpenRequestStatus-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return opRequestBO;
	}


	public static int deleteOpenRequest(String p_tripid,String Assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.deleteOpenRequest");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.deleteOpenRequest);
			pst.setString(1, p_tripid);
			pst.setString(2, Assoccode);
			cat.info(pst.toString());
			resultValue = pst.executeUpdate();
			pst.close();
			//con.close();
			if(resultValue > 0) {
				cat.info("Successfully DELETED TDS_OPENREQUEST");
			} else {
				cat.info("Failed to DELETE TDS_OPENREQUEST");
			}
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.deleteOpenRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.deleteOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static ArrayList getOpenRequest(OpenRequestBO p_openRequest,String askTrip) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequest");
		ArrayList al_list = new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null ;
		ResultSet rs = null; 
		OpenRequestBO openRequestBO = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query = ""; 
			if(p_openRequest.getTripid() != "") {
				query += " AND OR_TRIP_ID='"+p_openRequest.getTripid()+"'"; 
			} 
			if(p_openRequest.getPhone() != "") {
				query += " AND OR_PHONE='"+ TDSValidation.getDBPhoneFormat(p_openRequest.getPhone())+"'";
			}
			if(p_openRequest.getSdate() != "") {
				query += " AND DATEDIFF(OR_SERVICEDATE,'"+TDSValidation.getDBdateFormat(p_openRequest.getSdate())+"') = 0";
			} 
			if(p_openRequest.getScity() != "") {
				query += " AND OR_STCITY ='"+p_openRequest.getScity()+"'";
			}
			if(p_openRequest.getSstate() != "") {
				query += " AND OR_STSTATE='"+p_openRequest.getSstate() +"'";
			}
			if(p_openRequest.getSzip() != "") {
				query += " AND OR_STZIP ='"+p_openRequest.getSzip()+"'";
			} 
			if(p_openRequest.getDriverid() != "" && p_openRequest.getTripid() == "") {
				query = " AND OR_TRIP_STATUS = 'D' AND OR_DRIVERID = '"+p_openRequest.getDriverid()+"'";
			}

			if(askTrip.equalsIgnoreCase("T")) {
				pst = con.prepareStatement(TDSSQLConstants.getOpenRequestMTrip+" and  case OR_TRIP_STATUS when 'B' then 1 when '' then 0 else OR_DRIVERID="+p_openRequest.getDriverid()+" end "+query);

			} else {
				pst = con.prepareStatement(TDSSQLConstants.getOpenRequestCriteria+query);
			}

			cat.info("Query in RequestDAO getOpenRequest "+pst);
			rs = pst.executeQuery();
			while(rs.next()) {
				openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				//openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setPhone(rs.getString("OR_PHONE"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(rs.getString("OR_STADD2"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setSstate(rs.getString("OR_STSTATE"));
				openRequestBO.setSzip(rs.getString("OR_STZIP"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBO.setEzip(rs.getString("OR_EDZIP"));
				//openRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("OR_SERVICEDATE")));
				openRequestBO.setSdate(rs.getString("OR_SERVICEDATE"));
				openRequestBO.setShrs(rs.getString("OR_SERVICETIME"));
				/*openRequestBO.setSmin(rs.getString("OR_SERVICETIME").substring(2,4));*/
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME")));
				/*				openRequestBO.setSaircode(rs.getString("OR_SAIRCODE"));
				openRequestBO.setEaircode(rs.getString("OR_EAIRCODE"));
				 */				openRequestBO.setAssociateCode(rs.getString("OR_ASSOCCODE"));
				 openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				 openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				 openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				 openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				 openRequestBO.setName(rs.getString("OR_NAME"));
				 openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				 openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				 openRequestBO.setSintersection(rs.getString("OR_INTERSECTION"));
				 openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));


				 al_list.add(openRequestBO);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getOpenRequest "+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getOpenRequest "+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}
	public static OpenRequestBO getOpenRequestOfferedTip(OpenRequestBO openRequest) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequestOfferedTip");
		ArrayList al_list = new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null ;
		ResultSet rs = null; 
		OpenRequestBO openRequestBO = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST WHERE OR_TRIP_ID=? AND OR_DRIVERID=? and OR_ASSOCCODE=? AND OR_TRIP_STATUS ='"+TDSConstants.performingDispatchProcesses+"'");
			pst.setString(1, openRequest.getTripid());
			pst.setString(2, openRequest.getDriverid());
			pst.setString(3, openRequest.getAssociateCode());

			cat.info("Query in RequestDAO getOpenRequestOffered "+pst);
			rs = pst.executeQuery();
			while(rs.next()) {
				openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setPhone(rs.getString("OR_PHONE"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(rs.getString("OR_STADD2"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setSstate(rs.getString("OR_STSTATE"));
				openRequestBO.setSzip(rs.getString("OR_STZIP"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBO.setEzip(rs.getString("OR_EDZIP"));
				openRequestBO.setSdate(rs.getString("OR_SERVICEDATE"));
				openRequestBO.setShrs(rs.getString("OR_SERVICETIME"));
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME")));
				/*				openRequestBO.setSaircode(rs.getString("OR_SAIRCODE"));
				openRequestBO.setEaircode(rs.getString("OR_EAIRCODE"));
				 */				openRequestBO.setAssociateCode(rs.getString("OR_ASSOCCODE"));
				 openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				 openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				 openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				 openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				 openRequestBO.setName(rs.getString("OR_NAME"));
				 openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				 openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				 openRequestBO.setSintersection(rs.getString("OR_INTERSECTION"));
				 openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				 openRequestBO.setStartTimeStamp(rs.getString("OR_SERVICEDATE"));


				 al_list.add(openRequestBO);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getOpenRequest "+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getOpenRequest "+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openRequestBO;
	}


	public static ArrayList<OpenRequestBO> getOpenRequest(String assoccode,String timeOffset,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequest");
		ArrayList<OpenRequestBO> al_openRequest = new ArrayList<OpenRequestBO>();
		OpenRequestBO openRequest;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getOpenRequestedXml);
			pst.setString(1, timeOffset);
			pst.setString(2, timeOffset);
			pst.setString(3, timeOffset);
			pst.setString(4, assoccode);
			pst.setString(5, masterAssoccode);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			//String time;
			while(rs.next()) {
				openRequest = new OpenRequestBO();
				openRequest.setName(rs.getString("OR_NAME"));
				openRequest.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequest.setTripid(rs.getString("OR_TRIP_ID"));
				openRequest.setSadd1(rs.getString("OR_STADD1"));
				openRequest.setSadd2(rs.getString("OR_STADD2"));
				openRequest.setScity(rs.getString("OR_STCITY"));
				openRequest.setSstate(rs.getString("OR_STSTATE"));
				openRequest.setEcity(rs.getString("OR_EDCITY"));
				openRequest.setEstate(rs.getString("OR_EDSTATE"));
				openRequest.setSttime(rs.getString("Service_Time"));
				openRequest.setSdate(rs.getString("Service_Date"));
				openRequest.setSlat(rs.getString("OR_STLATITUDE"));
				openRequest.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequest.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequest.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequest.setStartTimeStamp(rs.getString("TMP"));
				openRequest.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				openRequest.setAirName(rs.getString("OR_AIRLINE_NAME"));
				openRequest.setAirNo(rs.getString("OR_AIRLINE_NO"));
				openRequest.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
				openRequest.setAirTo(rs.getString("OR_AIRLINE_TO"));
				openRequest.setSharedRidePaymentType(rs.getInt("OR_SHAREDRIDE_PAYTYPE"));
				openRequest.setQueueno(rs.getString("OR_QUEUENO"));
				openRequest.setEndQueueno(rs.getString("OR_END_QUEUENO"));
				openRequest.setEmail(rs.getString("OR_EMAIL"));
				al_openRequest.add(openRequest);
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getOpenRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_openRequest;
	}



	public static OpenRequestBO getOpenRequestByTripID(String tripId, String assoccode,String timeOffset) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequestByTripID");
		OpenRequestBO opRequestBO = null;
		//TDSConnection dbcon = null ;
		//Connection con = null;
		String query="";
		PreparedStatement pst = null,pst1=null;
		ResultSet rs = null,rs1=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String tripid[]=tripId.split(";");

		try {
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];

				pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_METER_TYPE,OR_TRIP_STATUS,OR_AMT,OR_TRIP_STATUS,OR_QUEUENO,OR_DRCABFLAG,OR_SMS_SENT,OR_DRIVERID,OR_DISPATCH_HISTORY_DRIVERS,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_SPLINS,OR_PAYACC,OR_PAYTYPE,OR_ROUTE_NUMBER,OR_DISPATCH_LEAD_TIME,"+
						"OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_SHARED_RIDE,OR_ELANDMARK,OR_EDSTATE,OR_NAME,OR_EDZIP,OR_REPEAT_GROUP,OR_PHONE,OR_STLATITUDE,OR_COMMENTS,OR_STLONGITUDE,OR_EDLATITUDE,OR_DRIVERID,OR_VEHICLE_NO,OR_EDLONGITUDE,OR_LANDMARK,OR_DRCABFLAG,OR_QUEUENO," +	"DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"'),'%H%i') "+"as TIME, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"') AS OFFSET_DATETIME,OR_AIRLINE_NAME,OR_AIRLINE_NO,OR_AIRLINE_FROM,OR_AIRLINE_TO, "
								+ " time_to_sec(timediff(OR_SERVICEDATE,now())) as st_time_diff" 
								+ " FROM TDS_OPENREQUEST WHERE OR_TRIP_ID=? AND OR_ASSOCCODE=?");

				pst.setString(1, trip);
				pst.setString(2, assoccode);
				//System.out.println("str : "+pst.toString());
				rs = pst.executeQuery();
				//String time;
				if(rs.next()) {
					opRequestBO = new OpenRequestBO();
					opRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
					opRequestBO.setPhone(rs.getString("OR_PHONE"));
					opRequestBO.setName(rs.getString("OR_NAME"));
					//opRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("DATE")));
					//opRequestBO.setShrs((rs.getString("TIME")));
					opRequestBO.setPaytype((rs.getString("OR_PAYTYPE")));
					opRequestBO.setAcct(rs.getString("OR_PAYACC"));
					opRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
					opRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
					opRequestBO.setComments(rs.getString("OR_COMMENTS"));
					opRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
					opRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
					opRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
					opRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
					opRequestBO.setStartTimeStamp(rs.getString("OFFSET_DATETIME"));
					opRequestBO.setChckTripStatus(rs.getInt("OR_TRIP_STATUS"));
					opRequestBO.setOR_SMS_SENT(rs.getString("OR_SMS_SENT"));
					opRequestBO.setHistoryOfDrivers(rs.getString("OR_DISPATCH_HISTORY_DRIVERS"));
					opRequestBO.setSadd1(rs.getString("OR_STADD1"));
					opRequestBO.setSadd2(rs.getString("OR_STADD2"));
					opRequestBO.setScity(rs.getString("OR_STCITY"));
					opRequestBO.setSstate(rs.getString("OR_STSTATE"));
					opRequestBO.setSzip(rs.getString("OR_STZIP"));
					opRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
					opRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
					opRequestBO.setEadd1(rs.getString("OR_EDADD1"));
					opRequestBO.setEadd2(rs.getString("OR_EDADD2"));
					opRequestBO.setEcity(rs.getString("OR_EDCITY"));
					opRequestBO.setEstate(rs.getString("OR_EDSTATE"));
					opRequestBO.setEzip(rs.getString("OR_EDZIP"));
					opRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
					opRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
					opRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
					opRequestBO.setRepeatGroup(rs.getString("OR_REPEAT_GROUP"));
					opRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
					opRequestBO.setElandmark(rs.getString("OR_ELANDMARK"));
					opRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
					opRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
					opRequestBO.setDispatchLeadTime(rs.getInt("OR_DISPATCH_LEAD_TIME"));
					opRequestBO.setPaymentMeter(rs.getInt("OR_METER_TYPE"));
					opRequestBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
					opRequestBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
					opRequestBO.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
					opRequestBO.setAirTo(rs.getString("OR_AIRLINE_TO"));
					
					opRequestBO.setTimeToStartTripInSeconds(rs.getInt("st_time_diff"));
					
					if(rs.getInt("OR_METER_TYPE")==0){
						query = " AND MD_DEFAULT=1";
					} else {
						query = " AND MD_KEY='"+rs.getInt("OR_METER_TYPE")+"'";
					}
					pst1=con.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+assoccode+"'"+query);
					rs1=pst1.executeQuery();
					if(rs1.next()){
						opRequestBO.setRatePerMile(Double.parseDouble(rs1.getString("MD_RATE_PER_MILE")));
						opRequestBO.setRatePerMin(Double.parseDouble(rs1.getString("MD_RATE_PER_MIN")));
						opRequestBO.setStartAmt(Double.parseDouble(rs1.getString("MD_START_AMOUNT")));
						opRequestBO.setMinSpeed(Integer.parseInt(rs1.getString("MD_MINIMUM_SPEED")));
					}
				}
				rs.close();
				pst.close();
			} }catch (SQLException sqex) {
				cat.error("TDSException RequestDAO.getOpenRequestByTripID-->Error in getOpenRequest "+sqex.getMessage());
				cat.error(pst.toString());
				//System.out.println("TDSException RequestDAO.getOpenRequestByTripID-->Error in getOpenRequest "+sqex.getMessage());
				sqex.printStackTrace();
			} finally {
				dbcon.closeConnection();
			}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return opRequestBO;
	}

	public static OpenRequestBO getOpenRequestByTripIDForAutoAllocate(String tripId, String assoccode,String timeOffset) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequestByTripID");
		OpenRequestBO opRequestBO = null;
		//TDSConnection dbcon = null ;
		//Connection con = null;
		String query="";
		PreparedStatement pst = null,pst1=null;
		ResultSet rs = null,rs1=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String tripid[]=tripId.split(";");

		try {
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];

				pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_METER_TYPE,OR_TRIP_STATUS,OR_AMT,OR_TRIP_STATUS,OR_QUEUENO,OR_DRCABFLAG,OR_SMS_SENT,OR_DRIVERID,OR_DISPATCH_HISTORY_DRIVERS,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_SPLINS,OR_PAYACC,OR_PAYTYPE,OR_ROUTE_NUMBER,OR_DISPATCH_LEAD_TIME,"+
						"OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_SHARED_RIDE,OR_ELANDMARK,OR_EDSTATE,OR_NAME,OR_EDZIP,OR_REPEAT_GROUP,OR_PHONE,OR_STLATITUDE,OR_COMMENTS,OR_STLONGITUDE,OR_EDLATITUDE,OR_DRIVERID,OR_VEHICLE_NO,OR_EDLONGITUDE,OR_LANDMARK,OR_DRCABFLAG,OR_QUEUENO,"
						+ " QD_PHONE_CALL_WAIT_TIME, QD_SMS_RES_TIME, QD_ADJACENT_ZONE_SORT_TYPE, QD_DISPATCH_TYPE, "
						+ " case QD_DELAYTIME IS NULL WHEN true THEN  (OR_DISPATCH_LEAD_TIME * 60) ELSE CASE OR_DISPATCH_LEAD_TIME = -1 when true THEN QD_DELAYTIME*60 else (OR_DISPATCH_LEAD_TIME * 60) END END  AS QD_DELAYTIME, "
						+ " DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"'),'%H%i') "+"as TIME, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"') AS OFFSET_DATETIME,"
							+" date_format(OR_SERVICEDATE,'%r') as st_time," +
							" time_to_sec(timediff(OR_SERVICEDATE,now())) as st_time_diff," +
							" time_to_sec(timediff(now(),OR_DISPATCH_START_TIME)) as stale_time," +
							"time_to_sec(timediff(now(),OR_SMS_SENT)) as  SMS_TIME_DIFF,QD.QD_BROADCAST_DELAY,"
								+ "OR_AIRLINE_NAME,OR_AIRLINE_NO,OR_AIRLINE_FROM,OR_AIRLINE_TO FROM TDS_OPENREQUEST "
								+ " LEFT JOIN TDS_QUEUE_DETAILS QD ON OR_QUEUENO = QD_QUEUENAME AND QD_ASSOCCODE = OR_ASSOCCODE "
								+ " WHERE OR_TRIP_ID=? AND OR_ASSOCCODE=?");

				pst.setString(1, trip);
				pst.setString(2, assoccode);
				System.out.println("query:"+pst.toString());
				
				rs = pst.executeQuery();
				//String time;
				if(rs.next()) {
					opRequestBO = new OpenRequestBO();
					opRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
					opRequestBO.setPhone(rs.getString("OR_PHONE"));
					opRequestBO.setName(rs.getString("OR_NAME"));
					//opRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("DATE")));
					//opRequestBO.setShrs((rs.getString("TIME")));
					
					opRequestBO.setSttime(rs.getString("st_time"));
					opRequestBO.setStaleCallTime(rs.getInt("stale_time"));
					opRequestBO.setTimeToStartTripInSeconds(rs.getInt("st_time_diff"));
					opRequestBO.setQC_DELAYTIME(rs.getInt("QD_DELAYTIME"));
					opRequestBO.setAdjacentZoneOrderType(rs.getInt("QD_ADJACENT_ZONE_SORT_TYPE"));
					opRequestBO.setDispatchMethod(rs.getString("QD_DISPATCH_TYPE"));
					//opRequestBO.setStartTimeStamp (rs.getString("OR_OFFSET_SERVICEDATE"));
					
					opRequestBO.setPaytype((rs.getString("OR_PAYTYPE")));
					opRequestBO.setAcct(rs.getString("OR_PAYACC"));
					opRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
					opRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
					opRequestBO.setComments(rs.getString("OR_COMMENTS"));
					opRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
					opRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
					opRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
					opRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
					opRequestBO.setStartTimeStamp(rs.getString("OFFSET_DATETIME"));
					opRequestBO.setChckTripStatus(rs.getInt("OR_TRIP_STATUS"));
					opRequestBO.setOR_SMS_SENT(rs.getString("OR_SMS_SENT"));
					opRequestBO.setHistoryOfDrivers(rs.getString("OR_DISPATCH_HISTORY_DRIVERS"));
					opRequestBO.setSadd1(rs.getString("OR_STADD1"));
					opRequestBO.setSadd2(rs.getString("OR_STADD2"));
					opRequestBO.setScity(rs.getString("OR_STCITY"));
					opRequestBO.setSstate(rs.getString("OR_STSTATE"));
					opRequestBO.setSzip(rs.getString("OR_STZIP"));
					opRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
					opRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
					opRequestBO.setEadd1(rs.getString("OR_EDADD1"));
					opRequestBO.setEadd2(rs.getString("OR_EDADD2"));
					opRequestBO.setEcity(rs.getString("OR_EDCITY"));
					opRequestBO.setEstate(rs.getString("OR_EDSTATE"));
					opRequestBO.setEzip(rs.getString("OR_EDZIP"));
					opRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
					opRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
					opRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
					opRequestBO.setRepeatGroup(rs.getString("OR_REPEAT_GROUP"));
					opRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
					opRequestBO.setElandmark(rs.getString("OR_ELANDMARK"));
					opRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
					opRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
					opRequestBO.setDispatchLeadTime(rs.getInt("OR_DISPATCH_LEAD_TIME"));
					opRequestBO.setPaymentMeter(rs.getInt("OR_METER_TYPE"));
					opRequestBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
					opRequestBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
					opRequestBO.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
					opRequestBO.setAirTo(rs.getString("OR_AIRLINE_TO"));

					if(rs.getInt("OR_METER_TYPE")==0){
						query = " AND MD_DEFAULT=1";
					} else {
						query = " AND MD_KEY='"+rs.getInt("OR_METER_TYPE")+"'";
					}
					pst1=con.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+assoccode+"'"+query);
					rs1=pst1.executeQuery();
					if(rs1.next()){
						opRequestBO.setRatePerMile(Double.parseDouble(rs1.getString("MD_RATE_PER_MILE")));
						opRequestBO.setRatePerMin(Double.parseDouble(rs1.getString("MD_RATE_PER_MIN")));
						opRequestBO.setStartAmt(Double.parseDouble(rs1.getString("MD_START_AMOUNT")));
						opRequestBO.setMinSpeed(Integer.parseInt(rs1.getString("MD_MINIMUM_SPEED")));
					}
				}
				rs.close();
				pst.close();
			} }catch (SQLException sqex) {
				cat.error("TDSException RequestDAO.getOpenRequestByTripID-->Error in getOpenRequest "+sqex.getMessage());
				cat.error(pst.toString());
				//System.out.println("TDSException RequestDAO.getOpenRequestByTripID-->Error in getOpenRequest "+sqex.getMessage());
				sqex.printStackTrace();
			} finally {
				dbcon.closeConnection();
			}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return opRequestBO;
	}

	/*	public static ArrayList getAllOpenRequestForAutoAlloc(String assoccode) {
		cat.info("TDS INFO RequestDAO.getAllOpenRequestForAutoAlloc");
		ArrayList al_openRequest = new ArrayList();
		OpenRequestBO openRequest;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null,rs1=null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst.setString(1, assoccode);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			//String time;
			while(rs.next()) {
				openRequest = new OpenRequestBO();
				openRequest.setName(rs.getString("OR_NAME"));
				openRequest.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequest.setTripid(rs.getString("OR_TRIP_ID"));
				openRequest.setSadd1(rs.getString("OR_STADD1"));
				openRequest.setSadd2(rs.getString("OR_STADD2"));
				openRequest.setScity(rs.getString("OR_STCITY"));
				openRequest.setSstate(rs.getString("OR_STSTATE"));
				openRequest.setEcity(rs.getString("OR_EDCITY"));
				openRequest.setEstate(rs.getString("OR_EDSTATE"));
				//time = rs.getString("OR_SERVICETIME");
				//openRequest.setShrs(time.substring(0,2));
				//openRequest.setSmin(time.substring(2,4));
				openRequest.setSdate(rs.getString("Service_Date"));
				openRequest.setSlat(rs.getString("OR_STLATITUDE"));
				openRequest.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequest.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequest.setEdlongitude(rs.getString("OR_EDLONGITUDE"));

				pst = con.prepareStatement(" select ((QD_LATITUDENORTH+QD_LATITUDESOUTH)/2) as LATITUDE,((QD_LONGITUDEEAST+QD_LONGITUDEWEST)/2) as LONGITUDE from TDS_QUEUE_DETAILS where QD_QUEUENAME=?");
				pst.setString(1, rs.getString("OR_QUEUENO"));
				rs1 = pst.executeQuery();

				if(rs1.first())
				{
					openRequest.setSlat(rs1.getString("LATITUDE"));
					openRequest.setStlongitude2(rs1.getString("LONGITUDE"));
				} else {
					openRequest.setSlat("0");
					openRequest.setStlongitude2("0");
				}


				al_openRequest.add(openRequest);
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getAllOpenRequestForAutoAlloc-->"+sqex.getMessage());
			System.out.println("TDSException RequestRequestDAO.getAllOpenRequestForAutoAllocDAO.getAllOpenRequestForAutoAlloc-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		return al_openRequest;
	}
	 */
	/**
	 * @param openRequestBO set only the trip id in this bean
	 * @return OpenRequestBO bean which contains all the information about given trip id
	 * @author vimal
	 * @see Description
	 * This method is used to give the Details of open request data based on the trip id
	 */
	public static OpenRequestBO getOpenRequestTableData (OpenRequestBO openRequestBO)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequestTableData");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getOpenRequestContent);
			//SELECT OR_TRIP_ID,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EDSTATE,OR_EDZIP,OR_SAIRCODE,OR_EAIRCODE,OR_SERVICETIME,OR_SERVICEDATE,OR_ASSOCCODE,OR_ENTEREDTIME,OR_QUEUENO FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = ? AND OR_TRIP_STATUS in ('P','B')
			//SELECT OR_ENTEREDTIME,OR_Slat,OR_EDLATITUDE,OR_Slong,OR_EDLONGITUDE,OR_QUEUENO FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = ?
			pst.setString(1, openRequestBO.getTripid());
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.next()) {
				openRequestBO.setRequestTime(rs.getString("OR_ENTEREDTIME"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setIsBeandata(1);
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getOpenRequestTableData-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getOpenRequestTableData-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openRequestBO;
	}

	public static String getDriverKey (String p_driverName) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getDriverKey");
		String m_driverKey = "";
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getDriverKey);
			pst.setString(1, p_driverName);
			rs = pst.executeQuery();
			if(rs.next()) {
				m_driverKey = rs.getString("DR_SNO");
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getDriverKey-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getDriverKey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_driverKey;
	}

	public static OpenRequestBO getOpenRequestBean(OpenRequestBO openRequestBO,String phoneno,String Opr_mod, AdminRegistrationBO adminBO,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+adminBO.getAssociateCode()+"')";
			}
		} else {
			fleets = "('"+adminBO.getAssociateCode()+"')";
		}
		OpenRequestBO orBo = getOpenRequestBean(openRequestBO,phoneno,Opr_mod,adminBO,fleets);
		return orBo;
	}

	
	public static OpenRequestBO getOpenRequestBean(OpenRequestBO openRequestBO,String phoneno,String Opr_mod, AdminRegistrationBO adminBO,String allFleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequestBean");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null,pst1=null;
		ResultSet rs = null,rs1=null;
		String query="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(openRequestBO.getTripid() != "" || openRequestBO.getTripid().length() == 0 ){
				if(Opr_mod.equalsIgnoreCase("1")) {
					pst = con.prepareStatement("SELECT OR_CREATED_BY, OR_TRIP_SOURCE,OR_RATINGS,OR_METER_TYPE,OR_TRIP_ID,OR_NAME,OR_DRIVERID,OR_VEHICLE_NO,OR_PHONE,OR_DISPATCH_LEAD_TIME,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EDSTATE,OR_EDZIP,OR_SAIRCODE,OR_EAIRCODE,OR_SERVICETIME,OR_SHAREDRIDE_PAYTYPE,CONVERT_TZ(OR_SERVICEDATE, 'UTC', '"+adminBO.getTimeZone()+"') AS OR_SERVICEDATE_OFFSET ,OR_ASSOCCODE,OR_ENTEREDTIME,OR_QUEUENO,OR_END_QUEUENO,OR_STLATITUDE,OR_EDLATITUDE,OR_STLONGITUDE,OR_EDLONGITUDE,OR_LANDMARK,OR_ELANDMARK,OR_INTERSECTION,OR_PAYTYPE,OR_PAYACC,OR_SPLINS,OR_AMT, OR_TRIP_STATUS, OR_ROUTE_NUMBER,OR_AIRLINE_NAME,OR_AIRLINE_NO,OR_AIRLINE_FROM,OR_AIRLINE_TO,QD_SMS_RES_TIME,QD_DELAYTIME,OR_NUMBER_OF_PASSENGERS,VC_COMPANYNAME FROM TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD ON OR_QUEUENO = QD_QUEUENAME AND QD_ASSOCCODE=OR_ASSOCCODE LEFT JOIN TDS_GOVT_VOUCHER ON OR_MASTER_ASSOCCODE=VC_ASSOCCODE AND VC_VOUCHERNO=OR_PAYACC WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE IN "+allFleets+" AND OR_TRIP_STATUS in ('P','B',' ')"); //Allocate From Dash Board
				} else if(Opr_mod.equalsIgnoreCase("2")){
					pst = con.prepareStatement("SELECT OR_CREATED_BY, OR_TRIP_SOURCE,OR_RATINGS,OR_METER_TYPE,OR_TRIP_ID,OR_NAME,OR_DRIVERID,OR_VEHICLE_NO,OR_PHONE,OR_DISPATCH_LEAD_TIME,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EDSTATE,OR_EDZIP,OR_SAIRCODE,OR_EAIRCODE,OR_SERVICETIME,OR_SHAREDRIDE_PAYTYPE,CONVERT_TZ(OR_SERVICEDATE, 'UTC', '"+adminBO.getTimeZone()+"') AS OR_SERVICEDATE_OFFSET ,OR_ASSOCCODE,OR_ENTEREDTIME,OR_QUEUENO,OR_END_QUEUENO,OR_STLATITUDE,OR_EDLATITUDE,OR_STLONGITUDE,OR_EDLONGITUDE,OR_LANDMARK,OR_ELANDMARK,OR_INTERSECTION,OR_PAYTYPE,OR_PAYACC,OR_SPLINS,OR_AMT, OR_TRIP_STATUS, OR_ROUTE_NUMBER,OR_AIRLINE_NAME,OR_AIRLINE_NO,OR_AIRLINE_FROM,OR_AIRLINE_TO,QD_SMS_RES_TIME,QD_DELAYTIME,OR_NUMBER_OF_PASSENGERS,VC_COMPANYNAME FROM TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD ON  OR_QUEUENO = QD_QUEUENAME AND QD_ASSOCCODE=OR_ASSOCCODE LEFT JOIN TDS_GOVT_VOUCHER ON OR_MASTER_ASSOCCODE=VC_ASSOCCODE AND VC_VOUCHERNO=OR_PAYACC WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE IN "+allFleets+" AND OR_TRIP_STATUS in ('P','B','S',' ')"); //Allocate From Short Path Driver
				} else{
					pst = con.prepareStatement("SELECT OR_CREATED_BY, OR_TRIP_SOURCE,OR_RATINGS,OR_METER_TYPE,OR_TRIP_ID,OR_NAME, OR_DRIVERID,OR_VEHICLE_NO,OR_PHONE,OR_DISPATCH_LEAD_TIME,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EDSTATE,OR_EDZIP,OR_SAIRCODE,OR_EAIRCODE,OR_SERVICETIME,OR_SHAREDRIDE_PAYTYPE,CONVERT_TZ(OR_SERVICEDATE, 'UTC', '"+adminBO.getTimeZone()+"') AS OR_SERVICEDATE_OFFSET ,OR_ASSOCCODE,OR_ENTEREDTIME,OR_QUEUENO,OR_END_QUEUENO,OR_STLATITUDE,OR_EDLATITUDE,OR_STLONGITUDE,OR_EDLONGITUDE,OR_LANDMARK,OR_ELANDMARK,OR_INTERSECTION,OR_PAYTYPE,OR_PAYACC,OR_SPLINS,OR_AMT, OR_TRIP_STATUS, OR_ROUTE_NUMBER,OR_AIRLINE_NAME,OR_AIRLINE_NO,OR_AIRLINE_FROM,OR_AIRLINE_TO,QD_SMS_RES_TIME,QD_DELAYTIME,OR_NUMBER_OF_PASSENGERS,VC_COMPANYNAME FROM TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD ON  OR_QUEUENO = QD_QUEUENAME AND QD_ASSOCCODE=OR_ASSOCCODE LEFT JOIN TDS_GOVT_VOUCHER ON OR_MASTER_ASSOCCODE=VC_ASSOCCODE AND VC_VOUCHERNO=OR_PAYACC WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE IN "+allFleets+" AND ((OR_TRIP_STATUS IN('"+ TDSConstants.performingDispatchProcesses+ "','"+TDSConstants.srJobHold+"','"+TDSConstants.tripOnHold+"','"+TDSConstants.manualAllocation+"','"+TDSConstants.srAllocatedToCab+"') AND (OR_DRIVERID = '" + adminBO.getUid() + "' OR OR_VEHICLE_NO='"+adminBO.getVehicleNo()+"')) OR ((OR_TRIP_STATUS = '"+ TDSConstants.performingDispatchProcesses+ "' OR OR_TRIP_STATUS = '"+TDSConstants.manualAllocation+"') AND LOCATE('" + adminBO.getUid() + ";',OR_DRIVERID)>0) OR OR_TRIP_STATUS ='"+ TDSConstants.broadcastRequest+ "')");
				}
				pst.setString(1, openRequestBO.getTripid());
//				pst.setString(2, adminBO.getAssociateCode());
			} else if(phoneno != "" || phoneno.length() == 0){
				pst = con.prepareStatement(TDSSQLConstants.getTripAvailabilityByPhoneNo);
				pst.setString(1, phoneno);
			}
			rs = pst.executeQuery();

			if(rs.next()) {
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setPhone(rs.getString("OR_PHONE"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1") != null?rs.getString("OR_STADD1"):"");
				openRequestBO.setSadd2(rs.getString("OR_STADD2") != null?rs.getString("OR_STADD2"):"");
				openRequestBO.setScity(rs.getString("OR_STCITY") != null?rs.getString("OR_STCITY"):"");
				openRequestBO.setSstate(rs.getString("OR_STSTATE")!= null?rs.getString("OR_STSTATE"):"");
				openRequestBO.setSzip(rs.getString("OR_STZIP")!= null?rs.getString("OR_STZIP"):"");
				openRequestBO.setEadd1(rs.getString("OR_EDADD1")!= null?rs.getString("OR_EDADD1"):"");
				openRequestBO.setEadd2(rs.getString("OR_EDADD2") != null?rs.getString("OR_EDADD2"):"");
				openRequestBO.setEcity(rs.getString("OR_EDCITY") != null?rs.getString("OR_EDCITY"):"");
				openRequestBO.setEstate(rs.getString("OR_EDSTATE") != null?rs.getString("OR_EDSTATE"):"");
				openRequestBO.setEzip(rs.getString("OR_EDZIP") != null?rs.getString("OR_EDZIP"):"");
				/*				openRequestBO.setSaircode(rs.getString("OR_SAIRCODE") != null?rs.getString("OR_SAIRCODE"):"");
				openRequestBO.setEaircode(rs.getString("OR_EAIRCODE") != null?rs.getString("OR_EAIRCODE"):"");
				 */				
				openRequestBO.setAssociateCode(rs.getString("OR_ASSOCCODE")!= null ? rs.getString("OR_ASSOCCODE"):"");
				openRequestBO.setSdate(rs.getString("OR_SERVICEDATE_OFFSET"));
				openRequestBO.setRequestTime(rs.getString("OR_ENTEREDTIME"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setEndQueueno(rs.getString("OR_END_QUEUENO"));
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID")); 
				openRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				openRequestBO.setElandmark(rs.getString("OR_ELANDMARK")); 
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				openRequestBO.setSintersection(rs.getString("OR_INTERSECTION"));
				openRequestBO.setStartTimeStamp(rs.getString("OR_SERVICEDATE_OFFSET"));
				openRequestBO.setPaytype((rs.getString("OR_PAYTYPE")));
				openRequestBO.setAcct(rs.getString("OR_PAYACC"));
				openRequestBO.setAcctName(rs.getString("VC_COMPANYNAME"));
				openRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
				openRequestBO.setDispatchLeadTime(rs.getInt("OR_DISPATCH_LEAD_TIME"));
				String reqTime = rs.getString("OR_SERVICETIME");
				openRequestBO.setShrs(reqTime.substring(0,2));
				openRequestBO.setSmin(reqTime.substring(2,4));
				openRequestBO.setIsBeandata(1);
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				openRequestBO.setQC_SMS_RES_TIME(rs.getInt("QD_SMS_RES_TIME"));
				openRequestBO.setDispatchLeadTime(rs.getInt("QD_DELAYTIME"));
				openRequestBO.setJobRating(rs.getInt("OR_RATINGS"));
				openRequestBO.setPaymentMeter(rs.getInt("OR_METER_TYPE"));
				openRequestBO.setCreatedBy(rs.getString("OR_CREATED_BY"));
				openRequestBO.setTripSource(rs.getInt("OR_TRIP_SOURCE"));
				openRequestBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
				openRequestBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
				openRequestBO.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
				openRequestBO.setAirTo(rs.getString("OR_AIRLINE_TO"));
				openRequestBO.setSharedRidePaymentType(rs.getInt("OR_SHAREDRIDE_PAYTYPE"));
				openRequestBO.setNumOfPassengers(rs.getString("OR_NUMBER_OF_PASSENGERS")!=null&&!rs.getString("OR_NUMBER_OF_PASSENGERS").equals("")?Integer.parseInt(rs.getString("OR_NUMBER_OF_PASSENGERS")):1);
				if(rs.getInt("OR_METER_TYPE")==0){
					query = " AND MD_DEFAULT=1";
				} else {
					query = " AND MD_KEY='"+rs.getInt("OR_METER_TYPE")+"'";
				}
				pst1=con.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+adminBO.getMasterAssociateCode()+"'"+query);
				rs1=pst1.executeQuery();
				if(rs1.next()){
					openRequestBO.setRatePerMile(Double.parseDouble(rs1.getString("MD_RATE_PER_MILE")));
					openRequestBO.setRatePerMin(Double.parseDouble(rs1.getString("MD_RATE_PER_MIN")));
					openRequestBO.setStartAmt(Double.parseDouble(rs1.getString("MD_START_AMOUNT")));
					openRequestBO.setMinSpeed(Integer.parseInt(rs1.getString("MD_MINIMUM_SPEED")));
				}
			} else{
				openRequestBO = null;
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getOpenRequestBean-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getOpenRequestBean-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openRequestBO;
	}


	public static HashMap checkPhoneisAvailable (String p_phone) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.checkPhoneisAvailable");
		HashMap hmp_userData = new HashMap();
		int result = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.isPhoneAvailable);
			pst.setString(1, TDSValidation.getDBPhoneFormat(p_phone));
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.next()) {
				result = 1;
				hmp_userData.put("stadd1", rs.getString("CU_STADD1"));
				hmp_userData.put("stadd2", rs.getString("CU_STADD2"));
				hmp_userData.put("stcity", rs.getString("CU_STCITY"));
				hmp_userData.put("ststate", rs.getString("CU_STSTATE"));
				hmp_userData.put("stzip", rs.getString("CU_STZIP"));
				hmp_userData.put("edadd1", rs.getString("CU_EDADD1"));
				hmp_userData.put("edadd2", rs.getString("CU_EDADD2"));
				hmp_userData.put("edcity", rs.getString("CU_EDCITY"));
				hmp_userData.put("edstate", rs.getString("CU_EDSTATE"));
				hmp_userData.put("edzip", rs.getString("CU_EDZIP"));
			}
			hmp_userData.put("result", result+"");
			pst.close();
			rs.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.checkPhoneisAvailable-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.checkPhoneisAvailable-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return hmp_userData;
	}



	public static ArrayList getAddrForPhone(String p_phone) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getAddrForPhone");
		ArrayList al_list = new ArrayList();
		HashMap hmp_userData;
		int result = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.isPhoneAvailable);
			pst.setString(1, TDSValidation.getDBPhoneFormat(p_phone));
			cat.info(pst.toString());

			rs = pst.executeQuery();
			while(rs.next()) {
				hmp_userData = new HashMap();
				result = 1;
				hmp_userData.put("stadd1", rs.getString("CU_STADD1"));
				hmp_userData.put("stadd2", rs.getString("CU_STADD2"));
				hmp_userData.put("stcity", rs.getString("CU_STCITY"));
				hmp_userData.put("ststate", rs.getString("CU_STSTATE"));
				hmp_userData.put("stzip", rs.getString("CU_STZIP"));
				hmp_userData.put("edadd1", rs.getString("CU_EDADD1"));
				hmp_userData.put("edadd2", rs.getString("CU_EDADD2"));
				hmp_userData.put("edcity", rs.getString("CU_EDCITY"));
				hmp_userData.put("edstate", rs.getString("CU_EDSTATE"));
				hmp_userData.put("edzip", rs.getString("CU_EDZIP"));

				al_list.add(hmp_userData);
			}
			pst.close();
			rs.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.getAddrForPhone-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getAddrForPhone-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static int checkPhoneisAvailable (String p_phone,Connection con) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.checkPhoneisAvailable");
		int result = 0;
		PreparedStatement pst = null;
		ResultSet rs = null ;
		try{
			pst = con.prepareStatement(TDSSQLConstants.isPhoneAvailable);
			pst.setString(1, p_phone);
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.next()) {
				result = 1;
			}
			pst.close();
			rs.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.checkPhoneisAvailable-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.checkPhoneisAvailable-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static boolean insertClientLandMark (OpenRequestBO openRequestBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean result = false;
		PreparedStatement pst = null;
		TDSConnection dbcon = null ;
		Connection con = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("Insert TDS_USERLANDMARK (CUL_MASTERKEY, CUL_ASSOCIATIONCODE, CUL_PHONE,CUL_LANDMARK_KEY,CUL_NAME) values (?,?,?,?,?)");
			pst.setString(1, openRequestBO.getMasterAddressKey());
			pst.setString(2, openRequestBO.getAssociateCode());
			pst.setString(3, openRequestBO.getPhone());
			pst.setString(4, openRequestBO.getLandMarkKey());
			pst.setString(5, openRequestBO.getName());
			pst.execute();
		} catch(SQLException sqex) {
			//if(sqex.getErrorCode())
			//cat.error("TDSException RequestDAO.insertClientLandMark-->"+sqex.getMessage());
			//System.out.println("TDSException RequestDAO.insertClientLandMark-->Dupicate LandmarEntry. Its ok");
			//sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}



	public static boolean insertClientUser (OpenRequestBO p_openRequestBO,int status,String Assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.insertClientUser");
		boolean m_result = false;
		PreparedStatement m_pst = null;
		//ResultSet rs=null;
		//String roundedLati="";
		//String roundedLongi="";
		//		if(status==0){
		//			String lati[]=p_openRequestBO.getSlat().split("\\.");
		//			String longi[]=p_openRequestBO.getSlong().split("\\.");
		//			roundedLati=lati[0]+"."+lati[1].substring(0,3);
		//			roundedLongi=longi[0]+"."+longi[1].substring(0,3);
		//		} else {
		//			String lati[]=p_openRequestBO.getEdlatitude().split("\\.");
		//			String longi[]=p_openRequestBO.getEdlongitude().split("\\.");
		//			roundedLati=lati[0]+"."+lati[1].substring(0,3);
		//			roundedLongi=longi[0]+"."+longi[1].substring(0,3);
		//		}
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			//m_pst=con.prepareStatement("SELECT * FROM TDS_USERADDRESS WHERE CU_MASTERASSOCIATIONCODE='"+p_openRequestBO.getAssociateCode()+"' AND CU_LATITUDE like '"+roundedLati+"%' AND CU_LONGITUDE like '"+roundedLongi+"%' AND CU_PHONENO='"+p_openRequestBO.getPhone()+"'");
			//rs=m_pst.executeQuery();
			//if(!rs.next()){
			m_pst = con.prepareStatement(TDSSQLConstants.insertClientDetail);
			if(status == 0) {
				m_pst.setString(1, TDSValidation.getDBPhoneFormat(p_openRequestBO.getPhone()));
				m_pst.setString(2, p_openRequestBO.getSadd1());
				m_pst.setString(3, p_openRequestBO.getSadd2());
				m_pst.setString(4, p_openRequestBO.getScity());
				m_pst.setString(5, p_openRequestBO.getSstate());
				m_pst.setString(6, p_openRequestBO.getSzip());
				m_pst.setString(7, p_openRequestBO.getSlat());
				m_pst.setString(8, p_openRequestBO.getSlong());
				m_pst.setString(9, Assoccode);
				m_pst.setString(10, p_openRequestBO.getMasterAddressKey());
			} else {
				m_pst.setString(1, TDSValidation.getDBPhoneFormat(p_openRequestBO.getPhone()));
				m_pst.setString(2, p_openRequestBO.getEadd1());
				m_pst.setString(3, p_openRequestBO.getEadd2());
				m_pst.setString(4, p_openRequestBO.getEcity());
				m_pst.setString(5, p_openRequestBO.getEstate());
				m_pst.setString(6, p_openRequestBO.getEzip());
				m_pst.setString(7, p_openRequestBO.getEdlatitude());
				m_pst.setString(8, p_openRequestBO.getEdlongitude());
				m_pst.setString(9, Assoccode);
				m_pst.setString(10, p_openRequestBO.getMasterAddressKey());
			}
			cat.info("In Update Query "+m_pst.toString());
			//System.out.println("Insert user Address for status="+status+"---->"+m_pst.toString());
			if(m_pst.executeUpdate() > 0) {
				m_result = true;
			}
			//			} else {
			//				m_result=true;
			//			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.insertClientUser-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RequestDAO.insertClientUser-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}


	/*public static int insertClientUser (OpenRequestBO openRequestBO, Connection con) {
		 int result = 0;
		 PreparedStatement pst = null;
		 try {
			 pst = con.prepareStatement(TDSSQLConstants.insertClientDetail);
			 pst.setString(1, TDSValidation.getDBPhoneFormat(openRequestBO.getPhone()));
			 pst.setString(2, openRequestBO.getSadd1());
			 pst.setString(3, openRequestBO.getSadd2());
			 pst.setString(4, openRequestBO.getScity());
			 pst.setString(5, openRequestBO.getSstate());
			 pst.setString(6, openRequestBO.getSzip());
			 pst.setString(7, openRequestBO.getEadd1());
			 pst.setString(8, openRequestBO.getEadd2());
			 pst.setString(9, openRequestBO.getEcity());
			 pst.setString(10, openRequestBO.getEstate());
			 pst.setString(11, openRequestBO.getEzip());
			 result = pst.executeUpdate();
		 } catch(SQLException sqex) {
			 cat.info("Error in insert Client info "+sqex.getMessage());
		 }
		 return result;
	 }*/

	public static int checkDriverAvailable (String p_driverid) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.checkDriverAvailable");
		int result = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.checkDriverAvilability);
			pst.setString(1, p_driverid);
			cat.info(pst.toString());

			rs = pst.executeQuery();
			if(rs.next()) {
				result = 1;
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.checkDriverAvailable-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.checkDriverAvailable-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static ArrayList<OpenRequestBO> getAcceptedRequestDataXML(String driverid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getAcceptedRequestDataXML");
		ArrayList<OpenRequestBO> al_ARData = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst = con.prepareStatement(TDSSQLConstants.getAcceptRequestData +"WHERE AR_DRIVERID ="+driverid);
			cat.info(pst.toString());

			rs=pst.executeQuery();
			//String time;
			while(rs.next()){
				OpenRequestBO openBO = new OpenRequestBO();
				openBO.setTripid(rs.getString("AR_TRIPID"));
				openBO.setPhone(rs.getString("AR_CUSTOMERPHONE"));
				openBO.setSadd1(rs.getString("AR_STADD1"));
				openBO.setSadd2(rs.getString("AR_STADD2"));
				openBO.setScity(rs.getString("AR_STCITY"));
				openBO.setSstate(rs.getString("AR_STSTATE"));
				openBO.setSzip(rs.getString("AR_STZIP"));
				openBO.setEadd1(rs.getString("AR_EDADD1"));
				openBO.setEadd2(rs.getString("AR_EDADD2"));
				openBO.setEcity(rs.getString("AR_EDCITY"));
				openBO.setEstate(rs.getString("AR_EDSTATE"));
				openBO.setEzip(rs.getString("AR_EDZIP"));				
				openBO.setDriverid(rs.getString("AR_DRIVERID"));
				openBO.setSlat(rs.getString("AR_Slat")!=null?rs.getString("AR_Slat"):"");
				openBO.setSlong(rs.getString("AR_Slong")!=null?rs.getString("AR_Slong"):"");
				openBO.setEdlatitude(rs.getString("AR_EDLATITUDE")!=null?rs.getString("AR_EDLATITUDE"):"");
				openBO.setEdlongitude(rs.getString("AR_EDLONGITUDE")!=null?rs.getString("AR_EDLONGITUDE"):"");
				openBO.setAssociateCode(rs.getString("AR_ASSOCCODE")!=null?rs.getString("AR_ASSOCCODE"):"");
				openBO.setName(rs.getString("AR_NAME")!=null?rs.getString("AR_NAME"):"");
				openBO.setQueueno(rs.getString("AR_QUEUENO")!=null?rs.getString("AR_QUEUENO"):"");
				openBO.setSlandmark(rs.getString("AR_LANDMARK"));
				openBO.setSintersection(rs.getString("AR_INTERSECTION"));
				al_ARData.add(openBO);
			}
			pst.close();
			rs.close();
			con.close();
		}
		catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getAcceptedRequestDataXML-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getAcceptedRequestDataXML-->"+sqex.getMessage());
			sqex.printStackTrace();				}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_ARData;
	}

	public static OpenRequestBO getHoldJobs(String driverId,String cabNo,String assoccode,String timeOffset){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		PreparedStatement pst = null ;
		ResultSet rs = null; 
		OpenRequestBO openRequestBO= null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT *,CONVERT_TZ(OR_SERVICEDATE, 'UTC',?) AS OR_SERVICEDATE_OFFSET FROM TDS_OPENREQUEST WHERE (OR_VEHICLE_NO=? OR OR_DRIVERID=?) and OR_ASSOCCODE=? AND OR_TRIP_STATUS ='"+TDSConstants.tripOnHold+"' LIMIT 0,1");
			pst.setString(1, timeOffset);
			pst.setString(2, cabNo);
			pst.setString(3, driverId);
			pst.setString(4, assoccode);
			cat.info("Query in RequestDAO getHoldJobs "+pst);
			//System.out.println(pst);
			rs = pst.executeQuery();
			if(rs.next()) {
				openRequestBO = new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setPhone(rs.getString("OR_PHONE"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(rs.getString("OR_STADD2"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setSstate(rs.getString("OR_STSTATE"));
				openRequestBO.setSzip(rs.getString("OR_STZIP"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBO.setEzip(rs.getString("OR_EDZIP"));
				openRequestBO.setSdate(rs.getString("OR_SERVICEDATE"));
				openRequestBO.setShrs(rs.getString("OR_SERVICETIME"));
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME")));
				openRequestBO.setAssociateCode(rs.getString("OR_ASSOCCODE"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				openRequestBO.setSintersection(rs.getString("OR_INTERSECTION"));
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBO.setStartTimeStamp(rs.getString("OR_SERVICEDATE_OFFSET"));
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
				openRequestBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
				openRequestBO.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
				openRequestBO.setAirTo(rs.getString("OR_AIRLINE_TO"));
				openRequestBO.setEmail(rs.getString("OR_EMAIL"));

			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getHoldJobs "+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getHoldJobs"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openRequestBO;
	}

	public static int updateAcceptedRequest(OpenRequestBO openBO,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateAcceptedRequest");
		int status = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			if(openBO.getDriverid()!=null){
				pst=con.prepareStatement(TDSSQLConstants.updateAcceptedRequestData);
				pst.setString(1,openBO.getPhone());
				pst.setString(2,openBO.getSlat());
				pst.setString(3, openBO.getSlong());
				pst.setString(4, openBO.getSadd1());
				pst.setString(5, openBO.getSadd2());
				pst.setString(6, openBO.getScity());
				pst.setString(7, openBO.getSstate());
				pst.setString(8, openBO.getSzip());
				pst.setString(9, openBO.getEdlatitude());
				pst.setString(10, openBO.getEdlongitude());
				pst.setString(11, openBO.getEadd1());
				pst.setString(12, openBO.getEadd2());
				pst.setString(13, openBO.getEcity());
				pst.setString(14, openBO.getEstate());
				pst.setString(15, openBO.getEzip());
				pst.setString(16,openBO.getSintersection());
				pst.setString(17,openBO.getSlandmark());
				pst.setString(18,openBO.getTripid());
				pst.setString(19,adminBO.getAssociateCode());

			}
			else{
				pst=con.prepareStatement(TDSSQLConstants.updateOpenRequestData);
				pst.setString(1,openBO.getTripid());
				pst.setString(2, openBO.getPhone());
				pst.setString(3, openBO.getSadd1());
				pst.setString(4, openBO.getSadd2());
				pst.setString(5, openBO.getScity());
				pst.setString(6, openBO.getSstate());
				pst.setString(7, openBO.getSzip());
				pst.setString(8, openBO.getEadd1());
				pst.setString(9, openBO.getEadd2());
				pst.setString(10, openBO.getEcity());
				pst.setString(11, openBO.getEstate());
				pst.setString(12, openBO.getEzip());
				/*				pst.setString(13, openBO.getSaircode());
				pst.setString(14, openBO.getEaircode());
				 */				pst.setString(13,openBO.getSintersection());
				 pst.setString(14,openBO.getSlandmark());
				 pst.setString(15,openBO.getTripid());
				 pst.setString(16,adminBO.getAssociateCode());
			}
			cat.info(pst.toString());

			status=pst.executeUpdate();
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateAcceptedRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateAcceptedRequest-->"+sqex.getMessage());
			sqex.printStackTrace();		
		}

		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}

	public static int deleteAcceptedRequest(OpenRequestBO openBO,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.deleteAcceptedRequest");
		int status=0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			if(openBO.getDriverid()!=null){
				pst=con.prepareStatement(TDSSQLConstants.deleteAcceptedRequestData);
				pst.setString(1,openBO.getTripid());
				pst.setString(2, adminBO.getAssociateCode());

			}
			else{
				pst=con.prepareStatement(TDSSQLConstants.deleteOpenRequestData);
				pst.setString(1,openBO.getTripid());
				pst.setString(2,adminBO.getAssociateCode());

			}
			cat.info(pst.toString());

			status=pst.executeUpdate();
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.deleteAcceptedRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.deleteAcceptedRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}

	//?? Is this required We have a similiar fnction in ServceDAO
	public static OpenRequestBO getAcceptRequest(String tripID, AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getAcceptRequest");
		//ArrayList al_open = new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null ;
		ResultSet rs = null; 
		OpenRequestBO openRequestBO = new OpenRequestBO();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			String query = ""; 
			pst = con.prepareStatement(TDSSQLConstants.getAcceptRequestCriteria+query);
			pst.setString(1,tripID);
			pst.setString(2,adminBO.getAssociateCode());
			pst.setString(3,adminBO.getUid());
			pst.setString(4,TDSConstants.jobAllocated+"");

			rs = pst.executeQuery();
			while(rs.next()) {
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(rs.getString("OR_STADD2"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setSstate(rs.getString("OR_STSTATE"));
				openRequestBO.setSzip(rs.getString("OR_STZIP"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBO.setEzip(rs.getString("OR_EDZIP"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("OR_SERVICEDATE")));
				if(!TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME")).equalsIgnoreCase("")){
					openRequestBO.setShrs(rs.getString("OR_SERVICETIME").substring(0,2));
					openRequestBO.setSmin(rs.getString("OR_SERVICETIME").substring(2,4));
				}
				else {
					openRequestBO.setShrs("");
					openRequestBO.setSmin("");
				}
				openRequestBO.setSttime(TDSValidation.getUserTimeFormat(rs.getString("OR_SERVICETIME")));
				/*				openRequestBO.setSaircode(rs.getString("OR_SAIRCODE"));
				openRequestBO.setEaircode(rs.getString("OR_EAIRCODE"));
				 */				//al_open.add(openRequestBO);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.geRequestDAO.getAcceptRequesttAcceptRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getAcceptRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return openRequestBO;
	}

	public static List getDriverLocation() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getDriverLocation");
		List m_driverLocationList = new ArrayList();
		TDSConnection m_dbcon = null;
		Connection m_con = null;
		PreparedStatement m_pst = null ;
		ResultSet m_rs = null; 
		Map m_driverLocation = null;
		try {
			m_dbcon = new TDSConnection();
			m_con = m_dbcon.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.getDriverExtension);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();

			while(m_rs.next()) {
				m_driverLocation = new HashMap();
				m_driverLocation.put("driverid", "\""+m_rs.getString("DR_USERID")+"\"");
				m_driverLocation.put("driverext", m_rs.getString("DR_DRIVEREXT"));
				m_driverLocation.put("latitude", m_rs.getString("DL_LATITUDE"));
				m_driverLocation.put("longitude", m_rs.getString("DL_LONGITUDE"));
				m_driverLocationList.add(m_driverLocation);
			}
			if(m_driverLocationList.size() < 0 ) {
				m_driverLocationList = null;
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getDriverLocation-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RequestDAO.getDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_driverLocationList;
	}

	public static List getDriverInfo() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getDriverInfo");
		List al_driverList = new ArrayList();
		TDSConnection m_dbcon = null;
		Connection m_con = null;
		PreparedStatement m_pst = null ;
		ResultSet m_rs = null; 
		Map mp_driverDetail = null;
		try {
			m_dbcon = new TDSConnection();
			m_con = m_dbcon.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.getDriverInformation);
			cat.info("Query in Driver Information "+ m_pst);
			m_rs = m_pst.executeQuery();
			while (m_rs.next()) {
				mp_driverDetail = new HashMap();
				mp_driverDetail.put("driverid", m_rs.getString("DR_SNO"));
				mp_driverDetail.put("driverName", m_rs.getString("DR_USERID"));
				al_driverList.add(mp_driverDetail);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getDriverInfo-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RequestDAO.getDriverInfo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_driverList;
	}

	public static List getCompanyInfo() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getCompanyInfo");
		List al_companyList = new ArrayList();
		TDSConnection m_dbcon = null;
		Connection m_con = null;
		PreparedStatement m_pst = null ;
		ResultSet m_rs = null; 
		Map mp_companyDetail = null;
		try {
			m_dbcon = new TDSConnection();
			m_con = m_dbcon.getConnection();
			m_pst = m_con.prepareStatement(TDSSQLConstants.getCompanyInformation);
			cat.info("Query in Company Information "+ m_pst);
			m_rs = m_pst.executeQuery();
			while (m_rs.next()) {
				mp_companyDetail = new HashMap();
				mp_companyDetail.put("companyid", m_rs.getString("CD_ASSOCCODE"));
				mp_companyDetail.put("companyName", m_rs.getString("CD_COMPANYNAME"));
				al_companyList.add(mp_companyDetail);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getCompanyInfo-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RequestDAO.getCompanyInfo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_companyList;
	}

	public static List getAvailableDriver() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getAvailableDriver");
		List m_driverList = null;
		Map m_driverMap = null;
		TDSConnection m_dbcon = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_dbcon = new TDSConnection();
			m_conn = m_dbcon.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getAllAvailableDriver);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_driverList = new ArrayList();
				do {
					m_driverMap = new HashMap();
					m_driverMap.put("driverid", m_rs.getString("DL_DRIVERID"));
					m_driverMap.put("driverid", m_rs.getString("DL_LATITUDE"));
					m_driverMap.put("longitude", m_rs.getString("DL_LONGITUDE"));
					m_driverMap.put("phoneno", m_rs.getString("DR_PHONE"));
					m_driverList.add(m_driverMap);
				} while(m_rs.next());
			}
			//cat.info("Driver Available List "+m_driverList);
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getAvailableDriver->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RequestDAO.getAvailableDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_driverList;
	}

	public static String getUserInformation(String phone,String add1,String add2,String state,String city,String zip) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getUserInformation");
		List m_conditionList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String retStr="############";
		try {
			m_tdsConn = new TDSConnection(); 
			m_con = m_tdsConn.getConnection();

			m_pst = m_con.prepareStatement("SELECT * FROM TDS_USERADDRESS WHERE CU_PHONENO = ? AND CU_ADD1 like ? and CU_ADD2 like ? and CU_CITY like ? and CU_STATE like ? and CU_ZIP  LIKE ? order by CU_UPDATEDTIME desc limit 1");

			m_pst.setString(1, TDSValidation.getDBPhoneFormat(phone));
			m_pst.setString(2, add1+"%");
			m_pst.setString(3, add2+"%");
			m_pst.setString(4, city+"%");
			m_pst.setString(5, state+"%");
			m_pst.setString(6, zip+"%");
			cat.info("Query in retrieve User information "+m_pst);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				retStr  = m_rs.getString("CU_ADD1")+"###"+m_rs.getString("CU_ADD2")+"###"+m_rs.getString("CU_CITY")+"###"+m_rs.getString("CU_STATE")+"###"+m_rs.getString("CU_ZIP");
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException RequestDAO.getUserInformation->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RequestDAO.getUserInformation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return retStr;
	}
	//	public static ArrayList openRequestHistory(OpenRequestBO openRequestHistory,AdminRegistrationBO adminBO,String value,String fromDate,String toDate,String specialFlags,int tripStatus) {
	//		long startTime = System.currentTimeMillis();
	//		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
	//		ArrayList<OpenRequestBO>openReqHistory = new ArrayList<OpenRequestBO>();
	//		PreparedStatement pst = null;
	//		ResultSet rs = null;
	//		String query= "SELECT *,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i') AS ORH_SERVICEDATE_FORMATTED, case when ORH_TRIP_STATUS=40 THEN 'Job Allocated' when ORH_TRIP_STATUS=43 THEN 'On Route' when ORH_TRIP_STATUS=47 THEN 'Driver NoShow' when ORH_TRIP_STATUS=4 THEN 'Performing Dispatch' when ORH_TRIP_STATUS=8 THEN 'New Request' when ORH_TRIP_STATUS=30 THEN 'Broadcast' when ORH_TRIP_STATUS=50 THEN 'Customer cancelled' when ORH_TRIP_STATUS=51 THEN 'NoShow' when ORH_TRIP_STATUS=52 THEN 'Couldnt service' when ORH_TRIP_STATUS=55 THEN 'Operator Cancelled' when ORH_TRIP_STATUS=61 THEN 'Job Completed' when ORH_TRIP_STATUS=99 THEN 'On Hold' end as TRIPSTATUS FROM TDS_OPENREQUEST_HISTORY where ORH_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"'";
	//		if(!value.equals("")){
	//			query =query+" AND( ORH_DRIVERID like '%"+value+"%' OR ORH_PHONE like'%"+value+"%' OR ORH_NAME like'%"+value+"%' OR ORH_TRIP_ID like'%"+value+"%' OR ORH_VEHICLE_NO like'%"+value+"%' OR ORH_STCITY like'%"+value+"%' OR ORH_STSTATE like'%"+value+"%' OR ORH_STZIP  like'%"+value+"%' OR ORH_VEHICLE_NO like '%"+value+"%'  OR ORH_STADD1 like'%"+value+"%'  OR ORH_STADD2 like'%"+value+"%'  OR ORH_EDADD1 like'%"+value+"%'  OR ORH_EDADD2 like'%"+value+"%'  OR ORH_PAYACC like'%"+value+"%')";
	//		}
	//		if(!fromDate.equals("")&&!toDate.equals("")){
	//			query=query+" AND ORH_SERVICEDATE <=CONVERT_TZ(STR_TO_DATE('"+toDate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') "
	//					+ " AND ORH_SERVICEDATE >=CONVERT_TZ(STR_TO_DATE('"+fromDate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') ";
	//		}
	//		if(!specialFlags.equals("")){
	//			query=query+"AND ORH_DRCABFLAG LIKE '%"+specialFlags+"%'";
	//		}
	//		if(tripStatus>0){
	//			query=query+"AND ORH_TRIP_STATUS ='"+tripStatus+"'";
	//		}
	//		query=query+" ORDER BY ORH_SERVICEDATE";
	//		TDSConnection dbConn = new TDSConnection();
	//		Connection conn = dbConn.getConnection(); 
	//		try {
	//			pst = conn.prepareStatement(query);
	//			rs = pst.executeQuery();
	//			while (rs.next()) {
	//				OpenRequestBO openRequestBean=new OpenRequestBO();
	//				openRequestBean.setDriverid(rs.getString("ORH_DRIVERID"));
	//				openRequestBean.setName(rs.getString("ORH_NAME"));
	//				openRequestBean.setPhone(rs.getString("ORH_PHONE"));
	//				openRequestBean.setTripid(rs.getString("ORH_TRIP_ID"));
	//				openRequestBean.setSadd1(rs.getString("ORH_STADD1"));
	//				openRequestBean.setSadd2(rs.getString("ORH_STADD2"));
	//				openRequestBean.setScity(rs.getString("ORH_STCITY"));
	//				openRequestBean.setSlat(rs.getString("ORH_STLATITUDE"));
	//				openRequestBean.setSlong(rs.getString("ORH_STLONGITUDE"));
	//				openRequestBean.setTripStatus(rs.getString("TRIPSTATUS"));
	//				openRequestBean.setSstate(rs.getString("ORH_STSTATE"));
	//				//openRequestBean.setTripStatus(rs.getString("OR_TRIP_STATUS"));
	//				openRequestBean.setSzip(rs.getString("ORH_STZIP"));
	//				openRequestBean.setEadd1(rs.getString("ORH_EDADD1"));
	//				openRequestBean.setEadd2(rs.getString("ORH_EDADD2"));
	//				openRequestBean.setEcity(rs.getString("ORH_EDCITY"));
	//				openRequestBean.setEstate(rs.getString("ORH_EDSTATE"));
	//				openRequestBean.setEdlatitude(rs.getString("ORH_EDLATITUDE"));
	//				openRequestBean.setEdlongitude(rs.getString("ORH_EDLONGITUDE"));
	//				openRequestBean.setEzip(rs.getString("ORH_EDZIP"));
	//				openRequestBean.setSdate(rs.getString("ORH_SERVICEDATE_FORMATTED"));
	//				openRequestBean.setPaytype(rs.getString("ORH_PAYTYPE"));
	//				openRequestBean.setAmt(new BigDecimal(rs.getString("ORH_AMT")));
	//				openRequestBean.setSpecialIns(rs.getString("ORH_SPLINS"));
	//				openRequestBean.setComments(rs.getString("ORH_OPERATOR_COMMENTS"));
	//				openRequestBean.setVehicleNo(rs.getString("ORH_VEHICLE_NO"));
	//				openRequestBean.setTripSource(rs.getInt("ORH_TRIP_SOURCE"));
	//				openRequestBean.setCreatedBy(rs.getString("ORH_CREATED_BY"));
	//				openRequestBean.setDrProfile(rs.getString("ORH_DRCABFLAG"));
	//				openRequestBean.setRouteNumber(rs.getString("ORH_ROUTE_NUMBER"));
	//				openRequestBean.setAcct(rs.getString("ORH_PAYACC"));
	//				openRequestBean.setCallerName(rs.getString("ORH_CALLER_NAME"));
	//				openRequestBean.setCallerPhone(rs.getString("ORH_CALLER_PHONE"));
	//				openRequestBean.setRefNumber(rs.getString("ORH_CUST_REF_NUM"));
	//				openRequestBean.setRefNumber1(rs.getString("ORH_CUST_REF_NUM1"));
	//				openRequestBean.setRefNumber2(rs.getString("ORH_CUST_REF_NUM2"));
	//				openRequestBean.setRefNumber3(rs.getString("ORH_CUST_REF_NUM3"));
	//				openRequestBean.setSlat(rs.getString("ORH_STLATITUDE"));
	//				openRequestBean.setSlong(rs.getString("ORH_STLONGITUDE"));
	//				openRequestBean.setEdlatitude(rs.getString("ORH_EDLATITUDE"));
	//				openRequestBean.setEdlongitude(rs.getString("ORH_EDLONGITUDE"));
	//			
	//				openRequestBean.setRouteNumber(rs.getString("ORH_ROUTE_NUMBER"));
	//				openRequestBean.setOR_SMS_SENT(rs.getString("TIME1"));
	//				openRequestBean.setCreatedTime(rs.getString("TIME2"));
	//				openRequestBean.setCompletedTime(rs.getString("TIME3"));
	//			 
	//				openReqHistory.add(openRequestBean);
	//			}
	//
	//		}
	//		catch (Exception sqex)
	//		{
	//			cat.error("TDSException RequestDAO.openRequestHistory-->"+sqex.getMessage());
	//			cat.error(pst.toString());
	//			System.out.println("TDSException RequestDAO.openRequestHistory-->"+sqex.getMessage());
	//			sqex.printStackTrace();
	//		} finally {
	//			dbConn.closeConnection();
	//		} 
	//		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	//
	//		return openReqHistory;
	//	}	

	public static ArrayList openRequestHistory(OpenRequestBO openRequestHistory,AdminRegistrationBO adminBO,String value,String fromDate,String toDate,String specialFlags,int tripStatus,String driverid, String assocCode, String trip_Src) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO>openReqHistory = new ArrayList<OpenRequestBO>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query="";
		query= "SELECT *,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i') AS ORH_SERVICEDATE_FORMATTED,DATE_FORMAT(CONVERT_TZ(TEST.ORL_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%Y/%m/%d %H:%i %p') AS TIME1,DATE_FORMAT(CONVERT_TZ(TEST1.ORL_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%Y/%m/%d %H:%i %p') AS TIME2,DATE_FORMAT(CONVERT_TZ(TEST2.ORL_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%Y/%m/%d %H:%i %p') AS TIME3,ODO.OD_VALUE AS STARTODO,ODO1.OD_VALUE AS ENDODO, case when ORH_TRIP_STATUS=40 THEN 'Job Allocated' when ORH_TRIP_STATUS=43 THEN 'On Route' when ORH_TRIP_STATUS=47 THEN 'Driver NoShow' when ORH_TRIP_STATUS=4 THEN 'Performing Dispatch' when ORH_TRIP_STATUS=8 THEN 'New Request' when ORH_TRIP_STATUS=30 THEN 'Broadcast' when ORH_TRIP_STATUS=50 THEN 'Customer cancelled' when ORH_TRIP_STATUS=51 THEN 'NoShow' when ORH_TRIP_STATUS=52 THEN 'Couldnt service' when ORH_TRIP_STATUS=55 THEN 'Operator Cancelled' when ORH_TRIP_STATUS=61 THEN 'Job Completed' when ORH_TRIP_STATUS=99 THEN 'On Hold' when ORH_TRIP_STATUS=70 THEN 'Job Payment Settled' end as TRIPSTATUS,TEST3.ORL_PAYMENT AS MeterPayment FROM TDS_OPENREQUEST_HISTORY "+
				"LEFT JOIN TDS_OPENREQUEST_LOGS AS TEST ON ORH_MASTER_ASSOCCODE=TEST.ORL_MASTER_ASSOCCODE AND ORH_TRIP_ID=TEST.ORL_TRIPID AND TEST.ORL_REASON ='DRIVER ONSITE' LEFT JOIN TDS_OPENREQUEST_LOGS AS TEST1 ON ORH_MASTER_ASSOCCODE=TEST1.ORL_MASTER_ASSOCCODE AND ORH_TRIP_ID=TEST1.ORL_TRIPID AND TEST1.ORL_REASON ='TRIP STARTED' LEFT JOIN TDS_OPENREQUEST_LOGS AS TEST2 ON ORH_MASTER_ASSOCCODE=TEST2.ORL_MASTER_ASSOCCODE AND ORH_TRIP_ID=TEST2.ORL_TRIPID  AND TEST2.ORL_REASON ='TRIP ENDED' LEFT JOIN TDS_OPENREQUEST_LOGS AS TEST3 ON ORH_MASTER_ASSOCCODE=TEST3.ORL_MASTER_ASSOCCODE AND ORH_TRIP_ID=TEST3.ORL_TRIPID  AND TEST3.ORL_STATUS ='"+TDSConstants.jobMeterPayment+"' LEFT JOIN TDS_FLEET ON ORH_MASTER_ASSOCCODE=FL_MASTER_ASSOCCODE AND ORH_ASSOCCODE=FL_ASSOCCODE LEFT JOIN TDS_ODOMETER AS ODO ON ORH_MASTER_ASSOCCODE=ODO.OD_ASSOCCODE AND ORH_TRIP_ID=ODO.OD_TRIP_ID AND ODO.OD_EVENT ='STARTTRIP' LEFT JOIN TDS_ODOMETER AS ODO1 ON ORH_MASTER_ASSOCCODE=ODO1.OD_ASSOCCODE AND ORH_TRIP_ID=ODO1.OD_TRIP_ID AND ODO1.OD_EVENT ='ENDTRIP'  WHERE ORH_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"'";
		 
		if(assocCode!=null && !assocCode.equals("")){
			query = query + "AND ORH_ASSOCCODE='"+assocCode+"'";
		}
		 
		if(!driverid.equals("") ){
            query=query+"AND  ORH_DRIVERID	 ='"+driverid+"'";
		}else{
			if(!value.equals("")){
				query =query+" AND( ORH_DRIVERID like '%"+value+"%' OR ORH_PHONE like'%"+value+"%' OR ORH_NAME like'%"+value+"%' OR ORH_TRIP_ID like'%"+value+"%' OR ORH_VEHICLE_NO like'%"+value+"%' OR ORH_STCITY like'%"+value+"%' OR ORH_STSTATE like'%"+value+"%' OR ORH_STZIP  like'%"+value+"%' OR ORH_VEHICLE_NO like '%"+value+"%'  OR ORH_STADD1 like'%"+value+"%'  OR ORH_STADD2 like'%"+value+"%'  OR ORH_EDADD1 like'%"+value+"%'  OR ORH_EDADD2 like'%"+value+"%'  OR ORH_PAYACC like'%"+value+"%')";
			}
			if(!fromDate.equals("")&&!toDate.equals("")){
				query=query+" AND ORH_SERVICEDATE <=CONVERT_TZ(STR_TO_DATE('"+toDate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') "
						+ " AND ORH_SERVICEDATE >=CONVERT_TZ(STR_TO_DATE('"+fromDate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') ";
			}
			if(!specialFlags.equals("")){
				query=query+"AND ORH_DRCABFLAG LIKE '%"+specialFlags+"%'";
			}
		
			if(tripStatus>0){
				query=query+"AND ORH_TRIP_STATUS ='"+tripStatus+"'";
			}
		}
		
		//trip_Src 0->ALL, 1->Web, 2->FlagTrip, 3->CUstomer, 4->Others
		if(trip_Src!=null && !trip_Src.equals("") && !trip_Src.equals("0")){
			if(trip_Src.equals("1")){
				query = query+" AND ORH_TRIP_SOURCE='3' ";
			}else if(trip_Src.equals("2")){
				query = query+" AND ORH_TRIP_SOURCE='2' ";
			}else if(trip_Src.equals("3")){
				query = query+" AND (ORH_TRIP_SOURCE='0' OR ORH_TRIP_SOURCE='4') ";
			}else{
				query = query+" AND (ORH_TRIP_SOURCE='1' OR ORH_TRIP_SOURCE='5' OR ORH_TRIP_SOURCE='6' ) ";
			}
		}
		query=query+" GROUP BY ORH_TRIP_ID ORDER BY ORH_SERVICEDATE";

		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection(); 
		try {
			pst = conn.prepareStatement(query);
			//System.out.println("search query:"+pst.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				OpenRequestBO openRequestBean=new OpenRequestBO();
				openRequestBean.setDriverid(rs.getString("ORH_DRIVERID"));
				openRequestBean.setAssociateCode(rs.getString("FL_DESCRIPTION"));
				openRequestBean.setName(rs.getString("ORH_NAME"));
				openRequestBean.setPhone(rs.getString("ORH_PHONE"));
				openRequestBean.setTripid(rs.getString("ORH_TRIP_ID"));
				openRequestBean.setSadd1(rs.getString("ORH_STADD1"));
				openRequestBean.setSadd2(rs.getString("ORH_STADD2"));
				openRequestBean.setScity(rs.getString("ORH_STCITY"));
				openRequestBean.setSlat(rs.getString("ORH_STLATITUDE"));
				openRequestBean.setSlong(rs.getString("ORH_STLONGITUDE"));
				openRequestBean.setTripStatus(rs.getString("TRIPSTATUS"));
				openRequestBean.setSstate(rs.getString("ORH_STSTATE"));
				//openRequestBean.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBean.setSzip(rs.getString("ORH_STZIP"));
				openRequestBean.setEadd1(rs.getString("ORH_EDADD1"));
				openRequestBean.setEadd2(rs.getString("ORH_EDADD2"));
				openRequestBean.setEcity(rs.getString("ORH_EDCITY"));
				openRequestBean.setEstate(rs.getString("ORH_EDSTATE"));
				openRequestBean.setEdlatitude(rs.getString("ORH_EDLATITUDE"));
				openRequestBean.setEdlongitude(rs.getString("ORH_EDLONGITUDE"));
				openRequestBean.setEzip(rs.getString("ORH_EDZIP"));
				openRequestBean.setSdate(rs.getString("ORH_SERVICEDATE_FORMATTED"));
				openRequestBean.setPaytype(rs.getString("ORH_PAYTYPE"));
				openRequestBean.setAmt(new BigDecimal(rs.getString("ORH_AMT")));
				openRequestBean.setSpecialIns(rs.getString("ORH_SPLINS"));
				openRequestBean.setComments(rs.getString("ORH_OPERATOR_COMMENTS"));
				openRequestBean.setVehicleNo(rs.getString("ORH_VEHICLE_NO"));
				openRequestBean.setTripSource(rs.getInt("ORH_TRIP_SOURCE"));
				openRequestBean.setCreatedBy(rs.getString("ORH_CREATED_BY"));
				openRequestBean.setDrProfile(rs.getString("ORH_DRCABFLAG"));

				openRequestBean.setRouteNumber(rs.getString("ORH_ROUTE_NUMBER"));

				openRequestBean.setOR_SMS_SENT(rs.getString("TIME1"));
				openRequestBean.setCreatedTime(rs.getString("TIME2"));
				openRequestBean.setCompletedTime(rs.getString("TIME3"));

				openRequestBean.setAcct(rs.getString("ORH_PAYACC"));
				openRequestBean.setCallerName(rs.getString("ORH_CALLER_NAME"));
				openRequestBean.setCallerPhone(rs.getString("ORH_CALLER_PHONE"));
				openRequestBean.setRefNumber(rs.getString("ORH_CUST_REF_NUM"));
				openRequestBean.setRefNumber1(rs.getString("ORH_CUST_REF_NUM1"));
				openRequestBean.setRefNumber2(rs.getString("ORH_CUST_REF_NUM2"));
				openRequestBean.setRefNumber3(rs.getString("ORH_CUST_REF_NUM3"));
				openRequestBean.setNumOfPassengers(rs.getInt("ORH_NUMBER_OF_PASSENGERS"));

				openRequestBean.setShrs(rs.getString("STARTODO"));
				openRequestBean.setSmin(rs.getString("ENDODO"));

				openRequestBean.setMaterPayment(rs.getString("MeterPayment")==null?"empty":rs.getString("MeterPayment"));
				openReqHistory.add(openRequestBean);
			}

		}
		catch (Exception sqex)
		{
			cat.error("TDSException RequestDAO.openRequestHistory-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.openRequestHistory-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openReqHistory;
	}	


	public static OpenRequestBO reDispatch(OpenRequestBO openRequestBO,AdminRegistrationBO adminBO,String tripId,String time) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		//ArrayList<OpenRequestBO>openReqHistory = new ArrayList<OpenRequestBO>();
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst,m_pst= null;
		PreparedStatement m_pst1=null;
		ResultSet rs = null;
		int rSet=0;
		ResultSet rSet1=null;
		String query ="SELECT * FROM TDS_OPENREQUEST_HISTORY where ORH_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND ORH_TRIP_ID ='"+tripId+"'";
		String keygen;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection(); 
		OpenRequestBO openRequestBean=new OpenRequestBO();

		try{

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			/*int keyValue = ConfigDAO.tdsKeyGen("TRIPID",conn);
			if(keyValue > 0) {
				keygen = ""+keyValue;
			} else {
				return openReqHistory;
			}*/
			while(rs.next()) {
				openRequestBean.setName(rs.getString("ORH_NAME"));
				openRequestBean.setPhone(rs.getString("ORH_PHONE"));
				openRequestBean.setTripid(rs.getString("ORH_TRIP_ID"));
				openRequestBean.setSadd1(rs.getString("ORH_STADD1"));
				openRequestBean.setSadd2(rs.getString("ORH_STADD2"));
				openRequestBean.setScity(rs.getString("ORH_STCITY"));
				openRequestBean.setSlat(rs.getString("ORH_STLATITUDE"));
				openRequestBean.setSlong(rs.getString("ORH_STLONGITUDE"));
				openRequestBean.setTripStatus(rs.getString("ORH_TRIP_STATUS"));
				openRequestBean.setSstate(rs.getString("ORH_STSTATE"));
				openRequestBean.setTripStatus(rs.getString("ORH_SERVICESTATUS"));
				openRequestBean.setSzip(rs.getString("ORH_STZIP"));
				openRequestBean.setEadd1(rs.getString("ORH_EDADD1"));
				openRequestBean.setEadd2(rs.getString("ORH_EDADD2"));
				openRequestBean.setEcity(rs.getString("ORH_EDCITY"));
				openRequestBean.setEstate(rs.getString("ORH_EDSTATE"));
				openRequestBean.setEdlatitude(rs.getString("ORH_EDLATITUDE"));
				openRequestBean.setEdlongitude(rs.getString("ORH_EDLONGITUDE"));
				openRequestBean.setEzip(rs.getString("ORH_EDZIP"));
				openRequestBean.setShrs(rs.getString("ORH_SERVICETIME"));
				openRequestBean.setAmt(new BigDecimal(rs.getString("ORH_AMT")));
				openRequestBean.setSlandmark(rs.getString("ORH_LANDMARK"));
				openRequestBean.setSpecialIns(rs.getString("ORH_SPLINS"));
				openRequestBean.setPaytype(rs.getString("ORH_PAYTYPE"));
				openRequestBean.setAcct(rs.getString("ORH_PAYACC"));
				openRequestBean.setComments(rs.getString("ORH_OPERATOR_COMMENTS"));
				openRequestBean.setElandmark(rs.getString("ORH_ELANDMARk"));
				openRequestBean.setQueueno(rs.getString("ORH_QUEUENO"));
				openRequestBean.setTypeOfRide(rs.getString("ORH_SHARED_RIDE"));
				openRequestBean.setDontDispatch(rs.getInt("ORH_DONT_DISPATCH"));
				openRequestBean.setTripSource(rs.getInt("ORH_TRIP_SOURCE"));
				openRequestBean.setAdvanceTime(rs.getString("ORH_DISPATCH_LEAD_TIME"));
				openRequestBean.setPaymentMeter(rs.getInt("ORH_METER_TYPE"));
				//openReqHistory.add(openRequestBean);

			}
			/*	m_pst = conn.prepareStatement("INSERT INTO TDS_OPENREQUEST (OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EDSTATE,OR_EDZIP,OR_ASSOCCODE,OR_STLATITUDE,OR_STLONGITUDE,OR_ENTEREDTIME,OR_NAME,OR_TRIP_STATUS,OR_DISPATCH_STATUS,OR_DRIVERID,OR_SERVICEDATE,OR_SERVICETIME,OR_SMS_SENT,OR_AMT,OR_DISPATCH_START_TIME,OR_LANDMARK,OR_SPLINS,OR_PAYTYPE,OR_PAYACC,OR_CREATED_BY,OR_COMMENTS,OR_TRIP_SOURCE,OR_ELANDMARK,OR_QUEUENO) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,NOW(),?,?,?,?,NOW(),?,NOW(),?,NOW(),?,?,?,?,?,?,1,?,?)");

			m_pst.setString(1,openRequestBean.getPhone());
			m_pst.setString(2,openRequestBean.getSadd1());
			m_pst.setString(3, openRequestBean.getSadd2());
			m_pst.setString(4,openRequestBean.getScity());
			m_pst.setString(5,openRequestBean.getSstate());
			m_pst.setString(6,openRequestBean.getSzip());
			m_pst.setString(7,openRequestBean.getEadd1());
			m_pst.setString(8,openRequestBean.getEadd2());
			m_pst.setString(9,openRequestBean.getEcity());
			m_pst.setString(10,openRequestBean.getEstate());
			m_pst.setString(11,openRequestBean.getEzip());
			m_pst.setString(12,adminBO.getAssociateCode());
			m_pst.setString(13,openRequestBean.getSlat());
			m_pst.setString(14,openRequestBean.getSlong());
			m_pst.setString(15,openRequestBean.getName());
			m_pst.setInt(16,8);
			m_pst.setInt(17,4);
			m_pst.setString(18,"");
			m_pst.setString(19, time);
			m_pst.setBigDecimal(20,openRequestBean.getAmt());
			m_pst.setString(21, openRequestBean.getSlandmark());
			m_pst.setString(22, openRequestBean.getSpecialIns());
			m_pst.setString(23, openRequestBean.getPaytype());
			m_pst.setString(24, openRequestBean.getAcct());
			m_pst.setString(25, adminBO.getUid());
			m_pst.setString(26, openRequestBean.getComments());
			m_pst.setString(27, openRequestBean.getElandmark());
			m_pst.setString(28,openRequestBean.getQueueno());
			rSet=m_pst.executeUpdate();
			if(rSet==1){
				pst=conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS,ORL_LATITUDE,ORL_LONGITUDE) VALUES (?,NOW(),?,?,?,?,?,?)");
				pst.setString(1, openRequestBean.getTripid());
				pst.setString(2, "Job Redispatched");
				pst.setString(3, adminBO.getUid());
				pst.setString(4, adminBO.getAssociateCode());
				pst.setInt(5, TDSConstants.reDispatch);
				pst.setString(6, openRequestBean.getSlat());
				pst.setString(7, openRequestBean.getSlong());
				pst.executeUpdate();
			}
			 */
		}catch (Exception sqex){
			cat.error("TDSException RequestDAO.reDispatch-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			sqex.printStackTrace();
		}finally{
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openRequestBean;
	}

	public static ArrayList<OpenRequestBO> openRequestSummary(OpenRequestBO openRequestSummary,AdminRegistrationBO adminBO,String value,String fromDate,String toDate,int filter, String assocCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO>openReqSummary = new ArrayList<OpenRequestBO>();
		TDSConnection dbConn = new TDSConnection();
		Connection conn =  dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query="SELECT *,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i') AS OR_SERVICEDATE_FORMATTED FROM TDS_OPENREQUEST  where ";
		if(assocCode!=null && !assocCode.equals("")){
			query = query + "(OR_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND OR_ASSOCCODE='"+assocCode+"') AND (OR_DISPATCH_START_TIME>NOW() )";
		}else{
			query = query + "(OR_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' OR OR_ASSOCCODE='"+adminBO.getAssociateCode()+"') AND (OR_DISPATCH_START_TIME>NOW() )";
		}
		if(!value.equals("")){
			query =query+" AND( OR_DRIVERID like '%"+value+"%' OR OR_PHONE like'%"+value+"%' OR OR_NAME like'%"+value+"%' OR OR_TRIP_ID like'%"+value+"%' OR OR_VEHICLE_NO like'%"+value+"%' OR OR_STCITY like'%"+value+"%' OR OR_STSTATE like'%"+value+"%' OR OR_STZIP  like'%"+value+"%' OR OR_STADD1  like'%"+value+"%' OR OR_STADD2  like'%"+value+"%' OR OR_EDADD1  like'%"+value+"%' OR OR_EDADD2  like'%"+value+"%'  ) ";
		}
		if(!fromDate.equals("")&&!fromDate.equals(null) && !toDate.equals("") && ! toDate.equals(null)){
			query=query+" AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d')<='"+toDate+"' AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d')>='"+fromDate+"'";	
		}
		if(filter==1){
			query=query+" AND OR_PAYTYPE='VC'";
		}else if(filter==2){
			query=query+" AND OR_REPEAT_GROUP <> 0 " ;
		}
		query=query+" ORDER BY OR_ASSOCCODE,OR_SERVICEDATE";

		try {

			pst = conn.prepareStatement(query); 
			rs = pst.executeQuery();
			//System.out.println("reservation query:"+pst.toString());
			while (rs.next()) {
				OpenRequestBO openRequestBean=new OpenRequestBO();
				openRequestBean.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBean.setName(rs.getString("OR_NAME"));
				openRequestBean.setPhone(rs.getString("OR_PHONE"));
				openRequestBean.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBean.setSadd1(rs.getString("OR_STADD1"));
				openRequestBean.setSadd2(rs.getString("OR_STADD2"));
				openRequestBean.setScity(rs.getString("OR_STCITY"));
				openRequestBean.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBean.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBean.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBean.setStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBean.setSstate(rs.getString("OR_STSTATE"));
				openRequestBean.setSzip(rs.getString("OR_STZIP"));
				openRequestBean.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBean.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBean.setEcity(rs.getString("OR_EDCITY"));
				openRequestBean.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBean.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBean.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBean.setEzip(rs.getString("OR_EDZIP"));
				openRequestBean.setStartTimeStamp(rs.getString("OR_SERVICEDATE_FORMATTED"));
				openRequestBean.setRepeatGroup(rs.getString("OR_REPEAT_GROUP"));
				openRequestBean.setComments(rs.getString("OR_COMMENTS"));
				openRequestBean.setSpecialIns(rs.getString("OR_SPLINS"));
				openRequestBean.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				openRequestBean.setCreatedBy(rs.getString("OR_CREATED_BY"));
				openRequestBean.setAssociateCode(rs.getString("OR_ASSOCCODE"));
				openRequestBean.setDontDispatch(rs.getInt("OR_DONT_DISPATCH"));
				openRequestBean.setSlandmark(rs.getString("OR_LANDMARK"));
				openRequestBean.setElandmark(rs.getString("OR_ELANDMARK"));
				openRequestBean.setDrProfile(rs.getString("OR_DRCABFLAG"));
				openRequestBean.setRefNumber(rs.getString("OR_CUST_REF_NUM"));
				openRequestBean.setNumOfPassengers(rs.getInt("OR_NUMBER_OF_PASSENGERS"));
				openRequestBean.setPaytype((rs.getString("OR_PAYTYPE")!=null && !rs.getString("OR_PAYTYPE").equals(""))?rs.getString("OR_PAYTYPE"):"Cash");
				openRequestBean.setAcct(rs.getString("OR_PAYACC"));
				openReqSummary.add(openRequestBean);
			}

		}
		catch (Exception sqex){
			cat.error("TDSException RequestDAO.openRequestSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.openRequestSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openReqSummary;
	}



	public static ArrayList<VoucherBO> getComments(AdminRegistrationBO adminBO,String account){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<VoucherBO> comments=new ArrayList<VoucherBO>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT VC_CONTACT_PERSON,VC_SPECIAL_FLAGS,VC_FIXED_RATE,VC_DRIVER_COMMENTS,VC_OPERATOR_COMMENTS,VC_DESCR,VC_EXPIRYDATE,VC_START_AMOUNT,VC_RATE_PER_MILE,VC_COMPANYNAME,VC_STATUS,VC_EXPIRYDATE - date_format(now(),'%Y%m%d') as DATE FROM TDS_GOVT_VOUCHER WHERE VC_ASSOCCODE=? AND VC_VOUCHERNO=?");
			pst.setString(1, adminBO.getMasterAssociateCode());
			pst.setString(2,account);
			rs=pst.executeQuery();
			while(rs.next()){
				VoucherBO comment=new VoucherBO();
				comment.setVcontact(rs.getString("VC_CONTACT_PERSON"));
				comment.setVamount(rs.getString("VC_FIXED_RATE"));
				comment.setVdesc(rs.getString("VC_DESCR"));
				comment.setVexpdate(TDSValidation.getUserDateFormat(rs.getString("VC_EXPIRYDATE")));
				comment.setTo_date(rs.getString("DATE"));
				comment.setCompanyName(rs.getString("VC_COMPANYNAME"));
				comment.setRatePerMile(rs.getString("VC_RATE_PER_MILE"));
				comment.setStartAmount(rs.getString("VC_START_AMOUNT"));
				comment.setDriverComments(rs.getString("VC_DRIVER_COMMENTS"));
				comment.setOperatorComments(rs.getString("VC_OPERATOR_COMMENTS"));
				comment.setSpecialFlags(rs.getString("VC_SPECIAL_FLAGS"));
				//comment.setV_status(rs.getInt("VC_STATUS")==1?true:false);
				comment.setVstatus(rs.getString("VC_STATUS"));
				comments.add(comment);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException RequestdDAO.getComments-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestdDAO.getComments-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return comments;
	}


	public static int insertNewLandMark(String associationCode,OpenRequestBO openRequestBean,int State,String landMark ){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null; 
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection(); 
		int result=0;
		try {
			m_pst = m_conn.prepareStatement("insert into TDS_LANDMARK_PENDING(LP_ASSCODE,LP_ADD1,LP_ADD2,LP_CITY,LP_STATE,LP_ZIP,LP_LATITUDE,LP_LONGITUDE,LP_PHONE,LP_NAME) values (?,?,?,?,?,?,?,?,?,?)");
			m_pst.setString(1, associationCode);
			m_pst.setString(9, openRequestBean.getPhone());
			m_pst.setString(10, landMark);
			if(State==1){
				m_pst.setString(2, openRequestBean.getSadd1());
				m_pst.setString(3, openRequestBean.getSadd2());
				m_pst.setString(4, openRequestBean.getScity());
				m_pst.setString(5, openRequestBean.getSstate());
				m_pst.setString(6, openRequestBean.getSzip()); 
				m_pst.setString(7, openRequestBean.getSlat());
				m_pst.setString(8, openRequestBean.getSlong());
			} else {
				m_pst.setString(2, openRequestBean.getEadd1());
				m_pst.setString(3, openRequestBean.getEadd2());
				m_pst.setString(4, openRequestBean.getEcity());
				m_pst.setString(5, openRequestBean.getEstate());
				m_pst.setString(6, openRequestBean.getEzip()); 
				m_pst.setString(7, openRequestBean.getEdlatitude());
				m_pst.setString(8, openRequestBean.getEdlongitude());
			}
			result=m_pst.executeUpdate(); 

		} catch (Exception sqex)
		{
			cat.error("TDSException RequestDAO.insertNewLandMark-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RequestDAO.insertNewLandMark-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static ArrayList<OpenRequestBO> getAddress(String MasrerCode,String phone){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ArrayList<OpenRequestBO> addAddress=new ArrayList<OpenRequestBO>();
		try {
			pst = con.prepareStatement("SELECT CU_ADD1,CU_PHONENO,CU_ADD2,CU_CITY,CU_STATE,CU_ZIP,CU_LATITUDE,CU_LONGITUDE,CU_MASTER_KEY FROM TDS_USERADDRESS WHERE CU_MASTERASSOCIATIONCODE=? AND CU_PHONENO=?");
			pst.setString(1, MasrerCode);
			pst.setString(2,phone);
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestBO Address= new OpenRequestBO();
				Address.setPhone(rs.getString("CU_PHONENO"));
				//Address.setName(rs.getString("CU_NAME"));
				Address.setSadd1(rs.getString("CU_ADD1"));
				Address.setSadd2(rs.getString("CU_ADD2"));
				Address.setScity(rs.getString("CU_CITY"));
				Address.setSstate(rs.getString("CU_STATE"));
				Address.setSzip(rs.getString("CU_ZIP"));
				Address.setSlat(rs.getString("CU_LATITUDE"));
				Address.setSlong(rs.getString("CU_LONGITUDE"));
				Address.setMasterAddressKey(rs.getString("CU_MASTER_KEY"));
				addAddress.add(Address);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException RequestdDAO.getAddress-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CRequestdDAO.getAddress-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return addAddress;
	}

	public static boolean setCallerIdAccept(AdminRegistrationBO adminBO,String lineNumber){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		boolean success=false;
		try{
			pst = con.prepareStatement("UPDATE TDS_CALLERID SET CID_STATUS=1,CID_ACCEPTED_BY=?,CID_ACCEPTED_TIME=now() WHERE CID_LINE_NO=?  AND CID_ASSOCIATIONCODE=?");
			pst.setString(1, adminBO.getUid());
			pst.setString(2, lineNumber);
			pst.setString(3, adminBO.getAssociateCode());
			pst.execute();
			pst=con.prepareStatement("UPDATE TDS_CALLERID SET CID_STATUS=9 where CID_ASSOCIATIONCODE='"+adminBO.getAssociateCode()+"' and CID_UPDATETIME < DATE_SUB(now(),INTERVAL 5 minute) and CID_STATUS < 5 ");
			pst.execute();
			success=true;
		}catch(SQLException sqex) {
			cat.error("TDSException RequestdDAO.setCallerIdAccept-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CRequestdDAO.setCallerIdAccept-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}

	public static boolean setCallerIdAccept(AdminRegistrationBO adminBO,int lineNumber,String phNum){

		
		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		boolean success=false;
		boolean newData=false;
		try{
			
			pst = con.prepareStatement("select * from TDS_CALLERID WHERE CID_LINE_NO=? AND CID_PHONE_NUMBER='"+phNum+"' AND CID_ASSOCIATIONCODE=?");
			pst.setLong(1, lineNumber);
			pst.setString(2, adminBO.getAssociateCode());
			rs=pst.executeQuery();
			while(rs.next()){
				newData=true;
			}
			if(newData){
				pst = con.prepareStatement("UPDATE TDS_CALLERID SET CID_STATUS=1,CID_ACCEPTED_BY=?,CID_ACCEPTED_TIME=CONVERT_TZ(now(),'UTC',?) WHERE (CID_LINE_NO=? OR CID_PHONE_NUMBER='"+phNum+"') AND CID_ASSOCIATIONCODE=?");
				pst.setString(1, adminBO.getUid());
				pst.setString(2, adminBO.getTimeZone());
				pst.setLong(3, lineNumber);
				pst.setString(4, adminBO.getAssociateCode());
				pst.execute();
				success= true;
			}
			//Commenting below code. Can update status on the callerid button refresh timer
			//pst=con.prepareStatement("UPDATE TDS_CALLERID SET CID_STATUS=9 where CID_ASSOCIATIONCODE='"+adminBO.getAssociateCode()+"' and CID_UPDATETIME < DATE_SUB(now(),INTERVAL 5 minute) and CID_STATUS < 5 ");
			//pst.execute();
		}catch(SQLException sqex) {
			cat.error("TDSException RequestdDAO.setCallerIdAccept-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CRequestdDAO.setCallerIdAccept-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}
	public static ArrayList<OpenRequestBO> getAllOR(AdminRegistrationBO adminBo,OpenRequestBO openBo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> orList=new ArrayList<OpenRequestBO>();
		boolean successOfOperation = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String Query="";
		if(openBo!=null){
			if(!openBo.getRouteNumber().equals("0") && !openBo.getRouteNumber().equals("")){
				Query=" AND OR_ROUTE_NUMBER='"+openBo.getRouteNumber()+"'";
			}else if (openBo.getChckTripStatus()==1 && openBo.getNumOfPassengers()==0){
				Query=Query+" AND OR_ROUTE_NUMBER <> '0'";
			} else if(openBo.getChckTripStatus()==0 && openBo.getNumOfPassengers()==1){
				Query=Query+" AND OR_ROUTE_NUMBER = '0'";
			}if(!openBo.getDriverid().equals("")){
				Query=Query+" AND OR_DRIVERID='"+openBo.getDriverid()+"'";
			} if(!openBo.getSdate().equals("")){
				Query=Query+" AND DATE_FORMAT(OR_SERVICEDATE,'%m/%d/%Y') >= '"+openBo.getSdate()+"'"; 
			}if(openBo.getDropTime()!=null && !openBo.getDropTime().equals("")){
				Query=Query+" AND DATE_FORMAT(OR_SERVICEDATE,'%m/%d/%Y') <= '"+openBo.getDropTime()+"'"; 
			}
		}
		try{
			con.setAutoCommit(false);
			pst =con.prepareStatement("SELECT OR_STLONGITUDE,OR_ROUTE_NUMBER,OR_ROUTE_NUMBER,OR_DRIVERID,OR_TRIP_STATUS,OR_STLATITUDE,OR_TRIP_ID,OR_NUMBER_OF_PASSENGERS,OR_DROP_TIME FROM TDS_OPENREQUEST WHERE OR_ASSOCCODE='" + adminBo.getAssociateCode() + "' AND OR_SHARED_RIDE=1"+Query);
			//System.out.println("Query--->"+pst);
			rs = pst.executeQuery();
			while(rs.next()){
				OpenRequestBO orBo =new OpenRequestBO();
				orBo.setSlat(rs.getString("OR_STLATITUDE"));
				orBo.setSlong(rs.getString("OR_STLONGITUDE"));
				orBo.setTripid(rs.getString("OR_TRIP_ID"));
				orBo.setNumOfPassengers(rs.getString("OR_NUMBER_OF_PASSENGERS")!=null&&!rs.getString("OR_NUMBER_OF_PASSENGERS").equals("")?Integer.parseInt(rs.getString("OR_NUMBER_OF_PASSENGERS")):1);
				orBo.setDropTime(rs.getString("OR_DROP_TIME"));
				orBo.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				orBo.setDriverid(rs.getString("OR_DRIVERID"));
				orBo.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				orList.add(orBo);
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.getAllOR-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getAllOR" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return orList;
	}
	public static ArrayList<OperatorShift> readOperatorShift(String associationCode,String operatorId,String date){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		ArrayList<OperatorShift> operatorList=new ArrayList<OperatorShift>();
		String query="";
		dbcon = new TDSConnection();
		csp_conn = dbcon.getConnection();
		if(!operatorId.equals("")){
			query="AND SRH_OPERATORID="+operatorId;
		}if(!date.equals("")){
			query=query+"AND SRH_DATE='"+date+"'";
		}
		try {
			csp_pst = csp_conn.prepareStatement("SELECT *,TIMEDIFF(SRH_CLOSING_TIME,SRH_OPEN_TIME) AS TIME FROM TDS_SHIFT_REGISTER_HISTORY WHERE SRH_ASSOCCODE=? AND SRH_REGISTER_STATUS!='L'"+query+" ORDER BY SRH_OPERATORID,SRH_REGISTER_STATUS"); 
			csp_pst.setString(1, associationCode);
			rs = csp_pst.executeQuery();
			while(rs.next()){
				OperatorShift OperatorBean = new OperatorShift();
				OperatorBean.setStatus(rs.getString("SRH_REGISTER_STATUS"));
				OperatorBean.setOperatorId(rs.getString("SRH_OPERATORID"));
				OperatorBean.setOpenTime(rs.getString("TIME"));
				OperatorBean.setOpenedBy(rs.getString("SRH_OPENED_BY"));
				OperatorBean.setClosedBy(rs.getString("SRH_CLOSED_BY"));
				OperatorBean.setCloseTime(rs.getString("SRH_DATE"));
				operatorList.add(OperatorBean);
			}
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.readShiftRegisterMaster-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException RequestDAO.readShiftRegisterMaster-->" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return operatorList;
	}



	public static OperatorShift readShiftRegisterMaster(String associationCode, String operatorID){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.readCashRegisterMaster");
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		OperatorShift OperatorBean = new OperatorShift();
		OperatorBean.setRegisterOpen(false);
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.getShiftRegister); 
			csp_pst.setString(1, associationCode);
			csp_pst.setString(2, operatorID);
			cat.info(csp_pst.toString());
			rs = csp_pst.executeQuery();
			if(rs.next()){
				OperatorBean.setStatus(rs.getString("SRM_REGISTER_STATUS"));
				OperatorBean.setOperatorId(rs.getString("SRM_OPERATORID"));
				OperatorBean.setRegisterOpen(true);
				readSumOfRegisterDetails(OperatorBean);
			}

		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.readShiftRegisterMaster-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException RequestDAO.readShiftRegisterMaster-->" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return OperatorBean;
	}
	public static void readSumOfRegisterDetails(OperatorShift OperatorBean ){
	}
	public static int insertShiftRegisterMaster(OperatorShift cspBean){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.insertCashRegisterMaster");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null,csp_pst1 = null;
		ResultSet rs=null;
		int uniqueKey=0;
		//		Date date=new Date();
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.insertShiftRegister, Statement.RETURN_GENERATED_KEYS); 
			csp_pst.setString(1, cspBean.getOperatorId());
			csp_pst.setString(2, cspBean.getAssociateCode());
			csp_pst.setString(3, cspBean.getOpenedBy());
			csp_pst.setString(4, cspBean.getStatus());
			csp_pst.setDouble(5, cspBean.getOdometerValue());
			csp_pst.setString(6, cspBean.getVehicleNo());
			csp_pst.execute();
			rs = csp_pst.getGeneratedKeys();
			if(rs.next()){
				uniqueKey = rs.getInt(1);
			}
			csp_pst1 = csp_conn.prepareStatement(TDSSQLConstants.insertShiftRegisterDetail, Statement.RETURN_GENERATED_KEYS); 
			csp_pst1.setString(1, cspBean.getOperatorId());
			csp_pst1.setString(2, cspBean.getAssociateCode());
			csp_pst1.setString(3, cspBean.getOpenedBy());
			csp_pst1.setString(4, cspBean.getStatus());
			csp_pst1.setInt(5, uniqueKey);
			csp_pst1.setString(6, cspBean.getVehicleNo());
			csp_pst1.execute();

			success = true;
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.insertShiftRegisterMaster-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException RequestDAO.insertShiftRegisterMaster" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return uniqueKey;
	}
	public static boolean updateShiftRegisterDetail(OperatorShift cspBean){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.closeRegister");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs=null;
		String status;
		//		Date date=new Date();
		if(cspBean.getStatus().equals("A")){
			status="B";
		} else {
			status="A";
		}
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.updateShiftRegisterDetail); 
			csp_pst.setString(1, cspBean.getClosedBy());
			csp_pst.setString(2, cspBean.getOperatorId());
			csp_pst.setString(3, cspBean.getAssociateCode());
			csp_pst.setString(4, cspBean.getStatus());
			csp_pst.execute();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.insertShiftRegisterDetail); 
			csp_pst.setString(1, cspBean.getOperatorId());
			csp_pst.setString(2, cspBean.getAssociateCode());
			csp_pst.setString(3, cspBean.getClosedBy());
			csp_pst.setString(4, status);
			csp_pst.setInt(5, cspBean.getKey());
			csp_pst.setString(6, cspBean.getVehicleNo());
			csp_pst.execute();
			csp_pst = csp_conn.prepareStatement("UPDATE TDS_SHIFT_REGISTER_MASTER SET SRM_REGISTER_STATUS=? WHERE SRM_OPERATORID=? AND SRM_ASSOCCODE=? "); 
			csp_pst.setString(1, status);
			csp_pst.setString(2, cspBean.getOperatorId());
			csp_pst.setString(3, cspBean.getAssociateCode());
			cat.info(csp_pst.toString());
			csp_pst.execute();

			success = true;
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.updateShiftRegisterDetail-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException RequestDAO.updateShiftRegisterDetail-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}
	public static boolean closeShiftRegisterMaster(OperatorShift cspBean){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO FinanceDAO.closeRegister");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs=null;
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.closeShiftRegisterMaster); 
			csp_pst.setString(1, cspBean.getClosedBy());
			csp_pst.setDouble(2, cspBean.getOdometerValue());
			csp_pst.setString(3, cspBean.getOperatorId());
			csp_pst.setString(4, cspBean.getAssociateCode());
			csp_pst.execute();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.closeShiftRegisterDetail); 
			csp_pst.setString(1, cspBean.getClosedBy());
			csp_pst.setString(2, cspBean.getOperatorId());
			csp_pst.setString(3, cspBean.getAssociateCode());
			csp_pst.setString(4, cspBean.getStatus());
			csp_pst.execute();
			csp_pst=csp_conn.prepareStatement("INSERT  INTO TDS_SHIFT_REGISTER_MASTER_HISTORY (SELECT * FROM TDS_SHIFT_REGISTER_MASTER WHERE SRM_OPERATORID=? AND SRM_ASSOCCODE=? AND SRM_KEY=?)");
			csp_pst.setString(1, cspBean.getOperatorId());
			csp_pst.setString(2, cspBean.getAssociateCode());
			csp_pst.setInt(3, cspBean.getKey());
			csp_pst.execute();
			csp_pst=csp_conn.prepareStatement("INSERT  INTO TDS_SHIFT_REGISTER_DETAIL_HISTORY (SELECT * FROM TDS_SHIFT_REGISTER_DETAIL WHERE SRD_OPERATORID=? AND SRD_ASSOCCODE=? AND SRD_KEY=?)");
			csp_pst.setString(1, cspBean.getOperatorId());
			csp_pst.setString(2, cspBean.getAssociateCode());
			csp_pst.setInt(3, cspBean.getKey());
			csp_pst.execute();
			csp_pst = csp_conn.prepareStatement("DELETE FROM TDS_SHIFT_REGISTER_MASTER WHERE SRM_ASSOCCODE = ? AND SRM_OPERATORID=?");
			csp_pst.setString(1, cspBean.getAssociateCode());
			csp_pst.setString(2, cspBean.getOperatorId());
			csp_pst.execute();
			csp_pst = csp_conn.prepareStatement("DELETE FROM TDS_SHIFT_REGISTER_DETAIL WHERE SRD_ASSOCCODE = ? AND SRD_OPERATORID=?");
			csp_pst.setString(1, cspBean.getAssociateCode());
			csp_pst.setString(2, cspBean.getOperatorId());
			csp_pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.closeShiftRegisterMaster-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException RequestDAO.closeShiftRegisterMaster-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}

	public static ArrayList<OperatorShift> readAllOperator(String associationCode,String operatorId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.readCashRegisterMaster");
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		String Query="";
		if(!operatorId.equals("")){
			Query=" AND SRM_OPERATORID='"+operatorId+"'";
		}
		ArrayList<OperatorShift> operatorList=new ArrayList<OperatorShift>();
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement("SELECT * FROM TDS_SHIFT_REGISTER_MASTER WHERE SRM_ASSOCCODE=?"+Query); 
			csp_pst.setString(1, associationCode);
			rs = csp_pst.executeQuery();
			while(rs.next()){
				OperatorShift OperatorBean = new OperatorShift();
				OperatorBean.setStatus(rs.getString("SRM_REGISTER_STATUS"));
				OperatorBean.setOperatorId(rs.getString("SRM_OPERATORID"));
				OperatorBean.setKey(Integer.parseInt(rs.getString("SRM_KEY")));
				operatorList.add(OperatorBean);
			}
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.readShiftRegisterMaster-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException RequestDAO.readShiftRegisterMaster-->" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return operatorList;
	}

	public static ArrayList<OpenRequestBO> getAllocatedJobs(String assocode,String driverId, String cabNo,String timeOffset,String master){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> orList=new ArrayList<OpenRequestBO>();
		boolean successOfOperation = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1 = null;
		String query1= "";
		ResultSet rs,rs1 = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query ="SELECT *, B.SRD_PICKUP_ORDER as pickup,C.SRD_PICKUP_ORDER as dropOff,A.OR_TRIP_ID,CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"') AS OFFSET_DATETIME,D.SRD_ORDER_SWITCH AS SWITCH FROM TDS_OPENREQUEST AS A  LEFT JOIN  TDS_SHARED_RIDE_DETAILS as B on A.OR_TRIP_ID = B.srd_trip_id AND A.OR_ASSOCCODE = B.SRD_ASSOCCODE AND B.SRD_ORDER_SWITCH ='0' LEFT JOIN TDS_SHARED_RIDE_DETAILS as C ON A.OR_TRIP_ID = C.SRD_TRIP_ID AND A.OR_ASSOCCODE =C.SRD_ASSOCCODE AND C.SRD_ORDER_SWITCH ='1' LEFT JOIN TDS_SHARED_RIDE_DETAILS as D ON A.OR_TRIP_ID = D.SRD_TRIP_ID AND A.OR_ASSOCCODE =D.SRD_ASSOCCODE LEFT JOIN TDS_GOVT_VOUCHER ON A.OR_MASTER_ASSOCCODE=VC_ASSOCCODE AND VC_VOUCHERNO=A.OR_PAYACC WHERE A.OR_MASTER_ASSOCCODE ='"+assocode+"' AND OR_TRIP_STATUS IN ('"+TDSConstants.jobAllocated+"','"+TDSConstants.onRotueToPickup+"','"+TDSConstants.onSite+"','"+TDSConstants.tripStarted+"','"+TDSConstants.driverReportedNoShow+"','"+TDSConstants.jobSTC+"') AND OR_DRIVERID='"+driverId+"' ORDER BY OR_SERVICEDATE" ;
		try{
			pst =  con.prepareStatement(query);
			//System.out.println("getalljobs:"+pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				OpenRequestBO openRequestBO=new OpenRequestBO();
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setPhone(rs.getString("OR_PHONE"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setSadd2(rs.getString("OR_STADD2"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setSstate(rs.getString("OR_STSTATE"));
				openRequestBO.setSzip(rs.getString("OR_STZIP"));
				openRequestBO.setStartTimeStamp(rs.getString("OFFSET_DATETIME"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setAcct(rs.getString("OR_PAYACC"));
				openRequestBO.setPaytype(rs.getString("OR_PAYTYPE"));
				openRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				openRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
				openRequestBO.setElandmark(rs.getString("OR_ELANDMARK"));
				openRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				openRequestBO.setEzip(rs.getString("OR_EDZIP"));
				openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBO.setNumOfPassengers(rs.getString("OR_NUMBER_OF_PASSENGERS")!=null&&!rs.getString("OR_NUMBER_OF_PASSENGERS").equals("")?Integer.parseInt(rs.getString("OR_NUMBER_OF_PASSENGERS")):1);
				openRequestBO.setPickupOrderSwitch(Integer.parseInt(rs.getString("SWITCH")!=null?rs.getString("SWITCH"):"99"));
				openRequestBO.setPickupOrder(rs.getString("pickup")!=null?rs.getString("pickup"):"0");
				openRequestBO.setDropOffOrder(rs.getString("dropOff")!=null?rs.getString("dropOff"):"0");
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setSharedRidePaymentType(rs.getInt("OR_SHAREDRIDE_PAYTYPE"));
				openRequestBO.setDispatchLeadTime(rs.getInt("OR_DISPATCH_LEAD_TIME"));
				openRequestBO.setPaymentMeter(rs.getInt("OR_METER_TYPE"));
				openRequestBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
				openRequestBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
				openRequestBO.setAirFrom(rs.getString("OR_AIRLINE_FROM"));
				openRequestBO.setAirTo(rs.getString("OR_AIRLINE_TO"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setEndQueueno(rs.getString("OR_END_QUEUENO"));
				openRequestBO.setAcct(rs.getString("OR_PAYACC"));
				openRequestBO.setAcctName(rs.getString("VC_COMPANYNAME"));
				openRequestBO.setEmail(rs.getString("OR_EMAIL"));
				
				openRequestBO.setTripSource(rs.getInt("OR_TRIP_SOURCE"));
				
				if(rs.getInt("OR_METER_TYPE")==0){
					query1 = " AND MD_DEFAULT=1";
				} else {
					query1 = " AND MD_KEY='"+rs.getInt("OR_METER_TYPE")+"'";
				}
				pst1=con.prepareStatement("SELECT * FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+master+"'"+query1);
				//System.out.println("Meter type query: "+pst1.toString());
				rs1=pst1.executeQuery();
				if(rs1.next()){
					openRequestBO.setRatePerMile(Double.parseDouble(rs1.getString("MD_RATE_PER_MILE")));
					openRequestBO.setRatePerMin(Double.parseDouble(rs1.getString("MD_RATE_PER_MIN")));
					openRequestBO.setStartAmt(Double.parseDouble(rs1.getString("MD_START_AMOUNT")));
					openRequestBO.setMinSpeed(Integer.parseInt(rs1.getString("MD_MINIMUM_SPEED")));
					//System.out.println("speed: "+openRequestBO.getMinSpeed());
					//System.out.println("satrt amount: "+openRequestBO.getStartAmt());
				}

				orList.add(openRequestBO); 
			}
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.getAllocatedJobs-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getAllocatedJobs-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return orList;
	}
	public static void updateClientAddress(String assocode,String addressKey, String masterKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("UPDATE TDS_USERADDRESS SET CU_UPDATEDTIME=NOW() WHERE CU_ADDRESS_KEY=? AND CU_MASTER_KEY=? AND CU_MASTERASSOCIATIONCODE=?");
			pst.setString(1, addressKey);
			pst.setString(2, masterKey);
			pst.setString(3, assocode);
			pst.execute();
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.updateClientAddress-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateClientAddresss-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static int updateJobDriver(String assocode,String driverId, ArrayList<String> trips,String routeNumber,int type){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int driverUpdated=0;
		String Query="",tripId="";
		String field="";
		if(type==1){
			field=",OR_ROUTE_NUMBER='"+routeNumber+"'";
		} else {
			field=",OR_VEHICLE_NO='"+routeNumber+"'";
		}
		if(trips!=null && trips.size()>0){
			for(int i=0;i<trips.size()-1;i++){
				tripId= tripId+"'"+trips.get(i)+"'"+",";
			} 
			tripId=tripId+"'"+trips.get(trips.size()-1)+"'";
			Query=" AND OR_TRIP_ID IN ("+tripId+")";
			cat.info("RequestDAO.updateJobDriver Query--->"+Query);
		}
		try{
			pst=con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_DRIVERID=?"+field+" WHERE OR_ASSOCCODE=?"+Query);
			pst.setString(1, driverId);
			pst.setString(2, assocode);
			cat.info("RequestDAO.updateJobDriver Execute--->"+pst);
			driverUpdated=pst.executeUpdate();
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.updateJobDriver-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateJobDriver-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverUpdated;
	}
	public static void updateCookies(ArrayList names,ArrayList values, String userName,String assoCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateCookies");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("DELETE FROM TDS_USER_COOKIES WHERE UC_ASSOCCODE=? AND UC_USER=?");
			pst.setString(1, assoCode);
			pst.setString(2, userName);
			cat.info(pst.toString());
			pst.executeUpdate();
			for(int i=0;i<names.size();i++){
				pst = con.prepareStatement("INSERT INTO TDS_USER_COOKIES(UC_NAME,UC_VALUE,UC_USER,UC_ASSOCCODE) VALUES(?,?,?,?) ");
				pst.setString(1, names.get(i).toString());
				pst.setString(2, values.get(i).toString());
				pst.setString(3, userName);
				pst.setString(4, assoCode);
				cat.info(pst.toString());
				pst.executeUpdate();
			}
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateCookies-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateCookies-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static ArrayList<TagSystemBean> readCookiesForAll(String userName,String assoCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.readCookiesForAll");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<TagSystemBean> al_cookie = new ArrayList<TagSystemBean>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_USER_COOKIES WHERE UC_ASSOCCODE =? AND UC_USER =?");
			pst.setString(1, assoCode);
			pst.setString(2, userName);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				TagSystemBean tagBo = new TagSystemBean();
				tagBo.setName(rs.getString("UC_NAME"));
				tagBo.setValue(rs.getString("UC_VALUE"));
				al_cookie.add(tagBo);
			}
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.readCookiesForAll-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.readCookiesForAll-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_cookie;
	}
	public static OpenRequestFieldOrderBO getORFieldsOrder(String associationCode,String vendor)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In getORFieldsOrder method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		OpenRequestFieldOrderBO orFieldOrderBO= new OpenRequestFieldOrderBO();
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_POSITION WHERE ORP_ASSOCCODE= ? AND ORP_VENDOR= ?");	
			pst.setString(1,associationCode);
			pst.setString(2, vendor);
			rs=pst.executeQuery();
			while(rs.next()){
				orFieldOrderBO.setTripid(rs.getString("ORP_TRIP_ID"));
				orFieldOrderBO.setPhone(rs.getString("ORP_PHONE"));
				orFieldOrderBO.setName(rs.getString("ORP_NAME"));
				orFieldOrderBO.setSadd1(rs.getString("ORP_STADD1"));
				orFieldOrderBO.setSadd2(rs.getString("ORP_STADD2"));
				orFieldOrderBO.setScity(rs.getString("ORP_STCITY"));
				orFieldOrderBO.setEadd1(rs.getString("ORP_EDADD1"));
				orFieldOrderBO.setEadd2(rs.getString("ORP_EDADD2"));
				orFieldOrderBO.setEcity(rs.getString("ORP_EDCITY"));
				orFieldOrderBO.setShrs(rs.getString("ORP_SERVICETIME"));
				orFieldOrderBO.setSdate(rs.getString("ORP_SERVICEDATE"));
				orFieldOrderBO.setSlat(rs.getString("ORP_STLATITUDE"));
				orFieldOrderBO.setSlong(rs.getString("ORP_STLONGITUDE"));
				orFieldOrderBO.setEdlatitude(rs.getString("ORP_EDLATITUDE"));
				orFieldOrderBO.setEdlongitude(rs.getString("ORP_EDLONGITUDE"));
				orFieldOrderBO.setSpecialIns(rs.getString("ORP_SPLINS"));
				orFieldOrderBO.setPaytype(rs.getString("ORP_PAYTYPE"));
				orFieldOrderBO.setAcct(rs.getString("ORP_PAYACC"));
				orFieldOrderBO.setAmt(rs.getString("ORP_AMT"));
				orFieldOrderBO.setSharedRide(rs.getString("ORP_SHARED_RIDE"));
				orFieldOrderBO.setNumberOfPassengers(rs.getString("ORP_NUMBER_OF_PASSENGERS"));
				orFieldOrderBO.setOperatorComments(rs.getString("ORP_OPERATOR_COMMENTS"));
				orFieldOrderBO.setDispatchComments(rs.getString("ORP_DISPATCH_COMMENTS"));
				orFieldOrderBO.setSeparatedBy(rs.getString("ORP_SEPARATED_BY"));
				/*orFieldOrderBO.setDateFormat(rs.getString("ORP_DATE_FORMAT"));*/
			}

		} catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getORFieldsOrder--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getORFieldsOrder--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();	
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return orFieldOrderBO;
	}
	public static int routeNumberInsertion(AdminRegistrationBO adminBO, OpenRequestBO orBO,String routeId,int numberOfTrips) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.insertFleet");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(!routeId.equals("") && numberOfTrips>=1){
				pst = con.prepareStatement("UPDATE TDS_SHARED_RIDE SET SR_TOTAL_TRIPS='"+numberOfTrips+"' WHERE SR_ROUTE_ID='"+routeId+"' AND SR_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
				result=pst.executeUpdate();
			}else{
				pst = con.prepareStatement("INSERT INTO TDS_SHARED_RIDE (SR_CREATED_TIME,SR_CREATED_BY,SR_ASSOCCODE,SR_STATUS,SR_TOTAL_TRIPS,SR_ROUTE_DESC) VALUES(NOW(),?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
				pst.setString(1, adminBO.getUid());
				pst.setString(2,adminBO.getAssociateCode());
				pst.setString(3, TDSConstants.newRequestDispatchProcessesNotStarted+"");
				pst.setString(4, "1");
				pst.setString(5, orBO.getRouteNumber());
				result = pst.executeUpdate();
				rs = pst.getGeneratedKeys();
				if (rs.next()) {
					result = rs.getInt(1);
				} 
			}
			cat.info(pst.toString());



		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.insertFleet-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.insertFleet-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int updateOpenRequestOnDriverOnSite(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+adminBO.getAssociateCode()+"')";
			}
		} else {
			fleets = "('"+adminBO.getAssociateCode()+"')";
		}
		int result = updateOpenRequestOnDriverOnSite(p_OpenRequestBO,adminBO,fleets);
		return result;
	}

	
	public static int updateOpenRequestOnDriverOnSite(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,String fleets) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateOpenRequestOnDriverOnSite");
		int resultValue = 0;
		PreparedStatement pst = null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();

		try {
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS = ? WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+fleets+" and (OR_TRIP_STATUS = ? OR  OR_TRIP_STATUS = ?) and OR_DRIVERID = ?");
			pst.setString(1, TDSConstants.onSite + "");
			pst.setString(2, p_OpenRequestBO.getTripid());
//			pst.setString(3, adminBO.getAssociateCode());
			pst.setString(3, TDSConstants.onRotueToPickup + "");
			pst.setString(4, TDSConstants.jobAllocated + "");
			pst.setString(5, p_OpenRequestBO.getDriverid());
			cat.info(pst.toString());
			resultValue = pst.executeUpdate();
			pst.close();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateOpenRequestOnDriverOnSite-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateOpenRequestOnDriverOnSite-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}	



	public static int sharedRideDetailsInsertion(OpenRequestBO p_OpenRequestBO,AdminRegistrationBO adminBO,String pickUpOrder) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.sharedRideDetailsInsertion");
		int resultValue = 0;
		PreparedStatement pst = null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		int result=0;
		ResultSet rs= null;
		try {
			pst = con.prepareStatement("INSERT INTO TDS_SHARED_RIDE_DETAILS  (SRD_TRIP_ID,SRD_ASSOCCODE,SRD_ROUTE_NO,SRD_PICKUP_ORDER,SRD_ORDER_SWITCH) VALUES(?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, p_OpenRequestBO.getTripid());
			pst.setString(2,adminBO.getAssociateCode());
			pst.setString(3, p_OpenRequestBO.getRouteNumber());
			if(p_OpenRequestBO.getPickupOrder()!=null && !p_OpenRequestBO.getPickupOrder().equals("")){
				pst.setString(4, p_OpenRequestBO.getPickupOrder());
				pst.setString(5, "0");
			}else if(p_OpenRequestBO.getDropOffOrder()!=null && !p_OpenRequestBO.getDropOffOrder().equals("")){
				pst.setString(4, p_OpenRequestBO.getDropOffOrder());
				pst.setString(5, "1");
			}
			else {
				pst.setString(4, pickUpOrder);
				pst.setString(5, "99");
			}
			result = pst.executeUpdate();
			rs = pst.getGeneratedKeys();
			if (rs.next()) {
				result = rs.getInt(1);
			} 
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.sharedRideDetailsInsertion-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.sharedRideDetailsInsertion-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}	
	public static String getTripId(String assoccode,int Status,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getTripId");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		String result = "";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("INSERT INTO TDS_OPENREQUEST (OR_ASSOCCODE,OR_TRIP_STATUS,OR_SERVICEDATE,OR_SMS_SENT,OR_MASTER_ASSOCCODE) VALUES (?,?,'9999-12-12 00:00:00',NOW(),?)",Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, assoccode);
			pst.setInt(2, Status);
			pst.setString(3, masterAssoccode);
			pst.execute();
			rs = pst.getGeneratedKeys();
			if(rs.next()){
				result = rs.getString(1);
			}
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.insertFleet-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.insertFleet-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int getShiftKey(AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getShiftKey");
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		int shiftKey=0;
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement("SELECT * FROM TDS_SHIFT_REGISTER_MASTER WHERE SRM_ASSOCCODE=? AND SRM_OPERATORID=?"); 
			csp_pst.setString(1, adminBO.getMasterAssociateCode());
			csp_pst.setString(2, adminBO.getUid());
			rs = csp_pst.executeQuery();
			if(rs.next()){
				shiftKey=rs.getInt("SRM_KEY");
			}
		} catch (Exception sqex){
			cat.error("TDSException RequestDAO.getShiftKey-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException RequestDAO.getShiftKey-->" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return shiftKey;
	}
	public static ArrayList<FleetBO> readFleetsForCompany(String assoCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.readFleetsForCompany");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst,pst1 = null;
		ResultSet rs =null;
		ArrayList<FleetBO> al_fleet = new ArrayList<FleetBO>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ResultSet rs1= null;
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_FLEET_MASTER_ACCESS WHERE  FM_MASTER_ASSOCCODE='"+assoCode+"'");
			pst1 = con.prepareStatement("SELECT * FROM TDS_USER_COOKIES WHERE UC_ASSOCCODE='"+assoCode+"' AND UC_NAME LIKE 'GAC-D-C-FC%' ORDER BY UC_NAME");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			rs1=pst1.executeQuery();
			int i=0;
			while(rs.next()){
				FleetBO fleetBo = new FleetBO();
				fleetBo.setFleetNumber(rs.getString("FM_FLEET_NAME"));
				fleetBo.setFleetName(rs.getString("FM_COMPANYNAME"));				
				while(rs1.next()){
					fleetBo.setFcolor(rs1.getString("UC_VALUE"));
					break;
				}				
				al_fleet.add(fleetBo);
				i++;
			}
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.readFleetsForCompany-->"+sqex.getMessage());
			//System.out.println("TDSException RequestDAO.readFleetsForCompany-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_fleet;
	}



	
	public static int insertFleet(AdminRegistrationBO adminBO, ArrayList<FleetBO> operatorFleets,String userId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.insertFleet");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst=null,pst1 = null;
		ResultSet rs =null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			con.setAutoCommit(false);
			pst1=con.prepareStatement("DELETE FROM TDS_FLEET WHERE FL_USER_ID='"+userId+"' AND FL_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"'");
			result =pst1.executeUpdate();
			for(int i=0;i<operatorFleets.size();i++){
				if(operatorFleets.get(i).getFleetNumber()!=null){
					pst = con.prepareStatement("INSERT INTO TDS_FLEET (FL_USER_ID,FL_MASTER_ASSOCCODE,FL_ASSOCCODE,FL_DESCRIPTION) VALUES(?,?,?,?)");
					pst.setString(1, userId);
					pst.setString(2,adminBO.getMasterAssociateCode());
					pst.setString(3, operatorFleets.get(i).getFleetNumber());
					pst.setString(4, operatorFleets.get(i).getFleetName());
					cat.info(pst.toString());
					result = pst.executeUpdate();
				}
			}
			con.setAutoCommit(true);
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.insertFleet-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.insertFleet-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int importCharges(AdminRegistrationBO adminBO,String oldTripId,String newTripId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.importCharges");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			con.setAutoCommit(false);
			pst=con.prepareStatement("SELECT * FROM TDS_DRIVER_CHARGE_CALC  WHERE  DC_TRIP_ID='"+oldTripId+"' AND (DC_ASSOC_CODE='"+adminBO.getAssociateCode()+"' OR DC_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"')");
			rs=pst.executeQuery();
			if(rs.next()){
				pst=con.prepareStatement("INSERT INTO TDS_DRIVER_CHARGE_CALC (DC_TRIP_ID,DC_DRIVER_ID,DC_ASSOC_CODE,DC_TYPE_CODE,DC_AMOUNT,DC_PROCESSING_DATE_TIME,DC_DRIVER_PMT_AMT,DC_DRIVER_PMT_STATUS) SELECT ?,?,DC_ASSOC_CODE,DC_TYPE_CODE,DC_AMOUNT,NOW(),DC_DRIVER_PMT_AMT,? FROM TDS_DRIVER_CHARGE_CALC WHERE DC_TRIP_ID=? AND DC_ASSOC_CODE=?");
				pst.setString(1, newTripId);
				pst.setString(2, "");
				pst.setString(3, "0");
				pst.setString(4, oldTripId);
				pst.setString(5, adminBO.getAssociateCode());
				pst.executeUpdate();
			}

		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.importCharges-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.importCharges-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static void updateTripAmt(String tripId,String assocCode,BigDecimal amount,String tipCheck,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateTripAmt");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		int status=TDSConstants.paymentReceived;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(!tipCheck.equalsIgnoreCase("no")){
				pst = con.prepareStatement("SELECT ORH_AMT FROM TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_ID='"+tripId+"' AND (ORH_ASSOCCODE='"+assocCode+"' OR ORH_MASTER_ASSOCCODE='"+masterAssoccode+"')");
				rs=pst.executeQuery();
				if(rs.next()){
					amount = amount.add(new BigDecimal(rs.getString("ORH_AMT")));
				}
			} else {
				pst = con.prepareStatement("SELECT ORH_TRIP_STATUS FROM TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_ID='"+tripId+"' AND (ORH_ASSOCCODE='"+assocCode+"' OR ORH_MASTER_ASSOCCODE='"+masterAssoccode+"')");
				rs=pst.executeQuery();
				if(rs.next()){
					status = rs.getInt("ORH_TRIP_STATUS");
				}
			}
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST_HISTORY SET ORH_AMT='"+amount.toString()+"',ORH_TRIP_STATUS='"+status+"' WHERE ORH_TRIP_ID='"+tripId+"' AND (ORH_ASSOCCODE='"+assocCode+"' OR ORH_MASTER_ASSOCCODE='"+masterAssoccode+"')");
			pst.executeUpdate();
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateTripAmt-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateTripAmt-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	public static int updateDriver(AdminRegistrationBO adminBO,String driverId,String rating) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateDriver");
		TDSConnection dbcon =  new TDSConnection();
		Connection con =  dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs =null;
		int result=0;
		try {
			pst=con.prepareStatement("UPDATE TDS_DRIVER SET DR_RATING=? WHERE DR_ASSOCODE=? AND DR_SNO=?");
			pst.setString(1, rating);
			pst.setString(2, adminBO.getAssociateCode());
			pst.setString(3, driverId);
			result=pst.executeUpdate();
			if(result==1){
				result=0;
				pst=con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_DRIVER_RATING=? WHERE DL_ASSOCCODE=? AND DL_DRIVERID=?");
				pst.setString(1, rating);
				pst.setString(2, adminBO.getAssociateCode());
				pst.setString(3, driverId);
				result=pst.executeUpdate();
			}
			if(result==1){
				result=0;
				pst=con.prepareStatement("SELECT * FROM TDS_QUEUE WHERE QU_ASSOCCODE=? AND QU_DRIVERID=?");
				pst.setString(1, adminBO.getAssociateCode());
				pst.setString(2, driverId);
				rs=pst.executeQuery();
				if(rs.next()){
					pst=con.prepareStatement("UPDATE TDS_QUEUE SET QU_DRIVER_RATING=? WHERE QU_ASSOCCODE=? AND QU_DRIVERID=?");
					pst.setString(1, rating);
					pst.setString(2, adminBO.getAssociateCode());
					pst.setString(3, driverId);
					result=pst.executeUpdate();
				}else{
					result=1;
				}
			}
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateDriver-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int getDriverLoginStatus(String driverId,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result =0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst =null;
		ResultSet rs=null;

		try {
			pst = con.prepareStatement("SELECT COUNT(*) as count FROM TDS_DRIVERLOCATION WHERE DL_ASSOCCODE = '"+assoccode+"'  AND DL_SWITCH <> 'L' AND (DL_DRIVERID ='"+driverId+"' OR  DL_VEHICLE_NO ='"+driverId+"' )");
			//System.out.println("checking query:"+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				result=rs.getInt("count");
			}
		} catch (SQLException  sqex) {
			cat.error("TDSException RequestDAO.getDriverLoginStatus-->"+sqex.getMessage());
			//System.out.println("TDSException RequestDAO.getDriverLoginStatus" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;	
	}
	public static void updateMultiTag(String tripId,String assoccode,Long tagValue){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst =null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_REPEAT_GROUP='"+tagValue+"' WHERE OR_TRIP_ID='"+tripId+"' AND OR_MASTER_ASSOCCODE='"+assoccode+"'");
			pst.execute();
		} catch (SQLException  sqex) {
			cat.error("TDSException RequestDAO.updateMultiTag-->"+sqex.getMessage());
			//System.out.println("TDSException RequestDAO.updateMultiTag" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static int updateCabNumber(AdminRegistrationBO adminBO,DriverLocationBO driverLocBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateCabNumber");
		TDSConnection dbcon =  new TDSConnection();
		Connection con = dbcon.getConnection(); 
		PreparedStatement pst = null ;
		int result=0;
		String query="UPDATE TDS_OPENREQUEST SET OR_VEHICLE_NO='"+driverLocBO.getVehicleNo()+"' WHERE OR_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND OR_DRIVERID='"+adminBO.getUid()+"' AND OR_VEHICLE_NO=''";
		try {
			pst=con.prepareStatement(query);
			result=pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.updateCabNumber-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updateCabNumber-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}




	public static String getCCInfobyTripID(String tripId) {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getTripDetails");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		String createdBy="";
		String cCode="";
		String customerId="";

		try {
			pst = con.prepareStatement("SELECT ORH_TRIP_ID,ORH_CREATED_BY,ORH_AMT,ORH_ASSOCCODE FROM TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_ID=? ");
			pst.setString(1, tripId);
			rs = pst.executeQuery();
			if(rs.next()){
				createdBy = rs.getString("ORH_CREATED_BY");
				cCode  =rs.getString("ORH_ASSOCCODE");
			}else{
				return customerId;
			}

			pst  = con.prepareStatement("SELECT CCU_SNO,CCU_ASSOCIATIONCODE FROM CLNT_CLIENTUSER WHERE CCU_ASSOCIATIONCODE=? AND CCU_USERID=? ");
			pst.setString(1, cCode);
			pst.setString(2, createdBy);
			rs = pst.executeQuery();
			if(rs.next()){
				customerId = rs.getString("CCU_SNO");
			}

		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.getTripDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getTripDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return customerId;
	}
	public static  ArrayList<UserDetail> customerdetail(String associationcode,String name,String number){
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		UserDetail val=null;
		con = dbcon.getConnection();
		String customerid=" ";
		String sno;
		String id;
		String email;
		String phone;
		String status;
		//System.out.println("dao in name" +name +"number is" +number);
		ArrayList <UserDetail> val1= new ArrayList <UserDetail>();

		try{
			if(name =="" &&number =="")
			{
				pst =con.prepareStatement("SELECT CCU_SNO,CCU_USERNAME,CCU_EMAIL,CCU_PHONE,CCU_USERID,CCU_DEACTIVATE,CCU_FORGOT_KEY FROM CLNT_CLIENTUSER where CCU_ASSOCIATIONCODE =?");
				pst.setString(1,associationcode);
				rs=pst.executeQuery();
			}
			else if(name !="" && number == ""){
				pst =con.prepareStatement("SELECT CCU_SNO,CCU_USERNAME,CCU_EMAIL,CCU_PHONE,CCU_USERID,CCU_DEACTIVATE,CCU_FORGOT_KEY FROM CLNT_CLIENTUSER where CCU_ASSOCIATIONCODE =? and CCU_USERNAME=?");
				pst.setString(1,associationcode);
				pst.setString(2, name);
				rs=pst.executeQuery();
			}else if(name == "" && number != "")
			{
				pst =con.prepareStatement("SELECT CCU_SNO,CCU_USERNAME,CCU_EMAIL,CCU_PHONE,CCU_USERID,CCU_DEACTIVATE,CCU_FORGOT_KEY FROM CLNT_CLIENTUSER where CCU_ASSOCIATIONCODE =? and CCU_PHONE=? ");
				pst.setString(1,associationcode);
				pst.setString(2, number);
				rs=pst.executeQuery();
			}
			else if(name !="" && number !=""){
				pst =con.prepareStatement("SELECT CCU_SNO,CCU_USERNAME,CCU_EMAIL,CCU_PHONE,CCU_USERID,CCU_DEACTIVATE,CCU_FORGOT_KEY FROM CLNT_CLIENTUSER where CCU_ASSOCIATIONCODE =? and CCU_USERNAME=? and CCU_PHONE=?");
				pst.setString(1,associationcode);
				pst.setString(2, name);
				pst.setString(3, number);
				rs=pst.executeQuery();

			}
			while(rs.next()){

				val = new UserDetail();
				sno = rs.getString("CCU_SNO");
				val.setsno(sno);
				id=rs.getString("CCU_USERID");
				val.setuserid(id);
				customerid=rs.getString("CCU_USERNAME");
				val.setusername(customerid);
				email=rs.getString("CCU_EMAIL");
				val.setemail(email);
				phone =rs.getString("CCU_PHONE");

				if(phone == null && !phone.isEmpty()){
					//System.out.println("phone" +phone);
				}
				val.setphone(phone);
				status =rs.getString("CCU_DEACTIVATE");
				val.setstatus(status);
				
				val.setKey((rs.getString("CCU_FORGOT_KEY")!=null && !rs.getString("CCU_FORGOT_KEY").equals("")?rs.getString("CCU_FORGOT_KEY"):""));
				val1.add(val);
				
			}

		}catch(SQLException sqex){

			//System.out.println("error");

		}finally {
			dbcon.closeConnection();

		}
		return val1;
	}

	public static int updatedetail(String assc, String no, String uid, String uname, String uemail, String unumber) {
		// TODO Auto-generated method stub
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		int rs1=0;
		dbcon = new TDSConnection();
		UserDetail val=null;
		con = dbcon.getConnection();
		//System.out.println("welcome");
		try{
			pst =con.prepareStatement("UPDATE CLNT_CLIENTUSER set CCU_USERID=?,CCU_USERNAME=?,CCU_EMAIL=?,CCU_PHONE =? Where CCU_SNO =?");
			pst.setString(1,uid);
			pst.setString(2,uname);
			pst.setString(3,uemail);
			pst.setString(4,unumber);
			pst.setString(5,no);
			rs1=pst.executeUpdate();
			if(rs1 > 0){
				return 1;
			}
			else{
				return 0;
			}
		}catch(SQLException sqex){
			System.out.println("error");
		}finally{
			dbcon.closeConnection();
		}
		return rs1;
	}

	public static int DeActivate(String value, String assc, String no, String uid, String uname, String uemail, String unumber) {
		// TODO Auto-generated method stub
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		UserDetail val=null;
		con = dbcon.getConnection();
		int rs2=0;
		try{
			pst =con.prepareStatement("UPDATE CLNT_CLIENTUSER set CCU_DEACTIVATE=? Where CCU_SNO=?");
			pst.setString(1,value);
			pst.setString(2,no);
			//System.out.println("pst is" +pst);
			rs2=pst.executeUpdate();
			if(rs2 >0)
			{	
				return 1;
			}else{
				return 0;
			}
		}catch(SQLException sqex){
			//System.out.println("error");
		}finally{
			dbcon.closeConnection();
		}
		return rs2;
	}

	public static int updatedriverZone(AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updatedriverZone");
		int status = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			pst=con.prepareStatement("UPDATE TDS_QUEUE SET QU_UPDATETIME=NOW(),QU_STATUS=1 WHERE QU_ASSOCCODE=? AND QU_MASTER_ASSOCCODE=? AND QU_VEHICLE_NO=? AND QU_DRIVERID=? ");
			pst.setString(1,adminBO.getAssociateCode());
			pst.setString(2,adminBO.getMasterAssociateCode());
			pst.setString(3, adminBO.getVehicleNo());
			pst.setString(4,adminBO.getUid());
			status=pst.executeUpdate();
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updatedriverZone-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updatedriverZone-->"+sqex.getMessage());
			sqex.printStackTrace();		
		}

		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}

	public static String getCheckAvailabilty(AdminRegistrationBO adminBO,int stepAway){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String result ="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst =null;
		ResultSet rs=null;
		PreparedStatement pst1 =null;
		ResultSet rs1=null;
		try {
			pst = con.prepareStatement("SELECT TIMESTAMPDIFF(SECOND,QU_UPDATETIME,NOW())AS Minutes FROM TDS_QUEUE WHERE QU_ASSOCCODE=? AND QU_MASTER_ASSOCCODE=? AND QU_VEHICLE_NO=? AND QU_DRIVERID=?");
			pst.setString(1,adminBO.getAssociateCode());
			pst.setString(2,adminBO.getMasterAssociateCode());
			pst.setString(3, adminBO.getVehicleNo());
			pst.setString(4,adminBO.getUid());
			rs=pst.executeQuery();
			if(rs.next()){
				int updatetime=rs.getInt("Minutes");
				if(updatetime > (stepAway*60)){
					pst1 = con.prepareStatement("DELETE FROM TDS_QUEUE WHERE QU_ASSOCCODE=? AND QU_MASTER_ASSOCCODE=? AND QU_VEHICLE_NO=? AND QU_DRIVERID=?");
					pst1.setString(1,adminBO.getAssociateCode());
					pst1.setString(2,adminBO.getMasterAssociateCode());
					pst1.setString(3, adminBO.getVehicleNo());
					pst1.setString(4,adminBO.getUid());
					pst1.executeUpdate();
				}
				else{
					pst=con.prepareStatement("UPDATE TDS_QUEUE SET QU_UPDATETIME=NOW(),QU_STATUS=0 WHERE QU_ASSOCCODE=? AND QU_MASTER_ASSOCCODE=? AND QU_VEHICLE_NO=? AND QU_DRIVERID=? ");
					pst.setString(1,adminBO.getAssociateCode());
					pst.setString(2,adminBO.getMasterAssociateCode());
					pst.setString(3, adminBO.getVehicleNo());
					pst.setString(4,adminBO.getUid());
					pst.executeUpdate();
					
					pst=con.prepareStatement("SELECT QU_NAME FROM TDS_QUEUE WHERE QU_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND QU_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND QU_VEHICLE_NO='"+adminBO.getVehicleNo()+"' AND QU_DRIVERID='"+adminBO.getUid()+"'");
					rs=pst.executeQuery();
					if(rs.next()){
						result=rs.getString("QU_NAME");
					}
				}
			}
		} catch (SQLException  sqex) {
			cat.error("TDSException RequestDAO.getCheckAvailabilty-->"+sqex.getMessage());
			//System.out.println("TDSException RequestDAO.getCheckAvailabilty" + sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;	
	}

	
	public static int updateEmail(AdminRegistrationBO adminBO,String tripid,String email){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updatemailfortrip");
		int status = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			pst=con.prepareStatement("UPDATE TDS_OPENREQUEST_HISTORY SET ORH_EMAIL=? WHERE (ORH_ASSOCCODE=? OR ORH_MASTER_ASSOCCODE=?) AND ORH_TRIP_ID=? ");
			pst.setString(1,email);
			pst.setString(2,adminBO.getAssociateCode());
			pst.setString(3, adminBO.getMasterAssociateCode());
			pst.setString(4,tripid);
			status=pst.executeUpdate();
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updatemailfortrip-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.updatemailfortrip-->"+sqex.getMessage());
			sqex.printStackTrace();		
		}

		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	
}
