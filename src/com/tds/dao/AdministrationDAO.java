package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.CompanyFlagBean;
import com.tds.cmp.bean.DriverDeactivation;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

public class AdministrationDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(AdministrationDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+AdministrationDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}

	public static void deleteQueue(String assoccode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.deleteQueue Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("delete from TDS_QUEUE where QU_ASSOCCODE = ?");
			m_pst.setString(1, assoccode);
			m_pst.execute();			

		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.deleteQueue-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.deleteQueue-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.deleteQueue End--->"+(System.currentTimeMillis()-startTime));
	}
	public static void updateMerchantProfile(String mid,String seid,String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateMerchantProfile Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("update TDS_COMPANYDETAIL SET CD_MERCHANTPROFILE=?,CD_SERVICEID= ?  WHERE CD_ASSOCCODE = ?");
			m_pst.setString(1, mid);
			m_pst.setString(2, seid);
			m_pst.setString(3, assocode);			
			m_pst.executeUpdate();

		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateMerchantProfile-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateMerchantProfile-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.updateMerchantProfile End--->"+(System.currentTimeMillis()-startTime));

	}
	public static int updateOPenRequestByOpr(String queue_no,String pickup,String trip,String driver,AdminRegistrationBO adminBO)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateOPenRequestByOpr Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("update TDS_OPENREQUEST SET OR_DRIVERID=?,OR_TRIP_STATUS='', OR_QUEUENO = ? ,OR_SERVICETIME=?,OR_SERVICEDATE = cast(concat(date_format(OR_SERVICEDATE,'%Y-%m-%d '),SUBSTRING(?,1,2),':',SUBSTRING(?,3,2)) as DATETIME)  WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE=?");
			m_pst.setString(1, driver);
			m_pst.setString(2, queue_no);
			m_pst.setString(3, pickup);
			m_pst.setString(4, pickup);
			m_pst.setString(5, pickup);
			m_pst.setString(6, trip);
			m_pst.setString(7, adminBO.getAssociateCode());
			status = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateOPenRequestByOpr-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateOPenRequestByOpr-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.updateOPenRequestByOpr End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static int allocateOPenRequestByOpr(String driver_id,String queue_no,String trip, AdminRegistrationBO adminBO)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.allocateOPenRequestByOpr Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("update TDS_OPENREQUEST SET OR_TRIP_STATUS='"+TDSConstants.jobAllocated+"', OR_QUEUENO = ? ,OR_DRIVERID=?  WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE=?");
			m_pst.setString(1, queue_no);
			m_pst.setString(2, driver_id);
			m_pst.setString(3, trip);	
			m_pst.setString(4, adminBO.getAssociateCode());	
			status = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.allocateOPenRequestByOpr-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.allocateOPenRequestByOpr-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.allocateOPenRequestByOpr End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	public static int stopQueueProcessByOpr(String trip,String Qstatus, String associationCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.stopQueueProcessByOpr Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("update TDS_OPENREQUEST SET OR_TRIP_STATUS=?,OR_DRIVERID='' where OR_TRIP_ID=? AND OR_ASSOCCODE=?");
			m_pst.setString(1, Qstatus);
			m_pst.setString(2, trip);		
			m_pst.setString(3, associationCode);			

			status = m_pst.executeUpdate();

		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.stopQueueProcessByOpr-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.stopQueueProcessByOpr-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}

		cat.info("AdministrationDAO.stopQueueProcessByOpr End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static int updateOpenRequest(String trip,String p_assoccode,String old,String statusValue,String reason,String userId) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateOpenRequest Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String query="";
		String query2="";
		if(old.equals("2")){
			query = "Update TDS_OPENREQUEST SET OR_TRIP_STATUS='"+statusValue+"', OR_DISPATCH_START_TIME=NOW(), OR_VEHICLE_NO='', OR_DRIVERID='', OR_DISPATCH_HISTORY_DRIVERS ='', OR_HISTORY_OF_VEHICLES='' WHERE OR_TRIP_ID = ? and  OR_ASSOCCODE = '"+p_assoccode+"'";
		}else if(old.equals("1")){
			query = "Update TDS_OPENREQUEST SET OR_TRIP_STATUS='"+statusValue+"' WHERE OR_TRIP_ID = ? and  OR_ASSOCCODE = '"+p_assoccode+"'";
		}else if(old.equals("3")){
			query = "Update TDS_OPENREQUEST SET OR_TRIP_STATUS='"+statusValue+"', OR_VEHICLE_NO='', OR_DRIVERID='', OR_DISPATCH_HISTORY_DRIVERS ='', OR_HISTORY_OF_VEHICLES='' WHERE OR_TRIP_ID = ? and  OR_ASSOCCODE = '"+p_assoccode+"'";
		}else if(old.equals("4")){
			query = "Update TDS_OPENREQUEST SET OR_TRIP_STATUS='"+statusValue+"', OR_DISPATCH_START_TIME=NOW(), OR_VEHICLE_NO='', OR_DRIVERID='', OR_DISPATCH_HISTORY_DRIVERS ='', OR_HISTORY_OF_VEHICLES='' WHERE OR_ROUTE_NUMBER = ? and  OR_ASSOCCODE = '"+p_assoccode+"'";
			query2 = "UPDATE TDS_SHARED_RIDE SET SR_STATUS='"+statusValue+"', SR_VEHICLE_NO='', SR_DRIVERID='' WHERE SR_ROUTE_ID= ? AND  SR_ASSOCCODE = '"+p_assoccode+"'";
		}else if(old.equals("5")){
			query = "Update TDS_OPENREQUEST SET OR_TRIP_STATUS='"+statusValue+"',OR_SMS_SENT=NOW() WHERE OR_TRIP_ID = ? and  OR_ASSOCCODE = '"+p_assoccode+"'";
		}
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			String tripid[]=trip.split(";");

			for(int i=0;i<tripid.length;i++){
				String tripId=tripid[i];


				if(old.equals("4")){
					m_pst = m_conn.prepareStatement(query2);
					m_pst.setString(1, tripId);
					m_pst.executeUpdate();
				}
				m_pst = m_conn.prepareStatement(query);
				m_pst.setString(1, tripId);
				status = m_pst.executeUpdate();
			} }
		catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateOpenRequest-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.updateOpenRequest End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static void deleteAllOpenRequest(String p_assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.deleteAllOpenRequest Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			m_pst = m_conn.prepareStatement("Update TDS_OPENREQUEST SET OR_TRIP_STATUS='50' WHERE OR_SERVICEDATE!=date_format(now(),'%Y%m%d') and  OR_ASSOCCODE = '"+p_assoccode+"'");
			m_pst.execute();

		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.deleteAllOpenRequest-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.deleteAllOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.deleteAllOpenRequest End--->"+(System.currentTimeMillis()-startTime));
	}


	/*//	public static boolean getCurrentDriverLoginDetails(String asscode,String driver_id,String from_date,String to_date, String from_time,String to_time) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.deleteAllOpenRequest Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		boolean result = false;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("SELECT LO_DriverID,LO_LOGINTIME,LO_Logout FROM TDS_LOGINDETAILS   where LO_Logout='0000-00-00 00:00:00' and LO_DriverID=? and LO_ASSCODE=? and LO_LOGINTIME BETWEEN ? AND  ? ");
			m_pst.setString(1, driver_id);
			m_pst.setString(2, asscode); 
			m_pst.setString(3, TDSValidation.getTimsStampDate(from_date, from_time));
			m_pst.setString(4, TDSValidation.getTimsStampDate(to_date, to_time)); 
			rs = m_pst.executeQuery();
			if(rs.next()) {
				result = true; 
			}
		}catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.getCurrentDriverLoginDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException AdministrationDAO.getCurrentDriverLoginDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		}  finally {
			m_dbConn.closeConnection();
		}
		return result;

	}*/
	public static boolean insertDriverDeactivate(DriverDeactivation deactivation, String timeZoneHour) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.insertDriverDeactivate Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		boolean m_result = false;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_conn.setAutoCommit(false);
			if(deactivation.getHour()!=null&&!deactivation.getHour().equals("")){
				m_pst = m_conn.prepareStatement("insert into TDS_DRIVER_DEACTIVATION (DD_DRIVER_ID,DD_ASSOCCODE,DD_ENTERED_DATE,DD_ENTERED_BY,DD_DESCR,DD_FROM_DATE_TIME,DD_TO_DATE_TIME) values(?,?,now(),?,?,now(),DATE_SUB(now(),Interval -? HOUR))");
				m_pst.setString(1, deactivation.getDriver_id());
				m_pst.setString(2, deactivation.getAssoccode()); 
				m_pst.setString(3, deactivation.getUser());
				m_pst.setString(4, deactivation.getDesc()); 
				m_pst.setString(5, deactivation.getHour());
			}else{
				m_pst = m_conn.prepareStatement("insert into TDS_DRIVER_DEACTIVATION (DD_DRIVER_ID,DD_ASSOCCODE,DD_ENTERED_DATE,DD_ENTERED_BY,DD_DESCR,DD_FROM_DATE_TIME,DD_TO_DATE_TIME) values(?,?,now(),?,?,CONVERT_TZ(?,'UTC',?),CONVERT_TZ(?,'UTC',?))");
				m_pst.setString(1, deactivation.getDriver_id());
				m_pst.setString(2, deactivation.getAssoccode()); 
				m_pst.setString(3, deactivation.getUser());
				m_pst.setString(4, deactivation.getDesc()); 
				m_pst.setString(5, TDSValidation.getTimsStampDate(deactivation.getFrom_date(),deactivation.getFrom_time()));
				m_pst.setString(6,timeZoneHour);
				m_pst.setString(7, TDSValidation.getTimsStampDate(deactivation.getTo_date(),deactivation.getTo_time())); 
				m_pst.setString(8,timeZoneHour);
			}
			m_result = m_pst.execute();
			m_pst.close(); 
			//Following code commented to prevent deactivation in the master table
			//In the new way Driver_deactivation table holds the deactivation record when temporarily deactivating.
			m_conn.commit();
			m_conn.setAutoCommit(true);

		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.insertDriverDeactivate-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.insertDriverDeactivate-->"+sqex.getMessage());
			sqex.printStackTrace();
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.insertDriverDeactivate End--->"+(System.currentTimeMillis()-startTime));
		return m_result;
	}



	public static ArrayList<DriverDeactivation> getDriverDeactivation(AdminRegistrationBO adminBO,DriverDeactivation deactivation)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.getDriverDeactivation Start--->"+startTime);
		ArrayList<DriverDeactivation> al_list = new ArrayList<DriverDeactivation>();
		String searchCrietria = "";
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String query="";
		query = "select " +
				" DD_DRIVER_ID,DD_ASSOCCODE,date_format(CONVERT_TZ(DD_FROM_DATE_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as FROM_DATE," +
				" date_format(CONVERT_TZ(DD_TO_DATE_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as TO_DATE,date_format(DD_ENTERED_DATE,'%m/%d/%Y %H:%i') as entered_date," +
				" DD_DESCR as descr,(select AU_USERNAME from TDS_ADMINUSER where AU_SNO=DD_ENTERED_BY) as ENTERED_BY" +
				" from " +
				" TDS_DRIVER_DEACTIVATION  where DD_ASSOCCODE='"+ adminBO.getAssociateCode() + "'  AND DD_TO_DATE_TIME > NOW()";
		if (deactivation.getDriver_id()==null){
		}
		else{
			if (!(deactivation.getDriver_id().equals(null))&&!(deactivation.getDriver_id().equals(""))) {

				searchCrietria += "AND DD_DRIVER_ID  =  '" + deactivation.getDriver_id() + "'";
			} 
			if (!(deactivation.getFrom_date().equals(""))&&deactivation.getFrom_date()!=null) {
				searchCrietria = "AND date_format(DD_FROM_DATE_TIME,'%m/%d/%Y')>='"+deactivation.getFrom_date()+"'";
			} 
			if (!(deactivation.getTo_date().equals(""))&&deactivation.getTo_date()!=null) {
				searchCrietria = "AND date_format(DD_TO_DATE_TIME,'%m/%d/%Y')<='" + deactivation.getTo_date() + "'";
			}
		}
		query=query+searchCrietria;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				DriverDeactivation driverDeactivation= new DriverDeactivation();
				driverDeactivation.setAssoccode(m_rs.getString("DD_ASSOCCODE"));
				driverDeactivation.setDesc(m_rs.getString("descr"));
				driverDeactivation.setDriver_id(m_rs.getString("DD_DRIVER_ID"));
				driverDeactivation.setEntered_date(m_rs.getString("entered_date"));
				driverDeactivation.setFrom_date(m_rs.getString("FROM_DATE"));
				driverDeactivation.setTo_date(m_rs.getString("TO_DATE"));
				driverDeactivation.setUser(m_rs.getString("ENTERED_BY"));
				al_list.add(driverDeactivation);
			}
			m_pst.close();	 	
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.getDriverDeactivation-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.getDriverDeactivation-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.getDriverDeactivation End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static boolean deleteDriverDeactivation(String driver_id, AdminRegistrationBO adminBO)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.deleteDriverDeactivation Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		ResultSet rs=null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			m_conn.setAutoCommit(false); 
			m_pst = m_conn.prepareStatement("update TDS_DRIVER,TDS_ADMINUSER set  AU_ACTIVE = 1 ,DR_ACTIVESTATUS = 1 where AU_SNO = DR_SNO and DR_SNO = '"+driver_id+"' AND AU_ASSOCCODE = ?");
			m_pst.setString(1, adminBO.getAssociateCode());
			m_pst.execute();
			m_pst = m_conn.prepareStatement("select * from TDS_DRIVER_DEACTIVATION where DD_DRIVER_ID = '"+ driver_id+"' AND DD_ASSOCCODE = ?");
			m_pst.setString(1, adminBO.getAssociateCode());
			rs = m_pst.executeQuery();
			while(rs.next()) { 
				m_pst1 = m_conn.prepareStatement("INSERT INTO TDS_DRIVER_DEACTIVATION_HISTOR (DH_DRIVER_ID,DH_ASSOCCODE,DH_ENTERED_DATE,DH_ENTERED_BY,DH_DESCR,DH_FROM_DATE_TIME,DH_TO_DATE_TIME) VALUES(?,?,?,?,?,?,NOW())"); 
				m_pst1.setString(1, rs.getString("DD_DRIVER_ID"));
				m_pst1.setString(2, rs.getString("DD_ASSOCCODE"));
				m_pst1.setString(3, rs.getString("DD_ENTERED_DATE"));
				m_pst1.setString(4, rs.getString("DD_ENTERED_BY"));
				m_pst1.setString(5, rs.getString("DD_DESCR"));
				m_pst1.setString(6, rs.getString("DD_FROM_DATE_TIME"));
				m_pst1.executeUpdate(); 
			} 
			m_pst = m_conn.prepareStatement("delete from TDS_DRIVER_DEACTIVATION where DD_DRIVER_ID = '"+ driver_id+"'  AND DD_ASSOCCODE = ?");
			m_pst.setString(1, adminBO.getAssociateCode());
			m_pst.execute();
			AuditDAO.insertauditRequest(adminBO.getAssociateCode(), adminBO.getUid(), "Re Activate Driver", "De Activate", "Activate", "103","","");
			m_conn.commit();
			m_conn.setAutoCommit(true);
			return true;

		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.deleteDriverDeactivation-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.deleteDriverDeactivation-->"+sqex.getMessage());
			sqex.printStackTrace();

			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			cat.info("AdministrationDAO.deleteDriverDeactivation End--->"+(System.currentTimeMillis()-startTime));
			return false;
		} finally {
			m_dbConn.closeConnection();
		} 


	}
	public static ArrayList<QueueBean> getDriverinQDash(String p_assoccode,int flg,QueueBean queue,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.getDriverinQDash Start--->"+startTime);
		ArrayList<QueueBean> al_list = new ArrayList<QueueBean>();
		AdminRegistrationBO adminBo =new AdminRegistrationBO();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement q_pst= null;
		PreparedStatement m_pst= null;
		ResultSet r_Set=null;
		ResultSet rSet=null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="";
		String query1="";
		//String query2="";
		String searchCriteria="";
		query1 = "SELECT Q.*, QD.* FROM TDS_QUEUE Q, TDS_QUEUE_DETAILS QD WHERE QU_NAME = QD_QUEUENAME AND QU_MASTER_ASSOCCODE = QD_ASSOCCODE AND QU_MASTER_ASSOCCODE='" +masterAssoccode+"'";
		if(adminBo.getDispatchBasedOnVehicleOrDriver()==1){
			if(queue.getDriverId()!=null && !queue.getDriverId().equals("")){
				searchCriteria = " AND QU_DRIVERID  = '" + queue.getDriverId() + "'";
			}
		}else{
			if(queue.getVehicleNo()!=null && !queue.getVehicleNo().equals("")){
				searchCriteria = " AND QU_VEHICLE_NO  = '" + queue.getVehicleNo() + "'";
			}
		}
		if(!p_assoccode.equals(masterAssoccode)){
			searchCriteria = " AND QU_ASSOCCODE = '"+p_assoccode+"'";
		}
		query=query1+searchCriteria+" order by QU_NAME, QU_LOGINTIME";
		try {
			pst = con.prepareStatement(query);
			cat.info("pst"+pst.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				QueueBean queueBean = new QueueBean();
				//queueBean.setQU_DRIVERNAME(rs.getString("AU_USERNAME")==null?"":rs.getString("AU_USERNAME"));
				queueBean.setQU_DRIVERID(rs.getString("QU_DRIVERID")==null?"":rs.getString("QU_DRIVERID"));
				queueBean.setQU_DRIVERNAME("");
				queueBean.setQU_NAME(rs.getString("QU_NAME")==null?"":rs.getString("QU_NAME"));
				queueBean.setQU_LOGINTIME(rs.getString("QU_LOGINTIME")==null?"":rs.getString("QU_LOGINTIME"));
				queueBean.setDriverProfile(rs.getString("QU_DRPROFILE")==null?"":rs.getString("QU_DRPROFILE"));
				queueBean.setFlg(1);
				queueBean.setQueueDescription(rs.getString("QD_DESCRIPTION")==null?"":rs.getString("QD_DESCRIPTION"));
				queueBean.setVehicleNo(rs.getString("QU_VEHICLE_NO")==null?"":rs.getString("QU_VEHICLE_NO"));
				//q_pst=con.prepareStatement("SELECT COUNT(*) from TDS_QUEUE where QU_ASSOCCODE=?");
				//q_pst.setString(1, p_assoccode);
				//rSet =q_pst.executeQuery();
				//m_pst=con.prepareStatement("SELECT COUNT(*) from TDS_OPENREQUEST where OR_ASSOCCODE=?");
				//m_pst.setString(1, p_assoccode);
				//				r_Set =m_pst.executeQuery();
				//				if(rSet.next()){
				//					queueBean.setQU_WAITING(rSet.getInt(1));
				//				}
				//				if(r_Set.next()){
				//					queueBean.setQU_JOBWAITING(r_Set.getInt(1));
				//				}
				al_list.add(queueBean);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException AdministrationDAO.getDriverinQDash-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.getDriverinQDash-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("AdministrationDAO.getDriverinQDash End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList<OpenRequestBO> getOpnReqDash(String p_assoccode,int open, String timeOffset,int tripStatus,String zonesList,int rtSwitch,int routeSwitch,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+p_assoccode+"')";
			}
		} else {
			fleets = "('"+p_assoccode+"')";
		}
		ArrayList<OpenRequestBO> orBo = getOpnReqDash(fleets,open,timeOffset,tripStatus,zonesList,rtSwitch,routeSwitch);
		return orBo;
	}


	public static ArrayList<OpenRequestBO> getOpnReqDash(String p_assoccode,int open, String timeOffset,int tripStatus,String zonesList,int rtSwitch,int routeSwitch) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.getOpnReqDash Start--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String queryAppend="";
		String queryForFilter ="";

		if(rtSwitch==0){
			queryForFilter =" AND (OR_ROUTE_NUMBER ='' OR OR_ROUTE_NUMBER =NULL)";
		}
		String lZoneList ="";
		if(tripStatus==0){
			queryAppend = " AND OR_TRIP_STATUS != '"+TDSConstants.jobAllocated+ "'" +" AND OR_TRIP_STATUS != '"+TDSConstants.onRotueToPickup+ "'" +" AND OR_TRIP_STATUS != '"+TDSConstants.tripStarted+ "'"+" AND OR_TRIP_STATUS != '"+TDSConstants.onSite +"'"+" AND OR_TRIP_STATUS != '"+TDSConstants.jobSTC +"' AND OR_TRIP_STATUS !='"+TDSConstants.manualAllocation+"'";
		} if(routeSwitch==100){
			queryAppend = " AND OR_SHARED_RIDE=1 AND OR_ROUTE_NUMBER=0";
		}
		if(zonesList!=null&&zonesList!=""){
			String[] joblist = zonesList.split(";");
			lZoneList = " AND OR_QUEUENO IN(";
			for(int j=0;j<joblist.length;j++){
				lZoneList = lZoneList + "'" + joblist[j] + "',";
			}
			lZoneList = lZoneList + "'" + joblist[joblist.length-1]  + "')";
		}
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT " +
					"  OR_TRIP_ID,OR_MASTER_ASSOCCODE,OR_AMT,OR_ASSOCCODE,FM_COMPANYNAME,OR_PAYACC,OR_TRIP_SOURCE,SR_ROUTE_DESC,OR_AIRLINE_NO,OR_AIRLINE_NAME,OR_ROUTE_NUMBER,OR_EDADD1,OR_NAME,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y/%m/%d') as DATE, OR_STADD1,OR_EDADD1,OR_ELANDMARK,OR_END_QUEUENO,OR_STCITY,OR_EDCITY,OR_PHONE,OR_PREMIUM_CUSTOMER, OR_QUEUENO,OR_TRIP_STATUS,OR_DRCABFLAG, QD_DISPATCH_TYPE, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"') AS OR_SERVICEDATE,QD_DISPATCH_DISTANCE,"+
					" case OR_DISPATCH_START_TIME  < NOW() WHEN TRUE THEN TIMESTAMPDIFF(MINUTE,OR_DISPATCH_START_TIME,NOW()) END AS PENDING ,OR_STLATITUDE, OR_STLONGITUDE, OR_DISPATCH_HISTORY_DRIVERS," +
					"  case QD_DELAYTIME IS NULL WHEN true THEN  (OR_DISPATCH_LEAD_TIME * 60) ELSE CASE OR_DISPATCH_LEAD_TIME = -1 when true THEN QD_DELAYTIME*60 else (OR_DISPATCH_LEAD_TIME * 60) END END  AS QD_DELAYTIME," + 
					" OR_SMS_SENT,QD_SMS_RES_TIME, OR_DRIVERID,OR_VEHICLE_NO,OR_COMMENTS,OR_SPLINS,OR_DONT_DISPATCH,OR_SHARED_RIDE, OR_PAYTYPE, OR_ROUTE_NUMBER , " +
					" date_format(OR_SERVICEDATE,'%r') as st_time," +
					" time_to_sec(timediff(OR_SERVICEDATE,now())) as st_time_diff," +
					"time_to_sec(timediff(now(),OR_SMS_SENT)) as  SMS_TIME_DIFF,QD.QD_BROADCAST_DELAY,OR_LANDMARK,"+
					"date_format(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeOffset+"'),'%H%i') as SERVICE_TIME, "+
					"  OR_TRIP_STATUS" +
					" FROM" +
					" TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD " +
					" ON  OR_QUEUENO = QD_QUEUENAME AND OR_ASSOCCODE = QD_ASSOCCODE" +
					" LEFT JOIN TDS_SHARED_RIDE ON SR_ROUTE_ID = OR_ROUTE_NUMBER AND SR_ASSOCCODE = OR_ASSOCCODE LEFT JOIN TDS_FLEET_MASTER_ACCESS ON FM_FLEET_NAME = OR_ASSOCCODE AND FM_MASTER_ASSOCCODE = OR_MASTER_ASSOCCODE  WHERE " +
					" (OPR.OR_ASSOCCODE = ? OR OPR.OR_MASTER_ASSOCCODE=?)" +
					" AND OR_TRIP_STATUS != '"+TDSConstants.dummyTripId+ "'" +
					//		" AND OR_TRIP_STATUS != '"+TDSConstants.srAllocatedToCab+ "'" +
					" AND OR_TRIP_STATUS != '"+TDSConstants.tripCompleted+ "'" +
					" AND OR_DISPATCH_START_TIME < NOW()" +
					queryAppend +
					lZoneList +
					queryForFilter +
					" ORDER BY OR_ASSOCCODE,PENDING DESC");
			pst.setString(1, p_assoccode);
			pst.setString(2, p_assoccode);
			rs = pst.executeQuery();
			while(rs.next()) {
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setPendingTime(rs.getString("PENDING")==null?"":rs.getString("PENDING"));
				openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setRequestTime(rs.getString("SERVICE_TIME"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setSdate(rs.getString("DATE"));
				openRequestBO.setDontDispatch(rs.getInt("OR_DONT_DISPATCH"));
				openRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
				openRequestBO.setPaytype(rs.getString("OR_PAYTYPE"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				openRequestBO.setPremiumCustomer(rs.getInt("OR_PREMIUM_CUSTOMER"));
				openRequestBO.setEndQueueno(rs.getString("OR_END_QUEUENO"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				openRequestBO.setElandmark(rs.getString("OR_ELANDMARK"));
				openRequestBO.setTripSource(rs.getInt("OR_TRIP_SOURCE"));
				openRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				openRequestBO.setAirNo(rs.getString("OR_AIRLINE_NO"));
				openRequestBO.setAirName(rs.getString("OR_AIRLINE_NAME"));
				openRequestBO.setRouteDesc(rs.getString("SR_ROUTE_DESC"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setAcct(rs.getString("OR_PAYACC"));
				openRequestBO.setComments(rs.getString("OR_COMMENTS"));
				openRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				openRequestBO.setCustomField(rs.getString("FM_COMPANYNAME"));
				openRequestBO.setAssociateCode(rs.getString("OR_ASSOCCODE"));
				openRequestBO.setMasterAddressKey(rs.getString("OR_MASTER_ASSOCCODE"));
				openRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
				al_list.add(openRequestBO);
			}

		}catch(SQLException sqex) {
			cat.error("TDSException AdministrationDAO.getOpnReqDash-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AdministrationDAO.getOpnReqDash-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("AdministrationDAO.getOpnReqDash End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList<OpenRequestBO> getOpnReqDashForShared(String p_assoccode,int open, String timeOffset,int tripStatus,String date) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.getOpnReqDashForShared Start--->"+startTime);
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String queryAppend="";
		String queryForDate="";
		if(date!=null&&!date.equalsIgnoreCase("")){
			queryForDate = " AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE ,'UTC', '"+timeOffset+"'),'%m/%d/%Y') = '"+date+"'";
		}else{
			queryForDate = " AND DATE(OR_SERVICEDATE)=DATE(NOW())";
		}
		if(tripStatus==0){
			queryAppend = " AND OR_TRIP_STATUS != '"+TDSConstants.jobAllocated+ "'" +" AND OR_TRIP_STATUS != '"+TDSConstants.onRotueToPickup+ "'" +" AND OR_TRIP_STATUS != '"+TDSConstants.tripStarted+ "'" ;
		}
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT " +
					"  OR_TRIP_ID,OR_NAME, OR_STADD1,OR_EDADD1, OR_STCITY,OR_EDCITY,OR_PHONE, OR_QUEUENO,OR_TRIP_STATUS,OR_DRCABFLAG, QD_DISPATCH_TYPE, CONVERT_TZ(OR_SERVICEDATE ,'UTC', '"+timeOffset+"') AS OR_SERVICEDATE, DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE ,'UTC', '"+timeOffset+"'),'%m/%d/%Y') AS SERVICEDATE,QD_DISPATCH_DISTANCE,"+
					" case OR_DISPATCH_START_TIME  < NOW() WHEN TRUE THEN TIMESTAMPDIFF(MINUTE,OR_DISPATCH_START_TIME,NOW()) END AS PENDING ,OR_STLATITUDE, OR_STLONGITUDE, OR_DISPATCH_HISTORY_DRIVERS," +
					"  case QD_DELAYTIME IS NULL WHEN true THEN  (OR_DISPATCH_LEAD_TIME * 60) ELSE CASE OR_DISPATCH_LEAD_TIME = -1 when true THEN QD_DELAYTIME*60 else (OR_DISPATCH_LEAD_TIME * 60) END END  AS QD_DELAYTIME," + 
					" OR_SMS_SENT,QD_SMS_RES_TIME, OR_DRIVERID,OR_VEHICLE_NO,OR_DONT_DISPATCH,OR_SHARED_RIDE, OR_PAYTYPE, OR_ROUTE_NUMBER , " +
					" date_format(OR_SERVICEDATE,'%r') as st_time," +
					" time_to_sec(timediff(OR_SERVICEDATE,now())) as st_time_diff,OR_END_QUEUENO,OR_LANDMARK,OR_ELANDMARK,OR_COMMENTS,OR_SPLINS,OR_CUST_REF_NUM,OR_CUST_REF_NUM1,OR_CUST_REF_NUM2,OR_CUST_REF_NUM3,OR_CUSTOMFIELD, " +
					" time_to_sec(timediff(now(),OR_SMS_SENT)) as  SMS_TIME_DIFF,QD.QD_BROADCAST_DELAY,"+
					"date_format(CONVERT_TZ(OR_SERVICEDATE ,'UTC', '"+timeOffset+"'),'%H%i') as SERVICE_TIME,DL_LATITUDE ,DL_LONGITUDE, "+
					"  OR_TRIP_STATUS" +
					" FROM" +
					" TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD " +
					" ON  OR_QUEUENO = QD_QUEUENAME AND OR_ASSOCCODE = QD_ASSOCCODE" +
					" LEFT JOIN TDS_DRIVERLOCATION ON DL_DRIVERID = OPR.OR_DRIVERID AND DL_ASSOCCODE = OPR.OR_ASSOCCODE "+
					" WHERE " +
					" OPR.OR_ASSOCCODE = ? " +
					" AND OR_TRIP_STATUS != '"+TDSConstants.tripCompleted+ "'" +
					queryForDate+
					queryAppend +
					" ORDER BY OR_SERVICEDATE,OR_QUEUENO,OR_DRCABFLAG,OR_TRIP_STATUS,OR_TRIP_ID");
			pst.setString(1, p_assoccode);
			rs = pst.executeQuery();
			while(rs.next()) {
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1"));
				openRequestBO.setPendingTime(rs.getString("PENDING")==null?"":rs.getString("PENDING"));
				openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setRequestTime(rs.getString("SERVICE_TIME"));
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setSdate(rs.getString("SERVICEDATE"));
				openRequestBO.setDontDispatch(rs.getInt("OR_DONT_DISPATCH"));
				openRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
				openRequestBO.setPaytype(rs.getString("OR_PAYTYPE"));
				openRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				openRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				openRequestBO.setEndQueueno(rs.getString("OR_END_QUEUENO"));
				openRequestBO.setRefNumber(rs.getString("OR_CUST_REF_NUM"));
				openRequestBO.setRefNumber1(rs.getString("OR_CUST_REF_NUM1"));
				openRequestBO.setRefNumber2(rs.getString("OR_CUST_REF_NUM2"));
				openRequestBO.setRefNumber3(rs.getString("OR_CUST_REF_NUM3"));
				openRequestBO.setCustomField(rs.getString("OR_CUSTOMFIELD"));
				openRequestBO.setEdlatitude(rs.getString("DL_LATITUDE"));
				openRequestBO.setEdlongitude(rs.getString("DL_LONGITUDE"));
				openRequestBO.setScity(rs.getString("OR_STCITY"));
				openRequestBO.setEcity(rs.getString("OR_EDCITY"));
				openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				openRequestBO.setElandmark(rs.getString("OR_ELANDMARK"));
				openRequestBO.setComments(rs.getString("OR_COMMENTS"));
				openRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				al_list.add(openRequestBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException AdministrationDAO.getOpnReqDashForShared-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AdministrationDAO.getOpnReqDashForShared-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("AdministrationDAO.getOpnReqDashForShared End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}


	public static void deleteDriverinQ(String driver_id, AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.deleteDriverinQ Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		String driverOrCab="";
		if(adminBO.getDispatchBasedOnVehicleOrDriver()==1){
			driverOrCab="QU_DRIVERID";
		}else{
			driverOrCab="QU_VEHICLE_NO";
		}
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("delete from TDS_QUEUE where "+ driverOrCab+"='"+ driver_id+"' AND QU_ASSOCCODE=?");
			pst.setString(1, adminBO.getAssociateCode());
			pst.execute();

		}catch(SQLException sqex) {
			cat.error("TDSException AdministrationDAO.deleteDriverinQ-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AdministrationDAO.deleteDriverinQ-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("AdministrationDAO.deleteDriverinQ End--->"+(System.currentTimeMillis()-startTime));
	}

	public static void updateDriverinQ(String driver_id, AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateDriverinQ Start--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String driverOrCab="";
		if(adminBO.getDispatchBasedOnVehicleOrDriver()==1){
			driverOrCab="QU_DRIVERID";
		}else{
			driverOrCab="QU_VEHICLE_NO";
		}
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement("select min(QU_LOGINTIME) as timeq from TDS_QUEUE where concat(QU_ASSOCCODE,QU_NAME) in (select concat(QU_ASSOCCODE,QU_NAME) from TDS_QUEUE where "+driverOrCab+" = '"+driver_id+"')");
			rs = pst.executeQuery();

			if(rs.first())
			{
				pst = con.prepareStatement("update TDS_QUEUE set QU_LOGINTIME = date_sub('"+rs.getString("timeq")+"', INTERVAL 1 HOUR) where "+driverOrCab+" = '"+ driver_id+"'");
				pst.execute();
			} else {
				pst = con.prepareStatement("update TDS_QUEUE set QU_LOGINTIME = date_sub(QU_LOGINTIME, INTERVAL 1 HOUR) where "+driverOrCab+" = '"+ driver_id+"'");
				pst.execute();
			}

		}catch(SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateDriverinQ-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateDriverinQ-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("AdministrationDAO.updateDriverinQ End--->"+(System.currentTimeMillis()-startTime));
	}	
	public static int updateDriverInOpenRequestByOpr(String trip,String driver,AdminRegistrationBO adminBO, int jobStatus)
	{	
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateDriverInOpenRequestByOpr Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String sql = "";
		if(jobStatus >0){
			sql = "update TDS_OPENREQUEST SET OR_DRIVERID='"+driver+"', OR_TRIP_STATUS ='"+jobStatus+"' WHERE OR_TRIP_ID = '"+trip+"' AND OR_ASSOCCODE='"+adminBO.getAssociateCode()+"'";
		} else {
			sql = "update TDS_OPENREQUEST SET OR_DRIVERID='"+driver+"' WHERE OR_TRIP_ID = '"+trip+"' AND OR_ASSOCCODE='"+adminBO.getAssociateCode()+"'";
		}

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			//m_pst = m_conn.prepareStatement("update TDS_OPENREQUEST SET OR_DRIVERID='',OR_TRIP_STATUS='', OR_QUEUENO = ? ,OR_SERVICEDATE = concat(date_format(OR_SERVICEDATE,'%Y-%m-%d '),?)  WHERE OR_TRIP_ID = ?");
			m_pst = m_conn.prepareStatement(sql);
			status = m_pst.executeUpdate();
			System.out.println("status"+status+"query"+m_pst.toString());
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateOPenRequestByOpr-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateOPenRequestByOpr-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}

		cat.info("AdministrationDAO.updateDriverInOpenRequestByOpr End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static int updateOpenRequestTime(String pickup,String trip,String assocode,int value){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateOpenRequestTime Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String sql="";
		if(value==0){
			sql = "UPDATE TDS_OPENREQUEST SET OR_SERVICEDATE='"+pickup+"' WHERE OR_TRIP_ID="+trip+" AND OR_ASSOCCODE="+assocode;
		}else if(value==1){
			sql = "UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS='8', OR_SMS_SENT=NOW() WHERE OR_TRIP_ID="+trip+" AND OR_ASSOCCODE="+assocode;
		}
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(sql);
			status = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateOpenRequestTime-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateOpenRequestTime-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.updateOpenRequestTime End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	public static int updateOpenRequestzone(String zone,String tripId,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateOpenRequestzone Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String sql="";
		String tripid[]=tripId.split(";");
		try {

			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];
				sql = "UPDATE TDS_OPENREQUEST SET OR_QUEUENO='"+zone+"' WHERE OR_TRIP_ID='"+trip+"' AND OR_ASSOCCODE='"+assocode+"'";
				m_dbConn  = new TDSConnection();
				m_conn = m_dbConn.getConnection();
				m_pst = m_conn.prepareStatement(sql);
				//System.out.println("Query"+sql);
				status = m_pst.executeUpdate();
			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateOpenRequestzone-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateOpenRequestzone-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.updateOpenRequestzone End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	public static int restartDispatch(String pickup,String trip,String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.restartDispatch Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null, pst1= null;
		String sql="";

		sql = "UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS='"+TDSConstants.newRequestDispatchProcessesNotStarted+"',OR_SMS_SENT=NOW() WHERE OR_TRIP_ID='"+trip+"' AND OR_ASSOCCODE='"+associationCode+"'";
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(sql);
			status = m_pst.executeUpdate();
			if (status==1){
				m_pst1 = m_conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME) VALUES(?,NOW())");
				m_pst1.setString(1, trip);
				m_pst1.execute();
				pst1 = m_conn.prepareStatement("INSERT INTO TDS_CONSOLE (C_TRIPID,(C_TIME) VALUES(?,NOW())");
				pst1.setString(1, trip);
				pst1.execute();
			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.restartDispatch-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.restartDispatch-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.restartDispatch End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	public static int restartDispatchjob(String tripId,String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.restartDispatchjob Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null, pst1= null;
		String sql="";
		String tripid[]=tripId.split(";");


		try {
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];
				sql = "UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS='"+TDSConstants.newRequestDispatchProcessesNotStarted+"' WHERE OR_TRIP_ID='"+trip+"' AND OR_ASSOCCODE='"+associationCode+"'";
				//System.out.println("SQL---->"+sql);		
				m_dbConn  = new TDSConnection();
				m_conn = m_dbConn.getConnection();
				m_pst = m_conn.prepareStatement(sql);
				status = m_pst.executeUpdate();

			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.restartDispatchjob-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.restartDispatchjob-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.restartDispatchjob End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	public static int deleteOpenRequestSummary(String trip,String p_assoccode,String old,String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.deleteOpenRequestSummary Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("Update TDS_OPENREQUEST SET OR_TRIP_STATUS="+TDSConstants.customerCancelledRequest+",OR_SERVICEDATE=NOW() WHERE OR_TRIP_ID = ? and  (OR_ASSOCCODE = '"+p_assoccode+"' OR OR_MASTER_ASSOCCODE='"+masterAssoccode+"')");
			m_pst.setString(1, trip);
			cat.info("query--->"+m_pst.toString());
			status = m_pst.executeUpdate();
			if(status==1){
				m_pst=m_conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_HISTORY  ( SELECT * FROM TDS_OPENREQUEST WHERE (OR_ASSOCCODE = '"+p_assoccode+"' OR OR_MASTER_ASSOCCODE='"+masterAssoccode+"') AND OR_TRIP_ID = '"+trip+"')");
				cat.info("query--->"+m_pst.toString());
				status=m_pst.executeUpdate();
				if(status==1){
					m_pst=m_conn.prepareStatement("DELETE FROM TDS_OPENREQUEST WHERE (OR_ASSOCCODE = '"+p_assoccode+"' OR OR_MASTER_ASSOCCODE='"+masterAssoccode+"') AND OR_TRIP_ID= '"+trip+"'");
					status=m_pst.executeUpdate();
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.deleteOpenRequest-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.deleteOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.deleteOpenRequestSummary End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static byte[] getCompanyLogo(String assocode){	
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.getCompanyLogo Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs=null;
		String sql="";
		java.sql.Blob image = null; 
		byte[ ] imgData = null ; 
		sql="SELECT BINARYFILE FROM UPLOADS WHERE ASSOCCODE='"+assocode+"'";
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(sql);
			rs = m_pst.executeQuery();
			if(rs.first()){
				image = rs.getBlob(1);
				imgData = image.getBytes(1,(int)image.length());
			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.getCompanyLogo-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.getCompanyLogo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.getCompanyLogo End--->"+(System.currentTimeMillis()-startTime));
		return imgData;
	}
	public static ArrayList<OpenRequestBO> getJobsForToday(String assocode,String driver,String timeOffset){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.getJobsForToday Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs=null;
		ArrayList<OpenRequestBO> jobs = new ArrayList<OpenRequestBO>();
		String sql="";
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		sql="SELECT  OR_TRIP_ID,OR_NAME,OR_VEHICLE_NO,OR_DRIVERID, OR_STADD1, OR_STCITY,OR_PHONE, OR_QUEUENO,OR_TRIP_STATUS,OR_DRCABFLAG, CONVERT_TZ(OR_SERVICEDATE ,'UTC','"+timeOffset+"') AS OR_SERVICEDATE,date_format(CONVERT_TZ(OR_SERVICEDATE ,'UTC','"+timeOffset+"'),'%H%i') as SERVICE_TIME FROM TDS_OPENREQUEST WHERE DATE(OR_SERVICEDATE)=DATE(NOW()) AND OR_ASSOCCODE ='"+assocode+"' AND OR_DRIVERID='"+driver+"' UNION SELECT ORH_TRIP_ID,ORH_NAME,ORH_VEHICLE_NO,ORH_DRIVERID,ORH_STADD1, ORH_STCITY,ORH_PHONE, ORH_QUEUENO,ORH_TRIP_STATUS,ORH_DRCABFLAG, CONVERT_TZ(ORH_SERVICEDATE ,'UTC','"+timeOffset+"') AS ORH_SERVICEDATE,date_format(CONVERT_TZ(ORH_SERVICEDATE ,'UTC','"+timeOffset+"'),'%H%i') as SERVICE_TIME FROM TDS_OPENREQUEST_HISTORY WHERE DATE(ORH_SERVICEDATE)=DATE(NOW()) AND ORH_ASSOCCODE ='"+assocode+"' AND ORH_DRIVERID="+driver;
		try {
			m_pst = m_conn.prepareStatement(sql);
			rs = m_pst.executeQuery();
			while(rs.next()){
				OpenRequestBO openRequestBO = new OpenRequestBO();
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				openRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				openRequestBO.setScity(rs.getString("OR_STADD1"));
				openRequestBO.setPhone(TDSValidation.getViewPhoneFormat(rs.getString("OR_PHONE")));
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setRequestTime(rs.getString("SERVICE_TIME"));
				//openRequestBO.setSlat(rs.getString("ORH_STLATITUDE"));
				//openRequestBO.setSlong(rs.getString("ORH_STLONGITUDE"));
				openRequestBO.setSdate(rs.getString("OR_SERVICEDATE"));
				jobs.add(openRequestBO);
			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.getJobsForToday-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.getJobsForToday-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.getJobsForToday End--->"+(System.currentTimeMillis()-startTime));
		return jobs;
	}

	public static ArrayList<OpenRequestBO> getCountJobsForDriver(String assocode,String driver,String timeOffset){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.getCountJobsForDriver Start--->"+startTime);
		TDSConnection db =null;
		Connection con =null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<OpenRequestBO> arlist = new ArrayList<OpenRequestBO>();
		String query="SELECT ORH_DRIVERID,count(*) as total FROM TDS_OPENREQUEST_HISTORY WHERE DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE ,'UTC','"+timeOffset+"'),'%Y-%m-%d')=DATE(NOW()) AND ORH_ASSOCCODE ='"+assocode+"' GROUP BY ORH_DRIVERID UNION SELECT OR_DRIVERID,count(*) as total FROM TDS_OPENREQUEST WHERE DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE ,'UTC','"+timeOffset+"'),'%Y-%m-%d')=DATE(NOW()) AND OR_ASSOCCODE ='"+assocode+"' GROUP BY OR_DRIVERID";
		db =new TDSConnection();
		con =db.getConnection();
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestBO orBo = new OpenRequestBO();
				orBo.setDriverid(rs.getString("ORH_DRIVERID"));
				orBo.setSetTotalJobsPerDriver(rs.getString("total"));
				arlist.add(orBo);
			}
		} catch (Exception sqex) {
			cat.error("TDSException AdministrationDAO.getCountJobsForDriver-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AdministrationDAO.getCountJobsForDriver-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			db.closeConnection();
		}
		cat.info("AdministrationDAO.getCountJobsForDriver End--->"+(System.currentTimeMillis()-startTime));
		return arlist;

	}
	public static int loginDriverDashboardForLD(String driverId,String associationCode,String cabNo,String masterAssoccode){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.loginDriverDashboardForLD Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs =null;
		String sql1 ="";
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		sql1 ="insert into TDS_LOGINDETAILS(LO_DriverID,LO_SESSIONID,LO_LoginTime,LO_ASSCODE,LO_Logout,LO_DPHONENO,LO_PROVIDER,LO_VERSION,LO_DRIVER_PROFILE,LO_CABNO, LO_GOOG_REG_KEY,LO_IPADDRESS,LO_MASTER_ASSOCCODE) values(?,?,NOW(),?,'2020-01-19 03:14:07',?,?,?,?,?,?,?,?)"; 
		try {
			m_pst = m_conn.prepareStatement(sql1);
			m_pst.setString(1, driverId);
			m_pst.setString(2, driverId);
			m_pst.setString(3, associationCode);
			m_pst.setString(4, "0000000000");
			m_pst.setString(5, "");
			m_pst.setString(6, "");
			m_pst.setString(7, "");
			m_pst.setString(8, cabNo);
			m_pst.setString(9, "");
			m_pst.setString(10, "");
			m_pst.setString(11, masterAssoccode);
			status = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.loginDriverDashboardLD-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.loginDriverDashboardLD-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.loginDriverDashboardForLD End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static int updateDriverLocation(String driverId,String associationCode,String lat,String longi){	
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateDriverLocation Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String sql="";
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		sql = "UPDATE TDS_DRIVERLOCATION SET DL_LATITUDE ='"+lat+"',DL_LONGITUDE ='"+longi+"',DL_LASTUPDATE='2020-01-19 03:14:07'  WHERE DL_ASSOCCODE = ? AND DL_DRIVERID=? OR DL_VEHICLE_NO=?";
		try {
			m_pst = m_conn.prepareStatement(sql);
			m_pst.setString(1, associationCode);
			m_pst.setString(2, driverId);
			m_pst.setString(3, driverId);
			status = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateDriverLocation-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.updateDriverLocation End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static int loginDriverDashboard(String driverId,String associationCode,String vehicleNo,String masterAssoccode){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.loginDriverDashboard Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String sql="";
		String sql1 = "";
		String sql2 ="";
		ResultSet rs = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		sql = "UPDATE TDS_DRIVERLOCATION SET DL_SWITCH='Y',DL_VEHICLE_NO ='"+vehicleNo+"',DL_LASTUPDATE='2020-01-19 03:14:07' WHERE DL_ASSOCCODE = ? AND DL_DRIVERID=?";
		sql1 = "SELECT DR_PHONE,DR_SMSID,DR_FLAG FROM TDS_DRIVER  WHERE DR_ASSOCODE = '"+associationCode+"' AND DR_USERID ='"+driverId+"'";
		sql2 = "INSERT INTO TDS_DRIVERLOCATION(DL_PHONE,DL_SMSID,DL_PROFILE,DL_DRIVERID,DL_LATITUDE,DL_LONGITUDE,DL_SWITCH,DL_ASSOCCODE,DL_PROVIDER,DL_GOOG_REG_KEY,DL_VEHICLE_NO,DL_LASTUPDATE,DL_LOGIN_TIMEDL_MASTER_ASSOCCODE) VALUES(?,?,?,?,?,?,?,?,?,?,?,'2020-01-19 03:14:07',NOW(),?)";
		try {
			m_pst = m_conn.prepareStatement(sql);
			m_pst.setString(1, associationCode);
			m_pst.setString(2, driverId);
			status = m_pst.executeUpdate();
			if(status==0){
				m_pst = m_conn.prepareStatement(sql1);
				rs = m_pst.executeQuery();
				if(rs.next()){
					m_pst = m_conn.prepareStatement(sql2);
					m_pst.setString(1, rs.getString("DR_PHONE"));
					m_pst.setString(2, rs.getString("DR_SMSID"));
					m_pst.setString(3, rs.getString("DR_FLAG"));

					m_pst.setString(4, driverId);
					m_pst.setString(5, "0.00");
					m_pst.setString(6, "0.00");
					m_pst.setString(7, "Y");
					m_pst.setString(8, associationCode);
					m_pst.setString(9, "");
					m_pst.setString(10, "");
					m_pst.setString(11, vehicleNo);
					m_pst.setString(12, masterAssoccode);
					status = m_pst.executeUpdate();
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.loginDriverDashboard-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.loginDriverDashboard-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.loginDriverDashboard End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static String loadDefaultZone(String assocode){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.loadDefaultZone Start--->"+startTime);
		TDSConnection db =null;
		Connection con =null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		String defZone ="";
		String query="SELECT QD_QUEUENAME FROM TDS_QUEUE_DETAILS WHERE QD_ASSOCCODE ='"+assocode+"' AND QD_VIRTUAL_ACTUAL ='D'";
		db =new TDSConnection();
		con =db.getConnection();
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			if(rs.next()){
				defZone =rs.getString("QD_QUEUENAME");
			}
		} catch (Exception sqex) {
			cat.error("TDSException AdministrationDAO.loadDefaultZone-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AdministrationDAO.loadDefaultZone-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			db.closeConnection();
		}
		cat.info("AdministrationDAO.loadDefaultZone End--->"+(System.currentTimeMillis()-startTime));
		return defZone;

	}
	public static int changeJobToAnotherFleet(String tripId,String fleetNo,String assoCode,String userId,String masAssoCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.changeJobToAnotherFleet Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs =null;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst =m_conn.prepareStatement("SELECT * FROM TDS_FLEET WHERE FL_ASSOCCODE ='"+fleetNo+"' AND FL_USER_ID = '"+userId+"' AND FL_MASTER_ASSOCCODE ='"+masAssoCode+"'");
			rs = m_pst.executeQuery();
			System.out.println("result"+rs);

			if(rs.next()){
				m_pst = m_conn.prepareStatement("update TDS_OPENREQUEST SET OR_ASSOCCODE=?  WHERE OR_TRIP_ID = ?");
				m_pst.setString(1, fleetNo);
				m_pst.setString(2, tripId);
				status = m_pst.executeUpdate();
				System.out.println("status"+status);

			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.changeJobToAnotherFleet-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.changeJobToAnotherFleet-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.changeJobToAnotherFleet End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	public static int changeMultipleJobToAnotherFleet(String tripId,String fleetNo,String assoCode,String userId,String masAssoCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.changeJobToAnotherFleet Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs =null;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst =m_conn.prepareStatement("SELECT * FROM TDS_FLEET WHERE FL_ASSOCCODE ='"+fleetNo+"' AND FL_USER_ID = '"+userId+"' AND FL_MASTER_ASSOCCODE ='"+masAssoCode+"'");
			rs = m_pst.executeQuery();
			if(rs.next()){
				String trip[]=tripId.split(";");

				for(int i=0;i<trip.length;i++){
					String tripID1=trip[i];
					m_pst = m_conn.prepareStatement("update TDS_OPENREQUEST SET OR_ASSOCCODE=?  WHERE OR_TRIP_ID = ?");
					m_pst.setString(1, fleetNo);
					m_pst.setString(2, tripID1);
					status = m_pst.executeUpdate();
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.changeJobToAnotherFleet-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.changeJobToAnotherFleet-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.changeJobToAnotherFleet End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static int updateOPenRequestForEndZone(String endZone,String assoCode,String tripId)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateOPenRequestForEndZone Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("update TDS_OPENREQUEST SET  OR_END_QUEUENO = ?  WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE=?");
			m_pst.setString(1, endZone);
			m_pst.setString(2, tripId);
			m_pst.setString(3, assoCode);
			status = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateOPenRequestForEndZone-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateOPenRequestForEndZone-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}

		cat.info("AdministrationDAO.updateOPenRequestForEndZone End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static int setDriverAvailability(String vehicleNo,String driverID,String assoCode,String statusValue)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.setDriverAvailability Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String sql = "UPDATE TDS_DRIVERLOCATION SET DL_SWITCH='"+statusValue+"',DL_LASTUPDATE='2020-01-19 03:14:07' WHERE DL_ASSOCCODE ='"+assoCode+"' AND DL_DRIVERID='"+driverID+"'";
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst =m_conn.prepareStatement(sql);
			status = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.setDriverAvailability-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.setDriverAvailability-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.setDriverAvailability End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static ArrayList<CompanyFlagBean> getSplFlags(String assocode){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.getSplFlags Start--->"+startTime);
		ArrayList<CompanyFlagBean> flagList =new ArrayList<CompanyFlagBean>();
		TDSConnection db =null;
		Connection con =null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		String query="SELECT * FROM TDS_CMPY_FLAG_SETUP WHERE CS_ASSOCODE ='"+assocode+"'";
		db =new TDSConnection();
		con =db.getConnection();
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				CompanyFlagBean sysB = new CompanyFlagBean();
				sysB.setFlag1(rs.getString("CS_FLAG_VALUE"));
				sysB.setFlag1_lng_desc(rs.getString("CS_FLAG_LNG_DESC"));
				flagList.add(sysB);
			}
		} catch (Exception sqex) {
			cat.error("TDSException AdministrationDAO.getSplFlags-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AdministrationDAO.getSplFlags-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			db.closeConnection();
		}
		cat.info("AdministrationDAO.getSplFlags End--->"+(System.currentTimeMillis()-startTime));
		return flagList;
	}
	public static ArrayList<String> loadErrors(){
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.loadErrors Start--->"+startTime);
		TDSConnection db =null;
		Connection con =null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<String> al = new ArrayList(); 
		String query="SELECT ER_ANDROID_VERSION,ER_STACK_TRACE,ER_LOGCAT,ER_REPORT_TIME,ER_CUSTOM_FIELDS FROM TDS_ERROR_REPORTS ORDER BY ER_REPORT_TIME";
		db =new TDSConnection();
		con =db.getConnection();
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				al.add(rs.getString("ER_ANDROID_VERSION"));
				Blob blob = rs.getBlob("ER_STACK_TRACE");
				byte[] bdata = blob.getBytes(1, (int) blob.length());
				String text = new String(bdata);
				al.add(text);
				Blob blob1 = rs.getBlob("ER_STACK_TRACE");
				byte[] bdata1 = blob1.getBytes(1, (int) blob1.length());
				String text1 = new String(bdata1);
				al.add(text1);
				al.add(rs.getString("ER_CUSTOM_FIELDS"));
			}
		} catch (Exception sqex) {
			cat.error("TDSException AdministrationDAO.loadErrors-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AdministrationDAO.loadErrors-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			db.closeConnection();
		}
		cat.info("AdministrationDAO.loadErrors End--->"+(System.currentTimeMillis()-startTime));
		return al;

	}
	public static int updateOpenRequestforMjobs(String trip,String p_assoccode,String old,String statusValue,String reason,String userId,String rate) {
		long startTime = System.currentTimeMillis();
		cat.info("AdministrationDAO.updateOpenRequestforMjobs Start--->"+startTime);
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String query="";
		if(old.equals("1")){
			query = "Update TDS_OPENREQUEST SET OR_TRIP_STATUS='"+statusValue+"',OR_SMS_SENT=NOW(),OR_DONT_DISPATCH='1' WHERE OR_TRIP_ID = ? and  OR_ASSOCCODE = '"+p_assoccode+"'";
		}else if(old.equals("2")){
			query = "Update TDS_OPENREQUEST SET OR_TRIP_STATUS='"+statusValue+"',OR_SMS_SENT=NOW(),OR_RATINGS='"+rate+"' WHERE OR_TRIP_ID = ? and  OR_ASSOCCODE = '"+p_assoccode+"'";
		}
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			String tripid[]=trip.split(";");

			for(int i=0;i<tripid.length;i++){
				String tripId=tripid[i];
				m_pst = m_conn.prepareStatement(query);
				m_pst.setString(1, tripId);
				status = m_pst.executeUpdate();
			}
		}
		catch (SQLException sqex) {
			cat.error("TDSException AdministrationDAO.updateOpenRequestforMjobs-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AdministrationDAO.updateOpenRequestforMjobs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AdministrationDAO.updateOpenRequestforMjobs End--->"+(System.currentTimeMillis()-startTime));
		return status;
	}





}

