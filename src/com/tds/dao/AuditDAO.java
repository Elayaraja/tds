package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;
import org.apache.xerces.dom3.as.ASModel;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.util.TDSSQLConstants;

public class AuditDAO {
	private static Category cat =TDSController.cat;
	static {
		cat = Category.getInstance(AuditDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+AuditDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}


	public static  int insertauditRequest(String ASSOCIATION_CODE,String USER_ID,String DESCRIPTION,String STARTVALUE,String TOVALUE,String MODULE,String FROMTIME,String TOTIME) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int keyValue = 0;

		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			keyValue = ConfigDAO.tdsKeyGen("AUDITID",con);
			pst = con.prepareStatement(TDSSQLConstants.insertauditRequest);
			pst.setInt(1,keyValue);
			pst.setString(2,ASSOCIATION_CODE);
			pst.setString(3,USER_ID);
			pst.setString(4,DESCRIPTION);
			pst.setString(5,STARTVALUE);
			pst.setString(6,TOVALUE);
			pst.setString(7,MODULE);
			pst.setString(8,FROMTIME);
			pst.setString(9,TOTIME);

			pst.executeUpdate();
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException AuditDAO.insertauditRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AuditDAO.insertauditRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return keyValue;
	}
	public  void insertManualcreditRequest(PaymentProcessBean PaymentProcess) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();		TDSConnection dbcon = null;
		PreparedStatement pst = null;
		try {
			dbcon = new TDSConnection();
			Connection con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.insertmanualcreditRequest);
			pst.setString(1,PaymentProcess.getB_operator());
			pst.setString(2,PaymentProcess.getB_driverid());
			pst.setString(3,PaymentProcess.getB_card_no());
			pst.setString(4,PaymentProcess.getB_amount());
			pst.setString(5,PaymentProcess.getB_tip_amt());
			pst.setString(6,PaymentProcess.getB_desc());
			cat.info(pst.toString());
			pst.executeUpdate();
			pst.close();
			con.close();
		} catch(SQLException sqex) {
			cat.error("TDSException AuditDAO.insertManualcreditRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AuditDAO.insertManualcreditRequest-->"+sqex.getMessage());
			sqex.printStackTrace();		
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}

	public static int insertJobLogs(String reason,String tripId,String associationCode,int status, String madeBy,String latitude,String longitude,String masterAssociationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		
		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		int result=0,result1=0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		try {
			String tripid[]=tripId.split(";");
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];

				pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS,ORL_LATITUDE,ORL_LONGITUDE,ORL_MASTER_ASSOCCODE) VALUES (?,NOW(),?,?,?,?,?,?,?)");

				pst.setString(1, trip);
				pst.setString(2, reason);
				pst.setString(3, madeBy);
				pst.setString(4, associationCode);
				pst.setInt(5, status);
				pst.setString(6, latitude);
				pst.setString(7, longitude);
				pst.setString(8, masterAssociationCode);
				result = pst.executeUpdate();
				//System.out.println("result"+pst);

				pst1=con.prepareStatement("INSERT INTO TDS_CONSOLE (C_TRIPID,C_TIME,C_REASON,C_CHANGED_BY,C_ASSOCCODE,C_STATUS,C_LATITUDE,C_LONGITUDE,C_MASTER_ASSOCCODE) VALUES (?,NOW(),?,?,?,?,?,?,?)");
				pst1.setString(1, trip);
				pst1.setString(2, reason);
				pst1.setString(3, madeBy);
				pst1.setString(4, associationCode);
				pst1.setInt(5, status);
				pst1.setString(6, latitude);
				pst1.setString(7, longitude);
				pst1.setString(8, masterAssociationCode);
				result1 = pst1.executeUpdate();
				//System.out.println("result"+pst1);

			}
		}catch (Exception sqex){
			cat.error("TDSException AuditDAO.insertJobLogs-->"+sqex.getMessage());
			//System.out.println("TDSException AuditDAO.insertJobLogs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static int insertJobLogs_Payment(String reason,String tripId,String associationCode,int status, String madeBy,String latitude,String longitude,String masterAssociationCode,String jobsPayment){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		int result=0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		try {
			String tripid[]=tripId.split(";");
			for(int i=0;i<tripid.length;i++){
				String trip=tripid[i];
				pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS,ORL_LATITUDE,ORL_LONGITUDE,ORL_MASTER_ASSOCCODE,ORL_PAYMENT) VALUES (?,NOW(),?,?,?,?,?,?,?,?)");
				pst.setString(1, trip);
				pst.setString(2, reason);
				pst.setString(3, madeBy);
				pst.setString(4, associationCode);
				pst.setInt(5, status);
				pst.setString(6, latitude);
				pst.setString(7, longitude);
				pst.setString(8, masterAssociationCode);
				pst.setString(9, jobsPayment);
				//System.out.println("string:"+pst.toString());
				result = pst.executeUpdate();
			}
		}catch (Exception sqex){
			cat.error("TDSException AuditDAO.insertJobLogs_Payment-->"+sqex.getMessage());
			//cat.error(pst.toString());
			//System.out.println("TDSException AuditDAO.insertJobLogs_Payment-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		//System.out.println("result on insert:"+result);
		return result;
	}




	public static int insertJobLogsForSharedRide(String reason,String routeNumber,String associationCode,int status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		int result=0,result1=0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		PreparedStatement pst1 = null;
		try {
			pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS(ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS,ORL_LATITUDE,ORL_LONGITUDE,ORL_MASTER_ASSOCCODE) "
					+ "SELECT OR_TRIP_ID,NOW(),?,OR_CREATED_BY,OR_ASSOCCODE,?,OR_STLATITUDE,OR_STLONGITUDE,OR_MASTER_ASSOCCODE FROM TDS_OPENREQUEST "
					+ "WHERE OR_ROUTE_NUMBER =? AND OR_MASTER_ASSOCCODE=? ");
			pst.setString(1, reason);
			pst.setInt(2, status);
			pst.setString(3, routeNumber);
			pst.setString(4, associationCode);
			System.out.println("Query To Update--->"+pst);
			result = pst.executeUpdate();

			pst1=con.prepareStatement("INSERT INTO TDS_CONSOLE (C_TRIPID,C_TIME,C_REASON,C_CHANGED_BY,C_ASSOCCODE,C_STATUS,C_LATITUDE,C_LONGITUDE,C_MASTER_ASSOCCODE) "
					+ "SELECT OR_TRIP_ID,NOW(),?,OR_CREATED_BY,OR_ASSOCCODE,?,OR_STLATITUDE,OR_STLONGITUDE,OR_MASTER_ASSOCCODE FROM TDS_OPENREQUEST "
					+ "WHERE OR_ROUTE_NUMBER =? AND OR_MASTER_ASSOCCODE=? ");
			pst1.setString(1, reason);
			pst1.setInt(2, status);
			pst1.setString(3, routeNumber);
			pst1.setString(4, associationCode);

			result1 = pst1.executeUpdate();


		}catch (Exception sqex){
			cat.error("TDSException AuditDAO.insertJobLogs-->"+sqex.getMessage());
			cat.error(pst.toString());
			cat.error(pst1.toString());			
			//System.out.println("TDSException AuditDAO.insertJobLogs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int insertJobLogsForRoute(String reason,String tripId,String associationCode,int status, String madeBy,String latitude,String longitude){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		int result=0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1 = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection(); 
		try {
			pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_ASSOCCODE,ORL_STATUS,ORL_LATITUDE,ORL_LONGITUDE) VALUES (?,NOW(),?,?,?,?,?,?)");
			pst.setString(1, tripId);
			pst.setString(2, reason);
			pst.setString(3, madeBy);
			pst.setString(4, associationCode);
			pst.setInt(5, status);
			pst.setString(6, latitude);
			pst.setString(7, longitude);
			cat.info(pst.toString());
			result = pst.executeUpdate();

			pst1=con.prepareStatement("INSERT INTO TDS_CONSOLE (C_TRIPID,C_TIME,C_REASON,C_CHANGED_BY,C_ASSOCCODE,C_STATUS,C_LATITUDE,C_LONGITUDE) VALUES (?,NOW(),?,?,?,?,?,?)");
			pst1.setString(1, tripId);
			pst1.setString(2, reason);
			pst1.setString(3, madeBy);
			pst1.setString(4, associationCode);
			pst1.setInt(5, status);
			pst1.setString(6, latitude);
			pst1.setString(7, longitude);
			cat.info(pst1.toString());
			result = pst1.executeUpdate();




		}catch (Exception sqex){
			cat.error("TDSException AuditDAO.insertJobLogs-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AuditDAO.insertJobLogs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime)); 
		return result;
	}

	public static String getJobLogs(String assoccode,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String result="";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		try {
			pst=con.prepareStatement("SELECT ORL_REASON FROM TDS_OPENREQUEST_LOGS WHERE ORL_TRIPID='"+tripId+"' AND ORL_MASTER_ASSOCCODE='"+assoccode+"' AND ORL_STATUS='101' ORDER BY ORL_TIME DESC limit 0,1");
			rs = pst.executeQuery();
			if(rs.next()){
				result=rs.getString("ORL_REASON");
			}
		}catch (Exception sqex){
			cat.error("TDSException AuditDAO.getJobLogs-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AuditDAO.getJobLogs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static void updateJobLogs(AdminRegistrationBO adminBo,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		try {
			pst=con.prepareStatement("UPDATE TDS_OPENREQUEST_LOGS SET ORL_ASSOCCODE='"+adminBo.getAssociateCode()+"',ORL_MASTER_ASSOCCODE='"+adminBo.getMasterAssociateCode()+"' WHERE ORL_TRIPID='"+tripId+"' AND ORL_ASSOCCODE='"+adminBo.getChangeCompanyCode()+"'");
			pst.executeUpdate();
		}catch (Exception sqex){
			cat.error("TDSException AuditDAO.insertJobLogs-->"+sqex.getMessage());
			//System.out.println("TDSException AuditDAO.insertJobLogs-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}

}
