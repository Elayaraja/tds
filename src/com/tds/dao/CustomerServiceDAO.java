package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.ComplaintDetailBean;
import com.tds.cmp.bean.ComplaintsBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.util.TDSSQLConstants;
import com.common.util.TDSValidation;

public class CustomerServiceDAO {
	private static Category cat =TDSController.cat;
	
	static {
		cat = Category.getInstance(CustomerServiceDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+CustomerServiceDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	
	
	public static ArrayList<ComplaintsBean> getComplaintSummaryFromMaster(ComplaintsBean complaintsBean, String associationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ComplaintsDAO.getComplaints");
		ArrayList<ComplaintsBean> complaintList = new ArrayList<ComplaintsBean>();
		TDSConnection dbconn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String searchCrietria = "";
		dbconn = new TDSConnection();
		conn = dbconn.getConnection();
		String query = "SELECT CM_CNO,CM_DRIVERID,CM_PASSNAME,CM_PASS_PH,CM_STATUS,CM_SUBJECT,CM_CREATED_BY,CM_DATEANDTIME,CM_TRIPID FROM TDS_COMPLAINT_MASTER WHERE CM_ASSOCODE=?";
		if (!complaintsBean.getComplaintNo().equals("")) {
			searchCrietria += " AND  CM_CNO = '" + complaintsBean.getComplaintNo() + "'";
		}
		if (!complaintsBean.getDriverId().equals("")) {
			searchCrietria = " AND CM_DRIVERID = '" + complaintsBean.getDriverId()+ "'";
		}
		if (!complaintsBean.getPassengerPhone().equals("")) {
			searchCrietria = searchCrietria + " AND CM_PASS_PH = '"+ complaintsBean.getPassengerPhone() + "'";
		}
		if (!complaintsBean.getSubject().equalsIgnoreCase("")) {
			searchCrietria = searchCrietria + " AND CM_SUBJECT LIKE '%"+ complaintsBean.getSubject() + "'";
		}
		if (!complaintsBean.getStatus().equalsIgnoreCase("")) {
			searchCrietria = searchCrietria + " AND CM_STATUS = '"+ complaintsBean.getStatus() + "'";
		}
		query = query + searchCrietria ;
		try {
			pst = conn.prepareStatement(query);
			pst.setString(1, associationCode);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while (rs.next()) {
				ComplaintsBean complaintBean = new ComplaintsBean();
				complaintBean.setComplaintNo(rs.getString("CM_CNO"));
				complaintBean.setDriverId(rs.getString("CM_DRIVERID"));
				complaintBean.setPassengerName(rs.getString("CM_PASSNAME"));
				complaintBean.setPassengerPhone(rs.getString("CM_PASS_PH"));
				complaintBean.setStatus(rs.getString("CM_STATUS"));
				complaintBean.setSubject(rs.getString("CM_SUBJECT"));
				complaintBean.setTripId(rs.getString("CM_TRIPID"));
				complaintBean.setCreatedBy(rs.getString("CM_CREATED_BY"));
				complaintBean.setDateTime(TDSValidation.getNowFomat(rs.getString("CM_DATEANDTIME")));
				complaintList.add(complaintBean);
			}

		} catch (Exception sqex) {
			cat.error(("TDSException ComplaintsDAO.getComplaints-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException ComplaintsDAO.getComplaints-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbconn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return complaintList;
	}
	public static int insertComplaintMaster(ComplaintsBean complaintsBean, String associationcode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ComplaintsDAO.insertComplaintMaster");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		int complaintNumber = 0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			AdminRegistrationBO adminBO = new AdminRegistrationBO();
			pst = con.prepareStatement(TDSSQLConstants.insertComplaints);
			pst.setString(1, complaintsBean.getDriverId());
			pst.setString(3, complaintsBean.getPassengerName());
			pst.setString(4, complaintsBean.getPassengerPhone());
			pst.setString(5, complaintsBean.getSubject());
			pst.setString(6, associationcode);
			pst.setString(7, complaintsBean.getStatus());
			pst.setString(8, complaintsBean.getTripId());
			pst.setString(2,complaintsBean.getCreatedBy());
			cat.info(pst.toString());
			pst.execute();
			pst = con.prepareStatement("SELECT LAST_INSERT_ID() AS COMPLAINTNO FROM TDS_COMPLAINT_MASTER WHERE CM_ASSOCODE=?");
			pst.setString(1,associationcode);
			rs = pst.executeQuery();
			if(rs.next()){
				complaintNumber = rs.getInt("COMPLAINTNO");
			}
			success = true;
		} catch (Exception sqex) {
			cat.error(("TDSException ComplaintsDAO.insertComplaintMaster-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException ComplaintsDAO.insertComplaintMaster-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return complaintNumber;
	}
	public static void updateStatus(ComplaintDetailBean complaintDetailBean, String associationcode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ComplaintsDAO.updateStatus");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		int complaintNumber = 0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			AdminRegistrationBO adminBO = new AdminRegistrationBO();
			pst = con.prepareStatement("UPDATE TDS_COMPLAINT_MASTER SET CM_STATUS=? WHERE CM_CNO=? AND CM_ASSOCODE=?");
			pst.setString(1, complaintDetailBean.getStatus());
			pst.setInt(2, complaintDetailBean.getComplaintNo());
			pst.setString(3,associationcode);
			cat.info(pst.toString());
			pst.execute();
			success = true;
		} catch (Exception sqex) {
			cat.error(("TDSException ComplaintsDAO.updateStatus-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException ComplaintsDAO.updateStatus-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static boolean insertComplaintDetail(ComplaintDetailBean complaintDetailBean, String associationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ComplaintsDAO.insertComplaintDetail");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			m_pst = con.prepareStatement(TDSSQLConstants.setComplaints);
			m_pst.setInt(1,complaintDetailBean.getComplaintNo());
			m_pst.setString(2, complaintDetailBean.getDescription());
			m_pst.setString(3,complaintDetailBean.getCreatedBy());
			cat.info(m_pst.toString());
			m_pst.execute();
			success = true;
		} catch (Exception sqex) {
			cat.error(("TDSException ComplaintsDAO.insertComplaintDetail-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException ComplaintsDAO.insertComplaintDetail-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}
	public static ComplaintsBean getComplaintDetails(int complaintNO, String associationcode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ComplaintsDAO.getCustomerComplaintDetails");
		ComplaintsBean complaintDetail = new ComplaintsBean();
		TDSConnection dbconn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbconn = new TDSConnection();
		conn = dbconn.getConnection();
		String query = "SELECT CM_CNO,CM_DRIVERID,CM_PASSNAME,CM_PASS_PH,CM_STATUS,CM_SUBJECT,CM_CREATED_BY,CM_TRIPID FROM TDS_COMPLAINT_MASTER where CM_CNO =? and CM_ASSOCODE=?";
		try {
			pst = conn.prepareStatement(query);
			pst.setInt(1, complaintNO);
			pst.setString(2, associationcode);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			
			if (rs.next()) {
				complaintDetail.setComplaintNo(rs.getString("CM_CNO"));
				complaintDetail.setDriverId(rs.getString("CM_DRIVERID"));
				complaintDetail.setPassengerName(rs.getString("CM_PASSNAME"));
				complaintDetail.setPassengerPhone(rs.getString("CM_PASS_PH"));
				complaintDetail.setStatus(rs.getString("CM_STATUS"));
				complaintDetail.setSubject(rs.getString("CM_SUBJECT"));
				complaintDetail.setCreatedBy(rs.getString("CM_CREATED_BY"));
				complaintDetail.setTripId(rs.getString("CM_TRIPID"));
			}
		} catch (Exception sqex) {
			cat.error(("TDSException ComplaintsDAO.getCustomerComplaintDetails-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException ComplaintsDAO.getCustomerComplaintDetails-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbconn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return complaintDetail;
	}
	public static ArrayList<ComplaintsBean> getComplaintDetailsByComplaintNumber(int complaintNO,ComplaintsBean complaint) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ComplaintsDAO.getComplaints");
		ArrayList<ComplaintsBean> al_listD = new ArrayList<ComplaintsBean>();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			AdminRegistrationBO adminBO = new AdminRegistrationBO();
			m_pst = m_conn.prepareStatement("SELECT CD_DESCRIPTION,CD_DATEANDTIME,CD_CREATED_BY FROM TDS_COMPLAINT_DETAILS WHERE CD_CMP_NO=? ");
			m_pst.setInt(1, complaintNO);
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{        
				ComplaintsBean complaintDetail = new ComplaintsBean();
				complaintDetail.setDesc(rs.getString("CD_DESCRIPTION"));
				complaintDetail.setCreatedBy(rs.getString("CD_CREATED_BY"));
				complaintDetail.setDateTime(TDSValidation.getNowFomat(rs.getString("CD_DATEANDTIME")));
				al_listD.add(complaintDetail);
			}
		}catch (Exception sqex)
		{
			cat.error(("TDSException ComplaintsDAO.getComplaints-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException ComplaintsDAO.getComplaints-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_listD;
	}


}
