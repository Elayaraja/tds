package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverRestrictionBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.MessageBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.DistanceCalculation;
import com.common.util.TDSConstants;
import com.tds.util.TDSSQLConstants;
import com.common.util.TDSValidation;

public class DispatchDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(DispatchDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+DispatchDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}

	public static ArrayList<DriverCabQueueBean> allDriversWithSpecialFlags(ArrayList<String> assocode,String associationCode, String profileFlags, ArrayList<String> driversNotToConsider)
	{
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+associationCode+"')";
			}
		} else {
			fleets = "('"+associationCode+"')";
		}
		ArrayList<DriverCabQueueBean> orBo = allDriversWithSpecialFlags(fleets,profileFlags,driversNotToConsider);
		return orBo;
	}
	public static ArrayList<DriverCabQueueBean> allDriversWithSpecialFlags(String assoccode, String profileFlags, ArrayList<String> driversNotToConsider)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.allDriversWithSpecialFlags");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		ArrayList<DriverCabQueueBean> drList = new ArrayList<DriverCabQueueBean>();
		String openReqPrf = profileFlags.replaceAll("|", "");
		char[] openReqCharArray = openReqPrf.toCharArray();
		String sqlQuery = "";
		for(int i=0;i<openReqCharArray.length-1;i++)
		{
			sqlQuery=  sqlQuery + " AND LOCATE('" +openReqCharArray[i] + "', DL_PROFILE) > 0  ";
		}
		sqlQuery=  sqlQuery + " AND LOCATE('" +openReqCharArray[openReqCharArray.length-1] + "', DL_PROFILE) > 0";
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		String finalSQL = "SELECT * FROM TDS_DRIVERLOCATION  WHERE DL_ASSOCCODE  = '" + assoccode + "'" + sqlQuery +
				" AND DL_SWITCH = 'Y'";
		String removeDrivers = "";
		if(driversNotToConsider != null){
			if(driversNotToConsider.size() >0){
				removeDrivers = "AND DL_DRIVERID NOT IN(";
				for(int j=0; j<driversNotToConsider.size()-1;j++){
					removeDrivers = removeDrivers + "'" + driversNotToConsider.get(j) + "',";
				}
				removeDrivers = removeDrivers + "'" + driversNotToConsider.get(driversNotToConsider.size()-1) + "')";
			}
		}
		try {
			m_pst = m_conn.prepareStatement(finalSQL);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();			
			while(rs.next()){
				DriverCabQueueBean dr = new DriverCabQueueBean();
				dr.setDriverid(rs.getString("DL_DRIVERID"));
				dr.setPhoneNo(rs.getString("DL_PHONENO"));
				dr.setProvider(rs.getString("DL_PROVIDER"));
				dr.setCurrentLatitude(Double.parseDouble(rs.getString("DL_LATITUDE")));
				dr.setCurrentLongitude(Double.parseDouble(rs.getString("DL_LONGITUDE")));
				dr.setPushKey(rs.getString("DL_GOOG_REG_KEY"));
				dr.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				drList.add(dr);
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.allDriversWithSpecialFlags-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.allDriversWithSpecialFlags-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drList;
	}
	public static ArrayList<DriverCabQueueBean> allDriversWithInADistance(ArrayList<String> assoccode, double latitude, double longitude, double distanceInMiles, ArrayList<String> driversNotToConsider, int jobRating,double maxDist)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.allDriversWithInADistance");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,pst1=null;
		ResultSet rs = null, rs1=null;
		//The total distance occupied by 1 deg of earth's lat and long equates to 69.172 miles
		//This calculation uses that to defines how much deg is available in a 1 mile
		//For x miles multiply by the 1 mile degree to get the degree offset
		//Add and subtract that from the given lat and long to get a "square".
		//The edges of the square will be more than X miles, but this is a good enough approximation.

		double milesPerDeg = 1/69.172; 
		double degressToOffset = distanceInMiles * milesPerDeg;

		double northLatitude = latitude + degressToOffset;
		double southLatitude = latitude - degressToOffset;

		double eastLongitude = longitude + degressToOffset;
		double westLongitude = longitude - degressToOffset;

		String fleets ="";
		if(assoccode != null){
			if(assoccode.size() >0){
				fleets = "(";
				for(int j=0; j<assoccode.size()-1;j++){
					fleets = fleets + "'" + assoccode.get(j) + "',";
				}
				fleets = fleets + "'" + assoccode.get(assoccode.size()-1) + "')";
			}
		}

		String removeDrivers = "";
		if(driversNotToConsider != null){
			if(driversNotToConsider.size() >0){
				removeDrivers = "AND DL_DRIVERID NOT IN(";
				for(int j=0; j<driversNotToConsider.size()-1;j++){
					removeDrivers = removeDrivers + "'" + driversNotToConsider.get(j) + "',";
				}
				removeDrivers = removeDrivers + "'" + driversNotToConsider.get(driversNotToConsider.size()-1) + "')";
			}
		}

		ArrayList<DriverCabQueueBean> drList = new ArrayList<DriverCabQueueBean>();

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String finalSQL = "SELECT * FROM TDS_DRIVERLOCATION DL  WHERE " +
					"DL_ASSOCCODE IN "+fleets+ " AND  DL_DRIVER_RATING >="+jobRating+" AND DL_LATITUDE BETWEEN "+southLatitude+" AND "+northLatitude+ 
					" AND DL_LONGITUDE BETWEEN "+westLongitude+" AND "+eastLongitude + 
					" AND DL_SWITCH = 'Y' " + removeDrivers;
			m_pst = m_conn.prepareStatement(finalSQL);
			System.out.println("1st query"+m_pst.toString());
			rs = m_pst.executeQuery();			

			while(rs.next()){
				DriverCabQueueBean dr = new DriverCabQueueBean();
				String driverId=rs.getString("DL_VEHICLE_NO");

				pst1 = m_conn.prepareStatement("SELECT * FROM TDS_VEHICLE WHERE V_ASSOCCODE IN "+fleets+" AND V_VNO=? ");
				pst1.setString(1, driverId);
				//System.out.println("2nd auery"+pst1.toString());
				rs1 = pst1.executeQuery();

				dr.setDriverid(rs.getString("DL_DRIVERID"));
				dr.setPhoneNo(rs.getString("DL_PHONENO"));
				dr.setProvider(rs.getString("DL_PROVIDER"));
				dr.setCurrentLatitude(rs.getDouble("DL_LATITUDE"));
				dr.setCurrentLongitude(rs.getDouble("DL_LONGITUDE"));
				dr.setPushKey(rs.getString("DL_GOOG_REG_KEY"));
				dr.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				dr.setDrivername(rs.getString("DL_DRNAME"));
				if(rs1.next()){
					dr.setvMake(rs1.getString("V_VMAKE"));
					dr.setvType(rs1.getString("V_MODEL"));
				}
				double distance=DistanceCalculation.distance(latitude, dr.getCurrentLatitude(), longitude, dr.getCurrentLongitude());
				if(distance < maxDist || maxDist == 0){
					drList.add(dr);
				}
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.allDriversWithInADistance-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.allDriversWithInADistance-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drList;
	}
	public static ArrayList<DriverCabQueueBean> allDriversWithInADistanceWithSpLFLAGS(ArrayList<String> assoccode, double latitude, double longitude, double distanceInMiles, String profileFlags, ArrayList<String> driversNotToConsider, int jobRating,double maxDist)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.allDriversWithInADistanceWithSpLFLAGS");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		//The total distance occupied by 1 deg of earth's lat and long equates to 69.172 miles
		//This calculation uses that to defines how much deg is available in a 1 mile
		//For x miles multiply by the 1 mile degree to get the degree offset
		//Add and subtract that from the given lat and long to get a "square".
		//The edges of the square will be more than X miles, but this is a good enough approximation.

		double milesPerDeg = 1/69.172; 
		double degressToOffset = distanceInMiles * milesPerDeg;

		double northLatitude = latitude + degressToOffset;
		double southLatitude = latitude - degressToOffset;

		double eastLongitude = longitude + degressToOffset;
		double westLongitude = longitude - degressToOffset;

		String openReqPrf = profileFlags.replaceAll("|", "");
		char[] openReqCharArray = openReqPrf.toCharArray();

		String removeDrivers = "";
		if(driversNotToConsider != null && driversNotToConsider.size() >0){
			if(driversNotToConsider.size() >0){
				removeDrivers = " AND DL_DRIVERID NOT IN(";
				for(int j=0; j<driversNotToConsider.size()-1;j++){
					removeDrivers = removeDrivers + "'" + driversNotToConsider.get(j) + "',";
				}
				removeDrivers = removeDrivers + "'" + driversNotToConsider.get(driversNotToConsider.size()-1) + "')";
			}
		}
		String fleets ="";
		if(assoccode != null){
			if(assoccode.size() >0){
				fleets = "(";
				for(int j=0; j<assoccode.size()-1;j++){
					fleets = fleets + "'" + assoccode.get(j) + "',";
				}
				fleets = fleets + "'" + assoccode.get(assoccode.size()-1) + "')";
			}
		}

		String sqlQuery="";
		if(openReqCharArray.length>0){
			sqlQuery= " AND ";
			for(int i=0;i<openReqCharArray.length-1;i++)
			{
				sqlQuery=  sqlQuery + " LOCATE('" +openReqCharArray[i] + "', DL_PROFILE) > 0 AND ";
			}
			sqlQuery=  sqlQuery + " LOCATE('" +openReqCharArray[openReqCharArray.length-1] + "', DL_PROFILE) > 0";
		}

		ArrayList<DriverCabQueueBean> drList = new ArrayList<DriverCabQueueBean>();
		String finalSQL = "SELECT * FROM TDS_DRIVERLOCATION DL  WHERE  " +
				"DL_ASSOCCODE IN "+fleets+"  AND DL_SWITCH = 'Y' AND DL_DRIVER_RATING >="+jobRating+" AND DL_LATITUDE  BETWEEN "+southLatitude+" AND "+northLatitude+ " " +
				"AND DL_LONGITUDE BETWEEN "+westLongitude+" AND "+eastLongitude+" " +
				sqlQuery + removeDrivers;

		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement(finalSQL);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();			

			while(rs.next()){
				DriverCabQueueBean dr = new DriverCabQueueBean();
				dr.setDriverid(rs.getString("DL_DRIVERID"));
				dr.setPhoneNo(rs.getString("DL_PHONENO"));
				dr.setProvider(rs.getString("DL_PROVIDER"));
				dr.setPushKey(rs.getString("DL_GOOG_REG_KEY"));
				dr.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				dr.setCurrentLatitude(rs.getDouble("DL_LATITUDE"));
				dr.setCurrentLongitude(rs.getDouble("DL_LONGITUDE"));
				double distance=DistanceCalculation.distance(latitude, dr.getCurrentLatitude(), longitude, dr.getCurrentLongitude());
				if(distance < maxDist || maxDist == 0){
					drList.add(dr);
				}
			}


		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.allDriversWithInADistanceWithSpLFLAGS-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.allDriversWithInADistanceWithSpLFLAGS-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drList;
	}
	public static ArrayList<DriverCabQueueBean> getDriverDetail(String p_queueId,String assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getDriverDetail");
		ArrayList<DriverCabQueueBean> drList = new ArrayList<DriverCabQueueBean>();
		String m_driverId = "";

		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getQueueOrder);
			//FROM TDS_QUEUE WHERE QU_NAME LIKE ? ORDER BY QU_LOGINTIME LIMIT 1
			m_pst.setString(1, p_queueId);
			m_pst.setString(2, assocode);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				DriverCabQueueBean driverCabQueueBean = new DriverCabQueueBean();
				driverCabQueueBean.setDriverid(m_rs.getString("QU_DRIVERID"));
				driverCabQueueBean.setDrFlag(m_rs.getString("QU_DRPROFILE"));
				driverCabQueueBean.setPhoneNo(m_rs.getString("QU_PHONENO"));
				driverCabQueueBean.setProvider(m_rs.getString("QU_PROVIDER"));
				driverCabQueueBean.setPushKey(m_rs.getString("QU_GOOG_REG_KEY"));
				driverCabQueueBean.setVehicleNo(m_rs.getString("QU_VEHICLE_NO"));
				drList.add(driverCabQueueBean);
			}

			m_pst.close();
			m_conn.close();


		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getDriverDetail-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.getDriverDetail-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drList;
	}

	
	public static ArrayList<DriverCabQueueBean> getDriverDetails(String assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getDriverDetail");
		ArrayList<DriverCabQueueBean> drList = new ArrayList<DriverCabQueueBean>();
		String m_driverId = "";

		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String finalSQL = "SELECT * FROM TDS_DRIVERLOCATION DL  WHERE  " +
				"DL_ASSOCCODE ="+assocode+" ";

		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement(finalSQL);
			//FROM TDS_QUEUE WHERE QU_NAME LIKE ? ORDER BY QU_LOGINTIME LIMIT 1
			cat.info(m_pst.toString());
			//System.out.println("query--->"+m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				DriverCabQueueBean driverCabQueueBean = new DriverCabQueueBean();
				driverCabQueueBean.setDriverid(m_rs.getString("DL_DRIVERID"));
				driverCabQueueBean.setAssocode(m_rs.getString("DL_ASSOCCODE"));
				drList.add(driverCabQueueBean);
			}

			m_pst.close();
			m_conn.close();


		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getDriverDetail-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.getDriverDetail-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drList;
	}
	
	public static ArrayList<DriverCabQueueBean> getDriverDetailWithAdjZones(String p_queueId,ArrayList<String> assocode, int orderType) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getDriverDetail");
		ArrayList<DriverCabQueueBean> drList = new ArrayList<DriverCabQueueBean>();
		String m_driverId = "";
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			}
		}

		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			if(orderType == 1){
				m_pst = m_conn.prepareStatement("SELECT QU_NAME, QU_DRIVERID , QU_DRPROFILE, QU_PHONENO, QU_PROVIDER, QU_GOOG_REG_KEY, QU_DRIVER_RATING, QU_VEHICLE_NO, ordernum FROM TDS_QUEUE"
						+ " INNER JOIN "
						+ " ((SELECT AZ_ORDER_NUMBER as ordernum, AZ_ADJACENT_ZONES FROM  TDS_ADJACENT_ZONES TZ where AZ_ZONE_NUMBER = ?  AND AZ_ASSOCCODE  IN "+fleets+" ORDER BY "
						+ " AZ_ORDER_NUMBER) UNION (SELECT 0, ? FROM TDS_BIN) ORDER BY ordernum) B"
						+ " ON  AZ_ADJACENT_ZONES = QU_NAME"
						+ " WHERE  QU_ASSOCCODE IN "+fleets
						+ " AND ( (ordernum  = 0) OR (ordernum  <> 0 AND QU_ACCEPT_JOBS_FROM_OTHER_ZONES = 1))"
						+ " ORDER BY ordernum, QU_LOGINTIME");
			} else {
				m_pst = m_conn.prepareStatement("SELECT QU_NAME, QU_DRIVERID , QU_DRPROFILE, QU_PHONENO, QU_PROVIDER,QU_DRIVER_RATING, QU_GOOG_REG_KEY, QU_VEHICLE_NO, ordernum FROM TDS_QUEUE"
						+ " INNER JOIN "
						+ " ((SELECT 1 as ordernum, AZ_ADJACENT_ZONES FROM  TDS_ADJACENT_ZONES TZ where AZ_ZONE_NUMBER = ?  AND AZ_ASSOCCODE  IN "+fleets+" ORDER BY "
						+ " AZ_ORDER_NUMBER) UNION (SELECT 0, ? FROM TDS_BIN) ORDER BY ordernum) B"
						+ " ON  AZ_ADJACENT_ZONES = QU_NAME"
						+ " WHERE  QU_ASSOCCODE IN "+fleets
						+ " AND ( (ordernum  = 0) OR (ordernum  <> 0 AND QU_ACCEPT_JOBS_FROM_OTHER_ZONES = 1))"
						+ " ORDER BY ordernum, QU_LOGINTIME");
			}
			//FROM TDS_QUEUE WHERE QU_NAME LIKE ? ORDER BY QU_LOGINTIME LIMIT 1
			m_pst.setString(1, p_queueId);
			m_pst.setString(2, p_queueId);
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				DriverCabQueueBean driverCabQueueBean = new DriverCabQueueBean();
				driverCabQueueBean.setDriverid(m_rs.getString("QU_DRIVERID"));
				driverCabQueueBean.setDrFlag(m_rs.getString("QU_DRPROFILE"));
				driverCabQueueBean.setPhoneNo(m_rs.getString("QU_PHONENO"));
				driverCabQueueBean.setProvider(m_rs.getString("QU_PROVIDER"));
				driverCabQueueBean.setPushKey(m_rs.getString("QU_GOOG_REG_KEY"));
				driverCabQueueBean.setVehicleNo(m_rs.getString("QU_VEHICLE_NO"));
				driverCabQueueBean.setDriverRating(m_rs.getInt("QU_DRIVER_RATING"));
				drList.add(driverCabQueueBean);
			}

			m_pst.close();
			m_conn.close();


		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getDriverDetail-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.getDriverDetail-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drList;
	}

	/*public static int moveToHistory(String tripId, String associationCode, String multiGroup) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.moveToHistory");
		int returnCode = 0;

		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
	       	 try {
			if(!multiGroup.equalsIgnoreCase("")){
				m_pst = m_conn.prepareStatement(TDSSQLConstants.moveOpenRequestToHisotryByGroup);
				m_pst.setString(1, multiGroup);
				m_pst.setString(2, associationCode);
				m_pst.executeUpdate();

				m_pst = m_conn.prepareStatement(TDSSQLConstants.deleteOpenRequestByGroup);
				m_pst.setString(1, multiGroup);
				m_pst.setString(2, associationCode);
				cat.info(m_pst.toString());
				returnCode=m_pst.executeUpdate();
			} else {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.moveOpenRequestToHisotry);
				m_pst.setString(1, tripId);
				m_pst.setString(2, associationCode);
				cat.info(m_pst.toString());
				m_pst.executeUpdate();

				m_pst = m_conn.prepareStatement(TDSSQLConstants.deleteOpenRequest);
				m_pst.setString(1, tripId);
				m_pst.setString(2, associationCode);
				cat.info(m_pst.toString());
				returnCode=m_pst.executeUpdate();
			     }	
			m_pst.close();
			m_conn.close();

		} catch (SQLException sqex) {
			cat.error("TDSException DispatchDAO.moveToHistory-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException DispatchDAO.moveToHistory-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return returnCode;
	}

	 */ 

	public static int moveToHistory(String tripId, String associationCode, String multiGroup,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+associationCode+"')";
			}
		} else {
			fleets = "('"+associationCode+"')";
		}
		int orBo = moveToHistory(tripId,fleets, multiGroup);
		return orBo;
	}

	public static int moveToHistory(String tripId, String associationCode, String multiGroup) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.moveToHistory");
		int returnCode = 0;

		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		String tripid[]=tripId.split(";");
		try {
			if(!multiGroup.equalsIgnoreCase("")){
				m_pst = m_conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_HISTORY (SELECT * FROM TDS_OPENREQUEST WHERE OR_REPEAT_GROUP = ? AND OR_ASSOCCODE  IN "+associationCode+")");
				m_pst.setString(1, multiGroup);
				//				 m_pst.setString(2, associationCode);
				m_pst.executeUpdate();

				m_pst = m_conn.prepareStatement("DELETE FROM TDS_OPENREQUEST WHERE OR_REPEAT_GROUP = ? and OR_ASSOCCODE IN "+associationCode);
				m_pst.setString(1, multiGroup);
				//				 m_pst.setString(2, associationCode);
				cat.info(m_pst.toString());
				returnCode=m_pst.executeUpdate();
			} else {
				for(int i=0;i<tripid.length;i++){
					String trip=tripid[i];
					m_pst = m_conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_HISTORY (SELECT * FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE IN "+associationCode+")");
					m_pst.setString(1, trip);
					//					 m_pst.setString(2, associationCode);
					m_pst.executeUpdate();

					m_pst = m_conn.prepareStatement("DELETE FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = ? and OR_ASSOCCODE IN "+associationCode);
					m_pst.setString(1, trip);
					//					 m_pst.setString(2, associationCode);
					returnCode=m_pst.executeUpdate();
				}	
			}		
			m_pst.close();
			m_conn.close();

		} catch (SQLException sqex) {
			cat.error("TDSException DispatchDAO.moveToHistory-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.moveToHistory-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return returnCode;
	}


	public static ArrayList<DriverCabQueueBean> allDrivers(String assoccode, double latitude, double longitude, double distanceInMiles, String[] driversNotToConsider, String driverStatus)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.allDrivers");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;

		String removeDrivers = "";
		if(driversNotToConsider != null){
			if(driversNotToConsider.length >0){
				removeDrivers = "AND DL_DRIVERID NOT IN(";
				for(int j=0; j<driversNotToConsider.length-1;j++){
					removeDrivers = removeDrivers + "'" + driversNotToConsider[j] + "',";
				}
				removeDrivers = removeDrivers + "'" + driversNotToConsider[driversNotToConsider.length-1] + "')";
			}
		}

		ArrayList<DriverCabQueueBean> drList = new ArrayList<DriverCabQueueBean>();
		String finalSQL = "SELECT * FROM TDS_DRIVERLOCATION DL  WHERE " +
				"DL_ASSOCCODE='"+assoccode+ "' AND DL_SWITCH = 'Y' " + removeDrivers;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(finalSQL);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();			

			while(rs.next()){
				DriverCabQueueBean dr = new DriverCabQueueBean();
				dr.setDriverid(rs.getString("DL_DRIVERID"));
				dr.setPhoneNo(rs.getString("DL_PHONENO"));
				dr.setProvider(rs.getString("DL_PROVIDER"));
				dr.setCurrentLatitude(rs.getDouble("DL_LATITUDE"));
				dr.setCurrentLongitude(rs.getDouble("DL_LONGITUDE"));
				dr.setPushKey(rs.getString("DL_GOOG_REG_KEY"));
				dr.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				drList.add(dr);
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.allDrivers-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.allDrivers-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drList;
	}
	public static DriverCabQueueBean getDriverByDriverID(String associationCode, String driverID, int loggedInSwitch)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		DriverCabQueueBean dr = null;
		String finalSQL="";
		if(loggedInSwitch==1){
			finalSQL = "SELECT *, TIMESTAMPDIFF(SECOND, NOW(), DL_LASTUPDATE) AS TIMEDIFF FROM TDS_DRIVERLOCATION DL  WHERE " +
					"DL_DRIVERID=? AND (DL_MASTER_ASSOCCODE=? OR DL_ASSOCCODE=? ) AND  DL_SWITCH<>'L'" ;
		}
		else{
			finalSQL = "SELECT *, TIMESTAMPDIFF(SECOND, NOW(), DL_LASTUPDATE) AS TIMEDIFF  FROM TDS_DRIVERLOCATION DL  WHERE " +
					"DL_DRIVERID=? AND (DL_MASTER_ASSOCCODE=? OR DL_ASSOCCODE=?)" ;
		}

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			m_pst = m_conn.prepareStatement(finalSQL);
			m_pst.setString(1, driverID);
			m_pst.setString(2, associationCode);
			m_pst.setString(3, associationCode);

			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();			

			while(rs.next()){
				dr = new DriverCabQueueBean();
				dr.setDriverid(rs.getString("DL_DRIVERID"));
				dr.setPhoneNo(rs.getString("DL_PHONENO"));
				dr.setProvider(rs.getString("DL_PROVIDER"));
				dr.setCurrentLatitude(rs.getDouble("DL_LATITUDE"));
				dr.setCurrentLongitude(rs.getDouble("DL_LONGITUDE"));
				dr.setPushKey(rs.getString("DL_GOOG_REG_KEY"));
				dr.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				dr.setDrFlag(rs.getString("DL_PROFILE"));
				dr.setLastReportedTimeInMilliseconds(rs.getInt("TIMEDIFF"));
			}


		} catch (SQLException p_sqex) {
			p_sqex.printStackTrace();
			cat.error(m_pst.toString());
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return dr;
	}

	public static boolean updateAcceptTime(String associationCode, String tripID, int updateField,ArrayList<String> assocode) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('"+associationCode+"')";
			}
		} else {
			fleets = "('"+associationCode+"')";
		}
		boolean orBo = updateAcceptTime(associationCode,tripID,updateField,fleets);
		return orBo;
	}


	public static boolean updateAcceptTime(String associationCode, String tripID, int updateField,String fleets){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		DriverCabQueueBean dr = null;
		boolean returnValue = false;
		String finalSQL = "";
		switch (updateField) {
		case 1:
			finalSQL="UPDATE TDS_OPENREQUEST SET OR_JOB_ACCEPT_TIME = now() where OR_ASSOCCODE IN "+fleets+" AND OR_TRIP_ID=?";
			break;
		case 2:
			finalSQL="UPDATE TDS_OPENREQUEST SET OR_JOB_PICKUP_TIME = now() where OR_ASSOCCODE IN "+fleets+" AND OR_TRIP_ID=?";
			break;
		case 3:
			finalSQL="UPDATE TDS_OPENREQUEST SET OR_JOB_COMPLETE_TIME = now() where OR_ASSOCCODE IN "+fleets+" AND OR_TRIP_ID=?";
			break;
		default:
			break;
		}
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			m_pst = m_conn.prepareStatement(finalSQL);
			//			 m_pst.setString(1, associationCode);
			m_pst.setString(1, tripID);
			cat.info(m_pst.toString());
			int rc = m_pst.executeUpdate();	
			if(rc>0){
				returnValue = true;
			}

		} catch (SQLException p_sqex) {
			p_sqex.printStackTrace();
			cat.error(m_pst.toString());
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return returnValue;
	}

	public static ArrayList<DriverCabQueueBean> getJobLocation(String assoccode,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.getDriverLocation");
		ArrayList<DriverCabQueueBean> jobLocation = new ArrayList<DriverCabQueueBean>();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		String query="";
		if(!tripId.equals("")){
			query=" AND OR_TRIP_ID='"+tripId+"'";
		}
		try {
			pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_DRIVERID,OR_VEHICLE_NO,OR_STLATITUDE,OR_STLONGITUDE,OR_TRIP_STATUS,DL_LATITUDE,DL_LONGITUDE,DL_DRNAME,V_RCNO,V_MODEL,V_VMAKE FROM TDS_OPENREQUEST LEFT JOIN TDS_DRIVERLOCATION ON OR_DRIVERID=DL_DRIVERID AND OR_MASTER_ASSOCCODE=DL_ASSOCCODE LEFT JOIN TDS_VEHICLE  ON OR_VEHICLE_NO=V_VNO AND OR_MASTER_ASSOCCODE=V_MASTER_ASSOCCODE WHERE OR_MASTER_ASSOCCODE=? AND (OR_TRIP_STATUS='"+TDSConstants.performingDispatchProcesses+"' OR OR_TRIP_STATUS= '"+TDSConstants.onRotueToPickup+"' OR OR_TRIP_STATUS= '"+TDSConstants.jobAllocated+"'  OR OR_TRIP_STATUS= '"+TDSConstants.tripStarted+"' OR OR_TRIP_STATUS= '"+TDSConstants.customerCancelledRequest+"' OR OR_TRIP_STATUS= '20' OR OR_TRIP_STATUS= '"+TDSConstants.operatorCancelledRequest+"' OR OR_TRIP_STATUS='"+TDSConstants.jobCompleted+"'  OR OR_TRIP_STATUS='"+TDSConstants.driverReportedNoShow+"'  OR OR_TRIP_STATUS='"+TDSConstants.onSite+"' OR OR_TRIP_STATUS='8' OR OR_TRIP_STATUS='"+TDSConstants.srAllocatedToCab+"' OR OR_TRIP_STATUS='"+TDSConstants.jobSTC+"') "+query);
			pst.setString(1, assoccode);
			//System.out.println("List Query--->"+pst);
			rs=pst.executeQuery();
			if(rs.next()){
				DriverCabQueueBean latlong =new DriverCabQueueBean();
				if(rs.getString("OR_DRIVERID")!=null && !rs.getString("OR_DRIVERID").equals("")){
					latlong.setCurrentLatitude(rs.getDouble("DL_LATITUDE"));
					latlong.setCurrentLongitude(rs.getDouble("DL_LONGITUDE"));
					latlong.setvType(rs.getString("V_MODEL"));
					latlong.setvMake(rs.getString("V_VMAKE"));
					latlong.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
					latlong.setDriverid(rs.getString("OR_DRIVERID"));
				}
				jobLocation.add(latlong);
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.getJobLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.getJobLocation-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return jobLocation;
	}


	//	public static boolean updateDispatchStartTime(OpenRequestBO openRequestBO, int delayTimeInSeconds, String associationCode){
	//		boolean returnValue = false;
	//		TDSConnection m_dbConn = null;
	//		Connection m_conn = null;
	//		PreparedStatement m_pst = null;
	//		String finalSQL="UPDATE TDS_OPENREQUEST SET OR_DISPATCH_START_TIME = DATE_SUB(OR_SERVICEDATE,Interval "+delayTimeInSeconds+" SECOND) where OR_ASSOCCODE=? AND OR_TRIP_ID=?";
	//		m_dbConn  = new TDSConnection();
	//		m_conn = m_dbConn.getConnection();
	//
	//		try {
	//			m_pst = m_conn.prepareStatement(finalSQL);
	//			m_pst.setString(1, associationCode);
	//			m_pst.setString(2, openRequestBO.getTripid());
	//			System.out.println(m_pst.toString());
	//			//System.out.println("updating time to start dispatch"+m_pst.toString());
	//			int rc = m_pst.executeUpdate();	
	//			if(rc>0){
	//				returnValue = true;
	//			}
	//			
	//		} catch (SQLException p_sqex) {
	//			p_sqex.printStackTrace();
	//			cat.error(m_pst.toString());
	//		} finally {
	//			m_dbConn.closeConnection();
	//		}
	//		return returnValue;	
	//	}
	public static boolean updateDispatchStartTime(OpenRequestBO openRequestBO, int delayTimeInSeconds, String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean returnValue = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String query="";
		if(openRequestBO.getRepeatGroup()!=null && !openRequestBO.getRepeatGroup().equals("")){
			query=" AND OR_REPEAT_GROUP='"+openRequestBO.getRepeatGroup()+"'";
		} else {
			query=" AND OR_TRIP_ID='"+openRequestBO.getTripid()+"'";
		}
		String finalSQL="UPDATE TDS_OPENREQUEST SET OR_DISPATCH_START_TIME = DATE_SUB(OR_SERVICEDATE,Interval "+delayTimeInSeconds+" SECOND) WHERE OR_MASTER_ASSOCCODE=?"+query+" AND TIMESTAMPDIFF(SECOND, NOW(), OR_SERVICEDATE) > '"+delayTimeInSeconds+"'";
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement(finalSQL);
			m_pst.setString(1, associationCode);
			int rc = m_pst.executeUpdate();	
			if(rc>0){
				returnValue = true;
			}
		} catch (SQLException p_sqex) {
			p_sqex.printStackTrace();
			cat.error(m_pst.toString());
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return returnValue;	
	}
	public static void updateStartTime(String tripId, String associationCode,int delayTimeInSeconds){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement m_pst = null;
		String finalSQL="UPDATE TDS_OPENREQUEST SET OR_DISPATCH_START_TIME = DATE_SUB(OR_SERVICEDATE,Interval "+delayTimeInSeconds+" SECOND) WHERE OR_MASTER_ASSOCCODE='"+associationCode+"' AND OR_TRIP_ID='"+tripId+"'";
		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement(finalSQL);
			m_pst.execute();
		} catch (SQLException p_sqex) {
			p_sqex.printStackTrace();
			cat.error(m_pst.toString());
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}

	public static ArrayList<String> getJobCount(String assocode,String timeZone) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getJobCount");
		ArrayList<String> jobCount = new ArrayList<String>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null, datePst = null;
		ResultSet m_rs = null, dateRs = null;
		//String query="SELECT count(*) AS JOBCOUNT,ORH_TRIP_STATUS FROM TDS_OPENREQUEST_HISTORY WHERE  DATE(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeZone+"')) = DATE(CONVERT_TZ(NOW(),'UTC','"+timeZone+"')) AND ORH_ASSOCCODE='"+assocode+"' group by ORH_TRIP_STATUS ";
		String query = "";
		try {
			
			datePst = m_conn.prepareStatement("SELECT DATE(CONVERT_TZ(NOW(),'UTC','"+timeZone+"')) AS Date");
			//System.out.println("date select:"+datePst.toString());
			dateRs = datePst.executeQuery();
			String date = "";
			if(dateRs.next()){
				date = dateRs.getString("Date");
			}
			
			query = "SELECT count(*) AS JOBCOUNT,ORH_TRIP_STATUS FROM TDS_OPENREQUEST_HISTORY WHERE  ORH_SERVICEDATE BETWEEN CONVERT_TZ('"+date+" 00:00:01','"+timeZone+"','UTC') AND CONVERT_TZ('"+date+" 23:59:59','"+timeZone+"','UTC') AND ORH_ASSOCCODE='"+assocode+"' group by ORH_TRIP_STATUS ";
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			//System.out.println("jobcount:"+m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				jobCount.add(m_rs.getString("ORH_TRIP_STATUS"));
				jobCount.add(m_rs.getString("JOBCOUNT"));
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getJobCount-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//	System.out.println("TDSException DispatchDAO.getJobCount-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return jobCount;
	}
	public static int sendMessage(String message,String driver,AdminRegistrationBO adminBo,int status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs;
		int resultValue=0;
		try {
			m_pst = m_conn.prepareStatement("INSERT INTO TDS_TEXT_MESSAGE (TM_ASSOCIATION_CODE,TM_DRIVER_ID,TM_TIME,TM_MESSAGE,TM_ENTERED_BY,TM_LOGIN_SWITCH) VALUES (?,?,NOW(),?,?,?)",Statement.RETURN_GENERATED_KEYS);
			m_pst.setString(1, adminBo.getMasterAssociateCode());
			m_pst.setString(2, driver);
			m_pst.setString(3, message);
			m_pst.setString(4, adminBo.getUid());
			m_pst.setInt(5, status);
			m_pst.execute();
			rs = m_pst.getGeneratedKeys();
			if(rs.next()){
				resultValue = rs.getInt(1);
			}
			cat.info(m_pst.toString());
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.sendMessage-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.sendMessage-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static ArrayList<MessageBO> getAllMessage(String assoCode,String update,String LastKey,String timeOffset){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<MessageBO> arrlist = new ArrayList<MessageBO>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		String query1="SELECT TM_KEY,TM_EMERGENCY_SWITCH,TM_DRIVER_ID,TM_MESSAGE,TM_ENTERED_BY,DATE(CONVERT_TZ(TM_TIME ,'UTC','"+timeOffset+"')) AS TM_DATE, TIME_FORMAT(CONVERT_TZ(TM_TIME ,'UTC','"+timeOffset+"'),'%H:%i') AS TM_TIMEVALE,TM_TIME FROM TDS_TEXT_MESSAGE WHERE TM_ASSOCIATION_CODE='"+assoCode+"' and TM_KEY>'"+LastKey+"' AND TM_EMERGENCY_SWITCH!='1' AND TM_TIME > (DATE_SUB(NOW(),INTERVAL 60 minute)) ORDER BY TM_TIME DESC LIMIT 10";
		String query2="SELECT * FROM (SELECT TM_KEY,TM_EMERGENCY_SWITCH,TM_DRIVER_ID,TM_MESSAGE,TM_ENTERED_BY,DATE(CONVERT_TZ(TM_TIME ,'UTC','"+timeOffset+"')) AS TM_DATE, TIME_FORMAT(CONVERT_TZ(TM_TIME ,'UTC','"+timeOffset+"'),'%H:%i') AS TM_TIMEVALE,CONVERT_TZ(TM_TIME ,'UTC','"+timeOffset+"') AS TM_TIME FROM TDS_TEXT_MESSAGE WHERE TM_ASSOCIATION_CODE='"+assoCode+"' AND TM_EMERGENCY_SWITCH!='1' AND TM_TIME > (DATE_SUB(NOW(),INTERVAL 60 minute)) ORDER BY TM_TIME DESC LIMIT 10 ) AS A  ORDER BY TM_TIME DESC";
		String query3 ="SELECT TM_KEY,TM_EMERGENCY_SWITCH,TM_DRIVER_ID,TM_MESSAGE,TM_ENTERED_BY,DATE(CONVERT_TZ(TM_TIME ,'UTC','"+timeOffset+"')) AS TM_DATE, TIME_FORMAT(CONVERT_TZ(TM_TIME ,'UTC', '"+timeOffset+"'),'%H:%i') AS TM_TIMEVALE,TM_TIME FROM TDS_TEXT_MESSAGE WHERE TM_ASSOCIATION_CODE='"+assoCode+"' AND TM_EMERGENCY_SWITCH='1' AND TM_READ_STATUS ='0' AND TM_TIME > (DATE_SUB(NOW(),INTERVAL 60 minute)) ORDER BY TM_TIME DESC LIMIT 10";
		ResultSet rs;
		try {
			m_pst = m_conn.prepareStatement(query3);
			//System.out.println("query3:"+m_pst.toString());
			rs = m_pst.executeQuery();
			if(rs.next()){
				MessageBO msgBo = new MessageBO();
				msgBo.setDriverid(rs.getString("TM_DRIVER_ID"));
				msgBo.setMessage(rs.getString("TM_MESSAGE"));
				msgBo.setEnteredBy(rs.getString("TM_ENTERED_BY"));
				msgBo.setTime(rs.getString("TM_TIMEVALE"));
				msgBo.setMsgDate(rs.getString("TM_DATE"));
				msgBo.setMsgKey(rs.getString("TM_KEY"));
				msgBo.setEmergencyCheck(rs.getInt("TM_EMERGENCY_SWITCH"));
				arrlist.add(msgBo);
			}else{
				if(update.equals("1")){
					m_pst=m_conn.prepareStatement(query1);
				}else{
					m_pst=m_conn.prepareStatement(query2);
				}
				//System.out.println("query 1/2:"+m_pst.toString());
				rs=m_pst.executeQuery();
				while(rs.next()){
					MessageBO msgBo = new MessageBO();
					msgBo.setDriverid(rs.getString("TM_DRIVER_ID"));
					msgBo.setMessage(rs.getString("TM_MESSAGE"));
					msgBo.setEnteredBy(rs.getString("TM_ENTERED_BY"));
					msgBo.setTime(rs.getString("TM_TIMEVALE"));
					msgBo.setMsgDate(rs.getString("TM_DATE"));
					msgBo.setMsgKey(rs.getString("TM_KEY"));
					msgBo.setEmergencyCheck(rs.getInt("TM_EMERGENCY_SWITCH"));
					arrlist.add(msgBo);
				}
			}
		} catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.getAllMessage-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			System.out.println("TDSException DispatchDAO.getAllMessage-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return arrlist;
	}
	public static ArrayList<MessageBO> getIndividualMessage(String assoCode,String sender,String reciever,String timeZoneOffset){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<MessageBO> arrlist = new ArrayList<MessageBO>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		String query="SELECT *,DATE(CONVERT_TZ(TM_TIME ,'UTC', '"+timeZoneOffset+"')) AS TM_DATE, TIME_FORMAT(CONVERT_TZ(TM_TIME ,'UTC', '"+timeZoneOffset+"'),'%H:%i') AS TM_TIMEVALE,TM_TIME FROM TDS_TEXT_MESSAGE WHERE (TM_ENTERED_BY ='"+sender+"' AND TM_DRIVER_ID='"+reciever+"') OR (TM_ENTERED_BY ='"+reciever+"' AND TM_DRIVER_ID='"+sender+"') AND TM_ASSOCIATION_CODE ='"+assoCode+"' ORDER BY TM_TIME";
		ResultSet rs;
		try {
			m_pst = m_conn.prepareStatement(query);
			rs = m_pst.executeQuery();

			while(rs.next()){
				MessageBO msgBo = new MessageBO();
				msgBo.setDriverid(rs.getString("TM_DRIVER_ID"));
				msgBo.setMessage(rs.getString("TM_MESSAGE"));
				msgBo.setEnteredBy(rs.getString("TM_ENTERED_BY"));
				msgBo.setTime(rs.getString("TM_TIMEVALE"));
				msgBo.setMsgDate(rs.getString("TM_DATE"));
				msgBo.setMsgKey(rs.getString("TM_KEY"));
				msgBo.setEmergencyCheck(rs.getInt("TM_EMERGENCY_SWITCH"));
				arrlist.add(msgBo);
			}
		} catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.getAllMessage-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.getAllMessage-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return arrlist;
	}

	public static OpenRequestBO getOpenRequestStatus(String tripId,AdminRegistrationBO adminBo,String driverID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getOpenRequestStatus");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		OpenRequestBO opRequestBO= null;
		try {
			pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_TRIP_STATUS,OR_AMT,OR_TRIP_STATUS,OR_QUEUENO,OR_DRCABFLAG,OR_SMS_SENT,OR_DRIVERID,OR_DISPATCH_HISTORY_DRIVERS,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_SPLINS,OR_PAYACC,OR_PAYTYPE,"+
					"OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_SHARED_RIDE,OR_ELANDMARK,OR_EDSTATE,OR_NAME,OR_EDZIP,OR_REPEAT_GROUP,OR_PHONE,OR_STLATITUDE,OR_COMMENTS,OR_STLONGITUDE,OR_EDLATITUDE,OR_DRIVERID,OR_VEHICLE_NO,OR_EDLONGITUDE,OR_LANDMARK,OR_DRCABFLAG,OR_QUEUENO," +	"DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"' ),'%Y%m%d') as DATE,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"'),'%H%i') "+"as TIME, CONVERT_TZ(OR_SERVICEDATE,'UTC','"+adminBo.getTimeZone()+"') AS OFFSET_DATETIME FROM TDS_OPENREQUEST WHERE OR_TRIP_ID=? AND OR_ASSOCCODE=? AND OR_DRIVERID =?");
			pst.setString(1,tripId);
			pst.setString(2,adminBo.getAssociateCode());
			pst.setString(3,driverID);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				opRequestBO = new OpenRequestBO();
				opRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				opRequestBO.setPhone(rs.getString("OR_PHONE"));
				opRequestBO.setName(rs.getString("OR_NAME"));
				opRequestBO.setSdate(TDSValidation.getUserDateFormat(rs.getString("DATE")));
				opRequestBO.setShrs((rs.getString("TIME")));
				opRequestBO.setPaytype((rs.getString("OR_PAYTYPE")));
				opRequestBO.setAcct(rs.getString("OR_PAYACC"));
				opRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				opRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				opRequestBO.setComments(rs.getString("OR_COMMENTS"));
				opRequestBO.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				opRequestBO.setDriverid(rs.getString("OR_DRIVERID"));
				opRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				opRequestBO.setDrProfile(rs.getString("OR_DRCABFLAG"));
				opRequestBO.setStartTimeStamp(rs.getString("OFFSET_DATETIME"));
				opRequestBO.setChckTripStatus(rs.getInt("OR_TRIP_STATUS"));
				opRequestBO.setOR_SMS_SENT(rs.getString("OR_SMS_SENT"));
				opRequestBO.setHistoryOfDrivers(rs.getString("OR_DISPATCH_HISTORY_DRIVERS"));
				opRequestBO.setSadd1(rs.getString("OR_STADD1"));
				opRequestBO.setSadd2(rs.getString("OR_STADD2"));
				opRequestBO.setScity(rs.getString("OR_STCITY"));
				opRequestBO.setSstate(rs.getString("OR_STSTATE"));
				opRequestBO.setSzip(rs.getString("OR_STZIP"));
				opRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				opRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				opRequestBO.setEadd1(rs.getString("OR_EDADD1"));
				opRequestBO.setEadd2(rs.getString("OR_EDADD2"));
				opRequestBO.setEcity(rs.getString("OR_EDCITY"));
				opRequestBO.setEstate(rs.getString("OR_EDSTATE"));
				opRequestBO.setEzip(rs.getString("OR_EDZIP"));
				opRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				opRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				opRequestBO.setTypeOfRide(rs.getString("OR_SHARED_RIDE"));
				opRequestBO.setRepeatGroup(rs.getString("OR_REPEAT_GROUP"));
				opRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
				opRequestBO.setElandmark(rs.getString("OR_ELANDMARK"));
				opRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
			}}

		catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.getOpenRequestStatus-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException DispatchDAO.getOpenRequestStatus-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return opRequestBO;
	}
	public static DriverLocationBO getPhoneForDriverID(String driverID,String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getPhoneForDriverID");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		DriverLocationBO drbo = new DriverLocationBO();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_DRIVERLOCATION WHERE (DL_ASSOCCODE='"+assoCode+"' OR DL_MASTER_ASSOCCODE='"+assoCode+"') AND DL_DRIVERID='"+driverID+"'");
			cat.info(pst.toString());
			System.out.println("pst: "+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				drbo.setPhone(rs.getString("DL_PHONENO"));
				drbo.setProvider(rs.getString("DL_PROVIDER"));
				drbo.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				drbo.setJson_details(rs.getString("DL_JSON_REFERENCE")==null?"":rs.getString("DL_JSON_REFERENCE"));
			}
		}catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.getPhoneForDriverID-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException DispatchDAO.getPhoneForDriverID-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return drbo;
	}
	public static int getDriverUpdateTime(String driverID,String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getDriverUpdateTime");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int time=0;
		try {
			pst = con.prepareStatement("SELECT TIME_TO_SEC(TIMEDIFF(NOW(),DL_LASTUPDATE)) AS DIFF FROM TDS_DRIVERLOCATION WHERE DL_ASSOCCODE='"+assoCode+"' AND DL_DRIVERID='"+driverID+"' ");
			cat.info("TDS INFO DispatchDAO.getDriverUpdateTime Query"+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				time=rs.getInt("DIFF");
			}
		}catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.getDriverUpdateTime-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException DispatchDAO.getDriverUpdateTime-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return time;
	}
	public static ArrayList getLogsTime(String tripId,String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getPhoneForDriverID");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ArrayList al_list = new ArrayList();
		try {
			pst = con.prepareStatement("SELECT DATE_SUB(orl_time,INTERVAL '20' minute) as time,orl_changed_by FROM TDS_OPENREQUEST_LOGS where orl_tripid='"+tripId+"' and orl_assoccode='"+assoCode+"' and orl_reason = 'Job Accepted'");
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("orl_changed_by"));
				al_list.add(rs.getString("time"));
			}
			pst = con.prepareStatement("SELECT DATE_SUB(orl_time,INTERVAL '-20' minute) as time FROM TDS_OPENREQUEST_LOGS where orl_tripid='"+tripId+"' and orl_assoccode='"+assoCode+"' and orl_reason = 'Trip Ended'");
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("time"));
			}
		}catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.getLogsTime-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException DispatchDAO.getLogsTime-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}
	public static int getFlagJobCount(String assocode,String timeZone) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getFlagJobCount");
		ArrayList<String> jobCount = new ArrayList<String>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null, datePst = null;
		ResultSet m_rs = null, dateRs = null;
		int total =0;
		//String query="SELECT count(*) AS JOBCOUNT FROM TDS_OPENREQUEST_HISTORY WHERE  DATE(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeZone+"')) = DATE(CONVERT_TZ(NOW(),'UTC','"+timeZone+"')) AND ORH_ASSOCCODE='"+assocode+"' AND ORH_STADD1 = 'Flag Trip' ";
		String query="";
		try {
			
			datePst = m_conn.prepareStatement("SELECT DATE(CONVERT_TZ(NOW(),'UTC','"+timeZone+"')) AS Date");
			//System.out.println("date select:"+datePst.toString());
			dateRs = datePst.executeQuery();
			String date = "";
			if(dateRs.next()){
				date = dateRs.getString("Date");
			}
			
			query = "SELECT count(*) AS JOBCOUNT FROM TDS_OPENREQUEST_HISTORY WHERE ORH_SERVICEDATE BETWEEN CONVERT_TZ('"+date+" 00:00:01','"+timeZone+"','UTC') AND CONVERT_TZ('"+date+" 23:59:59','"+timeZone+"','UTC') AND ORH_ASSOCCODE='"+assocode+"' AND ORH_STADD1 = 'Flag Trip' ";
			
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			//System.out.println("falg job count:"+m_pst.toString());
			m_rs = m_pst.executeQuery();
			if(m_rs.next()) {
				total = Integer.parseInt(m_rs.getString("JOBCOUNT"));
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getFlagJobCount-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.getFlagJobCount-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return total;
	}
	public static DriverCabQueueBean getDriverForCabNumber(int basedOnDriverOrCab, String assoccode, String cabNO, String driverID, String status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.getDriverForCabNumber");

		PreparedStatement m_pst = null;
		ResultSet rs = null;
		DriverCabQueueBean dr = null;

		String finalSQL;
		String cabOrDriver;
		if(basedOnDriverOrCab==1){
			finalSQL = "SELECT * FROM TDS_DRIVERLOCATION DL  WHERE DL_VEHICLE_NO = ? AND (DL_ASSOCCODE=? OR DL_MASTER_ASSOCCODE='"+assoccode+"')  AND DL_SWITCH = ?";
			cabOrDriver = cabNO;
		} else{
			finalSQL = "SELECT * FROM TDS_DRIVERLOCATION DL  WHERE DL_DRIVERID = ? AND (DL_ASSOCCODE=? OR DL_MASTER_ASSOCCODE='"+assoccode+"')  AND DL_SWITCH = ?";
			cabOrDriver = driverID;
		}

		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();

		try {
			m_pst = m_conn.prepareStatement(finalSQL);
			m_pst.setString(1, cabOrDriver);
			m_pst.setString(2, assoccode);
			m_pst.setString(3, status);

			cat.info(m_pst.toString());
			//System.out.println("Get Driver/Cab--->"+m_pst);
			rs = m_pst.executeQuery();			

			if(rs.next()){
				dr = new DriverCabQueueBean();
				dr.setDriverid(rs.getString("DL_DRIVERID"));
				dr.setPhoneNo(rs.getString("DL_PHONENO"));
				dr.setProvider(rs.getString("DL_PROVIDER"));
				dr.setPushKey(rs.getString("DL_GOOG_REG_KEY"));
				dr.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
			}


		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getDriverForCabNumber-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.getDriverForCabNumber-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return dr;
	}
	public static boolean checkDriverAvailability(String assocode,  String driverID, String driverAlarm) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO DispatchDAO.checkDriverAvailability");
		ArrayList<String> jobCount = new ArrayList<String>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		boolean available =true;
		String query="SELECT OR_TRIP_ID FROM TDS_OPENREQUEST WHERE  DATE_SUB(OR_SERVICEDATE ,INTERVAL "+driverAlarm+" MINUTE) < NOW() AND OR_ASSOCCODE='"+assocode+"' AND OR_DRIVERID = '"+driverID+"' AND OR_TRIP_STATUS <> '"+TDSConstants.tripCompleted+"'";
		try{
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			if(m_rs.next()) {
				available = false;
			}
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.checkDriverAvailability-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.checkDriverAvailability-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return available;
	}
	public static int createMsgTemplate(String assocode,String msg,int category,String masterassocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		int resultValue=0;
		try {
			m_pst = m_conn.prepareStatement("INSERT INTO TDS_CANNED_MESSAGE (CM_MESSAGE,CM_CATEGORY,CM_ASSOCCODE,CM_MASTER_ASSOCCODE) VALUES (?,?,?,?)");
			m_pst.setString(1, msg);
			m_pst.setInt(2, category);
			m_pst.setString(3, assocode);
			m_pst.setString(4, masterassocode);
			resultValue = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.createMsgTemplate-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.createMsgTemplate-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}
	public static ArrayList<MessageBO>  getCannedMessages(String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<MessageBO> arrlist = new ArrayList<MessageBO>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		String query = "SELECT * FROM TDS_CANNED_MESSAGE WHERE CM_MASTER_ASSOCCODE ='"+assoCode+"'";
		try {
			m_pst = m_conn.prepareStatement(query);
			rs =  m_pst.executeQuery();
			while(rs.next()){
				MessageBO msgBo = new MessageBO();	
				msgBo.setMessage(rs.getString("CM_MESSAGE"));
				msgBo.setCategory(rs.getString("CM_CATEGORY"));
				msgBo.setMsgKey(rs.getString("CM_MSGNO"));
				arrlist.add(msgBo);
			}
		} catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.getCannedMessages-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.getCannedMessages-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return arrlist ;
	}
	public static int removeMessageTemplate(String assocode,String msgKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		int resultValue=0;
		try {
			m_pst = m_conn.prepareStatement("DELETE FROM TDS_CANNED_MESSAGE WHERE CM_ASSOCCODE ='"+assocode+"' AND CM_MSGNO ='"+msgKey+"'");
			resultValue = m_pst.executeUpdate();
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.removeMessageTemplate-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.removeMessageTemplate-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}
	public static int sendMessageToOperator(String message,String driver,String assoCode,int status,int emc){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs;
		int resultValue=0;
		try {
			m_pst = m_conn.prepareStatement("INSERT INTO TDS_TEXT_MESSAGE (TM_ASSOCIATION_CODE,TM_DRIVER_ID,TM_TIME,TM_MESSAGE,TM_ENTERED_BY,TM_LOGIN_SWITCH,TM_EMERGENCY_SWITCH,TM_SENT_TO,TM_USERTYPE) VALUES (?,?,NOW(),?,?,?,?,'Operator','1')",Statement.RETURN_GENERATED_KEYS);
			m_pst.setString(1,assoCode);
			m_pst.setString(2, "Operator");
			m_pst.setString(3, message);
			m_pst.setString(4, driver);
			m_pst.setInt(5, status);
			m_pst.setInt(6, emc);
			m_pst.executeUpdate();
			rs = m_pst.getGeneratedKeys();
			if(rs.next()){
				resultValue = rs.getInt(1);
			}
			cat.info(m_pst.toString());
			m_pst.close();
			m_conn.close();
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.sendMessageToOperator-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.sendMessageToOperator-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static String  getRouteNoUsingTripId(String assoCode,String trip){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<MessageBO> arrlist = new ArrayList<MessageBO>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		String routeNo ="";
		String query = "SELECT OR_ROUTE_NUMBER FROM TDS_OPENREQUEST WHERE OR_ASSOCCODE ='"+assoCode+"' AND OR_TRIP_ID =?";
		try {
			String tripid[]=trip.split(";");
			for(int i=0;i<tripid.length;i++){
				String tripId=tripid[i];
				m_pst = m_conn.prepareStatement(query);
				m_pst.setString(1, tripId);
				rs =  m_pst.executeQuery();
				if(rs.next()){
					routeNo = rs.getString("OR_ROUTE_NUMBER");
				}
			}
		} catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.getRouteNoUsingTripId-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.getRouteNoUsingTripId-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return routeNo ;
	}
	public static void updateJobByPassenger(String assoCode,String tripId,boolean status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		cat.info("Dispatch DAO Update Job For Stale Time");
		try {
			if(status){
				m_pst = m_conn.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS='"+TDSConstants.performingDispatchProcesses+"',OR_DISPATCH_START_TIME=NOW() WHERE OR_TRIP_ID='"+tripId+"' AND OR_ASSOCCODE='"+assoCode+"'");
				m_pst.executeUpdate();
			} else {
				m_pst = m_conn.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS='"+TDSConstants.companyCouldntService+"' WHERE OR_TRIP_ID='"+tripId+"' AND OR_ASSOCCODE='"+assoCode+"'");
				m_pst.executeUpdate();
				m_pst = m_conn.prepareStatement(TDSSQLConstants.moveOpenRequestToHisotry);
				m_pst.setString(1, tripId);
				m_pst.setString(2, assoCode);
				m_pst.executeUpdate();
				m_pst = m_conn.prepareStatement(TDSSQLConstants.deleteOpenRequest);
				m_pst.setString(1, tripId);
				m_pst.setString(2, assoCode);
				m_pst.executeUpdate();
			}
		} catch (Exception sqex) {
			cat.error(("TDSException DispatchDAO.updateJobByPassenger-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.updateJobByPassenger-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static int enterRestrictionForVoucher(String[] arrays,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		int resultValue=0;
		try {
			m_pst = m_conn.prepareStatement("DELETE FROM TDS_OPENREQUEST_RESTRICTION WHERE ORR_TRIP_ID='"+tripId+"'");
			m_pst.execute();
			for(int i=0;i<arrays.length;i=i+2){
				if(arrays[i+1]!=null && !arrays[i+1].equals("")){
					m_pst = m_conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_RESTRICTION (ORR_TRIP_ID,ORR_ALLOCATE_TYPE,ORR_DRIVERID) VALUES (?,?,?)");
					m_pst.setString(1,tripId);
					m_pst.setString(2, arrays[i]);
					m_pst.setString(3, arrays[i+1]);
					resultValue = m_pst.executeUpdate();
				}
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.enterRestrictionForVoucher-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.enterRestrictionForVoucher-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static ArrayList<DriverRestrictionBean> readRestrictionForTrip(String tripId,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs=null;
		ArrayList<DriverRestrictionBean> restrictedDrivers = new ArrayList<DriverRestrictionBean>();
		try {
			m_pst = m_conn.prepareStatement("SELECT ORR_ALLOCATE_TYPE,ORR_DRIVERID FROM TDS_OPENREQUEST left join TDS_OPENREQUEST_RESTRICTION ON OR_TRIP_ID=ORR_TRIP_ID WHERE OR_TRIP_ID='"+tripId+"' AND OR_MASTER_ASSOCCODE='"+assoccode+"' ORDER BY ORR_ALLOCATE_TYPE");
			rs = m_pst.executeQuery();
			while (rs.next()) {
				DriverRestrictionBean restriction = new DriverRestrictionBean();
				restriction.setStatus(rs.getInt("ORR_ALLOCATE_TYPE"));
				restriction.setDriverId(rs.getString("ORR_DRIVERID"));
				restrictedDrivers.add(restriction);
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.enterRestrictionForVoucher-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.enterRestrictionForVoucher-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return restrictedDrivers;
	}
	public static ArrayList<OpenRequestBO> getAllTrips(String masterAssoccode,String value,String timeZone){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs=null;
		ArrayList<OpenRequestBO> orList = new ArrayList<OpenRequestBO>();
		try {
			m_pst = m_conn.prepareStatement("SELECT *,DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','"+timeZone+"'),'%m/%d/%Y %H:%i') AS OR_SERVICEDATE_FORMATTED FROM TDS_OPENREQUEST  WHERE OR_MASTER_ASSOCCODE='"+masterAssoccode+"' AND( OR_DRIVERID like '%"+value+"%' OR OR_PHONE like'%"+value+"%' OR OR_NAME like'%"+value+"%' OR OR_TRIP_ID like'%"+value+"%' OR OR_VEHICLE_NO like'%"+value+"%' OR OR_STCITY like'%"+value+"%' OR OR_STSTATE like'%"+value+"%' OR OR_STZIP  like'%"+value+"%' OR OR_STADD1  like'%"+value+"%' OR OR_STADD2  like'%"+value+"%' OR OR_EDADD1  like'%"+value+"%' OR OR_EDADD2  like'%"+value+"%') LIMIT 0,5");
			rs = m_pst.executeQuery();
			while (rs.next()) {
				OpenRequestBO orBo = new OpenRequestBO();
				orBo.setTripid(rs.getString("OR_TRIP_ID"));
				orBo.setName(rs.getString("OR_NAME"));
				orBo.setSdate(rs.getString("OR_SERVICEDATE_FORMATTED"));
				orBo.setStatus(rs.getString("OR_TRIP_STATUS"));
				orBo.setSadd1(rs.getString("OR_STADD1")+","+rs.getString("OR_STCITY"));
				orBo.setEadd1(rs.getString("OR_EDADD1")+","+rs.getString("OR_EDCITY"));
				orBo.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				orBo.setDriverid(rs.getString("OR_DRIVERID"));
				orBo.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				orList.add(orBo);
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getAllTrips-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.enterRestrictionForVoucher-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return orList;
	}
	//	public static String getCompCode(String tripId,ArrayList<String> compaList){
	//		String fleets="";
	//		if(compaList != null){
	//			if(compaList.size() >0){
	//				fleets = "(";
	//				for(int j=0; j<compaList.size()-1;j++){
	//					fleets = fleets + "'" + compaList.get(j) + "',";
	//				}
	//				fleets = fleets + "'" + compaList.get(compaList.size()-1) + "')";
	//			} else {
	//				fleets = "('')";
	//			}
	//		} else {
	//			fleets = "('')";
	//		}
	//		return getCompCode(tripId, fleets);
	//	}
	public static String getCompCode(String tripId, String masterCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = new TDSConnection();
		Connection m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs,rs1=null;
		String assoccode="";
		try {
			m_pst = m_conn.prepareStatement("SELECT OR_ASSOCCODE  FROM TDS_OPENREQUEST WHERE OR_TRIP_ID= '"+tripId+"'");// OR_MASTER_ASSOCCODE='"+masterCode+"' AND  OR_TRIP_ID= '"+tripId+"'");
			rs = m_pst.executeQuery();
			if(rs.next()) {
				assoccode = rs.getString("OR_ASSOCCODE");
			} else {
				m_pst = m_conn.prepareStatement("SELECT ORH_ASSOCCODE FROM TDS_OPENREQUEST_HISTORY  WHERE ORH_TRIP_ID= '"+tripId+"'"); //ORH_MASTER_ASSOCCODE='"+masterCode+"' AND  ORH_TRIP_ID= '"+tripId+"'");
				rs1 = m_pst.executeQuery();
				if(rs1.next()) {
					assoccode = rs1.getString("ORH_ASSOCCODE");
				}
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getAllTrips-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.enterRestrictionForVoucher-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		return assoccode;
	}
	public static OpenRequestBO getAllTripsForDriver(ArrayList<String> assocode,String driverId,String cabNumber,String alarmTime,int type) {
		String fleets ="";
		if(assocode != null){
			if(assocode.size() >0){
				fleets = "(";
				for(int j=0; j<assocode.size()-1;j++){
					fleets = fleets + "'" + assocode.get(j) + "',";
				}
				fleets = fleets + "'" + assocode.get(assocode.size()-1) + "')";
			} else {
				fleets = "('')";
			}
		} else {
			fleets = "('')";
		}
		OpenRequestBO orBo = getAllTripsForDriver(fleets, driverId,cabNumber,alarmTime,type);
		return orBo;
	}

	public static OpenRequestBO getAllTripsForDriver(String assoCode,String driverId, String cabNum,String alarmTime,int type){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_tdsConn = new TDSConnection();
		Connection m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet rs=null;
		String extStatus="";
		if(type>1){
			extStatus = ",'"+TDSConstants.tripStarted+"'";
		}
		OpenRequestBO orBo = new OpenRequestBO();
		try {
			m_pst = m_conn.prepareStatement("SELECT OR_TRIP_ID FROM TDS_OPENREQUEST WHERE OR_ASSOCCODE IN "+assoCode+" AND OR_DRIVERID='"+driverId+"' AND OR_VEHICLE_NO='"+cabNum+"' AND OR_TRIP_STATUS IN ('"+TDSConstants.jobAllocated+"','"+TDSConstants.onRotueToPickup+"','"+TDSConstants.onSite+"','"+TDSConstants.driverReportedNoShow+"','"+TDSConstants.srAllocatedToCab+"'"+extStatus+") AND OR_DISPATCH_START_TIME <= DATE_SUB(NOW(),INTERVAL '-"+alarmTime+"' MINUTE)");
			System.out.println("Find Existing Job Of Driver When Meter On/Mobile Type--->"+type+"--->"+m_pst);
			rs = m_pst.executeQuery();
			if(rs.next()) {
				orBo.setTripid(rs.getString("OR_TRIP_ID"));
				orBo.setDriverid(driverId);
				orBo.setVehicleNo(cabNum);
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException DispatchDAO.getAllTrips-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//System.out.println("TDSException DispatchDAO.enterRestrictionForVoucher-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		return orBo;
	}

}