package com.tds.dao;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.charges.bean.ChargesBO;
import com.tds.cmp.bean.CashSettlement;
import com.tds.cmp.bean.DriverDisbursment;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.InvoiceBO;
import com.tds.tdsBO.PaymentSettledDetail;
import com.tds.tdsBO.VoucherBO;
import com.tds.util.CustomizedException;
import com.tds.util.TDSSQLConstants;
import com.common.util.TDSValidation;
@SuppressWarnings("deprecation")
public class FinanceDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(FinanceDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+FinanceDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	//Get the values of assocode oprid openCash and so on and insert it into the CashRegister table	

	public static boolean insertCashRegisterMaster(CashSettlement cspBean){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.insertCashRegisterMaster");
		boolean success = false;

		PreparedStatement csp_pst = null;

		TDSConnection dbcon = new TDSConnection();
		Connection csp_conn = dbcon.getConnection();
		try {
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.insertCashRegister); 
			csp_pst.setString(1, cspBean.getOperatorId());
			csp_pst.setString(2, cspBean.getAssociateCode());
			csp_pst.setInt(3, cspBean.getOpenCash());
			csp_pst.setInt(4, cspBean.getIntialChequeNo());
			csp_pst.setString(5, cspBean.getOpenedBy());
			csp_pst.setInt(6, cspBean.getClosingCash());
			csp_pst.setInt(7, cspBean.getClosingCheckNo());
			cat.info(csp_pst.toString());
			csp_pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.insertCashRegisterMaster-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException FinanceDAO.insertCashRegisterMaster" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}

	//Get the Operator disbursements to the drivers from registration action and insert it into the Cashregister Details table 	

	public static boolean insertCashRegisterDetail(DriverDisbursment disbursment, AdminRegistrationBO admBo,String payKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.insertCashRegisterDetail");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		//ResultSet rs=null;
		try {
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.insertCashRegisterDetail); 
			csp_pst.setString(1, admBo.getUid());
			csp_pst.setString(2, admBo.getAssociateCode());
			csp_pst.setString(3, disbursment.getDriverid());
			dbcon = new TDSConnection();
			csp_pst.setString(4, disbursment.getDescr());
			csp_pst.setString(5, disbursment.getCheck_cash());
			csp_pst.setString(6, disbursment.getCheckno());
			csp_pst.setString(7, disbursment.getSet_amount());
			csp_pst.setString(8, payKey);
			if(disbursment.getSet_amount().contains("-")){
				csp_pst.setString(9, disbursment.getCheckno());
			} else {
				csp_pst.setString(9, "");
			}					
			cat.info(csp_pst.toString());
			csp_pst.execute();
			if(disbursment.getSet_amount().contains("-")){
				csp_pst = csp_conn.prepareStatement(TDSSQLConstants.insertReceivedChequeDetail); 
				csp_pst.setString(1, disbursment.getDriverid());
				csp_pst.setString(2, admBo.getAssociateCode());
				csp_pst.setString(3, disbursment.getCheckno());
				csp_pst.setString(4, disbursment.getSet_amount());
				csp_pst.setString(5, disbursment.getDescr());
				csp_pst.setString(6, payKey);
				csp_pst.setString(7, admBo.getUid());
				csp_pst.execute();

			}


			success = true;
		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.insertCashRegisterDetail-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException FinanceDAO.insertCashRegisterDetail" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}

	//Update the cashRegister Detail table with the disbursment amount to the operator's open cash and the current check number

	public static boolean updateCashRegisterMaster(DriverDisbursment disbursment, String assocode,String oprid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.updateCashRegisterMaster");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		//ResultSet rs=null;
		String query ="";
		if(disbursment.getCheck_cash().equals("2")){
			query="CRM_CLOSING_CASH_AMT = (CRM_CLOSING_CASH_AMT - "+disbursment.getSet_amount()+")";
		}
		else if(!disbursment.getSet_amount().contains("-") && !(disbursment.getCheck_cash().equals("2"))){
			query = "CRM_CLOSING_CHECK_NO = (CRM_CLOSING_CHECK_NO + 1) ";
		}
		else if(disbursment.getSet_amount().contains("-") && !(disbursment.getCheck_cash().equals("2"))){
			query = "CRM_RECEIVED_CHEQUES = (CRM_RECEIVED_CHEQUES + 1) ";
		}
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement("UPDATE TDS_CASH_REGISTER_MASTER SET"+query+"WHERE CRM_OPERATORID=? AND CRM_ASSOCCODE=?");
			//			csp_pst.setString(1, disbursment.getSet_amount());
			csp_pst.setString(1, oprid);
			csp_pst.setString(2, assocode);
			cat.info(csp_pst.toString());
			csp_pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.updateCashRegisterMaster-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException FinanceDAO.updateCashRegisterMaster" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}

	//Close the register for the operator if it is opened
	//When closing send all the contents of master table to the history and similarly the detail table contents to the details history 	

	public static boolean closeCashRegisterMaster(CashSettlement cspBean){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.closeRegister");
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		//ResultSet rs=null;
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement("DELETE FROM TDS_CASH_REGISTER_MASTER WHERE CRM_ASSOCCODE = ? AND CRM_OPERATORID=?");
			csp_pst.setString(1, cspBean.getAssociateCode());
			csp_pst.setString(2, cspBean.getOperatorId());
			csp_pst.execute();	
			csp_pst = csp_conn.prepareStatement("INSERT INTO TDS_CASH_REGISTER_DETAIL_HISTORY SELECT * FROM TDS_CASH_REGISTER_DETAIL WHERE CRD_ASSOCCODE = ? AND CRD_OPERATORID=?");
			csp_pst.setString(1, cspBean.getAssociateCode());
			csp_pst.setString(2, cspBean.getOperatorId());
			csp_pst.execute();	
			csp_pst = csp_conn.prepareStatement("DELETE FROM TDS_CASH_REGISTER_DETAIL WHERE CRD_ASSOCCODE = ? AND CRD_OPERATORID=?");
			csp_pst.setString(1, cspBean.getAssociateCode());
			csp_pst.setString(2, cspBean.getOperatorId());
			csp_pst.execute();	
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.closeCashRegister); 
			csp_pst.setString(1, cspBean.getOperatorId());
			csp_pst.setString(2, cspBean.getAssociateCode());
			csp_pst.setInt(3, cspBean.getOpenCash());
			csp_pst.setInt(4, cspBean.getIntialChequeNo());
			csp_pst.setString(5, cspBean.getClosedBy());
			csp_pst.setInt(6, cspBean.getClosingCash());
			csp_pst.setInt(7, cspBean.getClosingCheckNo());
			csp_pst.setString(8,cspBean.getReceivedCheques());
			cat.info(csp_pst.toString());
			csp_pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.closeRegister-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException FinanceDAO.closeRegister-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}

	//Get the details of open cash, close cash and checque numbers and show it to the administrator when closing the register

	public static CashSettlement readCashRegisterMaster(String associationCode, String operatorID){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.readCashRegisterMaster");
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		CashSettlement cashSettlementBean = new CashSettlement();
		cashSettlementBean.setRegisterOpen(false);
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();

			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.getCashRegister); 
			csp_pst.setString(1, associationCode);
			csp_pst.setString(2, operatorID);
			cat.info(csp_pst.toString());
			rs = csp_pst.executeQuery();
			if(rs.next()){
				cashSettlementBean.setRegisterOpen(true);
				cashSettlementBean.setOpenCash(rs.getInt("CRM_OPEN_CASH_AMT"));
				cashSettlementBean.setIntialChequeNo(rs.getInt("CRM_INTIAL_CHECK_NO"));	
				cashSettlementBean.setClosingCash(rs.getInt("CRM_CLOSING_CASH_AMT"));
				cashSettlementBean.setClosingCheckNo(rs.getInt("CRM_CLOSING_CHECK_NO"));
				cashSettlementBean.setReceivedCheques(rs.getString("CRM_RECEIVED_CHEQUES"));
				readSumOfRegisterDetails(cashSettlementBean);
			}

		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.readCashRegisterMaster-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException FinanceDAO.readCashRegisterMaster-->" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return cashSettlementBean;
	}
	public static void readSumOfRegisterDetails(CashSettlement cashSettlementBean ){
	}

	//Get the details of operator to whom he disbursed the money/cheque and collected the money/cheque from driver

	public static ArrayList<DriverDisbursment> FinanceDetails(String associationCode, String operatorID){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.readFinanceDetails");
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		ArrayList<DriverDisbursment> disbursementList = new ArrayList<DriverDisbursment> (); 
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.getCashRegisterDetail); 
			csp_pst.setString(1, operatorID);
			csp_pst.setString(2, associationCode);
			cat.info(csp_pst.toString());
			rs = csp_pst.executeQuery();
			while (rs.next()){
				DriverDisbursment driverDisbursmentBean = new DriverDisbursment();
				driverDisbursmentBean.setDriverid(rs.getString("CRD_DRIVERID"));
				driverDisbursmentBean.setDescr(rs.getString("CRD_DISBURSEMENT_DISCRIPTION"));
				driverDisbursmentBean.setCheckno(rs.getString("CRD_CHEQUE_NO"));
				driverDisbursmentBean.setAmount(rs.getString("CRD_DISBURSEMENT_AMT"));
				driverDisbursmentBean.setPayment_id(rs.getString("CRD_DISBURSEMENT_PAY_ID"));
				driverDisbursmentBean.setReceivedCheque(rs.getString("CRD_RECEIVED_CHEQUE_NO"));
				disbursementList.add(driverDisbursmentBean);
			}

		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.readFinanceDetails" + sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException FinanceDAO.readFinanceDetails" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return disbursementList;
	}

	public static ArrayList<DriverDisbursment> getChequeDetails(DriverDisbursment disbursement,String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getChequeDetails");
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		//DriverDisbursment driverDisbursmentBean = new DriverDisbursment();
		ArrayList<DriverDisbursment> chequeList = new ArrayList<DriverDisbursment>(); 
		String query="";
		String query1="0";
		if(!(disbursement.getDate()).equals("")){
			query = " AND DATE_FORMAT(RCD_DISBURSEMENT_TIME,'%Y%m%d')='" +TDSValidation.getDBdateFormat(disbursement.getDate())+"'";
		}
		if(!(disbursement.getDriverid()).equals("")){
			query = query + " AND RCD_DRIVERID='"+disbursement.getDriverid()+"'";
		}

		if(!disbursement.getChequeBounced().equals("") && !disbursement.getChequeReceived().equals("")){
			query1 = " AND RCD_CHEQUE_DETAILS in (" + disbursement.getChequeReceived() + ", " +" " + disbursement.getChequeBounced() + ")";
		} else if(!(disbursement.getChequeReceived()).equals("")){
			query1 = " AND RCD_CHEQUE_DETAILS ='" + disbursement.getChequeReceived() +"'";
		} else if(!(disbursement.getChequeBounced().equals(""))){
			query1 = " AND RCD_CHEQUE_DETAILS ='" + disbursement.getChequeBounced() + "'";
		} else{
			query1 = " AND RCD_CHEQUE_DETAILS ='" + query1 + "'";
		}
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.getChequeDetail+query+query1); 
			csp_pst.setString(1, associationCode);
			cat.info(csp_pst.toString());
			rs = csp_pst.executeQuery();
			while (rs.next()){
				DriverDisbursment disbursementbean = new DriverDisbursment();
				disbursementbean.setDriverid(rs.getString("RCD_DRIVERID"));
				disbursementbean.setCheckno(rs.getString("RCD_CHEQUE_NO"));
				disbursementbean.setAmount(rs.getString("RCD_DISBURSEMENT_AMT"));
				disbursementbean.setPayment_id(rs.getString("RCD_DISBURSEMENT_PAY_ID"));
				disbursementbean.setChequeStatus(rs.getString("RCD_CHEQUE_DETAILS"));
				chequeList.add(disbursementbean);
			}
		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.getChequeDetails--->" +sqex.getMessage());
			cat.error(csp_pst.toString());
	//		System.out.println("TDSException FinanceDAO.getChequeDetails--->" + sqex.getMessage());
			sqex.getStackTrace();
		} finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return chequeList;
	}
	public static int updateChecksAsReceivedOrBounced(ArrayList<DriverDisbursment> payIDsToMarkBounced,String associationCode, int receivedBouncedInidcator){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.updateChecksAsreceived");
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		//ResultSet rs = null;
		String inClause = "";
		int x=0;
		for(int i = 0; i<payIDsToMarkBounced.size()-1; i++){
			inClause = (inClause + "'" + payIDsToMarkBounced.get(i).getPayment_id() + "', ");
		}
		inClause = (inClause + "'" + payIDsToMarkBounced.get(payIDsToMarkBounced.size()-1).getPayment_id() + "' ");


		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst=csp_conn.prepareStatement("UPDATE TDS_RECEIVED_CHEQUE_DETAIL SET RCD_CHEQUE_DETAILS="+ receivedBouncedInidcator+" WHERE RCD_ASSOCCODE=? AND RCD_DISBURSEMENT_PAY_ID in (" +inClause+ ")");
			csp_pst.setString(1, associationCode);
			cat.info(csp_pst.toString());
			csp_pst.execute();
			x=1;
		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.updateChecksAsreceived--->"+sqex.getMessage());
			cat.error(csp_pst.toString());
		//	System.out.println("TDSException FinanceDAO.updateChecksAsreceived--->" + sqex.getMessage());
			sqex.getStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return x;
	}
	public static ArrayList<DriverDisbursment> readValuesByPayID(ArrayList<DriverDisbursment> payIDsToMarkBounced,String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.readValuesByPayID");
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		String inClause="";
		String query="";
		ArrayList<DriverDisbursment> receivedChequeList = new ArrayList<DriverDisbursment>(); 
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			if(payIDsToMarkBounced.size()>0){
				for(int i = 0; i<payIDsToMarkBounced.size()-1; i++){
					inClause = (inClause + "'" + payIDsToMarkBounced.get(i).getPayment_id() + "', ");
				}
				inClause = (inClause + "'" + payIDsToMarkBounced.get(payIDsToMarkBounced.size()-1).getPayment_id() + "' ");
				query = " AND RCD_DISBURSEMENT_PAY_ID in (" +inClause+ ")";
			}
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.getReceivedChequeDetail+query); 
			csp_pst.setString(1, associationCode);
			cat.info(csp_pst.toString());
			rs = csp_pst.executeQuery();
			while (rs.next()){
				DriverDisbursment disbursementbean = new DriverDisbursment();
				disbursementbean.setDriverid(rs.getString("RCD_DRIVERID"));
				disbursementbean.setCheckno(rs.getString("RCD_CHEQUE_NO"));
				disbursementbean.setAmount(rs.getString("RCD_DISBURSEMENT_AMT"));
				disbursementbean.setPayment_id(rs.getString("RCD_DISBURSEMENT_PAY_ID"));
				disbursementbean.setChequeStatus(rs.getString("RCD_CHEQUE_DETAILS"));
				disbursementbean.setDate(rs.getString("RCD_DISBURSEMENT_TIME"));
				disbursementbean.setDescr(rs.getString("RCD_DISBURSEMENT_DISCRIPTION"));
				receivedChequeList.add(disbursementbean);
			}
		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.readValuesByPayID--->"+sqex.getMessage());
			cat.error(csp_pst.toString());
		//	System.out.println("TDSException FinanceDAO.readValuesByPayID--->" + sqex.getMessage());
			sqex.getStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return receivedChequeList;
	}
	public static ArrayList<PaymentSettledDetail> getVoucherDetails(AdminRegistrationBO adminBO,PaymentSettledDetail invoiceBo,int source,String fDate,String tDate){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getVoucherDetails");
		PreparedStatement pst = null;
		ArrayList<PaymentSettledDetail> voucher =new ArrayList<PaymentSettledDetail>();
		ResultSet rs=null;

		PaymentSettledDetail invoiceBO = null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String query ="";
		String queryApend ="";
		if(fDate!=""&&fDate!=null){
			String[] fromD = fDate.split("/");
			fDate = fromD[2] +"-"+  fromD[0]+"-"+ fromD[1];
		}
		if(tDate!=""&&tDate!=null){
			String[] toD = tDate.split("/");
			tDate = toD[2] +"-"+  toD[0]+"-"+ toD[1];
		}
		if(fDate!=null&&fDate!=""&&tDate!=null&&tDate!=""){
			queryApend =" AND (date_format(CONVERT_TZ(VD_PROCESSING_DATE_TIME, 'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d') BETWEEN '"+fDate+"' AND '"+tDate+"'"+")";
		}
		if(source==1){
			query = "SELECT *,date_format(CONVERT_TZ(VD_EXPIRY_DATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as VD_EXPIRYDATE,date_format(CONVERT_TZ(VD_PROCESSING_DATE_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as VD_PROCESSING_DATE1 from TDS_VOUCHER_DETAIL WHERE (VD_INVOICE_NO IS NULL OR VD_INVOICE_NO='"+invoiceBo.getInvoiceNumber()+"'OR VD_INVOICE_NO='') and  VD_ASSOC_CODE= '"+adminBO.getAssociateCode()+"'"+queryApend;
		}else{
			query = "SELECT *,date_format(CONVERT_TZ(VD_EXPIRY_DATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as VD_EXPIRYDATE,date_format(CONVERT_TZ(VD_PROCESSING_DATE_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as VD_PROCESSING_DATE1 from TDS_VOUCHER_DETAIL WHERE (VD_INVOICE_NO='"+invoiceBo.getInvoiceNumber()+"') and  VD_ASSOC_CODE= '"+adminBO.getAssociateCode()+"'";
		}

		if(invoiceBo.getVoucherNo()!=null&&invoiceBo.getVoucherNo()!=""){
			query =query + " AND VD_VOUCHERNO ='"+invoiceBo.getVoucherNo()+"'";
		}
		if(invoiceBo.getCostCenter()!=null&&invoiceBo.getCostCenter()!=""){
			query =query + " AND VD_COSTCENTER ='"+invoiceBo.getCostCenter()+"'";
		}
		if(invoiceBo.getCompanyCode()!=null&&invoiceBo.getCompanyCode()!=""){
			query =query + " AND VD_COMPANY_CODE ='"+invoiceBo.getCompanyCode()+"'";
		}
		if(invoiceBo.getVerified()==3){
			query =query + " AND VD_VERIFIED = 0";
		}else if(invoiceBo.getVerified()==2){
			query =query + " AND VD_VERIFIED IN ('1','4')";
		}else if(invoiceBo.getVerified()!=0){
			query =query + " AND VD_VERIFIED ='"+invoiceBo.getVerified()+"'";
		} 
		query =query + " ORDER BY VD_TRIP_ID";
		try{
			pst=con.prepareStatement(query);
			//System.out.println("SearchVouchers : "+pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				invoiceBO = new PaymentSettledDetail();
				invoiceBO.setInvoiceNumber(rs.getInt("VD_INVOICE_NO"));
				invoiceBO.setTransId(rs.getString("VD_TXN_ID"));
				invoiceBO.setCostCenter(rs.getString("VD_COSTCENTER"));
				invoiceBO.setRiderName(rs.getString("VD_RIDER_NAME"));
				invoiceBO.setVoucherNo(rs.getString("VD_VOUCHERNO"));
				invoiceBO.setAssociationCode(rs.getString("VD_ASSOC_CODE"));
				invoiceBO.setAmount(rs.getString("VD_TOTAL"));
				invoiceBO.setExpDate((rs.getString("VD_EXPIRYDATE")));
				invoiceBO.setDescription(rs.getString("VD_DESCR"));
				invoiceBO.setCompanyCode(rs.getString("VD_COMPANY_CODE"));
				invoiceBO.setContact(rs.getString("VD_CONTACT"));
				invoiceBO.setDelayDays(rs.getString("VD_DELAYDAYS"));
				invoiceBO.setFreq(rs.getString("VD_FREQUENCY"));
				invoiceBO.setTripId(rs.getString("VD_TRIP_ID"));
				invoiceBO.setDriverId(rs.getString("VD_DRIVER_ID"));
				invoiceBO.setPaymentReceivedStatus(rs.getInt("VD_PAYMENT_RECEIVED_STATUS"));
				invoiceBO.setVerified(rs.getInt("VD_VERIFIED"));
				invoiceBO.setDriverPayedStatus(rs.getInt("VD_DRIVER_PMT_STATUS"));
				invoiceBO.setVoucherFormat(rs.getString("VD_IMAGE_TYPE"));
				invoiceBO.setCc_Percent_processed((rs.getString("VD_CC_PERCENT_PROCESSED")!=null && !rs.getString("VD_CC_PERCENT_PROCESSED").equals(""))?rs.getInt("VD_CC_PERCENT_PROCESSED"):0);
				invoiceBO.setCc_Prcnt_Amt((rs.getString("VD_CC_PRCNT_AMT")!=null && !rs.getString("VD_CC_PRCNT_AMT").equals(""))?rs.getString("VD_CC_PRCNT_AMT"):"0");
				invoiceBO.setServiceDate(rs.getString("VD_PROCESSING_DATE1"));
				voucher.add(invoiceBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getVoucherDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.getVoucherDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucher;
	}

	public static void updateInvoice(AdminRegistrationBO adminBo,String[] voucherNo,int total,String chequeNumber,int paymentType){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.updateInvoice");
		TDSConnection dbcon = null;

		PreparedStatement pst = null;
		String query="UPDATE TDS_VOUCHER_INVOICE_SUMMARY SET VIS_PAYMENT_STATUS='"+paymentType+"',VIS_CHEQUE_NUMBER='"+chequeNumber+"' WHERE VIS_ASSOCCODE='"+adminBo.getAssociateCode()+"' AND VIS_INVOICE_NUMBER IN( ";
		for(int i=0;i<total-1;i++){
			if(voucherNo[i]!=null){
				query=query+voucherNo[i]+",";
			}
		}
		for(int i= total-1;i<total;i++){
			if(voucherNo[i]!=null){
				query=query+voucherNo[i]+")";
			}else{
				query=query+0+")";
			}
		}
		dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			pst.execute();
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.updateInvoice-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.updateInvoice-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static void updateVoucher(AdminRegistrationBO adminBo,int invoiceNo,ArrayList<PaymentSettledDetail> voucher){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.updateVoucher");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		String query="UPDATE TDS_VOUCHER_DETAIL SET VD_INVOICE_NO ='"+invoiceNo+"' WHERE VD_ASSOC_CODE='"+adminBo.getAssociateCode()+"' AND VD_VOUCHERNO IN( ";
		for(int i=0;i<voucher.size()-1;i++){
			query=query+voucher.get(i).getVoucherNo()+",";
		}
		query=query+voucher.get(voucher.size()-1).getVoucherNo()+")";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			pst.execute();
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.updateVoucher-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.updateVoucher-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	/*	public static ArrayList<PaymentSettledDetail> getVoucherByIdDelete(AdminRegistrationBO adminBO,String[] setID){
		cat.info("TDS INFO FinanceDAO.getVoucherById");
		TDSConnection dbcon = null;
				PaymentSettledDetail invoiceBO = null;
		PreparedStatement pst = null;
		ArrayList<PaymentSettledDetail> voucher =new ArrayList<PaymentSettledDetail>();
		ResultSet rs=null;
		String query = "Select VSD_VOUCHERNO,SUM(VSD_AMOUNT) as total,count(*) as countrow from("; 
		query +="SELECT VSD_TRANS_ID,VSD_COSTCENTER,VSD_RIDERNAME,VSD_VOUCHERNO," +
		" VSD_ASSOCCODE,VSD_CCODE,VSD_AMOUNT,VSD_DESCR,VSD_CONTACT,VSD_DELAYDAYS,VSD_FREQ,date_format(VSD_EXPIRYDATE,'%m/%d/%Y') as VSD_EXPIRYDATE FROM TDS_VOUCHER_SETELED_DETAIL WHERE VSD_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND VSD_TRANS_ID IN(";
		if(setID.length -1 >= 0)
			query += setID[setID.length-1];

		for(int i=0;i<setID.length-1;i++){
			query +=  ","+setID[i] ;
		}
		query += ")";

		query += "UNION ";

		query +="SELECT VW_TRANS_ID,VW_COSTCENTER,VW_RIDERNAME,VW_VOUCHERNO," +
		" VW_ASSOCCODE,VW_CCODE,VW_AMOUNT,VW_DESCR,VW_CONTACT,VW_DELAYDAYS,VW_FREQ,date_format(VW_EXPIRYDATE,'%m/%d/%Y') as VW_EXPIRYDATE" +
		" FROM TDS_VOUCHER_WORK WHERE VW_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND VW_TRANS_ID IN (";

		if(setID.length -1 >= 0)
			query += setID[setID.length-1];

		for(int i=0;i<setID.length-1;i++){
			query +=  ","+setID[i] ;
		}
		query += ")) as A group by VSD_VOUCHERNO";
		dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			pst=con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				invoiceBO = new PaymentSettledDetail();
				invoiceBO.setVoucherNo(rs.getString("VSD_VOUCHERNO"));
				invoiceBO.setTotal(rs.getString("total"));
				invoiceBO.setTotalcount(rs.getString("countrow"));
				voucher.add(invoiceBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getVoucherById-->"+sqex.getMessage());
			System.out.println("TDSException FinanceDAO.getVoucherById-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		return voucher;
	}
	 */
	public static int insertInvoice(AdminRegistrationBO adminBO,PaymentSettledDetail voucher,String[] setID) throws Exception{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int invoiceNo = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		int updateQuery1=0;
		int updateQuery2=0;
		int total =0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		con.setAutoCommit(false);
		String query = "INSERT INTO TDS_VOUCHER_INVOICE_SUMMARY(VIS_PROCESS_DATE,VIS_TOTAL_NO_OF_VOUCHERS,VIS_TOTAL_AMOUNT,VIS_ASSOCCODE,VIS_VOUCHER_NO) VALUES(NOW(),?,?,?,?) ";
		synchronized (con) {
			try{
				pst=con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
				pst.setString(1, voucher.getTotalcount());
				pst.setString(2, voucher.getTotal());
				pst.setString(3, adminBO.getAssociateCode());
				pst.setString(4, voucher.getVoucherNo());
				cat.info(pst.toString());
				//System.out.println("generate Invoice:"+pst.toString());
				pst.executeUpdate();
				rs = pst.getGeneratedKeys(); 
				if(rs != null && rs.next()) {
					query="UPDATE TDS_VOUCHER_DETAIL SET VD_INVOICE_NO ='"+rs.getInt(1)+"' WHERE VD_ASSOC_CODE='"+adminBO.getAssociateCode()+"' AND VD_TXN_ID in (";
					invoiceNo=rs.getInt(1);
					
					for(int j=0;j<setID.length-1;j++){
						query +=  setID[j]+"," ;
					}
					query += setID[setID.length-1] + ")";
					pst=con.prepareStatement(query);
					cat.info(pst.toString());
					//System.out.println("Update Invoice Number to TDS_VOUCHER_DETAIL: "+pst.toString());
					updateQuery2=pst.executeUpdate();
				} 
				total = updateQuery1+updateQuery2;
				if(total==setID.length){
					con.commit();
					//throw new CustomizedException("Invoice Generated SuccessFully,InVoice No:"+invoiceNo);
				}else{
					con.rollback();
					throw new CustomizedException("Invoice can't Generated,the updated transactions are not equal to the total No. of Vouchers");
				}
			}catch(SQLException sqex) {
				cat.error("TDSException FinanceDAO.insertInvoice-->"+sqex.getMessage());
				cat.error(pst.toString());
			//	System.out.println("TDSException FinanceDAO.insertInvoice-->"+sqex.getMessage());
				sqex.printStackTrace();
				
			} finally {
				con.setAutoCommit(true);
				dbcon.closeConnection();
			}
			cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

				return invoiceNo;
			}
		}

	
	public static void update_VD_CCSettled_OnInvoiceGenerate(AdminRegistrationBO adminBO,String[] setID) throws Exception{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		String query="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		
		try{
			query="UPDATE TDS_VOUCHER_DETAIL SET VD_PAYMENT_RECEIVED_STATUS ='2' WHERE VD_ASSOC_CODE='"+adminBO.getAssociateCode()+"' AND VD_TXN_ID in (";
			for(int j=0;j<setID.length-1;j++){
				query +=  setID[j]+"," ;
			}
			query += setID[setID.length-1] + ")";
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			System.out.println("Update payment status to TDS_VOUCHER_DETAIL: "+pst.toString());
			pst.executeUpdate();
			
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.insertInvoice-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.insertInvoice-->"+sqex.getMessage());
			sqex.printStackTrace();
			
		} finally {
			con.setAutoCommit(true);
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		
		}

	public static void update_VD_CC_PRCNT_AMT_InvoiceGenerate(AdminRegistrationBO adminBO,String txnId, String totalAmt, String processedAmt) throws Exception{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		String query="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		
		try{
			query="UPDATE TDS_VOUCHER_DETAIL SET VD_CC_PERCENT_PROCESSED='1',VD_CC_PRCNT_AMT='"+processedAmt+"',VD_TOTAL='"+totalAmt+"' WHERE VD_TXN_ID='"+txnId+"' ";
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			System.out.println("update_VD_CC_PRCNT_AMT_InvoiceGenerate: "+pst.toString());
			pst.executeUpdate();
			
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.update_VD_CC_PRCNT_AMT_InvoiceGenerate-->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.printStackTrace();
		} finally {
			con.setAutoCommit(true);
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		
		}
	
	public static int insertprepaidcard(String[] cardno,String associatecode,String[] driverid,String[] pinno,String[] amount,int size) throws Exception{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			for(int i=1;i<=size;i++){
				if(!cardno[i].equals("") && !driverid[i].equals("") && !amount[i].equals("")){
					pst= con.prepareStatement("INSERT INTO TDS_PREPAID_CARDS(PC_CARD_NUMBER	,PC_ASSOCIATION_CODE,PC_ALLOCATED_TO,PC_PIN,PC_AMOUNT)VALUES(?,?,?,?,?)");
					pst.setString(1, cardno[i]);
					pst.setString(2,associatecode);
					pst.setString(3,driverid[i]);
					pst.setString(4,pinno[i]);
					pst.setString(5,amount[i]);
					result=pst.executeUpdate();
				}
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException FinanceDAO.insertprepaidcard--->"+sqex.getMessage());
			cat.error("TDSException FinanceDAO.insertprepaidcard--->"+sqex.getMessage());

			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

       return result;
	}

	public static ArrayList<InvoiceBO> getInvoiceDetailsDelete1(AdminRegistrationBO adminBO,InvoiceBO invoiceBo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getInvoiceDetails");
		TDSConnection dbcon = null;
		PreparedStatement pst = null;
		ArrayList<InvoiceBO> voucher =new ArrayList<InvoiceBO>();
		ResultSet rs=null;
		String query="";
		dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		query = "SELECT * FROM TDS_VOUCHER_INVOICE_SUMMARY WHERE VIS_ASSOCCODE='"+ adminBO.getAssociateCode()+"' AND VIS_PAYMENT_STATUS =0";
		if(invoiceBo.getVoucherNo()!=null && invoiceBo.getVoucherNo()!="" ){
			query =query + " AND VIS_VOUCHERNO ='"+invoiceBo.getVoucherNo()+"'";
		}
		try{
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				InvoiceBO invoiceBO = new InvoiceBO();
				invoiceBO.setInvoiceNumber(rs.getInt("VIS_INVOICE_NUMBER"));
				invoiceBO.setProcessDate(rs.getString("VIS_PROCESS_DATE"));
				invoiceBO.setPaymentNo(rs.getString("VIS_PAYMENT_NO"));
				invoiceBO.setChequeNumber(rs.getString("VIS_CHEQUE_NUMBER"));
				invoiceBO.setPaymentRecievedDate(rs.getString("VIS_PAYMENT_RECEIVED_DATE"));
				invoiceBO.setDescription(rs.getString("VIS_DESC"));
				invoiceBO.setTotalNoOfVoucher(rs.getString("VIS_TOTAL_NO_OF_VOUCHERS"));
				invoiceBO.setTotalAmount(rs.getString("VIS_TOTAL_AMOUNT"));
				voucher.add(invoiceBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getInvoiceDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.getInvoiceDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucher;
	}

	public static PaymentSettledDetail getVoucherById(AdminRegistrationBO adminBO,String[] setID){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getVoucherById");
		TDSConnection dbcon = null;
		PaymentSettledDetail invoiceBO = new PaymentSettledDetail();
		PreparedStatement pst = null;
		ArrayList<PaymentSettledDetail> voucher =new ArrayList<PaymentSettledDetail>();
		ResultSet rs=null;
		String query = "SELECT VD_TXN_ID,VD_COSTCENTER,VC_EMAIL_ADDRESS,VD_RIDER_NAME,VD_VOUCHERNO," +
		" VD_ASSOC_CODE,VD_COMPANY_CODE,SUM(VD_TOTAL) as total,count(*) as countrow,VD_DESCR,VD_CONTACT,VD_DELAYDAYS,VD_FREQUENCY,date_format(VD_EXPIRY_DATE,'%m/%d/%Y') as VD_EXPIRYDATE" +
		" FROM TDS_VOUCHER_DETAIL LEFT JOIN TDS_GOVT_VOUCHER ON VC_VOUCHERNO=VD_VOUCHERNO WHERE VD_ASSOC_CODE='"+adminBO.getAssociateCode()+"' AND VD_TXN_ID IN (";
		if(setID.length -1 >= 0)
			query += setID[setID.length-1];

		for(int i=0;i<setID.length-1;i++){
			query +=  ","+setID[i] ;
		}
		query += ")";
		dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			pst=con.prepareStatement(query);
			rs=pst.executeQuery();
			if(rs.next()){
				invoiceBO.setVoucherNo(rs.getString("VD_VOUCHERNO"));
				invoiceBO.setEmailAddress(rs.getString("VC_EMAIL_ADDRESS"));
				invoiceBO.setTotal(rs.getString("total"));
				invoiceBO.setTotalcount(rs.getString("countrow"));
				voucher.add(invoiceBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getVoucherById-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.getVoucherById-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return invoiceBO;
	}
	public static ArrayList<PaymentSettledDetail> getVoucherByVoucher(AdminRegistrationBO adminBO,String vouchers){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getVoucherById");
		TDSConnection dbcon = null;
		PreparedStatement pst = null;
		ArrayList<PaymentSettledDetail> voucher =new ArrayList<PaymentSettledDetail>();
		ResultSet rs=null;
		String query = "SELECT VD_TXN_ID,VD_COSTCENTER,VD_RIDER_NAME,VD_VOUCHERNO,VD_ASSOC_CODE,VD_COMPANY_CODE,VD_TOTAL,VD_DESCR,VD_CONTACT,VD_DELAYDAYS,VD_FREQUENCY,date_format(VD_EXPIRY_DATE,'%m/%d/%Y') as VD_EXPIRYDATE" +
		" FROM TDS_VOUCHER_DETAIL WHERE VD_ASSOC_CODE='"+adminBO.getAssociateCode()+"' AND (VD_INVOICE_NO is NULL OR VD_INVOICE_NO='') AND VD_VOUCHERNO ='"+vouchers+"'";
//		if(vouchers.length -1 >= 0)
//			query += vouchers[vouchers.length-1];
//
//		for(int i=0;i<vouchers.length-1;i++){
//			query +=  ","+vouchers[i] ;
//		}
		query += " ORDER BY VD_VOUCHERNO";
		dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			pst=con.prepareStatement(query);
			//System.out.println("getVoucherByVoucher"+pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				PaymentSettledDetail invoiceBO = new PaymentSettledDetail();
				invoiceBO.setVoucherNo(rs.getString("VD_VOUCHERNO"));
				invoiceBO.setTransId(rs.getString("VD_TXN_ID"));
				invoiceBO.setTotal(rs.getString("VD_TOTAL"));
				voucher.add(invoiceBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getVoucherById-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.getVoucherById-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucher;
	}

	
	public static ArrayList<PaymentSettledDetail> getNewPaidUnpaidVoucherByVoucher(AdminRegistrationBO adminBO,String vouchers, String paidOrUnpaid,String verify){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getNewPaidUnpaidVoucherByVoucher");
		int verified = (verify!=null && !verify.equals(""))?Integer.parseInt(verify):0;
		
		TDSConnection dbcon = null;
		PreparedStatement pst = null;
		ArrayList<PaymentSettledDetail> voucher =new ArrayList<PaymentSettledDetail>();
		ResultSet rs=null;
		String query = "SELECT VD_TXN_ID,VD_COSTCENTER,VD_RIDER_NAME,VD_VOUCHERNO,VD_ASSOC_CODE,VD_COMPANY_CODE,VD_TOTAL,VD_DESCR,VD_CONTACT,VD_DELAYDAYS,VD_FREQUENCY,VD_PAYMENT_RECEIVED_STATUS,date_format(VD_EXPIRY_DATE,'%m/%d/%Y') as VD_EXPIRYDATE" +
		" FROM TDS_VOUCHER_DETAIL WHERE VD_ASSOC_CODE='"+adminBO.getAssociateCode()+"' AND (VD_INVOICE_NO is NULL OR VD_INVOICE_NO='')  AND VD_PAYMENT_RECEIVED_STATUS='"+paidOrUnpaid+"' AND VD_VOUCHERNO ='"+vouchers+"'";
		
		if(verified==3){
			query =query + " AND VD_VERIFIED = 0";
		}else if(verified==2){
			query =query + " AND VD_VERIFIED IN ('1','4')";
		}else if(verified!=0){
			query =query + " AND VD_VERIFIED ='"+verified+"'";
		}else{
			query =query + " AND VD_VERIFIED != 0";
		}
		
		query += " ORDER BY VD_VOUCHERNO";
		dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			pst=con.prepareStatement(query);
			//System.out.println("getNewPaidUnpaidVoucherByVoucher:"+pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				PaymentSettledDetail invoiceBO = new PaymentSettledDetail();
				invoiceBO.setVoucherNo(rs.getString("VD_VOUCHERNO"));
				invoiceBO.setTransId(rs.getString("VD_TXN_ID"));
				invoiceBO.setTotal(rs.getString("VD_TOTAL"));
				voucher.add(invoiceBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getNewPaidUnpaidVoucherByVoucher-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.getNewPaidUnpaidVoucherByVoucher-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucher;
	}

	
	public static ArrayList<InvoiceBO> getInvoiceDetails(AdminRegistrationBO adminBO,InvoiceBO invoiceBo,int searchBasedOn){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getInvoiceDetails");
		TDSConnection dbcon = null;
		PreparedStatement pst = null;
		ArrayList<InvoiceBO> voucher =new ArrayList<InvoiceBO>();
		ResultSet rs=null;
		String query="";
		dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		//left join TDS_VOUCHER_DETAIL ON VIS_ASSOCCODE = VD_ASSOC_CODE  AND VIS_INVOICE_NUMBER=VD_INVOICE_NO
		query = "SELECT *,DATE_FORMAT(CONVERT_TZ(VIS_PROCESS_DATE, 'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y') as PROCESS_DATE FROM TDS_VOUCHER_INVOICE_SUMMARY LEFT JOIN TDS_GOVT_VOUCHER ON VC_VOUCHERNO=VIS_VOUCHER_NO AND VC_ASSOCCODE = VIS_ASSOCCODE WHERE VIS_ASSOCCODE='"+ adminBO.getAssociateCode()+"' ";
		
		if(invoiceBo.getVoucherNo()!=null && invoiceBo.getVoucherNo()!="" ){
			query =query + " AND VIS_VOUCHER_NO  ='"+invoiceBo.getVoucherNo()+"'";
		}
		
		if(searchBasedOn==1){
			query =query + "AND VIS_PAYMENT_STATUS=1";
		}else if(searchBasedOn==2){
			query =query + "AND VIS_PAYMENT_STATUS=2";
		}else if(searchBasedOn==3){
			query =query + "AND VIS_PAYMENT_STATUS=9";
		}else {
			query =query + "AND VIS_PAYMENT_STATUS=0";
		}
		
		query=query+" GROUP BY VIS_INVOICE_NUMBER ORDER BY VIS_INVOICE_NUMBER DESC";
		try{
			pst=con.prepareStatement(query);
			System.out.println("Checking query:"+pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				InvoiceBO invoiceBO = new InvoiceBO();
				invoiceBO.setInvoiceNumber(rs.getInt("VIS_INVOICE_NUMBER"));
				invoiceBO.setProcessDate(rs.getString("PROCESS_DATE"));
				invoiceBO.setPaymentNo(rs.getString("VIS_PAYMENT_NO"));
				invoiceBO.setChequeNumber(rs.getString("VIS_CHEQUE_NUMBER"));
				invoiceBO.setPaymentRecievedDate(rs.getString("VIS_PAYMENT_RECEIVED_DATE"));
				invoiceBO.setDescription(rs.getString("VIS_DESC"));
				invoiceBO.setTotalNoOfVoucher(rs.getString("VIS_TOTAL_NO_OF_VOUCHERS"));
				invoiceBO.setTotalAmount(rs.getString("VIS_TOTAL_AMOUNT"));
				invoiceBO.setPaymentStatus(rs.getInt("VIS_PAYMENT_STATUS"));
				invoiceBO.setVoucherNo( (rs.getString("VC_VOUCHERNO")!=null && !rs.getString("VC_VOUCHERNO").equals(""))?rs.getString("VC_VOUCHERNO"):rs.getString("VIS_VOUCHER_NO") );
				invoiceBO.setAccountName((rs.getString("VC_RIDERNAME")!=null && !rs.getString("VC_RIDERNAME").equals("")?rs.getString("VC_RIDERNAME"):"N/A"));
				voucher.add(invoiceBO);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getInvoiceDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.getInvoiceDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucher;
	}
	public static PaymentSettledDetail editVoucherByVoucherNo(String assoCode,PaymentSettledDetail invoiceBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.updateVoucherByVoucherNo");

		PreparedStatement pst = null;
		String query="";
		ResultSet rs = null;
		query ="SELECT VD_RET_AMOUNT,VD_PAYMENT_RECEIVED_STATUS,VD_TIP,VD_DRIVER_ID,VD_TXN_ID,VD_PROCESSING_DATE_TIME,date_format(VD_PROCESSING_DATE_TIME,'%m/%d/%Y') as VD_PROCESSING_DATE,VD_USER,VD_TRIP_ID,VD_COSTCENTER,VD_RIDER_NAME,VD_VOUCHERNO,VD_ASSOC_CODE,VD_COMPANY_CODE,VD_AMOUNT,VD_DESCR,VD_CONTACT,VD_DELAYDAYS,VD_FREQUENCY,date_format(VD_EXPIRY_DATE,'%m/%d/%Y') as VD_EXPIRYDATE,VD_INVOICE_NO FROM TDS_VOUCHER_DETAIL WHERE VD_TXN_ID='"+invoiceBO.getTransId()+"' AND VD_ASSOC_CODE='"+assoCode+"'";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst= con.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				invoiceBO.setRiderName(rs.getString("VD_RIDER_NAME"));
				invoiceBO.setCostCenter(rs.getString("VD_COSTCENTER"));
				invoiceBO.setAmount(rs.getString("VD_AMOUNT"));
				invoiceBO.setExpDate(rs.getString("VD_EXPIRYDATE"));
				invoiceBO.setDescription(rs.getString("VD_DESCR"));
				invoiceBO.setCreationDate(rs.getString("VD_PROCESSING_DATE"));
				invoiceBO.setUser(rs.getString("VD_USER"));
				invoiceBO.setTripId(rs.getString("VD_TRIP_ID"));
				invoiceBO.setCompanyCode(rs.getString("VD_COMPANY_CODE"));
				invoiceBO.setContact(rs.getString("VD_CONTACT"));
				invoiceBO.setDelayDays(rs.getString("VD_DELAYDAYS"));
				invoiceBO.setFreq(rs.getString("VD_FREQUENCY"));
				invoiceBO.setServiceDate(rs.getString("VD_PROCESSING_DATE"));
				invoiceBO.setTransId(rs.getString("VD_TXN_ID"));
				invoiceBO.setDriverId(rs.getString("VD_DRIVER_ID"));
				invoiceBO.setRetAmount(rs.getString("VD_RET_AMOUNT"));
				invoiceBO.setTip(rs.getString("VD_TIP"));
				invoiceBO.setInvoiceNumber(rs.getInt("VD_INVOICE_NO"));
				invoiceBO.setPaymentReceivedStatus(rs.getInt("VD_PAYMENT_RECEIVED_STATUS"));
			}
		} catch (Exception sqex) {
			cat.error("TDSException FinanceDAO.editVoucherByVoucherNo-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.editVoucherByVoucherNo-->"+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return invoiceBO;
	}
	public static void updateVoucherByVoucherNo (String assoCode,PaymentSettledDetail invoiceBo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.updateVoucherByVoucherNo");

		PreparedStatement pst = null,pst1 = null;
		String invNo="";
		if(invoiceBo.getExpDate().contains("/")){
			invoiceBo.setExpDate(TDSValidation.getDBdateFormat(invoiceBo.getExpDate()));
		}
		if(invoiceBo.getServiceDate().contains("/")){

			invoiceBo.setServiceDate(TDSValidation.getTimsStampDate(invoiceBo.getServiceDate(),System.currentTimeMillis()+""));
		}
		String query1 = "UPDATE TDS_OPENREQUEST_HISTORY SET ORH_AMT ='"+invoiceBo.getTotalAmount()+"' WHERE ORH_ASSOCCODE ='"+assoCode+"' AND ORH_TRIP_ID ='"+invoiceBo.getTripId()+"'";
		String query="update TDS_VOUCHER_DETAIL SET "+ 
		"VD_RIDER_NAME=?,VD_COSTCENTER=?,VD_AMOUNT=?,VD_EXPIRY_DATE=?,VD_DESCR=?,"+
		"VD_USER=?,VD_TRIP_ID=?,VD_COMPANY_CODE=?,VD_CONTACT=?,VD_DELAYDAYS=?,VD_FREQUENCY=?,VD_PROCESSING_DATE_TIME=?,"+
		"VD_TXN_ID=?,VD_DRIVER_ID =?,VD_RET_AMOUNT=?,VD_INVOICE_NO=?,VD_TOTAL = ?,VD_TIP =?, VD_PAYMENT_RECEIVED_STATUS=?,VD_VOUCHERNO=? WHERE VD_ASSOC_CODE=? AND VD_TXN_ID=?";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst1 = con.prepareStatement(query1);
			pst1.executeUpdate();

			pst= con.prepareStatement(query);
			pst.setString(1, invoiceBo.getRiderName());
			pst.setString(2, invoiceBo.getCostCenter());
			pst.setString(3, invoiceBo.getAmount());
			pst.setString(4, invoiceBo.getExpDate());
			pst.setString(5, invoiceBo.getDescription());
			pst.setString(6, invoiceBo.getUser());
			pst.setString(7, invoiceBo.getTripId());
			pst.setString(8, invoiceBo.getCompanyCode());
			pst.setString(9, invoiceBo.getContact());
			pst.setString(10, invoiceBo.getDelayDays());
			pst.setString(11, invoiceBo.getFreq());
			pst.setString(12, invoiceBo.getServiceDate());
			pst.setString(13, invoiceBo.getTransId());
			pst.setString(14, invoiceBo.getDriverId());
			pst.setString(15, invoiceBo.getRetAmount());
			//pst.setString(16, invoiceBo.getTip());
			if( invoiceBo.getInvoiceNumber()!=0){
				invNo=invoiceBo.getInvoiceNumber()+"";
			}else{
				invNo="";
			}
			pst.setString(16, invNo);
			pst.setDouble(17, invoiceBo.getTotalAmount());
			if(invoiceBo.getTip().equalsIgnoreCase("")||invoiceBo.getTip()==null){
				invoiceBo.setTip("0.00");
			}
			pst.setString(18, invoiceBo.getTip());
			pst.setInt(19, invoiceBo.getPaymentReceivedStatus());
			pst.setString(20,invoiceBo.getVoucherNo());
			pst.setString(21, assoCode);
			pst.setString(22, invoiceBo.getTransId());
			pst.executeUpdate();

		} catch (Exception sqex) {
			cat.error("TDSException FinanceDAO.updateVoucherByVoucherNo-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.updateVoucherByVoucherNo-->"+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}	

	public static void deleteInvoice (String assoCode,int invoiceNo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.deleteInvoice");
		PreparedStatement pst = null;
		
		//String query1 ="DELETE FROM TDS_VOUCHER_INVOICE_SUMMARY WHERE VIS_ASSOCCODE ='"+assoCode+"' AND VIS_INVOICE_NUMBER ="+invoiceNo;
		//String query ="update TDS_VOUCHER_DETAIL set VD_INVOICE_NO =NULL,VD_PROCESSING_DATE_TIME=VD_PROCESSING_DATE_TIME  WHERE VD_ASSOC_CODE ='"+assoCode+"' AND VD_INVOICE_NO ='"+invoiceNo+"'";
		
		String queryUpdateStatus = "UPDATE TDS_VOUCHER_INVOICE_SUMMARY SET VIS_PAYMENT_STATUS='9', VIS_PROCESS_DATE=NOW() WHERE VIS_ASSOCCODE ='"+assoCode+"' AND VIS_INVOICE_NUMBER ='"+invoiceNo+"'";
		String queryWithLogsTime = "UPDATE TDS_VOUCHER_DETAIL LEFT JOIN TDS_OPENREQUEST_LOGS ON VD_TRIP_ID=ORL_TRIPID AND ORL_REASON='Voucher Pmt Received' SET VD_INVOICE_NO =NULL, VD_PROCESSING_DATE_TIME = ORL_TIME WHERE VD_ASSOC_CODE= '"+assoCode+"' AND VD_INVOICE_NO ='"+invoiceNo+"'";
		//String queryWithLogsTime = "UPDATE TDS_VOUCHER_DETAIL SET VD_INVOICE_NO =NULL, VD_PROCESSING_DATE_TIME = NOW() WHERE VD_ASSOC_CODE= '"+assoCode+"' AND VD_INVOICE_NO ='"+invoiceNo+"'";
		
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst= con.prepareStatement(queryWithLogsTime);
			System.out.println("update query:"+pst.toString());
			pst.executeUpdate();
			
			pst =con.prepareStatement(queryUpdateStatus);
			System.out.println("update status:"+pst.toString());
			pst.executeUpdate();
		} catch (Exception sqex) {
			cat.error("TDSException FinanceDAO.deleteInvoice-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.deleteInvoice-->"+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	public static ArrayList<VoucherBO> getPaymentByTrip (String assoCode,int tripId,String masterAssoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getPaymentByTrip");
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs,rs1=null;
		ArrayList<VoucherBO> paymentArray=new ArrayList<VoucherBO>();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst= con.prepareStatement("select * from TDS_DRIVER_CHARGE_CALC as a,TDS_VOUCHER_DETAIL as b,TDS_CHARGE_TYPE as c where a.DC_TRIP_ID = b.VD_TRIP_ID and a.DC_TYPE_CODE = c.CT_KEY and c.CT_ASSOCIATION_CODE='"+masterAssoccode+"' and a.DC_TRIP_ID='"+tripId+"' group by DC_TXN_NO");
			rs=pst.executeQuery();
			while(rs.next()){
				VoucherBO payment=new VoucherBO();
				payment.setVamount(rs.getString("a.DC_AMOUNT"));
				payment.setVstatus("Voucher");
				payment.setVno(rs.getString("b.VD_VOUCHERNO"));
				payment.setTip(rs.getString("b.VD_TIP"));
				payment.setVdesc(rs.getString("b.VD_DESCR"));
				payment.setVcode(rs.getString("c.CT_DESCRIPTION"));
				paymentArray.add(payment);
			} 
			if(paymentArray.size()==0) {
				pst1=con.prepareStatement("select * from TDS_DRIVER_CHARGE_CALC as a,TDS_CREDIT_CARD_DETAIL as b,TDS_CHARGE_TYPE as c where a.DC_TRIP_ID = b.CCD_TRIP_ID and a.DC_TYPE_CODE = c.CT_KEY and c.CT_ASSOCIATION_CODE='"+masterAssoccode+"' and a.DC_TRIP_ID='"+tripId+"' group by DC_TXN_NO");
				rs1=pst1.executeQuery();  
				while(rs1.next()){
					VoucherBO payment=new VoucherBO();
					payment.setVamount(rs1.getString("a.DC_AMOUNT"));
					payment.setVstatus("Credit Card");
					payment.setVno(rs1.getString("b.CCD_CARD_NO"));
					payment.setTip(rs1.getString("b.CCD_TIP_AMOUNT"));
					payment.setVtrans(rs1.getString("b.CCD_TXN_NO"));
					payment.setVdesc(rs1.getString("b.CCD_CARD_HOLDER_NAME"));
					payment.setVcode(rs1.getString("c.CT_DESCRIPTION"));
					paymentArray.add(payment);
				}
			}
		} catch (Exception sqex) {
			cat.error("TDSException FinanceDAO.getPaymentByTrip-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.getPaymentByTrip-->"+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return paymentArray;
	}
	public static Blob viewSign (String documentId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.viewSign");

		PreparedStatement pst = null;

		String query = "";

		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		ResultSet rs=null;
		Blob blobData = null;
		try {
			if (!documentId.equals("")) {
				query = "select VD_RIDER_SIGN FROM TDS_VOUCHER_DETAIL where VD_TXN_ID='"+documentId+"'";
				pst = con.prepareStatement(query);
				rs = pst.executeQuery();
				while (rs.next()) {
					blobData = rs.getBlob(1);
				}

			}
		} catch (Exception sqex) {
			cat.error("TDSException FinanceDAO.viewSign-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.viewSign-->"+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return blobData;
	}
	public static ArrayList<String> getUninvoicedVouchers(String master,String assocode,int verified){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getUninvoicedVouchers");
		PreparedStatement pst = null;
		ArrayList<String> voucher =  new ArrayList<String>();
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String query = "SELECT COUNT(*) AS TOTAL ,VD_VOUCHERNO,VC_RIDERNAME  FROM TDS_VOUCHER_DETAIL LEFT JOIN TDS_GOVT_VOUCHER ON VD_VOUCHERNO = VC_VOUCHERNO WHERE (VD_INVOICE_NO IS NULL OR VD_INVOICE_NO = '') AND VD_MASTER_ASSOCCODE ='"+master+"'";
		if(!assocode.equals("")){
			query = query + " AND VD_ASSOC_CODE='"+assocode+"'";
		}
		if(verified==3){
			query =query + " AND VD_VERIFIED = 0";
		}else if(verified==2){
			query =query + " AND VD_VERIFIED IN ('1','4')";
		}else if(verified!=0){
			query =query + " AND VD_VERIFIED ='"+verified+"'";
		} 
		query=query+" GROUP BY VD_VOUCHERNO";
		try{
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			//System.out.println("getUnInvoicedVouchers : "+pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				voucher.add(rs.getString("VD_VOUCHERNO"));
				voucher.add(rs.getString("TOTAL"));
				voucher.add(rs.getString("VC_RIDERNAME"));
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getUninvoicedVouchers-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.getUninvoicedVouchers-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucher;
	}
	
	public static ArrayList<VoucherBO> getVerifiedUninvoicedVouchers(AdminRegistrationBO adminBO,String verify, String fDate, String tDate,String assocCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.getUninvoicedVouchers");
		int verified = (verify!=null && !verify.equals(""))?Integer.parseInt(verify):0;
		ArrayList<VoucherBO> voucherList = new ArrayList<VoucherBO>();
		
		PreparedStatement pst = null;
		//ArrayList<String> voucher =  new ArrayList<String>();
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String query = "SELECT VD_VOUCHERNO,VD_CC_PRCNT_AMT,VD_CC_PERCENT_PROCESSED,VC_CC_PERCENTAGE,VC_DESCR,VC_PAYMENT_TYPE, VC_INFOKEY, VC_EMAIL_ADDRESS,VC_EMAIL_STATUS FROM TDS_VOUCHER_DETAIL LEFT JOIN TDS_GOVT_VOUCHER ON VD_VOUCHERNO = VC_VOUCHERNO WHERE (VD_INVOICE_NO IS NULL OR VD_INVOICE_NO = '') AND VD_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' ";
		
		if(assocCode!=null && !assocCode.equals("")){
			query = query + " AND VD_ASSOC_CODE ='"+adminBO.getAssociateCode()+"'";
		}
		
		if(verified==3){
			query =query + " AND VD_VERIFIED = 0";
		}else if(verified==2){
			query =query + " AND VD_VERIFIED IN ('1','4')";
		}else if(verified!=0){
			query =query + " AND VD_VERIFIED ='"+verified+"'";
		}
		
		/*if(verified.equalsIgnoreCase("0")){
			query =query + " AND VD_VERIFIED ='0'";
		}else {
			query =query + " AND VD_VERIFIED !='0'";
		} */
		
		if(fDate!=""&&fDate!=null){
			String[] fromD = fDate.split("/");
			fDate = fromD[2] +"-"+  fromD[0]+"-"+ fromD[1];
		}
		if(tDate!=""&&tDate!=null){
			String[] toD = tDate.split("/");
			tDate = toD[2] +"-"+  toD[0]+"-"+ toD[1];
		}
		if(!fDate.equals("") && !tDate.equals("VC_DESCR")){
			query =query + " AND (date_format(CONVERT_TZ(VD_PROCESSING_DATE_TIME, 'UTC','"+adminBO.getTimeZone()+"'),'%Y-%m-%d') BETWEEN '"+fDate+"' AND '"+tDate+"'"+")";
		}
		
		query=query+" GROUP BY VD_VOUCHERNO";
		try{
			pst=con.prepareStatement(query);
			cat.info(pst.toString());
			//System.out.println("getVerifiedUnInvoicedVouchers : "+pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				VoucherBO voucherBo = new VoucherBO();
				voucherBo.setVno(rs.getString("VD_VOUCHERNO"));
				voucherBo.setVname(rs.getString("VC_DESCR"));
				voucherBo.setProcessMethod(rs.getInt("VC_PAYMENT_TYPE"));
				voucherBo.setInfokey(rs.getString("VC_INFOKEY"));
				voucherBo.setEmailAddress(rs.getString("VC_EMAIL_ADDRESS"));
				voucherBo.setEmailStatus(rs.getInt("VC_EMAIL_STATUS"));
				voucherBo.setCcProcessPercentage((rs.getString("VC_CC_PERCENTAGE")!=null && !rs.getString("VC_CC_PERCENTAGE").equals(""))?rs.getInt("VC_CC_PERCENTAGE"):0);
				voucherBo.setCc_Percent_processed((rs.getString("VD_CC_PERCENT_PROCESSED")!=null && !rs.getString("VD_CC_PERCENT_PROCESSED").equals(""))?rs.getInt("VD_CC_PERCENT_PROCESSED"):0);
				voucherBo.setCc_Prcnt_Amt((rs.getString("VD_CC_PRCNT_AMT")!=null && !rs.getString("VD_CC_PRCNT_AMT").equals(""))?rs.getString("VD_CC_PRCNT_AMT"):"");
				voucherList.add(voucherBo);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.getUninvoicedVouchers-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.getUninvoicedVouchers-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucherList;
	}

	
	public static ArrayList<ChargesBO> getChargesForVoucher(String assocode,String tripId,String masterAssoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<ChargesBO> al_list = new ArrayList<ChargesBO>();
		PreparedStatement pst = null; 
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String query="SELECT DC_TYPE_CODE,DC_AMOUNT,CT_DESCRIPTION FROM TDS_DRIVER_CHARGE_CALC  JOIN TDS_CHARGE_TYPE   ON CT_ASSOCIATION_CODE='"+masterAssoccode+"' AND CT_KEY =DC_TYPE_CODE AND  DC_ASSOC_CODE='"+assocode+"' AND DC_TRIP_ID ='"+tripId+"' AND DC_MASTER_ASSOCCODE='"+masterAssoccode+"'";
		ResultSet rs=null;
		try {
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				ChargesBO chBo = new ChargesBO();
				chBo.setPayTypeDesc(rs.getString("CT_DESCRIPTION"));
				chBo.setAmount(rs.getString("DC_AMOUNT"));
				chBo.setPayTypeKey(rs.getInt("DC_TYPE_CODE"));
				al_list.add(chBo);
			}
		} catch (Exception sqex) {
			cat.error("TDSException FinanceDAO.getChargesForVoucher-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException FinanceDAO.getChargesForVoucher-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}


	public static int deleteVoucherEntry (AdminRegistrationBO adminBO,String transId,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.deleteVoucherEntry");
		PreparedStatement pst = null, pst1=null;
		int status=0,status1=0;
		String query1 ="DELETE FROM TDS_VOUCHER_DETAIL WHERE VD_ASSOC_CODE ='"+adminBO.getAssociateCode()+"' AND VD_TXN_ID ='"+transId+"' AND VD_TRIP_ID='"+tripId+"'";
		String query ="INSERT INTO TDS_OPENREQUEST_LOGS (ORL_TRIPID,ORL_ASSOCCODE,ORL_TIME,ORL_REASON,ORL_CHANGED_BY,ORL_STATUS) VALUES('"+tripId+"','"+adminBO.getAssociateCode()+"',NOW(),'Deleted voucher pmt','"+adminBO.getUid()+"',0)";
		String query2="INSERT INTO TDS_CONSOLE (C_TRIPID,C_ASSOCCODE,C_TIME,C_REASON,C_CHANGED_BY,C_STATUS) VALUES('"+tripId+"','"+adminBO.getAssociateCode()+"',NOW(),'Deleted voucher pmt','"+adminBO.getUid()+"',0)";
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst= con.prepareStatement(query);
			cat.info("query--->"+pst.toString());
			status=pst.executeUpdate();
			if(status>=1){
				status=0;
				pst =con.prepareStatement(query1);
				cat.info("query--->"+pst.toString());
				status=pst.executeUpdate();
				
				pst1 =con.prepareStatement(query2);
				cat.info("query--->"+pst1.toString());
				status1=pst1.executeUpdate();
			}
		} catch (Exception sqex) {
			cat.error("TDSException FinanceDAO.deleteVoucherEntry-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException FinanceDAO.deleteVoucherEntry-->"+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	public static int upDateVoucherAsverified(String assoCode,String[] setID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.upDateVoucherAsverified");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int status =0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query = "UPDATE TDS_VOUCHER_DETAIL SET VD_VERIFIED ='4',VD_PROCESSING_DATE_TIME=VD_PROCESSING_DATE_TIME WHERE  VD_ASSOC_CODE='"+assoCode+"' AND VD_TXN_ID in (";
		if(setID.length -1 >= 0)
			query += setID[setID.length-1];

		for(int j=0;j<setID.length-1;j++){
			query +=  ","+setID[j] ;
		}
		query += ")";	
		try{
			pst=con.prepareStatement(query);
			status=pst.executeUpdate();
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.upDateVoucherAsverified-->"+sqex.getMessage());
			//System.out.println("TDSException FinanceDAO.upDateVoucherAsverified-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	public static boolean insertAccessForAssistants(String[] voucherNos,String emailId,String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.insertAccessForAssistants");
		boolean success = false;
		PreparedStatement pst = null;
		TDSConnection dbcon = new TDSConnection();
		Connection csp_conn = dbcon.getConnection();
		try {
			for(int i=0;i<voucherNos.length;i++){
				pst = csp_conn.prepareStatement("INSERT INTO TDS_ASSISTANTOPERATOR_ACCESS(AOA_ACCOUNT_NO,AOA_EMAILID,AOA_ASSOCCODE) VALUES(?,?,?)"); 
				pst.setString(1, voucherNos[i]);
				pst.setString(2, emailId);
				pst.setString(3, assoCode);
				cat.info(pst.toString());
				pst.execute();
				success = true;
			}
		} catch (Exception sqex){
			cat.error("TDSException FinanceDAO.insertAccessForAssistants-->"+sqex.getMessage());
			//System.out.println("TDSException FinanceDAO.insertAccessForAssistants" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return success;
	}
	public static int unLinkVoucherFromInvoice(AdminRegistrationBO adminBO,String transID,String invoiceNo,String voucherNumber ) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO FinanceDAO.unLinkVoucherFromInvoice");
		int result = 0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		InvoiceBO voucherBO=new InvoiceBO();
		try{
			pst=con.prepareStatement("SELECT * FROM TDS_VOUCHER_DETAIL WHERE VD_TXN_ID='"+transID+"' AND VD_INVOICE_NO='"+invoiceNo+"' AND VD_VOUCHERNO='"+voucherNumber+"' AND VD_ASSOC_CODE='"+adminBO.getAssociateCode()+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				voucherBO.setTotal(rs.getString("VD_TOTAL"));
			}
			pst=con.prepareStatement("UPDATE TDS_VOUCHER_INVOICE_SUMMARY SET VIS_TOTAL_NO_OF_VOUCHERS=VIS_TOTAL_NO_OF_VOUCHERS-1,VIS_TOTAL_AMOUNT=VIS_TOTAL_AMOUNT-"+voucherBO.getTotal()+" WHERE VIS_INVOICE_NUMBER='"+invoiceNo+"' AND VIS_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
			result=pst.executeUpdate();
			if(result>=1){
				result=0;
				pst=con.prepareStatement("UPDATE TDS_VOUCHER_DETAIL SET VD_INVOICE_NO=NULL WHERE VD_INVOICE_NO='"+invoiceNo+"' AND VD_ASSOC_CODE='"+adminBO.getAssociateCode()+"' AND VD_TXN_ID='"+transID+"'");
				result=pst.executeUpdate();
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.unLinkVoucherFromInvoice-->"+sqex.getMessage());
			//System.out.println("TDSException FinanceDAO.unLinkVoucherFromInvoice-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static String[] readVoucherAllocation(String voucherNo,String companyCode ) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		String[] values = new String[4];
		try{
			pst=con.prepareStatement("SELECT VC_ALLOCATE_ONLY_TO,VC_NOT_ALLOCATE_TO FROM TDS_GOVT_VOUCHER WHERE VC_ASSOCCODE='"+companyCode+"' AND VC_VOUCHERNO='"+voucherNo+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				values[0] = "1";
				values[1] = rs.getString("VC_ALLOCATE_ONLY_TO");
				values[2] = "2";
				values[3] = rs.getString("VC_NOT_ALLOCATE_TO")==null?"":rs.getString("VC_NOT_ALLOCATE_TO");
			}
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.readVoucherAllocation-->"+sqex.getMessage());
			//System.out.println("TDSException FinanceDAO.readVoucherAllocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return values;
	}
	public static boolean checkPrepaidCard(String cardNum,String pin,String uName, String pass) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		boolean returnMessage=false;
		try{
			pst=con.prepareStatement(TDSSQLConstants.isUserAvailable);
			pst.setString(1, uName);
			pst.setString(2, pass);
			rs=pst.executeQuery();
			if(rs.next()){
				pst=con.prepareStatement("SELECT * FROM TDS_PREPAID_CARDS WHERE PC_CARD_NUMBER='"+cardNum+"' AND PC_PIN='"+pin+"' AND PC_ALLOCATED_TO='"+uName+"' AND PC_STATUS='0'");
				rs=pst.executeQuery();
				if(rs.next()){
					pst=con.prepareStatement("INSERT INTO TDS_DRIVER_BALANCE (DB_ASSOCIATION_CODE,DB_DRIVER_ID,DB_BALANCE_AMOUNT,DB_CARD_NUMBER) VALUES (?,?,?,?)");
					pst.setString(1, rs.getString("PC_ASSOCIATION_CODE"));
					pst.setString(2, uName);
					pst.setString(3, rs.getString("PC_AMOUNT"));
					pst.setString(4, cardNum);
					pst.execute();
					pst=con.prepareStatement("UPDATE TDS_PREPAID_CARDS SET PC_STATUS='1' WHERE PC_CARD_NUMBER='"+cardNum+"' AND PC_PIN='"+pin+"'");
					pst.execute();
					returnMessage=true;
				}
			} 
		}catch(SQLException sqex) {
			cat.error("TDSException FinanceDAO.checkPrepaidCard-->"+sqex.getMessage());
			//System.out.println("TDSException FinanceDAO.checkPrepaidCard-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnMessage;
	}

public static double checkDriverBalance(String uName, String assoccode) {
	long startTime = System.currentTimeMillis();
	cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
	TDSConnection dbcon = new TDSConnection();
	Connection con = dbcon.getConnection();
	PreparedStatement pst = null;
	ResultSet rs=null;
	double amount=0.00;
	try{
		pst=con.prepareStatement("SELECT DB_BALANCE_AMOUNT FROM TDS_DRIVER_BALANCE WHERE DB_DRIVER_ID='"+uName+"' AND DB_ASSOCIATION_CODE='"+assoccode+"' ORDER BY DB_BALANCE_AMOUNT DESC");
		rs=pst.executeQuery();
		if(rs.next()){
			amount = rs.getDouble("DB_BALANCE_AMOUNT");
		} 
	}catch(SQLException sqex) {
		cat.error("TDSException FinanceDAO.checkDriverBalance-->"+sqex.getMessage());
		//System.out.println("TDSException FinanceDAO.checkDriverBalance-->"+sqex.getMessage());
		sqex.printStackTrace();
	} finally {
		dbcon.closeConnection();
	}
	cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	return amount;
}
public static int updateprepaidcard(String assoCode,String driverid,String amount,String pinno) {
	long startTime = System.currentTimeMillis();
	cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
	cat.info("TDS INFO FinanceDAO.updateprepaid");
	TDSConnection dbcon = null;
	Connection con = null;
	PreparedStatement pst = null;
	int status =0;
	dbcon = new TDSConnection();
	con = dbcon.getConnection();
	String query="UPDATE TDS_PREPAID_CARDS SET PC_ALLOCATED_TO ='"+driverid+"',PC_AMOUNT ='"+amount+"' WHERE PC_ASSOCIATION_CODE='"+assoCode+"'  AND PC_PIN='"+pinno+"'";
		try{
		pst=con.prepareStatement(query);
		status=pst.executeUpdate();
	}catch(SQLException sqex) {
		cat.error("TDSException FinanceDAO.updateprepaidcard-->"+sqex.getMessage());
		//System.out.println("TDSException FinanceDAO.updateprepaidcard-->"+sqex.getMessage());
		sqex.printStackTrace();
	} finally {
		dbcon.closeConnection();
	}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	return status;
}

public static String[] getprepaidcard(String assocode,String cardno ) {
	long startTime = System.currentTimeMillis();
	cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
	TDSConnection dbcon = new TDSConnection();
	Connection con = dbcon.getConnection();
	PreparedStatement pst = null;
	ResultSet rs=null;
	String[] values = new String[4];
	try{
		pst=con.prepareStatement("SELECT PC_CARD_NUMBER,PC_ALLOCATED_TO,PC_AMOUNT,PC_PIN FROM TDS_PREPAID_CARDS WHERE PC_ASSOCIATION_CODE='"+assocode+"' AND PC_CARD_NUMBER='"+cardno+"'");
		rs=pst.executeQuery();
		if(rs.next()){
			values[0] = rs.getString("PC_CARD_NUMBER");
			values[1] = rs.getString("PC_ALLOCATED_TO");
			values[2] = rs.getString("PC_AMOUNT");
			values[3] = rs.getString("PC_PIN");
				}
	}catch(SQLException sqex) {
		cat.error("TDSException FinanceDAO.getprepaidcard-->"+sqex.getMessage());
		//System.out.println("TDSException FinanceDAO.getprepaidcard-->"+sqex.getMessage());
		sqex.printStackTrace();
	} finally {
		dbcon.closeConnection();
	}
	cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	return values;
}

}






