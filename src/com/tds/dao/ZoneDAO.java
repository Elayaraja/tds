package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Category;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.QueueBean;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.tds.tdsBO.ZoneCoordinatesBo;
import com.tds.tdsBO.ZoneRateBean;

public class ZoneDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(ZoneDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ZoneDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}

	public static ArrayList<ZoneTableBeanSP> readZonesBoundries(String p_assocode, String zoneNum, boolean readAll){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.readZonesBoundries Start--->"+startTime);
		ArrayList<ZoneTableBeanSP> al_list=new ArrayList<ZoneTableBeanSP>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query = "";
			if(readAll){
				query="SELECT * FROM TDS_QUEUE_DETAILS WHERE QD_VIRTUAL_ACTUAL NOT IN ('D','DO') AND QD_ASSOCCODE='" + p_assocode + "' order by QD_QUEUENAME, QD_ORDER";
			} else
				query="SELECT * FROM TDS_QUEUE_DETAILS WHERE QD_ASSOCCODE='" + p_assocode + "' and QD_QUEUENAME = '" + zoneNum +"' order by QD_QUEUENAME, QD_ORDER";

			pst=con.prepareStatement(query);
			rs = pst.executeQuery();
			int j = 1;
			while(rs.next()){
				ZoneTableBeanSP zoneTable = new ZoneTableBeanSP();
				zoneTable.setZoneKey(rs.getString("QD_QUEUENAME"));
				zoneTable.setDispatchType(rs.getString("QD_DISPATCH_TYPE"));
				zoneTable.setZoneDesc(rs.getString("QD_DESCRIPTION"));
				zoneTable.setAdvance(rs.getInt("QD_DELAYTIME"));
				zoneTable.setWaitTime(rs.getInt("QD_SMS_RES_TIME"));
				zoneTable.setZoneType(rs.getString("QD_VIRTUAL_ACTUAL"));
				zoneTable.setZoneSwitch(rs.getString("QD_LOGIN_SWITCH"));
				ArrayList<Double> latitude = new ArrayList<Double>();
				ArrayList<Double> longitude = new ArrayList<Double>();
				ArrayList<Double> maxAndMinLatitude = new ArrayList<Double>();
				ArrayList<Double> maxAndMinLongitude = new ArrayList<Double>();
				String currentQueue = rs.getString("QD_QUEUENAME");
				readQueueBoundries(p_assocode, currentQueue, latitude, longitude,maxAndMinLatitude,maxAndMinLongitude);
				double[] lats = new double[latitude.size()];
				double[] longs = new double[longitude.size()];
				for(int i = 0; i < latitude.size(); i++){
					lats[i] = latitude.get(i);
					longs[i]= longitude.get(i);
				}
				zoneTable.setZoneCoord(lats, longs);
				zoneTable.setEastLongitude(maxAndMinLongitude.get(0));
				zoneTable.setWestLongitude(maxAndMinLongitude.get(1));
				zoneTable.setNorthLatitude(maxAndMinLatitude.get(0));
				zoneTable.setSouthLatitude(maxAndMinLatitude.get(1));
				double externalLatitude = 0;
				double externalLongitude = 0;
				ArrayList<Double> latitideAndLongitude = readQueueExternalPoint(p_assocode, currentQueue);
				zoneTable.setExternalPoint(latitideAndLongitude.get(0), latitideAndLongitude.get(1));
				al_list.add(zoneTable);
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.readZonesBoundries-->"+sqex.getMessage());
			//System.out.println("TDSException ZoneDAO.readZonesBoundries-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.readZonesBoundries End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ZoneTableBeanSP getDefaultZone(String p_assocode){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.getDefaultZone Start--->"+startTime);
		ZoneTableBeanSP defaultZone = null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query = "";
			query="SELECT * FROM TDS_QUEUE_DETAILS WHERE QD_VIRTUAL_ACTUAL = 'D' AND QD_ASSOCCODE='" + p_assocode + "'";
			pst=con.prepareStatement(query);
			rs = pst.executeQuery();
			if(rs.next()){
				defaultZone = new ZoneTableBeanSP();
				defaultZone.setZoneKey(rs.getString("QD_QUEUENAME"));
				defaultZone.setZoneSwitch(rs.getString("QD_LOGIN_SWITCH"));
				defaultZone.setZoneDesc(rs.getString("QD_DESCRIPTION"));
				defaultZone.setAdvance(rs.getInt("QD_DELAYTIME")*60);
				defaultZone.setWaitTime(rs.getInt("QD_SMS_RES_TIME"));
				defaultZone.setZoneType(rs.getString("QD_VIRTUAL_ACTUAL"));
				defaultZone.setDispatchType(rs.getString("QD_DISPATCH_TYPE"));
				defaultZone.setDistance(rs.getInt("QD_DISPATCH_DISTANCE"));
				if(rs.getString("QD_BROADCAST_DELAY") != null){
					defaultZone.setBroadCastTime(rs.getInt("QD_BROADCAST_DELAY"));
				} else {
					defaultZone.setBroadCastTime(null);
				}
				defaultZone.setPhoneCallWaitTime(rs.getInt("QD_PHONE_CALL_WAIT_TIME"));
				defaultZone.setMaxDistance(rs.getString("QD_MAX_DISTANCE"));
				defaultZone.setZnProfile(rs.getString("QD_FLAG"));
				if(rs.wasNull()){
					defaultZone.setBroadCastTime(null);
				}
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.getDefaultZone-->"+sqex.getMessage());
			//System.out.println("TDSException ZoneDAO.getDefaultZone" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.getDefaultZone End--->"+(System.currentTimeMillis()-startTime));
		return defaultZone;
	}
	public static double[] getDefaultLocation(String p_assocode){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.getDefaultLocation Start--->"+startTime);
		double[] defaultLocation = null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query = "";
			query="SELECT * FROM TDS_QUEUE_DETAILS LEFT JOIN TDS_QUEUE_BOUNDRIES ON QD_ASSOCCODE= QB_ASSOCCODE AND QB_QUEUENAME = QD_QUEUENAME AND QB_SEQUENCE > 0 WHERE QD_VIRTUAL_ACTUAL = 'D' AND QD_ASSOCCODE='" + p_assocode + "'";
			pst=con.prepareStatement(query);
			rs = pst.executeQuery();
			if(rs.next()){
				if(rs.getString("QB_LATITUDE")!=null && rs.getString("QB_LONGITUDE")!=null){
					double[] tempdefaultLocation = {rs.getDouble("QB_LATITUDE"),rs.getDouble("QB_LONGITUDE")};
					defaultLocation = tempdefaultLocation;
				}
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.getDefaultLocation-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.getDefaultLocation" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.getDefaultLocation End--->"+(System.currentTimeMillis()-startTime));
		return defaultLocation;
	}

	public static double getMaxDistance(String assocode,String zoneId){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.getMaxDistance Start--->"+startTime);
		double defaultDistance = 0.00;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query = "";
			query="SELECT QD_MAX_DISTANCE FROM TDS_QUEUE_DETAILS WHERE QD_ASSOCCODE='"+assocode+"' AND QD_QUEUENAME='"+zoneId+"'";
			pst=con.prepareStatement(query);
			rs = pst.executeQuery();
			if(rs.next()){
				defaultDistance = Double.parseDouble(rs.getString("QD_MAX_DISTANCE"));
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.getMaxDistance-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.getMaxDistance" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.getMaxDistance End--->"+(System.currentTimeMillis()-startTime));
		return defaultDistance;
	}

	public static int getShortDistanceType(String assocode,String zoneId){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.getMaxDistance Start--->"+startTime);
		int defaultType = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query = "";
			query="SELECT QD_BDSWITCH FROM TDS_QUEUE_DETAILS WHERE QD_ASSOCCODE='"+assocode+"' AND QD_QUEUENAME='"+zoneId+"'";
			pst=con.prepareStatement(query);
			//System.out.println("Check Query--->"+pst);
			rs = pst.executeQuery();
			if(rs.next()){
				defaultType = rs.getInt("QD_BDSWITCH");
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.getMaxDistance-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.getMaxDistance" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.getMaxDistance End--->"+(System.currentTimeMillis()-startTime));
		return defaultType;
	}


	public static void readQueueBoundries(String assocode, String queueName, ArrayList<Double> latitude, ArrayList<Double> longitude,ArrayList<Double> maxAndMinLatitude,ArrayList<Double> maxAndMinLongitude){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.readQueueBoundries Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="SELECT QB_LONGITUDE, QB_LATITUDE FROM TDS_QUEUE_BOUNDRIES WHERE  QB_SEQUENCE >= 0 AND QB_ASSOCCODE='" + assocode + "' AND QB_QUEUENAME = '"+ queueName +"' order by QB_SEQUENCE";
		try{
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				latitude.add(rs.getDouble("QB_LATITUDE"));
				longitude.add(rs.getDouble("QB_LONGITUDE"));
			}
			//			int count =latitude.size();
			//			latitude.remove(count-1);
			//			longitude.remove(count-1);
			query = "SELECT MAX(QB_LATITUDE) as NLat,MIN(QB_LATITUDE) as SLat,MAX(QB_LONGITUDE)as ELon,MIN(QB_LONGITUDE)as WLon FROM TDS_QUEUE_BOUNDRIES WHERE  QB_SEQUENCE >= 0 AND QB_ASSOCCODE='" + assocode + "' AND QB_QUEUENAME = '"+queueName+"'";
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				maxAndMinLongitude.add(Double.parseDouble(rs.getString("ELon")));
				maxAndMinLongitude.add(Double.parseDouble(rs.getString("WLon")));
				maxAndMinLatitude.add(Double.parseDouble(rs.getString("NLat")));
				maxAndMinLatitude.add(Double.parseDouble(rs.getString("SLat")));
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.readQueueBoundries-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.readQueueBoundries" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.readQueueBoundries End--->"+(System.currentTimeMillis()-startTime));
	} 		

	public static ArrayList<Double> readQueueExternalPoint(String assocode, String queueName){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.readQueueExternalPoint Start--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<Double> latitudeAndLongitude = new ArrayList<Double>();
		try{
			con.setAutoCommit(false);
			String query="SELECT QB_LONGITUDE, QB_LATITUDE FROM TDS_QUEUE_BOUNDRIES WHERE QB_SEQUENCE < 0 AND QB_ASSOCCODE='" + assocode + "' AND QB_QUEUENAME = '"+ queueName +"' order by QB_SEQUENCE";
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				latitudeAndLongitude.add(rs.getDouble("QB_LATITUDE"));
				latitudeAndLongitude.add(rs.getDouble("QB_LONGITUDE"));
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.readQueueExternalPoint-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.readQueueExternalPoint" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.readQueueExternalPoint End--->"+(System.currentTimeMillis()-startTime));
		return latitudeAndLongitude;
	} 		

	public static ArrayList<String> readAllZones(){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.readAllZones Start--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query = "SELECT DISTINCT(QD_QUEUENAME) AS QUEUENAME FROM TDS_QUEUE_DETAILS";
		try{
			pst=con.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("QUEUENAME"));
				al_list.add(rs.getString("QUEUENAME"));
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.readAllZones-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.readAllZones" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.readAllZones End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	//	public static ArrayList<String> readAllZonesForMap(String associationCode){
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ZoneDAO.getDriverDisbrushmentSummary Start--->"+startTime);
	//		ArrayList<String> al_list=new ArrayList<String>();
	//		TDSConnection dbcon = null;
	//		Connection con = null;
	//		PreparedStatement pst = null;
	//		ResultSet rs = null;
	//		dbcon = new TDSConnection();
	//		con = dbcon.getConnection();
	//		String query = "SELECT QD_QUEUENAME, QD_DESCRIPTION FROM TDS_QUEUE_DETAILS WHERE QD_VIRTUAL_ACTUAL NOT IN ('D','DO') AND QD_ASSOCCODE=?";
	//		try{
	//			pst=con.prepareStatement(query);
	//			pst.setString(1,associationCode);
	//			rs = pst.executeQuery();
	//			while(rs.next()){
	//				al_list.add(rs.getString("QD_QUEUENAME"));
	//				al_list.add(rs.getString("QD_DESCRIPTION"));
	//			}
	//		}
	//		catch(SQLException sqex) {
	//			cat.error("TDSException ZoneDAO.readAllZonesForMap-->"+sqex.getMessage());
	//			System.out.println("TDSException ZoneDAO.readAllZonesForMap" + sqex.getMessage());
	//			sqex.printStackTrace();
	//		}finally {
	//			dbcon.closeConnection();
	//		}
	//		cat.info("ZoneDAO.getDriverDisbrushmentSummary End--->"+(System.currentTimeMillis()-startTime));
	//		return al_list;
	//	}

	public static int insertZone(double[] latitude,double[] longitude,String associationCode,String ZoneNumber,int Position){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.insertZone Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		int rows = latitude.length;
		int x=0;
		dbcon = new TDSConnection();
		csp_conn = dbcon.getConnection();
		try {
			if(Position==1){
				for(int i = 0; i<rows; i++){
					csp_pst = csp_conn.prepareStatement("INSERT INTO TDS_QUEUE_BOUNDRIES (QB_ASSOCCODE,QB_QUEUENAME,QB_LATITUDE,QB_LONGITUDE,QB_SEQUENCE) VALUES (?,?,?,?,?)"); 
					csp_pst.setString(1,associationCode);
					csp_pst.setString(2,ZoneNumber);
					csp_pst.setDouble(3,latitude[i]);
					csp_pst.setDouble(4,longitude[i]);
					csp_pst.setString(5, i+1+"" );
					csp_pst.execute();
				}
			} else {
				csp_pst = csp_conn.prepareStatement("INSERT INTO TDS_QUEUE_BOUNDRIES (QB_ASSOCCODE,QB_QUEUENAME,QB_LATITUDE,QB_LONGITUDE,QB_SEQUENCE) VALUES (?,?,?,?,?)"); 
				csp_pst.setString(1,associationCode);
				csp_pst.setString(2,ZoneNumber);
				csp_pst.setDouble(3,latitude[0]);
				csp_pst.setDouble(4,longitude[0]);
				csp_pst.setString(5, "-1" );
				csp_pst.execute();
			}
			x=1;
		} catch (Exception sqex){
			cat.error("TDSException ZoneDAO.insertZone-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.insertZone" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.insertZone End--->"+(System.currentTimeMillis()-startTime));
		return x;
	}

	//	public static int getAdjacentZones(String associationCode,String ZoneNumber){
	//		long startTime = System.currentTimeMillis();
	//		cat.info("ZoneDAO.getDriverDisbrushmentSummary Start--->"+startTime);
	//		TDSConnection dbcon = null;
	//		Connection conn = null;
	//		PreparedStatement pst = null;
	//		ResultSet rs = null;
	//		dbcon = new TDSConnection();
	//		conn = dbcon.getConnection();
	//		ArrayList<String> adjacentZones = new ArrayList<String>();
	//		String query = "";
	//		query="SELECT AZ_ADJACENT_ZONES FROM TDS_ADJACENT_ZONES WHERE AZ_ASSOCCODE =? AND AZ_ZONE_NUMBER ORDER BY AZ_ORDER_NUMBER";
	//		try {
	//			pst=conn.prepareStatement(query);
	//			rs = pst.executeQuery();
	//			int j = 1;
	//			while(rs.next()){
	//				adjacentZones.add(rs.getString("AZ_ADJACENT_ZONES"));
	//			}
	//		}
	//		catch (Exception sqex){
	//			adjacentZones = null;
	//			cat.error("TDSException ZoneDAO.getAdjacentZones-->"+sqex.getMessage());
	//			System.out.println("TDSException ZoneDAO.getAdjacentZones" + sqex.getMessage());
	//			sqex.printStackTrace();
	//		}finally { 
	//			dbcon.closeConnection();
	//		}
	//		cat.info("ZoneDAO.getDriverDisbrushmentSummary End--->"+(System.currentTimeMillis()-startTime));
	//		return 1;
	//	}

	public static int updateZoneDispatch(String associationCode,String ZoneNumber,String dispatchType){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.updateZoneDispatch Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection conn = null;
		PreparedStatement pst = null;
		int i=0;
		dbcon = new TDSConnection();
		conn = dbcon.getConnection();
		try {
			pst=conn.prepareStatement("UPDATE TDS_QUEUE_DETAILS SET QD_DISPATCH_TYPE=? WHERE QD_QUEUENAME=? AND QD_ASSOCCODE=?");
			pst.setString(1, dispatchType);
			pst.setString(2, ZoneNumber);
			pst.setString(3, associationCode);
			i=pst.executeUpdate();
		}
		catch (Exception sqex){
			cat.error("TDSException ZoneDAO.updateZoneDispatch-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.updateZoneDispatch" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.updateZoneDispatch End--->"+(System.currentTimeMillis()-startTime));
		return i;
	}
	public static ArrayList<ZoneCoordinatesBo> readQueueBoundriesForDash(String assocode){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.readQueueBoundriesForDash Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<ZoneCoordinatesBo> zList = new ArrayList<ZoneCoordinatesBo>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="SELECT * FROM TDS_QUEUE_BOUNDRIES JOIN TDS_QUEUE_DETAILS WHERE QB_ASSOCCODE='"+assocode+"' AND QB_ASSOCCODE=QD_ASSOCCODE AND  QB_SEQUENCE >= 0 AND QD_VIRTUAL_ACTUAL <> 'D' AND QD_QUEUENAME=QB_QUEUENAME ORDER BY QB_QUEUENAME,QB_SEQUENCE";
		try{
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				ZoneCoordinatesBo qBo = new ZoneCoordinatesBo();
				qBo.setQueueId(rs.getString("QB_QUEUENAME"));
				qBo.setQDescription(rs.getString("QD_DESCRIPTION"));
				qBo.setLatitude(rs.getDouble("QB_LATITUDE"));
				qBo.setLongitude(rs.getDouble("QB_LONGITUDE"));
				//				qBo = new QueueCoordinatesBO();
				qBo.setQDelayTime(rs.getString("QD_DELAYTIME"));
				qBo.setSmsResponseTime(rs.getString("QD_SMS_RES_TIME"));
				qBo.setQDescription(rs.getString("QD_DESCRIPTION"));
				qBo.setQueueId(rs.getString("QD_QUEUENAME"));
				qBo.setQLoginSwitch(rs.getString("QD_LOGIN_SWITCH"));
				qBo.setQDispatchType(rs.getString("QD_DISPATCH_TYPE"));
				qBo.setQTypeofQueue(rs.getString("QD_VIRTUAL_ACTUAL"));
				qBo.setQBroadcastDelay(rs.getString("QD_BROADCAST_DELAY"));
				qBo.setQStartTime(rs.getString("QD_START_TIME"));
				qBo.setQEndTime(rs.getString("QD_END_TIME"));
				qBo.setQDayofWeek(rs.getString("QD_DAY_OF_WEEK"));
				qBo.setQDispatchDistance(rs.getString("QD_DISPATCH_DISTANCE"));
				qBo.setQRequestBefore(rs.getString("QD_REQUEST_BEFORE"));
				qBo.setVersionNumber(rs.getInt("QD_VERSION"));
				qBo.setPhoneCallTime(rs.getString("QD_PHONE_CALL_WAIT_TIME"));
				qBo.setAdjacentZoneProperties(rs.getString("QD_ADJACENT_ZONE_SORT_TYPE"));
				qBo.setStaleCallTime(rs.getInt("QD_STALE_CALL_TIME"));
				qBo.setStaleCallbasedOn(rs.getInt("QD_STALE_CALL_BASED_ON"));
				qBo.setqSequence(rs.getInt("QB_SEQUENCE"));
				zList.add(qBo);
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.readQueueBoundries-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.readQueueBoundries" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.readQueueBoundriesForDash End--->"+(System.currentTimeMillis()-startTime));
		return zList;
	} 

	public static void updateVersion(String assocode,int method){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.updateVersion Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		String query="";
		int version=0;
		if(method==1){
			query="zV";
		} else if(method==2){
			query="cV";
		}
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst = con.prepareStatement("UPDATE TDS_VERSION SET V_VERSION=V_VERSION+1 WHERE V_ASSOCCODE='"+assocode+"' AND V_NAME='"+query+"'");
			version=pst.executeUpdate();
			if(version==0) {
				pst = con.prepareStatement("INSERT INTO TDS_VERSION (V_VERSION,V_ASSOCCODE,V_NAME) VALUES (?,?,?)");
				pst.setInt(1,1);
				pst.setString(2, assocode);
				pst.setString(3, query);
				pst.execute();
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.updateVersion-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.updateVersion" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.updateVersion End--->"+(System.currentTimeMillis()-startTime));
	}

	public static void insertForAllTest(){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.insertForAllTest Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		String query="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			for(int i=100;i<150;i++){
				query="zV";
				pst = con.prepareStatement("INSERT INTO TDS_VERSION (V_VERSION,V_ASSOCCODE,V_NAME) VALUES (?,?,?)");
				pst.setInt(1,1);
				pst.setInt(2, i);
				pst.setString(3, query);
				pst.execute();
				query="cV";
				pst = con.prepareStatement("INSERT INTO TDS_VERSION (V_VERSION,V_ASSOCCODE,V_NAME) VALUES (?,?,?)");
				pst.setInt(1,1);
				pst.setInt(2, i);
				pst.setString(3, query);
				pst.execute();
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.insertForAllTest-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.insertForAllTest" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.insertForAllTest End--->"+(System.currentTimeMillis()-startTime));
	}

	public static ArrayList<ZoneCoordinatesBo> readZoneLevelAccess(String masterAssocCode, String driverID){
		long startTime = System.currentTimeMillis();
		cat.info("ZoneDAO.readZoneLevelAccess Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ArrayList<ZoneCoordinatesBo> accessList = new ArrayList<ZoneCoordinatesBo>();
		String query="SELECT * FROM TDS_QUEUE_DRIVER_MAPPING where ASS_CODE=? AND DRIVER_ID=?";

		try{
			pst = con.prepareStatement(query);
			pst.setString(1, masterAssocCode);
			pst.setString(2, driverID);
			rs = pst.executeQuery();
			while(rs.next()){
				ZoneCoordinatesBo access = new ZoneCoordinatesBo();
				ZoneCoordinatesBo qBo = new ZoneCoordinatesBo();
				access.setQueueId(rs.getString("QUE_NAME"));
				access.setQLoginSwitch(rs.getString("LOGIN_FLAG"));
				accessList.add(access);
			}				
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.insertForAllTest-->"+sqex.getMessage());
		//	System.out.println("TDSException ZoneDAO.insertForAllTest" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.readZoneLevelAccess End--->"+(System.currentTimeMillis()-startTime));
		return accessList;
	}
	
	public static String getZoneCharges(String masterAssocCode, String startZone,String endZone){
		long startTime = System.currentTimeMillis();
		String charge="";
		cat.info("ZoneDAO.getZoneCharges Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="SELECT ZN_AMOUNT FROM TDS_ZONE_RATE_SETUP WHERE ZN_ASSOCODE='"+masterAssocCode+"' AND ZN_STARTZONE='"+startZone+"' AND ZN_ENDZONE='"+endZone+"'";
		String query1="SELECT ZN_AMOUNT FROM TDS_ZONE_RATE_SETUP WHERE ZN_ASSOCODE='"+masterAssocCode+"' AND ZN_STARTZONE='"+endZone+"' AND ZN_ENDZONE='"+startZone+"'";
		try{
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			if(rs.next()){
				charge = rs.getString("ZN_AMOUNT");
			} else {
				pst = con.prepareStatement(query1);
				System.out.println("Query1--->"+pst);
				rs = pst.executeQuery();
				if(rs.next()){
					charge = rs.getString("ZN_AMOUNT");
				}
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.getZoneCharges-->"+sqex.getMessage());
			//	System.out.println("TDSException ZoneDAO.getZoneCharges" + sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("ZoneDAO.getZoneCharges End--->"+(System.currentTimeMillis()-startTime));
		return charge;
	}

	public static int deletezoneRate(String  assocode,String flagKey,String ezone)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		TDSConnection dbCon = new TDSConnection();
		Connection con = dbCon.getConnection();
		int result=0;
		try{

			m_pst =  con.prepareStatement("DELETE FROM TDS_ZONE_RATE_SETUP WHERE ZN_ASSOCODE='"+assocode+"' AND ZN_STARTZONE='"+flagKey+"' AND ZN_ENDZONE='"+ezone+"'");
			m_pst.execute();
			result=1;
			//System.out.println("m-pst"+m_pst);
		} catch (Exception sqex){
			cat.error("TDSException RegistrationDAO.deletezoneRate-->"+sqex.getMessage());
			//System.out.println("TDSException RegistrationDAO.deletezoneRate-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbCon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static ArrayList<QueueCoordinatesBO> getzoneList(String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<QueueCoordinatesBO> latitudeAndLongitude = new ArrayList<QueueCoordinatesBO>();

		TDSConnection m_tdsConn =  new TDSConnection();
		Connection m_con =  m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_pst = m_con.prepareStatement("select * from TDS_QUEUE_DETAILS where QD_ASSOCCODE='"+assoccode+"'");
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				QueueCoordinatesBO zonelist= new QueueCoordinatesBO();
				zonelist.setQueueId(m_rs.getString("QD_QUEUENAME"));
				zonelist.setQDescription(m_rs.getString("QD_DESCRIPTION"));;
				latitudeAndLongitude.add(zonelist);
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException ServiceRequestDAO.getAccountList->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.getAccountList-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return latitudeAndLongitude;
	}

	public static ArrayList<ZoneRateBean> getZoneRate(String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.getZoneRate");
		ArrayList<ZoneRateBean> al_list = new ArrayList<ZoneRateBean>();
		PreparedStatement m_pst = null;
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();

		try {

			m_pst = m_conn.prepareStatement("select * from TDS_ZONE_RATE_SETUP where ZN_ASSOCODE='"+assocode+"'");
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();

			while(rs.next())
			{
				ZoneRateBean znRateBean = new ZoneRateBean();	
				znRateBean.setAssocode(rs.getString("ZN_ASSOCODE"));
				znRateBean.setStartzone(rs.getString("ZN_STARTZONE"));
				znRateBean.setEndzone(rs.getString("ZN_ENDZONE"));
				znRateBean.setAmount(rs.getString("ZN_AMOUNT"));
				al_list.add(znRateBean);
			}


		}catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.getZoneRate-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.getZoneRate-->"+sqex.getMessage());
			sqex.printStackTrace();


		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static void insertZoneRateSetup(ArrayList<ZoneRateBean> al_list,String assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertZoneRateSetup");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {



			m_pst = m_conn.prepareStatement("insert into TDS_ZONE_RATE_SETUP (ZN_ASSOCODE,ZN_STARTZONE,ZN_ENDZONE,ZN_AMOUNT,ZN_ENTERED_TIME)values(?,?,?,?,now())");

			for(int i=0;i<al_list.size();i++)
			{
				if(!(al_list.get(i).getStartzone().equals("") && al_list.get(i).getEndzone().equals(""))){

					m_pst.setString(1, al_list.get(i).getAssocode());
					m_pst.setString(2, al_list.get(i).getStartzone());
					m_pst.setString(3, al_list.get(i).getEndzone());
					m_pst.setString(4, al_list.get(i).getAmount());
					cat.info(m_pst.toString());
					m_pst.execute();
				}
			}
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.insertZoneRateSetup-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException RegistrationDAO.insertZoneRateSetup-->"+sqex.getMessage());
			sqex.printStackTrace();

			try
			{
				m_conn.rollback();
			} catch (Exception e1) {
				// TODO: handle exception
			}
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}


	public static ArrayList<DriverCabQueueBean> getZoneActivity(String queueName, String assocode, String masterAssocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ZoneDAO.getZoneActivity");
		ArrayList<DriverCabQueueBean> al_list = new ArrayList<DriverCabQueueBean>();
		PreparedStatement m_pst = null;
		TDSConnection  m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();

		try {
			m_pst = m_conn.prepareStatement("SELECT QU_DRIVERID,QU_VEHICLE_NO,QU_QUEUEPOSITION FROM TDS_QUEUE WHERE QU_NAME =? AND QU_ASSOCCODE=? AND QU_MASTER_ASSOCCODE=? ORDER BY QU_QUEUEPOSITION");
			m_pst.setString(1, queueName);
			m_pst.setString(2, assocode);
			m_pst.setString(3, masterAssocode);
			cat.info(m_pst.toString());
			//System.out.println("query:"+m_pst.toString());
			ResultSet rs = m_pst.executeQuery();
			int zonepos=1;
			while(rs.next())
			{
				DriverCabQueueBean queueDetails = new DriverCabQueueBean();	
				queueDetails.setDriverid(rs.getString("QU_DRIVERID"));
				queueDetails.setVehicleNo(rs.getString("QU_VEHICLE_NO"));
				queueDetails.setZonepos(zonepos+"");
				al_list.add(queueDetails);
				zonepos++;
			}
		}catch (Exception sqex){
			cat.error("TDSException ZoneDAO.getZoneActivity-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ZoneDAO.getZoneActivity-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}


	public static ArrayList<QueueBean> getDriverZoneActivity(AdminRegistrationBO adminBO, String zoneId, String driverId, String fdate, String tdate) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<QueueBean> dzaBO = new ArrayList<QueueBean>();

		TDSConnection m_tdsConn =  new TDSConnection();
		Connection m_con 		=  m_tdsConn.getConnection();
		PreparedStatement m_pst =  null;
		ResultSet m_rs 			=  null;
		String query			=  "";
	  //String query1="";

		try {
			query = "SELECT *,DATE_FORMAT(CONVERT_TZ(DZH_LOGINTIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS DZH_LOGINTIME_FORMATTED,DATE_FORMAT(CONVERT_TZ(DZH_LOGOUTTIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS DZH_LOGOUTTIME_FORMATTED FROM TDS_DRIVER_ZONE_HISTORY WHERE DZH_ASSOCCODE='"+adminBO.getAssociateCode()+"'";
			if(zoneId!=null && !zoneId.equals("")){
				query = query + " AND DZH_ZONEID='"+zoneId+"'";
			}
			if(driverId!=null && !driverId.equals("")){
				query = query + " AND DZH_DRIVERID='"+driverId+"'";
			}
			if(!fdate.equals("") && !tdate.equals("")){
				query=query+" AND DZH_UPDATED_TIME <=CONVERT_TZ(STR_TO_DATE('"+tdate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') "
						+ " AND DZH_LOGINTIME >=CONVERT_TZ(STR_TO_DATE('"+fdate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') ";
			}
			query = query +"ORDER BY DZH_UPDATED_TIME DESC";
			m_pst = m_con.prepareStatement(query);
		
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				QueueBean zonelist= new QueueBean();
				zonelist.setDriverId(m_rs.getString("DZH_DRIVERID"));
				zonelist.setQU_NAME(m_rs.getString("DZH_ZONEID"));
				zonelist.setDq_LoginTime(m_rs.getString("DZH_LOGINTIME_FORMATTED"));
				zonelist.setDq_LogoutTime((m_rs.getString("DZH_LOGOUTTIME")!=null && !m_rs.getString("DZH_LOGOUTTIME").equals("") && !m_rs.getString("DZH_LOGOUTTIME").equals("0000-00-00 00:00:00"))?m_rs.getString("DZH_LOGOUTTIME_FORMATTED"):"");				
				zonelist.setVehicleNo(m_rs.getString("DZH_CABNO"));
				zonelist.setStatus(m_rs.getString("DZH_SWITCH"));
				zonelist.setDq_Reason(m_rs.getString("DZH_REASON"));
				//m_rs.getString("")
				dzaBO.add(zonelist);
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException zoneDAO.getDriverZoneActivity->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException zoneDAO.getDriverZoneActivity-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return dzaBO;
	}
	
	
	public static ArrayList<QueueBean> getDriverActivity(AdminRegistrationBO adminBO, String driverId, String fdate, String tdate) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<QueueBean> dzaBO = new ArrayList<QueueBean>();

		TDSConnection m_tdsConn =  new TDSConnection();
		Connection m_con =  m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String query="";
		
		try {
			query= "SELECT DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS STARTIME, DATE_FORMAT(CONVERT_TZ(ORH_JOB_COMPLETE_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS ENDTIME, concat('JOB ID ', ORH_TRIP_ID) AS DESCRIP, 1 AS querystatus FROM TDS_OPENREQUEST_HISTORY where orh_ASSOCCODE='"+adminBO.getAssociateCode()+"'" ;
			
			if(driverId!=null && !driverId.equals("") ){
				query= query + " AND ORH_DRIVERID='"+driverId+"'";
			}
			if(!fdate.equals("") && !tdate.equals("")){
				query= query +" AND ORH_JOB_COMPLETE_TIME <=CONVERT_TZ(STR_TO_DATE('"+tdate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') "
						+ " AND ORH_JOB_ACCEPT_TIME >=CONVERT_TZ(STR_TO_DATE('"+fdate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') ";
			}
			
			query= query + " union SELECT DATE_FORMAT(CONVERT_TZ(LO_LOGINTIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS STARTIME,DATE_FORMAT(CONVERT_TZ(LO_logout,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS ENDTIME, concat('CAB NO ', LO_CABNO)  AS DESCRIP, 2 AS querystatus FROM TDS_LOGINDETAILS WHERE LO_ASSCODE='"+adminBO.getAssociateCode()+"'";
			if(driverId!=null && !driverId.equals("")){
				query= query + " AND LO_DriverID='"+driverId+"'";
			}
			if(!fdate.equals("") && !tdate.equals("")){
				query= query +" AND LO_logout <=CONVERT_TZ(STR_TO_DATE('"+tdate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') "
						+ " AND LO_LOGINTIME >=CONVERT_TZ(STR_TO_DATE('"+fdate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') ";
			}
			
			query = query + " union SELECT DATE_FORMAT(CONVERT_TZ(DZH_LOGINTIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS STARTIME, DATE_FORMAT(CONVERT_TZ(DZH_LOGOUTTIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS ENDTIME, DZH_REASON AS DESCRIP, 3 AS querystatus FROM TDS_DRIVER_ZONE_HISTORY WHERE DZH_ASSOCCODE='"+adminBO.getAssociateCode()+"'";
			if(driverId!= null && !driverId.equals("")){
				query= query + " AND DZH_DRIVERID='"+driverId+"'";
			}
			if(!fdate.equals("") && !tdate.equals("")){
				query= query +" AND DZH_UPDATED_TIME <=CONVERT_TZ(STR_TO_DATE('"+tdate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') "
						+ " AND DZH_LOGINTIME >=CONVERT_TZ(STR_TO_DATE('"+fdate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') ";
			}
			
			
			query= query +"ORDER BY startime DESC";
			m_pst = m_con.prepareStatement(query);
			System.out.println(m_pst);

			cat.info(m_pst.toString());
			
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {

				System.out.println("");
			   if ((m_rs.getString("STARTIME")==null || m_rs.getString("STARTIME").equals("")) && (m_rs.getString("ENDTIME")==null || m_rs.getString("ENDTIME").equals("")) && (m_rs.getString("DESCRIP")==null || m_rs.getString("DESCRIP").equals(""))){
				   continue;
			   }
			   QueueBean zonelist= new QueueBean();
			   int querystatus = m_rs.getInt("querystatus"); 
			 //  System.out.println("query status:"+querystatus);
				zonelist.setQuerystatus(""+querystatus);
				if (querystatus==1)
				{
					//System.out.println("1 executed");
					zonelist.setORH_JOB_ACCEPT_TIME((m_rs.getString("STARTIME")!=null && !m_rs.getString("STARTIME").equals(""))?m_rs.getString("STARTIME"):"N/A");
					zonelist.setORH_JOB_COMPLETE_TIME((m_rs.getString("ENDTIME")!=null && !m_rs.getString("ENDTIME").equals(""))?m_rs.getString("ENDTIME"):"N/A");
					zonelist.setORH_REASON((m_rs.getString("DESCRIP")!=null && !m_rs.getString("DESCRIP").equals(""))?m_rs.getString("DESCRIP"):"N/A");

				}
				//else if(m_rs.getInt("querystatus")=='2' && m_rs.getString("STARTIME")!=null && m_rs.getString("ENDTIME")!=null && m_rs.getString("DESCRIP")!=null)
				else if(querystatus==2)
				{
					//System.out.println("2 executed");
					zonelist.setLO_Logintime((m_rs.getString("STARTIME")!=null && !m_rs.getString("STARTIME").equals(""))?m_rs.getString("STARTIME"):"N/A");
					zonelist.setLO_LogoutTime((m_rs.getString("ENDTIME")!=null && !m_rs.getString("ENDTIME").equals(""))?m_rs.getString("ENDTIME"):"N/A");
					zonelist.setLO_REASON((m_rs.getString("DESCRIP")!=null && !m_rs.getString("DESCRIP").equals(""))?m_rs.getString("DESCRIP"):"N/A");

				}
			  //else if (m_rs.getString("STARTIME")!=null && m_rs.getString("ENDTIME")!=null && m_rs.getString("DESCRIP")!=null)
				else
				{
					//System.out.println("3 executed");
				zonelist.setDq_LoginTime((m_rs.getString("STARTIME")!=null && !m_rs.getString("STARTIME").equals(""))?m_rs.getString("STARTIME"):"N/A");
				zonelist.setDq_LogoutTime((m_rs.getString("ENDTIME")!=null && !m_rs.getString("ENDTIME").equals(""))?m_rs.getString("ENDTIME"):"N/A");
				zonelist.setDq_Reason((m_rs.getString("DESCRIP")!=null && !m_rs.getString("DESCRIP").equals(""))?m_rs.getString("DESCRIP"):"N/A");

				}
			
				dzaBO.add(zonelist);
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException zoneDAO.getDriverActivity->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException zoneDAO.getDriverActivity-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally 
		{
			m_tdsConn.closeConnection();
		}
		
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return dzaBO;
	}
	
	public static ArrayList<QueueBean> getDriverWorkingHours(AdminRegistrationBO adminBO, String driverId, String fdate, String tdate) 
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<QueueBean> dzaBO = new ArrayList<QueueBean>();

		TDSConnection m_tdsConn =  new TDSConnection();
		Connection m_con =  m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String query="";

		try {
			query = "SELECT DATE_FORMAT(CONVERT_TZ(ORH_JOB_PICKUP_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS STARTTIME,DATE_FORMAT(CONVERT_TZ(ORH_JOB_COMPLETE_TIME,'UTC','"+adminBO.getTimeZone()+"'),'%m/%d/%Y %H:%i:%s') AS ENDTIME, ORH_TRIP_ID AS TRIPID, TIMEDIFF(ORH_JOB_COMPLETE_TIME,ORH_JOB_PICKUP_TIME) AS TIMEDIFF FROM TDS_OPENREQUEST_HISTORY WHERE ORH_ASSOCCODE='"+adminBO.getAssociateCode()+"'";
			if(driverId!=null && !driverId.equals("")){
				query = query + " AND ORH_DRIVERID='"+driverId+"'";
			}
			if(!fdate.equals("") && !tdate.equals("")){
				query=query+" AND ORH_JOB_COMPLETE_TIME <=CONVERT_TZ(STR_TO_DATE('"+tdate+" 23:59:59', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') "
						+ " AND ORH_JOB_PICKUP_TIME >=CONVERT_TZ(STR_TO_DATE('"+fdate+" 00:00:01', '%Y-%m-%d %H:%i:%s'),'"+adminBO.getTimeZone()+"','UTC') ";
			}
			query = query +"ORDER BY ORH_TRIP_ID DESC";
			m_pst = m_con.prepareStatement(query);
			System.out.println(m_pst);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next()) {
				
				QueueBean zonelist= new QueueBean();
				zonelist.setORH_JOB_PICKUP_TIME((m_rs.getString("STARTTIME")!=null && !m_rs.getString("STARTTIME").equals(""))?m_rs.getString("STARTTIME"):"N/A");
				zonelist.setORH_JOB_COMPLETE_TIME((m_rs.getString("ENDTIME")!=null && !m_rs.getString("ENDTIME").equals(""))?m_rs.getString("ENDTIME"):"N/A");
				zonelist.setORH_TRIP_ID((m_rs.getString("TRIPID")!=null && !m_rs.getString("TRIPID").equals(""))?m_rs.getString("TRIPID"):"N/A");
				zonelist.setORH_TIME_DIFF((m_rs.getString("TIMEDIFF")!=null && !m_rs.getString("TIMEDIFF").equals(""))?m_rs.getString("TIMEDIFF"):"N/A");
				
				/*try {
					SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
					timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

					Date date1 = timeFormat.parse("0:00:00");
					Date date2 = timeFormat.parse("TIMEDIFF");

					long sum = date1.getTime() + date2.getTime();

					String date3 = timeFormat.format(new Date(sum));
					System.out.println("The sum is "+date3);
				    
				} catch (ParseException e) {
				   
				   e.printStackTrace();
				  }
				
				zonelist.setSum_ORH_TIME_DIFF(m_rs.getString("date3"));*/
				
				dzaBO.add(zonelist);
				
			}
		} catch (SQLException sqex) {
			
			cat.error("TDSException zoneDAO.getDriverZoneActivity->"+sqex.getMessage());
			cat.error(m_pst.toString());
			System.out.println("TDSException zoneDAO.getDriverZoneActivity-->"+sqex.getMessage());
			sqex.printStackTrace();
		}
		
		finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return dzaBO;
	}
	

	public static ArrayList<Double> readMaxMinPoints(String assocode){
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ArrayList<Double> values = new ArrayList<Double>();
		try{
			pst = con.prepareStatement("SELECT MAX(QB_LATITUDE) as NLat,MIN(QB_LATITUDE) as SLat,MAX(QB_LONGITUDE)as ELon,MIN(QB_LONGITUDE)as WLon FROM TDS_QUEUE_BOUNDRIES WHERE  QB_SEQUENCE >= 0 AND QB_ASSOCCODE='" + assocode + "' AND QB_QUEUENAME != '000'");
			rs = pst.executeQuery();
			while(rs.next()){
				values.add(Double.parseDouble(rs.getString("ELon")));
				values.add(Double.parseDouble(rs.getString("WLon")));
				values.add(Double.parseDouble(rs.getString("NLat")));
				values.add(Double.parseDouble(rs.getString("SLat")));
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.readQueueBoundries-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		return values; 
	}
	
	public static ArrayList<QueueBean> getDriverPositionByTime(String zoneNum,String historyDate,String time,AdminRegistrationBO adminBo){
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String date[] = historyDate.split("/");
		String processedDate=date[2]+"-"+date[0]+"-"+date[1]+" "+time.substring(0,2)+":"+time.substring(2,4)+":"+"00";
		ArrayList<QueueBean> resultPositions = new ArrayList<QueueBean>();
		try{
			pst = con.prepareStatement("SELECT *,DATE_FORMAT(CONVERT_TZ(DZH_LOGINTIME,'UTC','"+adminBo.getTimeZone()+"'),'%Y/%m/%d %h:%i %p') AS RESULTDATE FROM TDS_DRIVER_ZONE_HISTORY LEFT JOIN TDS_QUEUE_DETAILS ON QD_QUEUENAME=DZH_ZONEID AND QD_ASSOCCODE=DZH_MASTER_ASSOCCODE WHERE DZH_ZONEID='"+zoneNum+"' AND CONVERT_TZ(DZH_LOGINTIME,'UTC','"+adminBo.getTimeZone()+"') <'"+processedDate+"' AND CONVERT_TZ(DZH_LOGOUTTIME,'UTC','"+adminBo.getTimeZone()+"') >'"+processedDate+"' AND DZH_MASTER_ASSOCCODE='"+adminBo.getMasterAssociateCode()+"' ORDER BY DZH_LOGINTIME");
			rs = pst.executeQuery();
			int postions = 1;
			while(rs.next()){
				QueueBean myQueue = new QueueBean();
				myQueue.setDriverId(rs.getString("DZH_DRIVERID"));
				myQueue.setVehicleNo(rs.getString("DZH_CABNO"));
				myQueue.setQueueDescription(rs.getString("QD_DESCRIPTION"));
				myQueue.setFlg(postions++);
				myQueue.setDq_LoginTime(rs.getString("RESULTDATE"));
				resultPositions.add(myQueue);
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException ZoneDAO.readQueueBoundries-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		return resultPositions; 
	
	}	
}
