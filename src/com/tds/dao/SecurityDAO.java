package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.common.util.TDSConstants;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.security.bean.TagSystemBean;
import com.tds.security.bean.UserAccessRights;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.FleetBO;
import com.tds.util.TDSSQLConstants;

public class SecurityDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(SecurityDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+SecurityDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}
	public static void setUserAccess(String userid,ArrayList al_access){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO SecurityDAO.setUserAccess");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try{


			con.setAutoCommit(false);

			pst = con.prepareStatement("delete from TDS_SECURITY_ACCESS where S_UID='"+userid+"'");
			cat.info(pst.toString());
			pst.execute();

			for(int i=0;i<al_access.size();i++){
				UserAccessRights rights = (UserAccessRights)al_access.get(i);
				pst=con.prepareStatement("insert into TDS_SECURITY_ACCESS values(?,?,?)");
				pst.setString(1, userid);
				pst.setString(2, rights.getModule_no());
				pst.setInt(3, rights.isAcces()?1:0);
				cat.info(pst.toString());
				pst.execute();
			}

			con.commit();
			con.setAutoCommit(true);
		}
		catch(SQLException sqex) {
			cat.error("TDSException SecurityDAO.setUserAccess-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException SecurityDAO.setUserAccess-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			if(con!=null){
				try{
					con.rollback();
				} catch (SQLException e) {
					// TODO: handle exception
				}
			}
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));


	}
	public static void insertTagDetail(String assocode,TagSystemBean tsBean){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO SecurityDAO.InsertTagDetail");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement(TDSSQLConstants.insertTagSystem);
			pst.setString(1,assocode);
			pst.setString(2,tsBean.getPassword());
			pst.setString(3,tsBean.getStartDate());
			pst.setString(4,tsBean.getToDate());
			pst.setString(5,tsBean.getUserId());
			cat.info(pst.toString());
			pst.executeUpdate();
		}catch(SQLException sqex)
		{
			cat.error("TDSException SecurityDAO.InsertTagDetail-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.print("TDSException SecurityDAO.InsertTagDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));


	}
	public static void insertTagId(String assocode,TagSystemBean tsBean,int num){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO SecurityDAO.InsertTagDetail");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			//System.out.println("assocode-->"+assocode);
			if(num==1){
				pst=con.prepareStatement("INSERT INTO TDS_TAG_COOKIE(TC_ASSOCODE,TC_TAG_ID,TC_START_DATE,TC_TO_DATE,TC_USERID) VALUES(?,?,date_format(NOW(),'%Y-%m-%d'),'9999-12-31','')");
				pst.setString(1,assocode);
				pst.setString(2,tsBean.getTagId());
			}
			else{
				pst=con.prepareStatement("INSERT INTO TDS_TAG_COOKIE(TC_ASSOCODE,TC_TAG_ID,TC_START_DATE,TC_TO_DATE,TC_USERID)  VALUES(?,?,?,?,?)");
				pst.setString(1,tsBean.getAssoCode());
				pst.setString(2,tsBean.getTagId());
				pst.setString(3,tsBean.getStartDate());
				pst.setString(4,tsBean.getToDate());
				pst.setString(5,tsBean.getUserId());

			}
			cat.info(pst.toString());
			pst.executeUpdate();
		}catch(SQLException sqex)
		{
			cat.error("TDSException SecurityDAO.InsertTagDetail-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.print("TDSException SecurityDAO.InsertTagDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));


	}
	public static TagSystemBean comparePassword(TagSystemBean tsBean){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO SecurityDAO.comparePassword");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		tsBean.setPassW(false);
		String query="SELECT TS_PASSWORD, TS_START_DATE, TS_TO_DATE,TS_VALIDITY_DAYS FROM TDS_TAG_SYSTEM WHERE TS_PASSWORD=? AND TS_START_DATE < NOW() AND TS_TO_DATE > NOW() AND TS_ASSOCODE=? AND TS_USERID=?";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement(query);
			pst.setString(1,tsBean.getPassword());
			pst.setString(2,tsBean.getAssoCode());
			pst.setString(3,tsBean.getUserId());
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if (rs.next()){
				tsBean.setPassW(true);
				tsBean.setStartDate(rs.getString("TS_START_DATE"));
				tsBean.setToDate(rs.getString("TS_TO_DATE"));
				tsBean.setValidityDays(rs.getInt("TS_VALIDITY_DAYS"));
				tsBean.setUserId(rs.getString("TS_USERID"));
			}
		}catch(SQLException sqex)
		{
			cat.error("TDSException SecurityDAO.comparePassword-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.print("TDSException SecurityDAO.comparePassword-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return tsBean;
	}
	public static void deletePassKey(String assocode,TagSystemBean tsBean){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO SecurityDAO.DeletePassKey");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("DELETE  FROM TDS_TAG_SYSTEM WHERE TS_PASSWORD=?");
			pst.setString(1,tsBean.getPassword());
			cat.info(pst.toString());
			pst.executeUpdate();
		}catch(SQLException sqex)
		{
			cat.error("TDSException SecurityDAO.DeletePassKey-->"+sqex.getMessage());
			cat.error(pst.toString());
			System.out.print("TDSException SecurityDAO.DeletePassKey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}

		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	public static boolean compareCookie(String tagId, String associationCode,String userId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.compareCookie");
		TDSConnection dbConn = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean validTagID = false;

		dbConn = new TDSConnection();
		con = dbConn.getConnection();
		try {
			pst=con.prepareStatement("SELECT TC_TAG_ID FROM TDS_TAG_COOKIE WHERE  TC_TAG_ID=? AND TC_ASSOCODE = ? AND TC_START_DATE < NOW() AND TC_TO_DATE > NOW() AND (TC_USERID='"+userId+"' OR TC_USERID='')");
			pst.setString(1,tagId);
			pst.setString(2, associationCode);
			cat.info(pst.toString());
			rs=pst.executeQuery();

			if(rs.next()){
				validTagID = true;
			}
		}
		catch (Exception e) {
			cat.error("TDSException RegistrationDAO.compareCookie-->"+e.getMessage());
			cat.error(pst.toString());
			System.out.print("TDSException RegistrationDAO.compareCookie-->"+e.getMessage());
			e.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return validTagID; 
	}
	public static boolean compareIPAddress(String ipAdd, String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO SecurityDAO.compareIPAddress");
		TDSConnection dbConn = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean validIPAddress = false;

		dbConn = new TDSConnection();
		con = dbConn.getConnection();
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_IPADDRESS WHERE  IP_ADDRESS=? AND IP_ASSOCCODE = ? AND STR_TO_DATE(IP_EXPIREDATE,'%m/%d/%Y') >=DATE_FORMAT(NOW(),'%Y-%m-%d')");
			pst.setString(1,ipAdd);
			pst.setString(2, associationCode);
			cat.info(pst.toString());
			rs=pst.executeQuery();

			if(rs.next()){
				validIPAddress = true;
			}
		}
		catch (Exception e) {
			cat.error("TDSException SecurityDAO.compareIPAddress-->"+e.getMessage());
			cat.error(pst.toString());
			System.out.print("TDSException SecurityDAO.compareIPAddress-->"+e.getMessage());
			e.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return validIPAddress; 
	}
	public static int insertIPAdd(String[] IPAdd,String[] ExpireDate,int size,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.insertIPAdd method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(IPAdd[1]!=""){
				for(int i=1;i<=size;i++){

					pst= con.prepareStatement("INSERT INTO TDS_IPADDRESS(IP_ADDRESS,IP_EXPIREDATE,IP_ASSOCCODE)VALUES(?,?,?) ");
					pst.setString(1, IPAdd[i]);
					pst.setString(2,ExpireDate[i]);
					pst.setString(3,adminBO.getAssociateCode());
					pst.execute();
					result=1;
				}
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.insertIPAdd--->"+sqex.getMessage());
			cat.error(pst.toString());
			cat.error("TDSException SecurityDAO.insertIPAdd--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int deleteIPAddress(String ipAddress,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.deleteIPAdd method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="DELETE FROM TDS_IPADDRESS WHERE IP_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND IP_ADDRESS='"+ipAddress+"'";
		try {
			pst=con.prepareStatement(query);
			pst.execute();
			result=1;

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.deleteIPAdd--->"+sqex.getMessage());
			cat.error(pst.toString());
			cat.error("TDSException SecurityDAO.deleteIPAdd--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static ArrayList<TagSystemBean>  getIPAdd(String associationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.getIPAdd method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<TagSystemBean> al_list=new ArrayList<TagSystemBean>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="SELECT * FROM TDS_IPADDRESS WHERE IP_ASSOCCODE='"+associationCode+"' ";
		try {
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			while(rs.next()){
				TagSystemBean tsBean =new TagSystemBean();
				tsBean.setIpAddress(rs.getString("IP_ADDRESS"));
				tsBean.setExpDate(rs.getString("IP_EXPIREDATE"));
				al_list.add(tsBean);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.getIPAdd--->"+sqex.getMessage());
			cat.error(pst.toString());
			cat.error("TDSException SecurityDAO.getIPAdd--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static int deleteDocumentType(String serialNumber,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.deleteDocumentType method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="DELETE FROM TDS_DOCUMENTS_MASTER  WHERE DM_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND MASTER_ID='"+serialNumber+"'";
		try {
			pst=con.prepareStatement(query);
			pst.execute();
			result=1;

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.deleteDocumentType--->"+sqex.getMessage());
			cat.error(pst.toString());
			cat.error("TDSException SecurityDAO.deleteDocumentType--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static void copyUserAccess(String newUserid,String userid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO SecurityDAO.setUserAccess");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			con.setAutoCommit(false);
			pst = con.prepareStatement("DELETE FROM TDS_SECURITY_ACCESS where S_UID='"+newUserid+"'");
			cat.info(pst.toString());
			pst.execute();
			pst=con.prepareStatement("INSERT INTO TDS_SECURITY_ACCESS  ( SELECT '"+newUserid+"',S_MODULE_NO,S_ACESS FROM TDS_SECURITY_ACCESS WHERE S_UID='"+userid+"')");
			cat.info(pst.toString());
			pst.execute();
			con.commit();
			con.setAutoCommit(true);
		}
		catch(SQLException sqex) {
			cat.error("TDSException SecurityDAO.setUserAccess-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException SecurityDAO.setUserAccess-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			if(con!=null){
				try{
					con.rollback();
				} catch (SQLException e) {
					// TODO: handle exception
				}
			}
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}
	public static int getFleetAccess(AdminRegistrationBO adminBO,String fleet) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.getFleetAccess method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_FLEET WHERE FL_USER_ID='"+adminBO.getUid()+"' AND FL_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND FL_ASSOCCODE='"+fleet+"'");
			//System.out.println("query--->"+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				result=1;
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.getFleetAccess--->"+sqex.getMessage());
			cat.error("TDSException SecurityDAO.getFleetAccess--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static AdminRegistrationBO getFleet(String userId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.getFleetAccess method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = null ;
		AdminRegistrationBO adminBO=new AdminRegistrationBO();
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		boolean adminUserIsNotEmpty = false;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="SELECT AU_USERTYPE,AU_MASTER_ASSOCCODE,AU_SNO,AU_USERNAME,AU_USERTYPE,AU_ACCESS,AU_PASSWORD,AU_PARENTCOMPANY,AU_PAYMENTTYPE,AU_ASSOCCODE,AU_OPENREQENTRY,AU_DRIVERREGENTRY,AU_CREATEVOUCHER,AU_ACCEPTOPENREQ,AU_CHANGEOPENREQ,AU_USERREG,AU_OPENREQXML,AU_ACCEPTREQXML,AU_TRIPSUMMARYREPORT,AU_USERLIST,AU_DRIVERSUMMARY,AU_COMPANYMAS,AU_ASSOCCODE,AU_CHANGEVOUCHER,AU_VOUCHERREPORT,AU_DRIVERBEHAVIOR,AU_BEHAVIORSUMMARY,AU_MAINTENANCEENTRY,AU_MAINTENANCESUMMARY,AU_LOSTANDFOUNDENTRY,AU_LOSTANDFOUNDSUMMARY,AU_QUEUECOORDINATEENTRY,AU_QUEUECOORDINATESUMMARY,AU_DRIVERPAYMENTREPORT,AU_COMPANYPAYMENTREPORT,AU_DRIVERAVAILABILITY,CD_SERVICE_KEY,CD_MERCHANTPROFILE,CD_SERVICEID,AU_FNAME,AU_LNAME FROM TDS_ADMINUSER u,TDS_COMPANYDETAIL c WHERE c.CD_ASSOCCODE = u.AU_ASSOCCODE and AU_USERNAME = ? AND AU_ACTIVE = 1";
		try{
			pst = con.prepareStatement(query);
			pst.setString(1, userId);
			//System.out.println("query-->"+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				adminBO.setUname(rs.getString("AU_USERNAME"));
				adminBO.setUserNameDisplay(rs.getString("AU_FNAME").concat(" "+rs.getString("AU_LNAME"))); 
				adminBO.setUid(rs.getString("AU_SNO"));
				//adminBO.setPhnumber(phno);
				//adminBO.setProvider(provider);
				adminBO.setPaymentType(rs.getString("AU_PAYMENTTYPE"));
				adminBO.setPcompany(rs.getString("AU_PARENTCOMPANY"));
				adminBO.setUsertype(rs.getString("AU_USERTYPE"));
				adminBO.setUsertypeDesc(rs.getString("AU_USERTYPE"));
				adminBO.setAssociateCode(rs.getString("AU_ASSOCCODE"));
				adminBO.setMasterAssociateCode(rs.getString("AU_MASTER_ASSOCCODE"));
				//System.out.println("Association code "+rs.getString("AU_ASSOCCODE")+" "+adminBO.getAssociateCode());
				/*adminBO.setAcceptopenreq(rs.getString("AU_ACCEPTOPENREQ"));
			adminBO.setOpenreqentry(rs.getString("AU_OPENREQENTRY"));
			adminBO.setDriverreg(rs.getString("AU_DRIVERREGENTRY"));
			adminBO.setCreatevoucher(rs.getString("AU_CREATEVOUCHER"));
			adminBO.setChangeopenreq(rs.getString("AU_CHANGEOPENREQ"));
			adminBO.setUserreg(rs.getString("AU_USERREG"));
			adminBO.setOpenreqxml(rs.getString("AU_OPENREQXML"));
			adminBO.setAcceptreqxml(rs.getString("AU_ACCEPTREQXML"));
			adminBO.setTripsummaryrep(rs.getString("AU_TRIPSUMMARYREPORT"));
			adminBO.setUserlist(rs.getString("AU_USERLIST"));
			adminBO.setDriversummary(rs.getString("AU_DRIVERSUMMARY"));
			adminBO.setCompanymaster(rs.getString("AU_COMPANYMAS"));
			adminBO.setChangeVoucher(rs.getString("AU_CHANGEVOUCHER"));
			adminBO.setVoucherReport(rs.getString("AU_VOUCHERREPORT"));
			adminBO.setBehavior(rs.getString("AU_DRIVERBEHAVIOR"));
			adminBO.setBehaviorSummary(rs.getString("AU_BEHAVIORSUMMARY"));
			adminBO.setMaintenance(rs.getString("AU_MAINTENANCEENTRY"));
			adminBO.setMaintenanceSummary(rs.getString("AU_MAINTENANCESUMMARY"));
			adminBO.setLostandfound(rs.getString("AU_LOSTANDFOUNDENTRY"));
			adminBO.setLostandfoundSummary(rs.getString("AU_LOSTANDFOUNDSUMMARY"));
			adminBO.setQueuecoordinate(rs.getString("AU_QUEUECOORDINATEENTRY"));
			adminBO.setQueuecoordinateSummary(rs.getString("AU_QUEUECOORDINATESUMMARY"));
			adminBO.setDriverAvailability(rs.getString("AU_DRIVERAVAILABILITY"));
			adminBO.setDriverPaymentReport(rs.getString("AU_DRIVERPAYMENTREPORT"));
			adminBO.setCompanyPaymentReport(rs.getString("AU_COMPANYPAYMENTREPORT"));*/
				adminBO.setServiceKey(rs.getString("CD_SERVICE_KEY"));
				adminBO.setPassword(rs.getString("AU_PASSWORD"));
				adminBO.setMerchid(rs.getString("CD_MERCHANTPROFILE"));
				adminBO.setServiceid(rs.getString("CD_SERVICEID"));
				adminBO.setIsAvailable(1); 
				//Following line picks up the default state where the company operates to enable doing address
				//verification in the OpenRequest/Job screen
				String place=SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(),"State" );
				adminBO.setState(place);
				double timeZoneOffset = 0.0;
				adminBO.setTimeZone(SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(),"timeZoneArea" ));
				try {
					timeZoneOffset = Double.parseDouble(SystemPropertiesDAO.getParameter(adminBO.getMasterAssociateCode(),"TimeZone" ));
				} catch (Exception e){
					e.printStackTrace();
				}
				adminBO.setTimeZoneOffet(timeZoneOffset);
				adminUserIsNotEmpty = true;
			}
			if(adminUserIsNotEmpty){

				pst = con.prepareStatement("select * from TDS_SECURITY_ACCESS where S_UID= ? and S_ACESS=1");
				pst.setString(1, rs.getString("AU_SNO"));
				cat.info(pst.toString());

				rs = pst.executeQuery(); 
				if(rs.first()) {
					ArrayList roles= new ArrayList(); 
					do
					{
						roles.add(rs.getString("S_MODULE_NO"));
					}while(rs.next());
					adminBO.setRoles(roles);
				}

			}
		}catch(Exception sqex){
			cat.error(pst.toString());

		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return adminBO;
	}
	public static int updateDriverFleet(String userId,String fleet,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.updateDriverFleet method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon =  new TDSConnection() ;
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		ResultSet rs=null;
		try {
			pst=con.prepareStatement("SELECT * from TDS_DRIVERLOCATION  WHERE DL_DRIVERID='"+userId+"' AND DL_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND DL_SWITCH='Y' ");
			rs=pst.executeQuery();
			if(rs.next()){
				return -1;
			}
			pst=con.prepareStatement("UPDATE TDS_ADMINUSER SET AU_ASSOCCODE='"+fleet+"' WHERE AU_SNO='"+userId+"' AND AU_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND AU_ASSOCCODE='"+adminBO.getAssociateCode()+"'" );
			result=pst.executeUpdate();
			if(result==1){
				result=0;
				pst=con.prepareStatement("UPDATE TDS_DRIVER SET DR_ASSOCODE='"+fleet+"' WHERE DR_SNO='"+userId+"' AND DR_ASSOCODE='"+adminBO.getAssociateCode()+"'" );
				result=pst.executeUpdate();
			}
			if(result==1){
				result=0;
				pst=con.prepareStatement("SELECT * from TDS_DRIVERLOCATION  WHERE DL_DRIVERID='"+userId+"' AND DL_ASSOCCODE='"+adminBO.getAssociateCode()+"'" );
				rs=pst.executeQuery();
				if(rs.next()){
					pst=con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_ASSOCCODE='"+fleet+"' WHERE DL_DRIVERID='"+userId+"' AND DL_ASSOCCODE='"+adminBO.getAssociateCode()+"'" );
					result=pst.executeUpdate();
				}else{
					result=1;
				}
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.updateDriverFleet--->"+sqex.getMessage());
			cat.error("TDSException SecurityDAO.updateDriverFleet--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int updateCabFleet(String cabNumber,String fleet,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.updateCabFleet method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon =  new TDSConnection() ;
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		try {
			pst=con.prepareStatement("UPDATE TDS_VEHICLE SET V_ASSOCCODE='"+fleet+"' WHERE V_VKEY='"+cabNumber+"' AND V_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"'" );
			result=pst.executeUpdate();
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.updateCabFleet--->"+sqex.getMessage());
			cat.error("TDSException SecurityDAO.updateCabFleet--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static ArrayList<FleetBO> getFleetListForOperator(ArrayList<AdminRegistrationBO> operatorList,String masterAssociationCode,String fleet) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); 
		cat.info("In SecurityDAO.getFleetListForOperator method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs;
		ArrayList<FleetBO> fleetList= new ArrayList<FleetBO>();
		StringBuffer operators=new StringBuffer();
		if(operatorList.size()==0){
			return fleetList;
		}else{
			for(int i=0;i<operatorList.size()-1;i++){
				operators.append("'"+operatorList.get(i).getUid()+"',");
			}
			operators.append("'"+operatorList.get(operatorList.size()-1).getUid()+"'");
		}
		try {

			pst=con.prepareStatement("SELECT * FROM TDS_FLEET WHERE FL_USER_ID IN ("+operators+") AND FL_MASTER_ASSOCCODE='"+masterAssociationCode+"'");
			rs=pst.executeQuery();
			while(rs.next()){
				FleetBO fleetBO=new FleetBO();
				fleetBO.setUserId(rs.getString("FL_USER_ID"));
				fleetBO.setFleetNumber(rs.getString("FL_ASSOCCODE"));
				fleetList.add(fleetBO);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.getFleetListForOperator--->"+sqex.getMessage());
			cat.error("TDSException SecurityDAO.getFleetListForOperator--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return fleetList;
	}
	public static int setDriverStatus(String status,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET  DL_SWITCH = ? WHERE DL_DRIVERID = ? AND DL_ASSOCCODE = ?");
			pst.setString(1, status);
			pst.setString(2, adminBO.getUid());
			pst.setString(3, adminBO.getAssociateCode());
			cat.info("UpdateDriverLocation-->"+pst);
			result = pst.executeUpdate();
			if(result<1){
				pst = con.prepareStatement("INSERT INTO TDS_DRIVERLOCATION (DL_DRIVERID,DL_SWITCH) VALUES (?,?)");
				//System.out.println("Query "+pst);
				pst.setString(1, adminBO.getUid());
				pst.setString(2,status);
				cat.info("InsertDriverLocation-->"+pst.toString());
				result = pst.executeUpdate();
			}
			pst.close();
			con.close();
		} catch (SQLException sqex) {
			//cat not defined
			cat.error("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ServiceRequestDAO.updateDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int getAlarmTime(String tripId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); 
		cat.info("In SecurityDAO.getAlarmTime method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs;
		int alarmTime=0;
		String query="";
		query="SELECT TIMESTAMPDIFF(MINUTE,NOW(),OR_SERVICEDATE) as time FROM TDS_OPENREQUEST WHERE OR_TRIP_ID='"+tripId+"'";
		try {
			pst=con.prepareStatement(query);
			//System.out.println("query--->"+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				alarmTime=rs.getInt("time");	
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.getAlarmTime--->"+sqex.getMessage());
			cat.error("TDSException SecurityDAO.getAlarmTime--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return alarmTime;
	}
	public static boolean getDriverAvailabilityStatus(AdminRegistrationBO adminBO, int alarmInMinutes) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs;
		boolean available=true;
		String query="";
		query="SELECT OR_SERVICEDATE,TIMESTAMPDIFF(MINUTE,NOW(),OR_SERVICEDATE) as time FROM TDS_OPENREQUEST WHERE OR_DRIVERID='"+adminBO.getUid()+"' AND OR_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND TIMESTAMPDIFF(MINUTE,NOW(),OR_SERVICEDATE)< '"+ alarmInMinutes+"' AND OR_TRIP_STATUS > 39 AND OR_TRIP_STATUS < 50  ORDER BY time asc LIMIT 0,1";
		try {
			pst=con.prepareStatement(query);
			rs=pst.executeQuery();
			if(rs.next()){
				//				if(rs.getInt("time") > 0){
				available=false;	
				//				}
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.getDriverAvailabilityStatus--->"+sqex.getMessage());
			cat.error("TDSException SecurityDAO.getDriverAvailabilityStatus--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return available;
	}

	public static String getAlarmTimeForDriverId(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); 
		cat.info("In SecurityDAO.getAlarmTimeForDriverId method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs;
		String alarmTime=null;
		String query="";
		query="SELECT OR_SERVICEDATE,TIMESTAMPDIFF(MINUTE,NOW(),OR_SERVICEDATE) as time FROM TDS_OPENREQUEST WHERE OR_DRIVERID='"+adminBO.getUid()+"' AND OR_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' AND OR_TRIP_STATUS NOT IN ('"+TDSConstants.tripCompleted+"','"+TDSConstants.paymentReceived+"') ORDER BY time asc LIMIT 0,1";
		try {
			pst=con.prepareStatement(query);
			rs=pst.executeQuery();
			if(rs.next()){        
				alarmTime=rs.getString("time");    
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.getAlarmTime--->"+sqex.getMessage());
			cat.error("TDSException SecurityDAO.getAlarmTime--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return alarmTime;
	}
	public static String fleetOfUser(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In SecurityDAO.fleetOfUser method ");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		String assoccode="";
		ResultSet rs;
		String extraData = "";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_FLEET WHERE FL_USER_ID='"+adminBO.getUid()+"' LIMIT 0,1");
			rs=pst.executeQuery();
			if(rs.next()){
				assoccode=rs.getString("FL_ASSOCCODE");
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SecurityDAO.fleetOfUser--->"+sqex.getMessage());
			cat.error("TDSException SecurityDAO.fleetOfUser--->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return assoccode;
	}
	public static ArrayList<String> getAllAssoccode(String masterCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.readFleetsForCompany");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<String> al_fleet = new ArrayList<String>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT FM_FLEET_NAME FROM TDS_FLEET_MASTER_ACCESS WHERE FM_MASTER_ASSOCCODE='"+masterCode+"'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				al_fleet.add(rs.getString("FM_FLEET_NAME"));
			}
			if(al_fleet.size()<=0){
				al_fleet.add(masterCode);
			}
			pst = con.prepareStatement("SELECT SF_MASTER_CODE FROM TDS_SUPER_FLEET WHERE SF_FLEET_CODE='"+masterCode+"'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				al_fleet.add(rs.getString("SF_MASTER_CODE"));
			}
			pst = con.prepareStatement("SELECT SF_FLEET_CODE FROM TDS_SUPER_FLEET WHERE SF_MASTER_CODE='"+masterCode+"'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()){
				al_fleet.add(rs.getString("SF_FLEET_CODE"));
			}
		} catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.readFleetsForCompany-->"+sqex.getMessage());
			//System.out.println("TDSException RequestDAO.readFleetsForCompany-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_fleet;
	}



}
