package com.tds.dao;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.common.util.TDSConstants;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.MeterStatusBO;
public class MeterDAO {
	private static Category cat =TDSController.cat;
	static {
		cat = Category.getInstance(MeterDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+MeterDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static int meterStatusInsert(MeterStatusBO meterBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);


		int status=0;
		TDSConnection dbcon = null;
		Connection con = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		PreparedStatement pst= null;

		try {	
			//Just Location Feed
			if(meterBO.getOp().equals("3")){
				pst = con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_LATITUDE=?,DL_LONGITUDE=? where DL_DRIVERID=?");
				pst.setString(1, meterBO.getLatitude()==null?"0.000000":meterBO.getLatitude());
				pst.setString(2, meterBO.getLongitude()==null?"0.000000":meterBO.getLongitude());
				pst.setString(3, meterBO.getDriverId());
				status=pst.executeUpdate();
				if(status==0){
					pst = con.prepareStatement("INSERT INTO TDS_DRIVERLOCATION (DL_DRIVERID,DL_LATITUDE,DL_LONGITUDE) VALUES(?,?,?)");
					pst.setString(1, meterBO.getDriverId());
					pst.setString(2, meterBO.getLatitude()==null?"0.000000":meterBO.getLatitude());
					pst.setString(3, meterBO.getLongitude()==null?"0.000000":meterBO.getLongitude());
					status=pst.executeUpdate();
				}
			}else if(meterBO.getOp().equals("2")){
				//Feed for Meter Off
				pst = con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_LATITUDE=?,DL_LONGITUDE=?,DL_SWITCH=? WHERE DL_DRIVERID=?");
				pst.setString(1, meterBO.getLatitude());
				pst.setString(2, meterBO.getLongitude());
				pst.setString(3, "Y");
				pst.setString(4, meterBO.getDriverId());
				status=pst.executeUpdate();
				if(status==0){
					pst = con.prepareStatement("INSERT INTO TDS_DRIVERLOCATION (DL_DRIVERID,DL_LATITUDE,DL_LONGITUDE,DL_SWITCH) VALUES(?,?,?,?)");
					pst.setString(1, meterBO.getDriverId());
					pst.setString(2, meterBO.getLatitude());
					pst.setString(3, meterBO.getLongitude());
					pst.setString(4, "Y");//Making driver Active
					status=pst.executeUpdate();
				}
			}else if(meterBO.getOp().equals("1")){
				//Feed for meter On
				pst = con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_LATITUDE=?,DL_LONGITUDE=?,DL_SWITCH=? WHERE DL_DRIVERID=?");
				pst.setString(1, meterBO.getLatitude());
				pst.setString(2, meterBO.getLongitude());
				pst.setString(3, "N");
				pst.setString(4, meterBO.getDriverId());
				status=pst.executeUpdate();
				if(status==0){
					pst = con.prepareStatement("INSERT INTO TDS_DRIVERLOCATION (DL_DRIVERID,DL_LATITUDE,DL_LONGITUDE,DL_SWITCH) VALUES(?,?,?,?)");
					pst.setString(1, meterBO.getDriverId());
					pst.setString(2, meterBO.getLatitude());
					pst.setString(3, meterBO.getLongitude());
					pst.setString(4, "N");
				}
			}
			status=pst.executeUpdate();
		} catch (Exception sqex) {
			cat.error("TDSException MeterDAO.meterStatusInsert--->"+sqex.getMessage());
			//System.out.println("TDSException MeterDAO.meterStatusInsert--->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	public static String getFlagTripByDriver(String driverId,String vehicleNum,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		String flag="";
		PreparedStatement pst= null;
		ResultSet rs = null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT OR_TRIP_ID FROM TDS_OPENREQUEST WHERE OR_MASTER_ASSOCCODE='"+assoccode+"' AND OR_DRIVERID='"+driverId+"' AND OR_VEHICLE_NO='"+vehicleNum+"' AND OR_TRIP_STATUS='"+TDSConstants.tripStarted+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				flag= rs.getString("OR_TRIP_ID");
			}
		} catch (Exception sqex){
			cat.error("TDSException MeterDAO.getFlagTripByDriver--->"+sqex.getMessage());
			//System.out.println("TDSException MeterDAO.getFlagTripByDriver--->"+sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
        return flag;
	}
	
}

