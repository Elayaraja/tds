package com.tds.dao;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.ios.push.CertificateBO;
import com.lowagie.text.pdf.codec.Base64;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.ClientAuthenticationBO;
import com.tds.tdsBO.CustomerMobileBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.TDSConstants;
import com.tds.util.TDSSQLConstants;

public class CustomerMobileDAO {

	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(CustomerMobileDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+CustomerMobileDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static CustomerMobileBO customerLogin(CustomerMobileBO CMBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerLogin");
		//int result =0;
		CustomerMobileBO customerBO= null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM CLNT_CLIENTUSER WHERE CCU_USERID=? AND CCU_PASSWORD=? AND CCU_ASSOCIATIONCODE=?");
			pst.setString(1, CMBO.getUserId());
			pst.setString(2, CMBO.getPassword());
			pst.setString(3, CMBO.getAssoccode());
			//System.out.println(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				customerBO = new CustomerMobileBO();
				customerBO.setName(rs.getString("CCU_USERNAME"));
				customerBO.setPhoneNumber(rs.getString("CCU_PHONE"));
				customerBO.setEmailId(rs.getString("CCU_EMAIL"));
				customerBO.setUserId(rs.getString("CCU_USERID"));
				customerBO.setAssoccode(rs.getString("CCU_ASSOCIATIONCODE"));
				customerBO.setCustomerUniqueId(rs.getString("CCU_SNO"));
				customerBO.setVerified(rs.getString("CCU_LOGIN").equals("1")?true:false);
				customerBO.setBlocked(rs.getInt("CCU_DEACTIVATE")==1?true:false);
			}else{
				pst = con.prepareStatement("SELECT * FROM CLNT_CLIENTUSER WHERE CCU_USERID=? AND CCU_ASSOCIATIONCODE=?");
				pst.setString(1, CMBO.getUserId());
				pst.setString(2, CMBO.getAssoccode());
				//System.out.println(pst.toString());
				rs=pst.executeQuery();
				if(rs.next()){
					customerBO = new CustomerMobileBO();
				}
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerLogin-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerLogin-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return customerBO;
	}

	public static int customerRegistrationForLogin(CustomerMobileBO CMBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerRegistrationForLogin");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst0=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			
			CustomerMobileBO cus_details = customerUserIdVerification(CMBO.getUserId(), CMBO.getAssoccode());
			if(cus_details!=null){
				return 2;
			}
			
			pst = con.prepareStatement("INSERT INTO CLNT_CLIENTUSER (CCU_USERNAME,CCU_EMAIL,CCU_PHONE,CCU_USERID,CCU_PASSWORD,CCU_ASSOCIATIONCODE,CCU_FORGOT_KEY,CCU_APP_SOURCE,CCU_LOGIN) values(?,?,?,?,?,?,?,?,?) ");
			pst.setString(1, CMBO.getUserName());
			pst.setString(2, CMBO.getEmailId());
			pst.setString(3, CMBO.getPhoneNumber());
			pst.setString(4, CMBO.getUserId());
			pst.setString(5, CMBO.getPassword());
			pst.setString(6, CMBO.getAssoccode());
			pst.setString(7, "");
			pst.setString(8, CMBO.getApp_Source());
			pst.setString(9, "0");
			result=pst.executeUpdate();

			pst.close();
			con.close();
		}catch (Exception sqex) {
			result=0;
			cat.error("TDSException CustomerMobileDAO.customerRegistrationForLogin-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerRegistrationForLogin-->"+sqex.getMessage());
			//sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static CustomerMobileBO customerUserIdVerification(String userID,String cCode){
		cat.info("TDS INFO CustomerMobileDAO.customerUserIdVerification");
		CustomerMobileBO cmBo =null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM CLNT_CLIENTUSER WHERE CCU_ASSOCIATIONCODE=? and CCU_USERID=? ");
			pst.setString(1, cCode);
			pst.setString(2, userID);
			rs = pst.executeQuery();
			if(rs.next()){
				cmBo = new CustomerMobileBO();
				cmBo.setUserName(rs.getString("CCU_SNO"));
				cmBo.setPhoneNumber(rs.getString("CCU_PHONE"));
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerUserIdVerification-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerUserIdVerification-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		return cmBo;
	}

	public static int customerRegeneratedPasswordKeyUpdate(String cCode, String userID, String key){
		cat.info("TDS INFO CustomerMobileDAO.customerRegeneratedPasswordKeyUpdate");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {

			pst = con.prepareStatement("UPDATE CLNT_CLIENTUSER SET CCU_FORGOT_KEY=? WHERE CCU_ASSOCIATIONCODE=? AND CCU_USERID=? ");
			pst.setString(1, key);
			pst.setString(2, cCode);
			pst.setString(3, userID);
			result=pst.executeUpdate();

			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerRegeneratedPasswordKeyUpdate-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerRegeneratedPasswordKeyUpdate-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		return result;
	}

	public static int customerSubmitPasswordresttingKey(String cCode, String userID, String key){
		cat.info("TDS INFO CustomerMobileDAO.customerSubmitPasswordresttingKey");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null, pst1 = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			String update_pst = "UPDATE CLNT_CLIENTUSER SET CCU_LOGIN='1' WHERE CCU_SNO=?";
			pst = con.prepareStatement("SELECT * FROM CLNT_CLIENTUSER WHERE CCU_ASSOCIATIONCODE=? AND CCU_USERID=? AND CCU_FORGOT_KEY=? ");
			pst.setString(1, cCode);
			pst.setString(2, userID);
			pst.setString(3, key);
			rs=pst.executeQuery();
			if(rs.next()){
				result=1;
				pst1 = con.prepareStatement(update_pst);
				pst1.setInt(1, rs.getInt("CCU_SNO"));
				pst1.executeUpdate();
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerSubmitPasswordresttingKey-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerSubmitPasswordresttingKey-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		return result;
	}

	public static int changeCustomerPasswordByPRSK(String cCode, String userID, String key, String password){
		cat.info("TDS INFO CustomerMobileDAO.changeCustomerPasswordByPRSK");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {

			pst = con.prepareStatement("UPDATE CLNT_CLIENTUSER SET CCU_PASSWORD=? WHERE CCU_ASSOCIATIONCODE=? AND CCU_USERID=? AND CCU_FORGOT_KEY=? ");
			pst.setString(1, password);
			pst.setString(2, cCode);
			pst.setString(3, userID);
			pst.setString(4, key);
			result = pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.changeCustomerPasswordByPRSK-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.changeCustomerPasswordByPRSK-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		return result;
	}


	public static int changeCustomerPasswordByOldPasssword(CustomerMobileBO CMBO, String newPassword){
		cat.info("TDS INFO CustomerMobileDAO.changeCustomerPasswordByOldPasssword");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {

			pst = con.prepareStatement("UPDATE CLNT_CLIENTUSER SET CCU_PASSWORD=? WHERE CCU_ASSOCIATIONCODE=? AND CCU_USERID=? AND CCU_PASSWORD=? ");
			pst.setString(1, newPassword);
			pst.setString(2, CMBO.getAssoccode());
			pst.setString(3, CMBO.getUserId());
			pst.setString(4, CMBO.getPassword());

			result = pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.changeCustomerPasswordByOldPasssword-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.changeCustomerPasswordByOldPasssword-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		return result;
	}

	public static int customerAccountRegistration(CustomerMobileBO CMBO,int mode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountRegistration");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(mode==1){
				pst = con.prepareStatement("INSERT INTO CLNT_PAYMENT (CCA_USERNAME,CCA_USERID,CCA_PAYMENTTYPE,CCA_CARD_NUMBER,CCA_CARD_EXPIRY_DATE,CCA_VOUCHER_NUMBER,CCA_VOUCHER_EXPIRY_DATE) values(?,?,?,?,?,?,?)  ");
				pst.setString(1, CMBO.getUserName());
				pst.setString(2, CMBO.getUserId());
				pst.setString(3, CMBO.getPaymentType());
				pst.setString(4, CMBO.getCardNumber());
				pst.setString(5, CMBO.getCardExpiryDate());
				pst.setString(6, CMBO.getVoucherNumber());
				pst.setString(7, CMBO.getVoucherExpiryDate());
				result=pst.executeUpdate();
			}else if(mode==2){
				pst = con.prepareStatement("UPDATE CLNT_PAYMENT SET CCA_USERNAME=?,CCA_PAYMENTTYPE=?,CCA_CARD_NUMBER=?,CCA_CARD_EXPIRY_DATE=?,CCA_VOUCHER_NUMBER=?,CCA_VOUCHER_EXPIRY_DATE=?  WHERE CCA_SNO=? AND CCA_USERID=?");
				pst.setString(1, CMBO.getUserName());
				pst.setString(2, CMBO.getPaymentType());
				pst.setString(3, CMBO.getCardNumber());
				pst.setString(4, CMBO.getCardExpiryDate());
				pst.setString(5, CMBO.getVoucherNumber());
				pst.setString(6, CMBO.getVoucherExpiryDate());
				pst.setString(7, CMBO.getSerialNo());
				pst.setString(8, CMBO.getUserId());
				result=pst.executeUpdate();
			}else if(mode==3){
				pst = con.prepareStatement("DELETE FROM CLNT_PAYMENT WHERE CCA_SNO=? AND CCA_USERID=? ");
				pst.setString(1, CMBO.getSerialNo());
				pst.setString(2, CMBO.getUserId());
				result=pst.executeUpdate();
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerAccountRegistration-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerAccountRegistration-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int tagAddress(CustomerMobileBO CMBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountRegistration");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("INSERT INTO CLNT_ADDRESS_TAGS (CADD_USER_ID,CADD_TAG_NAME,CADD_ADDRESS,CADD_LATITUDE,CADD_LONGITUDE,CADD_ASSOCCODE) values(?,?,?,?,?,?)  ");
			pst.setString(1, CMBO.getUserId());
			pst.setString(2, CMBO.getTagAddress());
			pst.setString(3, CMBO.getAddress());
			pst.setString(4, CMBO.getLatitude());
			pst.setString(5, CMBO.getLongitude());
			pst.setString(6, CMBO.getAssoccode());
			result=pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.tagAddress-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.tagAddress-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int checkAddress(CustomerMobileBO CMBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountRegistration");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM CLNT_ADDRESS_TAGS WHERE CADD_USER_ID=? AND CADD_ADDRESS=?");
			pst.setString(1, CMBO.getUserId());
			pst.setString(2, CMBO.getAddress());
			rs=pst.executeQuery();
			if(rs.next()){
				result=1;
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.checkAddress-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.checkAddress-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static ArrayList<CustomerMobileBO> taggedAddressestoShow(CustomerMobileBO CMBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountRegistration");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ArrayList<CustomerMobileBO> taggedAddressesList =new ArrayList<CustomerMobileBO>();
		try {
			pst = con.prepareStatement("SELECT * FROM CLNT_ADDRESS_TAGS WHERE CADD_USER_ID=? AND CADD_ASSOCCODE=? ");
			pst.setString(1, CMBO.getUserId());
			pst.setString(2, CMBO.getAssoccode());

			rs=pst.executeQuery();
			while(rs.next()){
				CustomerMobileBO addressesList = new CustomerMobileBO();
				addressesList.setAddress(rs.getString("CADD_ADDRESS"));
				addressesList.setTagAddress(rs.getString("CADD_TAG_NAME"));
				addressesList.setAddressKey(rs.getString("CADD_KEY"));
				addressesList.setLatitude(rs.getString("CADD_LATITUDE"));
				addressesList.setLongitude(rs.getString("CADD_LONGITUDE"));
				taggedAddressesList.add(addressesList);
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.taggedAddressestoShow-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.taggedAddressestoShow-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return taggedAddressesList;
	}
	public static int deleteFavAddress(String key,ClientAuthenticationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.deleteFavAddress");
		TDSConnection dbcon =  new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		try {
			pst = con.prepareStatement("DELETE FROM CLNT_ADDRESS_TAGS WHERE CADD_KEY=? AND CADD_USER_ID=? AND CADD_ASSOCCODE=? ");
			pst.setString(1, key);
			pst.setString(2, adminBO.getUserId());
			pst.setString(3, adminBO.getAssoccode());
			result=pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.deleteFavAddress-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.deleteFavAddress-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static ArrayList<CustomerMobileBO> customerAccountSummary(String userId,String serialNo,int mode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountSummary");
		ArrayList<CustomerMobileBO> customerAcctSummary = new ArrayList<CustomerMobileBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(mode==1){
				pst = con.prepareStatement("SELECT * FROM CLNT_PAYMENT WHERE CCA_USERID=? ");
				pst.setString(1, userId);
			}else{
				pst = con.prepareStatement("SELECT * FROM CLNT_PAYMENT WHERE CCA_SNO=? ");
				pst.setString(1, serialNo);
			}
			rs=pst.executeQuery();
			while(rs.next()){
				CustomerMobileBO customerAccountList = new CustomerMobileBO();
				customerAccountList.setPaymentType(rs.getString("CCA_PAYMENTTYPE"));
				customerAccountList.setCardNumber(rs.getString("CCA_CARD_NUMBER"));
				customerAccountList.setCardExpiryDate(rs.getString("CCA_CARD_EXPIRY_DATE"));
				customerAccountList.setVoucherNumber(rs.getString("CCA_VOUCHER_NUMBER"));
				customerAccountList.setVoucherExpiryDate(rs.getString("CCA_VOUCHER_EXPIRY_DATE"));
				customerAccountList.setSerialNo(rs.getString("CCA_SNO"));
				customerAcctSummary.add(customerAccountList);
			}

			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return customerAcctSummary;
	}
	public static ArrayList<CustomerMobileBO> getCharges(String tripId,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountSummary");
		ArrayList<CustomerMobileBO> customerAcctSummary = new ArrayList<CustomerMobileBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_DRIVER_CHARGE_CALC inner Join TDS_CHARGE_TYPE ON CT_KEY=DC_TYPE_CODE where DC_TRIP_ID='"+tripId+"' AND CT_ASSOCIATION_CODE='"+assoccode+"'");
			rs=pst.executeQuery();
			pst = con.prepareStatement("SELECT * FROM TDS_DRIVER_CHARGE_CALC inner Join TDS_CHARGE_TYPE ON CT_KEY=DC_TYPE_CODE where DC_TRIP_ID='"+tripId+"' AND CT_ASSOCIATION_CODE='"+assoccode+"'");
			rs=pst.executeQuery();
			while(rs.next()){
				CustomerMobileBO customerAccountList = new CustomerMobileBO();
				customerAccountList.setAmount(rs.getString("DC_AMOUNT"));
				customerAccountList.setPaymentType(rs.getString("CT_DESCRIPTION"));
				customerAccountList.setChargesTypeCode(rs.getString("DC_TYPE_CODE"));
				customerAcctSummary.add(customerAccountList);
			}

			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return customerAcctSummary;
	}
	public static int updateCharges(String tripId,String assoccode,String masterAssoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountSummary");
		//ArrayList<CustomerMobileBO> customerAcctSummary = new ArrayList<CustomerMobileBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		//ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int result=0;
		try {
			pst = con.prepareStatement("UPDATE TDS_DRIVER_CHARGE_CALC SET DC_DRIVER_PMT_STATUS=1 where DC_TRIP_ID='"+tripId+"' AND DC_ASSOC_CODE='"+assoccode+"' AND DC_MASTER_ASSOCCODE='"+masterAssoccode+"'");
			result=pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static CustomerMobileBO customerAccount(String serialNo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountSummary");
		CustomerMobileBO customerAccountList = new CustomerMobileBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {

			pst = con.prepareStatement("SELECT * FROM CLNT_PAYMENT WHERE CCA_SNO=? ");
			pst.setString(1, serialNo);
			rs=pst.executeQuery();
			if(rs.next()){
				customerAccountList = new CustomerMobileBO();
				customerAccountList.setName(rs.getString("CCA_USERNAME"));
				customerAccountList.setUserId(rs.getString("CCA_USERID"));
				customerAccountList.setPaymentType(rs.getString("CCA_PAYMENTTYPE"));
				customerAccountList.setCardNumber(rs.getString("CCA_CARD_NUMBER"));
				customerAccountList.setCardExpiryDate(rs.getString("CCA_CARD_EXPIRY_DATE"));
				customerAccountList.setVoucherNumber(rs.getString("CCA_VOUCHER_NUMBER"));
				customerAccountList.setVoucherExpiryDate(rs.getString("CCA_VOUCHER_EXPIRY_DATE"));
			}

			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return customerAccountList;
	}

	public static ArrayList<CustomerMobileBO> tripSummary(String userId, int src){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerAccountSummary");
		ArrayList<CustomerMobileBO> customerTripSummary = new ArrayList<CustomerMobileBO>();
		PreparedStatement pst = null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST WHERE OR_CREATED_BY=? AND OR_TRIP_SOURCE=? AND (OR_TRIP_STATUS=8 OR OR_TRIP_STATUS=37)");
			pst.setString(1, userId);
			pst.setInt(2, src);			
			rs=pst.executeQuery();
			while(rs.next()){
				CustomerMobileBO tripSummary = new CustomerMobileBO();
				tripSummary.setTripId(rs.getString("OR_TRIP_ID"));
				tripSummary.setPhoneNumber(rs.getString("OR_PHONE"));
				tripSummary.setAddress(rs.getString("OR_STADD1"));
				tripSummary.setCity(rs.getString("OR_STCITY"));
				tripSummary.setName(rs.getString("OR_NAME"));
				customerTripSummary.add(tripSummary);
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.customerAccountSummary-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return customerTripSummary;
	}

	public static ArrayList<CustomerMobileBO> tripHistory(String userId, String cCode,String timeZone){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.tripHistory");
		ArrayList<CustomerMobileBO> customerTripSummary = new ArrayList<CustomerMobileBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {

			pst = con.prepareStatement("SELECT *,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeZone+"'),'%m/%d/%Y %h:%i %p') AS FORMATEDDATE FROM TDS_OPENREQUEST_HISTORY WHERE ORH_CREATED_BY=? AND ORH_ASSOCCODE=? ORDER BY ORH_ENTEREDTIME DESC LIMIT 0,10 ");
			pst.setString(1, userId);
			pst.setString(2, cCode);
			rs=pst.executeQuery();
			while(rs.next()){
				CustomerMobileBO tripSummary = new CustomerMobileBO();
				tripSummary.setTripId(rs.getString("ORH_TRIP_ID"));
				tripSummary.setPhoneNumber(rs.getString("ORH_PHONE"));
				tripSummary.setAddress(rs.getString("ORH_STADD1"));
				tripSummary.setCity(rs.getString("ORH_STCITY")==null?"":rs.getString("ORH_STCITY"));
				tripSummary.setName(rs.getString("ORH_NAME"));
				tripSummary.setStLatitude(Double.toString(rs.getDouble("ORH_STLATITUDE")));
				tripSummary.setStLongitude(Double.toString(rs.getDouble("ORH_STLONGITUDE")));
				tripSummary.setEdAddress(rs.getString("ORH_EDADD1"));
				tripSummary.setEdLatitude(Double.toString(rs.getDouble("ORH_EDLATITUDE")));
				tripSummary.setEdLongitude(Double.toString(rs.getDouble("ORH_EDLONGITUDE")));
				tripSummary.setEdcity(rs.getString("ORH_EDCITY")==null?"":rs.getString("ORH_EDCITY"));
				tripSummary.setServiceDate(rs.getString("FORMATEDDATE"));
				customerTripSummary.add(tripSummary);
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.tripHistory-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.tripHistory-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return customerTripSummary;
	}
	public static ArrayList<CustomerMobileBO> getDriverLocation(String userId,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.getDriverLocation");
		ArrayList<CustomerMobileBO> driverLocation = new ArrayList<CustomerMobileBO>();
		TDSConnection dbcon =  new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs,rs1=null;
		String driverId="";
		String query="";
		if(!tripId.equals("")){
			query=" AND OR_TRIP_ID='"+tripId+"'";
		}
		try {
			pst = con.prepareStatement("SELECT OR_DRIVERID FROM TDS_OPENREQUEST WHERE OR_CREATED_BY=? AND (OR_TRIP_STATUS="+TDSConstants.driverReportedNoShow+" OR OR_TRIP_STATUS= "+TDSConstants.onRotueToPickup+" OR OR_TRIP_STATUS= "+TDSConstants.onSite+" OR OR_TRIP_STATUS= "+TDSConstants.jobSTC+" OR OR_TRIP_STATUS="+TDSConstants.tripStarted+"  OR OR_TRIP_STATUS="+TDSConstants.jobAllocated+")"+query);
			pst.setString(1, userId);
			//System.out.println(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				driverId=rs.getString("OR_DRIVERID");
				pst1=con.prepareStatement("SELECT * FROM TDS_DRIVERLOCATION LEFT JOIN TDS_VEHICLE ON DL_VEHICLE_NO=V_VNO AND DL_ASSOCCODE=V_MASTER_ASSOCCODE WHERE DL_DRIVERID=?");
				pst1.setString(1, driverId);

				rs1=pst1.executeQuery();
				if(rs1.next()){
					CustomerMobileBO latlong =new CustomerMobileBO();
					latlong.setStLatitude(Double.toString(rs1.getDouble("DL_LATITUDE")));
					latlong.setStLongitude(Double.toString(rs1.getDouble("DL_LONGITUDE")));
					latlong.setDriver(rs1.getString("DL_DRIVERID"));
					latlong.setVehicleNo(rs1.getString("DL_VEHICLE_NO"));
					latlong.setName(rs1.getString("DL_DRNAME"));
					latlong.setvMake(rs1.getString("V_VMAKE"));
					latlong.setvType(rs1.getString("V_MODEL"));
					driverLocation.add(latlong);
				}
			}

			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.getDriverLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.getDriverLocation-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return driverLocation;
	}

	public static OpenRequestBO getHistory(String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.getDriverLocation");
		OpenRequestBO jobHistory = new OpenRequestBO();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		try {
			pst = con.prepareStatement("SELECT ORH_DRIVERID,ORH_MASTER_ASSOCCODE,ORH_VEHICLE_NO,ORH_TRIP_STATUS,ORH_PAYTYPE,ORH_TRIP_ID FROM TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_ID='"+tripId+"'");
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				jobHistory.setStatus(rs.getString("ORH_TRIP_STATUS"));
				jobHistory.setDriverid(rs.getString("ORH_DRIVERID"));
				jobHistory.setVehicleNo(rs.getString("ORH_VEHICLE_NO"));
				jobHistory.setPaytype(rs.getString("ORH_PAYTYPE"));
				jobHistory.setTripid(rs.getString("ORH_TRIP_ID"));
				jobHistory.setAssociateCode(rs.getString("ORH_MASTER_ASSOCCODE"));
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.getJobLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.getJobLocation-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return jobHistory;

	}
	public static ArrayList<CustomerMobileBO> getJobLocation(String userId,String tripId,int src){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.getDriverLocation");
		ArrayList<CustomerMobileBO> jobLocation = new ArrayList<CustomerMobileBO>();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		String query="";
		if(!tripId.equals("")){
			query=" AND OR_TRIP_ID="+tripId;
		}
		try {
			pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_DRIVERID,OR_SERVICEDATE,OR_STADD1,OR_EDADD1,OR_VEHICLE_NO,OR_STLATITUDE,OR_STLONGITUDE,OR_EDLATITUDE,OR_EDLONGITUDE,OR_TRIP_STATUS,DL_LATITUDE,DL_LONGITUDE,DL_DRNAME,DL_PHONENO,V_RCNO,V_MODEL,V_VMAKE FROM TDS_OPENREQUEST LEFT JOIN TDS_DRIVERLOCATION ON OR_DRIVERID=DL_DRIVERID LEFT JOIN TDS_VEHICLE  ON OR_VEHICLE_NO=V_VNO AND DL_MASTER_ASSOCCODE=V_MASTER_ASSOCCODE WHERE OR_CREATED_BY=? "+query);
			pst.setString(1, userId);
			//System.out.println("pst:"+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				CustomerMobileBO latlong =new CustomerMobileBO();
				latlong.setStLatitude(Double.toString(rs.getDouble("OR_STLATITUDE")));
				latlong.setStLongitude(Double.toString(rs.getDouble("OR_STLONGITUDE")));
				latlong.setAddress(rs.getString("OR_STADD1"));
				latlong.setEdAddress(rs.getString("OR_EDADD1"));
				latlong.setEdLatitude(Double.toString(rs.getDouble("OR_EDLATITUDE")));
				latlong.setEdLongitude(Double.toString(rs.getDouble("OR_EDLONGITUDE")));
				latlong.setStatus(rs.getString("OR_TRIP_STATUS"));
				latlong.setTripId(rs.getString("OR_TRIP_ID"));
				latlong.setDriver(rs.getString("OR_DRIVERID"));
				latlong.setVehicleNo(rs.getString("OR_VEHICLE_NO"));
				latlong.setServiceDate(rs.getString("OR_SERVICEDATE"));
				if(rs.getString("OR_DRIVERID")!=null && !rs.getString("OR_DRIVERID").equals("")){
					latlong.setLatitude(rs.getString("DL_LATITUDE"));
					latlong.setLongitude(rs.getString("DL_LONGITUDE"));
					latlong.setName(rs.getString("DL_DRNAME"));
					latlong.setVehRegNum(rs.getString("V_RCNO"));
					latlong.setvType(rs.getString("V_MODEL"));
					latlong.setvMake(rs.getString("V_VMAKE"));
					latlong.setPhoneNumber(rs.getString("DL_PHONENO"));
				}
				jobLocation.add(latlong);
			}else if(!tripId.equals("")){
				pst = con.prepareStatement("SELECT ORH_DRIVERID,ORH_VEHICLE_NO,ORH_TRIP_STATUS,ORH_PAYTYPE FROM TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_ID='"+tripId+"'");
				cat.info(pst.toString());
				rs=pst.executeQuery();
				if(rs.next()){
					CustomerMobileBO latlong =new CustomerMobileBO();
					latlong.setStLatitude("");
					latlong.setStLongitude("");
					latlong.setStatus(rs.getString("ORH_TRIP_STATUS"));
					latlong.setDriver(rs.getString("ORH_DRIVERID"));
					latlong.setVehicleNo(rs.getString("ORH_VEHICLE_NO"));
					latlong.setPaymentType(rs.getString("ORH_PAYTYPE"));
					latlong.setTripId(tripId);
					jobLocation.add(latlong);
				}
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.getJobLocation-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.getJobLocation-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return jobLocation;
	}

	public static int userIdCheck(String userId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.userIdCheck");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {

			pst = con.prepareStatement("SELECT CCU_USERID FROM CLNT_CLIENTUSER WHERE CCU_USERID=?");
			pst.setString(1, userId);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				result=1;
			}

			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.userIdCheck-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.userIdCheck-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int updateGCMKey(String userId,String pushKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.updateGCMKey");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("UPDATE CLNT_CLIENTUSER SET CCU_PUSH_KEY=? WHERE CCU_USERID=?");
			pst.setString(1,pushKey);
			pst.setString(2, userId);
			cat.info(pst.toString());
			result=pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.updateGCMKey-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.updateGCMKey-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static String getPushKey(String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.getPushKey");
		String pushKey="";
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String userId="";
		try {
			pst=con.prepareStatement("SELECT OR_CREATED_BY FROM TDS_OPENREQUEST WHERE OR_TRIP_SOURCE=0 AND OR_TRIP_ID= "+tripId);
			rs=pst.executeQuery();
			if(rs.next()){
				userId=rs.getString("OR_CREATED_BY");
			}
			pst = con.prepareStatement("SELECT CCU_PUSH_KEY FROM CLNT_CLIENTUSER WHERE CCU_USERID=? ");
			pst.setString(1, userId);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				pushKey=rs.getString("CCU_PUSH_KEY");
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.getPushKey-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.getPushKey-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return pushKey;
	}
	public static ArrayList<CustomerMobileBO> getCreditCards(String userId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.getCreditCards");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ArrayList<CustomerMobileBO> custArray =new ArrayList<CustomerMobileBO>();
		try {
			pst = con.prepareStatement("SELECT CCA_CARD_NUMBER,CCA_SNO FROM CLNT_PAYMENT WHERE CCA_USERID=? AND CCA_PAYMENTTYPE='creditcard'");
			pst.setString(1,userId);
			rs=pst.executeQuery();
			if(rs.next()){
				CustomerMobileBO cmBO =new CustomerMobileBO();
				cmBO.setCardNumber(rs.getString("CCA_CARD_NUMBER"));
				cmBO.setSerialNo(rs.getString("CCA_SNO"));
				custArray.add(cmBO);
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.getPushKey-->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.getPushKey-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return custArray;
	}

	public static int cancelReservation(String userId,String trip, int src){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.cancelReservation");
		int status=0;
		PreparedStatement m_pst = null;
		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("Update TDS_OPENREQUEST SET OR_TRIP_STATUS="+TDSConstants.customerCancelledRequest+",OR_SERVICEDATE=NOW() WHERE OR_TRIP_ID = ? and  OR_CREATED_BY = '"+userId+"' and OR_TRIP_SOURCE=? ");			
			m_pst.setString(1, trip);
			m_pst.setInt(2, src);
			status = m_pst.executeUpdate();
			if(status==1){
				status=0;
				m_pst=m_conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_HISTORY  ( SELECT * FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = '"+trip+"'  and  OR_CREATED_BY = '"+userId+"' and OR_TRIP_SOURCE=? )");
				m_pst.setInt(1, src);
				status=m_pst.executeUpdate();
				if(status==1){
					status=0;
					m_pst=m_conn.prepareStatement("DELETE FROM TDS_OPENREQUEST WHERE OR_TRIP_ID= '"+trip+"'  and  OR_CREATED_BY = '"+userId+"' and OR_TRIP_SOURCE=? ");
					m_pst.setInt(1, src);
					status=m_pst.executeUpdate();
				}
			}
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.cancelReservation-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.cancelReservation-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}

	public static int updateJobByPassenger(String assoCode,String tripId,boolean status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement m_pst = null;
		TDSConnection m_tdsConn = new TDSConnection();
		Connection m_conn = m_tdsConn.getConnection();
		int result=0;
		try {
			if(status){
				m_pst = m_conn.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS='"+TDSConstants.performingDispatchProcesses+"',OR_DISPATCH_START_TIME=NOW() WHERE OR_TRIP_ID='"+tripId+"'");
				//System.out.println("Query--->"+m_pst);
				result=m_pst.executeUpdate();
			} else {
				m_pst = m_conn.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_TRIP_STATUS='"+TDSConstants.companyCouldntService+"' WHERE OR_TRIP_ID='"+tripId+"'");
				result=m_pst.executeUpdate();
				m_pst = m_conn.prepareStatement(TDSSQLConstants.moveOpenRequestToHisotry);
				m_pst.setString(1, tripId);
				m_pst.setString(2, assoCode);
				result=m_pst.executeUpdate();
				m_pst = m_conn.prepareStatement(TDSSQLConstants.deleteOpenRequest);
				m_pst.setString(1, tripId);
				m_pst.setString(2, assoCode);
				result=m_pst.executeUpdate();
			}
		} catch (Exception sqex) {
			cat.error(("TDSException CustomerMobileDAO.updateJobByPassenger-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
			//                System.out.println("TDSException CustomerMobileDAO.updateJobByPassenger-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static String getUserIdForThisCookie(String cookieValue){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.getUserIdForThisCookie");
		String userId="";
		PreparedStatement pst = null;
		TDSConnection dbConn  = new TDSConnection();
		Connection con = dbConn.getConnection();
		ResultSet rs=null;
		try {
			pst=con.prepareStatement("SELECT * from CLNT_CUSTOMER_COOKIES where CC_COOKIE='"+cookieValue+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				userId=rs.getString("CC_USER_ID");
			}
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.getUserIdForThisCookie-->"+sqex.getMessage());
			cat.error(pst.toString());
			//		System.out.println("TDSException CustomerMobileDAO.getUserIdForThisCookie-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return userId;
	}
	public static CustomerMobileBO customerLoginWithCookie(String userId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.customerLogin");
		//int result =0;
		CustomerMobileBO customerBO = new CustomerMobileBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM CLNT_CLIENTUSER WHERE CCU_USERID=?");
			pst.setString(1, userId);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				customerBO.setName(rs.getString("CCU_USERNAME"));
				customerBO.setPhoneNumber(rs.getString("CCU_PHONE"));
				customerBO.setEmailId(rs.getString("CCU_EMAIL"));
				customerBO.setUserId(rs.getString("CCU_USERID"));
				customerBO.setAssoccode(rs.getString("CCU_ASSOCIATIONCODE"));
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerLogin-->"+sqex.getMessage());
			cat.error(pst.toString());
			//		System.out.println("TDSException CustomerMobileDAO.customerLogin-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return customerBO;
	}

	public static int setCookieAndUserId(String fingerPrint,String userId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.setCookieAndUserId");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("INSERT INTO CLNT_CUSTOMER_COOKIES (CC_USER_ID,CC_COOKIE) values(?,?)  ");
			pst.setString(1, userId);
			pst.setString(2, fingerPrint);
			result=pst.executeUpdate();

		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.setCookieAndUserId-->"+sqex.getMessage());
			cat.error(pst.toString());
			//		System.out.println("TDSException CustomerMobileDAO.setCookieAndUserId-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int deleteCookieForUserId(String userId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.deleteCookieForUserId");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("DELETE FROM CLNT_CUSTOMER_COOKIES where CC_USER_ID=?");
			pst.setString(1, userId);
			result=pst.executeUpdate();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.deleteCookieForUserId-->"+sqex.getMessage());
			cat.error(pst.toString());
			//		System.out.println("TDSException CustomerMobileDAO.deleteCookieForUserId-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static int deleteOpenRequest(String trip,String p_assoccode,String old) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.deleteOpenRequest");
		int status=0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("Update TDS_OPENREQUEST SET OR_TRIP_STATUS="+TDSConstants.customerCancelledRequest+",OR_SERVICEDATE=NOW() WHERE OR_TRIP_ID = ? and  (OR_ASSOCCODE = '"+p_assoccode+"')");
			m_pst.setString(1, trip);
			status = m_pst.executeUpdate();
			if(status==1){
				m_pst=m_conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_HISTORY  ( SELECT * FROM TDS_OPENREQUEST WHERE OR_ASSOCCODE='"+p_assoccode+"' AND OR_TRIP_ID = '"+trip+"')");
				status=m_pst.executeUpdate();
				if(status==1){
					m_pst=m_conn.prepareStatement("DELETE FROM TDS_OPENREQUEST WHERE OR_ASSOCCODE='"+p_assoccode+"' AND OR_TRIP_ID= '"+trip+"'");
					status=m_pst.executeUpdate();
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException CustomerMobileDAO.deleteOpenRequest-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//		System.out.println("TDSException CustomerMobileDAO.deleteOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}	

	/*public static void getDriverDocument(String driverId) {
			TDSConnection dbcon = null;
			Connection con = null;
			PreparedStatement pst = null;
			ResultSet rs = null;
			CustomerMobileBO cmbo=new CustomerMobileBO();
			Blob blobData = null;
			String fileName = "";
			String query = "";
			//String driverId = request.getParameter("driverId") != null ? request.getParameter("driverId") : "";
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			try {
				if (!driverId.equals("")) {
					query = "SELECT DOC_DATA,DOCUMENT_NAME FROM TDS_DOCUMENTS WHERE DOC_TYPE = 'Driver Photo' AND DOC_UPLOADED_BY='"+driverId+"'";
					pst = con.prepareStatement(query);
					rs = pst.executeQuery();
					while (rs.next()) {
						blobData = rs.getBlob(1);
						fileName = rs.getString(2);
					}
					response.setContentType(getServletContext().getMimeType(fileName));
					OutputStream outStream = response.getOutputStream();
					InputStream in = blobData.getBinaryStream();
					int nextByte = in.read();
					while (nextByte != -1) {
						outStream.write(nextByte);
						nextByte = in.read();
					}
					outStream.flush();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/


	public static void customerLoginDetails(CustomerMobileBO CMBO, String sessionID, String regKey){
		//System.out.println("here");
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.CustomerLoginDetails");
		int result =0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null, chk_pst=null, up_pst = null;
		ResultSet chk_rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			chk_pst = con.prepareStatement("SELECT * FROM CLNT_CLIENT_LOGIN WHERE CLN_USER_ID='"+CMBO.getUserId()+"' AND CLN_SESSION_ID='"+sessionID+"' AND CLN_LOGOUT_TIME IS NULL");
			chk_rs = chk_pst.executeQuery();
			if(chk_rs.next()){
				System.out.println("Skipped to insert login again"+CMBO.getUserId());
				return;
			}
			
			int tripId = 0;
			String google_key = "";
			chk_pst = con.prepareStatement("SELECT * FROM CLNT_CLIENT_LOGIN WHERE CLN_USER_ID='"+CMBO.getUserId()+"' AND CLN_CCODE = '"+CMBO.getAssoccode()+"' AND CLN_LOGOUT_TIME IS NULL");
			chk_rs = chk_pst.executeQuery();
			if(chk_rs.next()){
				tripId = chk_rs.getInt("CLN_TRIPID");
				google_key = chk_rs.getString("CLN_GOOGLE_REG_KEY");
				up_pst = con.prepareStatement("UPDATE CLNT_CLIENT_LOGIN SET CLN_LOGOUT_TIME = NOW() WHERE CLN_USER_ID='"+CMBO.getUserId()+"' AND CLN_CCODE = '"+CMBO.getAssoccode()+"' AND CLN_LOGOUT_TIME IS NULL");
				//System.out.println("execute_update"+up_pst.toString());
				up_pst.executeUpdate();
			}
			
			pst = con.prepareStatement("INSERT INTO CLNT_CLIENT_LOGIN (CLN_USER_ID,CLN_NAME,CLN_PHONE_NO,CLN_SESSION_ID,CLN_LOGIN_TIME,CLN_GOOGLE_REG_KEY,CLN_TRIPID,CLN_CCODE,CLN_APP_SOURCE) values(?,?,?,?,NOW(),?,?,?,?) ");
			pst.setString(1, CMBO.getUserId());
			pst.setString(2, CMBO.getName());
			pst.setString(3, CMBO.getPhoneNumber());
			pst.setString(4, sessionID);
			pst.setString(5, google_key);
			pst.setInt(6, tripId);
			pst.setString(7, CMBO.getAssoccode());
			pst.setString(8, CMBO.getApp_Source());
			//System.out.println("customerlogindetails"+pst.toString());
			result=pst.executeUpdate();
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.customerLoginDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//		System.out.println("TDSException CustomerMobileDAO.customerloginDeatils-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}


	public static int updateGoogleKeyForCustomer(String key,String sessionId,String uid) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("UPDATE CLNT_CLIENT_LOGIN SET CLN_GOOGLE_REG_KEY='"+key+"' WHERE CLN_USER_ID='"+uid+"' AND CLN_SESSION_ID='"+sessionId+"'");
			cat.info(pst.toString());
			result=pst.executeUpdate();
		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.updateGoogleKey->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.updateGoogleKey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int updateCustomerLogoutTime(String sessionId,String uID, String ccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1=null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if((sessionId==null || sessionId.equals(""))){
				if(!ccode.equals("")){
					pst1 = con.prepareStatement("SELECT * FROM CLNT_CLIENT_LOGIN WHERE CLN_USER_ID ='"+uID+"' AND CLN_CCODE='"+ccode+"' ORDER BY CLN_LOGIN_TIME desc");
					//System.out.println("query with ccode:"+pst1.toString());
					rs= pst1.executeQuery();
					if(rs.next()){
						sessionId = rs.getString("CLN_SESSION_ID");
					}
				}else{
					pst1 = con.prepareStatement("SELECT * FROM CLNT_CLIENT_LOGIN WHERE CLN_USER_ID ='"+uID+"' ORDER BY CLN_LOGIN_TIME desc");
					//System.out.println("query without ccode:"+pst1.toString());
					rs= pst1.executeQuery();
					if(rs.next()){
						sessionId = rs.getString("CLN_SESSION_ID");
					}
				}
			}
			if(uID==null || uID.equals("")){
				pst=con.prepareStatement("UPDATE CLNT_CLIENT_LOGIN SET CLN_LOGOUT_TIME=NOW() WHERE CLN_SESSION_ID='"+sessionId+"'");
			}else{
				pst=con.prepareStatement("UPDATE CLNT_CLIENT_LOGIN SET CLN_LOGOUT_TIME=NOW() WHERE CLN_USER_ID ='"+uID+"' AND CLN_SESSION_ID='"+sessionId+"'");
			}

			cat.info(pst.toString());
			//System.out.println("query:"+pst.toString());
			result=pst.executeUpdate();
		} catch (SQLException sqex ) {
			//System.out.println("TDSException CustomerMobileDAO.updateGoogleKey-->"+sqex.getMessage());
			cat.error("TDSException CustomerMobileDAO.updateGoogleKey->"+sqex.getMessage());
			//cat.error(pst.toString());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static int updateTripIDCustomer(String uID,String sessionId, String tripID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("UPDATE CLNT_CLIENT_LOGIN SET CLN_TRIPID='"+tripID+"' WHERE CLN_USER_ID ='"+uID+"' AND CLN_SESSION_ID='"+sessionId+"'");
			cat.info(pst.toString());
			result=pst.executeUpdate();
		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.updateGoogleKey->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.updateGoogleKey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}

	public static DriverCabQueueBean getKeyForGooglePushById(OpenRequestBO openBO, String cCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		// TODO Auto-generated method stub
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		DriverCabQueueBean customerBO = new DriverCabQueueBean();
		try {
			if(openBO.getCreatedBy()!=null && !openBO.getCreatedBy().equals("")){
				pst=con.prepareStatement("SELECT * FROM CLNT_CLIENT_LOGIN WHERE CLN_TRIPID='"+openBO.getTripid()+"' AND CLN_USER_ID ='"+openBO.getCreatedBy()+"' GROUP BY CLN_GOOGLE_REG_KEY");
			}else{
				pst=con.prepareStatement("SELECT * FROM CLNT_CLIENT_LOGIN WHERE CLN_TRIPID='"+openBO.getTripid()+"' GROUP BY CLN_GOOGLE_REG_KEY" );
			}
			cat.info(pst.toString());
			rs = pst.executeQuery();
			//System.out.println("query"+pst.toString());
			while(rs.next()){
				customerBO.setUid(rs.getString("CLN_USER_ID"));
				customerBO.setUname(rs.getString("CLN_NAME"));
				customerBO.setGkey(rs.getString("CLN_GOOGLE_REG_KEY"));
				customerBO.setTripID(rs.getString("CLN_TRIPID"));
				customerBO.setAssocode(cCode);
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.getKeyForGooglePushById->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.updateGoogleKey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return customerBO;
	}

	public static void setReconnectKey(String uniqueKey, String userId, String password,String cmpCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		// TODO Auto-generated method stub
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int rs=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("INSERT INTO TDS_RECONNECT (RC_ID,RC_PASSWORD,RC_UNIQUE_KEY,RC_CCODE,RC_LOGIN_TIME) VALUES(?,?,?,?,NOW()) ");
			pst.setString(1, userId);
			pst.setString(2, password);
			pst.setString(3, uniqueKey);
			pst.setString(4, cmpCode);

			cat.info(pst.toString());
			rs = pst.executeUpdate();

		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.ReConnect->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.ReConnect-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	public static CustomerMobileBO checkReconnectKey(String reConnectKey) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		// TODO Auto-generated method stub
		CustomerMobileBO custBO=null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_RECONNECT WHERE RC_UNIQUE_KEY=?");
			pst.setString(1, reConnectKey);
			rs=pst.executeQuery();
			if(rs.next()){
				custBO = new CustomerMobileBO();
				custBO.setUserId(rs.getString("RC_ID"));
				custBO.setPassword(rs.getString("RC_PASSWORD"));
				custBO.setAssoccode(rs.getString("RC_CCODE"));
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.checkreconnectkey->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.checkreconnectkey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return custBO;
	}

	public static void updateReconnectKey(String uniqueKey, String reConnectKey) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		// TODO Auto-generated method stub
		CustomerMobileBO CMBO=new CustomerMobileBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("UPDATE TDS_RECONNECT SET RC_UNIQUE_KEY='"+uniqueKey+"' WHERE RC_UNIQUE_KEY='"+reConnectKey+"' ");

			pst.executeUpdate();
			/*while(rs.next()){
					CMBO.setUserId(rs.getString("RC_ID"));
					CMBO.setPassword(rs.getString("RC_PASSWORD"));
				}*/
		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.checkreconnectkey->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.checkreconnectkey-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	public static int driverrating(String driverID, String userId, String tripID, String rating, String comments,String ccode, String cabNo) {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int result = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("INSERT INTO TDS_CUSTOMER_DRIVER_RATING (CDR_DRIVERID,CDR_CUSTOMER_ID,CDR_TRIP_ID,CDR_RATING,CDR_COMMENTS,CDR_ASSOCCODE,CDR_CABNO) VALUES(?,?,?,?,?,?,?) ");
			pst.setString(1, driverID);
			pst.setString(2, userId);
			pst.setString(3, tripID);
			pst.setString(4, rating);
			pst.setString(5, comments);
			pst.setString(6, ccode);
			pst.setString(7, cabNo);
			cat.info(pst.toString());
			result = pst.executeUpdate();

			if(result>0){
				pst=con.prepareStatement("UPDATE TDS_DRIVER SET DR_RATING=(SELECT AVG(CDR_RATING) FROM TDS_CUSTOMER_DRIVER_RATING WHERE CDR_DRIVERID='"+driverID+"') WHERE DR_SNO='"+driverID+"'");
				pst.execute();
				
				pst=con.prepareStatement("UPDATE TDS_DRIVERLOCATION SET DL_DRIVER_RATING=(SELECT AVG(CDR_RATING) FROM TDS_CUSTOMER_DRIVER_RATING WHERE CDR_DRIVERID='"+driverID+"') WHERE DL_DRIVERID='"+driverID+"'");
				pst.execute();
			}

		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.driverrating->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;	
	}


	public static String getDriverDocument(String driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Blob blobData = null;
		String fileName = "";
		String query = "";
		String result="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			if (!driverId.equals("")) {
				query = "SELECT DOC_DATA,DOCUMENT_NAME FROM TDS_DOCUMENTS WHERE DOC_TYPE = 'Driver Photo' AND DOC_UPLOADED_BY='"+driverId+"'";
				pst = con.prepareStatement(query);
				rs = pst.executeQuery();
				//System.out.println("ResultSet"+rs.toString());
				while (rs.next()) {
					blobData = rs.getBlob(1);
					fileName = rs.getString(2);
				}
				if(blobData!=null){
					//System.out.println("ImageBlob"+blobData.toString());
					InputStream binaryStream = blobData.getBinaryStream();
					byte[] imageBytes = blobData.getBytes(1, (int) blobData.length());
					String s = new String(imageBytes); 
					String encode = Base64.encodeBytes(imageBytes);
					//System.out.println("String ensode"+encode);
					result = encode;
				}
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.getDriverDocument->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.getDriverDocument-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}		
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}


	public static CustomerMobileBO tripDetails(String tripid,String ccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = "";
		String result="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		CustomerMobileBO cmBo = null;
		try{
			query = "SELECT ORH_PAYMENT_STATUS,ORH_TRIP_ID,ORH_AMT,ORH_ASSOCCODE FROM TDS_OPENREQUEST_HISTORY WHERE ORH_ASSOCCODE=? AND ORH_TRIP_ID=?";
			pst = con.prepareStatement(query);
			pst.setString(1, ccode);
			pst.setString(2, tripid);
			rs = pst.executeQuery();
			//System.out.println("ResultSet"+rs.toString());
			if (rs.next()) {
				cmBo = new CustomerMobileBO();
				cmBo.setTripAMount(rs.getString("ORH_AMT"));
				cmBo.setTripPaymentStatus(rs.getInt("ORH_PAYMENT_STATUS"));
			}
		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.tripDetails->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.tripDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return cmBo;
	}

	public static void setTotalPaymentAmountToORH(String tripId, String amt, String cCode) {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		// TODO Auto-generated method stub
		CustomerMobileBO CMBO=new CustomerMobileBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("UPDATE TDS_OPENREQUEST_HISTORY SET ORH_AMT=? WHERE ORH_TRIP_ID=? AND ORH_ASSOCCODE=? ");
			pst.setString(1, amt);
			pst.setString(2, tripId);
			pst.setString(3, cCode);
			pst.executeUpdate();

		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.setTotalPaymentAmountToORH->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.setTotalPaymentAmountToORH-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}


	public static ArrayList<DriverCabQueueBean> getShortestDriver(String associationCode, double latitude, double longitude, double distanceInMiles)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		double milesPerDeg = 1/69.172; 
		double degressToOffset = distanceInMiles * milesPerDeg;

		double northLatitude = latitude + degressToOffset;
		double southLatitude = latitude - degressToOffset;

		double eastLongitude = longitude + degressToOffset;
		double westLongitude = longitude - degressToOffset;

		ArrayList<DriverCabQueueBean> hash_list = new ArrayList<DriverCabQueueBean>();
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("SELECT * FROM TDS_DRIVERLOCATION DL  WHERE " +
					"DL_ASSOCCODE='"+associationCode+ "' AND DL_LATITUDE BETWEEN "+southLatitude+" AND "+northLatitude+ 
					" AND DL_LONGITUDE BETWEEN "+westLongitude+" AND "+eastLongitude +" AND DL_SWITCH = 'Y' LIMIT 0,10");
			//System.out.println("query:"+m_pst.toString());
			ResultSet rs = m_pst.executeQuery();

			while(rs.next())
			{
				DriverCabQueueBean driver = new DriverCabQueueBean();
				driver.setDriverid(rs.getString("DL_DRIVERID"));
				driver.setCurrentLatitude(rs.getDouble("DL_LATITUDE"));
				driver.setCurrentLongitude(rs.getDouble("DL_LONGITUDE"));
				driver.setDrivername(rs.getString("DL_DRNAME"));
				hash_list.add(driver);
			}
		}catch (Exception sqex)
		{
			hash_list=null;
			cat.error("TDSException UtilityDAO.getCmpyVehicleFlag-->"+sqex.getMessage());
			//System.out.println("TDSException UtilityDAO.getCmpyVehicleFlag-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return hash_list;
	}

	public static CertificateBO getIos_CertificateDetails(String cCode) {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		// TODO Auto-generated method stub
		CertificateBO certBo = new CertificateBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT * FROM CLNT_IOS_CERTIFICATE_DETAILS wHERE CICD_ASSOCCODE=? ");
			pst.setString(1, cCode);

			rs = pst.executeQuery();
			if(rs.next()){
				certBo.setPath(rs.getString("CICD_CERTIFICATE_LOCATION"));
				certBo.setPassword(rs.getString("CICD_CERTIFICATE_PSWD"));
				certBo.setAssoccode(rs.getString("CICD_ASSOCCODE"));
			}

		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.getIos_CertificateDetails->"+sqex.getMessage());
			cat.error(pst.toString());
			//	System.out.println("TDSException CustomerMobileDAO.getIos_CertificateDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return certBo;
	}

	public static int updateCustomerProfile(String userId, String pswd, String ph, int app_Src, String email, String name, String ccode) {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		// TODO Auto-generated method stub
		CustomerMobileBO CMBO=new CustomerMobileBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int rs = 0 ;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("UPDATE CLNT_CLIENTUSER SET CCU_PHONE=?, CCU_USERNAME=?, CCU_EMAIL=? WHERE CCU_ASSOCIATIONCODE=? AND CCU_USERID=? AND CCU_PASSWORD=? ");
			pst.setString(1, ph);
			pst.setString(2, name);
			pst.setString(3, email);
			pst.setString(4, ccode);
			pst.setString(5, userId);
			pst.setString(6, pswd);
			pst.setInt(7, app_Src);

			//System.out.println("query:"+pst.toString());
			rs = pst.executeUpdate();

		} catch (SQLException sqex ) {
			cat.error("TDSException CustomerMobileDAO.setTotalPaymentAmountToORH->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CustomerMobileDAO.setTotalPaymentAmountToORH-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));	
		return rs;
	}
	
	public static int auto_cancelReservation(String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CustomerMobileDAO.auto_cancelReservation");
		int status=0;
		PreparedStatement m_pst = null;
		TDSConnection m_dbConn  = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("Update TDS_OPENREQUEST SET OR_TRIP_STATUS="+TDSConstants.companyCouldntService+",OR_SERVICEDATE=NOW() WHERE OR_TRIP_ID = ? ");			
			m_pst.setString(1, tripId);
			status = m_pst.executeUpdate();
			if(status==1){
				status=0;
				m_pst=m_conn.prepareStatement("INSERT INTO TDS_OPENREQUEST_HISTORY  ( SELECT * FROM TDS_OPENREQUEST WHERE OR_TRIP_ID = ? )");
				m_pst.setString(1, tripId);
				status=m_pst.executeUpdate();
				if(status==1){
					status=0;
					m_pst=m_conn.prepareStatement("DELETE FROM TDS_OPENREQUEST WHERE OR_TRIP_ID=? ");
					m_pst.setString(1, tripId);
					status=m_pst.executeUpdate();
				}
			}
		}catch (Exception sqex) {
			cat.error("TDSException CustomerMobileDAO.auto_cancelReservation-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			sqex.printStackTrace();	
		}finally{
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	
}
