package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.ReverseDisbrusementBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;


public class ReverseDisbrusementDAO {
	
private static Category cat =TDSController.cat;
	
	static {
		cat = Category.getInstance(ReverseDisbrusementDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ReverseDisbrusementDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static boolean updateReverseDisbrusement(String userID, String associationCode, String payID)
	{ 	
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ReverseDisbrusementDAO.updateReverseDisbrusement");

		boolean successFlag = false;
		ArrayList al_revdis1 = new ArrayList ();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null; 
		ResultSet rs =null;
		
		try {
			
			//String query = "SELECT DM_DRIVERID,date_format(DM_PAYMENTDATE,'%m/%d/%Y')as DM_PAYMENTDATE,DM_AMOUNT,DM_CHECKNO,DM_CHK_CASH ,DM_PAYID,B.AU_USERNAME ,B.AU_FNAME ,B.AU_LNAME  FROM TDS_DISBURSEMENT A, TDS_ADMINUSER B where A.DM_OPR_ID= B.AU_SNO";
			
			
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 		  
			m_conn.setAutoCommit(false);

			
			m_pst = m_conn.prepareStatement("UPDATE TDS_DISBURSEMENT SET DM_REVERSE_INDICATOR = 1 , DM_REVERSE_BY= ?, DM_REVERSE_DATE_TIME= now() where DM_PAYID =? and DM_ASSOCCODE= ?");
			m_pst.setString(1,userID);
			m_pst.setString(2,payID);
			m_pst.setString(3, associationCode);
			cat.info(m_pst.toString());
			m_pst.executeUpdate(); 
			
		    m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER_SETELED_SUMMARY SET CSS_PAY_KEY = 0,CSS_DIS_FLG = 0 where CSS_PAY_KEY =?");
			m_pst.setString(1,payID);
			cat.info(m_pst.toString());
			m_pst.executeUpdate(); 
			m_pst = m_conn.prepareStatement("UPDATE TDS_PAYMENT_SETTLED_SUMMARY SET PAY_ID = 0, PSS_DIS_FLG = 0 WHERE PAY_ID = ?");
			m_pst.setString(1, payID);
			cat.info(m_pst.toString());
			m_pst.executeUpdate(); 
			m_pst = m_conn.prepareStatement("UPDATE TDS_DRIVER_PENALITY_SETELED_SUMMARY SET DPS_PAY_KEY = 0, DPS_DIS_FLG = 0 WHERE DPS_PAY_KEY = ?");
			m_pst.setString(1, payID);
			cat.info(m_pst.toString());
			m_pst.executeUpdate(); 
			m_pst = m_conn.prepareStatement("UPDATE TDS_VOUCHER_SETELED_SUMMARY SET VSS_PAY_KEY = 0,VSS_DIS_FLG = 0 where VSS_PAY_KEY =?");
			m_pst.setString(1, payID);
			cat.info(m_pst.toString());
			m_pst.executeUpdate(); 
			successFlag = true;
			m_conn.commit();
			m_conn.setAutoCommit(true);
		} catch (Exception sqex)
		{
			cat.error("TDSException ReverseDisbursementDAO.updateReverseDisbrusement-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ReverseDisbursementDAO.updateReverseDisbrusement-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return successFlag;
	}
	
	public static ArrayList getReverseDisbrusement(String assocode, String driver_id,String paymentdate,String operator_id,String chk_cash,String amount,String checkno)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ReverseDisbrusementDAO.getReverseDisbrusement");
		ArrayList al_revdis = new ArrayList ();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ReverseDisbrusementBean revdisBO = new ReverseDisbrusementBean();
		
		String query = "SELECT DM_DRIVERID,date_format(DM_PAYMENTDATE,'%m/%d/%Y')as DM_PAYMENTDATE,DM_AMOUNT,DM_CHECKNO,DM_CHK_CASH, DM_PAYID,DM_REVERSE_INDICATOR,DM_REVERSE_BY,date_format(DM_REVERSE_DATE_TIME,'%m/%d/%Y @:%H:%i')as DM_REVERSE_DATE_TIME, B.AU_USERNAME ,B.AU_FNAME ,B.AU_LNAME FROM TDS_DISBURSEMENT A, TDS_ADMINUSER B  where A.DM_OPR_ID= B.AU_SNO";		
		if(!driver_id.equals("")) {
			if(query.endsWith("TDS_DISBURSEMENT")) {
				query =query+" WHERE DM_DRIVERID ='"+driver_id+"' ";
			} else {
				query = query+" AND DM_DRIVERID  ='"+driver_id+"' ";
			}
		}
			if(!paymentdate.equals("")) {
			
			if(query.endsWith("TDS_DISBURSEMENT")) {
				query =query+" WHERE date_format(DM_PAYMENTDATE,'%m/%d/%Y') ='"+paymentdate+"' ";
			} else {
				query = query+" AND date_format(DM_PAYMENTDATE,'%m/%d/%Y')  ='"+paymentdate+"' ";
			}
		}
		
		if(!amount.equals("")) {
			if(query.endsWith("TDS_DISBURSEMENT")) {
				query =query+" WHERE DM_AMOUNT ='"+amount+"' ";
			} else {
				query = query+" AND DM_AMOUNT  ='"+amount+"' ";
			}
		}
		
		
		if(!operator_id.equals("")) {
			if(query.endsWith("TDS_DISBURSEMENT")) {
				query =query+" WHERE B.AU_USERNAME ='"+operator_id+"' ";
			} else {
				query = query+" AND B.AU_USERNAME  ='"+operator_id+"' ";
			}
		}
		
		if(!checkno.equals("")) {
			if(query.endsWith("TDS_DISBURSEMENT")) {
				query =query+" WHERE DM_CHECKNO ='"+checkno+"' ";
			} else {
				query = query+" AND DM_CHECKNO  ='"+checkno+"' ";
			}
		}
		
	   if(!chk_cash.equals("")) {
			if(query.endsWith("TDS_DISBURSEMENT")) {
				query =query+" WHERE DM_CHK_CASH ='"+chk_cash+"' ";
			} else {
				query = query+" AND DM_CHK_CASH  ='"+chk_cash+"' ";
			}
		}		
		try { 

		 dbcon = new TDSConnection();
		 con =dbcon.getConnection();
		 pst = con.prepareStatement(query); 
		 cat.info(pst.toString());
		 rs = pst.executeQuery();
		 
		 while (rs.next()) {
			    revdisBO = new ReverseDisbrusementBean();
			    revdisBO.setDriverIdDis(rs.getString("DM_DRIVERID"));
			    revdisBO.setPaymentDateDis(rs.getString("DM_PAYMENTDATE"));
			    revdisBO.setAmountDis(rs.getString("DM_AMOUNT"));
			    revdisBO.setUserName(rs.getString("B.AU_USERNAME"));
			    revdisBO.setChequeORCashDis(rs.getString("DM_CHK_CASH"));
			    revdisBO.setCheckNO(rs.getString("DM_CHECKNO"));
			    revdisBO.setFirstName(rs.getString("B.AU_FNAME"));
			    revdisBO.setLastName(rs.getString("B.AU_LNAME"));
			    revdisBO.setPayId(rs.getString("DM_PAYID"));
			    revdisBO.setReverseIndi(rs.getInt("DM_REVERSE_INDICATOR"));
			    revdisBO.setOperatorID(rs.getString("DM_REVERSE_BY"));
			    revdisBO.setReverseDateTime(rs.getString("DM_REVERSE_DATE_TIME"));
			 
			    al_revdis.add(revdisBO);
			}

			rs.close();
			pst.close();
		 
			 
			 
			
		} catch(SQLException sqex) {
			cat.error("TDSException ReverseDisbursementDAO.getReverseDisbrusement-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException ReverseDisbursementDAO.getReverseDisbrusement-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_revdis;
	}
	
}
