package com.tds.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Category;

import com.tds.cmp.bean.CompanyFlagBean;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverVehicleBean;
import com.tds.cmp.bean.FileProperty;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.security.bean.Module_detail;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.ZoneFlagBean;
import com.common.util.TDSValidation;

public class UtilityDAO {
	private static Category cat =TDSController.cat;	

	static {

		cat = Category.getInstance(UtilityDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+UtilityDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}


	public static ArrayList getAndVerifyDriverNumbers(String p_assocode,String[] driver,String fromDriver,String toDriver,int all){
		long startTime = System.currentTimeMillis();
		cat.info("UtilityDAO.readZonesBoundries Start--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query="";

			if (all == 3){
				if(driver!=null)
				{
					for(int i=0;i < driver.length-1;i++)
					{
						query =    query+ "'" + driver[i]+"',";
					}

					query = query+ "'" + driver[driver.length-1]+ "'" ;
				}
			}
			cat.info("Query:" + query);
			if(all == 1){
				pst=con.prepareStatement("select DR_SNO from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = '1'");		
			}else if (all == 2){
				pst=con.prepareStatement("select DR_SNO from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1 and DR_SNO between '"+ fromDriver + "' and '" + toDriver  +"'");
			}else if (all == 3){
				pst=con.prepareStatement("select DR_SNO from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1 and DR_SNO in ("+query+")");			
			}	
			cat.info(pst.toString());	
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("DR_SNO"));
			}
		}
		catch(SQLException sqex) {
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	public static boolean  insertChargesForMultipleDriverNumbers(String assocode, ArrayList driver, String effectiveDate, String description, String amt, int chargeType, int location){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean successOfOperation = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		//PreparedStatement  m_pst_pay;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			con.setAutoCommit(false);
			if (location == 1){// Inserting into Driver Penalty Table
				pst = con.prepareStatement("insert into TDS_DRIVER_PENALITY(DP_TRANS_ID,DP_DRIVER_ID,DP_ASSOCCODE,DP_TYPE,DP_AMOUNT,DP_ENTERED_DATE,DP_PENALITY_DATE,DP_DESC) values(?,?,'" + assocode    + "','" + chargeType   + "'," + amt  + ",NOW(),'" + TDSValidation.getDBdateFormat(effectiveDate)  + "', '" + description + "')");
				for(int i=0;i < driver.size();i++)
				{
					pst.setInt(1, ConfigDAO.tdsKeyGen("TDS_DRIVER_PENALITY_SEQ", con));
					pst.setString(2, (String) driver.get(i));
					pst.execute();
					//System.out.println("query::"+pst.toString());
				}
			}else{
				pst = con.prepareStatement("insert into TDS_DRIVER_CHARGES_APPROVAL(DCA_DRIVER_ID, DCA_COMPANY_ID, DCA_CHARGE_NO,DCA_DESC,DCA_AMOUNT,DCA_CHARGE_DATE,DCA_PROCESSING_DATE) values(?,?,?,?,?,?, NOW())"); 
				// System.out.println("insert into TDS_DRIVER_CHARGES_APPROVAL(DCA_DRIVER_ID, DCA_COMPANY_ID, DCA_CHARGE_NO,DCA_DESC,DCA_AMOUNT,DCA_CHARGE_DATE) values(?,'" + assocode    + "',0," + description   + "," + amt + "'" + TDSValidation.getDBdateFormat(effectiveDate)  + "')");


				for(int i=0;i < driver.size();i++)
				{
					pst.setString(1, (String) driver.get(i));
					pst.setString(2, assocode);
					pst.setString(3, "0");
					pst.setString(4, description);
					pst.setString(5, amt);
					pst.setString(6, TDSValidation.getDBdateFormat(effectiveDate)); 	
					pst.execute();	
				}

			}  
			con.commit();
			con.setAutoCommit(true);
			successOfOperation = true;	
		}
		catch(SQLException sqex) {
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return successOfOperation ;
	} 		


	public static boolean isHavingRights(String action, String event,String userid)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean rights = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select M_MODULE_NAME from TDS_MODULES_DETAIL , TDS_SECURITY_ACCESS where S_MODULE_NO = M_MODULE_NO and S_UID= ? and M_ACTION = ? and M_EVENT= ? and S_ACESS=true");
			pst.setString(1, userid);
			pst.setString(2, action);
			pst.setString(3, event);
			cat.info(pst.toString());

			rs = pst.executeQuery();

			//System.out.println("PERMISSION QUERY::"+pst.toString());

			if(rs.first())
			{
				rights = true;
			}
		} catch(SQLException sqex) {
			//cat.info("Error in inserting TDS_DRIVER table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return rights;
	}

	public static FileProperty getFileProperty(String id){

		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		FileProperty fileProperty = new FileProperty();

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select * from UPLOADS where UPLOADID=?");
			pst.setString(1, id);
			cat.info(pst.toString());

			rs = pst.executeQuery();

			if(rs.first())
			{
				fileProperty.setFilesize(rs.getInt("filesize"));
				fileProperty.setFilename(rs.getString("filename"));
				fileProperty.setFile(rs.getBytes("BINARYFILE"));
			}
		}catch(SQLException sqex) {
			//cat.info("Error in inserting TDS_DRIVER table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return fileProperty;
	}

	public static ArrayList getModulesName(String userid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		String curr="",prev="";
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Module_detail module_detail = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{

			pst=con.prepareStatement("select *,case (select S_UID from TDS_SECURITY_ACCESS where S_MODULE_NO = M_MODULE_NO and S_UID='"+userid+"' and S_ACESS=1) is null when true then 0 else 1 end as Access  from TDS_MODULES_DETAIL WHERE M_MODULE_NO < '9000' order by M_CATEGORY,m_module_no");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			int counter=0;
			while(rs.next())
			{
				curr = rs.getString("M_CATEGORY");
				module_detail = new Module_detail();

				if(!curr.equalsIgnoreCase(prev)) 
				{
					module_detail.setBold(true);
					if(al_list.size()>0 &&  counter > 0)
					{
						((Module_detail)al_list.get((counter-1))).setChange(true);
						//((Module_detail)al_list.get((counter-1))).setEnd(rs.getString("M_MODULE_NO"));
					}
				}
				prev = rs.getString("M_CATEGORY");
				module_detail.setModule_name(rs.getString("M_MODULE_NAME"));
				module_detail.setModule_no(rs.getInt("M_MODULE_NO"));
				module_detail.setAccess(rs.getInt("Access")==1?true:false);
				if(rs.isLast()){
					module_detail.setChange(true);
					//module_detail.setEnd(rs.getString("M_MODULE_NO"));
				}
				al_list.add(module_detail);
				counter++;
			}

		}
		catch(SQLException sqex) {
			cat.error("Error in inserting TDS_DRIVER table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}


	public static ArrayList getDriverLocationList(String p_assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select l.*,a.AU_SNO from TDS_DRIVERLOCATION l,TDS_ADMINUSER a where DL_DRIVERID = AU_USERNAME and  DL_ASSOCCODE = '"+p_assocode+"' and DL_SWITCH='Y'");
			cat.info(pst.toString());

			rs = pst.executeQuery();
			while(rs.next())
			{
				DriverLocationBO driverLocationBO = new DriverLocationBO();

				driverLocationBO.setUid(rs.getString("AU_SNO"));
				driverLocationBO.setDriverid(rs.getString("DL_DRIVERID"));
				driverLocationBO.setLatitude(rs.getString("DL_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("DL_LONGITUDE"));
				driverLocationBO.setSmsid(rs.getString("DL_SMSID"));
				driverLocationBO.setPhone(rs.getString("DL_PHONE"));

				al_list.add(driverLocationBO);

			}

		}
		catch(SQLException sqex) {
			//cat.info("Error in inserting TDS_DRIVER table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		return al_list;
	}
	public static ArrayList<DriverLocationBO> getDriverLocationListByStatus(String p_assocode, int status,String timeZone,String driver){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DriverLocationBO> al_list=new ArrayList<DriverLocationBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String statusQuery = "";
		String searchQuery ="";
		if(driver!=""){
			searchQuery = " AND ( DL_DRIVERID ='"+driver+"' OR DL_VEHICLE_NO='"+driver+"' ) ";
		}
		switch(status) {
		case 1:
			statusQuery = "and DL_SWITCH <> 'L'";
			break;
		case 2:
			statusQuery = "and DL_SWITCH = 'N'";
			break;
		case 3: 
			statusQuery = "and DL_SWITCH = 'L'";
			break;
		case 4:
			statusQuery = "and DL_SWITCH = 'Y'";
			break; 	
		default:
			statusQuery = "";
			break;
		}

		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try{
			pst=con.prepareStatement("SELECT *, TIMESTAMPDIFF(MINUTE,DL_LASTUPDATE,NOW()) AS LASTUPDATETIMEDIFF,  TIMESTAMPDIFF(MINUTE,CASE DL_LOGIN_TIME WHEN '1970-01-01 00:00:00' THEN NOW()  ELSE  DL_LOGIN_TIME  END ,NOW()) AS LOGINTIME,CASE DL_LAST_LOGINTIME WHEN '1970-01-01 00:00:00' THEN NOW()  ELSE  DL_LAST_LOGINTIME  END as lastlogin,SRM_REGISTER_STATUS from TDS_DRIVERLOCATION LEFT JOIN TDS_DRIVER_DEACTIVATION ON DD_ASSOCCODE=DL_ASSOCCODE AND DL_DRIVERID=DD_DRIVER_ID LEFT JOIN TDS_SHIFT_REGISTER_MASTER ON SRM_OPERATORID=DL_DRIVERID AND SRM_ASSOCCODE=DL_MASTER_ASSOCCODE WHERE (DL_ASSOCCODE ='"+p_assocode+"' OR DL_MASTER_ASSOCCODE='"+p_assocode+"')" + statusQuery +" "+searchQuery+" GROUP BY DL_DRIVERID ORDER BY CONVERT(DL_VEHICLE_NO,UNSIGNED INTEGER) DESC");     
			//System.out.println("Pst--->"+pst);
			rs = pst.executeQuery();
			while(rs.next())
			{
				DriverLocationBO driverLocationBO = new DriverLocationBO();

				driverLocationBO.setDriverid(rs.getString("DL_DRIVERID"));
				driverLocationBO.setLatitude(rs.getString("DL_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("DL_LONGITUDE"));
				driverLocationBO.setSmsid(rs.getString("DL_SMSID"));
				driverLocationBO.setPhone(rs.getString("DL_PHONENO"));
				driverLocationBO.setProfile(rs.getString("DL_PROFILE"));
				driverLocationBO.setStatus(rs.getString("DL_SWITCH"));
				driverLocationBO.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				driverLocationBO.setLastUpdatedMilliSeconds(rs.getInt("LASTUPDATETIMEDIFF"));
				driverLocationBO.setDrName(rs.getString("DL_DRNAME"));
				driverLocationBO.setLoginTime(rs.getInt("LOGINTIME"));
				driverLocationBO.setAppVersion(rs.getString("DL_APP_VERSION"));
				driverLocationBO.setLastLoginTime(rs.getString("lastlogin"));
				driverLocationBO.setProvider(rs.getString("SRM_REGISTER_STATUS"));
				driverLocationBO.setDriverActivateStatus((rs.getString("DD_DRIVER_ID")==null || rs.getString("DD_DRIVER_ID").equals("") || rs.getString("DD_DRIVER_ID").equals("null") ) ? 0:1);
				if(p_assocode.equalsIgnoreCase("182")){
					driverLocationBO.setDistance(rs.getString("DL_DISTANCE"));
				}else{
					driverLocationBO.setDistance("NF");
				}
				//System.out.println("driverstatua:"+rs.getString("DD_DRIVER_ID")+"---in bean:"+driverLocationBO.getDriverActivateStatus());
				//driverLocationBO.setAge(TDSValidation.formatIntoHHMMSS(Integer.parseInt(rs.getString("AGE")!=null?rs.getString("AGE"):"0"))) ;
				al_list.add(driverLocationBO);

			}

		}
		catch(SQLException sqex) {
			//System.out.println("TDSException UtilityDAO.getDriverLocationListByStatus-->"+sqex.getMessage());
			cat.error("TDSException UtilityDAO.getDriverLocationListByStatus-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList getDriverLocationByFlag(String p_assocode, int Status){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		//1 --> all drivers that logged
		//2 --> all inactive drivers
		//3 --> Add drivers

		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst=con.prepareStatement("select * TDS_DRIVERLOCATION where DL_DRIVERID = AU_USERNAME and  DL_ASSOCCODE = '"+p_assocode+"' and DL_SWITCH='Y'");
			cat.info(pst.toString());

			rs = pst.executeQuery();
			while(rs.next())
			{
				DriverLocationBO driverLocationBO = new DriverLocationBO();

				driverLocationBO.setUid(rs.getString("AU_SNO"));
				driverLocationBO.setDriverid(rs.getString("DL_DRIVERID"));
				driverLocationBO.setLatitude(rs.getString("DL_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("DL_LONGITUDE"));
				driverLocationBO.setSmsid(rs.getString("DL_SMSID"));
				driverLocationBO.setPhone(rs.getString("DL_PHONE"));

				al_list.add(driverLocationBO);

			}

		}
		catch(SQLException sqex) {
			//cat.info("Error in inserting TDS_DRIVER table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList getDriverList(String p_assocode,int all){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;

		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(all == 1)
				pst=con.prepareStatement("select DR_SNO,DR_FNAME, DR_LNAME from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "'");
			else
				pst=con.prepareStatement("select DR_SNO,DR_FNAME, DR_LNAME from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1");	

			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("DR_SNO"));
				al_list.add(rs.getString("DR_FNAME").concat(rs.getString("DR_LNAME")));
			}
		}
		catch(SQLException sqex) {
			//cat.info("Error in inserting TDS_DRIVER table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	public static boolean Assocodedriveridmatch(String Driverid,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean s=false;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT AU_SNO FROM TDS_ADMINUSER where AU_ASSOCCODE=? and AU_SNO=?");
			pst.setString(1, assocode);
			pst.setString(2, Driverid);
			cat.info(pst.toString());

			rs=pst.executeQuery();
			if(rs.first()){
				s=true;	
			}

		}catch(SQLException ex){
			//System.out.println(ex.getMessage());
			return s;
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return s;
	}
	public static ArrayList getzonelist(String Driverid,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst,pst1 = null;
		ResultSet rs,rs1 = null;
		String s="";
		ArrayList result = new ArrayList();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String driverID="";
		try {
			pst=con.prepareStatement("SELECT LO_DRIVERID FROM TDS_LOGINDETAILS  where LO_ASSCODE= ? AND (LO_DRIVERID = "+Driverid+" OR LO_CABNO="+Driverid+")");
			pst.setString(1,assocode);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.next())
			{
				driverID = rs.getString("LO_DRIVERID");
			} 
			pst=con.prepareStatement("SELECT QD_QUEUENAME,QD_DESCRIPTION FROM TDS_QUEUE_DETAILS WHERE QD_ASSOCCODE=? AND QD_LOGIN_SWITCH='A'");			
			pst.setString(1, assocode);
			rs=pst.executeQuery();
			pst1 = con.prepareStatement("SELECT A.QUE_NAME,B.QD_DESCRIPTION FROM TDS_QUEUE_DRIVER_MAPPING A, TDS_QUEUE_DETAILS B WHERE A.ASS_CODE=? AND A.DRIVER_ID=?   AND A.QUE_NAME=B.QD_QUEUENAME");
			pst1.setString(1, assocode);
			pst1.setString(2, driverID);
			cat.info(pst1.toString());
			rs1=pst1.executeQuery();


			while(rs.next()){ 
				result.add(rs.getString("QD_QUEUENAME"));
				result.add(rs.getString("QD_DESCRIPTION"));
			}
			while(rs1.next()) {
				result.add(rs1.getString("QUE_NAME"));
				result.add(rs1.getString("QD_DESCRIPTION")); 
			} 
			//
		}catch(SQLException ex){
			//System.out.println(ex.getMessage());
			return result;
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static String IdtodriverName(String Driverid,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String s="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT concat(DR_FNAME,' ',DR_LNAME) as name FROM TDS_DRIVER where DR_SNO=? and DR_ASSOCODE=?");
			pst.setString(1, Driverid);
			pst.setString(2, assocode);
			cat.info(pst.toString());

			rs=pst.executeQuery();
			if(rs.first()){
				s=rs.getString("name");	
			}
			//System.out.println("Drivernames"+s);
		}catch(SQLException ex){
			//System.out.println(ex.getMessage());
			return s;
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return s;
	}
	public static ArrayList getDriverList(String p_assocode,String driver,int all){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			if(all == 1)
				pst=con.prepareStatement("select DR_SNO, DR_USERID,DR_FNAME,DR_LNAME from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and (DR_SNO like (?) OR DR_FNAME like ?) and AU_ASSOCCODE =  '" + p_assocode + "' ORDER BY DR_USERID");
			else if(all == 2)
				pst=con.prepareStatement("select DR_SNO,DR_USERID,DR_FNAME,DR_LNAME from TDS_DRIVER d,TDS_DRIVERLOCATION l where d.DR_USERID = l.DL_DRIVERID and DL_SWITCH = 'Y' and (DR_SNO like (?) OR DR_FNAME like ?) and DL_ASSOCCODE = '"+p_assocode+"' ORDER BY DR_USERID");
			else
				pst=con.prepareStatement("select DR_SNO, DR_USERID,DR_FNAME,DR_LNAME from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and (DR_SNO like (?) OR DR_FNAME like ?) and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1 ORDER BY DR_USERID");	

			pst.setString(1,("%"+driver+"%")); 
			pst.setString(2,(driver+"%")); 
			//System.out.println(pst);
			//System.out.println(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("DR_SNO"));
				al_list.add(rs.getString("DR_FNAME")+" "+rs.getString("DR_LNAME"));
			}
		}
		catch(SQLException sqex) {
			//cat.info("Error in inserting TDS_DRIVER table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static String returnEmail(String username){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String result="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("select AU_EMAIL from TDS_ADMINUSER  where AU_USERNAME=?");
			pst.setString(1, username);
			cat.info(pst.toString());

			rs=pst.executeQuery();
			if(rs.first()){
				result= rs.getString("AU_EMAIL");	
			}  
		}catch(SQLException ex){
			//System.out.println(ex.getMessage());
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList getDriverNoList(String p_assocode,String[] driver,String all){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query="";

			if(driver!=null)
			{
				for(int i=0;i < driver.length-1;i++)
				{
					//	if(i<(driver.length-1))
					query = query+driver[i]+",";
					//	else
					//	query = query+driver[i];
				}

				query = query+driver[driver.length-1];
			}

			cat.info("Query:" + query);
			if(all.equals("0"))
				pst=con.prepareStatement("select concat(DR_PHONE) as email from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1 and DR_SNO in ("+query+")");
			else
				pst=con.prepareStatement("select concat(DR_PHONE) as email from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = '1'");
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("email"));
			}
		}
		catch(SQLException sqex) {
			sqex.printStackTrace();

			//cat.info("Error in getDriverNoList table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList<DriverCabQueueBean> getDriverListForMessaging(String p_assocode,String[] driver,int all, boolean msgToLoggedInDriversOnly){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DriverCabQueueBean> al_list=new ArrayList<DriverCabQueueBean>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query="";
			if(driver!=null && driver.length>0)
			{
				query =  " and DL_DriverID in ("; 
				for(int i=0;i < driver.length-1;i++)
				{
					query = query+driver[i]+",";
				}

				query = query+driver[driver.length-1]+") ";
			}
			String loggedinStatusQuery = "";

			if(msgToLoggedInDriversOnly){
				loggedinStatusQuery = " AND DL_SWITCH <> 'L' ";
			} 

			if(all == 0){
				pst=con.prepareStatement("SELECT DL_DRIVERID, DL_PHONENO, DL_PROVIDER, DL_GOOG_REG_KEY from TDS_DRIVERLOCATION where (DL_ASSOCCODE =  '" + p_assocode + "' OR DL_MASTER_ASSOCCODE='"+p_assocode+"')" +query + loggedinStatusQuery);
			} else {
				pst=con.prepareStatement("SELECT DL_DRIVERID, DL_PHONENO, DL_PROVIDER, DL_GOOG_REG_KEY from TDS_DRIVERLOCATION where (DL_ASSOCCODE =  '" + p_assocode + "' OR DL_MASTER_ASSOCCODE='"+p_assocode+"')" + loggedinStatusQuery);				
			}
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				DriverCabQueueBean driverNumber = new DriverCabQueueBean();
				driverNumber.setPhoneNo(rs.getString("DL_PHONENO"));
				driverNumber.setProvider(rs.getString("DL_PROVIDER"));
				driverNumber.setPushKey(rs.getString("DL_GOOG_REG_KEY"));
				driverNumber.setDriverid(rs.getString("DL_DRIVERID")); 
				al_list.add(driverNumber);
			}
		} catch(SQLException sqex) {

			sqex.printStackTrace();

			//cat.info("Error in getDriverNoList table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList<DriverCabQueueBean> getDriverListForMessage(String p_assocode,String driver,int all, boolean msgToLoggedInDriversOnly){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DriverCabQueueBean> al_list=new ArrayList<DriverCabQueueBean>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query="";
			/*if(driver!=null && driver.length>0)
			{
				query =  " and DL_DriverID in ("; 
				for(int i=0;i < driver.length-1;i++)
				{
					query = query+driver[i]+",";
				}

				
			}*/

			pst=con.prepareStatement("SELECT DL_DRIVERID, DL_PHONENO, DL_PROVIDER, DL_GOOG_REG_KEY from TDS_DRIVERLOCATION where (DL_ASSOCCODE =  '" + p_assocode + "' OR DL_MASTER_ASSOCCODE='"+p_assocode+"') AND DL_DRIVERID='"+driver+"'");
			/*
			if(all == 0){
				pst=con.prepareStatement("SELECT DL_DRIVERID, DL_PHONENO, DL_PROVIDER, DL_GOOG_REG_KEY from TDS_DRIVERLOCATION where (DL_ASSOCCODE =  '" + p_assocode + "' OR DL_MASTER_ASSOCCODE='"+p_assocode+"')" +query + loggedinStatusQuery);
			} else {
				pst=con.prepareStatement("SELECT DL_DRIVERID, DL_PHONENO, DL_PROVIDER, DL_GOOG_REG_KEY from TDS_DRIVERLOCATION where (DL_ASSOCCODE =  '" + p_assocode + "' OR DL_MASTER_ASSOCCODE='"+p_assocode+"')" + loggedinStatusQuery);				
			}*/
			rs=pst.executeQuery();
			while(rs.next()){
				DriverCabQueueBean driverNumber = new DriverCabQueueBean();
				driverNumber.setPhoneNo(rs.getString("DL_PHONENO"));
				driverNumber.setProvider(rs.getString("DL_PROVIDER"));
				driverNumber.setPushKey(rs.getString("DL_GOOG_REG_KEY"));
				driverNumber.setDriverid(rs.getString("DL_DRIVERID")); 
				al_list.add(driverNumber);
			}
		} catch(SQLException sqex) {

			sqex.printStackTrace();

			//cat.info("Error in getDriverNoList table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}




	public static ArrayList getDriverNoList(ArrayList al_l){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String list = "";

			if(!al_l.isEmpty())
			{
				for(int i=0;i<al_l.size()-1;i++)
					list = list + al_l.get(i).toString()+",";

				list = list + al_l.get(al_l.size()-1).toString();

				pst=con.prepareStatement("select DR_PHONE as email from TDS_DRIVER where DR_SNO in ("+list+")");
				cat.info(pst.toString());

				rs=pst.executeQuery();
				while(rs.next()){
					al_list.add(rs.getString("email"));
				}
			}
		}
		catch(SQLException sqex) {
			sqex.printStackTrace();

			//cat.info("Error in getDriverNoList table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static String getDriverNo(String did){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String no ="";
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select DR_PHONE as email from TDS_DRIVER where DR_SNO ='"+did+"' ");
			cat.info(pst.toString());

			rs=pst.executeQuery();
			if(rs.first()){
				no = rs.getString("email");
			}
		}
		catch(SQLException sqex) {
			sqex.printStackTrace();

			//cat.info("Error in getDriverNoList table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return no;
	}



	public static ArrayList getDriverNoQ(String p_assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query="";


			//System.out.println("Query:" + query);
			//pst=con.prepareStatement("select DL_DRIVERID,concat(DL_PHONE,'@',DL_SMSID) as email from TDS_DRIVERLOCATION where DL_ASSOCCODE =  '" + p_assocode + "' and date_sub(now(),INTERVAL 1 HOUR) <= DL_LASTUPDATE");
			pst=con.prepareStatement("select DL_DRIVERID,DL_SMSID as email from TDS_DRIVERLOCATION where DL_ASSOCCODE =  '" + p_assocode + "' and date_sub(now(),INTERVAL 1 HOUR) <= DL_LASTUPDATE");
			cat.info(pst.toString());

			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("DL_DRIVERID"));
				al_list.add(rs.getString("email"));
			}
		}
		catch(SQLException sqex) {
			sqex.printStackTrace();

			//cat.info("Error in getDriverNoList table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static ArrayList getQueueForAssoccode(String Assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList m_conditionList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		m_tdsConn = new TDSConnection(); 
		m_con = m_tdsConn.getConnection();
		try {
			m_conditionList = new ArrayList();
			m_pst = m_con.prepareStatement("SELECT QD_QUEUENAME,QD_DESCRIPTION FROM TDS_QUEUE_DETAILS where QD_ASSOCCODE= ? ");
			m_pst.setString(1, Assocode);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				m_conditionList.add(m_rs.getString("QD_QUEUENAME"));
				m_conditionList.add(m_rs.getString("QD_DESCRIPTION"));
			}

		} catch (SQLException p_sqex ) {
			//System.out.println("Error in getUserInformation "+p_sqex.getMessage());
			//cat.info("Error in getUserInformation "+p_sqex.getMessage());
			p_sqex.getStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_conditionList;
	}
	public static boolean chckCronJobExec(String date,String cronjob,String yest)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean status = false;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		m_tdsConn = new TDSConnection(); 
		m_con = m_tdsConn.getConnection();
		try {

			if(yest.equals("Y"))
				m_pst = m_con.prepareStatement("select date_format(date_sub(SL_DAY,interval 1 day),'%Y%m%d') from TDS_SCHEDULED_PROCESSING_LOG where date_format(date_sub(SL_DAY,interval 1 day),'%Y%m%d') = ? and  SL_CRON_JOB = ?");
			else
				m_pst = m_con.prepareStatement("select date_format(SL_DAY,'%Y%m%d') from TDS_SCHEDULED_PROCESSING_LOG where date_format(SL_DAY,'%Y%m%d') = ? and  SL_CRON_JOB = ?");

			m_pst.setString(1, date);
			m_pst.setString(2, cronjob);
			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();
			//System.out.println("CRON CKECK::"+m_pst.toString());
			if(m_rs.first())
				status = true;

		}catch (SQLException p_sqex ) {
			//System.out.println("Error in getUserInformation "+p_sqex.getMessage());
			//cat.info("Error in getUserInformation "+p_sqex.getMessage());
			p_sqex.getStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static List getDriverLocation(String assocode,String driverids) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_driverLocationList = new ArrayList();
		TDSConnection m_dbcon = null;
		Connection m_con = null;
		PreparedStatement m_pst = null ;
		ResultSet m_rs = null; 
		Map m_driverLocation = null;
		try {
			m_dbcon = new TDSConnection();
			m_con = m_dbcon.getConnection();
			if(driverids.equals(""))
				m_pst = m_con.prepareStatement("SELECT DR_USERID,DR_DRIVEREXT,DL_LATITUDE,DL_LONGITUDE FROM TDS_DRIVER, TDS_DRIVERLOCATION WHERE DL_DRIVERID = DR_USERID and DL_SWITCH = 'Y' and DL_ASSOCCODE = ?");
			else {
				String sepDriver = "";
				if(!(driverids.indexOf(",")>0))
				{
					sepDriver = driverids;
				} else {

					String driverid[]  = driverids.split(","); 

					for(int i=0;i<driverid.length;i++)
					{
						if(i !=driverid.length -1)
							sepDriver = sepDriver + driverid[i] + ",";
						else
							sepDriver = sepDriver + driverid[i] ;
					}
				}

				m_pst = m_con.prepareStatement("SELECT DR_USERID,DR_DRIVEREXT,DL_LATITUDE,DL_LONGITUDE FROM TDS_DRIVER, TDS_DRIVERLOCATION WHERE  DL_DRIVERID = DR_USERID and DL_SWITCH = 'Y' and DL_ASSOCCODE = ? and DR_SNO in ("+sepDriver+")");
			}
			m_pst.setString(1, assocode);

			cat.info(m_pst.toString());

			m_rs = m_pst.executeQuery();

			while(m_rs.next()) {
				m_driverLocation = new HashMap();
				m_driverLocation.put("driverid", "\""+m_rs.getString("DR_USERID")+"\"");
				m_driverLocation.put("driverext", m_rs.getString("DR_DRIVEREXT"));
				m_driverLocation.put("latitude", m_rs.getString("DL_LATITUDE"));
				m_driverLocation.put("longitude", m_rs.getString("DL_LONGITUDE"));
				m_driverLocationList.add(m_driverLocation);
			}
			if(m_driverLocationList.size() < 0 ) {
				m_driverLocationList = null;
			}
		} catch (SQLException sqex) {
			//System.out.println("Exception in getDriverLocation "+sqex.getMessage());
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_driverLocationList;
	}

	public static boolean isExistOrNot(String p_query)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbCon = null;
		Connection con = null;
		PreparedStatement pst;
		ArrayList al_batchList = new ArrayList();	
		//System.out.println("\n\nInventory DAO\nTo Check Any thing Exist or not\n");
		boolean result=false;
		String uItemCode="",qty="";
		try
		{
			dbCon = new TDSConnection();
			con = dbCon.getConnection();
			pst = con.prepareStatement(p_query);
			cat.info(pst.toString());

			ResultSet rs = pst.executeQuery();
			//System.out.println("Query:" + p_query);
			if(rs.first())
			{
				result= true;
			}
			else
			{
				result= false;	
			}
		}
		catch(SQLException sqex)
		{
			sqex.printStackTrace();
			result= false;
		}
		finally
		{			
			dbCon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static HashMap<String, DriverVehicleBean> getCmpyVehicleFlag(String associationCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		CompanyFlagBean cmFlagBean = new CompanyFlagBean();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		HashMap<String, DriverVehicleBean> hash_list = new HashMap<String, DriverVehicleBean>();
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("select * from TDS_CMPY_FLAG_SETUP where CS_ASSOCODE=? order by CS_FLAG_SW");
			m_pst.setString(1,associationCode);
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{
				DriverVehicleBean driverVehicle=new DriverVehicleBean();
				driverVehicle.setLongDesc(rs.getString("CS_FLAG_LNG_DESC"));
				driverVehicle.setKey(rs.getString("CS_FLAG_VALUE"));
				driverVehicle.setDriverVehicleSW(rs.getInt("CS_FLAG_SW"));
				driverVehicle.setShortDesc(rs.getString("CS_FLAG"));
				driverVehicle.setGroupId(rs.getInt("CS_GRP"));
				hash_list.put(rs.getString("CS_FLAG_VALUE"), driverVehicle);
			}

		}catch (Exception sqex)
		{
			hash_list=null;
			cat.error("TDSException UtilityDAO.getCmpyVehicleFlag-->"+sqex.getMessage());
			//System.out.println("TDSException UtilityDAO.getCmpyVehicleFlag-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return hash_list;
	}

	public static ArrayList<DriverLocationBO> searchDrivers(String p_assocode, DriverLocationBO driverLocBo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DriverLocationBO> al_list=new ArrayList<DriverLocationBO>();
		AdminRegistrationBO adminBo = new AdminRegistrationBO();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String statusQuery = "";
		if(driverLocBo.getDriverid()!=null&&!driverLocBo.getDriverid().equals("")){
			statusQuery=" AND DL_DRIVERID = "+driverLocBo.getDriverid(); 
		}
		if(driverLocBo.getVehicleNo()!=null&&!driverLocBo.getVehicleNo().equals("")){
			statusQuery=" AND DL_VEHICLE_NO = "+driverLocBo.getVehicleNo(); 
		}
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst=con.prepareStatement("select * from TDS_DRIVERLOCATION  where DL_ASSOCCODE = '"+p_assocode+"'AND DL_SWITCH <> 'L'"+ statusQuery);
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next())
			{
				DriverLocationBO driverLocationBO = new DriverLocationBO();
				driverLocationBO.setDriverid(rs.getString("DL_DRIVERID"));
				driverLocationBO.setLatitude(rs.getString("DL_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("DL_LONGITUDE"));
				driverLocationBO.setSmsid(rs.getString("DL_SMSID"));
				driverLocationBO.setPhone(rs.getString("DL_PHONE"));
				driverLocationBO.setProfile(rs.getString("DL_PROFILE"));
				driverLocationBO.setStatus(rs.getString("DL_SWITCH"));
				driverLocationBO.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				al_list.add(driverLocationBO);

			}
		}
		catch(SQLException sqex) {
			//System.out.println("TDSException UtilityDAO.searchDrivers-->"+sqex.getMessage());
			cat.error("TDSException UtilityDAO.searchDrivers-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}



	public static ArrayList<String> getEmployeeNames(String associationCode,String driver,int all){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<String> al_list=new ArrayList<String>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select AU_SNO, AU_USERNAME,AU_FNAME from TDS_ADMINUSER  where AU_USERNAME like ? and AU_ASSOCCODE =  '" + associationCode + "'");

			pst.setString(1,(driver+"%")); 
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("AU_USERNAME"));
				al_list.add(rs.getString("AU_FNAME"));
			}
		}
		catch(SQLException sqex) {
			cat.error("TDSException UtilityDAO.getEmployeeNames->"+sqex.getMessage());
			//System.out.println("TDSException UtilityDAO.getEmployeeNames-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	public static int insertLogo(String fileName,long fileSize,InputStream file,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		int size=Integer.parseInt(String.valueOf(fileSize));
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="INSERT INTO UPLOADS (FILENAME,BINARYFILE,FILESIZE,ASSOCCODE) VALUES(?,?,?,?)";
		try{
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM UPLOADS WHERE ASSOCCODE='"+assoccode+"'");
			pst.execute();
			pst=con.prepareStatement(query);
			pst.setString(1,fileName);
			pst.setBinaryStream(2,file,size);
			pst.setLong(3,fileSize);
			pst.setString(4,assoccode);
			result=pst.executeUpdate();
			con.commit();
			con.setAutoCommit(true);
		}catch(Exception sqex){
			cat.error("TDSException UtilityDAO.insertLogo--->"+sqex.getMessage());
			//System.out.println("TDSException UtilityDAO.insertLogo--->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();

		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int makeMsgRead(String msgId,String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int time=0;
		try {
			pst = con.prepareStatement("UPDATE TDS_TEXT_MESSAGE SET TM_READ_STATUS ='1' WHERE TM_KEY='"+msgId+"' AND TM_ASSOCIATION_CODE ='"+assoCode+"'");
			cat.info("TDS INFO UtilityDAO.makeMsgRead Query"+pst.toString());
			pst.executeUpdate();

		}catch (Exception sqex) {
			cat.error(("TDSException UtilityDAO.makeMsgRead-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException UtilityDAO.makeMsgRead-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return time;
	}

	public static HashMap<String,ZoneFlagBean> getZoneFlag(String associationCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		CompanyFlagBean cmFlagBean = new CompanyFlagBean();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		HashMap<String, ZoneFlagBean> hash_list = new HashMap<String, ZoneFlagBean>();
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();

		try {
			m_pst = m_conn.prepareStatement("select * from TDS_ZONE_FLAG_SETUP where ZN_ASSOCODE=? order by ZN_FLAG_VALUE");
			m_pst.setString(1,associationCode);
			cat.info(m_pst.toString());
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{
				ZoneFlagBean zoneFlag=new ZoneFlagBean();
				zoneFlag.setFlag1(rs.getString("ZN_FLAG_VALUE"));
				//cmFlagBean.setFlag1_sw(rs.getString("CS_FLAG_SW"));
				zoneFlag.setFlag1_value(rs.getString("ZN_FLAG"));
				zoneFlag.setFlag1_lng_desc(rs.getString("ZN_FLAG_LNG_DESC"));
				hash_list.put(rs.getString("ZN_FLAG_VALUE"), zoneFlag);
				System.out.println("Query"+rs);
			}

		}catch (Exception sqex)
		{
			hash_list=null;
			cat.error("TDSException UtilityDAO.getZoneFlag-->"+sqex.getMessage());
			//System.out.println("TDSException UtilityDAO.getZoneFlag-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return hash_list;
	}

	public static ArrayList<DriverLocationBO> getDriverLocationListForWasl(String p_assocode, int status,String timeZone,String driver){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DriverLocationBO> al_list=new ArrayList<DriverLocationBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try{
			pst=con.prepareStatement("SELECT *, TIMESTAMPDIFF(MINUTE,DL_LASTUPDATE,NOW()) AS LASTUPDATETIMEDIFF,  TIMESTAMPDIFF(MINUTE,CASE DL_LOGIN_TIME WHEN '1970-01-01 00:00:00' THEN NOW()  ELSE  DL_LOGIN_TIME  END ,NOW()) AS LOGINTIME,CASE DL_LAST_LOGINTIME WHEN '1970-01-01 00:00:00' THEN NOW()  ELSE  DL_LAST_LOGINTIME  END as lastlogin,SRM_REGISTER_STATUS from TDS_DRIVERLOCATION LEFT JOIN TDS_DRIVER_DEACTIVATION ON DD_ASSOCCODE=DL_ASSOCCODE AND DL_DRIVERID=DD_DRIVER_ID LEFT JOIN TDS_OPENREQUEST ON OR_ASSOCCODE = DL_ASSOCCODE AND OR_DRIVERID = DL_DRIVERID LEFT JOIN TDS_SHIFT_REGISTER_MASTER ON SRM_OPERATORID=DL_DRIVERID AND SRM_ASSOCCODE=DL_MASTER_ASSOCCODE WHERE DL_ASSOCCODE ='"+p_assocode+"' AND DL_SWITCH <> 'L' GROUP BY DL_DRIVERID ORDER BY CONVERT(DL_VEHICLE_NO,UNSIGNED INTEGER) DESC");     
			//System.out.println("Pst--->"+pst);
			rs = pst.executeQuery();
			while(rs.next())
			{
				DriverLocationBO driverLocationBO = new DriverLocationBO();
				driverLocationBO.setDriverid(rs.getString("DL_DRIVERID"));
				driverLocationBO.setLatitude(rs.getString("DL_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("DL_LONGITUDE"));
				driverLocationBO.setSmsid(rs.getString("DL_SMSID"));
				driverLocationBO.setPhone(rs.getString("DL_PHONENO"));
				driverLocationBO.setProfile(rs.getString("DL_PROFILE"));
				driverLocationBO.setStatus(rs.getString("DL_SWITCH"));
				driverLocationBO.setVehicleNo(rs.getString("DL_VEHICLE_NO"));
				driverLocationBO.setLastUpdatedMilliSeconds(rs.getInt("LASTUPDATETIMEDIFF"));
				driverLocationBO.setDrName(rs.getString("DL_DRNAME"));
				driverLocationBO.setLoginTime(rs.getInt("LOGINTIME"));
				driverLocationBO.setAppVersion(rs.getString("DL_APP_VERSION"));
				driverLocationBO.setLastLoginTime(rs.getString("lastlogin"));
				driverLocationBO.setProvider(rs.getString("SRM_REGISTER_STATUS"));
				driverLocationBO.setDriverActivateStatus((rs.getString("DD_DRIVER_ID")==null || rs.getString("DD_DRIVER_ID").equals("") || rs.getString("DD_DRIVER_ID").equals("null") ) ? 0:1);
				if(rs.getString("OR_DRIVERID")!=null && !rs.getString("OR_DRIVERID").equals("") && driverLocationBO.getStatus().equals("N")){
					driverLocationBO.setIn_job(true);
				}else{
					driverLocationBO.setIn_job(false);
				}
				driverLocationBO.setJson_details(rs.getString("DL_JSON_REFERENCE")==null?"":rs.getString("DL_JSON_REFERENCE"));
				al_list.add(driverLocationBO);
			}

		}
		catch(SQLException sqex) {
			//System.out.println("TDSException UtilityDAO.getDriverLocationListByStatus-->"+sqex.getMessage());
			cat.error("TDSException UtilityDAO.getDriverLocationListByStatus-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	
	public static ArrayList<String> getFormattedTimeDetailsForWasl(String assocode, String timeZone,String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<String> al_list=new ArrayList<String>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try{
			pst=con.prepareStatement("SELECT TIMESTAMPDIFF(SECOND,ONSITE.ORL_TIME,START.ORL_TIME) AS ONSITE_DIFFERENCE, TIMESTAMPDIFF(SECOND,START.ORL_TIME,END.ORL_TIME) AS TRIP_DIFFERENCE, DATE_FORMAT(CONVERT_TZ(START.ORL_TIME,'UTC','"+timeZone+"'),'%Y-%m-%dT%T.%f') AS ST_FORMATTED,DATE_FORMAT(CONVERT_TZ(END.ORL_TIME,'UTC','"+timeZone+"'),'%Y-%m-%dT%T.%f') AS ET_FORMATTED "
					+ " from TDS_OPENREQUEST_LOGS AS START LEFT JOIN TDS_OPENREQUEST_LOGS AS ONSITE ON ONSITE.ORL_ASSOCCODE = START.ORL_ASSOCCODE AND ONSITE.ORL_TRIPID=START.ORL_TRIPID AND ONSITE.ORL_REASON='Driver Onsite' "
					+ " LEFT JOIN TDS_OPENREQUEST_LOGS AS END ON START.ORL_ASSOCCODE=END.ORL_ASSOCCODE AND START.ORL_TRIPID=END.ORL_TRIPID AND END.ORL_REASON='Trip Ended' "
					+ " WHERE START.ORL_ASSOCCODE = ? AND START.ORL_TRIPID=? AND START.ORL_REASON='Trip Started'  ");
			pst.setString(1, assocode);
			pst.setString(2, tripId);
			
			//System.out.println("Pst--->"+pst);
			rs = pst.executeQuery();
			if(rs.next())
			{
				al_list.add(rs.getString("ONSITE_DIFFERENCE")==null?"0":rs.getString("ONSITE_DIFFERENCE"));
				al_list.add(rs.getString("TRIP_DIFFERENCE")==null?"0":rs.getString("TRIP_DIFFERENCE"));
				al_list.add(rs.getString("ST_FORMATTED")==null?"":rs.getString("ST_FORMATTED"));
				al_list.add(rs.getString("ET_FORMATTED")==null?"":rs.getString("ET_FORMATTED"));
			}

		}
		catch(SQLException sqex) {
			cat.error("TDSException UtilityDAO.getFormattedTimeDetailsForWasl-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	
	public static ArrayList<OpenRequestBO> getWaslPendingTrips(String assocode, String timeZone){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestBO> al_list=new ArrayList<OpenRequestBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try{
			pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_WASL WHERE ORW_UPDATEDTIME < DATE_SUB(NOW(),INTERVAL 10 MINUTE)");
			//System.out.println("Pst--->"+pst);
			rs = pst.executeQuery();
			while(rs.next())
			{
				OpenRequestBO openBo = new OpenRequestBO();
				openBo.setTripid(rs.getString("ORW_TRIPID"));
				openBo.setJobRating(rs.getInt("ORW_RATING"));
				openBo.setCustomField(rs.getString("ORW_JSON"));
				openBo.setAssociateCode(rs.getString("ORW_ASSOCCODE"));
				al_list.add(openBo);
			}

		}
		catch(SQLException sqex) {
			cat.error("TDSException UtilityDAO.getFormattedTimeDetailsForWasl-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}
	
	public static int insertIntoWaslOpenrequest(String tripid,String json, String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.insertIntoWaslOpenrequest");
		int status = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			pst=con.prepareStatement("INSERT INTO TDS_OPENREQUEST_WASL (ORW_TRIPID, ORW_RATING, ORW_ASSOCCODE, ORW_JSON, ORW_UPDATEDTIME) VALUES(?,?,?,?,NOW())");
			pst.setString(1,tripid);
			pst.setInt(2, 0);
			pst.setString(3, assoccode);
			pst.setString(4,json);
			status=pst.executeUpdate();
			
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.insertIntoWaslOpenrequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.printStackTrace();		
		}

		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	
	public static int updateWaslReferencekey(String asscoocde,String tripid,String referenceKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateWaslReferencekey");
		int status = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			pst=con.prepareStatement("UPDATE TDS_OPENREQUEST_HISTORY SET ORH_CUST_REF_NUM=? WHERE (ORH_ASSOCCODE=? OR ORH_MASTER_ASSOCCODE=?) AND ORH_TRIP_ID=? ");
			pst.setString(1, referenceKey);
			pst.setString(2, asscoocde);
			pst.setString(3, asscoocde);
			pst.setString(4,tripid);
			status=pst.executeUpdate();
			
			pst=con.prepareStatement("DELETE FROM TDS_OPENREQUEST_WASL WHERE ORW_ASSOCCODE=? AND ORW_TRIPID=?");
			pst.setString(1, asscoocde);
			pst.setString(2, tripid);
			status=pst.executeUpdate();
			
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateWaslReferencekey-->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.printStackTrace();		
		}

		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	
	public static OpenRequestBO getWaslTripByTripId(String assocode, String tripId){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		OpenRequestBO openBo = null;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try{
			pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_WASL WHERE ORW_ASSOCCODE=? AND ORW_TRIPID=?");
			pst.setString(1, assocode);
			pst.setString(2, tripId);
			//System.out.println("Pst--->"+pst);
			rs = pst.executeQuery();
			while(rs.next())
			{
				openBo = new OpenRequestBO();
				openBo.setTripid(rs.getString("ORW_TRIPID"));
				openBo.setJobRating(rs.getInt("ORW_RATING"));
				openBo.setCustomField(rs.getString("ORW_JSON"));
				openBo.setAssociateCode(rs.getString("ORW_ASSOCCODE"));
			}

		}
		catch(SQLException sqex) {
			cat.error("TDSException UtilityDAO.getWaslTripByTripId-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return openBo;
	}
	
	public static int updateWaslRating(String asscoocde,String tripid,String rating){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.updateWaslReferencekey");
		int status = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon=new TDSConnection();
		con=dbcon.getConnection();
		try{
			pst=con.prepareStatement("UPDATE TDS_OPENREQUEST_WASL SET ORW_RATING='"+rating+"' WHERE ORW_ASSOCCODE=? AND ORW_TRIPID=? ");
			pst.setString(1, asscoocde);
			pst.setString(2, tripid);
			System.out.println("wasl update : "+pst.toString());
			status=pst.executeUpdate();
			
			pst.close();
			con.close();
		}
		catch(SQLException sqex) {
			cat.error("TDSException RequestDAO.updateWaslReferencekey-->"+sqex.getMessage());
			cat.error(pst.toString());
			sqex.printStackTrace();		
		}

		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
}
