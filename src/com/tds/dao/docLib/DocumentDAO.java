package com.tds.dao.docLib;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Category;

import com.tds.bean.docLib.DocBean;
import com.tds.constants.docLib.DocumentConstant;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;

public class DocumentDAO {
	private static Category cat =TDSController.cat;

	public List<DocBean> getDocumentList(String role, String compCodeOrUserId,
			String completeQuery) {
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		List<DocBean> docBeanList = new ArrayList<DocBean>();
		String query = "";

		try {

			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if (completeQuery != null && !completeQuery.equals("")) {
				pst = con.prepareStatement(completeQuery);
			} else if (role.equalsIgnoreCase("admin")) {
				query = "select *,DATE_FORMAT(DOC_EXP_DATE,'%m/%d/%Y') AS EXPIRY_DATE from TDS_DOCUMENTS where DOC_STATUS=? and TDS_COMP_CODE=?";
				pst = con.prepareStatement(query);
				pst.setString(1, DocumentConstant.STATUS_PENDING);
				pst.setString(2, compCodeOrUserId);
				DocumentMaterDAO dmDao = new DocumentMaterDAO();
				// request.setAttribute("docTypeData", dmDao.getMasterData());
			} else {
				query = "select *,DATE_FORMAT(DOC_EXP_DATE,'%m/%d/%Y') AS EXPIRY_DATE from TDS_DOCUMENTS where DOC_UPLOADED_BY=?";
				pst = con.prepareStatement(query);
				pst.setString(1, compCodeOrUserId);
			}
			rs = pst.executeQuery();

			while (rs.next()) {
				DocBean doc = new DocBean();
				doc.setDocId(rs.getInt("DOCUMENT_ID"));
				doc.setDocName(rs.getString("DOCUMENT_NAME"));
				doc.setDocStatus(rs.getString("DOC_STATUS"));
				doc.setDocExpiry(rs.getString("EXPIRY_DATE"));
				doc.setDocNumber(rs.getString("DOC_NUMBER"));
				doc.setDocType(rs.getString("DOC_TYPE"));
				doc.setTdsCompCode(rs.getString("TDS_COMP_CODE"));
				doc.setDocUploadedBy(rs.getString("DOC_UPLOADED_BY"));


				docBeanList.add(doc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				rs.close();
				pst.close();
				con.close();
				dbcon.closeConnection();
			} catch (Exception e) {

			}
		}

		return docBeanList;

	}

	public void deleteDocument(String docId) {
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;

		try {
			String query = "";
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			query = "delete from TDS_DOCUMENTS where DOCUMENT_ID=" + docId;
			pst = con.prepareStatement(query);
			pst.execute(query);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {

				pst.close();
				con.close();
				dbcon.closeConnection();
			} catch (Exception e) {

			}
		}

	}

	public ArrayList<String> updateDocStatus(String docId, String status, String userId) {

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int count = 0;
		ArrayList<String> mailContents= new ArrayList<String>();
		ResultSet rs=null;
		try {
			String query = "";
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			query = "update TDS_DOCUMENTS set DOC_STATUS= '" + status
			+ "', DOC_MODIFIED_BY='" + userId + "' where DOCUMENT_ID="
			+ docId;
			pst = con.prepareStatement(query);
			count = pst.executeUpdate(query);
			if (count > 0 && status.equals(DocumentConstant.STATUS_REJECT)) {
				query="SELECT * FROM TDS_DOCUMENTS WHERE DOCUMENT_ID="+docId;
				pst=con.prepareStatement(query);
				rs=pst.executeQuery();
				if(rs.next()){
					mailContents.add(rs.getString("DOC_EMAIL_ID"));
					mailContents.add(rs.getString("DOC_TYPE"));
					mailContents.add(rs.getString("DOCUMENT_NAME"));
					deleteDocument(docId);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {

				pst.close();
				con.close();
				dbcon.closeConnection();
			} catch (Exception e) {

			}
		}
		return mailContents;
	}

	public ArrayList<String> getMailAddress(String associationCode) {
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<String> emailAddressandPassword = new ArrayList<String>();
		try {
			String query = "";
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			query="SELECT * FROM TDS_EMAIL WHERE EM_ASSOCCODE="+associationCode;
			pst = con.prepareStatement(query);
			rs = pst.executeQuery();
			if(rs.next()){
				emailAddressandPassword.add(rs.getString("EM_USERID"));
				emailAddressandPassword.add(rs.getString("EM_PASSWORD"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pst.close();
				con.close();
				dbcon.closeConnection();
			} catch (Exception e) {

			}
		}
		return emailAddressandPassword;
	}



public ArrayList<String> updateDocInfo(DocBean docBean, String userId) {

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		int count = 0;
		ResultSet rs=null;
		ArrayList<String > mailContents =new ArrayList<String>();
		try {
			String query = "";
			String date[]=docBean.getDocExpiry().split("/");
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("UPDATE TDS_DOCUMENTS SET DOC_EXP_DATE='"+date[2]+"-"+date[0]+"-"+date[1]+" 00:00:00"+"',DOC_EXP_SEVERITY=?,DOC_STATUS='APPROVED',DOC_TYPE=?,DOC_NUMBER=?,TDS_MODULE=?,DOC_UPLOADED_BY=?,DOC_DRIVER_OR_CAB=? WHERE DOCUMENT_ID=?");
			pst.setString(1, docBean.getServerity());
			pst.setString(2, docBean.getDocType());
			pst.setString(3, docBean.getDocNumber());
			pst.setString(4, docBean.getDocModule());
			pst.setString(5, docBean.getDocUploadedBy());
			pst.setInt(6, docBean.getDriverOrCab());
			pst.setInt(7, docBean.getDocId());
			count = pst.executeUpdate();
			if (count > 0 ) {
				query="SELECT * FROM TDS_DOCUMENTS WHERE DOCUMENT_ID="+docBean.getDocId();
				pst=con.prepareStatement(query);
				rs=pst.executeQuery();
				if(rs.next()){
					mailContents.add(rs.getString("DOC_EMAIL_ID"));
					mailContents.add(rs.getString("DOC_TYPE"));
					mailContents.add(rs.getString("DOCUMENT_NAME"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {

				pst.close();
				con.close();
				dbcon.closeConnection();
			} catch (Exception e) {
			}
		}
		return mailContents;
	}
	
	public static ArrayList<DocBean> getExpiredLicenses(String assoccode){
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<DocBean> expiredLicenses =new ArrayList<DocBean>();
		try{
			String query="select *,DATE_FORMAT(DOC_EXP_DATE,'%m/%d/%Y') AS EXPIRY_DATE from TDS_DOCUMENTS where TDS_COMP_CODE='"+assoccode+"' AND DOC_EXP_DATE>=NOW() and DOC_EXP_DATE <=DATE_ADD(CURDATE(),INTERVAL 7 DAY) ";
			pst = con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				DocBean document= new DocBean();
				document.setDocName(rs.getString("DOCUMENT_NAME"));
				document.setDocType(rs.getString("DOC_TYPE"));
				document.setDocExpiry(rs.getString("EXPIRY_DATE"));
				document.setDocUploadedBy(rs.getString("DOC_UPLOADED_BY"));
				expiredLicenses.add(document);
			}
		}catch(Exception sqex){
			cat.error("TDSException DocumentDAO.getExpiredLicenses-->"+sqex.getMessage());
			//System.out.println("TDSException DocumentDAO.getExpiredLicenses-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		return expiredLicenses;
	}
	public static ArrayList<DocBean> getDocumentId(String assoccode,String driverId,int value){
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs =null;
		ArrayList<DocBean> documentList =new ArrayList<DocBean>();
		String query="SELECT * FROM TDS_DOCUMENTS where TDS_COMP_CODE='"+assoccode+"' AND DOC_UPLOADED_BY='"+driverId+"'  AND DOC_EXP_DATE>=NOW()  ";
		if(value==1){
			query=query+" AND DOC_DRIVER_OR_CAB='1'";	
		}else if (value==2){
			query=query+" AND DOC_DRIVER_OR_CAB='2'";	
		}
		
		try{
			pst = con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				DocBean document= new DocBean();
				document.setDocId(rs.getInt("DOCUMENT_ID"));
				document.setDocName(rs.getString("DOCUMENT_NAME"));
				documentList.add(document);
			}
		}catch(Exception sqex){
			cat.error("TDSException DocumentDAO.getDocumentId-->"+sqex.getMessage());
			//System.out.println("TDSException DocumentDAO.getDocumentId-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		return documentList;
	}
	public static int archiveFile(String assoccode,String docId){
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		String query="UPDATE TDS_DOCUMENTS SET  DOC_STATUS='ARCHIVED' WHERE DOCUMENT_ID='"+docId+"' AND TDS_COMP_CODE='"+assoccode+"'";
		
		try{
			pst = con.prepareStatement(query);
			result=pst.executeUpdate();
		}catch(Exception sqex){
			cat.error("TDSException DocumentDAO.getDocumentId-->"+sqex.getMessage());
			//System.out.println("TDSException DocumentDAO.getDocumentId-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		return result;
	}
	public static int deleteFile(String assoccode,String docId){
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		String query="DELETE FROM TDS_DOCUMENTS WHERE DOCUMENT_ID='"+docId+"' AND TDS_COMP_CODE='"+assoccode+"'";
		
		try{
			pst = con.prepareStatement(query);
			result=pst.executeUpdate();
		}catch(Exception sqex){
			cat.error("TDSException DocumentDAO.getDocumentId-->"+sqex.getMessage());
			//System.out.println("TDSException DocumentDAO.getDocumentId-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		return result;
	}


}
