package com.tds.dao.docLib;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.tds.bean.docLib.NameValueTag;
import com.tds.db.TDSConnection;

public class DocumentMaterDAO {

	public static List<NameValueTag> getMasterData(String associationCode,int value) {
		List<NameValueTag> nameValueList = new ArrayList<NameValueTag>();
		TDSConnection dbcon = null;
		Connection con = null;
		Statement pst = null;
		ResultSet rs = null;

		try {
			String query = "";

			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(value==0){
				query = "select * from TDS_DOCUMENTS_MASTER where DM_ASSOCCODE= "+associationCode;
			}else if(value==1){
				query = "select * from TDS_DOCUMENTS_MASTER where DM_ASSOCCODE= "+associationCode+" AND DM_DOCUMENT_BELONGS_TO='Driver/Operator'";
			}else if(value==2){
				query = "select * from TDS_DOCUMENTS_MASTER where DM_ASSOCCODE= "+associationCode+" AND DM_DOCUMENT_BELONGS_TO='Vehicle'";

			}
			pst = con.createStatement();
			rs = pst.executeQuery(query);

			while (rs.next()) {
				NameValueTag nameTag = new NameValueTag();
				nameTag.setDocType(rs.getString("DOCUMENT_TYPE"));
				nameTag.setModule(rs.getString("MODULE_NAME"));
				nameTag.setSeverity(rs.getString("SERVERITY_TYPE"));
				nameValueList.add(nameTag);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pst.close();
				con.close();
				dbcon.closeConnection();
			} catch (Exception e) {

			}

		}

		return nameValueList;
	}

	public String getServerity(String associationCode,String docType) {
		TDSConnection dbcon = null ;
		Connection con = null;
		java.sql.PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		ResultSet rs = null;
		String severity="";
		String moduleName="";
		String severityandModule="";
		String query = "";
		try {
			query = "SELECT SERVERITY_TYPE,MODULE_NAME FROM TDS_DOCUMENTS_MASTER where DM_ASSOCCODE="+associationCode+" AND DOCUMENT_TYPE='"+docType+"'";
			pst=con.prepareStatement(query);
			rs = pst.executeQuery();
			if (rs.next()) {
				severity=rs.getString("SERVERITY_TYPE");
				moduleName=rs.getString("MODULE_NAME");
				severityandModule=docType+"###"+severity+"###"+moduleName;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pst.close();
				con.close();
				dbcon.closeConnection();
			} catch (Exception e) {

			}

		}
		return severityandModule;
	}
}
