package com.tds.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.CompanyccsetupDO;



public class CompanySetupDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(CompanySetupDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+CompanySetupDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	
	public static boolean deleteCompanySetup(ArrayList al_list,String p_assocode) {	
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CompanySetupDAO.deleteCompanySetup");
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			for(int i=0;i<al_list.size();i++)
			{
				CompanyccsetupDO CompanyccsetupDO = (CompanyccsetupDO)al_list.get(i);

				if(CompanyccsetupDO.getChk().equals("1")){
					m_pst = m_conn.prepareStatement("delete from TDS_COMPANY_CC_SETUP where CCC_COMPANY_ID = ? and CCC_CARD_TYPE = ? and CCC_CARD_ISSUER = ? and CCC_SWIPE_TYPE = ?");
					m_pst.setString(1, p_assocode);	
					m_pst.setString(2, CompanyccsetupDO.getCCC_CARD_TYPE());
					m_pst.setString(3, CompanyccsetupDO.getCCC_CARD_ISSUER()); 
					m_pst.setString(4, CompanyccsetupDO.getCCC_SWIPE_TYPE());
					cat.info(m_pst.toString());
					m_pst.execute();
				}

			}

		} catch (SQLException sqex) {
			cat.error("TDSException CompanySetupDAO.deleteCompanySetup-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException CompanySetupDAO.deleteCompanySetup-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_result;

	}
	public static boolean insertCompanySetup(ArrayList al_list,String ins_flg,String p_assocode) {	
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CompanySetupDAO.insertCompanySetup");
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		try {
			m_conn.setAutoCommit(false);
			m_pst=m_conn.prepareStatement("DELETE FROM TDS_COMPANY_CC_SETUP WHERE CCC_COMPANY_ID='"+p_assocode+"'");
			m_pst.execute();
			for(int i=0;i<al_list.size();i++)
			{
				CompanyccsetupDO CompanyccsetupDO = (CompanyccsetupDO)al_list.get(i);
				m_pst = m_conn.prepareStatement("insert into TDS_COMPANY_CC_SETUP(CCC_COMPANY_ID,CCC_CARD_TYPE,CCC_CARD_ISSUER,CCC_TXN_AMT_PERCENTAGE,CCC_TXN_AMOUNT,CCC_SWIPE_TYPE,CCC_TEMPLATE_TYPE) values(?,?,?,?,?,?,?)");
				m_pst.setString(1, p_assocode);	
				m_pst.setString(2, CompanyccsetupDO.getCCC_CARD_TYPE());
				m_pst.setString(3, CompanyccsetupDO.getCCC_CARD_ISSUER()); 
				m_pst.setString(4, CompanyccsetupDO.getCCC_TXN_AMT_PERCENTAGE());
				m_pst.setString(5, CompanyccsetupDO.getCCC_TXN_AMOUNT());
				m_pst.setString(6, CompanyccsetupDO.getCCC_SWIPE_TYPE());
				m_pst.setInt(7, CompanyccsetupDO.getCCC_TEMPLATE_TYPE());

				cat.info(m_pst.toString());
				m_pst.execute();				
			}
			m_conn.commit();
			m_conn.setAutoCommit(true);
			m_result= true;
		} catch (SQLException sqex) {
			cat.error("TDSException CompanySetupDAO.insertCompanySetup-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException CompanySetupDAO.insertCompanySetup-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_result;
	}
	public static ArrayList<CompanyccsetupDO> Companysetupadd(String p_asscode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CompanySetupDAO.Companysetupadd");
		ArrayList<CompanyccsetupDO> al_list = new ArrayList<CompanyccsetupDO>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs = null;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			m_pst = m_conn.prepareStatement("select * from TDS_COMPANY_CC_SETUP where CCC_COMPANY_ID=?");
			m_pst.setString(1, p_asscode);
			cat.info(m_pst.toString());
			rs = m_pst.executeQuery();
			while(rs.next())
			{
				CompanyccsetupDO CompanyccsetupDO = new CompanyccsetupDO();
				CompanyccsetupDO.setCCC_TEMPLATE_TYPE(Integer.parseInt(rs.getString("CCC_TEMPLATE_TYPE")));
				CompanyccsetupDO.setCCC_COMPANY_ID(rs.getString("CCC_COMPANY_ID"));
				CompanyccsetupDO.setCCC_CARD_TYPE(rs.getString("CCC_CARD_TYPE"));
				CompanyccsetupDO.setCCC_CARD_ISSUER(rs.getString("CCC_CARD_ISSUER"));
				CompanyccsetupDO.setCCC_TXN_AMT_PERCENTAGE(rs.getString("CCC_TXN_AMT_PERCENTAGE"));
				CompanyccsetupDO.setCCC_TXN_AMOUNT(rs.getString("CCC_TXN_AMOUNT"));
				CompanyccsetupDO.setCCC_SWIPE_TYPE(rs.getString("CCC_SWIPE_TYPE"));
				al_list.add(CompanyccsetupDO);
			}  
		} catch (SQLException sqex) {
			cat.error("TDSException CompanySetupDAO.Companysetupadd-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException CompanySetupDAO.Companysetupadd-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;

	}

	public static boolean updateCompanySetup(ArrayList al_list,String ins_flg,String p_assocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CompanySetupDAO.updateCompanySetup");
		boolean m_result = false;
		int flg=0;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();

			for(int i=0;i<al_list.size();i++)
			{
				CompanyccsetupDO CompanyccsetupDO = (CompanyccsetupDO)al_list.get(i);

				m_pst = m_conn.prepareStatement("update TDS_COMPANY_CC_SETUP set CCC_TXN_AMT_PERCENTAGE = ?,CCC_TXN_AMOUNT = ? where CCC_COMPANY_ID = ? and CCC_CARD_TYPE=? and CCC_CARD_ISSUER=? and CCC_SWIPE_TYPE=?");

				m_pst.setString(1, CompanyccsetupDO.getCCC_TXN_AMT_PERCENTAGE());
				m_pst.setString(2, CompanyccsetupDO.getCCC_TXN_AMOUNT());
				m_pst.setString(3, p_assocode);	
				m_pst.setString(4, CompanyccsetupDO.getCCC_CARD_TYPE());
				m_pst.setString(5, CompanyccsetupDO.getCCC_CARD_ISSUER());
				m_pst.setString(6, CompanyccsetupDO.getCCC_SWIPE_TYPE());
				cat.info(m_pst.toString());
				flg = m_pst.executeUpdate();	

				if(flg == 0)
				{ 
					if(i==0){
						m_pst=m_conn.prepareStatement("DELETE FROM TDS_COMPANY_CC_SETUP WHERE CCC_COMPANY_ID='"+p_assocode+"'");
						m_pst.execute();
					}	
					m_pst = m_conn.prepareStatement("insert into TDS_COMPANY_CC_SETUP(CCC_COMPANY_ID,CCC_CARD_TYPE,CCC_CARD_ISSUER,CCC_TXN_AMT_PERCENTAGE,CCC_TXN_AMOUNT,CCC_SWIPE_TYPE) values(?,?,?,?,?,?)");
					m_pst.setString(1, p_assocode);	
					m_pst.setString(2, CompanyccsetupDO.getCCC_CARD_TYPE());
					m_pst.setString(3, CompanyccsetupDO.getCCC_CARD_ISSUER()); 
					m_pst.setString(4, CompanyccsetupDO.getCCC_TXN_AMT_PERCENTAGE());
					m_pst.setString(5, CompanyccsetupDO.getCCC_TXN_AMOUNT());
					m_pst.setString(6, CompanyccsetupDO.getCCC_SWIPE_TYPE());
					m_pst.execute();

				}else {
					flg =0;
				}
			}
			m_result=true;
			m_pst.close(); 
		} catch (SQLException sqex) {
			m_result=false;
			cat.error("TDSException CompanySetupDAO.updateCompanySetup-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException CompanySetupDAO.updateCompanySetup-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_result;	
	}

}

