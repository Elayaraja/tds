package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
public class VoiceMessageDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(VoiceMessageDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+VoiceMessageDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}

	
	public static int updateVoiceMessageByKey(String associationCode,String drivers,byte[] mp3, int channel,int key){
		long startTime = System.currentTimeMillis();
		cat.info("VoiceMessageDAO.updateVoiceMessageByKey Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int returnCode = 0;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		String query = "UPDATE TDS_VOICE_MESSAGE SET VM_TIME=now(), VM_MESSAGE=?, VM_CHANNEL=?, VM_PLAYED_BY=';' WHERE VM_ASSOCIATION_CODE=? AND VM_KEY=?";
		try {
			m_pst = m_conn.prepareStatement(query);
			m_pst.setBytes(1, mp3);
			m_pst.setInt(2, channel);
			m_pst.setString(3, associationCode);
			m_pst.setInt(4, key);
			returnCode=m_pst.executeUpdate(); 
		}catch (Exception e){
			cat.error("Error in VoiceMessageDAO.updateVoiceMessageByKey" + e.getMessage());
			//System.out.println("Error in VoiceMessageDAO.updateVoiceMessageByKey" + e.getMessage());
			e.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("VoiceMessageDAO.updateVoiceMessageByKey End--->"+(System.currentTimeMillis()-startTime));
		return returnCode;
	}

	public static int insertPendingVoiceMessage(String associationCode, int channel,String driverIds){
		long startTime = System.currentTimeMillis();
		cat.info("VoiceMessageDAO.insertPendingVoiceMessage Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int messageKey = 0;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		String query = "INSERT INTO TDS_VOICE_MESSAGE (VM_ASSOCIATION_CODE, VM_CHANNEL, VM_TIME, VM_PLAYED_BY,VM_MSG_STATUS,VM_DRIVER_IDS,VM_MESSAGE) VALUES (?,?,now(),';', 0,?,?)";
		try {
			m_pst = m_conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			m_pst.setString(1, associationCode);
			m_pst.setInt(2, channel);
			m_pst.setString(3, driverIds);
			m_pst.setBytes(4, new byte[7]);
			if(m_pst.executeUpdate()>0) {
				ResultSet rs = m_pst.getGeneratedKeys();
				if (rs.next()) {
					messageKey = rs.getInt(1);
				}
			}
		}catch (Exception e){
			cat.error("Error in VoiceMessageDAO.insertPendingVoiceMessage" + e.getMessage());
			//System.out.println("Error in VoiceMessageDAO.insertPendingVoiceMessage" + e.getMessage());
			e.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("VoiceMessageDAO.insertPendingVoiceMessage End--->"+(System.currentTimeMillis()-startTime));
		return messageKey;
	}

//	public static int updateVoiceMessage(String associationCode,String drivers,byte[] mp3, int channel){
//		long startTime = System.currentTimeMillis();
//		cat.info("VoiceMessageDAO.readZonesBoundries Start--->"+startTime);
//		TDSConnection m_dbConn = null;
//		Connection m_conn = null;
//		PreparedStatement m_pst = null;
//		int returnCode = 0;
//		int messageKey = 0;
//		m_dbConn = new TDSConnection();
//		m_conn = m_dbConn.getConnection();
//		String query = "UPDATE TDS_VOICE_MESSAGE SET  VM_DRIVER_IDS, VM_TIME, VM_MESSAGE, VM_CHANNEL, VM_PLAYED_BY) VALUES (?,?,now(), ?, ?,';')";
//		try {
//			m_pst = m_conn.prepareStatement(query);
//			m_pst.setString(1, associationCode);
//			if (drivers.equals("")){
//				m_pst.setString(2, null);
//			} else {
//				m_pst.setString(2, drivers);
//			}
//			m_pst.setBytes(3, mp3);
//			m_pst.setInt(4, channel);
//			if(m_pst.executeUpdate()>0) {
//				returnCode = 1;
//				ResultSet rs = m_pst.getGeneratedKeys();
//				if (rs.next()) {
//					messageKey = rs.getInt(1);
//				}
//			}
//		}catch (Exception e){
//			cat.error("Error in VoiceMessageDAO.insertVoiceMessage" + e.getMessage());
//			System.out.println("Error in VoiceMessageDAO.insertVoiceMessage" + e.getMessage());
//			e.printStackTrace();
//		} 
//		finally {
//			m_dbConn.closeConnection();
//		}
//		cat.info("VoiceMessageDAO.readZonesBoundries End--->"+(System.currentTimeMillis()-startTime));
//		return messageKey;
//	}

	public static int insertVoiceMessage(String associationCode,String drivers,byte[] mp3, int channel){
		long startTime = System.currentTimeMillis();
		cat.info("VoiceMessageDAO.insertVoiceMessage Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int returnCode = 0;
		int messageKey = 0;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		String query = "INSERT INTO TDS_VOICE_MESSAGE (VM_ASSOCIATION_CODE, VM_DRIVER_IDS, VM_TIME, VM_MESSAGE, VM_CHANNEL, VM_PLAYED_BY) VALUES (?,?,now(), ?, ?,';')";
		try {
			m_pst = m_conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			m_pst.setString(1, associationCode);
			if (drivers.equals("")){
				m_pst.setString(2, null);
			} else {
				m_pst.setString(2, drivers);
			}
			m_pst.setBytes(3, mp3);
			m_pst.setInt(4, channel);
			if(m_pst.executeUpdate()>0) {
				returnCode = 1;
				ResultSet rs = m_pst.getGeneratedKeys();
				if (rs.next()) {
					messageKey = rs.getInt(1);
				}
			}
		}catch (Exception e){
			cat.error("Error in VoiceMessageDAO.insertVoiceMessage" + e.getMessage());
			//System.out.println("Error in VoiceMessageDAO.insertVoiceMessage" + e.getMessage());
			e.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("VoiceMessageDAO.insertVoiceMessage End--->"+(System.currentTimeMillis()-startTime));
		return messageKey;
	}

	public static byte[] getVoiceMessage(String associationCode,String driverID, String messageKey){
		long startTime = System.currentTimeMillis();
		cat.info("VoiceMessageDAO.getVoiceMessage Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		//int returnCode = 0;
		ResultSet m_rs = null;
		byte[] mp3 = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		String query = "SELECT VM_MESSAGE FROM TDS_VOICE_MESSAGE WHERE VM_KEY = ? AND (LOCATE(?,VM_DRIVER_IDS)>0 OR VM_DRIVER_IDS IS NULL) AND VM_ASSOCIATION_CODE = ?";
		try {
			m_pst = m_conn.prepareStatement("DELETE FROM TDS_VOICE_MESSAGE WHERE TIMESTAMPDIFF(MINUTE,VM_TIME,NOW())>50");
			m_pst.execute();
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1,messageKey);
			m_pst.setString(2,driverID);
			m_pst.setString(3,associationCode);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()){
				mp3 = m_rs.getBytes("VM_MESSAGE");
			}
		}catch (Exception e){
			cat.error("Error in VoiceMessageDAO.getVoiceMessage" + e.getMessage());
			//System.out.println("Error in VoiceMessageDAO.getVoiceMessage" + e.getMessage());
			e.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("VoiceMessageDAO.getVoiceMessage End--->"+(System.currentTimeMillis()-startTime));
		return mp3;
	}

	public static byte[] getVoiceMessageOperator(String associationCode,String driverID, String messageKey){
		long startTime = System.currentTimeMillis();
		cat.info("VoiceMessageDAO.getVoiceMessageOperator Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		byte[] mp3 = null;
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		String query = "SELECT VM_MESSAGE FROM TDS_VOICE_MESSAGE WHERE VM_KEY = ? AND VM_ASSOCIATION_CODE = ?";
		try {
			m_pst = m_conn.prepareStatement("DELETE FROM TDS_VOICE_MESSAGE WHERE TIMESTAMPDIFF(MINUTE,VM_TIME,NOW())>50");
			m_pst.execute();
			m_pst = m_conn.prepareStatement(query);
			m_pst.setString(1,messageKey);
			m_pst.setString(2,associationCode);
			m_rs = m_pst.executeQuery();
			if(m_rs.first()){
				mp3 = m_rs.getBytes("VM_MESSAGE");
			}
		}catch (Exception e){
			cat.error("Error in VoiceMessageDAO.getVoiceMessage" + e.getMessage());
			//System.out.println("Error in VoiceMessageDAO.getVoiceMessage" + e.getMessage());
			e.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("VoiceMessageDAO.getVoiceMessageOperator End--->"+(System.currentTimeMillis()-startTime));
		return mp3;
	}

	public static ArrayList<String> getNewMessageIDs(String associationCode,String operatorID, String[] driverList){
		long startTime = System.currentTimeMillis();
		cat.info("VoiceMessageDAO.getNewMessageIDs Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<String> messageIDs = new ArrayList<String>();
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		String query = "SELECT VM_DRIVER_IDS, VM_KEY FROM TDS_VOICE_MESSAGE WHERE VM_ASSOCIATION_CODE = ? AND LOCATE(?,VM_PLAYED_BY)=0 AND VM_CHANNEL = 1 ";
		if(driverList.length>0){
			String addQuery = "AND VM_DRIVER_IDS IN( ";
			for(int i =0;i<driverList.length-1;i++){
				addQuery = addQuery + ",'"+driverList[i]+"',";
			}
			addQuery = addQuery + ",'"+driverList[driverList.length-1]+"'";
			query = query + addQuery;
		}
		try {
			pst = m_conn.prepareStatement(query);
			pst.setString(1,associationCode);
			pst.setString(2,";"+operatorID+";");
			rs = pst.executeQuery();
			while(rs.next()){
				messageIDs.add(rs.getString("VM_KEY"));
				messageIDs.add(rs.getString("VM_DRIVER_IDS"));
			}
		}catch (Exception e){
			cat.error("Error in VoiceMessageDAO.getNewMessageIDs" + e.getMessage());
			//System.out.println("Error in VoiceMessageDAO.getNewMessageIDs" + e.getMessage());
			e.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("VoiceMessageDAO.getNewMessageIDs End--->"+(System.currentTimeMillis()-startTime));
		return messageIDs;
	}

	public static int updateMessageAsRead(String associationCode,String operatorID, String[] messageIDs){
		long startTime = System.currentTimeMillis();
		cat.info("VoiceMessageDAO.updateMessageAsRead Start--->"+startTime);
		int returnCode = 0;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement pst = null;
		String whereQuery = "";
		if(messageIDs.length>0){
			whereQuery = " AND VM_KEY IN(";
			for(int i = 0;i<messageIDs.length-1;i++){
				whereQuery = whereQuery + " '"+messageIDs[i]+"',";
			}
			whereQuery = whereQuery + " '"+messageIDs[messageIDs.length-1]+"')";
		}
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query = "UPDATE TDS_VOICE_MESSAGE SET VM_PLAYED_BY = CONCAT(VM_PLAYED_BY,?,';') WHERE VM_ASSOCIATION_CODE =? " + whereQuery;
			pst = m_conn.prepareStatement(query);
			pst.setString(1,operatorID);
			pst.setString(2,associationCode);
			returnCode = pst.executeUpdate();
		}catch (Exception e){
			cat.error("Error in VoiceMessageDAO.updateMessageAsRead" + e.getMessage());
			//System.out.println("Error in VoiceMessageDAO.updateMessageAsRead" + e.getMessage());
			e.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("VoiceMessageDAO.updateMessageAsRead End--->"+(System.currentTimeMillis()-startTime));
		return returnCode;
	}

	public static int updateAllMessagesAsRead(String associationCode,String operatorID){
		long startTime = System.currentTimeMillis();
		cat.info("VoiceMessageDAO.updateAllMessagesAsRead Start--->"+startTime);
		int returnCode = 0;
		PreparedStatement pst = null;
		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection();
		String query = "UPDATE TDS_VOICE_MESSAGE SET VM_PLAYED_BY = CONCAT(VM_PLAYED_BY,?,';') WHERE VM_ASSOCIATION_CODE =? AND LOCATE('"+operatorID+";'"+",VM_PLAYED_BY)=0";
		try {
			pst = m_conn.prepareStatement(query);
			pst.setString(1,operatorID);
			pst.setString(2,associationCode);
			returnCode = pst.executeUpdate();
		}catch (Exception e){
			cat.error("Error in VoiceMessageDAO.updateAllMessagesAsRead" + e.getMessage());
			//System.out.println("Error in VoiceMessageDAO.updateAllMessagesAsRead" + e.getMessage());
			e.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("VoiceMessageDAO.updateAllMessagesAsRead End--->"+(System.currentTimeMillis()-startTime));
		return returnCode;
	}

//	public static String getCabNumber(String assoccode, String driverNumber)
//	{
//		long startTime = System.currentTimeMillis();
//		cat.info("VoiceMessageDAO.readZonesBoundries Start--->"+startTime);
//		TDSConnection m_dbConn = null;
//		Connection m_conn = null;
//		PreparedStatement m_pst = null;
//		ResultSet rs = null;
//		String vehicleNo="";
//		String finalSQL = "SELECT DL_VEHICLE_NO FROM TDS_DRIVERLOCATION DL  WHERE " +
//				"DL_ASSOCCODE='"+assoccode+ "' AND DL_SWITCH <> 'L' AND DL_DRIVER_ID='" + driverNumber +"'";
//		m_dbConn  = new TDSConnection();
//		m_conn = m_dbConn.getConnection();
//		try {
//			m_pst = m_conn.prepareStatement(finalSQL);
//			rs = m_pst.executeQuery();			
//			if(rs.next()){
//				vehicleNo = rs.getString("DL_VEHICLE_NO");
//			}
//		} catch (SQLException sqex) {
//			cat.error(("TDSException VoiceMessageDAO.getCabNumber-->" + sqex.getMessage()));
//			System.out.println("TDSException VoiceMessageDAO.getCabNumber-->"+ sqex.getMessage());
//			sqex.printStackTrace();
//		} finally {
//			m_dbConn.closeConnection();
//		}
//		cat.info("VoiceMessageDAO.readZonesBoundries End--->"+(System.currentTimeMillis()-startTime));
//		return vehicleNo;
//	}
}


