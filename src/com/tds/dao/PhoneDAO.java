package com.tds.dao;

import java.awt.geom.QuadCurve2D;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.AsteriskBean;
import com.tds.cmp.bean.SMSBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.MessageBO;

public class PhoneDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(PhoneDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+PhoneDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static AsteriskBean getAsteriskParameter(String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RequestDAO.getAllOpenRequestForAutoAlloc");
		AsteriskBean asBean=new AsteriskBean();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_ASTERISK WHERE AS_ASSOCIATIONCODE='"+assoccode+"'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				asBean.setUserName(rs.getString("AS_USER_NAME"));
				asBean.setPassword(rs.getString("AS_PASSWORD"));
				asBean.setLocation(rs.getString("AS_LOCATION"));
				asBean.setName(rs.getString("AS_NAME"));
				asBean.setPhoneNumber(rs.getString("AS_PHONE_NUMBER"));
				asBean.setIpAddress(rs.getString("AS_IP_ADDRESS"));
			}
		} catch (SQLException sqex) {
			cat.error("TDSException PhoneDAO.getAsteriskParameter-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException PhoneDAO.getAsteriskParameter-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return asBean;
	}
	public static int updateAsteriskParameter(String assoccode,AsteriskBean asBean) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO RequestDAO.getAllOpenRequestForAutoAlloc");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("UPDATE TDS_ASTERISK SET AS_USER_NAME=?,AS_PASSWORD=?,AS_LOCATION=?,AS_NAME=?,AS_PHONE_NUMBER=?,AS_IP_ADDRESS=? WHERE AS_ASSOCIATIONCODE=?");
			pst.setString(1, asBean.getUserName());
			pst.setString(2, asBean.getPassword());
			pst.setString(3, asBean.getLocation());
			pst.setString(4, asBean.getName());
			pst.setString(5, asBean.getPhoneNumber());
			pst.setString(6, asBean.getIpAddress());
			pst.setString(7, assoccode);
			result=pst.executeUpdate();
			if(result<1){
				pst=con.prepareStatement("INSERT INTO TDS_ASTERISK (AS_USER_NAME,AS_PASSWORD,AS_LOCATION,AS_NAME,AS_PHONE_NUMBER,AS_IP_ADDRESS,AS_ASSOCIATIONCODE) VALUES (?,?,?,?,?,?,?)");
				pst.setString(1, asBean.getUserName());
				pst.setString(2, asBean.getPassword());
				pst.setString(3, asBean.getLocation());
				pst.setString(4, asBean.getName());
				pst.setString(5, asBean.getPhoneNumber());
				pst.setString(6, asBean.getIpAddress());
				pst.setString(7, assoccode);
				result=pst.executeUpdate();
			}	
		} catch (SQLException sqex) {
			cat.error("TDSException PhoneDAO.getAsteriskParameter-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException PhoneDAO.getAsteriskParameter-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static SMSBean getSMSParameter(String assoccode,int type) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		SMSBean smsBean=new SMSBean();
		PreparedStatement pst = null;
		ResultSet rs = null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String query="";
		if(type==1){
			query="SELECT * FROM TDS_SMS_CREDENTIALS WHERE SMS_ASSOCIATIONCODE='"+assoccode+"'";
		} else {
			query="SELECT * FROM TDS_SMS_CREDENTIALS WHERE SMS_PHONE='"+assoccode+"'";
		}
		try {
			pst=con.prepareStatement(query);
			//System.out.println("getsmsparameter wquery:"+pst.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				smsBean.setUserName(rs.getString("SMS_USER_NAME"));
				smsBean.setPassword(rs.getString("SMS_PASSWORD"));
				smsBean.setType(rs.getInt("SMS_TYPE"));
				smsBean.setPhone(rs.getString("SMS_PHONE"));
				smsBean.setCompCode(rs.getString("SMS_ASSOCIATIONCODE"));
				smsBean.setCompNum(rs.getString("SMS_COMP_NUM"));
			}
		} catch (SQLException sqex) {
			cat.error("TDSException PhoneDAO.getSMSParameter-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException PhoneDAO.getSMSParameter-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return smsBean;
	}
	public static int updateSMSParameter(String assoccode,SMSBean smsBean) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("UPDATE TDS_SMS_CREDENTIALS SET SMS_USER_NAME=?,SMS_PASSWORD=?,SMS_TYPE=?,SMS_PHONE=? WHERE SMS_ASSOCIATIONCODE=?");
			pst.setString(1, smsBean.getUserName());
			pst.setString(2, smsBean.getPassword());
			pst.setInt(3, smsBean.getType());
			pst.setString(4, smsBean.getPhone());
			pst.setString(5, assoccode);
			result=pst.executeUpdate();
			if(result<1){
				pst=con.prepareStatement("INSERT INTO TDS_SMS_CREDENTIALS (SMS_USER_NAME,SMS_PASSWORD,SMS_TYPE,SMS_PHONE,SMS_ASSOCIATIONCODE) VALUES (?,?,?,?,?)");
				pst.setString(1, smsBean.getUserName());
				pst.setString(2, smsBean.getPassword());
				pst.setInt(3, smsBean.getType());
				pst.setString(4, smsBean.getPhone());
				pst.setString(5, assoccode);
				result=pst.executeUpdate();
			}	
		} catch (SQLException sqex) {
			cat.error("TDSException PhoneDAO.updateSMSParameter-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException PhoneDAO.updateSMSParameter-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	
	
	public static int insertMessagefromMob(String fromNum,String message,String assocode) throws Exception{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
		
				if(!fromNum.equals("") && !message.equals("") && !assocode.equals("")){
					pst= con.prepareStatement("INSERT INTO TDS_SMS_FROM_MOBILE(MS_FROM_NUMBER,MS_MESSAGE,MS_ASSOCCODE,MS_ENTERED_TIME)VALUES(?,?,?,now())");
					pst.setString(1, fromNum);
					pst.setString(2,message);
					pst.setString(3,assocode);
					result=pst.executeUpdate();
				}
			
		}catch ( SQLException sqex) {
		//	System.out.println("TDSException PhoneDAO.insertMessagefromMob--->"+sqex.getMessage());
			cat.error("TDSException PhoneDAO.insertMessagefromMob--->"+sqex.getMessage());

			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

       return result;
	}
	
	public static ArrayList<SMSBean> getAllMessage(String assoCode,String update,String LastKey,String timeOffset){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<SMSBean> arrlist = new ArrayList<SMSBean>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		String query1="SELECT MS_FROM_NUMBER,MS_MESSAGE,MS_ASSOCCODE,DATE(CONVERT_TZ(MS_ENTERED_TIME ,'UTC','"+timeOffset+"')) AS MS_ENTERED_TIME,MS_KEY,MS_READ_STATUS FROM TDS_SMS_FROM_MOBILE WHERE MS_ASSOCCODE='"+assoCode+"'  AND MS_READ_STATUS='0' ORDER BY  MS_ENTERED_TIME DESC LIMIT 10";
		ResultSet rs;
		try {
				m_pst=m_conn.prepareStatement(query1);
				rs=m_pst.executeQuery();
				while(rs.next()){
					SMSBean msgdata = new SMSBean();
					msgdata.setFromno(rs.getString("MS_FROM_NUMBER"));
					msgdata.setMessage(rs.getString("MS_MESSAGE"));
					msgdata.setTime(rs.getString("MS_ENTERED_TIME"));
					msgdata.setKey(rs.getString("MS_KEY"));
					arrlist.add(msgdata);
				}
				//}
			} catch (Exception sqex) {
			cat.error(("TDSException PhoneDAO.getAllMessage-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
		//	System.out.println("TDSException PhoneDAO.getAllMessage-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return arrlist;
		
	}
	
	public static ArrayList<SMSBean> getAllReadMessage(String assoCode,String timeOffset){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<SMSBean> arrlist = new ArrayList<SMSBean>();
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		PreparedStatement m_pst = null;
		String query1="SELECT MS_FROM_NUMBER,MS_MESSAGE,MS_ASSOCCODE,DATE_FORMAT(CONVERT_TZ( MS_ENTERED_TIME,'UTC','"+timeOffset+"'),'%Y-%m-%d %h:%i:%s %p') AS  MS_ENTERED_TIME,MS_KEY,MS_READ_STATUS FROM TDS_SMS_FROM_MOBILE WHERE MS_ASSOCCODE='"+assoCode+"'  AND MS_READ_STATUS='1' ORDER BY  MS_ENTERED_TIME DESC LIMIT 10";
		ResultSet rs;
		try {
				m_pst=m_conn.prepareStatement(query1);
				rs=m_pst.executeQuery();
				while(rs.next()){
					SMSBean msgdata = new SMSBean();
					msgdata.setFromno(rs.getString("MS_FROM_NUMBER"));
					msgdata.setMessage(rs.getString("MS_MESSAGE"));
					msgdata.setTime(rs.getString("MS_ENTERED_TIME"));
					msgdata.setKey(rs.getString("MS_KEY"));
					arrlist.add(msgdata);
				}
				//}
			} catch (Exception sqex) {
			cat.error(("TDSException PhoneDAO.getAllMessage-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
		//	System.out.println("TDSException PhoneDAO.getAllMessage-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return arrlist;
		
	}

	public static int makeMsgRead(String msgId,String assoCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int time=0;
		try {
			pst = con.prepareStatement("UPDATE TDS_SMS_FROM_MOBILE SET MS_READ_STATUS ='1' WHERE MS_KEY='"+msgId+"' AND MS_ASSOCCODE ='"+assoCode+"'");
		  // System.out.println("update"+pst);
			cat.info("TDS INFO PhoneDAO.makeSmsRead Query"+pst.toString());
			pst.executeUpdate();

		}catch (Exception sqex) {
			cat.error(("TDSException PhoneDAO.makeSmsRead-->" + sqex.getMessage()));
			cat.error(pst.toString());
		//	System.out.println("TDSException PhoneDAO.makeMsgRead-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}
		finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return time;
	}

}
