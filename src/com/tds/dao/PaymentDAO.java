package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.util.TDSSQLConstants;

public class PaymentDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(PaymentDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ConfigDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static boolean insertPendingPayment() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO PaymentDAO.insertPaymentSummary");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		boolean m_result = false;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getNightPayment);
			m_rs = m_pst.executeQuery();
			while (m_rs.next()) {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.insertPendingPayment);
				m_pst.setString(1, m_rs.getString("PA_DRIVERID"));
				m_pst.setString(2, m_rs.getString("PA_ASSOCCODE"));
				m_pst.setString(3, m_rs.getString("PA_TRIPID"));
				m_pst.setString(4, m_rs.getString("PA_PAYMENTDATE"));
				m_pst.setString(5, m_rs.getString("PA_AMOUNT"));
				m_pst.setString(6, m_rs.getString("PA_PAYMENTTYPE"));
				m_pst.setString(7, m_rs.getString("PA_SECURITYCODE"));
				m_pst.setString(8, m_rs.getString("PA_SERVICECHARGE"));
				m_pst.setString(9, "0");
				cat.info(m_pst.toString());
				if(m_pst.executeUpdate() > 0) {
					m_result = true;
				}
			}

		} catch (SQLException sqex) {
			cat.error("TDSException PaymentDAO.insertPendingPayment-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException PaymentDAO.insertPendingPayment-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}

	public static boolean insertPaymentSummary() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO PaymentDAO.insertPaymentSummary");
		boolean m_result = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getPendingPayment);
			m_rs = m_pst.executeQuery();
			while (m_rs.next()) {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.insertPendingSummary);
				m_pst.setString(1, m_rs.getString("PP_DRIVERID"));
				m_pst.setString(2, m_rs.getString("PP_ASSOCCODE"));
				m_pst.setString(3, m_rs.getString("PP_PAYMENTDATE"));
				m_pst.setString(4, m_rs.getString("PP_AMOUNT"));
				cat.info(m_pst.toString());
				if(m_pst.executeUpdate() > 0) {
					m_result = true;
				}
			}

		} catch (SQLException sqex) {
			cat.error("TDSException PaymentDAO.insertPaymentSummary-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException PaymentDAO.insertPaymentSummary-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}

	public static boolean insertPendingDetailPayment() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO PaymentDAO.insertPendingDetailPayment");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		boolean m_result = false;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getPendingPaymentForDetail);
			m_rs = m_pst.executeQuery();
			while (m_rs.next()) {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.insertPendingPaymentDetail);
				m_pst.setString(1, m_rs.getString("PP_DRIVERID"));
				m_pst.setString(2, m_rs.getString("PP_ASSOCCODE"));
				m_pst.setString(3, m_rs.getString("PP_TRIPID"));
				m_pst.setString(4, m_rs.getString("PP_PAYMENTDATE"));
				m_pst.setString(5, m_rs.getString("PP_AMOUNT"));
				m_pst.setString(6, m_rs.getString("PP_PAYMENTTYPE"));
				m_pst.setString(7, m_rs.getString("PP_PAYMENTAUTHCODE"));
				m_pst.setString(8, m_rs.getString("PP_SERVICECHARGES"));
				m_pst.setString(9, m_rs.getString("PP_OTHERCHARGES"));
				cat.info(m_pst.toString());
				if(m_pst.executeUpdate() > 0) {
					m_pst = m_conn.prepareStatement(TDSSQLConstants.deletePendingPayment);
					m_pst.executeUpdate();
					m_result = true;
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException PaymentDAO.insertPendingDetailPayment-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException PaymentDAO.insertPendingDetailPayment-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}


	public static boolean insertPaymentSummaryCompleted() {
		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO PaymentDAO.insertPaymentSummaryCompleted");
		boolean m_result = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getPendingPaymentSummary);
			m_rs = m_pst.executeQuery();
			while (m_rs.next()) {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.insertPendingPaymentSummaryCompleted);
				m_pst.setString(1, m_rs.getString("SP_DRIVERID"));
				m_pst.setString(2, m_rs.getString("SP_ASSOCCODE"));
				m_pst.setString(3, m_rs.getString("SP_PAYMENTDATE"));
				m_pst.setString(4, m_rs.getString("SP_AMOUNT"));
				m_pst.setString(5, m_rs.getString("SP_PAYMENTTYPE"));
				m_pst.setString(6, m_rs.getString("SP_ACCOUNTNO"));
				m_pst.setString(7, m_rs.getString("SP_ROUTINGNO"));
				m_pst.setString(8, m_rs.getString("SP_CREATEDDATE"));
				cat.info(m_pst.toString());
				if(m_pst.executeUpdate() > 0) {
					m_pst = m_conn.prepareStatement(TDSSQLConstants.deletePendingPaymentSummary);
					m_pst.executeUpdate();
					m_result = true;
				}
			}
		} catch (SQLException sqex) {
			cat.error("TDSException PaymentDAO.insertPaymentSummaryCompleted-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException PaymentDAO.insertPaymentSummaryCompleted-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_result;
	}


}
