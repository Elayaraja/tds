package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
public class Registration2DAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(Registration2DAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+Registration2DAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static int insertPendingLandMark(String asscode,ArrayList<String> lmKeys,String cabComments){ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RegistrationDAO.insertLandMark");
		PreparedStatement m_pst,m_pst1 = null; 
		int result=0;
		String keys="";
		for(int i=0;i<lmKeys.size()-1;i++){
			keys= keys+"'"+lmKeys.get(i)+"'"+",";
		} 
		keys=keys+"'"+lmKeys.get(lmKeys.size()-1)+"'";
		String Query=" LP_SNO IN ("+keys+")";
		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection(); 
		try {
			m_pst = m_conn.prepareStatement("INSERT INTO TDS_LANDMARK (LM_ASSCODE,LM_NAME,LM_ADD1,LM_ADD2,LM_CITY,LM_STATE,LM_ZIP,LM_LATITUDE,LM_LONGITUDE,LM_ZONE,LM_PHONE,LM_COMMENTS) (SELECT LP_ASSCODE,LP_NAME,LP_ADD1,LP_ADD2,LP_CITY,LP_STATE,LP_ZIP,LP_LATITUDE,LP_LONGITUDE,LP_ZONE,LP_PHONE,'"+cabComments+"' FROM TDS_LANDMARK_PENDING WHERE"+Query+" AND LP_ASSCODE='"+asscode+"')");
			result=m_pst.executeUpdate();
			if(result>=1){
				m_pst1 = m_conn.prepareStatement("DELETE FROM TDS_LANDMARK_PENDING WHERE"+Query);
				m_pst1.execute();
			}
		} catch (Exception sqex)
		{
			cat.error("TDSException RegistrationDAO.insertLandMark-->"+sqex.getMessage());
			//System.out.println("TDSException RegistrationDAO.insertLandMark-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}



	public static ArrayList<String> getLast3Ids(AdminRegistrationBO adminBO){    
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO Registration2DAO.getLast3Ids");
		PreparedStatement m_pst1 = null; 
		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection(); 
		ArrayList<String> last3Ids= new ArrayList<String>();
		ResultSet rs= null;
		try {
			m_pst1 = m_conn.prepareStatement("SELECT * FROM TDS_DRIVER WHERE DR_MASTER_ASSOCCODE='"+adminBO.getAssociateCode()+"' ORDER BY DR_SNO DESC LIMIT 0,3");
			rs=m_pst1.executeQuery();
			while(rs.next()){
				String driverID=rs.getString("DR_SNO");
				last3Ids.add(driverID);
			}
		} catch (Exception sqex)
		{
			cat.error("TDSException Registration2DAO.getLast3Ids-->"+sqex.getMessage());
			//System.out.println("TDSException Registration2DAO.getLast3Ids-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return last3Ids;

	}

}
