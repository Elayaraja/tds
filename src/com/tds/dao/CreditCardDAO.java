package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.charges.constant.ICCConstant;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.process.OneToOneSMS;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.ClientAuthenticationBO;
import com.tds.tdsBO.CreditCardBO;
import com.tds.tdsBO.VantivBO;
import com.common.util.TDSValidation;

public class CreditCardDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(CreditCardDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+CreditCardDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	
	public static int insertCreditCardDetails(String assocode,String customerName, String creditcardNumber,String nameOnCard,String expiryDate,String customerKey,String infoKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int cardInsertion=0;
		String query="";
		query=" INSERT INTO TDS_CREDIT_CARD_ACCOUNTS (CC_USERNAME,CC_CUSTOMERID,CC_NAME_ON_CARD,CC_EXPIRY_DATE,CC_CARDNUMBER,CC_ASSOCCODE,CC_INFO_KEY) VALUES(?,?,?,?,?,?,?)";
		try{
			pst=con.prepareStatement(query);
			pst.setString(1,customerName);
			pst.setString(2,customerKey);
			pst.setString(3,nameOnCard);
			pst.setString(4,expiryDate);
			pst.setString(5,creditcardNumber);
			pst.setString(6,assocode);
			pst.setString(7,infoKey);
			cardInsertion=pst.executeUpdate();
			
		} catch (Exception sqex){
			cat.error("TDSException CreditCardDAO.insertCreditCardDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CreditCardDAO.insertCreditCardDetails-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return cardInsertion;
	}
	
	public static int updateCreditCardDetails(String assocode,String customerName, String creditcardNumber,String nameOnCard,String expiryDate,String customerKey,String infoKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int cardInsertion=0;
		String query="";
		query=" UPDATE TDS_CREDIT_CARD_ACCOUNTS SET CC_USERNAME=?,CC_CUSTOMERID=?,CC_NAME_ON_CARD=?,CC_EXPIRY_DATE=?,CC_CARDNUMBER=? WHERE CC_ASSOCCODE=? AND CC_INFO_KEY=?";
		try{
			pst=con.prepareStatement(query);
			pst.setString(1,customerName);
			pst.setString(2,customerKey);
			pst.setString(3,nameOnCard);
			pst.setString(4,expiryDate);
			pst.setString(5,creditcardNumber);
			pst.setString(6,assocode);
			pst.setString(7,infoKey);
			cardInsertion=pst.executeUpdate();
			
		} catch (Exception sqex){
			cat.error("TDSException CreditCardDAO.updateCreditCardDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CreditCardDAO.updateCreditCardDetails-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return cardInsertion;
	}
	public static int deleteCreditCardDetails(String assocode,String customerKey,String infoKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int cardInsertion=0;
		String query="";
		query=" DELETE FROM TDS_CREDIT_CARD_ACCOUNTS WHERE CC_ASSOCCODE=? AND CC_INFO_KEY=? AND CC_CUSTOMERID=?";
		try{
			pst=con.prepareStatement(query);
			pst.setString(1,assocode);
			pst.setString(2,infoKey);
			pst.setString(3,customerKey);
			cardInsertion=pst.executeUpdate();
			
		} catch (Exception sqex){
			cat.error("TDSException CreditCardDAO.deleteCreditCardDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CreditCardDAO.deleteCreditCardDetails-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return cardInsertion;
	}
	public static int updateClientUser(String assocode, String masterKey,String customerKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int cardInsertion=0;
		String query="";
		query=" UPDATE TDS_USERADDRESSMASTER SET CU_CUSTOMER_KEY=? WHERE CU_MASTERASSOCIATIONCODE=? AND CU_MASTER_KEY=?";
		try{
			pst=con.prepareStatement(query);
			pst.setString(1,customerKey);
			pst.setString(2,assocode);
			pst.setString(3,masterKey);
			cardInsertion=pst.executeUpdate();
			
		} catch (Exception sqex){
			cat.error("TDSException CreditCardDAO.insertCreditCardDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CreditCardDAO.insertCreditCardDetails-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return cardInsertion;
	}

	public static String checkCustomerKey(AdminRegistrationBO adminBO,String masterKey){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CreditCardDAO.checkCustomerKey");
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String customerKey="";
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_USERADDRESSMASTER WHERE CU_MASTER_KEY=? AND CU_MASTERASSOCIATIONCODE=?");
			pst.setString(1, masterKey);
			pst.setString(2, adminBO.getMasterAssociateCode());
		//	System.out.println("query--->"+pst.toString());
			rs=pst.executeQuery();
			if(rs.next()){
				customerKey=rs.getString("CU_CUSTOMER_KEY");
			}
			pst.close();
			con.close();
		}catch (Exception sqex) {
			cat.error("TDSException CreditCardDAO.checkCustomerKey-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CreditCardDAO.checkCustomerKey-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return customerKey;
	}
	public static CreditCardBO getCardDetails(String masterKey,String assoccode)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CreditCardDAO.getCardDetails");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		CreditCardBO cardBO=new CreditCardBO();
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query2="";
			ResultSet rs2=null;
//			query1 = "select CU_CUSTOMER_KEY from TDS_USERADDRESSMASTER where CU_MASTER_KEY='"+masterKey+"'AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'";
//			m_pst = m_conn.prepareStatement(query1);
//			ResultSet rs1= m_pst.executeQuery();
//			if(rs1.next()){
//				customerKey=rs1.getString("CU_CUSTOMER_KEY");
//			}
			if(masterKey!=null && !masterKey.equals("")){
				query2 = "SELECT * FROM TDS_CREDIT_CARD_ACCOUNTS where CC_CUSTOMERID='"+masterKey+"'AND CC_ASSOCCODE='"+assoccode+"'";
				m_pst1 = m_conn.prepareStatement(query2);
				rs2 = m_pst1.executeQuery();
				if (rs2.next()){
					cardBO.setUserName(rs2.getString("CC_USERNAME"));
					cardBO.setCardNumber(rs2.getString("CC_CARDNUMBER"));
					cardBO.setNameOnCard(rs2.getString("CC_NAME_ON_CARD"));
					cardBO.setExpiryDate(rs2.getString("CC_EXPIRY_DATE"));
					cardBO.setCcInfoKey(rs2.getString("CC_INFO_KEY"));
					cardBO.setCustomerKey(masterKey);
				}
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
		//	System.out.println("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 

		finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return cardBO;
	}
	
	public static CreditCardBO getCardDetailsByKey(String masterKey)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO CreditCardDAO.getCardDetails");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		CreditCardBO cardBO=new CreditCardBO();
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query2="";
			ResultSet rs2=null;
			if(masterKey!=null && !masterKey.equals("")){
				query2 = "SELECT * FROM TDS_CREDIT_CARD_ACCOUNTS where CC_INFO_KEY='"+masterKey+"'";
				m_pst1 = m_conn.prepareStatement(query2);
				//System.out.println("creditcard qury:"+m_pst1.toString());
				rs2 = m_pst1.executeQuery();
				if (rs2.next()){
					cardBO.setUserName(rs2.getString("CC_USERNAME"));
					cardBO.setCardNumber(rs2.getString("CC_CARDNUMBER"));
					cardBO.setNameOnCard(rs2.getString("CC_NAME_ON_CARD"));
					cardBO.setExpiryDate(rs2.getString("CC_EXPIRY_DATE"));
					cardBO.setCcInfoKey(rs2.getString("CC_INFO_KEY"));
					cardBO.setCustomerKey(rs2.getString("CC_CUSTOMERID"));
				}
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
		//	System.out.println("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			sqex.printStackTrace();

		} 

		finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return cardBO;
	}
	
	public static boolean customerCcAccount(String masterKey,String associateCode)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean availabele=false;
		cat.info("TDS INFO CreditCardDAO.getCardDetails");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ArrayList<CreditCardBO> cardDetails = new ArrayList<CreditCardBO>();
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query1="SELECT * FROM TDS_CREDIT_CARD_ACCOUNTS WHERE CC_ASSOCCODE=? AND CC_CUSTOMERID=?";
			m_pst = m_conn.prepareStatement(query1);
			m_pst.setString(1,associateCode);
			m_pst.setString(2,masterKey);
			
			ResultSet rs1= m_pst.executeQuery();
			if(rs1.next()){
				availabele=true;
			}
			
		}catch (Exception sqex)
		{
			cat.error("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
		//	System.out.println("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 

		finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return availabele;
	}
	public static void updateCCDetail(int tripId,int txnId)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("UPDATE TDS_CREDIT_CARD_DETAIL SET CCD_TRIP_ID='"+tripId+"' WHERE CCD_TXN_NO='"+txnId+"'");
			m_pst.execute();
			
		}catch (Exception sqex)
		{
			cat.error("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
		//	System.out.println("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	
	public static void updateInfoKey(String asso,String vocNo,String infoKey)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("UPDATE TDS_GOVT_VOUCHER SET VC_PAYMENT_TYPE=1,VC_INFOKEY='"+infoKey+"' where VC_VOUCHERNO='"+vocNo+"'");
			m_pst.execute();
		//	System.out.println("coverment voucher executed successfully");
			
		}catch (Exception sqex)
		{
			cat.error("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			cat.error(m_pst.toString());
		//	System.out.println("TDSException CreditCardDAO.getCardDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
	
	public static int insertPaymentIntoCC(PaymentProcessBean processBean,String path,ApplicationPoolBO poolBO, ClientAuthenticationBO adminBO,int authStatus) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.insertPaymentIntoWork");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		ResultSet rs;
		int txnNum=0;
		try {
			//m_conn.setAutoCommit(false);
			if(authStatus==1){
				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1 ) values" +
				"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			} else {
				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1,CCD_CAPTUREDATE ) values" +
				"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?,NOW())",Statement.RETURN_GENERATED_KEYS);

			}
			m_pst.setString(1, processBean.getB_driverid());
			m_pst.setString(2, processBean.getB_trip_id());
			m_pst.setString(3, processBean.getB_card_issuer());
			m_pst.setString(4, processBean.getB_card_type());
			m_pst.setString(5, processBean.getB_paymentType());
			m_pst.setString(6, processBean.getB_card_no());
			if(processBean.getB_approval_status().equals("1")){
				m_pst.setDouble(7, Double.parseDouble(processBean.getB_amount()));
				m_pst.setDouble(8, Double.parseDouble(processBean.getB_tip_amt()));
				m_pst.setDouble(9, Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt())  );
			} else {
				//Declained made zero
				m_pst.setDouble(7, 0.0);
				m_pst.setDouble(8, 0.0);
				m_pst.setDouble(9, 0.0);
			}
			m_pst.setString(10, processBean.getB_trans_id());
			m_pst.setString(11, processBean.getB_trans_id2());
			m_pst.setString(12, processBean.getB_approval_status());
			m_pst.setString(13, processBean.getB_approval_code());			 
			m_pst.setBytes(14, TDSValidation.getDefaultsign(path));
			m_pst.setString(15, adminBO.getAssoccode());
			m_pst.setString(16, processBean.getB_cardHolderName());
			m_pst.setString(17, adminBO.getUserId());
			m_pst.setInt(18, authStatus);
			m_pst.setString(19, (authStatus == ICCConstant.SETTLED?processBean.getB_trans_id():""));
			cat.info(m_pst.toString());
		//	System.out.println("Query--->"+m_pst);
			m_pst.execute();
			rs=m_pst.getGeneratedKeys();
			if(rs.next()){
				txnNum = rs.getInt(1);
			}

			//m_conn.commit();
			//m_conn.setAutoCommit(true);
		} catch (SQLException sqex) {
			//TODO Check with Vijayan
			// This code makes a second try to enter a payment record into the work table if the first (above attempt) failed due to weird and rare connection errors
			// In this code, the prev conneciton is and new connection is reopened.
			//if(m_conn!=null)
			//{
			try {
				//m_conn.rollback();
				if(m_conn!=null){
					m_conn.close();
				}
				m_dbConn.closeConnection();

				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1 ) values" +
				"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);

				m_pst.setString(1, processBean.getB_driverid());
				m_pst.setString(2, processBean.getB_trip_id());
				m_pst.setString(3, processBean.getB_card_issuer());
				m_pst.setString(4, processBean.getB_card_type());
				m_pst.setString(5, processBean.getB_paymentType());
				m_pst.setString(6, processBean.getB_card_no());
				if(processBean.getB_approval_status().equals("1")){
					m_pst.setDouble(7, Double.parseDouble(processBean.getB_amount()));
					m_pst.setDouble(8, Double.parseDouble(processBean.getB_tip_amt()));
					m_pst.setDouble(9, Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt())  );
				} else {
					//Declined made zero
					m_pst.setDouble(7, 0.0);
					m_pst.setDouble(8, 0.0);
					m_pst.setDouble(9, 0.0);
				}
				m_pst.setString(10, processBean.getB_trans_id());
				m_pst.setString(11, processBean.getB_trans_id2());
				m_pst.setString(12, processBean.getB_approval_status());
				m_pst.setString(13, processBean.getB_approval_code());			 
				m_pst.setBytes(14, TDSValidation.getDefaultsign(path));
				m_pst.setString(15, adminBO.getAssoccode());
				m_pst.setString(16, processBean.getB_cardHolderName());
				m_pst.setString(17, adminBO.getUserId());
				m_pst.setInt(18, authStatus);
				m_pst.setString(19, (authStatus == ICCConstant.SETTLED?processBean.getB_trans_id():""));
				cat.info(m_pst.toString());
		//		System.out.println("Query--->"+m_pst);
				m_pst.execute();
				rs=m_pst.getGeneratedKeys();
				if(rs.next()){
					txnNum = rs.getInt(1);
				}
				m_conn.close();
			} catch (SQLException e) {
				cat.error("TDSException ProcessingDAO.insertPaymentIntoWork-->"+e.getMessage());
			//	System.out.println("TDSException ProcessingDAO.insertPaymentIntoWork.SecontTime-->"+e.getMessage());
				e.printStackTrace();

				try {
					if(m_conn!=null)
					{
						m_conn.close();
					}
				} catch (SQLException e1) {
					// TODO: handle exception
				}

				e.printStackTrace();
			}

			//} 

			ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,sqex.getMessage(),poolBO,1);
			oneDirect.start();

			cat.error("TDSException ProcessingDAO.insertPaymentIntoWork-->"+sqex.getMessage());
			//System.out.println("TDSException ProcessingDAO.insertPaymentIntoWork-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return txnNum;
	}
	
	public static int insertPaymentIntoCCFromDriver(PaymentProcessBean processBean,String path,ApplicationPoolBO poolBO, AdminRegistrationBO adminBO,int authStatus) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.insertPaymentIntoWork");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		ResultSet rs;
		int txnNum=0;
		try {
			//m_conn.setAutoCommit(false);
			if(authStatus==1){
				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1 ) values" +
				"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			} else {
				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1,CCD_CAPTUREDATE ) values" +
				"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?,NOW())",Statement.RETURN_GENERATED_KEYS);

			}
			m_pst.setString(1, processBean.getB_driverid());
			m_pst.setString(2, processBean.getB_trip_id());
			m_pst.setString(3, processBean.getB_card_issuer());
			m_pst.setString(4, processBean.getB_card_type());
			m_pst.setString(5, processBean.getB_paymentType());
			m_pst.setString(6, processBean.getB_card_no());
			if(processBean.getB_approval_status().equals("1")){
				m_pst.setDouble(7, Double.parseDouble(processBean.getB_amount()));
				m_pst.setDouble(8, Double.parseDouble(processBean.getB_tip_amt()));
				m_pst.setDouble(9, Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt())  );
			} else {
				//Declained made zero
				m_pst.setDouble(7, 0.0);
				m_pst.setDouble(8, 0.0);
				m_pst.setDouble(9, 0.0);
			}
			m_pst.setString(10, processBean.getB_trans_id());
			m_pst.setString(11, processBean.getB_trans_id2());
			m_pst.setString(12, processBean.getB_approval_status());
			m_pst.setString(13, processBean.getB_approval_code());			 
			m_pst.setBytes(14, TDSValidation.getDefaultsign(path));
			m_pst.setString(15, adminBO.getAssociateCode());
			m_pst.setString(16, processBean.getB_cardHolderName());
			m_pst.setString(17, adminBO.getUid());
			m_pst.setInt(18, authStatus);
			m_pst.setString(19, (authStatus == ICCConstant.SETTLED?processBean.getB_trans_id():""));
			cat.info(m_pst.toString());
			//System.out.println("Query--->"+m_pst);
			m_pst.execute();
			rs=m_pst.getGeneratedKeys();
			if(rs.next()){
				txnNum = rs.getInt(1);
			}

			//m_conn.commit();
			//m_conn.setAutoCommit(true);
		} catch (SQLException sqex) {
			//TODO Check with Vijayan
			// This code makes a second try to enter a payment record into the work table if the first (above attempt) failed due to weird and rare connection errors
			// In this code, the prev conneciton is and new connection is reopened.
			//if(m_conn!=null)
			//{
			try {
				//m_conn.rollback();
				if(m_conn!=null){
					m_conn.close();
				}
				m_dbConn.closeConnection();

				m_pst = m_conn.prepareStatement("insert into  TDS_CREDIT_CARD_DETAIL (CCD_DRIVER_ID, CCD_TRIP_ID,CCD_CARD_ISSUER," +
						"CCD_CARD_TYPE,CCD_SWIPETYPE,CCD_CARD_NO,CCD_AMOUNT,CCD_TIP_AMOUNT,CCD_TOTAL_AMOUNT,CCD_TRANS_ID_1,CCD_TRANS_ID_2 ," +
						"CCD_PROCESSING_DATE_TIME,CCD_APPROVAL_STATUS,CCD_APPROVAL_CODE,CCD_RIDER_SIGNATURE" +
						", CCD_ASSOC_CODE,CCD_CARD_HOLDER_NAME,CCD_APPROVED_BY,CCD_AUTHCAPFLG,CCD_CAPTURE_ID1 ) values" +
				"(?,?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);

				m_pst.setString(1, processBean.getB_driverid());
				m_pst.setString(2, processBean.getB_trip_id());
				m_pst.setString(3, processBean.getB_card_issuer());
				m_pst.setString(4, processBean.getB_card_type());
				m_pst.setString(5, processBean.getB_paymentType());
				m_pst.setString(6, processBean.getB_card_no());
				if(processBean.getB_approval_status().equals("1")){
					m_pst.setDouble(7, Double.parseDouble(processBean.getB_amount()));
					m_pst.setDouble(8, Double.parseDouble(processBean.getB_tip_amt()));
					m_pst.setDouble(9, Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt())  );
				} else {
					//Declined made zero
					m_pst.setDouble(7, 0.0);
					m_pst.setDouble(8, 0.0);
					m_pst.setDouble(9, 0.0);
				}
				m_pst.setString(10, processBean.getB_trans_id());
				m_pst.setString(11, processBean.getB_trans_id2());
				m_pst.setString(12, processBean.getB_approval_status());
				m_pst.setString(13, processBean.getB_approval_code());			 
				m_pst.setBytes(14, TDSValidation.getDefaultsign(path));
				m_pst.setString(15, adminBO.getAssociateCode());
				m_pst.setString(16, processBean.getB_cardHolderName());
				m_pst.setString(17, adminBO.getUid());
				m_pst.setInt(18, authStatus);
				m_pst.setString(19, (authStatus == ICCConstant.SETTLED?processBean.getB_trans_id():""));
				cat.info(m_pst.toString());
				//System.out.println("Query--->"+m_pst);
				m_pst.execute();
				rs=m_pst.getGeneratedKeys();
				if(rs.next()){
					txnNum = rs.getInt(1);
				}
				m_conn.close();
			} catch (SQLException e) {
				cat.error("TDSException ProcessingDAO.insertPaymentIntoWork-->"+e.getMessage());
				//System.out.println("TDSException ProcessingDAO.insertPaymentIntoWork.SecontTime-->"+e.getMessage());
				e.printStackTrace();

				try {
					if(m_conn!=null)
					{
						m_conn.close();
					}
				} catch (SQLException e1) {
					// TODO: handle exception
				}

				e.printStackTrace();
			}

			//} 

			ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,sqex.getMessage(),poolBO,1);
			oneDirect.start();

			cat.error("TDSException ProcessingDAO.insertPaymentIntoWork-->"+sqex.getMessage());
			//System.out.println("TDSException ProcessingDAO.insertPaymentIntoWork-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return txnNum;
	}
	
	
	public static boolean vantivInsertSetup(AdminRegistrationBO adminBo, VantivBO vantivBean) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		PreparedStatement csp_pst1 = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		csp_conn = dbcon.getConnection();
		try {
			csp_pst1=csp_conn.prepareStatement("DELETE FROM TDS_VANTIV_SETUP WHERE VS_ASSOCCODE=?");
			csp_pst1.setString(1, adminBo.getMasterAssociateCode());
			csp_pst1.execute();
			csp_pst = csp_conn.prepareStatement("INSERT INTO TDS_VANTIV_SETUP (VS_ASSOCCODE,VS_USERID,VS_PASSWORD) VALUES(?,?,?)");
			csp_pst.setString(1, adminBo.getMasterAssociateCode());
			csp_pst.setString(2, vantivBean.getUserId());
			csp_pst.setString(3, vantivBean.getPassword());
			csp_pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.error("TDSException CreditCardDAO.vantivInsertSetup-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
			//System.out.println("TDSException CreditCardDAO.vantivInsertSetup-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return success;
	}
	
	public static  VantivBO getVantivUserIdPassword(AdminRegistrationBO adminBo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		VantivBO vantivBean = new VantivBO();
		
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_VANTIV_SETUP WHERE VS_ASSOCCODE=?");
			pst.setString(1, adminBo.getMasterAssociateCode());
			rs=pst.executeQuery();
			if(rs.next()){
				vantivBean.setUserId(rs.getString("VS_USERID"));
				vantivBean.setPassword(rs.getString("VS_PASSWORD"));
			}
		}catch(SQLException sqex) {
			cat.error("TDSException CreditCardDAO.vantivInsertSetup-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CreditCardDAO.vantivInsertSetup-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return vantivBean;
	}

}
