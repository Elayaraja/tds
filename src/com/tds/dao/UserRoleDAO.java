package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.util.TDSSQLConstants;
import com.common.util.TDSValidation;


public class UserRoleDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(UserRoleDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+UserRoleDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	
	
	public static ArrayList getUserList(AdminRegistrationBO adminbo,String assocode,String masterAssocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList al_userdata=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		AdminRegistrationBO adminBO=null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst=con.prepareStatement(TDSSQLConstants.getUserList);
			pst.setString(1, assocode);
			pst.setString(2, masterAssocode);
			pst.setInt(3, Integer.parseInt(adminbo.getStart()));
			pst.setInt(4,Integer.parseInt(adminbo.getEnd()));
			//System.out.println("Query---->"+pst.toString());
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				adminBO=new AdminRegistrationBO();
				adminBO.setActive(rs.getInt("AU_ACTIVE")+"");
				adminBO.setUname(rs.getString("AU_USERNAME"));
				adminBO.setPcompany(rs.getString("AU_PARENTCOMPANY"));
				adminBO.setUsertype(rs.getString("AU_ACCESS"));
				adminBO.setUsertypeDesc(rs.getString("AU_USERTYPE"));
				adminBO.setUid(rs.getString("AU_SNO"));
				adminBO.setFname(rs.getString("AU_FNAME"));
				adminBO.setLname(rs.getString("AU_LNAME"));
				al_userdata.add(adminBO);
			}
		}
		catch(SQLException sqex) {
			cat.info("Error in inserting TDS_DRIVER table "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_userdata;
	}


	public static ArrayList<AdminRegistrationBO> getuserData(String Assocode, String userId, String userType , String driver, String nonDrivers,String masterAssocode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<AdminRegistrationBO> al_list = new ArrayList<AdminRegistrationBO>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		AdminRegistrationBO adminBO=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query ="SELECT AU_SNO,AU_USERNAME,AU_USERTYPE,AU_ACCESS,AU_PASSWORD,AU_PARENTCOMPANY,AU_PAYMENTTYPE,AU_ACTIVE from TDS_ADMINUSER WHERE AU_ASSOCCODE = '"+Assocode+"'  and AU_MASTER_ASSOCCODE='"+masterAssocode+"' ";
		if(driver.equals("") && nonDrivers.equals("") && userId.equals("") && userType.equals("")){
			query = query+" AND AU_USERTYPE!= 'Driver'"; 
		}
		if(!userId.equals("")) {
			query = query+" AND AU_USERNAME= '"+userId+"'";
		}
		if(!userType.equals("")) {
			query = query+" AND AU_USERTYPE= '"+userType+"'"; 
		}
		if(!driver.equals("") && !nonDrivers.equals("")){

		}else if(!driver.equals("")) {
			if(driver.equals("1"))
				query = query+" AND AU_USERTYPE= 'Driver'"; 

		}else if(!nonDrivers.equals("")) {
			if(nonDrivers.equals("2"))
				query = query+" AND AU_USERTYPE!= 'Driver'"; 
		}

		try {
			pst = con.prepareStatement(query);
			
			cat.info("QUERY FOR GET USER DATA::   "+pst.toString());
			//System.out.println("query--->"+pst.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				adminBO=new AdminRegistrationBO();
				adminBO.setActive(rs.getInt("AU_ACTIVE")+"");
				adminBO.setUname(rs.getString("AU_USERNAME"));
				adminBO.setPcompany(rs.getString("AU_PARENTCOMPANY"));
				adminBO.setUsertype(rs.getString("AU_ACCESS"));
				adminBO.setUsertypeDesc(rs.getString("AU_USERTYPE"));
				adminBO.setUid(rs.getString("AU_SNO"));
				al_list.add(adminBO);
			} 
		}catch (SQLException sqex) {
			cat.error("TDSException UserRoleDAO.getuserData-->"+sqex.getMessage());
			//System.out.println("TDSException UserRoleDAO.getuserData-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}



	public static int transtotalRecordLength(String Assocode,String from_date,String to_date,String amount,String fleet ,String driver  , String approval_code,String card_no, String approval){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int trl = 0;
		int approve=0,work=0,setteled=0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query = "";
			query = "SELECT COUNT(*) FROM TDS_CREDIT_CARD_DETAIL LEFT JOIN TDS_DRIVER ON DR_SNO=CCD_DRIVER_ID";
			if(!fleet.equals("")){
				query+=" WHERE DR_MASTER_ASSOCCODE ='"+Assocode+"' AND DR_ASSOCCODE ='"+ fleet + "'";
			}else{
				query+=" WHERE DR_MASTER_ASSOCCODE ='"+ Assocode + "'";
			}
			if(driver!=""  && driver!=null){
				query+=" AND CCD_DRIVER_ID ='"+driver+"'";
			}
			if(from_date!="" &&  from_date!=null){
				if(  query.length() > 0) {
					query+=" AND DATE_FORMAT(CCD_PROCESSING_DATE_TIME,'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(from_date)+"'";
				}
			}
			if(to_date!="" &&  to_date!=null){
				if( query.length() > 0) {
					query+=" AND DATE_FORMAT(CCD_PROCESSING_DATE_TIME,'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(to_date)+"'";
				}
			}
			if(amount!="" &&  amount!=null){
				if( query.length() > 0) {
					query+=" AND CCD_AMOUNT >='"+amount+"'";
				}
			}
			if(approval_code!="" &&  approval_code!=null  ){
				if(approval_code.length() > 0) {
					query+=" AND CCD_APPROVAL_CODE ='"+approval_code+"'";
				}
			}

			if(card_no!="" && card_no!=null ){
				if( card_no.length() > 0) {
					query+=" AND CCD_CARD_NO ='"+card_no+"'";
				}
			}
			if(approval ==null)
			{
				query+="  AND CCD_APPROVAL_STATUS =1";
			}
			query = query + " GROUP BY CCD_ASSOC_CODE";
			pst = con.prepareStatement(query);
			System.out.println("Get Query--->"+pst);
			rs = pst.executeQuery();

			if(rs.first())
			{
				work = rs.getInt(1);
			}
			trl = setteled+work+approve;
		}
		catch (SQLException sqex) {
			sqex.printStackTrace();
			cat.info("Error in totalRecordLength "+sqex.getMessage());
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return trl;
	}

	public static int AdmintotalRecordLength(String Assocode,String masterAssocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int trl = 0;
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement(TDSSQLConstants.getAdminUserList);
			pst.setString(1, Assocode);
			pst.setString(2, masterAssocode);
			cat.info(pst.toString());

			rs=pst.executeQuery();
			if(rs.next()){
				trl= rs.getInt(1);
				cat.info("the total recored length = "+trl);
			}
		}
		catch (SQLException sqex) {
			cat.info("Error in totalRecordLength "+sqex.getMessage());
		}
		finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return trl;
	}

	public static AdminRegistrationBO getUserRole(AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		//HashMap hmp_userRole = new HashMap();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try{
			dbcon=new TDSConnection();
			con=dbcon.getConnection();
			pst=con.prepareStatement(TDSSQLConstants.getUserPrivilage);
			pst.setString(1, adminBO.getUname());
			cat.info(pst.toString());

			rs= pst.executeQuery();
			while(rs.next()){
				//hmp_userRole.put(""+rs.getInt("ROLE"), "");
				adminBO.setUname(rs.getString("AU_USERNAME"));
				adminBO.setPaymentType(rs.getString("AU_PAYMENTTYPE"));
				adminBO.setPcompany(rs.getString("AU_PARENTCOMPANY"));
				adminBO.setUsertype(rs.getString("AU_ACCESS"));
				adminBO.setUsertypeDesc(rs.getString("AU_USERTYPE"));
				adminBO.setAssociateCode(rs.getString("AU_ASSOCCODE"));
				adminBO.setAcceptopenreq(rs.getInt("AU_ACCEPTOPENREQ")+"");
				adminBO.setOpenreqentry(rs.getInt("AU_OPENREQENTRY")+"");
				adminBO.setDriverreg(rs.getInt("AU_DRIVERREGENTRY")+"");
				adminBO.setCreatevoucher(rs.getInt("AU_CREATEVOUCHER")+"");
				adminBO.setChangeopenreq(rs.getInt("AU_CHANGEOPENREQ")+"");
				adminBO.setUserreg(rs.getInt("AU_USERREG")+"");
				adminBO.setOpenreqxml(rs.getInt("AU_OPENREQXML")+"");
				adminBO.setAcceptreqxml(rs.getInt("AU_ACCEPTREQXML")+"");
				adminBO.setTripsummaryrep(rs.getInt("AU_TRIPSUMMARYREPORT")+"");
				adminBO.setUserlist(rs.getInt("AU_USERLIST")+"");
				adminBO.setDriversummary(rs.getInt("AU_DRIVERSUMMARY")+"");
				adminBO.setCompanymaster(rs.getString("AU_COMPANYMAS"));
				adminBO.setActive(rs.getString("AU_ACTIVE")+"");
				adminBO.setChangeVoucher(rs.getString("AU_CHANGEVOUCHER"));
				adminBO.setVoucherReport(rs.getString("AU_VOUCHERREPORT"));
				adminBO.setBehavior(rs.getString("AU_DRIVERBEHAVIOR"));
				adminBO.setBehaviorSummary(rs.getString("AU_BEHAVIORSUMMARY"));
				adminBO.setMaintenance(rs.getString("AU_MAINTENANCEENTRY"));
				adminBO.setMaintenanceSummary(rs.getString("AU_MAINTENANCESUMMARY"));
				adminBO.setLostandfound(rs.getString("AU_LOSTANDFOUNDENTRY"));
				adminBO.setLostandfoundSummary(rs.getString("AU_LOSTANDFOUNDSUMMARY"));
				adminBO.setQueuecoordinate(rs.getString("AU_QUEUECOORDINATEENTRY"));
				adminBO.setQueuecoordinateSummary(rs.getString("AU_QUEUECOORDINATESUMMARY"));
				adminBO.setDriverAvailability(rs.getString("AU_DRIVERAVAILABILITY"));
				adminBO.setDriverPaymentReport(rs.getString("AU_DRIVERPAYMENTREPORT"));
				adminBO.setCompanyPaymentReport(rs.getString("AU_COMPANYPAYMENTREPORT"));
				//System.out.println("AU_DRIVERSUMMARY == "+adminBO.getDriversummary());
			}

		}
		catch(SQLException sqex) {
			cat.info("Error in inserting getUserRole "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return adminBO;
	}
	public static int updateRole(AdminRegistrationBO  adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		int status=0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement(TDSSQLConstants.insertUserPrivilage);
			pst.setInt(1,Integer.parseInt(adminBO.getActive()));
			pst.setInt(2,Integer.parseInt(adminBO.getOpenreqentry()));
			pst.setInt(3,Integer.parseInt(adminBO.getDriverreg()));
			pst.setInt(4,Integer.parseInt(adminBO.getCreatevoucher()));
			pst.setInt(5,Integer.parseInt(adminBO.getAcceptopenreq()));
			pst.setInt(6,Integer.parseInt(adminBO.getChangeopenreq()));
			pst.setInt(7,Integer.parseInt(adminBO.getUserreg()));
			pst.setInt(8,Integer.parseInt(adminBO.getOpenreqxml()));
			pst.setInt(9,Integer.parseInt(adminBO.getAcceptreqxml()));
			pst.setInt(10,Integer.parseInt(adminBO.getTripsummaryrep()));
			pst.setInt(11,Integer.parseInt(adminBO.getUserlist()));
			pst.setInt(12, Integer.parseInt(adminBO.getCompanymaster()));
			pst.setInt(13, Integer.parseInt(adminBO.getChangeVoucher()));
			pst.setInt(14, Integer.parseInt(adminBO.getVoucherReport()));
			pst.setInt(15, Integer.parseInt(adminBO.getBehavior()));
			pst.setInt(16, Integer.parseInt(adminBO.getBehaviorSummary()));
			pst.setInt(17, Integer.parseInt(adminBO.getMaintenance()));
			pst.setInt(18, Integer.parseInt(adminBO.getMaintenanceSummary()));
			pst.setInt(19, Integer.parseInt(adminBO.getLostandfound()));
			pst.setInt(20, Integer.parseInt(adminBO.getLostandfoundSummary()));
			pst.setInt(21, Integer.parseInt(adminBO.getQueuecoordinate()));
			pst.setInt(22, Integer.parseInt(adminBO.getQueuecoordinateSummary()));
			pst.setInt(23, Integer.parseInt(adminBO.getDriverPaymentReport()));
			pst.setInt(24, Integer.parseInt(adminBO.getCompanymaster()));
			pst.setInt(25, Integer.parseInt(adminBO.getDriverAvailability()));
			pst.setString(26,adminBO.getUname());		
			cat.info("Query "+pst);
			status = pst.executeUpdate();

			//System.out.println("Status in updateRole" +status);
		}
		catch(SQLException sqex) {
			cat.info("Error in inserting USER_ROLE "+sqex) ;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}	
}
