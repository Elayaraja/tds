package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.TDSConstants;
import com.tds.util.TDSSQLConstants;

public class IVRDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(IVRDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+IVRDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static int updateOpenRequestOnDriverAccept(String _associationCode, String _tripID, String _driverID, String _vehicle) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO IVRDAO.updateOpenRequestOnDriverAccept");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		//String date[] = p_OpenRequestBO.getSdate().split("/");
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try {
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_JOB_ACCEPT_TIME = NOW(), OR_DRIVERID = ?, OR_VEHICLE_NO = ?, OR_TRIP_STATUS = ?  WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE = ? AND ((OR_TRIP_STATUS = '"+ TDSConstants.performingDispatchProcesses+ "' AND OR_DRIVERID = '" + _driverID + "') OR (OR_TRIP_STATUS = '"+ TDSConstants.performingDispatchProcesses+ "' AND LOCATE('" + _driverID + ";',OR_DRIVERID)>0) OR OR_TRIP_STATUS ='"+ TDSConstants.broadcastRequest+ "')");

			pst.setString(1, _driverID);
			pst.setString(2, _vehicle);
			pst.setString(3, TDSConstants.jobAllocated + "");
			pst.setString(4, _tripID);
			pst.setString(5, _associationCode);


			cat.info(pst.toString());
			resultValue = pst.executeUpdate();

			pst.close();

		} catch(SQLException sqex) {
			cat.error("TDSException IVRDAO.updateOpenRequest-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException IVRDAO.updateOpenRequest-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}
	public static void insertIVREntry(String associationCode,String fromNumber, String toNumber) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO IVRDAO.insertIVREntry");
		PreparedStatement pst = null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("INSERT INTO TDS_IVR (IV_ASSOCCODE,IV_FROM,IV_TO,IV_TIME) VALUES (?,?,?,NOW())");
			pst.setString(1, associationCode);
			pst.setString(2, fromNumber);
			pst.setString(3, toNumber);
			pst.execute();
		} catch(SQLException sqex) {
			cat.error("TDSException IVRDAO.insertIVREntry-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException IVRDAO.insertIVREntry-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}
	public static void smsEntry(String associationCode,String message, String toNumber,int type) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst = null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("INSERT INTO TDS_SMS (SMS_ASSOCCODE,SMS_MESSAGE,SMS_TO,SMS_TIME,SMS_VENDOR) VALUES (?,?,?,NOW(),?)");
			pst.setString(1, associationCode);
			pst.setString(2, message);
			pst.setString(3, toNumber);
			pst.setInt(4, type);
			pst.execute();
		} catch(SQLException sqex) {
			cat.error("TDSException IVRDAO.smsIVREntry-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException IVRDAO.smsIVREntry-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}

	public static int updateOpenRequestOnDriverReject(String _associationCode, String _tripID, String _driverID) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO IVRDAO.updateOpenRequestOnDriverReject");
		int resultValue = 0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		//String date[] = p_OpenRequestBO.getSdate().split("/");
		dbcon = new TDSConnection();
		con = dbcon.getConnection();

		try {
			pst = con.prepareStatement("UPDATE TDS_OPENREQUEST SET OR_DRIVERID = '', OR_VEHICLE_NO = '' WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE = ? AND ((OR_TRIP_STATUS = '"+ TDSConstants.performingDispatchProcesses+ "' AND OR_DRIVERID = '" + _driverID + "') OR (OR_TRIP_STATUS = '"+ TDSConstants.performingDispatchProcesses+ "' AND LOCATE('" + _driverID + ";',OR_DRIVERID)>0) OR OR_TRIP_STATUS ='"+ TDSConstants.broadcastRequest+ "')");

			pst.setString(1, _tripID);
			pst.setString(2, _associationCode);
			//pst.setString(3, p_OpenRequestBO.getDriverid());

			cat.info(pst.toString());
			resultValue = pst.executeUpdate();

			pst.close();

		} catch(SQLException sqex) {
			cat.error("TDSException IVRDAO.updateOpenRequestOnDriverReject-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException IVRDAO.updateOpenRequestOnDriverReject-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return resultValue;
	}

	public static OpenRequestBO getOpenRequestBean(OpenRequestBO openRequestBO,String phoneno,String Opr_mod, AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO RequestDAO.getOpenRequestBean");
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			if(openRequestBO.getTripid() != "" || openRequestBO.getTripid().length() == 0 ){
				if(Opr_mod.equalsIgnoreCase("1")){
					pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_NAME,OR_DRIVERID,OR_VEHICLE_NO,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EDSTATE,OR_EDZIP,OR_SAIRCODE,OR_EAIRCODE,OR_SERVICETIME,CONVERT_TZ(OR_SERVICEDATE, 'UTC', '"+adminBO.getTimeZone()+"') AS OR_SERVICEDATE_OFFSET ,OR_ASSOCCODE,OR_ENTEREDTIME,OR_QUEUENO,OR_STLATITUDE,OR_EDLATITUDE,OR_STLONGITUDE,OR_EDLONGITUDE,OR_LANDMARK,OR_ELANDMARK,OR_INTERSECTION,OR_PAYTYPE,OR_PAYACC,OR_SPLINS,OR_AMT, OR_TRIP_STATUS, OR_ROUTE_NUMBER,QD_SMS_RES_TIME FROM TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD ON OR_QUEUENO = QD_QUEUENAME AND QD_ASSOCCODE=OR_ASSOCCODE WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE = ? AND OR_TRIP_STATUS in ('P','B',' ')"); //Allocate From Dash Board
				}else if(Opr_mod.equalsIgnoreCase("2")){
					pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_NAME,OR_DRIVERID,OR_VEHICLE_NO,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EDSTATE,OR_EDZIP,OR_SAIRCODE,OR_EAIRCODE,OR_SERVICETIME,CONVERT_TZ(OR_SERVICEDATE, 'UTC', '"+adminBO.getTimeZone()+"') AS OR_SERVICEDATE_OFFSET ,OR_ASSOCCODE,OR_ENTEREDTIME,OR_QUEUENO,OR_STLATITUDE,OR_EDLATITUDE,OR_STLONGITUDE,OR_EDLONGITUDE,OR_LANDMARK,OR_ELANDMARK,OR_INTERSECTION,OR_PAYTYPE,OR_PAYACC,OR_SPLINS,OR_AMT, OR_TRIP_STATUS, OR_ROUTE_NUMBER,QD_SMS_RES_TIME FROM TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD ON  OR_QUEUENO = QD_QUEUENAME AND QD_ASSOCCODE=OR_ASSOCCODE WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE = ? AND OR_TRIP_STATUS in ('P','B','S',' ')"); //Allocate From Short Path Driver
				}else{
					pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_NAME, OR_DRIVERID,OR_VEHICLE_NO,OR_PHONE,OR_STADD1,OR_STADD2,OR_STCITY,OR_STSTATE,OR_STZIP,OR_EDADD1,OR_EDADD2,OR_EDCITY,OR_EDSTATE,OR_EDZIP,OR_SAIRCODE,OR_EAIRCODE,OR_SERVICETIME,CONVERT_TZ(OR_SERVICEDATE, 'UTC', '"+adminBO.getTimeZone()+"') AS OR_SERVICEDATE_OFFSET ,OR_ASSOCCODE,OR_ENTEREDTIME,OR_QUEUENO,OR_STLATITUDE,OR_EDLATITUDE,OR_STLONGITUDE,OR_EDLONGITUDE,OR_LANDMARK,OR_ELANDMARK,OR_INTERSECTION,OR_PAYTYPE,OR_PAYACC,OR_SPLINS,OR_AMT, OR_TRIP_STATUS, OR_ROUTE_NUMBER,QD_SMS_RES_TIME FROM TDS_OPENREQUEST OPR LEFT JOIN TDS_QUEUE_DETAILS QD ON  OR_QUEUENO = QD_QUEUENAME AND QD_ASSOCCODE=OR_ASSOCCODE WHERE OR_TRIP_ID = ? AND OR_ASSOCCODE = ? AND ((OR_TRIP_STATUS IN('"+ TDSConstants.performingDispatchProcesses+ "','"+TDSConstants.srJobHold+"','"+TDSConstants.tripOnHold+"') AND (OR_DRIVERID = '" + adminBO.getUid() + "' OR OR_VEHICLE_NO='"+adminBO.getVehicleNo()+"')) OR (OR_TRIP_STATUS = '"+ TDSConstants.performingDispatchProcesses+ "' AND LOCATE('" + adminBO.getUid() + ";',OR_DRIVERID)>0) OR OR_TRIP_STATUS ='"+ TDSConstants.broadcastRequest+ "')");
				}
				pst.setString(1, adminBO.getTimeZone());
				pst.setString(2, openRequestBO.getTripid());
				pst.setString(3,adminBO.getAssociateCode());
			} else if(phoneno != "" || phoneno.length() == 0){
				pst = con.prepareStatement(TDSSQLConstants.getTripAvailabilityByPhoneNo);
				pst.setString(1, phoneno);
			}
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.next()) {
				openRequestBO.setTripid(rs.getString("OR_TRIP_ID"));
				openRequestBO.setPhone(rs.getString("OR_PHONE"));
				openRequestBO.setSadd1(rs.getString("OR_STADD1") != null?rs.getString("OR_STADD1"):"");
				openRequestBO.setSadd2(rs.getString("OR_STADD2") != null?rs.getString("OR_STADD2"):"");
				openRequestBO.setScity(rs.getString("OR_STCITY") != null?rs.getString("OR_STCITY"):"");
				openRequestBO.setSstate(rs.getString("OR_STSTATE")!= null?rs.getString("OR_STSTATE"):"");
				openRequestBO.setSzip(rs.getString("OR_STZIP")!= null?rs.getString("OR_STZIP"):"");
				openRequestBO.setEadd1(rs.getString("OR_EDADD1")!= null?rs.getString("OR_EDADD1"):"");
				openRequestBO.setEadd2(rs.getString("OR_EDADD2") != null?rs.getString("OR_EDADD2"):"");
				openRequestBO.setEcity(rs.getString("OR_EDCITY") != null?rs.getString("OR_EDCITY"):"");
				openRequestBO.setEstate(rs.getString("OR_EDSTATE") != null?rs.getString("OR_EDSTATE"):"");
				openRequestBO.setEzip(rs.getString("OR_EDZIP") != null?rs.getString("OR_EDZIP"):"");
				//				openRequestBO.setSaircode(rs.getString("OR_SAIRCODE") != null?rs.getString("OR_SAIRCODE"):"");
				//				openRequestBO.setEaircode(rs.getString("OR_EAIRCODE") != null?rs.getString("OR_EAIRCODE"):"");
				openRequestBO.setAssociateCode(rs.getString("OR_ASSOCCODE")!= null ? rs.getString("OR_ASSOCCODE"):"");
				openRequestBO.setSdate(rs.getString("OR_SERVICEDATE_OFFSET"));
				openRequestBO.setRequestTime(rs.getString("OR_ENTEREDTIME"));
				openRequestBO.setQueueno(rs.getString("OR_QUEUENO"));
				openRequestBO.setDriverid(rs.getString("OR_DRIVERID")); 
				openRequestBO.setElandmark(rs.getString("OR_ELANDMARK")); 
				openRequestBO.setSlat(rs.getString("OR_STLATITUDE"));
				openRequestBO.setEdlatitude(rs.getString("OR_EDLATITUDE"));
				openRequestBO.setSlong(rs.getString("OR_STLONGITUDE"));
				openRequestBO.setEdlongitude(rs.getString("OR_EDLONGITUDE"));
				openRequestBO.setSlandmark(rs.getString("OR_LANDMARK"));
				openRequestBO.setSintersection(rs.getString("OR_INTERSECTION"));
				openRequestBO.setStartTimeStamp(rs.getString("OR_SERVICEDATE_OFFSET"));
				openRequestBO.setPaytype((rs.getString("OR_PAYTYPE")));
				openRequestBO.setAcct(rs.getString("OR_PAYACC"));
				openRequestBO.setAmt(new BigDecimal(rs.getString("OR_AMT")));
				String reqTime = rs.getString("OR_SERVICETIME");
				openRequestBO.setShrs(reqTime.substring(0,2));
				openRequestBO.setSmin(reqTime.substring(2,4));
				openRequestBO.setIsBeandata(1);
				openRequestBO.setName(rs.getString("OR_NAME"));
				openRequestBO.setSpecialIns(rs.getString("OR_SPLINS"));
				openRequestBO.setTripStatus(rs.getString("OR_TRIP_STATUS"));
				openRequestBO.setRouteNumber(rs.getString("OR_ROUTE_NUMBER"));
				openRequestBO.setQC_SMS_RES_TIME(rs.getInt("QD_SMS_RES_TIME"));
			} else{
				openRequestBO = null;
			}
			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException RequestDAO.getOpenRequestBean-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException RequestDAO.getOpenRequestBean-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return openRequestBO;
	}
	public static OpenRequestBO getJobDetails(String phoneNum) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO IVRDAO.getJobDetails");
		OpenRequestBO orBo=new OpenRequestBO();
		PreparedStatement pst = null;
		ResultSet rs=null;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT OR_TRIP_ID,OR_STADD1,DR_PHONE FROM TDS_OPENREQUEST LEFT JOIN  TDS_DRIVER ON OR_DRIVERID=DR_USERID WHERE OR_PHONE='"+phoneNum+"'");
			rs=pst.executeQuery();
			while(rs.next()){
				orBo.setTripid(rs.getString("OR_TRIP_ID"));
				orBo.setSadd1(rs.getString("OR_STADD1"));
				orBo.setPhone(rs.getString("DR_PHONE"));
			}
		} catch(SQLException sqex) {
			cat.error("TDSException IVRDAO.getJobDetails-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException IVRDAO.getJobDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return orBo;
	}
}
