package com.tds.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletConfig;

import org.apache.log4j.Category;

import com.common.action.PaymentProcessingAction;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.process.OnetoOneDirect;
import com.tds.scheduler.ResettingSessionCron;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.TDSValidation;

public class BatchDAO {	

	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(BatchDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+BatchDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}


	public static int getNoOfUnCapTxn(String date)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		int result=0;
		if(UtilityDAO.chckCronJobExec(date, "C5","Y"))
			result= 1;

		String trace = "A-" +System.currentTimeMillis();
		int countCap = 0;
		Calendar c = Calendar.getInstance();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst_cnt,m_pst_log;
		ResultSet m_rs = null,m_rs_cnt = null;
		ServletConfig config = ResettingSessionCron.config;
		ApplicationPoolBO poolBO = null;
		String pr_st_hr= c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);
		if(config != null)
			poolBO = (ApplicationPoolBO)config.getServletContext().getAttribute("poolBO");
		try {

			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			ArrayList al = new ArrayList();

			if(poolBO!=null && !UtilityDAO.chckCronJobExec(date, "C5","Y"))
			{
				m_pst = m_conn.prepareStatement("select CD_ASSOCCODE,CD_EMAIL from  TDS_COMPANYDETAIL");
				m_rs = m_pst.executeQuery();
				while(m_rs.next())
				{
					m_pst_cnt = m_conn.prepareStatement("select count(*) as txn from TDS_WORK,TDS_ADMINUSER  where AU_SNO = TW_DRIVERID and  TW_AUTHCAPFLG != 'C' and AU_ASSOCCODE = '"+m_rs.getString("CD_ASSOCCODE")+"' and date_format(TW_PROCESSING_DATE_TIME,'%Y%m%d')  =  '"+date+"'");
					m_rs_cnt = m_pst_cnt.executeQuery();

					if(m_rs_cnt.first())
					{
						al.add(0, m_rs.getString("CD_EMAIL"));
						countCap = m_rs.getInt("txn");

						OnetoOneDirect oneDirect = new OnetoOneDirect(al.get(0).toString(), "TDSTDS;U;"+System.currentTimeMillis()+";Un Captured Txn For the Day "+TDSValidation.getUserDateFormat(date)+" is :"+ countCap,poolBO);
						oneDirect.start();
						al.clear();

					}

					if(m_rs.isLast())
					{
						m_pst_log = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING_LOG (SL_DAY,SL_CRON_JOB,SL_START_TIME,SL_END_TIME)values(now(),'C5',?,time_format(now(),'%H:%i:%s'))");
						m_pst_log.setString(1, pr_st_hr);
						m_pst_log.execute();

					}
				}
			}
			result= 1;


		}catch (SQLException sqex) {
			cat.error("TDSException BatchDAO.getNoOfUnCapTxn-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException BatchDAO.getNoOfUnCapTxn-->"+sqex.getMessage());
			sqex.printStackTrace();
			result= 1;
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));	
		return result;
	}

	public static int moveToApproval(String date)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String trace = "B-" +System.currentTimeMillis();
		Calendar c = Calendar.getInstance();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst_chk_del, m_pst_app,m_pst_log ;
		ResultSet m_rs = null,m_rs_chk = null;
		int status=0;
		String pr_st_hr= c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);
		double CCC_TXN_AMT_PERCENTAGE= 0,CCC_TXN_AMOUNT=0;
		try {

			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			cat.info(trace);
			m_conn.setAutoCommit(false);

			//!!!! Join Changed into loop !!!!

			// if(!UtilityDAO.chckCronJobExec(date, "C","Y"))
			{
				/* m_pst = m_conn.prepareStatement("select " +
						" w.*,ad.AU_ASSOCCODE,cc.CCC_TXN_AMOUNT,cc.CCC_TXN_AMT_PERCENTAGE,c.CD_REC_SET" +
						" from TDS_WORK w, TDS_COMPANY_CC_SETUP cc,TDS_ADMINUSER ad,TDS_COMPANYDETAIL c" +
						" WHERE c.CD_ASSOCCODE=cc.CCC_COMPANY_ID and  w.TW_DRIVERID=ad.AU_SNO " +
						" and cc.CCC_COMPANY_ID = ad.AU_ASSOCCODE and cc.CCC_CARD_ISSUER = w.TW_CARDISSUER" +
						" and cc.CCC_CARD_TYPE = w.TW_CARDTYPE and w.TW_SWIPETYPE = cc.CCC_SWIPE_TYPE and TW_AUTHCAPFLG='C' " +
						" order by ad.AU_ASSOCCODE,w.TW_DRIVERID");*/

				m_pst = m_conn.prepareStatement("select W.*, CC.* " +
						" from TDS_WORK W LEFT JOIN TDS_COMPANY_CC_SETUP CC " +
						"ON TW_CARDISSUER=CCC_CARD_ISSUER AND TW_ASSOCCODE=CCC_COMPANY_ID " +
						" WHERE TW_AUTHCAPFLG=case TW_APPROVALSTATUS when 1 then 'C' else '' end order by TW_ASSOCCODE,TW_DRIVERID");

				m_rs = m_pst.executeQuery();


				while(m_rs.next())
				{
					m_pst_chk_del = m_conn.prepareStatement("select '"+m_rs.getString("TW_TRANSID")+"' in (select ap.AP_TRANS_ID  from TDS_APPROVAL ap ) as flg");
					m_rs_chk = m_pst_chk_del.executeQuery();

					if(m_rs_chk.first() && m_rs_chk.getInt("flg") == 0)
					{
						//m_pst_chk_del = m_conn.prepareStatement("select * from TDS_COMPANY_CC_SETUP where CCC_COMPANY_ID = '"+m_rs.getString("AU_ASSOCCODE")+"' and CCC_CARD_ISSUER='"+m_rs.getString("TW_CARDISSUER")+"'");
						//m_rs_chk = m_pst_chk_del.executeQuery();



						if(m_rs_chk.first())
						{
							CCC_TXN_AMT_PERCENTAGE = m_rs.getString("CCC_TXN_AMT_PERCENTAGE")==null?0:m_rs.getDouble("CCC_TXN_AMT_PERCENTAGE"); 
							CCC_TXN_AMOUNT = m_rs.getString("CCC_TXN_AMOUNT")==null?0:m_rs.getDouble("CCC_TXN_AMOUNT");
						}

						// Move the correct entry to Approval Table

						m_pst_app = m_conn.prepareStatement("insert into TDS_APPROVAL(AP_DRIVER_ID,AP_COMPANY_ID, AP_TRIP_ID,AP_CARD_ISSUER,AP_SWIPETYPE,AP_CARD_TYPE,AP_CARD_NO,AP_AMOUNT,AP_TIP_AMOUNT,AP_TOTAL_AMOUNT,AP_TXN_PERCENT_AMT,AP_TXN_AMT , AP_TOTAL_AMOUNT_WITH_TXN,AP_TRANS_ID,AP_PROCESSING_DATE_TIME,AP_APPROVAL_STATUS,AP_RIDER_SIGNATURE,AP_APPROVALCODE,AP_REC_SET,AP_CAPTUREID,AP_CAPTUREDATE,AP_AUTHCAPFLG,AP_VOID_FLG,AP_VOID_DATE,AP_VOIDED_FLG,AP_STATUSCODE)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
						m_pst_app.setString(1, m_rs.getString("TW_DRIVERID"));
						m_pst_app.setString(2, m_rs.getString("TW_ASSOCCODE"));
						m_pst_app.setString(3, m_rs.getString("TW_TRIPID"));
						m_pst_app.setString(4, m_rs.getString("TW_CARDISSUER"));
						m_pst_app.setString(5, m_rs.getString("TW_SWIPETYPE"));
						m_pst_app.setString(6, m_rs.getString("TW_CARDTYPE"));
						m_pst_app.setString(7, m_rs.getString("TW_CARDNO"));
						m_pst_app.setDouble(8, m_rs.getDouble("TW_AMOUNT"));
						m_pst_app.setDouble(9, m_rs.getDouble("TW_TIPAMOUNT"));
						m_pst_app.setDouble(10, m_rs.getDouble("TW_TOTALAMOUNT"));
						if(m_rs.getInt("TW_APPROVALSTATUS") == 1)
						{
							//m_pst_app.setDouble(11, ((m_rs.getDouble("TW_TOTALAMOUNT")-m_rs.getDouble("CCC_TXN_AMOUNT"))*(m_rs.getDouble("CCC_TXN_AMT_PERCENTAGE")/100)));
							//m_pst_app.setDouble(12, m_rs.getDouble("CCC_TXN_AMOUNT"));
							//m_pst_app.setDouble(13, (m_rs.getDouble("TW_TOTALAMOUNT")-(m_rs.getDouble("CCC_TXN_AMOUNT")) - ((m_rs.getDouble("TW_TOTALAMOUNT")-m_rs.getDouble("CCC_TXN_AMOUNT"))*(m_rs.getDouble("CCC_TXN_AMT_PERCENTAGE")/100))));
							m_pst_app.setDouble(11, ((m_rs.getDouble("TW_TOTALAMOUNT")-CCC_TXN_AMOUNT) * (CCC_TXN_AMT_PERCENTAGE/100)));
							m_pst_app.setDouble(12, CCC_TXN_AMOUNT);
							m_pst_app.setDouble(13, (m_rs.getDouble("TW_TOTALAMOUNT")-(CCC_TXN_AMOUNT) - ((m_rs.getDouble("TW_TOTALAMOUNT")-CCC_TXN_AMOUNT)*(CCC_TXN_AMT_PERCENTAGE/100))));
						} else if(m_rs.getInt("TW_APPROVALSTATUS") == 2) {
							m_pst_app.setDouble(11, 0);
							m_pst_app.setDouble(12, CCC_TXN_AMOUNT);
							m_pst_app.setDouble(13, CCC_TXN_AMOUNT);

						} 

						else {
							m_pst_app.setDouble(11, 0);
							/*m_pst_app.setDouble(12, m_rs.getDouble("CCC_TXN_AMOUNT"));
							m_pst_app.setDouble(13, -(m_rs.getDouble("CCC_TXN_AMOUNT")));*/
							m_pst_app.setDouble(12, CCC_TXN_AMOUNT);
							m_pst_app.setDouble(13, -CCC_TXN_AMT_PERCENTAGE);
						}
						m_pst_app.setString(14, m_rs.getString("TW_TRANSID"));
						m_pst_app.setString(15, m_rs.getString("TW_PROCESSING_DATE_TIME"));
						m_pst_app.setString(16, m_rs.getString("TW_APPROVALSTATUS"));
						m_pst_app.setBlob(17, m_rs.getBlob("TW_RIDERSIGN"));
						m_pst_app.setString(18, m_rs.getString("TW_APPROVALCODE"));
						m_pst_app.setString(19, "R");
						m_pst_app.setString(20, m_rs.getString("TW_CAPTUREID"));
						m_pst_app.setString(21, m_rs.getString("TW_CAPTUREDATE"));
						m_pst_app.setString(22, m_rs.getString("TW_AUTHCAPFLG"));
						m_pst_app.setString(23, m_rs.getString("TW_VOID_FLG"));
						m_pst_app.setString(24, m_rs.getString("TW_VOID_DATE"));
						m_pst_app.setString(25, m_rs.getString("TW_VOIDED_FLG"));
						m_pst_app.setString(26, m_rs.getString("TW_STATUSCODE"));
						m_pst_app.execute();

						m_pst_app.close();

					}	else {

						// Move The Duplicate Entry to error table 

						m_pst_app = m_conn.prepareStatement("insert into TDS_WORK_ERROR(WE_DRIVER_ID,WE_TRIP_ID,WE_CARD_ISSUER,WE_SWIPETYPE,WE_CARD_TYPE,WE_CARD_NO,WE_AMOUNT,WE_TIP_AMOUNT,WE_TOTAL_AMOUNT,WE_TRANS_ID,WE_PROCESSING_DATE_TIME,WE_APPROVAL_STATUS,WE_APPROVALCODE,WE_RIDER_SIGNATURE,WE_CAPTUREID,WE_CAPTUREDATE,WE_AUTHCAPFLG)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
						m_pst_app.setString(1, m_rs.getString("TW_DRIVERID"));
						m_pst_app.setString(2, m_rs.getString("TW_TRIPID"));
						m_pst_app.setString(3, m_rs.getString("TW_CARDISSUER"));
						m_pst_app.setString(4, m_rs.getString("TW_SWIPETYPE"));
						m_pst_app.setString(5, m_rs.getString("TW_CARDTYPE"));
						m_pst_app.setString(6, m_rs.getString("TW_CARDNO"));
						m_pst_app.setString(7, m_rs.getString("TW_AMOUNT"));
						m_pst_app.setString(8, m_rs.getString("TW_TIPAMOUNT"));
						m_pst_app.setString(9, m_rs.getString("TW_TOTALAMOUNT"));
						m_pst_app.setString(10, m_rs.getString("TW_TRANSID"));
						m_pst_app.setString(11, m_rs.getString("TW_PROCESSING_DATE_TIME"));
						m_pst_app.setString(12, m_rs.getString("TW_APPROVALSTATUS"));
						m_pst_app.setString(13, m_rs.getString("TW_APPROVALCODE"));
						m_pst_app.setBlob(14, m_rs.getBlob("TW_RIDERSIGN"));
						m_pst_app.setString(15, m_rs.getString("TW_CAPTUREID"));
						m_pst_app.setString(16, m_rs.getString("TW_CAPTUREDATE"));
						m_pst_app.setString(17, m_rs.getString("TW_AUTHCAPFLG"));

						m_pst_app.execute();

						m_pst_app.close();

					}

					m_pst_chk_del.close();

					m_pst_chk_del = m_conn.prepareStatement("delete from TDS_WORK where TW_TRANSID = '"+m_rs.getString("TW_TRANSID")+"'");
					m_pst_chk_del.execute();
					m_pst_chk_del.close();			

					if(m_rs.isLast())
					{
						m_pst_log = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING_LOG (SL_DAY,SL_CRON_JOB,SL_START_TIME,SL_END_TIME)values(now(),'C',?,time_format(now(),'%H:%i:%s'))");
						m_pst_log.setString(1, pr_st_hr);
						//m_pst_log.execute();
					}
				}

				m_conn.commit();
				m_pst.close();
			}
			m_conn.setAutoCommit(true);



		} catch (SQLException sqex) {

			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			cat.error("TDSException BatchDAO.moveToApproval-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException BatchDAO.moveToApproval-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static int moveToPaymentSetteled(String date)
	{
		//!!!! Genarate a error report of Transaction without having signature !!!!
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();

		if(UtilityDAO.chckCronJobExec(date, "C1","Y"))
			return 1;


		String trace = "C-" +System.currentTimeMillis();
		Calendar c = Calendar.getInstance();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst_summ = null,m_pst_pay = null,m_pst_pay_summ = null;
		PreparedStatement m_pst_del=null,m_pst_cmp=null, m_pst_cmp_up = null,m_pst_log=null;
		ResultSet m_rs_summ = null,rs_cmp=null;
		String prev_cc,prev_dri,cc_cc,c_dri,pre_rec_set;
		int status=0;
		String pr_st_hr= c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);

		try {


			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			int key =0 , counter=0,no_of_rec=0,no_of_accp=0,no_of_dec=0;
			double AMOUNT =0,TIP_AMOUNT=0,TOTAL_AMOUNT=0,TXN_PERCENT_AMT=0,TXN_AMT=0,TOTAL_AMOUNT_WITH_TXN=0;
			m_conn.setAutoCommit(false);
			if(!UtilityDAO.chckCronJobExec(date, "C1","Y"))
			{ 
				m_pst_cmp = m_conn.prepareStatement("select " +
						" * from " +
						" TDS_SCHEDULED_PROCESSING p, TDS_COMPANYDETAIL cd " +
						" where  p.SCH_ASSOCCODE = cd.CD_ASSOCCODE and p.SCH_STATUS=1" +
						" and p.SCH_DAY= UCASE(date_format('"+date+"','%a'))");

				/*m_pst_cmp =  m_conn.prepareStatement("select * from TDS_SCHEDULED_PROCESSING p, TDS_COMPANYDETAIL cd" +
						" where  p.SCH_ASSOCCODE = cd.CD_ASSOCCODE and p.SCH_STATUS=1 and" +
						" p.SCH_DAY= UCASE(date_format('"+date+"','%a')) and" +
						" case p.SCH_FREQ when 1 then 1" +
						" when 2 then" +
						"   case SCH_LDAY when 1 then" +
						"   case date_format(GET_FIRST_DAY('"+date+"'),'%Y%m%d') when '"+date+"' then 1 else 0 end" +
						"   when 2 then" +
						"   case date_format(GET_LAST_DAY('"+date+"'),'%Y%m%d') when '"+date+"' then 1 else 0 end" +
						"   end" +
						"  end");*/

				rs_cmp = m_pst_cmp.executeQuery();

				while(rs_cmp.next()) 
				{

					prev_cc="";prev_dri="";cc_cc="";c_dri="";pre_rec_set="";
					counter = 0;

					status=1;

					m_pst_summ = m_conn.prepareStatement("select * from TDS_APPROVAL where AP_COMPANY_ID = '"+rs_cmp.getString("CD_ASSOCCODE")+"' and date_format(AP_PROCESSING_DATE_TIME,'%Y%m%d%H%i%s') <= concat('"+date+"',time_format('"+rs_cmp.getString("SCH_TIME")+"','%H%i%s')) order by AP_DRIVER_ID");
					m_rs_summ = m_pst_summ.executeQuery();

					m_pst_pay = m_conn.prepareStatement("insert into TDS_PAYMENT_SETTLED_DETAIL (PSD_SET_ID,PSD_DRIVER_ID,PSD_COMPANY_ID,PSD_TRIP_ID,PSD_CARD_ISSUER,PSD_CARD_TYPE,PSD_SWIPETYPE," +
							"PSD_CARD_NO,PSD_AMOUNT,PSD_TIP_AMOUNT,PSD_TOTAL_AMOUNT,PSD_TRANS_ID,PSD_PROCESSING_DATE_TIME," +
							"PSD_APPROVAL_STATUS,PSD_APPROVALCODE  ,PSD_TXN_PERCENT_AMT,PSD_TXN_AMT,PSD_TOTAL_AMOUNT_WITH_TXN,PSD_RIDER_SIGNATURE,PSD_REC_SET,PSD_CAPTUREID,PSD_CAPTUREDATE,PSD_AUTHCAPFLG, " +
							"PSD_VOID_FLG,PSD_VOID_DATE,PSD_VOIDED_FLG,PSD_STATUSCODE)" +
							"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");


					m_pst_pay_summ = m_conn.prepareStatement("insert into TDS_PAYMENT_SETTLED_SUMMARY(PSS_SET_ID,PSS_DRIVER_ID,PSS_COMPANY_ID,PSS_PROCESSING_DATE_TIME,PSS_AMOUNT,PSS_TIP_AMOUNT,PSS_TOTAL_AMOUNT," +
							"PSS_TXN_PERCENT_AMT,PSS_TXN_AMT,PSS_TOTAL_AMOUNT_WITH_TXN,PSS_NO_OF_REC,PSS_REPO_GEN_FLG,PSS_NO_OF_A,PSS_NO_OF_DEC,PSS_REC_SET) values(?,?,?,now(),?,?,?,?,?,?,?,?,?,?,?)");

					while(m_rs_summ.next())
					{
						cc_cc=m_rs_summ.getString("AP_COMPANY_ID");
						c_dri=m_rs_summ.getString("AP_DRIVER_ID");

						if(!prev_dri.equalsIgnoreCase(c_dri))
						{ 

							if(counter !=0)
							{
								m_pst_pay_summ.setInt(1, key);
								m_pst_pay_summ.setString(2,prev_dri);
								m_pst_pay_summ.setString(3,prev_cc);
								m_pst_pay_summ.setString(4,""+AMOUNT);
								m_pst_pay_summ.setString(5,""+TIP_AMOUNT);
								m_pst_pay_summ.setString(6,""+TOTAL_AMOUNT);
								m_pst_pay_summ.setString(7,""+TXN_PERCENT_AMT);
								m_pst_pay_summ.setString(8,""+TXN_AMT);
								m_pst_pay_summ.setString(9,""+TOTAL_AMOUNT_WITH_TXN);
								m_pst_pay_summ.setString(10,""+no_of_rec);
								m_pst_pay_summ.setString(11,"1");
								m_pst_pay_summ.setString(12,""+no_of_accp);
								m_pst_pay_summ.setString(13,""+no_of_dec);
								m_pst_pay_summ.setString(14,pre_rec_set);
								m_pst_pay_summ.execute();
								m_pst_cmp_up = m_conn.prepareStatement("update TDS_SCHEDULED_PROCESSING set SCH_UP_TO_DATE = now()  where SCH_ASSOCCODE = ?");
								m_pst_cmp_up.setString(1, rs_cmp.getString("CD_ASSOCCODE"));
								m_pst_cmp.execute();

								m_conn.commit();

								AMOUNT=0;TIP_AMOUNT =0;TOTAL_AMOUNT=0;TXN_AMT=0;TXN_PERCENT_AMT=0;TOTAL_AMOUNT_WITH_TXN=0;
								no_of_rec = 0;no_of_accp =0;no_of_dec=0;
							}

							key  =  ConfigDAO.tdsKeyGen("TDS_PAYMENT_SETTELED_SEQ", m_conn);

						}


						m_pst_pay.setInt(1, key);
						m_pst_pay.setString(2, m_rs_summ.getString("AP_DRIVER_ID"));
						m_pst_pay.setString(3, m_rs_summ.getString("AP_COMPANY_ID"));
						m_pst_pay.setString(4, m_rs_summ.getString("AP_TRIP_ID"));
						m_pst_pay.setString(5, m_rs_summ.getString("AP_CARD_ISSUER"));
						m_pst_pay.setString(6, m_rs_summ.getString("AP_CARD_TYPE"));
						m_pst_pay.setString(7, m_rs_summ.getString("AP_SWIPETYPE"));
						m_pst_pay.setString(8, m_rs_summ.getString("AP_CARD_NO"));
						m_pst_pay.setString(9, m_rs_summ.getString("AP_AMOUNT"));
						m_pst_pay.setString(10, m_rs_summ.getString("AP_TIP_AMOUNT"));
						m_pst_pay.setString(11, m_rs_summ.getString("AP_TOTAL_AMOUNT"));
						m_pst_pay.setString(12, m_rs_summ.getString("AP_TRANS_ID"));
						m_pst_pay.setString(13, m_rs_summ.getString("AP_PROCESSING_DATE_TIME"));
						m_pst_pay.setString(14, m_rs_summ.getString("AP_APPROVAL_STATUS"));
						m_pst_pay.setString(15, m_rs_summ.getString("AP_APPROVALCODE"));
						m_pst_pay.setString(16, m_rs_summ.getString("AP_TXN_PERCENT_AMT"));
						m_pst_pay.setString(17, m_rs_summ.getString("AP_TXN_AMT"));
						m_pst_pay.setString(18, m_rs_summ.getString("AP_TOTAL_AMOUNT_WITH_TXN"));
						m_pst_pay.setBlob(19, m_rs_summ.getBlob("AP_RIDER_SIGNATURE"));
						m_pst_pay.setString(20, m_rs_summ.getString("AP_REC_SET"));
						m_pst_pay.setString(21, m_rs_summ.getString("AP_CAPTUREID"));
						m_pst_pay.setString(22, m_rs_summ.getString("AP_CAPTUREDATE"));
						m_pst_pay.setString(23, m_rs_summ.getString("AP_AUTHCAPFLG"));
						m_pst_pay.setString(24, m_rs_summ.getString("AP_VOID_FLG"));
						m_pst_pay.setString(25, m_rs_summ.getString("AP_VOID_DATE"));
						m_pst_pay.setString(26, m_rs_summ.getString("AP_VOIDED_FLG"));
						m_pst_pay.setString(27, m_rs_summ.getString("AP_STATUSCODE"));

						m_pst_pay.execute();

						if(m_rs_summ.getInt("AP_APPROVAL_STATUS") == 1)
						{
							AMOUNT = AMOUNT  + m_rs_summ.getDouble("AP_AMOUNT");
							TIP_AMOUNT = TIP_AMOUNT + m_rs_summ.getDouble("AP_TIP_AMOUNT");
							TOTAL_AMOUNT = TOTAL_AMOUNT + m_rs_summ.getDouble("AP_TOTAL_AMOUNT");
							no_of_accp = no_of_accp + 1;
						} else {
							no_of_dec = no_of_dec + 1; 
						}

						TXN_AMT = TXN_AMT + m_rs_summ.getDouble("AP_TXN_AMT");
						TXN_PERCENT_AMT = TXN_PERCENT_AMT +  m_rs_summ.getDouble("AP_TXN_PERCENT_AMT");
						TOTAL_AMOUNT_WITH_TXN = TOTAL_AMOUNT_WITH_TXN + m_rs_summ.getDouble("AP_TOTAL_AMOUNT_WITH_TXN");

						no_of_rec = no_of_rec+1;

						counter =1;

						prev_cc=m_rs_summ.getString("AP_COMPANY_ID");
						prev_dri=m_rs_summ.getString("AP_DRIVER_ID");
						pre_rec_set = m_rs_summ.getString("AP_REC_SET");

						m_pst_del = m_conn.prepareStatement("delete from TDS_APPROVAL where AP_TRANS_ID = '" + m_rs_summ.getString("AP_TRANS_ID") + "'");
						m_pst_del.execute();

						if(m_rs_summ.isLast())
						{
							cat.info("prev_dri:" + prev_dri);
							m_pst_pay_summ.setInt(1, key);
							m_pst_pay_summ.setString(2,prev_dri);
							m_pst_pay_summ.setString(3,prev_cc);
							m_pst_pay_summ.setString(4,""+AMOUNT);
							m_pst_pay_summ.setString(5,""+TIP_AMOUNT);
							m_pst_pay_summ.setString(6,""+TOTAL_AMOUNT);
							m_pst_pay_summ.setString(7,""+TXN_PERCENT_AMT);
							m_pst_pay_summ.setString(8,""+TXN_AMT);
							m_pst_pay_summ.setString(9,""+TOTAL_AMOUNT_WITH_TXN);
							m_pst_pay_summ.setString(10,""+no_of_rec);
							m_pst_pay_summ.setString(11,"1");
							m_pst_pay_summ.setString(12,""+no_of_accp);
							m_pst_pay_summ.setString(13,""+no_of_dec);
							m_pst_pay_summ.setString(14,pre_rec_set);
							m_pst_pay_summ.execute();	

							m_pst_cmp_up = m_conn.prepareStatement("update TDS_SCHEDULED_PROCESSING set SCH_UP_TO_DATE = now() where SCH_ASSOCCODE = ?");
							m_pst_cmp_up.setString(1, rs_cmp.getString("CD_ASSOCCODE"));
							m_pst_cmp.execute();

							m_conn.commit();
						}

					}

					if(rs_cmp.isLast())
					{
						m_pst_log = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING_LOG (SL_DAY,SL_CRON_JOB,SL_START_TIME,SL_END_TIME)values(now(),'C1',?,time_format(now(),'%H:%i:%s'))");
						m_pst_log.setString(1, pr_st_hr);
						m_pst_log.execute();

						m_conn.commit();
					}
				}
				m_pst_cmp.close();
			}
			m_conn.setAutoCommit(true);

		} catch (SQLException sqex) {
			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			cat.error("TDSException BatchDAO.moveToPaymentSetteled-->"+sqex.getMessage());
			cat.error(m_pst_pay.toString());
			//System.out.println("TDSException BatchDAO.moveToPaymentSetteled-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
			cat.info(trace);
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static String exportData() {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		String trace = "D-" +System.currentTimeMillis();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String query;
		String filename="";
		File f;
		int flg = 0;
		try {

			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			stmt = m_conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
			query = "SELECT * FROM TDS_PAYMENT_SETTLED_SUMMARY t where t.PSS_REPO_GEN_FLG = 1 and PSS_REC_SET = 'S'";
			rs = stmt.executeQuery(query);
			if(rs.first())
			{

				do
				{
					filename="Bank"+"_"+System.currentTimeMillis()+".csv";
					f = new File("/var/lib/mysql/tds/"+filename);
					if(f.exists()){
						flg=1;
					}else{ 
						flg=0;
					} 
				}while(flg==1);
				query = "SELECT * into OUTFILE  '"+f.getAbsolutePath()+"' FIELDS TERMINATED BY ',' FROM TDS_PAYMENT_SETTLED_SUMMARY t where t.PSS_REPO_GEN_FLG = 1 and PSS_REC_SET = 'S' ";
				stmt.executeQuery(query);
				query = "update TDS_PAYMENT_SETTLED_SUMMARY set PSS_REPO_GEN_FLG = 2 where PSS_REPO_GEN_FLG =1";
				stmt.execute(query);
			}
		} catch(SQLException sqex) {
			cat.error("TDSException BatchDAO.exportData-->"+sqex.getMessage());
			cat.error(stmt.toString());
			//System.out.println("TDSException BatchDAO.exportData-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {

			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return filename;
	}


	public static int generateDriverChargesDetails(String date)
	{

		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		if(UtilityDAO.chckCronJobExec(date, "C2","Y"))
			return 1;

		String trace = "E-" +System.currentTimeMillis();
		Calendar c = Calendar.getInstance();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst_summ = null,m_pst_pay = null,m_pst_pay_summ = null,m_pst_approval = null;
		PreparedStatement m_pst_cmp=null, m_pst_log=null,m_pst_cmp_chr,m_pst_chk_del;
		ResultSet m_rs_summ = null,rs_cmp=null,rs_outer=null,rs_chr;
		String prev_cc,prev_dri,c_dri;
		int status=0;
		String pr_st_hr= c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);

		try {

			int key =0 , counter=0,no_of_rec=0;
			double AMOUNT =0; 
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_conn.setAutoCommit(false); 


			m_pst_cmp_chr = m_conn.prepareStatement("select " +
					" * from " +
					" TDS_SCHEDULED_PROCESSING p, TDS_COMPANYDETAIL cd " +
					" where  p.SCH_ASSOCCODE = cd.CD_ASSOCCODE and p.SCH_STATUS=1" +
					" and p.SCH_DAY= UCASE(date_format('"+date+"','%a')) and SCH_DRIVER_CHARGES = 1 order by CD_ASSOCCODE"); 
			rs_chr = m_pst_cmp_chr.executeQuery();

			cat.info("CHARGES::"+m_pst_cmp_chr.toString());

			while(rs_chr.next())
			{


				m_pst_cmp = m_conn.prepareStatement("select  " +
						"	distinct DC_ASSOCODE " +
						"from TDS_DRIVER_CHARGES dc " +
						"where " +
						" dc.DC_STATUS=1 and DC_ASSOCODE = '"+rs_chr.getString("CD_ASSOCCODE")+"' and " +
						" case DC_FREQUECY when 1 then 1" +
						" when 2 then" +
						"   case DC_LDAY when 1 then" +
						"   case date_format(GET_FIRST_DAY('"+date+"'),'%Y%m%d') when '"+date+"' then 1 else 0 end" +
						"   when 2 then" +
						"   case date_format(GET_LAST_DAY('"+date+"'),'%Y%m%d') when '"+date+"' then 1 else 0 end" +
						"   end" +
						"  end" ); 
				rs_cmp = m_pst_cmp.executeQuery();

				while(rs_cmp.next()) 
				{

					prev_cc="";prev_dri="";c_dri="";
					counter = 0;
					status=1; 
					m_pst_summ = m_conn.prepareStatement("select a.* ,dc.* from TDS_ADMINUSER a, TDS_DRIVER_CHARGES dc where a.AU_ASSOCCODE = '"+rs_cmp.getString("DC_ASSOCODE")+"' and dc.DC_ASSOCODE = a.AU_ASSOCCODE  and a.AU_USERTYPE = 'Driver' and AU_ACTIVE = 1 order by a.AU_SNO,dc.DC_CHARGE_NO");
					m_rs_summ = m_pst_summ.executeQuery();

					m_pst_approval = m_conn.prepareStatement("insert into TDS_DRIVER_CHARGES_APPROVAL(DCA_DRIVER_ID,DCA_COMPANY_ID,DCA_CHARGE_NO,DCA_DESC ,DCA_AMOUNT,DCA_CHARGE_DATE,DCA_PROCESSING_DATE) values(?,?,?,?,?,?,now())");

					while(m_rs_summ.next())
					{

						m_pst_approval.setString(1, m_rs_summ.getString("AU_SNO"));
						m_pst_approval.setString(2, m_rs_summ.getString("DC_ASSOCODE"));
						m_pst_approval.setString(3, m_rs_summ.getString("DC_CHARGE_NO"));
						m_pst_approval.setString(4, m_rs_summ.getString("DC_DESC"));
						m_pst_approval.setString(5, m_rs_summ.getString("DC_AMOUNT"));
						m_pst_approval.setString(6, m_rs_summ.getString("DC_ENTERED_DATE"));  
						m_pst_approval.execute(); 

						/*m_pst_chk_del = m_conn.prepareStatement("delete from TDS_WORK where TW_TRANSID = '"+m_rs.getString("TW_TRANSID")+"'");
							m_pst_chk_del.execute();
							m_pst_chk_del.close();	*/	
					}

					cat.info("TDS DRIVER CHAR APPROVAL:::"+m_pst_approval.toString());
					if(rs_cmp.isLast())
					{
						m_pst_log = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING_LOG (SL_DAY,SL_CRON_JOB,SL_START_TIME,SL_END_TIME)values(now(),'C2',?,time_format(now(),'%H:%i:%s'))");
						m_pst_log.setString(1, pr_st_hr);
						m_pst_log.execute();

						m_conn.commit();
					} 
				} 

				m_pst_cmp.close();
			}

			m_conn.setAutoCommit(true);

		} catch (SQLException p_sqex) {
			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException sqex) {
					cat.error("TDSException AuditDAO.insertauditRequest-->"+sqex.getMessage());
					cat.error(m_pst_cmp.toString());
					//System.out.println("TDSException AuditDAO.insertauditRequest-->"+sqex.getMessage());
					sqex.printStackTrace();		
				}
			}

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}
	public static int moveToDriverApproveltoChargesSetteled(String date)
	{


		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		if(UtilityDAO.chckCronJobExec(date, "C6","Y"))
			return 1;

		String trace = "E-" +System.currentTimeMillis();
		Calendar c = Calendar.getInstance();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst_summ = null,m_pst_pay = null,m_pst_pay_summ = null,m_pst_approvel = null,m_com_cc=null;
		PreparedStatement m_pst_cmp=null, m_pst_log=null,m_pst_cmp_chr,m_pst_del;
		ResultSet rs=null,cc_setup = null,m_rs_summ = null,rs_cmp=null,rs_outer=null,rs_chr;
		String prev_cc,prev_dri,c_dri;
		int status=0;
		String pr_st_hr= c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);

		try {

			//boolean  result = UtilityDAO.chckCronJobExec(date, "C2","Y");
			//if(result = true) {

			//if(UtilityDAO.chckCronJobExec(date, "C2","Y")) {




			int key =0 , counter=0,no_of_rec=0;
			double AMOUNT =0,TIP_AMOUNT=0,TOTAL_AMOUNT=0,TXN_PERCENT_AMT=0,TXN_AMT=0,TOTAL_AMOUNT_WITH_TXN=0;
			double CCC_TXN_AMT_PERCENTAGE=0,CCC_TXN_AMOUNT=0;


			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_conn.setAutoCommit(false);



			m_pst_cmp_chr = m_conn.prepareStatement("select " +
					" * from " +
					" TDS_SCHEDULED_PROCESSING p, TDS_COMPANYDETAIL cd " +
					" where  p.SCH_ASSOCCODE = cd.CD_ASSOCCODE and p.SCH_STATUS=1" +
					" and p.SCH_DAY= UCASE(date_format('"+date+"','%a')) and SCH_DRIVER_CHARGES = 1 order by CD_ASSOCCODE"); 
			rs_chr = m_pst_cmp_chr.executeQuery();

			while(rs_chr.next())
			{ 
				prev_cc="";prev_dri="";c_dri="";
				counter = 0;
				status=1;

				m_pst_summ = m_conn.prepareStatement("select * from TDS_DRIVER_CHARGES_APPROVAL where DCA_COMPANY_ID = '"+rs_chr.getString("CD_ASSOCCODE")+"' and date_format(DCA_CHARGE_DATE,'%Y%m%d%H%i%s') <= concat('"+date+"',time_format('"+rs_chr.getString("SCH_TIME")+"','%H%i%s')) order by DCA_DRIVER_ID");
				m_rs_summ = m_pst_summ.executeQuery();



				m_pst_pay = m_conn.prepareStatement("insert into TDS_DRIVER_SETELED_DETAIL (CSD_SET_ID,CSD_TRAN_ID,CSD_DRIVER_ID,CSD_COMPANY_ID,CSD_CHRAGE_NO,CSD_DESC,CSD_AMOUNT,CSD_PROCESSING_DATE,CSD_CHARGE_DATE) values(?,?,?,?,?,?,?,now(),?)"); 

				m_pst_pay_summ = m_conn.prepareStatement("insert into TDS_DRIVER_SETELED_SUMMARY(CSS_SET_ID,CSS_DRIVER_ID,CSS_COMPANY_ID,CSS_TOTAL_AMOUNT,CSS_PROCESSING_DATE,CSS_NO_OF_REC) values(?,?,?,?,now(),?)");

				while(m_rs_summ.next())
				{				


					c_dri=m_rs_summ.getString("DCA_DRIVER_ID"); 
					if(!prev_dri.equalsIgnoreCase(c_dri))
					{  
						if(counter !=0)
						{ 	
							m_pst_pay_summ.setInt(1, key);
							m_pst_pay_summ.setString(2,prev_dri);
							m_pst_pay_summ.setString(3,prev_cc);
							m_pst_pay_summ.setString(4,""+AMOUNT);
							m_pst_pay_summ.setString(5,""+no_of_rec); 
							m_pst_pay_summ.execute();

							m_pst_del = m_conn.prepareStatement("delete from TDS_DRIVER_CHARGES_APPROVAL where DCA_DRIVER_ID = '" + prev_dri + "'");
							m_pst_del.execute();

							m_conn.commit(); 
							AMOUNT=0;TIP_AMOUNT =0;TOTAL_AMOUNT=0;
							no_of_rec = 0;
						}

						key  =  ConfigDAO.tdsKeyGen("TDS_DRIVER_SETTELED_SEQ", m_conn);

					} 
					m_pst_pay.setInt(1, key);
					m_pst_pay.setInt(2, ConfigDAO.tdsKeyGen("TDS_DRIVER_SETTELED_DETAIL_SEQ", m_conn));
					m_pst_pay.setString(3, m_rs_summ.getString("DCA_DRIVER_ID"));
					m_pst_pay.setString(4, m_rs_summ.getString("DCA_COMPANY_ID"));
					m_pst_pay.setString(5, m_rs_summ.getString("DCA_CHARGE_NO"));
					m_pst_pay.setString(6, m_rs_summ.getString("DCA_DESC"));
					m_pst_pay.setString(7, m_rs_summ.getString("DCA_AMOUNT"));
					m_pst_pay.setString(8, m_rs_summ.getString("DCA_CHARGE_DATE"));  

					m_pst_pay.execute(); 

					AMOUNT = AMOUNT  + m_rs_summ.getDouble("DCA_AMOUNT");  

					no_of_rec = no_of_rec+1; 
					counter =1; 
					prev_cc=m_rs_summ.getString("DCA_COMPANY_ID");
					prev_dri=m_rs_summ.getString("DCA_DRIVER_ID"); 


					if(m_rs_summ.isLast())
					{
						m_pst_pay_summ.setInt(1, key);
						m_pst_pay_summ.setString(2,prev_dri);
						m_pst_pay_summ.setString(3,prev_cc);
						m_pst_pay_summ.setString(4,""+AMOUNT);
						m_pst_pay_summ.setString(5,""+no_of_rec);   

						m_pst_pay_summ.execute();	

						m_pst_del = m_conn.prepareStatement("delete from TDS_DRIVER_CHARGES_APPROVAL where DCA_DRIVER_ID = '" + prev_dri + "'");
						m_pst_del.execute();

						m_conn.commit();
						AMOUNT=0;TIP_AMOUNT =0;TOTAL_AMOUNT=0;
						no_of_rec = 0;
					}
				}

				if(rs_chr.isLast())
				{
					m_pst_log = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING_LOG (SL_DAY,SL_CRON_JOB,SL_START_TIME,SL_END_TIME)values(now(),'C6',?,time_format(now(),'%H:%i:%s'))");
					m_pst_log.setString(1, pr_st_hr);
					m_pst_log.execute();

					m_conn.commit();
				} 
			}

			m_conn.setAutoCommit(true);

		} catch (SQLException p_sqex) {
			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException sqex) {
					cat.error("TDSException BatchDAO.moveToDriverApproveltoChargesSetteled-->"+sqex.getMessage());
					cat.error(m_pst_cmp.toString());
					//System.out.println("TDSException BatchDAO.moveToDriverApproveltoChargesSetteled-->"+sqex.getMessage());
					sqex.printStackTrace();		
				}
			}

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


	public static int moveToVoucherSetelment(String date)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		//		if(UtilityDAO.chckCronJobExec(date, "C4","Y"))
		//			return 1;

		boolean allowSetleFlag;
		String trace = "F-" +System.currentTimeMillis();
		Calendar c = Calendar.getInstance();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst_summ ,m_pst_pay,m_pst_pay_summ;
		PreparedStatement m_pst_cmp = null, m_pst_log,m_pst_del,mst_pst_allow;
		ResultSet m_rs_summ ,rs_cmp,rs,rs_allow;
		String prev_dri,c_dri;
		int status=0;
		String pr_st_hr= c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);

		try {

			int key =0 , counter=0,no_of_rec=0;
			double AMOUNT =0,TIP_AMOUNT=0,TOTAL_AMOUNT=0,TXN_PERCENT_AMT=0,TXN_AMT=0,TOTAL_AMOUNT_WITH_TXN=0;
			double CCC_TXN_AMT_PERCENTAGE=0,CCC_TXN_AMOUNT=0;

			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_conn.setAutoCommit(false);
			if(!UtilityDAO.chckCronJobExec(date, "C4","Y"))
			{ 
				m_pst_cmp = m_conn.prepareStatement("select " +
						" * from " +
						" TDS_SCHEDULED_PROCESSING p, TDS_COMPANYDETAIL cd " +
						" where  p.SCH_ASSOCCODE = cd.CD_ASSOCCODE and p.SCH_STATUS=1" +
						" and p.SCH_DAY= UCASE(date_format('"+date+"','%a'))");
				cat.info(m_pst_cmp.toString());
				//System.out.println(m_pst_cmp.toString());
				rs_cmp = m_pst_cmp.executeQuery();

				while(rs_cmp.next()) 
				{
					m_pst_cmp = m_conn.prepareStatement("select * from TDS_COMPANY_CC_SETUP where CCC_COMPANY_ID = '"+rs_cmp.getString("CD_ASSOCCODE")+"' and CCC_CARD_TYPE = 'V' ");
					cat.info(m_pst_cmp.toString());
					//	System.out.println(m_pst_cmp.toString());
					rs = m_pst_cmp.executeQuery();

					if(rs.first())
					{
						CCC_TXN_AMT_PERCENTAGE = rs.getDouble("CCC_TXN_AMT_PERCENTAGE"); 
						CCC_TXN_AMOUNT = rs.getDouble("CCC_TXN_AMOUNT");
					}

					prev_dri="";c_dri="";
					counter = 0;

					status=1;

					//					m_pst_summ = m_conn.prepareStatement("select * from TDS_VOUCHER_WORK where VW_ASSOCCODE = '"+rs_cmp.getString("CD_ASSOCCODE")+"' and date_format(date_add(VW_SERVICE_DATE,INTERVAL VW_DELAYDAYS DAY),'%Y%m%d%H%i%s') <= concat('"+date+"',time_format('"+rs_cmp.getString("SCH_TIME")+"','%H%i%s')) and VW_PAYMENT_RECEIVEDFLG =true order by VW_DRIVER_ID");
					m_pst_summ = m_conn.prepareStatement("select *, DATEDIFF(NOW(),VW_CREATION_DATE) AS AGEINDAYS from TDS_VOUCHER_WORK left outer join TDS_VOUCHER_INVOICE_SUMMARY on VW_INVOICE_NO=VIS_INVOICE_NUMBER where VW_ASSOCCODE = '"+rs_cmp.getString("CD_ASSOCCODE")+"' and date_format(date_add(VW_SERVICE_DATE,INTERVAL VW_DELAYDAYS DAY),'%Y%m%d%H%i%s') <= concat('"+date+"',time_format('"+rs_cmp.getString("SCH_TIME")+"','%H%i%s')) order by VW_DRIVER_ID");
					cat.info(m_pst_summ.toString());
					//	System.out.println(m_pst_summ.toString());

					m_rs_summ = m_pst_summ.executeQuery();

					m_pst_pay = m_conn.prepareStatement("insert into TDS_VOUCHER_SETELED_DETAIL(VSD_ID,VSD_VOUCHERNO,VSD_RIDERNAME,VSD_COSTCENTER,VSD_ASSOCCODE,VSD_AMOUNT," +
							"VSD_EXPIRYDATE,VSD_DESCR,VSD_CREATION_DATE,VSD_USER,VSD_TRIP_ID,VSD_CCODE,VSD_CONTACT,VSD_DELAYDAYS," +
							"VSD_FREQ,VSD_SERVICE_DATE ,VSD_TRANS_ID,VSD_DRIVER_ID,VSD_RIDER_SIGN,VSD_RET_AMOUNT,VSD_TIP,VSD_TOTAL,VSD_TXN_PERCENT_AMT,VSD_TXN_AMT,VSD_TOTAL_AMOUNT_WITH_TXN,VSD_INVOICE_NO)" +
							"values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");


					m_pst_pay_summ = m_conn.prepareStatement("insert into TDS_VOUCHER_SETELED_SUMMARY(VSS_ID,VSS_DRIVER_ID,VSS_ASSOCCODE,VSS_PROCESSING_DATE,VSS_RET_AMOUNT,VSS_TIP,VSS_TOTAL," +
							"VSS_TXN_PERCENT_AMT,VSS_TXN_AMT,VSS_TOTAL_AMOUNT_WITH_TXN,VSS_NO_OF_REC) values(?,?,?,now(),?,?,?,?,?,?,?)");



					while(m_rs_summ.next())
					{
						allowSetleFlag = false;
						int delayDays = m_rs_summ.getInt("VW_DELAYDAYS");
						if(delayDays >= 0 && m_rs_summ.getInt("AGEINDAYS") >= delayDays){
							allowSetleFlag = true;
						}
						else if(m_rs_summ.getInt("VW_DELAYDAYS") < 0 && !m_rs_summ.getString("VW_INVOICE_NO").equals("") && (m_rs_summ.getString("VW_PAYMENT_RECEIVEDFLG")!=null && (m_rs_summ.getInt("VW_PAYMENT_RECEIVEDFLG") ==1 || m_rs_summ.getInt("VW_PAYMENT_RECEIVEDFLG") ==2)))
						{

							//mst_pst_allow = m_conn.prepareStatement("select * from TDS_VOUCHER_INVOICE_SUMMARY where VIS_INVOICE_NUMBER="+m_rs_summ.getString("VW_INVOICE_NO"));
							allowSetleFlag = true;	

						} 

						if(allowSetleFlag)
						{
							c_dri=m_rs_summ.getString("VW_DRIVER_ID");

							if(!prev_dri.equalsIgnoreCase(c_dri))
							{ 

								if(counter !=0)
								{

									m_pst_pay_summ.setInt(1, key);
									m_pst_pay_summ.setString(2,prev_dri);
									m_pst_pay_summ.setString(3,m_rs_summ.getString("VW_ASSOCCODE"));
									m_pst_pay_summ.setString(4,""+AMOUNT);
									m_pst_pay_summ.setString(5,""+TIP_AMOUNT);
									m_pst_pay_summ.setString(6,""+TOTAL_AMOUNT);
									m_pst_pay_summ.setString(7,""+TXN_PERCENT_AMT);
									m_pst_pay_summ.setString(8,""+TXN_AMT);
									m_pst_pay_summ.setString(9,""+TOTAL_AMOUNT_WITH_TXN);
									m_pst_pay_summ.setString(10,""+no_of_rec);

									m_pst_pay_summ.execute();

									m_conn.commit();

									AMOUNT=0;TIP_AMOUNT =0;TOTAL_AMOUNT=0;TXN_AMT=0;TXN_PERCENT_AMT=0;TOTAL_AMOUNT_WITH_TXN=0;
									no_of_rec = 0;
								}

								key  =  ConfigDAO.tdsKeyGen("TDS_VOUCHER_SETTELED_SEQ", m_conn);

							}
							m_pst_pay.setInt(1, key);
							m_pst_pay.setString(2, m_rs_summ.getString("VW_VOUCHERNO"));
							m_pst_pay.setString(3, m_rs_summ.getString("VW_RIDERNAME"));
							m_pst_pay.setString(4, m_rs_summ.getString("VW_COSTCENTER"));
							m_pst_pay.setString(5, m_rs_summ.getString("VW_ASSOCCODE"));
							m_pst_pay.setString(6, m_rs_summ.getString("VW_AMOUNT"));
							m_pst_pay.setString(7, m_rs_summ.getString("VW_EXPIRYDATE"));
							m_pst_pay.setString(8, m_rs_summ.getString("VW_DESCR"));
							m_pst_pay.setString(9, m_rs_summ.getString("VW_CREATION_DATE"));
							m_pst_pay.setString(10, m_rs_summ.getString("VW_USER"));
							m_pst_pay.setString(11, m_rs_summ.getString("VW_TRIP_ID"));
							m_pst_pay.setString(12, m_rs_summ.getString("VW_CCODE"));
							m_pst_pay.setString(13, m_rs_summ.getString("VW_CONTACT"));
							m_pst_pay.setString(14, m_rs_summ.getString("VW_DELAYDAYS"));
							m_pst_pay.setString(15, m_rs_summ.getString("VW_FREQ"));
							m_pst_pay.setString(16, m_rs_summ.getString("VW_SERVICE_DATE"));
							m_pst_pay.setString(17, m_rs_summ.getString("VW_TRANS_ID"));
							m_pst_pay.setString(18, m_rs_summ.getString("VW_DRIVER_ID"));
							m_pst_pay.setBlob(19, m_rs_summ.getBlob("VW_RIDER_SIGN"));
							m_pst_pay.setString(20, m_rs_summ.getString("VW_RET_AMOUNT"));
							m_pst_pay.setString(21, m_rs_summ.getString("VW_TIP"));
							m_pst_pay.setDouble(22, (m_rs_summ.getDouble("VW_RET_AMOUNT")+m_rs_summ.getDouble("VW_TIP")));
							m_pst_pay.setDouble(23, ((CCC_TXN_AMT_PERCENTAGE/100)*((m_rs_summ.getDouble("VW_RET_AMOUNT")+m_rs_summ.getDouble("VW_TIP")))));
							m_pst_pay.setDouble(24, CCC_TXN_AMOUNT);
							m_pst_pay.setDouble(25, ((m_rs_summ.getDouble("VW_RET_AMOUNT")+m_rs_summ.getDouble("VW_TIP"))-CCC_TXN_AMOUNT-((CCC_TXN_AMT_PERCENTAGE/100)*((m_rs_summ.getDouble("VW_RET_AMOUNT")+m_rs_summ.getDouble("VW_TIP"))))));
							m_pst_pay.setString(26, m_rs_summ.getString("VW_INVOICE_NO"));

							m_pst_pay.execute();


							AMOUNT = AMOUNT  + m_rs_summ.getDouble("VW_RET_AMOUNT");
							TIP_AMOUNT = TIP_AMOUNT + m_rs_summ.getDouble("VW_TIP");
							TOTAL_AMOUNT = TOTAL_AMOUNT + m_rs_summ.getDouble("VW_RET_AMOUNT") + m_rs_summ.getDouble("VW_TIP");

							TXN_AMT = TXN_AMT + CCC_TXN_AMOUNT;
							TXN_PERCENT_AMT = TXN_PERCENT_AMT +  ((CCC_TXN_AMT_PERCENTAGE/100)*((m_rs_summ.getDouble("VW_RET_AMOUNT")+m_rs_summ.getDouble("VW_TIP"))));
							TOTAL_AMOUNT_WITH_TXN = TOTAL_AMOUNT_WITH_TXN + ((m_rs_summ.getDouble("VW_RET_AMOUNT")+m_rs_summ.getDouble("VW_TIP"))-CCC_TXN_AMOUNT-((CCC_TXN_AMT_PERCENTAGE/100)*((m_rs_summ.getDouble("VW_RET_AMOUNT")+m_rs_summ.getDouble("VW_TIP")))));

							no_of_rec = no_of_rec+1;

							counter =1;

							prev_dri=m_rs_summ.getString("VW_DRIVER_ID");


							m_pst_del = m_conn.prepareStatement("delete from TDS_VOUCHER_WORK where VW_TRANS_ID = '" + m_rs_summ.getString("VW_TRANS_ID") + "'");
							m_pst_del.execute();

							if(m_rs_summ.isLast())
							{

								m_pst_pay_summ.setInt(1, key);
								m_pst_pay_summ.setString(2,prev_dri);
								m_pst_pay_summ.setString(3,m_rs_summ.getString("VW_ASSOCCODE"));
								m_pst_pay_summ.setString(4,""+AMOUNT);
								m_pst_pay_summ.setString(5,""+TIP_AMOUNT);
								m_pst_pay_summ.setString(6,""+TOTAL_AMOUNT);
								m_pst_pay_summ.setString(7,""+TXN_PERCENT_AMT);
								m_pst_pay_summ.setString(8,""+TXN_AMT);
								m_pst_pay_summ.setString(9,""+TOTAL_AMOUNT_WITH_TXN);
								m_pst_pay_summ.setString(10,""+no_of_rec);

								m_pst_pay_summ.execute();

								m_conn.commit();

								AMOUNT=0;TIP_AMOUNT =0;TOTAL_AMOUNT=0;TXN_AMT=0;TXN_PERCENT_AMT=0;TOTAL_AMOUNT_WITH_TXN=0;
								no_of_rec = 0;				



							}
						}

					}

					if(rs_cmp.isLast())
					{
						m_pst_log = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING_LOG (SL_DAY,SL_CRON_JOB,SL_START_TIME,SL_END_TIME)values(now(),'C4',?,time_format(now(),'%H:%i:%s'))");
						m_pst_log.setString(1, pr_st_hr);
						m_pst_log.execute();

						m_conn.commit();
					}
				}
				m_pst_cmp.close();
			}	

			m_conn.setAutoCommit(true);

		} catch (SQLException sqex) {

			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException e) {
					cat.error("TDSException BatchDAO.moveToVoucherSetelment-->"+e.getMessage());
					cat.error(m_pst_cmp.toString());
					//		System.out.println("TDSException BatchDAO.moveToVoucherSetelment-->"+e.getMessage());
					sqex.printStackTrace();		
				}
			}
			cat.error("TDSException BatchDAO.moveToVoucherSetelment-->"+sqex.getMessage());
			//	System.out.println("TDSException BatchDAO.moveToVoucherSetelment-->"+sqex.getMessage());
			sqex.printStackTrace();	

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}

	public static int moveToDriverPenality(String date)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		if(UtilityDAO.chckCronJobExec(date, "C3","Y"))
			return 1;
		String trace = "H-" +System.currentTimeMillis();
		Calendar c = Calendar.getInstance();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst_summ = null,m_pst_pay = null,m_pst_pay_summ = null;
		PreparedStatement m_pst_del=null,m_pst_cmp=null, m_pst_cmp_up = null,m_pst_log=null;
		ResultSet m_rs_summ = null,rs_cmp=null;
		String prev_cc,prev_dri,c_dri;
		int key =0 , counter=0,no_of_rec=0;
		double AMOUNT =0;
		int status=0;
		String pr_st_hr= c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);
		try {

			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_conn.setAutoCommit(false);
			if(!UtilityDAO.chckCronJobExec(date, "C3","Y"))
			{  
				m_pst_cmp = m_conn.prepareStatement("select " +
						" * from " +
						" TDS_SCHEDULED_PROCESSING p, TDS_COMPANYDETAIL cd " +
						" where  p.SCH_ASSOCCODE = cd.CD_ASSOCCODE and p.SCH_STATUS=1" +
						" and p.SCH_DAY= UCASE(date_format('"+date+"','%a'))");			
				rs_cmp = m_pst_cmp.executeQuery();

				while(rs_cmp.next()) 
				{
					prev_cc="";prev_dri="";c_dri="";
					counter = 0;

					status=1;

					m_pst_summ = m_conn.prepareStatement("select * from TDS_DRIVER_PENALITY where DP_ASSOCCODE  = '"+rs_cmp.getString("CD_ASSOCCODE")+"' and date_format(DP_PENALITY_DATE,'%Y%m%d') <= '"+date+"' order by DP_DRIVER_ID");
					m_rs_summ = m_pst_summ.executeQuery();

					m_pst_pay = m_conn.prepareStatement("insert into TDS_DRIVER_PENALITY_SETELED_DETAIL (DPD_SET_ID,DPD_TRAN_ID,DPD_DRIVER_ID,DPD_COMPANY_ID,DPD_DESC,DPD_AMOUNT,DPD_PENALITY_DATE,DPD_PROCESSING_DATE)" +
							"values(?,?,?,?,?,?,?,now())");


					m_pst_pay_summ = m_conn.prepareStatement("insert into TDS_DRIVER_PENALITY_SETELED_SUMMARY(DPS_SET_ID,DPS_DRIVER_ID,DPS_COMPANY_ID,DPS_TOTAL_AMOUNT,DPS_PROCESSING_DATE,DPS_NO_OF_REC)" +
							"values(?,?,?,?,now(),?)");

					while(m_rs_summ.next())
					{
						c_dri=m_rs_summ.getString("DP_DRIVER_ID");

						if(!prev_dri.equalsIgnoreCase(c_dri))
						{ 

							if(counter !=0)
							{

								m_pst_pay_summ.setInt(1, key);
								m_pst_pay_summ.setString(2,prev_dri);
								m_pst_pay_summ.setString(3,prev_cc);
								m_pst_pay_summ.setString(4,""+AMOUNT);
								m_pst_pay_summ.setString(5,""+no_of_rec);

								m_pst_pay_summ.execute();

								m_pst_cmp_up = m_conn.prepareStatement("update TDS_SCHEDULED_PROCESSING set SCH_PEN_UP_TO_DATE = now()  where SCH_ASSOCCODE = ?");
								m_pst_cmp_up.setString(1, rs_cmp.getString("CD_ASSOCCODE"));
								m_pst_cmp.execute();

								m_conn.commit();

								AMOUNT=0;
								no_of_rec = 0;
							}

							key  =  ConfigDAO.tdsKeyGen("TDS_DRIVER_PENALITY_SETTELED_SEQ", m_conn);

						}


						m_pst_pay.setInt(1, key);
						m_pst_pay.setString(2, m_rs_summ.getString("DP_TRANS_ID"));
						m_pst_pay.setString(3, m_rs_summ.getString("DP_DRIVER_ID"));
						m_pst_pay.setString(4, m_rs_summ.getString("DP_ASSOCCODE"));
						m_pst_pay.setString(5, m_rs_summ.getString("DP_DESC"));
						m_pst_pay.setString(6, m_rs_summ.getString("DP_AMOUNT"));
						m_pst_pay.setString(7, m_rs_summ.getString("DP_PENALITY_DATE"));

						m_pst_pay.execute();



						AMOUNT = AMOUNT  + m_rs_summ.getDouble("DP_AMOUNT");

						no_of_rec = no_of_rec+1;

						counter =1;

						prev_cc=m_rs_summ.getString("DP_ASSOCCODE");
						prev_dri=m_rs_summ.getString("DP_DRIVER_ID");

						m_pst_del = m_conn.prepareStatement("delete from TDS_DRIVER_PENALITY where DP_TRANS_ID = '" + m_rs_summ.getString("DP_TRANS_ID")+"'");
						m_pst_del.execute();

						if(m_rs_summ.isLast())
						{
							m_pst_pay_summ.setInt(1, key);
							m_pst_pay_summ.setString(2,prev_dri);
							m_pst_pay_summ.setString(3,prev_cc);
							m_pst_pay_summ.setString(4,""+AMOUNT);
							m_pst_pay_summ.setString(5,""+no_of_rec);

							m_pst_pay_summ.execute();

							m_pst_cmp_up = m_conn.prepareStatement("update TDS_SCHEDULED_PROCESSING set SCH_PEN_UP_TO_DATE = now() where SCH_ASSOCCODE = ?");
							m_pst_cmp_up.setString(1, rs_cmp.getString("CD_ASSOCCODE"));
							m_pst_cmp.execute();

							m_conn.commit();
						}

					}

					if(rs_cmp.isLast())
					{
						m_pst_log = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING_LOG (SL_DAY,SL_CRON_JOB,SL_START_TIME,SL_END_TIME)values(now(),'C3',?,time_format(now(),'%H:%i:%s'))");
						m_pst_log.setString(1, pr_st_hr);
						m_pst_log.execute();

						m_conn.commit();
					}
				}
				m_pst_cmp.close();
				cat.info(trace);
			}


			m_conn.setAutoCommit(true);

		} catch (SQLException sqex) {
			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
					m_conn.setAutoCommit(true);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			cat.error("TDSException BatchDAO.moveToDriverPenality-->"+sqex.getMessage());
			cat.error(m_pst_summ.toString());
			//System.out.println("TDSException BatchDAO.moveToDriverPenality-->"+sqex.getMessage());
			sqex.printStackTrace();				
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;

	}

	public static void deleteOlderMessages(){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst,m_pst = null;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst=conn.prepareStatement("DELETE FROM TDS_TEXT_MESSAGE where DATE_ADD( TM_TIME, INTERVAL 720 HOUR) <= CURRENT_DATE() ");
			cat.info("query--->"+pst.toString());
			pst.execute();
			m_pst=conn.prepareStatement("DELETE FROM TDS_VOICE_MESSAGE where DATE_ADD( VM_TIME, INTERVAL 720 HOUR) <= CURRENT_DATE() ");
			cat.info("query--->"+m_pst.toString());
			m_pst.execute();
		}catch(Exception sqex){
			cat.error("TDSException BatchDAO.deleteOlderMessages-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException BatchDAO.deleteOlderMessages-->"+sqex.getMessage());
			sqex.printStackTrace();	
		}finally{
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}




	@SuppressWarnings("unused")
	public static int doCaptureAll(String date)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);		ArrayList<OpenRequestBO> al_list = new ArrayList<OpenRequestBO>();
		if(true){
			return 1;
		}


		if(UtilityDAO.chckCronJobExec(date, "C7","Y"))
			return 1;

		String trace = "I-" +System.currentTimeMillis();
		ApplicationPoolBO poolBO = null;
		PaymentProcessBean processBean = new PaymentProcessBean();
		ServletConfig config = ResettingSessionCron.config;
		Calendar c = Calendar.getInstance();
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst_chk_del, m_pst_app,m_pst_log ;
		ResultSet m_rs = null,m_rs_chk = null;
		int status=0;
		String pr_st_hr= c.get(Calendar.HOUR_OF_DAY)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND);
		if(config != null)
			poolBO = (ApplicationPoolBO)config.getServletContext().getAttribute("poolBO");

		try {

			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_conn.setAutoCommit(false);
			if(poolBO!=null && !UtilityDAO.chckCronJobExec(date, "C7","Y"))
			{
				m_pst = m_conn.prepareStatement("select CD_ASSOCCODE from  TDS_COMPANYDETAIL");
				m_rs = m_pst.executeQuery();
				while(m_rs.next())
				{
					m_pst = m_conn.prepareStatement("select " +
							" w.*,ad.AU_ASSOCCODE,c.CD_REC_SET,TIMESTAMPDIFF(MINUTE,TW_PROCESSING_DATE_TIME,now()) >= c.CD_LAGTIME as flg," +
							" from TDS_WORK w, TDS_ADMINUSER ad,TDS_COMPANYDETAIL c" +
							" WHERE w.TW_DRIVERID=ad.AU_SNO and c.CD_ASSOCCODE= ad.AU_ASSOCCODE " +
							" and TW_AUTHCAPFLG !='C' and  w.TW_DRIVERID=ad.AU_SNO  and ad.AU_ASSOCCODE = "+m_rs.getString("CD_ASSOCCODE")+" " +
							" order by ad.AU_ASSOCCODE,w.TW_DRIVERID");
					m_rs_chk = m_pst.executeQuery();
					while(m_rs_chk.next() && m_rs.getInt("flg") == 1)
					{					 

						processBean.setB_amount(m_rs_chk.getString("TW_AMOUNT"));
						processBean.setB_trans_id(m_rs_chk.getString("TW_TRANSID"));;
						processBean.setB_tip_amt(m_rs_chk.getString("TW_TIPAMOUNT"));
						processBean.setB_sign(m_rs_chk.getBytes("TW_RIDERSIGN"));
						processBean = PaymentProcessingAction.capturePayment(poolBO,processBean,m_rs.getString("CD_ASSOCCODE"));
						if(processBean.getB_approval_status().equalsIgnoreCase("1")){
							//ProcessingDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), processBean.getB_trans_id(), processBean.getB_cap_trans(),processBean.getB_sign());
							/*ChargesDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), processBean.getB_trans_id(),
									processBean.getB_trans_id2(), processBean.getB_cap_trans(), "", processBean.getB_sign(),
									((AdminRegistrationBO) p_request.getSession()
											.getAttribute("user")).getUid());*/

						}
						m_conn.commit();
					}
					if(m_rs.isLast())
					{
						m_pst_log = m_conn.prepareStatement("insert into TDS_SCHEDULED_PROCESSING_LOG (SL_DAY,SL_CRON_JOB,SL_START_TIME,SL_END_TIME)values(now(),'C7',?,time_format(now(),'%H:%i:%s'))");
						m_pst_log.setString(1, pr_st_hr);
						m_pst_log.execute();

						m_conn.commit();
					}
				}
			}
		} catch (SQLException sqex) {
			status=0;
			if(m_conn != null)
			{
				try {
					m_conn.rollback();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			cat.error("TDSException BatchDAO.doCaptureAll-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//	System.out.println("TDSException BatchDAO.doCaptureAll-->"+sqex.getMessage());
			sqex.printStackTrace();				
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return status;
	}


}
