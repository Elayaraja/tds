package com.tds.dao;

public class UserDetail {

	 
	private String sno;
	private String userid;
	private String username;
	private String email;
	private String phone;
	private String status;
	
	private String key;
	
	public String getsno(){
		return sno;
	}
	public void setsno(String sno){
		this.sno = sno;
	}
	public String getuserid(){
		return userid;
	}
	public void setuserid(String userid){
		this.userid = userid;
	}
	public String getusername(){
		return username;
	}
	public void setusername(String username){
		this.username = username;
	}
	public String getemail(){
		return email;
	}
	public void setemail(String email){
		this.email = email;
	}
	public String getphone(){
		return phone;
	}
	public void setphone(String phone){
		this.phone = phone;
	}
	public String getstatus(){
		return status;
	}
	public void setstatus(String status){
		this.status = status; 
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
}
