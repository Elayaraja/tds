package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;

public class AcraDAO {
	private static Category cat =TDSController.cat;
	
	static {
		cat = Category.getInstance(ZoneDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ZoneDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
	}
	
	public static void insertError(String driverID, String cabNO, String androidVerion, String appVersion, String logCat, String stackTrace, String reportID, String customFields){
		long startTime = System.currentTimeMillis();
		cat.info("AcraDAO.insertError Start--->"+startTime);
		ArrayList<ZoneTableBeanSP> al_list=new ArrayList<ZoneTableBeanSP>();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query = "INSERT INTO TDS_ERROR_REPORTS (ER_REPORT_TIME, ER_DRIVERID,ER_CAB,ER_LOGCAT,ER_ANDROID_VERSION,ER_APP_VERION, ER_ASSOCCODE, ER_STACK_TRACE,ER_REPORT_ID, ER_CUSTOM_FIELDS) VALUES(now(),?,?,?,?,?,?,?,?,?)";
		try{
			
			pst=con.prepareStatement(query);
			pst.setString(1, driverID);
			pst.setString(2, cabNO);
			pst.setString(3,logCat);
			pst.setString(4,androidVerion);
			pst.setString(5, appVersion);
			pst.setString(6, "");
			pst.setString(7, stackTrace);
			pst.setString(8, reportID);;
			pst.setString(9, customFields);;
			
			pst.executeUpdate();
		}
		catch(SQLException sqex) {
			cat.error("TDSException AcraDAO.insertError-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AcraDAO.insertError-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info("AcraDAO.insertError End--->"+(System.currentTimeMillis()-startTime));
		return;
	}

}
