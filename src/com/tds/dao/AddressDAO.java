package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.sound.midi.SysexMessage;

import org.apache.log4j.Category;

import com.tds.cmp.bean.Address;
import com.tds.cmp.bean.CustomerProfile;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
public class AddressDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(AddressDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+AddressDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	public static ArrayList<CustomerProfile> getCustomerProfile(String phoneno,String name,AdminRegistrationBO adminBO,int value,String masterKey){
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.getCustomerProfile Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ArrayList<CustomerProfile> profileHistory = new ArrayList<CustomerProfile>();
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query="";
			if(value==1){
				query = "select * from TDS_USERADDRESSMASTER where CU_MASTERASSOCIATIONCODE = '"+adminBO.getMasterAssociateCode()+"'AND CU_MASTER_KEY='"+masterKey+"'";
			}else{
				query = "select * from TDS_USERADDRESSMASTER where CU_MASTERASSOCIATIONCODE = '"+ adminBO.getMasterAssociateCode()+"'";


				if(!phoneno.equals("") && !name.equals("")){
					query = query + " and CU_PHONENO='"+phoneno+"' and CU_Name LIKE'"+name+"'";
				} else if (!phoneno.equals("") && name.equals("")){
					query = query + " and CU_PHONENO='"+phoneno+"'";					
				} else {
					query = query + " and CU_Name LIKE '"+name+"'";
				}
			}
			m_pst = m_conn.prepareStatement(query);
			ResultSet rs = m_pst.executeQuery();

			while(rs.next())
			{
				CustomerProfile orBO = new CustomerProfile();

				orBO.setName(rs.getString("CU_NAME"));
				orBO.setAdd1(rs.getString("CU_ADD1"));
				orBO.setAdd2(rs.getString("CU_ADD2"));
				orBO.setNumber(rs.getString("CU_PHONENO"));
				orBO.setCity(rs.getString("CU_CITY"));
				orBO.setState(rs.getString("CU_STATE"));
				orBO.setZip(rs.getString("CU_ZIP"));
				orBO.setAccount(rs.getString("CU_ACCOUNT"));
				orBO.setMasterKey(rs.getString("CU_MASTER_KEY"));


				orBO.setCCprofile(rs.getString("CU_CCPROFILE"));
				orBO.setCCtype(rs.getString("CU_CCTYPE"));
				orBO.setLongitude(rs.getString("CU_LONGITUDE"));
				orBO.setLatitude(rs.getString("CU_LATITUDE"));

				profileHistory.add(orBO);
			}

		}catch (Exception sqex)
		{
			cat.error("TDSException AddressDAO.Customerprofile-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.Customerprofile-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("AddressDAO.getCustomerProfile End--->"+(System.currentTimeMillis()-startTime));
		return profileHistory;
	}

	public static ArrayList<Address> getFromAndToAddress(String phoneno,int value,String masterKey,String addressKey,AdminRegistrationBO adminBO)
	{ 
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.getFromAndToAddress Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		ArrayList<Address> addressHistory = new ArrayList<Address>();
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			String query1,query2="";
			ResultSet rs2=null;
			if(value==2){
				query1 = "select * from TDS_USERADDRESS where CU_ADDRESS_KEY='"+addressKey+"'AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'";
			}else if(value==1){
				query1="select * from TDS_USERADDRESS where CU_MASTER_KEY='"+masterKey+"'AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'";
				query2="SELECT CUL_LANDMARK_KEY,LM_NAME FROM TDS_USERLANDMARK,TDS_LANDMARK where CUL_MASTERKEY='"+masterKey+"' and CUL_LANDMARK_KEY=LM_KEY AND CUL_ASSOCIATIONCODE='"+adminBO.getAssociateCode()+"'";
			}else{
				query1 = "select * from TDS_USERADDRESS where CU_PHONENO='"+phoneno+"'AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'";
			}
			m_pst = m_conn.prepareStatement(query1);
			ResultSet rs1 = m_pst.executeQuery();
			if(!query2.equals("") && query2!=null){
				m_pst1=m_conn.prepareStatement(query2);
				rs2=m_pst1.executeQuery();
			}
			while(rs1.next())
			{
				Address orBO= new Address();
				orBO.setAdd1(rs1.getString("CU_ADD1"));
				orBO.setAdd2(rs1.getString("CU_ADD2"));
				orBO.setNumber(rs1.getString("CU_PHONENO"));
				orBO.setCity(rs1.getString("CU_CITY"));
				orBO.setState(rs1.getString("CU_STATE"));
				orBO.setZip(rs1.getString("CU_ZIP"));
				orBO.setAddressKey(rs1.getString("CU_ADDRESS_KEY"));
				if(rs2!=null){
					while(rs2.next()){
						orBO.setLandmarkKey(rs2.getString("CUL_LANDMARK_KEY"));
						orBO.setLandmarkName(rs2.getString("LM_NAME"));	
					}
				}
				addressHistory.add(orBO);
			}
		}catch (Exception sqex)
		{
			cat.error("TDSException AddressDAO.getFromandToAddress-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.getFromandToAddress-->"+sqex.getMessage());
			sqex.printStackTrace();
		} 
		finally {
			m_dbConn.closeConnection();
		}
		cat.info("AddressDAO.getFromAndToAddress End--->"+(System.currentTimeMillis()-startTime));
		return addressHistory;
	}

	public static int deleteUserAddress(String addressKey,AdminRegistrationBO adminBO,String landmarkKey,String masterKey,int mode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.deleteUserAddress Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		int delete=0;
		int result=0;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 
			if(mode==1){
				m_pst = m_conn.prepareStatement("DELETE FROM TDS_USERADDRESS where CU_ADDRESS_KEY=? AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'");
				m_pst.setString(1,  addressKey);
			}else if(mode==2){
				m_pst = m_conn.prepareStatement("DELETE FROM TDS_USERLANDMARK where CUL_MASTERKEY=? AND CUL_LANDMARK_KEY=? AND CUL_ASSOCIATIONCODE='"+adminBO.getAssociateCode()+"'");
				m_pst.setString(1,  masterKey);
				m_pst.setString(2,  landmarkKey);
			}
			delete = m_pst.executeUpdate();
		} catch (Exception sqex){
			cat.error("TDSException AddressDAO.deleteUserAddress-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.deleteUserAddress-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info("AddressDAO.deleteUserAddress End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int deleteUserAddressMaster(String masterKey,AdminRegistrationBO adminBO)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.deleteUserAddressMaster Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		PreparedStatement pst =null;

		int delete=0;
		int result=0;
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 

			m_pst = m_conn.prepareStatement("DELETE FROM TDS_USERADDRESSMASTER where CU_MASTER_KEY=? AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'");
			m_pst.setString(1,masterKey);
			delete = m_pst.executeUpdate();
			if(delete>0) {
				pst =m_conn.prepareStatement("DELETE FROM TDS_USERADDRESS where CU_MASTER_KEY=? AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'");
				pst.setString(1,masterKey);
				pst.execute();
			}
		} catch (Exception sqex){
			cat.error("TDSException AddressDAO.deleteUserAddressMaster-->"+sqex.getMessage());
			//System.out.println("TDSException AddressDAO.deleteUserAddressMaster-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info("AddressDAO.deleteUserAddressMaster End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList searchPhones(String assoccode,String primaryNumber)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.searchPhones Start--->"+startTime);
		PreparedStatement pst =null;
		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection(); 
		ResultSet rs = null;
		ArrayList phone = new ArrayList();
		
		try {
			pst =m_conn.prepareStatement("SELECT UP_PHONE_NUMBER FROM TDS_USER_PHONE WHERE UP_ASSOCIATION_CODE='"+assoccode+"' AND UP_CUSTOMER_NUMBER='"+primaryNumber+"'");
			rs=pst.executeQuery();
			while(rs.next()){
				phone.add(rs.getString("UP_PHONE_NUMBER"));
			}
		} catch (Exception sqex){
			cat.error("TDSException AddressDAO.searchPhones-->"+sqex.getMessage());
			//System.out.println("TDSException AddressDAO.searchPhones-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		} 
		cat.info("AddressDAO.searchPhones Start--->"+(System.currentTimeMillis()-startTime));
		return phone;
	}
//	public static void submitPhones(String assoccode,String primaryNumber,ArrayList arrayPhones){
//		PreparedStatement pst =null;
//		TDSConnection m_dbConn = new TDSConnection();
//		Connection m_conn = m_dbConn.getConnection(); 
//		try {
//			if(arrayPhones!=null && arrayPhones.size() >0){
//				pst =m_conn.prepareStatement("INSERT INTO TDS_USER_PHONE (UP_PHONE_NUMBER,UP_ASSOCIATION_CODE,UP_CUSTOMER_NUMBER) VALUES (?,?,?)");
//				pst.setString(2, assoccode);
//				pst.setString(3, primaryNumber);
//				for(int i=0;i<arrayPhones.size();i++){
//					pst.setString(1, arrayPhones.get(i)+"");
//					pst.execute();
//				}
//			}
//		} catch (Exception sqex){
//			cat.error("TDSException AddressDAO.submitPhones Duplicate Entry. Its Ok-->"+sqex.getMessage());
//			System.out.println("TDSException AddressDAO.submitPhones Duplicate Entry. Its Ok-->"+sqex.getMessage());
//			sqex.printStackTrace();
//		}finally {
//			m_dbConn.closeConnection();
//		} 
//	}
	public static void submitPhones(String assoccode,String primaryNumber,String[] arrayPhones)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.submitPhones Start--->"+startTime);
		PreparedStatement pst =null;
		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection(); 
		try {
			if(arrayPhones!=null && arrayPhones.length >0){
				pst =m_conn.prepareStatement("DELETE FROM TDS_USER_PHONE WHERE UP_ASSOCIATION_CODE='"+assoccode+"' AND UP_CUSTOMER_NUMBER='"+primaryNumber+"'");
				pst.execute();
				pst =m_conn.prepareStatement("INSERT INTO TDS_USER_PHONE (UP_PHONE_NUMBER,UP_ASSOCIATION_CODE,UP_CUSTOMER_NUMBER) VALUES (?,?,?)");
				pst.setString(2, assoccode);
				pst.setString(3, primaryNumber);
				for(int i=0;i<arrayPhones.length;i++){
					pst.setString(1, arrayPhones[i]);
					pst.execute();
				}
			}
		} catch (Exception sqex){
			cat.error("TDSException AddressDAO.submitPhones-->"+sqex.getMessage());
			//System.out.println("TDSException AddressDAO.submitPhones-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			m_dbConn.closeConnection();
		} 
		cat.info("AddressDAO.submitPhones End--->"+(System.currentTimeMillis()-startTime));
		
	}

	public static void enterAllPhones()
	{
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.enterAllPhones Start--->"+startTime);
		PreparedStatement pst =null;
		ResultSet rs = null;
		TDSConnection m_dbConn = new TDSConnection();
		Connection m_conn = m_dbConn.getConnection(); 
		boolean result=true;
		try {
			pst =m_conn.prepareStatement("SELECT CU_PHONENO,CU_MASTER_KEY,CU_MASTERASSOCIATIONCODE FROM TDS_USERADDRESSMASTER");
			rs=pst.executeQuery();
			while(rs.next()){
				pst =m_conn.prepareStatement("UPDATE TDS_USER_PHONE SET UP_PHONE_NUMBER='"+rs.getString("CU_PHONENO")+"' WHERE UP_ASSOCIATION_CODE='"+rs.getString("CU_MASTERASSOCIATIONCODE")+"' AND UP_CUSTOMER_NUMBER='"+rs.getString("CU_MASTER_KEY")+"' AND UP_PHONE_NUMBER='"+rs.getString("CU_PHONENO")+"'");
				result=pst.execute();
				if(!result){
					pst =m_conn.prepareStatement("INSERT INTO TDS_USER_PHONE (UP_PHONE_NUMBER,UP_ASSOCIATION_CODE,UP_CUSTOMER_NUMBER) VALUES (?,?,?)");
					pst.setString(1, rs.getString("CU_PHONENO"));
					pst.setString(2, rs.getString("CU_MASTERASSOCIATIONCODE"));
					pst.setString(3, rs.getString("CU_MASTER_KEY"));
					pst.execute();
				}
			}
		} catch (Exception sqex){
			cat.error("TDSException AddressDAO.enterAllPhones-->"+sqex.getMessage());
			//System.out.println("TDSException AddressDAO.enterAllPhones-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			m_dbConn.closeConnection();
		} 
		cat.info("AddressDAO.enterAllPhones End--->"+(System.currentTimeMillis()-startTime));
	}
	
	public static Address getUserAddress(String phoneNO,AdminRegistrationBO adminBO,String addresskey,int value) {
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.getUserAddress Start--->"+startTime);
		Address addBO=null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		String query;
		if(value==1){
			query = "select * from TDS_USERADDRESS where CU_ADDRESS_KEY='"+addresskey+"' AND CU_MASTERASSOCIATIONCODE = '" +adminBO.getMasterAssociateCode()+ "'";

		}
		else{
			query = "select * from TDS_USERADDRESS where CU_PHONENO='"+phoneNO+"' AND CU_MASTERASSOCIATIONCODE = '" +adminBO.getMasterAssociateCode()+ "'";

		}
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(query);
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{
				addBO = new Address();
				addBO.setAddressKey(rs.getString("CU_ADDRESS_KEY"));
				addBO.setNumber(rs.getString("CU_PHONENO"));
				addBO.setAdd1(rs.getString("CU_ADD1"));
				addBO.setAdd2(rs.getString("CU_ADD2"));
				addBO.setCity(rs.getString("CU_CITY"));
				addBO.setState(rs.getString("CU_STATE"));
				addBO.setZip(rs.getString("CU_ZIP"));
				addBO.setAddverify(rs.getInt("CU_ADDRESS_VERIFIED"));
			}
		} catch (SQLException sqex) {
			cat.error("TDSException AddressDAO.getUserAddress-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.getUserAddress-->"+sqex.getMessage());
			sqex.printStackTrace();			
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AddressDAO.getUserAddress End--->"+(System.currentTimeMillis()-startTime));
		return addBO;
	}

	public static CustomerProfile getUserAddressMaster(String phoneno,String name, AdminRegistrationBO adminBO,String masterKey,int value) {
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.getUserAddressMaster Start--->"+startTime);
		CustomerProfile customerProfileBO=null;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		String query;
		if(value==1){
			query="select * from TDS_USERADDRESSMASTER WHERE CU_MASTERASSOCIATIONCODE = '"+adminBO.getMasterAssociateCode()+"'AND CU_MASTER_KEY='"+masterKey+"'";
		}else{
			query = "select * from TDS_USERADDRESSMASTER WHERE CU_MASTERASSOCIATIONCODE = '" +adminBO.getMasterAssociateCode()+"'";
			if(!phoneno.equals("") && !name.equals("")){
				query = query + "and CU_PHONENO='"+phoneno+"'and CU_Name LIKE'"+name+"'";
			} else if (!phoneno.equals("") && name.equals("")){
				query = query + "and CU_PHONENO='"+phoneno+"'";					
			} else {
				query = query + " and CU_Name LIKE '"+name+"'";
			}

		}
		try {
			m_dbConn = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement(query);
			cat.info("query for usermaster:"+m_pst);
			ResultSet rs = m_pst.executeQuery();
			while(rs.next())
			{
				customerProfileBO = new CustomerProfile();
				customerProfileBO.setName(rs.getString("CU_NAME"));
				customerProfileBO.setMasterKey(rs.getString("CU_MASTER_KEY"));
				//customerProfileBO.setPaymentType(rs.getString("CU_PMTTYPE"));
				customerProfileBO.setAccount(rs.getString("CU_ACCOUNT"));
				customerProfileBO.setCClastfourdigits(rs.getString("CU_CCLASTFOURDIGIT"));
				customerProfileBO.setCCtype(rs.getString("CU_CCTYPE"));
				customerProfileBO.setLatitude(rs.getString("CU_LATITUDE"));
				customerProfileBO.setLongitude(rs.getString("CU_LONGITUDE"));
				//customerProfileBO.setDescription(rs.getString("CU_DESC"));
				customerProfileBO.setCorporateCode(rs.getString("CORPORATECODE"));
				customerProfileBO.setCustomerNo(rs.getString("CUSTOMERNO"));
				customerProfileBO.setNumber(rs.getString("CU_PHONENO"));
				customerProfileBO.setAdd1(rs.getString("CU_ADD1"));
				customerProfileBO.setAdd2(rs.getString("CU_ADD2"));
				customerProfileBO.setCity(rs.getString("CU_CITY"));
				customerProfileBO.setState(rs.getString("CU_STATE"));
				customerProfileBO.setZip(rs.getString("CU_ZIP"));
				customerProfileBO.setCCprofile(rs.getString("CU_CCPROFILE"));
				customerProfileBO.setPremiumcustomer(rs.getString("PREMIUMCUSTOMER"));
				customerProfileBO.setCCtype(rs.getString("CU_CCTYPE"));
				customerProfileBO.setComments(rs.getString("CU_COMMENTS"));
				customerProfileBO.setOperComments(rs.getString("CU_OPERATOR_COMMENTS"));
				customerProfileBO.setPaymentType(rs.getString("CU_PMTTYPE"));
				customerProfileBO.setDrProfile(rs.getString("CU_FLAG"));
				customerProfileBO.seteMail(rs.getString("CU_EMAIL"));
			}

		} catch (SQLException sqex) {
			cat.error("TDSException AddressDAO.getUserAddressMaster-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.getUserAddressMaster-->"+sqex.getMessage());
			sqex.printStackTrace();			
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AddressDAO.getUserAddressMaster End--->"+(System.currentTimeMillis()-startTime));
		return customerProfileBO;
	}

	public static int updateUserAddress(Address addBean,AdminRegistrationBO adminBO)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO. updateUserAddress Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null;
		int result =0;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement("UPDATE TDS_USERADDRESS SET CU_ADD1=?,CU_ADD2=?,CU_CITY=?,CU_STATE=?,CU_ZIP=?,CU_UPDATEDTIME=NOW(),CU_ZONE=?,CU_ADDRESS_VERIFIED=? WHERE CU_ADDRESS_KEY=?  AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'");
			pst.setString(1, addBean.getAdd1());
			pst.setString(2, addBean.getAdd2());
			pst.setString(3, addBean.getCity());
			pst.setString(4, addBean.getState());
			pst.setString(5, addBean.getZip());
			pst.setString(6, addBean.getQueueNo());
			pst.setInt(7,addBean.getAddverify());
			pst.setString(8,addBean.getAddressKey());
			result = pst.executeUpdate();
		} catch (Exception sqex){
			cat.error("TDSException AddressDAO.updateUserAddress-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AddressDAO.updateUserAddress-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		cat.info("AddressDAO. updateUserAddress End--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


  public static int updateUserAddressMaster(CustomerProfile cprofileBean,AdminRegistrationBO adminBO )
    {
	  long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.updateUserAddressMaster Start--->"+startTime);
        TDSConnection dbConn = null;
        Connection conn = null;
        PreparedStatement pst ,m_pst = null;
        int result =0;
        dbConn = new TDSConnection();
        conn = dbConn.getConnection();
        try {
            if((cprofileBean.getAccount()!=null && !cprofileBean.getAccount().equals("null")) && (!cprofileBean.getAccount().equals(""))){
                pst=conn.prepareStatement("SELECT * FROM TDS_GOVT_VOUCHER WHERE VC_VOUCHERNO='"+cprofileBean.getAccount()+"'");
                ResultSet rs = pst.executeQuery();
                if(rs.next()){
                    m_pst=conn.prepareStatement("UPDATE TDS_USERADDRESSMASTER SET CU_NAME=?,CU_ADD1=?,CU_ADD2=?,CU_CITY=?,CU_STATE=?,CU_ZIP=?,CU_COMMENTS=?,CU_OPERATOR_COMMENTS=?,CU_ACCOUNT=?,PREMIUMCUSTOMER=?,CU_FLAG=?,CU_EMAIL=? WHERE CU_MASTER_KEY=? AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'");
                    m_pst.setString(1, cprofileBean.getName());
                    m_pst.setString(2, cprofileBean.getAdd1());
                    m_pst.setString(3, cprofileBean.getAdd2());
                    m_pst.setString(4, cprofileBean.getCity());
                    m_pst.setString(5,cprofileBean.getState());
                    m_pst.setString(6,cprofileBean.getZip());
                    m_pst.setString(7,cprofileBean.getComments());
                    m_pst.setString(8,cprofileBean.getOperComments());
                    m_pst.setString(9,cprofileBean.getAccount());
                    m_pst.setString(10, cprofileBean.getPremiumcustomer());
                    m_pst.setString(11, cprofileBean.getDrProfile()+cprofileBean.getVecProfile());
                    m_pst.setString(12,cprofileBean.geteMail());
                    m_pst.setString(13,cprofileBean.getMasterKey());
                    result = m_pst.executeUpdate();
                }
            }else{
                m_pst=conn.prepareStatement("UPDATE TDS_USERADDRESSMASTER SET CU_NAME=?,CU_ADD1=?,CU_ADD2=?,CU_CITY=?,CU_STATE=?,CU_ZIP=?,CU_COMMENTS=?,CU_OPERATOR_COMMENTS=?,CU_ACCOUNT=?,PREMIUMCUSTOMER=?,CU_FLAG=?,CU_EMAIL=? WHERE CU_MASTER_KEY=? AND CU_MASTERASSOCIATIONCODE='"+adminBO.getMasterAssociateCode()+"'");
                m_pst.setString(1, cprofileBean.getName());
                m_pst.setString(2, cprofileBean.getAdd1());
                m_pst.setString(3, cprofileBean.getAdd2());
                m_pst.setString(4, cprofileBean.getCity());
                m_pst.setString(5,cprofileBean.getState());
                m_pst.setString(6,cprofileBean.getZip());
                m_pst.setString(7,cprofileBean.getComments());
                m_pst.setString(8,cprofileBean.getOperComments());
                m_pst.setString(9,cprofileBean.getAccount());
                m_pst.setString(10, cprofileBean.getPremiumcustomer());
                m_pst.setString(11, cprofileBean.getDrProfile()+cprofileBean.getVecProfile());
                m_pst.setString(12,cprofileBean.geteMail());
                m_pst.setString(13,cprofileBean.getMasterKey());
                result = m_pst.executeUpdate();
            }

        } catch (Exception sqex)
        {
            cat.error("TDSException AddressDAO.updateUserAddressMaster-->"+sqex.getMessage());
			cat.error(m_pst.toString());
            //System.out.println("TDSException AddressDAO.updateUserAddressMaster-->"+sqex.getMessage());
            sqex.printStackTrace();   
        } finally {
            dbConn.closeConnection();
        }
        cat.info("AddressDAO.updateUserAddressMaster End--->"+(System.currentTimeMillis()-startTime));
        return result;

    }



	public static int insertUserAddressMaster(OpenRequestBO openRequest,String dispComm,String driverComm,String Assoccode )
	{
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.insertUserAddressMaster Start--->"+startTime);
		TDSConnection dbConn = null;
		Connection conn = null;
		PreparedStatement pst = null ,m_pst = null;
		int result =0;
		int customerKey = 0;
		dbConn = new TDSConnection();
		conn = dbConn.getConnection();
		try {
			m_pst=conn.prepareStatement("INSERT INTO TDS_USERADDRESSMASTER  (CU_MASTERASSOCIATIONCODE, CU_PHONENO, CU_NAME,CU_ADD1,CU_ADD2,CU_CITY,CU_STATE,CU_ZIP,PREMIUMCUSTOMER,CU_ACCOUNT,CU_PMTTYPE,CU_COMMENTS,CU_OPERATOR_COMMENTS,CU_ADVANCE_TIME,CU_FLAG,CU_EMAIL,CU_LATITUDE,CU_LONGITUDE) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			m_pst.setString(1, Assoccode);
			m_pst.setString(2, openRequest.getPhone());
			m_pst.setString(3, openRequest.getName());
			m_pst.setString(4, openRequest.getSadd1());
			m_pst.setString(5, openRequest.getSadd2());
			m_pst.setString(6, openRequest.getScity());
			m_pst.setString(7, openRequest.getSstate());
			m_pst.setString(8, openRequest.getSzip());
			m_pst.setInt(9, 0);
			m_pst.setString(10, openRequest.getAcct());
			m_pst.setString(11, openRequest.getPaytype());
			m_pst.setString(12, driverComm);
			m_pst.setString(13, dispComm);
			m_pst.setString(14, openRequest.getAdvanceTime());
			m_pst.setString(15, openRequest.getDrProfile()+openRequest.getVecProfile());
			m_pst.setString(16, openRequest.getEmail());
			m_pst.setString(17, openRequest.getSlat());
			m_pst.setString(18, openRequest.getSlong());
			result = m_pst.executeUpdate();
			ResultSet rs = m_pst.getGeneratedKeys();
			if (rs.next()) {
				customerKey = rs.getInt(1);
			} if(openRequest.getLandMarkKey().equals("")) {
				pst = conn.prepareStatement("Insert TDS_USERADDRESS (CU_MASTER_KEY, CU_MASTERASSOCIATIONCODE, CU_PHONENO, CU_ADD1, CU_ADD2, CU_CITY, CU_STATE, CU_ZIP, CU_UPDATEDTIME, CU_LATITUDE, CU_LONGITUDE, CU_ADDRESS_VERIFIED, CU_Zone) values (?,?,?,?,?,?,?,?,now(),?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
				pst.setInt(1, customerKey);
				pst.setString(2, Assoccode);
				pst.setString(3, openRequest.getPhone());
				pst.setString(4, openRequest.getSadd1());
				pst.setString(5, openRequest.getSadd2());
				pst.setString(6, openRequest.getScity());
				pst.setString(7, openRequest.getSstate());
				pst.setString(8, openRequest.getSzip());
				pst.setDouble(9, Double.parseDouble(openRequest.getSlat()));
				pst.setDouble(10, Double.parseDouble(openRequest.getSlong()));
				pst.setInt(11, 0);
				pst.setString(12, openRequest.getZoneNumber());
				result = pst.executeUpdate();
			} else {
				pst = conn.prepareStatement("INSERT INTO TDS_USERLANDMARK (CUL_MASTERKEY, CUL_ASSOCIATIONCODE, CUL_PHONE,CUL_LANDMARK_KEY,CUL_NAME) values (?,?,?,?,?)");
				pst.setInt(1, customerKey);
				pst.setString(2, Assoccode);
				pst.setString(3, openRequest.getPhone());
				pst.setString(4, openRequest.getLandMarkKey());
				pst.setString(5, openRequest.getName());
				result=pst.executeUpdate();
			}
			pst = conn.prepareStatement("INSERT INTO TDS_USER_PHONE (UP_ASSOCIATION_CODE, UP_PHONE_NUMBER, UP_CUSTOMER_NUMBER) values (?,?,?)");
			pst.setString(1, Assoccode);
			pst.setString(2, openRequest.getPhone());
			pst.setInt(3, customerKey);
			result=pst.executeUpdate();
			
		} catch (Exception sqex)
		{
			cat.error("TDSException AddressDAO.insertUserAddressMaster-->"+sqex.getMessage());
			//cat.error(pst.toString());
			//System.out.println("TDSException AddressDAO.insertUserAddressMaster-->"+sqex.getMessage());
			sqex.printStackTrace();		
		} finally {
			dbConn.closeConnection();
		}
		cat.info("AddressDAO.insertUserAddressMaster End--->"+(System.currentTimeMillis()-startTime));
		return result;

	}

	public static ArrayList<OpenRequestBO> getPreviousLM(String phoneno, String associationCode)
	{
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.getPreviousLM Start--->"+startTime);
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null,m_pst1 = null;
		ResultSet rs,rs1;
		ArrayList al_list=new ArrayList();
		m_dbConn = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("SELECT * FROM TDS_USERLANDMARK LEFT JOIN TDS_LANDMARK ON CUL_LANDMARK_KEY=LM_KEY WHERE CUL_ASSOCIATIONCODE='"+associationCode+"' AND CUL_PHONE='"+phoneno+"' AND LM_NAME IS NOT NULL");
			rs=m_pst.executeQuery();
			while(rs.next()){
				OpenRequestBO orBO= new OpenRequestBO();
				orBO.setLandMarkKey(rs.getString("LM_KEY"));
				orBO.setSadd1(rs.getString("LM_ADD1"));
				orBO.setSadd2(rs.getString("LM_ADD2"));
				orBO.setScity(rs.getString("LM_CITY"));
				orBO.setSstate(rs.getString("LM_STATE"));
				orBO.setSzip(rs.getString("LM_ZIP"));
				orBO.setSlat(rs.getString("LM_LATITUDE"));
				orBO.setSlong(rs.getString("LM_LONGITUDE"));
				orBO.setSlandmark(rs.getString("LM_NAME"));
				orBO.setComments(rs.getString("LM_COMMENTS"));
				al_list.add(orBO);
			}
		}catch (Exception sqex)
		{
			cat.error("TDSException AddressDAO.getPreviousLM-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException AddressDAO.getPreviousLM-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}
		cat.info("AddressDAO.getPreviousLM End--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	//public static OpenRequestBO getAddressByMasterKey(AdminRegistrationBO adminBO,String masterKey){
	//	TDSConnection dbcon = null;
	//Connection con = null;
	//	PreparedStatement pst = null;
	//	ResultSet rs = null;
	//	dbcon = new TDSConnection();
	//	con = dbcon.getConnection();
	//OpenRequestBO Address= new OpenRequestBO();
	//	try {
		//	pst = con.prepareStatement("SELECT CU_ADD1,CU_PHONENO,CU_NAME,CU_ADD2,CU_CITY,CU_STATE,CU_ZIP,CU_ACCOUNT,CU_PMTTYPE,CU_LATITUDE,CU_LONGITUDE,CU_MASTER_KEY FROM TDS_USERADDRESSMASTER WHERE CU_MASTERASSOCIATIONCODE=? AND CU_MASTER_KEY=?");
		//	pst.setString(1, adminBO.getMasterAssociateCode());
		//	pst.setString(2,masterKey);
		//	rs=pst.executeQuery();
		//	if(rs.next()){
		//		Address.setPhone(rs.getString("CU_PHONENO"));
		//		Address.setName(rs.getString("CU_NAME"));
		//		Address.setSadd1(rs.getString("CU_ADD1"));
		//		Address.setSadd2(rs.getString("CU_ADD2"));
		//		Address.setScity(rs.getString("CU_CITY"));
		//		Address.setSstate(rs.getString("CU_STATE"));
		//		Address.setSzip(rs.getString("CU_ZIP"));
		//		Address.setPaytype(rs.getString("CU_PMTTYPE"));
		//		Address.setAcct(rs.getString("CU_ACCOUNT"));
		//		Address.setSlat(rs.getString("CU_LATITUDE"));
		//		Address.setSlong(rs.getString("CU_LONGITUDE"));
		//		Address.setMasterAddressKey(rs.getString("CU_MASTER_KEY"));
		//	}
		//}catch(SQLException sqex) {
		//	cat.error("TDSException AddressDAO.getAddressByMasterKey-->"+sqex.getMessage());
		//	cat.error(pst.toString());
		//	System.out.println("TDSException AddressDAO.getAddressByMasterKey-->"+sqex.getMessage());
		//	sqex.printStackTrace();
		//} finally {
		//	dbcon.closeConnection();
		//}
		//return Address;
	//}
	
	
	public static void updateUserAddressMasterComments(String comments,String phone,String assoccode,int vip,int type){
		long startTime = System.currentTimeMillis();
		cat.info("AddressDAO.updateUserAddressMasterComments Start--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="";
		if(type==1){
			query=" SET CU_COMMENTS='"+comments+"'";
		} else if(type==2){
			query=" SET CU_OPERATOR_COMMENTS='"+comments+"'";
		}else if(type==4){
			query=" SET CU_EMAIL='"+comments+"'";
		} else {
			query=" SET PREMIUMCUSTOMER='"+vip+"'";
		}
		try {
			pst = con.prepareStatement("UPDATE TDS_USERADDRESSMASTER"+query+" WHERE CU_PHONENO='"+phone+"' AND CU_MASTERASSOCIATIONCODE='"+assoccode+"'");
			pst.execute();
		}catch(SQLException sqex) {
			cat.error("TDSException AddressDAO.updateUserAddressMasterComments-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException AddressDAO.updateUserAddressMasterComments-->"+sqex.getMessage());
			//System.out.println("TDSException AddressDAO.updateUserAddressMasterComments-->"+query);
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info("AddressDAO.updateUserAddressMasterComments End--->"+(System.currentTimeMillis()-startTime));
	}




}

