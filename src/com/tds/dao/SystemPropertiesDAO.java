package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Category;

import com.tds.cmp.bean.CompanySystemProperties;
import com.tds.cmp.bean.MeterType;
import com.tds.cmp.bean.OperatorShift;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdjacentZonesBO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.DispatchPropertiesBO;
import com.tds.tdsBO.DocumentBO;
import com.tds.tdsBO.FleetBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.OpenRequestFieldOrderBO;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.tds.tdsBO.WrapperBO;
import com.tds.util.TDSSQLConstants;
public class SystemPropertiesDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(SystemPropertiesDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+SystemPropertiesDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static CompanySystemProperties getCompanySystemPropeties(String associationCode)  {

		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		CompanySystemProperties comSysProBO = new CompanySystemProperties();

		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement(TDSSQLConstants.getCompanySystem);

			pst.setString(1,associationCode);
			cat.info(pst.toString());
			//System.out.println("Exectued sql " + pst);
			rs = pst.executeQuery();
			while(rs.next()) {
				//System.out.println("VenkiDAO" +rs.getString("CC_CONSTANT_NAME"));
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Preline1")){
					comSysProBO.setPreline1(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Preline2")){
					comSysProBO.setPreline2(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Preline3")){
					comSysProBO.setPreline3(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Preline4")){
					comSysProBO.setPreline4(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Preline5")){
					comSysProBO.setPreline5(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Postline1")){
					comSysProBO.setPostline1(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Postline2")){
					comSysProBO.setPostline2(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Postline3")){
					comSysProBO.setPostline3(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Postline4")){
					comSysProBO.setPostline4(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Postline5")){
					comSysProBO.setPostline5(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Postline6")){
					comSysProBO.setPostline6(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Postline7")){
					comSysProBO.setPostline7(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} else if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("TimeOutOperator")){
					comSysProBO.setTimeOutOperator(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("State")){
					comSysProBO.setState(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ORAddress")){
					comSysProBO.setAddressFill((rs.getInt("CC_CONSTANT_VALUE")));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("fareByDistance")){
					comSysProBO.setDistanceValueToFare((rs.getInt("CC_CONSTANT_VALUE")));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("TimeZone")){
					double time=(Double.parseDouble(rs.getString("CC_CONSTANT_VALUE")));
					int hours=(int) (time*60)/60;
					int mins=(int) (time*60)%60;
					comSysProBO.setTimezone(time);
					if(mins<0){
						comSysProBO.setTimezoneMin(-(double) mins);
					}else{
						comSysProBO.setTimezoneMin((double) mins);
					}
					continue;
				}

				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("TimeOutDriver")){
					comSysProBO.setTimeOutDriver(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("country")){
					comSysProBO.setCountry(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("DefaultLanguage")){
					comSysProBO.setDefaultLanguage(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("DispatchBasedOnVehicleOrDriver")){
					comSysProBO.setDispatchBasedOnDriverOrVehicle(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("AllowCreditCardreturn")){
					comSysProBO.setAllowCreditCardReturns(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				}
				//				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("DSR")){
				//					comSysProBO.setDSR(rs.getString("CC_CONSTANT_VALUE"));
				//					continue;
				//				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("CvvCodeManadatory")){
					comSysProBO.setCvvCodeManadatory(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("CheckCabNoOnLogin")){
					comSysProBO.setCheckCabNoOnLogin(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				} if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("providePhoneNum")){
					comSysProBO.setProvidePhoneNum(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("endAddress")){
					comSysProBO.setProvideEndAddress(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("driverAlarm")){
					comSysProBO.setDriveralarm(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ORTime")){
					comSysProBO.setORTime(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("CallerId")){ 	
					comSysProBO.setCallerId(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("CallerIdType")){ 	
					comSysProBO.setCallerIdType(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ORFormat")){ 	
					comSysProBO.setORFormat(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ratePerMile")){ 	
					comSysProBO.setRatePerMile(rs.getDouble("CC_CONSTANT_VALUE"));
				} if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("calculateZones")){
					comSysProBO.setCalculateZones((rs.getBoolean("CC_CONSTANT_VALUE")));
				} if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("checkDocuments")){
					comSysProBO.setCheckDocuments((rs.getInt("CC_CONSTANT_VALUE")));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("futurecounts")){
					comSysProBO.setFutureCounts((rs.getInt("CC_CONSTANT_VALUE")));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("Payment_Mandatory_For_Jobs")){
					comSysProBO.setPaymentMandatory((rs.getString("CC_CONSTANT_VALUE")));
				}

				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("totalDigitsForPh")){
					comSysProBO.setTotalDigitsForPh(rs.getString("CC_CONSTANT_VALUE")!=null?rs.getString("CC_CONSTANT_VALUE"):"");
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ratePerMinute")){
					comSysProBO.setRatePerMinute(rs.getDouble("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("minimumSpeed")){
					comSysProBO.setMinimumSpeed((rs.getString("CC_CONSTANT_VALUE")));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("startAmount")){
					comSysProBO.setStartAmount((rs.getString("CC_CONSTANT_VALUE")));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("simultaneousLogin")){
					comSysProBO.setSimultaneousLogin((rs.getInt("CC_CONSTANT_VALUE")));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("MobilePassword")){
					comSysProBO.setMobilePassword((rs.getString("CC_CONSTANT_VALUE")));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("MobileDriver")){
					comSysProBO.setDriverListMobile((rs.getInt("CC_CONSTANT_VALUE")));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("currencyPrefix")){
					comSysProBO.setCurrencyPrefix(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("distanceBasedOn")){ 	
					comSysProBO.setDistanceBasedOn(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("meterMandatory")){ 	
					comSysProBO.setMeterMandatory(rs.getBoolean("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("startMeterAuto")){ 	
					comSysProBO.setStartMeterAutomatically(rs.getBoolean("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("startMeterHidden")){ 	
					comSysProBO.setStartmeterHidden(rs.getBoolean("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("startMeterHidden")){ 	
					comSysProBO.setStartmeterHidden(rs.getBoolean("CC_CONSTANT_VALUE"));
				}
	            if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("windowTime")){ 	
					comSysProBO.setWindowTime(rs.getString("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("loginTimeLimit")){ 	
					comSysProBO.setLoginLimitTime(rs.getString("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("serviceAlert_NoProgress")){ 	
					comSysProBO.setNoProgress(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("serviceAlert_RemoteNoTrip")){ 	
					comSysProBO.setRemoteNoTrip(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("serviceAlert_MeterOn")){ 	
					comSysProBO.setMeterOn(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("serviceAlert_NoTripTooSoon")){ 	
					comSysProBO.setNoTripTooSoon(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("distance_NoProgress")){ 	
					comSysProBO.setDistanceNoProgress(rs.getDouble("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("distance_RemoteNoTrip")){ 	
					comSysProBO.setDistanceRemoteNoTrip(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("distance_MeterOn")){ 	
					comSysProBO.setDistanceMeterOn(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("distance_NoTripTooSoon")){ 	
					comSysProBO.setDistanceNoTripTooSoon(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("app_Rate_Per_Mile")){ 	
					comSysProBO.setAppRatePerMile(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("app_Rate_Per_Minute")){ 	
					comSysProBO.setAppRatePerMinute(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("app_Start_Rate")){ 	
					comSysProBO.setAppStartRate(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("timeZoneArea")){ 	
					comSysProBO.setTimeZoneArea(rs.getString("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("tagDeviceCab")){ 
					comSysProBO.setTagDeviceCab(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("zoneLogoutTime")){ 	
					comSysProBO.setZoneLogoutTime(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("numberOfMeters")){ 	
					comSysProBO.setMeterTypes(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("direction")){ 	
					comSysProBO.setDriverDirection(rs.getInt("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("onSiteDistance")){ 	
					comSysProBO.setOnsiteDist(rs.getDouble("CC_CONSTANT_VALUE"));
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("postingFrequency")){ 	
					comSysProBO.setFrequencyOfPosting(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("autoBooking")){ 	
					comSysProBO.setBookAutomatically(rs.getBoolean("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("driverBalance")){ 	
					comSysProBO.setCheckBalance(rs.getBoolean("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("driverBalanceAmount")){ 	
					comSysProBO.setDriverBalanceAmount(rs.getDouble("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("prefixPhone")){ 	
					comSysProBO.setMobilePrefix(rs.getString("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("prefixSMS")){ 	
					comSysProBO.setSmsPrefix(rs.getString("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("jobAction")){ 	
					comSysProBO.setJobAction(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("CCForCustomerApp")){ 	
					comSysProBO.setCcForCustomerApp(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("PaymentForCustApp")){ 	
					comSysProBO.setPaymentForCustomerApp(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ETA_Calculate")){
					comSysProBO.setETA_Calculate(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ccDefaultAmount")){ 	
					comSysProBO.setCcDefaultAmt(rs.getString("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("sharedRideBatch")){ 	
					comSysProBO.setBatchForSR(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("sendBroadcastJob")){ 	
					comSysProBO.setSendBCJ(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("seeZoneStatus")){ 	
					comSysProBO.setSeeZoneStatus(rs.getInt("CC_CONSTANT_VALUE"));
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("zonesStep")){
					comSysProBO.setZonesStepaway(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("no_show")){
					comSysProBO.setNo_show(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ns_Login")){
					comSysProBO.setZone_login(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("fleetDispatch")){
					comSysProBO.setFleetDispatchSwitch(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("storeHistory")){
					comSysProBO.setStoreGPSHistory(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("checkDriverMobile")){
					comSysProBO.setCheckDriverMobile(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("checkDrMobVersion")){
					comSysProBO.setCheckDr_Mob_Version(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("sendPushToCustomer")){
					comSysProBO.setSendPushToCustomer(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("staddress_flagtrip")){
					comSysProBO.setEnable_stAdd_flagTrip(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("power_mode")){
					comSysProBO.setPower_mode(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("enable_power_save")){
					comSysProBO.setEnable_power_save(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
			}	

			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException  SystemPropertiesDAO.getCompanySystemPropeties--> "+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException  SystemPropertiesDAO.getCompanySystemPropeties-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return comSysProBO;
	}


	public static CompanySystemProperties getCSPForCustomerAppCCProcess(String associationCode)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		CompanySystemProperties comSysProBO = new CompanySystemProperties();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			pst = con.prepareStatement("SELECT * FROM TDS_COMPANY_CONSTANTS WHERE CC_ASSOCCODE = ? ");

			pst.setString(1,associationCode);
			cat.info(pst.toString());
			//System.out.println("Exectued sql " + pst);
			rs = pst.executeQuery();
			while(rs.next()) {
				//System.out.println("VenkiDAO" +rs.getString("CC_CONSTANT_NAME"));
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("CCForCustomerApp")){ 	
					comSysProBO.setCcForCustomerApp(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ccDefaultAmount")){ 	
					comSysProBO.setCcDefaultAmt(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("prefixSMS")){ 	
					comSysProBO.setSmsPrefix(rs.getString("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("PaymentForCustApp")){
					comSysProBO.setPaymentForCustomerApp(rs.getString("CC_CONSTANT_VALUE")==null?0:rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("ETA_Calculate")){
					comSysProBO.setETA_Calculate(rs.getString("CC_CONSTANT_VALUE")==null?0:rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
				if(rs.getString("CC_CONSTANT_NAME").equalsIgnoreCase("sendPushToCustomer")){
					comSysProBO.setSendPushToCustomer(rs.getInt("CC_CONSTANT_VALUE"));
					continue;
				}
			}			

			rs.close();
			pst.close();
		} catch (SQLException sqex) {
			cat.error("TDSException  SystemPropertiesDAO.getCSPForCustomerAppCCProcess--> "+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException  SystemPropertiesDAO.getCSPForCustomerAppCCProcess-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return comSysProBO;
	}

	public static boolean insertCmpnySysPro(CompanySystemProperties cspBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst= null;

		boolean success = false;
		dbcon = new TDSConnection();
		csp_conn = dbcon.getConnection(); 

		try
		{
			csp_conn.setAutoCommit(false);

			String delQuery="DELETE FROM TDS_COMPANY_CONSTANTS WHERE CC_ASSOCCODE= ?";

			csp_pst = csp_conn.prepareStatement(delQuery);
			csp_pst.setString(1, cspBO.getAssociateCode());
			cat.info(csp_pst.toString());
			csp_pst.execute();	

			String query="INSERT INTO TDS_COMPANY_CONSTANTS(CC_ASSOCCODE,CC_CONSTANT_NAME,CC_CONSTANT_VALUE) VALUES (?,?,?)";

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Preline1"); 	
			csp_pst.setString(3, cspBO.getPreline1());		
			csp_pst.execute();	


			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Preline2"); 	
			csp_pst.setString(3, cspBO.getPreline2());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Preline3"); 	
			csp_pst.setString(3, cspBO.getPreline3());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Preline4"); 	
			csp_pst.setString(3, cspBO.getPreline4());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Preline5"); 	
			csp_pst.setString(3, cspBO.getPreline5());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Postline1"); 	
			csp_pst.setString(3, cspBO.getPostline1());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Postline2"); 	
			csp_pst.setString(3, cspBO.getPostline2());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Postline3"); 	
			csp_pst.setString(3, cspBO.getPostline3());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Postline4"); 	
			csp_pst.setString(3, cspBO.getPostline4());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Postline5"); 	
			csp_pst.setString(3, cspBO.getPostline5());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Postline6"); 	
			csp_pst.setString(3, cspBO.getPostline6());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Postline7"); 	
			csp_pst.setString(3, cspBO.getPostline7());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "TimeOutOperator"); 	
			csp_pst.setInt(3, cspBO.getTimeOutOperator());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "TimeOutDriver"); 	
			csp_pst.setInt(3, cspBO.getTimeOutDriver());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "CheckCabNoOnLogin"); 	
			csp_pst.setString(3, cspBO.getCheckCabNoOnLogin());		
			csp_pst.execute();	


			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "CvvCodeManadatory"); 	
			csp_pst.setString(3, cspBO.getCvvCodeManadatory());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "AllowCreditCardreturn"); 	
			csp_pst.setString(3, cspBO.getAllowCreditCardReturns());		
			csp_pst.execute();	

			//			csp_pst = csp_conn.prepareStatement(query);
			//			csp_pst.setString(1, cspBO.getAssociateCode());
			//			csp_pst.setString(2, "DSR"); 	
			//			csp_pst.setString(3, cspBO.getDSR());		
			//			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "TimeZone"); 	
			csp_pst.setDouble(3, cspBO.getTimezone());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "State"); 	
			csp_pst.setString(3, cspBO.getState());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "country"); 	
			csp_pst.setString(3, cspBO.getCountry());		
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "DefaultLanguage"); 	
			csp_pst.setString(3, cspBO.getDefaultLanguage());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "DispatchBasedOnVehicleOrDriver"); 	
			csp_pst.setInt(3, cspBO.getDispatchBasedOnDriverOrVehicle());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "CallerId"); 	
			csp_pst.setInt(3, cspBO.getCallerId());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "direction"); 	
			csp_pst.setInt(3, cspBO.getDriverDirection());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "CallerIdType"); 	
			csp_pst.setInt(3, cspBO.getCallerIdType());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "providePhoneNum"); 	
			csp_pst.setInt(3, cspBO.getProvidePhoneNum());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "endAddress"); 	
			csp_pst.setInt(3, cspBO.getProvideEndAddress());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "driverAlarm"); 	
			csp_pst.setInt(3, cspBO.getDriveralarm());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "ORTime"); 	
			csp_pst.setString(3, cspBO.getORTime());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "ORFormat"); 	
			csp_pst.setInt(3, cspBO.getORFormat());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "ratePerMile"); 	
			csp_pst.setDouble(3, cspBO.getRatePerMile());		
			csp_pst.execute();


			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "calculateZones"); 	
			csp_pst.setBoolean(3, cspBO.getCalculateZones());	
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "checkDocuments"); 	
			csp_pst.setInt(3, cspBO.getCheckDocuments());	
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "futurecounts"); 	
			csp_pst.setInt(3, cspBO.getFutureCounts());	
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "Payment_Mandatory_For_Jobs");
			csp_pst.setString(3, cspBO.getPaymentMandatory());	
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "totalDigitsForPh"); 	
			csp_pst.setString(3, cspBO.getTotalDigitsForPh());	
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "currencyPrefix"); 	
			csp_pst.setInt(3, cspBO.getCurrencyPrefix());	
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "minimumSpeed"); 	
			csp_pst.setString(3, cspBO.getMinimumSpeed());	
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "ratePerMinute"); 	
			csp_pst.setDouble(3, cspBO.getRatePerMinute());	
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "startAmount"); 	
			csp_pst.setString(3, cspBO.getStartAmount());	
			csp_pst.execute();	


			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "simultaneousLogin"); 	
			csp_pst.setInt(3, cspBO.getSimultaneousLogin());	
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "ORAddress"); 	
			csp_pst.setInt(3, cspBO.getAddressFill());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "MobilePassword"); 	
			csp_pst.setString(3, cspBO.getMobilePassword());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "MobileDriver"); 	
			csp_pst.setInt(3, cspBO.getDriverListMobile());		
			csp_pst.execute();	

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "distanceBasedOn"); 	
			csp_pst.setInt(3, cspBO.getDistanceBasedOn());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "meterMandatory"); 	
			csp_pst.setBoolean(3, cspBO.isMeterMandatory());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "startMeterAuto"); 	
			csp_pst.setBoolean(3, cspBO.isStartMeterAutomatically());		
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "startMeterHidden"); 	
			csp_pst.setBoolean(3, cspBO.isStartmeterHidden());		
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "startMeterHidden"); 	
			csp_pst.setBoolean(3, cspBO.isStartmeterHidden());		
			csp_pst.execute();
		
			
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "loginTimeLimit"); 	
			csp_pst.setString(3, cspBO.getLoginLimitTime());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "windowTime"); 	
			csp_pst.setString(3, cspBO.getWindowTime());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "serviceAlert_NoProgress"); 	
			csp_pst.setInt(3, cspBO.getNoProgress());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "serviceAlert_MeterOn"); 	
			csp_pst.setInt(3, cspBO.getMeterOn());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "serviceAlert_RemoteNoTrip"); 	
			csp_pst.setInt(3, cspBO.getRemoteNoTrip());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "serviceAlert_NoTripTooSoon"); 	
			csp_pst.setInt(3, cspBO.getNoTripTooSoon());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "distance_NoProgress"); 	
			csp_pst.setDouble(3, cspBO.getDistanceNoProgress());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "distance_MeterOn"); 	
			csp_pst.setDouble(3, cspBO.getDistanceMeterOn());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "distance_RemoteNoTrip"); 	
			csp_pst.setDouble(3, cspBO.getDistanceRemoteNoTrip());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "distance_NoTripTooSoon"); 	
			csp_pst.setDouble(3, cspBO.getDistanceNoTripTooSoon());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "app_Rate_Per_Mile"); 	
			csp_pst.setDouble(3, cspBO.getAppRatePerMile());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "app_Rate_Per_Minute"); 	
			csp_pst.setDouble(3, cspBO.getAppRatePerMinute());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "app_Start_Rate"); 	
			csp_pst.setDouble(3, cspBO.getAppStartRate());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "timeZoneArea"); 	
			csp_pst.setString(3, cspBO.getTimeZoneArea());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "tagDeviceCab"); 	
			csp_pst.setInt(3, cspBO.getTagDeviceCab());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "fareByDistance"); 	
			csp_pst.setInt(3, cspBO.getDistanceValueToFare());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "prefixPhone"); 	
			csp_pst.setString(3, cspBO.getMobilePrefix());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "prefixSMS"); 	
			csp_pst.setString(3, cspBO.getSmsPrefix());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "zoneLogoutTime"); 	
			csp_pst.setInt(3, cspBO.getZoneLogoutTime());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "numberOfMeters"); 	
			csp_pst.setInt(3, cspBO.getMeterTypes());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "onSiteDistance"); 	
			csp_pst.setDouble(3, cspBO.getOnsiteDist());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "postingFrequency"); 	
			csp_pst.setInt(3, cspBO.getFrequencyOfPosting());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "autoBooking"); 	
			csp_pst.setBoolean(3, cspBO.isBookAutomatically());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "driverBalance"); 	
			csp_pst.setBoolean(3, cspBO.isCheckBalance());		
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "driverBalanceAmount"); 	
			csp_pst.setDouble(3, cspBO.getDriverBalanceAmount());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "jobAction"); 	
			csp_pst.setInt(3, cspBO.getJobAction());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "CCForCustomerApp"); 	
			csp_pst.setInt(3, cspBO.getCcForCustomerApp());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "PaymentForCustApp"); 	
			csp_pst.setInt(3, cspBO.getPaymentForCustomerApp());		
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "ETA_Calculate"); 	
			csp_pst.setInt(3, cspBO.getETA_Calculate());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "ccDefaultAmount"); 	
			csp_pst.setString(3, cspBO.getCcDefaultAmt());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "sharedRideBatch"); 	
			csp_pst.setInt(3, cspBO.getBatchForSR());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "sendBroadcastJob"); 	
			csp_pst.setInt(3, cspBO.getSendBCJ());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "zonesStep"); 	
			csp_pst.setInt(3, cspBO.getZonesStepaway());		
			csp_pst.execute();

		
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "seeZoneStatus"); 	
			csp_pst.setInt(3, cspBO.getSeeZoneStatus());		
			csp_pst.execute();

			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "no_show"); 	
			csp_pst.setInt(3, cspBO.getNo_show());		
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "ns_Login");
			csp_pst.setInt(3, cspBO.getZone_login());
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "fleetDispatch");
			csp_pst.setInt(3, cspBO.getFleetDispatchSwitch());
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "storeHistory");
			csp_pst.setInt(3, cspBO.getStoreGPSHistory());
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "checkDriverMobile");
			csp_pst.setInt(3, cspBO.getCheckDriverMobile());
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "checkDrMobVersion");
			csp_pst.setInt(3, cspBO.getCheckDr_Mob_Version());
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "sendPushToCustomer");
			csp_pst.setInt(3, cspBO.getSendPushToCustomer());
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "staddress_flagtrip");
			csp_pst.setInt(3, cspBO.getEnable_stAdd_flagTrip());
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "power_mode");
			csp_pst.setInt(3, cspBO.getPower_mode());
			csp_pst.execute();
			
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, cspBO.getAssociateCode());
			csp_pst.setString(2, "enable_power_save");
			csp_pst.setInt(3, cspBO.getEnable_power_save());
			csp_pst.execute();
			
			success = true;
			csp_conn.setAutoCommit(true);

		} catch (Exception e){
			//System.out.println(e.toString());
			cat.error(csp_pst.toString());
			//		System.out.println("TDSException SharedRideDAO.insertSharedRide-->"+ sqex.getMessage());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return success;
	}

	public static String getParameter(String associationCode, String name){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String query="SELECT CC_CONSTANT_VALUE FROM TDS_COMPANY_CONSTANTS WHERE CC_ASSOCCODE = ? AND CC_CONSTANT_NAME = ?";

		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst= null;
		ResultSet rs = null;
		String returnValue = "";
		try
		{
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection(); 
			csp_pst = csp_conn.prepareStatement(query);
			csp_pst.setString(1, associationCode);
			csp_pst.setString(2, name); 
			//System.out.println(csp_pst.toString());

			//System.out.println(csp_pst);
			rs = csp_pst.executeQuery();	

			while(rs.next()) {
				returnValue =  rs.getString("CC_CONSTANT_VALUE");
			}
		} catch (Exception e){
			//System.out.println(e.toString());
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return returnValue;

	}		

	public static boolean editQueueCoordinates(QueueCoordinatesBO p_queueBO,int p_status,String Zone ) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean m_result = false;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs;
		int version=1;
		try {
			m_tdsConn = new TDSConnection();
			m_conn = m_tdsConn.getConnection();
			if(p_status == 0) {
				m_pst = m_conn.prepareStatement(TDSSQLConstants.insertQueueCoordinates);
				m_pst.setString(1, p_queueBO.getQDelayTime());
				m_pst.setString(2, p_queueBO.getSmsResponseTime());
				m_pst.setString(3, p_queueBO.getQDescription());
				m_pst.setString(4, p_queueBO.getQLoginSwitch());
				m_pst.setString(5, p_queueBO.getQDispatchType());
				m_pst.setString(6, p_queueBO.getQTypeofQueue());
				if(p_queueBO.getQBroadcastDelay().equals("")){
					m_pst.setNull(7,  java.sql.Types.INTEGER);
				}else{
					m_pst.setString(7, p_queueBO.getQBroadcastDelay());
				}
				m_pst.setString(8, p_queueBO.getQStartTime());
				m_pst.setString(9, p_queueBO.getQEndTime());
				m_pst.setString(10, p_queueBO.getQDayofWeek());
				m_pst.setString(11, p_queueBO.getQDispatchDistance());
				m_pst.setString(12, p_queueBO.getQueueId());
				m_pst.setString(13, p_queueBO.getQAssocCode());
				m_pst.setString(14, p_queueBO.getQRequestBefore());
				m_pst.setString(15, p_queueBO.getPhoneCallTime());
				m_pst.setString(16,p_queueBO.getAdjacentZoneProperties());
				m_pst.setInt(17,p_queueBO.getStaleCallTime());
				m_pst.setInt(18,p_queueBO.getStaleCallbasedOn());
				m_pst.setString(19, p_queueBO.getMaxDistance());
				m_pst.setString(20, p_queueBO.getZnprofile());
				m_pst.setInt(21, p_queueBO.getQBdSwitch());

				cat.info("Query "+m_pst);
				if(m_pst.executeUpdate() > 0) {
					m_result = true;
				} 


			} else {
				m_pst = m_conn.prepareStatement("SELECT QD_VERSION FROM TDS_QUEUE_DETAILS WHERE QD_QUEUENAME='"+Zone+"' AND QD_ASSOCCODE='"+p_queueBO.getQAssocCode()+"'");
				rs=m_pst.executeQuery();
				if(rs.next()){
					version=rs.getInt("QD_VERSION");
				}
				version=version+1;
				m_pst = m_conn.prepareStatement(TDSSQLConstants.updateQueueCoordinates);
				m_pst.setString(1, p_queueBO.getQueueId());
				m_pst.setString(2, p_queueBO.getQDelayTime());
				m_pst.setString(3, p_queueBO.getSmsResponseTime());
				m_pst.setString(4, p_queueBO.getQDescription());
				m_pst.setString(5, p_queueBO.getQLoginSwitch());
				m_pst.setString(6, p_queueBO.getQDispatchType());
				m_pst.setString(7, p_queueBO.getQTypeofQueue());
				if(p_queueBO.getQBroadcastDelay().equals("")){
					m_pst.setNull(8,  java.sql.Types.INTEGER);

				}else{
					m_pst.setString(8, p_queueBO.getQBroadcastDelay());
				}
				m_pst.setString(9, p_queueBO.getQStartTime());
				m_pst.setString(10, p_queueBO.getQEndTime());
				m_pst.setString(11, p_queueBO.getQDayofWeek());
				m_pst.setString(12, p_queueBO.getQDispatchDistance());
				m_pst.setString(13, p_queueBO.getQRequestBefore());
				m_pst.setInt(14, version);
				m_pst.setString(15, p_queueBO.getPhoneCallTime());
				m_pst.setString(16, p_queueBO.getAdjacentZoneProperties());
				m_pst.setInt(17, p_queueBO.getStaleCallTime());
				m_pst.setInt(18, p_queueBO.getStaleCallbasedOn());
				m_pst.setString(19, p_queueBO.getMaxDistance());
				m_pst.setString(20, p_queueBO.getZnprofile());
				m_pst.setInt(21, p_queueBO.getQBdSwitch());
				m_pst.setString(22, Zone);
				m_pst.setString(23, p_queueBO.getQAssocCode());

				cat.info("Query "+m_pst);
				if(m_pst.executeUpdate() > 0) {
					m_result = true;
				}
				//System.out.println("Query2"+m_pst);
				m_pst = m_conn.prepareStatement("UPDATE TDS_QUEUE_BOUNDRIES SET QB_QUEUENAME=? WHERE QB_ASSOCCODE=? AND QB_QUEUENAME=?");
				m_pst.setString(1, p_queueBO.getQueueId());
				m_pst.setString(2, p_queueBO.getQAssocCode());
				m_pst.setString(3, Zone);
				cat.info("Query "+m_pst);

				m_pst.execute();

				m_pst = m_conn.prepareStatement("UPDATE TDS_QUEUE_BOUNDRIES SET QB_QUEUENAME=? WHERE QB_ASSOCCODE=? AND QB_QUEUENAME=?");
				m_pst.setString(1, p_queueBO.getQueueId());
				m_pst.setString(2, p_queueBO.getQAssocCode());
				m_pst.setString(3, Zone);
				cat.info("Query "+m_pst);

				m_pst.execute();

				m_result = true;
			}

		} catch (SQLException sqex) {
			cat.error("TDSException SystemPropertiesDAO.editQueueCoordinates-->"+sqex.getMessage());
			//System.out.println("TDSException SystemPropertiesDAO.editQueueCoordinates-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_result;
	}

	public static List getQueueCoOrdinateList(String p_queueId, String p_assocCode, String p_qDescription ) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		List m_queueList = null;
		TDSConnection m_tdsConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		QueueCoordinatesBO m_queueBO;
		ResultSet m_rs = null;
		String m_query = "";
		if(!p_queueId.equals("")) {
			m_query = " AND QD_QUEUENAME LIKE '"+p_queueId+"'";
		}  
		if(!p_qDescription.equals("")) {
			m_query = m_query + " AND QD_DESCRIPTION LIKE '"+p_qDescription +"'";
		}
		m_tdsConn = new TDSConnection();
		m_conn = m_tdsConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement(TDSSQLConstants.getQueueCoordinate + m_query+" ORDER BY ABS(QD_QUEUENAME)");
			m_pst.setString(1, p_assocCode);
			cat.info("Query "+m_pst);

			m_rs = m_pst.executeQuery();
			if(m_rs.first()) {
				m_queueList = new ArrayList();
				do {
					m_queueBO = new QueueCoordinatesBO();
					m_queueBO.setQDelayTime(m_rs.getString("QD_DELAYTIME"));
					m_queueBO.setSmsResponseTime(m_rs.getString("QD_SMS_RES_TIME"));
					m_queueBO.setQDescription(m_rs.getString("QD_DESCRIPTION"));
					m_queueBO.setQueueId(m_rs.getString("QD_QUEUENAME"));
					m_queueBO.setQLoginSwitch(m_rs.getString("QD_LOGIN_SWITCH"));
					m_queueBO.setQDispatchType(m_rs.getString("QD_DISPATCH_TYPE"));
					m_queueBO.setQTypeofQueue(m_rs.getString("QD_VIRTUAL_ACTUAL"));
					m_queueBO.setQBroadcastDelay(m_rs.getString("QD_BROADCAST_DELAY"));
					m_queueBO.setQStartTime(m_rs.getString("QD_START_TIME"));
					m_queueBO.setQEndTime(m_rs.getString("QD_END_TIME"));
					m_queueBO.setQDayofWeek(m_rs.getString("QD_DAY_OF_WEEK"));
					m_queueBO.setQDispatchDistance(m_rs.getString("QD_DISPATCH_DISTANCE"));
					m_queueBO.setQRequestBefore(m_rs.getString("QD_REQUEST_BEFORE"));
					m_queueBO.setVersionNumber(m_rs.getInt("QD_VERSION"));
					m_queueBO.setPhoneCallTime(m_rs.getString("QD_PHONE_CALL_WAIT_TIME"));
					m_queueBO.setAdjacentZoneProperties(m_rs.getString("QD_ADJACENT_ZONE_SORT_TYPE"));
					m_queueBO.setStaleCallTime(m_rs.getInt("QD_STALE_CALL_TIME"));
					m_queueBO.setStaleCallbasedOn(m_rs.getInt("QD_STALE_CALL_BASED_ON"));
					m_queueBO.setMaxDistance(m_rs.getString("QD_MAX_DISTANCE"));
					m_queueBO.setZnprofile(m_rs.getString("QD_FLAG"));
					m_queueBO.setQBdSwitch(m_rs.getInt("QD_BDSWITCH"));
					m_queueList.add(m_queueBO);
				} while (m_rs.next());
			}
		}  catch (SQLException p_sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.editQueueCoordinates--->"+p_sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.editQueueCoordinates--->"+p_sqex.getMessage());
			p_sqex.getStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return m_queueList;

	}
	public static int deleteZone(String zoneNumber, String associationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		int x=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST WHERE OR_QUEUENO='"+zoneNumber+"' AND OR_ASSOCCODE='"+associationCode+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				x=0;
			} else {
				pst = con.prepareStatement("DELETE FROM TDS_QUEUE_DETAILS WHERE QD_QUEUENAME= ? AND QD_ASSOCCODE= ? ");	
				pst.setString(1,zoneNumber);
				pst.setString(2,associationCode);
				pst.execute();
				pst = con.prepareStatement("DELETE FROM TDS_QUEUE_BOUNDRIES WHERE QB_QUEUENAME= ? AND QB_ASSOCCODE= ? ");	
				pst.setString(1,zoneNumber);
				pst.setString(2,associationCode);
				pst.execute();
				pst = con.prepareStatement("DELETE FROM TDS_ADJACENT_ZONES WHERE AZ_ZONE_NUMBER= ? AND AZ_ASSOCCODE= ? ");	
				pst.setString(1,zoneNumber);
				pst.setString(2,associationCode);
				pst.execute();
				x=1;
			}
		} catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.deleteZone--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.deleteZone--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return x;
	}	

	public static int insertAdjacentZones(String[] zoneNumber,String[] adjacentZones,String[] orderNumber,int size,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement m_pst,m_pst1=null;
		ResultSet rs,rs1= null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(zoneNumber[1]!=""){
				con.setAutoCommit(false);
				pst= con.prepareStatement("DELETE FROM TDS_ADJACENT_ZONES WHERE AZ_ZONE_NUMBER='"+zoneNumber[1]+"' AND AZ_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"'");
				pst.execute();
				for(int i=1;i<=size;i++){
					m_pst=con.prepareStatement("SELECT * FROM TDS_QUEUE_DETAILS where QD_QUEUENAME=?");
					m_pst.setString(1,zoneNumber[i]);
					rs=m_pst.executeQuery();
					m_pst1=con.prepareStatement("SELECT * FROM TDS_QUEUE_DETAILS where QD_QUEUENAME=?");
					m_pst1.setString(1,adjacentZones[i]);
					rs1=m_pst1.executeQuery();
					if(rs.next()&& rs1.next()){
						pst= con.prepareStatement("INSERT INTO TDS_ADJACENT_ZONES(AZ_ASSOCCODE,AZ_ZONE_NUMBER,AZ_ADJACENT_ZONES,AZ_ORDER_NUMBER)VALUES(?,?,?,?) ");
						pst.setString(1, adminBO.getMasterAssociateCode());
						pst.setString(2,zoneNumber[i]);
						pst.setString(3,adjacentZones[i]);
						pst.setString(4,orderNumber[i]);
						pst.execute();
						result=1;
					}
					con.commit();
				}
				con.setAutoCommit(true);
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.insertAdjacentZones--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertAdjacentZones--->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<AdjacentZonesBO> ReviewAdjacentZones(AdjacentZonesBO zoneBO,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<AdjacentZonesBO> zone_list=new ArrayList<AdjacentZonesBO>();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try {
			String query="SELECT * FROM TDS_ADJACENT_ZONES WHERE AZ_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"'";
			if(zoneBO.getZoneNumber()!=null && zoneBO.getZoneNumber()!="" ){
				query=query+"AND AZ_ZONE_NUMBER='"+zoneBO.getZoneNumber()+"'";
			} if(zoneBO.getAdjacentZones()!=null && zoneBO.getAdjacentZones()!="" ){
				query=query+"AND AZ_ADJACENT_ZONES='"+zoneBO.getAdjacentZones()+"'";
			} if(zoneBO.getOrderNumber()!=null && zoneBO.getOrderNumber()!="" ){
				query=query+"AND AZ_ORDER_NUMBER='"+zoneBO.getOrderNumber()+"'";
			}
			query=query+"order by AZ_ZONE_NUMBER";
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				AdjacentZonesBO zoneBo=new AdjacentZonesBO();

				zoneBo.setAdjacentZones(rs.getString("AZ_ADJACENT_ZONES"));
				zoneBo.setOrderNumber(rs.getString("AZ_ORDER_NUMBER"));
				zoneBo.setZoneNumber(rs.getString("AZ_ZONE_NUMBER"));
				zoneBo.setSerialNo(rs.getInt("AZ_SERIAL_NUMBER"));
				zone_list.add(zoneBo);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.reviewAdjacentZones--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.reviewAdjacentZones--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return zone_list;
	}

	public static int deleteAdjacentZones(String zoneNum,String adjZone,String orderNum,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int delete=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst = con.prepareStatement("DELETE FROM TDS_ADJACENT_ZONES WHERE AZ_ZONE_NUMBER= ? AND AZ_ADJACENT_ZONES= ? AND AZ_ORDER_NUMBER=? AND AZ_ASSOCCODE=? ");	
			pst.setString(1,zoneNum);
			pst.setString(2,adjZone);
			pst.setString(3,orderNum);
			pst.setString(4,adminBO.getMasterAssociateCode());
			pst.execute();
			delete=1;
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.deleteAdjacentZones--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.deleteAdjacentZones--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return delete;
	}
	public static String getZoneName(String zoneNum,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		String zoneName="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst = con.prepareStatement("SELECT QD_DESCRIPTION FROM TDS_QUEUE_DETAILS WHERE QD_QUEUENAME='"+zoneNum+"' AND QD_ASSOCCODE='"+adminBO.getAssociateCode()+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				zoneName=rs.getString("QD_DESCRIPTION");
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getZoneName--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getZoneName--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return zoneName;

	}
	public static ArrayList<OpenRequestBO> getLandmarkPending(OpenRequestBO landmarkBo,AdminRegistrationBO adminBO,int value) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<OpenRequestBO> landmark_pending=new ArrayList<OpenRequestBO>();
		//OpenRequestBO landmarkBean = new OpenRequestBO();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		//String query="";
		String query="SELECT * FROM TDS_LANDMARK_PENDING WHERE LP_ASSCODE='"+adminBO.getAssociateCode()+"' ";
		if(landmarkBo.getSlandmark()!=null && landmarkBo.getSlandmark()!="" ){
			query=query+"AND LP_NAME like '%"+landmarkBo.getSlandmark()+"%' ";
		}
		if(landmarkBo.getPhone()!=null && landmarkBo.getPhone()!="" ){
			query=query+"AND LP_PHONE='"+landmarkBo.getPhone()+"' ";
		}
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestBO landMarkBo = new OpenRequestBO();
				landMarkBo.setPhone(rs.getString("LP_PHONE"));
				landMarkBo.setSlandmark(rs.getString("LP_NAME"));
				landMarkBo.setSadd1(rs.getString("LP_ADD1"));
				landMarkBo.setScity(rs.getString("LP_CITY"));
				landMarkBo.setSstate(rs.getString("LP_STATE"));
				landMarkBo.setSzip(rs.getString("LP_ZIP"));
				landMarkBo.setSlat(rs.getString("LP_LONGITUDE"));
				landMarkBo.setSlong(rs.getString("LP_LATITUDE"));
				landMarkBo.setZoneNumber(rs.getString("LP_ZONE"));
				landMarkBo.setSerialNo(rs.getInt("LP_SNO"));
				landmark_pending.add(landMarkBo);
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getLandmarkPending--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getLandmarkPending--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return landmark_pending;
	}

	public static void getLandmarkPendingDelete(AdminRegistrationBO adminBO,String sNo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst1=null;
		//ResultSet rs=null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			//String query="";
			String query1="";
			query1="DELETE FROM TDS_LANDMARK_PENDING where LP_ASSCODE='"+adminBO.getAssociateCode()+"' and LP_SNO='"+sNo+"'";
			pst1= con.prepareStatement(query1);
			pst1.execute();
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getLandmarkPendingDelete--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getLandmarkPendingDelete--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}
	public static OpenRequestBO getLandmarkPendingSearch(String sNO,AdminRegistrationBO adminBO,int value) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		//PreparedStatement pst1=null;
		ResultSet rs=null;
		OpenRequestBO landmarkBO = new OpenRequestBO();
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query="";
			//String query1="";
			if(value==0){
				query="SELECT * FROM TDS_LANDMARK_PENDING WHERE LP_ASSCODE='"+adminBO.getAssociateCode()+"'";
				if(sNO!=null){
					query=query+"AND LP_SNO='"+sNO+"'";
				}
				pst= con.prepareStatement(query);
				rs=pst.executeQuery();
				while(rs.next()){
					landmarkBO.setPhone(rs.getString("LP_PHONE"));
					landmarkBO.setSlandmark(rs.getString("LP_NAME"));
					landmarkBO.setSadd1(rs.getString("LP_ADD1"));
					landmarkBO.setSadd2(rs.getString("LP_ADD2"));
					landmarkBO.setScity(rs.getString("LP_CITY"));
					landmarkBO.setSstate(rs.getString("LP_STATE"));
					landmarkBO.setSzip(rs.getString("LP_ZIP"));
					landmarkBO.setZoneNumber(rs.getString("LP_ZONE"));
					landmarkBO.setSerialNo(rs.getInt("LP_SNO"));
					landmarkBO.setSlat(rs.getString("LP_LATITUDE"));
					landmarkBO.setSlong(rs.getString("LP_LONGITUDE"));
				}
			}
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getLandmarkPendingSearch-->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getLandmarkPendingSearch--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return landmarkBO;
	}
	public static int deleteAdjacentZones(String serialNum,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		//ResultSet rs=null;
		int delete=0;
		try{
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("DELETE FROM TDS_ADJACENT_ZONES WHERE AZ_SERIAL_NUMBER= ? AND AZ_ASSOCCODE=? ");	
			pst.setString(1,serialNum);
			pst.setString(2,adminBO.getAssociateCode());
			cat.info("query---->"+pst.toString());
			pst.execute();
			delete=1;
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.deleteAdjacentZones--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.deleteAdjacentZones--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return delete;
	}
	public static ArrayList<AdjacentZonesBO> reviewAdjacentZones(AdjacentZonesBO zoneBO,AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<AdjacentZonesBO>  zone_list=new ArrayList<AdjacentZonesBO> ();
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();

			String query="SELECT * FROM TDS_ADJACENT_ZONES WHERE AZ_ASSOCCODE='"+adminBO.getAssociateCode()+"' ";
			if(zoneBO.getZoneNumber()!=null && zoneBO.getZoneNumber()!="" ){
				query=query+"AND AZ_ZONE_NUMBER='"+zoneBO.getZoneNumber()+"' ";
			} if(zoneBO.getAdjacentZones()!=null && zoneBO.getAdjacentZones()!="" ){
				query=query+"AND AZ_ADJACENT_ZONES='"+zoneBO.getAdjacentZones()+"' ";
			} if(zoneBO.getOrderNumber()!=null && zoneBO.getOrderNumber()!="" ){
				query=query+"AND AZ_ORDER_NUMBER='"+zoneBO.getOrderNumber()+"' ";
			}
			query=query+"order by AZ_ZONE_NUMBER";
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				AdjacentZonesBO zoneBo=new AdjacentZonesBO();

				zoneBo.setAdjacentZones(rs.getString("AZ_ADJACENT_ZONES"));
				zoneBo.setOrderNumber(rs.getString("AZ_ORDER_NUMBER"));
				zoneBo.setZoneNumber(rs.getString("AZ_ZONE_NUMBER"));
				zoneBo.setSerialNo(rs.getInt("AZ_SERIAL_NUMBER"));
				zone_list.add(zoneBo);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.reviewAdjacentZones--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.reviewAdjacentZones--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return zone_list;
	}

	public static ArrayList<QueueCoordinatesBO> zoneDetails(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<QueueCoordinatesBO> zone_list=new ArrayList<QueueCoordinatesBO>();
		String query="SELECT * FROM TDS_QUEUE_DETAILS WHERE QD_ASSOCCODE='"+adminBO.getAssociateCode()+"'";
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				QueueCoordinatesBO zoneBo=new QueueCoordinatesBO();
				zoneBo.setQueueId(rs.getString("QD_QUEUENAME"));
				zoneBo.setQDescription(rs.getString("QD_DESCRIPTION"));
				zone_list.add(zoneBo);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.zoneDetails--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.zoneDetails--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return zone_list;
	}
	public static int insertWrapper(AdminRegistrationBO adminBO,WrapperBO wrapper,int mode,String driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		//ResultSet rs= null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(mode==1){
				pst= con.prepareStatement("INSERT INTO TDS_WRAPPER_XREF(WX_DRIVERID,WX_ASSOCIATION_CODE,WX_PASSWORD,WX_CABNO,WX_PHONE_NO, WX_SESSION_ID)VALUES(?,?,?,?,?,'') ");
				pst.setString(1, wrapper.getDriverId());
				pst.setString(2,adminBO.getAssociateCode());
				pst.setString(3,wrapper.getPassword());
				pst.setString(4,wrapper.getCabNumber());
				pst.setString(5,wrapper.getPhoneNumber());
			}else if(mode==2){
				pst= con.prepareStatement("UPDATE TDS_WRAPPER_XREF SET WX_DRIVERID=?,WX_PASSWORD=?,WX_CABNO=?,WX_PHONE_NO=? WHERE WX_DRIVERID='"+driverId+"' ");
				pst.setString(1, wrapper.getDriverId());
				pst.setString(2,wrapper.getPassword());
				pst.setString(3,wrapper.getCabNumber());
				pst.setString(4,wrapper.getPhoneNumber());
			}
			pst.execute();
			result=1;

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.insertWrapper--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertWrapper--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static ArrayList getWrapper(AdminRegistrationBO adminBO,WrapperBO wrapper) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<WrapperBO>getWrapper = new ArrayList<WrapperBO>();


		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs= null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query="SELECT * FROM TDS_WRAPPER_XREF WHERE WX_ASSOCIATION_CODE='"+adminBO.getAssociateCode()+"' ";
			if(wrapper.getDriverId()!=null && wrapper.getDriverId()!=""){
				query=query+"AND WX_DRIVERID='"+wrapper.getDriverId()+"'";
			}
			if(wrapper.getPhoneNumber()!=null && wrapper.getPhoneNumber()!=""){
				query=query+"AND WX_PHONE_NO='"+wrapper.getPhoneNumber()+"'";
			}
			if(wrapper.getCabNumber()!=null && wrapper.getCabNumber()!=""){

				query=query+"AND WX_CABNO='"+wrapper.getCabNumber()+"'";
			}
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while (rs.next()) {
				WrapperBO wrapperFromDB =new WrapperBO();
				wrapperFromDB.setDriverId(rs.getString("WX_DRIVERID"));
				wrapperFromDB.setCabNumber(rs.getString("WX_CABNO"));
				wrapperFromDB.setPassword(rs.getString("WX_PASSWORD"));
				wrapperFromDB.setPhoneNumber(rs.getString("WX_PHONE_NO"));
				getWrapper.add(wrapperFromDB);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getWrapper--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getWrapper--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return getWrapper;
	}

	public static WrapperBO getWrapperbyDriver(AdminRegistrationBO adminBO,String driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs= null;
		WrapperBO wrapper =new WrapperBO();

		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			String query="SELECT * FROM TDS_WRAPPER_XREF WHERE WX_ASSOCIATION_CODE='"+adminBO.getAssociateCode()+"'AND WX_DRIVERID='"+driverId+"' ";
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while (rs.next()) {
				wrapper.setDriverId(rs.getString("WX_DRIVERID"));
				wrapper.setCabNumber(rs.getString("WX_CABNO"));
				wrapper.setPassword(rs.getString("WX_PASSWORD"));
				wrapper.setPhoneNumber(rs.getString("WX_PHONE_NO"));
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getWrapperbyDriver--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getWrapperbyDriver--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return wrapper;
	}
	public static int deleteWrapperbyDriver(AdminRegistrationBO adminBO,String driverId) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst= con.prepareStatement("DELETE FROM TDS_WRAPPER_XREF WHERE WX_DRIVERID='"+driverId+"' AND WX_ASSOCIATION_CODE='"+adminBO.getAssociateCode()+"' ");
			pst.execute();
			result=1;

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.deleteWrapperbyDriver--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.deleteWrapperbyDriver--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int deleteBoundries(String zoneNumber, String associationCode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int x=0;
		//ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("DELETE FROM TDS_QUEUE_BOUNDRIES WHERE QB_QUEUENAME= ? AND QB_ASSOCCODE= ? ");	
			pst.setString(1,zoneNumber);
			pst.setString(2,associationCode);
			pst.execute();
			x=1;
		} catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.deleteZone--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.deleteZone--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return x;
	}
	/*public static int insertORFieldsPosition(OpenRequestFieldOrderBO orFieldOrderBO,AdminRegistrationBO adminBO,int vendor,String vendorName,String separatedBy,String dateFormat) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement m_pst=null;
		ResultSet rs= null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(vendor!=0 ){
				m_pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_POSITION where ORP_ASSOCCODE=? AND ORP_VENDOR=?");
				m_pst.setString(1,adminBO.getAssociateCode());
				m_pst.setInt(2,vendor);
				rs=m_pst.executeQuery();
				if(rs.next()){
					pst= con.prepareStatement("UPDATE TDS_OPENREQUEST_POSITION SET ORP_ASSOCCODE=?,ORP_TRIP_ID=?,ORP_PHONE=?,ORP_STADD1=?,ORP_STADD2=?,ORP_STCITY=?,ORP_EDADD1=?,ORP_EDADD2=?,ORP_EDCITY=?,ORP_SERVICETIME=?,ORP_SERVICEDATE=?,ORP_STLATITUDE=?,ORP_STLONGITUDE=?,ORP_EDLATITUDE=?,ORP_EDLONGITUDE=?,ORP_NAME=?,ORP_SPLINS=?,ORP_PAYTYPE=?,ORP_PAYACC=?,ORP_AMT=?,ORP_SHARED_RIDE=?,ORP_NUMBER_OF_PASSENGERS=?,ORP_OPERATOR_COMMENTS=?,ORP_DISPATCH_COMMENTS=?,ORP_SEPARATED_BY=?,ORP_ROUTE_NUMBER=?,ORP_DATE_FORMAT=?,ORP_PICKUPORDER=?,ORP_DROPOFFORDER=? WHERE ORP_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND ORP_VENDOR='"+vendor+"'");
				}else{
					pst= con.prepareStatement("INSERT INTO TDS_OPENREQUEST_POSITION(ORP_ASSOCCODE,ORP_TRIP_ID,ORP_PHONE,ORP_STADD1,ORP_STADD2,ORP_STCITY,ORP_EDADD1,ORP_EDADD2,ORP_EDCITY,ORP_SERVICETIME,ORP_SERVICEDATE,ORP_STLATITUDE,ORP_STLONGITUDE,ORP_EDLATITUDE,ORP_EDLONGITUDE,ORP_NAME,ORP_SPLINS,ORP_PAYTYPE,ORP_PAYACC,ORP_AMT,ORP_SHARED_RIDE,ORP_NUMBER_OF_PASSENGERS,ORP_OPERATOR_COMMENTS,ORP_DISPATCH_COMMENTS,ORP_SEPARATED_BY,ORP_ROUTE_NUMBER,ORP_DATE_FORMAT,ORP_PICKUPORDER,ORP_DROPOFFORDER)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
				}
				pst.setString(1, adminBO.getAssociateCode());
				pst.setString(2,orFieldOrderBO.getTripid()==null?"":orFieldOrderBO.getTripid());
				pst.setString(3,orFieldOrderBO.getPhone()==null?"":orFieldOrderBO.getPhone());
				pst.setString(4,orFieldOrderBO.getSadd1()==null?"":orFieldOrderBO.getSadd1());
				pst.setString(5,orFieldOrderBO.getSadd2()==null?"":orFieldOrderBO.getSadd2());
				pst.setString(6,orFieldOrderBO.getScity()==null?"":orFieldOrderBO.getScity());
				pst.setString(7,orFieldOrderBO.getEadd1()==null?"":orFieldOrderBO.getEadd1());
				pst.setString(8,orFieldOrderBO.getEadd2()==null?"":orFieldOrderBO.getEadd2());
				pst.setString(9,orFieldOrderBO.getEcity()==null?"":orFieldOrderBO.getEcity());
				pst.setString(10,orFieldOrderBO.getShrs()==null?"":orFieldOrderBO.getShrs());
				pst.setString(11,orFieldOrderBO.getSdate()==null?"":orFieldOrderBO.getSdate());
				pst.setString(12,orFieldOrderBO.getSlat()==null?"":orFieldOrderBO.getSlat());
				pst.setString(13,orFieldOrderBO.getSlong()==null?"":orFieldOrderBO.getSlong());
				pst.setString(14,orFieldOrderBO.getEdlatitude()==null?"":orFieldOrderBO.getEdlatitude());
				pst.setString(15,orFieldOrderBO.getEdlongitude()==null?"":orFieldOrderBO.getEdlongitude());
				pst.setString(16,orFieldOrderBO.getName()==null?"":orFieldOrderBO.getName());
				pst.setString(17,orFieldOrderBO.getSpecialIns()==null?"":orFieldOrderBO.getSpecialIns());
				pst.setString(18,orFieldOrderBO.getPaytype()==null?"":orFieldOrderBO.getPaytype());
				pst.setString(19,orFieldOrderBO.getAcct()==null?"":orFieldOrderBO.getAcct());
				pst.setString(20,orFieldOrderBO.getAmt()==null?"":orFieldOrderBO.getAmt());
				pst.setString(21,orFieldOrderBO.getSharedRide()==null?"":orFieldOrderBO.getSharedRide());
				pst.setString(22,orFieldOrderBO.getNumberOfPassengers()==null?"":orFieldOrderBO.getNumberOfPassengers());
				pst.setString(23,orFieldOrderBO.getOperatorComments()==null?"":orFieldOrderBO.getOperatorComments());
				pst.setString(24,orFieldOrderBO.getDispatchComments()==null?"":orFieldOrderBO.getDispatchComments());
				pst.setString(25, separatedBy==null?"":separatedBy);
				pst.setString(26,orFieldOrderBO.getRouteNumber()==null?"":orFieldOrderBO.getRouteNumber());
				pst.setString(27,dateFormat==null?"":dateFormat);
				pst.setString(28,orFieldOrderBO.getPickUpOrder()==null?"":orFieldOrderBO.getPickUpOrder());
				pst.setString(29,orFieldOrderBO.getDropOffOrder()==null?"":orFieldOrderBO.getDropOffOrder());
				pst.execute();
				result=1;
			}else{
				pst= con.prepareStatement("INSERT INTO TDS_OPENREQUEST_POSITION(ORP_ASSOCCODE,ORP_TRIP_ID,ORP_PHONE,ORP_STADD1,ORP_STADD2,ORP_STCITY,ORP_EDADD1,ORP_EDADD2,ORP_EDCITY,ORP_SERVICETIME,ORP_SERVICEDATE,ORP_STLATITUDE,ORP_STLONGITUDE,ORP_EDLATITUDE,ORP_EDLONGITUDE,ORP_NAME,ORP_SPLINS,ORP_PAYTYPE,ORP_PAYACC,ORP_AMT,ORP_SHARED_RIDE,ORP_NUMBER_OF_PASSENGERS,ORP_OPERATOR_COMMENTS,ORP_DISPATCH_COMMENTS,ORP_VENDOR_NAME,ORP_SEPARATED_BY,ORP_ROUTE_NUMBER,ORP_DATE_FORMAT,ORP_PICKUPORDER,ORP_DROPOFFORDER)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
				pst.setString(1, adminBO.getAssociateCode());
				pst.setString(2,orFieldOrderBO.getTripid()==null?"":orFieldOrderBO.getTripid());
				pst.setString(3,orFieldOrderBO.getPhone()==null?"":orFieldOrderBO.getPhone());
				pst.setString(4,orFieldOrderBO.getSadd1()==null?"":orFieldOrderBO.getSadd1());
				pst.setString(5,orFieldOrderBO.getSadd2()==null?"":orFieldOrderBO.getSadd2());
				pst.setString(6,orFieldOrderBO.getScity()==null?"":orFieldOrderBO.getScity());
				pst.setString(7,orFieldOrderBO.getEadd1()==null?"":orFieldOrderBO.getEadd1());
				pst.setString(8,orFieldOrderBO.getEadd2()==null?"":orFieldOrderBO.getEadd2());
				pst.setString(9,orFieldOrderBO.getEcity()==null?"":orFieldOrderBO.getEcity());
				pst.setString(10,orFieldOrderBO.getShrs()==null?"":orFieldOrderBO.getShrs());
				pst.setString(11,orFieldOrderBO.getSdate()==null?"":orFieldOrderBO.getSdate());
				pst.setString(12,orFieldOrderBO.getSlat()==null?"":orFieldOrderBO.getSlat());
				pst.setString(13,orFieldOrderBO.getSlong()==null?"":orFieldOrderBO.getSlong());
				pst.setString(14,orFieldOrderBO.getEdlatitude()==null?"":orFieldOrderBO.getEdlatitude());
				pst.setString(15,orFieldOrderBO.getEdlongitude()==null?"":orFieldOrderBO.getEdlongitude());
				pst.setString(16,orFieldOrderBO.getName()==null?"":orFieldOrderBO.getName());
				pst.setString(17,orFieldOrderBO.getSpecialIns()==null?"":orFieldOrderBO.getSpecialIns());
				pst.setString(18,orFieldOrderBO.getPaytype()==null?"":orFieldOrderBO.getPaytype());
				pst.setString(19,orFieldOrderBO.getAcct()==null?"":orFieldOrderBO.getAcct());
				pst.setString(20,orFieldOrderBO.getAmt()==null?"":orFieldOrderBO.getAmt());
				pst.setString(21,orFieldOrderBO.getSharedRide()==null?"":orFieldOrderBO.getSharedRide());
				pst.setString(22,orFieldOrderBO.getNumberOfPassengers()==null?"":orFieldOrderBO.getNumberOfPassengers());
				pst.setString(23,orFieldOrderBO.getOperatorComments()==null?"":orFieldOrderBO.getOperatorComments());
				pst.setString(24,orFieldOrderBO.getDispatchComments()==null?"":orFieldOrderBO.getDispatchComments());
				pst.setString(25,vendorName==null?"":vendorName);
				pst.setString(26, separatedBy==null?"":separatedBy);
				pst.setString(27,orFieldOrderBO.getRouteNumber()==null?"":orFieldOrderBO.getRouteNumber());
				pst.setString(28,dateFormat==null?"":dateFormat);pst.setString(1, adminBO.getAssociateCode());
				pst.setString(29,orFieldOrderBO.getPickUpOrder()==null?"":orFieldOrderBO.getPickUpOrder());
				pst.setString(30,orFieldOrderBO.getDropOffOrder()==null?"":orFieldOrderBO.getDropOffOrder());
				pst.execute();
				result=1;
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException SystemPropertiesDAO.insertORFieldsPosition--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertORFieldsPosition--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	 */


	public static int insertORFieldsPosition(OpenRequestFieldOrderBO orFieldOrderBO,AdminRegistrationBO adminBO,int vendor,String vendorName,String separatedBy,String dateFormat) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement m_pst=null;
		ResultSet rs= null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			if(vendor!=0 ){
				m_pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_POSITION where ORP_ASSOCCODE=? AND ORP_VENDOR=?");
				m_pst.setString(1,adminBO.getAssociateCode());
				m_pst.setInt(2,vendor);
				rs=m_pst.executeQuery();
				if(rs.next()){
					pst= con.prepareStatement("UPDATE TDS_OPENREQUEST_POSITION SET ORP_ASSOCCODE=?,ORP_TRIP_ID=?,ORP_PHONE=?,ORP_STADD1=?,ORP_STADD2=?,ORP_STCITY=?,ORP_EDADD1=?,ORP_EDADD2=?,ORP_EDCITY=?,ORP_SERVICETIME=?,ORP_SERVICEDATE=?,ORP_STLATITUDE=?,ORP_STLONGITUDE=?,ORP_EDLATITUDE=?,ORP_EDLONGITUDE=?,ORP_NAME=?,ORP_SPLINS=?,ORP_PAYTYPE=?,ORP_PAYACC=?,ORP_AMT=?,ORP_SHARED_RIDE=?,ORP_NUMBER_OF_PASSENGERS=?,ORP_OPERATOR_COMMENTS=?,ORP_DISPATCH_COMMENTS=?,ORP_SEPARATED_BY=?,ORP_ROUTE_NUMBER=?,ORP_DATE_FORMAT=?,ORP_PICKUPORDER=?,ORP_DROPOFFORDER=?,ORP_CLIENTREFNUM=?,ORP_CLIENTREFNUM1=?,ORP_CLIENTREFNUM2=?,ORP_CLIENTREFNUM3=?,ORP_CUSTOMFIELD=? WHERE ORP_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND ORP_VENDOR='"+vendor+"'");
				}else{
					pst= con.prepareStatement("INSERT INTO TDS_OPENREQUEST_POSITION(ORP_ASSOCCODE,ORP_TRIP_ID,ORP_PHONE,ORP_STADD1,ORP_STADD2,ORP_STCITY,ORP_EDADD1,ORP_EDADD2,ORP_EDCITY,ORP_SERVICETIME,ORP_SERVICEDATE,ORP_STLATITUDE,ORP_STLONGITUDE,ORP_EDLATITUDE,ORP_EDLONGITUDE,ORP_NAME,ORP_SPLINS,ORP_PAYTYPE,ORP_PAYACC,ORP_AMT,ORP_SHARED_RIDE,ORP_NUMBER_OF_PASSENGERS,ORP_OPERATOR_COMMENTS,ORP_DISPATCH_COMMENTS,ORP_SEPARATED_BY,ORP_ROUTE_NUMBER,ORP_DATE_FORMAT,ORP_PICKUPORDER,ORP_DROPOFFORDER,ORP_CLIENTREFNUM,ORP_CLIENTREFNUM1=?,ORP_CLIENTREFNUM2=?,ORP_CLIENTREFNUM3=?,ORP_CUSTOMFIELD=?)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
				}
				pst.setString(1, adminBO.getAssociateCode());
				pst.setString(2,orFieldOrderBO.getTripid()==null?"":orFieldOrderBO.getTripid());
				pst.setString(3,orFieldOrderBO.getPhone()==null?"":orFieldOrderBO.getPhone());
				pst.setString(4,orFieldOrderBO.getSadd1()==null?"":orFieldOrderBO.getSadd1());
				pst.setString(5,orFieldOrderBO.getSadd2()==null?"":orFieldOrderBO.getSadd2());
				pst.setString(6,orFieldOrderBO.getScity()==null?"":orFieldOrderBO.getScity());
				pst.setString(7,orFieldOrderBO.getEadd1()==null?"":orFieldOrderBO.getEadd1());
				pst.setString(8,orFieldOrderBO.getEadd2()==null?"":orFieldOrderBO.getEadd2());
				pst.setString(9,orFieldOrderBO.getEcity()==null?"":orFieldOrderBO.getEcity());
				pst.setString(10,orFieldOrderBO.getShrs()==null?"":orFieldOrderBO.getShrs());
				pst.setString(11,orFieldOrderBO.getSdate()==null?"":orFieldOrderBO.getSdate());
				pst.setString(12,orFieldOrderBO.getSlat()==null?"":orFieldOrderBO.getSlat());
				pst.setString(13,orFieldOrderBO.getSlong()==null?"":orFieldOrderBO.getSlong());
				pst.setString(14,orFieldOrderBO.getEdlatitude()==null?"":orFieldOrderBO.getEdlatitude());
				pst.setString(15,orFieldOrderBO.getEdlongitude()==null?"":orFieldOrderBO.getEdlongitude());
				pst.setString(16,orFieldOrderBO.getName()==null?"":orFieldOrderBO.getName());
				pst.setString(17,orFieldOrderBO.getSpecialIns()==null?"":orFieldOrderBO.getSpecialIns());
				pst.setString(18,orFieldOrderBO.getPaytype()==null?"":orFieldOrderBO.getPaytype());
				pst.setString(19,orFieldOrderBO.getAcct()==null?"":orFieldOrderBO.getAcct());
				pst.setString(20,orFieldOrderBO.getAmt()==null?"":orFieldOrderBO.getAmt());
				pst.setString(21,orFieldOrderBO.getSharedRide()==null?"":orFieldOrderBO.getSharedRide());
				pst.setString(22,orFieldOrderBO.getNumberOfPassengers()==null?"":orFieldOrderBO.getNumberOfPassengers());
				pst.setString(23,orFieldOrderBO.getOperatorComments()==null?"":orFieldOrderBO.getOperatorComments());
				pst.setString(24,orFieldOrderBO.getDispatchComments()==null?"":orFieldOrderBO.getDispatchComments());
				pst.setString(25, separatedBy==null?"":separatedBy);
				pst.setString(26,orFieldOrderBO.getRouteNumber()==null?"":orFieldOrderBO.getRouteNumber());
				pst.setString(27,dateFormat==null?"":dateFormat);
				pst.setString(28,orFieldOrderBO.getPickUpOrder()==null?"":orFieldOrderBO.getPickUpOrder());
				pst.setString(29,orFieldOrderBO.getDropOffOrder()==null?"":orFieldOrderBO.getDropOffOrder());
				pst.setString(30,orFieldOrderBO.getRefNumber()==null?"":orFieldOrderBO.getRefNumber());
				pst.setString(31,orFieldOrderBO.getRefNumber1()==null?"":orFieldOrderBO.getRefNumber1());
				pst.setString(32,orFieldOrderBO.getRefNumber2()==null?"":orFieldOrderBO.getRefNumber2());
				pst.setString(33,orFieldOrderBO.getRefNumber3()==null?"":orFieldOrderBO.getRefNumber3());
				pst.setString(34,orFieldOrderBO.getCustomField()==null?"":orFieldOrderBO.getCustomField());
				pst.execute();
				result=1;
			}else{
				pst= con.prepareStatement("INSERT INTO TDS_OPENREQUEST_POSITION(ORP_ASSOCCODE,ORP_TRIP_ID,ORP_PHONE,ORP_STADD1,ORP_STADD2,ORP_STCITY,ORP_EDADD1,ORP_EDADD2,ORP_EDCITY,ORP_SERVICETIME,ORP_SERVICEDATE,ORP_STLATITUDE,ORP_STLONGITUDE,ORP_EDLATITUDE,ORP_EDLONGITUDE,ORP_NAME,ORP_SPLINS,ORP_PAYTYPE,ORP_PAYACC,ORP_AMT,ORP_SHARED_RIDE,ORP_NUMBER_OF_PASSENGERS,ORP_OPERATOR_COMMENTS,ORP_DISPATCH_COMMENTS,ORP_VENDOR_NAME,ORP_SEPARATED_BY,ORP_ROUTE_NUMBER,ORP_DATE_FORMAT,ORP_PICKUPORDER,ORP_DROPOFFORDER,ORP_CLIENTREFNUM,ORP_CLIENTREFNUM1,ORP_CLIENTREFNUM2,ORP_CLIENTREFNUM3,ORP_CUSTOMFIELD)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
				pst.setString(1, adminBO.getAssociateCode());
				pst.setString(2,orFieldOrderBO.getTripid()==null?"":orFieldOrderBO.getTripid());
				pst.setString(3,orFieldOrderBO.getPhone()==null?"":orFieldOrderBO.getPhone());
				pst.setString(4,orFieldOrderBO.getSadd1()==null?"":orFieldOrderBO.getSadd1());
				pst.setString(5,orFieldOrderBO.getSadd2()==null?"":orFieldOrderBO.getSadd2());
				pst.setString(6,orFieldOrderBO.getScity()==null?"":orFieldOrderBO.getScity());
				pst.setString(7,orFieldOrderBO.getEadd1()==null?"":orFieldOrderBO.getEadd1());
				pst.setString(8,orFieldOrderBO.getEadd2()==null?"":orFieldOrderBO.getEadd2());
				pst.setString(9,orFieldOrderBO.getEcity()==null?"":orFieldOrderBO.getEcity());
				pst.setString(10,orFieldOrderBO.getShrs()==null?"":orFieldOrderBO.getShrs());
				pst.setString(11,orFieldOrderBO.getSdate()==null?"":orFieldOrderBO.getSdate());
				pst.setString(12,orFieldOrderBO.getSlat()==null?"":orFieldOrderBO.getSlat());
				pst.setString(13,orFieldOrderBO.getSlong()==null?"":orFieldOrderBO.getSlong());
				pst.setString(14,orFieldOrderBO.getEdlatitude()==null?"":orFieldOrderBO.getEdlatitude());
				pst.setString(15,orFieldOrderBO.getEdlongitude()==null?"":orFieldOrderBO.getEdlongitude());
				pst.setString(16,orFieldOrderBO.getName()==null?"":orFieldOrderBO.getName());
				pst.setString(17,orFieldOrderBO.getSpecialIns()==null?"":orFieldOrderBO.getSpecialIns());
				pst.setString(18,orFieldOrderBO.getPaytype()==null?"":orFieldOrderBO.getPaytype());
				pst.setString(19,orFieldOrderBO.getAcct()==null?"":orFieldOrderBO.getAcct());
				pst.setString(20,orFieldOrderBO.getAmt()==null?"":orFieldOrderBO.getAmt());
				pst.setString(21,orFieldOrderBO.getSharedRide()==null?"":orFieldOrderBO.getSharedRide());
				pst.setString(22,orFieldOrderBO.getNumberOfPassengers()==null?"":orFieldOrderBO.getNumberOfPassengers());
				pst.setString(23,orFieldOrderBO.getOperatorComments()==null?"":orFieldOrderBO.getOperatorComments());
				pst.setString(24,orFieldOrderBO.getDispatchComments()==null?"":orFieldOrderBO.getDispatchComments());
				pst.setString(25,vendorName==null?"":vendorName);
				pst.setString(26, separatedBy==null?"":separatedBy);
				pst.setString(27,orFieldOrderBO.getRouteNumber()==null?"":orFieldOrderBO.getRouteNumber());
				pst.setString(28,dateFormat==null?"":dateFormat);pst.setString(1, adminBO.getAssociateCode());
				pst.setString(29,orFieldOrderBO.getPickUpOrder()==null?"":orFieldOrderBO.getPickUpOrder());
				pst.setString(30,orFieldOrderBO.getDropOffOrder()==null?"":orFieldOrderBO.getDropOffOrder());
				pst.setString(31,orFieldOrderBO.getRefNumber()==null?"":orFieldOrderBO.getRefNumber());
				pst.setString(32,orFieldOrderBO.getRefNumber1()==null?"":orFieldOrderBO.getRefNumber1());
				pst.setString(33,orFieldOrderBO.getRefNumber2()==null?"":orFieldOrderBO.getRefNumber2());
				pst.setString(34,orFieldOrderBO.getRefNumber3()==null?"":orFieldOrderBO.getRefNumber3());
				pst.setString(35,orFieldOrderBO.getCustomField()==null?"":orFieldOrderBO.getCustomField());
				pst.execute();
				result=1;
			}
		}catch ( SQLException sqex) {
			System.out.println("TDSException SystemPropertiesDAO.insertORFieldsPosition--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertORFieldsPosition--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int deleteORFieldsPosition(String vendor) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs= null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst= con.prepareStatement("DELETE FROM TDS_OPENREQUEST_POSITION WHERE ORP_VENDOR=?");
			pst.setString(1,vendor);
			//System.out.println("query"+pst.toString());
			result = pst.executeUpdate();
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.deleteORFieldsPosition--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.deleteORFieldsPosition--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<OpenRequestFieldOrderBO> getVendorList(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<OpenRequestFieldOrderBO>getVendors = new ArrayList<OpenRequestFieldOrderBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs= null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="SELECT * FROM TDS_OPENREQUEST_POSITION WHERE ORP_ASSOCCODE='"+adminBO.getAssociateCode()+"' ORDER BY ORP_VENDOR";
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while (rs.next()) {
				OpenRequestFieldOrderBO orfBO =new OpenRequestFieldOrderBO();
				orfBO.setVendor(rs.getString("ORP_VENDOR"));
				orfBO.setVendorName(rs.getString("ORP_VENDOR_NAME"));
				getVendors.add(orfBO);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getVendorList--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getVendorList--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return getVendors;
	}



	public static OpenRequestFieldOrderBO getORFieldsOrder(String associationCode)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		OpenRequestFieldOrderBO orFieldOrderBO= new OpenRequestFieldOrderBO();
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_POSITION WHERE ORP_ASSOCCODE= ?");	
			pst.setString(1,associationCode);
			rs=pst.executeQuery();
			while(rs.next()){
				orFieldOrderBO.setTripid(rs.getString("ORP_TRIP_ID"));
				orFieldOrderBO.setName(rs.getString("ORP_NAME"));
				orFieldOrderBO.setSadd1(rs.getString("ORP_STADD1"));
				orFieldOrderBO.setSadd2(rs.getString("ORP_STADD2"));
				orFieldOrderBO.setScity(rs.getString("ORP_STCITY"));
				orFieldOrderBO.setEadd1(rs.getString("ORP_EDADD1"));
				orFieldOrderBO.setEadd2(rs.getString("ORP_EDADD2"));
				orFieldOrderBO.setEcity(rs.getString("ORP_EDCITY"));
				orFieldOrderBO.setShrs(rs.getString("ORP_SERVICETIME"));
				orFieldOrderBO.setSdate(rs.getString("ORP_SERVICEDATE"));
				orFieldOrderBO.setSlat(rs.getString("ORP_STLATITUDE"));
				orFieldOrderBO.setSlong(rs.getString("ORP_STLONGITUDE"));
				orFieldOrderBO.setEdlatitude(rs.getString("ORP_EDLATITUDE"));
				orFieldOrderBO.setEdlongitude(rs.getString("ORP_EDLONGITUDE"));
				orFieldOrderBO.setSpecialIns(rs.getString("ORP_SPLINS"));
				orFieldOrderBO.setPaytype(rs.getString("ORP_PAYTYPE"));
				orFieldOrderBO.setAcct(rs.getString("ORP_PAYACC"));
				orFieldOrderBO.setAmt(rs.getString("ORP_AMT"));
				orFieldOrderBO.setSharedRide(rs.getString("ORP_SHARED_RIDE"));
				orFieldOrderBO.setNumberOfPassengers(rs.getString("ORP_NUMBER_OF_PASSENGERS"));
			}

		} catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getORFieldsOrder--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getORFieldsOrder--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return orFieldOrderBO;
	}


	public static int  insertDispatchProperties(String associationCode,DispatchPropertiesBO dispatchBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			con.setAutoCommit(false);

			String delQuery="DELETE FROM TDS_DISPATCH_PROPERTIES WHERE DP_ASSOCCODE= ?";

			pst = con.prepareStatement(delQuery);
			pst.setString(1, associationCode);
			pst.execute();	
			String query="INSERT INTO TDS_DISPATCH_PROPERTIES(DP_ASSOCCODE,DP_NAME,DP_PHONE_NUMBER,DP_START_ADDRESS1,DP_START_ADDRESS2,DP_START_CITY,DP_END_ADDRESS1,DP_END_ADDRESS2,DP_END_CITY,DP_PAYMENT_TYPE,DP_ON_OFFER,DP_ZONES,DP_END_ZONES,DP_JOB_ATTRIBUTE,DP_AMOUNT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			pst=con.prepareStatement(query);
			pst.setString(1, associationCode);
			pst.setInt(2,dispatchBO.getName_onOffer());
			pst.setInt(3, dispatchBO.getPhoneNumber_onOffer());
			pst.setInt(4, dispatchBO.getsAddress1_onOffer());
			pst.setInt(5, dispatchBO.getsAddress2_onOffer());
			pst.setInt(6,dispatchBO.getsCity_onOffer());
			pst.setInt(7, dispatchBO.geteAddress1_onOffer());
			pst.setInt(8, dispatchBO.geteAddress2_onOffer());
			pst.setInt(9, dispatchBO.geteCity_onOffer());
			pst.setInt(10, dispatchBO.getPaymentType_onOffer());
			pst.setInt(11, 1);
			pst.setInt(12, dispatchBO.getZone_onOffer());
			pst.setInt(13,dispatchBO.getEndZone_onOffer());
			pst.setInt(14, dispatchBO.getJobAttributeOnOffer());
			pst.setInt(15, dispatchBO.getFareAmt_onOffer());
			result=pst.executeUpdate();
			if(result==1){
				result=0;
				String query1="INSERT INTO TDS_DISPATCH_PROPERTIES(DP_ASSOCCODE,DP_NAME,DP_PHONE_NUMBER,DP_START_ADDRESS1,DP_START_ADDRESS2,DP_START_CITY,DP_END_ADDRESS1,DP_END_ADDRESS2,DP_END_CITY,DP_PAYMENT_TYPE,DP_ON_ACCEPT,DP_ZONES,DP_END_ZONES,DP_JOB_ATTRIBUTE,DP_AMOUNT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				pst=con.prepareStatement(query1);
				pst.setString(1, associationCode);
				pst.setInt(2,dispatchBO.getName_onAccept());
				pst.setInt(3, dispatchBO.getPhoneNumber_onAccept());
				pst.setInt(4, dispatchBO.getsAddress1_onAccept());
				pst.setInt(5, dispatchBO.getsAddress2_onAccept());
				pst.setInt(6,dispatchBO.getsCity_onAccept());
				pst.setInt(7, dispatchBO.geteAddress1_onAccept());
				pst.setInt(8, dispatchBO.geteAddress2_onAccept());
				pst.setInt(9, dispatchBO.geteCity_onAccept());
				pst.setInt(10, dispatchBO.getPaymentType_onAccept());
				pst.setInt(11, 1);
				pst.setInt(12,dispatchBO.getZone_onAccept());
				pst.setInt(13,dispatchBO.getEndZone_onAccept());
				pst.setInt(14, dispatchBO.getJobAttributeOnAccept());
				pst.setInt(15, dispatchBO.getFareAmt_onAccept());
				result=pst.executeUpdate();
				if(result==1){
					String query2="INSERT INTO TDS_DISPATCH_PROPERTIES(DP_ASSOCCODE,DP_NAME,DP_PHONE_NUMBER,DP_START_ADDRESS1,DP_START_ADDRESS2,DP_START_CITY,DP_END_ADDRESS1,DP_END_ADDRESS2,DP_END_CITY,DP_PAYMENT_TYPE,DP_ON_SITE,DP_ZONES,DP_END_ZONES,DP_JOB_ATTRIBUTE,DP_AMOUNT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					pst=con.prepareStatement(query2);
					pst.setString(1, associationCode);
					pst.setInt(2,dispatchBO.getName_onSite());
					pst.setInt(3, dispatchBO.getPhoneNumber_onSite());
					pst.setInt(4, dispatchBO.getsAddress1_onSite());
					pst.setInt(5, dispatchBO.getsAddress2_onSite());
					pst.setInt(6,dispatchBO.getsCity_onSite());
					pst.setInt(7, dispatchBO.geteAddress1_onSite());
					pst.setInt(8, dispatchBO.geteAddress2_onSite());
					pst.setInt(9, dispatchBO.geteCity_onSite());
					pst.setInt(10, dispatchBO.getPaymentType_onSite());
					pst.setInt(11, 1);
					pst.setInt(12,dispatchBO.getZone_onSite());
					pst.setInt(13,dispatchBO.getEndZone_onSite());
					pst.setInt(14, dispatchBO.getJobAttributeOnSite());
					pst.setInt(15, dispatchBO.getFareAmt_onSite());
					result=pst.executeUpdate();
				}
				con.setAutoCommit(true);
			}
		}catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.insertDispatchProperties--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertDispatchProperties--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static DispatchPropertiesBO getDispatchPropeties(String associationCode)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		DispatchPropertiesBO dispatchBO = new DispatchPropertiesBO();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_DISPATCH_PROPERTIES WHERE DP_ASSOCCODE='"+associationCode+"'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				if(rs.getInt("DP_ON_OFFER")==1){
					dispatchBO.setName_onOffer(rs.getInt("DP_NAME"));
					dispatchBO.setsAddress1_onOffer(rs.getInt("DP_START_ADDRESS1"));
					dispatchBO.setsAddress2_onOffer(rs.getInt("DP_START_ADDRESS2"));
					dispatchBO.setsCity_onOffer(rs.getInt("DP_START_CITY"));
					dispatchBO.seteAddress1_onOffer(rs.getInt("DP_END_ADDRESS1"));
					dispatchBO.seteAddress2_onOffer(rs.getInt("DP_END_ADDRESS2"));
					dispatchBO.seteCity_onOffer(rs.getInt("DP_END_CITY"));
					dispatchBO.setPhoneNumber_onOffer(rs.getInt("DP_PHONE_NUMBER"));
					dispatchBO.setPaymentType_onOffer(rs.getInt("DP_PAYMENT_TYPE"));
					dispatchBO.setZone_onOffer(rs.getInt("DP_ZONES"));
					dispatchBO.setEndZone_onOffer(rs.getInt("DP_END_ZONES"));
					dispatchBO.setAcceptanceTimeOnOffer(0);
					dispatchBO.setJobAttributeOnOffer(rs.getInt("DP_JOB_ATTRIBUTE"));
					dispatchBO.setFareAmt_onOffer(rs.getInt("DP_AMOUNT"));
				}else if(rs.getInt("DP_ON_ACCEPT")==1){
					dispatchBO.setName_onAccept(rs.getInt("DP_NAME"));
					dispatchBO.setsAddress1_onAccept(rs.getInt("DP_START_ADDRESS1"));
					dispatchBO.setsAddress2_onAccept(rs.getInt("DP_START_ADDRESS2"));
					dispatchBO.setsCity_onAccept(rs.getInt("DP_START_CITY"));
					dispatchBO.seteAddress1_onAccept(rs.getInt("DP_END_ADDRESS1"));
					dispatchBO.seteAddress2_onAccept(rs.getInt("DP_END_ADDRESS2"));
					dispatchBO.seteCity_onAccept(rs.getInt("DP_END_CITY"));
					dispatchBO.setPhoneNumber_onAccept(rs.getInt("DP_PHONE_NUMBER"));
					dispatchBO.setPaymentType_onAccept(rs.getInt("DP_PAYMENT_TYPE"));
					dispatchBO.setZone_onAccept(rs.getInt("DP_ZONES"));
					dispatchBO.setEndZone_onAccept(rs.getInt("DP_END_ZONES"));
					dispatchBO.setAcceptanceTimeOnAccept(0);
					dispatchBO.setJobAttributeOnAccept(rs.getInt("DP_JOB_ATTRIBUTE"));
					dispatchBO.setFareAmt_onAccept(rs.getInt("DP_AMOUNT"));
				}else if(rs.getInt("DP_ON_SITE")==1){
					dispatchBO.setName_onSite(rs.getInt("DP_NAME"));
					dispatchBO.setsAddress1_onSite(rs.getInt("DP_START_ADDRESS1"));
					dispatchBO.setsAddress2_onSite(rs.getInt("DP_START_ADDRESS2"));
					dispatchBO.setsCity_onSite(rs.getInt("DP_START_CITY"));
					dispatchBO.seteAddress1_onSite(rs.getInt("DP_END_ADDRESS1"));
					dispatchBO.seteAddress2_onSite(rs.getInt("DP_END_ADDRESS2"));
					dispatchBO.seteCity_onSite(rs.getInt("DP_END_CITY"));
					dispatchBO.setPhoneNumber_onSite(rs.getInt("DP_PHONE_NUMBER"));
					dispatchBO.setPaymentType_onSite(rs.getInt("DP_PAYMENT_TYPE"));
					dispatchBO.setZone_onSite(rs.getInt("DP_ZONES"));
					dispatchBO.setEndZone_onSite(rs.getInt("DP_END_ZONES"));
					dispatchBO.setJobAttributeOnSite(rs.getInt("DP_JOB_ATTRIBUTE"));
					dispatchBO.setFareAmt_onSite(rs.getInt("DP_AMOUNT"));
					dispatchBO.setAcceptanceTimeOnOnSite(0);

				}
			}
			rs.close();
			pst.close();
		}catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getDispatchPropeties--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getDispatchPropeties--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return dispatchBO;
	}




	public static int insertDocumentTypes (String associationCode,String[] documentType,String[] shortName,String[] serverityType,int size,String[] driverOrVehicle,String[] checkForExpiry)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			if(documentType[1]!=""){
				for(int i=1;i<=size;i++){
					con.setAutoCommit(false);
					String delQuery="DELETE FROM TDS_DOCUMENTS_MASTER WHERE DM_ASSOCCODE= ? and MODULE_NAME=?";
					pst = con.prepareStatement(delQuery);
					pst.setString(1, associationCode);
					pst.setString(2,shortName[i]);
					cat.info(pst.toString());
					pst.execute();	
					String query="INSERT INTO TDS_DOCUMENTS_MASTER(DM_ASSOCCODE,DOCUMENT_TYPE,SERVERITY_TYPE,MODULE_NAME,DM_DOCUMENT_BELONGS_TO,DM_CHECK_FOR_EXPIRY) VALUES (?,?,?,?,?,?)";
					pst=con.prepareStatement(query);
					pst.setString(1, associationCode);
					pst.setString(2,documentType[i]);
					pst.setString(3, serverityType[i]);
					pst.setString(4,shortName[i]);
					pst.setString(5, driverOrVehicle[i]);
					pst.setString(6, checkForExpiry[i]);

					result=pst.executeUpdate();
					con.commit();
					con.setAutoCommit(true);
				}
			}
		}catch(Exception sqex){
			//System.out.println("TDSException SystemPropertiesDAO.insertDocumentTypes--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertDocumentTypes--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList getDocumentTypes(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<DocumentBO>getDocuments = new ArrayList<DocumentBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs= null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="SELECT * FROM TDS_DOCUMENTS_MASTER WHERE DM_ASSOCCODE='"+adminBO.getAssociateCode()+"' ";
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while (rs.next()) {
				DocumentBO docBO =new DocumentBO();
				docBO.setSerialNumber(rs.getString("MASTER_ID"));
				docBO.setDocumentType(rs.getString("DOCUMENT_TYPE"));
				docBO.setServerityType(rs.getString("SERVERITY_TYPE"));
				docBO.setShortName(rs.getString("MODULE_NAME"));
				docBO.setDocumentBelongsTo(rs.getString("DM_DOCUMENT_BELONGS_TO"));
				docBO.setCheckForExpiry(rs.getInt("DM_CHECK_FOR_EXPIRY"));
				getDocuments.add(docBO);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getWrapper--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getWrapper--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return getDocuments;
	}



	public static OpenRequestFieldOrderBO getORFieldsOrder(String associationCode,String vendor)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		OpenRequestFieldOrderBO orFieldOrderBO= new OpenRequestFieldOrderBO();
		ResultSet rs = null;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_POSITION WHERE ORP_ASSOCCODE= ? AND ORP_VENDOR= ?");	
			pst.setString(1,associationCode);
			pst.setString(2, vendor);
			rs=pst.executeQuery();
			while(rs.next()){
				orFieldOrderBO.setTripid(rs.getString("ORP_TRIP_ID"));
				orFieldOrderBO.setPhone(rs.getString("ORP_PHONE"));
				orFieldOrderBO.setName(rs.getString("ORP_NAME"));
				orFieldOrderBO.setSadd1(rs.getString("ORP_STADD1"));
				orFieldOrderBO.setSadd2(rs.getString("ORP_STADD2"));
				orFieldOrderBO.setScity(rs.getString("ORP_STCITY"));
				orFieldOrderBO.setEadd1(rs.getString("ORP_EDADD1"));
				orFieldOrderBO.setEadd2(rs.getString("ORP_EDADD2"));
				orFieldOrderBO.setEcity(rs.getString("ORP_EDCITY"));
				orFieldOrderBO.setShrs(rs.getString("ORP_SERVICETIME"));
				orFieldOrderBO.setSdate(rs.getString("ORP_SERVICEDATE"));
				orFieldOrderBO.setSlat(rs.getString("ORP_STLATITUDE"));
				orFieldOrderBO.setSlong(rs.getString("ORP_STLONGITUDE"));
				orFieldOrderBO.setEdlatitude(rs.getString("ORP_EDLATITUDE"));
				orFieldOrderBO.setEdlongitude(rs.getString("ORP_EDLONGITUDE"));
				orFieldOrderBO.setSpecialIns(rs.getString("ORP_SPLINS"));
				orFieldOrderBO.setPaytype(rs.getString("ORP_PAYTYPE"));
				orFieldOrderBO.setAcct(rs.getString("ORP_PAYACC"));
				orFieldOrderBO.setAmt(rs.getString("ORP_AMT"));
				orFieldOrderBO.setSharedRide(rs.getString("ORP_SHARED_RIDE"));
				orFieldOrderBO.setNumberOfPassengers(rs.getString("ORP_NUMBER_OF_PASSENGERS"));
				orFieldOrderBO.setOperatorComments(rs.getString("ORP_OPERATOR_COMMENTS"));
				orFieldOrderBO.setDispatchComments(rs.getString("ORP_DISPATCH_COMMENTS"));
				orFieldOrderBO.setSeparatedBy(rs.getString("ORP_SEPARATED_BY"));
				orFieldOrderBO.setRouteNumber(rs.getString("ORP_ROUTE_NUMBER"));
				orFieldOrderBO.setDateFormat(rs.getString("ORP_DATE_FORMAT"));
				orFieldOrderBO.setPickUpOrder(rs.getString("ORP_PICKUPORDER"));
				orFieldOrderBO.setDropOffOrder(rs.getString("ORP_DROPOFFORDER"));

				orFieldOrderBO.setRefNumber(rs.getString("ORP_CLIENTREFNUM"));
				orFieldOrderBO.setRefNumber1(rs.getString("ORP_CLIENTREFNUM1"));
				orFieldOrderBO.setRefNumber2(rs.getString("ORP_CLIENTREFNUM2"));
				orFieldOrderBO.setRefNumber3(rs.getString("ORP_CLIENTREFNUM3"));
				orFieldOrderBO.setCustomField(rs.getString("ORP_CUSTOMFIELD"));

				/*orFieldOrderBO.setDateFormat(rs.getString("ORP_DATE_FORMAT"));*/
			}

		} catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getORFieldsOrder--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getORFieldsOrder--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();	
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return orFieldOrderBO;
	}


	public static int insertORFieldsPosition(OpenRequestFieldOrderBO orFieldOrderBO,AdminRegistrationBO adminBO,String vendor) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		PreparedStatement m_pst=null;
		ResultSet rs= null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			m_pst=con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_POSITION where ORP_ASSOCCODE=? AND ORP_VENDOR=?");
			m_pst.setString(1,adminBO.getAssociateCode());
			m_pst.setString(2,vendor);
			rs=m_pst.executeQuery();
			if(rs.next()){
				pst= con.prepareStatement("UPDATE TDS_OPENREQUEST_POSITION SET ORP_ASSOCCODE=?,ORP_TRIP_ID=?,ORP_PHONE=?,ORP_STADD1=?,ORP_STADD2=?,ORP_STCITY=?,ORP_EDADD1=?,ORP_EDADD2=?,ORP_EDCITY=?,ORP_SERVICETIME=?,ORP_SERVICEDATE=?,ORP_STLATITUDE=?,ORP_STLONGITUDE=?,ORP_EDLATITUDE=?,ORP_EDLONGITUDE=?,ORP_NAME=?,ORP_SPLINS=?,ORP_PAYTYPE=?,ORP_PAYACC=?,ORP_AMT=?,ORP_SHARED_RIDE=?,ORP_NUMBER_OF_PASSENGERS=?,ORP_OPERATOR_COMMENTS=?,ORP_DISPATCH_COMMENTS=?,ORP_VENDOR=?");
			}else{
				pst= con.prepareStatement("INSERT INTO TDS_OPENREQUEST_POSITION(ORP_ASSOCCODE,ORP_TRIP_ID,ORP_PHONE,ORP_STADD1,ORP_STADD2,ORP_STCITY,ORP_EDADD1,ORP_EDADD2,ORP_EDCITY,ORP_SERVICETIME,ORP_SERVICEDATE,ORP_STLATITUDE,ORP_STLONGITUDE,ORP_EDLATITUDE,ORP_EDLONGITUDE,ORP_NAME,ORP_SPLINS,ORP_PAYTYPE,ORP_PAYACC,ORP_AMT,ORP_SHARED_RIDE,ORP_NUMBER_OF_PASSENGERS,ORP_OPERATOR_COMMENTS,ORP_DISPATCH_COMMENTS,ORP_VENDOR)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
			}
			pst.setString(1, adminBO.getAssociateCode());
			pst.setString(2,orFieldOrderBO.getTripid());
			pst.setString(3,orFieldOrderBO.getPhone());
			pst.setString(4,orFieldOrderBO.getSadd1());
			pst.setString(5,orFieldOrderBO.getSadd2());
			pst.setString(6,orFieldOrderBO.getScity());
			pst.setString(7,orFieldOrderBO.getEadd1());
			pst.setString(8,orFieldOrderBO.getEadd2());
			pst.setString(9,orFieldOrderBO.getEcity());
			pst.setString(10,orFieldOrderBO.getShrs());
			pst.setString(11,orFieldOrderBO.getSdate());
			pst.setString(12,orFieldOrderBO.getSlat());
			pst.setString(13,orFieldOrderBO.getSlong());
			pst.setString(14,orFieldOrderBO.getEdlatitude());
			pst.setString(15,orFieldOrderBO.getEdlongitude());
			pst.setString(16,orFieldOrderBO.getName());
			pst.setString(17,orFieldOrderBO.getSpecialIns());
			pst.setString(18,orFieldOrderBO.getPaytype());
			pst.setString(19,orFieldOrderBO.getAcct());
			pst.setString(20,orFieldOrderBO.getAmt());
			pst.setString(21,orFieldOrderBO.getSharedRide());
			pst.setString(22,orFieldOrderBO.getNumberOfPassengers());
			pst.setString(23,orFieldOrderBO.getOperatorComments());
			pst.setString(24,orFieldOrderBO.getDispatchComments());
			pst.setString(25,vendor);
			pst.execute();
			result=1;
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.insertORFieldsPosition--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertORFieldsPosition--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<OpenRequestFieldOrderBO> getORFieldsOrderForjsp(String associationCode,String vendorId)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ArrayList<OpenRequestFieldOrderBO> al_list=new ArrayList<OpenRequestFieldOrderBO>();
		ResultSet rs = null;
		String query = "";
		if(!vendorId.equals("")){
			query = " AND ORP_VENDOR='"+vendorId+"'";
		}
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT * FROM TDS_OPENREQUEST_POSITION WHERE ORP_ASSOCCODE= ?"+query);	
			pst.setString(1,associationCode);
			rs=pst.executeQuery();
			while(rs.next()){
				OpenRequestFieldOrderBO orFieldOrderBO= new OpenRequestFieldOrderBO();
				orFieldOrderBO.setTripid(rs.getString("ORP_TRIP_ID"));
				orFieldOrderBO.setName(rs.getString("ORP_NAME"));
				orFieldOrderBO.setSadd1(rs.getString("ORP_STADD1"));
				orFieldOrderBO.setSadd2(rs.getString("ORP_STADD2"));
				orFieldOrderBO.setScity(rs.getString("ORP_STCITY"));
				orFieldOrderBO.setEadd1(rs.getString("ORP_EDADD1"));
				orFieldOrderBO.setEadd2(rs.getString("ORP_EDADD2"));
				orFieldOrderBO.setEcity(rs.getString("ORP_EDCITY"));
				orFieldOrderBO.setShrs(rs.getString("ORP_SERVICETIME"));
				orFieldOrderBO.setSdate(rs.getString("ORP_SERVICEDATE"));
				orFieldOrderBO.setSlat(rs.getString("ORP_STLATITUDE"));
				orFieldOrderBO.setSlong(rs.getString("ORP_STLONGITUDE"));
				orFieldOrderBO.setEdlatitude(rs.getString("ORP_EDLATITUDE"));
				orFieldOrderBO.setEdlongitude(rs.getString("ORP_EDLONGITUDE"));
				orFieldOrderBO.setSpecialIns(rs.getString("ORP_SPLINS"));
				orFieldOrderBO.setPaytype(rs.getString("ORP_PAYTYPE"));
				orFieldOrderBO.setAcct(rs.getString("ORP_PAYACC"));
				orFieldOrderBO.setAmt(rs.getString("ORP_AMT"));
				orFieldOrderBO.setSharedRide(rs.getString("ORP_SHARED_RIDE"));
				orFieldOrderBO.setNumberOfPassengers(rs.getString("ORP_NUMBER_OF_PASSENGERS"));
				orFieldOrderBO.setVendorName(rs.getString("ORP_VENDOR_NAME"));
				orFieldOrderBO.setVendor(rs.getString("ORP_VENDOR"));
				al_list.add(orFieldOrderBO);
			}

		} catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getORFieldsOrderForjsp--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getORFieldsOrderForjsp--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	}

	public static int  insertDispatchCallProperties(String associationCode,DispatchPropertiesBO dispatchBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			con.setAutoCommit(false);

			String delQuery="DELETE FROM TDS_DISPATCH_CALL_PROPERTIES WHERE DCP_ASSOCIATION_CODE= ?";

			pst = con.prepareStatement(delQuery);
			pst.setString(1, associationCode);
			cat.info(pst.toString());
			pst.execute();	
			String query="INSERT INTO TDS_DISPATCH_CALL_PROPERTIES(DCP_ASSOCIATION_CODE,DCP_CALL_ACCEPT,DCP_CALL_ROUTE,DCP_CALL_SITE,DCP_MESSAGE_ACCEPT,DCP_MESSAGE_ROUTE,DCP_MESSAGE_SITE,DCP_ACCEPT_MESSAGE,DCP_ROUTE_MESSAGE,DCP_SITE_MESSAGE) VALUES (?,?,?,?,?,?,?,?,?,?)";
			pst=con.prepareStatement(query);
			pst.setString(1, associationCode);
			pst.setInt(2,dispatchBO.getCall_onAccept());
			pst.setInt(3, dispatchBO.getCall_onRoute());
			pst.setInt(4, dispatchBO.getCall_onSite());
			pst.setInt(5, dispatchBO.getMessage_onAccept());
			pst.setInt(6,dispatchBO.getMessage_onRoute());
			pst.setInt(7, dispatchBO.getMessage_onSite());
			pst.setString(8, dispatchBO.getMessageAccept());
			pst.setString(9, dispatchBO.getMessageRoute());
			pst.setString(10, dispatchBO.getMessageSite());
			result=pst.executeUpdate();
			con.setAutoCommit(true);
		}catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.insertDispatchCallProperties--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertDispatchCallProperties--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static DispatchPropertiesBO getDispatchCallPropeties(String associationCode)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		DispatchPropertiesBO dispatchBO = new DispatchPropertiesBO();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_DISPATCH_CALL_PROPERTIES WHERE DCP_ASSOCIATION_CODE='"+associationCode+"'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next()) {
				dispatchBO.setCall_onAccept(rs.getInt("DCP_CALL_ACCEPT"));
				dispatchBO.setCall_onRoute(rs.getInt("DCP_CALL_ROUTE"));
				dispatchBO.setCall_onSite(rs.getInt("DCP_CALL_SITE"));
				dispatchBO.setMessage_onAccept(rs.getInt("DCP_MESSAGE_ACCEPT"));
				dispatchBO.setMessage_onRoute(rs.getInt("DCP_MESSAGE_ROUTE"));
				dispatchBO.setMessage_onSite(rs.getInt("DCP_MESSAGE_SITE"));
				dispatchBO.setMessageAccept(rs.getString("DCP_ACCEPT_MESSAGE"));
				dispatchBO.setMessageRoute(rs.getString("DCP_ROUTE_MESSAGE"));
				dispatchBO.setMessageSite(rs.getString("DCP_SITE_MESSAGE"));
			}
		}catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getDispatchCallPropeties--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getDispatchCallPropeties--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return dispatchBO;
	}
	public static CompanyMasterBO getCCProperties(String associationCode)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		CompanyMasterBO cmpBean = new CompanyMasterBO();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_COMPANY_CC_DETAILS WHERE CCD_ASSOCIATION_CODE='"+associationCode+"'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.next()) {
				cmpBean.setCcUsername(rs.getString("CCD_USER_NAME"));
				cmpBean.setCcPassword(rs.getString("CCD_PASSWORD"));
				cmpBean.setCcProvider(rs.getInt("CCD_PROVIDER"));
				cmpBean.setCname(rs.getString("CCD_CUSTOMER_NAME"));
				cmpBean.setEmail(rs.getString("CCD_EMAIL"));
				cmpBean.setSiteId(rs.getString("CCD_SITEID"));
				cmpBean.setPriceId(rs.getString("CCD_PRICEID"));
				cmpBean.setKey(rs.getString("CCD_KEY"));
			}
		}catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getCCProperties--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getCCProperties--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return cmpBean;
	}
	public static int insertCCProperties(CompanyMasterBO cmpBean,String associationCode)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		int result=0;
		try {
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM TDS_COMPANY_CC_DETAILS WHERE CCD_ASSOCIATION_CODE='"+associationCode+"'");
			pst.execute();
			pst = con.prepareStatement("INSERT INTO TDS_COMPANY_CC_DETAILS (CCD_ASSOCIATION_CODE,CCD_USER_NAME,CCD_PASSWORD,CCD_PROVIDER,CCD_CUSTOMER_NAME,CCD_EMAIL,CCD_SITEID,CCD_PRICEID,CCD_KEY) VALUES (?,?,?,?,?,?,?,?,?)");
			pst.setString(1, associationCode);
			pst.setString(2, cmpBean.getCcUsername());
			pst.setString(3, cmpBean.getCcPassword());
			pst.setInt(4, cmpBean.getCcProvider());
			pst.setString(5, cmpBean.getCname());
			pst.setString(6, cmpBean.getEmail());
			pst.setString(7, cmpBean.getSiteId());
			pst.setString(8, cmpBean.getPriceId());
			pst.setString(9, cmpBean.getKey());
			pst.execute();
			result=1;
			con.commit();
			con.setAutoCommit(true);
		}catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.insertCCProperties--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertCCProperties--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}


	public static int readShiftRegisterMaster(String associationCode, String operatorID){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement(TDSSQLConstants.getShiftRegister); 
			csp_pst.setString(1, associationCode);
			csp_pst.setString(2, operatorID);
			//System.out.println(csp_pst.toString());
			rs = csp_pst.executeQuery();
			if(rs.next()){
				String status=rs.getString("SRM_REGISTER_STATUS");
				if(status.equals("A")){
					result=1;
				}else if(status.equals("B")){
					result=2;
				}
			}

		} catch (Exception sqex){
			cat.error("TDSException SystemPropertiesDAO.readShiftRegisterMaster-->"+sqex.getMessage());
			//System.out.println("TDSException SystemPropertiesDAO.readShiftRegisterMaster-->" + sqex.getMessage());
			sqex.printStackTrace();

		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int insertUserProfile(String format,AdminRegistrationBO adminBO)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM TDS_USER_COOKIES  WHERE UC_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND UC_NAME='GAC_OR_FORMAT'");
			pst.execute();
			pst = con.prepareStatement("INSERT INTO TDS_USER_COOKIES  (UC_NAME,UC_VALUE,UC_USER,UC_ASSOCCODE) VALUES (?,?,?,?)");
			pst.setString(1, "GAC_OR_FORMAT");
			pst.setString(2, format);
			pst.setString(3, adminBO.getUid());
			pst.setString(4, adminBO.getAssociateCode());
			cat.info(pst.toString());
			result=pst.executeUpdate();
			con.commit();
			con.setAutoCommit(true);
		}catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.insertUserProfile--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertUserProfile--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}




	public static String getUserProfile(AdminRegistrationBO adminBO)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String value="";
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT * FROM TDS_USER_COOKIES WHERE UC_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND UC_NAME='GAC_OR_FORMAT' AND UC_USER='"+adminBO.getUid()+"'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			if(rs.next()) {
				value=rs.getString("UC_VALUE");
			}
		}catch (SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getCCProperties--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getCCProperties--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return value;
	}
	public static int changePassword(AdminRegistrationBO adminBO,String password,String oldPassword) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;

		int result=0;
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst= con.prepareStatement("UPDATE TDS_ADMINUSER SET AU_PASSWORD=? where AU_SNO=? AND AU_ASSOCCODE=? AND AU_PASSWORD=?");
			pst.setString(1, password);
			pst.setString(2, adminBO.getUid());
			pst.setString(3, adminBO.getAssociateCode());
			pst.setString(4, oldPassword);
			result=pst.executeUpdate();
		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.insertORFieldsPosition--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertORFieldsPosition--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static int insertFleet (AdminRegistrationBO adminBO,String[] fleetNumber,String[] fleetName,int size)  {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			if(fleetNumber[1]!=""){
				for(int i=1;i<=size;i++){
					con.setAutoCommit(false);
					String delQuery="DELETE FROM TDS_FLEET_MASTER_ACCESS WHERE FM_FLEET_NAME= ? AND FM_MASTER_ASSOCCODE=?";
					pst = con.prepareStatement(delQuery);
					pst.setString(1, fleetNumber[i]);
					pst.setString(2, adminBO.getMasterAssociateCode());
					cat.info(pst.toString());
					pst.execute();	
					String query="INSERT INTO TDS_FLEET_MASTER_ACCESS(FM_FLEET_NAME,FM_MASTER_ASSOCCODE,FM_COMPANYNAME) VALUES (?,?,?)";
					pst=con.prepareStatement(query);
					pst.setString(1, fleetNumber[i]);
					pst.setString(2,adminBO.getMasterAssociateCode());
					pst.setString(3, fleetName[i]);
					result=pst.executeUpdate();
					con.commit();
					con.setAutoCommit(true);
				}
			}
		}catch(Exception sqex){
			//System.out.println("TDSException SystemPropertiesDAO.insertFleet--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.insertFleet--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static ArrayList getFleets(AdminRegistrationBO adminBO) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		ArrayList<FleetBO>getFleets = new ArrayList<FleetBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs= null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query="SELECT * FROM TDS_COMPANYDETAIL WHERE CD_MASTER_ASSOCCODE='"+adminBO.getMasterAssociateCode()+"' ";
		try {
			pst= con.prepareStatement(query);
			rs=pst.executeQuery();
			while (rs.next()) {
				FleetBO fleetBO =new FleetBO();
				fleetBO.setFleetNumber(rs.getString("CD_ASSOCCODE"));
				fleetBO.setFleetName(rs.getString("CD_COMPANYNAME"));
				getFleets.add(fleetBO);
			}

		}catch ( SQLException sqex) {
			//System.out.println("TDSException SystemPropertiesDAO.getFleets--->"+sqex.getMessage());
			cat.error("TDSException SystemPropertiesDAO.getFleets--->"+sqex.getMessage());
			sqex.getStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return getFleets;
	}
	public static int deleteFleet(String masteAssoccode,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		String query="DELETE FROM TDS_FLEET_MASTER_ACCESS  WHERE FM_FLEET_NAME='"+assoccode+"' AND FM_MASTER_ASSOCCODE='"+masteAssoccode+"'";
		try{
			pst = con.prepareStatement(query);
			result=pst.executeUpdate();
		}catch(Exception sqex){
			cat.error("TDSException SystemPropertiesDAO.deleteFleet-->"+sqex.getMessage());
			//System.out.println("TDSException SystemPropertiesDAO.deleteFleet-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static int updateDocumentTypes(DocumentBO docBO,AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		int result=0;
		String query="UPDATE TDS_DOCUMENTS_MASTER SET DOCUMENT_TYPE=?,SERVERITY_TYPE=?,MODULE_NAME=?,DM_DOCUMENT_BELONGS_TO=?,DM_CHECK_FOR_EXPIRY=? WHERE DM_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND MASTER_ID='"+docBO.getSerialNumber()+"'";
		try{
			pst = con.prepareStatement(query);
			pst.setString(1, docBO.getDocumentType());
			pst.setString(2, docBO.getServerityType());
			pst.setString(3, docBO.getShortName());
			pst.setString(4, docBO.getDocumentBelongsTo());
			pst.setInt(5, docBO.getCheckForExpiry());
			cat.info(pst.toString());
			result=pst.executeUpdate();
		}catch(Exception sqex){
			cat.error("TDSException SystemPropertiesDAO.updateDocumentTypes-->"+sqex.getMessage());
			//System.out.println("TDSException SystemPropertiesDAO.updateDocumentTypes-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}

	public static ArrayList<OperatorShift> driverShiftDetails(AdminRegistrationBO adminBO, String driverId,String fromDate,String toDate){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<OperatorShift> shiftList=new ArrayList<OperatorShift>();
		String query="SELECT * FROM TDS_SHIFT_REGISTER_MASTER_HISTORY WHERE SRH_ASSOCCODE='"+adminBO.getAssociateCode()+"' AND SRH_OPERATORID='"+driverId+"' AND SRH_REGISTER_STATUS='L' AND date_format(SRH_OPEN_TIME,'%m/%d/%Y') >= '"+fromDate+"' AND date_format(SRH_CLOSING_TIME,'%m/%d%Y')<='"+toDate+"'";
		try{
			pst = con.prepareStatement(query);
			//System.out.println("shift-->"+pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				OperatorShift osBean=new OperatorShift();
				osBean.setDriverId(rs.getString("SRH_OPERATORID"));
				osBean.setKey(rs.getInt("SRH_KEY"));
				osBean.setOpenTime(rs.getString("SRH_OPEN_TIME"));
				osBean.setCloseTime(rs.getString("SRH_CLOSING_TIME"));
				shiftList.add(osBean);
			}
		}catch(Exception sqex){
			cat.error("TDSException SystemPropertiesDAO.driverShiftDetails-->"+sqex.getMessage());
			//System.out.println("TDSException SystemPropertiesDAO.driverShiftDetails-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return shiftList;
	}
	public static ArrayList<String> getVersion(String associationCode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<String> version=new ArrayList<String>();
		String query="SELECT * FROM TDS_VERSION WHERE V_ASSOCCODE= '"+associationCode+"'";
		try{
			pst = con.prepareStatement(query);
			rs=pst.executeQuery();
			while(rs.next()){
				version.add(rs.getString("V_NAME"));
				version.add(rs.getString("V_VERSION"));
			}
		}catch(Exception sqex){
			cat.error("TDSException SystemPropertiesDAO.getZoneVersion-->"+sqex.getMessage());
			//System.out.println("TDSException SystemPropertiesDAO.getZoneVersion-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return version;
	}
	public static int insertMeterValues(String assoccode,ArrayList<MeterType> meterArray,int meterDefault){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst = null;
		int result=0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+assoccode+"'");
			result =pst.executeUpdate();
			for(int i=0;i<meterArray.size();i++){
				pst = con.prepareStatement("INSERT INTO TDS_METER_DETAILS (MD_ASSOCIATION_CODE,MD_RATE_PER_MILE,MD_RATE_PER_MIN,MD_START_AMOUNT,MD_MINIMUM_SPEED,MD_METER_NAME,MD_KEY,MD_DEFAULT,MD_DIS1,MD_DIS2,MD_DIS3,MD_RATE1,MD_RATE2,MD_RATE3) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
				pst.setString(1, assoccode);
				pst.setBigDecimal(2, meterArray.get(i).getRatePerMile());
				pst.setBigDecimal(3, meterArray.get(i).getRatePerMin());
				pst.setBigDecimal(4, meterArray.get(i).getStartAmt());
				pst.setInt(5, meterArray.get(i).getMinSpeed());
				pst.setString(6, meterArray.get(i).getMeterName());
				pst.setInt(7, i+1);
				pst.setInt(8, i+1==meterDefault?1:0);
				pst.setInt(9, meterArray.get(i).getDis1());
				pst.setInt(10, meterArray.get(i).getDis2());
				pst.setInt(11, meterArray.get(i).getDis3());
				pst.setBigDecimal(12, meterArray.get(i).getRate1()==null?new BigDecimal(0.00):meterArray.get(i).getRate1());
				pst.setBigDecimal(13, meterArray.get(i).getRate2()==null?new BigDecimal(0.00):meterArray.get(i).getRate2());
				pst.setBigDecimal(14, meterArray.get(i).getRate3()==null?new BigDecimal(0.00):meterArray.get(i).getRate3());
				result=pst.executeUpdate();
			}
			con.setAutoCommit(true);
		}catch(Exception sqex){
			cat.error("TDSException SystemPropertiesDAO.insertMeterValues-->"+sqex.getMessage());
			//System.out.println("TDSException SystemPropertiesDAO.insertMeterValues-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	public static ArrayList<MeterType> getMeterValues(String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst = null;
		ResultSet result;
		ArrayList<MeterType> meterDetails = new ArrayList<MeterType>();
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		String query="SELECT MD_RATE_PER_MILE,MD_RATE_PER_MIN,MD_START_AMOUNT,MD_MINIMUM_SPEED,MD_METER_NAME,MD_KEY,MD_DEFAULT,MD_DIS1,MD_DIS2,MD_DIS3,MD_RATE1,MD_RATE2,MD_RATE3 FROM TDS_METER_DETAILS WHERE MD_ASSOCIATION_CODE='"+assoccode+"'";
		try{
			pst = con.prepareStatement(query);
			result=pst.executeQuery();
			while(result.next()){
				MeterType meters = new MeterType();
				meters.setMeterName(result.getString("MD_METER_NAME"));
				meters.setRatePerMile(new BigDecimal(result.getString("MD_RATE_PER_MILE")));
				meters.setRatePerMin(new BigDecimal(result.getString("MD_RATE_PER_MIN")));
				meters.setStartAmt(new BigDecimal(result.getString("MD_START_AMOUNT")));
				meters.setMinSpeed(Integer.parseInt(result.getString("MD_MINIMUM_SPEED")));
				meters.setDis1(result.getInt("MD_DIS1"));
				meters.setDis2(result.getInt("MD_DIS2"));
				meters.setDis3(result.getInt("MD_DIS3"));
				meters.setRate1(new BigDecimal(result.getString("MD_RATE1")));
				meters.setRate2(new BigDecimal(result.getString("MD_RATE2")));
				meters.setRate3(new BigDecimal(result.getString("MD_RATE3")));
				meters.setKey(result.getInt("MD_KEY"));
				meters.setDefaultMeter(result.getInt("MD_DEFAULT"));
				meterDetails.add(meters);
			}
		}catch(Exception sqex){
			cat.error("TDSException SystemPropertiesDAO.getMeterValues-->"+sqex.getMessage());
			//System.out.println("TDSException SystemPropertiesDAO.getMeterValues-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return meterDetails;
	}


	public static int deleteVendorValue(String vendor){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		PreparedStatement pst = null;
		int result=0;
		TDSConnection dbcon = new TDSConnection();
		Connection con = dbcon.getConnection();
		try{
			con.setAutoCommit(false);
			pst=con.prepareStatement("DELETE FROM TDS_OPENREQUEST_POSITION WHERE ORP_VENDOR='"+vendor+"'");
			result =pst.executeUpdate();
			con.setAutoCommit(true);
		}catch(Exception sqex){
			//System.out.println("Error occur in---->"+sqex);
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return result;
	}
	
	





}

