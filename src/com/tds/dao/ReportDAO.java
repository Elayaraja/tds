package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.groupby.parser.JSON.JSON;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;

public class ReportDAO {
	private static Category cat = TDSController.cat;
	static {
		cat = Category.getInstance(ReportDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is " + ReportDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static ArrayList<OpenRequestBO> getHistoryForReport(AdminRegistrationBO adminBO, String fromDate, String toDate) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "") + "." + Thread.currentThread().getStackTrace()[1].getMethodName() + "--->" + startTime);
		ArrayList<OpenRequestBO> openReqHistory = new ArrayList<OpenRequestBO>();
		PreparedStatement pst = null;
		ResultSet rs = null;
		String query = "SELECT *,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + adminBO.getTimeZone() + "'),'%d-%b-%Y') AS DATE,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + adminBO.getTimeZone() + "'),'%H:%i') AS TIME,case when ORH_TRIP_SOURCE=2 THEN 'F' WHEN ORH_TRIP_SOURCE=1 THEN 'D' WHEN ORH_TRIP_SOURCE=3 THEN 'D' WHEN ORH_TRIP_SOURCE=4 THEN 'D' WHEN ORH_TRIP_SOURCE=0 THEN 'D' WHEN ORH_TRIP_SOURCE=5 THEN 'D' WHEN ORH_TRIP_SOURCE=6 THEN 'D' end as TRIPSOURCE FROM TDS_OPENREQUEST_HISTORY JOIN TDS_ADMINUSER ON ORH_DRIVERID=AU_SNO WHERE ORH_MASTER_ASSOCCODE='" + adminBO.getMasterAssociateCode() + "'";
		if (!fromDate.equals("") && !toDate.equals("")) {
			query = query + " AND ORH_SERVICEDATE <=CONVERT_TZ(STR_TO_DATE('" + toDate + " 23:59:59', '%Y-%m-%d %H:%i:%s'),'" + adminBO.getTimeZone() + "','UTC') " + " AND ORH_SERVICEDATE >=CONVERT_TZ(STR_TO_DATE('" + fromDate + " 00:00:01', '%Y-%m-%d %H:%i:%s'),'" + adminBO.getTimeZone() + "','UTC') ";
		}
		query = query + " ORDER BY ORH_SERVICEDATE";
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				OpenRequestBO openRequestBean = new OpenRequestBO();
				openRequestBean.setSdate(rs.getString("DATE"));
				openRequestBean.setShrs(rs.getString("TIME"));
				openRequestBean.setDispatchMethod(rs.getString("TRIPSOURCE"));
				openRequestBean.setName(rs.getString("AU_SOCIALSECURITY_NO"));
				openRequestBean.setQueueno(rs.getString("ORH_QUEUENO"));
				openRequestBean.setEndQueueno(rs.getString("ORH_END_QUEUNO"));
				openReqHistory.add(openRequestBean);
			}
		} catch (Exception sqex) {
			// w("TDSException RequestDAO.openRequestHistory-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "") + "." + Thread.currentThread().getStackTrace()[1].getMethodName() + "--->" + (System.currentTimeMillis() - startTime));

		return openReqHistory;
	}

	public static String daillyJobCount(String timeOffset, String assocode, String date) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT * from (SELECT case ORH_TRIP_STATUS when '40' then 'Job Allocated' when '43' then 'On Route To Pickup' when '47' then 'Trip Started' when '4' then 'Performing Dispatch Process' when '48' then 'Driver Reported No Show' when '8' then 'New Request Dispatch Process Not Started Yet' when '30' then 'Broadcast Request' when '50' then 'Customer Canceled Request' when '51' then 'No Show Request' when '52' then 'Company could not service' when '55' then 'Operator Canceled Request' when '61' then 'Trip Completed' when '99' then 'Trip On Hold' when '70' then 'Payment Received' END AS STATUS,COUNT(*) AS TOTAL FROM TDS_OPENREQUEST_HISTORY WHERE  DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') ='" + date + "' AND ORH_ASSOCCODE='" + assocode + "' GROUP BY STATUS UNION SELECT case OR_TRIP_STATUS when '40' then 'Job Allocated' when '43' then 'On Route To Pickup' when '47' then 'Trip Started' when '4' then 'Performing Dispatch Process' when '48' then 'Driver Reported No Show' when '8' then 'New Request Dispatch Process Not Started Yet' when '30' then 'Broadcast Request' when '50' then 'Customer Canceled Request' when '51' then 'No Show Request' when '52' then 'Company could not service' when '55' then 'Operator Canceled Request' when '61' then 'Trip Completed' when '70' then 'Payment Received' END AS STATUS,COUNT(*) AS TOTAL FROM TDS_OPENREQUEST WHERE DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') ='" + date + "' AND OR_ASSOCCODE='" + assocode + "' GROUP BY STATUS) AS A";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();

			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("status", rs.getString("STATUS"));
				obj.put("total", rs.getString("TOTAL"));
				array.put(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String operatorPerformance(String timeOffset, String assocode, String date) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT COUNT(*)AS AA,OR_CREATED_BY as creator FROM TDS_OPENREQUEST AS A WHERE A.OR_ASSOCCODE ='" + assocode + "' AND DATE_FORMAT(CONVERT_TZ(OR_ENTEREDTIME,'UTC','" + timeOffset + "'),'%Y-%m-%d') = '" + date + "' GROUP BY creator  UNION SELECT COUNT(*) AS BB,ORH_CREATED_BY  as creator FROM TDS_OPENREQUEST_HISTORY AS B WHERE B.ORH_ASSOCCODE ='" + assocode + "' AND DATE_FORMAT(CONVERT_TZ(ORH_ENTEREDTIME ,'UTC','" + timeOffset + "'),'%Y-%m-%d') = '" + date + "' GROUP BY creator";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();

			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("total", rs.getLong("AA") + "");
				obj.put("creator", rs.getString("creator"));
				array.put(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String jobStatistics(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT COUNT(*) AS TOTAL_JOBS,DATE(ORH_SERVICEDATE) AS SERVICEDATE,ORH_TRIP_STATUS,CASE ORH_TRIP_STATUS WHEN '50' THEN 'CUSTOMER CANCELED' WHEN '51' THEN 'NOSHOW' WHEN '52' THEN 'NO SERVICE' WHEN '61' THEN 'COMPLETED SUCCESSFULLY' WHEN '99' THEN 'TRIP ON HOLD' WHEN '55' THEN 'OPERATOR CANCELED' WHEN '3' THEN 'CANT FIND DRIVERS' WHEN '4' THEN 'PERFORMING DISPATCH PROCESS' WHEN '8' THEN 'DISPATCH PROECSS NOT STARTED' WHEN '70' THEN 'PAYMENT RECEIVED' END AS MOD_TRIP FROM TDS_OPENREQUEST_HISTORY WHERE DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' AND ORH_ASSOCCODE='" + assocode + "' GROUP BY MONTH(ORH_SERVICEDATE),ORH_TRIP_STATUS";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();

			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("totalJobs", rs.getLong("TOTAL_JOBS") + "");
				obj.put("tripStatus", rs.getString("MOD_TRIP"));
				obj.put("serviceDate", rs.getDate("SERVICEDATE"));
				array.put(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String tripDetail(String timeOffset, String assocode, String tripID) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT  CONVERT_TZ(L1.ORL_TIME,'UTC','" + timeOffset + "') as TRIPSTARTED , CONVERT_TZ(L2.ORL_TIME,'UTC','" + timeOffset + "')  as TRIPENDED,CONVERT_TZ(L3.ORL_TIME,'UTC','" + timeOffset + "') as ONSITE ,ORH_TRIP_ID  FROM TDS_OPENREQUEST_HISTORY left join TDS_OPENREQUEST_LOGS L1 ON ORH_TRIP_ID = L1.ORL_TRIPID AND ORH_ASSOCCODE=ORL_ASSOCCODE AND L1.ORL_STATUS = '35' left join TDS_OPENREQUEST_LOGS L2 ON ORH_TRIP_ID = L2.ORL_TRIPID AND L2.ORL_STATUS = '45' left join TDS_OPENREQUEST_LOGS L3 ON ORH_TRIP_ID = L2.ORL_TRIPID AND L2.ORL_STATUS = '42'  WHERE ORH_ASSOCCODE = '" + assocode + "' AND ORH_TRIP_ID =  '" + tripID + "' GROUP BY ORH_TRIP_ID";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();

			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("tripStart", rs.getString("TRIPSTARTED"));
				obj.put("tripEnd", rs.getString("TRIPENDED"));
				obj.put("onsite", rs.getString("ONSITE"));
				obj.put("tripid", rs.getString("ORH_TRIP_ID"));
				array.put(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String specailFlags(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT ORH_TRIP_ID,ORH_NAME,ORH_PHONE,CASE WHEN ORH_LANDMARK IS NOT NULL AND ORH_LANDMARK <> '' THEN ORH_LANDMARK ELSE CONCAT_WS(',',ORH_STADD1,ORH_STCITY) END AS FROMADDRESS,CASE WHEN ORH_ELANDMARK IS NOT NULL AND ORH_ELANDMARK <> '' THEN ORH_ELANDMARK ELSE CONCAT_WS(',',ORH_STADD1,ORH_STCITY) END AS TOADDRESS,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "'),'%m/%d/%Y %h:%i %p') AS SERVDATE,ORH_DRIVERID,ORH_VEHICLE_NO,ORH_AMT,ORH_PAYACC FROM TDS_OPENREQUEST_HISTORY WHERE ORH_ASSOCCODE='" + assocode + "' AND   DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' AND ORH_DRCABFLAG IN( '' ) ORDER BY ORH_SERVICEDATE";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("tripId", rs.getString("ORH_TRIP_ID"));
				obj.put("name", rs.getString("ORH_NAME"));
				obj.put("phone", rs.getString("ORH_PHONE"));
				obj.put("from", rs.getString("FROMADDRESS"));
				obj.put("to", rs.getString("TOADDRESS"));
				obj.put("date", rs.getString("SERVDATE"));
				obj.put("driverId", rs.getString("ORH_DRIVERID"));
				obj.put("vNo", rs.getString("ORH_VEHICLE_NO"));
				obj.put("amt", rs.getString("ORH_AMT"));
				obj.put("payAcc", rs.getString("ORH_PAYACC"));
				array.put(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String driverTripSheet(String timeOffset, String assocode, String date) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT CAST(TIMEDIFF(CONVERT_TZ(SRH_CLOSING_TIME,'UTC','" + timeOffset + "'),CONVERT_TZ(SRH_OPEN_TIME,'UTC','" + timeOffset + "')as CHAR) AS TIME, CONCAT(DR_FNAME,DR_LNAME) AS DR_FNAME,SRH_OPERATORID,CONVERT_TZ(SRH_OPEN_TIME,'UTC','" + timeOffset + "') as SRH_OPEN_TIME,SRH_OPENED_BY,CASE SRH_CLOSING_TIME WHEN '1970-01-01 00:00:00' THEN CONVERT_TZ(SRH_OPEN_TIME,'UTC','" + timeOffset + "')  ELSE CONVERT_TZ(SRH_CLOSING_TIME,'UTC','" + timeOffset + "') END AS SRH_CLOSING_TIME,SRH_CLOSED_BY,SRH_VEHICLE_NO FROM TDS_SHIFT_REGISTER_MASTER_HISTORY JOIN TDS_DRIVER ON SRH_OPERATORID = DR_USERID AND SRH_ASSOCCODE =  DR_ASSOCODE  AND SRH_ASSOCCODE = '" + assocode + "' AND (DATE(CONVERT_TZ(SRH_DATE,'UTC','" + timeOffset + "')) = '" + date + "'} OR DATE(CONVERT_TZ(SRH_CLOSING_TIME,'UTC','" + timeOffset + "')) ='" + date + "') ORDER BY SRH_OPERATORID";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("time", rs.getString("TIME"));
				obj.put("name", rs.getString("DR_FNAME"));
				obj.put("opId", rs.getString("SRH_OPERATORID"));
				obj.put("opTime", rs.getString("SRH_OPEN_TIME"));
				obj.put("opBy", rs.getString("SRH_OPENED_BY"));
				obj.put("cTime", rs.getString("SRH_CLOSING_TIME"));
				obj.put("cBy", rs.getString("SRH_CLOSED_BY"));
				obj.put("vNo", rs.getString("SRH_VEHICLE_NO"));
				array.put(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String ReservedJobs(String timeOffset, String assocode, String startDate, String endDate, String voucherNo) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT OR_TRIP_ID,OR_NAME,OR_PHONE,CONVERT_TZ(OR_SERVICEDATE,'UTC','" + timeOffset + "') AS SDATE,CONCAT(OR_STADD1,OR_STADD2) AS PAddress,CONCAT(OR_EDADD1,OR_EDADD2) AS DAddress,OR_AMT,OR_SPLINS,OR_VEHICLE_NO,OR_DRIVERID FROM TDS_OPENREQUEST WHERE (OR_TRIP_STATUS ='8' OR OR_TRIP_STATUS ='37') AND OR_ASSOCCODE ='" + assocode + "' AND DATE_FORMAT(CONVERT_TZ(OR_SERVICEDATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' AND OR_PAYACC ='" + voucherNo + "' ORDER BY OR_SERVICEDATE";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("tripId", rs.getString("OR_TRIP_ID"));
				obj.put("name", rs.getString("OR_NAME"));
				obj.put("phone", rs.getString("OR_PHONE"));
				obj.put("date", rs.getString("SDATE"));
				obj.put("padd", rs.getString("PAddress"));
				obj.put("dadd", rs.getString("DAddress"));
				obj.put("amt", rs.getString("OR_AMT"));
				obj.put("ins", rs.getString("OR_SPLINS"));
				obj.put("vNo", rs.getString("OR_VEHICLE_NO"));
				obj.put("driverId", rs.getString("OR_DRIVERID"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String invoiceByDriver(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "') AS PICKUP, VD_DRIVER_ID,CONCAT(DR_FNAME, DR_LNAME)AS NAME, VD_TXN_ID, VD_TRIP_ID, ORH_STADD1, VD_VOUCHERNO, VD_AMOUNT, VD_TIP FROM TDS_VOUCHER_DETAIL,TDS_OPENREQUEST_HISTORY, TDS_DRIVER WHERE ORH_TRIP_ID = VD_TRIP_ID AND VD_ASSOC_CODE = '" + assocode + "' AND DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') between '" + startDate + "' and '" + endDate + "'  AND DR_USERID = VD_DRIVER_ID ORDER BY VD_DRIVER_ID,VD_PROCESSING_DATE_TIME";
		try {

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("name", rs.getString("NAME"));
				obj.put("pDate", rs.getString("PICKUP"));
				obj.put("dId", rs.getString("VD_DRIVER_ID"));
				obj.put("trnsId", rs.getString("VD_TXN_ID"));
				obj.put("tripId", rs.getString("VD_TRIP_ID"));
				obj.put("add", rs.getString("ORH_STADD1"));
				obj.put("vouNo", rs.getString("VD_VOUCHERNO"));
				obj.put("amt", rs.getString("VD_AMOUNT"));
				obj.put("tip", rs.getString("VD_TIP"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String orangeTaxi(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT 'A ORANGE CAB' AS COMPANY,DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "'),'%d-%M-%Y') AS DATE,AU_SOCIALSECURITY_NO,CASE ORH_STADD1 WHEN 'Flag Trip' THEN 'F' ELSE 'D' END AS DISPATCH ,CONCAT(SUBSTRING(ORH_SERVICETIME,1,2),\":\",SUBSTRING(ORH_SERVICETIME,3)) AS TIME,ORH_QUEUENO,ORH_END_QUEUNO,ORH_TRIP_ID,CASE ORH_TRIP_STATUS WHEN '50' THEN 'CUSTOMER CANCELED' WHEN '51' THEN 'NOSHOW' WHEN '52' THEN 'NO SERVICE' WHEN '61' THEN 'COMPLETED SUCCESSFULLY' WHEN '99' THEN 'TRIP ON HOLD' WHEN '55' THEN 'OPERATOR CANCELED' WHEN '3' THEN 'CANT FIND DRIVERS' WHEN '4' THEN 'PERFORMING DISPATCH PROCESS' WHEN '8' THEN 'DISPATCH PROECSS NOT STARTED' END AS ORH_TRIP_STATUS FROM TDS_OPENREQUEST_HISTORY LEFT JOIN  TDS_ADMINUSER ON ORH_ASSOCCODE = AU_ASSOCCODE AND ORH_DRIVERID=AU_USERNAME WHERE ORH_ASSOCCODE = '" + assocode + "' AND DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "'";
		try {

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("date", rs.getString("DATE"));
				obj.put("ssNo", rs.getString("AU_SOCIALSECURITY_NO"));
				obj.put("dispatch", rs.getString("DISPATCH"));
				obj.put("time", rs.getString("TIME"));
				obj.put("qNo", rs.getString("ORH_QUEUENO"));
				obj.put("eQno", rs.getString("ORH_END_QUEUNO"));
				obj.put("tripId", rs.getString("ORH_TRIP_ID"));
				obj.put("tripStatus", rs.getString("ORH_TRIP_STATUS"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String driverDisbursement(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT ORH_TRIP_ID,ORH_STADD1,CONVERT_TZ(ORH_SERVICEDATE,'UTC','" + timeOffset + "') as ORH_SERVICEDATE,ORH_NAME,ORH_DRIVERID,ORH_AMT,DC_TRIP_ID,DC_DRIVER_ID,CAST(DC_AMOUNT AS CHAR) AS DC_AMOUNT,CONVERT_TZ(DC_PROCESSING_DATE_TIME,'UTC','" + timeOffset + "') AS DC_PROCESSING_DATE_TIME,DC_DRIVER_PMT_NUM,CT_DESCRIPTION   FROM TDS_DRIVER_CHARGE_CALC,TDS_OPENREQUEST_HISTORY,TDS_CHARGE_TYPE WHERE ORH_TRIP_ID = DC_TRIP_ID AND DC_TYPE_CODE = CT_KEY AND ORH_ASSOCCODE = DC_ASSOC_CODE AND  CT_ASSOCIATION_CODE =DC_ASSOC_CODE AND ORH_ASSOCCODE ='" + assocode + "' AND DATE_FORMAT(CONVERT_TZ( ORH_SERVICEDATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' ORDER BY ORH_TRIP_ID";
		try {

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("tripId", rs.getString("ORH_TRIP_ID"));
				obj.put("name", rs.getString("ORH_NAME"));
				obj.put("driverId", rs.getString("ORH_DRIVERID"));
				obj.put("sdate", rs.getString("ORH_SERVICEDATE"));
				obj.put("pDate", rs.getString("DC_PROCESSING_DATE_TIME"));
				obj.put("cType", rs.getString("CT_DESCRIPTION"));
				obj.put("amt", rs.getString("DC_AMOUNT"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String dailyDispactcPerformance(String timeOffset, String assocode, String startDate, String endDate, int query) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query1 = "select tripId,AU_FNAME,DATE(SERVICEDATE),LAT,LON,EDLAT,EDLONGI,TIME_TO_SEC(TIMEDIFF(ACCEPTEDTIME,SERVICEDATE)) AS TIMETOALLOCATE,TIME_TO_SEC(TIMEDIFF(pickup,SERVICEDATE)) AS TIMETOPICKUP from(select OR_TRIP_ID as tripId,OR_SERVICEDATE As SERVICEDATE,OR_JOB_ACCEPT_TIME AS ACCEPTEDTIME,OR_JOB_PICKUP_TIME AS pickup,OR_DRIVERID AS driverId,OR_STLATITUDE AS LAT,OR_STLONGITUDE AS LON,OR_EDLATITUDE AS EDLAT, OR_EDLONGITUDE AS EDLONGI from TDS_OPENREQUEST WHERE OR_TRIP_STATUS IN(40,43,47,4) AND OR_MASTER_ASSOCCODE='" + assocode + "'   UNION select ORH_TRIP_ID as tripId,ORH_SERVICEDATE As SERVICEDATE,ORH_JOB_ACCEPT_TIME AS ACCEPTEDTIME,ORH_JOB_PICKUP_TIME AS pickup,ORH_DRIVERID AS driverId,ORH_STLATITUDE AS LAT,ORH_STLONGITUDE AS LONGI,ORH_EDLATITUDE AS EDLAT, ORH_EDLONGITUDE AS EDLONGI from TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_STATUS =61 AND ORH_MASTER_ASSOCCODE='" + assocode + "' ) AS A LEFT JOIN TDS_ADMINUSER B ON driverId=B.AU_SNO WHERE A.ACCEPTEDTIME!=\"\" AND DATE_FORMAT(CONVERT_TZ(SERVICEDATE, 'UTC','" + timeOffset + "'),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' and B.AU_MASTER_ASSOCCODE='" + assocode + "'ORDER BY driverId";
		String query2 = "select AU_FNAME,AVG(TIME_TO_SEC(TIMEDIFF(ACCEPTEDTIME,SERVICEDATE))) AS TIMETOALLOCATE,AVG(TIME_TO_SEC(TIMEDIFF(pickup,SERVICEDATE))) AS TIMETOPICKUP from(select OR_TRIP_ID as tripId,OR_SERVICEDATE As SERVICEDATE,OR_JOB_ACCEPT_TIME AS ACCEPTEDTIME,OR_JOB_PICKUP_TIME AS pickup,OR_DRIVERID AS driverId,OR_STLATITUDE AS LAT,OR_STLONGITUDE AS LON,OR_EDLATITUDE AS EDLAT, OR_EDLONGITUDE AS EDLONGI from TDS_OPENREQUEST WHERE OR_TRIP_STATUS IN(40,43,47,4) AND OR_MASTER_ASSOCCODE='" + assocode + "'   UNION select ORH_TRIP_ID as tripId,ORH_SERVICEDATE As SERVICEDATE,ORH_JOB_ACCEPT_TIME AS ACCEPTEDTIME,ORH_JOB_PICKUP_TIME AS pickup,ORH_DRIVERID AS driverId,ORH_STLATITUDE AS LAT,ORH_STLONGITUDE AS LONGI,ORH_EDLATITUDE AS EDLAT, ORH_EDLONGITUDE AS EDLONGI from TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_STATUS =61 AND ORH_MASTER_ASSOCCODE='" + assocode + "' ) AS A LEFT JOIN TDS_ADMINUSER B ON driverId=B.AU_SNO WHERE A.ACCEPTEDTIME!=\"\" AND DATE_FORMAT(CONVERT_TZ(SERVICEDATE, 'UTC','" + timeOffset + "'),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' and B.AU_MASTER_ASSOCCODE='" + assocode + "' GROUP BY AU_FNAME";
		try {
			if (query == 1) {
				pst = conn.prepareStatement(query1);
				rs = pst.executeQuery();
				while (rs.next()) {
					JSONObject obj = new JSONObject();
					obj.put("tripid", rs.getString("tripId"));
					obj.put("name", rs.getString("AU_FNAME"));
					obj.put("date", rs.getString("DATE(SERVICEDATE)"));
					obj.put("lat", rs.getString("LAT"));
					obj.put("lon", rs.getString("LON"));
					obj.put("elat", rs.getString("EDLAT"));
					obj.put("elon", rs.getString("EDLONGI"));
					obj.put("timeToAllocate", rs.getString("TIMETOALLOCATE"));
					obj.put("timeToPickup", rs.getString("TIMETOPICKUP"));
					array.put(obj);
				}
			} else if (query == 2) {
				pst = conn.prepareStatement(query2);
				rs = pst.executeQuery();
				while (rs.next()) {
					JSONObject obj = new JSONObject();
					obj.put("name", rs.getString("AU_FNAME"));
					obj.put("timeToAllocate", rs.getString("TIMETOALLOCATE"));
					obj.put("timeToPickup", rs.getString("TIMETOPICKUP"));
					array.put(obj);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String dispatcherShiftDetails(String timeOffset, String assocode, String date, int query) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query1 = "SELECT CONCAT(AU_FNAME,' ',AU_LNAME) AS OPERATOR,SRH_OPERATORID,CAST(TIMEDIFF(CONVERT_TZ(SRH_CLOSING_TIME,'UTC','" + timeOffset + "'),CONVERT_TZ(SRH_OPEN_TIME,'UTC','" + timeOffset + "'))AS CHAR) AS TOTALTIME,CONVERT_TZ(SRH_OPEN_TIME,'UTC','" + timeOffset + "') AS OPEN,CASE SRH_CLOSING_TIME WHEN '1970-01-01 00:00:00' THEN CONVERT_TZ(SRH_OPEN_TIME,'UTC','" + timeOffset + "')  ELSE CONVERT_TZ(SRH_CLOSING_TIME,'UTC','" + timeOffset + "') END AS CLOSE,SRH_OPENED_BY,SRH_CLOSED_BY FROM TDS_SHIFT_REGISTER_MASTER_HISTORY   JOIN TDS_ADMINUSER ON SRH_OPERATORID = AU_SNO AND AU_ASSOCCODE = SRH_ASSOCCODE AND  AU_USERTYPE ='Operator'   WHERE AU_ASSOCCODE ='" + assocode + "' AND DATE(CONVERT_TZ(SRH_DATE,'UTC','" + timeOffset + "')) = '" + date + "'";
		String query2 = "SELECT COUNT(*) as TOTAL,ORH_CREATED_BY FROM TDS_OPENREQUEST_HISTORY WHERE ORH_ASSOCCODE ='" + assocode + "' AND DATE(DATE_SUB(ORH_SERVICEDATE,INTERVAL -'" + timeOffset + "' HOUR))= '" + date + "'  GROUP BY ORH_CREATED_BY";
		try {
			if (query == 1) {
				pst = conn.prepareStatement(query1);
				rs = pst.executeQuery();
				while (rs.next()) {
					JSONObject obj = new JSONObject();
					obj.put("op", rs.getString("OPERATOR"));
					obj.put("opId", rs.getString("SRH_OPERATORID"));
					obj.put("tTim", rs.getString("TOTALTIME"));
					obj.put("oTim", rs.getString("OPEN"));
					obj.put("cTim", rs.getString("CLOSE"));
					obj.put("oBy", rs.getString("SRH_OPENED_BY"));
					obj.put("cBy", rs.getString("SRH_CLOSED_BY"));
					array.put(obj);
				}
			} else if (query == 2) {
				pst = conn.prepareStatement(query2);
				rs = pst.executeQuery();
				while (rs.next()) {
					JSONObject obj = new JSONObject();
					obj.put("optr", rs.getString("ORH_CREATED_BY"));
					obj.put("total", rs.getString("TOTAL"));
					array.put(obj);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String responseOfACallByCity(String timeOffset, String assocode, String startDate, String endDate, int query) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query1 = "select ORH_STCITY AS CITY,ORH_TRIP_ID AS TRIP, DATE_SUB(ORH_SERVICEDATE,INTERVAL -'" + timeOffset + "' HOUR) AS SDATE,TIME_TO_SEC(TIMEDIFF( ORH_JOB_ACCEPT_TIME,ORH_SERVICEDATE)) AS TIMETOALLOCATE,TIME_TO_SEC(TIMEDIFF(ORH_JOB_PICKUP_TIME,ORH_SERVICEDATE)) AS TIMETOPICKUP ,ORH_DRIVERID AS DRIVER,ORH_STLATITUDE AS STARTLAT ,ORH_STLONGITUDE AS STARTLONG,ORH_EDLATITUDE AS EDLAT , ORH_EDLONGITUDE AS EDLONG  from TDS_OPENREQUEST_HISTORY   AS A  LEFT JOIN TDS_ADMINUSER AS B ON ORH_DRIVERID=B.AU_SNO    WHERE ORH_JOB_ACCEPT_TIME!=\"\" AND DATE_FORMAT(DATE_SUB(ORH_SERVICEDATE,INTERVAL -'" + timeOffset + "'} HOUR),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' and AU_ASSOCCODE='" + assocode + "' AND ORH_TRIP_STATUS =61 AND ORH_ASSOCCODE='" + assocode + "' AND ORH_PREMIUM_CUSTOMER=0 AND ORH_JOB_ACCEPT_TIME  IS NOT NULL AND ORH_JOB_PICKUP_TIME IS NOT NULL AND ORH_DONT_DISPATCH=0 ORDER BY ORH_STCITY";
		String query2 = "select AU_FNAME,CITY, AVG(TIMETOALLOCATE),AVG(TIMETOPICKUP) from (select tripId, AU_FNAME,CITY,SERVICEDATE,LAT,LON,EDLAT,EDLONGI,TIME_TO_SEC(TIMEDIFF(ACCEPTEDTIME,SERVICEDATE)) AS TIMETOALLOCATE,TIME_TO_SEC(TIMEDIFF(pickup,SERVICEDATE)) AS TIMETOPICKUP from(select OR_TRIP_ID as tripId,OR_SERVICEDATE As SERVICEDATE,OR_JOB_ACCEPT_TIME AS ACCEPTEDTIME,OR_JOB_PICKUP_TIME AS pickup,OR_DRIVERID AS driverId,OR_STLATITUDE AS LAT,OR_STLONGITUDE AS LON,OR_EDLATITUDE AS EDLAT, OR_EDLONGITUDE AS EDLONGI,OR_STCITY AS CITY from TDS_OPENREQUEST WHERE OR_TRIP_STATUS IN(40,43,47,4) AND OR_ASSOCCODE='" + assocode + "'  UNION select ORH_TRIP_ID as tripId,ORH_SERVICEDATE As SERVICEDATE,ORH_JOB_ACCEPT_TIME AS ACCEPTEDTIME,ORH_JOB_PICKUP_TIME AS pickup,ORH_DRIVERID AS driverId,ORH_STLATITUDE AS LAT,ORH_STLONGITUDE AS LONGI,ORH_EDLATITUDE AS EDLAT, ORH_EDLONGITUDE AS EDLONGI,ORH_STCITY AS CITY from TDS_OPENREQUEST_HISTORY WHERE ORH_TRIP_STATUS =61 AND ORH_ASSOCCODE='" + assocode + "' ) AS A LEFT JOIN TDS_ADMINUSER B ON driverId=B.AU_SNO   WHERE A.ACCEPTEDTIME!=\"\" AND  DATE_FORMAT(SERVICEDATE,'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' and B.AU_ASSOCCODE='" + assocode + "' ORDER BY driverId ) AS C GROUP BY CITY";
		try {
			if (query == 1) {
				pst = conn.prepareStatement(query1);
				rs = pst.executeQuery();
				while (rs.next()) {
					JSONObject obj = new JSONObject();
					obj.put("tripId", rs.getString("TRIP"));
					obj.put("city", rs.getString("CITY"));
					obj.put("date", rs.getString("SDATE"));
					obj.put("slat", rs.getString("STARTLAT"));
					obj.put("slon", rs.getString("STARTLONG"));
					obj.put("elat", rs.getString("EDLAT"));
					obj.put("elon", rs.getString("EDLONG"));
					obj.put("tta", rs.getString("TIMETOALLOCATE"));
					obj.put("ttp", rs.getString("TIMETOPICKUP"));
					array.put(obj);
				}
			} else if (query == 2) {
				pst = conn.prepareStatement(query2);
				rs = pst.executeQuery();
				while (rs.next()) {
					JSONObject obj = new JSONObject();
					obj.put("name", rs.getString("AU_FNAME"));
					obj.put("city", rs.getString("CITY"));
					obj.put("tta", rs.getString("AVG(TIMETOALLOCATE)"));
					obj.put("ttp", rs.getString("AVG(TIMETOPICKUP)"));
					array.put(obj);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}

	public static String DriverDeactivationHistory(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT CONCAT(DR_FNAME,DR_LNAME) AS NAME,DH_DRIVER_ID,DH_DESCR,CONVERT_TZ(DH_FROM_DATE_TIME,'UTC','" + timeOffset + "') AS FROMDATE,CONVERT_TZ(DH_TO_DATE_TIME,'UTC','" + timeOffset + "') AS TODATE FROM TDS_DRIVER_DEACTIVATION_HISTOR LEFT JOIN TDS_DRIVER ON DH_DRIVER_ID= DR_USERID AND DH_ASSOCCODE = DR_ASSOCODE  WHERE DATE_FORMAT(CONVERT_TZ(DH_ENTERED_DATE,'UTC','" + timeOffset + "'),'%Y-%m-%d') BETWEEN '" + startDate + "' AND '" + endDate + "' AND DH_ASSOCCODE ='" + assocode + "' ORDER BY DH_ENTERED_DATE";
		try {

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("name", rs.getString("NAME"));
				obj.put("drId", rs.getString("DH_DRIVER_ID"));
				obj.put("des", rs.getString("DH_DESCR"));
				obj.put("fDate", rs.getString("FROMDATE"));
				obj.put("tDate", rs.getString("TODATE"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
	
	
	public static String OperatorTODriverJobCount(String timeOffset, String assocode, String date) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT COUNT(ORH_DRIVERID) AS TOTAL ,ORH_DRIVERID,ORH_CREATED_BY,CONCAT(AU_FNAME,\" \",AU_LNAME) AS OPERATOR,CONCAT(DR_FNAME,\" \",DR_LNAME) AS DRIVER FROM TDS_OPENREQUEST_HISTORY LEFT JOIN TDS_ADMINUSER ON  AU_SNO =ORH_CREATED_BY  AND AU_ASSOCCODE = ORH_ASSOCCODE LEFT JOIN TDS_DRIVER ON  DR_USERID =ORH_DRIVERID  AND DR_ASSOCODE = ORH_ASSOCCODE WHERE DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y-%m-%d') = '"+date+"' AND ORH_ASSOCCODE ='"+assocode+"' GROUP BY ORH_DRIVERID,ORH_CREATED_BY ORDER BY ORH_CREATED_BY";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("opr", rs.getString("OPERATOR"));
				obj.put("oprId", rs.getString("ORH_CREATED_BY"));
				obj.put("driver", rs.getString("DRIVER"));
				obj.put("driverId", rs.getString("ORH_DRIVERID"));
				obj.put("total", rs.getString("TOTAL"));
				array.put(obj);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
	
	
	public static String manuallyChangedTrip(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT ORL_TRIPID,DATE_FORMAT(CONVERT_TZ(ORL_TIME,'UTC','"+timeOffset+"'),'%m/%d/%Y %h:%i %p') AS LOGTIME,ORL_REASON,ORL_CHANGED_BY FROM TDS_OPENREQUEST_LOGS WHERE ORL_MASTER_ASSOCCODE = '"+assocode+"' AND DATE_FORMAT(CONVERT_TZ(ORL_TIME,'UTC','"+timeOffset+"'),'%Y-%m-%d') >= '"+startDate+"' AND DATE_FORMAT(CONVERT_TZ(ORL_TIME,'UTC','"+timeOffset+"'),'%Y-%m-%d') <= '"+endDate+"' AND ORL_STATUS='105'";
		try {

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("tripId", rs.getString("ORL_TRIPID"));
				obj.put("time", rs.getString("LOGTIME"));
				obj.put("by", rs.getString("ORL_CHANGED_BY"));
				obj.put("reason", rs.getString("ORL_REASON"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
	
	public static String driverShiftDetail(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT SRH_OPERATORID,DATE_FORMAT(CONVERT_TZ(SRH_OPEN_TIME,'UTC','"+timeOffset+"'),'%m/%d/%Y %h:%i %p') AS STARTTIME, DATE_FORMAT(CONVERT_TZ(SRH_CLOSING_TIME,'UTC','"+timeOffset+"'),'%m/%d/%Y %h:%i %p') AS ENDTIME, DATE_FORMAT(CONVERT_TZ(SRDH_OPEN_TIME,'UTC','"+timeOffset+"'),'%m/%d/%Y %h:%i %p') AS OPENTIME, DATE_FORMAT(CONVERT_TZ(SRDH_CLOSING_TIME,'UTC','"+timeOffset+"'),'%m/%d/%Y %h:%i %p') AS CLOSETIME,SRH_VEHICLE_NO,CASE SRDH_REGISTER_STATUS WHEN 'B' THEN 'Break' END AS STATUS FROM TDS_SHIFT_REGISTER_MASTER_HISTORY LEFT JOIN TDS_SHIFT_REGISTER_DETAIL_HISTORY ON SRDH_OPERATORID=SRH_OPERATORID AND SRDH_ASSOCCODE = SRH_ASSOCCODE AND DATE(SRDH_OPEN_TIME) = DATE(SRH_OPEN_TIME) WHERE DATE_FORMAT(CONVERT_TZ(SRH_OPEN_TIME,'UTC','"+timeOffset+"'),'%Y-%m-%d') >= '"+startDate+"' AND DATE_FORMAT(CONVERT_TZ(SRH_OPEN_TIME,'UTC','"+timeOffset+"'),'%Y-%m-%d') <= '"+endDate+"' AND SRH_ASSOCCODE = '"+assocode+"' AND SRDH_REGISTER_STATUS='B' ORDER BY ABS(SRH_OPERATORID)";
		try {

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("drId", rs.getString("SRH_OPERATORID"));
				obj.put("vNo", rs.getString("SRH_VEHICLE_NO"));
				obj.put("sTim", rs.getString("STARTTIME"));
				obj.put("eTim", rs.getString("ENDTIME"));
				obj.put("boTim", rs.getString("OPENTIME"));
				obj.put("bcTim", rs.getString("CLOSETIME"));
				obj.put("status", rs.getString("STATUS"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
	
	
	public static String driverLogouts(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT DL_DRIVERID,LOGOUTTIME,NAME,DL_REASON FROM(SELECT DL_DRIVERID,CONVERT_TZ(DL_TIME,'UTC','"+timeOffset+"') AS LOGOUTTIME,CONCAT(AU_FNAME,\" \",AU_LNAME)AS NAME,DL_LOGGEDOUT_BY,DL_REASON FROM TDS_DRIVER_LOGOUT LEFT JOIN TDS_ADMINUSER ON AU_SNO=DL_LOGGEDOUT_BY LEFT JOIN TDS_COMPANYDETAIL ON CD_MASTER_ASSOCCODE = DL_COMPANY_CODE WHERE CONVERT_TZ(DL_TIME,'UTC','"+timeOffset+"') BETWEEN '"+startDate+"' AND '"+endDate+"' AND DL_COMPANY_CODE='"+assocode+"' GROUP BY DL_DRIVERID,LOGOUTTIME ORDER BY DL_DRIVERID,LOGOUTTIME DESC) AS A ";
		try {

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("drId", rs.getString("DL_DRIVERID"));
				obj.put("loTim", rs.getString("LOGOUTTIME"));
				obj.put("loBy", rs.getString("NAME"));
				obj.put("reas", rs.getString("DL_REASON"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
	
	
	public static String tripLogs(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT ORL_TRIPID,ORL_CHANGED_BY,CONVERT_TZ(ORL_TIME,'UTC','US/EASTERN') AS ORL_TIME,ORL_REASON FROM TDS_OPENREQUEST_LOGS WHERE ORL_ASSOCCODE='"+assocode+"' AND DATE_FORMAT(DATE_SUB(ORL_TIME,INTERVAL -'"+timeOffset+"' HOUR),'%Y-%m-%d') BETWEEN '"+startDate+"' AND '"+endDate+"' ORDER BY ORL_TRIPID;";
		try {

			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("tripId", rs.getString("ORL_TRIPID"));
				obj.put("cngBy", rs.getString("ORL_CHANGED_BY"));
				obj.put("tim", rs.getString("ORL_TIME"));
				obj.put("reas", rs.getString("ORL_REASON"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
	
	
	public static String monthlyConsolidatedStatisticsGraph(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT MONTH(ORH_SERVICEDATE) AS MONTHNAME, CASE ORH_TRIP_STATUS WHEN '50' THEN 'CUSTOMER CANCELLED' WHEN '51' THEN 'NOSHOW' WHEN '52' THEN 'NO SERVICE' WHEN '61' THEN 'COMPLETED SUCCESSFULLY' WHEN '99' THEN 'TRIP ON HOLD' WHEN '55' THEN 'OPERATOR CANCELED' WHEN '3' THEN 'CANT FIND DRIVERS' WHEN '4' THEN 'PERFORMING DISPATCH PROCESS' WHEN '8' THEN 'DISPATCH PROECSS NOT STARTED' WHEN '70' THEN 'PAYMENT RECEIVED' END AS MOD_TRIP,COUNT(*) AS TOTAL_JOBS FROM TDS_OPENREQUEST_HISTORY WHERE DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y-%m-%d') BETWEEN '"+startDate+"' AND '"+endDate+"' AND ORH_ASSOCCODE='"+assocode+"' GROUP BY MONTH(ORH_SERVICEDATE),MOD_TRIP ORDER BY MONTH(ORH_SERVICEDATE)";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("mnth", rs.getString("MONTHNAME"));
				obj.put("trpSts", rs.getString("MOD_TRIP"));
				obj.put("tot", rs.getString("TOTAL_JOBS"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
	
	
	public static String monthlyJobCountGraph(String timeOffset, String assocode, String startDate, String endDate) {
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		JSONArray array = new JSONArray();
		String query = "SELECT ORH_SERVICEDATE, CASE ORH_TRIP_STATUS WHEN '50' THEN 'CUSTOMER CANCELLED' WHEN '51' THEN 'NOSHOW' WHEN '52' THEN 'NO SERVICE' WHEN '61' THEN 'COMPLETED SUCCESSFULLY' WHEN '99' THEN 'TRIP ON HOLD' WHEN '55' THEN 'OPERATOR CANCELED' WHEN '3' THEN 'CANT FIND DRIVERS' WHEN '4' THEN 'PERFORMING DISPATCH PROCESS' WHEN '8' THEN 'DISPATCH PROECSS NOT STARTED' WHEN '70' THEN 'PAYMENT RECEIVED' ELSE 'UNKNOWN' END AS MOD_TRIP,COUNT(*) AS TOTAL_JOBS FROM TDS_OPENREQUEST_HISTORY WHERE DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y-%m-%d') BETWEEN '"+startDate+"' AND '"+endDate+"' AND ORH_ASSOCCODE='"+assocode+"' GROUP BY MONTH(ORH_SERVICEDATE),MOD_TRIP ORDER BY MONTH(ORH_SERVICEDATE)";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("date", rs.getString("ORH_SERVICEDATE"));
				obj.put("trpSts", rs.getString("MOD_TRIP"));
				obj.put("tot", rs.getString("TOTAL_JOBS"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
	
	public static String DriverJobDetails(String timeOffset, String assocode, String startDate, String endDate) {
		System.out.println("");
		TDSConnection dbConn = new TDSConnection();
		Connection conn = dbConn.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		JSONArray array = new JSONArray();
		String query = "select ORH_DRIVERID,ORH_TRIP_ID,ORH_CREATED_BY,CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"')AS ORH_SERVICEDATE,ORL_REASON,ORH_STADD1,ORH_EDADD1  from TDS_OPENREQUEST_HISTORY LEFT JOIN TDS_OPENREQUEST_LOGS ON ORH_ASSOCCODE =ORL_ASSOCCODE AND ORH_TRIP_ID = ORL_TRIPID WHERE ORL_ASSOCCODE='"+assocode+"' AND DATE_FORMAT(CONVERT_TZ(ORH_SERVICEDATE,'UTC','"+timeOffset+"'),'%Y-%m-%d') BETWEEN '"+startDate+"' AND '"+endDate+"' AND (ORL_REASON='Job Accepted' OR ORL_REASON='Job Rejected') AND ORH_DRIVERID<>''ORDER BY ORH_DRIVERID,ORH_TRIP_ID,ORH_SERVICEDATE";
		try {
			pst = conn.prepareStatement(query);
			rs = pst.executeQuery();
			while (rs.next()) {
				JSONObject obj = new JSONObject();
				obj.put("dvrId", rs.getString("ORH_DRIVERID"));
				obj.put("trpId", rs.getString("ORH_TRIP_ID"));
				obj.put("crtBy", rs.getString("ORH_CREATED_BY"));
				obj.put("serDat", rs.getString("ORH_SERVICEDATE"));
				obj.put("res", rs.getString("ORL_REASON"));
				obj.put("sAdd", rs.getString("ORH_STADD1"));
				obj.put("eAdd", rs.getString("ORH_EDADD1"));
				array.put(obj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException j) {
			j.printStackTrace();
		} finally {
			dbConn.closeConnection();
		}
		return array.toString();
	}
}
