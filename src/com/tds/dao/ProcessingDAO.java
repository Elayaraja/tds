package com.tds.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.tdsBO.VoucherBO;
import com.common.util.TDSValidation;



public class ProcessingDAO {

	private static Category cat =TDSController.cat;
	static {
		cat = Category.getInstance(ProcessingDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ProcessingDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static String getCardIssuer(String card_no)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.getCardIssuer");

		String card_issuer = "";
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("select CM_CARD_ISSUER from TDS_CARD_MASTER where CM_CARD_NO='"+card_no+"'");
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();

			if(m_rs.first())
				card_issuer = m_rs.getString("CM_CARD_ISSUER");


		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.getCardIssuer-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ProcessingDAO.getCardIssuer-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return card_issuer;
	}

	public static String getCardType(String card_no)
	{
		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.getCardType");

		String card_type = "";
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("select B_CARD_TYPE from TDS_BIN where B_CARDNO='"+card_no+"'");
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();

			if(m_rs.first()){
				card_type = m_rs.getString("B_CARD_TYPE");
			}


		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.getCardType-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ProcessingDAO.getCardType-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return card_type;
	}

	/*public static void insertPaymentIntoWork(PaymentProcessBean processBean,String path,ApplicationPoolBO poolBO, AdminRegistrationBO adminBO) {

		cat.info("TDS INFO ProcessingDAO.insertPaymentIntoWork");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			//m_conn.setAutoCommit(false);

			m_pst = m_conn.prepareStatement("insert into  TDS_WORK (TW_DRIVERID, TW_TRIPID,TW_CARDISSUER," +
					"TW_CARDTYPE,TW_SWIPETYPE,TW_CARDNO,TW_AMOUNT,TW_TIPAMOUNT,TW_TOTALAMOUNT,TW_TRANSID ,TW_PROCESSING_DATE_TIME,TW_APPROVALSTATUS,TW_APPROVALCODE,TW_RIDERSIGN,TW_STATUSCODE, TW_ASSOCCODE ) values" +
			"(?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?)");

			m_pst.setString(1, processBean.getB_driverid());
			m_pst.setString(2, processBean.getB_trip_id());
			m_pst.setString(3, processBean.getB_card_issuer());
			m_pst.setString(4, processBean.getB_card_type());
			m_pst.setString(5, processBean.getB_paymentType());
			m_pst.setString(6, processBean.getB_card_no());
			m_pst.setDouble(7, Double.parseDouble(processBean.getB_amount()));
			m_pst.setDouble(8, Double.parseDouble(processBean.getB_tip_amt()));
			m_pst.setDouble(9, Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt())  );
			m_pst.setString(10, processBean.getB_trans_id());
			m_pst.setString(11, processBean.getB_approval_status());
			m_pst.setString(12, processBean.getB_approval_code());			 
			m_pst.setBytes(13, TDSValidation.getDefaultsign(path));
			m_pst.setString(14, processBean.getStatus_code());
			m_pst.setString(15, adminBO.getAssociateCode());
			cat.info(m_pst.toString());
			m_pst.execute();
			//m_conn.commit();
			//m_conn.setAutoCommit(true);
		} catch (SQLException sqex) {
			//TODO Check with Vijayan
			// This code makes a second try to enter a payment record into the work table if the first (above attempt) failed due to weird and rare connection errors
			// In this code, the prev conneciton is and new connection is reopened.
			//if(m_conn!=null)
			//{
			try {
				//m_conn.rollback();
				if(m_conn!=null){
					m_conn.close();
				}
				m_dbConn.closeConnection();

				m_dbConn  = new TDSConnection();
				m_conn = m_dbConn.getConnection();

				m_pst = m_conn.prepareStatement("insert into  TDS_WORK (TW_DRIVERID,TW_TRIPID,TW_CARDISSUER," +
						"TW_CARDTYPE,TW_SWIPETYPE,TW_CARDNO,TW_AMOUNT,TW_TIPAMOUNT,TW_TOTALAMOUNT,TW_TRANSID ,TW_PROCESSING_DATE_TIME,TW_APPROVALSTATUS,TW_APPROVALCODE,TW_RIDERSIGN,TW_STATUSCODE, TW_ASSOCCODE ) values" +
				"(?,?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?)");

				m_pst.setString(1, processBean.getB_driverid());
				m_pst.setString(2, processBean.getB_trip_id());
				m_pst.setString(3, processBean.getB_card_issuer());
				m_pst.setString(4, processBean.getB_card_type());
				m_pst.setString(5, processBean.getB_paymentType());
				m_pst.setString(6, processBean.getB_card_no());
				m_pst.setDouble(7, Double.parseDouble(processBean.getB_amount()));
				m_pst.setDouble(8, Double.parseDouble(processBean.getB_tip_amt()));
				m_pst.setDouble(9, Double.parseDouble(processBean.getB_amount())+Double.parseDouble(processBean.getB_tip_amt())  );
				m_pst.setString(10, processBean.getB_trans_id());
				m_pst.setString(11, processBean.getB_approval_status());
				m_pst.setString(12, processBean.getB_approval_code());			 
				m_pst.setBytes(13, TDSValidation.getDefaultsign(path));
				m_pst.setString(14, processBean.getStatus_code());
				m_pst.setString(15, adminBO.getAssociateCode());
				cat.info(m_pst.toString());
				m_pst.execute();
				m_conn.close();
			} catch (SQLException e) {
				cat.error("TDSException ProcessingDAO.insertPaymentIntoWork-->"+e.getMessage());
				System.out.println("TDSException ProcessingDAO.insertPaymentIntoWork.SecontTime-->"+e.getMessage());
				e.printStackTrace();

				try {
					if(m_conn!=null)
					{
						m_conn.close();
					}
				} catch (SQLException e1) {
					// TODO: handle exception
				}

				e.printStackTrace();
			}
			
			//} 

			ArrayList al = new ArrayList();
			al.add("100999");
			OneToOneSMS oneDirect = new OneToOneSMS(al,sqex.getMessage(),poolBO,1);
			oneDirect.start();

			cat.error("TDSException ProcessingDAO.insertPaymentIntoWork-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.insertPaymentIntoWork-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}

	}


	public static void updateSignIntoWork(byte[] b,String trans_id) {
		cat.info("TDS INFO ProcessingDAO.updateSignIntoWork");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		Blob blob = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("update TDS_WORK set TW_RIDERSIGN = ? where TW_TRANSID = ?");
			m_pst.setBytes(1, b==null?new byte[0]:b);
			m_pst.setString(2, trans_id);
			int st = m_pst.executeUpdate();
			if(st == 0)
			{
				m_pst = m_conn.prepareStatement("update TDS_APPROVAL set AP_RIDER_SIGNATURE = ? where AP_TRANS_ID = ?");
				m_pst.setBytes(1, b==null?new byte[0]:b);
				m_pst.setString(2, trans_id);
				cat.info(m_pst.toString());
				st = m_pst.executeUpdate();
				if(st == 0)
				{
					m_pst = m_conn.prepareStatement("update TDS_PAYMENT_SETTLED_DETAIL set PSD_RIDER_SIGNATURE = ? where PSD_TRANS_ID = ?");
					m_pst.setBytes(1, b==null?new byte[0]:b);
					m_pst.setString(2, trans_id);
					cat.info(m_pst.toString());
					st = m_pst.executeUpdate();

				}
			}

		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.updateSignIntoWork-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.updateSignIntoWork-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}

	}

	public static int alterTransct(String tip_amount, String amt,String trans_id,String cap_tran_id,byte b[]) {
		cat.info("TDS INFO ProcessingDAO.alterTransct");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		Blob blob;
		int status = 0; 
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("update TDS_WORK set TW_TIPAMOUNT = ?,TW_AMOUNT=?,TW_TOTALAMOUNT = ?,TW_CAPTUREID =?,TW_CAPTUREDATE=now(),TW_AUTHCAPFLG='C',TW_RIDERSIGN=? where TW_TRANSID = ?");
			m_pst.setDouble(1, Double.parseDouble(tip_amount));
			m_pst.setDouble(2, Double.parseDouble(amt));
			m_pst.setDouble(3, (Double.parseDouble(amt)+Double.parseDouble(tip_amount)));
			m_pst.setString(4, cap_tran_id);
			//blob = new SerialBlob(b==null?new byte[0]:b);
			//m_pst.setBlob(5, blob);
			m_pst.setBytes(5, b==null?new byte[0]:b);
			m_pst.setString(6, trans_id);
			cat.info(m_pst.toString());
			status = m_pst.executeUpdate();

		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.alterTransct-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.alterTransct-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;

	}
*/
	public static VoucherBO getVaidityOfVoucher(String vno,String assoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO ProcessingDAO.getVaidityOfVoucher");
		boolean status = false;
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		VoucherBO  voucherBO = null;
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("select * from TDS_GOVT_VOUCHER where VC_VOUCHERNO=? and VC_ASSOCCODE = ? and  VC_EXPIRYDATE >= date_format(now(),'%Y%m%d') and VC_SWITH = 'P'" );
			m_pst.setString(1, vno);
			m_pst.setString(2, assoccode);
			cat.info(m_pst.toString());
			//System.out.println("vouchervalidity query:"+m_pst.toString());
			m_rs = m_pst.executeQuery();

			if(m_rs.first())
			{
				voucherBO = new VoucherBO();
				voucherBO.setVamount(m_rs.getString("VC_AMOUNT"));
				voucherBO.setVname(m_rs.getString("VC_RIDERNAME"));
				voucherBO.setVdesc(m_rs.getString("VC_DESCR"));
				voucherBO.setNoTip(m_rs.getInt("VC_TIP_ALLOWED"));
				voucherBO.setDriverComments(m_rs.getString("VC_DRIVER_COMMENTS"));
				voucherBO.setOperatorComments(m_rs.getString("VC_OPERATOR_COMMENTS"));
				voucherBO.setV_status(true);
				voucherBO.setFixedAmount(m_rs.getString("VC_FIXED_RATE"));
				voucherBO.setCcNum(m_rs.getString("VC_INFOKEY"));
				if(voucherBO.getCcNum()!=null && voucherBO.getCcNum()!=""){
					voucherBO.setProcessMethod(1);
				}
			} else {
				m_pst = m_conn.prepareStatement("select case VC_EXPIRYDATE < date_format(now(),'%Y%m%d') when true then concat('Voucher Expired on ',date_format(VC_EXPIRYDATE,'%m/%d/%Y')) else case VC_SWITH when 'A'  then concat('Voucher Used on ',date_format(VC_CREATION_DATE,'%m/%d/%Y')) else 'Voucher Not Found' end end as reason from  TDS_GOVT_VOUCHER where VC_VOUCHERNO=? and VC_ASSOCCODE = ? " );
				m_pst.setString(1, vno);
				m_pst.setString(2, assoccode);
				m_pst.setString(1, vno);
				m_pst.setString(2, assoccode);
				cat.info(m_pst.toString());
				//System.out.println("vouchervalidity query:"+m_pst.toString());
				m_rs = m_pst.executeQuery();

				if(m_rs.first())
				{
					voucherBO = new VoucherBO();
					voucherBO.setV_status(false);
					voucherBO.setVreason(m_rs.getString("reason"));
				} 

			}


		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.getVaidityOfVoucher-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ProcessingDAO.getVaidityOfVoucher-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucherBO;
	}


/*	public static ArrayList getUnCaptureTransactionToReverse(String fr_date,String to_date,String amount,String assoccode,String driver)
	{
		cat.info("TDS INFO ProcessingDAO.getUnCaptureTransactionToReverse");
		ArrayList al_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;

		try {
			String query="";
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			query = "select " +
			"	concat(TW_DRIVERID,'-',DR_FNAME) as Driver,TW_TRIPID as Trip,TW_CARDISSUER as c_issuer,TW_AUTHCAPFLG," +
			"	TW_CARDTYPE as card_type,TW_SWIPETYPE  as s_type,TW_CARDNO as card_no," +
			"	format(TW_AMOUNT,2) as amount,format(TW_TIPAMOUNT,2) as tip,format(TW_TOTALAMOUNT,2) as total," +
			"	TW_TRANSID as trans_id,date_format(TW_PROCESSING_DATE_TIME,'%m/%d/%Y') as date," +
			"	TW_APPROVALSTATUS as status,0 as per,0 as txn_amt," +
			"	0 as total,TW_VOID_FLG,date_format(TW_VOID_DATE,'%m/%d/%Y %h:%s %p') as TW_VOID_DATE,TW_VOIDED_FLG " +
			" from " +
			"	TDS_WORK,TDS_ADMINUSER,TDS_DRIVER  WHERE DR_SNO = AU_SNO and  AU_SNO = TW_DRIVERID and AU_ASSOCCODE ='"+ assoccode + "'  and TW_APPROVALSTATUS = 1 and TW_AUTHCAPFLG!='C' and " +
			" TW_VOID_FLG !=1 ";

			if(driver!=""){
				query+=" AND TW_DRIVERID ='"+driver+"'";
			}

			if(fr_date!="" && query.length() > 0){
				query+=" AND date_format(TW_PROCESSING_DATE_TIME,'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(fr_date)+"'";
			}
			if(to_date!="" && query.length() > 0){
				query+=" AND date_format(TW_PROCESSING_DATE_TIME,'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(to_date)+"'";
			}
			if(amount!="" && query.length() > 0){
				query+=" AND TW_AMOUNT >='"+amount+"'";
			}


			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();

			while(m_rs.next())
			{
				TransactionSummaryBean summaryBean = new TransactionSummaryBean();
				summaryBean.setT_amout(m_rs.getString("amount"));
				summaryBean.setT_card_no(m_rs.getString("card_no"));
				summaryBean.setT_processDate(m_rs.getString("date"));
				summaryBean.setT_tip(m_rs.getString("tip"));
				summaryBean.setT_trip(m_rs.getString("Trip"));
				summaryBean.setT_trans_id(m_rs.getString("trans_id"));
				summaryBean.setT_total(m_rs.getString("total"));
				summaryBean.setT_authcapflg(m_rs.getString("TW_AUTHCAPFLG"));
				summaryBean.setT_driver(m_rs.getString("driver"));
				summaryBean.setT_voidedflg(m_rs.getBoolean("TW_VOID_FLG"));
				summaryBean.setT_voidedflg(m_rs.getBoolean("TW_VOIDED_FLG"));
				summaryBean.setT_voiddate(m_rs.getString("TW_VOID_DATE"));
				al_list.add(summaryBean);
			}


		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.getUnCaptureTransactionToReverse-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.getUnCaptureTransactionToReverse-->"+sqex.getMessage());
			sqex.printStackTrace();

		} finally {
			m_dbConn.closeConnection();
		}

		return al_list;
	}*/


	/*public static ArrayList getTransactionToReverse(int start,int end,String fr_date,String to_date,String amount,String assoccode,String driver,String approval_code,String card_no,String approval)
	{
		cat.info("TDS INFO ProcessingDAO.getTransactionToReverse");
		ArrayList al_list = new ArrayList();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;

		try {
			String query="";
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			query = "select AP_APPROVALCODE," +
			"	concat(AP_DRIVER_ID,'-',DR_FNAME) as Driver,AP_TRIP_ID as Trip,AP_CARD_ISSUER as c_issuer," +
			"	AP_CARD_TYPE as card_type,AP_SWIPETYPE as s_type,AP_CARD_NO as card_no," +
			"	format(AP_AMOUNT,2) as amount,format(AP_TIP_AMOUNT,2) as tip,format(AP_TOTAL_AMOUNT,2) as total," +
			"	AP_CAPTUREID as trans_id,date_format(AP_PROCESSING_DATE_TIME,'%m/%d/%Y') as date," +
			"	AP_APPROVAL_STATUS as status,AP_TXN_PERCENT_AMT as per,AP_TXN_AMT as txn_amt," +
			"	AP_TOTAL_AMOUNT_WITH_TXN as total,AP_VOID_FLG,date_format(AP_VOID_DATE,'%m/%d/%Y %h:%s %p') as AP_VOID_DATE,AP_VOIDED_FLG,AP_AUTHCAPFLG " +
			" from " +
			"	TDS_APPROVAL,TDS_DRIVER  WHERE DR_SNO = AP_DRIVER_ID and AP_COMPANY_ID='"+ assoccode + "' ";

			if(driver!="" && driver!=null){
				query+=" AND AP_DRIVER_ID ='"+driver+"'";
			}

			if(fr_date!="" && fr_date!=null){
				if( query.length() > 0) {
					query+=" AND date_format(AP_PROCESSING_DATE_TIME,'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(fr_date)+"'";
				}
			}
			if(to_date!="" &&  to_date!=null){ 
				if( query.length() > 0) {
					query+=" AND date_format(AP_PROCESSING_DATE_TIME,'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(to_date)+"'";
				}
			}
			if(amount!="" &&  amount!=null  ){
				if(query.length() > 0) {
					query+=" AND AP_TOTAL_AMOUNT_WITH_TXN >='"+amount+"'";
				}
			}
			if(approval_code!="" &&  approval_code!=null){
				if(   approval_code.length() > 0) {
					query+=" AND AP_APPROVALCODE ='"+approval_code+"'";
				}
			}
			if(card_no!="" &&  card_no!=null ){
				if(  card_no.length() > 0) {
					query+=" AND AP_CARD_NO ='"+card_no+"'";
				}
			}
			if(approval ==null)
			{
				query+="  and AP_APPROVAL_STATUS =1";
			}

			query = query + " order by AP_PROCESSING_DATE_TIME desc " ;

			query = query + " limit "+start+","+end+"";
			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();

			while(m_rs.next())
			{
				TransactionSummaryBean summaryBean = new TransactionSummaryBean();
				summaryBean.setT_amout(m_rs.getString("amount"));
				summaryBean.setT_card_no(m_rs.getString("card_no"));
				summaryBean.setT_processDate(m_rs.getString("date"));
				summaryBean.setT_tip(m_rs.getString("tip"));
				summaryBean.setT_trip(m_rs.getString("Trip"));
				summaryBean.setT_trans_id(m_rs.getString("trans_id"));
				summaryBean.setT_total(m_rs.getString("total"));
				summaryBean.setT_driver(m_rs.getString("driver"));
				summaryBean.setT_authcapflg(m_rs.getString("AP_AUTHCAPFLG"));
				summaryBean.setT_voidflg(m_rs.getBoolean("AP_VOID_FLG"));
				summaryBean.setT_voidedflg(m_rs.getBoolean("AP_VOIDED_FLG"));
				summaryBean.setT_voiddate(m_rs.getString("AP_VOID_DATE"));
				summaryBean.setT_approval_code(m_rs.getString("AP_APPROVALCODE"));
				summaryBean.setT_approval_status(m_rs.getString("status"));
				al_list.add(summaryBean);
			}

			m_pst.close();

			query = "select PSD_APPROVALCODE," +
			"	concat(PSD_DRIVER_ID,'-',DR_FNAME) as Driver,PSD_TRIP_ID as Trip,PSD_CARD_ISSUER as c_issuer," +
			"	PSD_CARD_TYPE as card_type,PSD_SWIPETYPE  as s_type,PSD_CARD_NO as card_no," +
			"	format(PSD_AMOUNT,2) as amount,format(PSD_TIP_AMOUNT,2) as tip,format(PSD_TOTAL_AMOUNT,2) as total," +
			"	PSD_CAPTUREID as trans_id,date_format(PSD_PROCESSING_DATE_TIME,'%m/%d/%Y') as date," +
			"	PSD_APPROVAL_STATUS as status,PSD_TXN_PERCENT_AMT as per,PSD_TXN_AMT as txn_amt," +
			"	PSD_TOTAL_AMOUNT_WITH_TXN as total,PSD_VOID_FLG,date_format(PSD_VOID_DATE,'%m/%d/%Y %h:%s %p') as PSD_VOID_DATE,PSD_VOIDED_FLG,PSD_AUTHCAPFLG " +
			" from " +
			"	TDS_PAYMENT_SETTLED_DETAIL,TDS_DRIVER  WHERE DR_SNO = PSD_DRIVER_ID and PSD_COMPANY_ID='"+ assoccode + "' ";

			if(driver!="" && driver!=null){
				query+=" AND PSD_DRIVER_ID ='"+driver+"'";
			}

			if(fr_date!="" &&  fr_date!=null){
				if( query.length() > 0) {
					query+=" AND date_format(PSD_PROCESSING_DATE_TIME,'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(fr_date)+"'";
				}
			}
			if(to_date!="" &&  to_date!=null){
				if( query.length() > 0) {
					query+=" AND date_format(PSD_PROCESSING_DATE_TIME,'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(to_date)+"'";
				}
			}
			if(amount!="" &&  amount!=null ){
				if( query.length() > 0) {
					query+=" AND PSD_AMOUNT >='"+amount+"'";
				}
			}
			if(approval_code!=""  &&   approval_code!=null  ){
				if(approval_code.length() > 0) {
					query+=" AND PSD_APPROVALCODE ='"+approval_code+"'";
				}
			}
			if(card_no!="" &&   card_no!=null ){
				if(card_no.length() > 0) {
					query+=" AND PSD_CARD_NO ='"+card_no+"'";
				} 
			}
			if(approval ==null)
			{
				query+="  and PSD_APPROVAL_STATUS =1";
			}

			query  = query + " order by PSD_PROCESSING_DATE_TIME desc " ;

			query = query + " limit "+start+","+end+"";

			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();

			while(m_rs.next())
			{
				TransactionSummaryBean summaryBean = new TransactionSummaryBean();
				summaryBean.setT_amout(m_rs.getString("amount"));
				summaryBean.setT_card_no(m_rs.getString("card_no"));
				summaryBean.setT_processDate(m_rs.getString("date"));
				summaryBean.setT_tip(m_rs.getString("tip"));
				summaryBean.setT_trip(m_rs.getString("Trip"));
				summaryBean.setT_trans_id(m_rs.getString("trans_id"));
				summaryBean.setT_total(m_rs.getString("total"));
				summaryBean.setT_driver(m_rs.getString("driver"));
				summaryBean.setT_authcapflg(m_rs.getString("PSD_AUTHCAPFLG"));
				summaryBean.setT_voidflg(m_rs.getBoolean("PSD_VOID_FLG"));
				summaryBean.setT_voidedflg(m_rs.getBoolean("PSD_VOIDED_FLG"));
				summaryBean.setT_voiddate(m_rs.getString("PSD_VOID_DATE"));
				summaryBean.setT_approval_code(m_rs.getString("PSD_APPROVALCODE"));
				summaryBean.setT_approval_status(m_rs.getString("status"));
				al_list.add(summaryBean);
			}	

			query = "select TW_APPROVALCODE," +
			"	concat(TW_DRIVERID,'-',DR_FNAME) as Driver,TW_TRIPID as Trip,TW_CARDISSUER as c_issuer,TW_AUTHCAPFLG," +
			"	TW_CARDTYPE as card_type,TW_SWIPETYPE  as s_type,TW_CARDNO as card_no," +
			"	format(TW_AMOUNT,2) as amount,format(TW_TIPAMOUNT,2) as tip,format(TW_TOTALAMOUNT,2) as total," +
			"	case TW_AUTHCAPFLG when 'C' then TW_CAPTUREID else TW_TRANSID end as trans_id,date_format(TW_PROCESSING_DATE_TIME,'%m/%d/%Y') as date," +
			"	TW_APPROVALSTATUS as status,0 as per,0 as txn_amt," +
			"	0 as total,TW_VOID_FLG,date_format(TW_VOID_DATE,'%m/%d/%Y %h:%s %p') as TW_VOID_DATE,TW_VOIDED_FLG " +
			" from " +
			"	TDS_WORK,TDS_ADMINUSER,TDS_DRIVER  WHERE DR_SNO = AU_SNO and  AU_SNO = TW_DRIVERID and AU_ASSOCCODE ='"+ assoccode + "' ";

			if(driver!="" && driver!=null ){
				query+=" AND TW_DRIVERID ='"+driver+"'";
			}

			if(fr_date!="" &&  fr_date!=null ){
				if(query.length() > 0) {
					query+=" AND date_format(TW_PROCESSING_DATE_TIME,'%Y%m%d') >= '"+TDSValidation.getDBdateFormat(fr_date)+"'";
				}
			}
			if(to_date!="" &&  to_date!=null ){
				if( query.length() > 0) {
					query+=" AND date_format(TW_PROCESSING_DATE_TIME,'%Y%m%d') <= '"+TDSValidation.getDBdateFormat(to_date)+"'";
				}
			}
			if(amount!="" &&   amount!=null ){
				if(query.length() > 0) {
					query+=" AND TW_AMOUNT >='"+amount+"'";
				}
			}
			if(approval_code!="" &&    approval_code!=null  ){
				if( approval_code.length() > 0) {
					query+=" AND TW_APPROVALCODE ='"+approval_code+"'";
				}
			}

			if(card_no!=""  &&    card_no!=null ){
				if(card_no.length() > 0) {
					query+=" AND TW_CARDNO ='"+card_no+"'";
				}
			}
			if(approval ==null)
			{
				query+="  and TW_APPROVALSTATUS =1";
			}

			query = query + " order by TW_PROCESSING_DATE_TIME desc" ;

			query = query + " limit "+start+","+end+"";

			m_pst = m_conn.prepareStatement(query);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();

			while(m_rs.next())
			{
				TransactionSummaryBean summaryBean = new TransactionSummaryBean();
				summaryBean.setT_amout(m_rs.getString("amount"));
				summaryBean.setT_card_no(m_rs.getString("card_no"));
				summaryBean.setT_processDate(m_rs.getString("date"));
				summaryBean.setT_tip(m_rs.getString("tip"));
				summaryBean.setT_trip(m_rs.getString("Trip"));
				summaryBean.setT_trans_id(m_rs.getString("trans_id"));
				summaryBean.setT_total(m_rs.getString("total"));
				summaryBean.setT_authcapflg(m_rs.getString("TW_AUTHCAPFLG"));
				summaryBean.setT_driver(m_rs.getString("driver"));
				summaryBean.setT_voidflg(m_rs.getBoolean("TW_VOID_FLG"));
				summaryBean.setT_voidedflg(m_rs.getBoolean("TW_VOIDED_FLG"));
				summaryBean.setT_voiddate(m_rs.getString("TW_VOID_DATE"));
				summaryBean.setT_approval_code(m_rs.getString("TW_APPROVALCODE"));
				summaryBean.setT_approval_status(m_rs.getString("status"));
				al_list.add(summaryBean);
			}


		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.getTransactionToReverse-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.getTransactionToReverse-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}

		return al_list;
	}*/

	/*public static boolean voidTransAction(String trans_id,String desc)
	{
		cat.info("TDS INFO ProcessingDAO.voidTransAction");
		boolean status = false;

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();

			m_conn.setAutoCommit(false);

			m_pst = m_conn.prepareStatement("select * from TDS_WORK where TW_CAPTUREID = '"+trans_id+"'");			
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();

			if(m_rs.first())
			{
				status = true;
				m_pst = m_conn.prepareStatement("update TDS_WORK set TW_VOID_FLG = true,TW_VOID_DATE = now() where TW_CAPTUREID = '"+trans_id+"'");
				m_pst.execute();

				m_pst = m_conn.prepareStatement("insert into TDS_WORK select TW_DRIVERID,TW_ASSOCCODE, TW_TRIPID,TW_CARDISSUER,TW_CARDTYPE,TW_SWIPETYPE,TW_CARDNO,-TW_AMOUNT,-TW_TIPAMOUNT,-TW_TOTALAMOUNT,concat('R',TW_TRANSID),now(),TW_APPROVALSTATUS,TW_APPROVALCODE,TW_RIDERSIGN,TW_CAPTUREID,TW_CAPTUREDATE,TW_AUTHCAPFLG,false,'0000-00-00 00:00:00',true,TW_STATUSCODE from TDS_WORK where TW_CAPTUREID = '"+ trans_id+"'");
				m_pst.execute();
			} 
			
			m_pst = m_conn.prepareStatement("insert into TDS_VOID_DESC values(?,?)");
			m_pst.setString(1, trans_id);
			m_pst.setString(2, desc);
			cat.info(m_pst.toString());
			m_pst.execute();

			m_conn.commit();
			m_conn.setAutoCommit(true);

		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.voidTransAction-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.voidTransAction-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {

			try {
				m_conn.rollback();
			} catch (SQLException e) {
				// TODO: handle exception
			}
			m_dbConn.closeConnection();
		}

		return status;
	}*/

	/*public static boolean alterTransAction(String trans_id,String alter_amount,String tip_amount)
	{

		boolean status = true;

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;

		//int flg = alterTransct(tip_amount, alter_amount, trans_id);
		//if(flg!=1)
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("select * from TDS_APPROVAL ad,TDS_COMPANY_CC_SETUP cc where ad.AP_TRANS_ID ='"+trans_id+"' and cc.CCC_COMPANY_ID = ad.AP_COMPANY_ID");
			m_rs = m_pst.executeQuery();

			if(m_rs.first())
			{
				status = true;
				m_pst = m_conn.prepareStatement("update TDS_APPROVAL set AP_AMOUNT = ("+Double.parseDouble(alter_amount)+"),AP_TIP_AMOUNT=("+Double.parseDouble(tip_amount)+"),AP_TOTAL_AMOUNT=("+Double.parseDouble(alter_amount)+" + "+Double.parseDouble(tip_amount)+"),AP_TXN_PERCENT_AMT = (case AP_APPROVAL_STATUS when 1 then ((("+Double.parseDouble(alter_amount)+" + "+Double.parseDouble(tip_amount)+") - AP_TXN_AMT )*("+m_rs.getDouble("CCC_TXN_AMT_PERCENTAGE")+"/100))  else 0 end),AP_TOTAL_AMOUNT_WITH_TXN = (("+Double.parseDouble(alter_amount)+" + "+Double.parseDouble(tip_amount)+") - AP_TXN_AMT ) -  (case AP_APPROVAL_STATUS when 1 then ((("+Double.parseDouble(alter_amount)+" + "+Double.parseDouble(tip_amount)+") - AP_TXN_AMT )*("+m_rs.getDouble("CCC_TXN_AMT_PERCENTAGE")+"/100))  else 0 end)   where AP_TRANS_ID = '"+ trans_id+"'");
				m_pst.execute();
			} else {
				status = true;
				m_pst = m_conn.prepareStatement("select * from TDS_PAYMENT_SETTLED_DETAIL where PSD_TRANS_ID ='"+trans_id+"'");
				m_rs = m_pst.executeQuery();
				if(m_rs.first()) 
				{
					m_pst = m_conn.prepareStatement("insert into TDS_APPROVAL select PSD_DRIVER_ID,PSD_COMPANY_ID,PSD_TRIP_ID,PSD_CARD_ISSUER,PSD_CARD_TYPE,PSD_SWIPETYPE,PSD_CARD_NO,(("+Double.parseDouble(alter_amount)+" + "+Double.parseDouble(tip_amount)+")  - PSD_TOTAL_AMOUNT),0 as PSD_TIP_AMOUNT,(("+Double.parseDouble(alter_amount)+" + "+Double.parseDouble(tip_amount)+")  - PSD_TOTAL_AMOUNT),concat('R',PSD_TRANS_ID),now(),PSD_APPROVAL_STATUS,PSD_APPROVALCODE,0 as PSD_TXN_PERCENT_AMT,0 as PSD_TXN_AMT,(("+Double.parseDouble(alter_amount)+" + "+Double.parseDouble(tip_amount)+")  - PSD_TOTAL_AMOUNT),PSD_RIDER_SIGNATURE,PSD_REC_SET from TDS_PAYMENT_SETTLED_DETAIL where PSD_TRANS_ID = '"+ trans_id+"'");
					m_pst.execute();
				}

			}


		} catch (SQLException p_sqex) {
			System.out.println("Error in getTransactionToReverse "+p_sqex.getMessage() +"\n"+p_sqex.getStackTrace());
		} finally {
			m_dbConn.closeConnection();
		}
		return status;
	}*/

	   public static VoucherBO updateVoucherDetail(String vocher_no,String assoccode,String trip,String driverid,String amt,String tip,byte[] b,String path,String riderName)
	   {
		   long startTime = System.currentTimeMillis();
			cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
	       cat.info("TDS INFO ProcessingDAO.updateVoucherDetail");
	       VoucherBO voucherBO = new VoucherBO();

	       TDSConnection m_dbConn = null;
	       Connection m_conn = null;
	       PreparedStatement m_pst = null;
	       ResultSet 	rs = null;
	       String key;
	       BigDecimal totalAmt = BigDecimal.ZERO;
	       totalAmt=totalAmt.add(new BigDecimal(amt)).add(new BigDecimal(tip));
	       m_dbConn  = new TDSConnection();
	       m_conn = m_dbConn.getConnection();
	       try {
	           m_pst = m_conn.prepareStatement("update TDS_GOVT_VOUCHER set VC_TRIP_ID = ?,  VC_SWITH = case VC_FREQ when 1 then 'A' else 'P' end where VC_VOUCHERNO = ? and VC_ASSOCCODE = ?");
	           m_pst.setString(1, trip);
	           m_pst.setString(2, vocher_no);
	           m_pst.setString(3, assoccode);
	           cat.info(m_pst.toString());
	           m_pst.executeUpdate();
	           //key = ""+ConfigDAO.tdsKeyGen("VOUCHER_TRANS_ID", m_conn);
	           m_pst = m_conn.prepareStatement("insert into TDS_VOUCHER_DETAIL(VD_TRIP_ID,VD_VOUCHERNO,VD_RIDER_NAME,VD_COSTCENTER,VD_COMPANY_CODE,VD_ASSOC_CODE,VD_DESCR,VD_PROCESSING_DATE_TIME,VD_DELAYDAYS,VD_DRIVER_ID,VD_RIDER_SIGN,VD_AMOUNT,VD_TIP,VD_TOTAL,VD_EXPIRY_DATE,VD_USER,VD_CONTACT,VD_FREQUENCY,VD_MASTER_ASSOCCODE) select ?,VC_VOUCHERNO,?,VC_COSTCENTER,VC_CCODE,VC_ASSOCCODE,VC_DESCR,NOW(),VC_DELAYDAYS,?,?,?,?,?,VC_EXPIRYDATE,VC_USER,VC_CONTACT,VC_FREQ,VC_ASSOCCODE from TDS_GOVT_VOUCHER where VC_VOUCHERNO = ? and VC_ASSOCCODE = ?",Statement.RETURN_GENERATED_KEYS);
	           m_pst.setString(1, trip );
	           m_pst.setString(2, riderName );
	           m_pst.setString(3, driverid);
	           m_pst.setBytes(4,  TDSValidation.getDefaultsign(path));
	           m_pst.setBytes(4,  b);
	           m_pst.setString(5,amt);
	           m_pst.setString(6, tip);
	           m_pst.setString(7, totalAmt+"" );
	           m_pst.setString(8, vocher_no );
	           m_pst.setString(9, assoccode);
	           cat.info(m_pst.toString());
	           //System.out.println("query---->"+m_pst.toString());
	           m_pst.execute();
	           voucherBO.setVamount(amt);
	           voucherBO.setTip(tip);
	           voucherBO.setV_status(true);
	           rs = m_pst.getGeneratedKeys();
	           if(rs.next()){
	        	   voucherBO.setVtrans(rs.getString(1));
	           }
	       } catch (SQLException sqex) {
	           voucherBO.setV_status(false);
	           cat.error("TDSException ProcessingDAO.updateVoucherDetail-->"+sqex.getMessage());
	           cat.error(m_pst.toString());
	        //   System.out.println("TDSException ProcessingDAO.updateVoucherDetail-->"+sqex.getMessage());
	           sqex.printStackTrace();
	       } finally {
	           m_dbConn.closeConnection();
	       }
			cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	       return voucherBO;
	   }

	public static void updateVoucherSign(String transid,byte[] b)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.updateVoucherSign");

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		/*ResultSet m_rs = null;
		String key;*/
		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection(); 

			m_pst = m_conn.prepareStatement("update TDS_VOUCHER_DETAIL set VD_RIDER_SIGN = ? where VD_TXN_ID = ?  ");
			m_pst.setBytes(1, b==null?new byte[0]:b);
			m_pst.setString(2, transid);
			//System.out.println("query : "+m_pst.toString());
			
			cat.info(m_pst.toString());
			m_pst.executeUpdate();

			/*if(st == 0)
			{
				m_pst = m_conn.prepareStatement("update TDS_VOUCHER_SETELED_DETAIL set VSD_RIDER_SIGN = ? where VSD_TRANS_ID = ?  ");
				m_pst.setBytes(1, b==null?new byte[0]:b);
				m_pst.setString(2, transid);
				cat.info(m_pst.toString());
				st = m_pst.executeUpdate();

			}*/

		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.updateVoucherSign-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ProcessingDAO.updateVoucherSign-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}


	/*public static PaymentProcessBean chckCapture(PaymentProcessBean processBean)
	{

		cat.info("TDS INFO ProcessingDAO.chckCapture");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("select TW_AMOUNT,TW_TIPAMOUNT,TW_RIDERSIGN,case TW_AUTHCAPFLG when 'C' then 1 else 0 end as flg from TDS_WORK where TW_TRANSID = ?");
			m_pst.setString(1, processBean.getB_trans_id());
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			if(m_rs.first() && m_rs.getInt("flg") == 1)
			{
				processBean.setReturnById(true);
				processBean.setB_amount(m_rs.getString("TW_AMOUNT"));
				processBean.setB_tip_amt(m_rs.getString("TW_TIPAMOUNT"));
				processBean.setB_sign(m_rs.getBytes("TW_RIDERSIGN"));
				processBean.setError_log("0");

			}else if(m_rs.first() && m_rs.getInt("flg") == 0){
				processBean.setB_amount(m_rs.getString("TW_AMOUNT"));
				processBean.setB_tip_amt(m_rs.getString("TW_TIPAMOUNT"));
				processBean.setB_sign(m_rs.getBytes("TW_RIDERSIGN"));
				processBean.setReturnById(false);
				processBean.setError_log("0");
			} else {
				processBean.setReturnById(true);
				processBean.setError_log("1");
			}


		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDProcessingDAO.chckCaptureAO.chckCapture-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.chckCapture-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		return processBean;
	}

*/	
	/*public static PaymentProcessBean chckReturnOrUndo(PaymentProcessBean processBean)
	{

		cat.info("TDS INFO ProcessingDAO.chckReturnOrUndo");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("select TW_AMOUNT,TW_TIPAMOUNT,TW_RIDERSIGN,case TW_AUTHCAPFLG when 'C' then 1 else 0 end as flg from TDS_WORK where case TW_AUTHCAPFLG when 'C' then TW_CAPTUREID else TW_TRANSID end = ?");
			m_pst.setString(1, processBean.getB_trans_id());
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			if(m_rs.first() && m_rs.getInt("flg") == 1)
			{

				processBean.setB_amount(m_rs.getString("TW_AMOUNT"));
				processBean.setB_tip_amt(m_rs.getString("TW_TIPAMOUNT"));
				processBean.setB_sign(m_rs.getBytes("TW_RIDERSIGN"));
				processBean.setReturnById(true);
				processBean.setTxn_total(""+(Double.parseDouble(m_rs.getString("TW_AMOUNT"))+Double.parseDouble(m_rs.getString("TW_TIPAMOUNT"))));
				processBean.setError_log("0");

			}else if(m_rs.first() && m_rs.getInt("flg") == 0){
				processBean.setB_amount(m_rs.getString("TW_AMOUNT"));
				processBean.setB_tip_amt(m_rs.getString("TW_TIPAMOUNT"));
				processBean.setTxn_total(""+(Double.parseDouble(m_rs.getString("TW_AMOUNT"))+Double.parseDouble(m_rs.getString("TW_TIPAMOUNT"))));
				processBean.setB_sign(m_rs.getBytes("TW_RIDERSIGN"));
				processBean.setReturnById(false);
				processBean.setError_log("0");
			} else {

				m_pst = m_conn.prepareStatement("select AP_AMOUNT,AP_TIP_AMOUNT,AP_RIDER_SIGNATURE from TDS_APPROVAL where AP_CAPTUREID  = ?");
				m_pst.setString(1, processBean.getB_trans_id());
				cat.info(m_pst.toString());
				m_rs = m_pst.executeQuery();
				if(m_rs.first())
				{
					processBean.setB_amount(m_rs.getString("AP_AMOUNT"));
					processBean.setB_tip_amt(m_rs.getString("AP_TIP_AMOUNT"));
					processBean.setTxn_total(m_rs.getString("AP_TOTAL_AMOUNT_WITH_TXN"));
					processBean.setB_sign(m_rs.getBytes("AP_RIDER_SIGNATURE"));
					processBean.setReturnById(true);
					processBean.setError_log("0");
				} else {

					m_pst = m_conn.prepareStatement("select PSD_AMOUNT,PSD_TIP_AMOUNT,PSD_RIDER_SIGNATURE from TDS_PAYMENT_SETTLED_DETAIL where PSD_CAPTUREID  = ?");
					m_pst.setString(1, processBean.getB_trans_id());
					cat.info(m_pst.toString());
					m_rs = m_pst.executeQuery();
					if(m_rs.first())
					{
						processBean.setB_amount(m_rs.getString("PSD_AMOUNT"));
						processBean.setB_tip_amt(m_rs.getString("PSD_TIP_AMOUNT"));
						processBean.setTxn_total(m_rs.getString("PSD_TOTAL_AMOUNT_WITH_TXN"));
						processBean.setB_sign(m_rs.getBytes("PSD_RIDER_SIGNATURE"));
						processBean.setReturnById(true);
						processBean.setError_log("0");
					} else {
						processBean.setReturnById(true);
						processBean.setError_log("1");
					}

				}

			}


		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.chckReturnOrUndo-->"+sqex.getMessage());
			System.out.println("TDSException ProcessingDAO.chckReturnOrUndo-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		return processBean;
	}*/

	public static void insertManualCCLog(PaymentProcessBean processBean)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.insertManualCCLog");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("insert into TDS_MANUAL_CREDIT (OPERATOR_ID,DRIVER_ID,CREDIT_CARD_NUMBER,DATE_TIME,AMOUNT,TIP_AMT,DESCRIPTION,TRANS_ID) values (?,?,?,now(),?,?,?,?)");
			m_pst.setString(1, processBean.getB_operator());
			m_pst.setString(2, processBean.getB_driverid());
			m_pst.setString(3, processBean.getB_card_no());
			m_pst.setDouble(4, Double.parseDouble(processBean.getB_amount()));
			m_pst.setDouble(5, Double.parseDouble(processBean.getB_tip_amt()));
			m_pst.setString(6, processBean.getB_desc());
			m_pst.setString(7, processBean.getB_trans_id());
			cat.info(m_pst.toString());
			m_pst.execute();

		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.insertManualCCLog-->"+sqex.getMessage());
			cat.error(m_pst.toString());
			//System.out.println("TDSException ProcessingDAO.insertManualCCLog-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	public static void insertManualCCErrorLog(PaymentProcessBean processBean)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.insertManualCCLog");
		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;

		try {
			m_dbConn  = new TDSConnection();
			m_conn = m_dbConn.getConnection();
			m_pst = m_conn.prepareStatement("insert into TDS_MANUAL_CREDIT_ERROR (OPERATOR_ID,DRIVER_ID,CREDIT_CARD_NUMBER,DATE_TIME,AMOUNT,TIP_AMT,DESCRIPTION) values (?,?,?,now(),?,?,?)");
			m_pst.setString(1, processBean.getB_operator());
			m_pst.setString(2, processBean.getB_driverid());
			m_pst.setString(3, processBean.getB_card_no());
			m_pst.setDouble(4, Double.parseDouble(processBean.getB_amount()));
			m_pst.setDouble(5, Double.parseDouble(processBean.getB_tip_amt()));
			m_pst.setString(6, processBean.getB_desc());
			//m_pst.setString(7, processBean.getB_trans_id());
			cat.info(m_pst.toString());
			m_pst.execute();

		} catch (SQLException sqex) {
			cat.error("TDSException ProcessingDAO.insertManualCCLog-->"+sqex.getMessage());
			cat.error(m_pst.toString());
		//	System.out.println("TDSException ProcessingDAO.insertManualCCErrorLog-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}

	public static int insertVoucherDetail(String vocher_no,String assoccode,String trip,String driverid,String amt,String tip,byte[] b,String path,String riderName,String masterAssoccode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.insertVoucherDetail");
		VoucherBO voucherBO = new VoucherBO();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		ResultSet rs,rs1=null;
		int key=0;
		BigDecimal totalAmt = BigDecimal.ZERO;
		totalAmt = (new BigDecimal(amt)).add(new BigDecimal(tip));
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("SELECT VD_TXN_ID FROM TDS_VOUCHER_DETAIL WHERE VD_TRIP_ID='"+trip+"' AND VD_VOUCHERNO='"+vocher_no+"'");
			rs1=m_pst.executeQuery();
			if(rs1.next()){
				key=rs1.getInt("VD_TXN_ID");
			} else {
				m_pst = m_conn.prepareStatement("update TDS_GOVT_VOUCHER set VC_TRIP_ID = ?,  VC_SWITH = case VC_FREQ when 1 then 'A' else 'P' end where VC_VOUCHERNO = ? and VC_ASSOCCODE = ?");
				m_pst.setString(1, trip);
				m_pst.setString(2, vocher_no);
				m_pst.setString(3, assoccode);
				cat.info(m_pst.toString());
				m_pst.executeUpdate();
				m_pst = m_conn.prepareStatement("insert into TDS_VOUCHER_DETAIL(VD_TRIP_ID,VD_VOUCHERNO,VD_RIDER_NAME,VD_COSTCENTER,VD_COMPANY_CODE,VD_ASSOC_CODE,VD_DESCR,VD_PROCESSING_DATE_TIME,VD_DELAYDAYS,VD_DRIVER_ID,VD_RIDER_SIGN,VD_AMOUNT,VD_TIP,VD_TOTAL,VD_EXPIRY_DATE,VD_USER,VD_CONTACT,VD_FREQUENCY,VD_PAYMENT_RECEIVED_STATUS,VD_IMAGE_TYPE,VD_MASTER_ASSOCCODE) select ?,VC_VOUCHERNO,?,VC_COSTCENTER,VC_CCODE,?,VC_DESCR,NOW(),VC_DELAYDAYS,?,?,?,?,?,VC_EXPIRYDATE,VC_USER,VC_CONTACT,VC_FREQ,1,VC_FORMAT,VC_ASSOCCODE from TDS_GOVT_VOUCHER where VC_VOUCHERNO = ? and VC_ASSOCCODE = ?",Statement.RETURN_GENERATED_KEYS);
				m_pst.setString(1, trip );
				m_pst.setString(2, riderName );
				m_pst.setString(3, assoccode);
				m_pst.setString(4, driverid);
				m_pst.setBytes(5,  TDSValidation.getDefaultsign(path));
				m_pst.setBytes(5,  b);
				m_pst.setString(6,amt);
				m_pst.setString(7, tip);
				m_pst.setString(8, totalAmt+"" );
				m_pst.setString(9, vocher_no );
				m_pst.setString(10, masterAssoccode);
				cat.info(m_pst.toString());
				System.out.println("query---->"+m_pst.toString());
				m_pst.execute();
				rs = m_pst.getGeneratedKeys();
				if(rs.next()){
					key = rs.getInt(1);
				}
			}
		} catch (SQLException sqex) {
			voucherBO.setV_status(false);
			cat.error("TDSException ProcessingDAO.insertVoucherDetail-->"+sqex.getMessage());
			cat.error(m_pst.toString());
		//	System.out.println("TDSException ProcessingDAO.insertVoucherDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return key;
	}

	public static VoucherBO updateVoucherDetail(String vocher_no,String assoccode,String trip,String driverid,String amt,String tip,byte[] b,String path,String riderName,int key)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ProcessingDAO.updateVoucherDetail");
		VoucherBO voucherBO = new VoucherBO();

		TDSConnection m_dbConn = null;
		Connection m_conn = null;
		PreparedStatement m_pst = null;
		BigDecimal totalAmt = (new BigDecimal(amt)).add(new BigDecimal(tip));
		m_dbConn  = new TDSConnection();
		m_conn = m_dbConn.getConnection();
		try {
			m_pst = m_conn.prepareStatement("UPDATE TDS_VOUCHER_DETAIL SET VD_TRIP_ID=?,VD_RIDER_NAME=?,VD_DRIVER_ID=?,VD_RIDER_SIGN=?,VD_AMOUNT=?,VD_TIP=?,VD_TOTAL=?,VD_PAYMENT_RECEIVED_STATUS=2 WHERE VD_TXN_ID = ?");
			m_pst.setString(1, trip );
			m_pst.setString(2, riderName );
			m_pst.setString(3, driverid);
			m_pst.setBytes(4,  TDSValidation.getDefaultsign(path));
			m_pst.setString(5,amt);
			m_pst.setString(6, tip);
			m_pst.setString(7, totalAmt+"" );
			m_pst.setInt(8, key );
			m_pst.execute();
			voucherBO.setVamount(amt);
			voucherBO.setTip(tip);
			voucherBO.setV_status(true);
			voucherBO.setVtrans(key+"");
		} catch (SQLException sqex) {
			voucherBO.setV_status(false);
			cat.error("TDSException ProcessingDAO.updateVoucherDetail-->"+sqex.getMessage());
			cat.error(m_pst.toString());
		//	System.out.println("TDSException ProcessingDAO.updateVoucherDetail-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return voucherBO;
	}
}
