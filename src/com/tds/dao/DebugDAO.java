package com.tds.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;

public class DebugDAO {
	private static Category cat =TDSController.cat;	


	static {

		cat = Category.getInstance(DebugDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+DebugDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static void writeRequestResponse(StringBuffer values, String userID, String sessionID){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		Connection conn = null;
		PreparedStatement pst = null;
		TDSConnection dbconn = null;
		dbconn = new TDSConnection();
		conn = dbconn.getConnection();
		String query = "INSERT INTO TDS_REQUESTANDRESPONSE(RR_USERNAME,RR_SESSIONID,RR_REQUEST) VALUES(?,?,?)";
		try {
			pst = conn.prepareStatement(query);
			pst.setString(1, userID);
			pst.setString(2, sessionID);
			pst.setString(3, values.toString());
			System.out.println("Checking Query:"+pst);
			pst.execute();
		} catch (Exception sqex) {
			cat.error(("TDSException ComplaintsDAO.insertComplaintMaster-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException ComplaintsDAO.insertComplaintMaster-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbconn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

	}
}