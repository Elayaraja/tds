package com.tds.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Category;

import com.tds.cmp.bean.FileProperty;
import com.tds.constants.docLib.DocumentConstant;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.security.bean.Module_detail;
import com.tds.tdsBO.DocumentBO;
import com.tds.tdsBO.DriverLocationBO;
import com.common.util.TDSValidation;

public class EmailSMSDAO {

	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(EmailSMSDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+EmailSMSDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	
	public static ArrayList getAndVerifyDriverNumbers(String p_assocode,String[] driver,String fromDriver,String toDriver,int all){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getAndVerifyDriverNumbers");
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query="";

			if (all == 3){
				if(driver!=null)
				{
					for(int i=0;i < driver.length-1;i++)
					{
						query =    query+ "'" + driver[i]+"',";
					}

					query = query+ "'" + driver[driver.length-1]+ "'" ;
				}
			}
			if(all == 1){
				pst=con.prepareStatement("select DR_SNO from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = '1'");		
			}else if (all == 2){
				pst=con.prepareStatement("select DR_SNO from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1 and DR_SNO between '"+ fromDriver + "' and '" + toDriver  +"'");
			}else if (all == 3){
				pst=con.prepareStatement("select DR_SNO from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1 and DR_SNO in ("+query+")");			

			}	
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("DR_SNO"));
			}
		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getAndVerifyDriverNumbers-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.getAndVerifyDriverNumbers-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}
	public static boolean  insertChargesForMultipleDriverNumbers(String assocode, ArrayList driver, String effectiveDate, String description, String amt, int chargeType, int location){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.insertChargesForMultipleDriverNumbers");
		boolean successOfOperation = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		//PreparedStatement  m_pst_pay;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			con.setAutoCommit(false);
			// ?? PERFORM COMMIT OFF ?? 
			if (location == 1){// Inserting into Driver Penalty Table
				pst = con.prepareStatement("insert into TDS_DRIVER_PENALITY(DP_TRANS_ID,DP_DRIVER_ID,DP_ASSOCCODE,DP_TYPE,DP_AMOUNT,DP_ENTERED_DATE,DP_PENALITY_DATE,DP_DESC) values(?,?,'" + assocode    + "','" + chargeType   + "'," + amt  + ",NOW(),'" + TDSValidation.getDBdateFormat(effectiveDate)  + "', '" + description + "')");
				for(int i=0;i < driver.size();i++)
				{
					pst.setInt(1, ConfigDAO.tdsKeyGen("TDS_DRIVER_PENALITY_SEQ", con));
					pst.setString(2, (String) driver.get(i));
					pst.execute();
				}
			}else{
				pst = con.prepareStatement("insert into TDS_DRIVER_CHARGES_APPROVAL(DCA_DRIVER_ID, DCA_COMPANY_ID, DCA_CHARGE_NO,DCA_DESC,DCA_AMOUNT,DCA_CHARGE_DATE,DCA_PROCESSING_DATE) values(?,?,?,?,?,?, NOW())"); 


				for(int i=0;i < driver.size();i++)
				{
					pst.setString(1, (String) driver.get(i));
					pst.setString(2, assocode);
					pst.setString(3, "0");
					pst.setString(4, description);
					pst.setString(5, amt);
					pst.setString(6, TDSValidation.getDBdateFormat(effectiveDate)); 						
					cat.info(pst.toString());

					pst.execute();	
				}

			}  
			//?? PERFORM COMMIT ??
			con.commit();
			//?? PERFORM COMMIT ON ??
			con.setAutoCommit(true);
			successOfOperation = true;	

		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.insertChargesForMultipleDriverNumbers-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.insertChargesForMultipleDriverNumbers-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return successOfOperation ;
	} 		

	public static boolean isHavingRights(String action, String event,String userid)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.isHavingRights");
		boolean rights = false;

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select M_MODULE_NAME from TDS_MODULES_DETAIL , TDS_SECURITY_ACCESS where S_MODULE_NO = M_MODULE_NO and S_UID= ? and M_ACTION = ? and M_EVENT= ? and S_ACESS=true");
			pst.setString(1, userid);
			pst.setString(2, action);
			pst.setString(3, event);
			cat.info(pst.toString());
			rs = pst.executeQuery();


			if(rs.first())
			{
				rights = true;
			}
		} catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.isHavingRights-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.isHavingRights-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return rights;
	}

	public static FileProperty getFileProperty(String id){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getFileProperty");

		FileProperty fileProperty = new FileProperty();

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select * from UPLOADS where UPLOADID=?");
			pst.setString(1, id);
			cat.info(pst.toString());
			rs = pst.executeQuery();

			if(rs.first())
			{
				fileProperty.setFilesize(rs.getInt("filesize"));
				fileProperty.setFilename(rs.getString("filename"));
				fileProperty.setFile(rs.getBytes("BINARYFILE"));
			}
		}catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getFileProperty-->" + sqex.getMessage()));
			cat.error(pst.toString());
		//	System.out.println("TDSException EmailSMSDAO.getFileProperty-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return fileProperty;
	}

	public static ArrayList getModulesName(String userid){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		cat.info("TDS INFO EmailSMSDAO.getModulesName");

		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		String curr="",prev="";
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			Module_detail module_detail = null;
			pst=con.prepareStatement("select *,case (select S_UID from TDS_SECURITY_ACCESS where S_MODULE_NO = M_MODULE_NO and S_UID='"+userid+"' and S_ACESS=1) is null when true then 0 else 1 end as Access from TDS_MODULES_DETAIL order by m_module_no");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			int counter=0;
			while(rs.next())
			{
				curr = rs.getString("M_MODULE_NO").substring(0,1);
				module_detail = new Module_detail();

				if(!curr.equalsIgnoreCase(prev)) 
				{
					module_detail.setBold(true);
					if(al_list.size()>0 &&  counter > 0)
					{
						((Module_detail)al_list.get((counter-1))).setChange(true);
						//((Module_detail)al_list.get((counter-1))).setEnd(rs.getString("M_MODULE_NO"));
					}
				}
				prev = rs.getString("M_MODULE_NO").substring(0,1);
				module_detail.setModule_name(rs.getString("M_MODULE_NAME"));
				module_detail.setModule_no(rs.getInt("M_MODULE_NO"));
				module_detail.setAccess(rs.getInt("Access")==1?true:false);
				if(rs.isLast()){
					module_detail.setChange(true);
					//module_detail.setEnd(rs.getString("M_MODULE_NO"));
				}
				al_list.add(module_detail);
				counter++;
			}

		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getModulesName-->" + sqex.getMessage()));
			cat.error(pst.toString());
		//	System.out.println("TDSException EmailSMSDAO.getModulesName-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}


	public static ArrayList getDriverLocationList(String p_assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getDriverLocationList");

		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select l.*,a.AU_SNO from TDS_DRIVERLOCATION l,TDS_ADMINUSER a where DL_DRIVERID = AU_USERNAME and  DL_ASSOCCODE = '"+p_assocode+"' and DL_SWITCH='Y'");
			cat.info(pst.toString());
			rs = pst.executeQuery();
			while(rs.next())
			{
				DriverLocationBO driverLocationBO = new DriverLocationBO();

				driverLocationBO.setUid(rs.getString("AU_SNO"));
				driverLocationBO.setDriverid(rs.getString("DL_DRIVERID"));
				driverLocationBO.setLatitude(rs.getString("DL_LATITUDE"));
				driverLocationBO.setLongitude(rs.getString("DL_LONGITUDE"));
				driverLocationBO.setSmsid(rs.getString("DL_SMSID"));
				driverLocationBO.setPhone(rs.getString("DL_PHONE"));

				al_list.add(driverLocationBO);

			}

		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getDriverLocationList-->" + sqex.getMessage()));
			cat.error(pst.toString());
		//	System.out.println("TDSException EmailSMSDAO.getDriverLocationList-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static ArrayList getDriverList(String p_assocode,int all){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getDriverList");

		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			if(all == 1)
				pst=con.prepareStatement("select DR_SNO,DR_FNAME, DR_LNAME from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "'");
			else
				pst=con.prepareStatement("select DR_SNO,DR_FNAME, DR_LNAME from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1");	
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("DR_SNO"));
				al_list.add(rs.getString("DR_FNAME").concat(rs.getString("DR_LNAME")));
			}
		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getDriverList-->" + sqex.getMessage()));
			cat.error(pst.toString());
		//	System.out.println("TDSException EmailSMSDAO.getDriverList-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}
	public static boolean Assocodedriveridmatch(String Driverid,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.Assocodedriveridmatch");

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		boolean s=false;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT AU_SNO FROM TDS_ADMINUSER where AU_ASSOCCODE=? and AU_SNO=?");
			pst.setString(1, assocode);
			pst.setString(2, Driverid);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.first()){
				s=true;	
			}

		}catch(SQLException sqex){
			cat.error(("TDSException EmailSMSDAO.Assocodedriveridmatch-->" + sqex.getMessage()));
			cat.error(pst.toString());
		//	System.out.println("TDSException EmailSMSDAO.Assocodedriveridmatch-->"+ sqex.getMessage());
			sqex.printStackTrace();
			return s;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return s;
	}
	public static ArrayList getzonelist(String Driverid,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getzonelist");

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null,pst1 = null;
		ResultSet rs,rs1 = null;
		String s="";
		ArrayList result = new ArrayList();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT QD_QUEUENAME,QD_DESCRIPTION FROM TDS_QUEUE_DETAILS WHERE QD_ASSOCCODE=? AND QD_LOGIN_SWITCH='A'");			
			pst.setString(1, assocode);
			rs=pst.executeQuery();
			pst1 = con.prepareStatement("SELECT A.QUE_NAME,B.QD_DESCRIPTION FROM TDS_QUEUE_DRIVER_MAPPING A, TDS_QUEUE_DETAILS B WHERE A.ASS_CODE=? AND A.DRIVER_ID=? AND A.QUE_NAME=B.QD_QUEUENAME");
			pst1.setString(1, assocode);
			pst1.setString(2, Driverid);
			cat.info(pst.toString());
			rs1=pst1.executeQuery();


			if(rs.first()){ 
				result.add(rs.getString("QD_QUEUENAME"));
				result.add(rs.getString("QD_DESCRIPTION")); 

				while(rs1.next()) {
					result.add(rs1.getString("QUE_NAME"));
					result.add(rs1.getString("QD_DESCRIPTION")); 
				} 
			}

		}catch(SQLException sqex){
			cat.error(("TDSException EmailSMSDAO.getzonelist-->" + sqex.getMessage()));
			cat.error(pst.toString());
		//	System.out.println("TDSException EmailSMSDAO.getzonelist-->"+ sqex.getMessage());
			sqex.printStackTrace();			
			return result;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;
	}
	public static String IdtodriverName(String Driverid,String assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.IdtodriverName");

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String s="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT concat(DR_FNAME,' ',DR_LNAME) as name FROM TDS_DRIVER where DR_SNO=? and DR_ASSOCODE=?");
			pst.setString(1, Driverid);
			pst.setString(2, assocode);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.first()){
				s=rs.getString("name");	
			}
		}catch(SQLException sqex){
			cat.error(("TDSException EmailSMSDAO.IdtodriverName-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.IdtodriverName-->"+ sqex.getMessage());
			sqex.printStackTrace();	
			return s;
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return s;
	}
	public static ArrayList getDriverList(String p_assocode,String driver,int all){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getDriverList");
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null; 
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			if(all == 1)
				pst=con.prepareStatement("select DR_SNO, DR_USERID,DR_FNAME from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and DR_SNO like (?) and AU_ASSOCCODE =  '" + p_assocode + "'");
			else if(all == 2)
				pst=con.prepareStatement("select DR_SNO,DR_USERID,DR_FNAME from TDS_DRIVER d,TDS_DRIVERLOCATION l where d.DR_USERID = l.DL_DRIVERID and DL_SWITCH = 'Y' and DR_SNO like (?) and DL_ASSOCCODE = '"+p_assocode+"'");
			else
				pst=con.prepareStatement("select DR_SNO, DR_USERID,DR_FNAME from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and DR_SNO like (?) and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1");	

			pst.setString(1,("%"+driver+"%")); 
			cat.info(pst.toString());
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("DR_SNO"));
				al_list.add(rs.getString("DR_FNAME"));
			}
		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getDriverList-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.getDriverList-->"+ sqex.getMessage());
			sqex.printStackTrace();		
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static String returnEmail(String username){
		String email="";
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.returnEmail");

		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("select AU_EMAIL from TDS_ADMINUSER  where AU_USERNAME=?");
			pst.setString(1, username);
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.first()){
				email=rs.getString("AU_EMAIL");	
			}  else
				email= "";

		}catch(SQLException sqex){
			cat.error(("TDSException EmailSMSDAO.returnEmail-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.returnEmail-->"+ sqex.getMessage());
			sqex.printStackTrace();
			email= "";
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

        return email;

	}

	public static ArrayList getDriverNoList(String p_assocode,String[] driver,String all){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getDriverNoList");

		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query="";

			if(driver!=null)
			{
				for(int i=0;i < driver.length-1;i++)
				{
					//	if(i<(driver.length-1))
					query = query+driver[i]+",";
					//	else
					//	query = query+driver[i];
				}

				query = query+driver[driver.length-1];
			}

			cat.info(pst.toString());
			if(all.equals("0"))
				pst=con.prepareStatement("select concat(DR_PHONE) as email from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = 1 and DR_SNO in ("+query+")");
			else
				pst=con.prepareStatement("select concat(DR_PHONE) as email from TDS_DRIVER d,TDS_ADMINUSER a where a.AU_SNO=d.DR_SNO and AU_ASSOCCODE =  '" + p_assocode + "' and DR_ACTIVESTATUS = '1'");

			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("email"));
			}
		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getDriverNoList-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.getDriverNoList-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}


	public static ArrayList getDriverNoList(ArrayList al_l){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getDriverNoList");

		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String list = "";

			if(!al_l.isEmpty())
			{
				for(int i=0;i<al_l.size()-1;i++)
					list = list + al_l.get(i).toString()+",";

				list = list + al_l.get(al_l.size()-1).toString();

				pst=con.prepareStatement("select DR_PHONE as email from TDS_DRIVER where DR_SNO in ("+list+")");
				cat.info(pst.toString());
				rs=pst.executeQuery();
				while(rs.next()){
					al_list.add(rs.getString("email"));
				}
			}
		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getDriverNoList-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.getDriverNoList-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static String getDriverNo(String did ){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getDriverNo");

		String no ="";
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("select DR_PHONE as email from TDS_DRIVER where DR_SNO ='"+did+"' ");
			cat.info(pst.toString());
			rs=pst.executeQuery();
			if(rs.first()){
				no = rs.getString("email");
			}
		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getDriverNo-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.getDriverNo-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return no;
	}



	public static ArrayList getDriverNoQ(String p_assocode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getDriverNoQ");
		ArrayList al_list=new ArrayList();
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			String query="";
			//pst=con.prepareStatement("select DL_DRIVERID,concat(DL_PHONE,'@',DL_SMSID) as email from TDS_DRIVERLOCATION where DL_ASSOCCODE =  '" + p_assocode + "' and date_sub(now(),INTERVAL 1 HOUR) <= DL_LASTUPDATE");
			pst=con.prepareStatement("select DL_DRIVERID,DL_SMSID as email from TDS_DRIVERLOCATION where DL_ASSOCCODE =  '" + p_assocode + "' and date_sub(now(),INTERVAL 1 HOUR) <= DL_LASTUPDATE");
			cat.info(pst.toString());

			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add(rs.getString("DL_DRIVERID"));
				al_list.add(rs.getString("email"));
			}
		}
		catch(SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getDriverNoQ-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.getDriverNoQ-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
	}

	public static ArrayList getQueueForAssoccode(String Assocode)
	{
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getQueueForAssoccode");
		ArrayList m_conditionList = null;
		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		m_tdsConn = new TDSConnection(); 
		m_con = m_tdsConn.getConnection();
		try {
			m_conditionList = new ArrayList();
			m_pst = m_con.prepareStatement("SELECT QD_QUEUENAME,QD_DESCRIPTION FROM TDS_QUEUE_DETAILS where QD_ASSOCCODE= ? ");
			m_pst.setString(1, Assocode);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			while(m_rs.next())
			{
				m_conditionList.add(m_rs.getString("QD_QUEUENAME"));
				m_conditionList.add(m_rs.getString("QD_DESCRIPTION"));
			}

		} catch (SQLException sqex ) {
			cat.error(("TDSException EmailSMSDAO.getQueueForAssoccode-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
		//	System.out.println("TDSException EmailSMSDAO.getQueueForAssoccode-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_conditionList;
	}
	public static boolean chckCronJobExec(String date,String cronjob,String yest)
	{
		
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.chckCronJobExec");
		boolean status = false;

		TDSConnection m_tdsConn = null;
		Connection m_con = null;
		PreparedStatement m_pst = null;
		ResultSet m_rs = null;
		m_tdsConn = new TDSConnection(); 
		m_con = m_tdsConn.getConnection();
		try {
			if(yest.equals("Y"))
				m_pst = m_con.prepareStatement("select date_format(date_sub(SL_DAY,interval 1 day),'%Y%m%d') from TDS_SCHEDULED_PROCESSING_LOG where date_format(date_sub(SL_DAY,interval 1 day),'%Y%m%d') = ? and  SL_CRON_JOB = ?");
			else
				m_pst = m_con.prepareStatement("select date_format(SL_DAY,'%Y%m%d') from TDS_SCHEDULED_PROCESSING_LOG where date_format(SL_DAY,'%Y%m%d') = ? and  SL_CRON_JOB = ?");

			m_pst.setString(1, date);
			m_pst.setString(2, cronjob);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();
			if(m_rs.first())
				status = true;

		}catch (SQLException sqex ) {
			cat.error(("TDSException EmailSMSDAO.chckCronJobExec-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
	//		System.out.println("TDSException EmailSMSDAO.chckCronJobExec-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_tdsConn.closeConnection();
		}	
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return status;
	}
	public static List getDriverLocation(String assocode,String driverids) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.getDriverLocation");
		List m_driverLocationList = new ArrayList();
		TDSConnection m_dbcon = null;
		Connection m_con = null;
		PreparedStatement m_pst = null ;
		ResultSet m_rs = null; 
		Map m_driverLocation = null;
		m_dbcon = new TDSConnection();
		m_con = m_dbcon.getConnection();
		try {
			if(driverids.equals(""))
				m_pst = m_con.prepareStatement("SELECT DR_USERID,DR_DRIVEREXT,DL_LATITUDE,DL_LONGITUDE FROM TDS_DRIVER, TDS_DRIVERLOCATION WHERE DL_DRIVERID = DR_USERID and DL_SWITCH = 'Y' and DL_ASSOCCODE = ?");
			else {
				String sepDriver = "";
				if(!(driverids.indexOf(",")>0))
				{
					sepDriver = driverids;
				} else {

					String driverid[]  = driverids.split(","); 

					for(int i=0;i<driverid.length;i++)
					{
						if(i !=driverid.length -1)
							sepDriver = sepDriver + driverid[i] + ",";
						else
							sepDriver = sepDriver + driverid[i] ;
					}
				}

				m_pst = m_con.prepareStatement("SELECT DR_USERID,DR_DRIVEREXT,DL_LATITUDE,DL_LONGITUDE FROM TDS_DRIVER, TDS_DRIVERLOCATION WHERE  DL_DRIVERID = DR_USERID and DL_SWITCH = 'Y' and DL_ASSOCCODE = ? and DR_SNO in ("+sepDriver+")");
			}
			m_pst.setString(1, assocode);
			cat.info(m_pst.toString());
			m_rs = m_pst.executeQuery();

			while(m_rs.next()) {
				m_driverLocation = new HashMap();
				m_driverLocation.put("driverid", "\""+m_rs.getString("DR_USERID")+"\"");
				m_driverLocation.put("driverext", m_rs.getString("DR_DRIVEREXT"));
				m_driverLocation.put("latitude", m_rs.getString("DL_LATITUDE"));
				m_driverLocation.put("longitude", m_rs.getString("DL_LONGITUDE"));
				m_driverLocationList.add(m_driverLocation);
			}
			if(m_driverLocationList.size() < 0 ) {
				m_driverLocationList = null;
			}
		} catch (SQLException sqex) {
			cat.error(("TDSException EmailSMSDAO.getDriverLocation-->" + sqex.getMessage()));
			cat.error(m_pst.toString());
		//	System.out.println("TDSException EmailSMSDAO.getDriverLocation-->"+ sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			m_dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return m_driverLocationList;
	}

	public static boolean isExistOrNot(String p_query)
	{
		boolean result=false;
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO EmailSMSDAO.isExistOrNot");
		TDSConnection dbCon = null;
		Connection con = null;
		PreparedStatement pst1 = null, pst2, pst3, pst4;
		//ArrayList al_batchList = new ArrayList();	

		String uItemCode="",qty="";
		dbCon = new TDSConnection();
		con = dbCon.getConnection();
		try
		{
			pst1 = con.prepareStatement(p_query);
			cat.info(pst1.toString());
			ResultSet rs = pst1.executeQuery();
			if(rs.first())
			{
				result= true;
			}
			else
			{
				result= false;	
			}
		}
		catch(SQLException sqex)
		{
			cat.error(("TDSException EmailSMSDAO.isExistOrNot-->" + sqex.getMessage()));
			cat.error(pst1.toString());
			//System.out.println("TDSException EmailSMSDAO.isExistOrNot-->"+ sqex.getMessage());
			sqex.printStackTrace();
			
		}
		finally
		{			
			dbCon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
        return result;
	}
	public static ArrayList<String> emailUsers(String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		// gmail pop3 host name   
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<String> al_list =new ArrayList<String>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String query1 = "SELECT * FROM TDS_EMAIL";
		if(!assoccode.equals("")){
			query1=query1+" WHERE EM_ASSOCCODE='"+assoccode+"'";
		}
		try {
			pst = con.prepareStatement(query1);
			rs=pst.executeQuery();
			while(rs.next()){
				al_list.add( rs.getString("EM_USERID")); //Put here Gmail Username without @ sign 
				al_list.add(rs.getString("EM_PASSWORD")); 
				al_list.add(rs.getString("EM_ASSOCCODE"));
				al_list.add(rs.getString("EM_HOST"));
				al_list.add(rs.getString("EM_PORT"));
				al_list.add(rs.getString("EM_SMTP_ADD"));
			}
		}catch(Exception sqex){
			cat.error(("TDSException EmailSMSDAO.emailUsers-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.emailUsers-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return al_list;
		
	}
	public static ArrayList<DocumentBO> getDocumentTypes(String assoccode,String tmpFileName){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		ArrayList<DocumentBO> documentTypes=new ArrayList<DocumentBO>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
		pst=con.prepareStatement("SELECT MODULE_NAME,DOCUMENT_TYPE FROM TDS_DOCUMENTS_MASTER WHERE DM_ASSOCCODE='"+assoccode+"' AND MODULE_NAME='"+tmpFileName+"'");
		rs=pst.executeQuery();
		while(rs.next()){
			DocumentBO docBO=new DocumentBO();
			docBO.setShortName(rs.getString("MODULE_NAME"));
			docBO.setDocumentType(rs.getString("DOCUMENT_TYPE"));
			documentTypes.add(docBO);
		}
		}catch(Exception sqex){
			cat.error(pst.toString());
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return documentTypes;
	}

	public static int insertDocument(String driver,String assoccode,String tmpFileName,String fileName,String from,InputStream fis,int len,String type){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs,rs1=null;
		int result=0;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst=con.prepareStatement("SELECT * FROM TDS_DRIVER WHERE DR_USERID='"+driver+"' AND DR_ASSOCODE='"+assoccode+"'");
			rs=pst.executeQuery();
			if(rs.next()){
				String query = "insert into TDS_DOCUMENTS (DOCUMENT_NAME,TDS_COMP_CODE,DOC_DATA,DOC_STATUS,DOC_UPLOADED_BY,DOC_TYPE,DOC_EMAIL_ID) "
					+ " values (?,?,?,?,?,?,?)";
				pst = con.prepareStatement(query);
				pst.setString(1, fileName);
				pst.setString(2, assoccode);
				pst.setBinaryStream(3, fis, len);
				pst.setString(4, DocumentConstant.STATUS_PENDING);
				pst.setString(5, driver);
				pst.setString(6, type);
				pst.setString(7, from);
				
				result=pst.executeUpdate();
			}else{
				result=3;
			}
		
		}catch(Exception sqex){
			cat.error(("TDSException EmailSMSDAO.insertDocument-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.insertDocument-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return result;

	}
	public static String getEmailAddresses(String driver,String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		String emailAddress= "";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
		pst=con.prepareStatement("SELECT AU_EMAIL FROM TDS_ADMINUSER WHERE AU_ASSOCCODE='"+assoccode+"' AND AU_SNO='"+driver+"'");
		rs=pst.executeQuery();
		while(rs.next()){
			emailAddress=(rs.getString("AU_EMAIL"));
		
		}
		}catch(Exception sqex){
			cat.error(("TDSException EmailSMSDAO.getEmailAddresses-->" + sqex.getMessage()));
			cat.error(pst.toString());
			//System.out.println("TDSException EmailSMSDAO.getEmailAddresses-->"+ sqex.getMessage());
			sqex.printStackTrace();
		}finally{
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));

		return emailAddress;
	}

}
