package com.tds.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ConsoleBO;


public class ConsoleDAO {
	
	private static Category cat =TDSController.cat;
	

	static {
		cat = Category.getInstance(ConsoleDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+ConsoleDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}

	
	public static ArrayList<ConsoleBO> consoleDbAccess(String assoccode){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		cat.info("TDS INFO ConsoleDAO.ConsoleDbAccess");
		AdminRegistrationBO adminBO = new AdminRegistrationBO();
		ArrayList<ConsoleBO> al_list = new ArrayList<ConsoleBO>();
		TDSConnection dbcon = null ;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			dbcon = new TDSConnection();
			con = dbcon.getConnection();
			pst = con.prepareStatement("SELECT * FROM TDS_CONSOLE WHERE C_MASTER_ASSOCCODE ='"+assoccode+"' ORDER BY C_TIME DESC LIMIT 0,200");
			rs = pst.executeQuery();
			//System.out.println("Execute try");
			while(rs.next()){
				ConsoleBO consoleBo = new ConsoleBO();
				consoleBo.setAcode(rs.getString("C_ASSOCCODE"));
				consoleBo.setTripid(rs.getString("C_TRIPID"));
				consoleBo.setBy(rs.getString("C_CHANGED_BY"));
				consoleBo.setMacode(rs.getString("C_MASTER_ASSOCCODE"));
				consoleBo.setReason(rs.getString("C_REASON"));
				consoleBo.setTime(rs.getString("C_TIME"));
				consoleBo.setStatus(rs.getInt("C_STATUS"));
				al_list.add(consoleBo);				
				/*System.out.println(+consoleBo.getStatus());
				System.out.println(consoleBo.getReason());
				System.out.println(consoleBo.getTripid());
				System.out.println("\n");*/
			}
		}
		catch(SQLException sqex){			
			cat.error(pst.toString());
			//System.out.println("TDSException ConsoleDAO.ConsoleDbAccess-->"+sqex.getMessage());
			sqex.printStackTrace();
		}finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return al_list;
	
	}
}
