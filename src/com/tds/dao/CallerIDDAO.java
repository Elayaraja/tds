package com.tds.dao;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.log4j.Category;

import com.lowagie.text.pdf.codec.Base64.OutputStream;
import com.tds.cmp.bean.CallerIDBean;
import com.tds.controller.TDSController;
import com.tds.db.TDSConnection;
import com.tds.tdsBO.AdminRegistrationBO;
public class CallerIDDAO {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(CallerIDDAO.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+CallerIDDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

	}

	
	public static  CallerIDBean getUserIdPassword(AdminRegistrationBO adminBo) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		CallerIDBean callerId=new CallerIDBean();
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_CALLERID_USER WHERE CID_ASSOCIATIONCODE=?");
			pst.setString(1, adminBo.getAssociateCode());
			rs=pst.executeQuery();
			if(rs.next()){
				callerId.setUserID(rs.getString("CID_USER_ID"));
				callerId.setPassword(rs.getString("CID_PASSWORD"));
			}
		}catch(SQLException sqex) {
			cat.error("TDSException CallerIdDAO.getCaller-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CallerIdDAO.getCaller-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return callerId;
	}
	public static  String getPasswordId(String masterAssoccode) {
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		String callerId="";
		try {
			pst = con.prepareStatement("SELECT * FROM TDS_METERID_PASS WHERE MID_ASSOCIATIONCODE=?");
			pst.setString(1, masterAssoccode);
			rs=pst.executeQuery();
			if(rs.next()){
				callerId=rs.getString("MID_PASSWORD");
			}
		}catch(SQLException sqex) {
			cat.error("TDSException CallerIdDAO.getMeterId-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CallerIdDAO.getMeterId-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return callerId;
	}
	
	public static ArrayList<CallerIDBean> getCallerIdHistory(AdminRegistrationBO adminBo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<CallerIDBean> caller=new ArrayList<CallerIDBean>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT CIH_LINE_NO,CIH_PHONE_NUMBER,CIH_NAME,DATE_FORMAT(CONVERT_TZ(CIH_UPDATETIME,'UTC','"+adminBo.getTimeZone()+"'),'%Y/%m/%d') as DATE,DATE_FORMAT(CONVERT_TZ(CIH_UPDATETIME,'UTC','"+adminBo.getTimeZone()+"'),'%H:%i') as TIME FROM TDS_CALLERID_HISTORY WHERE CIH_ASSOCIATIONCODE='"+adminBo.getAssociateCode()+"' AND CIH_UPDATETIME > DATE_SUB(NOW(),INTERVAL 1 DAY) ORDER BY CIH_UPDATETIME");
			rs=pst.executeQuery();
			while(rs.next()){
				CallerIDBean callerId=new CallerIDBean();
				callerId.setLineNo(rs.getString("CIH_LINE_NO"));
				callerId.setPhoneNo(rs.getString("CIH_PHONE_NUMBER"));
				callerId.setName(rs.getString("CIH_NAME"));
				callerId.setDate(rs.getString("DATE"));
				callerId.setTime(rs.getString("TIME"));
				caller.add(callerId);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException CallerIdDAO.getCallerIdHistory-->"+sqex.getMessage());
			cat.error(pst.toString());
			//System.out.println("TDSException CallerIdDAO.getCallerIdHistory-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return caller;
	}
	
	public static void storeCallerIDInfo(CallerIDBean callerIDDAta){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		csp_conn = dbcon.getConnection();
		
		String[] lines = null;
		if(callerIDDAta.getLineNo().contains(";")){
			lines = callerIDDAta.getLineNo().split("\\;");
		}else{
			lines=new String[1];
			lines[0]=callerIDDAta.getLineNo();
		}
		//System.out.println("Lines:"+Arrays.toString(lines));
		
		try {
			for(int i=0; i<lines.length;i++){
				if(callerIDDAta.isRinging()){
					csp_pst = csp_conn.prepareStatement("UPDATE TDS_CALLERID SET CID_PHONE_NUMBER=?,CID_NAME=?,CID_UPDATETIME=NOW(),CID_STATUS=9,CID_ACCEPTED_BY='' WHERE CID_ASSOCIATIONCODE =? AND CID_LINE_NO=?");
				}else{
					csp_pst = csp_conn.prepareStatement("UPDATE TDS_CALLERID SET CID_PHONE_NUMBER=?,CID_NAME=?,CID_UPDATETIME=NOW(),CID_STATUS=0,CID_ACCEPTED_BY='' WHERE CID_ASSOCIATIONCODE =? AND CID_LINE_NO=?");
				}
				csp_pst.setString(1, callerIDDAta.getPhoneNo());
				csp_pst.setString(2, callerIDDAta.getName());
				csp_pst.setString(3, callerIDDAta.getAssociationCode());
				csp_pst.setString(4, lines[i]);
				//System.out.println("Before update");
				if(csp_pst.executeUpdate()<1){
					//System.out.println("Before insert");
					csp_pst = csp_conn.prepareStatement("INSERT INTO TDS_CALLERID(CID_ASSOCIATIONCODE,CID_LINE_NO,CID_PHONE_NUMBER,CID_NAME,CID_UPDATETIME,CID_STATUS,CID_ACCEPTED_BY) VALUES(?,?,?,?,now(),?,'')");
					csp_pst.setString(1, callerIDDAta.getAssociationCode());
					csp_pst.setString(2, lines[i]);
					csp_pst.setString(3, callerIDDAta.getPhoneNo());
					csp_pst.setString(4, callerIDDAta.getName());
					//csp_pst.setString(5, callerIDDAta.getTime());
					if(callerIDDAta.isRinging()){
						csp_pst.setString(5, "9");
					}else{
						csp_pst.setString(5, "0");
					}
					csp_pst.execute();				
				}
				csp_pst = csp_conn.prepareStatement("INSERT INTO TDS_CALLERID_HISTORY(CIH_ASSOCIATIONCODE,CIH_LINE_NO,CIH_PHONE_NUMBER,CIH_NAME,CIH_UPDATETIME) VALUES(?,?,?,?,now())");
				csp_pst.setString(1, callerIDDAta.getAssociationCode());
				csp_pst.setString(2, lines[i]);
				csp_pst.setString(3, callerIDDAta.getPhoneNo());
				csp_pst.setString(4, callerIDDAta.getName());
				csp_pst.execute();				
			}
		} catch (Exception sqex){
			cat.error("TDSException CallerIdDAO.storeCallerIDInfo-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
		//	System.out.println("TDSException CallerIdDAO.storeCallerIDInfo-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
	}

	public static String getAssociationCode(String userID, String password){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		String assocCode = "";
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs=null;
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement("SELECT CID_ASSOCIATIONCODE FROM TDS_CALLERID_USER WHERE CID_USER_ID=? AND CID_PASSWORD=? ");
			csp_pst.setString(1, userID);
			csp_pst.setString(2, password);
			rs=csp_pst.executeQuery();
			if(rs.next()){
				assocCode=rs.getString("CID_ASSOCIATIONCODE");
			}
		}catch (Exception sqex){
			cat.error("TDSException CallerIdDAO.getAssociationCode-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
		//	System.out.println("TDSException CallerIdDAO.getAssociationCode-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return assocCode;
	}
	public static boolean storeUserIDPassword(CallerIDBean callerIDDAta,AdminRegistrationBO admiBo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		PreparedStatement csp_pst1 = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		csp_conn = dbcon.getConnection();
		try {
			csp_pst1=csp_conn.prepareStatement("DELETE FROM TDS_CALLERID_USER WHERE CID_ASSOCIATIONCODE=?");
			csp_pst1.setString(1, admiBo.getAssociateCode());
			csp_pst1.execute();
			csp_pst = csp_conn.prepareStatement("INSERT INTO TDS_CALLERID_USER (CID_ASSOCIATIONCODE,CID_USER_ID,CID_PASSWORD) VALUES(?,?,?)");
			csp_pst.setString(1, admiBo.getAssociateCode());
			csp_pst.setString(2, callerIDDAta.getUserID());
			csp_pst.setString(3, callerIDDAta.getPassword());
			csp_pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.error("TDSException CallerIdDAO.storeUserIDPassword-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
		//	System.out.println("TDSException CallerIdDAO.storeUserIDPassword-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return success;
	}

	public static boolean storePasswordID(String callerIDDAta,AdminRegistrationBO admiBo){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean success = false;
		TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		PreparedStatement csp_pst1 = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		csp_conn = dbcon.getConnection();
		try {
			csp_pst1=csp_conn.prepareStatement("DELETE FROM TDS_METERID_PASS WHERE MID_ASSOCIATIONCODE=?");
			csp_pst1.setString(1, admiBo.getMasterAssociateCode());
			csp_pst1.execute();
			csp_pst = csp_conn.prepareStatement("INSERT INTO TDS_METERID_PASS (MID_ASSOCIATIONCODE,MID_PASSWORD) VALUES(?,?)");
			csp_pst.setString(1, admiBo.getMasterAssociateCode());
			csp_pst.setString(2, callerIDDAta);
			csp_pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.error("TDSException CallerIdDAO.storePasswordID-->"+sqex.getMessage());
			cat.error(csp_pst.toString());
		//	System.out.println("TDSException CallerIdDAO.storePasswordID-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return success;
	}

	public static ArrayList<CallerIDBean> getCallerId(AdminRegistrationBO adminBO){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		ArrayList<CallerIDBean> caller=new ArrayList<CallerIDBean>();
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			//Updating status on the callerid button refresh timer
			//pst=con.prepareStatement("UPDATE TDS_CALLERID SET CID_STATUS=9 where CID_ASSOCIATIONCODE='"+adminBO.getAssociateCode()+"' and CID_UPDATETIME < DATE_SUB(now(),INTERVAL 5 minute) and CID_STATUS < 5 ");
			//pst.execute();
			pst = con.prepareStatement("SELECT AU_FNAME, CID_LINE_NO,CID_STATUS,CID_PHONE_NUMBER,CID_ACCEPTED_BY,CID_NAME,DATE_FORMAT(CID_UPDATETIME,'%Y/%m/%d') as DATE,DATE_FORMAT(CONVERT_TZ(CID_UPDATETIME,'UTC','"+adminBO.getTimeZone()+"'),'%H:%i') as TIME, DATE_FORMAT(CID_ACCEPTED_TIME,'%Y/%m/%d') as ACCEPTEDDATE,DATE_FORMAT(CID_ACCEPTED_TIME,'%H:%i') as ACCEPTEDTIME,CID_ASSOCIATIONCODE FROM TDS_CALLERID as B LEFT JOIN TDS_ADMINUSER A ON B.CID_ASSOCIATIONCODE=A.AU_ASSOCCODE AND B.CID_ACCEPTED_BY=A.AU_SNO where B.CID_ASSOCIATIONCODE=?  AND (CID_UPDATETIME > (DATE_SUB(now(),INTERVAL 5 minute)) OR (CID_STATUS=0 OR CID_STATUS=9)) GROUP BY CID_PHONE_NUMBER ORDER BY CID_LINE_NO");
			pst.setString(1, adminBO.getAssociateCode());
			rs=pst.executeQuery();
			while(rs.next()){
				CallerIDBean callerId=new CallerIDBean();
				callerId.setLineNo(rs.getString("CID_LINE_NO"));
				callerId.setPhoneNo(rs.getString("CID_PHONE_NUMBER"));
				callerId.setName(rs.getString("CID_NAME"));
				callerId.setDate(rs.getString("DATE"));
				callerId.setTime(rs.getString("TIME"));
				callerId.setAcceptedBy(rs.getString("AU_FNAME"));
				callerId.setStatus(Integer.parseInt(rs.getString("CID_STATUS")));
				callerId.setAcceptedDate(rs.getString("ACCEPTEDDATE"));
				callerId.setAcceptedTime(rs.getString("ACCEPTEDTIME"));
				caller.add(callerId);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException CallerIdDAO.getCaller-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CallerIdDAO.getCaller-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return caller;
	}
	
	public static ArrayList<String> getAllDates(){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null; 
		ArrayList<String> dates=new ArrayList<String>();
		String day="";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try {
			pst = con.prepareStatement("SELECT DATE_ADD(CURDATE(), INTERVAL row DAY) as genDate from (SELECT @row:=@row + 1 as row FROM	(select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t,(select 0 union all select 1 union all select 3 union all select 4 union all select 5 union all select 6 union all select 6 union all select 7 union all select 8 union all select 9) t2,(SELECT @row:=0) t5) m where DATE_ADD(CURDATE(), INTERVAL row DAY) between ? and ? AND (DAYOFWEEK(DATE_ADD(CURDATE(), INTERVAL row DAY)) = 1)");
			rs=pst.executeQuery();
			if(rs.next()){
				day=rs.getString("DATE_ADD");
				dates.add(day);
			}
		}catch(SQLException sqex) {
			cat.error("TDSException CallerIdDAO.getAllDates-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CallerIdDAO.getAllDates-->"+sqex.getMessage());
			sqex.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return dates;
	}
	public static boolean acceptCall(AdminRegistrationBO adminBO,String lineNumber){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean success = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst = con.prepareStatement("UPDATE TDS_CALLERID SET CID_ACCEPTED_BY=?,CID_ACCEPTED_TIME=now() WHERE CID_LINE_NO=? AND CID_ASSOCIATIONCODE=?");
			pst.setString(1, adminBO.getUid());
			pst.setString(2, lineNumber);
			pst.setString(3, adminBO.getAssociateCode());
			pst.execute();
			success = true;
		} catch (Exception sqex){
			cat.error("TDSException CallerIdDAO.acceptCall-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CallerIdDAO.acceptCall-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return success;
	}
	public static boolean checkStatus(String phoneNumber,String assoccode,String accepted){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);
		boolean success = false;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst = con.prepareStatement("SELECT * FROM TDS_CALLERID WHERE CID_PHONE_NUMBER='"+phoneNumber+"'");
			rs = pst.executeQuery();
			if(rs.next()){
				pst = con.prepareStatement("UPDATE TDS_CALLERID SET CID_ACCEPTED_BY='"+accepted+"',CID_ACCEPTED_TIME=NOW(),CID_STATUS=1 WHERE CID_PHONE_NUMBER='"+phoneNumber+"' AND CID_ASSOCIATIONCODE='"+assoccode+"'");
				pst.execute();
				success = true;
			}
		} catch (Exception sqex){
			cat.error("TDSException CallerIdDAO.checkStatus-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CallerIdDAO.checkStatus-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return success;
	}

	public static int checkTime(String serviceDate,String serviceTime,String timeDiff){
		long startTime = System.currentTimeMillis();
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+startTime);

		String date[] = serviceDate.split("/");
		String processedDate=date[2]+"-"+date[0]+"-"+date[1]+" "+serviceTime.substring(0,2)+":"+serviceTime.substring(2,4)+":"+"00";
		
		int hours=0;
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs=null;
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		try{
			pst=con.prepareStatement("SELECT TIMESTAMPDIFF(HOUR,CONVERT_TZ(NOW(),'UTC','"+timeDiff+"'),'"+processedDate+"') AS HOURS");
			rs = pst.executeQuery();
			if(rs.next()){
				hours = rs.getInt("HOURS");
			}
		} catch (Exception sqex){
			cat.error("TDSException CallerIdDAO.checkStatus-->"+sqex.getMessage());
			cat.error(pst.toString());
		//	System.out.println("TDSException CallerIdDAO.checkStatus-->" + sqex.getMessage());
			sqex.printStackTrace();
		}finally { 
			dbcon.closeConnection();
		}
		cat.info(Thread.currentThread().getStackTrace()[1].getClassName().replace("com.tds.dao.", "")+"."+Thread.currentThread().getStackTrace()[1].getMethodName()+"--->"+(System.currentTimeMillis()-startTime));
		return hours;
	}
	

	

}

