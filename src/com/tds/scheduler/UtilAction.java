package com.tds.scheduler;

import java.util.Calendar;
import java.util.TimerTask;



public class UtilAction extends TimerTask  {

	public UtilAction(){

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
	//	System.out.println("UtilAction method called.......");
		Calendar c = Calendar.getInstance();
		try{
			BatchSupportAction.performBatch();
		}catch (Exception e) {
			e.printStackTrace();
		}
		try{
			if(Double.parseDouble((c.get(Calendar.HOUR_OF_DAY))+"."+c.get(Calendar.MINUTE)) >= 1.00) { 
				BatchSupportAction.deleteMessages();
			} 
		}catch (Exception e){
			e.printStackTrace();
		}
	}

}
