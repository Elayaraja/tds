package com.tds.scheduler;
 
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.process.DriverLogout;
import com.tds.process.QueueProcess;
import com.tds.process.VerifyAutoDispatch;
import com.tds.process.WaslLocationUpdate;
import com.tds.util.DownloadMailAttachments;
import com.common.util.TDSProperties;
import com.tds.wrapper.GmailUtil;


public class SchedulerServlet extends HttpServlet implements Servlet {
	
//private Category cat = TDSController.cat;

		 
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
			
			 
		}
 
		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
			 
		}
 
		 
		@Override
		public void init(ServletConfig config) throws ServletException {
			
			/*cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			cat.info("Class Name "+ SchedulerServlet.class.getName());
			cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			super.init(config);*/
			
			//Getting google Key
//			config.getServletContext().setAttribute("GoogKey",GooglePush.register());
		//	System.out.println("googkey"+config.getServletContext().getAttribute("GoogKey"));

		 	
			long interval = Long.parseLong(config.getInitParameter("interval")) * 60 * 1000  ; // Run Demaon Every 30 min
			long intervalTimerHistory = Long.parseLong(config.getInitParameter("interval")) * 60 * 1000  ; // Run Demaon Every 30 min
			long interval1 = 1 * 60 * 1000  ;
			long queueInterval = 10 * 1000  ;
			long sessionInterval = 60 * 1000;
			long emailInterval = 10 * 1000;
			long vADInterval = 10 * 1000;
			//HttpSession session = req.getSession();
			    
		   
		   Calendar today = new GregorianCalendar();
		    //tomorrow.add(Calendar.DATE, fONE_DAY);
		    Calendar result = new GregorianCalendar(
		      today.get(Calendar.YEAR),
		      today.get(Calendar.MONTH),
		      today.get(Calendar.DATE),
		      today.get(Calendar.HOUR),
			  today.get(Calendar.MINUTE)
		    ); 
		    
		    //schedule process lookup the propertiesfile
			if(TDSProperties.getValue("batchStatus").equalsIgnoreCase("1")) { 
				BatchAction bch_action = new BatchAction();
				HistoryTimer hTimer = new HistoryTimer();
				Timer timer = new Timer();
				timer.schedule(bch_action, result.getTime() , interval);
				timer.schedule(hTimer, result.getTime() , intervalTimerHistory);
			}
			
			if(TDSProperties.getValue("utilProcess").equalsIgnoreCase("1")){
				UtilAction utilAction =new UtilAction();
				HistoryTimer hTimer = new HistoryTimer();
				Timer timer = new Timer();
				timer.schedule(utilAction, result.getTime() , interval);
				timer.schedule(hTimer, result.getTime() , intervalTimerHistory);
			}
			// Queue Process 
			if(TDSProperties.getValue("PerformQueueProcess").equalsIgnoreCase("1")) { 
				ArrayList m_AssocList = ServiceRequestDAO.getAssoccodeForQueue();
				Timer q_timer  ;
				int j=0;
				//System.out.println("size of assocode"+m_AssocList.size());
				String wasl_ccode = TDSProperties.getValue("WASL_Company");
				for(int counter = 0; counter < m_AssocList.size(); counter = counter + 3) {
					if(m_AssocList.get(counter).toString().equals(wasl_ccode)){
						WaslLocationUpdate wasl_updater = new WaslLocationUpdate(m_AssocList.get(counter).toString(), config);
						Timer wasl_location_timer = new Timer();
						wasl_location_timer.schedule(wasl_updater, 20*1000, 29*1000);
						config.getServletContext().setAttribute("WASL_"+m_AssocList.get(counter).toString()+"_Timer",wasl_location_timer);
						config.getServletContext().setAttribute("WASL_"+m_AssocList.get(counter).toString()+"Process",wasl_updater);
						System.out.println("Wasl udpater started for "+m_AssocList.get(counter).toString());
					}else{
						System.out.println("Skipped Wasl udpater for "+m_AssocList.get(counter).toString());
					}
					
					q_timer = new Timer();	 
					String timeoutdriver = SystemPropertiesDAO.getParameter(m_AssocList.get(counter+2).toString(),"TimeOutDriver");		
					DriverLogout performDriverLogout = new DriverLogout(m_AssocList.get(counter).toString(),m_AssocList.get(counter+2).toString(),timeoutdriver,config);
					q_timer.schedule(performDriverLogout,(30*1000) , sessionInterval); 		

					if(m_AssocList.get(counter+1).toString().equalsIgnoreCase("x")){
						continue;
					}
					QueueProcess performQueue = new QueueProcess(m_AssocList.get(counter).toString(),m_AssocList.get(counter+2).toString(),config);
					if(m_AssocList.get(counter+1).toString().equalsIgnoreCase("q") || m_AssocList.get(counter+1).toString().equalsIgnoreCase("a")){
						performQueue.setDispatchingJobs(true);
					} else{
						performQueue.setDispatchingJobs(false);
					}
					q_timer.schedule(performQueue,(30*1000) , queueInterval);
					config.getServletContext().setAttribute(m_AssocList.get(counter).toString()+"Timer",q_timer);
					config.getServletContext().setAttribute(m_AssocList.get(counter).toString()+"Process",performQueue);
					System.out.println("Job Started for "+m_AssocList.get(counter).toString() +":"+m_AssocList.get(counter+2).toString()+ "with dispatch switch = " + performQueue.isDispatchingJobs());
					
				}

				// Automatic Allocation
				//m_AssocList = ServiceRequestDAO.getAssoccodeForAutomatic();
				//Timer q_timer1  ;
				//for(int counter = 0; counter < m_AssocList.size(); counter ++) {
				//	q_timer1 = new Timer();	 
				//	PerformAutomaticAllocation peAutomaticAllocation = new PerformAutomaticAllocation(m_AssocList.get(counter).toString());
				//	q_timer1.schedule(peAutomaticAllocation,(2*60*1000) , queueInterval); 				
				//}
			}
		//	System.out.println("value of email"+TDSProperties.getValue("checkEmail").equalsIgnoreCase("1")); 
			if(TDSProperties.getValue("checkEmail").equalsIgnoreCase("1")) {
			 	GmailUtil emailAction = new GmailUtil();
			 	DownloadMailAttachments mailAttachments =new DownloadMailAttachments();
			 	try{
			 		emailAction.connect();
			 		emailAction.openFolder("INBOX");
			 	
			 	} catch (Exception e) {
		    		
					System.out.println("Exception Connecting To Server"+e.toString());
				}
			 	//emailAction.setUserPass(username, password)
				Timer timer = new Timer();
				//timer.schedule(emailAction, result.getTime() , emailInterval);
				timer.schedule(mailAttachments,result.getTime(),emailInterval);
			}
			
			//Verify Queue Process
			Timer autoDispatchVerify = new Timer();
			VerifyAutoDispatch v_AutoDispatch = new VerifyAutoDispatch(config);
			autoDispatchVerify.schedule(v_AutoDispatch, (60*1000), vADInterval);
		}
		
		@Override
		public void destroy() { 
		      
			//System.out.println("=============================Going to Destroyed The Session==================");
			//RegistrationDAO.destorySession();
			
	    } 
		
	}

