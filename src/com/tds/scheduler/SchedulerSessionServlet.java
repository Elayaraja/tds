package com.tds.scheduler;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.common.util.TDSProperties;

public class SchedulerSessionServlet  extends HttpServlet implements Servlet {
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		 
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		 
	}

	 
	public void init(ServletConfig config) throws ServletException {
		
	  	long interval1 = 14 * 60 * 1000  ;
	 	    
		   
		   Calendar today = new GregorianCalendar();
		    //tomorrow.add(Calendar.DATE, fONE_DAY);
		    Calendar result = new GregorianCalendar(
		      today.get(Calendar.YEAR),
		      today.get(Calendar.MONTH),
		      today.get(Calendar.DATE),
		      today.get(Calendar.HOUR),
			  today.get(Calendar.MINUTE)
		    ); 
			if(TDSProperties.getValue("PerformSessionToken").equalsIgnoreCase("1")) { 
				//System.out.println("Loading PoolBO");
			    ResettingSessionCron cron = new ResettingSessionCron(config);
				Timer timer1 = new Timer();
				timer1.schedule(cron,result.getTime(), interval1);
			}
	}


}
