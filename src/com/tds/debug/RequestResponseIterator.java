package com.tds.debug;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.tds.dao.DebugDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSProperties;

public class RequestResponseIterator {

	public static void writeRequestIntoDatabase(HttpServletRequest request){
		if(TDSProperties.getValue("debug").equals("1")){
			Enumeration attrs =  request.getParameterNames();
			StringBuffer values = new StringBuffer();
			while(attrs.hasMoreElements()) {
				String name = (String) attrs.nextElement();
				values= values.append("&"+name+"="+request.getParameter(name));
			}
			String sessionID = request.getSession().getId();
			String userName = ((AdminRegistrationBO)request.getSession().getAttribute("user")).getUid();
			DebugDAO.writeRequestResponse(values, userName, sessionID);
		}
	}
}
