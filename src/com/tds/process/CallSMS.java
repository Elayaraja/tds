package com.tds.process;

import java.util.ArrayList;

import com.common.util.SendingSMS;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.OpenRequestBO;

public class CallSMS{
	
	public static boolean sendSMStoDriverForAllocationDelete(String p_driverId,OpenRequestBO openRequestBO,ApplicationPoolBO poolBO) {
		
		final String m_openRequestStatus = "P"; 
		boolean m_status = false;
		String m_smsNo = "";
		String m_message = "";
		String m_subject = "TDSTDS";
		SendingSMS m_sendSMS;
		m_smsNo = ServiceRequestDAO.getDriverPhoneNumberForQueue(p_driverId);
		 
		//System.out.println("Driver Phone "+m_smsNo);
		 
		if(openRequestBO != null ) {
			//System.out.println("There is an Open Request for this Driver ");
			 
			//System.out.println("~~~~~~~~~~~~~~~~~~Tripid "+openRequestBO.getTripid());
			//ServiceRequestDAO.updateOpenRequestStatus(openRequestBO.getTripid(), m_openRequestStatus , p_driverId, "",openRequestBO.getVehicleNo());
			
			//_tripQueueMap.put(qname,openRequestBO.getTripid());
			m_message = m_subject + ";" + openRequestBO.getTripid() + ";" + openRequestBO.getSttime()+";"+p_driverId;
			m_sendSMS = new SendingSMS(poolBO.getSMTP_HOST_NAME(),poolBO.getSMTP_AUTH_USER(),poolBO.getSMTP_AUTH_PWD(),poolBO.getEmailFromAddress(),poolBO.getSMTP_PORT(),poolBO.getProtocol());
			try {
				//ArrayList al= new ArrayList();
				//al.add(m_smsNo);
				//MailingUtiil.sendMail(poolBO.getSMTP_HOST_NAME(), poolBO.getSMTP_AUTH_USER(), poolBO.getSMTP_AUTH_PWD(), poolBO.getEmailFromAddress(), al, m_message, "");
				m_sendSMS.sendMail(m_smsNo,  m_message);
			} catch (Exception p_messagingException) {
				//System.out.println("Error in Send Message ");
				p_messagingException.getStackTrace();
			}
			m_status = true;
		} 
		return m_status;
	}
	
	public static boolean sendPublicSMStoDriverDelete(String p_assocode,String qid,ApplicationPoolBO poolBO) {
		
		
		final String m_openRequestStatus = "B"; 
		boolean m_status = false;
		String m_message = "";
		String m_subject = "TDSTDS";
		 
		SendingSMS m_sendSMS;
		 
		OpenRequestBO openRequestBO = null;
		
		ArrayList al_list =  UtilityDAO.getDriverNoQ(p_assocode);
		ArrayList al_openList = ServiceRequestDAO.getORForPublicReq(qid);
		
		for( int counter = 0;counter<al_openList.size();counter++)
		{
			openRequestBO = (OpenRequestBO)al_openList.get(counter);
			//ServiceRequestDAO.updateOpenRequestStatus(openRequestBO.getTripid(), "B" , "", "", "");
			m_sendSMS = new SendingSMS(poolBO.getSMTP_HOST_NAME(),poolBO.getSMTP_AUTH_USER(),poolBO.getSMTP_AUTH_PWD(),poolBO.getEmailFromAddress(),poolBO.getSMTP_PORT(),poolBO.getProtocol());
			try {
				//MailingUtiil.sendMail(poolBO.getSMTP_HOST_NAME(), poolBO.getSMTP_AUTH_USER(), poolBO.getSMTP_AUTH_PWD(), poolBO.getEmailFromAddress(), al_list, m_message, "");
				for(int i=0;i<al_list.size();i=i+2){
					m_message = m_subject + ";" + openRequestBO.getTripid() + ";" + openRequestBO.getSttime()+";"+"" +al_list.get(i).toString() ;
					//System.out.println("Message:" + m_message);
					//m_sendSMS.sendMail(al_list.get(i+1).toString(),  m_message);
				}
			} catch (Exception p_messagingException) {
				//System.out.println("Error in Send Message ");
				p_messagingException.getStackTrace();
			}
				
			 
			
		}
	
		return m_status;
	}
}
