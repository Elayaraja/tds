package com.tds.process;

import java.util.ArrayList;
import java.util.TimerTask;

import javax.servlet.ServletConfig;

import org.apache.xml.resolver.apps.resolver;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.gac.wasl.LocationRegistration;
import com.gac.wasl.TripRegistration;
import com.gac.wasl.WaslIntegrationBO;
import com.tds.dao.AuditDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;

public class WaslLocationUpdate extends TimerTask{
	
	private String assoccode = "";
	private ServletConfig config = null;
	private String timeZone = "";
	public WaslLocationUpdate(String assoccode, ServletConfig config){
		this.assoccode = assoccode;
		this.config = config;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		String wasl_ccode = TDSProperties.getValue("WASL_Company");
		if(assoccode.equals(wasl_ccode)){
			
			//Driver location informed to Wasl system
			timeZone = SystemPropertiesDAO.getParameter(assoccode, "timeZoneArea");
			ArrayList<DriverLocationBO> driver_list = UtilityDAO.getDriverLocationListForWasl(assoccode, 1, timeZone, "");
			if(driver_list!=null && driver_list.size()>0){
				for(int i=0; i<driver_list.size();i++){
					DriverLocationBO driver_BO = driver_list.get(i);
					if(driver_BO.getJson_details().equals("")){
						System.out.println("Location update for "+driver_BO.getDriverid()+" is failed with empty vehicleReferenceNumber");
						//continue;
					}
					//JSONArray wasl_array = new JSONArray();
					JSONObject wasl_obj = new JSONObject();
					
					try {
						JSONArray driver_cab_array = new JSONArray(driver_BO.getJson_details());
						JSONObject driver_cab_obj = driver_cab_array.getJSONObject(0);
						if(driver_cab_obj==null || !driver_cab_obj.has("vehicleReferenceNumber") || driver_cab_obj.getString("vehicleReferenceNumber").equals("")){
							System.out.println("Wasl Driver:"+driver_BO.getDriverid()+" dont has vehicle reference number");
							/*driver_cab_obj.remove("vehicleReferenceNumber");
							driver_cab_obj.put("vehicleReferenceNumber", "3237122");*/
							continue;
						}
						wasl_obj.put("apiKey", "D42D418A-4FEE-42C9-9005-18C06F497DC6");
						wasl_obj.put("vehicleReferenceNumber", driver_cab_obj.getString("vehicleReferenceNumber"));
						wasl_obj.put("currentLatitude", driver_BO.getLatitude());
						wasl_obj.put("currentLongitude", driver_BO.getLongitude());
						wasl_obj.put("hasCustomer", driver_BO.isIn_job()?"1":"0");
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//System.out.println(""+wasl_obj.toString());
					WaslIntegrationBO wasl_BO = LocationRegistration.LocationRegistrationWASL(wasl_obj.toString());
					System.out.println("Location update for "+driver_BO.getDriverid()+" is "+wasl_BO.getError_message());
				}
				
			}else{
				System.out.println("Wasl Location update for "+assoccode+" has no drivers");
			}
			
			/*if(true){
				return;
			}*/
			
			//Wasl openrequest handling
			ArrayList<OpenRequestBO> jobs_list = UtilityDAO.getWaslPendingTrips(assoccode, timeZone);
			if(jobs_list!=null && jobs_list.size()>0){
				for(int i=0;i<jobs_list.size();i++){
					OpenRequestBO openBo = jobs_list.get(i);
					try {
						JSONObject jobj = new JSONObject(openBo.getCustomField());
						
						if(jobj.getString("vehicleReferenceNumber").equals("")){
							/*jobj.remove("vehicleReferenceNumber");
							jobj.put("vehicleReferenceNumber", "3237122");*/
							System.out.println("Missing values at Vehicle Reference Number for the trip : "+openBo.getTripid());
							continue;
						}
						
						if(jobj.getString("vehicleReferenceNumber").equals("")){
							/*jobj.remove("captainReferenceNumber");
							jobj.put("captainReferenceNumber", "1040961052");*/
							System.out.println("Missing values at Captain Identity Number for the trip : "+openBo.getTripid());
							continue;
						}
						
						if(jobj.getString("durationInSeconds").equals("")){
							jobj.remove("durationInSeconds");
							jobj.put("durationInSeconds", "10");
						}
						
						String rating = "";
						if(openBo.getJobRating()==0){
							rating = "90";
						}else{
							rating = ""+(openBo.getJobRating()*20);
						}
						jobj.put("customerRating", rating);
						
						WaslIntegrationBO wasl_BO = TripRegistration.RegisterTripWASL(jobj.toString());
						//if(!response.equals("") && !response.contains("GRACIE") && !response.equals("Failed")){
						if(wasl_BO.isValid()){
							//Remove from WASl table and update reference key in history table
							AuditDAO.insertJobLogs("WASL Updated with Ref.No : "+wasl_BO.getReferenceNumber(), openBo.getTripid(), assoccode, TDSConstants.tripWASL_updated, "System-WASL", "", "", assoccode);
							int resilt = UtilityDAO.updateWaslReferencekey(assoccode, openBo.getTripid(), wasl_BO.getReferenceNumber());
							
							if(resilt>0){
								System.out.println("WASL Reference key : "+wasl_BO.getReferenceNumber()+" updated for trip :"+openBo.getTripid());
							}else{
								System.out.println("WASL reference key : "+wasl_BO.getReferenceNumber()+" is failed for trip : "+openBo.getTripid());
							}
							
						}else{
							System.out.println("WASL trip update failed for trip : "+openBo.getTripid()+" with message : "+wasl_BO.getError_message());
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}else{
				System.out.println("No WASL update pending jobs found");
			}
			
		}
	}
	
}
