package com.tds.process;

import java.util.ArrayList;

import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.ZoneDAO;

public class LoadZones {

	/*public static boolean loadZonesInAppProfile(HttpServletRequest request){

		boolean successfulLoad = false;
		
		ArrayList<String> zonesInApplication = ZoneDAO.readAllZones();
		for(int i=1;i<zonesInApplication.size(); i++){
			ArrayList<ZoneTableBeanSP> zonesForAssocCode = ZoneDAO.readZonesBoundries(zonesInApplication.get(i), "", true);
		}
		successfulLoad = true;
		
		return successfulLoad;
	}*/
	
	public static ArrayList<ZoneTableBeanSP> returnZonesForAssocCode(String assocCode){

		return ZoneDAO.readZonesBoundries(assocCode, "", true);

	}

	public static ArrayList<ZoneTableBeanSP> returnDetailsOfAZone(String assocCode, String zoneName){

		return ZoneDAO.readZonesBoundries(assocCode, zoneName, false);
	}
	
}
