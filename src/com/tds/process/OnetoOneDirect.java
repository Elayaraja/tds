package com.tds.process;



import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.util.Message;
import com.common.util.SendingSMS;
import com.common.util.TDSProperties;

public class OnetoOneDirect extends Thread {

	public String email;
	public String message;
	public ApplicationPoolBO poolBo;
	
	public OnetoOneDirect(String email,String message,ApplicationPoolBO poolBo) {
		super();
		this.email = email;
		this.message = message;
		this.poolBo = poolBo;
	}

	public void run()
	{
		long start = System.currentTimeMillis();
		boolean m_status = false;
		//String m_smsNo = "";
		//String m_message = "";
		//String m_subject = "TDSTDS";
		SendingSMS m_sendSMS;
		//m_smsNo = ServiceRequestDAO.getDriverPhoneNumberForQueue(p_driverId);
		 
	//	System.out.println("Email: "+ email);
		 
		//if(openRequestBO != null ) {
		//	System.out.println("There is an Open Request for this Driver ");
		
			//m_message = m_subject + ";A;" + openRequestBO.getTripid() + ";" + openRequestBO.getSttime()+";"+"" +p_driverId+";"+System.currentTimeMillis()+";"+qName;
			
			//System.out.println("One to One Message "+ message);
			m_sendSMS = new SendingSMS(poolBo.getSMTP_HOST_NAME(),poolBo.getSMTP_AUTH_USER(),poolBo.getSMTP_AUTH_PWD(),poolBo.getEmailFromAddress(),poolBo.getSMTP_PORT(),poolBo.getProtocol());
			//try {
				//ArrayList al= new ArrayList();
				//al.add(m_smsNo);
				//MailingUtiil.sendMail(poolBO.getSMTP_HOST_NAME(), poolBO.getSMTP_AUTH_USER(), poolBO.getSMTP_AUTH_PWD(), poolBO.getEmailFromAddress(), al, m_message, "");
				//m_sendSMS.sendMail(email,  message);
				
				Message m = new Message(TDSProperties.getValue("smsIP"),Integer.parseInt(TDSProperties.getValue("port")), email, message);
			       m.SetLogin("getac", "8171");
			       try{
			           m.send();
			       }
			       catch (Exception e){
			           System.out.println(e.toString());

			       }

				
			//} catch (Exception p_messagingException) {
			//	System.out.println("Error in Send Message ");
			//	p_messagingException.getStackTrace();
			// }
			m_status = true;
		//} 
//		System.out.println("Total Time For ONE TO ONE: "+ (System.currentTimeMillis() - start));
		Thread.currentThread().stop();
		
	}
	
}
