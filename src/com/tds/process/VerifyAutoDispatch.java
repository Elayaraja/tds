package com.tds.process;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletConfig;

import com.common.util.TDSProperties;
import com.tds.dao.ServiceRequestDAO;

public class VerifyAutoDispatch extends TimerTask {

	ServletConfig config ;
	long queueInterval = 10 * 1000 ;
	public VerifyAutoDispatch(ServletConfig config){
		System.out.println("VerifyAutoDispatch :: Verification initiated");
		this.config = config;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		//System.out.println("VerifyAutoDispatch :: Timer Started");
		try{
			String verifyAutoDispatch = TDSProperties.getValue("VerifyAutoDispatch");
			if(verifyAutoDispatch.equalsIgnoreCase("1")){
				String performQueueProcess = TDSProperties.getValue("PerformQueueProcess");
				if(performQueueProcess.equalsIgnoreCase("1")) {
					ArrayList m_AssocList = ServiceRequestDAO.getAssoccodeForQueue();
					for(int counter = 0; counter < m_AssocList.size(); counter = counter + 3) {
						String asscococde = m_AssocList.get(counter).toString();
						String dispatchType = m_AssocList.get(counter+1).toString();
						String masterAssococde = m_AssocList.get(counter+2).toString();
						long lastTime = -1;
						
						QueueProcess queueProcess = (QueueProcess)config.getServletContext().getAttribute(asscococde+"Process");
						
						if(queueProcess!= null){
							lastTime = (System.currentTimeMillis() - queueProcess.getLastRunTime()) / 1000;
							if(lastTime>30){
								//if QueueProcess is not null, but last run time is greater than 30 Sec, Restart QueueProcess
								if(dispatchType.equalsIgnoreCase("a")|| dispatchType.equalsIgnoreCase("q")|| dispatchType.equalsIgnoreCase("m")){
									Timer jobTimer = (Timer) config.getServletContext().getAttribute(asscococde+"Timer");
									if(jobTimer!=null){
										jobTimer.cancel();
									}
									jobTimer = null;
									
									config.getServletContext().removeAttribute(asscococde+"Timer");
									config.getServletContext().removeAttribute(asscococde+"Process");
									
									QueueProcess queueProcessNew = new QueueProcess(asscococde,masterAssococde, config);
									queueProcessNew.setDispatchingJobs(true);
									Timer timer = new Timer();
									timer.schedule(queueProcessNew,0, queueInterval);
									config.getServletContext().setAttribute(asscococde+"Timer",timer);
									config.getServletContext().setAttribute(asscococde+"Process",queueProcessNew);
									//System.out.println("VerifyAutoDispatch :: AutoDispatch for Assoccode "+asscococde+ " Re-Started with dispatchType "+dispatchType+" in latTime-->"+lastTime);
								}else if(dispatchType.equals("")){
									//System.out.println("VerifyAutoDispatch :: AutoDispatch for Assoccode "+asscococde+ " failed due to unknown dispatchType is --> empty"+" in latTime-->"+lastTime);
								}else if(!dispatchType.equals("") && !dispatchType.equalsIgnoreCase("x")){
									//System.out.println("VerifyAutoDispatch :: AutoDispatch for Assoccode "+asscococde+ " failed due to dispatchType is -->"+dispatchType+" in latTime-->"+lastTime);
								} else {
									//System.out.println("VerifyAutoDispatch :: AutoDispatch for Assoccode "+asscococde+ " failed due to unknown dispatchType is -->"+dispatchType+" in latTime-->"+lastTime);
								}
							}else{
								lastTime = (System.currentTimeMillis() - queueProcess.getLastRunTime()) / 1000;
								//System.out.println("VerifyAutoDispatch :: Job running for Assoccode "+asscococde+" in "+lastTime+" ms ago");
							}
						}else if(queueProcess==null && (dispatchType.equalsIgnoreCase("a")|| dispatchType.equalsIgnoreCase("q")|| dispatchType.equalsIgnoreCase("m"))){
							//Verify QueueProcess is Null, if null, then check If dispatchType is any of a,q,m --> start QueueProcess blindly
							String manualStopped = (String) config.getServletContext().getAttribute(asscococde+"ManualStop");
							if(manualStopped==null || !manualStopped.equalsIgnoreCase("Yes")){
								Timer jobTimer = (Timer) config.getServletContext().getAttribute(asscococde+"Timer");
								if(jobTimer!=null){
									jobTimer.cancel();
								}
								jobTimer = null;
								QueueProcess queueProcessNew = new QueueProcess(asscococde,masterAssococde, config);
								queueProcessNew.setDispatchingJobs(true);
								Timer timer = new Timer();
								timer.schedule(queueProcessNew,30*1000, queueInterval);
								config.getServletContext().setAttribute(asscococde+"Timer",timer);
								config.getServletContext().setAttribute(asscococde+"Process",queueProcessNew);
								//System.out.println("VerifyAutoDispatch :: AutoDispatch(queueProcess null) for Assoccode "+asscococde+ " Started with diapacthType "+dispatchType);
							}else{
								//System.out.println("VerifyAutoDispatch :: AutoDispatch for "+asscococde+" failed due to manually stopped");
							}
						}else{
							//System.out.println("VerifyAutoDispatch :: AutoDispatch(queueProcess null) for Assoccode "+asscococde+ " skipped due to dispatchType "+dispatchType);
						}
					}
				}else{
					//System.out.println("VerifyAutoDispatch :: TDSProperties.Properties - PerformQueueProcess-->"+performQueueProcess);
				}
			}else{
				//System.out.println("VerifyAutoDispatch :: TDSProperties.Properties - VerifyAutoDispatch-->"+verifyAutoDispatch);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
