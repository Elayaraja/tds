package com.tds.process;

import org.asteriskjava.manager.ManagerConnection;
import org.asteriskjava.manager.ManagerConnectionFactory;
import org.asteriskjava.manager.action.OriginateAction;
import org.asteriskjava.manager.response.ManagerResponse;

public class MakePhoneCall {
	
	private String phoneNumber = "";
	private String secondPhoneNumber = "";
	private String message = "";
	private String minutesOfArrival="";
	private final int messageNumber = 0;
	private final boolean synchronous;
    private ManagerConnection managerConnection;
    private boolean conference = false;

	
	public MakePhoneCall(String phoneNumberV, String messageV, String minutes, boolean synchronousV){
		phoneNumber = phoneNumberV;
		message = messageV;
		synchronous = synchronousV;
		minutesOfArrival = minutes;
	}
	public MakePhoneCall(String phoneNumberV, String phoneNumber2V, boolean synchronousV){
		phoneNumber = phoneNumberV;
		secondPhoneNumber = phoneNumber2V;
		synchronous = synchronousV;
		
	}

	
	public int initiateCall(){
		int returnValue = -1;
		String messageToRead = "";
		if (message.equalsIgnoreCase("1")){
			messageToRead = "Your Driver will arrive in " + minutesOfArrival + " minutes";
		} else
			messageToRead = "Your Driver has arrived";
        ManagerConnectionFactory factory = new ManagerConnectionFactory("67.228.191.24", "admin", "amp111");

        this.managerConnection = factory.createManagerConnection();

        OriginateAction originateAction;
        ManagerResponse originateResponse = null;

        originateAction = new OriginateAction();
        //originateAction.setChannel("SIP/ipcomms"+ phoneNumber +"");
        originateAction.setChannel("Local/"+ phoneNumber +"@from-internal");
        originateAction.setCallerId("HareRama");
        originateAction.setApplication("flite");
        originateAction.setData(messageToRead);
        //originateAction.setChannel(channel)
        originateAction.setPriority(new Integer(1));
        originateAction.setTimeout(new Integer(30000));
        try{
	        // connect to Asterisk and log in
	        managerConnection.login();
	
	        // send the originate action and wait for a maximum of 30 seconds for Asterisk
	        // to send a reply
	        originateResponse = managerConnection.sendAction(originateAction, 30000);
        } catch (Exception e){
        	System.out.println(e.toString());
        }
        managerConnection.logoff();

		
		return returnValue;
	}
	public int initiateConf(){
		int returnValue = -1;

        ManagerConnectionFactory factory = new ManagerConnectionFactory("67.228.191.24", "admin", "amp111");

        this.managerConnection = factory.createManagerConnection();

        OriginateAction originateAction = new OriginateAction();
        ManagerResponse originateResponse = null;

        //originateAction.setChannel("SIP/ipcomms"+ phoneNumber +"");
        originateAction.setChannel("Local/913056623775@from-internal");
        originateAction.setCallerId("HareRama");
        originateAction.setApplication("flite");
        originateAction.setExten("5006");
        originateAction.setVariable("PhoneToCall", "7154382222");
        originateAction.setPriority(new Integer(1));
        originateAction.setTimeout(new Integer(30000));
        try{
	        // connect to Asterisk and log in
	        managerConnection.login();
	
	        // send the originate action and wait for a maximum of 30 seconds for Asterisk
	        // to send a reply
	        originateResponse = managerConnection.sendAction(originateAction, 30000);
        } catch (Exception e){
        	System.out.println(e.toString());
        }
        managerConnection.logoff();

		
		return returnValue;
	}
}
