
package com.tds.process;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.io.IOUtils;
import org.eclipse.birt.chart.device.IDeviceRenderer;
import org.eclipse.birt.chart.device.IPrimitiveRenderer;
import org.eclipse.birt.chart.device.ITextRenderer;
import org.eclipse.birt.chart.exception.ChartException;
import org.eclipse.birt.chart.model.attribute.Bounds;
import org.eclipse.birt.chart.model.attribute.Location;
import org.eclipse.birt.chart.model.attribute.TextAlignment;
import org.eclipse.birt.chart.model.component.Label;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.codec.Base64;

/**
 * @author vimal
 * @category Class
 * @see Description
 * This is class is used to send a Mail Using Java Mail API
 * 
 */
public class Email {

	/*private static String SMTP_HOST_NAME = TDSProperties.getValue("mailHostName");// "smtp.gmail.comsmtp.mail.yahoo.co.in"
	private static String SMTP_AUTH_USER = TDSProperties.getValue("mailUserName");
	private static String SMTP_AUTH_PWD = TDSProperties.getValue("mailPassword");
	private static String emailFromAddress = TDSProperties.getValue("mailFromAddress");
	private static String mailPortNo  = TDSProperties.getValue("mailPortNo");*/
	//private Category _cat = TDSController.cat;
	
	private static String SMTP_HOST_NAME = "";
	private static String SMTP_AUTH_USER = "";
	private static String SMTP_AUTH_PWD = "";
	private static String emailFromAddress = "";
	private static String mailPortNo  = "";
	private static String mailProtocol  = "";
	
	public Email(String SMTP_HOST_NAME,String SMTP_AUTH_USER,String SMTP_AUTH_PWD,String emailFromAddress,String mailPortNo,String mailProtocol)
	{
		this.SMTP_AUTH_PWD = SMTP_AUTH_PWD;
		this.SMTP_AUTH_USER = SMTP_AUTH_USER;
		this.SMTP_HOST_NAME  = SMTP_HOST_NAME;
		this.emailFromAddress = emailFromAddress;
		this.mailPortNo = mailPortNo;
		this.mailProtocol= mailProtocol;
	}
	/**
	 * @param p_phoneNo
	 * @param p_subject
	 * @param p_message
	 * @throws MessagingException
	 * @author vimal
	 * @see Description
	 * This sendMail method is used to send mail through JavaMail API The input fields are to address, subject
	 * Message.
	 * 
	 */
	public void sendMail(String toAddress,String sub,String p_message) throws MessagingException {
		Properties m_properties  = System.getProperties();
		m_properties.put("mail.transport.protocol", mailProtocol);
		//m_properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		m_properties.put("mail.smtp.port", mailPortNo);
		m_properties.put("mail.smtp.host",SMTP_HOST_NAME);
		m_properties.put("mail.smtp.auth", "true");
		m_properties.put("mail.smtp.starttls.enable","true");
		m_properties.put("mail.debug", "true");
		Authenticator m_authentication =  new SMTPAuthenticator();
		Session m_session = Session.getDefaultInstance(m_properties,m_authentication);
		m_session.setDebug(true);
		Message m_message = new MimeMessage(m_session);
		Address m_internetFromAddress = new InternetAddress(emailFromAddress);
		m_message.setFrom(m_internetFromAddress);
		m_message.setSubject(sub);
		
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		//messageBodyPart.setText("PFA");

		Multipart multipart = new MimeMultipart();
		
		String filename = "/var/Write.txt";
		//String filename = "/home/ram/Desktop/Write.txt";
		DataSource source = new FileDataSource(filename);
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		
		multipart.addBodyPart(messageBodyPart);
		m_message.setContent(multipart);
		
		if(toAddress.contains(";")){
			String[] toAddresses=toAddress.split(";");
			for(int i=0;i<toAddresses.length;i++){
				Address m_internetToAddress = new InternetAddress(toAddresses[i]); //!!!! After Test Enable !!!!
				//Address m_internetToAddress = new InternetAddress("vijayan.j@amshuhu.com");
				m_message.setRecipient(Message.RecipientType.TO, m_internetToAddress);	
				//System.out.println("To Address--->"+toAddress);
				
				//_cat.info("Send Mail Method "+toAddress);
				m_message.setText(p_message.trim());//( p_message,"text/plain");
				m_message.setSentDate(new Date());
				Transport.send(m_message);
			}
		}else{
			Address m_internetToAddress = new InternetAddress(toAddress); //!!!! After Test Enable !!!!
			//Address m_internetToAddress = new InternetAddress("vijayan.j@amshuhu.com");
			m_message.setRecipient(Message.RecipientType.TO, m_internetToAddress);	
			//System.out.println("Send Mail Method "+toAddress);
			
			//_cat.info("Send Mail Method "+toAddress);
		
			m_message.setText(p_message.trim());//( p_message,"text/plain");
			m_message.setSentDate(new Date());
			Transport.send(m_message);
		}
	
	}
	
	
	public void sendMailPdf(String toAddress,String sub,String p_message,String text_Message) throws MessagingException {
		//System.out.println("Message--->"+mailProtocol);
		Properties m_properties  = System.getProperties();
		m_properties.put("mail.transport.protocol", mailProtocol);
		//m_properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		m_properties.put("mail.smtp.port", mailPortNo);
		m_properties.put("mail.smtp.host",SMTP_HOST_NAME);
//		m_properties.put("mail.smtp.ssl.enable","true");
		m_properties.put("mail.smtp.auth", "true");
		m_properties.put("mail.smtp.starttls.enable","true");
		m_properties.put("mail.smtp.localhost", "gacdispatch");
		m_properties.put("mail.debug", "true");
		Authenticator m_authentication =  new SMTPAuthenticator();
		Session m_session = Session.getDefaultInstance(m_properties,m_authentication);
		m_session.setDebug(true);
		Message m_message = new MimeMessage(m_session);
		Address m_internetFromAddress = new InternetAddress(emailFromAddress);
		m_message.setFrom(m_internetFromAddress);
		m_message.setSubject(sub);
		
		MimeBodyPart messageBodyPart1 = new MimeBodyPart();
		messageBodyPart1.setText(text_Message);

		ByteArrayOutputStream outputStream = null;
		
		//convert string to byteArray
		byte[] buff = Base64.decode(p_message);
		try {
			//create output stream
			outputStream = new ByteArrayOutputStream();
			//Append byte array to outputstraem
			outputStream.write(buff);
			//Create document
			Document document = new Document();
			//create PDF writer to create PDF file
	        PdfWriter.getInstance(document, outputStream);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("Exception:"+e1.getMessage());
			e1.printStackTrace();
		}
		
        //System.out.println("output stream"+outputStream.toString());
        byte[] bytes = ((ByteArrayOutputStream) outputStream).toByteArray();
        
        //construct the pdf body part
        DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
        MimeBodyPart pdfBodyPart = new MimeBodyPart();
        pdfBodyPart.setDataHandler(new DataHandler(dataSource));
        pdfBodyPart.setFileName(sub+".pdf");
		
		Multipart multipart = new MimeMultipart();
		
		multipart.addBodyPart(messageBodyPart1);
		multipart.addBodyPart(pdfBodyPart);
		m_message.setContent(multipart);
		
		if(toAddress.contains(";")){
			String[] toAddresses=toAddress.split(";");
			for(int i=0;i<toAddresses.length;){
				Address m_internetToAddress = new InternetAddress(toAddresses[i]); //!!!! After Test Enable !!!!
				Address m_internetCCAddress = new InternetAddress(toAddresses[i+1]); 
				m_message.setRecipient(Message.RecipientType.TO, m_internetToAddress);
				m_message.setRecipient(Message.RecipientType.CC, m_internetCCAddress);
				//System.out.println("To Address--->"+toAddress);
				m_message.setSentDate(new Date());
				Transport.send(m_message);
				i=toAddress.length();
			}
		}else{
			Address m_internetToAddress = new InternetAddress(toAddress); //!!!! After Test Enable !!!!
			m_message.setRecipient(Message.RecipientType.TO, m_internetToAddress);	
			//System.out.println("Send Mail Method "+toAddress);
			m_message.setSentDate(new Date());
			Transport.send(m_message);
		} 
	
	}
	
	
	/**
	 * @param p_toAddress
	 * @param p_message
	 * @throws MessagingException
	 * @author vimal
	 * @see Description
	 * This sendMail method is used to send mail through JavaMail API. Send a mail to multiple address, subject
	 * Message.
	 */
	public void sendMail(String p_toAddress[],String p_message) throws MessagingException {
		int m_counter = 0;
		//System.out.println("about to send email from email.java"+mailProtocol+":"+mailPortNo+":"+SMTP_HOST_NAME);
		Properties m_properties  = System.getProperties();
		m_properties.put("mail.transport.protocol", mailProtocol);
		m_properties.put("mail.smtp.port", mailPortNo);
		m_properties.put("mail.smtp.host",SMTP_HOST_NAME);
		m_properties.put("mail.smtp.localhost", "gacdispatch");
//		m_properties.put("mail.smtp.ssl.enable","true");
		m_properties.put("mail.smtp.auth", "true");
		m_properties.put("mail.smtp.starttls.enable","true");
		m_properties.put("mail.debug", "true");
		Authenticator m_authentication =  new SMTPAuthenticator();
		Session m_session = Session.getDefaultInstance(m_properties,m_authentication);
		m_session.setDebug(true);
		Message m_message = new MimeMessage(m_session);
		Address m_internetFromAddress = new InternetAddress(emailFromAddress);
		m_message.setFrom(m_internetFromAddress);
		Address[] m_internetToAddress = new InternetAddress[p_toAddress.length];
		for(m_counter=0;m_counter < p_toAddress.length; m_counter ++) {
			//System.out.println("Message to"+ p_toAddress[m_counter]);
			m_internetToAddress[m_counter] = new InternetAddress(p_toAddress[m_counter]);
		}
		//System.out.println("Message to"+ m_internetToAddress);

		m_message.setRecipients(Message.RecipientType.TO, m_internetToAddress);
		//m_message.setSubject(p_subject);
		m_message.setText(p_message.trim());//( p_message,"text/plain");
		Transport.send(m_message);
		//System.out.println("finished sending email from email.java");
	}
	
	/**
	 * @author vimal
	 * @see Description
	 * SimpleAuthenticator is used to do simple authentication
	 * when the SMTP server requires it.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator
	{

	    @Override
		public PasswordAuthentication getPasswordAuthentication()
	    {
	        String username = SMTP_AUTH_USER;
	        String password = SMTP_AUTH_PWD;
	        return new PasswordAuthentication(username, password);
	    }
	}
}
