package com.tds.process;

import java.util.ArrayList;

import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.util.Message;
import com.common.util.SendingSMS;
import com.common.util.TDSProperties;



public class OneToOneSMS extends Thread{


	public ArrayList p_driverIdArray;
	public String p_driverId;
	private boolean broadcastMsg = false;
	public String message;
	private int phone_driver;
	private String serviceProvider = "";
	public ApplicationPoolBO poolBo;
	
	public OneToOneSMS(ArrayList pDriverId,String message,ApplicationPoolBO poolBo,int phone_driver) {
		super();
		this.p_driverIdArray = pDriverId;
		this.message = message;
		this.poolBo = poolBo;
		this.phone_driver= phone_driver;
		this.broadcastMsg = true;
		this.serviceProvider = "Sprint";
	}
	public OneToOneSMS(ArrayList pDriverId,String message,ApplicationPoolBO poolBo,int phone_driver, String provider) {
		super();
		this.p_driverIdArray = pDriverId;
		this.message = message;
		this.poolBo = poolBo;
		this.phone_driver= phone_driver;
		this.broadcastMsg = true;
		this.serviceProvider = "Sprint";
	}


	
	public OneToOneSMS(String pDriverId,String message,ApplicationPoolBO poolBo,int phone_driver) {
		super();
		this.p_driverId = pDriverId;
		this.message = message;
		this.poolBo = poolBo;
		this.phone_driver= phone_driver;
		this.broadcastMsg = false;
		this.serviceProvider = "Sprint";
		
	}
	public OneToOneSMS(String pDriverId,String message,ApplicationPoolBO poolBo,int phone_driver, String provider) {
		super();
		this.p_driverId = pDriverId;
		this.message = message;
		this.poolBo = poolBo;
		this.phone_driver= phone_driver;
		this.broadcastMsg = false;
		this.serviceProvider = provider;
		
	}


	public void run()
	{
		long start = System.currentTimeMillis();
		boolean m_status = false;
		String m_smsNo = "";
		//String m_message = "";
		String m_subject = "TDSTDS";
		SendingSMS m_sendSMS;
		if (false){
			//m_smsNo = ServiceRequestDAO.getDriverPhoneNumberForQueue(p_driverId);
		}
		 
	//	System.out.println("Driver Phone "+p_driverId);
	//	System.out.println("Driver Phone "+p_driverIdArray);
	//	System.out.println("Service Provider "+serviceProvider);
		
		//if(openRequestBO != null ) {
		//	System.out.println("There is an Open Request for this Driver ");
		
			//m_message = m_subject + ";A;" + openRequestBO.getTripid() + ";" + openRequestBO.getSttime()+";"+"" +p_driverId+";"+System.currentTimeMillis()+";"+qName;
			
			//System.out.println("One to One Message "+ message);
			//m_sendSMS = new SendingSMS(poolBo.getSMTP_HOST_NAME(),poolBo.getSMTP_AUTH_USER(),poolBo.getSMTP_AUTH_PWD(),poolBo.getEmailFromAddress(),poolBo.getSMTP_PORT(),poolBo.getProtocol());
			//try {
				//ArrayList al= new ArrayList();
				//al.add(m_smsNo);
				//MailingUtiil.sendMail(poolBO.getSMTP_HOST_NAME(), poolBO.getSMTP_AUTH_USER(), poolBO.getSMTP_AUTH_PWD(), poolBO.getEmailFromAddress(), al, m_message, "");
				//m_sendSMS.sendMail(m_smsNo,  message);
				
				if (serviceProvider.equalsIgnoreCase("Sprint")){
				Message m = null;
				if (broadcastMsg)
				{
					ArrayList al_list = null;
					
				//	System.out.println("In broadcast message");
					if(phone_driver == 1)
						al_list = UtilityDAO.getDriverNoList(p_driverIdArray);
					else
						al_list = p_driverIdArray;
					
					m = new Message(TDSProperties.getValue("smsIP"),Integer.parseInt(TDSProperties.getValue("port")), al_list,  message);
				}else
				{
					String p_driver="";
					if(phone_driver == 1)
						p_driver = UtilityDAO.getDriverNo(p_driverId);
					else
						p_driver = p_driverId;
					
					
					m = new Message("64.28.80.123",444, p_driver,  message);
				}
				
				m.SetLogin("getac", "8171");
			      try{
			          m.send();
			      }
			      catch (Exception e){
			    	  e.printStackTrace();
			          System.out.println(e.toString());


			      }


				
			//} catch (Exception p_messagingException) {
			//	System.out.println("Error in Send Message ");
			//	p_messagingException.getStackTrace();
			//}
			} else
				//Non Sprint phone. Use the email
				//MailingUtiil mail = new MailingUtiil();
				
					
			m_status = true;
		//} 
		//System.out.println("Total Time For ONE TO ONE: "+ (System.currentTimeMillis() - start));
		//Thread.currentThread().stop();
		
	}
	
}