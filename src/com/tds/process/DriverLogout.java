package com.tds.process;

import java.util.ArrayList;
import java.util.TimerTask;

import javax.servlet.ServletConfig;

import com.common.util.MessageGenerateJSON;
import com.common.util.Messaging;
import com.gac.mobile.dao.MobileDAO;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.dao.DispatchDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.DriverLocationBO;

public class DriverLogout extends TimerTask{

	public String assocCode="";
	public String masterAssocCode="";
	public String timeoutdriver ="15";
	public boolean thisDriverRunning = false;
	public ServletConfig servConfig;
	
	public DriverLogout(String assocode,String masterAssociationCode,String timeOut,ServletConfig config){
		this.assocCode = assocode;
		this.masterAssocCode=masterAssociationCode;
		this.timeoutdriver = timeOut;
		this.servConfig = config;
	}

	public void run() {
		if(thisDriverRunning){
			return;
		}
		thisDriverRunning = true;
		ArrayList<DriverLocationBO> allDrivers = UtilityDAO.searchDrivers(masterAssocCode, new DriverLocationBO());
		for(int i=0;i<allDrivers.size();i++){
			boolean logoutSession = MobileDAO.logDriverOut(allDrivers.get(i).getDriverid(),allDrivers.get(i).getVehicleNo(),timeoutdriver.equals("0")?"30":timeoutdriver,masterAssocCode,1);
			if(logoutSession){
				MobileDAO.insertLogoutDetail("", allDrivers.get(i).getDriverid(), "SYSTEM", "Logout By System For TimeOut",masterAssocCode);
				System.out.println("Logout By System For TimeOut for DriverId:"+allDrivers.get(i).getDriverid());
				ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(allDrivers.get(i).getDriverid());
				if(currentQueue!=null && currentQueue.size()>0){
					ServiceRequestDAO.Update_DriverLogOutfromQueue(allDrivers.get(i).getDriverid(), allDrivers.get(i).getVehicleNo(), currentQueue.get(0), assocCode, "2", "Driver("+allDrivers.get(i).getDriverid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". For driver logout session expired");
				}
				ServiceRequestDAO.deleteDriverFromQueue(allDrivers.get(i).getDriverid(), masterAssocCode);
				DriverCabQueueBean cabQueueBean  = DispatchDAO.getDriverByDriverID(masterAssocCode, allDrivers.get(i).getDriverid(), 2);
				String[] message = new String[2];
				message = MessageGenerateJSON.generatePrivateMessage("You are logged out", "LO", System.currentTimeMillis());
				if(cabQueueBean.getDriverid()!=null && !cabQueueBean.getDriverid().equals("")){
					Messaging oneSMS = new Messaging(this.servConfig.getServletContext(), cabQueueBean, message, (ApplicationPoolBO)this.servConfig.getServletContext().getAttribute("poolBO"), assocCode);
					oneSMS.start();
				}
			}
		}
		thisDriverRunning = false;
	}
}
