package com.tds.process;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;

import org.json.JSONArray;
import org.json.JSONObject;

import com.common.util.TDSProperties;

import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.TopicManagementResponse;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;

public class FCMPush {

	public static void sendMessage2(ServletContext context, String messageKey, String messageToBeSent,
			ArrayList<String> driverList) {
		String topic = "Driver";

		try {
			
			/* // [START send_to_token]
		    // This registration token comes from the client FCM SDKs.
		    String registrationToken = "eX02RUCAl7w:APA91bEUehiTwsiouSzGwx9Z_3hQI-3BBEMbFIG8t-fb_H7tklqZ6BI590l92CLaUzkrdKjC42cshme7QR5_gLOlZiXAEm6cWvZLKX9UeZJpwCfc9n1TVcUZbhwd29txD3QncEdQRNTc";

		    // See documentation on defining a message payload.
		    Message message = Message.builder()
		        .putData("score", "850")
		        .putData("time", "2:45")
		        .setToken(registrationToken)
		        .build();

		    // Send a message to the device corresponding to the provided
		    // registration token.
		    String response = FirebaseMessaging.getInstance().send(message);
		    // Response is a message ID string.
		    System.out.println("Successfully sent message: " + response);
		    // [END send_to_token]
*/			
			TopicManagementResponse Subscriber_response;
			// Subscribe the devices corresponding to the registration tokens to
			// the
			// topic.
			Subscriber_response = FirebaseMessaging.getInstance().subscribeToTopic(driverList, topic);
			System.out.println(Subscriber_response.getSuccessCount() + " tokens were subscribed successfully");

			// See documentation on defining a message payload.
			Message message = Message.builder().putData("score", "850").putData("time", "2:45").setTopic(topic).build();
			// Send a message to the devices subscribed to the provided topic.
			String message_response = FirebaseMessaging.getInstance().send(message);
			// Response is a message ID string.
			System.out.println("Successfully sent message: " + message_response);

			// Unsubscribe the devices corresponding to the registration tokens
			// from
			// the topic.
			TopicManagementResponse unsubscribe_response = FirebaseMessaging.getInstance()
					.unsubscribeFromTopic(driverList, topic);
			// See the TopicManagementResponse reference documentation
			// for the contents of response.
			System.out.println(unsubscribe_response.getSuccessCount() + " tokens were unsubscribed successfully");
		} catch (FirebaseMessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void sendMessage(ServletContext context, String messageKey, String messageToBeSent,
			ArrayList<String> driverList) {
			    try{
			    	System.out.println("FCM Entered");
			    // Create URL instance.
			    URL url = new URL("https://fcm.googleapis.com/fcm/send");
			    // create connection.
			    HttpURLConnection conn;
			    conn = (HttpURLConnection) url.openConnection();
			    conn.setUseCaches(false);
			    conn.setDoInput(true);
			    conn.setDoOutput(true);
			    //set method as POST or GET
			    conn.setRequestMethod("POST");
			    //pass FCM server key
			    conn.setRequestProperty("Authorization","key="+TDSProperties.getValue("GCMAPIKey"));
			    //Specify Message Format
			    conn.setRequestProperty("Content-Type","application/json");
			    //Create JSON Object & pass value

			        JSONArray regId = null;
			        JSONObject objData = null;
			        JSONObject data = null;
			        JSONObject notif = null;

			            regId = new JSONArray();
			            for (int i = 0; i < driverList.size(); i++) {
			                regId.put(driverList.get(i));
			            }
			            data = new JSONObject();
			            data.put("payload", messageToBeSent);
			            notif = new JSONObject();
			            notif.put("title", "Drivers");

			            objData = new JSONObject();
			            objData.put("registration_ids", regId);
			            objData.put("data", data);
			            objData.put("notification", notif);
			            System.out.println("!_@rj@_group_PASS:>"+ objData.toString());


			    //System.out.println("json :" +objData.toString());
			    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

			    wr.write(objData.toString());
			    wr.flush();
			    int status = 0;
			    if( null != conn ){
			    status = conn.getResponseCode();
			    }
			    if( status != 0){

			    	if( status == 200 ){
			    //SUCCESS message
			    BufferedReader reader = new BufferedReader(new 
			    InputStreamReader(conn.getInputStream()));
			    System.out.println("Android Notification Response : " + 
			    reader.readLine());
			    }else if(status == 401){
			    //client side error
			    System.out.println("Notification Response : TokenId : " + 000 + "Error occurred :");
			    }else if(status == 501){
			    //server side error
			    System.out.println("Notification Response : [ errorCode=ServerError ]TokenId : " + 000);
			    }else if( status == 503){
			    //server side error
			    System.out.println("Notification Response : FCM Service is Unavailable TokenId : " + 000);
			    }
			    }
			    }catch(MalformedURLException mlfexception){
			    // Prototcal Error
			    System.out.println("Error occurred while sending push Notification!.." + 
			    mlfexception.getMessage());
			    }catch(IOException mlfexception){
			    //URL problem
			    System.out.println("Reading URL, Error occurred while sending push Notification!.." + mlfexception.getMessage());
			    }catch (Exception exception) {
			    //General Error or exception.
			    	System.out.println("Error occurred while sending push Notification!.." + 
			    	exception.getMessage());
			    }
	}


	public static void sendMessageCustomer(ServletContext context, String customerKey, String messageToBeSent) {
		ArrayList<String> tokenList=new ArrayList<String>();
		tokenList.add(customerKey);
		sendMessage(context, "0", messageToBeSent, tokenList);
		/*
		 * try{ Sender sender = new Sender(TDSProperties.getValue("GCMAPIKey"));
		 * Message message = new Message.Builder() .collapseKey("1")
		 * //.timeToLive(3) .delayWhileIdle(false)
		 * .addData("payload",messageToBeSent) .build(); Result result =
		 * sender.send(message,customerKey, 1);
		 * //System.out.println(result.toString()); } catch (Exception e) {
		 * System.err.println(e); e.printStackTrace(); } finally { }
		 */
	}

}
