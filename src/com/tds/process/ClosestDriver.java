package com.tds.process;

import java.util.ArrayList;

import com.tds.cmp.bean.DriverCabQueueBean;

public class ClosestDriver{
	
	public static DriverCabQueueBean findClosestDriver(ArrayList<DriverCabQueueBean> dr,double latitude, double longitude,double maxDist,String tripId,String assoccode,String masterAssoccode) {
		DriverCabQueueBean closestDriver = null;
		double closestDriverDistance = 7000;
		double currentDriverDistance = 0;
		StringBuffer finalResult = new StringBuffer();
		for (int i =0; i<dr.size(); i++){
			currentDriverDistance = distance(dr.get(i).getCurrentLatitude(), dr.get(i).getCurrentLongitude(),latitude, longitude, 0); 
			finalResult.append("Driver:"+dr.get(i).getDriverid()+";Vehicle:"+dr.get(i).getVehicleNo()+";Distance:"+currentDriverDistance+"^");
			if ( currentDriverDistance < closestDriverDistance){
				closestDriver = dr.get(i);
				closestDriverDistance = currentDriverDistance;
			}
		}
//		AuditDAO.insertJobLogs("Distance Of Drivers "+finalResult, tripId,assoccode,TDSConstants.performingDispatchProcesses, "System","","",masterAssoccode);
		System.out.println(finalResult);
		return closestDriver;
		
	}
	
	public static double distance(double lat1, double lon1, double lat2, double lon2, int unit) {
		//Pass 0 for Miles
		//Pass 1 for KM
		//Pass 2 for NM
		  double theta = lon1 - lon2;
		  double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		  dist = Math.acos(dist);
		  dist = rad2deg(dist);
		  dist = dist * 60 * 1.1515;
		  if (unit == 1) {
		    dist = dist * 1.609344;
		  } else if (unit == 2) {
		  	dist = dist * 0.8684;
		  }
		  return (dist);
		}
	private static double deg2rad(double deg) {
		  return (deg * Math.PI / 180.0);
		}

		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
		/*::  This function converts radians to decimal degrees             :*/
		/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private static double rad2deg(double rad) {
		  return (rad * 180.0 / Math.PI);
		}
}
