package com.tds.process;

import java.util.ArrayList;

import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.util.Message;
import com.common.util.SendingSMS;
import com.common.util.TDSProperties;

public class BroadCastSMS extends Thread{
	
	public String p_assocode;
	public ArrayList al_openList;
	public String qName;
	public ApplicationPoolBO poolBo;
	
	public BroadCastSMS(String pAssocode, ArrayList al_openList,String qName,ApplicationPoolBO poolBo) {
		super();
		this.p_assocode = pAssocode;
		this.al_openList = al_openList;
		this.qName = qName;
		this.poolBo = poolBo;
	}

	public void run()
	{
		long start = System.currentTimeMillis();
		String m_message = "";
		String m_subject = "TDSTDS";
		 
		SendingSMS m_sendSMS;
		OpenRequestBO openRequestBO = null;
		
		ArrayList al_list =  UtilityDAO.getDriverNoQ(p_assocode);
		for( int counter = 0;counter<al_openList.size();counter++)
		{
			openRequestBO = (OpenRequestBO)al_openList.get(counter);
			m_sendSMS = new SendingSMS(poolBo.getSMTP_HOST_NAME(),poolBo.getSMTP_AUTH_USER(),poolBo.getSMTP_AUTH_PWD(),poolBo.getEmailFromAddress(),poolBo.getSMTP_PORT(),poolBo.getProtocol());
			try {
				
				//MailingUtiil.sendMail(poolBO.getSMTP_HOST_NAME(), poolBO.getSMTP_AUTH_USER(), poolBO.getSMTP_AUTH_PWD(), poolBO.getEmailFromAddress(), al_list, m_message, "");
				/*for(int i=0;i<al_list.size();i=i+2){
					m_message = m_subject + ";B;" + openRequestBO.getTripid() + ";" + openRequestBO.getSttime()+";"+"" +al_list.get(i).toString()+";"+System.currentTimeMillis()+";"+qName;
					System.out.println("Broad Cast Message:" + m_message);
					m_sendSMS.sendMail(al_list.get(i+1).toString(),  m_message);
				}*/
				
				for(int i=0;i<al_list.size();i=i+2){
					m_message = m_subject + ";B;" + openRequestBO.getTripid() + ";" + openRequestBO.getSttime()+";"+"" +al_list.get(i).toString()+";"+System.currentTimeMillis()+";"+qName;
					Message m = new Message(TDSProperties.getValue("smsIP"),Integer.parseInt(TDSProperties.getValue("port")), al_list.get(i+1).toString(), m_message);
				       m.SetLogin("getac", "8171");
				       try{
				           m.send();
				       }
				       catch (Exception e){
				           System.out.println(e.toString());

				       }
				}
				 
			       
			} catch (Exception p_messagingException) {
				//System.out.println("Error in Send Message ");
				p_messagingException.getStackTrace();
			}
			
			
			
		}
		
		//System.out.println("Total Time For Broad Cast: "+ (System.currentTimeMillis() - start));
	}
	
}
