package com.tds.process;

import java.util.ArrayList;
import java.util.TimerTask;

import com.tds.dao.AdministrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.util.ShortestPathFiding;


public class PerformAutomaticAllocation extends TimerTask {
	
	public static String assocode;
	
	public PerformAutomaticAllocation(String assocode)
	{
		this.assocode = assocode;
	}
	
	@Override
	public synchronized void  run() {
		setAutomaticAlloction(assocode);
	}
	
	public static void setAutomaticAlloction(String assocode)
	{
		
		ArrayList al_trip = null,al_driver  = null;
		OpenRequestBO requestBO = null;
		al_trip = RequestDAO.getAllOpenRequestForAutoAlloc(assocode);
		
		dotheStopProcess(al_trip,assocode);
					
		al_driver = UtilityDAO.getDriverLocationList(assocode);
		for(int i=0;i<al_trip.size();i++)
		{
			requestBO = (OpenRequestBO)al_trip.get(i);
			requestBO = ShortestPathFiding.getShortestDriver(al_driver,requestBO);
			ServiceRequestDAO.insertAcceptedRequest(requestBO);
		}
		
		 
		 
	}
 
	
	public static void dotheStopProcess(ArrayList al_trip,String assoccode)
	{
		OpenRequestBO requestBO = null;
		for(int i=0;i<al_trip.size();i++)
		{
			requestBO = (OpenRequestBO)al_trip.get(i);
			AdministrationDAO.stopQueueProcessByOpr(requestBO.getTripid(),"S", assoccode);
			
		}
		AdministrationDAO.deleteQueue(assoccode);
	}
}
