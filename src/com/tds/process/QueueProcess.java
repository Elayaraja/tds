package com.tds.process;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.TimerTask;

import javax.servlet.ServletConfig;

import org.apache.log4j.Category;

import java.nio.file.FileSystems;
import java.nio.file.Paths;

import com.nexmo.client.NexmoClient;
import com.nexmo.client.auth.AuthMethod;
import com.nexmo.client.auth.JWTAuthMethod;
import com.nexmo.client.voice.Call;
import com.nexmo.client.voice.CallEvent;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.TDSController;
import com.tds.dao.AuditDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.ZoneDAO;
import com.tds.ivr.IVRDriverCaller;
import com.tds.ivr.IVRPassengerCallerJob;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.DispatchPropertiesBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.twilio.sdk.TwilioRestException;
import com.common.util.MessageGenerateJSON;
import com.common.util.Messaging;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;
import com.common.util.sendSMS;

public class QueueProcess extends TimerTask {

	public String assoccode = "";
	public String masterAssocCode="";
	public double maxDistance = 5;
	public int dispatchTypeOnNoZone = 0;
	public int dispatchAdvaceTime = 15*90;
	public int driverResponseTime = 90;
	public static ServletConfig config;
	public boolean generalReqFlag = false;
	public boolean thisJobIsRunning = false;
	public int broadcastDelaytime = 0;
	public boolean markRequestsAsPublic = true;
	public double dispatchDistance = 0;
	public long lastRunTime = 0;
	public long maintenanceJobLastRunTime = 0;
	public String defaultZoneName = "";
	public String jobName = "System";
	public int runFrequecy = 10;
	//private AdminRegistrationBO adminBO;
	private String timeOffset = "";
	//private String emailHost ="";
	//private String emailUserID = "";
	//private String emailPassword = "";
	private boolean defaultZoneDontMarkPublicRequests = false;
	private boolean dispatchingJobs = true;
	private DispatchPropertiesBO messageFields = null;
	private String driverAlarm = "0";
	private String timeZone="";
	private String phonePrefix;
	private int sharedRideProcess=0;
	private int bcJobs=0;
	ArrayList<String> companiesToLook = new ArrayList<String>();
	private  Category cat = TDSController.cat;
	private boolean is_send_msg_if_driver_not_reacted = false;
	private String smsPrefix;

	public QueueProcess(String assocode,String masterAssociationCode,ServletConfig config){
		this.assoccode = assocode;
		this.masterAssocCode=masterAssociationCode;
		this.timeZone=timeZone;
		//this.masterAssocCode = _masterAssoccode;
		ZoneTableBeanSP defaultZone = null;
		this.config = config;
		if(true){
			timeOffset = SystemPropertiesDAO.getParameter(masterAssocCode, "timeZoneArea");
			driverAlarm = SystemPropertiesDAO.getParameter(masterAssocCode, "driverAlarm");
			defaultZone = ZoneDAO.getDefaultZone(masterAssocCode);
		} else {
			timeOffset = SystemPropertiesDAO.getParameter(assoccode, "timeZoneArea");
			driverAlarm = SystemPropertiesDAO.getParameter(assoccode, "driverAlarm");
			defaultZone = ZoneDAO.getDefaultZone(assoccode);
		}
		phonePrefix = SystemPropertiesDAO.getParameter(masterAssocCode, "prefixPhone");
		smsPrefix = SystemPropertiesDAO.getParameter(masterAssocCode, "prefixSMS");
		String srJobs=SystemPropertiesDAO.getParameter(masterAssociationCode, "sharedRideBatch");
		String broadcastJobs=SystemPropertiesDAO.getParameter(masterAssociationCode, "sendBroadcastJob");
		sharedRideProcess = Integer.parseInt((srJobs==null||srJobs.equals(""))?"0":srJobs);
		bcJobs = Integer.parseInt((broadcastJobs==null||broadcastJobs.equals(""))?"0":broadcastJobs);
		//ZoneTableBeanSP defaultZone = ZoneDAO.getDefaultZone(assoccode);
		
		String msg_send = SystemPropertiesDAO.getParameter(masterAssociationCode, "power_mode");
		if(!msg_send.equals("") && msg_send.equals("1")){
			is_send_msg_if_driver_not_reacted = true;
		}
		
		if(assoccode.equals(masterAssocCode)){
			companiesToLook = SecurityDAO.getAllAssoccode(masterAssocCode);
			if(companiesToLook==null || companiesToLook.size()<1){
				companiesToLook.add(assoccode);
			}
		} else {
			companiesToLook.add(assoccode);
		}
		if(defaultZone!=null){
			dispatchAdvaceTime = defaultZone.getAdvance();
			driverResponseTime = defaultZone.getWaitTime();
			if(defaultZone.getBroadCastTime()!=null){
				broadcastDelaytime = defaultZone.getBroadCastTime()*60;
			} else {
				System.out.println("Setting public requests to true");
				defaultZoneDontMarkPublicRequests = true;	
			}
			System.out.println("BroadCastDelayTime:"+broadcastDelaytime);
			System.out.println("BroadCastDelayTime:"+defaultZoneDontMarkPublicRequests);

			maxDistance = defaultZone.getDistance();
			dispatchDistance = defaultZone.getDistance();
			defaultZoneName = defaultZone.getZoneKey();
			if(defaultZone.getDispatchType().equals("SD")){
				dispatchTypeOnNoZone =1;			
			} else {
				dispatchTypeOnNoZone =2;
			}
			if(defaultZone.getDispatchType().equals("D") || defaultZone.getDispatchType().equals("M")){
				dispatchTypeOnNoZone =0;
			}
			messageFields = SystemPropertiesDAO.getDispatchPropeties(masterAssocCode);

		}
		cat = Category.getInstance(QueueProcess.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ QueueProcess.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");


	}


	@Override
	public void run() {

		if(thisJobIsRunning){
			return;
		}
		//System.out.println("VErsion 1");
		ArrayList<String> unavailableDrivers = new ArrayList<String>();
//		ArrayList<String> uD = new ArrayList<String>();
		thisJobIsRunning = true;
		long start = System.currentTimeMillis();
		ApplicationPoolBO poolBO = (ApplicationPoolBO)config.getServletContext().getAttribute("poolBO");

		ArrayList<DriverCabQueueBean>  drList = new ArrayList<DriverCabQueueBean>(); 
		ArrayList<OpenRequestBO> opreqList = new ArrayList<OpenRequestBO>(); 

		DriverCabQueueBean cabQueueBean = null;
		/**Zone List*/
		OpenRequestBO openRequestBO = null;
		//OR_TRIP_STATUS (FOR TRIP STATUS CHECK TDSCONSTANT)
		opreqList	= ServiceRequestDAO.getOpenRequestDetailOnTime(assoccode, masterAssocCode, timeOffset,sharedRideProcess);
		String currentQueueInProcess = "";

		boolean processNoMoreGenericRequestsInThisQueue = false;
		int currentDriver = 0;
		for(int i=0;i<opreqList.size();i++)	{
			if(opreqList.get(i).getChckTripStatus() == TDSConstants.performingDispatchProcesses){
				if(opreqList.get(i).getDriverid()!=null && !opreqList.get(i).getDriverid().equals("")){
					unavailableDrivers.add(opreqList.get(i).getDriverid());
				}
			}
		}
		//unavailableDrivers.t
		for(int i=0;i<opreqList.size();i++)	{	
			openRequestBO = opreqList.get(i);
			//Condition 2
			//drList = ServiceRequestDAO.getDriverDetail(openRequestBO.getQueueno(),assoccode);				
			if (openRequestBO.getChckTripStatus() == TDSConstants.customerCancelledRequest || openRequestBO.getChckTripStatus() == TDSConstants.noShowRequest || 
					openRequestBO.getChckTripStatus() == TDSConstants.companyCouldntService	||openRequestBO.getChckTripStatus() == TDSConstants.tripCompleted){
				//Move a closed request to history table	
//				String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), masterAssocCode);
//				if(openRequestBO.getAssociateCode().equals(masterAssocCode)){
					DispatchDAO.moveToHistory(openRequestBO.getTripid(),assoccode, "",companiesToLook);
					AuditDAO.insertJobLogs("Job Cancelled/Completed", openRequestBO.getTripid(),assoccode,openRequestBO.getChckTripStatus(), jobName,"","",masterAssocCode);
//				} else {
//					DispatchDAO.moveToHistory(openRequestBO.getTripid(),assoccode, "",companiesToLook);
//				}
//				DispatchDAO.moveToHistory(openRequestBO.getTripid(), assoccode,"");
				cat.info("Job:"+openRequestBO.getTripid()+":Moving job to history");
				continue;
			}
//			ArrayList<String> companiesToLook = new ArrayList<String>();
//			if(openRequestBO.getAssociateCode().equals(masterAssocCode)){
//				if(config.getServletContext().getAttribute(masterAssocCode + "Fleets") == null){
//					SystemUtils.reloadFleets(config, masterAssocCode);
//				}
//				companiesToLook = (ArrayList<String>) config.getServletContext().getAttribute(masterAssocCode + "Fleets");
//			} else {
//				companiesToLook.add(assoccode);
//			}
			//Condition 1
			//Job not in Zone. Allocate
			if(openRequestBO.getQueueno().equals("")){
				//If Trip is on hold go to next trip
				cat.info("Job:"+openRequestBO.getTripid()+":DefaultZone Logic");

				if(openRequestBO.getChckTripStatus() == TDSConstants.tripOnHold ){
					continue;
				}
				//If Trip has no lat and long then mark it as "On Hold"
				if(openRequestBO.getSlat()==null || openRequestBO.getSlong()==null || openRequestBO.getSlat().equals("0.0") ||openRequestBO.getSlong().equals("0.0") ){
					//If Job has been manually changed to a certain state even though the system identifies it as a OnHold job
					// then dont alter the job status. ITs being manually dispatched.

					if(openRequestBO.getChckTripStatus() != TDSConstants.newRequestDispatchProcessesNotStarted){
						continue;
					}
					cat.info("Job:"+openRequestBO.getTripid()+":Marking job as on Hold");

					ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.tripOnHold+"" , "", "", "","");
					openRequestBO.setChckTripStatus(TDSConstants.tripOnHold);

					AuditDAO.insertJobLogs("Trip On Hold", openRequestBO.getTripid(),assoccode,TDSConstants.tripOnHold, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
					continue;
				}
				if(openRequestBO.getQC_DELAYTIME() <0){
					if(openRequestBO.getTimeToStartTripInSeconds() > dispatchAdvaceTime){
						cat.info("Job:"+openRequestBO.getTripid()+":Updating dispatch start time to correct time");
						DispatchDAO.updateDispatchStartTime(openRequestBO, dispatchAdvaceTime, masterAssocCode);
						continue;
					} else {
						openRequestBO.setQC_DELAYTIME(dispatchAdvaceTime);
					}
				} else if(openRequestBO.getTimeToStartTripInSeconds() >  openRequestBO.getQC_DELAYTIME()){
					//Skip request since time is > than current window of processing
					cat.info("Job:"+openRequestBO.getTripid()+":Updating dispatch start time to correct time");
					DispatchDAO.updateDispatchStartTime(openRequestBO, openRequestBO.getQC_DELAYTIME(), masterAssocCode);
					continue;
				}				
				//If no zone is found,  the job is put on hold if DispatchType = 0
				if (dispatchTypeOnNoZone == 0){
					cat.info("Job:"+openRequestBO.getTripid()+":dipsatch type on nozone is 0");

					//If Job has been manually changed to a certain state even though the system identifies it as a OnHold job
					// then dont alter the job status. ITs being manually dispatched.
					if(openRequestBO.getChckTripStatus() != TDSConstants.newRequestDispatchProcessesNotStarted){
						continue;
					}
					ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.tripOnHold+"" , "", "","","");
					openRequestBO.setChckTripStatus(TDSConstants.tripOnHold);

					AuditDAO.insertJobLogs("Trip On Hold", openRequestBO.getTripid(),assoccode,TDSConstants.tripOnHold, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

					continue;
				} else if (dispatchTypeOnNoZone == 1){
					//If no zone is found,  the job is sent to shortest distance if DispatchType = 1
					openRequestBO.setDispatchMethod("SD");
					//Updating the trip to in progress if its not already in progress
					if((openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted) || (openRequestBO.getChckTripStatus() == TDSConstants.srAllocatedToCab)){
						if(openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted){
							openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);
						}
						ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), openRequestBO.getChckTripStatus()+"" , openRequestBO.getDriverid() , "",openRequestBO.getVehicleNo(),"");

						ServiceRequestDAO.updateOpenRequestZone(openRequestBO.getTripid(), defaultZoneName);
						AuditDAO.insertJobLogs("Performing Dispatch", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatch, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
					}
				} else if (dispatchTypeOnNoZone == 2){
					openRequestBO.setDispatchMethod("S");
					//Updating the trip to in progress if its not already in progress
					if(openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted){
						openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);
						ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.performingDispatchProcesses+"" , "", "","","");							
						openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);

						ServiceRequestDAO.updateOpenRequestZone(openRequestBO.getTripid(), defaultZoneName);
						AuditDAO.insertJobLogs("Performing Dispatch", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatch, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

					}
				} 
				//If Trip is not to be processed now move to next trip
				//if(openRequestBO.getChckTripStatus() != TDSConstants.newRequestDispatchProcessesNotStarted){
				//	continue;
				//}
				cat.info("Job:"+openRequestBO.getTripid()+":Setting Switches"+defaultZoneDontMarkPublicRequests);

				openRequestBO.setBroadCastDelay(broadcastDelaytime);
				openRequestBO.setDontMarkRequestsAsPublic(defaultZoneDontMarkPublicRequests);
				openRequestBO.setDispatchDistance(dispatchDistance);
				openRequestBO.setDontMarkRequestsAsPublic(defaultZoneDontMarkPublicRequests);
			}
			//Check for null on important fields
			if(openRequestBO.getDriverid()==null){
				openRequestBO.setDriverid("");
			}

			//Condition 3
			if(openRequestBO.getTimeToStartTripInSeconds() >  openRequestBO.getQC_DELAYTIME()){
				cat.info("Job:"+openRequestBO.getTripid()+":Updating dispatch start time to correct time");
				//Skip request since time is > than current window of processing
				DispatchDAO.updateDispatchStartTime(openRequestBO, openRequestBO.getQC_DELAYTIME(), masterAssocCode);
				AuditDAO.insertJobLogs("Updated Dispatch Start Time", openRequestBO.getTripid(),assoccode,TDSConstants.broadcastTrip, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
				continue;
			}
			if(openRequestBO.getChckTripStatus() == TDSConstants.broadcastRequest){
				cat.info("Job:"+openRequestBO.getTripid()+":Skipping since its broadcast request");
				//Skip if its a broadcast request
				//continue;
			}

			//Below is to change the dispatch status to started dispatch. This is done so that if there are no drivers in the queue and its not be marked public at LineNo:A then the job needs to be marked as being public. 
			if(openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted){
				cat.info("Job:"+openRequestBO.getTripid()+":Updating job as dispatching");
				ServiceRequestDAO.updateOpenRequestStatusAlone(openRequestBO.getTripid(), TDSConstants.performingDispatchProcesses + "");
				openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);
				AuditDAO.insertJobLogs("Performing Dispatch", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatch, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
			}
			//If Shared Ride & Don't Dispatch SR Is Turned On Just Continue
			if(openRequestBO.getTypeOfRide().equals("1") && sharedRideProcess==1){
				continue;
			}
			//If Shared Ride & Route Number Is Not Allocated (OR) Its Allocated But Not The First Job Of The Route Then Continue
			if(openRequestBO.getTypeOfRide().equals("1") && (openRequestBO.getRouteNumber().equals("") ||(!openRequestBO.getRouteNumber().equals("") && openRequestBO.getPickupOrder()!=null && !openRequestBO.getPickupOrder().equals("1")))){
				continue;
			}
			if(openRequestBO.getDispatchMethod()== null ){
				ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.tripOnHold+"" , "", "", "","");
				openRequestBO.setChckTripStatus(TDSConstants.tripOnHold);

				AuditDAO.insertJobLogs("Trip On Hold.Dispatch Type?", openRequestBO.getTripid(),assoccode,TDSConstants.tripOnHold, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);					
			}

			if(!dispatchingJobs){
				continue;
			}
			if(openRequestBO.getDontDispatch()>0){
				if(openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses && !openRequestBO.getDriverid().equals("")){
					if(openRequestBO.getPhoneCallDelayTime()>0 && (openRequestBO.getSMS_TIME_DIFF()+(runFrequecy/2))> openRequestBO.getPhoneCallDelayTime() && (openRequestBO.getSMS_TIME_DIFF()+(runFrequecy/2))< (openRequestBO.getPhoneCallDelayTime()+runFrequecy) ){
						DriverLocationBO driverDetails = DispatchDAO.getPhoneForDriverID(openRequestBO.getDriverid(), masterAssocCode);
						if(driverDetails!=null){
							try{
								//System.out.println("Making call @"+openRequestBO.getSMS_TIME_DIFF()+":with parms-->"+openRequestBO.getDriverid() +":"+ openRequestBO.getTripid()+":"+ phonePrefix+driverDetails.getPhone()+":"+ ""+":"+ ""+":"+ ""+":"+ openRequestBO.getVehicleNo()+":"+assoccode+":"+ timeOffset);
								IVRDriverCaller ivr = new IVRDriverCaller(openRequestBO.getDriverid(), openRequestBO.getTripid(), phonePrefix+driverDetails.getPhone(), "", "", "", openRequestBO.getVehicleNo(),assoccode, timeOffset,masterAssocCode,config);
								ivr.start();
								AuditDAO.insertJobLogs("IVR Call to" +driverDetails.getPhone(), openRequestBO.getTripid(),assoccode,TDSConstants.ivrCall, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

							}catch (Exception e){
								System.out.println("Error Making Phone call " + e.toString() );
							}
						}
					}
					
					if(is_send_msg_if_driver_not_reacted && (openRequestBO.getSMS_TIME_DIFF()>10 && openRequestBO.getSMS_TIME_DIFF()<20 )){
						//System.out.println("Have to send a message");
						DriverLocationBO driverDetails = DispatchDAO.getPhoneForDriverID(openRequestBO.getDriverid(), assoccode);
						try {
							if(driverDetails!=null){
								String msg = "You have allocated to new Trip! Go to https://play.google.com/store/apps/details?id=com.gac.action&hl=en";
								System.out.println("Sending Message to driver for Not accepted - TripId : "+openRequestBO.getTripid() +" , DriverId : "+openRequestBO.getDriverid());
								sendSMS.main(msg, smsPrefix+driverDetails.getPhone(), assoccode);
								AuditDAO.insertJobLogs("Sending Message to driver for Not accepted "+openRequestBO.getDriverid()+":"+driverDetails.getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName, openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
							}
							
						} catch (TwilioRestException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.out.println("Exception on sending message to driver on job not accepted : "+openRequestBO.getDriverid());
						}
					}
				}

				continue;
			}
			//IF JOB ALLOCATED TO A SPECIFIC CAB Then look for the cab and give it to driver. If not available, 
			if(openRequestBO.getChckTripStatus() == TDSConstants.srAllocatedToCab){
				DriverCabQueueBean dr = null;
				//System.out.println("Status 37,Vehicle No:"+openRequestBO.getVehicleNo());
				if (!openRequestBO.getVehicleNo().equals("")){
					dr = DispatchDAO.getDriverForCabNumber(1, masterAssocCode, openRequestBO.getVehicleNo(), "","Y");
				} else if(openRequestBO.getDriverid().equals("")){
					dr = DispatchDAO.getDriverForCabNumber(2, masterAssocCode,"", openRequestBO.getDriverid(), "Y");
				}
				if(dr==null){
					ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.tripOnHold+"" , openRequestBO.getDriverid(), "", openRequestBO.getVehicleNo(),"");
					openRequestBO.setChckTripStatus(TDSConstants.tripOnHold);
					AuditDAO.insertJobLogs("**SR**On Hold. Req cab not available", openRequestBO.getTripid(),assoccode,TDSConstants.tripOnHold, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
				}else if(ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.srJobHold+"" , dr.getDriverid(), "", dr.getVehicleNo(),"" )>0){
					openRequestBO.setTripStatus(TDSConstants.performingDispatchProcesses+"");
					AuditDAO.insertJobLogs("**SR**Offering Job To Driver,Put On Hold To Prevent Other Allocation"+dr.getDriverid()+":"+dr.getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
					cat.info("Job:"+openRequestBO.getTripid()+":Sending message to driver");
					long msgID = System.currentTimeMillis();
					String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID,messageFields,masterAssocCode);
					Messaging oneSMS = new Messaging(config.getServletContext(),dr, message,poolBO,assoccode);
					oneSMS.start();
				} else {
					ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.tripOnHold+"" , "", "", "","");
					openRequestBO.setChckTripStatus(TDSConstants.tripOnHold);

					AuditDAO.insertJobLogs("**SR**On Hold. Error alloc cab", openRequestBO.getTripid(),assoccode,TDSConstants.tripOnHold, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

				}
				continue;
			}


			//Go to next job if current job has been with driver not for long. If Call driver function is enable make IVR call to driver.
			if((openRequestBO.getSMS_TIME_DIFF() < openRequestBO.getQC_SMS_RES_TIME()) && (openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses|| openRequestBO.getChckTripStatus() == TDSConstants.srJobHold) && !openRequestBO.getDriverid().equals("")){
				//TODO
				//Add variable into application pool if its not there. This need not be done now because we can have the android device remind the driver.
				cat.info("Job:"+openRequestBO.getTripid()+":Not reached wait time for driver after job offered to him/her");
				System.out.println("Not reached wait time for driver after job offered to him/her : "+openRequestBO.getTripid());
				//Making a phone call if the job is no accepted within the phonecalldelaytime.
				if(openRequestBO.getPhoneCallDelayTime()>0 && (openRequestBO.getSMS_TIME_DIFF()+(runFrequecy/2))> openRequestBO.getPhoneCallDelayTime() && (openRequestBO.getSMS_TIME_DIFF()+(runFrequecy/2))< (openRequestBO.getPhoneCallDelayTime()+runFrequecy) ){
					DriverLocationBO driverDetails = DispatchDAO.getPhoneForDriverID(openRequestBO.getDriverid(), assoccode);
					if(driverDetails!=null){
						try{
							//System.out.println("Making call @"+openRequestBO.getSMS_TIME_DIFF()+":with parms-->"+openRequestBO.getDriverid() +":"+ openRequestBO.getTripid()+":"+ phonePrefix+driverDetails.getPhone()+":"+ ""+":"+ ""+":"+ ""+":"+ openRequestBO.getVehicleNo()+":"+assoccode+":"+ timeOffset);
							IVRDriverCaller ivr = new IVRDriverCaller(openRequestBO.getDriverid(), openRequestBO.getTripid(), phonePrefix+driverDetails.getPhone(), "", "", "", openRequestBO.getVehicleNo(),assoccode, timeOffset,masterAssocCode,config);
							ivr.start();
							AuditDAO.insertJobLogs("IVR Call to " +driverDetails.getPhone(), openRequestBO.getTripid(),assoccode,TDSConstants.ivrCall, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
						}catch (Exception e){
							System.out.println("Error Making Phone call " + e.toString() );
						}
					}
				}
				if(is_send_msg_if_driver_not_reacted && openRequestBO.getSMS_TIME_DIFF()<15){
					//System.out.println("Have to send a message");
					DriverLocationBO driverDetails = DispatchDAO.getPhoneForDriverID(openRequestBO.getDriverid(), assoccode);
					
						if(driverDetails!=null){
							/*try {
								String msg = "You have allocated to new Trip! Go to https://play.google.com/store/apps/details?id=com.gac.action&hl=en";
								System.out.println("Sending Message to driver for Not accepted - TripId : "+openRequestBO.getTripid() +" , DriverId : "+openRequestBO.getDriverid());
								sendSMS.main(msg, smsPrefix+driverDetails.getPhone(), assoccode);
								AuditDAO.insertJobLogs("Sending Message to driver for Not accepted "+openRequestBO.getDriverid()+":"+driverDetails.getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName, openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
							} catch (TwilioRestException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								System.out.println("Exception on sending message to driver on job not accepted : "+openRequestBO.getDriverid());
							}*/
							
							try {
								AuditDAO.insertJobLogs("Nexmo call to driver for Not accepted "+openRequestBO.getDriverid()+":"+driverDetails.getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName, openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
								NexmoClient client = new NexmoClient(new JWTAuthMethod("87a6d766-53e1-490f-b5d3-067ba84ac931",FileSystems.getDefault().getPath("/usr/local/privateram.key")));
								
								Call call = new Call(
								        "919566432877",
								        "12407544443",
								        "http://www.getacabdemo.com/TDS/job.json"
								);
								client.getVoiceClient().createCall(call);
								
							} catch (InvalidKeyException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (NoSuchAlgorithmException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (InvalidKeySpecException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
							
						}
					
				}
				continue;
			}
			//IF JOB ALLOCATED TO A SPECIFIC CAB And the job offer times out put the job on hold. 
			if(openRequestBO.getChckTripStatus() == TDSConstants.srJobHold){
				ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.tripOnHold+"" ,openRequestBO.getDriverid(), "", openRequestBO.getVehicleNo(),"");
				openRequestBO.setChckTripStatus(TDSConstants.tripOnHold);
				AuditDAO.insertJobLogs("**SR**On Hold. Job Offer Timed out", openRequestBO.getTripid(),assoccode,TDSConstants.tripOnHold, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
				continue;
			}
			//			if(openRequestBO.getTypeOfRide().equals("1")){
			//				continue;
			//			}
			if(openRequestBO.getDispatchMethod().equalsIgnoreCase("Q") ){
				cat.info("Job:"+openRequestBO.getTripid()+":Entering Q based dispatch");

				//TODO
				//Load list of drivers only if this is zone has queue based processing.
				//Since Condition 1 and 2 don't require processing, the driver list is loaded only now.
				if (!openRequestBO.getQueueno().equalsIgnoreCase(currentQueueInProcess) ){
					//TODO
					//Load the queue defaults for this queue
					//Load the first driver for this queue (if available)
					cat.info("Job:"+openRequestBO.getTripid()+":Loading drivers for zones with adjacent zones");
					if(openRequestBO.getAdjacentZoneOrderType()==1){
						drList = DispatchDAO.getDriverDetailWithAdjZones(openRequestBO.getQueueno(),companiesToLook,1);
					} else if (openRequestBO.getAdjacentZoneOrderType()==2){
						drList = DispatchDAO.getDriverDetailWithAdjZones(openRequestBO.getQueueno(),companiesToLook,2);
					} else if (openRequestBO.getAdjacentZoneOrderType()==3){
						drList = DispatchDAO.getDriverDetailWithAdjZones(openRequestBO.getQueueno(),companiesToLook,1);					
					}
					//markTheRestInQueueToBroadCast = false;
					//processNoMoreGenericRequestsInThisQueue = false;
					currentDriver = 0;
					currentQueueInProcess = openRequestBO.getQueueno(); 
				}

				//Condition 4 --> Special Request
				//TODO : Change this to do it inside the queue based processing
				// Removed the check for | on OR since removed it from the sql that inserted it.
				/* && !openRequestBO.getDrProfile().equals("|")){*/



				if (!openRequestBO.getDrProfile().equals("")){
					cat.info("Job:"+openRequestBO.getTripid()+":Q based dispatch: special request");

					boolean splFlag = false;
					if((openRequestBO.getSMS_TIME_DIFF() < openRequestBO.getQC_SMS_RES_TIME()) && openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses){
						//TODO
						//Add variable into application pool if its not there. This need not be done now because we can have the android device remind the driver.
						cat.info("Job:"+openRequestBO.getTripid()+":Not reached wait time for driver after job offered to him/her");
						//Making a phone call if the job is no accepted within the phonecalldelaytime.
						if(openRequestBO.getSMS_TIME_DIFF()> openRequestBO.getPhoneCallDelayTime()){

						}
						continue;
					}
					//Special request. Process. Dont need to check time since thats done earlier.
					//TODO
					//Check if a driver in the zone has these flags. IF no driver has it then, move as a public requests
					for(int k=0;k<drList.size();k++)
					{
						cabQueueBean = drList.get(k);
						if(TDSValidation.chckAllFlgExist(cabQueueBean.getDrFlag(),openRequestBO.getDrProfile()) && cabQueueBean.getDriverRating()>=openRequestBO.getJobRating()){
							cat.info("Job:"+openRequestBO.getTripid()+":Found drivers with specific flag");

							if(ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.performingDispatchProcesses+"" , cabQueueBean.getDriverid(), "", cabQueueBean.getVehicleNo(),"" )>0){
								openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);

								AuditDAO.insertJobLogs("Offering Job To Driver By Queue Based"+cabQueueBean.getDriverid()+":"+cabQueueBean.getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
								//TODO
								//DUDE-.... horrible way of doing this.;
								cat.info("Job:"+openRequestBO.getTripid()+":Sending message to driver");

								long msgID = System.currentTimeMillis();
								if(openRequestBO.getTripSource()==TDSConstants.mobile_android || openRequestBO.getTripSource()==TDSConstants.mobile_IOS){
									ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.jobAllocated+"" , cabQueueBean.getDriverid(), "", cabQueueBean.getVehicleNo(),"" );									
									String[] message = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO,"FA" , msgID,messageFields);
									Messaging oneSMS = new Messaging(config.getServletContext(),cabQueueBean, message,poolBO,assoccode);
									oneSMS.start();
								} else {
									String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID,messageFields,masterAssocCode);
									Messaging oneSMS = new Messaging(config.getServletContext(),cabQueueBean, message,poolBO,assoccode);
									oneSMS.start();
								}

								ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(cabQueueBean.getDriverid());
								if(currentQueue!=null && currentQueue.size()>0){
									ServiceRequestDAO.Update_DriverLogOutfromQueue(cabQueueBean.getDriverid(), "", currentQueue.get(0), masterAssocCode, "2", "Driver("+cabQueueBean.getDriverid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". By performing automatic dispatch process for trip("+openRequestBO.getTripid()+")");
								}
								ServiceRequestDAO.deleteDriverFromQueue(cabQueueBean.getDriverid(), masterAssocCode);
								unavailableDrivers.add(cabQueueBean.getDriverid());
								drList.remove(k);
								splFlag = true;
								//config.getServletContext().setAttribute(cabQueueBean.getDriverid()+"OR",openRequestBO);
								//MessageInMemory messageInMemory = new MessageInMemory("A", msgID, message, openRequestBO.getTripid());
								//MessageGenerateJSON.storeMessageInMemeory(config, cabQueueBean, message, "Q");
								break;
							}
						}

					}

					if (!splFlag && !openRequestBO.isDontMarkRequestsAsPublic()){
						//Start a thread to send SMS to all drivers with the same profile.
						//Mark this a broadcast request
						//If already a BC job then continue
						if(openRequestBO.getChckTripStatus()==TDSConstants.broadcastRequest){
							continue;
						}
						cat.info("Job:"+openRequestBO.getTripid()+":Special request about to be marked broadcast");

						//String message = MessageGenerateJSON.generateMessage(openRequestBO, "A", 0);
						ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "", "","","");
						openRequestBO.setChckTripStatus(TDSConstants.broadcastRequest);
						//?? TODO Mark Change
						ArrayList<DriverCabQueueBean> dr = DispatchDAO.allDriversWithSpecialFlags(companiesToLook,assoccode, openRequestBO.getDrProfile(), unavailableDrivers);
						//						Messaging msg = new Messaging(config.getServletContext(), dr, "TDSTDS;B;" + openRequestBO.getTripid(), poolBO, assoccode);
						//						msg.start();
						//?? What is this?
						if(dr.size()>0){
							long msgID = System.currentTimeMillis();
							String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "PR", msgID,messageFields, masterAssocCode);
							Messaging oneSMS = new Messaging(config.getServletContext(),dr, message,poolBO,assoccode);
							oneSMS.start();
						}

						//If Job Is Broadcasted Send To All Drivers Within The Distance 
						if(bcJobs==1){
							ArrayList<DriverCabQueueBean> allLoggedDrivers= DispatchDAO.allDriversWithInADistanceWithSpLFLAGS(companiesToLook, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()),openRequestBO.getDispatchDistance(), openRequestBO.getDrProfile(), null, openRequestBO.getJobRating(),ZoneDAO.getMaxDistance(masterAssocCode,openRequestBO.getQueueno()));
							String[] finalMessage = MessageGenerateJSON.generateMessage(openRequestBO, "BCJ", System.currentTimeMillis(), assoccode);
							Messaging messageDriver = new Messaging(config.getServletContext(), allLoggedDrivers, finalMessage, poolBO, assoccode);
							messageDriver.start();
						}

					} else if(!splFlag && openRequestBO.getChckTripStatus() != TDSConstants.performingDispatchProcessesCantFindDrivers){
						ServiceRequestDAO.updateOpenRequestStatusAndDrivers(openRequestBO.getTripid(), TDSConstants.performingDispatchProcessesCantFindDrivers+"" , "", "");

						openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcessesCantFindDrivers);
						AuditDAO.insertJobLogs("Cant find drivers in zone", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcessesCantFindDrivers, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

					}
					//Removing below continue so the we can check on stale calls at the bottom
					//continue;
				}


				//Condition 5 --> Normal Queue Based Dispatch
				if (openRequestBO.getDrProfile().equals("") && (openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted || openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses || openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcessesCantFindDrivers|| openRequestBO.getChckTripStatus() == TDSConstants.broadcastRequest)){
					cat.info("Job:"+openRequestBO.getTripid()+":Entering generic Q based dispatch");

					if (!processNoMoreGenericRequestsInThisQueue){
						//						if((openRequestBO.getSMS_TIME_DIFF() < openRequestBO.getQC_SMS_RES_TIME()) && openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses){
						//							//processNoMoreGenericRequestsInThisQueue = true;
						//							// Repeatedly sending sms is not programmed here since it can be done with a reminder at the drivers device
						//							//
						//							cat.info("Job:"+openRequestBO.getTripid()+":Not reached wait time for driver after job offered to him/her");
						//
						//							//Making a phone call if the job is no accepted within the phonecalldelaytime.
						//							if(openRequestBO.getSMS_TIME_DIFF()> openRequestBO.getPhoneCallDelayTime()){
						//
						//							}
						//
						//							continue;
						//						}
						//						Request is currently being processed. Check for next driver in queue
						boolean foundDriverWithRating = false;
						int removeDriverAtLocation = 0;
						for(int k=0;k<drList.size();k++){
							if(drList.get(k).getDriverRating()>=openRequestBO.getJobRating()){
								foundDriverWithRating = true;
								cabQueueBean = drList.get(k);
								removeDriverAtLocation = k;
								k=drList.size();
							}
						}	
						if (foundDriverWithRating){
							//cabQueueBean = drList.get(currentDriver);
							if(ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.performingDispatchProcesses + "" , cabQueueBean.getDriverid(), "", cabQueueBean.getVehicleNo(),"")>0){
								openRequestBO.setChckTripStatus(TDSConstants.performingDispatch);

								AuditDAO.insertJobLogs("Offering Job To Driver By Queue Based"+cabQueueBean.getDriverid()+":"+cabQueueBean.getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

								//TODO
								//DUDE-.... horrible way of doing this.;

								long msgID = System.currentTimeMillis();
								if(openRequestBO.getTripSource()==TDSConstants.mobile_android || openRequestBO.getTripSource()==TDSConstants.mobile_IOS){
									//System.out.println("Allocating Trip--->"+openRequestBO.getTripid()+" to driver--->"+cabQueueBean.getDriverid()+" by Queue Based");
									String[] message = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO,"FA" , msgID,messageFields);
									Messaging oneSMS = new Messaging(config.getServletContext(),cabQueueBean, message,poolBO,assoccode);
									oneSMS.start();
								} else {
									String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID,messageFields,masterAssocCode);
									Messaging oneSMS = new Messaging(config.getServletContext(),cabQueueBean, message,poolBO,assoccode);
									oneSMS.start();

								}
//								String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID,messageFields);
//								Messaging oneSMS = new Messaging(config.getServletContext(),cabQueueBean, message,poolBO,assoccode);
//								oneSMS.start();
								unavailableDrivers.add(cabQueueBean.getDriverid());

								ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(cabQueueBean.getDriverid());
								if(currentQueue!=null && currentQueue.size()>0){
									ServiceRequestDAO.Update_DriverLogOutfromQueue(cabQueueBean.getDriverid(), "", currentQueue.get(0), masterAssocCode, "2", "Driver("+cabQueueBean.getDriverid()+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". By performing automatic queue based dispatch for trip("+openRequestBO.getTripid()+")");
								}
								ServiceRequestDAO.deleteDriverFromQueue(cabQueueBean.getDriverid(), masterAssocCode); 
								drList.remove(removeDriverAtLocation);
							}
						}
						else if(openRequestBO.getChckTripStatus() != TDSConstants.broadcastRequest){
							if(!openRequestBO.isDontMarkRequestsAsPublic()  && openRequestBO.getTimeToStartTripInSeconds() < openRequestBO.getBroadCastDelay()){
								ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "", "", "","");
								openRequestBO.setChckTripStatus(TDSConstants.broadcastRequest);

								AuditDAO.insertJobLogs("Broadcasting Job", openRequestBO.getTripid(),assoccode,TDSConstants.broadcastRequest, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

								//If Job Is Broadcasted Send To All Drivers Within The Distance 
								if(bcJobs==1){
									ArrayList<DriverCabQueueBean> allLoggedDrivers= DispatchDAO.allDriversWithInADistanceWithSpLFLAGS(companiesToLook, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()),openRequestBO.getDispatchDistance(), openRequestBO.getDrProfile(), null, openRequestBO.getJobRating(),ZoneDAO.getMaxDistance(masterAssocCode,openRequestBO.getQueueno()));
									String[] finalMessage = MessageGenerateJSON.generateMessage(openRequestBO, "BCJ", System.currentTimeMillis(), assoccode);
									Messaging messageDriver = new Messaging(config.getServletContext(), allLoggedDrivers, finalMessage, poolBO, assoccode);
									messageDriver.start();
								}
							} else if(openRequestBO.getChckTripStatus() != TDSConstants.performingDispatchProcessesCantFindDrivers){
								ServiceRequestDAO.updateOpenRequestStatusAndDrivers(openRequestBO.getTripid(), TDSConstants.performingDispatchProcessesCantFindDrivers+"" , "", "");
								AuditDAO.insertJobLogs("Cant find drivers in zone", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcessesCantFindDrivers, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
							}
							//LineNo:A
						}

						//continue;
					} else {
						continue;
					}

				}
				if(openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcessesCantFindDrivers || openRequestBO.getChckTripStatus() == TDSConstants.broadcastRequest){
					if(openRequestBO.getAdjacentZoneOrderType()==3){
						openRequestBO.setDispatchMethod("SD");
					}
				}

			}
			//Condition 6 --> Shortest Distance based dispatch
			if (openRequestBO.getDispatchMethod().equalsIgnoreCase("S")){
				//Special Requests on Shortest Distance
				if (!openRequestBO.getDrProfile().equals("")){
					cat.info("Job:"+openRequestBO.getTripid()+":S based dispatch: special request");

					boolean splFlag = false;
					if((openRequestBO.getSMS_TIME_DIFF() < openRequestBO.getQC_SMS_RES_TIME()) && openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses){
						//processNoMoreGenericRequestsInThisQueue = true;
						cat.info("Job:"+openRequestBO.getTripid()+":Not reached wait time for driver after job offered to him/her");

						//Making a phone call if the job is no accepted within the phonecalldelaytime.
						if(openRequestBO.getSMS_TIME_DIFF()> openRequestBO.getPhoneCallDelayTime()){

						}

						continue;
					}
					String[] historyOfDriver = openRequestBO.getHistoryOfDrivers().split(";");
					ArrayList<String> driverHistArray = new ArrayList<String>();

					for(int loop =0;loop<historyOfDriver.length;loop++){
						driverHistArray.add(historyOfDriver[loop]);
					}

					ArrayList<DriverCabQueueBean> dr = DispatchDAO.allDriversWithInADistanceWithSpLFLAGS(companiesToLook, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()), openRequestBO.getDispatchDistance() , openRequestBO.getDrProfile(), driverHistArray, openRequestBO.getJobRating(),ZoneDAO.getMaxDistance(masterAssocCode,openRequestBO.getQueueno()));
					if (dr.size()>0){

						//?? Even though it marks it as being in dispatch, doesnt update driver ids
						ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.performingDispatchProcesses+"" , dr.get(0).getDriverid(), "",dr.get(0).getVehicleNo(),"");
						openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);

						AuditDAO.insertJobLogs("Offering Job To Driver By Shortest Dist"+dr.get(0).getDriverid()+":"+dr.get(0).getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

						long msgID = System.currentTimeMillis();

						String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "S", System.currentTimeMillis(),messageFields, masterAssocCode);
						Messaging msg = new Messaging(config.getServletContext(),dr, message, poolBO,assoccode);
						msg.start();
						unavailableDrivers.add(dr.get(0).getDriverid());
						//MessageInMemory messageInMemory = new MessageInMemory("A", msgID, message, openRequestBO.getTripid());
						//MessageGenerateJSON.storeMessageInMemeory(config, dr, messageInMemory, "Q");
						//continue;
					} else if(openRequestBO.getChckTripStatus() != TDSConstants.broadcastRequest){
						if(!openRequestBO.isDontMarkRequestsAsPublic() && openRequestBO.getTimeToStartTripInSeconds() < openRequestBO.getBroadCastDelay()){
							ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "", openRequestBO.getHistoryOfDrivers(), "",openRequestBO.getHistoryOfDrivers());
							openRequestBO.setChckTripStatus(TDSConstants.broadcastRequest);

							AuditDAO.insertJobLogs("Broadcasting Job", openRequestBO.getTripid(),assoccode,TDSConstants.broadcastRequest, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

						}else if(openRequestBO.getChckTripStatus()!=TDSConstants.performingDispatchProcessesCantFindDrivers){								
							ServiceRequestDAO.updateOpenRequestStatusAndDrivers(openRequestBO.getTripid(), TDSConstants.performingDispatchProcessesCantFindDrivers+"" , "", "");
							openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcessesCantFindDrivers);

							//ServiceRequestDAO.insertJobLogs("Cant find driver", m_openRequestBO,adminBO,TDSConstants.performingDispatchProcessesCantFindDrivers);
							AuditDAO.insertJobLogs("Cant find driver", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcessesCantFindDrivers, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
						}
					}
					continue;

				}
				cat.info("Job:"+openRequestBO.getTripid()+":Entering generic S based dispatch");

				if (openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted || openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses || openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcessesCantFindDrivers || openRequestBO.getChckTripStatus() == TDSConstants.broadcastRequest){
					// Shortest distance queue 
					// Pick Lat long and look for x number of drivers that are close by and send them all a message.
					if ((openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted || openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses)){
						if (!processNoMoreGenericRequestsInThisQueue){
							if((openRequestBO.getSMS_TIME_DIFF() < openRequestBO.getQC_SMS_RES_TIME()) && openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses){
								//processNoMoreGenericRequestsInThisQueue = true;
								cat.info("Job:"+openRequestBO.getTripid()+":Not reached wait time for driver after job offered to him/her");
								//Making a phone call if the job is no accepted within the phonecalldelaytime.
								if(openRequestBO.getSMS_TIME_DIFF()> openRequestBO.getPhoneCallDelayTime()){

								}

								continue;
							}
							if(!openRequestBO.isDontMarkRequestsAsPublic() && openRequestBO.getTimeToStartTripInSeconds() < openRequestBO.getBroadCastDelay()){
								ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "", "","","");
								openRequestBO.setChckTripStatus(TDSConstants.broadcastRequest);

								continue;
							}
							//double distance = openRequestBO.getDispatchDistance() * (openRequestBO.getDispatchIterationNumber()+1);
							String[] historyOfDriver = null;
							ArrayList<String> arrayHistoryOfDriver = new ArrayList<String>();

							if(openRequestBO.getHistoryOfDrivers()==null){
								openRequestBO.setHistoryOfDrivers("");
							}
							if(!openRequestBO.getHistoryOfDrivers().equals("")){
								historyOfDriver = openRequestBO.getHistoryOfDrivers().split(";");
								historyOfDriver = openRequestBO.getHistoryOfDrivers().split(";");
								for(String s:historyOfDriver){
									arrayHistoryOfDriver.add(s);
								}
							}
							arrayHistoryOfDriver.addAll(unavailableDrivers);
							ArrayList<DriverCabQueueBean> dr = DispatchDAO.allDriversWithInADistance(companiesToLook, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()), openRequestBO.getDispatchDistance(), arrayHistoryOfDriver, openRequestBO.getJobRating(),ZoneDAO.getMaxDistance(masterAssocCode,openRequestBO.getQueueno()));					
							if (dr.size()>0){
								long msgID = System.currentTimeMillis();
								String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "S", msgID,messageFields,masterAssocCode);
								Messaging msg = new Messaging(config.getServletContext(),dr, message, poolBO,assoccode);
								msg.start();
								String listOfdrivers = "";
								String listOfVehicles = "";

								for(int j =0;j<dr.size();j++){
									listOfdrivers = listOfdrivers + dr.get(j).getDriverid() + ";";
									listOfVehicles = listOfVehicles + dr.get(j).getVehicleNo() + ";";

								}
								ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.performingDispatchProcesses + "" , listOfdrivers, openRequestBO.getHistoryOfDrivers()+listOfdrivers, listOfVehicles, "");
								openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);

								//MessageInMemory messageInMemory = new MessageInMemory("A", msgID, message, openRequestBO.getTripid());
								//MessageGenerateJSON.storeMessageInMemeory(config, dr, messageInMemory, "Q");

							} else if(openRequestBO.getChckTripStatus() != TDSConstants.broadcastRequest){
								if(!openRequestBO.isDontMarkRequestsAsPublic() && openRequestBO.getTimeToStartTripInSeconds() < openRequestBO.getBroadCastDelay()){
									ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "", openRequestBO.getHistoryOfDrivers(), "",openRequestBO.getHistoryOfDrivers());
									openRequestBO.setChckTripStatus(TDSConstants.broadcastRequest);

									AuditDAO.insertJobLogs("Broadcasting Job", openRequestBO.getTripid(),assoccode,TDSConstants.broadcastRequest, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
								}else if(openRequestBO.getChckTripStatus()!=TDSConstants.performingDispatchProcessesCantFindDrivers){
									ServiceRequestDAO.updateOpenRequestStatusAndDrivers(openRequestBO.getTripid(), TDSConstants.performingDispatchProcessesCantFindDrivers+"" , "", "");
									openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcessesCantFindDrivers);
									AuditDAO.insertJobLogs("Cant find driver", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcessesCantFindDrivers, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

								}
							}

						}
					}
					continue;
				}
			}
			//Condition 7 --> Shortest Distance based dispatch
			if (openRequestBO.getDispatchMethod().equalsIgnoreCase("SD")){
				String statusToUpdate = TDSConstants.performingDispatchProcesses + "";
				//Special Requests on Shortest Distance
				if (!openRequestBO.getDrProfile().equals("")){
					cat.info("Job:"+openRequestBO.getTripid()+":SDO based dispatch: special request");

					boolean splFlag = false;
					if((openRequestBO.getSMS_TIME_DIFF() < openRequestBO.getQC_SMS_RES_TIME()) && openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses && !openRequestBO.getDriverid().equals("")){
						cat.info("Job:"+openRequestBO.getTripid()+":Not reached wait time for driver after job offered to him/her");
						//Making a phone call if the job is no accepted within the phonecalldelaytime.
						if(openRequestBO.getSMS_TIME_DIFF()> openRequestBO.getPhoneCallDelayTime()){

						}

						continue;
					}
					String[] historyOfDriver = null;
					ArrayList<String> arrayHistoryOfDriver = new ArrayList<String>();
					if(openRequestBO.getHistoryOfDrivers()==null){
						openRequestBO.setHistoryOfDrivers("");
					}
					if(!openRequestBO.getHistoryOfDrivers().equals("")){
						historyOfDriver = openRequestBO.getHistoryOfDrivers().split(";");
						for(String s:historyOfDriver){
							arrayHistoryOfDriver.add(s);
						}
					}
					arrayHistoryOfDriver.addAll(unavailableDrivers);

					ArrayList<DriverCabQueueBean> dr = DispatchDAO.allDriversWithInADistanceWithSpLFLAGS(companiesToLook, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()), openRequestBO.getDispatchDistance(),openRequestBO.getDrProfile(),arrayHistoryOfDriver, openRequestBO.getJobRating(),ZoneDAO.getMaxDistance(masterAssocCode,openRequestBO.getQueueno()));					
					//?? Performing dispatch but not allocating driver?
					if (dr.size()>0){
						DriverCabQueueBean closestDriver = null;
						if(dr.size()>1){
							System.out.println("Finding Closest Driver For Trip--->"+openRequestBO.getTripid());
							closestDriver = ClosestDriver.findClosestDriver(dr, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()),ZoneDAO.getMaxDistance(masterAssocCode,openRequestBO.getQueueno()),openRequestBO.getTripid(),assoccode,masterAssocCode);
						} else{
							closestDriver = dr.get(0);
						}
						//Make sure closest dirver doesnt have a job
						//Check for driver not having any other jobs..
						//For MArck Check???
						if(!DispatchDAO.checkDriverAvailability(assoccode, closestDriver.getDriverid(), driverAlarm)){
							unavailableDrivers.add(closestDriver.getDriverid());
							//Got to top and startover for this job 
							//System.out.println("Driver not available. Reallocating job");
							//On Master AssocCode with capacbility to look in other fleets
							// CHeck if this driver doesnt have a job that has just been dispatch to him/her in another fleet
							//this will avoid driver who just got a job (and hasnt accepted it yet)
							
							i = i -1;
							continue;
						}
						long msgID = System.currentTimeMillis();
						if(openRequestBO.getTripSource()==TDSConstants.mobile_android || openRequestBO.getTripSource()==TDSConstants.mobile_IOS){
							//System.out.println("Allocating Trip--->"+openRequestBO.getTripid()+" to driver--->"+cabQueueBean.getDriverid()+" by Shortest Dist");
							statusToUpdate = TDSConstants.jobAllocated+"";
							String[] message = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO,"FA" , msgID,messageFields);
							Messaging oneSMS = new Messaging(config.getServletContext(),closestDriver, message,poolBO,assoccode);
							oneSMS.start();
						} else {
							String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID,messageFields,masterAssocCode);
							Messaging oneSMS = new Messaging(config.getServletContext(),closestDriver, message,poolBO,assoccode);
							oneSMS.start();
						}
//						String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID,messageFields);
//						Messaging msg = new Messaging(config.getServletContext(),closestDriver, message, poolBO,masterAssocCode);
//						msg.start();
						String historyOfdrivers = openRequestBO.getHistoryOfDrivers() + closestDriver.getDriverid()+ ";" ;
						ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), statusToUpdate , closestDriver.getDriverid(), historyOfdrivers,closestDriver.getVehicleNo(),"");
						openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);
						AuditDAO.insertJobLogs("Offering Job To Driver By Shortest Dist"+closestDriver.getDriverid()+":"+closestDriver.getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
						unavailableDrivers.add(closestDriver.getDriverid());
					}  else if(openRequestBO.getChckTripStatus() != TDSConstants.broadcastRequest){
						if(!openRequestBO.isDontMarkRequestsAsPublic() && openRequestBO.getTimeToStartTripInSeconds() < openRequestBO.getBroadCastDelay()){
							ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "", openRequestBO.getHistoryOfDrivers(), "",openRequestBO.getHistoryOfDrivers());
							openRequestBO.setChckTripStatus(TDSConstants.broadcastRequest);
						}else if(openRequestBO.getChckTripStatus()!=TDSConstants.performingDispatchProcessesCantFindDrivers){
							ServiceRequestDAO.updateOpenRequestStatusAndDrivers(openRequestBO.getTripid(), TDSConstants.performingDispatchProcessesCantFindDrivers+"" , "", "");
							openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcessesCantFindDrivers);
							AuditDAO.insertJobLogs("Cant find driver", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcessesCantFindDrivers, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

						}
					}
					//Removing below continue so the we can check on stale calls at the bottom
					//continue;
				}
				cat.info("Job:"+openRequestBO.getTripid()+":Entering generic SD based dispatch");

				if (openRequestBO.getDrProfile().equals("") && (openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted || openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses ||openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcessesCantFindDrivers|| openRequestBO.getChckTripStatus() == TDSConstants.broadcastRequest)){
					// Shortest distance queue
					// Pick Lat long and look for x number of drivers that are close by and send them all a message.
					//if ((openRequestBO.getChckTripStatus() == TDSConstants.newRequestDispatchProcessesNotStarted || openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses)){
					if((openRequestBO.getSMS_TIME_DIFF() < openRequestBO.getQC_SMS_RES_TIME()) && openRequestBO.getChckTripStatus() == TDSConstants.performingDispatchProcesses  && !openRequestBO.getDriverid().equals("")){
						cat.info("Job:"+openRequestBO.getTripid()+":Not reached wait time for driver after job offered to him/her");
						//Making a phone call if the job is no accepted within the phonecalldelaytime.
						if(openRequestBO.getSMS_TIME_DIFF()> openRequestBO.getPhoneCallDelayTime()){

						}

						continue;
					}
					//if(openRequestBO.getTimeToStartTripInSeconds() < openRequestBO.getBroadCastDelay()){
					//	ServiceRequestDAO.updateOpenRequestStatus(openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "");
					//	continue;
					//}
					//int distance = openRequestBO.getDispatchDistance() * (openRequestBO.getDispatchIterationNumber()+1);
					String[] historyOfDriver = null;
					ArrayList<String> arrayHistoryOfDriver = new ArrayList<String>();
					if(openRequestBO.getHistoryOfDrivers()==null){
						openRequestBO.setHistoryOfDrivers("");
					}
					if(!openRequestBO.getHistoryOfDrivers().equals("")){
						historyOfDriver = openRequestBO.getHistoryOfDrivers().split(";");
						for(String s:historyOfDriver){
							arrayHistoryOfDriver.add(s);
						}

					}
					arrayHistoryOfDriver.addAll(unavailableDrivers);
					cat.info("Job:"+openRequestBO.getTripid()+":1234:About to get all drivers");

					ArrayList<DriverCabQueueBean> dr = DispatchDAO.allDriversWithInADistance(companiesToLook, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()), openRequestBO.getDispatchDistance(), arrayHistoryOfDriver, openRequestBO.getJobRating(),ZoneDAO.getMaxDistance(masterAssocCode,openRequestBO.getQueueno()));					
					if (dr.size()>0){
						DriverCabQueueBean closestDriver = null;
						if(dr.size()>1){
							System.out.println("Finding Closest Driver For Trip--->"+openRequestBO.getTripid());
							closestDriver = ClosestDriver.findClosestDriver(dr, Double.parseDouble(openRequestBO.getSlat()), Double.parseDouble(openRequestBO.getSlong()),ZoneDAO.getMaxDistance(masterAssocCode,openRequestBO.getQueueno()),openRequestBO.getTripid(),assoccode,masterAssocCode);
						} else{
							closestDriver = dr.get(0);
						}
						//Make sure closest dirver doesnt have a job
						if(!DispatchDAO.checkDriverAvailability(assoccode, closestDriver.getDriverid(), driverAlarm)){
							//System.out.println("Driver not available. Reallocating job");
							unavailableDrivers.add(closestDriver.getDriverid());
							//Got to top and startover for this job avain
							i = i -1;
							continue;
						}
						long msgID = System.currentTimeMillis();
						if(openRequestBO.getTripSource()==TDSConstants.mobile_android || openRequestBO.getTripSource()==TDSConstants.mobile_IOS){
							//System.out.println("Allocating Trip--->"+openRequestBO.getTripid()+" to driver--->"+cabQueueBean.getDriverid()+" by Shortest Dist");
							statusToUpdate = TDSConstants.jobAllocated+"";
							String[] message = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO,"FA" , msgID,messageFields);
							Messaging oneSMS = new Messaging(config.getServletContext(),closestDriver, message,poolBO,assoccode);
							oneSMS.start();
						} else {
							String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID,messageFields,masterAssocCode);
							Messaging oneSMS = new Messaging(config.getServletContext(),closestDriver, message,poolBO,assoccode);
							oneSMS.start();
						}
//						String[] message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID,messageFields);
//						Messaging msg = new Messaging(config.getServletContext(),closestDriver, message, poolBO,assoccode);
//						msg.start();
						String historyOfdrivers = openRequestBO.getHistoryOfDrivers() + closestDriver.getDriverid()+ ";" ;
						ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), statusToUpdate , closestDriver.getDriverid(), historyOfdrivers,closestDriver.getVehicleNo(),"");
						openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);
						AuditDAO.insertJobLogs("Offering Job To Driver By Shortest Distance"+closestDriver.getDriverid()+":"+closestDriver.getVehicleNo(), openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcesses, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
						unavailableDrivers.add(closestDriver.getDriverid());
						//MessageInMemory messageInMemory = new MessageInMemory("A", msgID, message, openRequestBO.getTripid());
						//MessageGenerateJSON.storeMessageInMemeory(config, closestDriver, message, "Q");
					} else if(openRequestBO.getChckTripStatus() != TDSConstants.broadcastRequest){
						if(!openRequestBO.isDontMarkRequestsAsPublic() && openRequestBO.getTimeToStartTripInSeconds() < openRequestBO.getBroadCastDelay()){
							cat.info("Job:"+openRequestBO.getTripid()+":12344:About to update job to broadcast");
							String pubSwi = openRequestBO.isDontMarkRequestsAsPublic()?"dontmarkpublic":"markpublic";
							cat.info("Job:"+openRequestBO.getTripid()+":12344:DontMarkPublicSwitch:"+pubSwi);
							ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "", openRequestBO.getHistoryOfDrivers(), "",openRequestBO.getHistoryOfDrivers());
							openRequestBO.setChckTripStatus(TDSConstants.broadcastRequest);

						} else if(openRequestBO.getChckTripStatus()!=TDSConstants.performingDispatchProcessesCantFindDrivers){
							ServiceRequestDAO.updateOpenRequestStatusAndDrivers(openRequestBO.getTripid(), TDSConstants.performingDispatchProcessesCantFindDrivers+"" , "", "");
							openRequestBO.setChckTripStatus(TDSConstants.performingDispatchProcessesCantFindDrivers);
							AuditDAO.insertJobLogs("Cant find driver", openRequestBO.getTripid(),assoccode,TDSConstants.performingDispatchProcessesCantFindDrivers, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);

						}
						//}

					}
					//Removing below continue so the we can check on stale calls at the bottom
					//continue;
				}
			}
			//Condition 8 --> BroadCast All Requests
			if (openRequestBO.getDispatchMethod().equalsIgnoreCase("B")){
				if(!(openRequestBO.getChckTripStatus() == TDSConstants.broadcastRequest)){ 	
					ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.broadcastRequest+"" , "", "","","");
					openRequestBO.setChckTripStatus(TDSConstants.broadcastRequest);

				}
			}
			if (openRequestBO.getDispatchMethod().equalsIgnoreCase("M")){
				//ServiceRequestDAO.updateOpenRequestStatus(openRequestBO.getTripid(), TDSConstants.tripOnHold+"" , "", "","","");
			}

			//About to check for stale calls
			if(openRequestBO.getStaleCallTimeForZone()>0 && (openRequestBO.getStaleCallTimeForZone()*60)<openRequestBO.getStaleCallTime() && (openRequestBO.getChckTripStatus()==TDSConstants.performingDispatchProcessesCantFindDrivers|| openRequestBO.getChckTripStatus()==TDSConstants.broadcastRequest)){
				ServiceRequestDAO.updateOpenRequestStatus(assoccode,openRequestBO.getTripid(), TDSConstants.staleCall + "" , "", "","","");
				openRequestBO.setChckTripStatus(TDSConstants.staleCall);
				AuditDAO.insertJobLogs("StaleTrip.Call made to Passenger", openRequestBO.getTripid(),assoccode,TDSConstants.staleCall, jobName,openRequestBO.getSlat(),openRequestBO.getSlong(),masterAssocCode);
				//System.out.println("IVR");
				try {
					String phone=phonePrefix+openRequestBO.getPhone();
					IVRPassengerCallerJob ivr= new IVRPassengerCallerJob(openRequestBO.getTripid(),phone,masterAssocCode);
					ivr.start();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			currentQueueInProcess = openRequestBO.getQueueno();

		}
		thisJobIsRunning = false;
		maintenanceJobLastRunTime = System.currentTimeMillis();

		if(!dispatchingJobs){
			lastRunTime = -1;
		} else {
			System.out.println(opreqList.size()+" Job read for assoc code " + assoccode + " in "+ (System.currentTimeMillis() - start)+ " ms.");
			lastRunTime = System.currentTimeMillis();
		}

		//String runtime=String.valueOf(lastRunTime);
		//config.getServletContext().setAttribute("lastRunTime",runtime);
		//System.out.println("String lastrimtime for assoccode:"+assoccode+":"+lastRunTime);

	}

	public boolean isDispatchingJobs() {
		return dispatchingJobs;
	}
	public boolean isNotDispatchingJobs() {
		return !dispatchingJobs;
	}



	public void setDispatchingJobs(boolean dispatchJobs) {
		this.dispatchingJobs = dispatchJobs;
	}


	public void specialRequestOnQueueDispatch(){

	}
	public long getLastRunTime(){
		return lastRunTime;
	}

	public long getMaintenanceJobLastRunTime() {
		return maintenanceJobLastRunTime;
	}


	public void setMaintenanceJobLastRunTime(long maintenanceJobLastRunTime) {
		this.maintenanceJobLastRunTime = maintenanceJobLastRunTime;
	}


}
