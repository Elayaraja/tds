package com.tds.process;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.servlet.ServletContext;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;

import com.common.util.TDSProperties;

public class GooglePush {

	public static String register(){
		HttpClient client = new HttpClient();
		BufferedReader br = null;
		String googleRegKey = "";

		PostMethod method = new PostMethod("https://www.google.com/accounts/ClientLogin");
		//System.out.println("google userid used "+ TDSProperties.getValue("c2dmuserid"));
		method.addParameter("Email", TDSProperties.getValue("c2dmuserid"));
		method.addParameter("Passwd", TDSProperties.getValue("c2dmpassword"));
		method.addParameter("accountType", "HOSTED_OR_GOOGLE");
		method.addParameter("service", "ac2dm");		
		
	    try{
	        int returnCode = client.executeMethod(method);

	        if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
	          System.err.println("The Post method is not implemented by this URI");
	          // still consume the response body
	          method.getResponseBodyAsString();
	        } else {
	          br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
	          String readLine;
	          while(((readLine = br.readLine()) != null)) {
	            System.err.println(readLine);
	            if(readLine.split("=")[0].equals("Auth")){
	            	googleRegKey = readLine.split("=")[1];
	            }
	          }

	        }
	      } catch (Exception e) {
	        System.err.println(e);
	      } finally {
	        method.releaseConnection();
	        if(br != null) try { br.close(); } catch (Exception fe) {}
	      }
	      //sendMessage(googleRegKey, DKey, "0", "HareKrishna");
	     return googleRegKey;
}

	
	
	public static void sendMessage(ServletContext context,String messageKey, String message, ArrayList<String> driverList){
		HttpClient client = new HttpClient();
		BufferedReader br = null;
		//driverRegKey = "APA91bEHrlOrbLEOMdt_kk7WjStPRjNlmQa6YkyoIuFTZBR0zxFlKE2_3lHTa65Gx1af8L5UaRLAwH3Fny_MIey84GqCdF664XXrT_TsjkCLAWlpvyUbIgsko6UODQxjGwvResH8o02x";
		String googleRegKey = (String)context.getAttribute("GoogKey");
		PostMethod method = null;
		method = new PostMethod("https://android.apis.google.com/c2dm/send");
		method.addRequestHeader(new Header("Authorization",
				"GoogleLogin auth="+googleRegKey));
		//System.out.println("GoogleRegKye"+googleRegKey);
		method.addParameter("collapse_key", messageKey);
		method.addParameter("data.payload", message);
		for(int i=0;i<driverList.size();i++){
			method.removeParameter("registration_id");
			method.addParameter("registration_id", driverList.get(i));		
			try{
				int returnCode = client.executeMethod(method);
				if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
					System.err.println("The Post method is not implemented by this URI");
					// still consume the response body
					method.getResponseBodyAsString();
				} else {
					if(method.getRequestHeader("Update-Client-Auth")!=null){
						String updateClientAuth = method.getRequestHeader("Update-Client-Auth").getValue();
						context.setAttribute("GoogKey", updateClientAuth);
						method.addRequestHeader(new Header("Authorization",
								"GoogleLogin auth="+updateClientAuth));
			//			System.out.println("Got new Goog key"+ updateClientAuth);

					}
				}



			} catch (Exception e) {
				System.err.println(e);
			} finally {
				method.releaseConnection();
				method = new PostMethod("https://android.apis.google.com/c2dm/send");
				method.addRequestHeader(new Header("Authorization",
						"GoogleLogin auth="+googleRegKey));
				//System.out.println("GoogleRegKye"+googleRegKey);
				method.addParameter("collapse_key", messageKey);
				method.addParameter("data.payload", message);
			}

		}
	}

}
