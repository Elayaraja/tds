package com.tds.tdsvalidation;

import com.tds.tdsBO.DriverRegistrationBO;
import com.tds.tdsBO.passengerBO;
import com.common.util.TDSValidation;

public class RegistrationValidation {
	

	
	
public static String validationPassenger(passengerBO passBO) {
		
//		System.out.println("ENTER INTO PASSENGER VALIDATION");
		StringBuffer errors = new StringBuffer();
		if(passBO.getFname().equals("")) {
			errors.append("Enter Fname<br>");
 		}
		if(passBO.getLname().equals("")) {
			errors.append("Enter Lname<br>");
		}
		if(passBO.getAdd1().equals("")) {
			errors.append("Enter Address1<br>");
		} 
		if(passBO.getPhone().equals("")) {
			errors.append("Enter Phone no<br>");
		}
		if(passBO.getCity().equals("")) {
			errors.append("Enter City<br>");
		}
		if(passBO.getState().equals("")) {
			errors.append("Enter State<br>");
		}
		if(passBO.getZip().equals("")) {
			errors.append("Enter Zip<br>");
		} else if(!TDSValidation.isOnlyNumbers(passBO.getZip())) {
			errors.append("Zip Code Shoud be a number<br>");
		}
		if(passBO.getPayment_type() == '0') {
			errors.append("Select Payment Type<br>");
		}
		if(passBO.getPayment_type() == 'V') {
			if(passBO.getPayment_ac().equals("")) {
				errors.append("Enter Payment A/c<br> ");
			}
		}
		if(passBO.getUid().equals("")) {
			errors.append("Enter User Name<br>");
		}
		if(passBO.getPassword().equals("")) {
			errors.append("Enter PassWord<br> ");
		}
		if(!passBO.getPassword().equals(passBO.getRepassword())) {
			errors.append("Password Mismatch Re Enter The Password<br>");
		} 
		return errors.toString();
		
	}
	
	public static String driverValidation (DriverRegistrationBO p_driverBO) {
		StringBuffer errors = new StringBuffer();
		if(p_driverBO.getUid().equalsIgnoreCase("") || p_driverBO.getUid() == "" || p_driverBO.getUid().length() == 0) {
			errors.append("You must enter UserName<br>");
		}
		if(p_driverBO.getPassword().equalsIgnoreCase("") || p_driverBO.getPassword() == "" || p_driverBO.getPassword().length() == 0) {
			errors.append("You must enter Password<br>");
		}
		if(p_driverBO.getFname().equalsIgnoreCase("") || p_driverBO.getFname() =="" || p_driverBO.getFname().length() == 0) {
			errors.append("You must enter First Name<br>");
		}
		if(p_driverBO.getLname().equalsIgnoreCase("") || p_driverBO.getLname() == "" || p_driverBO.getLname().length() == 0) {
			errors.append("You must enter Last Name<br>");
		}
		if(p_driverBO.getAdd1().equalsIgnoreCase("") || p_driverBO.getAdd1() == "" || p_driverBO.getAdd1().length() == 0) {
			errors.append("You must enter Address1<br>");
		}
		if(p_driverBO.getCity().equalsIgnoreCase("") || p_driverBO.getCity() == "" || p_driverBO.getCity().length() == 0) {
			errors.append("You must enter City<br>");
		}
		if(p_driverBO.getState().equalsIgnoreCase("") || p_driverBO.getState() == "" || p_driverBO.getState().length() == 0){
			errors.append("You must enter State<br>");
		}
		if(p_driverBO.getZip().equalsIgnoreCase("") || p_driverBO.getZip() == "" || p_driverBO.getZip().length() == 0){
			errors.append("You must enter Zip code<br>");
		}
		/*if(p_driverBO.getRoute().equalsIgnoreCase("") || p_driverBO.getRoute() == "" || p_driverBO.getRoute().length() == 0) {
			errors.append("You must enter Account Route<br>");
		}
		if(p_driverBO.getAcct().equalsIgnoreCase("") || p_driverBO.getAcct() == "" || p_driverBO.getAcct().length() == 0) {
			errors.append("You must enter Account No<br>");
		}*/
		if(p_driverBO.getPhone().equalsIgnoreCase("") || p_driverBO.getPhone() == "" || p_driverBO.getPhone().length() == 0) {
			errors.append("You must enter Phone Number<br>");
		}
		/*if(p_driverBO.getPhoneExt().equalsIgnoreCase("") || p_driverBO.getPhoneExt() == "" || p_driverBO.getPhoneExt().length() == 0) {
			errors.append("You must enter Phone Extention<br>");
		}
		if(p_driverBO.getCmake().equalsIgnoreCase("") || p_driverBO.getCmake() == "" || p_driverBO.getCmake().length() == 0) {
			errors.append("You must enter Car Make<br>");
		}
		if(p_driverBO.getCmodel().equalsIgnoreCase("") || p_driverBO.getCmodel() == "" || p_driverBO.getCmodel().length() == 0) {
			errors.append("You must enter Car Model<br>");
		}
		if(p_driverBO.getCyear().equalsIgnoreCase("") || p_driverBO.getCyear() == "" || p_driverBO.getCyear().length() == 0) {
			errors.append("You must enter Car Year<br>");
		}*/
		
//		if(!TDSValidation.isValidZipCode(p_driverBO.getZip()) && p_driverBO.getZip().length() > 0) {
//			errors.append("Invalid zip code<br>");
//		}
		if(!TDSValidation.isRightPhoneNumber(p_driverBO.getPhone()) && p_driverBO.getPhone().length() >0){
			errors.append("Phone No should contain only numbers<br>");
		}
		if(!TDSValidation.isOnlyAlphabetsAndNumbersWithAllowedSpecialChars(p_driverBO.getUid()) && p_driverBO.getUid().length() >0) {
			errors.append("User Name should not contains alpha-numberic and some special characters<br>");
		}
		
		/*if(!TDSValidation.isOnlyAlphabetsAndNumbersWithAllowedSpecialChars(p_driverBO.getPassword()) && p_driverBO.getPassword().length() >0) {
			errors.append("Password should contains alpha-numberic and some special characters<br>");
		}
		if(!TDSValidation.isOnlyAlphabetsAndNumbersWithAllowedSpecialChars(p_driverBO.getRepassword()) && p_driverBO.getRepassword().length() >0) {
			errors.append("Re-password should contains alpha-numberic and some special characters<br>");
		}*/
		
		/*if(!(p_driverBO.getPassword().length() >0)){
			errors.append("Password should contains alpha-numberic and some special characters<br>");
		}
		if(p_driverBO.getRepassword().length() >0) {
			errors.append("Re-password should contains alpha-numberic and some special characters<br>");
		}*/
		
		if(p_driverBO.getPassword().length() > 0 && p_driverBO.getRepassword().length() > 0 && !p_driverBO.getPassword().equals(p_driverBO.getRepassword())) {
			errors.append("Mismatch Password and Re-password<br>");
		}
		
		/*
		if(p_driverBO.getCabno().equalsIgnoreCase("") || p_driverBO.getCabno() == "" || p_driverBO.getCabno().length() == 0) {
			errors.append("Cab Number Not Valid<br>");
		}
		if(p_driverBO.getPassno().equalsIgnoreCase("") || p_driverBO.getCabno() == "" || p_driverBO.getCabno().length() == 0) {
			errors.append("Pass Number Not Valid<br>");
		}*/
		
		
		if(p_driverBO.getRegistration() == 'D') {
		/*
		if(p_driverBO.getPayPassword().equalsIgnoreCase("") || p_driverBO.getPayPassword() == "" || p_driverBO.getPayPassword().length() == 0) {
			errors.append("You must enter Payment Password<br>");
		}
		if(p_driverBO.getPayPassword().length() > 0 && p_driverBO.getPayPassword().length() > 0 && !p_driverBO.getPayPassword().equals(p_driverBO.getPayRePassword())) {
			errors.append("Payment Password and PaymentRe-password mismatch");
		}
		if(p_driverBO.getPayUserId().equalsIgnoreCase("") || p_driverBO.getPayUserId() == "" || p_driverBO.getPayUserId().length() == 0) {
			errors.append("You Must Enter Payment UserId<br>");
		}
		if(p_driverBO.getPayMerchant().equalsIgnoreCase("") || p_driverBO.getPayMerchant() == "" || p_driverBO.getPayMerchant().length() == 0) {
			errors.append("You Must Enter PaymentMerchant<br>");
		}*/
		}

		
		
		return errors.toString();
	}

	
}
