package com.tds.action;

 
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.charges.constant.ICCConstant;
import com.charges.dao.ChargesDAO;
import com.common.action.PaymentProcessingAction;
import com.tds.cmp.bean.ManualSettleBean;
import com.tds.controller.TDSController;
import com.tds.dao.ManualDAO;
import com.tds.dao.ProcessingDAO;
import com.tds.dao.UserRoleDAO;
import com.tds.dao.UtilityDAO;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;
 

public class MaualCreditCardProcessing extends HttpServlet {
	
	private final Category cat = TDSController.cat;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ ServiceRequestAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	
	@Override
	public void doGet(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */

	@Override
	public void doPost(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	
	
	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		
		String m_eventParam = "";
		
		if(p_request.getParameter(TDSConstants.eventParam) != null) {
			m_eventParam = p_request.getParameter(TDSConstants.eventParam);
		}
		
		cat.info("Event Param "+m_eventParam);
		//System.out.println("Event:"+ m_eventParam);
		HttpSession m_session = p_request.getSession();
	 
		if(m_session.getAttribute("user") != null) {
			if(m_eventParam.equalsIgnoreCase("manualCCProcess")) {			 
				manualCCProcess(p_request,p_response);
			}else if(m_eventParam.equalsIgnoreCase("getSummarytoAlter")) {			 
				getSummarytoAlter(p_request,p_response);
			}else if(m_eventParam.equalsIgnoreCase("getSummarytoAlterlimit")) {	
				getSummarytoAlterlimit(p_request,p_response);
			}else if(m_eventParam.equalsIgnoreCase("voidPayment")) {			 
				voidPayment(p_request,p_response);
			}else if(m_eventParam.equalsIgnoreCase("alterPayment")) {
				//alterManualPayment(p_request,p_response);			
			}  else if(m_eventParam.equalsIgnoreCase("getRiderSign")) {			 
				//getRiderSign(p_request,p_response);
			} else if(m_eventParam.equalsIgnoreCase("getUntxnSummary")) {			 
				getUntxnSummary(p_request,p_response);
			} else if(m_eventParam.equalsIgnoreCase("capturePayment")) {			 
				capturePayment(p_request,p_response);
			} else if (m_eventParam.equalsIgnoreCase(TDSConstants.manualSettle )) {
				manualSettle(p_request, p_response);
			} else if (m_eventParam.equalsIgnoreCase(TDSConstants.multiDriverCharges )) {
				multiDriverCharges(p_request, p_response);
			} 
			 
		} else {
			
			sentErrorReport(p_request,p_response);
		}
		
		
	}
	

	//Start
	
	public void multiDriverCharges(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		HttpSession m_session = request.getSession();
		cat.info("In Multi Driver Charges ");
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		
		 
		
		String detailsButton = request.getParameter("GetDetailsButton");
		String processButton = request.getParameter("ProcessButton");
		String effectiveDate = request.getParameter("EffectiveDate");
		String description = request.getParameter("Description");
		String chargeTypeDesc = request.getParameter("ChargeType");
		

		String amt = request.getParameter("amt") == null ?"0.00":(request.getParameter("amt").split(" ")[0].equals("$")?request.getParameter("amt").split(" ")[1]:request.getParameter("amt").split(" ")[0]);
		Boolean applyToAllDrivers = request.getParameter("ApplyToAllDrivers")==null?false:true;
		Boolean applyToRangeOfDrivers = request.getParameter("ApplyToRangeOfDrivers")==null?false:true;
		Boolean applyToListOfDrivers = request.getParameter("ApplyToListOfDrivers")==null?false:true;
		
		Boolean applyToMisc = request.getParameter("applyToMisc")==null?false:true;
		Boolean applyToCharges = request.getParameter("applyToCharges")==null?false:true;
		String fromDriverID = request.getParameter("fromDriver");
		String toDriverID = request.getParameter("toDriver");
		int all = 0;
		int chargeType = 0;
		String[] pieces1 = null;
		
		if (processButton != null){
			int location = 0;
			if (applyToMisc ){
				location = 1; //Apply to Driver Penalty Screen
			}else
				location = 2;//Apply to Driver Charges  Screen
	
			if (chargeTypeDesc.equals("Credit")) {
				chargeType = 1;
			}else{
				chargeType = 2;
			}
			
			ArrayList driverList; 
		
			if (applyToAllDrivers)
			{
				all = 1;
			}else if (applyToRangeOfDrivers){
				all = 2;		
			}else if (applyToListOfDrivers) {
				all = 3;
				pieces1 = fromDriverID.split( ";" );
			}
			StringBuffer error = validateMultipleCharges(request);
			if(!(error.toString().length()>0))
			{
			// Get and Verify the driver List
				driverList = UtilityDAO.getAndVerifyDriverNumbers(m_adminBO.getAssociateCode() , pieces1 , fromDriverID , toDriverID , all);
				if (driverList.size() < 1){
						request.setAttribute("error", "No Valid Drivers within this range");
					
				}else{
					boolean successfullInsert = UtilityDAO.insertChargesForMultipleDriverNumbers(m_adminBO.getAssociateCode(), driverList, effectiveDate, description, amt, chargeType, location);
					if (!successfullInsert){
						request.setAttribute("error", "Error Inserting Charges");
					 
						
					}else{
						request.setAttribute("message", "Inserted " + driverList.size() + "records");
					}
				}
			} else {
				request.setAttribute("error", error.toString());  
				request.setAttribute("EffectiveDate", effectiveDate);
				request.setAttribute("Description", description);
				request.setAttribute("amt", amt);
				request.setAttribute("fromDriver", fromDriverID);
				request.setAttribute("toDriver", toDriverID);
				
			}
		}
		
		request.setAttribute("screen", "/Financial/MultipleCharges.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}

	 public StringBuffer validateMultipleCharges(HttpServletRequest request)
	 {
		 StringBuffer error = new StringBuffer();
		 
		 	String effectiveDate = request.getParameter("EffectiveDate");
			//String description = request.getParameter("Description");
			//String chargeTypeDesc = request.getParameter("ChargeType");

			String amt = request.getParameter("amt") == null ?"0.00":(request.getParameter("amt").split(" ")[0].equals("$")?request.getParameter("amt").split(" ")[1]:request.getParameter("amt").split(" ")[0]);
			Boolean applyToAllDrivers = request.getParameter("ApplyToAllDrivers")==null?false:true;
			Boolean applyToRangeOfDrivers = request.getParameter("ApplyToRangeOfDrivers")==null?false:true;
			Boolean applyToListOfDrivers = request.getParameter("ApplyToListOfDrivers")==null?false:true;
			
			Boolean applyToMisc = request.getParameter("applyToMisc")==null?false:true;
			Boolean applyToCharges = request.getParameter("applyToCharges")==null?false:true;
			String fromDriverID = request.getParameter("fromDriver");
			String toDriverID = request.getParameter("toDriver");
			
			if(effectiveDate==null || effectiveDate.equalsIgnoreCase(""))
			{
				error.append("Effective Date should not be empty <br>");
			}
			if(amt == null || amt.equals("") || !TDSValidation.isDoubleNumber(amt))
			{
				error.append("Amount is not valid<br>");
			}
			if(!applyToAllDrivers && !applyToRangeOfDrivers && !applyToListOfDrivers)
			{
				error.append("Please choose atleast one option <br>");
			}
			if(applyToRangeOfDrivers)
			{
				if(fromDriverID == null || fromDriverID.equals("") || toDriverID == null || toDriverID.equals(""))
					error.append("Please enter From Driver and To Driver <br>");
			}
			if(applyToListOfDrivers)
			{
				
			}
			
			if(!applyToMisc && !applyToCharges )
			{
				error.append("Please select Misc or Charges <br>");
			}
			
			
		 return error;
	 }
	
	//End
	
	public void getUntxnSummary(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");	
		ArrayList al_list = null;
		 
		
		String Button = p_request.getParameter("Button");
		if("Search".equalsIgnoreCase(Button)) 
		{
			al_list = ChargesDAO.getUnCaptureTransactionToReverse(p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),m_adminBO.getAssociateCode() , p_request.getParameter("driver"),m_adminBO.getTimeZone());
		} 	
		
		p_request.setAttribute("al_list", al_list);
		p_request.setAttribute("screen", "/CreditCardProcess/UnCaptureTransactionSummary.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
    	requestDispatcher.forward(p_request, p_response);
	}
	
	
	public void getSummarytoAlter(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO = ((AdminRegistrationBO) m_session.getAttribute("user"));	
		ArrayList al_list = null;
		
		String fleetNo="";
		String fleetSize = p_request.getParameter("fleetSize")==null?"0":p_request.getParameter("fleetSize");
		String Button = p_request.getParameter("Button");

		if(!fleetSize.equals("0")){
			fleetNo = p_request.getParameter("fleetV").equals("0")?"":p_request.getParameter("fleetV");
			System.out.println("fleetsize && fleetNo:"+fleetSize+":"+fleetNo);
		}
		
		if("Search".equalsIgnoreCase(Button)) 
		{
			int no_limit = 0;
			int plength=5; // Increase or Decrease the numbers in the link sequence at the bottom of the page. Example <12345> can be increased to <12345678910>
			int start =0;
			int end = 20;
			int no_of_rows=20;
			int pstart=1;
			int pend=plength+1;
			int t_length=0;
			try{
							
				t_length = UserRoleDAO.transtotalRecordLength(m_adminBO.getMasterAssociateCode(),p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),fleetNo,p_request.getParameter("driver"),p_request.getParameter("approval_code"),p_request.getParameter("card_no"),p_request.getParameter("approval")) ;
				no_limit = (int)Math.ceil((t_length/20.0));
				if(p_request.getParameter("first") == null){
					al_list = ChargesDAO.getTransactionToReverse1(start,end,p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),m_adminBO.getMasterAssociateCode() , p_request.getParameter("driver"),p_request.getParameter("approval_code"),p_request.getParameter("card_no"),p_request.getParameter("approval"),m_adminBO.getTimeZone(),fleetNo);
					if(no_limit<end){
						pend=no_limit+1;
						p_request.setAttribute("pend", pend+"");
					}
					else{
						p_request.setAttribute("pend", pend+"");
					}
				} else {
					if(p_request.getParameter("pageNo")!=null || p_request.getParameter("previous")!=null || p_request.getParameter("next")!=null){
						if(p_request.getParameter("pageNo")!=null){
							int pagevalue=Integer.parseInt(p_request.getParameter("pageNo"));
							pstart=Integer.parseInt(p_request.getParameter("pstart"));
							pend=Integer.parseInt(p_request.getParameter("pend"));														
							start=(pagevalue*no_of_rows)-no_of_rows;
							p_request.setAttribute("pend", pend+"");							
						}
						if(p_request.getParameter("previous")!=null){
							int pagevalue=Integer.parseInt(p_request.getParameter("previous"));
							System.out.println("pagevalue="+pagevalue);
							if(pstart<pagevalue){
								if(pstart<(pagevalue-plength)){
									pstart=pagevalue-plength;
									start=pstart;
									if((pstart+plength)<no_limit){
										pend=pstart+plength;
									}
									else{
										pend=no_limit;
									}
								}	
								else if(pstart==(pagevalue-plength)){
									pstart=pagevalue-plength;
									start=pstart; 
									if(pagevalue<no_limit){
										pend=pagevalue;
									}else{
										pend=no_limit;
									}									
								}
								else{
									start=pstart;
								}
							}
							else if(pstart==pagevalue){
								pstart=pagevalue;
								start=pstart;
								if((pagevalue+plength)<no_limit){
									pend=pagevalue+plength;
								}
								else{
									pend=no_limit;
								}
							}
							else{
								start=pstart;
							}
							start=(start*no_of_rows)-no_of_rows;
							p_request.setAttribute("pend", pend+"");
						}
						if( p_request.getParameter("next")!=null){
							int pagevalue=Integer.parseInt(p_request.getParameter("next"));
							if(pagevalue<no_limit){
								if((pagevalue+plength)<=no_limit){
									pstart=pagevalue;
									start=pstart;
								}
								else{
									pstart=pagevalue;	
									start=pstart;
								}
								if((pagevalue+plength)<=no_limit){
									pend=pagevalue+plength;
								}
								else{
									pend=no_limit+1;
								}
							}
							else if(pagevalue==no_limit){
								pstart=pagevalue;
								start=pstart;
								pend=no_limit+1;
							}
							else{
								pstart=pagevalue-1;
								start=pstart;
								if((pstart+plength)<no_limit){
									pend=pstart+plength;
								}
								else{
									pend=no_limit;
								}
								if(pend==pstart){
									pend=pend+1;
								}
							}
							start=(start*no_of_rows)-no_of_rows;
							p_request.setAttribute("pend", pend+"");
						}
						al_list = ChargesDAO.getTransactionToReverse1(start,end,p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),m_adminBO.getAssociateCode() , p_request.getParameter("driver"),p_request.getParameter("approval_code"),p_request.getParameter("card_no"),p_request.getParameter("approval"),m_adminBO.getTimeZone(),fleetNo);
						
					}
				}	
				//System.out.println("T Length--->"+t_length);
				for(int i=0;i<t_length;i++){ 
					if(p_request.getParameter("page"+i)!=null){
						start=(i*10)-10;
						al_list = ChargesDAO.getTransactionToReverse1(start,end,p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),m_adminBO.getAssociateCode() , p_request.getParameter("driver"),p_request.getParameter("approval_code"),p_request.getParameter("card_no"),p_request.getParameter("approval"),m_adminBO.getTimeZone(),fleetNo);
					}
				}
				if(al_list.size()>0){
					p_request.setAttribute("pstart", pstart+"");
					p_request.setAttribute("plength", plength+"");
					p_request.setAttribute("startValue", start+"");
					p_request.setAttribute("limit", no_limit+"");
					p_request.setAttribute("start", start+"");
					p_request.setAttribute("end", end+"");
				}
			} catch(Exception ex){
					ex.printStackTrace();
					cat.info("The Exception in the method driver summary  : "+ex.getMessage());
			}
		}
		p_request.setAttribute("al_list", al_list);
		p_request.setAttribute("screen", "/CreditCardProcess/TransactionSummary.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
    	requestDispatcher.forward(p_request, p_response);
	}
	
	public void getSummarytoAlterlimit(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");	
		ArrayList al_list = null;
		 
		
		String Button = p_request.getParameter("Button");
		if("Search".equalsIgnoreCase(Button)) 
		{
			double no_limit = 0;
			int plength=5; // Increase or Decrease the numbers in the link sequence at the bottom of the page. Example <12345> can be increased to <12345678910>
			int start =0;
			int end = 20;
			int no_of_rows=20;
			int pstart=1;
			int pend=plength+1;
			int t_length=0;
			try{
							
				t_length = UserRoleDAO.transtotalRecordLength(m_adminBO.getAssociateCode(),p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),m_adminBO.getAssociateCode() , p_request.getParameter("driver"),p_request.getParameter("approval_code"),p_request.getParameter("card_no"),p_request.getParameter("approval")) ;
				no_limit = Math.ceil((t_length/20.0));
				if(p_request.getParameter("first") == null){
					al_list = ChargesDAO.getTransactionToReverse(start,end,p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),m_adminBO.getAssociateCode() , p_request.getParameter("driver"),p_request.getParameter("approval_code"),p_request.getParameter("card_no"),p_request.getParameter("approval"),m_adminBO.getTimeZone());
					if(no_limit<end){
						pend=(int)no_limit+1;
						System.out.println("pend ="+pend);
						p_request.setAttribute("pend", pend+"");
					}
					else{
						p_request.setAttribute("pend", pend+"");
					}
				} else {
					if(p_request.getParameter("pageNo")!=null || p_request.getParameter("previous")!=null || p_request.getParameter("next")!=null){
						if(p_request.getParameter("pageNo")!=null){
							int pagevalue=Integer.parseInt(p_request.getParameter("pageNo"));
							pstart=Integer.parseInt(p_request.getParameter("pstart"));
							pend=Integer.parseInt(p_request.getParameter("pend"));														
							start=(pagevalue*no_of_rows)-no_of_rows;
							p_request.setAttribute("pend", pend+"");							
						}
						else if(p_request.getParameter("previous")!=null){
							int pagevalue=Integer.parseInt(p_request.getParameter("previous"));
							System.out.println("pagevalue="+pagevalue);
							if(pstart<pagevalue){
								if(pstart<(pagevalue-plength)){
									pstart=pagevalue-plength;
									start=pstart;
									if((pstart+plength)<no_limit){
										pend=pstart+plength;
									}
									else{
										pend=(int)no_limit;
									}
								}	
								else if(pstart==(pagevalue-plength)){
									pstart=pagevalue-plength;
									start=pstart; 
									if(pagevalue<no_limit){
										pend=pagevalue;
									}else{
										pend=(int)no_limit;
									}									
								}
								else{
									start=pstart;
								}
							}
							else if(pstart==pagevalue){
								pstart=pagevalue;
								start=pstart;
								if((pagevalue+plength)<no_limit){
									pend=pagevalue+plength;
								}
								else{
									pend=(int)no_limit;
								}
							}
							else{
								start=pstart;
							}
							start=(start*no_of_rows)-no_of_rows;
							p_request.setAttribute("pend", pend+"");
						}
						else if( p_request.getParameter("next")!=null){
							int pagevalue=Integer.parseInt(p_request.getParameter("next"));
							if(pagevalue<no_limit){
								if((pagevalue+plength)<=no_limit){
									pstart=pagevalue;
									start=pstart;
								}
								else{
									pstart=pagevalue;	
									start=pstart;
								}
								if((pagevalue+plength)<=no_limit){
									pend=pagevalue+plength;
								}
								else{
									pend=(int)no_limit+1;
								}
							}
							else if(pagevalue==no_limit){
								pstart=pagevalue;
								start=pstart;
								pend=(int)no_limit+1;
							}
							else{
								pstart=pagevalue-1;
								start=pstart;
								if((pstart+plength)<no_limit){
									pend=pstart+plength;
								}
								else{
									pend=(int)no_limit;
								}
								if(pend==pstart){
									pend=pend+1;
								}
							}
							start=(start*no_of_rows)-no_of_rows;
							p_request.setAttribute("pend", pend+"");
						}
						al_list = ChargesDAO.getTransactionToReverse(start,end,p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),m_adminBO.getAssociateCode() , p_request.getParameter("driver"),p_request.getParameter("approval_code"),p_request.getParameter("card_no"),p_request.getParameter("approval"),m_adminBO.getTimeZone());
						
				}
				
				}
				for(int i=0;i<t_length;i++){
					if(p_request.getParameter("page"+i)!=null){
						start=(i*10)-10;
						al_list = ChargesDAO.getTransactionToReverse(start,end,p_request.getParameter("from_date"), p_request.getParameter("to_date"), p_request.getParameter("amount"),m_adminBO.getAssociateCode() , p_request.getParameter("driver"),p_request.getParameter("approval_code"),p_request.getParameter("card_no"),p_request.getParameter("approval"),m_adminBO.getTimeZone());

					}
				}
				if(al_list.size()>0){
					p_request.setAttribute("pstart", pstart+"");
					p_request.setAttribute("plength", plength+"");
					p_request.setAttribute("startValue", start+"");
					p_request.setAttribute("limit", no_limit+"");
					p_request.setAttribute("start", start+"");
					p_request.setAttribute("end", end+"");
					
					
				}
					
			} catch(Exception ex){
					cat.info("The Exception in the method driver summary  : "+ex.getMessage());
			}
		} 	
		
		p_request.setAttribute("type", "1");
		p_request.setAttribute("al_list", al_list);
		p_request.setAttribute("screen", "/CreditCardProcess/TransactionSummary.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
    	requestDispatcher.forward(p_request, p_response);
	}
	
	public void sentErrorReport(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		p_request.setAttribute("screen", "/failuer.jsp");
     	p_request.setAttribute("page", "Your Session Expired");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(p_request, p_response);

		
	}
	public void manualCCProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
	if(p_request.getParameter("ccno") == null){	
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		p_request.setAttribute("al_driver", UtilityDAO.getDriverList(m_adminBO.getAssociateCode(),0));
		p_request.setAttribute("screen", "/CreditCardProcess/ManualCreditCardProcess.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(p_request, p_response);
		
	}
	 else{
		
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		MobilePayment payment = null; 
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		PaymentProcessBean processBean = null;
		
		cat.info("DRIVER ID::"+p_request.getParameter("dd_driver_id"));
		cat.info("DRIVER ID in method::"+p_request.getParameter("driver_id"));
		
		
		p_request.setAttribute("al_driver", UtilityDAO.getDriverList(m_adminBO.getAssociateCode(),0));
		
		PaymentProcessingAction pay = new PaymentProcessingAction();
		payment = new MobilePayment();
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		
		
		String amt=p_request.getParameter("amt") == null ?"0.00":(p_request.getParameter("amt").split(" ")[0].equals("$")?p_request.getParameter("amt").split(" ")[1]:p_request.getParameter("amt").split(" ")[0]);
		String tip=p_request.getParameter("tip") == null ? "0.00":(p_request.getParameter("tip").split(" ")[0].equals("$")?p_request.getParameter("tip").split(" ")[1]:p_request.getParameter("tip").split(" ")[0]);
		String tripId=p_request.getParameter("tripid")==null?"null":p_request.getParameter("tripid");
		String Category="";
		int authCap;
		processBean = payment.magneticStripAndAmtParsing(p_request,p_response);
		processBean.setB_driverid(p_request.getParameter("driver_id")==null?"0":p_request.getParameter("driver_id"));
		processBean.setB_operator(m_adminBO.getUid());
		processBean.setB_amount(amt);
		processBean.setB_tip_amt(tip);
		processBean.setB_trip_id(tripId);
		//processBean.setB_cardExpiryDate(p_request.getParameter("expmonth")+p_request.getParameter("expyear"));
		processBean.setB_cardExpiryDate(p_request.getParameter("expdate"));
		processBean.setAdd1(p_request.getParameter("add1"));
		processBean.setAdd2(p_request.getParameter("add2"));
		processBean.setCity(p_request.getParameter("city"));
		processBean.setState(p_request.getParameter("state"));
		processBean.setZip(p_request.getParameter("zip"));
		processBean.setCvv(p_request.getParameter("cvv"));
		//processBean.setAvs(p_request.getParameter("avs"));
		processBean.setMerchid(m_adminBO.getMerchid());
		processBean.setServiceid(m_adminBO.getServiceid());
		
		//System.out.println(p_request.getParameter("category"));
		if(p_request.getParameter("category").equals("Auth")){
			Category="Mobile";
			authCap=ICCConstant.PREAUTH;
		} else {
			Category="Internet";
			authCap=ICCConstant.SETTLED;
		}
		
		StringBuffer error = validateCC(processBean.getB_driverid(), m_adminBO.getAssociateCode(), amt, tip, p_request.getParameter("ccno"),p_request.getParameter("expdate"));
		
		//if(UtilityDAO.Assocodedriveridmatch(processBean.getB_driverid(),m_adminBO.getAssociateCode()) && p_request.getParameter("amt").split(" ").length == 2 && TDSValidation.isDoubleNumber(tip) && UtilityDAO.Assocodedriveridmatch(p_request.getParameter("driver_id"),m_adminBO.getAssociateCode()) && TDSValidation.isDoubleNumber(amt) && !"".equals(p_request.getParameter("ccno")) && p_request.getParameter("ccno").length() == 16)
		if(!(error.toString().length()>0))
		{
			
			processBean = pay.initiatePayProcess(poolBO,processBean,Category,m_adminBO.getMasterAssociateCode());			
			
			if(processBean.getB_approval_status().equalsIgnoreCase("1") )
			{	
				//check1
				//AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
				//ProcessingDAO.insertPaymentIntoWork(processBean,p_request.getRealPath("/images/nosign.gif"),poolBO, m_adminBO);
				ChargesDAO.insertPaymentIntoCC(processBean, p_request.getRealPath("/images/nosign.gif"), poolBO, m_adminBO,authCap,ICCConstant.GAC);
				//ProcessingDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), processBean.getB_trans_id(), processBean.getB_trans_id(), TDSValidation.getDefaultsign(p_request.getRealPath("/images/nosign.gif")));
				/*ChargesDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), processBean.getB_trans_id(),
						processBean.getB_trans_id2(), processBean.getB_cap_trans(), "", processBean.getB_sign(),
						((AdminRegistrationBO) p_request.getSession()
								.getAttribute("user")).getUid());
				*/
				ProcessingDAO.insertManualCCLog(processBean);
								
			 	p_response.sendRedirect("/TDS/control?action=manualccp&module=financeView&event=manualCCProcess&msgtype=1&pageMsg="+"Payment Processed Successfully and Approval Code is:"+processBean.getB_trans_id());
			} else if(processBean.getB_approval_status().equalsIgnoreCase("2")) {
				//AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		
				ChargesDAO.insertPaymentIntoCC(processBean,p_request.getRealPath("/images/nosign.gif"),poolBO, m_adminBO,authCap,ICCConstant.GAC);
				ProcessingDAO.insertManualCCLog(processBean);
				//p_request.setAttribute("screen", "/Success.jsp");
	        	//p_request.setAttribute("page", "Transaction Denied");
				
	        	p_response.sendRedirect("/TDS/control?action=manualccp&module=financeView&event=manualCCProcess&msgtype=2&pageMsg="+ "Transaction Denied Status:"+processBean.getB_approval());
				
			} else  {
				processBean.setB_amount(p_request.getParameter("amt"));
				processBean.setB_tip_amt(p_request.getParameter("tip"));
				p_request.setAttribute("error", "Fail to Process Your Request <Br>Reason: <BR> Check Your Data <Br> Check Your Network");
	        	p_request.setAttribute("screen", "/CreditCardProcess/ManualCreditCardProcess.jsp");
	        	requestDispatcher.forward(p_request, p_response);
			}
		
		} else {
			processBean.setB_amount(p_request.getParameter("amt"));
			processBean.setB_tip_amt(p_request.getParameter("tip"));
			p_request.setAttribute("error", error.toString());
			p_request.setAttribute("screen", "/CreditCardProcess/ManualCreditCardProcess.jsp");
			requestDispatcher.forward(p_request, p_response);
		}
	}
}
	
public static StringBuffer validateCC(String driver,String assoc,String amt,String tip,String ccno,String expdate)
{
	StringBuffer error = new StringBuffer();
	if(driver.equals("") || driver.equals("0") )
	{
		error.append("Driver Id Can't Be Empty<br>");
		
	}else if(!UtilityDAO.Assocodedriveridmatch(driver,assoc))
	{
		error.append("Driver does not belong to your company<br>");
	}
	if(!TDSValidation.isDoubleNumber(tip) || !TDSValidation.isDoubleNumber(amt))
	{
		error.append("Check amount or tip amount entered<br>");
	}
	if(ccno.length() <12 || ccno.length()>16)
	{
		error.append("Pls provide valid card no<br>");
	}
	if(expdate.equals("") || expdate.length()!=4 || !TDSValidation.isOnlyNumbers(expdate))
	{
		error.append("InValid Expiry Date");
	}
	
	return error;
}

	/*public void alterManualPayment(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException 
	{	 
		  
		String Button = p_request.getParameter("Button");
		PaymentProcessBean processBean;
		ApplicationPoolBO poolBO;
		boolean status;
		String error=null;
		
		if(Button == null)
		{
			p_request.setAttribute("screen", "/CreditCardProcess/AlterManualPayment.jsp");
			
		} else  
		{
			if(TDSValidation.isDoubleNumber(p_request.getParameter("alter_amount"))) {
				processBean = new PaymentProcessBean();
				poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		 		
				processBean.setB_tip_amt( p_request.getParameter("tip")==null?"0":p_request.getParameter("tip"));
				processBean.setB_amount( p_request.getParameter("alter_amount")==null?"0":p_request.getParameter("alter_amount"));
				processBean.setB_trans_id(p_request.getParameter("trans_id")==null?"":p_request.getParameter("trans_id"));
				
				processBean = PaymentProcessingAction.alterPayment(poolBO,processBean);
				
				if(processBean.getB_approval_status().equalsIgnoreCase("1"))
				{
					ProcessingDAO.alterTransAction(p_request.getParameter("trans_id"), p_request.getParameter("alter_amount"),processBean.getB_tip_amt());
					
				} else if(processBean.getB_approval_status().equalsIgnoreCase("2"))
				{
					error = "Unable Perform Adjust";
				} else {
					error = "Communication Failed";
				}
			}else {
				error = "Please enter a valid amount";
			}
					
			if(error != null && error.length() > 0)
			{
				p_request.setAttribute("error", error);
				p_request.setAttribute("screen", "/CreditCardProcess/AlterManualPayment.jsp");
			} else {
				
				p_request.setAttribute("screen", "/Success.jsp");
	        	p_request.setAttribute("page", "Transaction Altered");
				
			}
				
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(p_request, p_response);

		
		
	}*/


	public void capturePayment(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		PaymentProcessBean processBean = new PaymentProcessBean();
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) p_request.getSession().getAttribute("user");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		processBean.setB_trans_id(p_request.getParameter("transid")==null?"":p_request.getParameter("transid"));
		String transactionId=processBean.getB_trans_id();
		processBean = ChargesDAO.chckCapture(processBean);
		processBean.setB_tip_amt(p_request.getParameter("tip_amount")==null?processBean.getB_tip_amt():p_request.getParameter("tip_amount"));
		processBean.setMerchid(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getMerchid());
		processBean.setServiceid(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getServiceid());
		
		if(processBean.getError_log().equalsIgnoreCase("0")){
			processBean = PaymentProcessingAction.capturePayment(poolBO,processBean,m_adminBO.getMasterAssociateCode());
			if(processBean.getB_approval_status().equalsIgnoreCase("1"))
			{	
				//ProcessingDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), processBean.getB_trans_id(), processBean.getB_cap_trans(), processBean.getB_sign());
				ChargesDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), transactionId,
						processBean.getB_trans_id2(), processBean.getB_cap_trans(), "", processBean.getB_sign(),
						((AdminRegistrationBO) p_request.getSession()
								.getAttribute("user")).getUid());
				p_request.setAttribute("error", "Transaction Captured Successfully");
//				p_response.sendRedirect("/TDS/control?action=redirect&module=financeView&screen=Success.jsp&page=Captured");
			}  else if(processBean.getB_approval_status().equalsIgnoreCase("2"))
			{
				p_request.setAttribute("error", "Failed To Capture Transaction");
//				p_response.sendRedirect("/TDS/control?action=redirect&module=financeView&screen=failure.jsp&page=Capture Process");
			}	else {
				p_request.setAttribute("error", "Failed To Capture Transaction");
//				p_response.sendRedirect("/TDS/control?action=redirect&module=financeView&screen=failure.jsp&page=Communication Failed");
				 
			}
		} else {
			p_request.setAttribute("error", "Failed To Capture Transaction");
//			p_response.sendRedirect("/TDS/control?action=redirect&module=financeView&screen=failure.jsp&page=Please Contact Your Administartor,Error in Transaction");
		}
		p_request.setAttribute("screen", "/CreditCardProcess/TransactionSummary.jsp");
		requestDispatcher.forward(p_request, p_response);

		
	}

	public void voidPayment(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		AdminRegistrationBO adminBo=(AdminRegistrationBO) p_request.getSession().getAttribute("user");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		if(p_request.getParameter("Button") == null)
		{
			p_request.setAttribute("screen", "/CreditCardProcess/VoidPayment.jsp");
			requestDispatcher.forward(p_request, p_response);
		} else  
		{
			PaymentProcessBean processBean = new PaymentProcessBean();
			ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
			processBean.setB_trans_id(p_request.getParameter("transid")==null?"":p_request.getParameter("transid"));
			processBean.setB_trans_id2(p_request.getParameter("transid2")==null?"":p_request.getParameter("transid2"));
			processBean.setB_approval_code(p_request.getParameter("approvalcode")==null?"":p_request.getParameter("approvalcode"));
			
			processBean = ChargesDAO.chckReturnOrUndo(processBean);	
		
			processBean.setMerchid(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getMerchid());
			processBean.setServiceid(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getServiceid());
			if(processBean.getB_flg().equals("1") && Integer.parseInt(processBean.getAvs()==null?"0":processBean.getAvs())>=24){
		 		processBean.setReturnById(true);
		 	} else {
		 		processBean.setReturnById(false);
		 	}
			processBean = PaymentProcessingAction.voidPayment(poolBO,processBean,adminBo.getMasterAssociateCode());
			
			if(!processBean.isAuth_status() &&  !processBean.isReturnById()){
				processBean.setReturnById(true);
				processBean = PaymentProcessingAction.voidPayment(poolBO,processBean,adminBo.getMasterAssociateCode());
			}
			
			if(processBean.isAuth_status())
			{
				//ProcessingDAO.voidTransAction(p_request.getParameter("transid"), p_request.getParameter("desc"));
				ChargesDAO.voidTransAction(processBean.getB_trans_id(), processBean.getB_trans_id2(), p_request.getParameter("desc"),((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getUid());
//				p_response.sendRedirect("/TDS/control?action=redirect&module=financeView&screen=Success.jsp&page=Transaction Void");
				p_request.setAttribute("error", "Transaction Voided Successfully");
				p_request.setAttribute("screen", "/CreditCardProcess/TransactionSummary.jsp");
				requestDispatcher.forward(p_request, p_response);
			} else	{
//				p_response.sendRedirect("/TDS/control?action=redirect&module=financeView&screen=failure.jsp&page=Transaction Declined");
				p_request.setAttribute("error", "Fail to Process Your Request <Br>Reason: <BR> Check Your Data <Br> Check Your Network");
				p_request.setAttribute("screen", "/CreditCardProcess/TransactionSummary.jsp");
				requestDispatcher.forward(p_request, p_response);
			} /*else {
				p_response.sendRedirect("/TDS/control?action=redirect&module=financeView&screen=failure.jsp&page=Void Transaction");
				 
			}*/
			
		}
		
	}
	
	public void manualSettle(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		HttpSession m_session = request.getSession();
		cat.info("In Manual Settle function ");
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		ArrayList  manualSettleBeanList = (ArrayList) m_session.getAttribute("manualSettle");
		String detailsButton = request.getParameter("GetDetailsButton");
		String processButton = request.getParameter("ProcessButton");
		Boolean settleCC = request.getParameter("CreditCard")==null?false:true;
		Boolean settlePenalty = request.getParameter("Penalty")==null?false:true;
		Boolean settleVoucher = request.getParameter("Voucher")==null?false:true;
		String driverID = request.getParameter("driver");
		
		
		if(detailsButton != null)
		{
			if (driverID != null & (settleCC  || settlePenalty  || settleVoucher ))
			{
				manualSettleBeanList = ManualDAO.getTxnsToSettle(driverID, m_adminBO.getAssociateCode() , settleCC, settleVoucher, settlePenalty);
				m_session.setAttribute("manualSettle", manualSettleBeanList );
				request.setAttribute("screen", "/CreditCardProcess/manualSettleDriver.jsp");
			}
		}
		else if (processButton  != null & manualSettleBeanList != null)
		{
			ArrayList ccTxns = new ArrayList();
			ArrayList penaltyTxns = new ArrayList();
			ArrayList voucherTxns= new ArrayList();
			for(int i = 0; i <manualSettleBeanList.size(); i++)
			{
				
				if (request.getParameter("Settle" + i) != null)
				{
					ManualSettleBean mSB = (ManualSettleBean) manualSettleBeanList.get(i);
					if (mSB.getTxnLocation().equals("C"))
						ccTxns.add(mSB.getTxnID());
					if (mSB.getTxnLocation().equals("V"))
						voucherTxns.add(mSB.getTxnID());
					if (mSB.getTxnLocation().equals("P"))
						penaltyTxns.add(mSB.getTxnID());
				}
			}
			if (ccTxns.size() >0)
			{
				ManualDAO.moveToPaymentSetteled(m_adminBO.getAssociateCode(),driverID , ccTxns);
			}
			if (penaltyTxns.size() >0)
			{
				ManualDAO.moveToDriverPenality(m_adminBO.getAssociateCode(),driverID , penaltyTxns);
			}
			if (voucherTxns.size() >0)
			{
				ManualDAO.moveToVoucherSetteled(m_adminBO.getAssociateCode(),driverID , voucherTxns);
			}
			
			m_session.removeAttribute("manualSettle");
		} else {
			
			m_session.removeAttribute("manualSettle");			
			request.setAttribute("screen", "/CreditCardProcess/manualSettleDriver.jsp");
		}
		request.setAttribute("screen", "/CreditCardProcess/manualSettleDriver.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}
	
}
