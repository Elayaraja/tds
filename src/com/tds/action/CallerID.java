package com.tds.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tds.cmp.bean.CallerIDBean;
import com.tds.dao.CallerIDDAO;

/**
 * Servlet implementation class CallerID
 */
public class CallerID extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CallerID() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String userID = request.getParameter("userID");
		String password = request.getParameter("password");
		String lineNo = request.getParameter("lineNo");
		String time = request.getParameter("time");
		String phoneNo =  request.getParameter("phoneNo").replaceAll("[\\s\\-()]","");
		String name = request.getParameter("name");
		String ringing=request.getParameter("ringStatus");
		
		String associationCode = CallerIDDAO.getAssociationCode(userID, password);
		if(!associationCode.equals("")){
			CallerIDBean callerIDData = new CallerIDBean();
			callerIDData.setAssociationCode(associationCode);
			callerIDData.setPassword(password);
			callerIDData.setLineNo(lineNo);
			callerIDData.setTime(time);
			callerIDData.setPhoneNo(phoneNo);
			callerIDData.setName(name);
			if(ringing!=null){
				callerIDData.setRinging(true);
			}else{
				callerIDData.setRinging(false);
			}
			CallerIDDAO.storeCallerIDInfo(callerIDData);
			
		}
		
	}

}
