package com.tds.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverVehicleBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.SystemUnavailableException;
import com.tds.dao.CustomerMobileDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.db.TDSConnection;
import com.common.util.CheckZone;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ClientAuthenticationBO;
import com.tds.tdsBO.CustomerMobileBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.PasswordGen;
import com.common.util.PasswordHash;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;

/**
 * Servlet implementation class CustomerMobileAction
 */
public class CustomerMobileAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustomerMobileAction() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException, SystemUnavailableException {
		long timeAtBegin = System.currentTimeMillis();
		//System.out.println("CustomerMobileAction Servlet");
		String eventParam = "";
		HttpSession session = request.getSession(false);
		//ClientAuthenticationBO clientAdminBO1=(ClientAuthenticationBO)session.getAttribute("clientUser");

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
			System.out.println("Event"+eventParam);
		}
		if(eventParam.equalsIgnoreCase("gPI")){
			getPushID(request,response);
			return;
		}else if(eventParam.equalsIgnoreCase("charges")){
			charges(request,response);
		}else if(eventParam.equalsIgnoreCase("payFare")){
			payFare(request,response);
		}else if(eventParam.equalsIgnoreCase("showCards")){
			showCards(request, response);
		}
		//System.out.println("Event Param "+m_eventParam);
		//HttpSession session = request.getSession(false);
		/*	if(session !=null && session.getAttribute("user") != null && !eventParam.equalsIgnoreCase(TDSConstants.mobileLoginService)) {
			// Comment:: Checks to see if an operator has forcibly logged this driver out. If they have, the system forces a logout on the driver
			//           and removes that attribute from application pool.
			AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
		}*/
		if(eventParam.equalsIgnoreCase("clientLogin")) {
			clientLogin(request, response);
		}else if(eventParam.equalsIgnoreCase("clientRegistration")){
			clientRegistration(request,response);	
		}else if(eventParam.equalsIgnoreCase("openRequest")){
			openRequest(request,response);	
		}/*else if(eventParam.equalsIgnoreCase("driverLocation")){
			driverLocation(request,response);
		}*/else if(eventParam.equalsIgnoreCase("userIdCheck")){
			userIdCheck(request,response);
		}else if(eventParam.equalsIgnoreCase("getFlagsForCompany")){
			try {
				getFlagsForCompany(request,response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(session!=null&&session.getAttribute("clientUser")!=null ){
			if(eventParam.equalsIgnoreCase("driverLocation")){
				driverLocation(request,response);
			}else if(eventParam.equalsIgnoreCase("tripHistory")){
				tripHistory(request,response);
			}else if(eventParam.equalsIgnoreCase("tripSummary")){
				tripSummary(request,response);	
			}else if(eventParam.equalsIgnoreCase("clientAccountSummary")){
				clientAccountSummary(request,response);	
			}else if(eventParam.equalsIgnoreCase("clientAccountRegistration")){
				clientAccountRegistration(request,response);	
			}else if(eventParam.equalsIgnoreCase("jobLocation")){
				jobLocation(request,response);
			}else if(eventParam.equalsIgnoreCase("tagAddress")){
				tagAddress(request,response);
			}else if(eventParam.equalsIgnoreCase("checkAddress")){
				checkAddress(request,response);
			}else if(eventParam.equalsIgnoreCase("taggedAddressestoShow")){
				taggedAddressesToShow(request,response);
			}else if(eventParam.equalsIgnoreCase("favouriteAddresses")){
				favoriteAddresses(request,response);
			}else if(eventParam.equalsIgnoreCase("cancelTrip")){
				cancelTrip(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteFavAddress")){
				deleteFavAddress(request,response);
			}else if(eventParam.equalsIgnoreCase("changeStatus")){
				changeStatus(request,response);
			}else if (eventParam.equalsIgnoreCase("driverList")) {
				driverList(request, response);
			}else if(eventParam.equalsIgnoreCase("getDriverDocument")){
				getDriverDocument(request,response);
			}

		}else{
			clientLogin(request, response);
			return;
		}
		//System.out.println("Event="+eventParam+":"+(System.currentTimeMillis()-timeAtBegin));
	}
	

	public void clientLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		//System.out.println("Clientlogin function");
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession();
		ClientAuthenticationBO clientAdminBO=new ClientAuthenticationBO();
		CMBO.setUserId(request.getParameter("userId")==null?"": request.getParameter("userId") );
		int mode= Integer.parseInt((request.getParameter("loginOrLogout")==null?"0":request.getParameter("loginOrLogout")));
		JSONArray customerLogin= new JSONArray();
		JSONObject loginStatus = new JSONObject();
		StringBuffer resultStrBuffer = new StringBuffer();
		Cookie[] cookie= request.getCookies();
		if(request.getParameter("newEntry")!=null){
			if(cookie!=null){
				for(int i=0; i<cookie.length;i++){
					if(cookie[i].getName().equals("GAC-fingerPrint")){
						String userId=CustomerMobileDAO.getUserIdForThisCookie(cookie[i].getValue());
						CustomerMobileBO cMobileBO =new CustomerMobileBO();
						cMobileBO=CustomerMobileDAO.customerLoginWithCookie(userId);
						if(!cMobileBO.getUserId().equals("")){
							clientAdminBO.setUserId(cMobileBO.getUserId());
							clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
							clientAdminBO.setEmailId(cMobileBO.getEmailId());
							clientAdminBO.setUserName(cMobileBO.getName());
							clientAdminBO.setAssoccode(request.getParameter("assoccode"));
							//resultStrBuffer.append("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
							session.setAttribute("clientUser", clientAdminBO);
							response.getWriter().write("1");
							return;
						}else{
							response.getWriter().write("0");
							return;
						}
					}
				}
			}
			return;
		}
		if(mode==1){
			CMBO.setPassword(PasswordHash.encrypt(request.getParameter("password")==null?"":request.getParameter("password")));
			if(!CMBO.getUserId().equals("") && !CMBO.getPassword().equals("") ){
				CustomerMobileBO cMobileBO =new CustomerMobileBO();
				cMobileBO=CustomerMobileDAO.customerLogin(CMBO);
				if(cMobileBO!=null){
					clientAdminBO.setUserId(cMobileBO.getUserId());
					clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
					clientAdminBO.setEmailId(cMobileBO.getEmailId());
					clientAdminBO.setUserName(cMobileBO.getName());
					
					//clientAdminBO.setAssoccode(request.getParameter("assoccode"));
					String cmpCode = request.getParameter("assoccode");
					if(cmpCode.equalsIgnoreCase("")){
						
					}else if(cmpCode.equalsIgnoreCase("")){
						
					}else if(cmpCode.equalsIgnoreCase("")){
						
					}else if(cmpCode.equalsIgnoreCase("")){
						
					}else if(cmpCode.equalsIgnoreCase("")){
						
					}else if(cmpCode.equalsIgnoreCase("")){
						
					}else if(cmpCode.equalsIgnoreCase("")){
						
					}else if(cmpCode.equalsIgnoreCase("")){
						
					}else if(cmpCode.equalsIgnoreCase("")){
						
					}
					session.setAttribute("clientUser", clientAdminBO);
					if(request.getParameter("rememberMe")!=null && !request.getParameter("rememberMe").equals("")){
						String fingerPrint=PasswordGen.generatePassword("A", 16);
						Cookie c = new Cookie("GAC-fingerPrint", fingerPrint);
						c.setMaxAge(365 * 24 * 60 * 60); // one year
						response.addCookie(c);
						CustomerMobileDAO.setCookieAndUserId(fingerPrint,cMobileBO.getUserId());
					}
					try {
						loginStatus.put("Act", "LI");
						loginStatus.put("status", "loggedIn");
						loginStatus.put("userId", cMobileBO.getUserId());
						loginStatus.put("phoneNo", cMobileBO.getPhoneNumber());
						loginStatus.put("emailId", cMobileBO.getEmailId());
						loginStatus.put("name", cMobileBO.getName());
						loginStatus.put("sessionId", request.getSession().getId());
						loginStatus.put("getMessage", "Login successful");
						customerLogin.put(loginStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//response.getWriter().write("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
					//System.out.println("done"+customerLogin.toString());
					response.getWriter().write(customerLogin.toString());
					return;
				}else{
					try {
						loginStatus.put("Act", "LI");
						loginStatus.put("status", "notloggedin");
						loginStatus.put("getMessage", "Please Provide valid Username or Password");
						customerLogin.put(loginStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}else{
				try {
					loginStatus.put("Act", "LI");
					loginStatus.put("status", "notloggedin");
					loginStatus.put("getMessage", "Please Provide Username and Password");
					customerLogin.put(loginStatus);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			response.getWriter().write(customerLogin.toString());
		}else if(mode==0){
			ClientAuthenticationBO clientAdminBO1=(ClientAuthenticationBO)session.getAttribute("clientUser");
			session.invalidate();
			if(clientAdminBO1!=null){
				CustomerMobileDAO.deleteCookieForUserId(clientAdminBO1.getUserId());
			Cookie[] cookies = request.getCookies();
			List<Cookie> cookieList = new ArrayList<Cookie>();
			cookieList = Arrays.asList(cookies);
			for(Cookie cooki:cookieList) {
				if(cooki.getName().equals("GAC-fingerPrint")) {
					cooki.setValue(null);
					cooki.setMaxAge(0);
					response.addCookie(cooki);
				}
			}
			if(request.getParameter("responseType").equals("HTML5")){
				response.getWriter().write("loggedOut");
			}else{				
				try {
					loginStatus.put("Act", "LO");
					loginStatus.put("status", "loggedout");
					loginStatus.put("getMessage", "Successfully Logged out");
					customerLogin.put(loginStatus);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				response.getWriter().write(customerLogin.toString());
			}
		}
		}

	}
	
//	public void clientLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
//		System.out.println("Clientlogin function");
//		CustomerMobileBO CMBO=new CustomerMobileBO();
//		HttpSession session = request.getSession();
//		ClientAuthenticationBO clientAdminBO=new ClientAuthenticationBO();
//		CMBO.setUserId(request.getParameter("userId")==null?"": request.getParameter("userId") );
//		int mode= Integer.parseInt((request.getParameter("loginOrLogout")==null?"0":request.getParameter("loginOrLogout")));
//		JSONArray customerLogin= new JSONArray();
//		JSONObject loginStatus = new JSONObject();
//		StringBuffer resultStrBuffer = new StringBuffer();
//		Cookie[] cookie= request.getCookies();
//		if(request.getParameter("newEntry")!=null){
//			if(cookie!=null){
//				for(int i=0; i<cookie.length;i++){
//					if(cookie[i].getName().equals("GAC-fingerPrint")){
//						String userId=CustomerMobileDAO.getUserIdForThisCookie(cookie[i].getValue());
//						CustomerMobileBO cMobileBO =new CustomerMobileBO();
//						cMobileBO=CustomerMobileDAO.customerLoginWithCookie(userId);
//						if(!cMobileBO.getUserId().equals("")){
//							clientAdminBO.setUserId(cMobileBO.getUserId());
//							clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
//							clientAdminBO.setEmailId(cMobileBO.getEmailId());
//							clientAdminBO.setUserName(cMobileBO.getName());
//							clientAdminBO.setAssoccode(request.getParameter("assoccode"));
//							//resultStrBuffer.append("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
//							session.setAttribute("clientUser", clientAdminBO);
//							response.getWriter().write("1");
//							return;
//						}else{
//							response.getWriter().write("0");
//							return;
//						}
//					}
//				}
//			}
//			return;
//		}
//		if(mode==1){
//			CMBO.setPassword(PasswordHash.encrypt(request.getParameter("password")==null?"":request.getParameter("password")));
//			if(!CMBO.getUserId().equals("") && !CMBO.getPassword().equals("") ){
//				CustomerMobileBO cMobileBO =new CustomerMobileBO();
//				cMobileBO=CustomerMobileDAO.customerLogin(CMBO);
//				if(cMobileBO!=null){
//					clientAdminBO.setUserId(cMobileBO.getUserId());
//					clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
//					clientAdminBO.setEmailId(cMobileBO.getEmailId());
//					clientAdminBO.setUserName(cMobileBO.getName());
//					
//					//clientAdminBO.setAssoccode(request.getParameter("assoccode"));
//					String cmpCode = request.getParameter("assoccode");
//					if(cmpCode.equalsIgnoreCase("")){
//						
//					}else if(cmpCode.equalsIgnoreCase("")){
//						
//					}else if(cmpCode.equalsIgnoreCase("")){
//						
//					}else if(cmpCode.equalsIgnoreCase("")){
//						
//					}else if(cmpCode.equalsIgnoreCase("")){
//						
//					}else if(cmpCode.equalsIgnoreCase("")){
//						
//					}else if(cmpCode.equalsIgnoreCase("")){
//						
//					}else if(cmpCode.equalsIgnoreCase("")){
//						
//					}else if(cmpCode.equalsIgnoreCase("")){
//						
//					}
//					session.setAttribute("clientUser", clientAdminBO);
//					if(request.getParameter("rememberMe")!=null && !request.getParameter("rememberMe").equals("")){
//						String fingerPrint=PasswordGen.generatePassword("A", 16);
//						Cookie c = new Cookie("GAC-fingerPrint", fingerPrint);
//						c.setMaxAge(365 * 24 * 60 * 60); // one year
//						response.addCookie(c);
//						CustomerMobileDAO.setCookieAndUserId(fingerPrint,cMobileBO.getUserId());
//					}
//					try {
//						loginStatus.put("Act", "LI");
//						loginStatus.put("status", "loggedIn");
//						loginStatus.put("userId", cMobileBO.getUserId());
//						loginStatus.put("phoneNo", cMobileBO.getPhoneNumber());
//						loginStatus.put("emailId", cMobileBO.getEmailId());
//						loginStatus.put("name", cMobileBO.getName());
//						loginStatus.put("sessionId", request.getSession().getId());
//						loginStatus.put("getMessage", "Login successful");
//						customerLogin.put(loginStatus);
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					//response.getWriter().write("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
//					System.out.println("done"+customerLogin.toString());
//					response.getWriter().write(customerLogin.toString());
//					return;
//				}else{
//					try {
//						loginStatus.put("Act", "LI");
//						loginStatus.put("status", "notloggedin");
//						loginStatus.put("getMessage", "Please Provide valid Username or Password");
//						customerLogin.put(loginStatus);
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
//			}else{
//				try {
//					loginStatus.put("Act", "LI");
//					loginStatus.put("status", "notloggedin");
//					loginStatus.put("getMessage", "Please Provide Username and Password");
//					customerLogin.put(loginStatus);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//			response.getWriter().write(customerLogin.toString());
//		}else if(mode==0){
//			ClientAuthenticationBO clientAdminBO1=(ClientAuthenticationBO)session.getAttribute("clientUser");
//			session.invalidate();
//			if(clientAdminBO1!=null){
//				CustomerMobileDAO.deleteCookieForUserId(clientAdminBO1.getUserId());
//			Cookie[] cookies = request.getCookies();
//			List<Cookie> cookieList = new ArrayList<Cookie>();
//			cookieList = Arrays.asList(cookies);
//			for(Cookie cooki:cookieList) {
//				if(cooki.getName().equals("GAC-fingerPrint")) {
//					cooki.setValue(null);
//					cooki.setMaxAge(0);
//					response.addCookie(cooki);
//				}
//			}
//			if(request.getParameter("responseType").equals("HTML5")){
//				response.getWriter().write("loggedOut");
//			}else{				
//				try {
//					loginStatus.put("Act", "LO");
//					loginStatus.put("status", "loggedout");
//					loginStatus.put("getMessage", "Successfully Logged out");
//					customerLogin.put(loginStatus);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				response.getWriter().write(customerLogin.toString());
//			}
//		}
//		}
//
//	}
	public void clientRegistration (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		if(request.getParameter("registerButton")==null){

			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/registration.jsp");
			dispatcher.forward(request, response);
			return;
		}else{
			CustomerMobileBO CMBO=new CustomerMobileBO();
			CMBO.setUserId(request.getParameter("userId"));
			CMBO.setPassword(PasswordHash.encrypt(request.getParameter("password")));
			CMBO.setEmailId(request.getParameter("emailId")==null?"": request.getParameter("emailId"));
			CMBO.setPhoneNumber(request.getParameter("phoneNumber")==null?"": request.getParameter("phoneNumber"));
			CMBO.setUserName(request.getParameter("userName")==null?"": request.getParameter("userName"));
			CMBO.setAssoccode(request.getParameter("ccode")==null?"": request.getParameter("ccode"));
			JSONArray customerRegistration= new JSONArray();
			JSONObject registrationStatus = new JSONObject();
			if(!CMBO.getUserId().equals("") && !CMBO.getPassword().equals("") ){

				int result=CustomerMobileDAO.customerRegistrationForLogin(CMBO);
				if(request.getParameter("responseType").equalsIgnoreCase("HTML5")){
					if(result==1){
						request.setAttribute("error", "Registered Successfully");
						response.getWriter().write("Registered");

						//RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/registration.jsp");
						//dispatcher.forward(request, response);
					}else{
						request.setAttribute("error", "Registration was Unsuccessful");
						response.getWriter().write("not Registered");

						//RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/registration.jsp");
						//dispatcher.forward(request, response);
					}
				}else{
					if(result==1){
						try {
							registrationStatus.put("getMessage","Successfully Registered");
							customerRegistration.put(registrationStatus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else{
						try {
							registrationStatus.put("getMessage","Registration Unsuccessful");
							customerRegistration.put(registrationStatus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					response.getWriter().write(customerRegistration.toString());
				}
			}else{
				if(request.getParameter("responseType")=="HTML5"){
					request.setAttribute("error", "Please enter the username and password");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/registration.jsp");
					dispatcher.forward(request, response);
					return;
				}else{
					try {
						registrationStatus.put("getMessage","Please enter the username and password");
						customerRegistration.put(registrationStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	public void clientAccountRegistration (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		if(clientAdminBO.getUserId()!=null){
			CustomerMobileBO customerAccount =new CustomerMobileBO();
			if(request.getParameter("registerButton")==null){
				if(request.getParameter("serialNo")!=null){
					customerAccount=CustomerMobileDAO.customerAccount(request.getParameter("serialNo"));
					request.setAttribute("accountDetail", customerAccount);
				}else{
					customerAccount.setName(clientAdminBO.getUserName());
					customerAccount.setUserId(clientAdminBO.getUserId());
					request.setAttribute("accountDetail", customerAccount);
				}
				RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/accountRegistration.jsp");
				dispatcher.forward(request, response);
				return;
			}
			CMBO.setUserId(clientAdminBO.getUserId());
			CMBO.setPaymentType(request.getParameter("paymentType")==null?"": request.getParameter("paymentType"));
			CMBO.setUserName(request.getParameter("userName")==null?"": request.getParameter("userName"));
			CMBO.setCardNumber(request.getParameter("cardNumber")==null?"": request.getParameter("cardNumber"));
			CMBO.setCardExpiryDate(request.getParameter("cardExpiryDate")==null?"": request.getParameter("cardExpiryDate"));
			CMBO.setVoucherNumber(request.getParameter("voucherNumber")==null?"": request.getParameter("voucherNumber"));
			CMBO.setVoucherExpiryDate(request.getParameter("voucherExpiryDate")==null?"": request.getParameter("voucherExpiryDate"));
			int mode= Integer.parseInt(request.getParameter("mode")==null?"0":request.getParameter("mode"));
			//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
			if(mode==1){
				int result=CustomerMobileDAO.customerAccountRegistration(CMBO,mode);
				JSONArray customerAccountRegistration= new JSONArray();
				JSONObject registrationStatus = new JSONObject();
				if(result==1){
					try {
						registrationStatus.put("getMessage","Successfully Registered");
						customerAccountRegistration.put(registrationStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					try {
						registrationStatus.put("getMessage","Registration Unsuccessful");
						customerAccountRegistration.put(registrationStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				response.getWriter().write(customerAccountRegistration.toString());	
				return;
			}else if(mode==2){
				CMBO.setSerialNo(request.getParameter("serialNumber")==null?"": request.getParameter("serialNumber"));
				int result = CustomerMobileDAO.customerAccountRegistration(CMBO,mode);
				if(result==1){
					response.getWriter().write("updated Successfully");
				}else{
					response.getWriter().write("update Unsuccessful");
				}
			}else if(mode==3){
				CMBO.setSerialNo(request.getParameter("serialNumber")==null?"": request.getParameter("serialNumber"));

				int result = CustomerMobileDAO.customerAccountRegistration(CMBO,mode);
				if(result==1){
					response.getWriter().write("Deleted Successfully");
				}else{
					response.getWriter().write("Delete Unsuccessful");
				}
			}

		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}

	public void clientAccountSummary (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		if(clientAdminBO.getUserId()!=null){
			//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
			ArrayList<CustomerMobileBO> customerAccountList=CustomerMobileDAO.customerAccountSummary(clientAdminBO.getUserId(),"",1);
			/*if(request.getParameter("responseType")!=null && request.getParameter("responseType").equals("HTML5")){
				request.setAttribute("accountSummary", customerAccountList);
				request.setAttribute("screen", "/ClientUsage/accountSummary.jsp");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/accountSummary.jsp");
				dispatcher.forward(request, response);
				return;
			}*/
			JSONArray customerAccountSummary = new JSONArray();
			if(customerAccountList.size()>0){
				for(int i=0;i<customerAccountList.size();i++){
					JSONObject account = new JSONObject();
					try {
						account.put("getPaymentType", customerAccountList.get(i).getPaymentType());
						account.put("getCardNumber", customerAccountList.get(i).getCardNumber());
						account.put("getCardExpiryDate", customerAccountList.get(i).getCardExpiryDate());
						account.put("getVoucherNumber", customerAccountList.get(i).getVoucherNumber());
						account.put("getVoucherExpiryDate", customerAccountList.get(i).getVoucherExpiryDate());
						customerAccountSummary.put(account);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				response.getWriter().write(customerAccountSummary.toString());
			}else{
				if(request.getParameter("responseType").equals("HTML5")){
					//	request.setAttribute("error", "No records Found");
					//	request.setAttribute("screen", "");
					response.getWriter().write("");

					return;
				}
				JSONObject account =new JSONObject();
				try {
					account.put("message", "");
					customerAccountSummary.put(account);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				response.getWriter().write(customerAccountSummary.toString());
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}
	public void tripSummary (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		if(clientAdminBO.getUserId()!=null){
			//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
			ArrayList<CustomerMobileBO> tripList=CustomerMobileDAO.tripSummary(clientAdminBO.getUserId(),0);
			JSONArray customerTripSummary = new JSONArray();
			if(tripList.size()>0){

				for(int i=0;i<tripList.size();i++){
					JSONObject trips = new JSONObject();
					try {
						trips.put("getTripId", tripList.get(i).getTripId());
						trips.put("getPhoneNumber", tripList.get(i).getPhoneNumber());
						trips.put("getCity", tripList.get(i).getCity());
						trips.put("getAddress", tripList.get(i).getAddress());
						trips.put("getName", tripList.get(i).getName());
						customerTripSummary.put(trips);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				response.getWriter().write(customerTripSummary.toString());
			}else{
				/*JSONObject trips = new JSONObject();
				try {
					trips.put("getMessage", "No records found");
					customerTripSummary.put(trips);
				} catch (JSONException e) {
					e.printStackTrace();
				}*/
				response.getWriter().write("");
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}
	public void tripHistory (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		if(clientAdminBO.getUserId()!=null){
			//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
			ArrayList<CustomerMobileBO> tripList=CustomerMobileDAO.tripHistory(clientAdminBO.getUserId(),clientAdminBO.getAssoccode(),SystemPropertiesDAO.getParameter(clientAdminBO.getAssoccode(), "timeZoneArea"));
			JSONArray customerTripSummary = new JSONArray();
			if(tripList.size()>0){
				for(int i=0;i<tripList.size();i++){
					JSONObject trips = new JSONObject();
					try {
						trips.put("getTripId", tripList.get(i).getTripId());
						trips.put("getPhoneNumber", tripList.get(i).getPhoneNumber());
						trips.put("getCity", tripList.get(i).getCity());
						trips.put("getName", tripList.get(i).getName());
						trips.put("getAddress", tripList.get(i).getAddress());
						trips.put("stlat", tripList.get(i).getStLatitude());
						trips.put("stlon", tripList.get(i).getStLongitude());
						//System.out.println("in triphistoryCMA:lat"+tripList.get(i).getStLatitude()+"+stlon:"+tripList.get(i).getStLongitude());
						customerTripSummary.put(trips);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				response.getWriter().write(customerTripSummary.toString());
			}else{
				JSONObject trips = new JSONObject();
				try {
					trips.put("getMessage", "No records found");
					customerTripSummary.put(trips);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				/*if(request.getParameter("responseType").equalsIgnoreCase("HTML5")){
					request.setAttribute("tripSummary", tripList);
					RequestDispatcher dispatcher=request.getRequestDispatcher("/ClientUsage/tripHistory.jsp");
					dispatcher.forward(request, response);
					return;
				}*/
				response.getWriter().write("");
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}
	public void openRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=new ClientAuthenticationBO();
		if(session!=null && session.getAttribute("clientUser")!=null){
			clientAdminBO=(ClientAuthenticationBO)(session.getAttribute("clientUser"));
		}
		AdminRegistrationBO adminBo=new AdminRegistrationBO();
		OpenRequestBO OpenBO = new OpenRequestBO(); 
		adminBo.setAssociateCode("");
		int result=0;
		if(request.getParameter("latitude")!=null && request.getParameter("longitude")!=null ){
			/*request.setAttribute("latitude",request.getParameter("latitude"));
			request.setAttribute("longitude", request.getParameter("longitude"));
			request.setAttribute("address", request.getParameter("address"));*/
			if((request.getParameter("phoneNumber")==null || request.getParameter("phoneNumber").equals("") ||  request.getParameter("phoneNumber").equals("null") )&& (session.getAttribute("clientUser")!=null)){
				request.setAttribute("phoneNumber", clientAdminBO.getPhone());
			}else{
				request.setAttribute("phoneNumber",request.getParameter("phoneNumber"));
			}
			if(session.getAttribute("clientUser")!=null){
				request.setAttribute("name", clientAdminBO.getUserName());
			}else{
				request.setAttribute("name", "");
			}
			request.setAttribute("screen", "/ClientUsage/bookAJob.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/bookAJob.jsp");
			dispatcher.forward(request, response);
			return;
			//	OpenBO.setCreatedBy(clientAdminBO.getUserId());

			//	int mode= Integer.parseInt(request.getParameter("mode")==null?"0":request.getParameter("mode"));
		}else if(request.getParameter("submit")==null){
			if(session.getAttribute("clientUser")!=null){
				request.setAttribute("name", clientAdminBO.getUserName());
				request.setAttribute("phoneNumber",clientAdminBO.getPhone());
			}else{
				request.setAttribute("name", "");
				request.setAttribute("phoneNumber","");
			}
			request.setAttribute("screen", "/ClientUsage/bookAJob.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/bookAJob.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(request.getParameter("submit")!=null){
			//System.out.println("clientBBO assoc code"+clientAdminBO.getAssoccode());
			//System.out.println("request assoc code"+request.getParameter("ccode"));

			if(getServletConfig().getServletContext().getAttribute( request.getParameter("ccode") + "ZonesLoaded") == null){
				//System.out.println("Loading zone for the first time for company" +  request.getParameter("ccode"));
				SystemUtils.reloadZones(this, request.getParameter("ccode"));
			}
			OpenBO.setName(request.getParameter("userName")==null?"": request.getParameter("userName"));
			OpenBO.setSadd1(request.getParameter("sAddress1")==null?"": request.getParameter("sAddress1"));
			OpenBO.setPhone(request.getParameter("phoneNumber")==null?"": request.getParameter("phoneNumber"));
			OpenBO.setSlat(request.getParameter("sLatitude")==null?"": request.getParameter("sLatitude"));
			OpenBO.setSlong(request.getParameter("sLongitude")==null?"0.00": request.getParameter("sLongitude"));
			OpenBO.setSdate(request.getParameter("sDate")==null?"0.00": request.getParameter("sDate"));
			OpenBO.setEdlatitude(request.getParameter("eLatitude")==null?"": request.getParameter("eLatitude"));
			OpenBO.setEdlongitude(request.getParameter("eLongitude")==null?"": request.getParameter("eLongitude"));
			OpenBO.setEadd1(request.getParameter("eAddress1")==null?"": request.getParameter("eAddress1"));
			OpenBO.setAmt(new BigDecimal("0.00"));
			clientAdminBO.setTimeZone(SystemPropertiesDAO.getParameter(request.getParameter("ccode"), "timeZoneArea"));
			if(OpenBO.getSdate().equals("")){
				OpenBO.setSdate("12/31/3000");
				OpenBO.setShrs("2525");
			}else{
				if(OpenBO.getSdate().length()!=10){
					String[] date=OpenBO.getSdate().split("/");
					if(date[0].length()<2){
						date[0]="0"+date[0];
					}
					if(date[1].length()<2){
						date[1]="0"+date[1];
					}
					OpenBO.setSdate(date[0]+"/"+date[1]+"/"+date[2]);
				}
				String time=request.getParameter("time")==null?"": request.getParameter("time");
				time=time.replace(":", "");
				time=time.replace(" ", "");
				if(time.contains("PM")){
					time=time.replace("PM","");
					if(Integer.parseInt(time)>1259){
						time=Integer.parseInt(time)+1200+"";
					}
				}else{
					time=time.replace("AM", "");
					if(Integer.parseInt(time)<1259){
						//	time=Integer.parseInt(time)+1200+"";
						time=time.replace("12","00");
					}
				}
				OpenBO.setShrs(time);
			}	
			OpenBO.setChckTripStatus(8);
			OpenBO.setTypeOfRide("0");
			OpenBO.setAssociateCode(request.getParameter("ccode")==null?"": request.getParameter("ccode")); 
			if(OpenBO.getSlat()!=null && !OpenBO.getSlat().equals("0") && !OpenBO.getSlong().equals("0")  ) {
				ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) this.getServletContext().getAttribute((OpenBO.getAssociateCode() +"Zones"));
				OpenBO.setQueueno( CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(OpenBO.getSlat()),Double.parseDouble(OpenBO.getSlong())));
			}
			/*	if(openRequestBO.getEdlatitude()!=null && openRequestBO.getEdlatitude()!=""){
                 ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) srvletConfig.getServletContext().getAttribute((adminBO.getAssociateCode() +"Zones"));
                 m_queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(openRequestBO.getEdlatitude()),Double.parseDouble(openRequestBO.getEdlongitude()));
                 openRequestBO.setEndQueueno(m_queueId);
			}*/
			if(session !=null && session.getAttribute("clientUser")!=null){
				OpenBO.setCreatedBy(clientAdminBO.getUserId());
			}
			try{
				if(request.getParameter("drprofileSize")!=null && request.getParameter("drprofileSize")!=""&& Integer.parseInt(request.getParameter("drprofileSize")) > 0)
				{
					ArrayList al_l = new ArrayList();
					String drProfile="";
					for(int i=0;i<Integer.parseInt(request.getParameter("drprofileSize"));i++)
					{
						if(request.getParameter("dchk"+i)!=null){
							al_l.add(request.getParameter("dchk"+i));
							drProfile = drProfile + request.getParameter("dchk"+i);
						}
					}
					OpenBO.setAl_drList(al_l);
					OpenBO.setDrProfile(drProfile);
				}
			}catch (NumberFormatException e) {
				// TODO: handle exception
			}
			try{
				if(request.getParameter("vprofileSize")!=null && request.getParameter("vprofileSize")!=""&& Integer.parseInt(request.getParameter("vprofileSize")) > 0)
				{
					ArrayList al_l = new ArrayList();
					String drProfile="";
					for(int i=0;i<Integer.parseInt(request.getParameter("vprofileSize"));i++)
					{
						if(request.getParameter("vchk"+i)!=null){
							al_l.add(request.getParameter("vchk"+i));
							drProfile = drProfile + request.getParameter("vchk"+i);
						}
					}
					OpenBO.setAl_vecList(al_l);
					OpenBO.setVecProfile(drProfile);
				}
			}catch (NumberFormatException e) {
				// TODO: handle exception
			}
			adminBo.setMasterAssociateCode(clientAdminBO.getAssoccode());
			adminBo.setTimeZone(clientAdminBO.getTimeZone());
			//OpenBO.setMasterAssociateCode(clientAdminBO.getAssoccode());
			adminBo.setMasterAssociateCode(OpenBO.getAssociateCode());
			result=RequestDAO.saveOpenRequest(OpenBO, adminBo,0);
			if(result>=1){
				response.getWriter().write(result+"");
				return;
			}
		}

		if(result>1){
			if(request.getParameter("fromHome")!=null && request.getParameter("fromHome").equals("1")){
				if(session.getAttribute("clientUser")!=null){
					RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/HomeForLoggedIn.jsp");
					dispatcher.forward(request, response);
					return;
				}else{
					RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Home.jsp");
					dispatcher.forward(request, response);
					return;
				}
			}else{
				request.setAttribute("error","Your Trip is Booked and the Unique Key is "+result);
				request.setAttribute("screen", "/ClientUsage/bookAJob.jsp");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/bookAJob.jsp");
				dispatcher.forward(request, response);
				return;
			} 
		}else {
			request.setAttribute("error","Failed to Book");
			request.setAttribute("screen", "/ClientUsage/bookAJob.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/bookAJob.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}


	public void driverLocation (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
		if(clientAdminBO.getUserId()!=null){
			String tripId=request.getParameter("tripId")==null?"":request.getParameter("tripId");
			ArrayList<CustomerMobileBO> driverLocation=new ArrayList<CustomerMobileBO>();
			if(!tripId.equals("")){
				driverLocation=CustomerMobileDAO.getDriverLocation(clientAdminBO.getUserId(),tripId);
			}else{
				driverLocation=CustomerMobileDAO.getDriverLocation(clientAdminBO.getUserId(),"");
			}
			if(driverLocation.size()>0){
				JSONArray driverCurrentLocation = new JSONArray();
				for(int i=0;i<driverLocation.size();i++){
					JSONObject latLong = new JSONObject();
					try {
						latLong.put("getLatitude", driverLocation.get(i).getStLatitude()=="0.0"?"":driverLocation.get(i).getStLatitude());
						latLong.put("getLongitude", driverLocation.get(i).getStLongitude()=="0.0"?"":driverLocation.get(i).getStLongitude());
						driverCurrentLocation.put(latLong);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				response.getWriter().write(driverCurrentLocation.toString());
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}
	public void jobLocation (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
		if(clientAdminBO.getUserId()!=null){
			String tripId=request.getParameter("tripId");
			if(tripId !=null && !tripId.equals("")){
				ArrayList<CustomerMobileBO> jobLocation=CustomerMobileDAO.getJobLocation(clientAdminBO.getUserId(),tripId,0);
				if(jobLocation.size()>0){
					JSONArray jobsLocation = new JSONArray();
					for(int i=0;i<jobLocation.size();i++){
						JSONObject latLong = new JSONObject();
						try {
							latLong.put("getLatitude", jobLocation.get(i).getStLatitude());
							latLong.put("getLongitude", jobLocation.get(i).getStLongitude());
							latLong.put("SA", jobLocation.get(i).getStatus()==null?"":jobLocation.get(i).getStatus());
							latLong.put("TI", jobLocation.get(i).getTripId()==null?"":jobLocation.get(i).getTripId());
							latLong.put("driver", jobLocation.get(i).getDriver());
							latLong.put("cab", jobLocation.get(i).getVehicleNo());
							latLong.put("dLat", jobLocation.get(i).getLatitude());
							latLong.put("dLongi", jobLocation.get(i).getLongitude());
							latLong.put("vReg", jobLocation.get(i).getVehRegNum());
							latLong.put("vMake", jobLocation.get(i).getvMake());
							latLong.put("vType", jobLocation.get(i).getvType());
							jobsLocation.put(latLong);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					response.getWriter().write(jobsLocation.toString());
				}
			}else{
				ArrayList<CustomerMobileBO> jobLocation=CustomerMobileDAO.getJobLocation(clientAdminBO.getUserId(),"",0);
				if(jobLocation.size()>0){
					JSONArray jobsLocation = new JSONArray();
					for(int i=0;i<jobLocation.size();i++){
						JSONObject latLong = new JSONObject();
						try {
							latLong.put("getLatitude", jobLocation.get(i).getStLatitude());
							latLong.put("getLongitude", jobLocation.get(i).getStLongitude());
							latLong.put("SA", jobLocation.get(i).getStatus()==null?"":jobLocation.get(i).getStatus());
							latLong.put("TI", jobLocation.get(i).getTripId()==null?"":jobLocation.get(i).getTripId());

							jobsLocation.put(latLong);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					response.getWriter().write(jobsLocation.toString());
				}
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}


	public void userIdCheck (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
		String userId=(request.getParameter("userId")==null?"":request.getParameter("userId"));
		int result= CustomerMobileDAO.userIdCheck(userId);
		response.getWriter().write(Integer.toString(result));
	}
	public void tagAddress (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setTagAddress(request.getParameter("tagName"));
		CMBO.setAddress(request.getParameter("address"));
		CMBO.setLatitude(request.getParameter("latitude"));
		CMBO.setLongitude(request.getParameter("longitude"));
		int result=CustomerMobileDAO.tagAddress(CMBO);
		response.getWriter().write(Integer.toString(result));		
	}
	public void checkAddress (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setAddress(request.getParameter("address"));
		int result=CustomerMobileDAO.checkAddress(CMBO);
		response.getWriter().write(Integer.toString(result));		
	}
	public void taggedAddressesToShow (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		ArrayList<CustomerMobileBO> taggedAddressesList =new ArrayList<CustomerMobileBO>();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setAddress(request.getParameter("address"));
		taggedAddressesList=CustomerMobileDAO.taggedAddressestoShow(CMBO);
		JSONArray jsonArr = new JSONArray();
		for(int i=0;i<taggedAddressesList.size();i++){
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put("address",taggedAddressesList.get(i).getAddress());
				jsonObj.put("tagAdd", taggedAddressesList.get(i).getTagAddress());
				jsonObj.put("addKey", taggedAddressesList.get(i).getAddressKey());
				jsonObj.put("getLatitude", taggedAddressesList.get(i).getLatitude());
				jsonObj.put("getLongitude",taggedAddressesList.get(i).getLongitude());
				jsonArr.put(jsonObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(jsonArr.toString());
		//response.getWriter().write(Integer.toString(result));		
	}
	public void favoriteAddresses(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		ArrayList<CustomerMobileBO> taggedAddressesList =new ArrayList<CustomerMobileBO>();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setAddress(request.getParameter("address"));
		taggedAddressesList=CustomerMobileDAO.taggedAddressestoShow(CMBO);
		JSONArray jsonArr = new JSONArray();
		for(int i=0;i<taggedAddressesList.size();i++){
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put("address",taggedAddressesList.get(i).getAddress());
				jsonObj.put("tagAdd", taggedAddressesList.get(i).getTagAddress());
				jsonObj.put("addKey", taggedAddressesList.get(i).getAddressKey());
				jsonObj.put("getLatitude", taggedAddressesList.get(i).getLatitude());
				jsonObj.put("getLongitude",taggedAddressesList.get(i).getLongitude());
				jsonArr.put(jsonObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(jsonArr.toString());
	
	}
	public void deleteFavAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String key=request.getParameter("addKey");
		int result=CustomerMobileDAO.deleteFavAddress(key,clientAdminBO);
		response.getWriter().write(result+"");
	}

	public void charges(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String tripId=request.getParameter("tripId");
		System.out.println("tripId--->"+tripId);
		ArrayList<CustomerMobileBO>charges=CustomerMobileDAO.getCharges(tripId, clientAdminBO.getAssoccode());
		if(charges.size()>0){
			JSONArray chargesArray = new JSONArray();
			for(int i=0;i<charges.size();i++){
				JSONObject chargesObject = new JSONObject();
				try {
					chargesObject.put("getType", charges.get(i).getPaymentType());
					chargesObject.put("getAmount", charges.get(i).getAmount());
					chargesObject.put("getChargeType", charges.get(i).getChargesTypeCode());
					chargesArray.put(chargesObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			response.getWriter().write(chargesArray.toString());
		}
	}
	public void payFare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String tripId=request.getParameter("tripId");
		int result=CustomerMobileDAO.updateCharges( tripId, clientAdminBO.getAssoccode(),clientAdminBO.getAssoccode());
		response.getWriter().write(result+"");
	}
	public void showCards(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		//System.out.println("Baskar checking event");
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		ArrayList<CustomerMobileBO> cardArray=CustomerMobileDAO.getCreditCards(clientAdminBO.getUserId());
		if(cardArray.size()>0){
			JSONArray chargesArray = new JSONArray();
			for(int i=0;i<cardArray.size();i++){
				JSONObject chargesObject = new JSONObject();
				try {
					chargesObject.put("getCard", cardArray.get(i).getCardNumber());
					chargesObject.put("getNumber", cardArray.get(i).getSerialNo());
					chargesArray.put(chargesObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			response.getWriter().write(chargesArray.toString());

		}
	}
	public void getPushID(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		String pushKey="";
		pushKey=TDSProperties.getValue("GCMPID");
		response.getWriter().write("ResVer=1.1;GPId="+pushKey);	
		String register=request.getParameter("register");
		HttpSession session = request.getSession(false);
		if(register!=null){
			ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
			pushKey=request.getParameter("pushKey");
			CustomerMobileDAO.updateGCMKey(clientAdminBO.getUserId(), pushKey);
		}
		/*	ArrayList<String> custList= new ArrayList<String>();
		custList.add(pushKey);
		GCMPush2.sendMessage(getServletContext(), pushKey, "Trip Ended;tripId=19120",custList);*/
		//response.getWriter().write(Integer.toString(result));		
	}
	public void cancelTrip(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String tripId=request.getParameter("tripId");
		int result=CustomerMobileDAO.cancelReservation(clientAdminBO.getUserId(),tripId,0);
		response.getWriter().write(result+"");
	}
	public void changeStatus (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		boolean status=false;
		if(request.getParameter("status").equals("1")){
			status=true;
		}
		int result=CustomerMobileDAO.updateJobByPassenger(clientAdminBO.getAssoccode(), request.getParameter("tripId"), status);
		response.getWriter().write(result+"");		
	}
	public void deleteJobSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		int result=CustomerMobileDAO.deleteOpenRequest(request.getParameter("tripId"), clientAdminBO.getAssoccode(), "1");
		response.getWriter().write(result+"");		
	}
	public void getFlagsForCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		ArrayList<DriverVehicleBean> dvList=new ArrayList<DriverVehicleBean>();
		HashMap<String, DriverVehicleBean> companyFlags =UtilityDAO.getCmpyVehicleFlag(request.getParameter("compCode"));
		Set hashMapEntries = companyFlags.entrySet();
		Iterator it = hashMapEntries.iterator();
		while(it.hasNext()){
			Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
			dvList.add(companyFlag.getValue());
		}
		JSONArray flagList = new JSONArray();
		for(int i=0;i<dvList.size();i++){
			JSONObject flagDetails = new JSONObject();
			try {
				flagDetails.put("LD", dvList.get(i).getLongDesc());
				flagDetails.put("SW", dvList.get(i).getDriverVehicleSW());
				flagDetails.put("K",dvList.get(i).getKey());
				flagDetails.put("SD", dvList.get(i).getShortDesc());
				flagDetails.put("GI", dvList.get(i).getGroupId()==0?"":dvList.get(i).getGroupId());
				flagList.put(flagDetails);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.getWriter().write(flagList.toString());
	}
	public void driverList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		ArrayList<DriverCabQueueBean> driverBO = DispatchDAO.allDrivers(request.getParameter("assoccode"), 0.00, 0.00, 0.00, null, "");
		ArrayList<String> fleets = new ArrayList<String>();
		fleets.add(request.getParameter("assoccode"));

		ArrayList<DriverCabQueueBean> driverBO = DispatchDAO.allDriversWithInADistance(fleets, Double.parseDouble(request.getParameter("sLat").equals("")?"0.00":request.getParameter("sLat")), Double.parseDouble(request.getParameter("sLong").equals("")?"0.00":request.getParameter("sLong")), 10.00, null, 1,0);
		JSONArray customerAccountRegistration= new JSONArray();
		for (int i = 0; i < driverBO.size(); i++) {
			JSONObject registrationStatus = new JSONObject();
			try {
				registrationStatus.put("getLatitude",driverBO.get(i).getCurrentLatitude());
				registrationStatus.put("getLongitude",driverBO.get(i).getCurrentLongitude());
				registrationStatus.put("getCabNo", driverBO.get(i).getVehicleNo());
				registrationStatus.put("DR", driverBO.get(i).getDriverid());
				customerAccountRegistration.put(registrationStatus);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.setContentType("application/json");
		response.getWriter().write(customerAccountRegistration.toString());	
	}
	public void getDriverDocument(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		TDSConnection dbcon = null;
		Connection con = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		CustomerMobileBO cmbo=new CustomerMobileBO();
		Blob blobData = null;
		String fileName = "";
		String query = "";
		String driverId = request.getParameter("driverId") != null ? request.getParameter("driverId") : "";
		dbcon = new TDSConnection();
		con = dbcon.getConnection();
		//CustomerMobileBO _getDriverImage & _SetDriverImage
		try {
			if (!driverId.equals("")) {
				query = "SELECT DOC_DATA,DOCUMENT_NAME FROM TDS_DOCUMENTS WHERE DOC_TYPE = 'Driver Photo' AND DOC_UPLOADED_BY='"+driverId+"'";
				pst = con.prepareStatement(query);
				rs = pst.executeQuery();
				while (rs.next()) {
					blobData = rs.getBlob(1);
					fileName = rs.getString(2);
				}
				response.setContentType(getServletContext().getMimeType(fileName));
				OutputStream outStream = response.getOutputStream();
				InputStream in = blobData.getBinaryStream();
				int nextByte = in.read();
				while (nextByte != -1) {
					outStream.write(nextByte);
					nextByte = in.read();
				}
				outStream.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			dbcon.closeConnection();
		}
	}
}