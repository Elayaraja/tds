package com.tds.action;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.controller.TDSController;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.VoiceMessageDAO;
import com.common.util.Messaging;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.common.util.MessageGenerateJSON;
import com.common.util.PasswordHash;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;

/**
 * Servlet implementation class SystemSetup
 */
public class VoiceMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Category cat = TDSController.cat;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		cat = Category.getInstance(VoiceMessage.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ VoiceMessage.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VoiceMessage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException 
	{
		String eventParam = "";
		
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if(eventParam.equalsIgnoreCase("loginGACRadio")){
			loginGACRadio(request,response);
		}else{
		if(request.getSession(false) != null){
			if(request.getSession().getAttribute("user")==null){
				return;
			} 
		} else {
			
			return;
			}
		}
		
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}

		//System.out.println("Event Param "+ eventParam);

		//HttpSession session = request.getSession();
		
		//if(session.getAttribute("user") != null) {
			
			if(eventParam.equalsIgnoreCase("UploadVoice")) {			 
				uploadVoiceFromOperator(request,response);
			} else if(eventParam.equalsIgnoreCase("DownloadVoice")) {			 
				downloadVoice(request,response);
			}  else if(eventParam.equalsIgnoreCase("UploadVoiceDriver")) {			 
				uploadVoiceFromDriver(request,response);
			}  else if(eventParam.equalsIgnoreCase("getMessageIDToBePlayed")) {			 
				getMessageIDToBePlayed(request,response);
			} else if(eventParam.equalsIgnoreCase("PlayMessage")){
				getMessage(request,response);
			}	else if(eventParam.equalsIgnoreCase("mobileLogoutForGACRadio")){
				mobileLogoutForGACRadio(request,response);
			}



		//}
	}
	public void uploadVoiceFromOperator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		long beginTime = System.currentTimeMillis();
		//System.out.println("START OF VM UPLOAD:"+ System.currentTimeMillis());

		//String action = request.getParameter("action");
		//String event = request.getParameter("event");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		int size = request.getContentLength();
		//System.out.println("Length of content from operator:"+ size);
		//byte[] bytes = new byte[size];
		byte[] bytestemp = new byte[10240];
		byte[] totalVoiceFile = new byte[size];
		//String driverId="";
		String[] driverIDsForMessage = null;
		String driverID = request.getParameter("driverid");
		if(driverID!=null && driverID.equals("")){
			driverID = null;
		}
		int pendingMessageKey = VoiceMessageDAO.insertPendingVoiceMessage(adminBO.getMasterAssociateCode(), 2,driverID);

		if(driverID!=null&& !driverID.equals("")){
			//driverId=request.getParameter("driverid");
			driverIDsForMessage = driverID.split(";");
		} 
		long msgID = System.currentTimeMillis();
		String[] message = MessageGenerateJSON.generateVoicePushMessage(pendingMessageKey+"", msgID);
		ArrayList<DriverCabQueueBean> driverDetails = UtilityDAO.getDriverListForMessaging(adminBO.getMasterAssociateCode(), driverIDsForMessage, 0, true);
		if(driverDetails!=null && driverDetails.size()>0){
			Messaging sendMessage = new Messaging(getServletContext(), driverDetails, message, poolBO, adminBO.getAssociateCode());
			sendMessage.start();
		}
		//System.out.println("TimeBetween entry and vsending vm"+(System.currentTimeMillis()-beginTime));

		ByteArrayOutputStream tempByteArray = new ByteArrayOutputStream();
		ByteArrayOutputStream processedByteArray = null;
		Process input = null;
		InputStream servletInput = request.getInputStream();
		long firstFileName = System.currentTimeMillis();
		FileOutputStream os = new FileOutputStream("/usr/local/vp/"+firstFileName+".mp3");

		int readInt=0;
		long beginTimeForUpload = System.currentTimeMillis();
		while (-1 != (readInt =servletInput.read(bytestemp,0,bytestemp.length))) {
			//System.out.println(readInt+":"+System.currentTimeMillis());
			tempByteArray.write(bytestemp,0,readInt);
		}
		//System.out.println("Time To extract voice is "+(System.currentTimeMillis()-beginTimeForUpload));
		os.write(tempByteArray.toByteArray());
		//System.out.println("Time To write voice file is "+(System.currentTimeMillis()-beginTimeForUpload));
		//int messageKey = VoiceMessageDAO.insertVoiceMessage("103", driverId, tempByteArray.toByteArray(),2);
		//Inform all drivers of the new message
		
		//long msgID = System.currentTimeMillis();
		//String[] message = MessageGenerate.generateVoicePushMessage(messageKey+"", msgID);
		//ArrayList<DriverCabQueueBean> driverDetails = UtilityDAO.getDriverListForMessaging("103", driverIDsForMessage, 0, true);
		//Messaging sendMessage = new Messaging(getServletContext(), driverDetails, message, poolBO, "103");
		//sendMessage.run();
		//processedByteArray.
		boolean compressionStatus = false;
		try{
			long conversionBeing = System.currentTimeMillis();
			String[] parms = {"/usr/local/vp/processVoiceFile.sh", firstFileName+"", (firstFileName+1)+""};
			 input = Runtime.getRuntime().exec(parms);
			 input.waitFor();
			//System.out.println("Time To convert is "+(System.currentTimeMillis()-conversionBeing));

			 if(input.exitValue()==0){
					File inputFile = new File("/usr/local/vp/"+(firstFileName+1)+".mp3");
					FileInputStream fis = new FileInputStream(inputFile);
					processedByteArray = new ByteArrayOutputStream();
					 byte[] buf = new byte[2048];
					 for (int readNum; (readNum = fis.read(buf)) != -1;) {
						 processedByteArray.write(buf, 0, readNum);
					 }
					//System.out.println("Time To read file is "+(System.currentTimeMillis()-conversionBeing));

					 compressionStatus = true;
					 System.out.println("Done");
			 } else {
					BufferedReader i = new BufferedReader(new InputStreamReader(input.getErrorStream()));

					//System.out.println(i.readLine());
					//System.out.println(i.readLine());
					//System.out.println(i.readLine());
					
					BufferedReader j = new BufferedReader(new InputStreamReader(input.getInputStream()));
					//System.out.println(j.readLine());
					//System.out.println(j.readLine());

					//System.out.println("finish in"+(System.currentTimeMillis()-time1));

			 }
		} catch (Exception e){
			System.out.println("Exception"+e.toString());
		}
		int messageKey = 0;
		if(compressionStatus){
			messageKey = VoiceMessageDAO.updateVoiceMessageByKey(adminBO.getMasterAssociateCode(), driverID, processedByteArray.toByteArray(),2,pendingMessageKey);
		} else {
			messageKey = VoiceMessageDAO.updateVoiceMessageByKey(adminBO.getMasterAssociateCode(), driverID, tempByteArray.toByteArray(),2,pendingMessageKey);
		}
		//Inform all drivers of the new message
		
		//System.out.println("END OF UPLOAD:"+ System.currentTimeMillis());
		
		
}
	public void uploadVoiceFromDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		//String action = request.getParameter("action");
		//String event = request.getParameter("event");
		//String driverID = request.getParameter("driverID");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		int size = request.getContentLength();
		//System.out.println("Request size =" +size);
		byte[] bytes = new byte[size];
		//byte[] bytestemp = new byte[8172];
		//ByteArrayOutputStream tempByteArray = new ByteArrayOutputStream();
		//InputStream servletInput = request.getInputStream();
		BufferedReader reader1 = request.getReader();
		//FileOutputStream os = new FileOutputStream("/HareRama.wav");

		//reader1.read(bytes);
		//int readInt=0;
		//int totalBytes = 0;

		//reader1.read(bytes);
		
		//while (reader1.read(bytestemp)!=-1) {
		//	tempByteArray.write(bytestemp);
			//totalBytes = totalBytes+readInt;
			//os.write(bytestemp);

		//}
		//System.out.println("B-"+System.currentTimeMillis());
	    int a = 0, counter = 0;
	    while((a = reader1.read()) != -1){
		    bytes[counter] = (byte)a;
		    counter++;
	    }
		//System.out.println("E-"+System.currentTimeMillis());

		//System.out.println( adminBO.getUid()+"Total file size =" +bytes.length);
		String driverIDForDisplay = "";
		if(adminBO.getDispatchBasedOnVehicleOrDriver()==2){
			//driverIDForDisplay = VoiceMessageDAO.getCabNumber(adminBO.getAssociateCode(), adminBO.getUid());
			driverIDForDisplay = " Cab #"+adminBO.getVehicleNo();
			if(driverIDForDisplay.equals("")){
				driverIDForDisplay = adminBO.getUid();				
			}
		} else{
			driverIDForDisplay = adminBO.getUid();
		}
			
		int messageKey = VoiceMessageDAO.insertVoiceMessage(adminBO.getMasterAssociateCode(), driverIDForDisplay, bytes,1);
		//Inform all drivers of the new message
		
		//long msgID = System.currentTimeMillis();
		//String message = MessageGenerate.generateVoicePushMessage(messageKey+"", msgID);
		//ArrayList<DriverCabQueueBean> driverDetails = UtilityDAO.getDriverListForMessaging(adminBO.getAssociateCode(), null, 0, true);
		//Messaging sendMessage = new Messaging(getServletContext(), driverDetails, message, poolBO);
		//sendMessage.run();
		
		
}

	protected void getMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("start of download1:"+ System.currentTimeMillis());
		String action = request.getParameter("action");
		//String event = request.getParameter("event");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession(false).getAttribute("user");
		if (adminBO ==null){
			return;
		}
		String messageKey = request.getParameter("msgKey");


		//System.out.println("About to download voice message"+messageKey);

		byte[] vm = VoiceMessageDAO.getVoiceMessageOperator(adminBO.getMasterAssociateCode(),"", messageKey);
		
		if(vm != null){
			//System.out.println("VMSize"+ vm.length);
			ServletOutputStream out = response.getOutputStream();
		    response.setContentType("audio/mpeg");
		    response.addHeader("Content-Disposition", "attachment; filename="+ messageKey+".mp3");
		    response.setContentLength((int) vm.length);
		    //while ((int readBytes = vm.read()) != -1)
		    //    stream.write(readBytes);	
			out.write(vm);
			//System.out.println("VMWrittenout");
		}
		//System.out.println("END of download1:"+ System.currentTimeMillis());
	}

	public void downloadVoice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("start of download2:"+ System.currentTimeMillis());

		//String action = request.getParameter("action");
		//String event = request.getParameter("event");
		
		String messageKey = request.getParameter("msgKey");

		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		//ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		String driverID = adminBO.getUid();
		//System.out.println("About to download voice message"+messageKey+":"+driverID);

		byte[] vm = VoiceMessageDAO.getVoiceMessage(adminBO.getMasterAssociateCode(),driverID, messageKey);
		int loop = 0;
		if(vm != null){
			while(vm.length<8){
				loop++;
				if(loop>7){
					return;
				}
				//System.out.println("WAITING FOR VOICE MESSAGE UPLOAD FOR MSGKEY:"+messageKey);
				vm = VoiceMessageDAO.getVoiceMessage(adminBO.getMasterAssociateCode(),driverID, messageKey);
				try{
					Thread.sleep(500);
					
				} catch(Exception e){
					//System.out.println("Error waiting for voice message");
					loop=20;
					return;
				}
			}
			//System.out.println("VMSize"+ vm.length);
			ServletOutputStream out = response.getOutputStream();
		    response.setContentType("audio/mpeg");
		    response.addHeader("Content-Disposition", "attachment; filename="+ messageKey);
		    response.setContentLength((int) vm.length);
		    //while ((int readBytes = vm.read()) != -1)
		    //    stream.write(readBytes);
			out.write(vm);
			//System.out.println("VMWrittenout");
		}
		//System.out.println("end of download2:"+ System.currentTimeMillis());
		
}
	public void sample(){
	//Mp3LameFormatConversionProvider mp3convertor = new Mp3LameFormatConversionProvider();
	
	//mp3convertor.
	}
	
	public void getMessageIDToBePlayed(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//System.out.println("Inside post");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String drivers = request.getParameter("driverid");
		ArrayList<String> messages =null;
		if(drivers==null){
			messages = VoiceMessageDAO.getNewMessageIDs(adminBO.getMasterAssociateCode(),adminBO.getUid(), new String[0]);			
		} else {
			String[] driverList = drivers.split(";");
			messages = VoiceMessageDAO.getNewMessageIDs(adminBO.getMasterAssociateCode(),adminBO.getUid(), driverList);
		}
		StringBuffer buffer = new StringBuffer();
		String[] messageIDs= null;
		if(messages.size()>0){
			messageIDs = new String[messages.size()/2];
			for(int i=0;i<messages.size()-1;i=i+2){
				buffer.append("MK="+(String)messages.get(i)+";DID="+(String)messages.get(i+1)+"^");
				messageIDs[i/2]=(String)messages.get(i);
			}
			VoiceMessageDAO.updateMessageAsRead(adminBO.getMasterAssociateCode(),adminBO.getUid(), messageIDs);
		} else{
			buffer.append("No Messages");
		}
			
		response.getWriter().write(buffer.toString());
}
	
	public void loginGACRadio(HttpServletRequest request, HttpServletResponse response){
		String m_userName = "";
		String m_password = "";
		String m_passwordHash = "";
		String version="";
		String reason="";
		String ipAddress=request.getRemoteAddr();
		HttpSession m_session = null;
		AdminRegistrationBO m_adAdminRegistrationBO = null;
		if(request.getParameter("loginName") != null) {
			m_userName = request.getParameter("loginName");
		}
		if(request.getParameter("password") != null) {
			m_password = request.getParameter("password");
			try{
			m_passwordHash=PasswordHash.encrypt(m_password);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		if(request.getParameter("version")!=null) {
			version = request.getParameter("version");
		}
		if(m_userName != "" && m_password != "") { 
			try{
			m_adAdminRegistrationBO = RegistrationDAO.getUserAvailable(m_userName, m_passwordHash,"","","");
			}catch(Exception e){
				e.printStackTrace();
			}
			if(m_adAdminRegistrationBO.getRoles().contains("7010")){
			m_session = request.getSession();
			RegistrationDAO.InsertLoginDetail(m_adAdminRegistrationBO.getAssociateCode(),m_adAdminRegistrationBO.getUid(), m_session.getId(),"1",0,System.currentTimeMillis(),m_adAdminRegistrationBO.getProvider(),m_adAdminRegistrationBO.getPhnumber(),version,m_adAdminRegistrationBO.getDriver_flag(),"", "",ipAddress,m_adAdminRegistrationBO.getMasterAssociateCode(),"");
			cat.info("Session Creation Time "+ m_session.getCreationTime());
			m_session.setAttribute("user", m_adAdminRegistrationBO);
			if(m_adAdminRegistrationBO.getIsAvailable() == 1) {
				try{
				response.getWriter().write("ResVer=1.1;Login=Valid;"+"SessionID="+m_session.getId()+";GPID="+TDSProperties.getValue("GCMPID"));
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			}else{
				reason="You doesn't Have access to GACRadio";
				try{
					response.getWriter().write("ResVer=1.1;Login=InValid; You doesn't have access to GACRadio");
				}catch(Exception e){
					e.printStackTrace();
				}

			}
		} else{
			reason=reason+"Enter UserName And Password";
			//System.out.println("reason--->"+reason);
			cat.info("Reason--->"+reason);
			try{
			response.getWriter().write("ResVer=1.1;Login=InValid;");
			}catch(Exception e){
				e.printStackTrace();
			}
			RegistrationDAO.InsertLoginAttempts(m_adAdminRegistrationBO.getUid(), m_passwordHash, ipAddress, reason);
		}
	}
	public void mobileLogoutForGACRadio(HttpServletRequest p_request,
			HttpServletResponse p_response) throws ServletException,
			IOException {
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext()
		.getAttribute("poolBO");
		HttpSession m_session = p_request.getSession();
		// Map m_queueMap = null;
		AdminRegistrationBO m_adminBO = null;
		String m_driverId = "";
		String m_queueId = "";
		int m_position = 0;
		cat.info("In User Logoed out event ");
		if (m_session.getAttribute("user") != null) {
			String m_session_id = m_session.getId();
			m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
			RegistrationDAO.InsertLoginDetail(m_adminBO.getAssociateCode(),m_adminBO.getUname(), m_session_id, "0", 0,System.currentTimeMillis(), m_adminBO.getProvider(),m_adminBO.getPhnumber(), "", "", "", "","",m_adminBO.getMasterAssociateCode(),"");
			m_driverId = m_adminBO.getUname();
			//System.out.println("Session Data " + m_session.getAttribute("user"));
			m_session.removeAttribute("user");
			m_session.invalidate();
		}
		p_response.getWriter().print("ResVer=1.1;Status=LoggedOut");
		// getServletContext().getRequestDispatcher(TDSConstants.mLogoutJSP).forward(p_request,
	}
}
