package com.tds.action;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.tdsBO.AdminRegistrationBO;

public class PingServelet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final Category cat = TDSController.cat;
	
	
	@Override
	public void init(ServletConfig config) throws ServletException {
//		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//		cat.info("Class Name "+ ServiceRequestAction.class.getName());
//		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	
	
	@Override
	public void doGet(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException 
	{
		HttpSession m_session = p_request.getSession(false);

		if(m_session != null && (AdminRegistrationBO)m_session.getAttribute("user")!=null)
		{
			p_response.getWriter().write("Alive");
		} else {
			p_response.getWriter().write("Your Session Expired");
		}
		 
		//p_response.setContentType("text");
		
	}

}
