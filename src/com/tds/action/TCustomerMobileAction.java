package com.tds.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.constant.ICCConstant;
import com.charges.dao.ChargesDAO;
import com.common.action.PaymentProcessingAction;
import com.tds.cmp.bean.CompanySystemProperties;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverVehicleBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.SystemUnavailableException;
import com.tds.dao.AuditDAO;
import com.tds.dao.CreditCardDAO;
import com.tds.dao.CustomerMobileDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.ZoneDAO;
import com.tds.ivr.IVRDriverPassengerConf;
import com.tds.payment.PaymentCatgories;
import com.tds.payment.PaymentGateway;
import com.tds.payment.PaymentGatewaySlimCD;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.process.ClosestDriver;
import com.common.util.CheckZone;
import com.common.util.Messaging;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.ClientAuthenticationBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.CreditCardBO;
import com.tds.tdsBO.CustomerMobileBO;
import com.tds.tdsBO.DispatchPropertiesBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.common.util.DistanceCalculation;
import com.common.util.MessageGenerateJSON;
import com.common.util.PasswordGen;
import com.common.util.PasswordHash;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.common.util.sendSMS;
import com.gac.wasl.TripRegistration;
import com.gac.wasl.WaslIntegrationBO;
import com.twilio.sdk.TwilioRestException;

/**
 * Servlet implementation class CustomerMobileAction
 */
public class TCustomerMobileAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TCustomerMobileAction() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException, SystemUnavailableException {
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		//long timeAtBegin = System.currentTimeMillis();
		//System.out.println("CustomerMobileAction Servlet");
		String eventParam = "";
		HttpSession session = request.getSession(false);

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
			//System.out.println("Event"+eventParam);
		}

		/*ClientAuthenticationBO cABoTest=(ClientAuthenticationBO)session.getAttribute("clientUser");
		if(cABoTest==null){
			if(!eventParam.equalsIgnoreCase("clientLogin")){

			}
		}*/

		if(eventParam.equalsIgnoreCase("gPI")){
			getPushID(request,response);
			return;
		}else if(eventParam.equalsIgnoreCase("charges")){
			charges(request,response);
		}else if(eventParam.equalsIgnoreCase("payFare")){
			payFare(request,response);
		}else if(eventParam.equalsIgnoreCase("showCards")){
			showCards(request, response);
		}else if(eventParam.equalsIgnoreCase("reconnect")){
			autoReconnect(request,response);	
			return;
		}else if(eventParam.equalsIgnoreCase("GuestAutoReconnect")){
			guestAutoReconnect(request,response);	
			return;
		}

		//System.out.println("Event Param "+m_eventParam);
		//HttpSession session = request.getSession(false);
		/*	if(session !=null && session.getAttribute("user") != null && !eventParam.equalsIgnoreCase(TDSConstants.mobileLoginService)) {
			// Comment:: Checks to see if an operator has forcibly logged this driver out. If they have, the system forces a logout on the driver
			//           and removes that attribute from application pool.
			AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
		}*/

		if(eventParam.equalsIgnoreCase("clientLogin")) {
			clientLogin(request, response);
		}else if(eventParam.equalsIgnoreCase("GuestClientLogin")){
			guestClientLogin(request,response);
		}else if(eventParam.equalsIgnoreCase("getPasswordKey")){
			getPasswordKey(request,response);
		}else if(eventParam.equalsIgnoreCase("submitPasswordKey")){
			submitPasswordKey(request,response);
		}else if(eventParam.equalsIgnoreCase("changePassword")){
			changePassword(request,response);
		}else if(eventParam.equalsIgnoreCase("clientRegistration")){
			clientRegistration(request,response);			
		}else if(eventParam.equalsIgnoreCase("userIdCheck")){
			userIdCheck(request,response);
		}else if(eventParam.equalsIgnoreCase("GVK")){
			sendVerificationKey(request,response);
		}else if(eventParam.equalsIgnoreCase("SVK")){
			submitVerificationKey(request, response);
		}else if(eventParam.equalsIgnoreCase("getFlagsForCompany")){
			try {
				getFlagsForCompany(request,response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(session!=null&&session.getAttribute("clientUser")!=null ){

			if(eventParam.equalsIgnoreCase("driverLocation")){
				driverLocation(request,response);
			}else if(eventParam.equalsIgnoreCase("tripHistory")){
				tripHistory(request,response);
			}else if(eventParam.equalsIgnoreCase("tripSummary")){
				tripSummary(request,response);	
			}else if(eventParam.equalsIgnoreCase("clientAccountSummary")){
				clientAccountSummary(request,response);
			}else if(eventParam.equalsIgnoreCase("clientAccountRegistration")){
				clientAccountRegistration(request,response);	
			}else if(eventParam.equalsIgnoreCase("jobLocation")){
				jobLocation(request,response);
			}else if(eventParam.equalsIgnoreCase("tagAddress")){
				tagAddress(request,response);
			}else if(eventParam.equalsIgnoreCase("checkAddress")){
				checkAddress(request,response);
			}else if(eventParam.equalsIgnoreCase("taggedAddressestoShow")){
				taggedAddressesToShow(request,response);
			}else if(eventParam.equalsIgnoreCase("favouriteAddresses")){
				favoriteAddresses(request,response);
			}else if(eventParam.equalsIgnoreCase("cancelTrip")){
				cancelTrip(request,response);
			}else if(eventParam.equalsIgnoreCase("deleteFavAddress")){
				deleteFavAddress(request,response);
			}else if(eventParam.equalsIgnoreCase("changeStatus")){
				changeStatus(request,response);
			}else if (eventParam.equalsIgnoreCase("driverList")) {
				driverList(request, response);
			}else if (eventParam.equalsIgnoreCase("shortestDriver")) {
				shortestDriver(request, response);
			}else if(eventParam.equalsIgnoreCase("getDriverDocument")){
				getDriverDocument(request,response);
			}else if(eventParam.equalsIgnoreCase("ugregkey")){
				updateUserRegKey(request,response);
			}else if(eventParam.equalsIgnoreCase("driverrating")){
				driverRating(request,response);
			}else if(eventParam.equalsIgnoreCase("tripamount")){
				tripAmount(request,response);
			}else if(eventParam.equalsIgnoreCase("submitTiptoDriver")){
				submitTiptoDriver(request,response);
			}else if(eventParam.equalsIgnoreCase("changeOldPassword")){
				changeOldpassword(request,response);
			}else if(eventParam.equalsIgnoreCase("openRequest")){
				openRequest(request,response);	
			}else if(eventParam.equalsIgnoreCase("getPaymentdetails")){
				getPaymentdetails(request,response);
			}else if(eventParam.equalsIgnoreCase("settlepayment")){
				alterPayment(request,response);
			}else if(eventParam.equalsIgnoreCase("addCC")){
				addCreditCard(request,response);
			}else if(eventParam.equalsIgnoreCase("calldriver")){
				callDriver(request,response);
			}else if(eventParam.equalsIgnoreCase("getFareEstimation")){
				getFareEstimation(request,response);
			}else if(eventParam.equalsIgnoreCase("editProfile")){
				editProfile(request,response);
			}
		}else{
			clientLogin(request, response);
			return;
		}
		//System.out.println("Event="+eventParam+":"+(System.currentTimeMillis()-timeAtBegin));
	}


	private void editProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String uid = request.getParameter("userId");
		String pswd =  PasswordHash.encrypt(request.getParameter("password")==null?"":request.getParameter("password"));
		int app_Sr = 0;
		if(request.getParameter("AppSrc")!=null){
			if(request.getParameter("AppSrc").equalsIgnoreCase("A")){
				app_Sr = 4;
			}else{
				app_Sr = 0;
			}				
		}else{
			app_Sr = 0;
		}
		String name = request.getParameter("name")==null?"":request.getParameter("name");
		String ph_NO = request.getParameter("ph")==null?"":request.getParameter("ph");
		String mail = request.getParameter("eMail")==null?"":request.getParameter("eMail");

		int result = CustomerMobileDAO.updateCustomerProfile(uid, pswd, ph_NO, app_Sr, mail, name, clientAdminBO.getAssoccode());
		JSONArray array = new JSONArray();
		try{
			JSONObject jobj = new JSONObject();
			if(result!=0){
				jobj.put("Act", "editProfile");
				jobj.put("status", 200);
				jobj.put("Message", "success");

			}else{
				jobj.put("Act", "editProfile");
				jobj.put("status", 300);
				jobj.put("Message", "failure");

			}
			array.put(jobj);
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	public void alterPayment(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {

		PaymentProcessBean processBean = new PaymentProcessBean();
		PaymentProcessBean checkBean = new PaymentProcessBean();
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		HttpSession session = p_request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");

		String tripId=p_request.getParameter("tripId");

		int size = p_request.getContentLength();
		BufferedReader reader1 = p_request.getReader();
		byte[] bytes = new byte[size];
		int a = 0, counter = 0;
		while ((a = reader1.read()) != -1) {
			bytes[counter] = (byte) a;
			counter++;
		}

		//byte[] bytes = p_request.getAttribute("rider_sign") == null ? new byte[0] : (byte[]) p_request.getAttribute("rider_sign");
		processBean.setB_amount(p_request.getParameter("amt") == null ? "0" : p_request.getParameter("amt"));
		processBean.setB_trans_id(p_request.getParameter("tranid") == null ? "" : p_request.getParameter("tranid"));
		processBean.setB_tip_amt(p_request.getParameter("tip") == null ? "0" : p_request.getParameter("tip"));
		//processBean.setMerchid(((AdminRegistrationBO) p_request.getSession().getAttribute("user")).getMerchid());
		String transId = processBean.getB_trans_id();
		//processBean.setServiceid(((AdminRegistrationBO) p_request.getSession().getAttribute("user")).getServiceid());

		JSONArray array = new JSONArray();
		JSONObject details = new JSONObject();
		try {
			/*checkBean = ChargesDAO.getCCStatusForTripIdBeforeAlter(clientAdminBO, tripId, transId);
			if(checkBean.getAuthCapFlag().equalsIgnoreCase("2")){
				p_response.getWriter().write(array.toString());
				return;
			}*/
			processBean = PaymentProcessingAction.capturePayment(poolBO, processBean, clientAdminBO.getAssoccode());
			if (processBean.getB_approval_status().equalsIgnoreCase("1")) {
				Double total = (Double.parseDouble(processBean.getB_tip_amt())+Double.parseDouble(processBean.getB_amount()));
				ChargesDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), transId, processBean.getB_trans_id2(), processBean.getB_cap_trans(), "", bytes, ((ClientAuthenticationBO) p_request.getSession().getAttribute("clientUser")).getUserId());
				CustomerMobileDAO.setTotalPaymentAmountToORH(tripId,total+"",clientAdminBO.getAssoccode());
				details.put("Act", "AlterPayment");
				details.put("status", 200);
				details.put("TXN_STATUS", "APPROVED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
				array.put(details);
			} else if (processBean.getB_approval_status().equalsIgnoreCase("2")) {
				details.put("Act", "AlterPayment");
				details.put("status", 300);
				details.put("TXN_STATUS", "DECLINED");
				details.put("TXNID", (processBean.getB_trans_id().equals("") ? "0" : processBean.getB_trans_id()));
				details.put("APPROVALCODE", (processBean.getB_approval().equals("") ? "0" : processBean.getB_approval()));
				array.put(details);
			} else {
				details.put("Act", "AlterPayment");
				details.put("status", 400);
				details.put("TXN_STATUS", "FAILURE");
				details.put("MESSAGE", "XXXXX");
				array.put(details);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println("output"+array.toString());
		p_response.getWriter().write(array.toString());
	}

	private void getPaymentdetails(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String tripID = request.getParameter("tripID");
		String cCode = request.getParameter("assoccode");
		PaymentProcessBean processBean = ChargesDAO.getCCDetailsToCustomer(clientAdminBO, tripID);
		JSONArray array = new JSONArray();
		try{
			JSONObject jobj = new JSONObject();
			if(processBean!=null){
				jobj.put("Act", "GetPaymentdetails");
				jobj.put("status", 200);
				jobj.put("desc", "success");
				jobj.put("TXNID", processBean.getB_trans_id());
				jobj.put("TXNNO", processBean.getB_cap_trans());
				jobj.put("AMOUNT", processBean.getB_amount());
			}else{
				jobj.put("Act", "GetPaymentdetails");
				jobj.put("status", 300);
				jobj.put("desc", "failure");
			}
			array.put(jobj);
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	private void submitTiptoDriver(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub

	}


	private void changeOldpassword(HttpServletRequest request, HttpServletResponse response) throws SystemUnavailableException, IOException {
		// TODO Auto-generated method stub

		String oldPassword = PasswordHash.encrypt(request.getParameter("oldPassword")==null?"":request.getParameter("oldPassword"));
		String newPassword = PasswordHash.encrypt(request.getParameter("newpassword")==null?"":request.getParameter("newpassword"));

		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CustomerMobileBO CMBO=new CustomerMobileBO();

		CMBO.setPassword(oldPassword);
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setAssoccode(clientAdminBO.getAssoccode());

		int result =0;
		JSONArray array = new JSONArray();

		try{
			if(!oldPassword.equals("") && !newPassword.equals("")){
				CustomerMobileBO cMobileBO =new CustomerMobileBO();
				cMobileBO=CustomerMobileDAO.customerLogin(CMBO);
				if(cMobileBO!=null){
					result = CustomerMobileDAO.changeCustomerPasswordByOldPasssword(CMBO, newPassword);
					if(result>0){
						JSONObject jobj = new JSONObject();
						jobj.put("Act", "ChangeOldPassword");
						jobj.put("status", 200);
						jobj.put("desc", "success");
						jobj.put("message", "passwords changed");
						array.put(jobj);

					}else{
						JSONObject jobj = new JSONObject();
						jobj.put("Act", "ChangeOldPassword");
						jobj.put("status", 300);
						jobj.put("desc", "failure");
						jobj.put("message", "Error in updating password");
						array.put(jobj);
					}
				}else{
					JSONObject jobj = new JSONObject();
					jobj.put("Act", "ChangeOldPassword");
					jobj.put("status", 300);
					jobj.put("desc", "failure");
					jobj.put("message", "Old Password wasnt matched");
					array.put(jobj);
				}
			}else{
				JSONObject jobj = new JSONObject();
				jobj.put("Act", "ChangeOldPassword");
				jobj.put("status", 400);
				jobj.put("desc", "failure");
				jobj.put("message", "Old Password or New Password is empty");
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		//System.out.println("response:"+array.toString());
		response.getWriter().write(array.toString());

	}


	private void changePassword(HttpServletRequest request, HttpServletResponse response) throws IOException, SystemUnavailableException {
		// TODO Auto-generated method stub

		String key = request.getParameter("Key");
		String userId = request.getParameter("UserId");
		String cCode = request.getParameter("assoccode");
		String password = PasswordHash.encrypt(request.getParameter("password"));

		int result =0;

		JSONArray array = new JSONArray();

		try{
			if(!userId.equalsIgnoreCase("") && !key.equalsIgnoreCase("") && !password.equalsIgnoreCase("")){
				result = CustomerMobileDAO.changeCustomerPasswordByPRSK(cCode, userId, key, password);
				if(result>0){
					JSONObject jobj = new JSONObject();
					jobj.put("Act", "ChangePassword");
					jobj.put("status", 200);
					jobj.put("desc", "success");
					jobj.put("message", "passwords changed");
					array.put(jobj);

				}else{
					JSONObject jobj = new JSONObject();
					jobj.put("Act", "ChangePassword");
					jobj.put("status", 300);
					jobj.put("desc", "failure");
					jobj.put("message", "error in updating password");
					array.put(jobj);
				}
			}else{
				JSONObject jobj = new JSONObject();
				jobj.put("Act", "ChangePassword");
				jobj.put("status", 400);
				jobj.put("desc", "failure");
				jobj.put("message", "UserId or password is empty");
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());

	}

	private void submitPasswordKey(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String key = request.getParameter("Key");
		String userId = request.getParameter("UserId");
		String cCode = request.getParameter("assoccode");
		int result =0;

		JSONArray array = new JSONArray();

		try{
			if(!userId.equalsIgnoreCase("") && !key.equalsIgnoreCase("")){
				result = CustomerMobileDAO.customerSubmitPasswordresttingKey(cCode, userId, key);
				if(result>0){
					JSONObject jobj = new JSONObject();
					jobj.put("Act", "SubmitPasswordKey");
					jobj.put("status", 200);
					jobj.put("desc", "success");
					jobj.put("message", "Your key is correct");
					array.put(jobj);

				}else{
					JSONObject jobj = new JSONObject();
					jobj.put("Act", "SubmitPasswordKey");
					jobj.put("status", 300);
					jobj.put("desc", "failure");
					jobj.put("message", "Your key is not correct");
					array.put(jobj);
				}
			}else{
				JSONObject jobj = new JSONObject();
				jobj.put("Act", "SubmitPasswordKey");
				jobj.put("status", 400);
				jobj.put("desc", "failure");
				jobj.put("message", "UserId or key is empty");
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		System.out.println("Response : "+array.toString());
		response.getWriter().write(array.toString());
	}

	private void getPasswordKey(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String userID = request.getParameter("UserId");
		String cCode = request.getParameter("assoccode");
		String fingerPrint=PasswordGen.generatePassword("A", 8);
		System.out.println("id:ccode:fingerprint"+userID+cCode+fingerPrint);
		int result1=0,result2=0;
		String phonenumber = "";
		int phlength = 0;
		CustomerMobileBO cmBo = new CustomerMobileBO();
		JSONArray array = new JSONArray();

		try{
			if(!userID.equals("") && !cCode.equals("")){
				cmBo = CustomerMobileDAO.customerUserIdVerification(userID, cCode);
				if(cmBo!=null){
					result2 = CustomerMobileDAO.customerRegeneratedPasswordKeyUpdate(cCode, userID, fingerPrint);
					CompanySystemProperties cspBO = SystemPropertiesDAO.getCSPForCustomerAppCCProcess(cCode);
					if(result2>0){
						try {
							sendSMS.main("Your 8-digit password Resetting key is "+fingerPrint, cspBO.getSmsPrefix()+cmBo.getPhoneNumber(), cCode);
						} catch (TwilioRestException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						phlength = cmBo.getPhoneNumber().length()-2;
						phonenumber=cmBo.getPhoneNumber().substring(cmBo.getPhoneNumber().length()-2);
						String stars="";
						for(int i =0; i<phlength; i++){
							stars = "*"+stars;
						}
						phonenumber=stars+phonenumber;

						JSONObject jObject = new JSONObject();
						jObject.put("Act", "GetPasswordkey");
						jObject.put("desc", "Success");
						jObject.put("status", 200);
						jObject.put("message", "Key is sended");
						jObject.put("phone", phonenumber);
						array.put(jObject);
					}else{
						JSONObject jObject = new JSONObject();
						jObject.put("Act", "GetPasswordkey");
						jObject.put("desc", "Failure");
						jObject.put("status", 300);
						jObject.put("message", "Cant able to update your key");
						array.put(jObject);
					}
				}else{
					JSONObject jObject = new JSONObject();
					jObject.put("Act", "GetPasswordkey");
					jObject.put("desc", "Failure");
					jObject.put("status", 400);
					jObject.put("message", "Not able to find your userId");
					array.put(jObject);
				}
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		System.out.println("Response : "+array.toString());
		response.getWriter().write(array.toString());
	}
	
	private void sendVerificationKey(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String userID = request.getParameter("UserId");
		String cCode = request.getParameter("assoccode");
		String cname = (request.getParameter("cName")!=null && !request.getParameter("cName").equals(""))?request.getParameter("cName"):"GAC Taxi Dispatch System";
		String fingerPrint=PasswordGen.generatePassword("A", 8);
		System.out.println("id:ccode:fingerprint -->> "+userID+" : "+cCode+" : "+fingerPrint);
		int result=0;
		String phonenumber = "";
		int phlength = 0;
		CustomerMobileBO cmBo = new CustomerMobileBO();
		JSONArray array = new JSONArray();

		try{
			if(!userID.equals("") && !cCode.equals("")){
				cmBo = CustomerMobileDAO.customerUserIdVerification(userID, cCode);
				if(cmBo!=null){
					result = CustomerMobileDAO.customerRegeneratedPasswordKeyUpdate(cCode, userID, fingerPrint);
					CompanySystemProperties cspBO = SystemPropertiesDAO.getCSPForCustomerAppCCProcess(cCode);
					if(result>0){
						try {
							sendSMS.main("Your Verification Key for "+cname+" is "+"GAC-"+fingerPrint, cspBO.getSmsPrefix()+cmBo.getPhoneNumber(), cCode);
						} catch (TwilioRestException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						phlength = cmBo.getPhoneNumber().length()-2;
						phonenumber=cmBo.getPhoneNumber().substring(cmBo.getPhoneNumber().length()-2);
						String stars="";
						for(int i =0; i<phlength; i++){
							stars = "*"+stars;
						}
						phonenumber=stars+phonenumber;
						
						JSONObject jObject = new JSONObject();
						jObject.put("Act", "GVK");
						jObject.put("desc", "Success");
						jObject.put("status", 200);
						jObject.put("message", "Key is sended");
						jObject.put("phone", phonenumber);
						array.put(jObject);
					}else{
						JSONObject jObject = new JSONObject();
						jObject.put("Act", "GVK");
						jObject.put("desc", "Failure");
						jObject.put("status", 300);
						jObject.put("message", "Cant able to update your key");
						array.put(jObject);
					}
				}else{
					JSONObject jObject = new JSONObject();
					jObject.put("Act", "GVK");
					jObject.put("desc", "Failure");
					jObject.put("status", 400);
					jObject.put("message", "Not able to find your userId");
					array.put(jObject);
				}
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		System.out.println("Response : "+array.toString());
		response.getWriter().write(array.toString());
	}
	
	private void submitVerificationKey(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String key = request.getParameter("Key");
		String userId = request.getParameter("UserId");
		String cCode = request.getParameter("assoccode");
		int result =0;

		System.out.println("id:ccode:fingerprint -->> "+userId+" : "+cCode+" : "+key);
		JSONArray array = new JSONArray();

		try{
			if(!userId.equalsIgnoreCase("") && !key.equalsIgnoreCase("")){
				result = CustomerMobileDAO.customerSubmitPasswordresttingKey(cCode, userId, key);
				if(result>0){
					JSONObject jobj = new JSONObject();
					jobj.put("Act", "SVK");
					jobj.put("status", 200);
					jobj.put("desc", "success");
					jobj.put("message", "Your key is correct");
					array.put(jobj);

				}else{
					JSONObject jobj = new JSONObject();
					jobj.put("Act", "SVK");
					jobj.put("status", 300);
					jobj.put("desc", "failure");
					jobj.put("message", "Your key is not correct");
					array.put(jobj);
				}
			}else{
				JSONObject jobj = new JSONObject();
				jobj.put("Act", "SVK");
				jobj.put("status", 400);
				jobj.put("desc", "failure");
				jobj.put("message", "UserId or key is empty");
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		System.out.println("Response : "+array.toString());
		response.getWriter().write(array.toString());
	}

	private void tripAmount(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String tripid="";
		String cCode = "";
		String resultAmount="";
		CustomerMobileBO cmBo = new CustomerMobileBO();
		tripid = request.getParameter("tripId");
		cCode = request.getParameter("assoccode");
		JSONArray array = new JSONArray();
		JSONObject jObj = new JSONObject();
		try{
			if(tripid!=""){
				cmBo = CustomerMobileDAO.tripDetails(tripid,cCode);
				if(cmBo!=null){
					jObj.put("Act", "GetTripAmount");
					jObj.put("status", 200);
					jObj.put("message", "success");
					jObj.put("tripamount", cmBo.getTripAMount());
					jObj.put("tripPaymentStatus", cmBo.getTripPaymentStatus());
					array.put(jObj);
				}else{
					jObj.put("Act", "GetTripAmount");
					jObj.put("status", 300);
					jObj.put("message", "UnSuccess");
					array.put(jObj);
				}
			}
		}
		catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	private void autoReconnect(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String reConnectKey = "";
		CustomerMobileBO custBO = new CustomerMobileBO();
		reConnectKey = request.getParameter("reconnectKey");
		String appSrc = request.getParameter("AppSrc")==null?"":request.getParameter("AppSrc");

		//System.out.println("reconnectkey"+reConnectKey);
		JSONArray customerLogin= null;
		JSONObject loginStatus = new JSONObject();
		ClientAuthenticationBO clientAdminBO=new ClientAuthenticationBO();
		try {
			if(reConnectKey !=null && !reConnectKey.equals("")){
				//System.out.println("reconnect key"+reConnectKey);
				customerLogin= new JSONArray();
				custBO = CustomerMobileDAO.checkReconnectKey(reConnectKey);
				if(custBO!=null){
					if(!appSrc.equals("")){
						if(appSrc.equalsIgnoreCase("A")){
							custBO.setApp_Source("4");
						}else{
							custBO.setApp_Source("0");
						}
					}else{
						custBO.setApp_Source("0");
					}
					//System.out.println("logged in");
					CustomerMobileBO cMobileBO =new CustomerMobileBO();
					cMobileBO=CustomerMobileDAO.customerLogin(custBO);
					if(cMobileBO!=null){
						HttpSession session = request.getSession(false);
						String sessionId = "";
						if(session==null){
							session = request.getSession();
						}
						sessionId = session.getId();
						//System.out.println("success login : "+sessionId);
						cMobileBO.setApp_Source(custBO.getApp_Source());
						CustomerMobileDAO.customerLoginDetails(cMobileBO,sessionId,"");
						String uniqueKey=PasswordGen.generatePassword("A", 16);
						CustomerMobileDAO.updateReconnectKey(uniqueKey,reConnectKey);
						
						clientAdminBO.setUserId(cMobileBO.getUserId());
						clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
						clientAdminBO.setEmailId(cMobileBO.getEmailId());
						clientAdminBO.setUserName(cMobileBO.getName());
						clientAdminBO.setCustomerUniqueId(cMobileBO.getCustomerUniqueId());
						clientAdminBO.setAssoccode(cMobileBO.getAssoccode());

						CompanySystemProperties cspBO = SystemPropertiesDAO.getCSPForCustomerAppCCProcess(cMobileBO.getAssoccode());
						clientAdminBO.setcCAuthorizeAmount(cspBO.getCcDefaultAmt());
						cMobileBO.setcCAuthorizeAmount(cspBO.getCcDefaultAmt());
						clientAdminBO.setcCMandatory(cspBO.getCcForCustomerApp());
						cMobileBO.setCcmandatory(cspBO.getCcForCustomerApp());
						clientAdminBO.setAppSrc(custBO.getApp_Source());
						Boolean ccresult = CreditCardDAO.customerCcAccount(cMobileBO.getCustomerUniqueId(),cMobileBO.getAssoccode());
						cMobileBO.setHasCcAccount(ccresult);
						
						//System.out.println("uniquekey:recoonectkey"+uniqueKey+":"+reConnectKey);
						//System.out.println("just before json");
						loginStatus.put("Act", "AutoReconnect");
						loginStatus.put("status", "loggedIn");
						loginStatus.put("userId", cMobileBO.getUserId());
						loginStatus.put("phoneNo", cMobileBO.getPhoneNumber());
						loginStatus.put("emailId", cMobileBO.getEmailId());
						loginStatus.put("name", cMobileBO.getName());
						loginStatus.put("sessionId", sessionId);
						loginStatus.put("getMessage", "Login successfully");
						loginStatus.put("GPID", TDSProperties.getValue("GCMPID"));
						loginStatus.put("UniqueId", cMobileBO.getCustomerUniqueId());
						loginStatus.put("UniqueKey", uniqueKey);
						loginStatus.put("ccprocess", cspBO.getCcForCustomerApp());
						loginStatus.put("customerUniqueId", cMobileBO.getCustomerUniqueId());
						loginStatus.put("cCAuthorizeAmount", cMobileBO.getcCAuthorizeAmount());
						loginStatus.put("customerCcAvailable", cMobileBO.isHasCcAccount()?"Yes":"No");
						loginStatus.put("paymentmandatory", cMobileBO.getPaymentMandatory());
						loginStatus.put("ETA_Calculate", cspBO.getETA_Calculate());
						
						ArrayList<CustomerMobileBO> jobLocation=CustomerMobileDAO.getJobLocation(clientAdminBO.getUserId(),"",Integer.parseInt(custBO.getApp_Source()));
						if(jobLocation!=null && jobLocation.size()>0){
							loginStatus.put("hT", "Yes");
							loginStatus.put("tId", jobLocation.get(0).getTripId());
						}
						
						customerLogin.put(loginStatus);
						response.getWriter().write(customerLogin.toString());
						System.out.println("session:"+sessionId +"----clientbo:"+clientAdminBO);
						session.setAttribute("clientUser", clientAdminBO);
						return;
						//response.getWriter().write("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
						//System.out.println("done Reconnect"+customerLogin.toString());
					}else{
						loginStatus.put("Act", "AutoReconnect");
						loginStatus.put("status", "failed");
						loginStatus.put("getMessage", "login failed");
						customerLogin.put(loginStatus);
					}
				}else{
					loginStatus.put("Act", "AutoReconnect");
					loginStatus.put("status", "failed");
					loginStatus.put("getMessage", "reconnect key havent matched");
					customerLogin.put(loginStatus);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("response"+customerLogin.toString());
		response.getWriter().write(customerLogin.toString());
	}
	
	private void guestAutoReconnect(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String reConnectKey = "";
		CustomerMobileBO custBO = new CustomerMobileBO();
		reConnectKey = request.getParameter("reconnectKey");
		String appSrc = request.getParameter("AppSrc")==null?"":request.getParameter("AppSrc");

		//System.out.println("reconnectkey"+reConnectKey);
		JSONArray customerLogin= null;
		JSONObject loginStatus = new JSONObject();
		ClientAuthenticationBO clientAdminBO=new ClientAuthenticationBO();
		try {
			if(reConnectKey !=null && !reConnectKey.equals("")){
				//System.out.println("reconnect key"+reConnectKey);
				customerLogin= new JSONArray();
				custBO = CustomerMobileDAO.checkReconnectKey(reConnectKey);
				if(custBO!=null){
					if(!appSrc.equals("")){
						if(appSrc.equalsIgnoreCase("A")){
							custBO.setApp_Source("4");
						}else{
							custBO.setApp_Source("0");
						}
					}else{
						custBO.setApp_Source("0");
					}
					//System.out.println("logged in");
					CustomerMobileBO cMobileBO =new CustomerMobileBO();
						HttpSession session = request.getSession(false);
						String sessionId = "";
						if(session!=null){
							sessionId = session.getId();
						}else{
							sessionId = request.getSession().getId();
						}
						//System.out.println("success login");
						cMobileBO.setApp_Source(custBO.getApp_Source());
						CustomerMobileDAO.customerLoginDetails(cMobileBO,sessionId,"");
						String uniqueKey=PasswordGen.generatePassword("A", 16);
						CustomerMobileDAO.updateReconnectKey(uniqueKey,reConnectKey);
						
						cMobileBO.setName("Guest User");
						System.out.println("userId:"+custBO.getUserId());
						cMobileBO.setPhoneNumber(custBO.getUserId());
						cMobileBO.setEmailId("");
						cMobileBO.setUserId(custBO.getUserId());
						cMobileBO.setAssoccode(custBO.getAssoccode());
						cMobileBO.setCustomerUniqueId(custBO.getUserId());
						
						clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
						clientAdminBO.setEmailId("");
						clientAdminBO.setUserId(custBO.getUserId());
						clientAdminBO.setAssoccode(custBO.getAssoccode());
						clientAdminBO.setCustomerUniqueId(custBO.getUserId());

						CompanySystemProperties cspBO = SystemPropertiesDAO.getCSPForCustomerAppCCProcess(cMobileBO.getAssoccode());
						clientAdminBO.setcCAuthorizeAmount(cspBO.getCcDefaultAmt());
						cMobileBO.setcCAuthorizeAmount(cspBO.getCcDefaultAmt());
						clientAdminBO.setcCMandatory(cspBO.getCcForCustomerApp());
						cMobileBO.setCcmandatory(cspBO.getCcForCustomerApp());
						clientAdminBO.setAppSrc(custBO.getApp_Source());
						Boolean ccresult = CreditCardDAO.customerCcAccount(cMobileBO.getCustomerUniqueId(),cMobileBO.getAssoccode());
						cMobileBO.setHasCcAccount(ccresult);
						
						session.setAttribute("clientUser", clientAdminBO);
						//System.out.println("uniquekey:recoonectkey"+uniqueKey+":"+reConnectKey);
						//System.out.println("just before json");
						loginStatus.put("Act", "AutoReconnect");
						loginStatus.put("status", "loggedIn");
						loginStatus.put("userId", cMobileBO.getUserId());
						loginStatus.put("phoneNo", cMobileBO.getPhoneNumber());
						loginStatus.put("emailId", cMobileBO.getEmailId());
						loginStatus.put("name", cMobileBO.getName());
						loginStatus.put("sessionId", sessionId);
						loginStatus.put("getMessage", "Login successfully");
						loginStatus.put("GPID", TDSProperties.getValue("GCMPID"));
						loginStatus.put("UniqueId", cMobileBO.getCustomerUniqueId());
						loginStatus.put("UniqueKey", uniqueKey);
						loginStatus.put("ccprocess", cspBO.getCcForCustomerApp());
						loginStatus.put("customerUniqueId", cMobileBO.getCustomerUniqueId());
						loginStatus.put("cCAuthorizeAmount", cMobileBO.getcCAuthorizeAmount());
						loginStatus.put("customerCcAvailable", cMobileBO.isHasCcAccount()?"Yes":"No");
						loginStatus.put("paymentmandatory", cMobileBO.getPaymentMandatory());
						loginStatus.put("ETA_Calculate", cspBO.getETA_Calculate());
						
						ArrayList<CustomerMobileBO> jobLocation=CustomerMobileDAO.getJobLocation(clientAdminBO.getUserId(),"",Integer.parseInt(custBO.getApp_Source()));
						if(jobLocation!=null && jobLocation.size()>0){
							loginStatus.put("hT", "Yes");
							loginStatus.put("tId", jobLocation.get(0).getTripId());
						}
						
					customerLogin.put(loginStatus);

						//response.getWriter().write("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
						//System.out.println("done Reconnect"+customerLogin.toString());
				}else{
					loginStatus.put("Act", "AutoReconnect");
					loginStatus.put("status", "failed");
					loginStatus.put("getMessage", "reconnect key havent matched");
					customerLogin.put(loginStatus);
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.out.println("response"+customerLogin.toString());
		response.getWriter().write(customerLogin.toString());
	}

	private void updateUserRegKey(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		HttpSession m_session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)m_session.getAttribute("clientUser");
		String googleRegistrationKey = request.getParameter("googRegKey") == null ? "" :request.getParameter("googRegKey");
		String uID = request.getParameter("uid");
		int result = 0;
		if (!googleRegistrationKey.equals("")) {
			if(clientAdminBO.getAppSrc().equalsIgnoreCase("4")){
				googleRegistrationKey=googleRegistrationKey.replace(" ","");
			}
			result = CustomerMobileDAO.updateGoogleKeyForCustomer(googleRegistrationKey, m_session.getId(), uID);
		}
		response.getWriter().write(result+"");
	}

	public void clientLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		//System.out.println("Clientlogin function called");
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession();
		ClientAuthenticationBO clientAdminBO=new ClientAuthenticationBO();
		CMBO.setUserId(request.getParameter("userId")==null?"": request.getParameter("userId") );
		int mode= Integer.parseInt((request.getParameter("loginOrLogout")==null?"0":request.getParameter("loginOrLogout")));
		JSONArray customerLogin= new JSONArray();
		JSONObject loginStatus = new JSONObject();
		StringBuffer resultStrBuffer = new StringBuffer();
		Cookie[] cookie= request.getCookies();
		if(request.getParameter("newEntry")!=null){
			if(cookie!=null){
				for(int i=0; i<cookie.length;i++){
					if(cookie[i].getName().equals("GAC-fingerPrint")){
						String userId=CustomerMobileDAO.getUserIdForThisCookie(cookie[i].getValue());
						CustomerMobileBO cMobileBO =new CustomerMobileBO();
						cMobileBO=CustomerMobileDAO.customerLoginWithCookie(userId);
						if(!cMobileBO.getUserId().equals("")){
							clientAdminBO.setUserId(cMobileBO.getUserId());
							clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
							clientAdminBO.setEmailId(cMobileBO.getEmailId());
							clientAdminBO.setUserName(cMobileBO.getName());
							clientAdminBO.setAssoccode(request.getParameter("assoccode"));
							//resultStrBuffer.append("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
							session.setAttribute("clientUser", clientAdminBO);
							response.getWriter().write("1");
							return;
						}else{
							response.getWriter().write("0");
							return;
						}
					}
				}
			}
			return;
		}
		if(mode==1){
			CMBO.setPassword(PasswordHash.encrypt(request.getParameter("password")==null?"":request.getParameter("password")));
			if(request.getParameter("AppSrc")!=null){
				if(request.getParameter("AppSrc").equalsIgnoreCase("A")){
					CMBO.setApp_Source("4");
				}else{
					CMBO.setApp_Source("0");
				}				
			}else{
				CMBO.setApp_Source("0");
			}

			if(!CMBO.getUserId().equals("") && !CMBO.getPassword().equals("") ){
				CMBO.setAssoccode(request.getParameter("assoccode"));
				CustomerMobileBO cMobileBO =new CustomerMobileBO();
				cMobileBO=CustomerMobileDAO.customerLogin(CMBO);
				
				if(cMobileBO!=null){
					String cmpCode = request.getParameter("assoccode");
					
					if(cmpCode.equalsIgnoreCase("184") && cMobileBO.getUserId().equals("")){
						try {
							loginStatus.put("Act", "LI");
							loginStatus.put("status", "wrongPassword");
							loginStatus.put("getMessage", "Please provide valid password to login");
							customerLogin.put(loginStatus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Response : "+customerLogin.toString());
						response.getWriter().write(customerLogin.toString());
						return;
					}
					
					if(cmpCode.equalsIgnoreCase("184") && !cMobileBO.isVerified()){
						try {
							loginStatus.put("Act", "LI");
							loginStatus.put("status", "notVerified");
							loginStatus.put("getMessage", "User verification is pending");
							customerLogin.put(loginStatus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Response : "+customerLogin.toString());
						response.getWriter().write(customerLogin.toString());
						return;
					}
					
					if(cmpCode.equalsIgnoreCase("184") && cMobileBO.isBlocked()){
						try {
							loginStatus.put("Act", "LI");
							loginStatus.put("status", "blocked");
							loginStatus.put("getMessage", "User is blocked from login");
							customerLogin.put(loginStatus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Response : "+customerLogin.toString());
						response.getWriter().write(customerLogin.toString());
						return;
					}
					
					String regKey= (request.getParameter("regkey")==null?"":request.getParameter("regkey"));
					clientAdminBO.setUserId(cMobileBO.getUserId());
					clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
					clientAdminBO.setEmailId(cMobileBO.getEmailId());
					clientAdminBO.setUserName(cMobileBO.getName());
					clientAdminBO.setCustomerUniqueId(cMobileBO.getCustomerUniqueId());
					clientAdminBO.setAssoccode(cmpCode);
					//clientAdminBO.setAssoccode(request.getParameter("assoccode"));

					cMobileBO.setAssoccode(cmpCode);

					CompanySystemProperties cspBO = SystemPropertiesDAO.getCSPForCustomerAppCCProcess(CMBO.getAssoccode());
					clientAdminBO.setcCAuthorizeAmount(cspBO.getCcDefaultAmt());
					clientAdminBO.setcCMandatory(cspBO.getCcForCustomerApp());

					cMobileBO.setcCAuthorizeAmount(cspBO.getCcDefaultAmt());
					cMobileBO.setCcmandatory(cspBO.getCcForCustomerApp());
					cMobileBO.setPaymentMandatory(cspBO.getPaymentForCustomerApp());
					cMobileBO.setETA_calculate(cspBO.getETA_Calculate());

					Boolean ccresult = CreditCardDAO.customerCcAccount(cMobileBO.getCustomerUniqueId(),cMobileBO.getAssoccode());
					cMobileBO.setHasCcAccount(ccresult);
					CMBO.setHasCcAccount(ccresult);

					//System.out.println("1"+CMBO.isHasCcAccount());
					cMobileBO.setApp_Source(CMBO.getApp_Source());
					clientAdminBO.setAppSrc(CMBO.getApp_Source());

					CustomerMobileDAO.customerLoginDetails(cMobileBO,session.getId(),regKey);
					session.setAttribute("clientUser", clientAdminBO);
					String uniqueKey="";
					if(request.getParameter("rememberMe")!=null && !request.getParameter("rememberMe").equals("")){
						String fingerPrint=PasswordGen.generatePassword("A", 16);
						Cookie c = new Cookie("GAC-fingerPrint", fingerPrint);
						c.setMaxAge(365 * 24 * 60 * 60); // one year
						response.addCookie(c);
						CustomerMobileDAO.setCookieAndUserId(fingerPrint,cMobileBO.getUserId());
						uniqueKey = fingerPrint;
						CustomerMobileDAO.setReconnectKey(uniqueKey,CMBO.getUserId(),CMBO.getPassword(),cmpCode);
					}
					try {
						loginStatus.put("Act", "LI");
						loginStatus.put("status", "loggedIn");
						loginStatus.put("userId", cMobileBO.getUserId());
						loginStatus.put("phoneNo", cMobileBO.getPhoneNumber());
						loginStatus.put("emailId", cMobileBO.getEmailId());
						loginStatus.put("name", cMobileBO.getName());
						loginStatus.put("sessionId", session.getId());
						loginStatus.put("getMessage", "Login successful");
						loginStatus.put("GPID", TDSProperties.getValue("GCMPID"));
						loginStatus.put("UniqueId", cMobileBO.getCustomerUniqueId());
						loginStatus.put("UniqueKey", uniqueKey);
						loginStatus.put("ccprocess", cspBO.getCcForCustomerApp());
						loginStatus.put("customerUniqueId", cMobileBO.getCustomerUniqueId());
						loginStatus.put("cCAuthorizeAmount", cMobileBO.getcCAuthorizeAmount());
						loginStatus.put("customerCcAvailable", CMBO.isHasCcAccount()?"Yes":"No");
						loginStatus.put("paymentmandatory", cMobileBO.getPaymentMandatory());
						loginStatus.put("ETA_Calculate", cMobileBO.getETA_calculate());
						
						ArrayList<CustomerMobileBO> jobLocation=CustomerMobileDAO.getJobLocation(clientAdminBO.getUserId(),"",Integer.parseInt(CMBO.getApp_Source()));
						if(jobLocation!=null && jobLocation.size()>0){
							loginStatus.put("hT", "Yes");
							loginStatus.put("tId", jobLocation.get(0).getTripId());
						}
						customerLogin.put(loginStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//response.getWriter().write("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
					//System.out.println("Login"+customerLogin.toString());
					response.getWriter().write(customerLogin.toString());
					return;
				}else{
					try {
						loginStatus.put("Act", "LI");
						loginStatus.put("status", "notRegister");
						loginStatus.put("getMessage", "You are not a registered user. Please register yourself to continue");
						customerLogin.put(loginStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}else{
				try {
					loginStatus.put("Act", "LI");
					loginStatus.put("status", "notloggedin");
					loginStatus.put("getMessage", "Please Provide Email-ID and Password");
					customerLogin.put(loginStatus);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("login"+customerLogin.toString());
			response.getWriter().write(customerLogin.toString());
		}else if(mode==0){
			//System.out.println("in logout mode:"+mode);
			ClientAuthenticationBO clientAdminBO1=(ClientAuthenticationBO)session.getAttribute("clientUser");
			String uid = request.getParameter("uid")==null?"":request.getParameter("uid");
			String ccode = request.getParameter("assoccode")==null?"":request.getParameter("assoccode");
			CustomerMobileDAO.updateCustomerLogoutTime(request.getSession().getId(),uid,ccode);
			session.invalidate();
			if(clientAdminBO1!=null){
				CustomerMobileDAO.deleteCookieForUserId(clientAdminBO1.getUserId());
				Cookie[] cookies = request.getCookies();
				List<Cookie> cookieList = new ArrayList<Cookie>();
				cookieList = Arrays.asList(cookies);
				for(Cookie cooki:cookieList) {
					if(cooki.getName().equals("GAC-fingerPrint")) {
						cooki.setValue(null);
						cooki.setMaxAge(0);
						response.addCookie(cooki);
					}
				}
			}
			/*else{
				System.out.println("else cALLED");			
			}*/

			if(request.getParameter("responseType")!=null &&  request.getParameter("responseType").equalsIgnoreCase("HTML5")){
				response.getWriter().write("loggedOut");
			}else{				
				try {
					loginStatus.put("Act", "LO");
					loginStatus.put("status", "loggedout");
					loginStatus.put("getMessage", "Successfully Logged out");
					customerLogin.put(loginStatus);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("login"+customerLogin.toString());
				response.getWriter().write(customerLogin.toString());
			}
		}
	}
	
	public void guestClientLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		//System.out.println("Clientlogin function called");
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession();
		ClientAuthenticationBO clientAdminBO=new ClientAuthenticationBO();
		CMBO.setUserId(request.getParameter("userId")==null?"": request.getParameter("userId") );
		System.out.println("userid:"+CMBO.getUserId());
		int mode= Integer.parseInt((request.getParameter("loginOrLogout")==null?"0":request.getParameter("loginOrLogout")));
		JSONArray customerLogin= new JSONArray();
		JSONObject loginStatus = new JSONObject();
		Cookie[] cookie= request.getCookies();
		if(request.getParameter("newEntry")!=null){
			if(cookie!=null){
				for(int i=0; i<cookie.length;i++){
					if(cookie[i].getName().equals("GAC-fingerPrint")){
						String userId=CustomerMobileDAO.getUserIdForThisCookie(cookie[i].getValue());
						CustomerMobileBO cMobileBO =new CustomerMobileBO();
						cMobileBO=CustomerMobileDAO.customerLoginWithCookie(userId);
						if(!cMobileBO.getUserId().equals("")){
							clientAdminBO.setUserId(cMobileBO.getUserId());
							clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
							clientAdminBO.setEmailId(cMobileBO.getEmailId());
							clientAdminBO.setUserName(cMobileBO.getName());
							clientAdminBO.setAssoccode(request.getParameter("assoccode"));
							//resultStrBuffer.append("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
							session.setAttribute("clientUser", clientAdminBO);
							response.getWriter().write("1");
							return;
						}else{
							response.getWriter().write("0");
							return;
						}
					}
				}
			}
			return;
		}
		if(mode==1){
			//CMBO.setPassword(PasswordHash.encrypt(request.getParameter("password")==null?"":request.getParameter("password")));
			if(request.getParameter("AppSrc")!=null){
				if(request.getParameter("AppSrc").equalsIgnoreCase("A")){
					CMBO.setApp_Source("4");
				}else{
					CMBO.setApp_Source("0");
				}				
			}else{
				CMBO.setApp_Source("0");
			}

			if(!CMBO.getUserId().equals("")){
				CMBO.setAssoccode(request.getParameter("assoccode"));
				CustomerMobileBO cMobileBO =new CustomerMobileBO();
				//cMobileBO=CustomerMobileDAO.customerLogin(CMBO);
				cMobileBO.setName("Guest User");
				cMobileBO.setPhoneNumber(CMBO.getUserId());
				cMobileBO.setEmailId("");
				cMobileBO.setUserId(CMBO.getUserId());
				cMobileBO.setAssoccode(CMBO.getAssoccode());
				cMobileBO.setCustomerUniqueId(CMBO.getUserId());
				
				String regKey= (request.getParameter("regkey")==null?"":request.getParameter("regkey"));
				String cmpCode = request.getParameter("assoccode");
				clientAdminBO.setUserId(cMobileBO.getUserId());
				clientAdminBO.setPhone(cMobileBO.getPhoneNumber());
				clientAdminBO.setEmailId(cMobileBO.getEmailId());
				clientAdminBO.setUserName(cMobileBO.getName());
				clientAdminBO.setCustomerUniqueId(cMobileBO.getCustomerUniqueId());
				clientAdminBO.setAssoccode(cmpCode);
				//clientAdminBO.setAssoccode(request.getParameter("assoccode"));

				cMobileBO.setAssoccode(cmpCode);

				CompanySystemProperties cspBO = SystemPropertiesDAO.getCSPForCustomerAppCCProcess(CMBO.getAssoccode());
				clientAdminBO.setcCAuthorizeAmount(cspBO.getCcDefaultAmt());
				clientAdminBO.setcCMandatory(cspBO.getCcForCustomerApp());

				cMobileBO.setcCAuthorizeAmount(cspBO.getCcDefaultAmt());
				cMobileBO.setCcmandatory(cspBO.getCcForCustomerApp());
				cMobileBO.setPaymentMandatory(cspBO.getPaymentForCustomerApp());
				cMobileBO.setETA_calculate(cspBO.getETA_Calculate());

				Boolean ccresult = CreditCardDAO.customerCcAccount(cMobileBO.getCustomerUniqueId(),cMobileBO.getAssoccode());
				cMobileBO.setHasCcAccount(ccresult);
				CMBO.setHasCcAccount(ccresult);

				//System.out.println("1"+CMBO.isHasCcAccount());
				cMobileBO.setApp_Source(CMBO.getApp_Source());
				clientAdminBO.setAppSrc(CMBO.getApp_Source());

				CustomerMobileDAO.customerLoginDetails(cMobileBO,session.getId(),regKey);
				session.setAttribute("clientUser", clientAdminBO);
				String uniqueKey="";
				if(request.getParameter("rememberMe")!=null && !request.getParameter("rememberMe").equals("")){
					String fingerPrint=PasswordGen.generatePassword("A", 16);
					Cookie c = new Cookie("GAC-fingerPrint", fingerPrint);
					c.setMaxAge(365 * 24 * 60 * 60); // one year
					response.addCookie(c);
					CustomerMobileDAO.setCookieAndUserId(fingerPrint,cMobileBO.getUserId());
					uniqueKey = fingerPrint;
					CustomerMobileDAO.setReconnectKey(uniqueKey,CMBO.getUserId(),CMBO.getPassword(),cmpCode);
				}
				try {
					loginStatus.put("Act", "LI");
					loginStatus.put("status", "loggedIn");
					loginStatus.put("userId", cMobileBO.getUserId());
					loginStatus.put("phoneNo", cMobileBO.getPhoneNumber());
					loginStatus.put("emailId", cMobileBO.getEmailId());
					loginStatus.put("name", cMobileBO.getName());
					loginStatus.put("sessionId", request.getSession().getId());
					loginStatus.put("getMessage", "Login successful");
					loginStatus.put("GPID", TDSProperties.getValue("GCMPID"));
					loginStatus.put("UniqueId", cMobileBO.getCustomerUniqueId());
					loginStatus.put("UniqueKey", uniqueKey);
					loginStatus.put("ccprocess", cspBO.getCcForCustomerApp());
					loginStatus.put("customerUniqueId", cMobileBO.getCustomerUniqueId());
					loginStatus.put("cCAuthorizeAmount", cMobileBO.getcCAuthorizeAmount());
					loginStatus.put("customerCcAvailable", CMBO.isHasCcAccount()?"Yes":"No");
					loginStatus.put("paymentmandatory", cMobileBO.getPaymentMandatory());
					loginStatus.put("ETA_Calculate", cMobileBO.getETA_calculate());
					
					ArrayList<CustomerMobileBO> jobLocation=CustomerMobileDAO.getJobLocation(clientAdminBO.getUserId(),"",Integer.parseInt(CMBO.getApp_Source()));
					if(jobLocation!=null && jobLocation.size()>0){
						loginStatus.put("hT", "Yes");
						loginStatus.put("tId", jobLocation.get(0).getTripId());
					}
					customerLogin.put(loginStatus);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//response.getWriter().write("status=loggedIn;userId="+cMobileBO.getUserId()+";phoneNo="+cMobileBO.getPhoneNumber()+";emailId="+cMobileBO.getEmailId()+";name="+cMobileBO.getName()+";sessionId="+request.getSession().getId());
				System.out.println("GuestLogin"+customerLogin.toString());
				response.getWriter().write(customerLogin.toString());
					return;
				/*}else{
					try {
						loginStatus.put("Act", "LI");
						loginStatus.put("status", "notloggedin");
						loginStatus.put("getMessage", "Please Provide valid Username or Password");
						customerLogin.put(loginStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}*/
			}else{
				try {
					loginStatus.put("Act", "LI");
					loginStatus.put("status", "notloggedin");
					loginStatus.put("getMessage", "Please Provide Username and Password");
					customerLogin.put(loginStatus);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//System.out.println("login"+customerLogin.toString());
			response.getWriter().write(customerLogin.toString());
		}else if(mode==0){
			//System.out.println("in logout mode:"+mode);
			ClientAuthenticationBO clientAdminBO1=(ClientAuthenticationBO)session.getAttribute("clientUser");
			String uid = request.getParameter("uid")==null?"":request.getParameter("uid");
			String ccode = request.getParameter("assoccode")==null?"":request.getParameter("assoccode");
			CustomerMobileDAO.updateCustomerLogoutTime(request.getSession().getId(),uid,ccode);
			session.invalidate();
			if(clientAdminBO1!=null){
				CustomerMobileDAO.deleteCookieForUserId(clientAdminBO1.getUserId());
				Cookie[] cookies = request.getCookies();
				List<Cookie> cookieList = new ArrayList<Cookie>();
				cookieList = Arrays.asList(cookies);
				for(Cookie cooki:cookieList) {
					if(cooki.getName().equals("GAC-fingerPrint")) {
						cooki.setValue(null);
						cooki.setMaxAge(0);
						response.addCookie(cooki);
					}
				}
			}
			/*else{
				System.out.println("else cALLED");			
			}*/

			if(request.getParameter("responseType")!=null &&  request.getParameter("responseType").equalsIgnoreCase("HTML5")){
				response.getWriter().write("loggedOut");
			}else{				
				try {
					loginStatus.put("Act", "LO");
					loginStatus.put("status", "loggedout");
					loginStatus.put("getMessage", "Successfully Logged out");
					customerLogin.put(loginStatus);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("login"+customerLogin.toString());
				response.getWriter().write(customerLogin.toString());
			}
		}

	}
	
	public void clientRegistration (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		if(request.getParameter("registerButton")==null){

			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/registration.jsp");
			dispatcher.forward(request, response);
			return;
		}else{
			CustomerMobileBO CMBO=new CustomerMobileBO();
			CMBO.setUserId(request.getParameter("userId"));
			CMBO.setPassword(PasswordHash.encrypt(request.getParameter("password")));
			CMBO.setEmailId(request.getParameter("emailId")==null?"": request.getParameter("emailId"));
			CMBO.setPhoneNumber(request.getParameter("phoneNumber")==null?"": request.getParameter("phoneNumber"));
			CMBO.setUserName(request.getParameter("userName")==null?"": request.getParameter("userName"));
			CMBO.setAssoccode(request.getParameter("ccode")==null?"": request.getParameter("ccode"));
			if(request.getParameter("os")!=null){
				if(request.getParameter("os").equalsIgnoreCase("A")){
					CMBO.setApp_Source("4");
				}else{
					CMBO.setApp_Source("0");
				}
			}else{
				System.out.println("No chance at all");
				CMBO.setApp_Source("0");
			}

			JSONArray customerRegistration= new JSONArray();
			JSONObject registrationStatus = new JSONObject();

			if(!CMBO.getUserId().equals("") && !CMBO.getPassword().equals("") ){
				int result=CustomerMobileDAO.customerRegistrationForLogin(CMBO);
				if(request.getParameter("responseType").equalsIgnoreCase("HTML5")){
					if(result==1){
						request.setAttribute("error", "Registered Successfully");
						response.getWriter().write("Registered");

						//RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/registration.jsp");
						//dispatcher.forward(request, response);
					}else{
						request.setAttribute("error", "Registration was Unsuccessful");
						response.getWriter().write("not Registered");

						//RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/registration.jsp");
						//dispatcher.forward(request, response);
					}
				}else{
					if(result==1){
						//CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(CMBO.getAssoccode());
						try {
							registrationStatus.put("Act", "RU");
							registrationStatus.put("status", 200);
							registrationStatus.put("getMessage","Successfully Registered");
							customerRegistration.put(registrationStatus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else if(result==2){
						try {
							registrationStatus.put("Act", "RU");
							registrationStatus.put("status", 300);
							registrationStatus.put("getMessage","Alreay Registered");
							customerRegistration.put(registrationStatus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}else{
						try {
							registrationStatus.put("Act", "RU");
							registrationStatus.put("status", 400);
							registrationStatus.put("getMessage","Registration Unsuccessful");
							customerRegistration.put(registrationStatus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					response.getWriter().write(customerRegistration.toString());
				}
			}else{
				if(request.getParameter("responseType")=="HTML5"){
					request.setAttribute("error", "Please enter the username and password");
					RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/registration.jsp");
					dispatcher.forward(request, response);
					return;
				}else{
					try {
						registrationStatus.put("Act", "RU");
						registrationStatus.put("status", 400);
						registrationStatus.put("getMessage","Please enter the username and password");
						customerRegistration.put(registrationStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	public void clientAccountRegistration (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		if(clientAdminBO.getUserId()!=null){
			CustomerMobileBO customerAccount =new CustomerMobileBO();
			if(request.getParameter("registerButton")==null){
				if(request.getParameter("serialNo")!=null){
					customerAccount=CustomerMobileDAO.customerAccount(request.getParameter("serialNo"));
					request.setAttribute("accountDetail", customerAccount);
				}else{
					customerAccount.setName(clientAdminBO.getUserName());
					customerAccount.setUserId(clientAdminBO.getUserId());
					request.setAttribute("accountDetail", customerAccount);
				}
				RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/accountRegistration.jsp");
				dispatcher.forward(request, response);
				return;
			}
			CMBO.setUserId(clientAdminBO.getUserId());
			CMBO.setPaymentType(request.getParameter("paymentType")==null?"": request.getParameter("paymentType"));
			CMBO.setUserName(request.getParameter("userName")==null?"": request.getParameter("userName"));
			CMBO.setCardNumber(request.getParameter("cardNumber")==null?"": request.getParameter("cardNumber"));
			CMBO.setCardExpiryDate(request.getParameter("cardExpiryDate")==null?"": request.getParameter("cardExpiryDate"));
			CMBO.setVoucherNumber(request.getParameter("voucherNumber")==null?"": request.getParameter("voucherNumber"));
			CMBO.setVoucherExpiryDate(request.getParameter("voucherExpiryDate")==null?"": request.getParameter("voucherExpiryDate"));
			int mode= Integer.parseInt(request.getParameter("mode")==null?"0":request.getParameter("mode"));
			//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
			if(mode==1){
				int result=CustomerMobileDAO.customerAccountRegistration(CMBO,mode);
				JSONArray customerAccountRegistration= new JSONArray();
				JSONObject registrationStatus = new JSONObject();
				if(result==1){
					try {
						registrationStatus.put("getMessage","Successfully Registered");
						customerAccountRegistration.put(registrationStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					try {
						registrationStatus.put("getMessage","Registration Unsuccessful");
						customerAccountRegistration.put(registrationStatus);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				response.getWriter().write(customerAccountRegistration.toString());	
				return;
			}else if(mode==2){
				CMBO.setSerialNo(request.getParameter("serialNumber")==null?"": request.getParameter("serialNumber"));
				int result = CustomerMobileDAO.customerAccountRegistration(CMBO,mode);
				if(result==1){
					response.getWriter().write("updated Successfully");
				}else{
					response.getWriter().write("update Unsuccessful");
				}
			}else if(mode==3){
				CMBO.setSerialNo(request.getParameter("serialNumber")==null?"": request.getParameter("serialNumber"));

				int result = CustomerMobileDAO.customerAccountRegistration(CMBO,mode);
				if(result==1){
					response.getWriter().write("Deleted Successfully");
				}else{
					response.getWriter().write("Delete Unsuccessful");
				}
			}

		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}

	public void clientAccountSummary (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		if(clientAdminBO.getUserId()!=null){
			//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
			ArrayList<CustomerMobileBO> customerAccountList=CustomerMobileDAO.customerAccountSummary(clientAdminBO.getUserId(),"",1);
			/*if(request.getParameter("responseType")!=null && request.getParameter("responseType").equals("HTML5")){
				request.setAttribute("accountSummary", customerAccountList);
				request.setAttribute("screen", "/ClientUsage/accountSummary.jsp");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/accountSummary.jsp");
				dispatcher.forward(request, response);
				return;
			}*/
			JSONArray customerAccountSummary = new JSONArray();
			if(customerAccountList.size()>0){
				for(int i=0;i<customerAccountList.size();i++){
					JSONObject account = new JSONObject();
					try {
						account.put("getPaymentType", customerAccountList.get(i).getPaymentType());
						account.put("getCardNumber", customerAccountList.get(i).getCardNumber());
						account.put("getCardExpiryDate", customerAccountList.get(i).getCardExpiryDate());
						account.put("getVoucherNumber", customerAccountList.get(i).getVoucherNumber());
						account.put("getVoucherExpiryDate", customerAccountList.get(i).getVoucherExpiryDate());
						customerAccountSummary.put(account);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				response.getWriter().write(customerAccountSummary.toString());
			}else{
				if(request.getParameter("responseType").equals("HTML5")){
					//	request.setAttribute("error", "No records Found");
					//	request.setAttribute("screen", "");
					response.getWriter().write("");

					return;
				}
				JSONObject account =new JSONObject();
				try {
					account.put("message", "");
					customerAccountSummary.put(account);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				response.getWriter().write(customerAccountSummary.toString());
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}
	public void tripSummary (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		int src = clientAdminBO.getAppSrc().equalsIgnoreCase("4")?4:0;
		if(clientAdminBO.getUserId()!=null){
			//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
			ArrayList<CustomerMobileBO> tripList=CustomerMobileDAO.tripSummary(clientAdminBO.getUserId(),src);
			JSONArray customerTripSummary = new JSONArray();
			if(tripList.size()>0){
				for(int i=0;i<tripList.size();i++){
					JSONObject trips = new JSONObject();
					try {
						trips.put("getTripId", tripList.get(i).getTripId());
						trips.put("getPhoneNumber", tripList.get(i).getPhoneNumber());
						trips.put("getCity", tripList.get(i).getCity());
						trips.put("getAddress", tripList.get(i).getAddress());
						trips.put("getName", tripList.get(i).getName());
						customerTripSummary.put(trips);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				response.getWriter().write(customerTripSummary.toString());
			}else{
				/*JSONObject trips = new JSONObject();
				try {
					trips.put("getMessage", "No records found");
					customerTripSummary.put(trips);
				} catch (JSONException e) {
					e.printStackTrace();
				}*/
				response.getWriter().write("");
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}
	public void tripHistory (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		if(clientAdminBO.getUserId()!=null){
			ArrayList<CustomerMobileBO> tripList=CustomerMobileDAO.tripHistory(clientAdminBO.getUserId(),clientAdminBO.getAssoccode(),SystemPropertiesDAO.getParameter(clientAdminBO.getAssoccode(), "timeZoneArea"));
			JSONArray customerTripSummary = new JSONArray();
			if(tripList.size()>0){
				for(int i=0;i<tripList.size();i++){
					JSONObject trips = new JSONObject();
					try {
						trips.put("Act", "TripHistory");
						trips.put("status", 200);
						trips.put("getMessage", "Records found");
						trips.put("TId", tripList.get(i).getTripId());
						trips.put("Ph", tripList.get(i).getPhoneNumber());
						trips.put("City", tripList.get(i).getCity());
						trips.put("Name", tripList.get(i).getName());
						trips.put("Add", tripList.get(i).getAddress());
						trips.put("stlat", tripList.get(i).getStLatitude());
						trips.put("stlon", tripList.get(i).getStLongitude());
						trips.put("edAdd", tripList.get(i).getEdAddress());
						trips.put("edlat", tripList.get(i).getEdLatitude());
						trips.put("edlon", tripList.get(i).getEdLongitude());
						trips.put("date", tripList.get(i).getServiceDate());
						trips.put("edCity", tripList.get(i).getEdcity());
						
						//System.out.println("in triphistoryCMA:lat"+tripList.get(i).getStLatitude()+"+stlon:"+tripList.get(i).getStLongitude());
						customerTripSummary.put(trips);
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				//System.out.println("reponse"+customerTripSummary.toString());
				response.getWriter().write(customerTripSummary.toString());
			}else{
				JSONObject trips = new JSONObject();
				try {
					trips.put("Act", "TripHistory");
					trips.put("status", 300);
					trips.put("getMessage", "No records found");
					customerTripSummary.put(trips);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				/*if(request.getParameter("responseType").equalsIgnoreCase("HTML5")){
					request.setAttribute("tripSummary", tripList);
					RequestDispatcher dispatcher=request.getRequestDispatcher("/ClientUsage/tripHistory.jsp");
					dispatcher.forward(request, response);
					return;
				}*/
				response.getWriter().write(customerTripSummary.toString());
			}
		}
	}
	public void openRequest (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=new ClientAuthenticationBO();
		if(session!=null && session.getAttribute("clientUser")!=null){
			clientAdminBO=(ClientAuthenticationBO)(session.getAttribute("clientUser"));
		}

		CompanyMasterBO cmpBean=null;;
		PaymentProcessBean processBean=null;
		CreditCardBO ccBean=null;
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");

		JSONArray jobArray = new JSONArray();
		AdminRegistrationBO adminBo=new AdminRegistrationBO();
		OpenRequestBO OpenBO = new OpenRequestBO(); 
		adminBo.setAssociateCode("");
		int result=0;
		int transId=0;
		int dispatchAdvaceTime=15*60;
		if(request.getParameter("latitude")!=null && request.getParameter("longitude")!=null ){
			if((request.getParameter("phoneNumber")==null || request.getParameter("phoneNumber").equals("") ||  request.getParameter("phoneNumber").equals("null") )&& (session.getAttribute("clientUser")!=null)){
				request.setAttribute("phoneNumber", clientAdminBO.getPhone());
			}else{
				request.setAttribute("phoneNumber",request.getParameter("phoneNumber"));
			}
			if(session.getAttribute("clientUser")!=null){
				request.setAttribute("name", clientAdminBO.getUserName());
			}else{
				request.setAttribute("name", "");
			}
			request.setAttribute("screen", "/ClientUsage/bookAJob.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/bookAJob.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(request.getParameter("submit")==null){
			if(session.getAttribute("clientUser")!=null){
				request.setAttribute("name", clientAdminBO.getUserName());
				request.setAttribute("phoneNumber",clientAdminBO.getPhone());
			}else{
				request.setAttribute("name", "");
				request.setAttribute("phoneNumber","");
			}
			request.setAttribute("screen", "/ClientUsage/bookAJob.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/bookAJob.jsp");
			dispatcher.forward(request, response);
			return;
		}
		if(request.getParameter("submit")!=null){
			OpenBO.setPaytype(request.getParameter("payType")==null?"Cash":request.getParameter("payType"));
			if(clientAdminBO.getcCMandatory()==1 || OpenBO.getPaytype().equalsIgnoreCase("CC")){
				if(request.getParameter("custId")!=null && !request.getParameter("custId").equals("") && request.getParameter("defaultAmt")!=null && !request.getParameter("defaultAmt").equals("")){
					cmpBean=SystemPropertiesDAO.getCCProperties(clientAdminBO.getAssoccode());
					ccBean= CreditCardDAO.getCardDetails(request.getParameter("custId"), clientAdminBO.getAssoccode());
					processBean = new PaymentProcessBean();
					processBean.setB_trip_id("00000");
					processBean.setCvv("");
					processBean.setB_card_full_no("");
					processBean.setB_amount(request.getParameter("defaultAmt").equals("")?"0":request.getParameter("defaultAmt"));
					processBean.setB_cardExpiryDate(ccBean.getExpiryDate());
					processBean.setZip("");
					processBean.setB_cardHolderName(ccBean.getNameOnCard());
					processBean.setB_trans_id(ccBean.getCcInfoKey());
					processBean.setB_tip_amt("0");
					if(cmpBean.getCcProvider()==2){
						processBean= PaymentGatewaySlimCD.makeProcess(processBean, cmpBean, PaymentCatgories.Auth);
					} else {
						processBean=PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Auth);
					}
					if(processBean.isAuth_status()){
						transId=CreditCardDAO.insertPaymentIntoCC(processBean, request.getRealPath("/images/nosign.gif"), poolBO, clientAdminBO, ICCConstant.PREAUTH);
					}else{
						JSONObject jobObject = new JSONObject();
						try {
							jobObject.put("Act", "BookAJob");
							jobObject.put("status", 300);
							jobObject.put("message", "Your job haven't booked. Failed to process CreditCard.");
							jobArray.put(jobObject);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						response.getWriter().write(jobArray.toString());
						return;
					}
				}
			}
			if(getServletConfig().getServletContext().getAttribute( request.getParameter("ccode") + "ZonesLoaded") == null){
				SystemUtils.reloadZones(this, request.getParameter("ccode"));
			}
			OpenBO.setName(request.getParameter("userName")==null?"": request.getParameter("userName"));
			OpenBO.setSadd1(request.getParameter("sAddress1")==null?"": request.getParameter("sAddress1"));
			OpenBO.setPhone(request.getParameter("phoneNumber")==null?clientAdminBO.getPhone(): request.getParameter("phoneNumber"));
			OpenBO.setSlat(request.getParameter("sLatitude")==null?"": request.getParameter("sLatitude"));
			OpenBO.setSlong(request.getParameter("sLongitude")==null?"0.00": request.getParameter("sLongitude"));
			OpenBO.setSdate(request.getParameter("sDate")==null?"": request.getParameter("sDate"));
			OpenBO.setEdlatitude(request.getParameter("eLatitude")==null?"": request.getParameter("eLatitude"));
			OpenBO.setEdlongitude(request.getParameter("eLongitude")==null?"": request.getParameter("eLongitude"));
			OpenBO.setEadd1(request.getParameter("eAddress1")==null?"": request.getParameter("eAddress1"));
			OpenBO.setSpecialIns((request.getParameter("comments")==null || request.getParameter("comments").equals(""))?"Job Booked from Customer App, Call Customer first":request.getParameter("comments")+". Job Booked from Customer App, Call Customer first");
			OpenBO.setScity(request.getParameter("scity")==null? "":request.getParameter("scity"));
			OpenBO.setEcity(request.getParameter("ecity")==null? "":request.getParameter("ecity"));
			System.out.println("Scity:" +OpenBO.getScity());
			/*if(true){
				return;
			}*/
			OpenBO.setAdvanceTime("-1");
			OpenBO.setAmt(new BigDecimal("0.00"));
			clientAdminBO.setTimeZone(SystemPropertiesDAO.getParameter(request.getParameter("ccode"), "timeZoneArea"));
			if(OpenBO.getSdate().equals("")){
				OpenBO.setSdate("12/31/3000");
				OpenBO.setShrs("2525");
			}else{
				if(OpenBO.getSdate().length()!=10){
					String[] date=OpenBO.getSdate().split("/");
					if(date[0].length()<2){
						date[0]="0"+date[0];
					}
					if(date[1].length()<2){
						date[1]="0"+date[1];
					}
					OpenBO.setSdate(date[0]+"/"+date[1]+"/"+date[2]);
				}
				String time=request.getParameter("time")==null?"": request.getParameter("time");
				time=time.replace(":", "");
				time=time.replace(" ", "");
				if(time.contains("PM")){
					time=time.replace("PM","");
					if(Integer.parseInt(time)>1259){
						time=Integer.parseInt(time)+1200+"";
					}
				}else if(time.contains("AM")){
					time=time.replace("AM", "");
					if(Integer.parseInt(time)<1259){
						//	time=Integer.parseInt(time)+1200+"";
						time=time.replace("12","00");
					}
				}
				OpenBO.setShrs(time);
			}	
			OpenBO.setChckTripStatus(8);
			OpenBO.setTypeOfRide("0");

			if(!clientAdminBO.getAppSrc().equals("")){
				if(clientAdminBO.getAppSrc().equalsIgnoreCase("4")){
					OpenBO.setTripSource(TDSConstants.mobile_IOS);
				}else{
					OpenBO.setTripSource(TDSConstants.mobile_android);
				}
			}else{
				//System.out.println("No way here");
				OpenBO.setTripSource(0);
			}

			OpenBO.setAssociateCode(request.getParameter("ccode")==null?"": request.getParameter("ccode")); 
			if(OpenBO.getSlat()!=null && !OpenBO.getSlat().equals("0") && !OpenBO.getSlong().equals("0")  ) {
				ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) this.getServletContext().getAttribute((OpenBO.getAssociateCode() +"Zones"));
				OpenBO.setQueueno( CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(OpenBO.getSlat()),Double.parseDouble(OpenBO.getSlong())));
				ZoneTableBeanSP zonesId;
				if(!OpenBO.getQueueno().equals("")){
					List<QueueCoordinatesBO> queueProp = SystemPropertiesDAO.getQueueCoOrdinateList(OpenBO.getQueueno(), OpenBO.getAssociateCode(), "");
					dispatchAdvaceTime = Integer.parseInt(queueProp.get(0).getQDelayTime())*60;
					OpenBO.setDispatchDistance(Double.parseDouble(queueProp.get(0).getQDispatchDistance()));
				}else{
					zonesId = ZoneDAO.getDefaultZone(clientAdminBO.getAssoccode());
					if(zonesId!=null){
						dispatchAdvaceTime = zonesId.getAdvance();
						OpenBO.setDispatchDistance(zonesId.getDistance());
					}else{
						dispatchAdvaceTime = 30;
						OpenBO.setDispatchDistance(100);
					}
				}
			}
			if(session !=null && session.getAttribute("clientUser")!=null){
				//System.out.println("userid:"+clientAdminBO.getUserId());
				OpenBO.setCreatedBy(clientAdminBO.getUserId());
			}
			else{
				OpenBO.setCreatedBy(request.getParameter("userID"));
			}

			OpenBO.setVecProfile(request.getParameter("cabFlag").equals("0")?"":request.getParameter("cabFlag"));
			OpenBO.setDrProfile(request.getParameter("drFlag").equals("0")?"":request.getParameter("drFlag"));
			adminBo.setAssociateCode(clientAdminBO.getAssoccode());
			adminBo.setTimeZone(clientAdminBO.getTimeZone());
			adminBo.setUid(clientAdminBO.getUserId());
			OpenBO.setEmail(clientAdminBO.getEmailId());
			adminBo.setMasterAssociateCode(OpenBO.getAssociateCode());
			result=RequestDAO.saveOpenRequest(OpenBO, adminBo,0);
			try{
				
				boolean is_driver_allocated = true;
				if(result>=1){
					String driverId = "";
					String driverName = "";
					JSONObject jobObject = new JSONObject();
					if(OpenBO.getPaytype().equalsIgnoreCase("CC")){
						CreditCardDAO.updateCCDetail(result,transId);
					}
					String tripID = result+"";
					OpenBO.setTripid(tripID);
					CustomerMobileDAO.updateTripIDCustomer(clientAdminBO.getUserId(),session.getId(),tripID);
					DispatchDAO.updateDispatchStartTime(OpenBO, dispatchAdvaceTime, clientAdminBO.getAssoccode());
					DispatchPropertiesBO messageFields = SystemPropertiesDAO.getDispatchPropeties(clientAdminBO.getAssoccode());
					ArrayList<String> companiesToLook = new ArrayList<String>();
					companiesToLook.add(clientAdminBO.getAssoccode());
					
					String driverAlarm = "0";
					double start_latti = Double.parseDouble(OpenBO.getSlat());
					double start_longi = Double.parseDouble(OpenBO.getSlong());
					
					ArrayList<DriverCabQueueBean> dr = DispatchDAO.allDriversWithInADistance(companiesToLook, start_latti, start_longi, OpenBO.getDispatchDistance(), null, OpenBO.getJobRating(),ZoneDAO.getMaxDistance(clientAdminBO.getAssoccode(),OpenBO.getQueueno()));
					driverAlarm = SystemPropertiesDAO.getParameter(clientAdminBO.getAssoccode(), "driverAlarm");
					
					if(dr!=null && dr.size()>0){
						DriverCabQueueBean closestDriver = null;
						if(dr.size()>1){
							System.out.println("Finding Closest Driver For Trip--->"+tripID);
							double closestDriverDistance = 7000;
							double currentDriverDistance = 0;
							StringBuffer finalResult = new StringBuffer();
							for (int i =0; i<dr.size(); i++){
								if(!DispatchDAO.checkDriverAvailability(clientAdminBO.getAssoccode(), dr.get(i).getDriverid(), driverAlarm)){
									continue;
								}
								
								currentDriverDistance = ClosestDriver.distance(dr.get(i).getCurrentLatitude(), dr.get(i).getCurrentLongitude(),start_latti, start_longi, 0); 
								finalResult.append("Driver:"+dr.get(i).getDriverid()+";Vehicle:"+dr.get(i).getVehicleNo()+";Distance:"+currentDriverDistance+"^");
								if ( currentDriverDistance < closestDriverDistance){
									closestDriver = dr.get(i);
									closestDriverDistance = currentDriverDistance;
								}
							}
							
							//closestDriver = ClosestDriver.findClosestDriver(dr, Double.parseDouble(OpenBO.getSlat()), Double.parseDouble(OpenBO.getSlong()),ZoneDAO.getMaxDistance(OpenBO.getAssociateCode(),OpenBO.getQueueno()),tripID,OpenBO.getAssociateCode(),OpenBO.getAssociateCode());
						} else{
							
							if(!DispatchDAO.checkDriverAvailability(clientAdminBO.getAssoccode(), dr.get(0).getDriverid(), driverAlarm)){
								closestDriver = null;
							}else{
								closestDriver = dr.get(0);
							}
						}
						
						if(closestDriver!=null){
							String statusToUpdate = TDSConstants.performingDispatchProcesses + "";
							long msgID = System.currentTimeMillis();
							if(OpenBO.getTripSource()==TDSConstants.mobile_android || OpenBO.getTripSource()==TDSConstants.mobile_IOS){
								//System.out.println("Allocating Trip--->"+openRequestBO.getTripid()+" to driver--->"+cabQueueBean.getDriverid()+" by Shortest Dist");
								statusToUpdate = TDSConstants.jobAllocated+"";
								String[] message = MessageGenerateJSON.generateMessageAfterAcceptance(OpenBO,"FA" , msgID,messageFields);
								Messaging oneSMS = new Messaging(getServletContext(),closestDriver, message,poolBO,clientAdminBO.getAssoccode());
								oneSMS.start();
							} else {
								String[] message = MessageGenerateJSON.generateMessage(OpenBO, "A", msgID,messageFields,clientAdminBO.getAssoccode());
								Messaging oneSMS = new Messaging(getServletContext(),closestDriver, message,poolBO,clientAdminBO.getAssoccode());
								oneSMS.start();
							}
							String historyOfdrivers = closestDriver.getDriverid() ;
							ServiceRequestDAO.updateOpenRequestStatus(clientAdminBO.getAssoccode(),OpenBO.getTripid(), statusToUpdate , closestDriver.getDriverid(), historyOfdrivers,closestDriver.getVehicleNo(),"");
							OpenBO.setChckTripStatus(TDSConstants.performingDispatchProcesses);
							System.out.println("Offering Job To Driver By Shortest Dist : "+closestDriver.getDriverid());
							driverId = closestDriver.getDriverid();
							driverName = closestDriver.getDrivername();
							AuditDAO.insertJobLogs("Offering Job To Driver By Shortest Dist -> "+closestDriver.getDriverid()+":"+closestDriver.getVehicleNo(), OpenBO.getTripid(),clientAdminBO.getAssoccode(),TDSConstants.performingDispatchProcesses, "System-A",OpenBO.getSlat(),OpenBO.getSlong(),clientAdminBO.getAssoccode());
							is_driver_allocated = true;
						}else{
							is_driver_allocated = false;
							System.out.println("Cant find driver - eventhough driver found");
							ServiceRequestDAO.updateOpenRequestStatusAndDrivers(OpenBO.getTripid(), TDSConstants.performingDispatchProcessesCantFindDrivers+"" , "", "");
							OpenBO.setChckTripStatus(TDSConstants.performingDispatchProcessesCantFindDrivers);
							AuditDAO.insertJobLogs("Cant find driver", OpenBO.getTripid(),clientAdminBO.getAssoccode(),TDSConstants.performingDispatchProcessesCantFindDrivers, "System-A",OpenBO.getSlat(),OpenBO.getSlong(),clientAdminBO.getAssoccode());
						}
					}else{
						is_driver_allocated = false;
						System.out.println("Cant find driver");
						ServiceRequestDAO.updateOpenRequestStatusAndDrivers(OpenBO.getTripid(), TDSConstants.performingDispatchProcessesCantFindDrivers+"" , "", "");
						OpenBO.setChckTripStatus(TDSConstants.performingDispatchProcessesCantFindDrivers);
						AuditDAO.insertJobLogs("Cant find driver", OpenBO.getTripid(),clientAdminBO.getAssoccode(),TDSConstants.performingDispatchProcessesCantFindDrivers, "System-A",OpenBO.getSlat(),OpenBO.getSlong(),clientAdminBO.getAssoccode());
					}
					
					//CustomerUtil customerAutoAllocate = new CustomerUtil(adminBo, tripID, getServletConfig());
					//customerAutoAllocate.run();
					
					if(is_driver_allocated){
						jobObject.put("Act", "BookAJob");
						jobObject.put("status", 200);
						jobObject.put("message", "Job Booked");
						jobObject.put("TripId", result);
						jobObject.put("dId", driverId);
						jobObject.put("dN", driverName);
						jobArray.put(jobObject);
					}else{
						CustomerMobileDAO.auto_cancelReservation(tripID);
						if(OpenBO.getPaytype().equalsIgnoreCase("CC")){
							PaymentProcessBean authorizedBean = ChargesDAO.getCCDetails(clientAdminBO.getAssoccode(), tripID, 0);
							if(cmpBean.getCcProvider()==2){
								authorizedBean = PaymentGatewaySlimCD.makeProcess(authorizedBean,cmpBean, PaymentCatgories.Void);
							} else {
								authorizedBean = PaymentGateway.makeProcess(authorizedBean,cmpBean, PaymentCatgories.Void);
							}if(authorizedBean.isAuth_status()){
								ChargesDAO.voidTransAction(authorizedBean.getB_trans_id(), authorizedBean.getB_trans_id2(), "Job cancelled", clientAdminBO.getUserId());
							}
						}
						
						AuditDAO.insertJobLogs("Due to no drivers found, System-A cancelled this job", tripID, clientAdminBO.getAssoccode(), TDSConstants.companyCouldntService, "System-A", "", "", clientAdminBO.getAssoccode());
						System.out.println("Trip Cancelled - no drivers - "+tripID);
						jobObject.put("Act", "BookAJob");
						jobObject.put("status", 250);
						jobObject.put("message", "Job Cancelled");
						jobObject.put("TripId", "");
						jobObject.put("dId", "");
						jobObject.put("dN", "");
						jobArray.put(jobObject);
					}
				}else{
					if(clientAdminBO.getcCMandatory()==1 || OpenBO.getPaytype().equalsIgnoreCase("CC")){
						if(cmpBean.getCcProvider()==2){
							processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Void);
						} else {
							processBean = PaymentGateway.makeProcess(processBean,cmpBean, PaymentCatgories.Void);
						}
						if(processBean.isAuth_status()){
							ChargesDAO.voidTransAction(processBean.getB_trans_id(), processBean.getB_trans_id2(), "Unable to create a job", clientAdminBO.getUserId());
						}
					}
					JSONObject jobObject = new JSONObject();
					jobObject.put("Act", "BookAJob");
					jobObject.put("status", 300);
					jobObject.put("message", "Error to Book");
					jobArray.put(jobObject);
				}
				if(request.getParameter("responseType").equalsIgnoreCase("JSON")){
					response.getWriter().write(jobArray.toString());
				}else{
					response.getWriter().write(result+"");
				}
				return;
			}
			catch(JSONException e){
				e.printStackTrace();
			}
		}

		if(result>1){
			if(request.getParameter("fromHome")!=null && request.getParameter("fromHome").equals("1")){
				if(session.getAttribute("clientUser")!=null){
					RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/HomeForLoggedIn.jsp");
					dispatcher.forward(request, response);
					return;
				}else{
					RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Home.jsp");
					dispatcher.forward(request, response);
					return;
				}
			}else if(request.getParameter("fromHome").equals("2")){
				response.getWriter().write(result+"");
				return;
			}

			else{
				request.setAttribute("error","Your Trip is Booked and the Unique Key is "+result);
				request.setAttribute("screen", "/ClientUsage/bookAJob.jsp");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/bookAJob.jsp");
				dispatcher.forward(request, response);
				return;
			} 
		}else {
			request.setAttribute("error","Failed to Book");
			request.setAttribute("screen", "/ClientUsage/bookAJob.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/bookAJob.jsp");
			dispatcher.forward(request, response);
			return;
		}
	}


	public void driverLocation (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
		if(clientAdminBO.getUserId()!=null){
			String tripId=request.getParameter("tripId")==null?"":request.getParameter("tripId");
			ArrayList<CustomerMobileBO> driverLocation=new ArrayList<CustomerMobileBO>();
			if(!tripId.equals("")){
				driverLocation=CustomerMobileDAO.getDriverLocation(clientAdminBO.getUserId(),tripId);
			}else{
				driverLocation=CustomerMobileDAO.getDriverLocation(clientAdminBO.getUserId(),"");
			}
			if(driverLocation.size()>0){
				JSONArray driverCurrentLocation = new JSONArray();
				for(int i=0;i<driverLocation.size();i++){
					JSONObject latLong = new JSONObject();
					try {
						latLong.put("Act", "DriverLocation");
						latLong.put("status", 200);
						latLong.put("Lat", driverLocation.get(i).getStLatitude()=="0.0"?"":driverLocation.get(i).getStLatitude());
						latLong.put("Lon", driverLocation.get(i).getStLongitude()=="0.0"?"":driverLocation.get(i).getStLongitude());
						latLong.put("DR", driverLocation.get(i).getDriver());
						latLong.put("CabNo", driverLocation.get(i).getVehicleNo());
						latLong.put("DrName", driverLocation.get(i).getName());
						latLong.put("VMake", driverLocation.get(i).getvMake());
						latLong.put("VModel", driverLocation.get(i).getvType());

						driverCurrentLocation.put(latLong);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				response.getWriter().write(driverCurrentLocation.toString());
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}
	public void jobLocation (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
		if(clientAdminBO.getUserId()!=null){
			String tripId=request.getParameter("tripId");
			String cCode=request.getParameter("assoccode");
			int appSrc = clientAdminBO.getAppSrc().equalsIgnoreCase("4")?4:0;

			if(tripId !=null && !tripId.equals("")){
				ArrayList<CustomerMobileBO> jobLocation=CustomerMobileDAO.getJobLocation(clientAdminBO.getUserId(),tripId,appSrc);
				JSONArray jobsLocation = new JSONArray();
				if(jobLocation.size()>0){
					for(int i=0;i<jobLocation.size();i++){
						JSONObject latLong = new JSONObject();
						try {
							latLong.put("Act", "JobLocation");
							latLong.put("status", 200);
							latLong.put("getMessage","DriverFound");
							latLong.put("getLatitude", jobLocation.get(i).getStLatitude());
							latLong.put("getLongitude", jobLocation.get(i).getStLongitude());
							latLong.put("stAdd", jobLocation.get(i).getAddress());
							latLong.put("edAdd", jobLocation.get(i).getEdAddress());
							latLong.put("getEdLati", jobLocation.get(i).getEdLatitude());
							latLong.put("getEdLongi", jobLocation.get(i).getEdLongitude());
							latLong.put("time", jobLocation.get(i).getServiceDate());

							latLong.put("SA", jobLocation.get(i).getStatus()==null?"":jobLocation.get(i).getStatus());
							latLong.put("TI", jobLocation.get(i).getTripId()==null?"":jobLocation.get(i).getTripId());
							//System.out.println("driver id in joblocation+"+jobLocation.get(i).getDriver());
							if(jobLocation.get(i).getDriver()!=null && !jobLocation.get(i).getDriver().equals("")){
								latLong.put("dFound","Yes");
								latLong.put("driver", jobLocation.get(i).getDriver());
								latLong.put("cab", jobLocation.get(i).getVehicleNo());
								latLong.put("dLat", jobLocation.get(i).getLatitude());
								latLong.put("dLongi", jobLocation.get(i).getLongitude());
								latLong.put("vReg", jobLocation.get(i).getVehRegNum());
								latLong.put("vMake", jobLocation.get(i).getvMake());
								latLong.put("vType", jobLocation.get(i).getvType());
								latLong.put("drName", jobLocation.get(i).getName());
								latLong.put("drPh", jobLocation.get(i).getPhoneNumber());
							}else{
								latLong.put("driver","");
								latLong.put("dFound","No");
							}
							jobsLocation.put(latLong);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}else{
					JSONObject latLong = new JSONObject();
					try {
						latLong.put("Act", "JobLocation");
						latLong.put("status", 200);
						latLong.put("getMessage","DriverFound");
						latLong.put("getLatitude", "");
						latLong.put("getLongitude","");
						latLong.put("SA", "1");
						latLong.put("TI", tripId);

						jobsLocation.put(latLong);
					}catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				//System.out.println("response:"+jobsLocation.toString());
				response.getWriter().write(jobsLocation.toString());
			}else{
				ArrayList<CustomerMobileBO> jobLocation=CustomerMobileDAO.getJobLocation(clientAdminBO.getUserId(),"",appSrc);
				if(jobLocation.size()>0){
					JSONArray jobsLocation = new JSONArray();
					for(int i=0;i<jobLocation.size();i++){
						JSONObject latLong = new JSONObject();
						try {
							latLong.put("Act", "JobLocation");
							latLong.put("status", 200);
							latLong.put("getMessage","Driver not found");
							latLong.put("getLatitude", jobLocation.get(i).getStLatitude());
							latLong.put("getLongitude", jobLocation.get(i).getStLongitude());
							latLong.put("SA", jobLocation.get(i).getStatus()==null?"":jobLocation.get(i).getStatus());
							latLong.put("TI", jobLocation.get(i).getTripId()==null?"":jobLocation.get(i).getTripId());

							jobsLocation.put(latLong);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					response.getWriter().write(jobsLocation.toString());
				}
			}
		}else{
			request.setAttribute("screen", "/ClientUsage/Login.jsp");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/ClientUsage/Login.jsp");
			dispatcher.forward(request, response);
		}
	}


	public void userIdCheck (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		//CustomerMobileDAO CMDAO =new CustomerMobileDAO();
		String userId=(request.getParameter("userId")==null?"":request.getParameter("userId"));
		int result= CustomerMobileDAO.userIdCheck(userId);
		response.getWriter().write(Integer.toString(result));
	}
	public void tagAddress (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setTagAddress(request.getParameter("tagName"));
		CMBO.setAddress(request.getParameter("address"));
		CMBO.setLatitude(request.getParameter("latitude"));
		CMBO.setLongitude(request.getParameter("longitude"));
		CMBO.setAssoccode(request.getParameter("assoccode"));
		//System.out.println("gettagAddress:"+CMBO.getAddress());
		JSONArray array = new JSONArray();
		JSONObject jobj = new JSONObject();
		int result=CustomerMobileDAO.tagAddress(CMBO);
		//int result=0;
		if(request.getParameter("returnType").equalsIgnoreCase("JSON")){
			try{
				if(result>0){
					jobj.put("Act", "TA");
					jobj.put("status", 200);
					jobj.put("Message", "Address sucessfully Added to Favourite");
					array.put(jobj);
				}else{
					jobj.put("Act", "TA");
					jobj.put("status", 400);
					jobj.put("Message", "Failed to Add Favorite Address");
					array.put(jobj);
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
			//System.out.println("favorite"+array.toString());
			response.getWriter().write(array.toString());
		}else{
			response.getWriter().write(result+"");
		}
	}
	public void checkAddress (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setAddress(request.getParameter("address"));
		int result=CustomerMobileDAO.checkAddress(CMBO);
		response.getWriter().write(Integer.toString(result));		
	}
	public void taggedAddressesToShow (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		ArrayList<CustomerMobileBO> taggedAddressesList =new ArrayList<CustomerMobileBO>();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setAssoccode(request.getParameter("assoccode"));
		taggedAddressesList=CustomerMobileDAO.taggedAddressestoShow(CMBO);
		JSONArray jsonArr = new JSONArray();
		if(taggedAddressesList.size()>0){
			for(int i=0;i<taggedAddressesList.size();i++){
				JSONObject jsonObj = new JSONObject();
				try {
					jsonObj.put("Act", "FA");
					jsonObj.put("status", 200);
					jsonObj.put("Msg", "Records Found");
					jsonObj.put("add",taggedAddressesList.get(i).getAddress());
					jsonObj.put("Name", taggedAddressesList.get(i).getTagAddress());
					jsonObj.put("Key", taggedAddressesList.get(i).getAddressKey());
					jsonObj.put("Lat", taggedAddressesList.get(i).getLatitude());
					jsonObj.put("Lon",taggedAddressesList.get(i).getLongitude());
					jsonArr.put(jsonObj);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}else{
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put("Act", "FA");
				jsonObj.put("status", 300);
				jsonObj.put("Msg", "No Records Found");
				jsonArr.put(jsonObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		//System.out.println("tagaddress"+jsonArr.toString());
		response.getWriter().write(jsonArr.toString());
		//response.getWriter().write(Integer.toString(result));		
	}
	public void favoriteAddresses(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		CustomerMobileBO CMBO=new CustomerMobileBO();
		ArrayList<CustomerMobileBO> taggedAddressesList =new ArrayList<CustomerMobileBO>();
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CMBO.setUserId(clientAdminBO.getUserId());
		CMBO.setAddress(request.getParameter("address"));
		taggedAddressesList=CustomerMobileDAO.taggedAddressestoShow(CMBO);
		JSONArray jsonArr = new JSONArray();
		for(int i=0;i<taggedAddressesList.size();i++){
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put("address",taggedAddressesList.get(i).getAddress());
				jsonObj.put("tagAdd", taggedAddressesList.get(i).getTagAddress());
				jsonObj.put("addKey", taggedAddressesList.get(i).getAddressKey());
				jsonObj.put("getLatitude", taggedAddressesList.get(i).getLatitude());
				jsonObj.put("getLongitude",taggedAddressesList.get(i).getLongitude());
				jsonArr.put(jsonObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(jsonArr.toString());

	}
	public void deleteFavAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String key=request.getParameter("addKey");
		JSONArray jsonArr = new JSONArray();
		int result=CustomerMobileDAO.deleteFavAddress(key,clientAdminBO);
		JSONObject jsonObj = new JSONObject();
		try {
			if(result>0){
				jsonObj.put("Act", "DFA");
				jsonObj.put("status", 200);
				jsonObj.put("getMessage", "Successfully Deleted");
				jsonArr.put(jsonObj);
			}else{
				jsonObj.put("Act", "DFA");
				jsonObj.put("status", 400);
				jsonObj.put("getMessage", "Un - Successfull");
				jsonArr.put(jsonObj);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		String returntype = request.getParameter("returnType")==null?"":request.getParameter("returnType");
		if(returntype.equalsIgnoreCase("JSON")){
			response.getWriter().write(jsonArr.toString());
		}else{
			response.getWriter().write(result+"");
		}
	}

	public void charges(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String tripId=request.getParameter("tripId");
		//System.out.println("tripId--->"+tripId);
		ArrayList<CustomerMobileBO>charges=CustomerMobileDAO.getCharges(tripId, clientAdminBO.getAssoccode());
		if(charges.size()>0){
			JSONArray chargesArray = new JSONArray();
			for(int i=0;i<charges.size();i++){
				JSONObject chargesObject = new JSONObject();
				try {
					chargesObject.put("getType", charges.get(i).getPaymentType());
					chargesObject.put("getAmount", charges.get(i).getAmount());
					chargesObject.put("getChargeType", charges.get(i).getChargesTypeCode());
					chargesArray.put(chargesObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			response.getWriter().write(chargesArray.toString());
		}
	}
	public void payFare(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		String tripId=request.getParameter("tripId");
		int result=CustomerMobileDAO.updateCharges( tripId, clientAdminBO.getAssoccode(),clientAdminBO.getAssoccode());
		response.getWriter().write(result+"");
	}
	public void showCards(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		//System.out.println("Baskar checking event");
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		ArrayList<CustomerMobileBO> cardArray=CustomerMobileDAO.getCreditCards(clientAdminBO.getUserId());
		if(cardArray.size()>0){
			JSONArray chargesArray = new JSONArray();
			for(int i=0;i<cardArray.size();i++){
				JSONObject chargesObject = new JSONObject();
				try {
					chargesObject.put("getCard", cardArray.get(i).getCardNumber());
					chargesObject.put("getNumber", cardArray.get(i).getSerialNo());
					chargesArray.put(chargesObject);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			response.getWriter().write(chargesArray.toString());

		}
	}

	public void getPushID(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		String pushKey="";
		pushKey=TDSProperties.getValue("GCMPID");
		response.getWriter().write("ResVer=1.1;GPId="+pushKey);	
		String register=request.getParameter("register");
		HttpSession session = request.getSession(false);
		if(register!=null){
			ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
			pushKey=request.getParameter("pushKey");
			CustomerMobileDAO.updateGCMKey(clientAdminBO.getUserId(), pushKey);
		}
	}

	public void cancelTrip(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		JSONArray array = new JSONArray();
		JSONObject jobj = new JSONObject();
		String tripId=request.getParameter("tripId");
		int src = clientAdminBO.getAppSrc().equalsIgnoreCase("4")?4:0;
		PaymentProcessBean authorizedBean = null;
		int result=CustomerMobileDAO.cancelReservation(clientAdminBO.getUserId(),tripId,src);
		if(request.getParameter("returnType").equalsIgnoreCase("JSON")){
			try{
				if(result==1){
					ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
					AdminRegistrationBO adminBo=new AdminRegistrationBO();
					adminBo.setMasterAssociateCode(clientAdminBO.getAssoccode());
					clientAdminBO.setTimeZone(clientAdminBO.getTimeZone()==null?"US/Eastern":clientAdminBO.getTimeZone());
					OpenRequestBO openBo = CustomerMobileDAO.getHistory(tripId);
					if (openBo!=null) {
						if(openBo.getPaytype().equalsIgnoreCase("CC")){
							authorizedBean = ChargesDAO.getCCDetails(clientAdminBO.getAssoccode(), tripId, 0);
							CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(clientAdminBO.getAssoccode());
							if(cmpBean.getCcProvider()==2){
								authorizedBean = PaymentGatewaySlimCD.makeProcess(authorizedBean,cmpBean, PaymentCatgories.Void);
							} else {
								authorizedBean = PaymentGateway.makeProcess(authorizedBean,cmpBean, PaymentCatgories.Void);
							}if(authorizedBean.isAuth_status()){
								ChargesDAO.voidTransAction(authorizedBean.getB_trans_id(), authorizedBean.getB_trans_id2(), "Job cancelled", clientAdminBO.getUserId());
							}
						}
						if(!openBo.getDriverid().equals("")){
							DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID(openBo.getAssociateCode(), openBo.getDriverid(), 1);
							String[] message = MessageGenerateJSON.generateMessage(openBo, "CC", System.currentTimeMillis(), openBo.getAssociateCode());
							Messaging oneSMS = new Messaging(getServletContext(), cabQueueBean, message, poolBO, openBo.getAssociateCode());
							oneSMS.start();
						}
					}
					AuditDAO.insertJobLogs("Job Cancelled By Customer", openBo.getTripid(), clientAdminBO.getAssoccode(), TDSConstants.customerCancelledRequest, clientAdminBO.getUserId(), openBo.getEdlatitude(), openBo.getEdlongitude(), clientAdminBO.getAssoccode());
					jobj.put("Act", "CT");
					jobj.put("status", 200);
					jobj.put("Message", "Trip cancelled successfully");
					array.put(jobj);
				}else{
					jobj.put("Act", "CT");
					jobj.put("status", 400);
					jobj.put("Message", "Trip havent cancelled");
					array.put(jobj);
				}
			}catch(JSONException e){
				e.printStackTrace();
			}
			response.getWriter().write(array.toString());
		}else{
			response.getWriter().write(result+"");
		}
	}
	public void changeStatus (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		boolean status=false;
		if(request.getParameter("status").equals("1")){
			status=true;
		}
		int result=CustomerMobileDAO.updateJobByPassenger(clientAdminBO.getAssoccode(), request.getParameter("tripId"), status);
		response.getWriter().write(result+"");		
	}
	public void deleteJobSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		int result=CustomerMobileDAO.deleteOpenRequest(request.getParameter("tripId"), clientAdminBO.getAssoccode(), "1");
		response.getWriter().write(result+"");		
	}
	public void getFlagsForCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception{
		ArrayList<DriverVehicleBean> dvList=new ArrayList<DriverVehicleBean>();
		HashMap<String, DriverVehicleBean> companyFlags =UtilityDAO.getCmpyVehicleFlag(request.getParameter("compCode"));
		Set hashMapEntries = companyFlags.entrySet();
		Iterator it = hashMapEntries.iterator();
		while(it.hasNext()){
			Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
			dvList.add(companyFlag.getValue());
		}
		JSONArray flagList = new JSONArray();
		for(int i=0;i<dvList.size();i++){
			JSONObject flagDetails = new JSONObject();
			try {
				flagDetails.put("Act", "CompanyFlags");
				flagDetails.put("status", 200);
				flagDetails.put("LD", dvList.get(i).getLongDesc());
				flagDetails.put("SW", dvList.get(i).getDriverVehicleSW());
				flagDetails.put("K",dvList.get(i).getKey());
				flagDetails.put("SD", dvList.get(i).getShortDesc());
				flagDetails.put("GI", dvList.get(i).getGroupId());
				flagList.put(flagDetails);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.getWriter().write(flagList.toString());
	}
	public void driverList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ArrayList<DriverCabQueueBean> driverBO = DispatchDAO.allDrivers(request.getParameter("assoccode"), 0.00, 0.00, 0.00, null, "");
		//ArrayList<DriverCabQueueBean> driverBO = DispatchDAO.allDriversWithInADistance(request.getParameter("assoccode"), Double.parseDouble(request.getParameter("sLat").equals("")?"0.00":request.getParameter("sLat")), Double.parseDouble(request.getParameter("sLong").equals("")?"0.00":request.getParameter("sLong")), 50.00, null, 1);
		ArrayList<String> fleets = new ArrayList<String>();
		String code = request.getParameter("assoccode");
		fleets = SecurityDAO.getAllAssoccode(code);
		//System.out.println("fleets:"+fleets);
		double distanceInMiles = Double.parseDouble(request.getParameter("distance")!=null?request.getParameter("distance"):"10000.00");
		ArrayList<DriverCabQueueBean> driverBO = DispatchDAO.allDriversWithInADistance(fleets, Double.parseDouble(request.getParameter("sLat").equals("")?"0.00":request.getParameter("sLat")), Double.parseDouble(request.getParameter("sLong").equals("")?"0.00":request.getParameter("sLong")), distanceInMiles, null, 1,1000.00);
		JSONArray customerAccountRegistration= new JSONArray();
		if(driverBO.size()>0){
			for (int i = 0; i < driverBO.size(); i++) {
				JSONObject registrationStatus = new JSONObject();
				try {
					registrationStatus.put("Act","DriverList");
					registrationStatus.put("status",200);
					registrationStatus.put("Message", "Drivers Found");
					registrationStatus.put("Lat",driverBO.get(i).getCurrentLatitude());
					registrationStatus.put("Lon",driverBO.get(i).getCurrentLongitude());
					registrationStatus.put("CabNo", driverBO.get(i).getVehicleNo());
					registrationStatus.put("DR", driverBO.get(i).getDriverid());
					registrationStatus.put("DrName", driverBO.get(i).getDrivername());
					registrationStatus.put("VMake", driverBO.get(i).getvMake());
					registrationStatus.put("VModel", driverBO.get(i).getvType());

					customerAccountRegistration.put(registrationStatus);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}else{
			JSONObject registrationStatus = new JSONObject();
			try {
				registrationStatus.put("Act","DriverList");
				registrationStatus.put("status",400);
				registrationStatus.put("Message", "Drivers Not Found");

				customerAccountRegistration.put(registrationStatus);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.setContentType("application/json");
		response.getWriter().write(customerAccountRegistration.toString());	
	}

	private void shortestDriver(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		String associationCode = request.getParameter("assoccode");
		double latitude = Double.parseDouble(request.getParameter("sLat"));
		double longitude = Double.parseDouble(request.getParameter("sLong"));
		double distanceInMiles = Double.parseDouble(request.getParameter("distance"));
		ArrayList<DriverCabQueueBean> listOfDrivers = CustomerMobileDAO.getShortestDriver(associationCode, latitude, longitude, distanceInMiles);

		if(listOfDrivers==null || listOfDrivers.size()<1){
			response.getWriter().write("");
			return;
		}
		JSONArray array = new JSONArray();
		try{
			for(int i=0; i<listOfDrivers.size();i++){
				JSONObject jobj = new JSONObject();
				jobj.put("driverId", listOfDrivers.get(i).getDriverid());
				jobj.put("lat", listOfDrivers.get(i).getCurrentLatitude());
				jobj.put("lon", listOfDrivers.get(i).getCurrentLongitude());
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	public void getDriverDocument(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		String driverId = request.getParameter("driverId") != null ? request.getParameter("driverId") : "";
		String result = "";
		JSONArray array = new JSONArray();
		JSONObject jobj = new JSONObject();
		result = CustomerMobileDAO.getDriverDocument(driverId);
		//System.out.println("reuslt"+result);
		try{
			if(!result.equalsIgnoreCase("")){
				jobj.put("Act", "DD");
				jobj.put("status", 200);
				jobj.put("getmessage", "Have document");
				jobj.put("result", result);
				array.put(jobj);
			}else{
				jobj.put("Act", "DD");
				jobj.put("status", 300);
				jobj.put("getmessage", "No document");
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		String returntype = request.getParameter("returnType")==null?"":request.getParameter("returnType");
		if(returntype.equalsIgnoreCase("JSON")){
			response.getWriter().write(array.toString());
		}else{
			if(!result.equalsIgnoreCase("")){
				//System.out.println("sending result");
				response.getWriter().write(result);
			}else{
				response.getWriter().write("");
			}
		}

	}
	public void addCreditCard(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		ClientAuthenticationBO clientAdminBO=(ClientAuthenticationBO)session.getAttribute("clientUser");
		CompanyMasterBO cmpBean=SystemPropertiesDAO.getCCProperties(request.getParameter("assoccode"));
		PaymentProcessBean processBean = new PaymentProcessBean();
		int result=0;
		JSONArray cCRegistration = new JSONArray();
		JSONObject registrationStatus = new JSONObject();

		processBean.setCvv(request.getParameter("cvvCode"));
		processBean.setB_card_full_no(request.getParameter("ccNum"));
		processBean.setB_amount("0");
		processBean.setB_cardExpiryDate(request.getParameter("expDate"));
		processBean.setZip("");

		processBean.setB_cardHolderName(request.getParameter("name"));

		processBean.setB_tip_amt("0");
		processBean = PaymentGatewaySlimCD.makeProcess(processBean,cmpBean, PaymentCatgories.Load);
		String cardNumberToStore="";
		if(processBean.getB_card_full_no().length()>4){
			cardNumberToStore=processBean.getB_card_full_no().substring(processBean.getB_card_full_no().length()-4);
			cardNumberToStore="************"+cardNumberToStore;
		}
		if(processBean.isAuth_status()){
			result = CreditCardDAO.insertCreditCardDetails(clientAdminBO.getAssoccode(), clientAdminBO.getUserName(), cardNumberToStore, processBean.getB_cardHolderName(), processBean.getB_cardExpiryDate(), clientAdminBO.getCustomerUniqueId(), processBean.getB_trans_id());
		}
		if(result==1){
			try {
				registrationStatus.put("Act", "CcAdd");
				registrationStatus.put("status", 200);
				registrationStatus.put("getMessage","Successfully Registered");
				cCRegistration.put(registrationStatus);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			try {
				registrationStatus.put("Act", "CcAdd");
				registrationStatus.put("status", 400);
				registrationStatus.put("getMessage","Registration Un Successfull");
				cCRegistration.put(registrationStatus);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println(""+cCRegistration.toString());
		response.getWriter().write(cCRegistration.toString());
	}
	
	public void driverRating(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String driverId = request.getParameter("driverID") != null ? request.getParameter("driverID") : "";
		String userId = request.getParameter("UserId") != null ? request.getParameter("UserId") : "";
		String tripID = request.getParameter("tripID") != null ? request.getParameter("tripID") : "";
		String rating = request.getParameter("rating") != null ? request.getParameter("rating") : "";
		String comments = request.getParameter("Comments") != null ? request.getParameter("Comments") : "";
		String ccode = request.getParameter("ccode") != null ? request.getParameter("ccode") : "";
		String cabNo = request.getParameter("cabNo") != null ? request.getParameter("cabNo") : "";
		int result=0;
		JSONArray array = new JSONArray();
		JSONObject jobj = new JSONObject();
		System.out.println("Driver id : "+driverId);
		if(!driverId.equals("")){
			result = CustomerMobileDAO.driverrating(driverId, userId, tripID, rating, comments,ccode,cabNo);
			
			String wasl_ccode = TDSProperties.getValue("WASL_Company");
			if(result>0 && ccode.equals(wasl_ccode)){
				OpenRequestBO openBo = UtilityDAO.getWaslTripByTripId(ccode, tripID);
				if(openBo!=null){
					try {
						JSONObject wasl_jobj = new JSONObject(openBo.getCustomField());
						String jobRating = ""+(Double.parseDouble(rating) * 20);
						wasl_jobj.put("customerRating", jobRating);
						
						WaslIntegrationBO wasl_BO = TripRegistration.RegisterTripWASL(wasl_jobj.toString());
						//if(!wasl_response.equals("") && !wasl_response.contains("GRACIE") && !wasl_response.equals("Failed")){
						if(wasl_BO.isValid()){
							//Remove from WASl table and update reference key in history table
							int resilt = UtilityDAO.updateWaslReferencekey(ccode, openBo.getTripid(), wasl_BO.getReferenceNumber());
							if(resilt>0){
								System.out.println("WASL Reference key : "+wasl_BO.getReferenceNumber()+" updated for trip :"+openBo.getTripid());
							}else{
								System.out.println("WASL reference key : "+wasl_BO.getReferenceNumber()+" is failed for trip : "+openBo.getTripid());
							}
							
						}else{
							UtilityDAO.updateWaslRating(ccode, tripID, rating);
							System.out.println("WASL trip update failed for trip : "+openBo.getTripid()+" with message : "+wasl_BO.getError_message());
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						System.out.println("Exception:"+e.getMessage());
					}
				}else{
					System.out.println("WASL trip rating failed for trip:"+tripID);
				}
			}
		}
		try{
			if(result>0){
				jobj.put("Act", "DriverRating");
				jobj.put("status", 200);
				jobj.put("getMessage", "Success");
				array.put(jobj);
			}else{
				jobj.put("Act", "DriverRating");
				jobj.put("status", 300);
				jobj.put("getMessage", "Un-Success");
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}

		String returntype = request.getParameter("returnType")==null?"":request.getParameter("returnType");
		if(returntype.equalsIgnoreCase("JSON")){
			response.getWriter().write(array.toString());
		}else{
			response.getWriter().write(result+"");
		}
	}
	public void callDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession m_session = request.getSession();
		ClientAuthenticationBO adminBO = (ClientAuthenticationBO) m_session.getAttribute("clientUser");
		String prefix=SystemPropertiesDAO.getParameter(adminBO.getAssoccode(), "prefixPhone");
		String tripID = request.getParameter("tripID");
		String phoneNum = prefix+adminBO.getPhone();

		JSONArray array = new JSONArray();
		JSONObject jobj = new JSONObject();

		DriverLocationBO drBo=DispatchDAO.getPhoneForDriverID(request.getParameter("driverId"),adminBO.getAssoccode());
		String dPhone=prefix+drBo.getPhone();
		try{
			if(!drBo.getPhone().equals("") && !drBo.getPhone().equalsIgnoreCase("0000000000")){
				IVRDriverPassengerConf ivr = new IVRDriverPassengerConf(dPhone, adminBO.getAssoccode(),phoneNum,tripID,request.getParameter("driverId"));
				ivr.start();
				jobj.put("Act", "CallDriver");
				jobj.put("status", 200);
				jobj.put("getMessage", "Success");
				array.put(jobj);
			}else{
				jobj.put("Act", "CallDriver");
				jobj.put("status", 300);
				jobj.put("getMessage", "failue");
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

	public void getFareEstimation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession m_session = request.getSession();
		ClientAuthenticationBO adminBO = (ClientAuthenticationBO) m_session.getAttribute("clientUser");
		double startLati = Double.parseDouble(request.getParameter("stLat"));
		double startLongi = Double.parseDouble(request.getParameter("stLon"));
		double endtLati = Double.parseDouble(request.getParameter("edLat"));
		double endLongi = Double.parseDouble(request.getParameter("edLon"));

		CompanySystemProperties cspBO=SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getAssoccode());

		JSONArray array = new JSONArray();
		JSONObject jobj = new JSONObject();

		double totalDistance = DistanceCalculation.distance(startLati, endtLati, startLongi, endLongi);
		String amount = "$ "+Math.round((totalDistance*cspBO.getRatePerMile())+Double.parseDouble(cspBO.getStartAmount()))+"";
		try{
			if(totalDistance>0){
				jobj.put("Act", "estAmt");
				jobj.put("amt", amount);
				jobj.put("status", "PASS");
				array.put(jobj);
			} else {
				jobj.put("Act", "estAmt");
				jobj.put("amt", "$ 0");
				jobj.put("status", "FAIL");
				array.put(jobj);
			}
		}catch(JSONException e){
			e.printStackTrace();
		}
		response.getWriter().write(array.toString());
	}

}