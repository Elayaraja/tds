package com.tds.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tds.dao.SecurityDAO;
import com.tds.security.bean.TagSystemBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.PasswordGen;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class PassKey
 */
public class PassKey extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PassKey() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{	
		HttpSession session = request.getSession(false);
		AdminRegistrationBO adminBo=new AdminRegistrationBO();
		TagSystemBean tsBean = new TagSystemBean();
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		if(request.getParameter("button")!=null&&request.getParameter("button").equalsIgnoreCase("Tag this Computer")){
			tsBean.setPassword(request.getParameter("password"));
			if(session!=null){
				tsBean.setAssoCode(session.getAttribute("associationCode").toString());
				tsBean.setUserId(session.getAttribute("userId").toString());
			}
			tsBean = SecurityDAO.comparePassword(tsBean);
			if(tsBean.isPassW()){
				String tagId = PasswordGen.generatePassword("A", 16);
				tsBean.setTagId(tagId);
				int value = tsBean.getValidityDays();
				SecurityDAO.insertTagId(adminBo.getAssociateCode(), tsBean, 2);
				Cookie cookies= new Cookie("tagId",""+tsBean.getTagId());
				cookies.setMaxAge(value*24*60*60);
				response.addCookie(cookies);
				SecurityDAO.deletePassKey(adminBo.getAssociateCode(), tsBean);
				if(session!=null){
					session.invalidate();
				}
				request.setAttribute("screen","/rightOption.jsp");
				requestDispatcher.forward(request, response);
			}else {
				request.setAttribute("screen","/Security/Password.jsp");
				requestDispatcher.forward(request, response);
			}
		}


	}

}
