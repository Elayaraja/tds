package com.tds.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tds.dao.RegistrationDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;
public class AjaxPageNo extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public AjaxPageNo() {
        super();
    }
    @Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
		try {
			doProcess(request,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		
		try {
			doProcess(request, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String eventParam = "";
    	if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if((AdminRegistrationBO)request.getSession().getAttribute("user")!=null)
		{
			if(eventParam.equalsIgnoreCase("getPage")) 
			{
				getPage(request,response);
			}
	}}
    public void getPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		//PrintWriter out = response.getWriter(); 
		//StringBuffer buffer = new StringBuffer();
		ArrayList al_list = new ArrayList();
		int startValue=0;
		int endValue=20;
		int rowCount=Integer.parseInt(request.getParameter("rowCount"));
	 	if(rowCount>1){
			endValue = rowCount*20;
			startValue = endValue-20;
	 	}
		al_list = RegistrationDAO.getDriverDisbrushmentSummaryByLimit(TDSValidation.getDBdateFormat(request.getParameter("fdate")), TDSValidation.getDBdateFormat(request.getParameter("tdate")),request.getParameter("assocode"),request.getParameter("driver"),startValue,20);
			request.setAttribute("al_list",al_list);
			getServletContext().getRequestDispatcher(TDSConstants.DisbursementJSP).forward(request, response);

			}
}



