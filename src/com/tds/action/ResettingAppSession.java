package com.tds.action;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.util.ConfigurationValues;
import com.tds.util.ServiceTxnProcess;
import com.common.util.TDSProperties;

public class ResettingAppSession extends HttpServlet implements Servlet {
	
 
	public void reset(ServletConfig config) throws ServletException {
		 
		ApplicationPoolBO poolBO = (ApplicationPoolBO)config.getServletContext().getAttribute("poolBO");
		ServiceTxnProcess txnProcess;
		ConfigurationValues _CV;
		if(poolBO != null)
		{
			 
			//txnProcess = new ServiceTxnProcess();
			//_CV =  txnProcess.signOnWithToken(poolBO);
			//if(_CV._Status)
			//{
			//	poolBO.set_SessionToken(_CV._SessionToken);
			//	poolBO.set_SessionTokenDateTime(_CV._SessionTokenDateTime);
			//	poolBO.setStatus(_CV._Status);
			//} else {
			//	poolBO.set_SessionToken(poolBO.get_SessionToken());
			//	poolBO.set_SessionTokenDateTime(poolBO.get_SessionTokenDateTime());
			//	poolBO.setStatus(true);
			//}
			 
		} else {
			
			 
			poolBO = loadConfigValueToAppPool();
				 			
			txnProcess = new ServiceTxnProcess();
			 _CV =  txnProcess.signOnWithToken(poolBO);
			
			if(! _CV._Status)
				//System.out.println("Sign on token Failed");
			 
			poolBO.set_SessionToken(_CV._SessionToken);
			poolBO.set_SessionTokenDateTime(_CV._SessionTokenDateTime);
			poolBO.setStatus(_CV._Status);
		}
		
		config.getServletContext().setAttribute("poolBO", poolBO);
	}



	 
	public static ApplicationPoolBO loadConfigValueToAppPool()
	{
		
		ApplicationPoolBO poolBO = new ApplicationPoolBO();
		
		poolBO.set_primaryServiceEndpoint(TDSProperties.getValue("_primaryServiceEndpoint"));
		poolBO.set_primaryTxnEndpoint(TDSProperties.getValue("_primaryTxnEndpoint"));
		poolBO.set_secondaryServiceEndpoint(TDSProperties.getValue("_secondaryServiceEndpoint"));
		poolBO.set_secondaryTxnEndpoint(TDSProperties.getValue("_secondaryTxnEndpoint"));
		poolBO.set_IdentityToken(TDSProperties.getValue("_IdentityToken"));
		poolBO.set_PTLSSocketId(TDSProperties.getValue("_PTLSSocketId"));
		poolBO.set_ApplicationProfileId(TDSProperties.getValue("_ApplicationProfileId"));
		//poolBO.set_ServiceId(TDSProperties.getValue("_ServiceId"));
		//poolBO.set_MerchantProfileId(TDSProperties.getValue("_MerchantProfileId"));
		poolBO.setSMTP_HOST_NAME(TDSProperties.getValue("mailHostName"));
		poolBO.setSMTP_AUTH_USER(TDSProperties.getValue("mailUserName"));
		poolBO.setSMTP_AUTH_PWD(TDSProperties.getValue("mailPassword"));
		poolBO.setEmailFromAddress(TDSProperties.getValue("mailFromAddress"));
		poolBO.setProtocol(TDSProperties.getValue("mailTransportProtocal"));
		poolBO.setSMTP_PORT(TDSProperties.getValue("mailPortNo"));
		//System.out.println("Loaded PoolBO");
		
		return poolBO;
		
	}
	
}
