package com.tds.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tds.cmp.bean.CallerIDBean;
import com.tds.dao.CallerIDDAO;
import com.tds.dao.CreditCardDAO;
import com.tds.dao.SecurityDAO;
import com.tds.security.bean.TagSystemBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.VantivBO;
import com.common.util.PasswordGen;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

/**
 * Servlet implementation class SecurityAction
 */
public class SecurityAction extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SecurityAction() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String eventParam="";
		boolean sessionStatus=false;
		if(request.getParameter(TDSConstants.eventParam)!=null){
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if(session.getAttribute("user")!=null){
			if(request.getParameter(TDSConstants.eventParam).equals("passKeyGen")) {
				passwordGen(request, response);
			} else if(request.getParameter(TDSConstants.eventParam).equals("callerIdGen")) {
				callerIdGen(request, response);
			} else if(request.getParameter(TDSConstants.eventParam).equals("meterIdGen")) {
				meterIdGen(request, response);
			} else if(request.getParameter(TDSConstants.eventParam).equalsIgnoreCase("vantivSetup")) {
				vanticCC_Setup(request, response);
			}
	
		}
	}

public void passwordGen(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO=(AdminRegistrationBO)request.getSession().getAttribute("user");
		TagSystemBean tsBean = new TagSystemBean();

		if(request.getParameter("buttonValue")==null){
		}else if(request.getParameter("buttonValue").equalsIgnoreCase("Generate Password")){
			String password=PasswordGen.generatePassword("N",6);
			tsBean.setPassword(password);
			tsBean.setStartDate(TDSValidation.getDBdateFormat(request.getParameter("StartDate")));
			tsBean.setToDate(TDSValidation.getDBdateFormat(request.getParameter("ToDate")));
			tsBean.setUserId(request.getParameter("userId"));
			SecurityDAO.insertTagDetail(adminBO.getAssociateCode(), tsBean);
			request.setAttribute("password",password);
		}else if(request.getParameter("tag")!=null && request.getParameter("tag").equalsIgnoreCase("tag this computer")){
			//String password=request.getParameter("password");
			String tagId = PasswordGen.generatePassword("A", 16);
			tsBean.setTagId(tagId);
			SecurityDAO.insertTagId(adminBO.getAssociateCode(), tsBean,1);
			Cookie cookies= new Cookie("tagId",""+tsBean.getTagId());
			cookies.setMaxAge(365*68*24*60*60);
			response.addCookie(cookies);
			request.setAttribute("page","This Computer has been tagged for operator use");
		}else {
			if (!request.getParameter("size").equals("")){
				int result=0;
				int numberOfRows = Integer.parseInt(request.getParameter("size"));
				String[] IPAdd=new String[numberOfRows+1];
				String[] expiryDate=new String[numberOfRows+1];
				for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
					IPAdd[rowIterator]=request.getParameter("IPAddress"+rowIterator);
					expiryDate[rowIterator]=request.getParameter("expireDate"+rowIterator);
				}
				result=SecurityDAO.insertIPAdd(IPAdd,expiryDate,numberOfRows,adminBO);
			}else{
				request.setAttribute("page","Please enter the IP address and submit");

			}
		}

		ArrayList<TagSystemBean> ipAddressList=new ArrayList<TagSystemBean>();
		ipAddressList=SecurityDAO.getIPAdd(adminBO.getAssociateCode());
		request.setAttribute("ipAddressList", ipAddressList);
		request.setAttribute("screen","/Security/PasswordGen.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}


	public void callerIdGen(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO=(AdminRegistrationBO)request.getSession().getAttribute("user");
		CallerIDBean callerId=new CallerIDBean();
		if(request.getParameter("button")==null){
			callerId=CallerIDDAO.getUserIdPassword(adminBO);
			request.setAttribute("callerId",callerId);
			request.setAttribute("screen","/Security/CallerIdGen.jsp");
		}
		else if(request.getParameter("button").equalsIgnoreCase("Generate Password")){
			callerId.setUserID( PasswordGen.generatePassword("A", 16));
			callerId.setPassword(PasswordGen.generatePassword("A", 16));
			boolean Status= CallerIDDAO.storeUserIDPassword(callerId,adminBO);
			if(Status){
				request.setAttribute("callerId",callerId);
				request.setAttribute("screen","/Security/CallerIdGen.jsp");
			} else {
				request.setAttribute("callerId",callerId);
				request.setAttribute("errors","UserId And Password Doesnot Created");
				request.setAttribute("screen","/Security/CallerIdGen.jsp");
			}
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}
	
	public void meterIdGen(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO=(AdminRegistrationBO)request.getSession().getAttribute("user");
		String meterId="";
		if(request.getParameter("button")==null){
			meterId=CallerIDDAO.getPasswordId(adminBO.getMasterAssociateCode());
			request.setAttribute("meterId",meterId);
			request.setAttribute("screen","/Security/MeterControlGen.jsp");
		}
		else if(request.getParameter("button").equalsIgnoreCase("Generate Password")){
			meterId=PasswordGen.generatePassword("A", 16);
			//boolean Status= CallerIDDAO.storePasswordID(meterId,adminBO);
			boolean Status=true;
			if(Status){
				request.setAttribute("meterId",meterId);
				request.setAttribute("screen","/Security/MeterControlGen.jsp");
			} else {
				request.setAttribute("meterId",meterId);
				request.setAttribute("errors"," Password Doesnot Created");
				request.setAttribute("screen","/Security/MeterControlGen.jsp");
			}
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	private void vanticCC_Setup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		AdminRegistrationBO adminBO=(AdminRegistrationBO)request.getSession().getAttribute("user");
		
		VantivBO vantivBean = new VantivBO();
		if(request.getParameter("button")==null){
			vantivBean = CreditCardDAO.getVantivUserIdPassword(adminBO);
		}
		else if(request.getParameter("button").equalsIgnoreCase("Generate Password")){
			/*vantivBean.setUserId(PasswordGen.generatePassword("A", 16));
			vantivBean.setPassword(PasswordGen.generatePassword("A", 16));*/
			vantivBean.setUserId(request.getParameter("userId"));
			vantivBean.setPassword(request.getParameter("password"));
			boolean Status= CreditCardDAO.vantivInsertSetup(adminBO, vantivBean);
			if(!Status){
				request.setAttribute("errors","UserId And Password Doesnot Created");
			}
		}
		request.setAttribute("vantiv",vantivBean);
		request.setAttribute("screen","/Company/VantivCcSetup.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	
}
