package com.tds.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Category;

import com.tds.cmp.bean.CashSettlement;
import com.tds.controller.TDSController;
import com.tds.dao.FinanceDAO;
import com.tds.db.TDSConnection;
import com.common.util.PasswordHash;
/**
 * Servlet implementation class MigratePassword
 */
public class MigratePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MigratePassword() {
        super();
        // TODO Auto-generated constructor stub
    }
private static Category cat =TDSController.cat;
	
	static {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given DAO is "+FinanceDAO.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			doProcess(request,response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
    protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   // String allUser="0";
    String userId = "";
    	userId=request.getParameter("User");
    	TDSConnection dbcon = null;
		Connection csp_conn = null;
		PreparedStatement csp_pst = null;
		ResultSet rs = null;
		CashSettlement cashSettlementBean = new CashSettlement();
		cashSettlementBean.setRegisterOpen(false);
		String passwordHash="";
		String password="";
		try {
			dbcon = new TDSConnection();
			csp_conn = dbcon.getConnection();
			csp_pst = csp_conn.prepareStatement("SELECT * FROM TDS_ADMINUSER WHERE AU_USERNAME='"+userId+"'");
			rs = csp_pst.executeQuery();
			if(rs.next()){
				 password=rs.getString("AU_PASSWORD");
				 passwordHash=PasswordHash.encrypt(password);
			}
				csp_pst = csp_conn.prepareStatement("UPDATE TDS_ADMINUSER SET AU_PASSWORD='"+passwordHash+"' WHERE AU_USERNAME='"+userId+"'");
				csp_pst.execute();
		} catch (Exception sqex){
			cat.info("TDSException MigratePassword-->"+sqex.getMessage());
			sqex.printStackTrace();
    }finally { 
		dbcon.closeConnection();
	}
    }
}