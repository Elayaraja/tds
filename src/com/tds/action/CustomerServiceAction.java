package com.tds.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.cmp.bean.ComplaintDetailBean;
import com.tds.cmp.bean.ComplaintsBean;
import com.tds.controller.TDSController;
import com.tds.dao.CustomerServiceDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class CustomerServiceAction
 */
public class CustomerServiceAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;
   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CustomerServiceAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
		
	}
	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException 
	{
		String eventParam = "";
		
		if(p_request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = p_request.getParameter(TDSConstants.eventParam);
			
		}
		
		cat.info("Event Param "+eventParam);
		HttpSession m_session = p_request.getSession();
		
		 
					 
		if(m_session.getAttribute("user") != null) {
			
			if(eventParam.equalsIgnoreCase("createComplaints")) {
				insertComplaints(p_request, p_response);
			} else if(eventParam.equalsIgnoreCase("reviewComplaints")) {
				complaintSummary(p_request, p_response);
			}

		}
	}
	public void complaintSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		//String warning= "";
		String forwardURL = TDSConstants.getMainNewJSP;
		ComplaintsBean complaintsBean=new ComplaintsBean();
		AdminRegistrationBO adminBO=(AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("button")==null){
			request.setAttribute("screen","/CustomerService/Complaints.jsp");
		}else{
			complaintsBean.setAssoCode(adminBO.getAssociateCode());
			complaintsBean.setComplaintNo(request.getParameter("complaintNo")==null?"":request.getParameter("complaintNo"));
			complaintsBean.setDriverId(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
			complaintsBean.setPassengerPhone(request.getParameter("passengerPhone")==null?"":request.getParameter("passengerPhone"));
			complaintsBean.setSubject(request.getParameter("Subject")==null?"":request.getParameter("Subject"));
			complaintsBean.setStatus(request.getParameter("status")==null?"":request.getParameter("status"));
			ArrayList<ComplaintsBean> complaintList=CustomerServiceDAO.getComplaintSummaryFromMaster(complaintsBean,adminBO.getAssociateCode());
			request.setAttribute("getComplaintList", complaintList);
			request.setAttribute("screen","/CustomerService/Complaints.jsp");
		}

		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	public void insertComplaints(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		String forwardURL = TDSConstants.getMainNewJSP;
		ComplaintsBean complaintBean=new ComplaintsBean();
		ComplaintDetailBean complaintDetailBean=new ComplaintDetailBean();
		AdminRegistrationBO adminBO=(AdminRegistrationBO)request.getSession().getAttribute("user");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		String s1;
		String error=null;	
		if(request.getParameter("Edit")!=null){
			int complaintNo = Integer.parseInt(request.getParameter("complaintNo"));
			complaintBean =CustomerServiceDAO.getComplaintDetails(complaintNo,adminBO.getAssociateCode());
			ArrayList<ComplaintsBean> a_listDS=CustomerServiceDAO.getComplaintDetailsByComplaintNumber(complaintNo,complaintBean);
			request.setAttribute("a_listDS", a_listDS);
			s1= request.getParameter("complaintNo");
			request.setAttribute("s2",s1);
			request.setAttribute("complaints", complaintBean);
		}else if(request.getParameter("submit")!=null&&request.getParameter("submit").equals("submit")) {
			complaintBean.setAssoCode(adminBO.getAssociateCode());
			complaintBean.setDriverId(request.getParameter("DriverId"));
			complaintBean.setPassengerName(request.getParameter("passengerName"));
			complaintBean.setPassengerPhone(request.getParameter("passengerPhone"));
			//System.out.println("passenger phone"+complaintBean.getPassengerPhone());
			complaintBean.setSubject(request.getParameter("Subject"));
			complaintBean.setCreatedBy(adminBO.getUid());
			complaintBean.setTripId(request.getParameter("tripId"));
			complaintBean.setStatus(request.getParameter("status"));
			error = complaintsValidation(complaintBean);
			int complaintNo = CustomerServiceDAO.insertComplaintMaster(complaintBean,adminBO.getAssociateCode());
			complaintDetailBean.setDescription(request.getParameter("description"));
			complaintDetailBean.setComplaintNo(complaintNo);
			complaintDetailBean.setCreatedBy(adminBO.getUid());
			CustomerServiceDAO.insertComplaintDetail(complaintDetailBean, adminBO.getAssociateCode());
			request.setAttribute("error",error+complaintNo);


		}
		if(request.getParameter("Update")!=null&&request.getParameter("Update").equals("Update")) {
			complaintBean.setAssoCode(adminBO.getAssociateCode());
			complaintBean.setDriverId(request.getParameter("DriverId"));
			complaintDetailBean.setStatus(request.getParameter("status"));
			int i = Integer.parseInt(request.getParameter("Cno"));
			complaintDetailBean.setComplaintNo(i);
			CustomerServiceDAO.updateStatus(complaintDetailBean, adminBO.getAssociateCode());
			complaintDetailBean.setDescription(request.getParameter("description"));
			error=detailsValidation(complaintDetailBean);
			complaintDetailBean.setComplaintNo(i);
			complaintDetailBean.setCreatedBy(adminBO.getUid());
			request.setAttribute("error",error+complaintDetailBean.getComplaintNo());
			CustomerServiceDAO.insertComplaintDetail(complaintDetailBean, adminBO.getAssociateCode());

		}    
		request.setAttribute("screen","/CustomerService/InsertComplaints.jsp");
		requestDispatcher.forward(request, response);

	}
	public static String complaintsValidation(ComplaintsBean complaintBean) {
		StringBuffer errors = new StringBuffer();
		ComplaintDetailBean complaintDetailBean=new ComplaintDetailBean();
		if(complaintBean.getDriverId().equals("")) {
			errors.append("Enter driverId<br>");
		}
		if(complaintBean.getPassengerName().equals("")) {
			errors.append("Enter passengerName<br>");
		}
		if(complaintBean.getPassengerPhone().equals("")) {
			errors.append("Enter passengerPhone<br>");
		} 
		if(complaintBean.getStatus().equals("")) {
			errors.append("Enter status<br>");
		}
		if(complaintBean.getSubject().equals("")) {
			errors.append("Enter Subject<br>");
		}
		else{
			errors.append("Complaint Created for ComplaintNo:");
		}
		return errors.toString();
	}
	public static String detailsValidation(ComplaintDetailBean ComplaintDetailBean){
		StringBuffer errors = new StringBuffer();
		if(ComplaintDetailBean.getDescription().equals("")) {
			errors.append("Enter Description <br>");
		}
		else
			errors.append("Updated complaints for complaint NO : ");
		return errors.toString();
	}




}
