package com.tds.action;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;
import javazoom.upload.UploadParameters;

import org.apache.log4j.Category;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import com.tds.cmp.bean.CompanySystemProperties;
import com.tds.cmp.bean.MeterType;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.SystemUnavailableException;
import com.tds.controller.TDSController;
import com.tds.dao.RequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.ZoneDAO;
import com.tds.tdsBO.AdjacentZonesBO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.DispatchPropertiesBO;
import com.tds.tdsBO.DocumentBO;
import com.tds.tdsBO.FleetBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.OpenRequestFieldOrderBO;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.tds.tdsBO.WrapperBO;
import com.common.util.PasswordHash;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class SystemSetup
 */
public class SystemSetup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;

	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~doc~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ ServiceRequestAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SystemSetup() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException 
	{
		String eventParam = "";

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}

		cat.info("Event Param "+ eventParam);

		HttpSession session = request.getSession();

		if(session.getAttribute("user") != null) {

			if(eventParam.equalsIgnoreCase("systemProperties")) {			 
				setSystemProperties(request,response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.queuecoordinate)) {
				createQueueCoordinates(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.queuecoordinatesummary)) {
				queueCoOrdinateSummary(request, response);
			}else if (eventParam.equalsIgnoreCase("adjacentZones")) {
				adjacentZones(request, response);
			} else if (eventParam.equalsIgnoreCase("reviewAdjacentZones")){
				reviewAdjacentZones(request,response);
			} else if (eventParam.equalsIgnoreCase("landMarkPending")){
				landMarkPending(request,response);
			} else if (eventParam.equalsIgnoreCase("insertWrapper")){
				insertWrapper(request,response);
			} else if (eventParam.equalsIgnoreCase("updateWrapper")){
				updateWrapper(request,response);
			}else if (eventParam.equalsIgnoreCase("openRequestFieldOrder")){
				openRequestFieldOrder(request,response);
			}else if (eventParam.equalsIgnoreCase("dispatchProperties")){
				dispatchProperties(request,response);
			}else if (eventParam.equalsIgnoreCase("documentTypes")){
				insertDocumentTypes(request,response);
			}else if (eventParam.equalsIgnoreCase("reviewOpenRequestFieldOrder")){
				reviewOpenRequestFieldOrder(request,response);
			}else if (eventParam.equalsIgnoreCase("ccUsername")){
				insertCCDetails(request,response);
			}else if(eventParam.equalsIgnoreCase("userProfile")){
				userProfile(request,response);	
			}else if(eventParam.equalsIgnoreCase("fleetMapping")){
				fleetMapping(request,response);
			}else if (eventParam.equalsIgnoreCase("getZones")) {
				getZones(request, response);
			}else if (eventParam.equalsIgnoreCase("deleteZoneRate")) {
				deleteZoneRate(request, response);
			}

		}
	}
	public void setSystemProperties(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();	
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		
		CompanySystemProperties cspBean = new CompanySystemProperties();
		
		if(request.getParameter("Button") == null){
			cspBean = SystemPropertiesDAO.getCompanySystemPropeties(adminBO.getMasterAssociateCode());
			request.setAttribute("cspBO", cspBean);
			//request.setAttribute("screen", "/cab/SystemProperties.jsp");

			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/SystemSetup/SystemPropertiesUsingJqueryUI.jsp");	
			requestDispatcher.forward(request, response); 

		} else {
			cspBean.setAssociateCode(((AdminRegistrationBO)request.getSession().getAttribute("user")).getMasterAssociateCode());
			cspBean.setPreline1(request.getParameter("Preline1"));
			cspBean.setPreline2(request.getParameter("Preline2"));
			cspBean.setPreline3(request.getParameter("Preline3"));
			cspBean.setPreline4(request.getParameter("Preline4"));
			cspBean.setPreline5(request.getParameter("Preline5"));
			cspBean.setPostline1(request.getParameter("Postline1"));
			cspBean.setPostline2(request.getParameter("Postline2"));
			cspBean.setPostline3(request.getParameter("Postline3"));
			cspBean.setPostline4(request.getParameter("Postline4"));
			cspBean.setPostline5(request.getParameter("Postline5"));
			cspBean.setPostline6(request.getParameter("Postline6"));
			cspBean.setPostline7(request.getParameter("Postline7"));
			cspBean.setState(request.getParameter("State"));
			cspBean.setCountry(request.getParameter("country"));
			cspBean.setDefaultLanguage((request.getParameter("DefaultLanguage")!=null && !request.getParameter("DefaultLanguage").equals(""))?request.getParameter("DefaultLanguage"):"en");
			cspBean.setRatePerMile(Double.parseDouble(request.getParameter("ratePerKM")));
			cspBean.setTimeZoneArea(request.getParameter("timeZoneArea"));
			cspBean.setTagDeviceCab(Integer.parseInt(request.getParameter("tagDeviceCab")==null?"0":"1"));
			double hours=Double.parseDouble(request.getParameter("TimeZone"));
			double mins=Double.parseDouble(request.getParameter("TimeZoneMin"));
			double time;
			if(hours<0){
				time=hours-(mins/60);
			}else{
				time=hours+(mins/60);
			}
			cspBean.setTimezone(time);
			//System.out.println("Checking time Zone:  "+request.getParameter("TimeZone"));
			cspBean.setCheckCabNoOnLogin(request.getParameter("CheckCabNo"));
			cspBean.setCvvCodeManadatory(request.getParameter("EnterCVVcode"));
			cspBean.setAllowCreditCardReturns(request.getParameter("AllowCreditCardreturn"));
		//	cspBean.setDSR(request.getParameter("DSR"));
			cspBean.setCallerId(Integer.parseInt(request.getParameter("CallerId")));
			cspBean.setProvidePhoneNum(Integer.parseInt(request.getParameter("providePhoneNum")));
			cspBean.setProvideEndAddress(Integer.parseInt(request.getParameter("endAddress")));
			cspBean.setDriveralarm(Integer.parseInt(request.getParameter("driverAlarm")));
			cspBean.setZonesStepaway(Integer.parseInt((request.getParameter("zonesStep")==null)?"0":request.getParameter("zonesStep")));
			cspBean.setORTime(request.getParameter("ORTime"));
			cspBean.setCalculateZones(Boolean.parseBoolean(request.getParameter("calculateZones")));
			cspBean.setRatePerMile(Double.parseDouble(request.getParameter("ratePerKM")));
			cspBean.setTotalDigitsForPh(request.getParameter("noOfDigitsPh"));
			cspBean.setCurrencyPrefix(Integer.parseInt(request.getParameter("currencyPrefix")));
			cspBean.setMinimumSpeed(request.getParameter("minimumSpeed"));
			cspBean.setStartAmount(request.getParameter("startAmount"));
			cspBean.setSimultaneousLogin(Integer.parseInt(request.getParameter("simultaneousLogin")));
			cspBean.setAddressFill(Integer.parseInt(request.getParameter("addressCheck")));
			cspBean.setMobilePassword(request.getParameter("mobilePassword"));
			cspBean.setDriverListMobile(Integer.parseInt(request.getParameter("mobileDriver")));
			cspBean.setDistanceBasedOn(Integer.parseInt(request.getParameter("distanceBasedOn")));
			cspBean.setMeterMandatory(Boolean.parseBoolean(request.getParameter("meterMandatory")));
			cspBean.setStartMeterAutomatically(Boolean.parseBoolean(request.getParameter("autoStartMeter")));
			cspBean.setStartmeterHidden(Boolean.parseBoolean(request.getParameter("startMeterHidden")));
			cspBean.setWindowTime(request.getParameter("windowTime"));
			cspBean.setLoginLimitTime(request.getParameter("loginTimeLimit"));
			cspBean.setNoProgress(Integer.parseInt(request.getParameter("noProgress")==null?"0":request.getParameter("noProgress")));
			cspBean.setMeterOn(Integer.parseInt(request.getParameter("meterOn")==null?"0":request.getParameter("meterOn")));
			cspBean.setRemoteNoTrip(Integer.parseInt(request.getParameter("remoteNoTrip")==null?"0":request.getParameter("remoteNoTrip")));
			cspBean.setNoTripTooSoon(Integer.parseInt(request.getParameter("noTripTooSoon")==null?"0":request.getParameter("noTripTooSoon")));
			cspBean.setDistanceNoProgress(Double.parseDouble(request.getParameter("distanceNoProgress")==null?"0.0":request.getParameter("distanceNoProgress")));
			cspBean.setDistanceMeterOn(Double.parseDouble(request.getParameter("distanceMeterOn")==null?"0.0":request.getParameter("distanceMeterOn")));
			cspBean.setDistanceRemoteNoTrip(Double.parseDouble(request.getParameter("distanceRemoteNOTrip")==null?"0.0":request.getParameter("distanceRemoteNOTrip")));
			cspBean.setDistanceNoTripTooSoon(Double.parseDouble(request.getParameter("distanceNoTripTooSoon")==null?"0.0":request.getParameter("distanceNoTripTooSoon")));
			cspBean.setAppRatePerMile(Double.parseDouble(request.getParameter("appRatePerMile")==null?"0.0":request.getParameter("appRatePerMile").equals("")?"0.0":request.getParameter("appRatePerMile")));
			cspBean.setAppRatePerMinute(Double.parseDouble(request.getParameter("appRatePerMinute")==null?"0.0":request.getParameter("appRatePerMinute").equals("")?"0.0":request.getParameter("appRatePerMinute")));
			cspBean.setAppStartRate(Double.parseDouble(request.getParameter("appStartRate")==null?"0.0":request.getParameter("appStartRate").equals("")?"0.0":request.getParameter("appStartRate")));
			cspBean.setDistanceValueToFare(Integer.parseInt(request.getParameter("distanceToFare")));
			cspBean.setZoneLogoutTime(Integer.parseInt(request.getParameter("zoneLogout").equals("")?"0":request.getParameter("zoneLogout")));
			cspBean.setMeterTypes(Integer.parseInt(request.getParameter("fieldSize")));
			cspBean.setOnsiteDist(Double.parseDouble(request.getParameter("onSiteDist").equals("")?"0.0":request.getParameter("onSiteDist")));
			cspBean.setFrequencyOfPosting(Integer.parseInt(request.getParameter("postFrequency").equals("")?"0":request.getParameter("postFrequency")));
			cspBean.setBookAutomatically(Boolean.parseBoolean(request.getParameter("bookAutomatically")));
			cspBean.setCheckBalance(Boolean.parseBoolean(request.getParameter("driverBalance")));
			cspBean.setDriverBalanceAmount(Double.parseDouble((request.getParameter("driverBalanceAmount")==null)?"0.00":request.getParameter("driverBalanceAmount")));
			cspBean.setMobilePrefix(request.getParameter("prefixNum")==null?"":request.getParameter("prefixNum"));
			cspBean.setSmsPrefix(request.getParameter("prefixSMS")==null?"":request.getParameter("prefixSMS"));
			cspBean.setJobAction(Integer.parseInt(request.getParameter("jobAction")));
			cspBean.setCcForCustomerApp(Integer.parseInt(request.getParameter("CustomerCCProcess")));
			cspBean.setPaymentForCustomerApp(Integer.parseInt(request.getParameter("Customerpayment")));
			cspBean.setETA_Calculate(Integer.parseInt(request.getParameter("ETA_Calculate")));
			cspBean.setCcDefaultAmt(request.getParameter("ccDefaultAmt")==null?"":request.getParameter("ccDefaultAmt"));
			cspBean.setBatchForSR(Integer.parseInt(request.getParameter("srBatchJob")==null?"0":request.getParameter("srBatchJob")));
			cspBean.setSendBCJ(Integer.parseInt(request.getParameter("bcJobs")==null?"0":request.getParameter("bcJobs")));
			cspBean.setSeeZoneStatus(Integer.parseInt(request.getParameter("seeZoneStatus")==null?"0":request.getParameter("seeZoneStatus")));
			cspBean.setCallerIdType(Integer.parseInt(request.getParameter("CIType")==null?"0":request.getParameter("CIType")));
			cspBean.setNo_show((request.getParameter("no_show")!=null && !request.getParameter("no_show").equals(""))?Integer.parseInt(request.getParameter("no_show")):0);
			cspBean.setDriverDirection(Integer.parseInt(request.getParameter("drDirection")==null?"0":request.getParameter("drDirection")));
			cspBean.setZone_login((request.getParameter("ns_Login")!=null && !request.getParameter("ns_Login").equals(""))?Integer.parseInt(request.getParameter("ns_Login")):0);
			cspBean.setFleetDispatchSwitch((request.getParameter("fleetDispatch")!=null && !request.getParameter("fleetDispatch").equals(""))?Integer.parseInt(request.getParameter("fleetDispatch")):0);
			cspBean.setStoreGPSHistory((request.getParameter("storeHistory")!=null)?Integer.parseInt(request.getParameter("storeHistory")):0);
			cspBean.setCheckDriverMobile((request.getParameter("checkDriverMobile")!=null)?Integer.parseInt(request.getParameter("checkDriverMobile")):0);
			cspBean.setCheckDr_Mob_Version((request.getParameter("checkDrMobVersion")!=null)?Integer.parseInt(request.getParameter("checkDrMobVersion")):0);
			cspBean.setSendPushToCustomer((request.getParameter("sendPushToCustomer")!=null)?Integer.parseInt(request.getParameter("sendPushToCustomer")):0);
			cspBean.setEnable_stAdd_flagTrip((request.getParameter("staddress_flagtrip")!=null)?Integer.parseInt(request.getParameter("staddress_flagtrip")):0);
			cspBean.setPower_mode((request.getParameter("power_mode")!=null)?Integer.parseInt(request.getParameter("power_mode")):0);
			cspBean.setEnable_power_save((request.getParameter("enable_power_save")!=null)?Integer.parseInt(request.getParameter("enable_power_save")):1);
			if(request.getParameter("checkDocuments")==null){
				cspBean.setCheckDocuments(0);
			} else {
				cspBean.setCheckDocuments(1);
			}
			//cspBean.setFutureCounts(Integer.parseInt(request.getParameter("futurecounts")));
			if(request.getParameter("futurecounts")==null){
				cspBean.setFutureCounts(0);
			} else {
				cspBean.setFutureCounts(1);
			}
			cspBean.setPaymentMandatory(request.getParameter("payment_mandatory")==null?"0":request.getParameter("payment_mandatory"));
			
			try
			{ 
				cspBean.setRatePerMinute(Double.parseDouble(request.getParameter("ratePerMinute")));
				cspBean.setDispatchBasedOnDriverOrVehicle(Integer.parseInt(request.getParameter("Dispatch")));
				cspBean.setTimeOutOperator(Integer.parseInt(request.getParameter("TimeOutOperator")));	
				cspBean.setRatePerMile(Double.parseDouble(request.getParameter("ratePerKM")));
				cspBean.setTimeOutDriver(Integer.parseInt(request.getParameter("TimeOutDriver")));
				cspBean.setORFormat(Integer.parseInt(request.getParameter("orFormat")==null?"0":request.getParameter("orFormat")));
			}
			catch(NumberFormatException ex)
			{
				ex.printStackTrace(); 
			}
			request.setAttribute("cspBO", cspBean);
			if(SystemPropertiesDAO.insertCmpnySysPro(cspBean)){
				request.setAttribute("errors", "Properties Updated");
				if(cspBean.getMeterTypes()>0){
					int numberOfRows = cspBean.getMeterTypes();
					ArrayList<MeterType> meterParameters = new ArrayList<MeterType>();
					for (int rowIterator = 0; rowIterator < numberOfRows; rowIterator++) {
						MeterType meterValues = new MeterType();
						meterValues.setMeterName(request.getParameter("meterName"+rowIterator));
						meterValues.setRatePerMile(new BigDecimal(request.getParameter("ratePerMile"+rowIterator).toString().equals("")?"0.00":request.getParameter("ratePerMile"+rowIterator).toString()));
						meterValues.setRatePerMin(new BigDecimal(request.getParameter("ratePerMin"+rowIterator).toString().equals("")?"0.00":request.getParameter("ratePerMin"+rowIterator).toString()));
						meterValues.setStartAmt(new BigDecimal(request.getParameter("startAmt"+rowIterator).toString().equals("")?"0.00":request.getParameter("startAmt"+rowIterator).toString()));
						meterValues.setMinSpeed(Integer.parseInt(request.getParameter("minSpeed"+rowIterator).equals("")?"0":request.getParameter("minSpeed"+rowIterator)));
						meterValues.setDis1(Integer.parseInt(request.getParameter("dist1"+rowIterator).equals("")?"0":request.getParameter("dist1"+rowIterator)));
						meterValues.setDis2(Integer.parseInt(request.getParameter("dist2"+rowIterator).equals("")?"0":request.getParameter("dist2"+rowIterator)));
						meterValues.setDis3(Integer.parseInt(request.getParameter("dist3"+rowIterator).equals("")?"0":request.getParameter("dist3"+rowIterator)));
						meterValues.setRate1(new BigDecimal(request.getParameter("rate1"+rowIterator).toString().equals("")?"0.00":request.getParameter("rate1"+rowIterator).toString()));
						meterValues.setRate2(new BigDecimal(request.getParameter("rate2"+rowIterator).toString().equals("")?"0.00":request.getParameter("rate2"+rowIterator).toString()));
						meterValues.setRate3(new BigDecimal(request.getParameter("rate3"+rowIterator).toString().equals("")?"0.00":request.getParameter("rate3"+rowIterator).toString()));
						meterParameters.add(meterValues);
					}
					int defaultMeter = Integer.parseInt(request.getParameter("defaultMeter").equals("")?"0":request.getParameter("defaultMeter"));
					SystemPropertiesDAO.insertMeterValues(adminBO.getMasterAssociateCode(),meterParameters,defaultMeter);
				} else {
					ArrayList<MeterType> meterParameters = new ArrayList<MeterType>();
					MeterType meterValues = new MeterType();
					meterValues.setMeterName("Default");
					meterValues.setRatePerMile(new BigDecimal(request.getParameter("ratePerKM").toString().equals("")?"0.00":request.getParameter("ratePerKM").toString()));
					meterValues.setRatePerMin(new BigDecimal(request.getParameter("ratePerMinute").toString().equals("")?"0.00":request.getParameter("ratePerMinute").toString()));
					meterValues.setStartAmt(new BigDecimal(request.getParameter("startAmount").toString().equals("")?"0.00":request.getParameter("startAmount").toString()));
					meterValues.setMinSpeed(Integer.parseInt(request.getParameter("minimumSpeed").equals("")?"0":request.getParameter("minimumSpeed")));
					meterParameters.add(meterValues);
					int defaultMeter = Integer.parseInt("1");
					SystemPropertiesDAO.insertMeterValues(adminBO.getMasterAssociateCode(),meterParameters,defaultMeter);
				}
				SystemUtils.reloadCompanyFlags(this, adminBO.getMasterAssociateCode());
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/SystemSetup/SystemPropertiesUsingJqueryUI.jsp");
				requestDispatcher.forward(request, response); 
			} else {
				request.setAttribute("errors", "Couldnt Update Properties");
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/SystemSetup/SystemPropertiesUsingJqueryUI.jsp");
				requestDispatcher.forward(request, response); 
			}
		}


	}
	public void createQueueCoordinates(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In createQueueCoordinates method");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		HttpSession session = request.getSession();
		List queueList = null;
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); ;
		QueueCoordinatesBO queueBO = new QueueCoordinatesBO();
		String errors = "",screen= TDSConstants.queueCoordinateJSP,queueId = "";
		int dbStatus = 0;
		if(session.getAttribute("user") != null) {
			adminBO = (AdminRegistrationBO) session.getAttribute("user");
			queueBO.setQAssocCode(adminBO.getMasterAssociateCode());
		}
		if(request.getParameter("update") != null) {
			queueBO = setQueueCoOrdinate(request,queueBO);
			errors = checkQueueCoordinate(queueBO);
		}
		if(request.getParameter("update") !=null) {
			queueBO.setUpdateStaus(true);
		}
		if(errors.length() > 0) {
			//System.out.println("In missed datas error");
			request.setAttribute("queueCoOrdinateBO", queueBO);
			request.setAttribute("errors", errors);
			request.setAttribute("screen", screen);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);

		} else if(request.getParameter("save") != null) {
			//System.out.println("In Save condition");
			//if(SystemPropertiesDAO.editQueueCoordinates(queueBO,dbStatus, "")) {

			//cat.info("Successfully inserted Queue Co ordinates");
			String mapscreen=TDSConstants.getMapJSP;
			request.setAttribute("screen", mapscreen);
			getServletContext().getRequestDispatcher(TDSConstants.getMapJSP).forward(request, response);
			/*} else {
				//System.out.println("Failed to insert Queue Co ordinates");
				cat.info("Failed to insert Queue Co ordinates");
				request.setAttribute("queueCoOrdinateBO", queueBO);
				request.setAttribute("screen", screen);
				//request.setAttribute("errors", "Failed to insert Queue/Zone " + queueBO.getQueueId());
				getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
			//}
			 */
		} else if(request.getParameter("update") !=null) {
			//System.out.println("In update condition");
			dbStatus = 1;
			String Zone=request.getParameter("zoneNo");
			if(SystemPropertiesDAO.editQueueCoordinates(queueBO,dbStatus, Zone)) {
				String zoneId = request.getParameter("queueid");
				ArrayList<Double> latArray = new ArrayList<Double>();
				ArrayList<Double> lonArray = new ArrayList<Double>();
				ArrayList<Double> maxAndMinLatitude = new ArrayList<Double>();
				ArrayList<Double> maxAndMinLongitude = new ArrayList<Double>();
				ArrayList<ZoneTableBeanSP> allzones =  ZoneDAO.readZonesBoundries(adminBO.getMasterAssociateCode(),zoneId, false);
				ZoneDAO.readQueueBoundries(adminBO.getMasterAssociateCode(), zoneId, latArray, lonArray,maxAndMinLatitude,maxAndMinLongitude);
				double[] latitudes = new double[latArray.size()];
				double[] longitudes = new double[latArray.size()];
				for(int i =0; i< latArray.size(); i++){
					latitudes[i] = latArray.get(i);
					longitudes[i] = lonArray.get(i);
				}
				request.setAttribute("zones", allzones);
				request.setAttribute("latitude", latitudes);
				request.setAttribute("longitude", longitudes);
				ArrayList<Double> externalLatAnddLon=ZoneDAO.readQueueExternalPoint(adminBO.getMasterAssociateCode(), zoneId);
				request.setAttribute("externalLatitudeAndLongitude", externalLatAnddLon);
				String mapscreen=TDSConstants.getReviewMapJSP;
				request.setAttribute("screen", mapscreen);
				getServletContext().getRequestDispatcher(TDSConstants.getReviewMapJSP).forward(request, response);
			} else {
				//System.out.println("Failed to Updated Queue Co ordinates");
				cat.info("Failed to Updated Queue Co ordinates");
				response.sendRedirect("control?action=registration&event=createqueueCoordinate&errors=Failed to update queue&module=systemsetupView");
			}

		} else if(request.getParameter("queuestatus") != null && request.getParameter("queueid") != null) {
			//System.out.println("getting data from queue Summary");
			if(request.getParameter("queueid") != null) {
				queueId = request.getParameter("queueid");
			}
			queueList = SystemPropertiesDAO.getQueueCoOrdinateList(queueId, queueBO.getQAssocCode(),"");
			//System.out.println("List "+queueList);
			if(queueList != null && queueList.size() > 0 ) {
				queueBO = (QueueCoordinatesBO) queueList.get(0);

				queueBO.setUpdateStaus(true);
				request.setAttribute("queueCoordinate", queueBO);
			}
			request.setAttribute("screen", screen);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		} else if (request.getParameter("makeZone") == null && request.getParameter("reset") == null && request.getParameter("delete")==null) {
			request.setAttribute("screen", screen);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}
		if (request.getParameter("makeZone") != null) {
			queueBO = setQueueCoOrdinate(request,queueBO);
			int numberOfRows = Integer.parseInt(request.getParameter("size"));
			String ZoneNumber=request.getParameter("zoneName");
			double[] latitude = new double[numberOfRows+1];
			double[] longitude = new double[numberOfRows+1];
			for (int rowIterator = 0; rowIterator < numberOfRows; rowIterator++) {
				if (request.getParameter("latbox"+rowIterator) != null) {
					latitude[rowIterator] = Double.parseDouble(request.getParameter("latbox"+rowIterator));
					longitude[rowIterator] = Double.parseDouble(request.getParameter("lonbox"+rowIterator));
				}
			}
			latitude[numberOfRows] = latitude[0]; 
			longitude[numberOfRows] = longitude[0];
			if(SystemPropertiesDAO.editQueueCoordinates(queueBO,dbStatus, "")){
				ZoneDAO.insertZone(latitude,longitude, adminBO.getMasterAssociateCode(),ZoneNumber,1);
				if (request.getParameter("outsideLat") != null) {
					//String outLatTemp = request.getParameter("outsideLat");

					double[] outLatitude = new double[1];
					double[] outLongitude = new double[1];
					outLatitude[0] = Double.parseDouble(request.getParameter("outsideLat")==""?"0.00":request.getParameter("outsideLat"));
					outLongitude[0] = Double.parseDouble(request.getParameter("outsideLon")==""?"0.00":request.getParameter("outsideLon"));
					ZoneDAO.insertZone(outLatitude,outLongitude, adminBO.getMasterAssociateCode(),ZoneNumber,2);
				}
				ZoneDAO.updateVersion(adminBO.getMasterAssociateCode(), 1);
				SystemUtils.reloadZones(this, adminBO.getMasterAssociateCode());
			} else {
				request.setAttribute("errors", "Failed to create zone "+queueBO.getQueueId());
			}
			request.setAttribute("screen", screen);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}
		if (request.getParameter("updateZone") != null) {
			int numberOfRows = Integer.parseInt(request.getParameter("rowSize"));
			//System.out.println("ROWS"+numberOfRows);
			String ZoneNumber=request.getParameter("zone");
			SystemPropertiesDAO.deleteBoundries(ZoneNumber, adminBO.getMasterAssociateCode());
			double[] latitude = new double[numberOfRows];
			double[] longitude = new double[numberOfRows];
			int rowIterator=0;
			for (int i=0; rowIterator < numberOfRows;rowIterator++) {
				if (request.getParameter("latbox_"+i) != null) {
					latitude[rowIterator] = Double.parseDouble(request.getParameter("latbox_"+i));
					longitude[rowIterator] = Double.parseDouble(request.getParameter("lonbox_"+i));
				}
				i=i+2;
			}
			
			if(	latitude[numberOfRows-1] != latitude[0] ){
				//System.out.println("new func");
				double[] latitude1 = new double[numberOfRows+1];
				double[] longitude1 = new double[numberOfRows+1];
				int i;
				for(i=0;i<latitude.length;i++){
					latitude1[i]=latitude[i];
					longitude1[i]=longitude[i];
				}
				latitude1[i] = latitude[0]; 
				longitude1[i] = longitude[0];
				ZoneDAO.insertZone(latitude1,longitude1, adminBO.getMasterAssociateCode(),ZoneNumber,1);
			}else{
				ZoneDAO.insertZone(latitude,longitude, adminBO.getMasterAssociateCode(),ZoneNumber,1);
			}
			//ZoneDAO.insertZone(latitude,longitude, adminBO.getMasterAssociateCode(),ZoneNumber,1);
			if (request.getParameter("externalLatitude_") != null) {
				double[] outLatitude = new double[1];
				double[] outLongitude = new double[1];
				outLatitude[0] = Double.parseDouble(request.getParameter("externalLatitude_")==""?"0.00":request.getParameter("externalLatitude_"));
				outLongitude[0] = Double.parseDouble(request.getParameter("externalLongitude_")==""?"0.00":request.getParameter("externalLongitude_"));
				ZoneDAO.insertZone(outLatitude,outLongitude, adminBO.getMasterAssociateCode(),ZoneNumber,2);
			}
			ZoneDAO.updateVersion(adminBO.getMasterAssociateCode(), 1);
			SystemUtils.reloadZones(this, adminBO.getMasterAssociateCode());
			request.setAttribute("screen", screen);
		}

		if(request.getParameter("reset")!=null){
			queueList = SystemPropertiesDAO.getQueueCoOrdinateList(queueId, adminBO.getMasterAssociateCode(), "");
			request.setAttribute("queue", queueList);
			request.setAttribute("screen", TDSConstants.queueSummaryJSP);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}else if(request.getParameter("delete")!=null){
			String zonenumber=request.getParameter("zone");
			int result=SystemPropertiesDAO.deleteZone(zonenumber,adminBO.getMasterAssociateCode());
			if(result==0){
				ArrayList<Double> latArray = new ArrayList<Double>();
				ArrayList<Double> lonArray = new ArrayList<Double>();
				ArrayList<Double> maxAndMinLatitude = new ArrayList<Double>();
				ArrayList<Double> maxAndMinLongitude = new ArrayList<Double>();
				ArrayList<ZoneTableBeanSP> allzones =  ZoneDAO.readZonesBoundries(adminBO.getMasterAssociateCode(),zonenumber, false);
				ZoneDAO.readQueueBoundries(adminBO.getMasterAssociateCode(), zonenumber, latArray, lonArray,maxAndMinLatitude,maxAndMinLongitude);
				double[] latitudes = new double[latArray.size()];
				double[] longitudes = new double[latArray.size()];
				for(int i =0; i< latArray.size(); i++){
					latitudes[i] = latArray.get(i);
					longitudes[i] = lonArray.get(i);
				}
				request.setAttribute("zones", allzones);
				request.setAttribute("latitude", latitudes);
				request.setAttribute("longitude", longitudes);
				ArrayList<Double> externalLatAnddLon=ZoneDAO.readQueueExternalPoint(adminBO.getMasterAssociateCode(), zonenumber);
				request.setAttribute("externalLatitudeAndLongitude", externalLatAnddLon);
				String mapscreen=TDSConstants.getReviewMapJSP;
				request.setAttribute("errors", "Failed to delete zone. Jobs may be pending.");
				request.setAttribute("screen", mapscreen);
				getServletContext().getRequestDispatcher(TDSConstants.getReviewMapJSP).forward(request, response);
			} else {
				ZoneDAO.updateVersion(adminBO.getMasterAssociateCode(), 1);
				SystemUtils.reloadZones(this, adminBO.getMasterAssociateCode());
				queueList = SystemPropertiesDAO.getQueueCoOrdinateList(queueId, adminBO.getMasterAssociateCode(), "");
				request.setAttribute("queue", queueList);
				request.setAttribute("screen", TDSConstants.queueSummaryJSP);
				getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
			}
		}		

	}
	public void queueCoOrdinateSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		String queueId = "";
		String screen = TDSConstants.queueSummaryJSP;
		String queueDescription="";
		List queueList = null;
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); ;
		if(request.getParameter("getAllZones")==null && request.getParameter("delete")==null){
			if(request.getParameter("queueid") != null) {
				queueId = request.getParameter("queueid");
			}
			if(request.getParameter("qdesc") != null) {
				queueDescription = request.getParameter("qdesc");
			}
			queueList = SystemPropertiesDAO.getQueueCoOrdinateList(queueId, adminBO.getMasterAssociateCode(), queueDescription);
			request.setAttribute("queue", queueList);
			request.setAttribute("screen", screen);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}else if(request.getParameter("delete")!=null){
			String zonenumber=request.getParameter("zone");
			int result=SystemPropertiesDAO.deleteZone(zonenumber,adminBO.getMasterAssociateCode());
			queueList = SystemPropertiesDAO.getQueueCoOrdinateList(queueId, adminBO.getMasterAssociateCode(), queueDescription);
			if(result==0){
				request.setAttribute("errors", "Failed to delete zone "+zonenumber+". May be jobs pending for this zone");
			} else {
				ZoneDAO.updateVersion(adminBO.getMasterAssociateCode(), 1);
				SystemUtils.reloadZones(this, adminBO.getMasterAssociateCode());
			}
			request.setAttribute("queue", queueList);
			request.setAttribute("screen", screen);
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}else if(request.getParameter("getAllZones")!=null){
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getReviewMapAllJSP);
			requestDispatcher.forward(request, response); 
		}
	}


	protected QueueCoordinatesBO setQueueCoOrdinate(HttpServletRequest request, QueueCoordinatesBO qcoordinateBO) {
		qcoordinateBO.setQueueId(request.getParameter("queueid").trim());
		qcoordinateBO.setQDescription(request.getParameter("qdesc").trim());
		qcoordinateBO.setQDelayTime(request.getParameter("qdelay").trim());
		qcoordinateBO.setSmsResponseTime(request.getParameter("qresponse").trim());
		qcoordinateBO.setQLoginSwitch(request.getParameter("qloginswitch").trim());
		qcoordinateBO.setQDispatchType(request.getParameter("qdispatchtype").trim());
		qcoordinateBO.setQTypeofQueue(request.getParameter("qqueuetype").trim());
		qcoordinateBO.setQDispatchDistance(request.getParameter("qdispatchdistance").trim());
		qcoordinateBO.setQStartTime(request.getParameter("startTime").trim());
		qcoordinateBO.setQEndTime(request.getParameter("endTime").trim());
		qcoordinateBO.setQDayofWeek(request.getParameter("dayOfWeek").trim());
		qcoordinateBO.setQRequestBefore(request.getParameter("qrequestbef"));
		qcoordinateBO.setPhoneCallTime(request.getParameter("phoneCallDelayTime"));
		qcoordinateBO.setStaleCallTime(Integer.parseInt(request.getParameter("staleCallTime")==null?"0":request.getParameter("staleCallTime").equals("")?"0":request.getParameter("staleCallTime")));
		qcoordinateBO.setStaleCallbasedOn(Integer.parseInt(request.getParameter("staleCallBasedOn")==null?"0":request.getParameter("staleCallBasedOn")));
		qcoordinateBO.setMaxDistance(request.getParameter("maxAcceptDistance")==null?"10":request.getParameter("maxAcceptDistance"));
		qcoordinateBO.setQBdSwitch(Integer.parseInt(request.getParameter("bdSwitch")==null?"0":request.getParameter("bdSwitch").equals("")?"0":request.getParameter("bdSwitch")));
		if(request.getParameter("noPhoneCall").equals("0")){
			qcoordinateBO.setAdjacentZoneProperties(request.getParameter("adjacentZonesCriteria"));
		}else{
			qcoordinateBO.setAdjacentZoneProperties("-1");

		}
		if(request.getParameter("qbroad")=="null"){
			qcoordinateBO.setQBroadcastDelay(null);
		}else{
			qcoordinateBO.setQBroadcastDelay(request.getParameter("qbroad").trim());
		}

		return qcoordinateBO;
	}
	protected String checkQueueCoordinate(QueueCoordinatesBO queueCoordinateBO) {
		StringBuffer errors = new StringBuffer();
		if(queueCoordinateBO.getQueueId() != null && queueCoordinateBO.getQueueId().length() <= 0) {
			errors.append("Queue Id is missing<br>");
		}
		return errors.toString();
	}


	public void adjacentZones(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In Insert Adjacent Zones method");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); ;
		QueueCoordinatesBO zoneBO= new QueueCoordinatesBO();
		int result=0;

		if(request.getParameter("button")==null){
			request.setAttribute("screen","/SystemSetup/InsertAdjacentzones.jsp");
		}else if(request.getParameter("button")!=null){
			int numberOfRows = Integer.parseInt(request.getParameter("size"));
			//System.out.println("Num Of Rows--->"+numberOfRows);
			String[] zoneNumber=new String[numberOfRows+1];
			String[] adjacentZones=new String[numberOfRows+1];
			String[] orderNumber=new String[numberOfRows+1];
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				zoneNumber[rowIterator]=request.getParameter("zoneNumber"+rowIterator);
				adjacentZones[rowIterator]=request.getParameter("AdjacentZones"+rowIterator);
				orderNumber[rowIterator]=request.getParameter("orderNumber"+rowIterator);
			}
			result=SystemPropertiesDAO.insertAdjacentZones(zoneNumber,adjacentZones,orderNumber,numberOfRows,adminBO);
			if(result==0){
				request.setAttribute("errors","Enter all the Values correctly/Zone number is not Valid");
			}
			request.setAttribute("screen","/SystemSetup/InsertAdjacentzones.jsp");
			request.setAttribute("al_list","");
		}
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);

	}

	public void landMarkPending(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		OpenRequestBO openReqBo =new OpenRequestBO();
		String button=request.getParameter("button");
		if(button!=null) {
			openReqBo.setPhone(request.getParameter("phone"));
			openReqBo.setSlandmark(request.getParameter("landmark"));
			ArrayList<OpenRequestBO> landMark_Pending =SystemPropertiesDAO.getLandmarkPending(openReqBo,adminBO,0);
			request.setAttribute("ar_list", landMark_Pending);
		}
		request.setAttribute("screen","/cab/landMarkPending.jsp");
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}


	public void reviewAdjacentZones(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("In Review Adjacent Zones method");
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); ;
		AdjacentZonesBO zoneBO= new AdjacentZonesBO();
		if(request.getParameter("button")==null){
			request.setAttribute("screen","/SystemSetup/ReviewAdjacentzones.jsp");
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}else if(request.getParameter("button")!=null){
			zoneBO.setZoneNumber(request.getParameter("zoneNumber"));
			zoneBO.setAdjacentZones(request.getParameter("AdjacentZones"));
			zoneBO.setOrderNumber(request.getParameter("orderNumber"));
			ArrayList<AdjacentZonesBO> zone_list =SystemPropertiesDAO.ReviewAdjacentZones(zoneBO,adminBO);
			request.setAttribute("zone_list", zone_list);
			request.setAttribute("screen","/SystemSetup/ReviewAdjacentzones.jsp");
			if(request.getParameter("review")!=null){
				zoneBO.setZoneNumber(request.getParameter("zoneNum"));
				ArrayList<AdjacentZonesBO> zone_list1 =SystemPropertiesDAO.ReviewAdjacentZones(zoneBO,adminBO);
				ArrayList latConsolidatedArray=new ArrayList();
				ArrayList lonConsolidatedArray=new ArrayList();
				ArrayList latConsolidatedArray1=new ArrayList();
				ArrayList lonConsolidatedArray1=new ArrayList();
				ArrayList centerLat = new ArrayList();
				ArrayList centerLon = new ArrayList();
				ArrayList centerLat1 = new ArrayList();
				ArrayList centerLon1 = new ArrayList();
				for(int i=0;i < zone_list1.size();i++){
					ArrayList<Double> latArray=new ArrayList<Double>();
					ArrayList<Double> lonArray=new ArrayList<Double>();
					ArrayList<Double> maxAndMinLatitude = new ArrayList<Double>();
					ArrayList<Double> maxAndMinLongitude = new ArrayList<Double>();
					ZoneDAO.readQueueBoundries(adminBO.getMasterAssociateCode(),zone_list1.get(i).getAdjacentZones(), latArray, lonArray,maxAndMinLatitude,maxAndMinLongitude);
					latConsolidatedArray.add(latArray);
					lonConsolidatedArray.add(lonArray);
					double latCenter=(maxAndMinLatitude.get(0)+maxAndMinLatitude.get(1))/2;
					double lonCenter=(maxAndMinLongitude.get(0)+maxAndMinLongitude.get(1))/2;
					centerLat.add(latCenter);
					centerLon.add(lonCenter);
				}
				ArrayList<Double> latArray1=new ArrayList<Double>();
				ArrayList<Double> lonArray1=new ArrayList<Double>();
				ArrayList<Double> maxAndMinLatitude1 = new ArrayList<Double>();
				ArrayList<Double> maxAndMinLongitude1 = new ArrayList<Double>();
				ZoneDAO.readQueueBoundries(adminBO.getMasterAssociateCode(),zoneBO.getZoneNumber(), latArray1, lonArray1,maxAndMinLatitude1,maxAndMinLongitude1);
				latConsolidatedArray1.add(latArray1);
				lonConsolidatedArray1.add(lonArray1);
				double latCenter1=(maxAndMinLatitude1.get(0)+maxAndMinLatitude1.get(1))/2;
				double lonCenter1=(maxAndMinLongitude1.get(0)+maxAndMinLongitude1.get(1))/2;
				centerLat1.add(latCenter1);
				centerLon1.add(lonCenter1);

				request.setAttribute("zones", zone_list1);
				request.setAttribute("zoneParent", zoneBO.getZoneNumber());
				request.setAttribute("latitude",latConsolidatedArray);
				request.setAttribute("longitude",lonConsolidatedArray);
				request.setAttribute("latitude1",latConsolidatedArray1);
				request.setAttribute("longitude1",lonConsolidatedArray1);
				request.setAttribute("centreLatitude", centerLat);
				request.setAttribute("centreLongitude", centerLon);
				request.setAttribute("centreLatitude1", centerLat1);
				request.setAttribute("centreLongitude1", centerLon1);

				request.setAttribute("screen",TDSConstants.getAdjZoneMap);
			} else if(request.getParameter("edit")!=null){
				zoneBO.setZoneNumber(request.getParameter("zoneNum"));
				ArrayList<AdjacentZonesBO> zone_list1 =SystemPropertiesDAO.ReviewAdjacentZones(zoneBO,adminBO);
				request.setAttribute("zones", zone_list1);
				request.setAttribute("screen","/SystemSetup/InsertAdjacentzones.jsp");
			}
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
		}
	}

	public void insertWrapper(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); ;
		WrapperBO wrapper = new WrapperBO();
		String screen = "/SystemSetup/insertWrapper.jsp";
		String forwardURL =TDSConstants.getMainNewJSP;
		if(request.getParameter("btnWrapperInsert")!=null){
			wrapper.setDriverId(request.getParameter("txtDriverId")==null?"":request.getParameter("txtDriverId"));
			wrapper.setPassword(request.getParameter("pwdWrapper")==null?"":request.getParameter("pwdWrapper"));
			wrapper.setCabNumber(request.getParameter("txtCabNumber")==null?"":request.getParameter("txtCabNumber"));
			wrapper.setPhoneNumber(request.getParameter("txtPhoneNumber")==null?"":request.getParameter("txtPhoneNumber"));
			int result=0;
			if(wrapper.getDriverId()!="" && wrapper.getPassword()!="" && wrapper.getPhoneNumber()!="" && wrapper.getCabNumber()!="" ){
				result=SystemPropertiesDAO.insertWrapper(adminBO,wrapper,1,"");
			}else{
				String error = "Not created,Plz enter all the values";
				request.setAttribute("error",error);
			}
			if(result==1){
				String error = "Inserted successfully";
				request.setAttribute("error",error);
			}

		}else if(request.getParameter("btnWrapperUpdate")!=null){
			String driverId=request.getParameter("hdnDriverId");
			wrapper.setDriverId(request.getParameter("txtDriverId")==null?"":request.getParameter("txtDriverId"));
			wrapper.setPassword(request.getParameter("pwdWrapper")==null?"":request.getParameter("pwdWrapper"));
			wrapper.setCabNumber(request.getParameter("txtCabNumber")==null?"":request.getParameter("txtCabNumber"));
			wrapper.setPhoneNumber(request.getParameter("txtPhoneNumber")==null?"":request.getParameter("txtPhoneNumber"));
			int result=SystemPropertiesDAO.insertWrapper(adminBO,wrapper,2,driverId);
			if(result==1){
				String error = "Updated successfully";
				request.setAttribute("error",error);
			}
		}
		request.setAttribute("wrapper",wrapper);
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	public void updateWrapper(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); ;
		ArrayList<WrapperBO> wrapperList = new ArrayList<WrapperBO>();
		String forwardURL =TDSConstants.getMainNewJSP;
		WrapperBO wrapper = new WrapperBO();
		if(request.getParameter("btnSearch")==null && request.getParameter("update")==null && request.getParameter("delete")==null){
			request.setAttribute("wrapperList", wrapperList);
			request.setAttribute("screen", "/SystemSetup/updateWrapper.jsp");
		}else if(request.getParameter("btnSearch")!=null){
			wrapper.setDriverId(request.getParameter("txtDriverId")==null?"":request.getParameter("txtDriverId"));
			wrapper.setCabNumber(request.getParameter("txtCabNumber")==null?"":request.getParameter("txtCabNumber"));
			wrapper.setPhoneNumber(request.getParameter("txtPhoneNumber")==null?"":request.getParameter("txtPhoneNumber"));
			wrapperList= SystemPropertiesDAO.getWrapper(adminBO,wrapper);
			request.setAttribute("wrapperList", wrapperList);
			request.setAttribute("screen", "/SystemSetup/updateWrapper.jsp");
		}else if(request.getParameter("update")!=null){
			String driverId=(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
			wrapper=SystemPropertiesDAO.getWrapperbyDriver(adminBO,driverId);
			request.setAttribute("wrapper",wrapper);
			request.setAttribute("screen", "/SystemSetup/insertWrapper.jsp");
		}

		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	public void openRequestFieldOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		OpenRequestFieldOrderBO orFieldOrderBean =new OpenRequestFieldOrderBO();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		ArrayList<OpenRequestFieldOrderBO> orfList=new ArrayList<OpenRequestFieldOrderBO>();
		int result=0;
		String fn="";
		if(request.getParameter("uploadFile")!=null){
			String separatedBy=request.getParameter("seperatedBy");
			String dateFormat=request.getParameter("dateFormat");
			String path = "/tmp";				
			try{
				//MultiPart request
				MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
				//getting the files
				Hashtable files = mrequest.getFiles();
				//Getting the path for the file.
				UploadFile file = ((UploadFile) files.get("filePath")); 

				if (MultipartFormDataRequest.isMultipartFormData(request)){
					// Uses MultipartFormDataRequest to parse the HTTP request.
					if ( (files != null) && (!files.isEmpty()) ){
						UploadBean upBean=new UploadBean();
						upBean.setFolderstore(path);
						upBean.store(mrequest); 
						Vector history = upBean.getHistory();
						UploadParameters up = (UploadParameters) history.elementAt(0);
						if(up.getAltFilename()!=null){
							fn=up.getAltFilename();
						}else{
							fn=((UploadFile) files.get("filePath")).getFileName();
						}
						if (file != null) 
							request.setAttribute("fileName", file.getFileName() );
					}
				}
				// Open the file that is the first 
				// command line parameter
				//csv file containing data
				String strFile = path+"/"+fn;
				//create BufferedReader to read csv file
				BufferedReader br = new BufferedReader( new FileReader(strFile));
				String strLine = "";
				int count = 0;
				String[] values=null;
				FileInputStream fstream = new FileInputStream(strFile);
				// Get the object of DataInputStream
				DataInputStream in = new DataInputStream(fstream);
				br = new BufferedReader(new InputStreamReader(in));
				//Read File Line By Line
				/*File f = new File(strFile);
					Scanner input = new Scanner(f);
					while (input.hasNextLine()) {
						String line = input.nextLine();
						count++;
					}*/
				if((strLine = br.readLine()) != null){
					// Print the content on the console
					if(separatedBy.equals("1")){
						values=strLine.split(",");
					}else if(separatedBy.equals("2")){
						values=strLine.split(";");
					}else if(separatedBy.equals("3")){
						values=strLine.split("\";\"");
					}else if(separatedBy.equals("4")){
						values=strLine.split("\",\"");
					}

					if(strLine!=null && !strLine.equalsIgnoreCase("")){
						//values=strLine.split(",");
						ArrayList<String>al_list=new ArrayList<String>();
						for(int j=0;j<=values.length-1;j++){
							al_list.add(values[j].replace("\"", ""));
						}
						request.setAttribute("arrayList", al_list);
						/*		orFieldOrderBean=SystemPropertiesDAO.getORFieldsOrder(adminBO.getAssociateCode());
						request.setAttribute("orFieldOrderBean", orFieldOrderBean);
						 */
					}

				}
				//Close the input stream
				in.close();
			}catch (Exception e){//Catch exception if any
				System.err.println("Error: " + e.getMessage());
			}
			ArrayList<OpenRequestFieldOrderBO> vendorList= new ArrayList<OpenRequestFieldOrderBO>();
			vendorList=SystemPropertiesDAO.getVendorList(adminBO);
			request.setAttribute("vendorList", vendorList);
			request.setAttribute("separatedBy", separatedBy);
			request.setAttribute("dateFormat", dateFormat);
			request.setAttribute("screen", "/SystemSetup/openRequestFieldOrder.jsp");
		}
		else if(request.getParameter("submit")!=null){
			int numberOfRows = Integer.parseInt(request.getParameter("size"));
			String[] orFields=new String[numberOfRows+1];
			for (int rowIterator = 0; rowIterator < numberOfRows; rowIterator++) {
				orFields[rowIterator]=request.getParameter("orFieldPosition"+rowIterator);
				if(orFields[rowIterator].equalsIgnoreCase("TripId")){
					if(!orFieldOrderBean.getTripid().equals("") && orFieldOrderBean.getTripid()!=null){
						orFieldOrderBean.setTripid(orFieldOrderBean.getTripid()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setTripid(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("Name")){
					if(!orFieldOrderBean.getName().equals("")  && orFieldOrderBean.getName()!=null){
						orFieldOrderBean.setName(orFieldOrderBean.getName()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setName(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("StartAddress1")){
					if(!orFieldOrderBean.getSadd1().equals("") && orFieldOrderBean.getSadd1()!=null){
						orFieldOrderBean.setSadd1(orFieldOrderBean.getSadd1()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setSadd1(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("StartAddress2")){
					if(!orFieldOrderBean.getSadd2().equals("") && orFieldOrderBean.getSadd2()!=null){
						orFieldOrderBean.setSadd2(orFieldOrderBean.getSadd2()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setSadd2(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("StartCity")){
					if(!orFieldOrderBean.getScity().equals("") && orFieldOrderBean.getScity()!=null){
						orFieldOrderBean.setScity(orFieldOrderBean.getScity()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setScity(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("StartLatitude")){
					if( orFieldOrderBean.getSlat()!=null && !orFieldOrderBean.getSlat().equals("") ){
						orFieldOrderBean.setSlat(orFieldOrderBean.getSlat()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setSlat(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("StartLongitude")){
					if(orFieldOrderBean.getSlong()!=null &&  !orFieldOrderBean.getSlong().equals("") ){
						orFieldOrderBean.setSlong(orFieldOrderBean.getSlong()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setSlong(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("EndAddress1")){
					if(!orFieldOrderBean.getEadd1().equals("") && orFieldOrderBean.getEadd1()!=null){
						orFieldOrderBean.setEadd1(orFieldOrderBean.getEadd1()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setEadd1(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("EndAddress2")){
					if(!orFieldOrderBean.getEadd2().equals("") && orFieldOrderBean.getEadd2()!=null){
						orFieldOrderBean.setEadd2(orFieldOrderBean.getEadd2()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setEadd2(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("EndCity")){
					if(!orFieldOrderBean.getEcity().equals("") && orFieldOrderBean.getEcity()!=null){
						orFieldOrderBean.setEcity(orFieldOrderBean.getEcity()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setEcity(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("EndLatitude")){
					if( orFieldOrderBean.getEdlatitude()!=null && !orFieldOrderBean.getEdlatitude().equals("") ){
						orFieldOrderBean.setEdlatitude(orFieldOrderBean.getEdlatitude()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setEdlatitude(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("EndLongitude")){
					if(orFieldOrderBean.getEdlongitude()!=null && !orFieldOrderBean.getEdlongitude().equals("") ){
						orFieldOrderBean.setEdlongitude(orFieldOrderBean.getEdlongitude()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setEdlongitude(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("ServiceTime")){
					if(orFieldOrderBean.getShrs()!=null && !orFieldOrderBean.getShrs().equals("") ){
						orFieldOrderBean.setShrs(orFieldOrderBean.getShrs()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setShrs(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("ServiceDate")){
					if( orFieldOrderBean.getSdate()!=null && !orFieldOrderBean.getSdate().equals("") ){
						orFieldOrderBean.setSdate(orFieldOrderBean.getSdate()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setSdate(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("PayType")){
					if(!orFieldOrderBean.getPaytype().equals("") && orFieldOrderBean.getPaytype()!=null){
						orFieldOrderBean.setPaytype(orFieldOrderBean.getPaytype()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setPaytype(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("account")){
					if(!orFieldOrderBean.getAcct().equals("")){
						orFieldOrderBean.setAcct(orFieldOrderBean.getAcct()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setAcct(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("amount")){
					if(!orFieldOrderBean.getAmt().equals("") && orFieldOrderBean.getAmt()!=null){
						orFieldOrderBean.setAmt(orFieldOrderBean.getAmt()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setAmt(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("sharedRide")){
					if(!orFieldOrderBean.getSharedRide().equals("") && orFieldOrderBean.getSharedRide()!=null){
						orFieldOrderBean.setSharedRide(orFieldOrderBean.getSharedRide()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setSharedRide(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("phone")){
					if(!orFieldOrderBean.getPhone().equals("") && orFieldOrderBean.getPhone()!=null){
						orFieldOrderBean.setPhone(orFieldOrderBean.getPhone()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setPhone(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("numberOfPassengers")){
					if(!orFieldOrderBean.getNumberOfPassengers().equals("") && orFieldOrderBean.getNumberOfPassengers()!=null){
						orFieldOrderBean.setNumberOfPassengers(orFieldOrderBean.getNumberOfPassengers()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setNumberOfPassengers(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("Dont Upload this field")){
					orFieldOrderBean.setDontUpload(1);
				}else if(orFields[rowIterator].equalsIgnoreCase("DispatchComments")){
					if(!orFieldOrderBean.getDispatchComments().equals("") && orFieldOrderBean.getDispatchComments()!=null){
						orFieldOrderBean.setDispatchComments(orFieldOrderBean.getDispatchComments()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setDispatchComments(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("OperatorComments")){
					if(!orFieldOrderBean.getOperatorComments().equals("") && orFieldOrderBean.getOperatorComments()!=null){
						orFieldOrderBean.setOperatorComments(orFieldOrderBean.getOperatorComments()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setOperatorComments(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("RouteNumber")){
					if(!orFieldOrderBean.getRouteNumber().equals("") && orFieldOrderBean.getRouteNumber()!=null){
						orFieldOrderBean.setRouteNumber(orFieldOrderBean.getRouteNumber()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setRouteNumber(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("CallerPhone")){
					if(!orFieldOrderBean.getCallerPhone().equals("") && orFieldOrderBean.getCallerPhone()!=null){
						orFieldOrderBean.setCallerPhone(orFieldOrderBean.getCallerPhone()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setCallerPhone(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("ClientRefNum")){
					if(!orFieldOrderBean.getRefNumber().equals("") && orFieldOrderBean.getRefNumber()!=null){
						orFieldOrderBean.setRefNumber(orFieldOrderBean.getRefNumber()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setRefNumber(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("ClientRefNum1")){
					if(!orFieldOrderBean.getRefNumber1().equals("") && orFieldOrderBean.getRefNumber1()!=null){
						orFieldOrderBean.setRefNumber1(orFieldOrderBean.getRefNumber1()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setRefNumber1(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("ClientRefNum2")){
					if(!orFieldOrderBean.getRefNumber2().equals("") && orFieldOrderBean.getRefNumber2()!=null){
						orFieldOrderBean.setRefNumber2(orFieldOrderBean.getRefNumber3()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setRefNumber2(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("ClientRefNum3")){
					if(!orFieldOrderBean.getRefNumber3().equals("") && orFieldOrderBean.getRefNumber3()!=null){
						orFieldOrderBean.setRefNumber3(orFieldOrderBean.getRefNumber3()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setRefNumber3(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("CustomField")){
					if(!orFieldOrderBean.getCustomField().equals("") && orFieldOrderBean.getCustomField()!=null){
						orFieldOrderBean.setCustomField(orFieldOrderBean.getCustomField()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setCustomField(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("pickUpOrder")){
					if(!orFieldOrderBean.getPickUpOrder().equals("") && orFieldOrderBean.getPickUpOrder()!=null){
						orFieldOrderBean.setPickUpOrder(orFieldOrderBean.getPickUpOrder()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setPickUpOrder(Integer.toString(rowIterator+1));
					}
				}else if(orFields[rowIterator].equalsIgnoreCase("dropOffOrder")){
					if(!orFieldOrderBean.getDropOffOrder().equals("") && orFieldOrderBean.getDropOffOrder()!=null){
						orFieldOrderBean.setDropOffOrder(orFieldOrderBean.getDropOffOrder()+";"+Integer.toString(rowIterator+1));
					}else{
						orFieldOrderBean.setDropOffOrder(Integer.toString(rowIterator+1));
					}
				}
			}
			int vendor=0;
			String separatedBy="";
			String dateFormat="";
			separatedBy=request.getParameter("separatedBy");
			dateFormat=request.getParameter("dateFormat");
			String vendorName=request.getParameter("vendorName")==null?"":request.getParameter("vendorName");
			if(vendorName==null || vendorName.equalsIgnoreCase("")){
				vendor=Integer.parseInt(request.getParameter("vendor")==null?"" : request.getParameter("vendor"));
			}
			result=SystemPropertiesDAO.insertORFieldsPosition(orFieldOrderBean,adminBO,vendor,vendorName,separatedBy,dateFormat);
			if(result==0){
				request.setAttribute("error","Enter all the Values correctly");
				request.setAttribute("screen", "/SystemSetup/openRequestFieldOrder.jsp");
			}else{
				request.setAttribute("screen", "/SystemSetup/openRequestFieldOrderFirst.jsp");
			}
		}else{
			request.setAttribute("screen", "/SystemSetup/openRequestFieldOrderFirst.jsp");
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}
	public void dispatchProperties(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		DispatchPropertiesBO dispatchBO =new DispatchPropertiesBO();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		if(request.getParameter("submit")!=null && !request.getParameter("submit").equals("")){
			dispatchBO.setName_onOffer(Integer.parseInt(request.getParameter("name")==null?"0" : "1"));
			dispatchBO.setPhoneNumber_onOffer(Integer.parseInt(request.getParameter("phoneNumber")==null?"0" : "1"));
			dispatchBO.setsAddress1_onOffer(Integer.parseInt(request.getParameter("sAdd1")==null ? "0" : "1"));
			dispatchBO.setsAddress2_onOffer(Integer.parseInt(request.getParameter("sAdd2")==null ? "0" : "1"));
			dispatchBO.seteAddress1_onOffer(Integer.parseInt(request.getParameter("eAdd1")==null ? "0" : "1"));
			dispatchBO.seteAddress2_onOffer(Integer.parseInt(request.getParameter("eAdd2")==null ? "0" : "1"));
			dispatchBO.setsCity_onOffer(Integer.parseInt(request.getParameter("sCity")==null ? "0" : "1"));
			dispatchBO.seteCity_onOffer(Integer.parseInt(request.getParameter("eCity")==null ? "0" : "1"));
			dispatchBO.setPaymentType_onOffer(Integer.parseInt(request.getParameter("paymentType")==null ? "0" : "1"));
			dispatchBO.setFareAmt_onOffer(Integer.parseInt(request.getParameter("fareAmt")==null ? "0" : "1"));
			dispatchBO.setZone_onOffer(Integer.parseInt(request.getParameter("zones")==null ? "0" : "1"));
			dispatchBO.setName_onAccept(Integer.parseInt(request.getParameter("name_OnAccept")==null?"0" : "1"));
			dispatchBO.setPhoneNumber_onAccept(Integer.parseInt(request.getParameter("phoneNumber_OnAccept")==null?"0" : "1"));
			dispatchBO.setsAddress1_onAccept(Integer.parseInt(request.getParameter("sAdd1_OnAccept")==null ? "0" : "1"));
			dispatchBO.setsAddress2_onAccept(Integer.parseInt(request.getParameter("sAdd2_OnAccept")==null ? "0" : "1"));
			dispatchBO.seteAddress1_onAccept(Integer.parseInt(request.getParameter("eAdd1_OnAccept")==null ? "0" : "1"));
			dispatchBO.seteAddress2_onAccept(Integer.parseInt(request.getParameter("eAdd2_OnAccept")==null ? "0" : "1"));
			dispatchBO.setsCity_onAccept(Integer.parseInt(request.getParameter("sCity_OnAccept")==null ? "0" : "1"));
			dispatchBO.seteCity_onAccept(Integer.parseInt(request.getParameter("eCity_OnAccept")==null ? "0" : "1"));
			dispatchBO.setPaymentType_onAccept(Integer.parseInt(request.getParameter("paymentType_OnAccept")==null ? "0" : "1"));
			dispatchBO.setFareAmt_onAccept(Integer.parseInt(request.getParameter("fareAmt_OnAccept")==null ? "0" : "1"));
			dispatchBO.setZone_onAccept(Integer.parseInt(request.getParameter("zones_OnAccept")==null ? "0" : "1"));
			dispatchBO.setName_onSite(Integer.parseInt(request.getParameter("name_OnSite")==null?"0" : "1"));
			dispatchBO.setPhoneNumber_onSite(Integer.parseInt(request.getParameter("phoneNumber_OnSite")==null?"0" : "1"));
			dispatchBO.setsAddress1_onSite(Integer.parseInt(request.getParameter("sAdd1_OnSite")==null ? "0" : "1"));
			dispatchBO.setsAddress2_onSite(Integer.parseInt(request.getParameter("sAdd2_OnSite")==null ? "0" : "1"));
			dispatchBO.seteAddress1_onSite(Integer.parseInt(request.getParameter("eAdd1_OnSite")==null ? "0" : "1"));
			dispatchBO.seteAddress2_onSite(Integer.parseInt(request.getParameter("eAdd2_OnSite")==null ? "0" : "1"));
			dispatchBO.setsCity_onSite(Integer.parseInt(request.getParameter("sCity_OnSite")==null ? "0" : "1"));
			dispatchBO.seteCity_onSite(Integer.parseInt(request.getParameter("eCity_OnSite")==null ? "0" : "1"));
			dispatchBO.setPaymentType_onSite(Integer.parseInt(request.getParameter("paymentType_OnSite")==null ? "0" : "1"));
			dispatchBO.setFareAmt_onSite(Integer.parseInt(request.getParameter("fareAmt_OnSite")==null ? "0" : "1"));
			dispatchBO.setZone_onSite(Integer.parseInt(request.getParameter("zones_OnSite")==null ? "0" : "1"));
			dispatchBO.setEndZone_onOffer(Integer.parseInt(request.getParameter("endZones")==null ? "0" : "1"));
			dispatchBO.setEndZone_onAccept(Integer.parseInt(request.getParameter("endZones_OnAccept")==null ? "0" : "1"));
			dispatchBO.setEndZone_onSite(Integer.parseInt(request.getParameter("endZones_OnSite")==null ? "0" : "1"));
			dispatchBO.setCall_onAccept(Integer.parseInt(request.getParameter("callPass_OnAccept")==null ? "0" : "1"));
			dispatchBO.setCall_onRoute(Integer.parseInt(request.getParameter("callPass_OnRoute")==null ? "0" : "1"));
			dispatchBO.setCall_onSite(Integer.parseInt(request.getParameter("callPass_OnSite")==null ? "0" : "1"));
			dispatchBO.setMessage_onAccept(Integer.parseInt(request.getParameter("msgPass_OnAccept")==null ? "0" : "1"));
			dispatchBO.setMessage_onRoute(Integer.parseInt(request.getParameter("msgPass_OnRoute")==null ? "0" : "1"));
			dispatchBO.setMessage_onSite(Integer.parseInt(request.getParameter("msgPass_OnSite")==null ? "0" : "1"));
			dispatchBO.setJobAttributeOnOffer(Integer.parseInt(request.getParameter("jobAttributes")==null ? "0" : "1"));
			dispatchBO.setJobAttributeOnAccept(Integer.parseInt(request.getParameter("jobAttributes_OnAccept")==null ? "0" : "1"));
			dispatchBO.setJobAttributeOnSite(Integer.parseInt(request.getParameter("jobAttributes_OnSite")==null ? "0" : "1"));

			dispatchBO.setMessageAccept(request.getParameter("onAcceptMessage")==null?"":request.getParameter("onAcceptMessage").replace("[cabNo]", "[CabNo]"));
			dispatchBO.setMessageRoute(request.getParameter("onRouteMessage")==null?"":request.getParameter("onRouteMessage").replace("[cabNo]", "[CabNo]"));
			dispatchBO.setMessageSite(request.getParameter("onSiteMessage")==null?"":request.getParameter("onSiteMessage").replace("[cabNo]", "[CabNo]"));
			
			int result1=SystemPropertiesDAO.insertDispatchCallProperties(adminBO.getMasterAssociateCode(),dispatchBO);
			int result=SystemPropertiesDAO.insertDispatchProperties(adminBO.getMasterAssociateCode(),dispatchBO);

			if(result==1 && result1==1){
				request.setAttribute("errors", "Dispatch Properties Updated Succesfully");
			}else{
				request.setAttribute("errors", "Dispatch Properties Update Process is UnSuccesful");
			}
		}
		DispatchPropertiesBO dispatchCallBO=SystemPropertiesDAO.getDispatchCallPropeties(adminBO.getMasterAssociateCode());
		dispatchBO=SystemPropertiesDAO.getDispatchPropeties(adminBO.getMasterAssociateCode());
		request.setAttribute("dispatchBO", dispatchBO);
		request.setAttribute("dispatchCallBO", dispatchCallBO);
		request.setAttribute("screen", "/SystemSetup/dispatchProperties.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void insertDocumentTypes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//	DocumentBO docBO =new DocumentBO();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		ArrayList<DocumentBO> getDocuments= new ArrayList<DocumentBO>();
		if(request.getParameter("submit")!=null){
			int numberOfRows = Integer.parseInt(request.getParameter("size"));
			String[] documentType=new String[numberOfRows+1];
			String[] shortName=new String[numberOfRows+1];
			String[] serverityType=new String[numberOfRows+1];
			String[] driverOrVehicle=new String[numberOfRows+1];
			String[] checkForExpiry=new String[numberOfRows+1];
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				documentType[rowIterator]=request.getParameter("documentType"+rowIterator);
				shortName[rowIterator]=request.getParameter("shortName"+rowIterator);
				serverityType[rowIterator]=request.getParameter("serverity"+rowIterator);
				driverOrVehicle[rowIterator]=request.getParameter("driverOrVehicle"+rowIterator);
				checkForExpiry[rowIterator]=request.getParameter("serverity"+rowIterator).equals("Critical")?"1":request.getParameter("checkForExpiry"+rowIterator);
			}
			SystemPropertiesDAO.insertDocumentTypes(adminBO.getAssociateCode(), documentType,shortName,serverityType,numberOfRows,driverOrVehicle,checkForExpiry);
		}
		getDocuments=SystemPropertiesDAO.getDocumentTypes(adminBO);
		request.setAttribute("getDocuments", getDocuments);
		request.setAttribute("screen", "/SystemSetup/documentTypes.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}


	public void reviewOpenRequestFieldOrder(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		ArrayList<OpenRequestFieldOrderBO> orfList=new ArrayList<OpenRequestFieldOrderBO>();
		orfList=SystemPropertiesDAO.getORFieldsOrderForjsp(adminBO.getAssociateCode(),"");
		request.setAttribute("fieldsList",orfList);
		request.setAttribute("screen", "/SystemSetup/reviewUploadFileMapping.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void insertCCDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		CompanyMasterBO cmpBean = new CompanyMasterBO();
		if(request.getParameter("Submit")!=null){
			cmpBean.setCcUsername(request.getParameter("userName"));
			cmpBean.setCcPassword(request.getParameter("password"));
			cmpBean.setCname(request.getParameter("customerName"));
			cmpBean.setCcProvider(Integer.parseInt(request.getParameter("provider")));
			cmpBean.setCcVendor(Integer.parseInt(request.getParameter("vendor")));
			cmpBean.setEmail(request.getParameter("Address"));
			cmpBean.setSiteId(request.getParameter("siteId")==null?"":request.getParameter("siteId"));
			cmpBean.setPriceId(request.getParameter("priceId")==null?"":request.getParameter("priceId"));
			cmpBean.setKey(request.getParameter("key")==null?"":request.getParameter("key"));
			int result=SystemPropertiesDAO.insertCCProperties(cmpBean,adminBO.getMasterAssociateCode());
			if(result==0){
				request.setAttribute("errors", "Failed to insert/update CC details");
			} else {
				request.setAttribute("errors", "CC details inserted/updated successfully");
			}
		}
		cmpBean=SystemPropertiesDAO.getCCProperties(adminBO.getMasterAssociateCode());
		request.setAttribute("cmpBean", cmpBean);
		request.setAttribute("screen", "/SystemSetup/insertCC.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void userProfile(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		if(request.getParameter("submitData")!=null && !request.getParameter("submitData").equals("")){
			String format=request.getParameter("openrequestFormat");
			int result=SystemPropertiesDAO.insertUserProfile(format, adminBO);
			if(result==0){
				request.setAttribute("errors", "Failed to insert/update userProfile details");
			} else {
				request.setAttribute("errors", "User Profile details inserted/updated successfully");
			}
		}else if(request.getParameter("changePassword")!=null && !request.getParameter("changePassword").equals("")){
			String passwordHash="";
			String oldPasswordHash="";
			try {
				passwordHash=PasswordHash.encrypt(request.getParameter("password"));
				oldPasswordHash=PasswordHash.encrypt(request.getParameter("currentPassword"));
			} catch (SystemUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			int result= SystemPropertiesDAO.changePassword(adminBO, passwordHash, oldPasswordHash);
			if(result==1){
				request.setAttribute("errors", "Password Updated successfully");
			}else{
				request.setAttribute("errors", "Failed to update password.Check your current password");
			}
		}
		String value=SystemPropertiesDAO.getUserProfile(adminBO);
		request.setAttribute("value", value);
		request.setAttribute("screen", "/SystemSetup/userProfile.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void fleetMapping(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//	DocumentBO docBO =new DocumentBO();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user"); 
		ArrayList<FleetBO> getFleet= new ArrayList<FleetBO>();
		if(request.getParameter("submit")!=null){
			int numberOfRows = Integer.parseInt(request.getParameter("size"));
			String[] fleetName=new String[numberOfRows+1];
			String[] fleetNumber=new String[numberOfRows+1];
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				fleetName[rowIterator]=request.getParameter("fleetName"+rowIterator);
				fleetNumber[rowIterator]=request.getParameter("fleet"+rowIterator);
			}
			SystemPropertiesDAO.insertFleet(adminBO, fleetNumber,fleetName,numberOfRows);
			session.setAttribute("fleetList", RequestDAO.readFleetsForCompany(adminBO.getMasterAssociateCode()));
		}
		getFleet=SystemPropertiesDAO.getFleets(adminBO);
		request.setAttribute("getFleet", getFleet);
		request.setAttribute("screen", "/SystemSetup/Fleet.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void getZones(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		QueueCoordinatesBO zoneBO= new QueueCoordinatesBO();
		ArrayList<QueueCoordinatesBO> allzones = ZoneDAO.getzoneList(adminBO.getMasterAssociateCode());
		JSONArray array = new JSONArray();
		if(allzones!=null && allzones.size()>0){
			for(int i=0;i<allzones.size();i++){
				JSONObject address = new JSONObject();
				try {
					address.put("QID", allzones.get(i).getQueueId()==null?"":allzones.get(i).getQueueId());
					address.put("QDES", allzones.get(i).getQDescription()==null?"":allzones.get(i).getQDescription());
					array.put(address);			
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}}
	
		response.getWriter().write(array.toString());
		}
	
	public void deleteZoneRate(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String sZone=request.getParameter("startzone");
		String eZone=request.getParameter("endzone");
		int result =ZoneDAO.deletezoneRate(adminBO.getMasterAssociateCode(),sZone,eZone);
		//System.out.println("Ajax--->"+result);
		response.getWriter().write(result==1?"001":"000");
	}

}
