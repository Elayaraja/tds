package com.tds.action;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;
import org.apache.woden.wsdl20.Description;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.Job;
import org.tritonus.share.midi.TMidiDevice.TTransmitter;

import com.charges.bean.ChargesBO;
import com.charges.dao.ChargesDAO;
import com.tds.cmp.bean.Address;
import com.tds.cmp.bean.CabDriverMappingBean;
import com.tds.cmp.bean.CabRegistrationBean;
import com.tds.cmp.bean.CashSettlement;
import com.tds.cmp.bean.CmpPaySeteledBean;
import com.tds.cmp.bean.CompanyFlagBean;
import com.tds.cmp.bean.CompanySystemProperties;
import com.tds.cmp.bean.CustomerProfile;
import com.tds.cmp.bean.DisbursementBean;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverChargeBean;
import com.tds.cmp.bean.DriverDisbrusementSubBean;
import com.tds.cmp.bean.DriverDisbursment;
import com.tds.cmp.bean.DriverVehicleBean;
import com.tds.cmp.bean.LoginAttempts;
import com.tds.cmp.bean.MeterType;
import com.tds.cmp.bean.PenalityBean;
import com.tds.cmp.bean.ZoneListBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.controller.SystemUnavailableException;
import com.tds.controller.TDSController;
import com.tds.dao.AddressDAO;
import com.tds.dao.AuditDAO;
import com.tds.dao.CompanySetupDAO;
import com.tds.dao.ConfigDAO;
import com.tds.dao.CreditCardDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.FinanceDAO;
import com.tds.dao.Registration2DAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.SecurityDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.ZoneDAO;
import com.common.util.CheckZone;
import com.common.util.Messaging;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.CabMaintenanceBO;
import com.tds.tdsBO.CompanyMasterBO;
import com.tds.tdsBO.CompanyccsetupDO;
import com.tds.tdsBO.CreditCardBO;
import com.tds.tdsBO.DriverBehaviorBO;
import com.tds.tdsBO.DriverLocationHistoryBO;
import com.tds.tdsBO.DriverRegistrationBO;
import com.tds.tdsBO.LostandFoundBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.tds.tdsBO.VoucherBO;
import com.tds.tdsBO.ZoneFlagBean;
import com.tds.tdsBO.ZoneRateBean;
import com.tds.tdsBO.passengerBO;
import com.tds.tdsvalidation.RegistrationValidation;
import com.common.util.DistanceCalculation;
import com.common.util.MessageGenerateJSON;
import com.common.util.PasswordHash;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.common.util.TDSValidation;
import com.gac.mobile.dao.MobileDAO;
import com.gac.wasl.DriverRegistration;
import com.gac.wasl.VehicleRegistration;
import com.gac.wasl.WaslIntegrationBO;
import com.logica.smpp.Session;

public class RegistrationAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static ServletConfig config;
	private final Category cat = TDSController.cat;

	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name " + RegistrationAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}

	/*
	 * Access Get method from JSP page or Browser (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Get Method  ");
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Access Post method from JSPR page or Browser (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Post Method");
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Access request from both getter and setter methods
	 */

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		cat.info("In Process Method");
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		HttpSession m_session = request.getSession(false);
		String eventParam = "";
		boolean m_sessionStatus = false;
		if (request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		//System.out.println("The user requested event as "+ m_session);
		cat.info("The user requested event as " + eventParam);
		if (m_session != null && m_session.getAttribute("user") != null && !eventParam.equalsIgnoreCase(TDSConstants.checkUserRegistration)) {
			if (eventParam.equalsIgnoreCase(TDSConstants.updateDriverAvailability)) {
				updateDriverDetails(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.editDriverDetail)) {
				editDriverDetail(request, response);
			} else if (eventParam.equalsIgnoreCase("ZoneEntry")) {
				zoneentrysubmit(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.driverSummary)) {
				driverSummary(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.editUpdateDriverDetail)) {
				editUpdateDriverDetail(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.createAdminRegistration)) {
				adminRegistration(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.driversavedetails)) {
				saveDriverregistration(request, response);
			} else if (eventParam.equalsIgnoreCase("adminviewdetails")) {
				adminviewdetails(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.drivermappingdetails)) {
				driverMappingDetails(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.queuemappingdetails)) {
				queuemappingdetails(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.driverZoneActivity)) {
				driverZoneActivity(request, response);
				
			}else if (eventParam.equalsIgnoreCase("driverReport")) {
				driverReport(request, response);
				
			} else if (eventParam.equalsIgnoreCase(TDSConstants.saveAdminRegistraion)) {
				saveAdminRegistraion(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.CompanyVouchercreation)) {
				CompanyVouchercreation(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.getCompanyVoucherEditPage)) {
				getCompanyVoucherEditPage(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.CompanyVoucherSave)) {
				CompanyVoucherSave(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.getAllcompanyVoucher)) {
				getAllcompanyVoucher(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.createVoucher)) {
				createVoucher(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.saveVoucher)) {
				saveVoucherEntry(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.logoutevent)) {
				userLogout(request, response);
			} else if (eventParam.equalsIgnoreCase("driverDetails")) {
				getDriverDetails(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.getVoucherEditPage)) {
				getVoucherEditPage(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.editVoucher)) {
				editVoucher(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.getAllVoucher)) {
				getAllVoucher(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.createDrvierBehavior)) {
				createBehavior(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.behaviorList)) {
				behaviorSummary(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.behaviorByID)) {
				getBehaviorById(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.createMaintenanceDetail)) {
				createCabMaintenance(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.maintenanceList)) {
				maintenanceSummary(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.maintenanceByID)) {
				getMaintenanceById(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.createLostorFoundDetail)) {
				createLostandFound(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.lostandFoundList)) {
				lostandFoundSummary(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.lostandFoundByID)) {
				getLostandFoundById(request, response);
			} else if (eventParam.equalsIgnoreCase("CompanyDetailview")) {
				CompanyDetailview(request, response);
			} else if (eventParam.equalsIgnoreCase("uncapturedamtdetails")) {
				uncapturedamtdetails(request, response);
				// } else if(eventParam.equalsIgnoreCase("companyview")) {
				// getCompanyMasterview(request,response);
			} else if (eventParam.equalsIgnoreCase("Adminview")) {
				getAdminMasterview(request, response);
			} else if (eventParam.equalsIgnoreCase("unsettleddetailssubmit")) {
				unsettleddetailssubmit(request, response);
			} else if (eventParam.equalsIgnoreCase("cmpPaymentSettelmentSch")) {
				cmpPaymentSettelmentSch(request, response);
			} else if (eventParam.equalsIgnoreCase("driverChargesMaster")) {
				driverChargesMaster(request, response);
			} else if (eventParam.equalsIgnoreCase("driverPenalityMaster")) {
				driverPenalityMaster(request, response);
			} else if (eventParam.equalsIgnoreCase("Summaryvoucher")) {
				voucherDetailSummarry(request, response);
			} else if (eventParam.equalsIgnoreCase("companyccsetup")) {
				Companyccsetupmaster(request, response);
			} else if (eventParam.equalsIgnoreCase("driverDisbursement")) {
				driverDisbursement(request, response);
			} else if (eventParam.equalsIgnoreCase("custInfo")) {
				clientAccount(request, response);
			} else if (eventParam.equalsIgnoreCase("driverDisbursementSummary")) {
				driverDisbursementSummary(request, response);

				// } else if(eventParam.equalsIgnoreCase("showDisDetail")){
				// showDisDetail(request,response);
				// } else
				// if(eventParam.equalsIgnoreCase("showDisDetailSummary")){
				// showDisDetailSummary(request,response);
			}else if (eventParam.equalsIgnoreCase("driverLocation")) {
				driverLocation(request, response);
			} else if (eventParam.equalsIgnoreCase("disbursementmessage")) {
				disbursmentMessage(request, response);
			} else if (eventParam.equalsIgnoreCase("disbursementMessageSummary")) {
				disbursementMessageSummary(request, response);

			} else if (eventParam.equalsIgnoreCase("landmark")) {
				landmark(request, response);
			} else if (eventParam.equalsIgnoreCase("landmarkSummary")) {
				landmarkSummary(request, response);
			} else if (eventParam.equalsIgnoreCase("driverlogout")) {
				driverlogout(request, response);
			} else if (eventParam.equalsIgnoreCase("cabRegistration")) {
				cabRegistration(request, response);
			} else if (eventParam.equalsIgnoreCase("cabRegistrationSummary")) {
				cabRegistrationSummary(request, response);
			} else if (eventParam.equalsIgnoreCase("CabDriverMapping")) {
				CabDriverMapping(request, response);
			} else if (eventParam.equalsIgnoreCase("CabDriverMappingSummary")) {
				CabDriverMappingSummary(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.saveCompany)) {
				m_sessionStatus = true;
				saveCompanyMasterEntry(request, response);
			} else if (eventParam.equalsIgnoreCase("cmpyFlagSetup")) {
				m_sessionStatus = true;
				cmpyFlagSetup(request, response);
			} else if (eventParam.equalsIgnoreCase("zoneFlagSetup")) {
				// m_sessionStatus = true;
				zoneFlagSetup(request, response);
			} else if (eventParam.equalsIgnoreCase("rateforZone")) {
				// m_sessionStatus = true;
				rateforZone(request, response);
			} else if (eventParam.equalsIgnoreCase("loginAttempts")) {
				loginAttempts(request, response);
			}else if (eventParam.equalsIgnoreCase("driverLocationGPS")) {
				driverLocationGPS(request, response);
			}
			// createQueueCoordinates
			m_sessionStatus = true;
			// System.out.println("In Registration Action in event "+m_sessionStatus);checkUserAvailable
		} else if (eventParam.equalsIgnoreCase(TDSConstants.checkUserRegistration)) {
			m_sessionStatus = true;
			checkUser(request, response);
		}  else if (eventParam.equalsIgnoreCase("checkUserAvailable")) {
			m_sessionStatus = true;
			checkUserAvailable(request, response);
		} else if (eventParam.equalsIgnoreCase("forgetpasswordinsert")) {
			m_sessionStatus = true;
			forgetpasswordinsertMaster(request, response);
		} else if (eventParam.equalsIgnoreCase(TDSConstants.saveDriverRegistration)) {
			m_sessionStatus = true;
			saveDriverRegistration(request, response);
		} else if (eventParam.equalsIgnoreCase("showsActiveUser")) {
			m_sessionStatus = true;
			showsActiveUser(request, response);
			// for passenger entry
		} else if (eventParam.equalsIgnoreCase("passengerEntry")) {
			m_sessionStatus = true;
			passengerEntry(request, response);
		} else if (eventParam.equalsIgnoreCase("passengerSave")) {
			m_sessionStatus = true;
			passengerSave(request, response);
		} else if (eventParam.equalsIgnoreCase("passengerSummary")) {
			passengerSummary(request, response);
		} else if (eventParam.equalsIgnoreCase("editPGR")) {
			editpassengerDetails(request, response);
		} else if (eventParam.equalsIgnoreCase(TDSConstants.passengerEditUpdate)) {
			passengerEditUpdate(request, response);
		} else {
			// (getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP)).forward(request,
			// response);
		}
		if (!m_sessionStatus) {
			// System.out.println("In Session Status");
			request.setAttribute("screen", TDSConstants.getLoginJSP);
			(getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP)).forward(request, response);
		}

	}

	public void cmpyFlagSetup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		CompanyFlagBean cmFlagBean = new CompanyFlagBean();
		ArrayList al_list = new ArrayList();
		if (request.getParameter("Button") != null && request.getParameter("Button").equals("Submit")) {
			al_list = assignCmpyFlagSetup(request, al_list);
			boolean insertSuccess = RegistrationDAO.insertCmpyFlagSetup(al_list, ((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode());
			if (insertSuccess) {
				request.setAttribute("error", "Company Flag Setup Inserted Successfully");
			} else {
				request.setAttribute("error", "Failed To Insert Flags");
			}
		}
		al_list = RegistrationDAO.getCmpyFlag(((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode());
		request.setAttribute("pageFor", "Submit");
		request.setAttribute("screen", "/Company/CompanyFlagSetup.jsp");
		request.setAttribute("al_list", al_list);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void zoneFlagSetup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ZoneFlagBean znFlagBean = new ZoneFlagBean();
		System.out.println("ENTER");
		ArrayList al_list = new ArrayList();
		if (request.getParameter("Button") != null && request.getParameter("Button").equals("Submit")) {
			al_list = assignZoneFlagSetup(request, al_list);
			RegistrationDAO.insertZoneFlagSetup(al_list, ((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
			request.setAttribute("error", "ZONE Flag Setup Inserted Successfully");
			// request.setAttribute("page", "This is submit page..");
		}
		al_list = RegistrationDAO.getZoneFlag(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
		request.setAttribute("pageFor", "Submit");
		request.setAttribute("screen", "/Company/ZoneFlagSetup.jsp");
		request.setAttribute("al_list", al_list);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void rateforZone(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList<ZoneRateBean> al_list = new ArrayList<ZoneRateBean>();
		if (request.getParameter("Button") != null && request.getParameter("Button").equals("Submit")) {
			al_list = assignZoneRateSetup(request, al_list);
			ZoneDAO.insertZoneRateSetup(al_list, ((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode());
		}
		al_list = ZoneDAO.getZoneRate(((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode());
		request.setAttribute("pageFor", "Submit");
		request.setAttribute("screen", "/Company/ZoneRateSetup.jsp");
		request.setAttribute("al_list", al_list);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public ArrayList assignZoneFlagSetup(HttpServletRequest request, ArrayList al_list) {
		int size = Integer.parseInt(request.getParameter("size"));
		for (int i = 0; i < size; i++) {
			ZoneFlagBean znFlagBean = new ZoneFlagBean();

			znFlagBean.setAssocode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
			znFlagBean.setFlag1(request.getParameter("flag" + i) == null ? "" : request.getParameter("flag" + i));
			znFlagBean.setFlag1_sw(request.getParameter("flag" + i + "_sw") == null ? "" : request.getParameter("flag" + i + "_sw"));
			znFlagBean.setFlag1_value(request.getParameter("flag" + i + "_value") == null ? "" : request.getParameter("flag" + i + "_value"));
			znFlagBean.setFlag1_lng_desc(request.getParameter("flag" + i + "_lng_desc") == null ? "" : request.getParameter("flag" + i + "_lng_desc"));
			al_list.add(znFlagBean);
		}

		return al_list;
	}

	public ArrayList<ZoneRateBean> assignZoneRateSetup(HttpServletRequest request, ArrayList<ZoneRateBean> al_list) {
		int size = Integer.parseInt(request.getParameter("size"));
		for (int i = 0; i <= size; i++) {
			ZoneRateBean znRateBean = new ZoneRateBean();
			znRateBean.setAssocode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode());
			znRateBean.setStartzone(request.getParameter("rate" + i + "_stzone") == null ? "" : request.getParameter("rate" + i + "_stzone"));
			znRateBean.setEndzone(request.getParameter("rate" + i + "_endzone") == null ? "" : request.getParameter("rate" + i + "_endzone"));
			znRateBean.setAmount(request.getParameter("rate" + i + "_amount") == null ? "" : request.getParameter("rate" + i + "_amount"));
			al_list.add(znRateBean);
		}
		return al_list;
	}

	public ArrayList assignCmpyFlagSetup(HttpServletRequest request, ArrayList al_list) {
		int size = Integer.parseInt(request.getParameter("size"));

		for (int i = 0; i < size; i++) {
			CompanyFlagBean cmFlagBean = new CompanyFlagBean();

			cmFlagBean.setAssocode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode());
			cmFlagBean.setFlag1(request.getParameter("flag" + i) == null ? "" : request.getParameter("flag" + i));
			cmFlagBean.setFlag1_sw(request.getParameter("flag" + i + "_sw") == null ? "" : request.getParameter("flag" + i + "_sw"));
			cmFlagBean.setFlag1_value(request.getParameter("flag" + i + "_value") == null ? "" : request.getParameter("flag" + i + "_value"));
			cmFlagBean.setFlag1_lng_desc(request.getParameter("flag" + i + "_lng_desc") == null ? "" : request.getParameter("flag" + i + "_lng_desc"));
			cmFlagBean.setFlag1_group(request.getParameter("flag" + i + "_grp") == null ? "0" : request.getParameter("flag" + i + "_grp").equals("") ? "0" : request.getParameter("flag" + i + "_grp").trim());
			cmFlagBean.setFlag1_cusap(request.getParameter("flag" + i + "_cusap") == null ? "0" : "1");
			al_list.add(cmFlagBean);
		}

		/*
		 * cmFlagBean.setAssocode(((AdminRegistrationBO)request.getSession().
		 * getAttribute("user")).getAssociateCode());
		 * cmFlagBean.setFlag1(request
		 * .getParameter("flag1")==null?"":request.getParameter("flag1"));
		 * cmFlagBean
		 * .setFlag1_sw(request.getParameter("flag1_sw")==null?"":request
		 * .getParameter("flag1_sw"));
		 * cmFlagBean.setFlag1_value(request.getParameter
		 * ("flag1_value")==null?"":request.getParameter("flag1_value"));
		 * cmFlagBean
		 * .setFlag1_lng_desc(request.getParameter("flag1_lng_desc")==null
		 * ?"":request.getParameter("flag1_lng_desc"));
		 * cmFlagBean.setFlag2(request
		 * .getParameter("flag2")==null?"":request.getParameter("flag2"));
		 * cmFlagBean
		 * .setFlag2_sw(request.getParameter("flag2_sw")==null?"":request
		 * .getParameter("flag2_sw"));
		 * cmFlagBean.setFlag2_value(request.getParameter
		 * ("flag2_value")==null?"":request.getParameter("flag2_value"));
		 * cmFlagBean
		 * .setFlag2_lng_desc(request.getParameter("flag2_lng_desc")==null
		 * ?"":request.getParameter("flag2_lng_desc"));
		 * cmFlagBean.setFlag3(request
		 * .getParameter("flag3")==null?"":request.getParameter("flag3"));
		 * cmFlagBean
		 * .setFlag3_sw(request.getParameter("flag3_sw")==null?"":request
		 * .getParameter("flag3_sw"));
		 * cmFlagBean.setFlag3_value(request.getParameter
		 * ("flag3_value")==null?"":request.getParameter("flag3_value"));
		 * cmFlagBean
		 * .setFlag3_lng_desc(request.getParameter("flag3_lng_desc")==null
		 * ?"":request.getParameter("flag3_lng_desc"));
		 * cmFlagBean.setFlag4(request
		 * .getParameter("flag4")==null?"":request.getParameter("flag4"));
		 * cmFlagBean
		 * .setFlag4_sw(request.getParameter("flag4_sw")==null?"":request
		 * .getParameter("flag4_sw"));
		 * cmFlagBean.setFlag4_value(request.getParameter
		 * ("flag4_value")==null?"":request.getParameter("flag4_value"));
		 * cmFlagBean
		 * .setFlag4_lng_desc(request.getParameter("flag4_lng_desc")==null
		 * ?"":request.getParameter("flag4_lng_desc"));
		 * cmFlagBean.setFlag5(request
		 * .getParameter("flag5")==null?"":request.getParameter("flag5"));
		 * cmFlagBean
		 * .setFlag5_sw(request.getParameter("flag5_sw")==null?"":request
		 * .getParameter("flag5_sw"));
		 * cmFlagBean.setFlag5_lng_desc(request.getParameter
		 * ("flag5_lng_desc")==null?"":request.getParameter("flag5_lng_desc"));
		 * cmFlagBean
		 * .setFlag5_value(request.getParameter("flag5_value")==null?""
		 * :request.getParameter("flag5_value"));
		 * cmFlagBean.setFlag6(request.getParameter
		 * ("flag6")==null?"":request.getParameter("flag6"));
		 * cmFlagBean.setFlag6_sw
		 * (request.getParameter("flag6_sw")==null?"":request
		 * .getParameter("flag6_sw"));
		 * cmFlagBean.setFlag6_value(request.getParameter
		 * ("flag6_value")==null?"":request.getParameter("flag6_value"));
		 * cmFlagBean
		 * .setFlag6_lng_desc(request.getParameter("flag6_lng_desc")==null
		 * ?"":request.getParameter("flag6_lng_desc"));
		 * cmFlagBean.setFlag7(request
		 * .getParameter("flag7")==null?"":request.getParameter("flag7"));
		 * cmFlagBean
		 * .setFlag7_sw(request.getParameter("flag7_sw")==null?"":request
		 * .getParameter("flag7_sw"));
		 * cmFlagBean.setFlag7_value(request.getParameter
		 * ("flag7_value")==null?"":request.getParameter("flag7_value"));
		 * cmFlagBean
		 * .setFlag7_lng_desc(request.getParameter("flag7_lng_desc")==null
		 * ?"":request.getParameter("flag7_lng_desc"));
		 * cmFlagBean.setFlag8(request
		 * .getParameter("flag8")==null?"":request.getParameter("flag8"));
		 * cmFlagBean
		 * .setFlag8_sw(request.getParameter("flag8_sw")==null?"":request
		 * .getParameter("flag8_sw"));
		 * cmFlagBean.setFlag8_value(request.getParameter
		 * ("flag8_value")==null?"":request.getParameter("flag8_value"));
		 * cmFlagBean
		 * .setFlag8_lng_desc(request.getParameter("flag8_lng_desc")==null
		 * ?"":request.getParameter("flag8_lng_desc"));
		 */
		return al_list;
	}

	public void passengerEntry(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String subevent = request.getParameter("subEvent") == null ? "" : request.getParameter("subEvent");
		String flagValue = request.getParameter("flag") == null ? "" : request.getParameter("flag");
		if (subevent.equalsIgnoreCase("psrEntry")) {

			request.setAttribute("screen", "/passengerEntry.jsp");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);

		} else if (subevent.equalsIgnoreCase("psrEditEntry")) {

			String seq = request.getParameter("key");
			passengerBO passenger = null;
			String forwardURL = TDSConstants.getMainNewJSP;
			// String forwardURL = TDSConstants.registrationJSP;
			passenger = RegistrationDAO.getPassengerDetails(seq);

			request.setAttribute("passengerDetails", passenger);
			request.setAttribute("edit", "editpassengerDetails");
			request.setAttribute("screen", "/passengerEntry.jsp");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			requestDispatcher.forward(request, response);

		} else if (subevent.equalsIgnoreCase("passengerSummary")) {
			String forwardURL = TDSConstants.getMainNewJSP;
			// System.out.println("PASSENGER SUMMARY ");
			cat.info("In Driver Summary Method");
			String screen = TDSConstants.passengerSummary;
			HttpSession session = request.getSession();
			ArrayList al_summary = null;
			double no_limit = 0;
			int plength = 5; // Increase or Decrease the numbers in the link
								// sequence at the bottom of the page. Example
								// <12345> can be increased to <12345678910>
			int start = 0;
			int end = 10;
			int no_of_rows = 10;
			int pstart = 1;
			int pend = plength + 1;
			int t_length = 0;
			passengerBO passenger = new passengerBO();
			try {
				t_length = RegistrationDAO.totalpasengerRecord();
				no_limit = Math.ceil((t_length / 10.0));
				// System.out.println("no_limit =="+no_limit);
				// System.out.println("pagelink ="+request.getParameter("pagelink"));
				String first_name = request.getParameter("first_name") == null ? "" : request.getParameter("first_name");
				String last_name = request.getParameter("last_name") == null ? "" : request.getParameter("last_name");
				String parId = request.getParameter("passID") == null ? "" : request.getParameter("passID");
				String parCity = request.getParameter("passCity") == null ? "" : request.getParameter("passCity");
				if (request.getParameter("first") == null) {

					passenger.setStart("" + start);
					passenger.setEnd("" + end);
					al_summary = RegistrationDAO.getpassengerDetails(passenger, first_name, last_name, parId, parCity);

					if (no_limit < end) {
						pend = (int) no_limit + 1;
						System.out.println("pend =" + pend);
						request.setAttribute("pend", pend + "");
					} else {
						request.setAttribute("pend", pend + "");
					}
				} else {
					// System.out.println("pagelink IN NEXT ="+request.getParameter("pageNo"));
					if (request.getParameter("pageNo") != null || request.getParameter("previous") != null || request.getParameter("next") != null) {

						if (request.getParameter("pageNo") != null) {
							int pagevalue = Integer.parseInt(request.getParameter("pageNo"));
							pstart = Integer.parseInt(request.getParameter("pstart"));
							pend = Integer.parseInt(request.getParameter("pend"));
							start = (pagevalue * no_of_rows) - no_of_rows;
							request.setAttribute("pend", pend + "");
						}
						if (request.getParameter("previous") != null) {
							int pagevalue = Integer.parseInt(request.getParameter("previous"));
							// System.out.println("pagevalue="+pagevalue);
							if (pstart < pagevalue) {
								if (pstart < (pagevalue - plength)) {
									pstart = pagevalue - plength;
									start = pstart;
									if ((pstart + plength) < no_limit) {
										pend = pstart + plength;
									} else {
										pend = (int) no_limit;
									}
								} else if (pstart == (pagevalue - plength)) {
									pstart = pagevalue - plength;
									start = pstart;
									if (pagevalue < no_limit) {
										pend = pagevalue;
									} else {
										pend = (int) no_limit;
									}
								} else {
									start = pstart;
								}
							} else if (pstart == pagevalue) {
								pstart = pagevalue;
								start = pstart;
								if ((pagevalue + plength) < no_limit) {
									pend = pagevalue + plength;
								} else {
									pend = (int) no_limit;
								}
							} else {
								start = pstart;
							}
							start = (start * no_of_rows) - no_of_rows;
							request.setAttribute("pend", pend + "");
						}
						if (request.getParameter("next") != null) {
							int pagevalue = Integer.parseInt(request.getParameter("next"));
							if (pagevalue < no_limit) {
								if ((pagevalue + plength) <= no_limit) {
									pstart = pagevalue;
									start = pstart;
								} else {
									pstart = pagevalue;
									start = pstart;
								}
								if ((pagevalue + plength) <= no_limit) {
									pend = pagevalue + plength;
								} else {
									pend = (int) no_limit + 1;
								}
							} else if (pagevalue == no_limit) {
								pstart = pagevalue;
								start = pstart;
								pend = (int) no_limit + 1;
							} else {
								pstart = pagevalue - 1;
								start = pstart;
								if ((pstart + plength) < no_limit) {
									pend = pstart + plength;
								} else {
									pend = (int) no_limit;
								}
								if (pend == pstart) {
									pend = pend + 1;
								}
							}
							start = (start * no_of_rows) - no_of_rows;
							request.setAttribute("pend", pend + "");
						}
						passenger.setStart("" + start);
						passenger.setEnd("" + end);
						al_summary = RegistrationDAO.getpassengerDetails(passenger, first_name, last_name, parId, parCity);
					}
				}
				for (int i = 0; i < t_length; i++) {
					if (request.getParameter("page" + i) != null) {
						start = (i * 10) - 10;
						passenger.setStart("" + start);
						passenger.setEnd("" + end);
						al_summary = RegistrationDAO.getpassengerDetails(passenger, first_name, last_name, parId, parCity);
					}
				}
				if (al_summary.size() > 0) {
					request.setAttribute("pstart", pstart + "");
					request.setAttribute("plength", plength + "");
					request.setAttribute("startValue", start + "");
					request.setAttribute("limit", no_limit + "");
					request.setAttribute("f_name", first_name);
					request.setAttribute("l_name", last_name);
					request.setAttribute("pasrId", parId);
					request.setAttribute("pasrCity", parCity);
				}
			} catch (Exception ex) {
				cat.info("The Exception in the method driver summary  : " + ex.getMessage());
			}
			request.setAttribute("screen", screen);
			request.setAttribute("DRBO", passenger);
			request.setAttribute("passengerdata", al_summary);
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			requestDispatcher.forward(request, response);
		} else {

			if (flagValue.equals("editPage")) {

				String forwardURL = TDSConstants.getMainNewJSP;
				passengerBO passenger = setPassenger(request);
				String error = RegistrationValidation.validationPassenger(passenger);
				if (error.toString().length() > 0) {
					// System.out.println("there is no error");
					request.setAttribute("errors", error);
					request.setAttribute("screen", "/passengerEntry.jsp");
				} else {
					// System.out.println("enter into error");
					int result = RegistrationDAO.updatePassenger(passenger);
					if (result == 1) {

						request.setAttribute("page", "Pasenger data updated sucessfully");
						request.setAttribute("screen", "/Success.jsp");
						RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
						requestDispatcher.forward(request, response);
					} else {
						System.out.println("error in updateing data data");
					}
					request.setAttribute("screen", "/passengerEntry.jsp");
				}
				request.setAttribute("flag", "editPage");
				request.setAttribute("passengerDetails", passenger);
				request.setAttribute("edit", "editpassengerDetails");
				request.setAttribute("screen", "/passengerEntry.jsp");
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			} else {

				passengerBO passgerBO = setPassenger(request);
				// StringBuffer error =null;
				String error = RegistrationValidation.validationPassenger(passgerBO);

				// System.out.println("ERROR:::"+error.toString());
				// System.out.println("ERROR length:::"+error.length());

				if (error.toString().length() > 0) {
					// System.out.println("there is   error");
					request.setAttribute("errors", error);
					request.setAttribute("passengerDetails", passgerBO);
					request.setAttribute("screen", "/passengerEntry.jsp");
				} else if (UtilityDAO.isExistOrNot("select PAS_LOGIN_ID from TDS_PASSENGER where PAS_LOGIN_ID='" + passgerBO.getUid() + "'")) {
					request.setAttribute("passengerDetails", passgerBO);
					request.setAttribute("errors", "Passenger Name Already Exit");
					request.setAttribute("screen", "/passengerEntry.jsp");
				} else {
					// System.out.println("enter into insert");
					int result = RegistrationDAO.insertPassenger(passgerBO);
					if (result == 1) {

						request.setAttribute("page", "Passenger Data Inserted Sucessfully");
						request.setAttribute("screen", "/Success.jsp");
						RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
						requestDispatcher.forward(request, response);
					} else {
						System.out.println("error in inserting data");
					}
					request.setAttribute("passengerDetails", passgerBO);
					request.setAttribute("screen", "/passengerEntry.jsp");
					RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
					requestDispatcher.forward(request, response);
				}
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
				requestDispatcher.forward(request, response);
			}
		}

	}

	public void passengerSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		passengerBO passgerBO = setPassenger(request);
		// StringBuffer error =null;
		String error = RegistrationValidation.validationPassenger(passgerBO);
		// System.out.println("ERROR:::"+error.toString());
		// System.out.println("ERROR length:::"+error.length());

		if (error.toString().length() > 0) {
			// System.out.println("there is   error");
			request.setAttribute("errors", error);
			request.setAttribute("passengerDetails", passgerBO);
			request.setAttribute("screen", "/passengerEntry.jsp");
		} else if (UtilityDAO.isExistOrNot("select PAS_LOGIN_ID from TDS_PASSENGER where PAS_LOGIN_ID='" + passgerBO.getUid() + "'")) {
			request.setAttribute("passengerDetails", passgerBO);
			request.setAttribute("errors", "Passenger Name Already Exit");
			request.setAttribute("screen", "/passengerEntry.jsp");
		} else {

			int result = RegistrationDAO.insertPassenger(passgerBO);
			if (result == 1) {

				request.setAttribute("page", "Passenger Data Inserted Sucessfully");
				request.setAttribute("screen", "/Success.jsp");
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
				requestDispatcher.forward(request, response);
			} else {

			}
			request.setAttribute("passengerDetails", passgerBO);
			request.setAttribute("screen", "/passengerEntry.jsp");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public passengerBO setPassenger(HttpServletRequest request) {

		passengerBO passBo = new passengerBO();
		passBo.setPass_key(request.getParameter("key") == null ? "" : request.getParameter("key"));
		passBo.setFname(request.getParameter("fname") == null ? "" : request.getParameter("fname"));
		passBo.setLname(request.getParameter("lname") == null ? "" : request.getParameter("lname"));
		passBo.setAdd1(request.getParameter("add1") == null ? "" : request.getParameter("add1"));
		passBo.setAdd2(request.getParameter("add2") == null ? "" : request.getParameter("add2"));
		passBo.setCity(request.getParameter("city") == null ? "" : request.getParameter("city"));
		passBo.setState(request.getParameter("state") == null ? "" : request.getParameter("state"));
		passBo.setZip(request.getParameter("zip") == null ? "" : request.getParameter("zip"));
		passBo.setPhone(request.getParameter("phone") == null ? "" : request.getParameter("phone"));

		passBo.setPayment_type(request.getParameter("payment").trim().toCharArray()[0]);
		passBo.setPayment_ac(request.getParameter("paymenta/c") == null ? "" : request.getParameter("paymenta/c"));
		passBo.setPassword(request.getParameter("password") == null ? "" : request.getParameter("password"));
		passBo.setRepassword(request.getParameter("repassword") == null ? "" : request.getParameter("repassword"));

		passBo.setUid(request.getParameter("uid") == null ? "" : request.getParameter("uid"));
		return passBo;

	}

	public String validationPassenger(passengerBO passBO) {

		StringBuffer errors = new StringBuffer();
		if (passBO.getFname().equals("")) {
			errors.append("Enter Fname</br>");
		}
		if (passBO.getLname().equals("")) {
			errors.append("Enter Lname</br>");
		}
		if (passBO.getAdd1().equals("")) {
			errors.append("Enter Address1</br> ");
		}
		if (passBO.getAdd2().equals("")) {
			errors.append("Enter Address2</br> ");
		}
		if (passBO.getPhone().equals("")) {
			errors.append("Enter Phone no</br>");
		}
		if (passBO.getCity().equals("")) {
			errors.append("Enter City</br> ");
		}
		if (passBO.getState().equals("")) {
			errors.append("Enter State</br> ");
		}
		if (passBO.getZip().equals("")) {
			errors.append("Enter Zip </br> ");
		}
		if (passBO.getPayment_type() == '0') {
			errors.append("Select Payment Type</br> ");
		}
		if (passBO.getPayment_type() == 'V') {
			if (passBO.getPayment_ac().equals("")) {
				errors.append("Enter Payment A/c</br> ");
			}
		}
		if (passBO.getUid().equals("")) {
			errors.append("Enter User Name</br> ");
		}
		if (passBO.getPassword().equals("")) {
			errors.append("Enter PassWord</br> ");
		}
		if (!passBO.getPassword().equals(passBO.getRepassword())) {
			errors.append("Password Mismatch Re Enter The Password</br> ");
		}
		return errors.toString();

	}

	public void passengerSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forwardURL = TDSConstants.getMainNewJSP;

		cat.info("In Driver Summary Method");
		String screen = TDSConstants.passengerSummary;
		HttpSession session = request.getSession();
		ArrayList al_summary = null;
		double no_limit = 0;
		int plength = 5; // Increase or Decrease the numbers in the link
							// sequence at the bottom of the page. Example
							// <12345> can be increased to <12345678910>
		int start = 0;
		int end = 10;
		int no_of_rows = 10;
		int pstart = 1;
		int pend = plength + 1;
		int t_length = 0;
		passengerBO passenger = new passengerBO();
		try {
			t_length = RegistrationDAO.totalpasengerRecord();
			no_limit = Math.ceil((t_length / 10.0));
			// System.out.println("no_limit =="+no_limit);
			// System.out.println("pagelink ="+request.getParameter("pagelink"));
			String first_name = request.getParameter("first_name") == null ? "" : request.getParameter("first_name");
			String last_name = request.getParameter("last_name") == null ? "" : request.getParameter("last_name");
			String parId = request.getParameter("passID") == null ? "" : request.getParameter("passID");
			String parCity = request.getParameter("passCity") == null ? "" : request.getParameter("passCity");
			if (request.getParameter("first") == null) {

				passenger.setStart("" + start);
				passenger.setEnd("" + end);
				al_summary = RegistrationDAO.getpassengerDetails(passenger, first_name, last_name, parId, parCity);

				if (no_limit < end) {
					pend = (int) no_limit + 1;
					// System.out.println("pend ="+pend);
					request.setAttribute("pend", pend + "");
				} else {
					request.setAttribute("pend", pend + "");
				}
			} else {
				// System.out.println("pagelink IN NEXT ="+request.getParameter("pageNo"));
				if (request.getParameter("pageNo") != null || request.getParameter("previous") != null || request.getParameter("next") != null) {

					if (request.getParameter("pageNo") != null) {
						int pagevalue = Integer.parseInt(request.getParameter("pageNo"));
						pstart = Integer.parseInt(request.getParameter("pstart"));
						pend = Integer.parseInt(request.getParameter("pend"));
						start = (pagevalue * no_of_rows) - no_of_rows;
						request.setAttribute("pend", pend + "");
					}
					if (request.getParameter("previous") != null) {
						int pagevalue = Integer.parseInt(request.getParameter("previous"));
						// System.out.println("pagevalue="+pagevalue);
						if (pstart < pagevalue) {
							if (pstart < (pagevalue - plength)) {
								pstart = pagevalue - plength;
								start = pstart;
								if ((pstart + plength) < no_limit) {
									pend = pstart + plength;
								} else {
									pend = (int) no_limit;
								}
							} else if (pstart == (pagevalue - plength)) {
								pstart = pagevalue - plength;
								start = pstart;
								if (pagevalue < no_limit) {
									pend = pagevalue;
								} else {
									pend = (int) no_limit;
								}
							} else {
								start = pstart;
							}
						} else if (pstart == pagevalue) {
							pstart = pagevalue;
							start = pstart;
							if ((pagevalue + plength) < no_limit) {
								pend = pagevalue + plength;
							} else {
								pend = (int) no_limit;
							}
						} else {
							start = pstart;
						}
						start = (start * no_of_rows) - no_of_rows;
						request.setAttribute("pend", pend + "");
					}
					if (request.getParameter("next") != null) {
						int pagevalue = Integer.parseInt(request.getParameter("next"));
						if (pagevalue < no_limit) {
							if ((pagevalue + plength) <= no_limit) {
								pstart = pagevalue;
								start = pstart;
							} else {
								pstart = pagevalue;
								start = pstart;
							}
							if ((pagevalue + plength) <= no_limit) {
								pend = pagevalue + plength;
							} else {
								pend = (int) no_limit + 1;
							}
						} else if (pagevalue == no_limit) {
							pstart = pagevalue;
							start = pstart;
							pend = (int) no_limit + 1;
						} else {
							pstart = pagevalue - 1;
							start = pstart;
							if ((pstart + plength) < no_limit) {
								pend = pstart + plength;
							} else {
								pend = (int) no_limit;
							}
							if (pend == pstart) {
								pend = pend + 1;
							}
						}
						start = (start * no_of_rows) - no_of_rows;
						request.setAttribute("pend", pend + "");
					}
					passenger.setStart("" + start);
					passenger.setEnd("" + end);
					al_summary = RegistrationDAO.getpassengerDetails(passenger, first_name, last_name, parId, parCity);
				}
			}
			for (int i = 0; i < t_length; i++) {
				if (request.getParameter("page" + i) != null) {
					start = (i * 10) - 10;
					passenger.setStart("" + start);
					passenger.setEnd("" + end);
					al_summary = RegistrationDAO.getpassengerDetails(passenger, first_name, last_name, parId, parCity);
				}
			}
			if (al_summary.size() > 0) {
				request.setAttribute("pstart", pstart + "");
				request.setAttribute("plength", plength + "");
				request.setAttribute("startValue", start + "");
				request.setAttribute("limit", no_limit + "");
				request.setAttribute("f_name", first_name);
				request.setAttribute("l_name", last_name);
				request.setAttribute("pasrId", parId);
				request.setAttribute("pasrCity", parCity);
			}
		} catch (Exception ex) {
			cat.error("The Exception in the method driver summary  : " + ex.getMessage());
		}
		request.setAttribute("screen", screen);
		request.setAttribute("DRBO", passenger);
		request.setAttribute("passengerdata", al_summary);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void editpassengerDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String seq = request.getParameter("key");
		passengerBO passenger = null;
		String forwardURL = TDSConstants.getMainNewJSP;
		// String forwardURL = TDSConstants.registrationJSP;
		passenger = RegistrationDAO.getPassengerDetails(seq);
		// System.out.println("pass detasil in action:::"+passenger.getFname());
		request.setAttribute("passengerDetails", passenger);
		request.setAttribute("edit", "editpassengerDetails");
		request.setAttribute("screen", "/passengerEntry.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void passengerEditUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String forwardURL = TDSConstants.getMainNewJSP;
		passengerBO passenger = setPassenger(request);
		String error = validationPassenger(passenger);
		if (error.toString().length() > 0) {

			request.setAttribute("errors", error);
			request.setAttribute("screen", "/passengerEntry.jsp");
		} else {

			int result = RegistrationDAO.updatePassenger(passenger);
			if (result == 1) {

				request.setAttribute("page", "Pasenger data updated sucessfully");
				request.setAttribute("screen", "/Success.jsp");
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			} else {

			}
			request.setAttribute("screen", "/passengerEntry.jsp");
		}

		request.setAttribute("passengerDetails", passenger);
		request.setAttribute("edit", "editpassengerDetails");
		request.setAttribute("screen", "/passengerEntry.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void showsActiveUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);

		if ((ApplicationPoolBO) getServletContext().getAttribute("poolBO") != null) {
			long expTime = ((ApplicationPoolBO) getServletContext().getAttribute("poolBO")).get_SessionTokenDateTime();
			long currTime = System.currentTimeMillis();
			long timeInMillis = expTime - currTime;

			long remdr = timeInMillis;
			remdr /= 1000;
			final int seconds = (int) (remdr % 60);
			remdr /= 60;
			final int minutes = (int) (remdr % 60);
			remdr /= 60;
			final int hours = (int) (remdr % 24);
			final int days = (int) (remdr / 24);

			// System.out.println("Day :" + days);
			// System.out.print("Hours:" + hours);
			// System.out.print("Mins:" + minutes);
			// System.out.print("Seconds : "+ seconds);

			// request.setAttribute("users", ""+SessionListener.sessionCount);
			// request.setAttribute("users",
			// ""+RegistrationDAO.getActiveUser());
			request.setAttribute("sessionTimeOut", (minutes < 10 ? ("0" + minutes) : minutes) + ":" + (seconds < 10 ? ("0" + seconds) : seconds));
			request.setAttribute("screen", "/ShowsActiveUser.jsp");
		} else {
			request.setAttribute("page", "Failed to intilalize, Pls Retry");
			request.setAttribute("screen", "/failure.jsp");
		}

		requestDispatcher.forward(request, response);

	}

	public void CabDriverMapping(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		ArrayList<CabDriverMappingBean> al_list = null;
		StringBuffer error = null;

		if (request.getParameter("Button") == null) {
			al_list = new ArrayList<CabDriverMappingBean>();
			CabDriverMappingBean mappingBean = new CabDriverMappingBean();

			mappingBean.setAssoccode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
			mappingBean.setCab_dr_key("0");
			mappingBean.setCab_no("");
			mappingBean.setDriver_id("");
			mappingBean.setFrom_date("");
			mappingBean.setTo_date("");
			mappingBean.setOpr_id(((AdminRegistrationBO) request.getSession().getAttribute("user")).getUid());
			al_list.add(mappingBean);

			request.setAttribute("al_list", al_list);
			request.setAttribute("screen", "/cab/CabDriverMapping.jsp");
			request.setAttribute("status", "Insert");
			requestDispatcher.forward(request, response);

		} else if (request.getParameter("Button").equals("Submit")) {
			al_list = setDriverCab(request);
			error = validateDriverCab(request);
			if (!(error.toString().length() > 0)) {
				RegistrationDAO.insertCabDriverMapping(al_list, ((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode());
				// response.sendRedirect("control?action=registration&event=CabDriverMapping&page=Successfully Inserted");
				al_list = new ArrayList<CabDriverMappingBean>();
				CabDriverMappingBean mappingBean = new CabDriverMappingBean();

				mappingBean.setAssoccode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
				mappingBean.setCab_dr_key("0");
				mappingBean.setCab_no("");
				mappingBean.setDriver_id("");
				mappingBean.setFrom_date("");
				mappingBean.setTo_date("");
				mappingBean.setOpr_id(((AdminRegistrationBO) request.getSession().getAttribute("user")).getUid());
				al_list.add(mappingBean);

				request.setAttribute("al_list", al_list);
				request.setAttribute("screen", "/cab/CabDriverMapping.jsp");
				request.setAttribute("status", "Insert");
				requestDispatcher.forward(request, response);
			} else {
				request.setAttribute("error", error);
				request.setAttribute("al_list", al_list);
				request.setAttribute("screen", "/cab/CabDriverMapping.jsp");
				request.setAttribute("status", "Insert");
				requestDispatcher.forward(request, response);

			}

		} else if (request.getParameter("Button").equals("Search")) {
			// System.out.println("Search condition");
			request.setAttribute("al_list", al_list);
			request.setAttribute("screen", "/cab/CabDriverMappingSummary.jsp");
			requestDispatcher.forward(request, response);
		}
		if (request.getParameter("subevent") != null && request.getParameter("subevent").equalsIgnoreCase("Summary")) {
			// System.out.println("summary drivercabmapping");

			request.setAttribute("screen", "/cab/CabDriverMappingSummary.jsp");
			requestDispatcher.forward(request, response);

		}
	}

	public void CabDriverMappingSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		CabDriverMappingBean driverMappingBean = new CabDriverMappingBean();
		ArrayList<CabDriverMappingBean> al_list = new ArrayList<CabDriverMappingBean>();

		if (request.getParameter("button") != null) {
			driverMappingBean.setAssoccode(adminBO.getAssociateCode());
			driverMappingBean.setDriver_id(request.getParameter("driver_id") == null ? "" : request.getParameter("driver_id"));
			driverMappingBean.setCab_no(request.getParameter("cabno") == null ? "" : request.getParameter("cabno"));
			al_list = RegistrationDAO.getCabMappingDetails(driverMappingBean.getDriver_id(), driverMappingBean.getCab_no(), adminBO.getMasterAssociateCode());
		} else if (request.getParameter("Delete") != null) {
			driverMappingBean.setUserKey(request.getParameter("U_id"));
			RegistrationDAO.deleteCabMappingDetails(driverMappingBean.getUserKey(), adminBO.getAssociateCode());
		}
		request.setAttribute("al_list", al_list);
		request.setAttribute("driverMappingBean", driverMappingBean);
		request.setAttribute("screen", "/cab/CabDriverMappingSummary.jsp");
		requestDispatcher.forward(request, response);
	}

	/*
	 * public void CabDriverMappingSummary(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException,IOException {
	 * RequestDispatcher requestDispatcher =
	 * getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
	 * ArrayList al_list =null;
	 * 
	 * double no_limit = 0; int plength=5; // Increase or Decrease the numbers
	 * in the link sequence at the bottom of the page. Example <12345> can be
	 * increased to <12345678910> int start =0; int end = 10; int no_of_rows=10;
	 * int pstart=1; int pend=plength+1; int t_length=0; String driverid="";
	 * String cabno="";
	 * 
	 * 
	 * try{
	 * t_length=RegistrationDAO.cabMapRecordLength((((AdminRegistrationBO)request
	 * .getSession().getAttribute("user")).getAssociateCode())); no_limit =
	 * Math.ceil((t_length/10.0)); System.out.println("no_limit =="+no_limit);
	 * System.out.println("pagelink ="+request.getParameter("pagelink"));
	 * 
	 * 
	 * if(request.getParameter("first") == null){ al_list =
	 * RegistrationDAO.getCabDriverMapping(""+start,""+end,
	 * (((AdminRegistrationBO
	 * )request.getSession().getAttribute("user")).getAssociateCode()));
	 * System.out.println("In if loop"); if(no_limit<end){ pend=(int)no_limit+1;
	 * System.out.println("pend ="+pend); request.setAttribute("pend", pend+"");
	 * } else{ request.setAttribute("pend", pend+""); }
	 * request.setAttribute("start", start); request.setAttribute("end", end); }
	 * else{ System.out.println("pagelink ="+request.getParameter("pageNo"));
	 * System.out.println("In else loop");
	 * 
	 * if(request.getParameter("pageNo")!=null ||
	 * request.getParameter("previous")!=null ||
	 * request.getParameter("next")!=null){
	 * if(request.getParameter("pageNo")!=null){ int
	 * pagevalue=Integer.parseInt(request.getParameter("pageNo"));
	 * pstart=Integer.parseInt(request.getParameter("pstart"));
	 * pend=Integer.parseInt(request.getParameter("pend"));
	 * start=(pagevalue*no_of_rows)-no_of_rows; request.setAttribute("pend",
	 * pend+""); request.setAttribute("start", start);
	 * request.setAttribute("end", end);
	 * 
	 * }
	 * 
	 * if(request.getParameter("previous")!=null){ int
	 * pagevalue=Integer.parseInt(request.getParameter("previous"));
	 * System.out.println("pagevalue="+pagevalue); if(pstart<pagevalue){
	 * if(pstart<(pagevalue-plength)){ pstart=pagevalue-plength; start=pstart;
	 * if((pstart+plength)<no_limit){ pend=pstart+plength; } else{
	 * pend=(int)no_limit; } } else if(pstart==(pagevalue-plength)){
	 * pstart=pagevalue-plength; start=pstart; if(pagevalue<no_limit){
	 * pend=pagevalue; }else{ pend=(int)no_limit; } } else{ start=pstart; } }
	 * else if(pstart==pagevalue){ pstart=pagevalue; start=pstart;
	 * if((pagevalue+plength)<no_limit){ pend=pagevalue+plength; } else{
	 * pend=(int)no_limit; } } else{ start=pstart; }
	 * start=(start*no_of_rows)-no_of_rows; request.setAttribute("pend",
	 * pend+""); request.setAttribute("start", start);
	 * request.setAttribute("end", end); } if(
	 * request.getParameter("next")!=null){ int
	 * pagevalue=Integer.parseInt(request.getParameter("next"));
	 * if(pagevalue<no_limit){ if((pagevalue+plength)<=no_limit){
	 * pstart=pagevalue; start=pstart; } else{ pstart=pagevalue; start=pstart; }
	 * if((pagevalue+plength)<=no_limit){ pend=pagevalue+plength; } else{
	 * pend=(int)no_limit+1; } } else if(pagevalue==no_limit){ pstart=pagevalue;
	 * start=pstart; pend=(int)no_limit+1; } else{ pstart=pagevalue-1;
	 * start=pstart; if((pstart+plength)<no_limit){ pend=pstart+plength; } else{
	 * pend=(int)no_limit; } if(pend==pstart){ pend=pend+1; } }
	 * start=(start*no_of_rows)-no_of_rows; request.setAttribute("pend",
	 * pend+""); request.setAttribute("start", start);
	 * request.setAttribute("end", end); }
	 * al_list=RegistrationDAO.getCabDriverMapping(""+start,""+end,
	 * (((AdminRegistrationBO
	 * )request.getSession().getAttribute("user")).getAssociateCode()));
	 * 
	 * } } for(int i=0;i<t_length;i++){
	 * if(request.getParameter("page"+i)!=null){ start=(i*10)-10;
	 * al_list=RegistrationDAO.getCabDriverMapping(""+start,""+end,
	 * (((AdminRegistrationBO
	 * )request.getSession().getAttribute("user")).getAssociateCode()));
	 * 
	 * } }
	 * 
	 * if(request.getParameter("button").equalsIgnoreCase("search")) {
	 * 
	 * System.out.println("enter search"); driverid =
	 * request.getParameter("driver_id"); cabno = request.getParameter("cabno");
	 * al_list = RegistrationDAO.getcabmapdetails(driverid, cabno,
	 * (((AdminRegistrationBO
	 * )request.getSession().getAttribute("user")).getAssociateCode()));
	 * 
	 * }
	 * 
	 * 
	 * if(al_list.size()>0){ request.setAttribute("pstart", pstart+"");
	 * request.setAttribute("plength", plength+"");
	 * request.setAttribute("startValue", start+"");
	 * request.setAttribute("limit", no_limit+""); request.setAttribute("start",
	 * start); request.setAttribute("end", end); } } catch(Exception ex){
	 * cat.info
	 * ("The Exception in the method driver summary  : "+ex.getMessage()); }
	 * request.setAttribute("al_list", al_list); request.setAttribute("screen",
	 * "/cab/CabDriverMappingSummary.jsp"); requestDispatcher.forward(request,
	 * response);
	 * 
	 * }
	 */
	public StringBuffer validateDriverCab(HttpServletRequest request) {

		ArrayList<String> cab_list = new ArrayList<String>();

		StringBuffer error = new StringBuffer();
		Integer previous = new Integer(Integer.MIN_VALUE);

		// System.out.println("PREVIOUS::"+previous);
		for (int i = 0; i < Integer.parseInt(request.getParameter("size")); i++) {
			cab_list.add(request.getParameter("cab_no" + i));

			// if(request.getParameter("cab_no"+i).equals(previous))
			// {
			// System.out.println("CAB CHECK CONDITION");
			// error.append("Each cab allow only one Driver Chose another cab: "+(i+1)+" <BR>");
			// }

			if (request.getParameter("cab_no" + i).equals("") || !UtilityDAO.isExistOrNot("select * from TDS_VEHICLE where V_VNO='" + request.getParameter("cab_no" + i) + "' and V_ASSOCCODE='" + ((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode() + "'")) {
				error.append("Invalid Cab No At Row: " + (i + 1) + " <BR>");
			}
			// if(
			// UtilityDAO.isExistOrNot("select * from TDS_CABDRIVERMAPPING where CM_CABNO='"+request.getParameter("cab_no"+i)+"' AND CM_ASSOCCODE='"+((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode()+"'"))
			// {
			// error.append("Cab Already Assign Chose Another cab At Row: "+(i+1)+" <BR>");
			// }
			if (request.getParameter("driver_id" + i).equals("") || !UtilityDAO.Assocodedriveridmatch(request.getParameter("driver_id" + i), ((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode())) {
				error.append("Invalid Driver Id At Row: " + (i + 1) + "  <BR>");
			}
			if (request.getParameter("from_date" + i).equals("")) {
				error.append("Invalid From Date At Row : " + (i + 1) + " <Br>");
			}

		}

		return error;

	}

	public ArrayList setDriverCab(HttpServletRequest request) {
		ArrayList al_list = new ArrayList();

		for (int i = 0; i < Integer.parseInt(request.getParameter("size")); i++) {
			CabDriverMappingBean mappingBean = new CabDriverMappingBean();

			mappingBean.setAssoccode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
			mappingBean.setCab_dr_key(request.getParameter("cab_dr_key" + i) == null ? "" : request.getParameter("cab_dr_key" + i));
			mappingBean.setCab_no(request.getParameter("cab_no" + i) == null ? "" : request.getParameter("cab_no" + i));
			mappingBean.setDriver_id(request.getParameter("driver_id" + i) == null ? "" : request.getParameter("driver_id" + i));
			mappingBean.setFrom_date(request.getParameter("from_date" + i) == null ? "" : request.getParameter("from_date" + i));
			mappingBean.setTo_date(request.getParameter("to_date" + i) == null ? "" : request.getParameter("to_date" + i));
			mappingBean.setOpr_id(((AdminRegistrationBO) request.getSession().getAttribute("user")).getUid());
			al_list.add(mappingBean);
		}
		return al_list;

	}

	public void cabRegistrationSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);

		if (request.getParameter("Button") == null) {
			// request.setAttribute("screen",
			// "/cab/CabRegistraitionSummary.jsp");
			// requestDispatcher.forward(request, response);
		} else if (request.getParameter("Button").equals("Search")) {
			// request.setAttribute("al_summary", RequestDAO.getCabSummary(
			// ((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode(),
			// request.getParameter("year"), request.getParameter("make")));
			String assoccode = request.getParameter("fleetSearch");
			request.setAttribute("al_summary", RequestDAO.getCabSummary(assoccode, request.getParameter("cab_cabno"), request.getParameter("cab_rcno"), request.getParameter("cab_cabyear"), request.getParameter("status")));
		}
		request.setAttribute("screen", "/cab/CabRegistraitionSummary.jsp");
		requestDispatcher.forward(request, response);

	}

	public void driverlogout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("DRIVER LOGOUT ");
		ArrayList al_list = new ArrayList();
		ArrayList al_hislist = new ArrayList();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");

		int result = 0;
		String driverid = "";
		String driverId = "";
		String driverLogout = "";
		String deletedriver = "";
		String cabNo = "";
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		driverid = request.getParameter("driverid") == null ? "" : request.getParameter("driverid");
		cabNo = request.getParameter("cabNo") == null ? "" : request.getParameter("cabNo");
		driverLogout = request.getParameter("logoutDriverId") == null ? "" : request.getParameter("logoutDriverId");
		driverId = request.getParameter("driverId") == null ? "" : request.getParameter("driverId");
		deletedriver = request.getParameter("deletekey") == null ? "" : request.getParameter("deletekey");
		int operatorType = request.getParameter("includeOpr") == null ? 0 : Integer.parseInt(request.getParameter("includeOpr"));
		int logoutDriver = request.getParameter("loutDriver") == null ? 0 : Integer.parseInt(request.getParameter("loutDriver"));
		String fromDate = request.getParameter("fromDate") == null ? "" : request.getParameter("fromDate");
		String toDate = request.getParameter("toDate") == null ? "" : request.getParameter("toDate");

		if (logoutDriver == 1) {
			if (request.getParameter("Button") != null && request.getParameter("Button").equalsIgnoreCase("Search")) {
				al_hislist = RegistrationDAO.getdriverlogHistorydetails(driverid, adminBO.getAssociateCode(), cabNo, operatorType, adminBO.getTimeZone(), logoutDriver, fromDate, toDate, 1);
			} else if (request.getParameter("Button") == null) {
				al_hislist = RegistrationDAO.getdriverlogHistorydetails(driverid, adminBO.getAssociateCode(), cabNo, operatorType, adminBO.getTimeZone(), logoutDriver, fromDate, toDate, 1);
			}
			request.setAttribute("ar_hislist", al_hislist);
			request.setAttribute("screen", "/cab/driverlogout.jsp");
			requestDispatcher.forward(request, response);

		} else {
			if (!driverLogout.equals("")) {
				// String[] message = new String[2];
				// long msgID = System.currentTimeMillis();
				// message=MessageGenerateJSON.logOutDriver(msgID);
				// ApplicationPoolBO poolBO =
				// (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
				// DriverCabQueueBean cabQueueBean=new DriverCabQueueBean();
				// cabQueueBean =
				// DispatchDAO.getDriverByDriverID(adminBO.getMasterAssociateCode(),
				// driverLogout, 1);
				// Messaging oneSMS = new
				// Messaging(getServletContext(),cabQueueBean,
				// message,poolBO,adminBO.getAssociateCode());
				// oneSMS.start();
				// result = RegistrationDAO.updateLogoutDriver(driverLogout,
				// adminBO.getAssociateCode());
				MobileDAO.removeSessionObjects("", driverid);
				MobileDAO.insertLogoutDetail("", driverid, adminBO.getUid(), "Logout From Registration By Operator", adminBO.getMasterAssociateCode());
				DispatchDAO.sendMessage("You are logged out", driverid, adminBO, 2);
				ServiceRequestDAO.deleteDriverFromQueue(driverid, adminBO.getMasterAssociateCode());
				result = RegistrationDAO.updateLogoutDriver(driverid, adminBO.getAssociateCode());
				ServiceRequestDAO.updateDriverAvailabilty("L", driverid, adminBO.getAssociateCode());
				al_list = RegistrationDAO.getdriverlogdetails(driverid, adminBO.getAssociateCode(), cabNo, operatorType, adminBO.getTimeZone());
			} else if (!deletedriver.equals("")) {
				result = RegistrationDAO.deleteMoveToHistory(deletedriver, adminBO.getAssociateCode());
				al_list = RegistrationDAO.getdriverlogdetails(driverid, adminBO.getAssociateCode(), cabNo, operatorType, adminBO.getTimeZone());
			} else if (request.getParameter("Button") != null && request.getParameter("Button").equalsIgnoreCase("Search")) {
				al_list = RegistrationDAO.getdriverlogdetails(driverid, adminBO.getAssociateCode(), cabNo, operatorType, adminBO.getTimeZone());
			} else if (request.getParameter("Button") == null) {
				al_list = RegistrationDAO.getdriverlogdetails(driverid, adminBO.getAssociateCode(), cabNo, operatorType, adminBO.getTimeZone());
			}
			request.setAttribute("ar_list", al_list);
			request.setAttribute("screen", "/cab/driverlogout.jsp");
			requestDispatcher.forward(request, response);

		}
	}

	public void landmarkSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		OpenRequestBO openRequestBean = new OpenRequestBO();
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		openRequestBean.setSlandmark(request.getParameter("slandmark") == null ? "" : request.getParameter("slandmark"));
		// openRequestBean.setAssociateCode(adminBO.getAssociateCode());
		// String error = null;
		if (request.getParameter("button") != null && request.getParameter("button").equalsIgnoreCase("search")) {
			ArrayList<OpenRequestBO> al_list = RegistrationDAO.landMarkSummary(adminBO, openRequestBean, 0, 0, 0, 0);
			request.setAttribute("ar_list", al_list);
		}
		request.setAttribute("screen", "/cab/landmarkSummary.jsp");
		requestDispatcher.forward(request, response);
	}

	public void landmark(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		OpenRequestBO openRequestBean = new OpenRequestBO();

		openRequestBean.setSlandmark(request.getParameter("Landmark") == null ? "" : request.getParameter("Landmark"));
		openRequestBean.setPhone(request.getParameter("phone") == null ? "" : request.getParameter("phone"));

		openRequestBean.setSadd1(request.getParameter("sadd1") == null ? "" : request.getParameter("sadd1"));
		openRequestBean.setSadd2(request.getParameter("sadd2") == null ? "" : request.getParameter("sadd2"));
		openRequestBean.setScity(request.getParameter("scity") == null ? "" : request.getParameter("scity"));
		openRequestBean.setSstate(request.getParameter("sstate") == null ? "" : request.getParameter("sstate"));
		openRequestBean.setSzip(request.getParameter("szip") == null ? "" : request.getParameter("szip"));
		openRequestBean.setSlat(request.getParameter("sLatitude") == null ? "0.000000" : request.getParameter("sLatitude"));
		openRequestBean.setSlong(request.getParameter("sLongitude") == null ? "0.000000" : request.getParameter("sLongitude"));
		openRequestBean.setLandMarkKey(request.getParameter("key") == null ? "" : request.getParameter("key"));
		openRequestBean.setAddressVerified(request.getParameter("addverified") == null ? "" : request.getParameter("addverified"));
		openRequestBean.setLandmarkKeyName(request.getParameter("keyName") == null ? "" : request.getParameter("keyName"));
		openRequestBean.setComments(request.getParameter("comments") == null ? "" : request.getParameter("comments"));
		if (request.getParameter("dontdis") == null) {
			openRequestBean.setDontDispatch(0);
		} else {
			openRequestBean.setDontDispatch(1);
		}
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String pendingLandmarkSNo = request.getParameter("sNo");
		String pendingLandmarkSerialNo = request.getParameter("snoLandmarkPending");
		String error = null;
		if (request.getParameter("Button") == null && request.getParameter("approve") == null && request.getParameter("approveMass") == null) {
			request.setAttribute("status", "Submit");
			request.setAttribute("screen", "/cab/landmarkCreation.jsp");
			getServletContext().getRequestDispatcher("/cab/landmarkCreation.jsp").forward(request, response);
		} else if (request.getParameter("approve") != null) {
			OpenRequestBO landMark_Pending = SystemPropertiesDAO.getLandmarkPendingSearch(pendingLandmarkSNo, adminBO, 0);
			request.setAttribute("sNo", pendingLandmarkSNo);
			request.setAttribute("status", "Submit");
			request.setAttribute("openRequestBean", landMark_Pending);
			request.setAttribute("screen", "/cab/landmarkCreation.jsp");
			getServletContext().getRequestDispatcher("/cab/landmarkCreation.jsp").forward(request, response);
		} else if (request.getParameter("approveMass") != null) {
			String LMKey = request.getParameter("sNo");
			String[] pendingLM = null;
			ArrayList<String> lmKeys = new ArrayList<String>();
			if (!LMKey.equals("")) {
				pendingLM = LMKey.split(";");
				for (int i = 0; i < pendingLM.length; i++) {
					lmKeys.add(pendingLM[i]);
				}
			}
			int value = Registration2DAO.insertPendingLandMark(adminBO.getMasterAssociateCode(), lmKeys, openRequestBean.getComments());
			request.setAttribute("screen", "/cab/landMarkPending.jsp");
			requestDispatcher.forward(request, response);
		} else if (request.getParameter("Button").equals("Submit")) {
			error = validateLandmark(openRequestBean);
			if (openRequestBean.getSlat() != null && openRequestBean.getSlat() != "") {
				ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getAssociateCode() + "Zones"));
				openRequestBean.setZoneNumber(CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(openRequestBean.getSlat()), Double.parseDouble(openRequestBean.getSlong())));
				if (openRequestBean.getZoneNumber() == "") {
					openRequestBean.setZoneNumber("0");
				}
			} else {
				openRequestBean.setZoneNumber("0");
			}

			if (!(error.length() > 0)) {
				int insert = RegistrationDAO.insertLandMark(adminBO.getMasterAssociateCode(), openRequestBean);
				if (pendingLandmarkSerialNo != null) {
					if (insert == 1) {
						SystemPropertiesDAO.getLandmarkPendingDelete(adminBO, pendingLandmarkSerialNo);
						request.setAttribute("screen", "/cab/landMarkPending.jsp");
						requestDispatcher.forward(request, response);
					} else {
						request.setAttribute("openRequestBean", openRequestBean);
						request.setAttribute("status", "Submit");
						request.setAttribute("error", "Fail to approve landmark");
						request.setAttribute("screen", "/cab/landmarkCreation.jsp");
						getServletContext().getRequestDispatcher("/cab/landmarkCreation.jsp").forward(request, response);
						// response.sendRedirect("control?action=registration&event=landmark&error=Fail To Insert LandMark&module=operationView");
					}
				} else {
					if (insert == 1) {
						request.setAttribute("status", "Submit");
						request.setAttribute("error", "Landmark created successfully");
						request.setAttribute("screen", "/cab/landmarkCreation.jsp");
						getServletContext().getRequestDispatcher("/cab/landmarkCreation.jsp").forward(request, response);
					} else {
						request.setAttribute("openRequestBean", openRequestBean);
						request.setAttribute("status", "Submit");
						request.setAttribute("error", "Fail to create landmark");
						request.setAttribute("screen", "/cab/landmarkCreation.jsp");
						getServletContext().getRequestDispatcher("/cab/landmarkCreation.jsp").forward(request, response);
					}
				}
			} else {
				request.setAttribute("landMark", openRequestBean.getLandMarkZone());
				request.setAttribute("Add1", openRequestBean.getSadd1());
				request.setAttribute("Add2", openRequestBean.getSadd2());
				request.setAttribute("City", openRequestBean.getScity());
				request.setAttribute("State", openRequestBean.getSstate());
				request.setAttribute("Zip", openRequestBean.getSzip());
				request.setAttribute("Latitude", openRequestBean.getSlat());
				request.setAttribute("Longitude", openRequestBean.getSlong());
				request.setAttribute("openRequestBean", openRequestBean);
				request.setAttribute("error", error);
				request.setAttribute("status", "Submit");
				request.setAttribute("Addverified", openRequestBean.getAddressVerified());
				request.setAttribute("Dontdispatch", openRequestBean.getDontDispatch());
				request.setAttribute("screen", "/cab/landmarkCreation.jsp");
				getServletContext().getRequestDispatcher("/cab/landmarkCreation.jsp").forward(request, response);
			}
		} else if (request.getParameter("Button").equals("Update")) {

			error = validateLandmark(openRequestBean);
			if (!(error.length() > 0)) {
				RegistrationDAO.updateLandMark(openRequestBean, adminBO);
				response.sendRedirect("control?action=registration&event=landmarkSummary&module=operationView");
			} else {
				request.setAttribute("landMark", openRequestBean.getLandMarkZone());
				request.setAttribute("Add1", openRequestBean.getSadd1());
				request.setAttribute("Add2", openRequestBean.getSadd2());
				request.setAttribute("City", openRequestBean.getScity());
				request.setAttribute("State", openRequestBean.getSstate());
				request.setAttribute("Zip", openRequestBean.getSzip());
				request.setAttribute("Latitude", openRequestBean.getSlat());
				request.setAttribute("Longitude", openRequestBean.getSlong());
				request.setAttribute("openRequestBean", openRequestBean);
				request.setAttribute("error", error);
				request.setAttribute("status", "Update");
				request.setAttribute("screen", "/cab/landmarkCreation.jsp");
				getServletContext().getRequestDispatcher("/cab/landmarkCreation.jsp").forward(request, response);
			}
		} else if (request.getParameter("Button").equals("Edit")) {
			openRequestBean = RegistrationDAO.editLandMark(openRequestBean);
			String key = request.getParameter("key");
			request.setAttribute("key", key);
			request.setAttribute("error", error);
			request.setAttribute("status", "Update");
			request.setAttribute("openRequestBean", openRequestBean);
			request.setAttribute("screen", "/cab/landmarkCreation.jsp");
			getServletContext().getRequestDispatcher("/cab/landmarkCreation.jsp").forward(request, response);
		}

	}

	public void disbursmentMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		cat.info("DISBURSHMENT DETAILS METHOD");
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		String startdate = "";
		String enddate = "";
		String shortdesc = "";
		String longdesc = "";
		startdate = request.getParameter("startDate") == null ? "" : request.getParameter("startDate");
		enddate = request.getParameter("endDate") == null ? "" : request.getParameter("endDate");
		shortdesc = request.getParameter("shortDesc") == null ? "" : request.getParameter("shortDesc");
		longdesc = request.getParameter("longDesc") == null ? "" : request.getParameter("longDesc");
		String error = null;
		if (request.getParameter("Button") != null && !request.getParameter("Button").equals("")) {
			error = validateDisMessage(startdate, enddate, shortdesc, longdesc);
			if (!(error.length() > 0)) {
				RegistrationDAO.insertDisbursementMessage(startdate, enddate, shortdesc, longdesc, (adminBO.getAssociateCode()), adminBO.getMasterAssociateCode());
				request.setAttribute("error", "Disbursement Message Inserted successfully");

			} else {
				request.setAttribute("stdate", startdate);
				request.setAttribute("eddate", enddate);
				request.setAttribute("shortdesc", shortdesc);
				request.setAttribute("longdesc", longdesc);
				request.setAttribute("error", error);
			}
		} else if (request.getParameter("update") != null && !request.getParameter("update").equals("")) {
			cat.info("update function");
			// Update
			String startdate1 = request.getParameter("startDate") == null ? "" : request.getParameter("startDate");
			String enddate1 = request.getParameter("endDate") == null ? "" : request.getParameter("endDate");
			String shortdesc1 = request.getParameter("shortDesc") == null ? "" : request.getParameter("shortDesc");
			String longdesc1 = request.getParameter("longDesc") == null ? "" : request.getParameter("longDesc");
			String disNum = request.getParameter("disNum");
			RegistrationDAO.updateDisMessage(startdate1, enddate1, shortdesc1, longdesc1, ((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode(), disNum);
			request.setAttribute("screen", "/Company/disbursementMessage.jsp");
			request.setAttribute("error", "Disbursement Message Updated successfully");
		}
		request.setAttribute("screen", "/Company/disbursementMessage.jsp");
		requestDispatcher.forward(request, response);
	}

	public void disbursementMessageSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DisbursementBean disbursementBean = new DisbursementBean();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");

		if (request.getParameter("details") != null && request.getParameter("details").equalsIgnoreCase("Get Details")) {
			disbursementBean.setAssoCode(adminBO.getAssociateCode());
			disbursementBean.setStartDate(request.getParameter("StartDate"));
			disbursementBean.setEndDate(request.getParameter("EndDate"));
			ArrayList<DisbursementBean> disbursementSummary = RegistrationDAO.getDisbursementMessages(disbursementBean, adminBO);
			request.setAttribute("disbursementSummary", disbursementSummary);
		}
		request.setAttribute("screen", "/Company/disbursementSummary.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}

	public String validateLandmark(OpenRequestBO openRequestBean) {

		StringBuffer buffer = new StringBuffer();

		if (openRequestBean.getSlandmark().equals("")) {
			buffer.append("Enter LandMark Name<br>");
		}
		if (openRequestBean.getSadd1().equals("")) {
			buffer.append("Enter Address1<br>");
		}
		// if(Add2.equals("")) {
		// buffer.append("Enter Address2<br>");
		// }
		if (openRequestBean.getSstate().equals("")) {
			buffer.append("Enter State <br>");
		}
		if (openRequestBean.getSzip().equals("")) {
			buffer.append("Enter Zip<br>");
		}
		if (openRequestBean.getScity().equals("")) {
			buffer.append("Enter City<br>");
		}
		if (openRequestBean.getSlat().equals("")) {
			buffer.append("Enter Latitude<br>");
		}
		if (openRequestBean.getSlong().equals("")) {
			buffer.append("Enter Longitude <br>");
		}

		return buffer.toString();

	}

	public String validateDisMessage(String stDate, String endDate, String shortDesc, String longDesc) {

		StringBuffer buffer = new StringBuffer();

		if (stDate.equals("")) {
			buffer.append("Enter Start Date<br>");
		}
		if (endDate.equals("")) {
			buffer.append("Enter end Date<br>");
		}
		if (shortDesc.equals("")) {
			buffer.append("Enter Short Desc<br>");
		}
		if (longDesc.equals("")) {
			buffer.append("Enter LongDesc <br>");
		}

		return buffer.toString();

	}

	public void cabRegistration(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		CabRegistrationBean registrationBean = assignCabBeanData(request, adminBO);
		request.setAttribute("cabBean", registrationBean);
		StringBuffer error = null;
		registrationBean.setCab_assocode(adminBO.getAssociateCode());
		request.setAttribute("registrationBean", registrationBean);
		request.setAttribute("cabFlag", RegistrationDAO.getCmpyVehicleFlag(adminBO.getMasterAssociateCode(), "2"));
		if (request.getParameter("Button") == null) {
			request.setAttribute("screen", "/cab/CabRegistraition.jsp");
			request.setAttribute("status", "Insert");
			requestDispatcher.forward(request, response);
		} else if (request.getParameter("Button").equals("Submit")) {
			error = validateCabRegistration(registrationBean, true);
			if (!(error.length() > 0)) {
				int result = RegistrationDAO.insertCabRegistration(registrationBean, adminBO.getMasterAssociateCode());
				if(result>0){
					String wasl_ccode = TDSProperties.getValue("WASL_Company");
					if(adminBO.getAssociateCode().equals(wasl_ccode)){
						//JSONArray array = new JSONArray();
						JSONObject obj = new JSONObject();
						
						try {
							obj.put("apiKey", "D42D418A-4FEE-42C9-9005-18C06F497DC6");
							obj.put("plateType", "2");
							obj.put("vehicleSequenceNumber", registrationBean.getCab_rcno());
							obj.put("plateNumber", registrationBean.getCab_plate_number());
							obj.put("plateLetterLeft", registrationBean.getCab_plate_name_left());
							obj.put("plateLetterMiddle", registrationBean.getCab_plate_name_middle());
							obj.put("plateLetterRight", registrationBean.getCab_plate_name_right());
							
							//array.put(obj);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						registrationBean.setCab_detials_json(obj.toString());
						WaslIntegrationBO wasl_BO = VehicleRegistration.Register_Vehicle_WASL(obj.toString());
						if(wasl_BO.isValid()){
							RegistrationDAO.updateWaslReferenceForVehicle(wasl_BO.getReferenceNumber(), obj.toString(), adminBO.getAssociateCode(), registrationBean.getCab_cabno(), registrationBean.getCab_rcno());
							System.out.println("Vehicle - Successful Wasl update");
							response.sendRedirect("control?action=registration&event=cabRegistration&pageMsg=Cab registered successfully&module=systemsetupView");
						}else{
							//remove
							System.out.println("Vehicle - Wasl reference failed");
							RegistrationDAO.deleteCabByBean(registrationBean);
							
							request.setAttribute("status", "Insert");
							request.setAttribute("registrationBean", registrationBean);
							error.append(wasl_BO.getError_message());
							request.setAttribute("error", error);
							request.setAttribute("screen", "/cab/CabRegistraition.jsp");
							requestDispatcher.forward(request, response);
						}
					}else{
						//System.out.println("Vehicle - No Wasl integration");
						response.sendRedirect("control?action=registration&event=cabRegistration&pageMsg=Cab registered successfully&module=systemsetupView");
					}
					
				}else{
					request.setAttribute("status", "Insert");
					request.setAttribute("registrationBean", registrationBean);
					error.append("Error, while updating Data");
					request.setAttribute("error", error);
					request.setAttribute("screen", "/cab/CabRegistraition.jsp");
					requestDispatcher.forward(request, response);
				}
				
			} else {
				request.setAttribute("status", "Insert");
				request.setAttribute("registrationBean", registrationBean);
				request.setAttribute("error", error);
				request.setAttribute("screen", "/cab/CabRegistraition.jsp");
				requestDispatcher.forward(request, response);
			}
		} else if (request.getParameter("Button").equals("Update")) {
			error = validateCabRegistration(registrationBean, false);
			if (!(error.length() > 0)) {
				RegistrationDAO.updateCabRegistration(registrationBean);
				response.sendRedirect("control?action=registration&event=cabRegistration&pageMsg=Cab details update successfully&module=systemsetupView");
			} else {
				request.setAttribute("registrationBean", registrationBean);
				request.setAttribute("status", "Update");
				request.setAttribute("error", error);
				request.setAttribute("screen", "/cab/CabRegistraition.jsp");
				requestDispatcher.forward(request, response);
			}

		} else if (request.getParameter("Button").equalsIgnoreCase("Edit")) {
			registrationBean = RequestDAO.getCabDetails(request.getParameter("key"), adminBO.getAssociateCode());
			if(TDSProperties.getValue("WASL_Company").equals(adminBO.getAssociateCode())){
				registrationBean.setCab_plate_name_left("N/A");
				registrationBean.setCab_plate_name_middle("N/A");
				registrationBean.setCab_plate_name_right("N/A");
				registrationBean.setCab_plate_number("N/A");
				if(!registrationBean.getCab_detials_json().equals("")){
					try{
						JSONObject jobj = new JSONObject(registrationBean.getCab_detials_json());
						registrationBean.setCab_plate_name_left(jobj.getString("plateLetterLeft"));
						registrationBean.setCab_plate_name_middle(jobj.getString("plateLetterMiddle"));
						registrationBean.setCab_plate_name_right(jobj.getString("plateLetterRight"));
						registrationBean.setCab_plate_number(jobj.getString("plateNumber"));
					}catch(JSONException e){
						System.out.println(e.getMessage());
					}
				}
			}
			ArrayList<MeterType> meters = SystemPropertiesDAO.getMeterValues(adminBO.getMasterAssociateCode());
			registrationBean.setCab_meterTypes(meters);
			request.setAttribute("registrationBean", registrationBean);
			request.setAttribute("screen", "/cab/CabRegistraition.jsp");
			request.setAttribute("status", "Update");
			requestDispatcher.forward(request, response);
		}
	}

	public CabRegistrationBean assignCabBeanData(HttpServletRequest request, AdminRegistrationBO adminBO) {
		CabRegistrationBean registrationBean = new CabRegistrationBean();

		registrationBean.setCab_cabkey(request.getParameter("cab_cabkey") == null ? "0" : request.getParameter("cab_cabkey"));
		registrationBean.setCab_cabmake(request.getParameter("cab_cabmake") == null ? "" : request.getParameter("cab_cabmake"));
		registrationBean.setCab_cabno(request.getParameter("cab_cabno") == null ? "" : request.getParameter("cab_cabno"));
		registrationBean.setCab_cabYear(request.getParameter("cab_cabyear") == null ? "" : request.getParameter("cab_cabyear"));
		registrationBean.setCab_rcno(request.getParameter("cab_rcno") == null ? "" : request.getParameter("cab_rcno"));
		registrationBean.setStatus(request.getParameter("status") == null ? "" : request.getParameter("status"));
		registrationBean.setCab_model(request.getParameter("cab_model") == null ? "" : request.getParameter("cab_model"));
		registrationBean.setCab_vinno(request.getParameter("cab_vin") == null ? "" : request.getParameter("cab_vin"));
		registrationBean.setCab_vtype(request.getParameter("vtype") == null ? "" : request.getParameter("vtype"));
		registrationBean.setCab_registrationDate(request.getParameter("txt_regDate") == null ? "" : request.getParameter("txt_regDate"));
		registrationBean.setCab_expiryDate(request.getParameter("txt_expDate") == null ? "" : request.getParameter("txt_expDate"));
		registrationBean.setOdoMeterValue(request.getParameter("odoMetervalue") == null ? "" : request.getParameter("odoMetervalue"));
		registrationBean.setCab_meterRate(request.getParameter("defaultMeterRate") == null ? "" : request.getParameter("defaultMeterRate"));
		registrationBean.setPayId(request.getParameter("vehPayId") == null ? "" : request.getParameter("vehPayId"));
		registrationBean.setPayPass(request.getParameter("vehPayPass") == null ? "" : request.getParameter("vehPayPass"));
		
		registrationBean.setCab_plate_number(request.getParameter("cab_plate_No") == null ? "" : request.getParameter("cab_plate_No"));
		registrationBean.setCab_plate_name_left(request.getParameter("cab_p_name_L") == null ? "" : request.getParameter("cab_p_name_L"));
		registrationBean.setCab_plate_name_middle(request.getParameter("cab_p_name_M") == null ? "" : request.getParameter("cab_p_name_M"));
		registrationBean.setCab_plate_name_right(request.getParameter("cab_p_name_R") == null ? "" : request.getParameter("cab_p_name_R"));
		
		try {
			if ((request.getParameter("profileSize") != null && Integer.parseInt(request.getParameter("profileSize")) > 0) || (request.getParameter("profileSizeAjax") != null && !request.getParameter("profileSizeAjax").equals(""))) {
				int size = 0;
				if (request.getParameter("profileSize") != null) {
					size = Integer.parseInt(request.getParameter("profileSize"));
				} else {
					size = Integer.parseInt(request.getParameter("profileSizeAjax"));
				}
				ArrayList al_l = new ArrayList();
				String vehicleProfile = "";
				for (int i = 0; i < size; i++) {
					if (request.getParameter("chk" + i) != null) {
						al_l.add(request.getParameter("chk" + i));
						vehicleProfile = vehicleProfile + request.getParameter("chk" + i);
					}
				}
				registrationBean.setAl_list(al_l);
				registrationBean.setVehicleProfile(vehicleProfile);
			}
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}

		// for meter Types
		ArrayList<MeterType> meters = SystemPropertiesDAO.getMeterValues(adminBO.getMasterAssociateCode());
		registrationBean.setCab_meterTypes(meters);

		return registrationBean;
	}

	public StringBuffer validateCabRegistration(CabRegistrationBean registrationBean , boolean isInsert) {
		StringBuffer error = new StringBuffer();

		if (registrationBean.getCab_cabno().equals("")) {
			error.append("Invalid Cab No<br>");
		}
		if (registrationBean.getCab_cabYear().equals("")) {
			error.append("Invalid Cab Year<br>");
		}
		if (registrationBean.getCab_cabmake().equals("")) {
			error.append("Invalid Cab Make<br>");
		}
		if (registrationBean.getCab_rcno().equals("")) {
			error.append("Invalid Cab RC NO<br>");
		}
		
		String wasl_ccode = TDSProperties.getValue("WASL_Company");
		if(registrationBean.getCab_assocode().equals(wasl_ccode) && isInsert){
			if(RegistrationDAO.checkCabAlreadyRegistered(registrationBean)>0){
				error.append("Cab No or RC/Sequence NO is already registered in our System<br>");
			}
			if(registrationBean.getCab_plate_number().equals("")){
				error.append("Shouldn't be empty Cab Plate Number NO<br>");
			}
			if(registrationBean.getCab_plate_name_left().equals("")){
				error.append("Shouldn't be empty Cab Plate Name Left NO<br>");
			}
			if(registrationBean.getCab_plate_name_middle().equals("")){
				error.append("Shouldn't be empty Cab Plate Name Middle NO<br>");
			}
			if(registrationBean.getCab_plate_name_right().equals("")){
				error.append("Shouldn't be empty Cab Plate Name Right<br>");
			}
		}
		
		return error;
	}

	/*
	 * public void showDisDetailSummary(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException,IOException {
	 * 
	 * request.setAttribute("al_list",
	 * RegistrationDAO.getSetteledTransSummmary(request
	 * .getParameter("pss_key"))); request.setAttribute("screen",
	 * "/Company/DriverDisbrusementSub.jsp"); RequestDispatcher
	 * requestDispatcher =
	 * getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
	 * requestDispatcher.forward(request, response); } public void
	 * showDisDetail(HttpServletRequest request, HttpServletResponse response)
	 * throws ServletException,IOException {
	 * 
	 * request.setAttribute("al_list",
	 * RegistrationDAO.getSetteledTransDetail(request
	 * .getParameter("pss_key"),request.getParameter("type")));
	 * request.setAttribute("type", request.getParameter("type"));
	 * request.setAttribute("screen", "/Company/DriverDisburshmentDetail.jsp");
	 * RequestDispatcher requestDispatcher =
	 * getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
	 * requestDispatcher.forward(request, response); }
	 */
	public void driverDisbursementSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList al_list = null;
		ArrayList al_list1 = null;
		HttpSession session = request.getSession();
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		String count = "";
		double rowCount = 0;
		int startValue = 0;
		int endValue = 20;

		if (request.getParameter("subevent") == null) {
			if (request.getParameter("Button") != null && request.getParameter("Button").equals("Get")) {
				String driver = "";

				if (!adminBO.getUsertypeDesc().equalsIgnoreCase("Driver")) {
					driver = request.getParameter("dd_driver_id") == null ? "" : request.getParameter("dd_driver_id");
				} else {
					driver = adminBO.getUid();
				}
				al_list1 = RegistrationDAO.getDriverDisbrushmentSummaryByLimit(TDSValidation.getDBdateFormat(request.getParameter("from_date")), TDSValidation.getDBdateFormat(request.getParameter("to_date")), adminBO.getAssociateCode(), driver, startValue, endValue);
				al_list = RegistrationDAO.getDriverDisbrushmentSummary(TDSValidation.getDBdateFormat(request.getParameter("from_date")), TDSValidation.getDBdateFormat(request.getParameter("to_date")), adminBO.getAssociateCode(), driver);
				rowCount = Math.ceil(al_list.size() / 20.0);
				int row = (int) rowCount;
				count = Integer.toString(row);

			}
			request.setAttribute("count", count);
			request.setAttribute("al_list", al_list1);
			request.setAttribute("al_driver", UtilityDAO.getDriverList(((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode(), 0));
			request.setAttribute("screen", "/Company/DriverDisbursementSummary.jsp");
			requestDispatcher.forward(request, response);
		} else if (request.getParameter("subevent").equalsIgnoreCase("showDisDetail")) {
			request.setAttribute("al_list", RegistrationDAO.getSetteledTransDetail(request.getParameter("pss_key"), request.getParameter("type"), adminBO.getAssociateCode()));
			request.setAttribute("type", request.getParameter("type"));
			request.setAttribute("screen", "/Company/DriverDisbursementDetail.jsp");
			requestDispatcher.forward(request, response);
		} else if (request.getParameter("subevent").equalsIgnoreCase("showDisDetailSummary")) {
			request.setAttribute("al_list", RegistrationDAO.getSetteledTransSummmary(request.getParameter("pss_key"), adminBO));
			request.setAttribute("screen", "/Company/DriverDisbursementSub.jsp");
			// RequestDispatcher requestDispatcher =
			// getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);
		}

	}

	public void driverDisbursement(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ArrayList al_list = null;
		HttpSession session = request.getSession();
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");

		if (request.getParameter("subevent") != null) {
			request.setAttribute("al_list", RegistrationDAO.getSetteledTransDetail(request.getParameter("pss_key"), request.getParameter("type"), adminBO.getAssociateCode()));
			request.setAttribute("type", request.getParameter("type"));
			request.setAttribute("screen", "/Company/DriverDisbursementDetail.jsp");
			requestDispatcher.forward(request, response);
		} else {

			if (request.getParameter("Button") == null) {
				CashSettlement cashSettle = FinanceDAO.readCashRegisterMaster(adminBO.getAssociateCode(), adminBO.getUid());
				request.setAttribute("CashSettlement", cashSettle);

				request.setAttribute("screen", "/Company/DriverDisbursement.jsp");

				request.setAttribute("al_list", al_list);
				request.setAttribute("al_driver", UtilityDAO.getDriverList(((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode(), 0));
				requestDispatcher.forward(request, response);
			} else if (request.getParameter("Button").equals("Get")) {
				String driver = "";

				if (!((AdminRegistrationBO) session.getAttribute("user")).getUsertypeDesc().equalsIgnoreCase("Driver")) {
					driver = request.getParameter("dd_driver_id") == null ? "" : request.getParameter("dd_driver_id");
				} else {
					driver = ((AdminRegistrationBO) session.getAttribute("user")).getUid();

				}
				CashSettlement cashSettle = FinanceDAO.readCashRegisterMaster(adminBO.getAssociateCode(), adminBO.getUid());
				request.setAttribute("CashSettlement", cashSettle);

				al_list = RegistrationDAO.getDriverSetelment(TDSValidation.getDBdateFormat(request.getParameter("from_date")), TDSValidation.getDBdateFormat(request.getParameter("to_date")), ((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode(), driver);
				request.setAttribute("screen", "/Company/DriverDisbursement.jsp");

				request.setAttribute("al_list", al_list);
				request.setAttribute("al_driver", UtilityDAO.getDriverList(((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode(), 0));
				requestDispatcher.forward(request, response);
			} else if (request.getParameter("Button").equals("Record Payment")) {
				DriverDisbursment disbursment = assignDisbrushment(request, response);

				int key = RegistrationDAO.insertDriverDisbrush(disbursment, ((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode(), ((AdminRegistrationBO) session.getAttribute("user")).getUid());
				FinanceDAO.insertCashRegisterDetail(disbursment, adminBO, key + "");
				FinanceDAO.updateCashRegisterMaster(disbursment, adminBO.getAssociateCode(), adminBO.getUid());

				request.setAttribute("error", "Inserted Successfully");
				response.sendRedirect("control?action=registration&event=driverDisbursement&error=Inserted Successfully&key=" + key + "&showRepo=1&module=financeView");

			}

		}

	}

	public static DriverDisbursment assignDisbrushment(HttpServletRequest request, HttpServletResponse response) {

		DriverDisbursment disbursment = new DriverDisbursment();
		disbursment.setDriverid(request.getParameter("driverid"));
		disbursment.setSet_amount(request.getParameter("manual_adj").split(" ")[0].equals("$") ? request.getParameter("manual_adj").split(" ")[1] : request.getParameter("manual_adj").split(" ")[0]);
		disbursment.setDescr(request.getParameter("desc"));
		disbursment.setCheckno(request.getParameter("check_no") == "" ? "0" : request.getParameter("check_no"));
		disbursment.setCheck_cash(request.getParameter("check_cash"));
		ArrayList al_List = new ArrayList();

		for (int i = 0; i < Integer.parseInt(request.getParameter("size")); i++) {
			if (request.getParameter("set" + i) != null) {
				DriverDisbrusementSubBean diBean = new DriverDisbrusementSubBean();
				diBean.setPss_id(request.getParameter("pss_id" + i) == null ? "" : request.getParameter("pss_id" + i));
				diBean.setAmount(request.getParameter("adj" + i) == null ? "" : request.getParameter("adj" + i));
				diBean.setDescr(request.getParameter("descr" + i));
				diBean.setType(request.getParameter("type" + i) == null ? "" : request.getParameter("type" + i));
				al_List.add(diBean);
				// al_List.add(request.getParameter("pss_id"+i));
			}
		}
		disbursment.setAl_list(al_List);
		return disbursment;

	}

	public void voucherDetailSummarry(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Open Request function");
		HttpSession session = request.getSession();
		// System.out.println("Session "+session.getAttribute("user"));

		request.setAttribute("screen", "/jsp/voucherreport.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void unsettleddetailssubmit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In unsettleddetailssubmit Details");
		String forwardURL = TDSConstants.getMainNewJSP;
		StringBuffer error = unsettledvalidate(request);
		HttpSession session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) session.getAttribute("user");
		String tripid = request.getParameter("checkedval");

		if (!(error.length() > 0)) {
			RegistrationDAO.uncapturedamtcapturedflag(tripid);
			error.append("SuccessFully Updated Trip ID :" + tripid);
		}
		ArrayList obj = RegistrationDAO.uncapturedamtdetails(m_adminBO.getAssociateCode());
		request.setAttribute("voucherdata", obj);
		request.setAttribute("screen", "/jsp/uncapturedamtdetails.jsp");
		request.setAttribute("error", error);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void uncapturedamtdetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Uncaptured Amt Details");
		// System.out.println("In Uncaptured Amt Details:::::::::::::" );
		HttpSession session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) session.getAttribute("user");
		ArrayList obj = new ArrayList();
		obj = RegistrationDAO.uncapturedamtdetails(m_adminBO.getAssociateCode());
		String forwardURL = TDSConstants.getMainNewJSP;
		request.setAttribute("voucherdata", obj);
		cat.info("Forward the request into the " + forwardURL + " JPS");
		request.setAttribute("screen", "/jsp/uncapturedamtdetails.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	void CompanyDetailview(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Open  CompanyDetailview Request function");
		HttpSession session = request.getSession();
		// System.out.println("Session "+session.getAttribute("user"));
		ArrayList obj = new ArrayList();
		obj = RegistrationDAO.CompanyDetailsview();
		request.setAttribute("details", obj);
		String forwardURL = "/jsp/CompanyDetailspopup.jsp";
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	/*
	 * public void getCompanyMasterview(HttpServletRequest
	 * request,HttpServletResponse response) throws ServletException,IOException
	 * { String forwardURL = TDSConstants.getMainNewJSP;
	 * cat.info("In getCompanyMasterview page"); HttpSession session =
	 * request.getSession(); ArrayList A_list=
	 * RegistrationDAO.CompanyMasterview(
	 * ((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
	 * request.setAttribute("companyBO", A_list); request.setAttribute("screen",
	 * "/jsp/CompanyView.jsp"); RequestDispatcher requestDispatcher =
	 * getServletContext().getRequestDispatcher(forwardURL);
	 * requestDispatcher.forward(request, response); }
	 */
	public void getAdminMasterview(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forwardURL = TDSConstants.getMainNewJSP;
		cat.info("In getAdminMasterview page");
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
		AdminRegistrationBO adminBOForData = new AdminRegistrationBO();

		if (request.getParameter("GetDetails") == null) {
			request.setAttribute("screen", "/jsp/AdminRegViewDetails.jsp");

		} else if (request.getParameter("GetDetails") != null) {
			adminBOForData.setFname(request.getParameter("firstName"));
			adminBOForData.setLname(request.getParameter("lastName"));
			adminBOForData.setUname(request.getParameter("userName"));
			ArrayList<AdminRegistrationBO> A_list = RegistrationDAO.AdminMasterview(adminBo.getAssociateCode(), "", "list", adminBOForData);
			request.setAttribute("fleetListForOperators", SecurityDAO.getFleetListForOperator(A_list, adminBo.getMasterAssociateCode(), adminBo.getAssociateCode()));
			request.setAttribute("adminBO", A_list);
			request.setAttribute("screen", "/jsp/AdminRegViewDetails.jsp");
		}
		request.setAttribute("csBean", adminBOForData);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);

	}

	public void adminviewdetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// String forwardURL = TDSConstants.getMainNewJSP;
		cat.info("In getAdminviewdetails page");
		HttpSession session = request.getSession();
		StringBuffer outData = new StringBuffer("");
		AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
		AdminRegistrationBO adminBOForData = new AdminRegistrationBO();
		adminBOForData.setFname(request.getParameter("firstName"));
		adminBOForData.setLname(request.getParameter("lastName"));
		adminBOForData.setUname(request.getParameter("userName"));
		ArrayList<AdminRegistrationBO> al_list = RegistrationDAO.AdminMasterview(adminBo.getAssociateCode(), "", "list", adminBOForData);
		JSONArray array = new JSONArray();
		if (al_list != null && al_list.size() > 0) {
			for (int i = 0; i < al_list.size(); i++) {
				JSONObject address = new JSONObject();
				try {
					address.put("SNO", al_list.get(i).getUid() == null ? "" : al_list.get(i).getUid());
					address.put("USERNAME", al_list.get(i).getUname() == null ? "" : al_list.get(i).getUname());
					address.put("ASSOCODE", al_list.get(i).getAssociateCode() == null ? "" : al_list.get(i).getAssociateCode());
					address.put("USERTYPE", al_list.get(i).getUsertype() == null ? "" : al_list.get(i).getUsertype());
					address.put("PARENTCOMPANY", al_list.get(i).getPcompany() == null ? "" : al_list.get(i).getPcompany());
					address.put("PAYMENTTYPE", al_list.get(i).getPaymentType() == null ? "" : al_list.get(i).getPaymentType());
					address.put("FIRSTNAME", al_list.get(i).getFname() == null ? "" : al_list.get(i).getFname());
					address.put("LASTNAME", al_list.get(i).getLname() == null ? "" : al_list.get(i).getLname());
					address.put("EMAIL", al_list.get(i).getEmail() == null ? "" : al_list.get(i).getEmail());
					address.put("DRIVERAVAILABILITY", al_list.get(i).getDriverAvailability() == null ? "" : al_list.get(i).getDriverAvailability());
					array.put(address);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		outData.append(array);
		response.setContentType("application/json");
		response.getWriter().write(outData.toString());
	}

	private void forgetpasswordinsertMaster(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// Used when user forgets password
		// TODO
		// Change random password generation process
		// Will this send an email?
		HttpSession session = request.getSession();
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		// AdminRegistrationBO m_adminBO = (AdminRegistrationBO)
		// session.getAttribute("user");
		if (request.getParameter("subevent") != null) {
			if (poolBO != null) {
				String username = request.getParameter("username") == null ? "" : request.getParameter("username");
				int VALID = RegistrationDAO.forgotpasswordDAO(username, poolBO);

				if (VALID == 1) {

					request.setAttribute("screen", "/successForgetpassword.jsp");
					RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
					requestDispatcher.forward(request, response);

				} else {
					request.setAttribute("errors", "<font color='red'>Enter Correct User Name</font>");
					request.setAttribute("screen", "/Forgetpassword.jsp");
					RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
					requestDispatcher.forward(request, response);
				}
			} else {
				request.setAttribute("errors", "<font color='red'>Pls Try After</font>");
				request.setAttribute("screen", "/Forgetpassword.jsp");
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
				requestDispatcher.forward(request, response);
			}
		} else {
			request.setAttribute("screen", "/Forgetpassword.jsp");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);
		}

	}

	/*
	 * private void forgetpasswordMaster(HttpServletRequest
	 * request,HttpServletResponse response) throws
	 * ServletException,IOException{
	 * 
	 * request.setAttribute("screen", "/Forgetpassword.jsp"); RequestDispatcher
	 * requestDispatcher =
	 * getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
	 * requestDispatcher.forward(request, response);
	 * 
	 * }
	 */

	public void Companyccsetupmaster(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		if (request.getParameter("buttonSubmit") != null && request.getParameter("buttonSubmit").equalsIgnoreCase("submit")) {
			int numberOfRows = Integer.parseInt(request.getParameter("fieldSize"));
			ArrayList<CompanyccsetupDO> insertList = new ArrayList<CompanyccsetupDO>();
			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				CompanyccsetupDO ccBo = new CompanyccsetupDO();
				ccBo.setCCC_TEMPLATE_TYPE(Integer.parseInt(request.getParameter("templateType" + rowIterator)));
				ccBo.setCCC_CARD_TYPE(request.getParameter("CCC_CARD_TYPE" + rowIterator));
				ccBo.setCCC_CARD_ISSUER(request.getParameter("CCC_CARD_ISSUER" + rowIterator));
				String amt = request.getParameter("CCC_TXN_AMOUNT" + rowIterator).replace("$ ", "");
				if (amt != null && !amt.equals("")) {
					ccBo.setCCC_TXN_AMOUNT(amt);
				} else {
					ccBo.setCCC_TXN_AMOUNT("0.00");
				}
				ccBo.setCCC_TXN_AMT_PERCENTAGE(request.getParameter("CCC_TXN_AMT_PERCENTAGE" + rowIterator).replace("$ ", ""));
				if (request.getParameter("CCC_SWIPE_TYPE" + rowIterator) != null) {
					ccBo.setCCC_SWIPE_TYPE(request.getParameter("CCC_SWIPE_TYPE" + rowIterator));
				}
				insertList.add(ccBo);
			}
			boolean result = CompanySetupDAO.insertCompanySetup(insertList, "insert", adminBO.getAssociateCode());

			if (result) {
				request.setAttribute("error", "CC/Voucher template successfully inserted");
			} else {
				request.setAttribute("error", "Fail to insert CC/Voucher template");
			}
		}
		ArrayList<CompanyccsetupDO> al_list = CompanySetupDAO.Companysetupadd(adminBO.getAssociateCode());
		ArrayList<ChargesBO> charges = ChargesDAO.getChargeTemplate(adminBO.getMasterAssociateCode(), 1);
		request.setAttribute("charges", charges);
		request.setAttribute("al_list", al_list);
		request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	private void Companyccsetupmaster1(HttpServletRequest request,

	HttpServletResponse response) throws ServletException, IOException {
		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		ArrayList al_list = null;
		StringBuffer error = null;
		CompanyccsetupDO CompanyccsetupDO = null;
		String pagefor = "Submit";

		String Button = request.getParameter("Button");
		if (Button == null) {
			al_list = CompanySetupDAO.Companysetupadd(m_adminBO.getAssociateCode());
			request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
			if (al_list == null) {
				pagefor = "Submit";
				CompanyccsetupDO = new CompanyccsetupDO();
				al_list = new ArrayList();
				al_list.add(CompanyccsetupDO);
			} else {
				pagefor = "Update";
			}
		} else if (Button.equalsIgnoreCase("Delete Selected")) {
			pagefor = request.getParameter("pagefor");
			al_list = CCsetupBean(request, response, m_adminBO.getAssociateCode());
			boolean saa1 = true;
			for (int i = 0; i < al_list.size() && saa1; i++) {
				CompanyccsetupDO Test = (CompanyccsetupDO) al_list.get(i);
				for (int j = i + 1; j < al_list.size(); j++) {
					CompanyccsetupDO Test1 = (CompanyccsetupDO) al_list.get(j);
					if (Test1.getCCC_CARD_TYPE().equalsIgnoreCase(Test.getCCC_CARD_TYPE()) && Test1.getCCC_CARD_ISSUER().equalsIgnoreCase(Test.getCCC_CARD_ISSUER()) && Test1.getCCC_SWIPE_TYPE().equalsIgnoreCase(Test.getCCC_SWIPE_TYPE())) {
						saa1 = false;
						break;
					}
				}
			}
			if (saa1 == true) {
				CompanySetupDAO.deleteCompanySetup(al_list, m_adminBO.getAssociateCode());
				al_list = CompanySetupDAO.Companysetupadd(m_adminBO.getAssociateCode());
				request.setAttribute("page", "Successfully Deleted <BR>");
				request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
			} else {
				request.setAttribute("page", "Data Already Exits <BR>");
				request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
			}

		} else if (Button.equalsIgnoreCase("Submit")) {

			al_list = CCsetupBean(request, response, m_adminBO.getAssociateCode());
			error = validateCompanySetupBean(request, response);
			if (!(error.length() > 0)) {
				boolean saa1 = true;

				for (int i = 0; i < al_list.size() && saa1; i++) {
					CompanyccsetupDO Test = (CompanyccsetupDO) al_list.get(i);
					for (int j = i + 1; j < al_list.size(); j++) {
						CompanyccsetupDO Test1 = (CompanyccsetupDO) al_list.get(j);
						if (Test1.getCCC_CARD_TYPE().equalsIgnoreCase(Test.getCCC_CARD_TYPE()) && Test1.getCCC_CARD_ISSUER().equalsIgnoreCase(Test.getCCC_CARD_ISSUER()) && Test1.getCCC_SWIPE_TYPE().equalsIgnoreCase(Test.getCCC_SWIPE_TYPE())) {
							saa1 = false;
							break;
						}
					}
				}
				if (saa1 == true) {
					CompanySetupDAO.insertCompanySetup(al_list, "insert", m_adminBO.getAssociateCode());
					al_list = CompanySetupDAO.Companysetupadd(m_adminBO.getAssociateCode());
					pagefor = "Update";
					request.setAttribute("page", "Successfully Inserted <BR>");
					request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
				} else {
					pagefor = "Submit";
					error.append("Data Already Exits <BR>");
					request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
				}
			} else {
				request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
				pagefor = "Submit";
			}

		} else {
			al_list = CCsetupBean(request, response, m_adminBO.getAssociateCode());
			error = validateCompanySetupBean(request, response);
			if (!(error.length() > 0)) {
				boolean sa1 = true;
				for (int i = 0; i < al_list.size() && sa1; i++) {
					CompanyccsetupDO Test = (CompanyccsetupDO) al_list.get(i);
					for (int j = i + 1; j < al_list.size(); j++) {
						CompanyccsetupDO Test1 = (CompanyccsetupDO) al_list.get(j);
						if (Test1.getCCC_CARD_TYPE().equalsIgnoreCase(Test.getCCC_CARD_TYPE()) && Test1.getCCC_CARD_ISSUER().equalsIgnoreCase(Test.getCCC_CARD_ISSUER())) {
							sa1 = false;
							break;
						}
					}
				}
				if (sa1 == true) {
					CompanySetupDAO.updateCompanySetup(al_list, "update", m_adminBO.getAssociateCode());
					al_list = CompanySetupDAO.Companysetupadd(m_adminBO.getAssociateCode());
					request.setAttribute("page", "Successfully Update <BR>");
					request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
					pagefor = "Update";
				} else {
					error.append("Already Data Exits");
					request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
				}

			}
			request.setAttribute("screen", "/Company/CompanyccSetup.jsp");
			pagefor = "Update";
		}
		request.setAttribute("pagefor", pagefor);
		request.setAttribute("error", error);
		request.setAttribute("al_list", al_list);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}

	public StringBuffer validateCompanySetupBean(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		StringBuffer error = new StringBuffer();
		String amout = "";
		String size = request.getParameter("size") == null ? "0" : request.getParameter("size");
		for (int i = 0; i < Integer.parseInt(size); i++) {
			// String
			// amt=request.getParameter("CCC_TXN_AMT_PERCENTAGE"+i).split(" ")[0].equals("$")
			// ? request.getParameter("CCC_TXN_AMT_PERCENTAGE"+i).split(" ")[1]
			// :request.getParameter("CCC_TXN_AMT_PERCENTAGE"+i).split(" ")[0];
			amout = request.getParameter("CCC_TXN_AMOUNT" + i).replace("$ ", "");
			if ((amout == null) || (amout.equals(""))) {
				error.append(" TXN Amount is Invalid At Row " + (i + 1) + " <br>");
			} else {
				// String
				// txtamt=request.getParameter("CCC_TXN_AMOUNT"+i).split(" ")[0];
				if (!TDSValidation.isDoubleNumber(amout)) {
					error.append(" TXN Amount is Invalid At Row " + (i + 1) + " <br>");
				}
			}
			if (!TDSValidation.isDoubleNumber(request.getParameter("CCC_TXN_AMT_PERCENTAGE" + i))) {
				error.append(" TXN Persantage is Invalid At Row " + (i + 1) + " <br>");
			}
		}
		return error;
	}

	public ArrayList CCsetupBean(HttpServletRequest request, HttpServletResponse response, String asscode) throws ServletException, IOException {

		ArrayList al_list = new ArrayList();
		String size = request.getParameter("size") == null ? "0" : request.getParameter("size");

		for (int i = 0; i < Integer.parseInt(size); i++) {
			String txtamt = null;
			CompanyccsetupDO CompanyccsetupDO = new CompanyccsetupDO();
			CompanyccsetupDO.setChk(request.getParameter("chck" + i) == null ? "0" : "1");
			CompanyccsetupDO.setCCC_COMPANY_ID(request.getParameter("CCC_COMPANY_ID" + i));
			CompanyccsetupDO.setCCC_CARD_TYPE(request.getParameter("CCC_CARD_TYPE" + i));
			CompanyccsetupDO.setCCC_CARD_ISSUER(request.getParameter("CCC_CARD_ISSUER" + i));
			String amt = request.getParameter("CCC_TXN_AMOUNT" + i).replace("$ ", "");
			if (amt != null && !amt.equals("")) {
				// txtamt=request.getParameter("CCC_TXN_AMOUNT"+i).split(" ")[0].equals("$")
				// ?request.getParameter("CCC_TXN_AMOUNT"+i).split(" ")[1]
				// :request.getParameter("CCC_TXN_AMOUNT"+i).split(" ")[0];
				CompanyccsetupDO.setCCC_TXN_AMOUNT(amt);
			} else {
				CompanyccsetupDO.setCCC_TXN_AMOUNT("0.00");
			}
			CompanyccsetupDO.setCCC_TXN_AMT_PERCENTAGE(request.getParameter("CCC_TXN_AMT_PERCENTAGE" + i).replace("$ ", ""));
			if (request.getParameter("CCC_SWIPE_TYPE" + i) != null) {
				CompanyccsetupDO.setCCC_SWIPE_TYPE(request.getParameter("CCC_SWIPE_TYPE" + i));
			}
			al_list.add(CompanyccsetupDO);
		}
		return al_list;
	}

	public void driverPenalityMaster(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		ArrayList al_list = null, al_view = null;
		StringBuffer error = null;
		PenalityBean penalityBean = null;
		String pagefor;
		String Button = request.getParameter("Button");

		if (Button == null) {
			al_list = RegistrationDAO.getDriverPenality(m_adminBO.getAssociateCode());
			request.setAttribute("screen", "/Company/DriverPenality.jsp");
		} else if (Button.equalsIgnoreCase("Submit")) {
			al_view = assignDriverPenalityBean(request, response, m_adminBO.getAssociateCode());
			error = validatePenalityChargeBean(request, response, m_adminBO.getAssociateCode());

			if (!(error.length() > 3)) {
				RegistrationDAO.insertDriverPenality(al_view, "insert", m_adminBO.getAssociateCode());
				al_view = null;
				request.setAttribute("sucess", "Successfully Inserted");
				request.setAttribute("screen", "/Company/DriverPenality.jsp");
			} else {

				request.setAttribute("screen", "/Company/DriverPenality.jsp");
			}
			pagefor = "Submit";
		} else {

			String check = request.getParameter("checkedval1");
			if (check != null) {
				boolean s = RegistrationDAO.delDriverPenality(check, m_adminBO);
				if (check != "")
					request.setAttribute("remove", "Successfully Deleted ");
			}

			request.setAttribute("screen", "/Company/DriverPenality.jsp");
		}
		// request.setAttribute("pagefor", pagefor);
		al_list = RegistrationDAO.getDriverPenality(m_adminBO.getAssociateCode());
		request.setAttribute("error", error);
		request.setAttribute("al_list", al_list);
		request.setAttribute("al_view", al_view);
		request.setAttribute("al_driver", UtilityDAO.getDriverList(m_adminBO.getAssociateCode(), 1));

		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}

	public void cmpPaymentSettelmentSch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		ArrayList al_list = null;
		StringBuffer error = null;
		String pagefor;
		String Button = request.getParameter("Button");

		if (Button == null) {
			al_list = RegistrationDAO.getCmpPaySetSch(m_adminBO.getAssociateCode());
			pagefor = al_list == null ? "Submit" : "Update";

		} else if (Button.equalsIgnoreCase("Submit")) {
			al_list = assignCmpSeteledBean(request, response, m_adminBO.getAssociateCode());
			error = validateCmpSeteledBean(request, response);
			if (!(error.length() > 0)) {
				RegistrationDAO.insertCmpPaySetSch(al_list, "insert", m_adminBO.getAssociateCode());
				int ano = AuditDAO.insertauditRequest(m_adminBO.getAssociateCode(), m_adminBO.getUname(), "New payment Schedule insert on " + System.currentTimeMillis(), "", "", "101", "", "");
				request.setAttribute("success", "Inserted and aduit trail no is:- " + ano);

				al_list = RegistrationDAO.getCmpPaySetSch(m_adminBO.getAssociateCode());
				pagefor = al_list == null ? "Submit" : "Update";

			} else {
				pagefor = "Submit";

			}

		} else {

			al_list = assignCmpSeteledBean(request, response, m_adminBO.getAssociateCode());
			error = validateCmpSeteledBean(request, response);
			if (!(error.length() > 0)) {
				RegistrationDAO.insertCmpPaySetSch(al_list, "update", m_adminBO.getAssociateCode());
				int ano = AuditDAO.insertauditRequest(m_adminBO.getAssociateCode(), m_adminBO.getUname(), "Setling a payment has Update on " + System.currentTimeMillis(), "", "", "102", "", "");
				request.setAttribute("success", "Updated Successfully. Audit trail no is:- " + ano);

				al_list = RegistrationDAO.getCmpPaySetSch(m_adminBO.getAssociateCode());
				pagefor = al_list == null ? "Submit" : "Update";

			} else {
				pagefor = "Update";
			}
		}
		request.setAttribute("pagefor", pagefor);
		request.setAttribute("error", error);
		request.setAttribute("al_list", al_list);
		request.setAttribute("screen", "/Company/CompanyPaySchedule.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}

	public void driverChargesMaster(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		ArrayList al_list = null;
		DriverChargeBean driverChargeBean = null;
		StringBuffer error = null;
		String pagefor;
		String Button = request.getParameter("Button");

		if (Button == null) {
			al_list = RegistrationDAO.getDriverCharges(m_adminBO.getAssociateCode());

			request.setAttribute("screen", "/Company/DriverCahrges.jsp");

			if (al_list == null) {
				pagefor = "Submit";
				driverChargeBean = new DriverChargeBean();
				al_list = new ArrayList();
				al_list.add(driverChargeBean);
			} else {
				pagefor = "Update";
			}
		} else if (Button.equalsIgnoreCase("Submit")) {
			al_list = assignDriverChargeBean(request, response, m_adminBO.getAssociateCode());
			error = validateDriverChargeBean(request, response);
			if (!(error.length() > 0)) {
				RegistrationDAO.insertDriverCharge(al_list, "insert", m_adminBO.getAssociateCode());
				request.setAttribute("success", "Inserted Charge(s)");
				al_list = RegistrationDAO.getDriverCharges(m_adminBO.getAssociateCode());
				request.setAttribute("screen", "/Company/DriverCahrges.jsp");
				// request.setAttribute("screen", "/Success.jsp");\
				pagefor = "Update";
			} else {
				pagefor = "Submit";
				request.setAttribute("screen", "/Company/DriverCahrges.jsp");
			}

		} else {

			al_list = assignDriverChargeBean(request, response, m_adminBO.getAssociateCode());
			error = validateDriverChargeBean(request, response);
			if (!(error.length() > 0)) {
				RegistrationDAO.insertDriverCharge(al_list, "update", m_adminBO.getAssociateCode());
				al_list = RegistrationDAO.getDriverCharges(m_adminBO.getAssociateCode());
				request.setAttribute("page", "Updated");
				request.setAttribute("success", "Successfully Updated");
				request.setAttribute("screen", "/Company/DriverCahrges.jsp");
				// request.setAttribute("screen", "/Success.jsp");

			} else {
				request.setAttribute("screen", "/Company/DriverCahrges.jsp");
			}
			pagefor = "Update";
		}
		request.setAttribute("pagefor", pagefor);
		request.setAttribute("error", error);
		request.setAttribute("al_list", al_list);

		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public StringBuffer validateCmpSeteledBean(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuffer error = new StringBuffer();
		if (request.getParameter("sch_driverchstatus0").equalsIgnoreCase("2")) {
			error.append(" Choose Driver Charges Run Day<br>");
		}
		for (int i = 0; i < 7; i++) {
			// System.out.println(request.getParameter("checkvalue").split(";")[i]);
			// if(request.getParameter("sch_status"+i)!=null &&
			// request.getParameter("sch_status"+i).equalsIgnoreCase("1"))
			if (request.getParameter("checkvalue").split(";")[i].equalsIgnoreCase("1")) {
				if (TDSValidation.isValid12HrsTime(request.getParameter("sch_time" + i)) == 0) {
					error.append(request.getParameter("sch_day_name" + i) + " Time Format is not Valid<br>");
				}

			}

		}

		return error;

	}

	public StringBuffer validateDriverChargeBean(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		StringBuffer error = new StringBuffer();

		String size = request.getParameter("size") == null ? "0" : request.getParameter("size");

		for (int i = 0; i <= Integer.parseInt(size); i++) {
			if (request.getParameter("DC_STATUS" + i).equalsIgnoreCase("1")) {
				if (request.getParameter("DC_DESC" + i).equals("")) {
					error.append(" Pls Provide Description At Row " + (i + 1) + " <br>");
				}
				String check = request.getParameter("DC_AMOUNT" + i).split(" ")[0].equals("$") ? request.getParameter("DC_AMOUNT" + i).split(" ")[1] : request.getParameter("DC_AMOUNT" + i);
				// System.out.println("---------------------------"+check);
				if (!TDSValidation.isDoubleNumber(check)) {
					error.append(" Amount is Invalid At Row " + (i + 1) + " <br>");
				}
			}

		}

		return error;

	}

	public StringBuffer validatePenalityChargeBean(HttpServletRequest request, HttpServletResponse response, String assocode) throws ServletException, IOException {

		StringBuffer error = new StringBuffer();
		HttpSession m_session = request.getSession();
		String size = request.getParameter("size") == null ? "0" : request.getParameter("size");
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		for (int i = 0; i <= Integer.parseInt(size); i++) {
			String Driverid = request.getParameter("DP_DRIVER_ID" + i);
			if (request.getParameter("DP_DESC" + i).equals("")) {
				error.append(" Pls Provide Description At Row " + (i + 1) + " <br>");
			} else if (!UtilityDAO.Assocodedriveridmatch(Driverid, assocode)) {
				error.append("Driver does not belong to Company<br> Pls Provide Correct Driver ID At Row " + (i + 1) + " <br>");
			}
			if (Driverid.equals("")) {
				error.append(" Pls Provide Correct DriverID At Row " + (i + 1) + " <br>");
			}
			String check = request.getParameter("DP_AMOUNT" + i);
			if (check != null && check != "" && !(check.trim()).equals("$")) {
				check = request.getParameter("DP_AMOUNT" + i).split(" ")[0].equals("$") ? request.getParameter("DP_AMOUNT" + i).split(" ")[1] : request.getParameter("DP_AMOUNT" + i).split(" ")[0];
				if (!TDSValidation.isOnlyNumbers(check) && !TDSValidation.isDoubleNumber(check)) {
					error.append(" Amount is Invalid At Row " + (i + 1) + " <br>");
				}
			} else {
				error.append(" Amount is Invalid At Row " + (i + 1) + " <br>");
			}
			if (!(request.getParameter("DP_PENALITY_DATE" + i).length() == 10)) {
				error.append(" Pls Provide Valid Credit/charge Date At Row " + (i + 1) + " <br>");
			}

		}

		return error;

	}

	public ArrayList assignCmpSeteledBean(HttpServletRequest request, HttpServletResponse response, String asscode) throws ServletException, IOException {

		ArrayList al_list = new ArrayList();

		for (int i = 0; i < 7; i++) {
			CmpPaySeteledBean seteledBean = new CmpPaySeteledBean();
			seteledBean.setC_assocode(asscode);
			seteledBean.setDchargesrunstatus(request.getParameter("sch_driverchstatus" + i));
			seteledBean.setC_sch_day(request.getParameter("sch_day" + i));
			seteledBean.setC_sch_day_name(request.getParameter("sch_day_name" + i));
			seteledBean.setC_sch_time(request.getParameter("sch_time" + i));
			seteledBean.setC_AMPM(request.getParameter("AMPM" + i));
			seteledBean.setC_sch_status(request.getParameter("checkvalue").split(";")[i]);

			al_list.add(seteledBean);
		}

		return al_list;
	}

	public ArrayList assignDriverChargeBean(HttpServletRequest request, HttpServletResponse response, String asscode) throws ServletException, IOException {
		String St = "";
		ArrayList al_list = new ArrayList();
		String size = request.getParameter("size") == null ? "0" : request.getParameter("size");

		for (int i = 0; i < Integer.parseInt(size); i++) {

			DriverChargeBean chargeBean = new DriverChargeBean();

			chargeBean.setDC_ASSOCODE(asscode);
			chargeBean.setDC_CHARGE_NO(request.getParameter("DC_CHARGE_NO" + i));
			chargeBean.setDC_DESC(request.getParameter("DC_DESC" + i));
			chargeBean.setDC_FREQUECY(request.getParameter("DC_FREQUECY" + i));
			if (request.getParameter("DC_FREQUECY" + i).equalsIgnoreCase("2"))
				chargeBean.setDC_LDAY(request.getParameter("lday" + i));
			else
				chargeBean.setDC_LDAY("0");

			String ss = request.getParameter("DC_AMOUNT" + i).split(" ")[0].equals("$") ? request.getParameter("DC_AMOUNT" + i).split(" ")[1] : request.getParameter("DC_AMOUNT" + i);
			chargeBean.setDC_AMOUNT(ss);
			chargeBean.setDC_STATUS(request.getParameter("DC_STATUS" + i));

			al_list.add(chargeBean);
		}
		return al_list;
	}

	public ArrayList assignDriverPenalityBean(HttpServletRequest request, HttpServletResponse response, String asscode) throws ServletException, IOException {

		ArrayList al_list = new ArrayList();
		String size = request.getParameter("size") == null ? "0" : request.getParameter("size");
		// System.out.println("size:"+size);

		for (int i = 0; i <= Integer.parseInt(size); i++) {
			PenalityBean penalityBean = new PenalityBean();
			String check = request.getParameter("DP_AMOUNT" + i);
			if (request.getParameter("DP_AMOUNT" + i) != null && request.getParameter("DP_AMOUNT" + i) != "" && !(request.getParameter("DP_AMOUNT" + i).trim()).equals("$")) {
				check = request.getParameter("DP_AMOUNT" + i).split(" ")[0].equals("$") ? request.getParameter("DP_AMOUNT" + i).split(" ")[1] : request.getParameter("DP_AMOUNT" + i).split(" ")[0];
				if (TDSValidation.isOnlyNumbers(check))
					penalityBean.setB_amount("" + (new BigDecimal(check).setScale(2, 5)));
				else
					penalityBean.setB_amount(check);
			}
			penalityBean.setB_ass_code(asscode);
			penalityBean.setB_desc(request.getParameter("DP_DESC" + i));
			penalityBean.setBtype(request.getParameter("btype" + i));
			penalityBean.setB_driver_id(request.getParameter("DP_DRIVER_ID" + i));
			penalityBean.setB_p_date(request.getParameter("DP_PENALITY_DATE" + i));
			penalityBean.setB_trans(request.getParameter("DP_TRANS_ID" + i));

			al_list.add(penalityBean);
		}

		return al_list;
	}

	/*
	 * public void addDriverRegistration(HttpServletRequest request,
	 * HttpServletResponse response) throws ServletException,IOException {
	 * cat.info("In add Driver Registration function "); String forwardURL =
	 * TDSConstants.getMainNewJSP; //String forwardURL =
	 * TDSConstants.registrationJSP; cat.info("Forward the request into the "+
	 * forwardURL +" JPS"); request.setAttribute("screen",
	 * TDSConstants.registrationJSP); RequestDispatcher requestDispatcher =
	 * getServletContext().getRequestDispatcher(forwardURL);
	 * requestDispatcher.forward(request, response); }
	 */

	public void saveDriverRegistration(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		cat.info("In Save Driver Registration function");
		// request.setAttribute("drFlag",
		// RegistrationDAO.getCmpyVehicleFlag(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode(),
		// "1"));
		// \System.out.println("SAVE REGISTRATION");
		HttpSession session = request.getSession();

		// System.out.println("savedrivrregistraion");

		if (request.getParameter("subevent") == null) {

			DriverRegistrationBO driverBO = new DriverRegistrationBO();
			driverBO = setDriverData(driverBO, request);
			
			String error = RegistrationValidation.driverValidation(driverBO);
			String screen = "";
			AdminRegistrationBO adminBO = null;
			String forwardURL = TDSConstants.getMainNewJSP;
			if (session.getAttribute("user") != null) {
				adminBO = (AdminRegistrationBO) session.getAttribute("user");
				driverBO.setAssociateCode(adminBO.getAssociateCode());
			}
			
			if (error.length() > 0) {
				screen = TDSConstants.registrationJSP;
				request.setAttribute("errors", error);
			} else if (UtilityDAO.isExistOrNot("select DR_USERID from TDS_DRIVER where DR_USERID='" + adminBO.getAssociateCode() + driverBO.getUid() + "'")) {
				screen = TDSConstants.registrationJSP;
				request.setAttribute("errors", "User Name Already Exit");
			} else {
				// Appending association code to the give driverid.
				driverBO.setUid(adminBO.getAssociateCode() + driverBO.getUid());
				int result = RegistrationDAO.insertDriverManager(driverBO, adminBO);
				if (result == 1) {
					request.setAttribute("page", " Created new Driver");
					// forwardURL = TDSConstants.getSuccessJSP;
					screen = TDSConstants.getSuccessJSP;
				} else {
					request.setAttribute("errors", "User Name Not Registered Yet");
					screen = TDSConstants.registrationJSP;
				}
			}
			request.setAttribute("driverBO", driverBO);
			request.setAttribute("screen", screen);
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			reqDispatcher.forward(request, response);
		} else {

			cat.info("In add Driver Registration function ");
			String forwardURL = TDSConstants.getMainNewJSP;
			// String forwardURL = TDSConstants.registrationJSP;
			cat.info("Forward the request into the " + forwardURL + " JPS");
			request.setAttribute("screen", TDSConstants.registrationJSP);
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			requestDispatcher.forward(request, response);
		}
	}

	public void checkUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {

		HttpSession session = null;
		cat.info("In Check User function ");
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.getLoginJSP;

		String provider = "";
		String phno = "";
		String cabno = "";
		String userName = request.getParameter("loginName");
		String password = request.getParameter("password");
		String version = request.getParameter("version");
		String ipAddress = request.getRemoteAddr();

		boolean cookieIsValid = false;
		boolean validIPAddress = false;
		AdminRegistrationBO adminBo = null;
		if (userName == null && password == null) {
			request.setAttribute("screen", screen);
			request.setAttribute("errors", "");
		} else if (userName != "" && password != "") {
			String passwordHash = PasswordHash.encrypt(password);
			adminBo = RegistrationDAO.getUserAvailable(userName, passwordHash, provider, phno, cabno);
			String associationCode = SecurityDAO.fleetOfUser(adminBo);
			if (associationCode != null && !associationCode.equals("")) {
				adminBo.setAssociateCode(associationCode);
			}
			if (adminBo.getIsAvailable() == 1) {
				session = request.getSession();
				// Cookie check to see if the system is a valid system

				Cookie[] delCookies = request.getCookies();

				// if(delCookies!=null){
				// for(int j=0;j<delCookies.length;j++){
				// if(delCookies[j].getName().equalsIgnoreCase("GAC-userName")&&!delCookies[j].getValue().equalsIgnoreCase(userName)){
				// for(int i=0;i<delCookies.length;i++){
				// if(delCookies[i].getName().substring(0,3).equalsIgnoreCase("GAC")){
				// delCookies[i].setValue(null);
				// delCookies[i].setMaxAge(0);
				// response.addCookie(delCookies[i]);
				// }
				// }
				// ArrayList<TagSystemBean> al_list =
				// RequestDAO.readCookiesForAll(adminBo.getUid(),
				// adminBo.getAssociateCode());
				// for(int i=0;i<al_list.size();i++){
				// Cookie cookie = new
				// Cookie(al_list.get(i).getName(),al_list.get(i).getValue());
				// cookie.setMaxAge(365*68*24*60*60);
				// response.addCookie(cookie);
				// }
				// }else{
				// ArrayList<TagSystemBean> al_list =
				// RequestDAO.readCookiesForAll(adminBo.getUid(),
				// adminBo.getAssociateCode());
				// for(int i=0;i<al_list.size();i++){
				// Cookie cookie = new
				// Cookie(al_list.get(i).getName(),al_list.get(i).getValue());
				// cookie.setMaxAge(365*68*24*60*60);
				// response.addCookie(cookie);
				// }
				// }
				// continue;
				// }
				// }

				String tagId = "";

				if (delCookies != null) {
					for (int i = 0; i < delCookies.length; i++) {
						if (delCookies[i].getName().equals("tagId")) {
							tagId = delCookies[i].getValue();
							break;
						}
					}
				}
				if (!UtilityDAO.isHavingRights("Security", "passKeyGen", (adminBo.getUid()))) {
					if (SecurityDAO.compareIPAddress(ipAddress, adminBo.getAssociateCode())) {
						validIPAddress = true;
					}
				}

				if (!tagId.equals("")) {
					if (SecurityDAO.compareCookie(tagId, adminBo.getAssociateCode(), adminBo.getUid())) {
						cookieIsValid = true;
					} else if (UtilityDAO.isHavingRights("Security", "passKeyGen", (adminBo.getUid()))) {
						cookieIsValid = true;
					} else {
						request.setAttribute("screen", "/Security/Password.jsp");
						screen = "/Security/Password.jsp";
					}

				} else {
					if (UtilityDAO.isHavingRights("Security", "passKeyGen", (adminBo.getUid()))) {
						cookieIsValid = true;
					} else {
						request.setAttribute("screen", "/Security/Password.jsp");
						screen = "/Security/Password.jsp";
					}
				}
				if (cookieIsValid || validIPAddress) {
					RegistrationDAO.InsertLoginDetail(adminBo.getAssociateCode(), adminBo.getUid(), session.getId(), "1", 0, System.currentTimeMillis(), adminBo.getProvider(), adminBo.getPhnumber(), version, adminBo.getDriver_flag(), cabno, "", ipAddress, adminBo.getMasterAssociateCode(), "");
					double[] ztb = ZoneDAO.getDefaultLocation(adminBo.getMasterAssociateCode());
					if (ztb != null) {
						adminBo.setDefaultLati(ztb[0]);
						adminBo.setDefaultLogi(ztb[1]);
					}
					CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(adminBo.getMasterAssociateCode());
					adminBo.setCountry(cspBO.getCountry());
					adminBo.setDefaultLanguage(cspBO.getDefaultLanguage());
					adminBo.setDispatchBasedOnVehicleOrDriver(cspBO.getDispatchBasedOnDriverOrVehicle());
					adminBo.setCallerId((cspBO.getCallerId()));
					adminBo.setTimeZoneOffet(cspBO.getTimezone());
					adminBo.setORTime(cspBO.getORTime());
					adminBo.setORFormat(cspBO.getORFormat());
					adminBo.setCurrencyPrefix(cspBO.getCurrencyPrefix());
					adminBo.setRatePerMile(cspBO.getRatePerMile());
					adminBo.setCheckDocuments(cspBO.getCheckDocuments());
					adminBo.setAddressFill(cspBO.getAddressFill());
					adminBo.setStartAmount(cspBO.getStartAmount());
					adminBo.setDistanceBasedOn(cspBO.getDistanceBasedOn());
					adminBo.setMeterMandatory(cspBO.isMeterMandatory());
					adminBo.setWindowTime(cspBO.getWindowTime());
					adminBo.setLoginLimitTime(cspBO.getLoginLimitTime());
					adminBo.setTimeZone(cspBO.getTimeZoneArea());
					// System.out.println("Main Action--->"+cspBO.getTagDeviceCab());
					adminBo.setTagDeviceCab(cspBO.getTagDeviceCab());
					adminBo.setDriverListMobile(cspBO.getDriverListMobile());
					adminBo.setFareByDistance(cspBO.getDistanceValueToFare());
					adminBo.setZoneLogoutTime(cspBO.getZoneLogoutTime());
					adminBo.setPhonePrefix(cspBO.getMobilePrefix());
					adminBo.setSmsPrefix(cspBO.getSmsPrefix());
					adminBo.setDispatchBroadcast(cspBO.getSendBCJ());
					adminBo.setMeterHidden(cspBO.isStartmeterHidden());
					adminBo.setCallerIdType(cspBO.getCallerIdType());
					adminBo.setFleetDispatch(cspBO.getFleetDispatchSwitch());
					
					adminBo.setCheckDriverMobile(cspBO.getCheckDriverMobile());
					
					int result = SystemPropertiesDAO.readShiftRegisterMaster(adminBo.getAssociateCode(), adminBo.getUid());
					adminBo.setShiftMode(result);
					session.setAttribute("fleetList", RequestDAO.readFleetsForCompany(adminBo.getMasterAssociateCode()));
					screen = "";
					cat.info("User session " + session.getId());
					cat.info("Session Creation Time " + session.getCreationTime());
					session.setAttribute("user", adminBo);

					ObjectMapper mapper = new ObjectMapper();
					System.out.println(mapper.writeValueAsString(adminBo));

					request.setAttribute("screen", screen);
					String value = SystemPropertiesDAO.getParameter(adminBo.getAssociateCode(), "TimeOutOperator");
					if (!value.equals("") && !value.equals("0")) {
						session.setMaxInactiveInterval(Integer.parseInt(value) * 60);
					}
					if (getServletConfig().getServletContext().getAttribute(adminBo.getMasterAssociateCode() + "ZonesLoaded") == null) {
						// System.out.println("Loading zone for the first time for company"
						// + adminBo.getMasterAssociateCode());
						SystemUtils.reloadZones(this, adminBo.getMasterAssociateCode());
					}
					HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBo.getMasterAssociateCode() + "Flags");
					if (companyFlags == null) {
						SystemUtils.reloadCompanyFlags(this, adminBo.getMasterAssociateCode());
					}
					adminBo.setCompanyList(SecurityDAO.getAllAssoccode(adminBo.getMasterAssociateCode()));
					// if(CompanyBean.getCompanyProperties().isEmpty()){
					// System.out.println("Loading properties for first time for the company--->"+adminBo.getMasterAssociateCode());
					// ArrayList properties = TDSProperties.getValueAll();
					// HashMap<String, String> hashProperties = new
					// HashMap<String, String>();
					// for(int i=0;i<properties.size();i=i+2){
					// hashProperties.put(properties.get(i)+"",
					// properties.get(i+1)+"");
					// }
					// CompanyBean.setCompanyProperties(hashProperties);
					// }

				} else {
					// The passkey screen is about to the thrown. So storing the
					// assoicaiton code to match
					// it against the tables when the operator provides the pass
					// key
					RegistrationDAO.InsertLoginAttempts(userName, passwordHash, ipAddress, "PassKey entered is wrong");
					session.setAttribute("associationCode", adminBo.getAssociateCode());
					session.setAttribute("userId", adminBo.getUid());
				}

			} else {
				RegistrationDAO.InsertLoginAttempts(userName, password, ipAddress, "Invalid Username or Password");
				request.setAttribute("errors", "Invalid Username or Password");
				request.setAttribute("screen", screen);
			}
		} else {
			RegistrationDAO.InsertLoginAttempts(userName, password, ipAddress, "Username or Password is not entered");
			request.setAttribute("screen", screen);
			request.setAttribute("errors", "You must enter Username and/or Password");
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	
	public void checkUserAvailable(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {

		HttpSession session = null;
		cat.info("In Check User function ");
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.getLoginJSP;

		String provider = "";
		String phno = "";
		String cabno = "";
		String userName = request.getParameter("loginName");
		String password = request.getParameter("password");
		String version = request.getParameter("version");
		String ipAddress = request.getRemoteAddr();
		JSONArray array = new JSONArray();
		boolean cookieIsValid = false;
		boolean validIPAddress = false;
		AdminRegistrationBO adminBo = null;
		if (userName == null && password == null) {
			JSONObject loginValues = new JSONObject();
			try {
				loginValues.put("rV", 1.2);
				loginValues.put("Act", "LI");
				loginValues.put("rC", 400);
				loginValues.put("Login", "InValid");
				loginValues.put("Msg", "All Fields are Mandatory");
				array.put(loginValues);
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			response.getWriter().write(array.toString());
		} else if (userName != "" && password != "") {
			String passwordHash = PasswordHash.encrypt(password);
			adminBo = RegistrationDAO.getUserAvailable(userName, passwordHash, provider, phno, cabno);
			String associationCode = SecurityDAO.fleetOfUser(adminBo);
			if (associationCode != null && !associationCode.equals("")) {
				adminBo.setAssociateCode(associationCode);
			}
			if (adminBo.getIsAvailable() == 1) {
				session = request.getSession();
				// Cookie check to see if the system is a valid system

				Cookie[] delCookies = request.getCookies();

				// if(delCookies!=null){
				// for(int j=0;j<delCookies.length;j++){
				// if(delCookies[j].getName().equalsIgnoreCase("GAC-userName")&&!delCookies[j].getValue().equalsIgnoreCase(userName)){
				// for(int i=0;i<delCookies.length;i++){
				// if(delCookies[i].getName().substring(0,3).equalsIgnoreCase("GAC")){
				// delCookies[i].setValue(null);
				// delCookies[i].setMaxAge(0);
				// response.addCookie(delCookies[i]);
				// }
				// }
				// ArrayList<TagSystemBean> al_list =
				// RequestDAO.readCookiesForAll(adminBo.getUid(),
				// adminBo.getAssociateCode());
				// for(int i=0;i<al_list.size();i++){
				// Cookie cookie = new
				// Cookie(al_list.get(i).getName(),al_list.get(i).getValue());
				// cookie.setMaxAge(365*68*24*60*60);
				// response.addCookie(cookie);
				// }
				// }else{
				// ArrayList<TagSystemBean> al_list =
				// RequestDAO.readCookiesForAll(adminBo.getUid(),
				// adminBo.getAssociateCode());
				// for(int i=0;i<al_list.size();i++){
				// Cookie cookie = new
				// Cookie(al_list.get(i).getName(),al_list.get(i).getValue());
				// cookie.setMaxAge(365*68*24*60*60);
				// response.addCookie(cookie);
				// }
				// }
				// continue;
				// }
				// }

				String tagId = "";

				if (delCookies != null) {
					for (int i = 0; i < delCookies.length; i++) {
						if (delCookies[i].getName().equals("tagId")) {
							tagId = delCookies[i].getValue();
							break;
						}
					}
				}
				if (!UtilityDAO.isHavingRights("Security", "passKeyGen", (adminBo.getUid()))) {
					if (SecurityDAO.compareIPAddress(ipAddress, adminBo.getAssociateCode())) {
						validIPAddress = true;
					}
				}

				if (!tagId.equals("")) {
					if (SecurityDAO.compareCookie(tagId, adminBo.getAssociateCode(), adminBo.getUid())) {
						cookieIsValid = true;
					} else if (UtilityDAO.isHavingRights("Security", "passKeyGen", (adminBo.getUid()))) {
						cookieIsValid = true;
					} else {
						//request.setAttribute("screen", "/Security/Password.jsp");
						//screen = "/Security/Password.jsp";
					}

				} else {
					if (UtilityDAO.isHavingRights("Security", "passKeyGen", (adminBo.getUid()))) {
						cookieIsValid = true;
					} else {
						//request.setAttribute("screen", "/Security/Password.jsp");
						//screen = "/Security/Password.jsp";
					}
				}
				if (cookieIsValid || validIPAddress) {
					RegistrationDAO.InsertLoginDetail(adminBo.getAssociateCode(), adminBo.getUid(), session.getId(), "1", 0, System.currentTimeMillis(), adminBo.getProvider(), adminBo.getPhnumber(), version, adminBo.getDriver_flag(), cabno, "", ipAddress, adminBo.getMasterAssociateCode(), "");
					double[] ztb = ZoneDAO.getDefaultLocation(adminBo.getMasterAssociateCode());
					if (ztb != null) {
						adminBo.setDefaultLati(ztb[0]);
						adminBo.setDefaultLogi(ztb[1]);
					}
					CompanySystemProperties cspBO = SystemPropertiesDAO.getCompanySystemPropeties(adminBo.getMasterAssociateCode());
					adminBo.setCountry(cspBO.getCountry());
					adminBo.setDefaultLanguage(cspBO.getDefaultLanguage());
					adminBo.setDispatchBasedOnVehicleOrDriver(cspBO.getDispatchBasedOnDriverOrVehicle());
					adminBo.setCallerId((cspBO.getCallerId()));
					adminBo.setTimeZoneOffet(cspBO.getTimezone());
					adminBo.setORTime(cspBO.getORTime());
					adminBo.setORFormat(cspBO.getORFormat());
					adminBo.setCurrencyPrefix(cspBO.getCurrencyPrefix());
					adminBo.setRatePerMile(cspBO.getRatePerMile());
					adminBo.setCheckDocuments(cspBO.getCheckDocuments());
					adminBo.setAddressFill(cspBO.getAddressFill());
					adminBo.setStartAmount(cspBO.getStartAmount());
					adminBo.setDistanceBasedOn(cspBO.getDistanceBasedOn());
					adminBo.setMeterMandatory(cspBO.isMeterMandatory());
					adminBo.setWindowTime(cspBO.getWindowTime());
					adminBo.setLoginLimitTime(cspBO.getLoginLimitTime());
					adminBo.setTimeZone(cspBO.getTimeZoneArea());
					// System.out.println("Main Action--->"+cspBO.getTagDeviceCab());
					adminBo.setTagDeviceCab(cspBO.getTagDeviceCab());
					adminBo.setDriverListMobile(cspBO.getDriverListMobile());
					adminBo.setFareByDistance(cspBO.getDistanceValueToFare());
					adminBo.setZoneLogoutTime(cspBO.getZoneLogoutTime());
					adminBo.setPhonePrefix(cspBO.getMobilePrefix());
					adminBo.setSmsPrefix(cspBO.getSmsPrefix());
					adminBo.setDispatchBroadcast(cspBO.getSendBCJ());
					adminBo.setMeterHidden(cspBO.isStartmeterHidden());
					adminBo.setCallerIdType(cspBO.getCallerIdType());
					adminBo.setFleetDispatch(cspBO.getFleetDispatchSwitch());
					
					adminBo.setCheckDriverMobile(cspBO.getCheckDriverMobile());
					
					int result = SystemPropertiesDAO.readShiftRegisterMaster(adminBo.getAssociateCode(), adminBo.getUid());
					adminBo.setShiftMode(result);
					session.setAttribute("fleetList", RequestDAO.readFleetsForCompany(adminBo.getMasterAssociateCode()));
					screen = "";
					cat.info("User session " + session.getId());
					cat.info("Session Creation Time " + session.getCreationTime());
					session.setAttribute("user", adminBo);

					ObjectMapper mapper = new ObjectMapper();
					System.out.println(mapper.writeValueAsString(adminBo));

				//	request.setAttribute("screen", screen);
					String value = SystemPropertiesDAO.getParameter(adminBo.getAssociateCode(), "TimeOutOperator");
					/*if (!value.equals("") && !value.equals("0")) {
						session.setMaxInactiveInterval(Integer.parseInt(value) * 60);
					}
					if (getServletConfig().getServletContext().getAttribute(adminBo.getMasterAssociateCode() + "ZonesLoaded") == null) {
						// System.out.println("Loading zone for the first time for company"
						// + adminBo.getMasterAssociateCode());
						SystemUtils.reloadZones(this, adminBo.getMasterAssociateCode());
					}
					HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBo.getMasterAssociateCode() + "Flags");
					if (companyFlags == null) {
						SystemUtils.reloadCompanyFlags(this, adminBo.getMasterAssociateCode());
					}
					adminBo.setCompanyList(SecurityDAO.getAllAssoccode(adminBo.getMasterAssociateCode()));
			*/		// if(CompanyBean.getCompanyProperties().isEmpty()){
					// System.out.println("Loading properties for first time for the company--->"+adminBo.getMasterAssociateCode());
					// ArrayList properties = TDSProperties.getValueAll();
					// HashMap<String, String> hashProperties = new
					// HashMap<String, String>();
					// for(int i=0;i<properties.size();i=i+2){
					// hashProperties.put(properties.get(i)+"",
					// properties.get(i+1)+"");
					// }
					// CompanyBean.setCompanyProperties(hashProperties);
					// }
					
				
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 200);
						loginValues.put("Login", "Valid");
						loginValues.put("SessionID", session.getId());
						loginValues.put("GPID", TDSProperties.getValue("GCMPID"));
						loginValues.put("zoneLogout", cspBO.getZoneLogoutTime());
						loginValues.put("minimumSpeed", cspBO.getMinimumSpeed());
						loginValues.put("ratePerMile", cspBO.getRatePerMile());
						loginValues.put("ratePerMinute", cspBO.getRatePerMinute());
						loginValues.put("startAmount", cspBO.getStartAmount());
						loginValues.put("meter", cspBO.isMeterMandatory());
						loginValues.put("startMeter", cspBO.isStartMeterAutomatically());
						loginValues.put("hideMeter", cspBO.isStartmeterHidden());
						loginValues.put("hideMeter", cspBO.isStartmeterHidden());
						loginValues.put("noProgressAlert", cspBO.getNoProgress());
						loginValues.put("meterOnAlert", cspBO.getMeterOn());
						loginValues.put("remoteNoTripAlert", cspBO.getRemoteNoTrip());
						loginValues.put("noTripTooSoonAlert", cspBO.getNoTripTooSoon());
						loginValues.put("distanceNoProgressAlert", cspBO.getDistanceNoProgress());
						loginValues.put("distanceMeterOnAlert", cspBO.getDistanceMeterOn());
						loginValues.put("distanceRemoteNOTripAlert", cspBO.getDistanceRemoteNoTrip());
						loginValues.put("distanceNoTripTooSoonAlert", cspBO.getDistanceNoTripTooSoon());
						loginValues.put("assocode", adminBo.getAssociateCode());
						array.put(loginValues);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					response.getWriter().write(array.toString());
					
				} else {
					JSONObject loginValues = new JSONObject();
					try {
						loginValues.put("rV", 1.2);
						loginValues.put("Act", "LI");
						loginValues.put("rC", 400);
						loginValues.put("Login", "InValid");
						loginValues.put("Msg", "PassKey entered is wrong");
						array.put(loginValues);
					} catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}
					response.getWriter().write(array.toString());
				}

			} else {
				JSONObject loginValues = new JSONObject();
				try {
					loginValues.put("rV", 1.2);
					loginValues.put("Act", "LI");
					loginValues.put("rC", 400);
					loginValues.put("Login", "InValid");
					loginValues.put("Msg", "Invalid Username or Password");
					array.put(loginValues);
				} catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
				response.getWriter().write(array.toString());

			}
		} else {
			JSONObject loginValues = new JSONObject();
			try {
				loginValues.put("rV", 1.2);
				loginValues.put("Act", "LI");
				loginValues.put("rC", 400);
				loginValues.put("Login", "InValid");
				loginValues.put("Msg", "User name And Password are Mandatory");
				array.put(loginValues);
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			response.getWriter().write(array.toString());

			//RegistrationDAO.InsertLoginAttempts(userName, password, ipAddress, "Username or Password is not entered");
			//request.setAttribute("screen", screen);
			//request.setAttribute("errors", "You must enter Username and/or Password");
		}
		//RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		//requestDispatcher.forward(request, response);
		response.getWriter().write(array.toString());
		System.out.println("array--->"+array.toString());
	}
	
	public void userLogout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		request.setAttribute("screen", TDSConstants.getLoginJSP);
		String forwardURL = TDSConstants.getMainNewJSP;
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) session.getAttribute("user");
		Cookie[] cookies = request.getCookies();
		Cookie cookie = new Cookie("GAC-userName", m_adminBO.getUid());
		cookie.setMaxAge(365 * 68 * 24 * 60 * 60);
		response.addCookie(cookie);
		// if(cookies!=null){
		// RequestDAO.updateCookies(cookies, m_adminBO.getUid(),
		// m_adminBO.getAssociateCode());
		// }

		// System.out.println("CHECK A USER LOG OUT TIME ");
		if (session != null) {
			String m_session_id = session.getId();
			RegistrationDAO.InsertLoginDetail(m_adminBO.getAssociateCode(), m_adminBO.getUname(), m_session_id, "0", 0, System.currentTimeMillis(), m_adminBO.getProvider(), m_adminBO.getPhnumber(), "", "", "", "", "", m_adminBO.getMasterAssociateCode(), "");
			// HttpSession session = request.getSession(false);
			cat.info("In User Logged out event ");
			// Isn't it better to just invalidate the session?
			// Commenting the below code
			// This will also help removing all Application Profile Variables
			// using the session listener

			// if(session.getAttribute("user") != null) {
			// session.removeAttribute("user");
			//
			// }
			session.invalidate();
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void zoneentrysubmit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ArrayList al_list = null;
		HttpSession session = request.getSession();

		DriverDisbursment disbursment = new DriverDisbursment();
		// disbursment.setDrivername(((AdminRegistrationBO)session.getAttribute("user")).getUname());
		String zone = request.getParameter("zone");
		String driver = request.getParameter("driverid") == null ? (request.getParameter("cabNo") == null ? "" : request.getParameter("cabNo")) : request.getParameter("driverid");

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		al_list = RegistrationDAO.zonelist(((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode());
		if (request.getParameter("button") == null) {
			request.setAttribute("screen", "/jsp/zoneentry.jsp");
			request.setAttribute("zonelist", al_list);
		} else {

			StringBuffer error = validzonedetail(request, ((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode());
			if (!(error.length() > 0)) {
				int success = 0;
				String currentQueue = RegistrationDAO.checkDriverCurrentlyLogggedInQueue(disbursment.getDriverid(), adminBO.getAssociateCode());
				if (currentQueue.equalsIgnoreCase("")) {
					success = RegistrationDAO.insertzonedetails(driver, ((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode(), zone, ((AdminRegistrationBO) session.getAttribute("user")).getMasterAssociateCode());
				} else if (!currentQueue.equalsIgnoreCase(al_list.get(0).toString())) {
					// Delete from current queue and add into new queue
					RegistrationDAO.deleteDriverFromQueueWithoutQueueID(disbursment.getDriverid(), adminBO.getAssociateCode());
					success = RegistrationDAO.insertzonedetails(driver, ((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode(), zone, ((AdminRegistrationBO) session.getAttribute("user")).getMasterAssociateCode());
				}
				if (success == 1) {
					error.append("Driver logged into zone/queue Successfully");
					request.setAttribute("screen", "/jsp/zoneentry.jsp");
				} else if (success == 2) {
					error.append("Driver is not logged in");
					request.setAttribute("screen", "/jsp/zoneentry.jsp");
				} else {
					error.append("Error logging driver into zone/queue");
					request.setAttribute("screen", "/jsp/zoneentry.jsp");
				}
				request.setAttribute("error", error);
			} else {
				request.setAttribute("screen", "/jsp/zoneentry.jsp");
				request.setAttribute("error", error);

				request.setAttribute("zonelist", al_list);
			}
		}
		request.setAttribute("zonelist", al_list);
		request.setAttribute("disbo", disbursment);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}

	public StringBuffer validzonedetail(HttpServletRequest request, String assocode) {
		StringBuffer error = new StringBuffer();
		if ((request.getParameter("driverid") == null || (request.getParameter("driverid").equals(""))) && (request.getParameter("cabNo") == null || request.getParameter("cabNo").equals(""))) {
			error.append("Enter A Valid Driver Number");
		}
		if (request.getParameter("driverid") != null) {
			if (!UtilityDAO.Assocodedriveridmatch(request.getParameter("driverid"), assocode)) {
				error.append("Driver Does Not Belong This Company");
			}
			if (UtilityDAO.isExistOrNot("Select * from TDS_QUEUE where QU_DRIVERID= " + request.getParameter("driverid") + "")) {
				error.append("Already Logged In Please Logout");
			}
		}
		return error;
	}

	public void driverSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String forwardURL = TDSConstants.getMainNewJSP;
		cat.info("In Driver Summary Method");
		String screen = TDSConstants.sumDriverRegister;
		// System.out.println("In Driver Summary");
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		ArrayList al_list = null;
		DriverRegistrationBO driverBO = new DriverRegistrationBO();
		try {
			// System.out.println("In Driver Summary try");
			String first_name = request.getParameter("first_name") == null ? "" : request.getParameter("first_name");
			String last_name = request.getParameter("last_name") == null ? "" : request.getParameter("last_name");
			String driver_id = request.getParameter("driver_id") == null ? "" : request.getParameter("driver_id");
			String cab_no = request.getParameter("cab_no") == null ? "" : request.getParameter("cab_no");
			String status = request.getParameter("status") == null ? "" : request.getParameter("status");

			/*
			 * System.out.println("firstname"+first_name);
			 * System.out.println("last_name"+last_name);
			 * System.out.println("driver_id"+driver_id);
			 * System.out.println("cab_no"+cab_no);
			 * System.out.println("status"+status);
			 */
			al_list = RegistrationDAO.getDriverDetailSearch(driverBO, adminBO.getAssociateCode(), first_name, last_name, driver_id, cab_no, status);

		} catch (Exception ex) {
			System.out.println("The Exception in the method driver summary  : " + ex.getMessage());
			cat.info("The Exception in the method driver summary  : " + ex.getMessage());
		}
		/*
		 * if(request.getAttribute("update")!=null){
		 * request.setAttribute("errors", "screen"); }
		 */
		request.setAttribute("screen", screen);
		request.setAttribute("DRBO", driverBO);
		request.setAttribute("driversumdata", al_list);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	/*
	 * public void driverSummary(HttpServletRequest request, HttpServletResponse
	 * response) throws ServletException,IOException { String forwardURL =
	 * TDSConstants.getMainNewJSP; cat.info("In Driver Summary Method"); String
	 * screen = TDSConstants.sumDriverRegister; HttpSession session =
	 * request.getSession(); AdminRegistrationBO adminBO =
	 * (AdminRegistrationBO)session.getAttribute("user"); ArrayList al_summary =
	 * null; double no_limit = 0; int plength=5; // Increase or Decrease the
	 * numbers in the link sequence at the bottom of the page. Example <12345>
	 * can be increased to <12345678910> int start =0; int end = 10; int
	 * no_of_rows=10; int pstart=1; int pend=plength+1; int t_length=0;
	 * DriverRegistrationBO driverBO = new DriverRegistrationBO(); try{
	 * t_length=
	 * RegistrationDAO.totalRecordLength((((AdminRegistrationBO)session.
	 * getAttribute("user")).getAssociateCode())); no_limit =
	 * Math.ceil((t_length/10.0)); String first_name =
	 * request.getParameter("first_name"
	 * )==null?"":request.getParameter("first_name"); String last_name =
	 * request.
	 * getParameter("last_name")==null?"":request.getParameter("last_name");
	 * String driver_id =
	 * request.getParameter("driver_id")==null?"":request.getParameter
	 * ("driver_id"); String cab_no =
	 * request.getParameter("cab_no")==null?"":request.getParameter("cab_no");
	 * String
	 * status=request.getParameter("status")==null?"":request.getParameter
	 * ("status"); if(request.getParameter("first") == null) {
	 * 
	 * driverBO.setStart(""+start); driverBO.setEnd(""+end);
	 * al_summary=RegistrationDAO
	 * .getDriverDetailSearch(driverBO,adminBO.getAssociateCode
	 * (),first_name,last_name,driver_id,cab_no,status);
	 * 
	 * if(no_limit<end){ pend=(int)no_limit+1; request.setAttribute("pend",
	 * pend+""); } else{ request.setAttribute("pend", pend+""); }
	 * 
	 * } else{ if(request.getParameter("pageNo")!=null ||
	 * request.getParameter("previous")!=null ||
	 * request.getParameter("next")!=null){
	 * 
	 * if(request.getParameter("pageNo")!=null){ int
	 * pagevalue=Integer.parseInt(request.getParameter("pageNo"));
	 * pstart=Integer.parseInt(request.getParameter("pstart"));
	 * pend=Integer.parseInt(request.getParameter("pend"));
	 * start=(pagevalue*no_of_rows)-no_of_rows; request.setAttribute("pend",
	 * pend+""); } if(request.getParameter("previous")!=null){ int
	 * pagevalue=Integer.parseInt(request.getParameter("previous"));
	 * System.out.println("pagevalue="+pagevalue); if(pstart<pagevalue){
	 * if(pstart<(pagevalue-plength)){ pstart=pagevalue-plength; start=pstart;
	 * if((pstart+plength)<no_limit){ pend=pstart+plength; } else{
	 * pend=(int)no_limit; } } else if(pstart==(pagevalue-plength)){
	 * pstart=pagevalue-plength; start=pstart; if(pagevalue<no_limit){
	 * pend=pagevalue; }else{ pend=(int)no_limit; } } else{ start=pstart; } }
	 * else if(pstart==pagevalue){ pstart=pagevalue; start=pstart;
	 * if((pagevalue+plength)<no_limit){ pend=pagevalue+plength; } else{
	 * pend=(int)no_limit; } } else{ start=pstart; }
	 * start=(start*no_of_rows)-no_of_rows; request.setAttribute("pend",
	 * pend+""); } if( request.getParameter("next")!=null){ int
	 * pagevalue=Integer.parseInt(request.getParameter("next"));
	 * if(pagevalue<no_limit){ if((pagevalue+plength)<=no_limit){
	 * pstart=pagevalue; start=pstart; } else{ pstart=pagevalue; start=pstart; }
	 * if((pagevalue+plength)<=no_limit){ pend=pagevalue+plength; } else{
	 * pend=(int)no_limit+1; } } else if(pagevalue==no_limit){ pstart=pagevalue;
	 * start=pstart; pend=(int)no_limit+1; } else{ pstart=pagevalue-1;
	 * start=pstart; if((pstart+plength)<no_limit){ pend=pstart+plength; } else{
	 * pend=(int)no_limit; } if(pend==pstart){ pend=pend+1; } }
	 * start=(start*no_of_rows)-no_of_rows; request.setAttribute("pend",
	 * pend+""); } driverBO.setStart(""+start); driverBO.setEnd(""+end);
	 * al_summary
	 * =RegistrationDAO.getDriverDetailSearch(driverBO,(((AdminRegistrationBO
	 * )session
	 * .getAttribute("user")).getAssociateCode()),first_name,last_name,driver_id
	 * ,cab_no,status);
	 * 
	 * 
	 * } } for(int i=0;i<t_length;i++){
	 * if(request.getParameter("page"+i)!=null){ start=(i*10)-10;
	 * driverBO.setStart(""+start); driverBO.setEnd(""+end);
	 * al_summary=RegistrationDAO
	 * .getDriverDetailSearch(driverBO,(((AdminRegistrationBO
	 * )session.getAttribute
	 * ("user")).getAssociateCode()),first_name,last_name,driver_id
	 * ,cab_no,status); } } if(al_summary.size()>0){
	 * request.setAttribute("pstart", pstart+"");
	 * request.setAttribute("plength", plength+"");
	 * request.setAttribute("startValue", start+"");
	 * request.setAttribute("limit", no_limit+"");
	 * request.setAttribute("f_name",first_name );
	 * request.setAttribute("l_name", last_name);
	 * request.setAttribute("driver_id", driver_id);
	 * request.setAttribute("cab_no", cab_no); } request.setAttribute("status",
	 * status);
	 * 
	 * } catch(Exception ex){
	 * cat.info("The Exception in the method driver summary  : "
	 * +ex.getMessage()); } request.setAttribute("screen", screen);
	 * request.setAttribute("DRBO", driverBO);
	 * request.setAttribute("driversumdata", al_summary); RequestDispatcher
	 * requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
	 * requestDispatcher.forward(request, response); }
	 */
	public void adminRegistration(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Create Admin Registration method");
		// check here

		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		screen = TDSConstants.creatAdminRegistration;
		request.setAttribute("pagefor", "createAdmin");
		cat.info("Forward the request in to " + screen);
		request.setAttribute("drkey", RegistrationDAO.Tdsdriverkey());
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	/*
	 * public void saveAdminRegistraion(HttpServletRequest
	 * request,HttpServletResponse response) throws ServletException,IOException
	 * { cat.info("In save admin Registration Page method "); String forwardURL
	 * = TDSConstants.getMainNewJSP; FFString screen =
	 * TDSConstants.creatAdminRegistration; String
	 * Button=request.getParameter("submit"); HttpSession m_session =
	 * request.getSession();
	 * 
	 * RequestDispatcher requestDispatcher =
	 * getServletContext().getRequestDispatcher(forwardURL) ; if(Button ==
	 * null){ request.setAttribute("pagefor", "createAdmin");
	 * request.setAttribute("screen", screen);
	 * 
	 * requestDispatcher.forward(request, response); } else
	 * if(Button.equalsIgnoreCase("Create admin")){
	 * request.setAttribute("pagefor", "createAdmin"); AdminRegistrationBO
	 * adminRegistrationBO = getAdminBean(request);
	 * adminRegistrationBO.setAssociateCode
	 * (((AdminRegistrationBO)request.getSession
	 * ().getAttribute("user")).getAssociateCode()); String error =
	 * checkAdminUser(adminRegistrationBO); if(error.length() == 0) {
	 * if(ConfigDAO.checkUsedIdExist(adminRegistrationBO.getUname(), " ") == 0)
	 * { int result =
	 * RegistrationDAO.saveAdminRegistration(adminRegistrationBO,"insert");
	 * if(result > 0) { request.setAttribute("page", " created new User");
	 * response
	 * .sendRedirect("control?action=security&event=insertRolesForUser&userid="
	 * +adminRegistrationBO
	 * .getUid()+"&uname="+adminRegistrationBO.getUname()+"&utype="
	 * +adminRegistrationBO
	 * .getUsertype()+"&userDesc="+adminRegistrationBO.getUsertypeDesc
	 * ()+"&status=1"); } else { request.setAttribute("page",
	 * " create new User"); screen = TDSConstants.getFailureJSP;
	 * request.setAttribute("screen", screen);
	 * requestDispatcher.forward(request, response);
	 * 
	 * } } else {
	 * 
	 * request.setAttribute("errors", "Username already Exist ");
	 * request.setAttribute("adminBO", adminRegistrationBO);
	 * request.setAttribute("screen", screen);
	 * requestDispatcher.forward(request, response); }
	 * 
	 * } else { request.setAttribute("pagefor", "createAdmin");
	 * request.setAttribute("adminBO", adminRegistrationBO);
	 * request.setAttribute("errors", error); request.setAttribute("screen",
	 * screen); requestDispatcher.forward(request, response); } } else
	 * if(Button.equalsIgnoreCase("Edit")){ ArrayList
	 * a_list=RegistrationDAO.AdminMasterview
	 * ("",request.getParameter("ivalue"),"one"); AdminRegistrationBO
	 * AdminBO=(AdminRegistrationBO) a_list.get(0);
	 * request.setAttribute("pagefor", "Update");
	 * request.setAttribute("adminBO", AdminBO); request.setAttribute("screen",
	 * screen);
	 * 
	 * requestDispatcher.forward(request, response); }else
	 * if(Button.equalsIgnoreCase("Update admin")){
	 * request.setAttribute("pagefor", "update Admin"); AdminRegistrationBO
	 * adminRegistrationBO = getAdminBean(request);
	 * adminRegistrationBO.setAssociateCode
	 * (((AdminRegistrationBO)request.getSession
	 * ().getAttribute("user")).getAssociateCode()); String error =
	 * checkAdminUser(adminRegistrationBO); if(error.length() == 0) {
	 * adminRegistrationBO.setUid(request.getParameter("Uid")); int result =
	 * RegistrationDAO.saveAdminRegistration(adminRegistrationBO,"update");
	 * if(result > 0) { request.setAttribute("page", " Update User");
	 * response.sendRedirect
	 * ("control?action=security&event=insertRolesForUser&userid="
	 * +adminRegistrationBO
	 * .getUid()+"&uname="+adminRegistrationBO.getUname()+"&utype="
	 * +adminRegistrationBO
	 * .getUsertype()+"&userDesc="+adminRegistrationBO.getUsertypeDesc
	 * ()+"&status=1"); } else { request.setAttribute("page", "to update User");
	 * screen = TDSConstants.getFailureJSP; request.setAttribute("screen",
	 * screen);
	 * 
	 * requestDispatcher.forward(request, response); }
	 * 
	 * } else { request.setAttribute("adminBO", adminRegistrationBO);
	 * request.setAttribute("errors", error); request.setAttribute("screen",
	 * screen);
	 * 
	 * requestDispatcher.forward(request, response); }
	 * 
	 * }
	 * 
	 * 
	 * }
	 */

	/*
	 * HttpSession m_session = request.getSession(); AdminRegistrationBO
	 * m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
	 * ArrayList al_list = null,al_view=null; StringBuffer error = null;
	 * PenalityBean penalityBean = null; String pagefor; String Button =
	 * request.getParameter("Button");
	 * 
	 * 
	 * 
	 * if(Button == null) { al_list =
	 * RegistrationDAO.getDriverPenality(m_adminBO.getAssociateCode());
	 * request.setAttribute("screen", "/Company/DriverPenality.jsp"); } else
	 * if(Button.equalsIgnoreCase("Submit")) { al_view =
	 * assignDriverPenalityBean(request, response,m_adminBO.getAssociateCode());
	 * error = validatePenalityChargeBean(request,
	 * response,m_adminBO.getAssociateCode());
	 * 
	 * 
	 * if(!(error.length()>3)) { RegistrationDAO.insertDriverPenality(al_view,
	 * "insert", m_adminBO.getAssociateCode()); al_view = null;
	 * request.setAttribute("sucess", "Successfully Inserted");
	 * request.setAttribute("screen", "/Company/DriverPenality.jsp"); }else {
	 * 
	 * request.setAttribute("screen", "/Company/DriverPenality.jsp"); } pagefor
	 * = "Submit"; } else {
	 * 
	 * String check=request.getParameter("checkedval1"); if(check !=null) {
	 * boolean s=RegistrationDAO.delDriverPenality(check); if(check !="")
	 * request.setAttribute("remove", "Successfully Deleted "); }
	 * 
	 * 
	 * request.setAttribute("screen", "/Company/DriverPenality.jsp"); }
	 * //request.setAttribute("pagefor", pagefor); al_list =
	 * RegistrationDAO.getDriverPenality(m_adminBO.getAssociateCode());
	 * request.setAttribute("error", error); request.setAttribute("al_list",
	 * al_list); request.setAttribute("al_view", al_view);
	 * request.setAttribute("al_driver",
	 * UtilityDAO.getDriverList(m_adminBO.getAssociateCode(),1));
	 * 
	 * RequestDispatcher requestDispatcher =
	 * getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
	 * requestDispatcher.forward(request, response);
	 */

	public String queueValidation(HttpServletRequest request, String QueueName, String Desc, String Order, String Date, String SmsTime, String LoingSwitch, String StTime, String EndTime, String DayWeek, String DisPipe, String BrdReqbefr, String size) {

		// QC.BROADCAST_DELAY

		StringBuffer buffer = new StringBuffer();
		if ((QueueName.equals("")) || (QueueName == null)) {
			buffer.append("Enter QueueName<br>");
		} else if (RegistrationDAO.queueNameCheck(QueueName)) {
			buffer.append("QueueName already exists<br>");
		}
		if ((Desc.equals("")) || (Desc == null)) {
			buffer.append("Enter Description<br>");
		}
		if ((Order.equals("")) || (Order == null)) {
			buffer.append("Enter Order<br>");
		} else if (!TDSValidation.isOnlyNumbers(Order)) {
			buffer.append("Order should be numeric<br>");
		}

		if ((Date.equals("")) || (Date == null)) {
			buffer.append("Enter Date & Time<br>");
		} else if (!TDSValidation.isOnlyNumbers(Date)) {
			buffer.append("Date&Time is not valid<br>");
		}

		if ((StTime.equals("")) || (StTime == null)) {
			buffer.append("Enter Start Time<br>");
		} else if (!TDSValidation.isOnlyNumbers(StTime)) {
			buffer.append("Start Time is not valid<br>");
		}

		if ((EndTime.equals("")) || (EndTime == null)) {
			buffer.append("Enter End Time<br>");
		} else if (!TDSValidation.isOnlyNumbers(EndTime)) {
			buffer.append("End Time is not valid<br>");
		}

		if ((BrdReqbefr.equals("")) || (BrdReqbefr == null)) {
			buffer.append("Enter Before Broadcast Request time<br>");
		} else if (!TDSValidation.isOnlyNumbers(BrdReqbefr)) {
			buffer.append("BroadCast Request should be a number<br>");
		}

		for (int i = 0; i < size.length(); i++) {

			if ((request.getParameter("longitude" + i).equals("")) || (request.getParameter("longitude" + i) == null)) {
				buffer.append("Enter Longitute<br>");
			}
			if ((request.getParameter("Latitude" + i).equals("")) || (request.getParameter("Latitude" + i) == null)) {
				buffer.append("Enter Latitude<br>");
			}
		}
		return buffer.toString();

	}

	public void queuemappingdetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		ArrayList al_list = new ArrayList();
		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		String forwardURL = "";
		String queuename = "";
		String desc = "";
		String order = "";
		String date = "";
		String smsTime = "";
		String loingSwitch = "";

		String sttime = "";
		String endtime = "";
		String dayweek = "";
		String dispipe = "";
		String brdreqbefr = "";

		String error = "";

		queuename = request.getParameter("queuename");
		desc = request.getParameter("description");
		order = request.getParameter("order");
		date = request.getParameter("datetime");
		smsTime = request.getParameter("smstime");
		loingSwitch = request.getParameter("loginSwitch");

		sttime = request.getParameter("stTime");
		endtime = request.getParameter("edTime");
		dayweek = request.getParameter("dayweek");
		dispipe = request.getParameter("dppipe");
		brdreqbefr = request.getParameter("brdcstrqbre");

		String size = request.getParameter("size") == null ? "0" : request.getParameter("size");

		int result = 0;
		String Button = request.getParameter("button") == null ? "" : request.getParameter("button");
		System.out.println("button:::" + Button);
		if (Button.equalsIgnoreCase("Submit")) {

			System.out.println("SUBMIT FUNCTION");
			// ZoneListBean zonebean = new ZoneListBean();
			// zonebean = setZoneDetails(zonebean,request);

			error = queueValidation(request, queuename, desc, order, date, smsTime, loingSwitch, sttime, endtime, dayweek, dispipe, brdreqbefr, size);
			if (error.length() > 0) {

				// System.out.println("QUEUE NAME::"+queuename);
				request.setAttribute("queueName", queuename);
				request.setAttribute("description", desc);
				request.setAttribute("order", order);
				request.setAttribute("date", date);
				request.setAttribute("smsTime", smsTime);
				request.setAttribute("Sttime", sttime);
				request.setAttribute("endtime", endtime);
				request.setAttribute("dayweek", dayweek);
				request.setAttribute("brdreqbefr", brdreqbefr);

				al_list = null;
				request.setAttribute("al_list", al_list);
				request.setAttribute("buttonflag", "submit");
				forwardURL = TDSConstants.getMainNewJSP;
				request.setAttribute("error", error.toString());
				request.setAttribute("screen", "/Company/queueMapping.jsp");
				RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				reqDispatcher.forward(request, response);
			} else {
				// m_pst = m_conn.prepareStatement("")

				for (int i = 0; i <= Integer.parseInt(size); i++) {
					al_list.add(request.getParameter("longitude" + i));
					al_list.add(request.getParameter("Latitude" + i));
				}
				RegistrationDAO.insertQueueMapping(queuename, desc, m_adminBO.getAssociateCode(), al_list, size, order, date, smsTime, loingSwitch, sttime, endtime, dayweek, dispipe, brdreqbefr);
				al_list = null;
				request.setAttribute("al_list", al_list);
				request.setAttribute("buttonflag", "submit");
				request.setAttribute("error", "Previous Record Inserted Sucessfully");
				forwardURL = TDSConstants.getMainNewJSP;
				request.setAttribute("screen", "/Company/queueMapping.jsp");
				RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				reqDispatcher.forward(request, response);
			}
		}

		if (Button.equalsIgnoreCase("search")) {
			ArrayList arr_list = new ArrayList();
			System.out.println("IN QUEUE EDIT METHOD");

			String name = request.getParameter("Name");
			System.out.println("queue name: in edit>>>:" + name);
			cat.info("In add Queue Mapping  function ");

			arr_list = RegistrationDAO.getqueueDetails(name);
			request.setAttribute("ar_list", arr_list);
			forwardURL = TDSConstants.getMainNewJSP;
			request.setAttribute("screen", "/Company/queueSummary.jsp");
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			reqDispatcher.forward(request, response);

		}

		if (request.getParameter("subevent").equals("home")) {

			cat.info("In add Queue Mapping  function ");
			request.setAttribute("buttonflag", "submit");
			// al_list =
			// RegistrationDAO.loginList(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(),driver_id);
			al_list = null;
			request.setAttribute("al_list", al_list);
			forwardURL = TDSConstants.getMainNewJSP;
			request.setAttribute("screen", "/Company/queueMapping.jsp");
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			reqDispatcher.forward(request, response);
		}

		if (request.getParameter("subevent").equals("summary")) {

			// System.out.println("queue summary");
			cat.info("In add Queue Summary  function ");

			al_list = null;
			request.setAttribute("ar_list", al_list);
			forwardURL = TDSConstants.getMainNewJSP;
			request.setAttribute("screen", "/Company/queueSummary.jsp");
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			reqDispatcher.forward(request, response);
		}

		forwardURL = TDSConstants.getMainNewJSP;
		request.setAttribute("screen", "/Company/queueMapping.jsp");
		RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		reqDispatcher.forward(request, response);

	}

	public void driverZoneActivity(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException
	{
		String forwardURL = "";
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String zoneId = request.getParameter("zoneIdDZA") != null ? request.getParameter("zoneIdDZA") : "";
		String driverId = request.getParameter("driverIdDZA") != null ? request.getParameter("driverIdDZA") : "";
		String zoneToload = zoneId;
		String search = request.getParameter("nDZA") != null ? request.getParameter("nDZA") : "";
		
		if (search != null && !search.equals("") && !search.equals("4") && !search.equals("7") && !search.equals("5")) 
		{
			if(search.equals("2"))
			{
				zoneToload = "";
			}
			System.out.println("zoneid:"+zoneId+"----driver:"+driverId+"-----zonetoload:"+zoneToload);
			String fDate = request.getParameter("dzaFromDate") != null ? request.getParameter("dzaFromDate") : "";
			String tDate = request.getParameter("dzaToDate") != null ? request.getParameter("dzaToDate") : "";
			String fromDateFormatted = "";
			String toDateFormatted = "";
				if (!fDate.equals("") && !tDate.equals("")) 
					{
						fromDateFormatted = TDSValidation.getTimeStampDateFormat(fDate);
						toDateFormatted = TDSValidation.getTimeStampDateFormat(tDate);
					}
				ArrayList<QueueBean> dzaList = new ArrayList<QueueBean>();
		
				dzaList = ZoneDAO.getDriverZoneActivity(adminBO, zoneToload, driverId, fromDateFormatted, toDateFormatted);
				if(search.equals("6"))
				{
					if (dzaList!=null && dzaList.size() > 0) 
					 {
						response.setContentType("text/csv");
						response.setHeader("Content-Disposition", "attachment; filename=Driver_Activity("+driverId+")_From_"+fDate+"_TO_"+tDate+".csv");
						response.getWriter().println("ZoneId,DriverId,VehicleNo,ZoneLoginTime,ZoneLogoutTime,Reason");
							for(int i=0;i<dzaList.size();i++)
							{
								QueueBean dza = (QueueBean) dzaList.get(i);
								response.getWriter().print(dzaList.get(i).getQU_NAME()+",");
								response.getWriter().print(dzaList.get(i).getDriverId()+",");
								response.getWriter().print(dzaList.get(i).getVehicleNo()+",");
								response.getWriter().print(dzaList.get(i).getDq_LoginTime()+",");
								if (dza.getStatus().equals("2"))
								{
								response.getWriter().print(dzaList.get(i).getDq_LogoutTime()+",");
								}
								else
								{
								response.getWriter().print("Present,");
								}
								response.getWriter().print(dzaList.get(i).getDq_Reason()+",");
							}
						StringBuffer outData = new StringBuffer();
						outData.append("ZoneId,DriverId,VehicleNo,ZoneLoginTime,ZoneLogoutTime,Reason\n");
						for (int i = 0; i < dzaList.size(); i++) 
						{
	                        outData.append(dzaList.get(i).getQU_NAME()+ "," +dzaList.get(i).getDriverId()+ "," +dzaList.get(i).getVehicleNo()+ "\n" +dzaList.get(i).getDq_LoginTime()+ "," +dzaList.get(i).getLO_LogoutTime()+ "," +dzaList.get(i).getDq_Reason()+"\n");                        
	                    }
					}
				}
				request.setAttribute("fDate", fDate);
				request.setAttribute("tDate", tDate);
				request.setAttribute("zoneId", zoneId);
				request.setAttribute("driverId", driverId);
				request.setAttribute("al_list", dzaList);
				request.setAttribute("ndza", search);
				forwardURL = TDSConstants.getMainNewJSP;
				request.setAttribute("screen", "/jsp/driverZoneActivity.jsp");
				RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				reqDispatcher.forward(request, response);
				
		}
		else if((search != null) && (!search.equals("")) && ((search.equals("4"))))
		{
			System.out.println("hello2");
			zoneToload = "";
			System.out.println("zoneid:"+zoneId+"----driver:"+driverId+"-----zonetoload:"+zoneToload);
			String fDate = request.getParameter("dzaFromDate") != null ? request.getParameter("dzaFromDate") : "";
			String tDate = request.getParameter("dzaToDate") != null ? request.getParameter("dzaToDate") : "";
			String startime = "";
			String endtime = "";
			if (!fDate.equals("") && !tDate.equals("")) {
				startime = TDSValidation.getTimeStampDateFormat(fDate);
				endtime = TDSValidation.getTimeStampDateFormat(tDate);
			}
			
			ArrayList<QueueBean> dzaList = new ArrayList<QueueBean>();
			dzaList = ZoneDAO.getDriverActivity(adminBO, driverId, startime, endtime);
		
			request.setAttribute("fDate", fDate);
			request.setAttribute("tDate", tDate);
			request.setAttribute("driverId", driverId);
			request.setAttribute("al_list", dzaList);
			request.setAttribute("ndza", search);
			forwardURL = TDSConstants.getMainNewJSP;
			request.setAttribute("screen", "/jsp/driverZoneActivity.jsp");
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			reqDispatcher.forward(request, response);
		}
		
		else if((search != null) && (!search.equals("")) && ((search.equals("5"))))
		{
			System.out.println("hello2");
			zoneToload = "";
			System.out.println("zoneid:"+zoneId+"----driver:"+driverId+"-----zonetoload:"+zoneToload);
			String fDate = request.getParameter("dzaFromDate") != null ? request.getParameter("dzaFromDate") : "";
			String tDate = request.getParameter("dzaToDate") != null ? request.getParameter("dzaToDate") : "";
			String startime = "";
			String endtime = "";
			if (!fDate.equals("") && !tDate.equals("")) {
				startime = TDSValidation.getTimeStampDateFormat(fDate);
				endtime = TDSValidation.getTimeStampDateFormat(tDate);
			}
			
			ArrayList<QueueBean> dzaList = new ArrayList<QueueBean>();
			dzaList = ZoneDAO.getDriverActivity(adminBO, driverId, startime, endtime);
			
			if (dzaList!=null && dzaList.size() > 0) 
			{
				response.setContentType("text/csv");
				response.setHeader("Content-Disposition", "attachment; filename=Driver_Activity("+driverId+")_From_"+fDate+"_TO_"+tDate+".csv");
				response.getWriter().println("STARTTIME ENDTIME  REASON/CABNO/DRIVERID");
				
					for(int i=0;i<dzaList.size();i++)
					{
						QueueBean dza = (QueueBean) dzaList.get(i);
						if (dza.getQuerystatus().equalsIgnoreCase("1"))
						{
						response.getWriter().print(dzaList.get(i).getORH_JOB_ACCEPT_TIME()+ ",");
						response.getWriter().print(dzaList.get(i).getORH_JOB_COMPLETE_TIME()+ ",");
						response.getWriter().print(dzaList.get(i).getORH_REASON()+ "\n");
						}
						else if (dza.getQuerystatus().equalsIgnoreCase("2"))
						{
						response.getWriter().print(dzaList.get(i).getLO_Logintime()+ ",");
						response.getWriter().print(dzaList.get(i).getLO_LogoutTime()+ ",");
						response.getWriter().print(dzaList.get(i).getLO_REASON()+ "\n");
						}
						else if (dza.getQuerystatus().equalsIgnoreCase("3"))
						{
						response.getWriter().println(dzaList.get(i).getDq_LoginTime()+ ",");
						response.getWriter().println(dzaList.get(i).getDq_LogoutTime()+ ",");
						response.getWriter().println(dzaList.get(i).getDq_Reason()+ "\n");
						}
					}
				
				StringBuffer outData = new StringBuffer();
				outData.append("ORH_JOB_ACCEPT_TIME,ORH_JOB_COMPLETE_TIME,ORH_REASON\nLO_Logintime,LO_LogoutTime,LO_REASON\nDq_LoginTime,Dq_LogoutTime,Dq_Reason\n");
				for (int i = 0; i < dzaList.size(); i++) {
                    outData.append(dzaList.get(i).getORH_JOB_ACCEPT_TIME()+ "," +dzaList.get(i).getORH_JOB_COMPLETE_TIME()+ "," +dzaList.get(i).getORH_REASON()+ "\n" +dzaList.get(i).getLO_Logintime()+ "," +dzaList.get(i).getLO_LogoutTime()+ "," +dzaList.get(i).getLO_REASON()+ "\n" +dzaList.get(i).getDq_LoginTime()+ "," +dzaList.get(i).getDq_LogoutTime()+ "," +dzaList.get(i).getDq_Reason()+ "\n");                        }
			}
		
			
			
			request.setAttribute("fDate", fDate);
			request.setAttribute("tDate", tDate);
			request.setAttribute("driverId", driverId);
			request.setAttribute("al_list", dzaList);
			request.setAttribute("ndza", search);
			forwardURL = TDSConstants.getMainNewJSP;
			request.setAttribute("screen", "/jsp/driverZoneActivity.jsp");
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			reqDispatcher.forward(request, response);
		}
		
		
		
		else if ((search != null) && (!search.equals("")) && ((search.equals("7"))))
		{
			System.out.println("hello7");
			zoneToload = "";
			System.out.println("zoneid:"+zoneId+"----driver:"+driverId+"-----zonetoload:"+zoneToload);
			String fDate = request.getParameter("dzaFromDate") != null ? request.getParameter("dzaFromDate") : "";
			String tDate = request.getParameter("dzaToDate") != null ? request.getParameter("dzaToDate") : "";
			String startime = "";
			String endtime = "";
			if (!fDate.equals("") && !tDate.equals("")) {
				startime = TDSValidation.getTimeStampDateFormat(fDate);
				endtime = TDSValidation.getTimeStampDateFormat(tDate);
			}
			
			ArrayList<QueueBean> dzaList = new ArrayList<QueueBean>();
			dzaList = ZoneDAO.getDriverWorkingHours(adminBO, driverId, startime, endtime);
			
			request.setAttribute("fDate", fDate);
			request.setAttribute("tDate", tDate);
			request.setAttribute("driverId", driverId);
			request.setAttribute("al_list", dzaList);
			request.setAttribute("ndza", search);
			forwardURL = TDSConstants.getMainNewJSP;
			request.setAttribute("screen", "/jsp/driverZoneActivity.jsp");
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			reqDispatcher.forward(request, response);
		
	}
}
		
	public ZoneListBean setZoneDetails(ZoneListBean zonebean, HttpServletRequest request) {
		ArrayList al_list = new ArrayList();
		zonebean.setZoneName(request.getParameter("queuename"));
		zonebean.setZoneOrder(Integer.parseInt(request.getParameter("order")));
		al_list = setOtherZone(zonebean, request);
		zonebean.setOtherZonesSP(al_list);
		return zonebean;

	}

	public ArrayList setOtherZone(ZoneListBean zonebean, HttpServletRequest request) {
		ArrayList ar_list = new ArrayList();

		String size = request.getParameter("size") == null ? "0" : request.getParameter("size");
		// System.out.println("size:"+size);

		for (int i = 0; i <= Integer.parseInt(size); i++) {
			ZoneTableBeanSP zonesp = new ZoneTableBeanSP();
			zonesp.setZoneDesc(request.getParameter("longitude" + i));
			zonesp.setOrderOfZone(request.getParameter("Latitude" + i));
			ar_list.add(zonesp);

		}

		for (int i = 0; i < ar_list.size(); i++) {
			ZoneTableBeanSP zonesp = (ZoneTableBeanSP) ar_list.get(i);
			// System.out.println("details::"+zonesp.getZoneDesc());
			// System.out.println("detailsasdfaf::"+zonesp.getOrderOfZone());

		}

		// System.out.println("zone child size:::"+ar_list.size());

		return ar_list;

	}

	public void driverMappingDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {

		ArrayList al_list = new ArrayList();
		ArrayList switch_list = new ArrayList();
		HttpSession session = request.getSession();
		String qc_name[] = null;
		String qc_flag[] = null;
		String map[] = null;
		String forwardURL = "";
		String errors = "";
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");

		String Button = request.getParameter("button") == null ? "" : request.getParameter("button");
		String driver_id = request.getParameter("driverid") == null ? "" : request.getParameter("driverid");

		if (Button.equalsIgnoreCase("Search")) {
			switch_list = RegistrationDAO.loginList(adminBO.getMasterAssociateCode(), driver_id, adminBO.getAssociateCode());
			if (switch_list == null) {
				request.setAttribute("buttonflag", "submit");
				request.setAttribute("errors", "Driver does not belong to this company");
				// request.setAttribute("screen", "/jsp/driverMapping.jsp");
				// getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request,
				// response);
			}
			request.setAttribute("switchlist", switch_list);
			request.setAttribute("buttonflag", "Update");
			request.setAttribute("driver", driver_id);
		}
		if (Button.equalsIgnoreCase("Update")) {
			ArrayList ar_list = new ArrayList();
			String queueName = "";
			qc_flag = request.getParameterValues("queue_name");
			qc_name = request.getParameterValues("desc");
			for (int i = 0; i < qc_flag.length; i++) {
				if (qc_flag[i].equalsIgnoreCase("true")) {
					ar_list.add(qc_name[i]);
					ar_list.add("R");
				} else if (qc_flag[i].equalsIgnoreCase("noAccess")) {
					ar_list.add(qc_name[i]);
					ar_list.add("NA");
				}
			}
			// int result =
			// RegistrationDAO.detailsexcistOrnot(adminBO.getAssociateCode(),driver_id);
			/* if(result == 1) { */
			int updateDetails = RegistrationDAO.updateDriverMapping(adminBO.getMasterAssociateCode(), driver_id, ar_list);
			/*
			 * } else { int insertDetails =
			 * RegistrationDAO.insertDriverMapping(adminBO
			 * .getAssociateCode(),driver_id,ar_list); }
			 */
			switch_list = RegistrationDAO.loginList(adminBO.getMasterAssociateCode(), driver_id, adminBO.getAssociateCode());
			request.setAttribute("switchlist", switch_list);
			request.setAttribute("buttonflag", "Update");
			request.setAttribute("driver", driver_id);
		}
		if (Button.equalsIgnoreCase("Next")) {
			if (Button.equalsIgnoreCase("Next")) {
				ArrayList<String> ar_list = new ArrayList<String>();
				qc_flag = request.getParameterValues("queue_name");
				qc_name = request.getParameterValues("desc");
				for (int i = 0; i < qc_flag.length; i++) {
					if (qc_flag[i].equalsIgnoreCase("true")) {
						ar_list.add(qc_name[i]);
					}
				}
				int updateDetails = RegistrationDAO.updateDriverMapping(adminBO.getMasterAssociateCode(), driver_id, ar_list);

				response.sendRedirect("control?action=registration&event=CabDriverMapping&module=operationView");
			}
		}
		if (request.getParameter("subevent") != null && request.getParameter("subevent").equals("home")) {

			cat.info("In add Driver Mapping  function ");
			request.setAttribute("buttonflag", "submit");
			request.setAttribute("driver", driver_id);
		}
		forwardURL = TDSConstants.getMainNewJSP;
		request.setAttribute("screen", "/jsp/driverMapping.jsp");
		RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		reqDispatcher.forward(request, response);
	}

	public void saveDriverregistration(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		String screen = "";
		String forwardURL = "";
		HttpSession session = request.getSession();
		request.setAttribute("drFlag", RegistrationDAO.getCmpyVehicleFlag(((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode(), "1"));
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) session.getAttribute("user");
		request.setAttribute("al_q", CheckZone.returnZonesForAssociatoinCode(getServletContext(), m_adminBO.getAssociateCode()));
		request.setAttribute("template", ChargesDAO.getChargeTemplate(m_adminBO.getAssociateCode(), 2));

		if (request.getParameter("subevent") == null) {
			DriverRegistrationBO driverBO = new DriverRegistrationBO();
			driverBO = setDriverData(driverBO, request);
			OpenRequestBO openBo = new OpenRequestBO();
			openBo.setQueueno(request.getParameter("queueno"));
			String error = RegistrationValidation.driverValidation(driverBO);
			screen = "";
			forwardURL = TDSConstants.getMainNewJSP;
			boolean selectResult = UtilityDAO.isExistOrNot("select AU_SNO from TDS_ADMINUSER where AU_SNO='" + driverBO.getUid() + "' OR AU_USERNAME='" + driverBO.getUid() + "'");
			if (error.length() > 0) {
				screen = TDSConstants.registrationJSP;
				request.setAttribute("errors", error);
			}
			if (selectResult) {
				screen = TDSConstants.registrationJSP;
				request.setAttribute("errors", "User Name Already Exits");
			} else if (!selectResult && error.length() <= 0) {
				String wasl_ccode = TDSProperties.getValue("WASL_Company");
				if((driverBO.getRegistration() == 'D' && m_adminBO.getAssociateCode().equals(wasl_ccode)) && (driverBO.getDr_dob().equals("") || driverBO.getDrLicense().equals(""))){
					screen = TDSConstants.registrationJSP;
					request.setAttribute("errors", "Driver License and Driver's Date of Birth should not be Empty");
				} else {
					if (session.getAttribute("user") != null) {
						AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
						driverBO.setAssociateCode(adminBO.getAssociateCode());
					}
					int result = RegistrationDAO.insertDriverManager(driverBO, m_adminBO);
					RegistrationDAO.setDefaultQueue(m_adminBO.getAssociateCode(), openBo.getQueueno(), driverBO.getUid());
					// This code checks if its a part of wizard.
					// If Wizard and successful addition it sends to next screen
					// based on wether the useradded
					// was operator or driver. If driver goes to zone mapping. If
					// not goes to Operator access screen.
					boolean iserror = false;
					String error_message = "";
					if (result == 1) {
						if(driverBO.getRegistration() == 'D' && m_adminBO.getAssociateCode().equals(wasl_ccode)) {
							//JSONArray array = new JSONArray();
							try {
								JSONObject obj = new JSONObject();
								obj.put("apiKey", "D42D418A-4FEE-42C9-9005-18C06F497DC6");
								obj.put("captainIdentityNumber", driverBO.getDrLicense());
								System.out.println("DOB : "+driverBO.getDr_dob());
								obj.put("dateOfBirth", driverBO.getDr_dob());
								obj.put("emailAddress", driverBO.getEmailId());
								obj.put("mobileNumber", driverBO.getPhone());
								//array.put(obj);
								WaslIntegrationBO waslBO = DriverRegistration.RegisterDriver(obj.toString());
								if(waslBO.isValid()){
									RegistrationDAO.updateWaslReferenceForDriver(waslBO.getReferenceNumber(), obj.toString(), m_adminBO.getAssociateCode(), driverBO.getUid());
									System.out.println("Wasl successfully updated ->"+waslBO.getReferenceNumber());
								}else{
									//Remove driver
									RegistrationDAO.deleteDriver(m_adminBO, driverBO.getUid(), 0);
									iserror = true;
									error_message = waslBO.getError_message();
									System.out.println(error_message);
								}
								
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else{
							System.out.println("No WASL integration");
						}
						if(!iserror){
							if (request.getParameter("NextScreen") != null) {
								String userid = "";
								String userdesc = "";
								String status = "";
								userid = request.getParameter("uid") == null ? "" : request.getParameter("uid");
								userdesc = request.getParameter("registration") == null ? "" : request.getParameter("registration");
								status = request.getParameter("status") == null ? "" : request.getParameter("status");
								if (userdesc.equalsIgnoreCase("O")) {
									response.sendRedirect("control?action=userrolerequest&module=systemsetupView&event=getUserList&wizard=yes&userid=" + userid + "&uname=" + userid + "&utype=" + userdesc + "&userDesc=" + userdesc + "&status=" + status);
								} else {
									response.sendRedirect("control?action=registration&event=driverMapping&module=operationView&wizard=yes&driverid=" + userid + "&button=Search");
								}
							} else {
								request.setAttribute("page", " created new User");
								DriverRegistrationBO driverBo = new DriverRegistrationBO();
								request.setAttribute("driverDetails", driverBo);
								screen = TDSConstants.registrationJSP;
							}
						}else{
							screen = TDSConstants.registrationJSP;
							request.setAttribute("errors", error_message);
						}
					} else {
						request.setAttribute("errors", "User Name Not Yet Registered");
						screen = TDSConstants.registrationJSP;
						RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
						reqDispatcher.forward(request, response);
					}
				}
			}
			if (request.getParameter("NextScreen") == null) {
				request.setAttribute("driverBO", driverBO);
				request.setAttribute("openBO", openBo);
				request.setAttribute("screen", screen);
				RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				reqDispatcher.forward(request, response);
			} else if (request.getParameter("NextScreen") != null && (selectResult || error.length() > 0)) {
				request.setAttribute("driverBO", driverBO);
				request.setAttribute("openBO", openBo);
				request.setAttribute("screen", screen);
				RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				reqDispatcher.forward(request, response);
			}
		} else {
			cat.info("In add Driver Registration function ");
			forwardURL = TDSConstants.getMainNewJSP;
			request.setAttribute("drkey", RegistrationDAO.Tdsdriverkey());
			// String forwardURL = TDSConstants.registrationJSP;
			cat.info("Forward the request into the " + forwardURL + " JPS");
			screen = TDSConstants.registrationJSP;
			request.setAttribute("screen", screen);
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			reqDispatcher.forward(request, response);
		}
	}

	public void saveAdminRegistraion(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		cat.info("In save admin Registration Page method ");
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.creatAdminRegistration;
		String Button = request.getParameter("submit");
		HttpSession m_session = request.getSession();
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		if (Button == null) {
			request.setAttribute("pagefor", "createAdmin");
			request.setAttribute("screen", screen);
			requestDispatcher.forward(request, response);
		} else if (Button.equalsIgnoreCase("Create admin")) {
			request.setAttribute("pagefor", "createAdmin");
			AdminRegistrationBO adminRegistrationBO = getAdminBean(request);
			adminRegistrationBO.setAssociateCode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
			String error = checkAdminUser(adminRegistrationBO);
			if (error.length() == 0) {
				if (ConfigDAO.checkUsedIdExist(adminRegistrationBO.getUname(), " ") == 0) {
					int result = RegistrationDAO.saveAdminRegistration(adminRegistrationBO, "insert", 0);
					if (result > 0) {
						request.setAttribute("page", " created new User");
						response.sendRedirect("control?action=security&event=insertRolesForUser&userid=" + adminRegistrationBO.getUid() + "&uname=" + adminRegistrationBO.getUname() + "&utype=" + adminRegistrationBO.getUsertype() + "&userDesc=" + adminRegistrationBO.getUsertypeDesc() + "&status=1");
					} else {
						request.setAttribute("page", "Failure To create new User");
						screen = TDSConstants.getFailureJSP;
						request.setAttribute("screen", screen);
						requestDispatcher.forward(request, response);

					}
				} else {

					request.setAttribute("errors", "Username already Exist ");
					request.setAttribute("adminBO", adminRegistrationBO);
					request.setAttribute("screen", screen);
					requestDispatcher.forward(request, response);
				}

			} else {
				request.setAttribute("pagefor", "createAdmin");
				request.setAttribute("adminBO", adminRegistrationBO);
				request.setAttribute("errors", error);
				request.setAttribute("screen", screen);
				requestDispatcher.forward(request, response);
			}
		} else if (Button.equalsIgnoreCase("Edit")) {
			ArrayList a_list = RegistrationDAO.AdminMasterview("", request.getParameter("ivalue"), "one", new AdminRegistrationBO());
			AdminRegistrationBO AdminBO = (AdminRegistrationBO) a_list.get(0);
			request.setAttribute("pagefor", "Update");
			request.setAttribute("adminBO", AdminBO);
			request.setAttribute("screen", screen);

			requestDispatcher.forward(request, response);
		} else if (Button.equalsIgnoreCase("UpdateAdmin")) {
			request.setAttribute("pagefor", "update Admin");
			AdminRegistrationBO adminRegistrationBO = getAdminBean(request);
			adminRegistrationBO.setAssociateCode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
			String error = checkAdminUser(adminRegistrationBO);
			if (error.length() == 0) {
				adminRegistrationBO.setUid(request.getParameter("Uid"));
				int result = RegistrationDAO.saveAdminRegistration(adminRegistrationBO, "update", 1);
				if (result > 0) {
					if (adminRegistrationBO.getActive().equalsIgnoreCase("1") || adminRegistrationBO.getActive().equalsIgnoreCase("0")) {
						response.sendRedirect("control?action=registration&event=Adminview&module=systemsetupView");
					}
				} else {
					request.setAttribute("page", "Failure to update User");
					screen = TDSConstants.getFailureJSP;
					request.setAttribute("screen", screen);

					requestDispatcher.forward(request, response);
				}

			} else {
				request.setAttribute("adminBO", adminRegistrationBO);
				request.setAttribute("errors", error);
				request.setAttribute("screen", screen);

				requestDispatcher.forward(request, response);
			}
		}
		if (Button.equalsIgnoreCase("UpdatePassword")) {
			AdminRegistrationBO adminRegistrationBO = getAdminBean(request);
			adminRegistrationBO.setAssociateCode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
			adminRegistrationBO.setUid(request.getParameter("Uid"));
			int result = RegistrationDAO.saveAdminRegistration(adminRegistrationBO, "update", 2);
			if (result > 0) {
				response.sendRedirect("control?action=registration&event=Adminview&module=systemsetupView");
			}
		}

	}

	public void updateDriverDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In updateDriverDetails function ");
		String driverid = "";
		String Latitude = "";
		String longitude = "";
		String availability = "";
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		if (request.getParameter("driverid") != null) {
			driverid = request.getParameter("driverid");
		}
		if (request.getParameter("Latitude") != null) {
			Latitude = request.getParameter("Latitude");
		}
		if (request.getParameter("longitude") != null) {
			longitude = request.getParameter("longitude");
		}
		if (request.getParameter("availability") != null) {
			availability = request.getParameter("availability");
		}

		cat.info("Driver id " + driverid);
		cat.info("Latitude " + Latitude);
		cat.info("Logitude " + longitude);
		cat.info("Availability Status " + availability);
		HashMap hmp_driver = new HashMap();
		hmp_driver.put("driverid", driverid);
		hmp_driver.put("Latitude", Latitude);
		hmp_driver.put("logitude", longitude);
		hmp_driver.put("status", availability);
		int result = 0;
		if (driverid != "") {
			result = RegistrationDAO.updateDriverDetails(hmp_driver);
		} else {
			request.setAttribute("page", "You must enter driver id");
			screen = TDSConstants.getFailureJSP;
		}
		if (result > 0) {
			cat.info("Successfully Updated Driver details");
			request.setAttribute("page", " Updated driver details");
			screen = TDSConstants.getSuccessJSP;
		} else {
			cat.info("Failure to updated Driver details");
			request.setAttribute("page", " Error updating driver details");
			screen = TDSConstants.getFailureJSP;
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void createVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("in Create Voucher Method ");
		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		String screen = TDSConstants.createVoucherJSP;
		String forwardURL = TDSConstants.getMainNewJSP;
		// VoucherBO
		// Vno=RegistrationDAO.Vouchersequence("COV",m_adminBO.getMasterAssociateCode());
		ArrayList<ChargesBO> chargeType = ChargesDAO.getChargeTypes(m_adminBO.getMasterAssociateCode(), 0);
		request.setAttribute("chargeTypes", chargeType);
		// request.setAttribute("voucherentry", Vno);
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getVoucherEditPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In getVoucherEditPage Voucher Method ");
		String screen = TDSConstants.getVoucherEditJSP;
		String forwardURL = TDSConstants.getMainNewJSP;
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void saveVoucherEntry(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Save Voucher Entry Method");
		HttpSession session = request.getSession();
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.createVoucherJSP;

		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");

		VoucherBO voucherBO = new VoucherBO();
		voucherBO = setVoucherBOData(voucherBO, request);
		voucherBO.setVassoccode(adminBO.getMasterAssociateCode());
		voucherBO.setVuser(adminBO.getUid());
		String baseVoucher = request.getParameter("voucherTrue");

		String error = checkVoucherData(voucherBO);

		if (request.getParameter("submit") != null) {
			if (error.length() == 0) {
				int result = RegistrationDAO.saveVoucherEntry(voucherBO);
				if (result > 0) {
					// screen = TDSConstants.getSuccessJSP;
					if (request.getParameter("chargeSize") != null && !request.getParameter("chargeSize").equals("")) {
						int numberOfRows = Integer.parseInt(request.getParameter("chargeSize"));
						String voucher = request.getParameter("vno");
						String[] amount = new String[numberOfRows + 1];
						String[] key = new String[numberOfRows + 1];
						for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
							amount[rowIterator] = (request.getParameter("chargeAmount" + rowIterator).equals("") ? "0" : request.getParameter("chargeAmount" + rowIterator));
							key[rowIterator] = request.getParameter("chargeKey" + rowIterator);
						}
						ChargesDAO.matchVoucherCharges(adminBO.getMasterAssociateCode(), amount, key, numberOfRows, voucher);
					}
					request.setAttribute("error", "Voucher Created");
				} else {
					// screen = TDSConstants.getFailureJSP;
					request.setAttribute("error", "Voucher Created");
				}

			} else {
				request.setAttribute("voucherentry", voucherBO);
				request.setAttribute("errors", error);
			}
		} else if (request.getParameter("update") != null) {

			int result = RegistrationDAO.UpdateVoucherEntry(voucherBO, "update", adminBO, baseVoucher);
			if (result > 0) {
				// screen = TDSConstants.getSuccessJSP;
				if (request.getParameter("chargeSize") != null && !request.getParameter("chargeSize").equals("")) {
					int numberOfRows = Integer.parseInt(request.getParameter("chargeSize"));
					String voucher = request.getParameter("vno");
					String[] amount = new String[numberOfRows + 1];
					String[] key = new String[numberOfRows + 1];
					for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
						amount[rowIterator] = (request.getParameter("chargeAmount" + rowIterator).equals("") ? "0" : request.getParameter("chargeAmount" + rowIterator));
						key[rowIterator] = request.getParameter("chargeKey" + rowIterator);
					}
					ChargesDAO.matchVoucherCharges(adminBO.getMasterAssociateCode(), amount, key, numberOfRows, voucher);
				}
				request.setAttribute("error", "Voucher Updated");
			} else {
				// screen = TDSConstants.getFailureJSP;
				request.setAttribute("error", "Voucher Updated");
			}
		}
		if (request.getParameter("delete") != null) {
			int result = RegistrationDAO.UpdateVoucherEntry(voucherBO, "delete", adminBO, baseVoucher);
			if (result > 0) {
				// screen = TDSConstants.getSuccessJSP;
				request.setAttribute("error", "Voucher Deleted");
			} else {
				// screen = TDSConstants.getFailureJSP;
				request.setAttribute("error", "Voucher Deleted");
			}
		}
		ArrayList<ChargesBO> chargeType = ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(), 0);
		request.setAttribute("chargeTypes", chargeType);
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void CompanyVouchercreation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("in Create Company  Voucher Method ");
		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		String screen = TDSConstants.createCompanyVoucherJSP;
		VoucherBO Vno = RegistrationDAO.Vouchersequence("CRV", m_adminBO.getAssociateCode());
		request.setAttribute("voucherentry", Vno);
		if (request.getParameter("VNO") != "" && request.getParameter("VNO") != null) {
			VoucherBO voucherBO = new VoucherBO();
			voucherBO.setVno(request.getParameter("VNO"));
			ArrayList voucherdata = RegistrationDAO.getAllCompanyVoucher(voucherBO);
			voucherBO = (VoucherBO) voucherdata.get(0);
			request.setAttribute("voucherentry", voucherBO);
			request.setAttribute("page", "update");
			request.setAttribute("check", "1");
		}
		String forwardURL = TDSConstants.getMainNewJSP;
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getCompanyVoucherEditPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In getCompanyVoucherEditPage Voucher Method ");
		String screen = TDSConstants.getCompanyVoucherEditJSP;
		String forwardURL = TDSConstants.getMainNewJSP;
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void CompanyVoucherSave(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Save Company Voucher Entry Method");
		HttpSession session = request.getSession();
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.createCompanyVoucherJSP;
		AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
		VoucherBO voucherBO = new VoucherBO();
		voucherBO = setVoucherfromcompany(voucherBO, request);
		voucherBO.setVassoccode(adminBo.getAssociateCode());
		voucherBO.setVuser(adminBo.getUid());

		String error = checkcompanyVoucherData(voucherBO);
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		if (request.getParameter("submit") != null) {
			if (error.length() == 0) {
				int result = RegistrationDAO.VoucherSavefromcompany(voucherBO);
				if (result > 0) {
					request.setAttribute("success", "SuccessFully Created");
				} else {
					request.setAttribute("success", "Failure  Voucher Creation");
				}

			} else {

				request.setAttribute("errors", error);
			}
		} else if (request.getParameter("update") != null) {
			if (error.length() == 0) {
				int result = RegistrationDAO.updatecompanyVoucherEntry(voucherBO, "update", adminBo);
				if (result > 0) {
					request.setAttribute("success", "SuccessFully Updated");
					request.setAttribute("page", "update");
				} else {
					request.setAttribute("success", "Failure to Create Voucher");
					request.setAttribute("page", "update");
					// screen = TDSConstants.getFailureJSP;
					// request.setAttribute("page", " update voucher entry");
				}
			} else {

				request.setAttribute("page", "update");
				request.setAttribute("errors", error);
			}
		}

		if (request.getParameter("delete") != null) {
			int result = RegistrationDAO.UpdateVoucherEntry(voucherBO, "delete", adminBo, "");
			if (result > 0) {
				screen = TDSConstants.getSuccessJSP;
				request.setAttribute("page", " deleted voucher entry");
			} else {
				screen = TDSConstants.getFailureJSP;
				request.setAttribute("page", " delete voucher entry");
			}

		}
		request.setAttribute("voucherentry", voucherBO);

		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getAllcompanyVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In getAllVoucher Voucher Method ");
		// System.out.println("In getAllVoasdasdasdasdasdasdasducher Voucher Method ");
		HttpSession session = request.getSession();
		String screen = "";
		String forwardURL = TDSConstants.getMainNewJSP;
		VoucherBO voucherBO = new VoucherBO();
		voucherBO = setCompanyVoucherBOData(voucherBO, request);
		String vassocode = "";
		if (session.getAttribute("user") != null) {
			AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
			vassocode = adminBo.getAssociateCode();
		}
		// System.out.println("Voucher no "+vassocode);
		voucherBO.setVassoccode(vassocode);
		try {
			ArrayList voucherdata = RegistrationDAO.getAllCompanyVoucher(voucherBO);
			// System.out.println("voucherdata ="+voucherdata);
			if (voucherdata.size() == 1) {
				/*
				 * request.setAttribute("voucherentry", voucherdata.get(0));
				 * screen = TDSConstants.createVoucherJSP;
				 */
				request.setAttribute("summary", voucherdata);
				screen = TDSConstants.getCompanyVoucherEditJSP;
			} else {
				request.setAttribute("summary", voucherdata);
				screen = TDSConstants.getCompanyVoucherEditJSP;
			}
		} catch (Exception ex) {
			cat.info("Exception in getAllVoucher" + ex.getMessage());
		}
		request.setAttribute("modify", "modify");
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getAllVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In getAllVoucher Voucher Method ");
		HttpSession session = request.getSession();
		String screen = "";
		String forwardURL = TDSConstants.getMainNewJSP;
		VoucherBO voucherBO = new VoucherBO();
		voucherBO = setVoucherBOData(voucherBO, request);
		String vassocode = "";
		if (session.getAttribute("user") != null) {
			AdminRegistrationBO adminBo = (AdminRegistrationBO) session.getAttribute("user");
			vassocode = adminBo.getMasterAssociateCode();
		}
		// System.out.println("Voucher no "+vassocode);
		voucherBO.setVassoccode(vassocode);
		try {
			ArrayList voucherdata = RegistrationDAO.getAllVoucher(voucherBO);
			// System.out.println("voucherdata ="+voucherdata);
			if (voucherdata.size() == 1) {
				/*
				 * request.setAttribute("voucherentry", voucherdata.get(0));
				 * screen = TDSConstants.createVoucherJSP;
				 */
				request.setAttribute("summary", voucherdata);
				screen = TDSConstants.getVoucherEditJSP;
			} else {
				request.setAttribute("summary", voucherdata);
				screen = TDSConstants.getVoucherEditJSP;
			}

			if ((request.getParameter("event1") != null) && (request.getParameter("event1").equalsIgnoreCase("editVoucher"))) {
				cat.info("in editVoucher Method ");
				screen = TDSConstants.createVoucherJSP;
				// forwardURL =TDSConstants.getMainNewJSP;
				String voucher_id = "";
				if (request.getParameter("vno") != null) {
					voucher_id = request.getParameter("vno");
				}
				VoucherBO voucherBo = RegistrationDAO.getRecForVoucherNo(voucher_id);
				if (voucherBo.getVexpdate().equals("12/12/9999") || voucherBo.getVexpdate().equals("12/31/9999")) {
					voucherBo.setDontExpire(true);
					voucherBo.setVexpdate("");
				}

				ArrayList<ChargesBO> chargeType = ChargesDAO.getChargeTypes(vassocode, 0);
				ArrayList<ChargesBO> availCharges = ChargesDAO.getVoucherCharge(vassocode, voucher_id);
				request.setAttribute("availCharges", availCharges.size());
				request.setAttribute("chargeTypes", chargeType);
				request.setAttribute("screen", screen);
				request.setAttribute("modify", "modify");
				request.setAttribute("voucherentry", voucherBo);
			}
		} catch (Exception ex) {
			cat.info("Exception in getAllVoucher" + ex.getMessage());
		}
		request.setAttribute("modify", "modify");
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void editVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("in editVoucher Method ");

		// System.out.println("check EDIT VOUCHER DETAILS");

		String screen = TDSConstants.createVoucherJSP;
		String forwardURL = TDSConstants.getMainNewJSP;
		String voucher_id = "";
		if (request.getParameter("vno") != null) {
			voucher_id = request.getParameter("vno");
		}
		VoucherBO voucherBO = RegistrationDAO.getRecForVoucherNo(voucher_id);
		request.setAttribute("screen", screen);
		request.setAttribute("modify", "modify");
		request.setAttribute("voucherentry", voucherBO);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void editDriverDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("drFlag", RegistrationDAO.getCmpyVehicleFlag(((AdminRegistrationBO) request.getSession().getAttribute("user")).getMasterAssociateCode(), "1"));
		HttpSession session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) session.getAttribute("user");
		request.setAttribute("al_q", CheckZone.returnZonesForAssociatoinCode(getServletContext(), m_adminBO.getAssociateCode()));
		String seq = request.getParameter("seq");
		DriverRegistrationBO driverBO = null;
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.registrationJSP;
		// String forwardURL = TDSConstants.registrationJSP;
		driverBO = RegistrationDAO.getDriverDetailBySeq(seq, m_adminBO.getAssociateCode());
		
		if(TDSProperties.getValue("WASL_Company").equals(m_adminBO.getAssociateCode())){
			try{
				if(!driverBO.getDr_details_json().equals("")){
					JSONObject pbj = new JSONObject(driverBO.getDr_details_json());
					if(pbj!=null){
						driverBO.setDr_dob(pbj.getString("dateOfBirth"));
					}else{
						driverBO.setDr_dob("N/A");
					}
				}else{
					driverBO.setDr_dob("N/A");
				}
				 
			}catch(JSONException e){
				driverBO.setDr_dob("N/A");
			}
		}
		
		OpenRequestBO openBO = new OpenRequestBO();
		openBO.setQueueno(RegistrationDAO.getZoneNo(seq, m_adminBO.getAssociateCode()));
		request.setAttribute("driverDetails", driverBO);
		request.setAttribute("defaultZone", openBO);
		request.setAttribute("edit", "editdriverDetails");
		request.setAttribute("template", ChargesDAO.getChargeTemplate(m_adminBO.getMasterAssociateCode(), 2));
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	/*
	 * public void editUpdateDriverDetail(HttpServletRequest
	 * request,HttpServletResponse response) throws ServletException,IOException
	 * { System.out.println("In editUpdateDriverDetail"); String seq =
	 * request.getParameter("seq"); DriverRegistrationBO driverBO = new
	 * DriverRegistrationBO(); String forwardURL = TDSConstants.getMainNewJSP;
	 * RequestDispatcher requestDispatcher =
	 * getServletContext().getRequestDispatcher(forwardURL); driverBO =
	 * setDriverData(driverBO, request); String screen =
	 * TDSConstants.registrationJSP; int status = 0; driverBO.setSno(seq);
	 * status = RegistrationDAO.editUpdateDriverDetail(driverBO); if(status >
	 * 0){ screen = TDSConstants.getSuccessJSP; request.setAttribute("page",
	 * " Driver " + driverBO.getUid()+" was updated"); //response.sendRedirect(
	 * "/TDS/control?action=registration&event=editUpdateDD&page=Driver " +
	 * driverBO.getUid()+" was updated"); requestDispatcher.forward(request,
	 * response); } else { screen = TDSConstants.getFailureJSP;
	 * request.setAttribute("page", " update driver " + driverBO.getUid());
	 * request.setAttribute("screen", screen);
	 * requestDispatcher.forward(request, response); }
	 * 
	 * }
	 */

	public void editUpdateDriverDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {

		String seq = request.getParameter("seq");
		DriverRegistrationBO driverBO = new DriverRegistrationBO();
		String forwardURL = TDSConstants.getMainNewJSP;
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		request.setAttribute("template", ChargesDAO.getChargeTemplate(adminBO.getMasterAssociateCode(), 2));
		request.setAttribute("al_q", CheckZone.returnZonesForAssociatoinCode(getServletContext(), adminBO.getAssociateCode()));
		request.setAttribute("drFlag", RegistrationDAO.getCmpyVehicleFlag(adminBO.getMasterAssociateCode(), "1"));
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		driverBO = setDriverData(driverBO, request);
		String screen = TDSConstants.registrationJSP;
		String queueNo = request.getParameter("queueno");

		int status = 0;
		driverBO.setSno(seq);
		String error = RegistrationValidation.driverValidation(driverBO);
		if (!(error.length() > 0)) {
			if (!request.getParameter("Information").equals("")) {
				status = RegistrationDAO.editUpdateDriverDetail(driverBO, adminBO, 1);
			} else {
				status = RegistrationDAO.editUpdateDriverDetail(driverBO, adminBO, 2);
			}
			
			if(TDSProperties.getValue("WASL_Company").equals(adminBO.getAssociateCode())){
				String drJson = request.getParameter("drJson")!=null?request.getParameter("drJson"):"";
				if(!drJson.equals("")){
					try{
						JSONArray array = new JSONArray(drJson);
						JSONObject jobj = array.getJSONObject(0);
						jobj.remove("emailAddress");
						jobj.remove("mobileNumber");
						
						jobj.put("emailAddress", driverBO.getEmailId());
						jobj.put("mobileNumber", driverBO.getPhone());
						
						RegistrationDAO.updateWaslJSONforDriver(jobj.toString(), adminBO.getAssociateCode(), driverBO.getUid());
					}catch(JSONException e){
						System.out.println(e.getMessage());
					}
				}
			}
			
			RegistrationDAO.updateDriverZone(queueNo, adminBO.getAssociateCode(), seq);
			if (status > 0) {

				if (request.getParameter("NextScreen") != null) {
					String userid = "";
					String userdesc = "";
					String status1 = "";
					userid = request.getParameter("uid") == null ? "" : request.getParameter("uid");
					userdesc = request.getParameter("registration") == null ? "" : request.getParameter("registration");
					status1 = request.getParameter("status") == null ? "" : request.getParameter("status");
					if (userdesc.equalsIgnoreCase("O")) {
						response.sendRedirect("control?action=userrolerequest&module=systemsetupView&event=getUserList&wizard=yes&userid=" + userid + "&uname=" + userid + "&utype=" + userdesc + "&userDesc=" + userdesc + "&status=" + status1);
					} else {
						response.sendRedirect("control?action=registration&event=driverMapping&module=operationView&wizard=yes&driverid=" + userid + "&button=Search");
					}
				} else {
					request.setAttribute("update", "Successfully Updated");
					response.sendRedirect("control?action=registration&event=summaryDR&module=systemsetupView");
				}
			} else {
				screen = TDSConstants.registrationJSP;
				request.setAttribute("errors", "Failed to update driver " + driverBO.getUid());
				request.setAttribute("screen", screen);
				requestDispatcher.forward(request, response);
			}
		} else {
			screen = TDSConstants.registrationJSP;
			// String
			// greatestDriver=SystemPropertiesDAO.getGreatestDriver(adminBO.getAssociateCode());
			// request.setAttribute("greatestDriver", greatestDriver);
			request.setAttribute("errors", error);
			request.setAttribute("edit", "1");
			request.setAttribute("driverBO", driverBO);
			request.setAttribute("screen", screen);
			requestDispatcher.forward(request, response);
		}
	}

	private VoucherBO setVoucherfromcompany(VoucherBO p_voucherBO, HttpServletRequest request) {
		if (request.getParameter("vno") != null) {
			p_voucherBO.setVno(request.getParameter("vno"));
		}
		if (request.getParameter("vname") != null) {
			p_voucherBO.setVname(request.getParameter("vname"));
		}
		if (request.getParameter("vfreq") != null) {
			p_voucherBO.setVfreq(request.getParameter("vfreq"));
		}
		if (request.getParameter("vamount") != null) {
			p_voucherBO.setVamount(request.getParameter("vamount"));
		}
		if (request.getParameter("vexpdate") != null && request.getParameter("vexpdate").length() == 10) {
			p_voucherBO.setVexpdate(request.getParameter("vexpdate"));
		}
		if (request.getParameter("vdesc") != null) {
			p_voucherBO.setVdesc(request.getParameter("vdesc"));
		}
		if (request.getParameter("from_date") != null) {
			p_voucherBO.setFrom_date(request.getParameter("from_date"));
		}
		if (request.getParameter("to_date") != null) {
			p_voucherBO.setTo_date(request.getParameter("to_date"));
		}

		p_voucherBO.setVcode(request.getParameter("vcode"));
		p_voucherBO.setVcontact(request.getParameter("vcontact"));
		p_voucherBO.setVdelay(request.getParameter("vdelay"));

		return p_voucherBO;
	}

	private String checkcompanyVoucherData(VoucherBO p_voucherBO) {
		StringBuffer errors = new StringBuffer();

		if (p_voucherBO.getVno() == "" || p_voucherBO.getVno().length() > 20) {
			errors.append("You must enter Voucher no<br>");
		}
		// if(p_voucherBO.getVname() == "") {
		// errors.append("You must enter Rider name<br>");
		// }

		String ss = p_voucherBO.getVamount().split(" ")[0].equals("$") ? p_voucherBO.getVamount().split(" ")[1] : p_voucherBO.getVamount().split(" ")[0];

		if (!TDSValidation.isDoubleNumber(ss)) {
			errors.append("You must enter a valid amount<br>");
		}
		String s = p_voucherBO.getVcode().split(";")[0];
		if (p_voucherBO.getVcode() == "") {
			errors.append("You Must Enter The Company Code  <br>");
		}
		if (p_voucherBO.getVexpdate().equalsIgnoreCase("")) {
			errors.append("You must enter Expire Date<br>");
		}
		if (RegistrationDAO.checkVoucherNo(p_voucherBO.getVno()) != 0) {
			errors.append("Voucher Doesnt Exist  <br>");
		}
		if (!TDSValidation.isOnlyNumbers(p_voucherBO.getVdelay())) {
			// errors.append("You Must Enter The Valid Days  <br>");
			p_voucherBO.setVdelay("0");
		}

		return errors.toString();
	}

	private VoucherBO setCompanyVoucherBOData(VoucherBO p_voucherBO, HttpServletRequest request) {
		if (request.getParameter("vno") != null) {
			p_voucherBO.setVno(request.getParameter("vno"));
		}
		if (request.getParameter("vname") != null) {
			p_voucherBO.setVname(request.getParameter("vname"));
		}

		if (request.getParameter("from_date") != null) {
			p_voucherBO.setFrom_date(request.getParameter("from_date"));
		}
		if (request.getParameter("to_date") != null) {
			p_voucherBO.setTo_date(request.getParameter("to_date"));
		}

		p_voucherBO.setVcode(request.getParameter("vcode"));
		return p_voucherBO;
	}

	/*
	 * public void getCompanyMasterEntry(HttpServletRequest
	 * request,HttpServletResponse response) throws ServletException,IOException
	 * { String screen = TDSConstants.getCompanyMasterJSP; String forwardURL =
	 * TDSConstants.getMainNewJSP; cat.info("In getCompanyMasterEntry page");
	 * request.setAttribute("pagefor","Submit"); request.setAttribute("screen",
	 * screen); RequestDispatcher requestDispatcher =
	 * getServletContext().getRequestDispatcher(forwardURL);
	 * requestDispatcher.forward(request, response); }
	 */
	private StringBuffer unsettledvalidate(HttpServletRequest request) throws ServletException, IOException {
		StringBuffer error = new StringBuffer();
		// System.out.println(error.toString());
		if (request.getParameter("checkedval").equals("") || request.getParameter("checkedval") == null) {
			error.append("Select Check Box for settled Txn");
		}
		return error;
	}

	public void saveCompanyMasterEntry(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Save Company Master Entry Method ");
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.getCompanyMasterJSP;
		HttpSession session = request.getSession();
		CompanyMasterBO companyBO = new CompanyMasterBO();
		String Button = request.getParameter("submit");
		String filename = request.getParameter("filename") == null ? "" : request.getParameter("filename");
		String fileid = request.getParameter("fileid") == null ? "" : request.getParameter("fileid");

		if (Button == null) {
			screen = TDSConstants.getCompanyMasterJSP;
			request.setAttribute("pagefor", "Submit");
			request.setAttribute("fileid", fileid);
			request.setAttribute("filename", filename);
			request.setAttribute("screen", screen);
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			requestDispatcher.forward(request, response);
		} else if (Button.equalsIgnoreCase("Edit")) {
			ArrayList A_list = RegistrationDAO.CompanyMasterview(((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode());
			filename = ((CompanyMasterBO) (A_list).get(0)).getFilename();
			fileid = ((CompanyMasterBO) (A_list).get(0)).getFileid();
			if (A_list != null)
				request.setAttribute("companyBO", (A_list).get(0));
			screen = TDSConstants.getCompanyMasterJSP;
			request.setAttribute("pagefor", "Update");
			request.setAttribute("fileid", fileid);
			request.setAttribute("filename", filename);
			request.setAttribute("screen", screen);
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			requestDispatcher.forward(request, response);
		} else if (Button.equalsIgnoreCase("Create Company")) {

			companyBO = assignCompanyData(companyBO, request);
			String error = "";
			error = checkCompanyMaster(companyBO);
			if (error.length() == 0) {
				int result = RegistrationDAO.saveCompanyMaster(companyBO, "insert", "");
				if (result > 0) {
					cat.info(" inserted Company Master");
					request.setAttribute("page", " created new company");
					request.setAttribute("pagefor", "Submit");
					screen = TDSConstants.getSuccessJSP;
					request.setAttribute("fileid", fileid);
					request.setAttribute("filename", filename);
					request.setAttribute("screen", screen);
					RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
					requestDispatcher.forward(request, response);
				} else {
					cat.info(" insert Company Master");
					request.setAttribute("page", " to create new company");
					request.setAttribute("pagefor", "Submit");
					screen = TDSConstants.getFailureJSP;
					request.setAttribute("fileid", fileid);
					request.setAttribute("filename", filename);
					request.setAttribute("screen", screen);
					RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
					requestDispatcher.forward(request, response);
				}
			} else {
				request.setAttribute("pagefor", "Submit");
				request.setAttribute("errors", error);
				request.setAttribute("companyBO", companyBO);
				request.setAttribute("fileid", fileid);
				request.setAttribute("filename", filename);
				request.setAttribute("screen", screen);
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			}
		} else if (Button.equalsIgnoreCase("Update")) {
			companyBO = assignCompanyData(companyBO, request);
			String error = "";
			error = checkCompanyMaster(companyBO);
			if (error.length() == 0) {
				int result = RegistrationDAO.saveCompanyMaster(companyBO, "Update", ((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode());
				if (result > 0) {
					response.sendRedirect("control?action=registration&event=savecompany&submit=View&module=systemsetupView");
				} else {
					cat.info(" insert Company Master");
					request.setAttribute("page", " to Update company Details");
					request.setAttribute("pagefor", "Submit");
					screen = TDSConstants.getFailureJSP;
					request.setAttribute("screen", screen);
					RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
					requestDispatcher.forward(request, response);
				}
			} else {
				request.setAttribute("pagefor", "Update");
				request.setAttribute("errors", error);
				request.setAttribute("companyBO", companyBO);
				request.setAttribute("fileid", fileid);
				request.setAttribute("filename", filename);
				request.setAttribute("screen", screen);
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			}

		} else if (Button.equalsIgnoreCase("View")) {
			cat.info("In getCompanyMasterview page");
			ArrayList A_list = RegistrationDAO.CompanyMasterview(((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode());
			request.setAttribute("companyBO", A_list);
			screen = "/jsp/CompanyView.jsp";
			request.setAttribute("fileid", fileid);
			request.setAttribute("filename", filename);
			request.setAttribute("screen", screen);
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			requestDispatcher.forward(request, response);
		}
	}

	/**
	 * @param p_request
	 * @param p_response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to create penalty of a driver
	 *      because if the driver is not attend the Open Request.
	 */
	public void createBehavior(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		String m_error = "";
		String m_screen = TDSConstants.driverBehaviorJSP;
		Map m_driverMap = null;
		DriverBehaviorBO m_behavoirBO = new DriverBehaviorBO();
		AdminRegistrationBO m_adminBo = null;
		if (m_session.getAttribute("user") != null) {
			m_adminBo = (AdminRegistrationBO) m_session.getAttribute("user");
			m_behavoirBO.setDriverId(m_adminBo.getUid());
		}
		// System.out.println("Save "+p_request.getParameter("save"));
		// System.out.println("Update "+p_request.getParameter("update"));
		m_behavoirBO = setDriverBehavior(p_request, m_behavoirBO);
		m_error = checkDriverBehavior(m_behavoirBO);
		if (m_behavoirBO.getDriverName() != null && m_behavoirBO.getDriverName().length() > 0) {
			m_driverMap = RegistrationDAO.getDriverIDandCode(m_behavoirBO.getDriverName());
			if (m_driverMap != null) {
				m_behavoirBO.setAssocCode(m_driverMap.get("assoccode").toString());
				m_behavoirBO.setDriverId(m_driverMap.get("driverId").toString());
			}
		}
		if (m_error.length() > 0) {
			// System.out.println("There is an error in page "+m_error);
			p_request.setAttribute("driverBehavior", m_behavoirBO);
			p_request.setAttribute("errors", m_error);
		} else if (p_request.getParameter("save") != null && p_request.getParameter("save").equalsIgnoreCase("Save")) {

			if (RegistrationDAO.insertDriverBehavior(m_behavoirBO)) {
				// System.out.println("Successfully inserted Driver Behavior");
				cat.info("Successfully inserted Driver Behavior");
				p_request.setAttribute("page", " created Driver penalty");
				m_screen = TDSConstants.getSuccessJSP;
			} else {
				// System.out.println("Failed to insert Driver Behavior");
				cat.info("Failed to insert Driver penalty");
				p_request.setAttribute("page", " create Driver Behavior");
				m_screen = TDSConstants.getFailureJSP;
			}
		} else if (p_request.getParameter("update") != null && p_request.getParameter("update").equalsIgnoreCase("Update")) {
			if (RegistrationDAO.updateDriverBehavior(m_behavoirBO)) {
				// System.out.println(" update the driver penalty");
				cat.info("Successfully Updated Driver Behavior");
				p_request.setAttribute("page", " updated Driver Behavior");
				m_screen = TDSConstants.getSuccessJSP;
			} else {
				System.out.println("Failed to Update Driver Behavior");
				cat.info("Failed to updated Driver Behavior");
				p_request.setAttribute("page", " update Driver Behavior");
				m_screen = TDSConstants.getFailureJSP;
			}
		}
		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	/**
	 * @param p_request
	 * @param p_response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to retrieve the particular behavior
	 *      id for edit and update the particular driver behavior.
	 */
	public void getBehaviorById(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		String m_screen = TDSConstants.driverBehaviorJSP;
		String m_behaviorId = p_request.getParameter("behaviorID");
		DriverBehaviorBO m_behaviorBO = RegistrationDAO.getBehaviorByNo(m_behaviorId);
		p_request.setAttribute("driverBehavior", m_behaviorBO);
		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	/**
	 * @param p_request
	 * @param p_response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to get all the behavior of a
	 *      particular User.
	 */
	public void behaviorSummary(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		String m_driverId = "";
		Map m_criteriaMap = null;
		String m_screen = TDSConstants.behaviorSummaryJSP;
		AdminRegistrationBO m_adminBO = null;
		List m_behavoirList = null;
		if (m_session.getAttribute("user") != null) {
			m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
			m_driverId = m_adminBO.getUid();
		}
		if (p_request.getParameter("btype") != null && p_request.getParameter("btype").length() > 0) {
			m_criteriaMap = (m_criteriaMap == null ? new HashMap() : m_criteriaMap);
			m_criteriaMap.put("type", p_request.getParameter("btype").toString());
		}
		if (p_request.getParameter("bdate") != null && p_request.getParameter("bdate").length() > 0) {
			m_criteriaMap = (m_criteriaMap == null ? new HashMap() : m_criteriaMap);
			m_criteriaMap.put("date", p_request.getParameter("bdate"));
		}
		// System.out.println("Criteria Detail "+ m_criteriaMap);
		m_behavoirList = RegistrationDAO.getBehaviorList(m_criteriaMap);
		System.out.println("Data Size " + m_behavoirList);
		p_request.removeAttribute("behavior");
		if (m_behavoirList != null) {
			p_request.setAttribute("behavior", m_behavoirList);
		}
		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	public void createCabMaintenance(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		String m_error = "";
		String m_screen = TDSConstants.cabMaintenanceJSP;
		CabMaintenanceBO m_cabMaintenanceBO = new CabMaintenanceBO();
		AdminRegistrationBO m_adminBo = null;
		if (m_session.getAttribute("user") != null) {
			m_adminBo = (AdminRegistrationBO) m_session.getAttribute("user");
			m_cabMaintenanceBO.setDriverKey(m_adminBo.getUid());
			m_cabMaintenanceBO.setAssocCode(m_adminBo.getAssociateCode());
		}
		// System.out.println("Save "+p_request.getParameter("save"));
		// System.out.println("Update "+p_request.getParameter("update"));
		m_cabMaintenanceBO = setCabMaintenance(p_request, m_cabMaintenanceBO);
		m_error = checkMaintenance(m_cabMaintenanceBO);
		if (m_error.length() > 0) {
			// System.out.println("There is an error in page "+m_error);
			p_request.setAttribute("cabMaintenance", m_cabMaintenanceBO);
			p_request.setAttribute("errors", m_error);
		} else if (p_request.getParameter("save") != null && p_request.getParameter("save").equalsIgnoreCase("Save")) {
			// System.out.println("data in save mwthod");
			if (RegistrationDAO.insertCabMaintenance(m_cabMaintenanceBO)) {
				// System.out.println("Successfully inserted Cab Maintenance");
				cat.info("Successfully inserted Cab Maintenance");
				p_request.setAttribute("errors", "Created Cab Maintenance");
				// m_screen = TDSConstants.getSuccessJSP;
			} else {
				// System.out.println("Failed to insert Cab Maintenance");
				cat.info("Failed to insert Cab Maintenance");
				p_request.setAttribute("errors", " Created Cab Maintenance");
				// m_screen = TDSConstants.getFailureJSP;
			}
		} else if (p_request.getParameter("update") != null && p_request.getParameter("update").equalsIgnoreCase("Update")) {
			if (RegistrationDAO.updateCabMaintenance(m_cabMaintenanceBO, m_adminBo)) {
				// System.out.println("Successfully Updated Cab Maintenance");
				cat.info("Successfully Updated Cab Maintenance");
				p_request.setAttribute("errors", "Updated cab maintenance");
				// m_screen = TDSConstants.getSuccessJSP;
			} else {
				// System.out.println("Failed to updated Cab Maintenance");
				cat.info("Failed to updated Cab Maintenance");
				p_request.setAttribute("errors", " Updated cab maintenance");
				// m_screen = TDSConstants.getFailureJSP;
			}
		}
		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	/**
	 * @param p_request
	 * @param p_response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to retrieve the particular Cab
	 *      Maintenance id for edit and update.
	 */
	public void getMaintenanceById(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		String m_screen = TDSConstants.cabMaintenanceJSP;
		String m_maintenanceId = p_request.getParameter("maintenanceID");
		CabMaintenanceBO m_cabMaintenanceBO = RegistrationDAO.getCabMaintenanceByNo(m_maintenanceId);
		p_request.setAttribute("cabMaintenance", m_cabMaintenanceBO);
		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	/**
	 * @param p_request
	 * @param p_response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to get all the behavior of a
	 *      particular User.
	 */
	public void maintenanceSummary(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		String m_screen = TDSConstants.cabMaintenanceSummaryJSP;
		Map m_criteriaMap = null;
		AdminRegistrationBO m_adminBO = null;
		List m_cabMaintenanceList = null;
		if (m_session.getAttribute("user") != null) {
			m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		} else {
			return;
		}
		if (p_request.getParameter("Button") != null) {
			if (p_request.getParameter("scode") != null && p_request.getParameter("scode").length() > 0) {
				m_criteriaMap = (m_criteriaMap == null ? new HashMap() : m_criteriaMap);
				m_criteriaMap.put("scode", p_request.getParameter("scode").toString());
			}
			if (p_request.getParameter("sdate") != null && p_request.getParameter("sdate").length() > 0) {
				m_criteriaMap = (m_criteriaMap == null ? new HashMap() : m_criteriaMap);
				m_criteriaMap.put("date", p_request.getParameter("sdate"));
			}
			m_cabMaintenanceList = RegistrationDAO.getCabMaintenanceList(m_criteriaMap, m_adminBO);

			if (m_cabMaintenanceList != null) {
				p_request.setAttribute("maintenance", m_cabMaintenanceList);
			}
		}
		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	public void createLostandFound(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		String m_error = "";
		String m_screen = TDSConstants.lostandFoundJSP;
		LostandFoundBO m_lostandFoundBO = new LostandFoundBO();
		AdminRegistrationBO m_adminBo = null;
		if (m_session.getAttribute("user") != null) {
			m_adminBo = (AdminRegistrationBO) m_session.getAttribute("user");
			m_lostandFoundBO.setDriverid(m_adminBo.getUid());
			m_lostandFoundBO.setAssocCode(m_adminBo.getAssociateCode());
		}

		m_lostandFoundBO = setLostandFound(p_request, m_lostandFoundBO);
		String mode = p_request.getParameter("mode");
		m_error = checkLostandFound(m_lostandFoundBO);

		if (m_error.length() > 0) {
			p_request.setAttribute("lostandfound", m_lostandFoundBO);
			p_request.setAttribute("errors", m_error);
		} else if (p_request.getParameter("save") != null && p_request.getParameter("save").equalsIgnoreCase("Save")) {
			if (RegistrationDAO.insertLostandFound(m_lostandFoundBO, mode)) {
				cat.info("Successfully inserted Lost and Found ");
				p_request.setAttribute("errors", "Previous Data Inserted Sucessfully ");

				p_request.setAttribute("page", " added lost and found data");
				m_screen = TDSConstants.lostandFoundSummaryJSP;
			} else {
				cat.info("Failed to insert Lost and Found");
				p_request.setAttribute("page", " add lost and found data");
				m_screen = TDSConstants.getFailureJSP;
			}
		} else if (p_request.getParameter("update") != null && p_request.getParameter("update").equalsIgnoreCase("Update")) {

			if (RegistrationDAO.updateLostandFound(m_lostandFoundBO, m_adminBo)) {
				cat.info("Successfully Updated Lost and Found");
				p_request.setAttribute("page", " updated Lost and Found record");
				p_request.setAttribute("errors", "Previous Data Updated Sucessfully ");
				m_screen = TDSConstants.lostandFoundSummaryJSP;
			} else {
				// System.out.println("Failed to updated Lost and Found");
				cat.info("Failed to updated Lost and Found");
				p_request.setAttribute("page", " update lost and found");
				m_screen = TDSConstants.getFailureJSP;
			}
		}
		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	/**
	 * @param p_request
	 * @param p_response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to retrieve the particular Lost and
	 *      Found id for edit and update.
	 */
	public void getLostandFoundById(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		String m_screen = TDSConstants.lostandFoundJSP;
		String m_lostandFoundId = p_request.getParameter("lostandFoundID");
		LostandFoundBO m_lostandFoundBO = RegistrationDAO.getLostandFoundByNo(m_lostandFoundId);
		p_request.setAttribute("lostandfound", m_lostandFoundBO);
		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	/**
	 * @param p_request
	 * @param p_response
	 * @throws ServletException
	 * @throws IOException
	 * @author vimal
	 * @see Description This method is used to get all the behavior of a
	 *      particular User.
	 */
	public void lostandFoundSummary(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession();
		String m_screen = TDSConstants.lostandFoundSummaryJSP;
		System.out.println(" In lost and Found  Method");
		AdminRegistrationBO m_adminBO = null;
		Map m_criteriaMap = null;
		List m_lostandFoundList = null;
		if (m_session.getAttribute("user") != null) {
			m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
			// m_driverId = m_adminBO.getUid();
		}

		if (p_request.getParameter("itemName") != null && p_request.getParameter("itemName").length() > 0) {
			m_criteriaMap = (m_criteriaMap == null ? new HashMap() : m_criteriaMap);
			m_criteriaMap.put("itemName", p_request.getParameter("itemName").toString());
		}
		if (p_request.getParameter("tripDate") != null && p_request.getParameter("tripDate").length() > 0) {
			m_criteriaMap = (m_criteriaMap == null ? new HashMap() : m_criteriaMap);
			m_criteriaMap.put("date", p_request.getParameter("tripDate"));
		}

		m_lostandFoundList = RegistrationDAO.getlostAndFoundList(m_criteriaMap, m_adminBO);
		if (m_lostandFoundList != null) {
			p_request.setAttribute("lostAndFound", m_lostandFoundList);
		}
		if (p_request.getParameter("detailsForLAF") != null) {
			m_screen = TDSConstants.lostandFoundJSP;
			String m_lostandFoundId = p_request.getParameter("lostandFoundID");
			LostandFoundBO m_lostandFoundBO = RegistrationDAO.getLostandFoundByNo(m_lostandFoundId);
			p_request.setAttribute("lostandfound", m_lostandFoundBO);
		}

		p_request.setAttribute("screen", m_screen);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	// public void

	/*
	 * 
	 * 
	 * Following code is used to Assign User data into Bean and having the
	 * validation code
	 */

	public DriverRegistrationBO setDriverData(DriverRegistrationBO driverBO, HttpServletRequest request) throws SystemUnavailableException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");

		// System.out.println("Account:" + request.getParameter("acct"));

		driverBO.setAcct(request.getParameter("acct").toString().trim());
		driverBO.setAdd1(request.getParameter("add1").toString().trim());
		driverBO.setAdd2(request.getParameter("add2").toString().trim());
		driverBO.setCity(request.getParameter("city").toString().trim());
		driverBO.setCmake(request.getParameter("cmake").toString().trim());
		driverBO.setCmodel(request.getParameter("cmodel").toString().trim());
		driverBO.setCyear(request.getParameter("cyear").toString().trim());
		driverBO.setFdeposit(request.getParameter("fdeposit").toString().trim());
		driverBO.setFname(request.getParameter("fname").toString().trim());
		driverBO.setLname(request.getParameter("lname").toString().trim());
		String passwordHash = PasswordHash.encrypt(request.getParameter("password"));
		driverBO.setPassword(passwordHash.trim());
		
		driverBO.setEmailId(request.getParameter("mailId").toString().trim());
		driverBO.setPhone(request.getParameter("phone").toString().trim());
		driverBO.setState(request.getParameter("state").toString().trim());
		driverBO.setStatus(request.getParameter("status").toString().trim());
		driverBO.setUid(request.getParameter("uid").trim());
		// driverBO.setDriExten(request.getParameter("drExt").trim());
		driverBO.setZip(request.getParameter("zip").trim());
		driverBO.setEhostid(request.getParameter("ehost").trim());

		String rePasswordHash = PasswordHash.encrypt(request.getParameter("repassword"));
		driverBO.setRepassword(rePasswordHash.trim());
		driverBO.setCabno(request.getParameter("cabno").trim());
		driverBO.setPassno(request.getParameter("passno").trim());
		driverBO.setRegistration(request.getParameter("registration").trim().toCharArray()[0]);
		driverBO.setPayUserId(request.getParameter("payUserId").toString().trim());
		driverBO.setPayPassword(request.getParameter("payPassWord").toString().trim());
		driverBO.setPayMerchant(request.getParameter("payMerchand").toString().trim());
		driverBO.setPayRePassword(request.getParameter("payrePassWord").toString().trim());
		driverBO.setDrLicense(request.getParameter("drLicense"));
		driverBO.setDr_dob(request.getParameter("drDOB")==null ?"":request.getParameter("drDOB"));
		driverBO.setDrLicenseExpiry(request.getParameter("drExpiryDt"));
		driverBO.setHkLicense(request.getParameter("hkLicense"));
		driverBO.setHkLicenseExpiry(request.getParameter("hkExpiryDt"));
		driverBO.setSocialSecurityNo(request.getParameter("soSecurityNo"));
		driverBO.setTemplate(Integer.parseInt(request.getParameter("templateType") == null ? "0" : request.getParameter("templateType")));
		driverBO.setDriverRatings(Integer.parseInt(request.getParameter("driverRatings")));
		driverBO.setSettingsAccess(Integer.parseInt(request.getParameter("drAccess") == null ? "0" : request.getParameter("drAccess")));
		try {
			if ((request.getParameter("profileSize") != null && Integer.parseInt(request.getParameter("profileSize")) > 0) || (request.getParameter("profileSizeAjax") != null && !request.getParameter("profileSizeAjax").equals(""))) {
				int size = 0;
				if (request.getParameter("profileSize") != null) {
					size = Integer.parseInt(request.getParameter("profileSize"));
				} else {
					size = Integer.parseInt(request.getParameter("profileSizeAjax"));
				}
				ArrayList al_l = new ArrayList();
				String drProfile = "";
				for (int i = 0; i < size; i++) {
					if (request.getParameter("chk" + i) != null) {
						al_l.add(request.getParameter("chk" + i));
						drProfile = drProfile + request.getParameter("chk" + i);
					}
				}
				driverBO.setAl_list(al_l);
				driverBO.setDrProfile(drProfile);
			}
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}
			
		return driverBO;

	}

	/*
	 * public AdminRegistrationBO getAdminBean(HttpServletRequest request) {
	 * AdminRegistrationBO adminRegistrationBO = new AdminRegistrationBO();
	 * adminRegistrationBO.setPcompany(request.getParameter("pcompany").trim());
	 * adminRegistrationBO.setUname(request.getParameter("uname").trim());
	 * adminRegistrationBO.setFname(request.getParameter("Fname").trim());
	 * adminRegistrationBO.setLname(request.getParameter("Lname").trim());
	 * adminRegistrationBO.setDrid(request.getParameter("Drid").trim());
	 * adminRegistrationBO.setPassword(request.getParameter("password").trim());
	 * adminRegistrationBO
	 * .setRepassword(request.getParameter("repassword").trim());
	 * adminRegistrationBO.setUsertype(request.getParameter("usertype").trim());
	 * adminRegistrationBO
	 * .setPaymentType(request.getParameter("paymentType").trim());
	 * adminRegistrationBO.setEmail(request.getParameter("Email").trim());
	 * return adminRegistrationBO; }
	 */
	public AdminRegistrationBO getAdminBean(HttpServletRequest request) throws SystemUnavailableException {
		AdminRegistrationBO adminRegistrationBO = new AdminRegistrationBO();
		/*
		 * adminRegistrationBO.setPcompany(request.getParameter("pcompany").trim(
		 * ));
		 */
		adminRegistrationBO.setUname(request.getParameter("uname").trim());
		adminRegistrationBO.setFname(request.getParameter("Fname").trim());
		adminRegistrationBO.setLname(request.getParameter("Lname").trim());
		adminRegistrationBO.setDrid(request.getParameter("Drid").trim());
		String password = PasswordHash.encrypt(request.getParameter("password"));
		adminRegistrationBO.setPassword(password.trim());
		String rePassoword = PasswordHash.encrypt(request.getParameter("repassword"));
		adminRegistrationBO.setRepassword(rePassoword.trim());
		adminRegistrationBO.setUsertype(request.getParameter("usertype").trim());
		adminRegistrationBO.setActive(request.getParameter("Active").trim());
		adminRegistrationBO.setEmail(request.getParameter("Email").trim());
		return adminRegistrationBO;
	}

	public String checkAdminUser(AdminRegistrationBO p_adminBO) {
		StringBuffer errors = new StringBuffer();
		/*
		 * if(p_adminBO.getPcompany() == "") {
		 * errors.append("You must entered Parent Company Name<br>"); }
		 */
		if (p_adminBO.getUname() == "") {
			errors.append("Please enter User name <br>");
		}
		if (p_adminBO.getFname() == "") {
			errors.append("Please enter First name <br>");
		}
		if (p_adminBO.getPassword() == "") {
			errors.append("You must enter Password <br>");
		}
		if (p_adminBO.getRepassword() == "") {
			errors.append("You must re-enter Password<br>");
		}
		if (p_adminBO.getUsertype() == "") {
			errors.append("You must select User Type<br>");
		}
		/*
		 * if(p_adminBO.getPaymentType() == "") {
		 * errors.append("You must entered Payment type<br>"); }
		 */

		if (p_adminBO.getPassword() != "" && p_adminBO.getRepassword() != "" && !p_adminBO.getPassword().equalsIgnoreCase(p_adminBO.getRepassword())) {
			errors.append("Password and Re-Type of Password dont match<br>");
		}
		return errors.toString();
	}

	private VoucherBO setVoucherBOData(VoucherBO p_voucherBO, HttpServletRequest request) {
		if (request.getParameter("vno") != null) {
			p_voucherBO.setVno(request.getParameter("vno"));
		}
		if (request.getParameter("vname") != null) {
			p_voucherBO.setVname(request.getParameter("vname"));
		}
		if (request.getParameter("vfreq") != null) {
			p_voucherBO.setVfreq(request.getParameter("vfreq"));
		}
		if (request.getParameter("vcostcenter") != null) {
			p_voucherBO.setVcostcenter(request.getParameter("vcostcenter"));
		}
		if (request.getParameter("vamount") != null) {
			p_voucherBO.setVamount(request.getParameter("vamount"));
		}
		if (request.getParameter("vexpdate") != null && request.getParameter("vexpdate").length() == 10) {
			p_voucherBO.setVexpdate(request.getParameter("vexpdate"));
		}
		p_voucherBO.setDontExpire(Boolean.parseBoolean(request.getParameter("dontExpire") == null ? "false" : request.getParameter("dontExpire").equals("") ? "false" : request.getParameter("dontExpire").equals("on") ? "true" : "false"));
		if (p_voucherBO.isDontExpire() == true) {
			p_voucherBO.setVexpdate("12/31/9999");
		}

		p_voucherBO.setOnlyForSR(Boolean.parseBoolean(request.getParameter("onlyForSR") == null ? "false" : request.getParameter("onlyForSR").equals("") ? "false" : request.getParameter("onlyForSR").equals("on") ? "true" : "false"));

		if (request.getParameter("vdesc") != null) {
			p_voucherBO.setVdesc(request.getParameter("vdesc"));
		}
		if (request.getParameter("from_date") != null) {
			p_voucherBO.setFrom_date(request.getParameter("from_date"));
		}
		if (request.getParameter("to_date") != null) {
			p_voucherBO.setTo_date(request.getParameter("to_date"));
		}

		if (request.getParameter("billingAddress") != null) {
			p_voucherBO.setBillingAddress(request.getParameter("billingAddress"));
		} else {
			p_voucherBO.setBillingAddress(request.getParameter("cAddress"));
		}
		if (request.getParameter("noTip") != null) {
			p_voucherBO.setNoTip(1);
		} else {
			p_voucherBO.setNoTip(0);
		}
		if (request.getParameter("status") != null) {
			p_voucherBO.setActiveOrInactive(Integer.parseInt(request.getParameter("status")));
		} else {
			p_voucherBO.setActiveOrInactive(0);
		}

		String decline = request.getParameter("decline");

		String pay = request.getParameter("paytype");
		p_voucherBO.setInfokey(request.getParameter("infokey"));
		p_voucherBO.setDriverComments(request.getParameter("driverComments"));
		p_voucherBO.setOperatorComments(request.getParameter("operatorComments"));
		p_voucherBO.setStartAmount(request.getParameter("sAmount"));
		p_voucherBO.setRatePerMile(request.getParameter("ratePerMile"));
		p_voucherBO.setVcode(request.getParameter("vcode"));
		p_voucherBO.setVcontact(request.getParameter("vcontact"));
		p_voucherBO.setVdelay(request.getParameter("vdelay"));
		p_voucherBO.setCompanyName(request.getParameter("cName"));
		p_voucherBO.setCompanyAddress(request.getParameter("cAddress"));
		p_voucherBO.setContactName(request.getParameter("contactPerson"));
		p_voucherBO.setContactTitle(request.getParameter("contactTitle"));
		p_voucherBO.setTelephoneNumber(request.getParameter("telNumber"));
		p_voucherBO.setFaxNumber(request.getParameter("faxNumber"));
		p_voucherBO.setEmailAddress(request.getParameter("emailId"));
		p_voucherBO.setEmailStatus((request.getParameter("emailStatus") != null && !request.getParameter("emailStatus").equals("")) ? Integer.parseInt(request.getParameter("emailStatus")) : 0);
		p_voucherBO.setVoucherFormat(request.getParameter("voucherFormat"));
		p_voucherBO.setFixedAmount(request.getParameter("fixedAmount"));
		p_voucherBO.setAllocateOnlyTo(request.getParameter("allocateOnlyTo") == null ? "" : request.getParameter("allocateOnlyTo"));
		p_voucherBO.setNotAllocateTo(request.getParameter("notAllocateTo") == null ? "" : request.getParameter("notAllocateTo"));
		int t = Integer.parseInt((request.getParameter("ccProcessPercentage") == null || request.getParameter("ccProcessPercentage").equals("")) ? "0" : request.getParameter("ccProcessPercentage"));
		p_voucherBO.setCcProcessPercentage(t);
		try {
			if (request.getParameter("drProfile") != null && request.getParameter("drProfile") != "" && Integer.parseInt(request.getParameter("drProfile")) > 0) {
				String drProfile = "";
				for (int i = 0; i < Integer.parseInt(request.getParameter("drProfile")); i++) {
					if (request.getParameter("dchk" + i) != null) {
						drProfile = drProfile + request.getParameter("dchk" + i);
						System.out.println("drprofile" + drProfile);
					}
				}
				p_voucherBO.setSpecialFlags(drProfile);
			}
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}
		return p_voucherBO;
	}

	private String checkVoucherData(VoucherBO p_voucherBO) {
		StringBuffer errors = new StringBuffer();
		if (p_voucherBO.getVno() == "") {
			errors.append("You must enter Voucher no<br>");
		}
		// if(p_voucherBO.getVname() == "") {
		// errors.append("You must enter Rider name<br>");
		// }
		String ss = p_voucherBO.getVamount().split(" ")[0].equals("$") ? p_voucherBO.getVamount().split(" ")[1] : p_voucherBO.getVamount().split(" ")[0];

		if (!TDSValidation.isDoubleNumber(ss)) {
			errors.append("You must enter a valid amount<br>");
		}
		/*
		 * if(p_voucherBO.getVexpdate().equalsIgnoreCase("")) {
		 * errors.append("You must enter Expire Date<br>"); }
		 */

		if (p_voucherBO.getVdelay().equalsIgnoreCase("")) {
			errors.append("You must provide delay days <br>");
		}
		if (RegistrationDAO.checkVoucherNo(p_voucherBO.getVno()) != 0) {
			errors.append("Voucher No is Already Exist  <br>");
		}

		return errors.toString();
	}

	private CompanyMasterBO assignCompanyData(CompanyMasterBO p_companyBO, HttpServletRequest request) {
		if (request.getParameter("add1") != null) {
			p_companyBO.setAdd1(request.getParameter("add1").trim());
		}
		if (request.getParameter("add2") != null) {
			p_companyBO.setAdd2(request.getParameter("add2").trim());
		}
		if (request.getParameter("associateCode") != null) {
			p_companyBO.setAssccode(request.getParameter("associateCode").trim());
		}
		if (request.getParameter("bankacno") != null) {
			p_companyBO.setBankacno(request.getParameter("bankacno").trim());
		}
		if (request.getParameter("bankname") != null) {
			p_companyBO.setBankname(request.getParameter("bankname").trim());
		}
		if (request.getParameter("bankroutingno") != null) {
			p_companyBO.setBankroutingno(request.getParameter("bankroutingno").trim());
		}
		if (request.getParameter("cdesc") != null) {
			p_companyBO.setCdesc(request.getParameter("cdesc").trim());
		}
		if (request.getParameter("city") != null) {
			p_companyBO.setCity(request.getParameter("city").trim());
		}
		if (request.getParameter("cname") != null) {
			p_companyBO.setCname(request.getParameter("cname").trim());
		}
		if (request.getParameter("state") != null) {
			p_companyBO.setState(request.getParameter("state").trim());
		}
		if (request.getParameter("zip") != null) {
			p_companyBO.setZip(request.getParameter("zip").trim());
		}
		if (request.getParameter("phoneno") != null) {
			p_companyBO.setPhoneno(request.getParameter("phoneno"));
		}
		if (request.getParameter("dispatchStatus") != null) {
			p_companyBO.setDipatchStatus((request.getParameter("dispatchStatus").trim().toCharArray())[0]);
			// System.out.println("Data in Dispatch "+p_companyBO.getDipatchStatus());
		}

		if (request.getParameter("fileid") != null) {
			p_companyBO.setFileid(request.getParameter("fileid"));
		}

		if (request.getParameter("Email") != null) {
			p_companyBO.setEmail(request.getParameter("Email"));
		}

		return p_companyBO;
	}

	private String checkCompanyMaster(CompanyMasterBO p_companyBO) {
		StringBuffer errors = new StringBuffer();
		if (p_companyBO.getCname() == "") {
			errors.append("You must enter the company name<br>");
		}
		if (p_companyBO.getPhoneno() == "" || p_companyBO.getPhoneno().length() > 10) {
			errors.append("You must enter the phone no<br>");
		}
		if (p_companyBO.getCity() == "") {
			errors.append("You must enter the city <br>");
		}
		if (p_companyBO.getState() == "") {
			errors.append("You must enter the state<br>");
		}
		return errors.toString();
	}

	/**
	 * @param p_request
	 * @param p_behaviorBO
	 * @return DriverPenaltyBO
	 * @author vimal
	 * @see Description This method is used set the driver penalty information
	 *      into penalty Bean from the Jsp or Browser page.
	 */
	private DriverBehaviorBO setDriverBehavior(HttpServletRequest p_request, DriverBehaviorBO p_behaviorBO) {
		p_behaviorBO.setBehaviorDate(p_request.getParameter("bdate"));
		p_behaviorBO.setComment(p_request.getParameter("bcomment"));
		p_behaviorBO.setAmount(p_request.getParameter("bamount"));
		p_behaviorBO.setBehaviorType(p_request.getParameter("btype"));
		p_behaviorBO.setBehaviorKey(p_request.getParameter("behaviorNo"));
		p_behaviorBO.setDriverName(p_request.getParameter("driver"));
		return p_behaviorBO;
	}

	/**
	 * @param p_behaviorBO
	 * @return String Error message
	 * @author vimal
	 * @see Description This method is used to jsp all inputs are properly given
	 *      or not.
	 */
	private String checkDriverBehavior(DriverBehaviorBO p_behaviorBO) {
		StringBuffer m_errors = new StringBuffer();
		if (p_behaviorBO.getBehaviorDate() == "") {
			m_errors.append("<BR>You must choose the Penalty Date</BR>");
		}
		if (p_behaviorBO.getAmount() == "") {
			m_errors.append("<BR>You must enter the Amount of penalty</BR>");
		}
		if (p_behaviorBO.getComment() == "") {
			m_errors.append("<BR>You must add your comment for penalty</BR>");
		}
		return m_errors.toString();
	}

	private CabMaintenanceBO setCabMaintenance(HttpServletRequest p_request, CabMaintenanceBO p_cabMaintenanceBO) {
		p_cabMaintenanceBO.setServiceCode(p_request.getParameter("mcode"));
		p_cabMaintenanceBO.setServiceDate(p_request.getParameter("sdate"));
		p_cabMaintenanceBO.setAmount(p_request.getParameter("samount"));
		p_cabMaintenanceBO.setServiceDesc(p_request.getParameter("scomment"));
		p_cabMaintenanceBO.setMaintenanceKey(p_request.getParameter("maintenceNo"));
		return p_cabMaintenanceBO;
	}

	private String checkMaintenance(CabMaintenanceBO p_maintenanceBO) {
		StringBuffer m_errors = new StringBuffer();
		if (p_maintenanceBO.getServiceCode() == "") {
			m_errors.append("1");
		} else if (p_maintenanceBO.getAmount() == "") {
			m_errors.append("2");
		}
		return m_errors.toString();
	}

	private LostandFoundBO setLostandFound(HttpServletRequest p_request, LostandFoundBO p_LostandFoundBO) {
		String mode = (p_request.getParameter("mode") == null ? "" : p_request.getParameter("mode"));
		String modeSummary = (p_request.getParameter("modefromSummary") == null ? "" : p_request.getParameter("modefromSummary"));
		p_LostandFoundBO.setMode(mode);
		if (!mode.equals("") || !modeSummary.equals("")) {
			if (mode.equals("0") || modeSummary.equals("0")) {
				p_LostandFoundBO.setTripId(p_request.getParameter("tripid"));
				p_LostandFoundBO.setPhoneNumber(p_request.getParameter("phoneNumber"));
				p_LostandFoundBO.setDriverid(p_request.getParameter("driverId"));
				p_LostandFoundBO.setItemName(p_request.getParameter("itemName"));
				p_LostandFoundBO.setItemDesc(p_request.getParameter("itemDesc"));
				p_LostandFoundBO.setLfdate(p_request.getParameter("tripDate"));
				p_LostandFoundBO.setLftime(p_request.getParameter("time"));
				p_LostandFoundBO.setStatus(p_request.getParameter("status"));
			} else if (mode.equals("1") || modeSummary.equals("1")) {
				p_LostandFoundBO.setTripId(p_request.getParameter("tripId_DLF"));
				p_LostandFoundBO.setDriverid(p_request.getParameter("driverId_DLF"));
				p_LostandFoundBO.setRiderName(p_request.getParameter("riderName"));
				p_LostandFoundBO.setPhoneNumber(p_request.getParameter("riderPhone"));
				p_LostandFoundBO.setItemName(p_request.getParameter("itemName_DLF"));
				p_LostandFoundBO.setItemDesc(p_request.getParameter("itemDesc_DLF"));
				p_LostandFoundBO.setLfdate(p_request.getParameter("tripDate_DLF"));
				p_LostandFoundBO.setLftime(p_request.getParameter("time_DLF"));
				p_LostandFoundBO.setStatus(p_request.getParameter("status_DLF"));

			} else {
				p_LostandFoundBO.setTripId(p_request.getParameter("tripid_MLF"));
				p_LostandFoundBO.setItemName(p_request.getParameter("itemName_MLF"));
				p_LostandFoundBO.setItemDesc(p_request.getParameter("itemDesc_MLF"));
				p_LostandFoundBO.setLfdate(p_request.getParameter("tripDate_MLF"));
				p_LostandFoundBO.setLftime(p_request.getParameter("time_MLF"));
				p_LostandFoundBO.setStatus(p_request.getParameter("status_MLF"));

			}
			p_LostandFoundBO.setLfKey(p_request.getParameter("lostandfoundNo"));
		}
		return p_LostandFoundBO;

	}

	private String checkLostandFound(LostandFoundBO p_lostandFoundBO) {
		StringBuffer m_errors = new StringBuffer();
		if (p_lostandFoundBO.getTripId() == "") {
			m_errors.append("<BR>You must enter the trip id</BR>");
		}
		if (p_lostandFoundBO.getItemName() == "") {
			m_errors.append("<BR>You must enter the Item name</BR>");
		}
		if (p_lostandFoundBO.getLftime() == "") {
			m_errors.append("<BR>You must enter the found time</BR>");
		}
		return m_errors.toString();
	}

	public void clientAccount(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		Address addBean = new Address();
		String search = request.getParameter("button");
		CustomerProfile cprofileBean = new CustomerProfile();
		ArrayList<Address> addressHistory = new ArrayList<Address>();
		ArrayList<CustomerProfile> profileHistory = new ArrayList<CustomerProfile>();
		ArrayList<Address> addressHistoryDemo = new ArrayList<Address>();
		ArrayList<CustomerProfile> profileHistoryDemo = new ArrayList<CustomerProfile>();
		CustomerProfile addressMaster = new CustomerProfile();
		String s1 = request.getParameter("Number");
		String s2 = request.getParameter("Name");
		String s3 = request.getParameter("masterKey");
		if (request.getParameter("button") != null) {
			addBean.setAssociateCode(adminBO.getAssociateCode());
			cprofileBean.setNumber(s1);
			cprofileBean.setName(s2);
			cprofileBean.setMasterKey(s3);
			if (!cprofileBean.getNumber().equals("") || !cprofileBean.getName().equals("")) {
				profileHistory = AddressDAO.getCustomerProfile(cprofileBean.getNumber(), cprofileBean.getName(), adminBO, 0, "");
				if (profileHistory.size() != 0) {
					addressHistory = AddressDAO.getFromAndToAddress(profileHistory.get(0).getNumber(), 0, "", "", adminBO);
				} else {
					addressHistory = AddressDAO.getFromAndToAddress("", 0, "", "", adminBO);
				}
				if (profileHistory != null && profileHistory.size() == 1) {
					String masterKey = request.getParameter("masterKey");
					for (int i = 0; i < profileHistory.size(); i++) {
						masterKey = profileHistory.get(i).getMasterKey();
					}
					addressHistory = AddressDAO.getFromAndToAddress(profileHistory.get(0).getNumber(), 1, masterKey, "", adminBO);
					CreditCardBO cardDetails = CreditCardDAO.getCardDetails(masterKey, adminBO.getAssociateCode());
					request.setAttribute("cardDetails", cardDetails);
					request.setAttribute("AddressHistory", addressHistory);
					request.setAttribute("ProfileHistory", profileHistory);
					request.setAttribute("add", addBean);
					request.setAttribute("cprofile", cprofileBean);
					request.setAttribute("emptyPage", "No Records Available");
					request.setAttribute("screen", "/jsp/ClientAccountOne.jsp");
				} else if (profileHistory != null && (profileHistory.size() != 1 && profileHistory.size() != 0) && (addressHistory.size() >= 1 && addressHistory.size() != 0) && request.getParameter("event2") == null) {
					addressHistory = AddressDAO.getFromAndToAddress(profileHistory.get(0).getNumber(), 0, "", "", adminBO);
					request.setAttribute("AddressHistory", addressHistory);
					request.setAttribute("ProfileHistory", profileHistory);
					request.setAttribute("add", addBean);
					request.setAttribute("cprofile", cprofileBean);
					request.setAttribute("screen", "/jsp/clientAccountMany.jsp");

				} else if (profileHistory.size() == 0) {
					request.setAttribute("AddressHistory", addressHistory);
					request.setAttribute("ProfileHistory", profileHistory);
					request.setAttribute("add", addBean);
					request.setAttribute("cprofile", cprofileBean);
					request.setAttribute("screen", "/jsp/ClientAccount.jsp");
				}
			} else {
				request.setAttribute("AddressHistory", addressHistory);
				request.setAttribute("ProfileHistory", profileHistory);
				request.setAttribute("add", addBean);
				request.setAttribute("cprofile", cprofileBean);
				request.setAttribute("screen", "/jsp/ClientAccount.jsp");
			}
		}
		if (request.getParameter("Add") != null) {
			HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "Flags");
			if (companyFlags == null) {
				SystemUtils.reloadCompanyFlags(this, adminBO.getMasterAssociateCode());
				companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "Flags");
			}
			request.setAttribute("companyFlags", companyFlags);
			request.setAttribute("screen", "/CustomerService/NewClient.jsp");
		}
		if (request.getParameter("event2") != null && request.getParameter("event2").equalsIgnoreCase("edit")) {
			String masterKey = request.getParameter("masterKey");
			CreditCardBO cardDetails = CreditCardDAO.getCardDetails(masterKey, adminBO.getAssociateCode());
			request.setAttribute("cardDetails", cardDetails);
			profileHistory = AddressDAO.getCustomerProfile(cprofileBean.getNumber(), cprofileBean.getName(), adminBO, 1, masterKey);
			addressHistory = AddressDAO.getFromAndToAddress(profileHistory.get(0).getNumber(), 1, masterKey, "", adminBO);
			request.setAttribute("AddressHistory", addressHistory);
			request.setAttribute("ProfileHistory", profileHistory);
			request.setAttribute("add", addBean);
			request.setAttribute("cprofile", cprofileBean);
			request.setAttribute("emptyPage", "No Records Available");
			request.setAttribute("screen", "/jsp/ClientAccountOne.jsp");
		} else if (request.getParameter("event2") != null && request.getParameter("event2").equalsIgnoreCase("deleteMaster")) {
			String masterKey = request.getParameter("masterKey");
			AddressDAO.deleteUserAddressMaster(masterKey, adminBO);
			request.setAttribute("AddressHistory", addressHistory);
			request.setAttribute("ProfileHistory", profileHistory);
			request.setAttribute("add", addBean);
			request.setAttribute("cprofile", cprofileBean);
			request.setAttribute("screen", "/jsp/ClientAccount.jsp");
		} else if (request.getParameter("event2") != null && request.getParameter("event2").equalsIgnoreCase("updateMaster")) {
			String masterKey = request.getParameter("masterKey");
			HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "Flags");
			if (companyFlags == null) {
				SystemUtils.reloadCompanyFlags(this, adminBO.getMasterAssociateCode());
				companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode() + "Flags");
			}
			request.setAttribute("companyFlags", companyFlags);
			CustomerProfile customerProfile = AddressDAO.getUserAddressMaster(cprofileBean.getNumber(), cprofileBean.getName(), adminBO, masterKey, 1);
			request.setAttribute("UserAddressMaster", customerProfile);
			request.setAttribute("screen", "/jsp/updateUserAddressMaster.jsp");
		} else if (request.getParameter("event2") != null && request.getParameter("event2").equalsIgnoreCase("updateAddress")) {
			String name = request.getParameter("Name");
			String add1 = request.getParameter("add1");
			String add2 = request.getParameter("add2");
			String city = request.getParameter("city");
			String state = request.getParameter("state");
			String zip = request.getParameter("zip");
			String phone = request.getParameter("Number");
			String account = request.getParameter("account");
			String masterKey = request.getParameter("masterKey");
			String addressKey = request.getParameter("addressKey");
			addressHistoryDemo = AddressDAO.getFromAndToAddress(phone, 2, masterKey, addressKey, adminBO);
			Address addBO = AddressDAO.getUserAddress(phone, adminBO, addressKey, 1);
			request.setAttribute("masterKey", masterKey);
			request.setAttribute("addressHistoryDemo", addressHistoryDemo);
			request.setAttribute("UserAddress", addBO);
			request.setAttribute("screen", "/jsp/updateUserAddress.jsp");
		} else if (request.getParameter("event2") != null && request.getParameter("event2").equalsIgnoreCase("deleteAddress")) {
			String addressKey = request.getParameter("addressKey");
			String masterKey = request.getParameter("masterKey");
			String landmarkKey = (request.getParameter("landmarkKey") == null ? "" : request.getParameter("landmarkKey"));
			if (!landmarkKey.equals(""))
				AddressDAO.deleteUserAddress(addressKey, adminBO, landmarkKey, masterKey, 2);
			else
				AddressDAO.deleteUserAddress(addressKey, adminBO, "", "", 1);
			addressHistory = AddressDAO.getFromAndToAddress(cprofileBean.getNumber(), 1, masterKey, addressKey, adminBO);
			profileHistory = AddressDAO.getCustomerProfile("", "", adminBO, 1, masterKey);
			request.setAttribute("AddressHistory", addressHistory);
			request.setAttribute("ProfileHistory", profileHistory);
			request.setAttribute("add", addBean);
			request.setAttribute("cprofile", cprofileBean);
			request.setAttribute("emptyPage", "No Records Available");
			if (profileHistory.size() > 1) {
				request.setAttribute("screen", "/jsp/ClientAccountMany.jsp");
			} else {
				request.setAttribute("screen", "/jsp/ClientAccountOne.jsp");
			}
		} else if (request.getParameter("event2") != null && request.getParameter("event2").equalsIgnoreCase("addNew")) {
			OpenRequestBO openRequestBO = new OpenRequestBO();
			// System.out.println("About To Insert");
			openRequestBO = setNewClient(openRequestBO, request);
			AddressDAO.insertUserAddressMaster(openRequestBO, request.getParameter("operatorComm"), request.getParameter("driverComm"), adminBO.getMasterAssociateCode());
			request.setAttribute("AddressHistory", addressHistory);
			request.setAttribute("ProfileHistory", profileHistory);
			request.setAttribute("add", addBean);
			request.setAttribute("cprofile", cprofileBean);
			request.setAttribute("screen", "/jsp/ClientAccount.jsp");
		} else if (search == null && request.getParameter("event2") == null && request.getParameter("Add") == null) {
			request.setAttribute("AddressHistory", addressHistory);
			request.setAttribute("ProfileHistory", profileHistory);
			request.setAttribute("add", addBean);
			request.setAttribute("cprofile", cprofileBean);
			request.setAttribute("screen", "/jsp/ClientAccount.jsp");
		}
		if (request.getParameter("UpdateUserAddress") != null && request.getParameter("UpdateUserAddress").equalsIgnoreCase("Update")) {
			addBean.setName(request.getParameter("Name"));
			addBean.setAdd1(request.getParameter("sadd1"));
			addBean.setAdd2(request.getParameter("sadd2"));
			addBean.setCity(request.getParameter("scity"));
			addBean.setState(request.getParameter("sstate"));
			addBean.setZip(request.getParameter("szip"));
			addBean.setNumber(request.getParameter("Number"));
			// addBean.setAccount(request.getParameter("account"));
			addBean.setMasterKey(request.getParameter("masterKey"));
			addBean.setAddressKey(request.getParameter("addressKey"));
			addBean.setLatitude(request.getParameter("latitemp"));
			addBean.setLongitude(request.getParameter("longitemp"));
			addBean.setAddverify(Integer.parseInt(request.getParameter("addverified")));
			if (!addBean.getLatitude().equals("") && !addBean.getLongitude().equals("")) {
				ArrayList<ZoneTableBeanSP> allZonesForCompany = (ArrayList<ZoneTableBeanSP>) getServletConfig().getServletContext().getAttribute((adminBO.getAssociateCode() + "Zones"));
				String m_queueId = CheckZone.checkZone(allZonesForCompany, null, Double.parseDouble(addBean.getLatitude()), Double.parseDouble(addBean.getLongitude()));
				addBean.setQueueNo(m_queueId);
			} else {
				addBean.setQueueNo("");
			}
			int i = AddressDAO.updateUserAddress(addBean, adminBO);

			if (i == 1) {
				profileHistory = AddressDAO.getCustomerProfile(addBean.getNumber(), addBean.getName(), adminBO, 1, addBean.getMasterKey());
				addressHistory = AddressDAO.getFromAndToAddress(addBean.getNumber(), 1, addBean.getMasterKey(), addBean.getAddressKey(), adminBO);

				request.setAttribute("page", "UserAddress updated Successfully");
				request.setAttribute("AddressHistory", addressHistory);
				request.setAttribute("ProfileHistory", profileHistory);
				request.setAttribute("add", addBean);
				request.setAttribute("cprofile", cprofileBean);
				request.setAttribute("emptyPage", "No Records Available");
				if (profileHistory.size() == 1) {
					request.setAttribute("screen", "/jsp/ClientAccountOne.jsp");
				} else {
					request.setAttribute("screen", "/jsp/ClientAccountMany.jsp");

				}
			}
		}
		if (request.getParameter("UpdateAddMaster") != null && request.getParameter("UpdateAddMaster").equalsIgnoreCase("Update")) {
			// System.out.println("updateAddMaster "+request.getParameter("account"));
			cprofileBean.setName(request.getParameter("Name"));
			cprofileBean.setAdd1(request.getParameter("add1"));
			cprofileBean.setAdd2(request.getParameter("add2"));
			cprofileBean.setCity(request.getParameter("city"));
			cprofileBean.setState(request.getParameter("state"));
			cprofileBean.setZip(request.getParameter("zip"));
			cprofileBean.setNumber(request.getParameter("Number"));
			cprofileBean.setAccount(request.getParameter("account"));
			cprofileBean.setMasterKey(request.getParameter("masterKey"));
			cprofileBean.setComments(request.getParameter("dispcomments"));
			cprofileBean.setOperComments(request.getParameter("comments"));
			cprofileBean.setAccount(request.getParameter("account"));
			cprofileBean.seteMail(request.getParameter("mailId"));
			if (request.getParameter("premiumCust") != null) {
				cprofileBean.setPremiumcustomer("1");
			} else {
				cprofileBean.setPremiumcustomer("0");
			}
			try {
				if (request.getParameter("drprofileSize") != null && request.getParameter("drprofileSize") != "" && Integer.parseInt(request.getParameter("drprofileSize")) > 0) {
					ArrayList al_l = new ArrayList();
					String drProfile = "";
					for (int i = 0; i < Integer.parseInt(request.getParameter("drprofileSize")); i++) {
						if (request.getParameter("dchk" + i) != null) {
							al_l.add(request.getParameter("dchk" + i));
							drProfile = drProfile + request.getParameter("dchk" + i);
						}
					}
					cprofileBean.setAl_drList(al_l);
					cprofileBean.setDrProfile(drProfile);
				}
			} catch (NumberFormatException e) {
				// TODO: handle exception
			}
			try {
				if (request.getParameter("vprofileSize") != null && request.getParameter("vprofileSize") != "" && Integer.parseInt(request.getParameter("vprofileSize")) > 0) {
					ArrayList al_l = new ArrayList();
					String drProfile = "";
					for (int i = 0; i < Integer.parseInt(request.getParameter("vprofileSize")); i++) {
						if (request.getParameter("vchk" + i) != null) {
							al_l.add(request.getParameter("vchk" + i));
							drProfile = drProfile + request.getParameter("vchk" + i);
						}
					}
					cprofileBean.setAl_vecList(al_l);
					cprofileBean.setVecProfile(drProfile);
				}
			} catch (NumberFormatException e) {
				// TODO: handle exception
			}
			int i = AddressDAO.updateUserAddressMaster(cprofileBean, adminBO);
			if (i == 1) {
				profileHistory = AddressDAO.getCustomerProfile(cprofileBean.getNumber(), cprofileBean.getName(), adminBO, 1, cprofileBean.getMasterKey());

				addressHistory = AddressDAO.getFromAndToAddress(cprofileBean.getNumber(), 1, cprofileBean.getMasterKey(), "", adminBO);
				request.setAttribute("page", "UserAddressMaster updated Successfully");
				request.setAttribute("AddressHistory", addressHistory);
				request.setAttribute("ProfileHistory", profileHistory);
				request.setAttribute("add", addBean);
				request.setAttribute("cprofile", cprofileBean);
				request.setAttribute("emptyPage", "No Records Available");
				request.setAttribute("screen", "/jsp/ClientAccountOne.jsp");
			}
		} else if (request.getParameter("cancel") != null) {
			String masterKey = request.getParameter("masterKey");
			profileHistory = AddressDAO.getCustomerProfile("", "", adminBO, 1, masterKey);
			addressHistory = AddressDAO.getFromAndToAddress("", 1, masterKey, "", adminBO);
			addBean.setAssociateCode(((AdminRegistrationBO) request.getSession().getAttribute("user")).getAssociateCode());
			request.setAttribute("AddressHistory", addressHistory);
			request.setAttribute("ProfileHistory", profileHistory);
			request.setAttribute("add", addBean);
			request.setAttribute("cprofile", cprofileBean);
			request.setAttribute("emptyPage", "No Records Available");
			request.setAttribute("screen", "/jsp/ClientAccountOne.jsp");
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public OpenRequestBO setNewClient(OpenRequestBO openRequestBO, HttpServletRequest request) {
		openRequestBO.setPhone(request.getParameter("phoneNum").replace("-", ""));
		openRequestBO.setName(request.getParameter("customerName"));
		openRequestBO.setSadd1(request.getParameter("address1"));
		openRequestBO.setSadd2(request.getParameter("address2"));
		openRequestBO.setScity(request.getParameter("city"));
		openRequestBO.setSstate(request.getParameter("state"));
		openRequestBO.setSzip(request.getParameter("zip"));
		openRequestBO.setSlat(request.getParameter("latitude"));
		openRequestBO.setSlong(request.getParameter("longitude"));
		openRequestBO.setEmail(request.getParameter("mail"));
		openRequestBO.setAdvanceTime("-1");
		openRequestBO.setPaytype("Cash");
		openRequestBO.setAcct("");
		openRequestBO.setLandMarkKey("");
		try {
			if (request.getParameter("drprofileSize") != null && request.getParameter("drprofileSize") != "" && Integer.parseInt(request.getParameter("drprofileSize")) > 0) {
				ArrayList al_l = new ArrayList();
				String drProfile = "";
				for (int i = 0; i < Integer.parseInt(request.getParameter("drprofileSize")); i++) {
					if (request.getParameter("dchk" + i) != null) {
						al_l.add(request.getParameter("dchk" + i));
						drProfile = drProfile + request.getParameter("dchk" + i);
					}
				}
				openRequestBO.setAl_drList(al_l);
				openRequestBO.setDrProfile(drProfile);
			}
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}
		try {
			if (request.getParameter("vprofileSize") != null && request.getParameter("vprofileSize") != "" && Integer.parseInt(request.getParameter("vprofileSize")) > 0) {
				ArrayList al_l = new ArrayList();
				String drProfile = "";
				for (int i = 0; i < Integer.parseInt(request.getParameter("vprofileSize")); i++) {
					if (request.getParameter("vchk" + i) != null) {
						al_l.add(request.getParameter("vchk" + i));
						drProfile = drProfile + request.getParameter("vchk" + i);
					}
				}
				openRequestBO.setAl_vecList(al_l);
				openRequestBO.setVecProfile(drProfile);
			}
		} catch (NumberFormatException e) {
			// TODO: handle exception
		}
		return openRequestBO;
	}

	public void driverLocation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<DriverLocationHistoryBO> driverLocation = new ArrayList<DriverLocationHistoryBO>();
		if (request.getParameter("showmap") == null) {
			request.setAttribute("driverLocation", driverLocation);
			request.setAttribute("distance", 0.00000);
			request.setAttribute("fuelConsumption", 0.00000);
			request.setAttribute("carbonEmission", 0.00000);
			request.setAttribute("screen", "/jsp/driverLocation.jsp");
		} else {
			String driverId = request.getParameter("driverId");
			String lastUpdatedDate = request.getParameter("lastUpdatedDate");
			String fromDate = request.getParameter("fromDate") == null ? "" : request.getParameter("fromDate");
			String toDate = request.getParameter("toDate") == null ? "" : request.getParameter("toDate");
			String firstValue = "";
			String lastValue = "";
			int count = 0;
			int timeDifference;
			// if(tripId!=null && !tripId.equals("")){
			// ArrayList
			// valueList=DispatchDAO.getLogsTime(tripId,adminBO.getAssociateCode());
			// if(valueList!=null && valueList.size()>2){
			// driverLocation=RegistrationDAO.getDriverLocation(adminBO.getAssociateCode(),lastUpdatedDate,(String)
			// valueList.get(0),(String) valueList.get(1),(String)
			// valueList.get(2),2);
			// }
			if (driverId != null && !driverId.equals("")) {
				driverLocation = RegistrationDAO.getDriverLocation(adminBO.getAssociateCode(), lastUpdatedDate, driverId, fromDate, toDate, 1);
				for (int i = 0; i < driverLocation.size(); i++) {
					if (i != 0 && driverLocation.get(i).getLatitude() == driverLocation.get(i - 1).getLatitude()) {
						if (count == 0) {
							firstValue = driverLocation.get(i).getUpdatedTime();
							count++;
						}
						lastValue = driverLocation.get(i).getUpdatedTime();
						timeDifference = Integer.parseInt(lastValue) - Integer.parseInt(firstValue);
					}
				}
			}
			if (driverLocation.size() != 0) {
				int i = driverLocation.size() - 1;
				double lat1 = Double.parseDouble(driverLocation.get(0).getLatitude());
				double lon1 = Double.parseDouble(driverLocation.get(0).getLongitude());
				double lat2 = Double.parseDouble(driverLocation.get(i).getLatitude());
				double lon2 = Double.parseDouble(driverLocation.get(i).getLongitude());
				double distance = DistanceCalculation.distance(lat1, lat2, lon1, lon2);
				double fuelConsumption = distance / 20;
				double carbonEmission = fuelConsumption * 112.81;
				request.setAttribute("distance", distance);
				request.setAttribute("fuelConsumption", fuelConsumption);
				request.setAttribute("carbonEmission", carbonEmission);
				request.setAttribute("driverLocation", driverLocation);
				request.setAttribute("screen", "/jsp/driverLocation.jsp");
			} else {
				request.setAttribute("distance", 0.00000);
				request.setAttribute("fuelConsumption", 0.00000);
				request.setAttribute("carbonEmission", 0.00000);
				request.setAttribute("driverLocation", driverLocation);
				request.setAttribute("error", "No Data Available For Your Search");
				request.setAttribute("screen", "/jsp/driverLocation.jsp");
			}
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void loginAttempts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<LoginAttempts> loginAttempts = new ArrayList<LoginAttempts>();
		if (request.getParameter("submit") != null) {
			String driverId = request.getParameter("driverId");
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate") == null ? "" : request.getParameter("toDate");
			String fromDateFormatted = TDSValidation.getTimeStampDateFormat(fromDate);
			String toDateFormatted = TDSValidation.getTimeStampDateFormat(toDate);
			loginAttempts = RegistrationDAO.loginAttempts(adminBO, adminBO.getAssociateCode(), driverId, fromDateFormatted, toDateFormatted);
		}
		request.setAttribute("loginAttempts", loginAttempts);
		request.setAttribute("screen", "/jsp/LoginAttempts.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}

	public void getDriverDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String forwardURL = TDSConstants.getMainNewJSP;
		cat.info("In Driver Summary Method");
		String screen = TDSConstants.sumDriverRegister;
		StringBuffer outData = new StringBuffer("");
		String first_name = request.getParameter("first_name") == null ? "" : request.getParameter("first_name");
		String last_name = request.getParameter("last_name") == null ? "" : request.getParameter("last_name");
		String driver_id = request.getParameter("driver_id") == null ? "" : request.getParameter("driver_id");
		String cab_no = request.getParameter("cab_no") == null ? "" : request.getParameter("cab_no");
		String status = request.getParameter("status") == null ? "" : request.getParameter("status");
		System.out.println("status" + status);
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		ArrayList<DriverRegistrationBO> al_list;
		JSONArray array = new JSONArray();
		DriverRegistrationBO driverBO = new DriverRegistrationBO();
		al_list = RegistrationDAO.getDriverDetailSearch(driverBO, adminBO.getAssociateCode(), first_name, last_name, driver_id, cab_no, status);
		if (al_list != null && al_list.size() > 0) {
			for (int i = 0; i < al_list.size(); i++) {
				JSONObject address = new JSONObject();
				try {
					address.put("SNO", al_list.get(i).getSno() == null ? "" : al_list.get(i).getSno());
					address.put("USERID", al_list.get(i).getUid() == null ? "" : al_list.get(i).getUid());
					address.put("ADD1", al_list.get(i).getAdd1() == null ? "" : al_list.get(i).getAdd1());
					address.put("ADD2", al_list.get(i).getAdd2() == null ? "" : al_list.get(i).getAdd2());
					address.put("CITY", al_list.get(i).getCity() == null ? "" : al_list.get(i).getCity());
					address.put("STATE", al_list.get(i).getState() == null ? "" : al_list.get(i).getState());
					address.put("ZIPCODE", al_list.get(i).getZip() == null ? "" : al_list.get(i).getZip());
					address.put("CARMAKE", al_list.get(i).getCmake() == null ? "" : al_list.get(i).getCmake());
					address.put("CARMODEL", al_list.get(i).getCmodel() == null ? "" : al_list.get(i).getCmodel());
					address.put("CARYEAR", al_list.get(i).getCyear() == null ? "" : al_list.get(i).getCyear());
					address.put("FNAME", al_list.get(i).getFname() == null ? "" : al_list.get(i).getFname());
					address.put("LNAME", al_list.get(i).getLname() == null ? "" : al_list.get(i).getLname());
					address.put("ROUTE", al_list.get(i).getRoute() == null ? "" : al_list.get(i).getRoute());
					address.put("PHONE", al_list.get(i).getPhone() == null ? "" : al_list.get(i).getPhone());
					address.put("CABNO", al_list.get(i).getCabno() == null ? "" : al_list.get(i).getCabno());
					address.put("PASSNO", al_list.get(i).getPassno() == null ? "" : al_list.get(i).getPassno());
					address.put("STATUS", al_list.get(i).getStatus() == null ? "" : al_list.get(i).getStatus());
					array.put(address);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		outData.append(array);
		response.setContentType("application/json");
		response.getWriter().write(outData.toString());
	}
	
	
	public void driverLocationGPS(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("gps");
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<DriverLocationHistoryBO> driverLocation = new ArrayList<DriverLocationHistoryBO>();
		ArrayList<DriverLocationHistoryBO> formatedDriverLocation = new ArrayList<DriverLocationHistoryBO>();
	
		String driverId = request.getParameter("driverId");
		String date = request.getParameter("Date");
		String fromHr = request.getParameter("stHr") == null ? "" : request.getParameter("stHr");
		String toHr = request.getParameter("etHr") == null ? "" : request.getParameter("etHr");
		
		String fromDateFormatted = TDSValidation.getTimeStampDateFormat(date);
		String fromTimeFormatted = formatTime(fromHr,1);
		String toTimeFormatted = formatTime(toHr,2);
		
		/*System.out.println("Date:"+fromDateFormatted);
		System.out.println("fromHr:"+fromTimeFormatted);
		System.out.println("toHr:"+toTimeFormatted);
		System.out.println("DriverID:"+driverId);*/
		
		String fromDate = fromDateFormatted+" "+fromTimeFormatted;
		String toDate = fromDateFormatted+" "+toTimeFormatted;
		String firstValue = "";
		String lastValue = "";
		int count = 0;
		int timeDifference;
		
		driverLocation = RegistrationDAO.getDriverLocationGPS(adminBO,driverId, fromDate, toDate);
		if (request.getParameter("showmap") == null) {
			request.setAttribute("driverLocation", driverLocation);
			request.setAttribute("driverId", "");
			request.setAttribute("screen", "/jsp/driverLocationView.jsp");
		} else{
			
		    //System.out.println("comes In");
//			String driverId = request.getParameter("driverId");
//			String date = request.getParameter("Date");
//			String fromHr = request.getParameter("stHr") == null ? "" : request.getParameter("stHr");
//			String toHr = request.getParameter("etHr") == null ? "" : request.getParameter("etHr");
//			
//			String fromDateFormatted = TDSValidation.getTimeStampDateFormat(date);
//			String fromTimeFormatted = formatTime(fromHr,1);
//			String toTimeFormatted = formatTime(toHr,2);
//			
//			/*System.out.println("Date:"+fromDateFormatted);
//			System.out.println("fromHr:"+fromTimeFormatted);
//			System.out.println("toHr:"+toTimeFormatted);
//			System.out.println("DriverID:"+driverId);*/
//			
//			String fromDate = fromDateFormatted+" "+fromTimeFormatted;
//			String toDate = fromDateFormatted+" "+toTimeFormatted;
//			String firstValue = "";
//			String lastValue = "";
//			int count = 0;
//			int timeDifference;
			
			double total_distance = 0.0;
			if (driverId != null && !driverId.equals("")) {
//				driverLocation = RegistrationDAO.getDriverLocationGPS(adminBO,driverId, fromDate, toDate);
				
				for (int i = 0; i < driverLocation.size(); i++) {
					DriverLocationHistoryBO dlBean = new DriverLocationHistoryBO();
					//System.out.println("I---"+i+"--------->"+driverLocation.get(i).getStatus());
					if(i==0){
						//System.out.println("returfirst or last");
						//System.out.println("lat  = "+driverLocation.get(i).getLatitude()+" = "+i);
						dlBean.setLatitude(driverLocation.get(i).getLatitude());
						dlBean.setLongitude(driverLocation.get(i).getLongitude());
						dlBean.setStatus(driverLocation.get(i).getStatus());
						dlBean.setUpdatedTime(driverLocation.get(i).getUpdatedTime());
						dlBean.setDriverid(driverLocation.get(i).getDriverid());
						dlBean.setsNo(driverLocation.get(i).getsNo());
						dlBean.setTimeDifference(driverLocation.get(i).getTimeDifference());
						dlBean.setDistance(total_distance);
						
						formatedDriverLocation.add(dlBean);
						continue;
					}
					if(driverLocation.get(i).getLatitude().equalsIgnoreCase("0.000000")){
						continue;
						/*if(driverLocation.get(i).getStatus().equalsIgnoreCase("0")){
							//System.out.println("login");
							dlBean.setLatitude(driverLocation.get(i).getLatitude());
							dlBean.setLongitude(driverLocation.get(i).getLongitude());
							dlBean.setStatus(driverLocation.get(i).getStatus());
							dlBean.setUpdatedTime(driverLocation.get(i).getUpdatedTime());
							dlBean.setDriverid(driverLocation.get(i).getDriverid());
							dlBean.setsNo(driverLocation.get(i).getsNo());
							dlBean.setTimeDifference(driverLocation.get(i).getTimeDifference());
							formatedDriverLocation.add(dlBean);
						}else if(driverLocation.get(i).getStatus().equalsIgnoreCase("1")){
							dlBean.setLatitude(driverLocation.get(i-1).getLatitude());
							dlBean.setLongitude(driverLocation.get(i-1).getLongitude());
							dlBean.setStatus(driverLocation.get(i).getStatus());
							dlBean.setUpdatedTime(driverLocation.get(i).getUpdatedTime());
							dlBean.setDriverid(driverLocation.get(i).getDriverid());
							dlBean.setsNo(driverLocation.get(i).getsNo());
							dlBean.setTimeDifference(driverLocation.get(i).getTimeDifference());
						}else{
							//System.out.println("0.00000 not added");
							continue;
						}*/
					}else{
						//if(driverLocation.get(i).getStatus().equalsIgnoreCase("8")){
							if(driverLocation.get(i-1).getLatitude().equals(driverLocation.get(i).getLatitude()) && driverLocation.get(i-1).getLongitude().equals(driverLocation.get(i).getLongitude())){
								//System.out.println("lat, lon equeal return");
								continue;
							}
							if(driverLocation.get(i-1).getLatitude().equalsIgnoreCase("0.000000") && driverLocation.get(i-1).getLongitude().equalsIgnoreCase("0.000000")){
								//System.out.println("prev lat lon 0.000000");
								dlBean.setLatitude(driverLocation.get(i).getLatitude());
								dlBean.setLongitude(driverLocation.get(i).getLongitude());
								dlBean.setStatus(driverLocation.get(i).getStatus());
								dlBean.setUpdatedTime(driverLocation.get(i).getUpdatedTime());
								dlBean.setDriverid(driverLocation.get(i).getDriverid());
								dlBean.setsNo(driverLocation.get(i).getsNo());
								dlBean.setTimeDifference(driverLocation.get(i).getTimeDifference());
								dlBean.setDistance(total_distance);
								
								formatedDriverLocation.add(dlBean);
								continue;
							}
							double lat1 = Double.parseDouble(driverLocation.get(i-1).getLatitude());
							double lon1 = Double.parseDouble(driverLocation.get(i-1).getLongitude());
							double lat2 = Double.parseDouble(driverLocation.get(i).getLatitude());
							double lon2 = Double.parseDouble(driverLocation.get(i).getLongitude());
							double distance = DistanceCalculation.distance(lat1, lat2, lon1, lon2);
							//System.out.println("distance:"+distance);
							total_distance = total_distance +distance;
							total_distance = Math.round( total_distance * 100.0 ) / 100.0;
							//total_distance = rounfoff(total_distance);
							/*if(distance>2){
								System.out.println("dist >2 added");*/
								dlBean.setDistance(total_distance);
								dlBean.setLatitude(driverLocation.get(i).getLatitude());
								dlBean.setLongitude(driverLocation.get(i).getLongitude());
								dlBean.setStatus(driverLocation.get(i).getStatus());
								dlBean.setUpdatedTime(driverLocation.get(i).getUpdatedTime());
								dlBean.setDriverid(driverLocation.get(i).getDriverid());
								dlBean.setsNo(driverLocation.get(i).getsNo());
								dlBean.setTimeDifference(driverLocation.get(i).getTimeDifference());
								formatedDriverLocation.add(dlBean);
							//}
						/*}else{
							//System.out.println("not 8 status");
							dlBean.setLatitude(driverLocation.get(i).getLatitude());
							dlBean.setLongitude(driverLocation.get(i).getLongitude());
							dlBean.setStatus(driverLocation.get(i).getStatus());
							dlBean.setUpdatedTime(driverLocation.get(i).getUpdatedTime());
							dlBean.setDriverid(driverLocation.get(i).getDriverid());
							dlBean.setsNo(driverLocation.get(i).getsNo());
							dlBean.setTimeDifference(driverLocation.get(i).getTimeDifference());
							formatedDriverLocation.add(dlBean);
						}*/
					}
				}
			}
			//System.out.println("drierlocation:"+driverLocation.size());
			//System.out.println("formated location:"+formatedDriverLocation.size());
			System.out.println("total dist: "+total_distance+" for drivre : "+driverId);
			if (formatedDriverLocation!=null && formatedDriverLocation.size() > 0) {
				//Deciding output format based on showmap field
				//IF show map is Yes then it sends json object 
				// else its assumed that its a download function
				if(request.getParameter("showmap").equals("yes")){

					JSONArray array = new JSONArray();
					for(int i=0;i<formatedDriverLocation.size();i++){
						JSONObject jobj = new JSONObject();
						try {
							
							jobj.put("A", formatedDriverLocation.get(i).getLatitude());
							jobj.put("B", formatedDriverLocation.get(i).getLongitude());
							jobj.put("C", formatedDriverLocation.get(i).getStatus());
							jobj.put("D", formatedDriverLocation.get(i).getUpdatedTime());
							jobj.put("E", formatedDriverLocation.get(i).getDriverid());
							jobj.put("F", formatedDriverLocation.get(i).getTimeDifference());
							jobj.put("G", formatedDriverLocation.get(i).getsNo());
							jobj.put("H", formatedDriverLocation.get(i).getDistance());
							
							array.put(jobj);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					response.getWriter().write(array.toString());
				} else {
					response.setContentType("text/csv");
					response.setHeader("Content-Disposition", "attachment; filename="+driverId+"_On_"+date+".csv");
					response.getWriter().println("Latitude,Longitude,Status,UpdatedTime,Driverid,TimeDifference,sNo");
					for(int i=0;i<formatedDriverLocation.size();i++){
						response.getWriter().print(formatedDriverLocation.get(i).getLatitude()+",");
						response.getWriter().print(formatedDriverLocation.get(i).getLongitude()+",");
						response.getWriter().print(formatedDriverLocation.get(i).getStatus()+",");
						response.getWriter().print(formatedDriverLocation.get(i).getUpdatedTime()+",");
						response.getWriter().print(formatedDriverLocation.get(i).getDriverid()+",");
						response.getWriter().print(formatedDriverLocation.get(i).getTimeDifference()+",");
						response.getWriter().println(formatedDriverLocation.get(i).getsNo());
				}
					StringBuffer outData = new StringBuffer();
					outData.append("driverLati,Passenger Name,driverLongi,status,lastUpdate,driverId,time,tripId\n");
					for (int i = 0; i < formatedDriverLocation.size(); i++) {
						outData.append(formatedDriverLocation.get(i).getLatitude()+ "," +formatedDriverLocation.get(i).getLongitude() + "," + formatedDriverLocation.get(i).getStatus() + "," + formatedDriverLocation.get(i).getUpdatedTime() + ", " +formatedDriverLocation.get(i).getDriverid() +"\n");
					}
					//InputStream in = new ByteArrayInputStream(outData.toString().getBytes("UTF-8"));
					//ServletOutputStream out = response.getOutputStream();
					//byte[] outputByte = new byte[4096];
					//while (in.read(outputByte, 0, 4096) != -1) {
					//	out.write(outputByte, 0, 4096);
					//}
					//in.close();
					//out.flush();
					//out.close();
					
					}
				
				/*request.setAttribute("driverLocation", formatedDriverLocation);
				request.setAttribute("screen", "/jsp/driverLocationView.jsp");*/
			} else {
				response.getWriter().write("Nothing");
				
				/*request.setAttribute("driverLocation", formatedDriverLocation);
				request.setAttribute("error", "No Data Available For Your Search");
				request.setAttribute("screen", "/jsp/driverLocationView.jsp");*/
			}
			return;
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}
	
	public String formatTime(String time, int type){
		String returnTime = "";
		if(type==1){
			returnTime = time.substring(0, 2)+":"+time.substring(2)+":00";
		}else{
			returnTime = time.substring(0, 2)+":"+time.substring(2)+":59";
		}
		
		return returnTime;
	}
	
	public void driverReport(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SystemUnavailableException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String search = request.getParameter("search") != null ? request.getParameter("search") : "";
		
		if(search!=null && search.equalsIgnoreCase("search")){
			String driverId = request.getParameter("driverId") != null ? request.getParameter("driverId") : "";
			String fDate = request.getParameter("stDate") != null ? request.getParameter("stDate") : "";
			String tDate = request.getParameter("edDate") != null ? request.getParameter("edDate") : "";
			ArrayList<QueueBean> dList = new ArrayList<QueueBean>();
			
			/*if(driverId.equals("")){
				request.setAttribute("fDate", fDate);
				request.setAttribute("tDate", tDate);
				request.setAttribute("driverId", driverId);
				request.setAttribute("al_list", dList);
				request.setAttribute("screen", "/jsp/driverZoneActivity.jsp");
				RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
				reqDispatcher.forward(request, response);
				return;
			}*/
			
			String fromDateFormatted = TDSValidation.getTimeStampDateFormat(fDate);
			String toDateFormatted = TDSValidation.getTimeStampDateFormat(tDate);
			dList = RegistrationDAO.getDriverReport(adminBO, driverId, fromDateFormatted, toDateFormatted);
			
			if(dList == null || dList.size()<1){
				request.setAttribute("fDate", fDate);
				request.setAttribute("tDate", tDate);
				request.setAttribute("driverId", driverId);
				request.setAttribute("al_list", dList);
				request.setAttribute("screen", "/jsp/driverReports.jsp");
				RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
				reqDispatcher.forward(request, response);
				return;
			}
			
			long totalTime = 0;
			for(int i=0; i<dList.size(); i++){
				if(dList.size()==1){
					dList.get(0).setDq_Reason(dList.get(0).getStartTimeStamp());
					dList.get(0).setFlg(1);
					continue;
				}
				
				if(i==0){
					totalTime = Long.parseLong(dList.get(i).getQU_NAME());
					continue;
				}
				
				QueueBean current = dList.get(i);
				QueueBean previous = dList.get(i-1);
				if(previous.getLastLoginTime().equals(current.getLastLoginTime())){
					totalTime = totalTime + Long.parseLong(current.getQU_NAME());
					//System.out.println("If total:"+totalTime+"--wait:"+current.getQU_NAME());
				}else{
					String[] dateFormat = splitToComponentTimes(totalTime);
					dList.get(i-1).setDq_Reason(dateFormat[0]+" hour : "+dateFormat[1]+" minute : "+dateFormat[2]+" seconds");
					dList.get(i-1).setFlg(1);
					//System.out.println("Else total:"+totalTime+"--wait:"+current.getQU_NAME());
					totalTime = Long.parseLong(dList.get(i).getQU_NAME());
				}
				
				if(i==(dList.size()-1)){
					String[] dateFormat = splitToComponentTimes(totalTime);
					dList.get(i).setDq_Reason(dateFormat[0]+" hour : "+dateFormat[1]+" minute : "+dateFormat[2]+" seconds");
					dList.get(i).setFlg(1);
				}
			}
			
			request.setAttribute("fDate", fDate);
			request.setAttribute("tDate", tDate);
			request.setAttribute("driverId", driverId);
			request.setAttribute("al_list", dList);
			request.setAttribute("screen", "/jsp/driverReports.jsp");
			RequestDispatcher reqDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			reqDispatcher.forward(request, response);
		}
		
	}
	
	public static String[] splitToComponentTimes(long longVal) {
	    int hours = (int) longVal / 3600;
	    int remainder = (int) longVal - hours * 3600;
	    int mins = remainder / 60;
	    remainder = remainder - mins * 60;
	    int secs = remainder;

	    String hr = ""+hours;
	    String mn = ""+mins;
	    String sc = ""+secs;
	    
	    //String[] ints = {(hr.length()==1)?("0"+hr):hr , (mn.length()==1)?("0"+mn):mn , (sc.length()==1)?("0"+sc):sc};
	    
	    String[] ints = {hr , mn , sc};
	    return ints;
	}

}