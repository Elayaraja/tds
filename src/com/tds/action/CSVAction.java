package com.tds.action;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.cmp.bean.PenalityBean;
import com.tds.controller.TDSController;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.common.util.TDSValidation;

public class CSVAction  extends HttpServlet 
{
	private final Category cat = TDSController.cat;
	  	
		@Override
		public void doGet(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
			doProcess(p_request, p_response);
		}
		
		/* (non-Javadoc)
		 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
		 */
		@Override
		public void doPost(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
			doProcess(p_request, p_response);
		}
		
		public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException 
		{
			String m_eventParam = "";
			
			if(p_request.getParameter(TDSConstants.eventParam) != null) {
				m_eventParam = p_request.getParameter(TDSConstants.eventParam);
			}
			cat.info("Event Param "+m_eventParam);

			HttpSession m_session = p_request.getSession();
			 
			if(m_session.getAttribute("user") != null) {
				
				if(m_eventParam.equalsIgnoreCase("csvUploadStart")) {			 
					csvUploadStart(p_request,p_response);
				}
			} else {
				sentErrorReport(p_request,p_response);
			}
		}
		
		public void csvUploadStart(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
			
			//File filePath = null;
			//String contextPath = getServletConfig().getServletContext().getRealPath("");
			//filePath = new File(AERPPropertyProviderIPAddress.getValue("empAttachments",contextPath) +"/EmployeeAttachments/"+empKey+"/"+subDirectory);
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			if(p_request.getParameter("event1")==null){
				p_request.setAttribute("screen", "/CSVUpload.jsp");
				//RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			 	requestDispatcher.forward(p_request, p_response);
			} else {
			
				String fname = p_request.getParameter("fname")==null?"":p_request.getParameter("fname");
				String include = p_request.getParameter("include")==null?"":p_request.getParameter("include");
				int total=0,err=0;
				String credit = "0", charge="0";
				//System.out.println("File Name:"+ fname);
				//System.out.println("include:"+ include);
				String path = TDSProperties.getValue("FolderStore");				
				ArrayList al_list = new ArrayList();
				ArrayList al_bean =  new ArrayList();
				try { 
						//csv file containing data
						String strFile = path+"/"+fname;
					    //create BufferedReader to read csv file
						BufferedReader br = new BufferedReader( new FileReader(strFile));
						String strLine = "";
						StringTokenizer st = null;
						int lineNumber = 0; //tokenNumber = 0;
						boolean flg = true;
						
						//read comma separated file line by line
						while( (strLine = br.readLine()) != null){
						  	lineNumber++; 			//break comma separated line using ",
						 	if(include.equals("1") && lineNumber == 1) {
						 		flg = false;
						 	} else {
						 		flg = true;
						 	}
						 	if(flg)
						 	{
						 		/*int j = 1;
								st = new StringTokenizer(strLine, ",");								
								while(st.hasMoreTokens() && j<6){
									//display csv values39.					
									tokenNumber++;		
									j++;
									String token = st.nextToken().toString().replace('\"', ' ');
									//System.out.println("Line # " + lineNumber+", Token # " + tokenNumber+ ", Token :"+token);
									al_list.add(token.trim());
								}*/
						 		
						 		//Added by Saravanan - Code replacement for above String Tokenizer
								if(!strLine.equals("")){
									String[] result = strLine.split(",");
									for(String token : result){										
										al_list.add(token.trim());									
									}
								}
						 	}
						 	
							//reset token number tokenNumber = 0;
						}
						PenalityBean penalityBean = null;
						for(int i=0;i< al_list.size();i=i+5)
						{
						 	penalityBean = new PenalityBean();
							
						  	penalityBean.setB_driver_id(al_list.get(i).toString().trim());
							penalityBean.setB_ass_code(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getAssociateCode().trim());
						 	penalityBean.setB_amount(al_list.get(i+1).toString().trim());	
						 	//Date and Month validation added by Saravanan
						  	penalityBean.setB_p_date(TDSValidation.getValidDate(al_list.get(i+2).toString().trim().replaceAll(" ", "")));
						  	penalityBean.setBtype(al_list.get(i+3).toString().trim());
							penalityBean.setB_desc(al_list.get(i+4).toString().trim());
							al_bean.add(penalityBean);
							
						}
						
						HashMap hm_container  = validatePenalityChargeBean(al_bean, ((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getAssociateCode());											 	
						ArrayList al_error = (ArrayList)hm_container.get("error");
						ArrayList al_valid = (ArrayList)hm_container.get("valid") ==null?new ArrayList():(ArrayList)hm_container.get("valid");
						String error = (String)hm_container.get("errBuff");
						
						total = al_bean.size();
						err = al_error.size();	
						charge = hm_container.get("charge")==null?"0":hm_container.get("charge").toString();
						credit = hm_container.get("credit")==null?"0":hm_container.get("credit").toString();
						
						/*if(al_error!=null && !al_error.isEmpty())
						{							
							RegistrationDAO.insertDriverPenality(al_valid, "insert", ((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getAssociateCode());
							p_request.setAttribute("al_driver", UtilityDAO.getDriverList(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getAssociateCode(),1));
							p_request.setAttribute("al_error", al_error);
							p_request.setAttribute("error", error);
							p_request.setAttribute("total", ""+total);
							p_request.setAttribute("err", ""+err);
							p_request.setAttribute("charge", charge);
							p_request.setAttribute("credit", credit);
							p_request.setAttribute("screen", "/CSV/CSVError.jsp");
							
							//RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/CSV/CSVError.jsp");
						 	requestDispatcher.forward(p_request, p_response);
						 	
						} else {
							RegistrationDAO.insertDriverPenality(al_valid, "insert", ((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getAssociateCode());
							p_response.sendRedirect("/TDS/control?action=registration&event=driverPenalityMaster&total="+total+"&err="+err+"&credit="+credit+"&charge="+charge);
						}*/
						
						//Added by Saravanan capturing SQL Error Messages
						HashMap hm_sql_container  =  RegistrationDAO.insertDriverPenality(al_valid, "insert", ((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getAssociateCode());
						ArrayList al_sql_error = (ArrayList)hm_sql_container.get("al_sql_error");
						String sql_errBuff = (String)hm_sql_container.get("sql_errBuff");
						
						RegistrationDAO.insertDriverPenalityError(al_error, "insert" , ((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getAssociateCode());
						p_request.setAttribute("al_driver", UtilityDAO.getDriverList(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getAssociateCode(),1));
						
						//checking for error on data validation, returns list of penalityBean error
						if(al_error.size()>0)
						p_request.setAttribute("al_error", al_error);
						//checking for SQL Error, returns list of penalityBean error
						else if(al_sql_error.size()>0)
						p_request.setAttribute("al_error", al_sql_error);						
						
						//checking for error on data validation, returns error message
						if(al_error.size()>0)
						p_request.setAttribute("error", error);
						//checking for SQL Error, returns error message
						else if(al_sql_error.size()>0){							
							err = al_sql_error.size();
							p_request.setAttribute("error", sql_errBuff);							
						}						
											
						p_request.setAttribute("total", ""+total);
						p_request.setAttribute("err", ""+err);
						p_request.setAttribute("charge", charge);
						p_request.setAttribute("credit", credit);
						p_request.setAttribute("screen", "/CSV/CSVError.jsp");
						
						//RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/CSV/CSVError.jsp");
					 	requestDispatcher.forward(p_request, p_response);
						
					} catch(Exception e){
						e.printStackTrace();
						//System.out.println("Exception while reading csv file: " + e.getMessage());	
					}
							
			}	      

			
		}
		
		public HashMap validatePenalityChargeBean(ArrayList al_bean,String assocode) throws ServletException,IOException {
			
			HashMap hm_container = new HashMap();			
			ArrayList al_error = new ArrayList();
			ArrayList al_valid = new ArrayList();
			StringBuffer error = new StringBuffer();
			double charge=0,credit=0;
			
	  		for(int i=0,j=1;i<al_bean.size();i++)
			{
				boolean flg = true;			 	
				PenalityBean bean = (PenalityBean)al_bean.get(i);
				String Driverid= bean.getB_driver_id();
			 	if(bean.getB_desc().equals(""))
				{
			 		flg=false;
					error.append("<li>Pls Provide Description At Row "+(j)+" </li>");
				} else if(!UtilityDAO.Assocodedriveridmatch(Driverid,assocode))
				{
					flg=false;
					error.append("<li>Driver does not belong to Company<br> Pls Provide Correct Driver ID At Row "+(i+1)+" </li>");
				}
				if(Driverid.equals(""))
				{
					flg=false;
					error.append(" <li>Pls Provide Correct DriverID At Row "+(j)+" </li>");
				}
				
				if(bean.getBtype().equals("") && (!bean.getBtype().equals("1") && !bean.getBtype().equals("2")))
				{
					flg = false; 
					error.append("<li>Invalid Credit/Charge type At Row "+(j)+"</li>");
				}
				
			 	if(bean.getB_amount().equals("") && !TDSValidation.isDoubleNumber(bean.getB_amount())) {
			 		flg=false;
					error.append("<li> Amount is Invalid At Row "+(j)+" </li>");
				}
			 	
			 	/*//System.out.println("Date Length:" +bean.getB_p_date().length()+":"+bean.getB_p_date());
			 	if(bean.getB_p_date().length() != 10) {
			 		flg=false;
					error.append(" <li>Pls Provide Valid Effective Date At Row "+(j)+" </li>"); 
				}*/
			 	
			 	//Added by Saravanan - Code replacement for Date validation
			 	boolean datevalidation = TDSValidation.validateDate(bean.getB_p_date(), "MM/dd/yyyy");
			 	if(!datevalidation){
			 		flg=false;
					error.append(" <li>Pls Provide Valid Effective Date At Row, Ensure date Format as MM/DD/YYYY"+(j)+" </li>"); 
			 	}
			 	
			 	boolean allowDate = TDSValidation.allowDate(bean.getB_p_date(), false, "MM/dd/yyyy");
			 	if(!allowDate){
			 		flg=false;
					error.append(" <li>Pls Provide Future Date At Row"+(j)+" </li>"); 
			 	}		 	
			 	
					
			 	if(flg)
			 	{
			 		if(bean.getBtype().equals("1"))
			 		{
			 			if(TDSValidation.isDoubleNumber(bean.getB_amount())) {
			 			   credit = credit + Double.parseDouble(bean.getB_amount())	;
			 			}
			 		} else {
			 			if(TDSValidation.isDoubleNumber(bean.getB_amount())) {
				 		   charge = charge + Double.parseDouble(bean.getB_amount())	;
				 		}
			 		}
			 	} else {
			 		j++;
			 	}
			 	
			 	if(!flg)
			 		al_error.add(bean);
			 	else
			 		al_valid.add(bean);
					 	
			}
			 
	  		hm_container.put("error", al_error);
	  		hm_container.put("valid", al_valid);
	  		hm_container.put("errBuff", error.toString());
	  		hm_container.put("charge", new BigDecimal(charge).setScale(2,5).toString());
	  		hm_container.put("credit", new BigDecimal(credit).setScale(2,5).toString());
	  		
			return hm_container;
			
		}
		
		public void sentErrorReport(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
			
			p_request.setAttribute("screen", "/failuer.jsp");
	     	p_request.setAttribute("page", "Your Session Expired");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(p_request, p_response);
			
		}
}
