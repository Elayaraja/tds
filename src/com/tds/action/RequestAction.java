package com.tds.action;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;
import javazoom.upload.UploadParameters;

import org.apache.axis.utils.Admin;
import org.apache.log4j.Category;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.charges.dao.ChargesDAO;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverVehicleBean;
import com.tds.cmp.bean.OperatorShift;
import com.tds.controller.TDSController;
import com.tds.dao.DispatchDAO;
import com.tds.dao.PaymentDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SharedRideDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UserDetail;
import com.tds.dao.UtilityDAO;
import com.tds.dao.docLib.DocumentDAO;
import com.common.util.CheckZone;
import com.common.util.Messaging;
import com.tds.security.bean.TagSystemBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.DriverLocationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.OpenRequestFieldOrderBO;
import com.common.util.MessageGenerateJSON;
import com.common.util.OpenRequestUtil;
import com.common.util.SystemUtils;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

public class RequestAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;


	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ RequestAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	/*
	 * Access Get method from JSP page or Browser
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Get Method  ");
		doProcess(request,response);
	}

	/*
	 * Access Post method from JSP page or Browser
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Post Method");
		doProcess(request, response);
	}

	/*
	 * Access request from both getter and setter methods
	 */

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Process Method");
		String eventParam = "";
		HttpSession m_session = request.getSession();
		boolean m_sessionStatus = false;
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if(request.getParameter(TDSConstants.eventParam).equals("getreceipt")) {
			getreceipt(request, response);
		}
		if(m_session.getAttribute("user") != null && !eventParam.equalsIgnoreCase(TDSConstants.createOpenRequest) && !eventParam.equalsIgnoreCase(TDSConstants.saveOpenRequest)) {
			//System.out.println("Event--->"+eventParam);
			if(eventParam.equalsIgnoreCase(TDSConstants.getOpenRequestasXML)) {
				getOpenRequestAsXML(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.acceptOpenRequestByWEB)) {
				getOpenRequestDetailForAccept(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.getAcceptedandOpenRequest)){
				getAcceptedRequest(request,response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.tripSummaryReport)) {
				tripSummaryReport(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.getOpenRequestCriteria)) {
				getOpenCriteria(request,response);
			}else if(eventParam.equalsIgnoreCase("schedulejobreport")) {
				schuduleReport(request, response);
			} else if(eventParam.equalsIgnoreCase("cabmaintanancereport")) {
				cabmaintanancereport(request, response);
			}else if(eventParam.equalsIgnoreCase("LoginReport")) {
				LoginReport(request, response);
			}else if(eventParam.equalsIgnoreCase(TDSConstants.openRequestCriteria)) {
				openRequestCriteria(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.getAcceptRequestCriteria)) {
				getAcceptRequestCriteria(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.voucherSummaryReoprt)) {
				voucherSummaryReoprt(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.driverPaymentReport)) {
				getDriverPaymentReport(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.companyPaymentReport)) {
				getCompanyPaymentReport(request, response);//getCompanyPaymentReport
			}  else if(eventParam.equalsIgnoreCase(TDSConstants.getAcceptedRequestDriverXML)) {
				getAcceptedRequestAsXML(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.getAcceptedRequestDriverXML)) {
				getAcceptedRequestAsXML(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.doNightPaymentPending)) {
				nightPendingPayment(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.doNightPaymentSummary)) {
				nightPaymentSummary(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.doNightPaymentPendingDetail)) {
				nightPendingDetailPayment(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.getAvailableDriver)) {
				driverAvailableSummary(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.getSummarryInduvigual)) {
				getOpenRequestDetailSummarryInduvigual(request, response);
			} else if(eventParam.equalsIgnoreCase("Summaryvoucher")) {
				voucherDetailSummarry(request, response);
			} else if(eventParam.equalsIgnoreCase(TDSConstants.getDriverSetteled)) {
				getSettledSummary(request, response);
			}  else if(eventParam.equalsIgnoreCase(TDSConstants.getSendSMS)) {
				getSendSMS(request, response);
			} else if(eventParam.equalsIgnoreCase("Report")) {
				Report(request, response);
			} else if (eventParam.equalsIgnoreCase("manualcreditcardsummary")) {
				manualcreditcardsummary(request, response);
			}else if (eventParam.equalsIgnoreCase("AuditSummary")) {
				AuditSummary(request, response);
			}else if(eventParam.equalsIgnoreCase("openRequestHistory")) {
				openRequestHistory(request, response);
			}else if(eventParam.equalsIgnoreCase("openRequestSummary")) {
				openRequestSummary(request, response);
			}else if(eventParam.equalsIgnoreCase("openRequestReview")) {
				openRequestReview(request, response);
			}else if(eventParam.equalsIgnoreCase("operatorShift")) {
				operatorShift(request, response);
			}else if(eventParam.equalsIgnoreCase("operatorShiftReview")) {
				operatorShiftReview(request, response);
			}else if(eventParam.equalsIgnoreCase("openRequestHistoryWithLogs")) {
				openRequestHistoryWithLogs(request, response);
			}else if(eventParam.equalsIgnoreCase("openRequestSummaryMain")) {
				openRequestSummaryMain(request, response);
			}else if(eventParam.equalsIgnoreCase("fileUploadForOR")) {
				fileUploadForOR(request, response);
			}else if(eventParam.equalsIgnoreCase("sharedJobLogs")) {
				sharedJobLogs(request, response);
			}else if(eventParam.equalsIgnoreCase("reviewDocuments")) {
				reviewDocuments(request, response);
			}else if (eventParam.equalsIgnoreCase("sharedRide")) {
				sharedRide(request, response);
			}else if (eventParam.equalsIgnoreCase("customerdetail")){
					//System.out.println("Calling Method");
					customerdetail(request, response);
			}
			m_sessionStatus = true;
			//} else if(eventParam.equalsIgnoreCase(TDSConstants.createOpenRequest)) {
			//	m_sessionStatus = true;
			//	createOpenRequest(request, response);
		} else if(m_session.getAttribute("user") != null && eventParam.equalsIgnoreCase(TDSConstants.saveOpenRequest)) {
			m_sessionStatus = true;
			saveOpenRequest(request, response);
		}
		/*else if(eventParam.equalsIgnoreCase(TDSConstants.sendMail)) {
			getOpenRequestDetailForsendmail(request, response);
		}*/
		if(!m_sessionStatus  ) {
			//System.out.println("In Session Status");
			(getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP)).forward(request, response);
		}
	}

	private void manualcreditcardsummary(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,IOException{


		request.setAttribute("screen", "/jsp/manualcreditcardsummary.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}
	private void AuditSummary(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,IOException{
		request.setAttribute("screen", "/jsp/Auditsummary.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);

	}
	private void getreceipt(HttpServletRequest request,
			HttpServletResponse response)  throws ServletException,IOException {
		request.setAttribute("screen", "/jsp/getReceipt.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}
	public void voucherDetailSummarry(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Open Request function");
		HttpSession session = request.getSession();
		//System.out.println("Session "+session.getAttribute("user"));

		String forwardURL = TDSConstants.getMainNewJSP;
		cat.info("Forward the request into the "+ forwardURL +" JPS");	
		request.setAttribute("screen", "/jsp/voucherreport.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);		
	}

	public void Report(HttpServletRequest p_request, HttpServletResponse p_resResponse) throws ServletException,IOException {
		//p_request.setAttribute("screen", "/jsp/MasterReport.jsp");
		getServletContext().getRequestDispatcher("/jsp/MasterReport.jsp").forward(p_request, p_resResponse);
	}


	public void getSendSMS(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {

		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		HttpSession m_session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		ArrayList<DriverCabQueueBean> al = new ArrayList<DriverCabQueueBean>();
		boolean msgToSelectedDrivers;
		boolean textMessage;
		boolean msgToLoggedinDriversOnly;
		long msgID = System.currentTimeMillis();


		// Check if if this is called because of submit buttom
		if(request.getParameter("selectAll")==null){
			msgToSelectedDrivers = true;
		} else {
			msgToSelectedDrivers = false;
		}
		if(request.getParameter("loggedout")!=null){
			msgToLoggedinDriversOnly = false;
		} else {
			msgToLoggedinDriversOnly = true;
		}
		if(request.getParameter("voiceMsg")!=null){
			textMessage = false;
		} else {
			textMessage = true;
		}
		//BAsed on variables call the corresponding DAO object to fetch the data
		if (msgToSelectedDrivers){
			String arr[]= !request.getParameter("driver_id").equals("")?request.getParameter("driver_id").split(";"):null;
			//			if(!(arr.length > 0))
			//			{
			//				arr[0] = request.getParameter("driver_id");
			//			} 
			if(!request.getParameter("driver_id").equals("")){
				for(int i=0;i<arr.length;i++){
					DispatchDAO.sendMessage(request.getParameter("message"), arr[i], m_adminBO, 0);
				}
			}
			al = UtilityDAO.getDriverListForMessaging(m_adminBO.getMasterAssociateCode(), arr,0, msgToLoggedinDriversOnly);	
		} else{
			al = UtilityDAO.getDriverListForMessaging(m_adminBO.getMasterAssociateCode(), null,1, msgToLoggedinDriversOnly);
		}
		//Generate the message based on the values selected in the screen
		String[] message = new String[2];
		if(!msgToLoggedinDriversOnly){

			if(!textMessage){
				message = MessageGenerateJSON.generatePrivateMessage(request.getParameter("message"), "VMLO", msgID);
			}else{
				message = MessageGenerateJSON.generatePrivateMessage(request.getParameter("message"), "TXLO", msgID);
			}
		}else { 
			if(!textMessage){
				message = MessageGenerateJSON.generatePrivateMessage(request.getParameter("message"), "VMLI", msgID);
			}else{
				message = MessageGenerateJSON.generatePrivateMessage(request.getParameter("message"), "TXLI", msgID);
				message[0]=message[0];//+";timeZone="+m_adminBO.getTimeZone();
				if(request.getParameter("date")!=null)
				{
					message[0]=message[0]+";delayDate="+request.getParameter("date");
				}
				if(request.getParameter("time")!=null)
				{
					message[0]=message[0]+";delayTime="+request.getParameter("time");
				}
				//TODO want to delete
				//System.out.println("Send Sms-->" + message[0]);
				//System.out.println("Send sms-->" + message[1]);
			}
		}
		Messaging sendMessage = new Messaging(getServletContext(), al, message, poolBO, m_adminBO.getMasterAssociateCode());
		sendMessage.start();

		//Use the message to send out
		//boolean returnCode = false;


		if (al.size()>0){
			//request.setAttribute("page", " Message Sent sucessfully");	
			response.getWriter().write("1");
		} else{
			//request.setAttribute("error", "Error Sending Message");
			response.getWriter().write("0");
		}
		//getServletContext().getRequestDispatcher("/Company/SMSSend.jsp").forward(request, response);

	}	

	public void getOpenRequestDetailSummarryInduvigual(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Open Request function");
		HttpSession session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) session.getAttribute("user");	
		//System.out.println("Session "+session.getAttribute("user"));

		String forwardURL = TDSConstants.getMainNewJSP;

		cat.info("Forward the request into the "+ forwardURL +" JPS");
		request.setAttribute("al_driver", UtilityDAO.getDriverList(m_adminBO.getAssociateCode(),1));
		request.setAttribute("screen", TDSConstants.SummarryInduvigual);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);		

	}

	public void getSettledSummary(HttpServletRequest p_request, HttpServletResponse p_resResponse) throws ServletException,IOException {
		//System.out.println(" In get Driver Setteled Summery ");
		//	p_request.setAttribute("companyList", RequestDAO.getCompanyInfo());
		p_request.setAttribute("screen", TDSConstants.DriverSettledSummery);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_resResponse);
	}





	/*	public void getOpenRequestDetailForsendmail(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Open Request function");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		try {

			String s3="/root/Example1.html";
			ArrayList arr = new ArrayList();

			arr.add(request.getParameter("msg"));

			MailingUtiil.sendMail(poolBO.getSMTP_HOST_NAME(), poolBO.getSMTP_AUTH_USER(), poolBO.getSMTP_AUTH_PWD(), poolBO.getEmailFromAddress(), arr, request.getParameter("msg"),s3,poolBO.getSMTP_PORT(),poolBO.getProtocol());

			System.out.println("DONE");


		} catch (MessagingException p_messageExp) {

			//System.out.println("Exception in payment verification Messaging "+p_messageExp);
			p_messageExp.getStackTrace();
		}

		String forwardURL = TDSConstants.getMainNewJSP;

		cat.info("Forward the request into the "+ forwardURL +" JPS");	
		request.setAttribute("screen", TDSConstants.openRequestJSP);

		request.setAttribute("screen", "");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);		
	}

	/*public void createOpenRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Open Request function");
		HttpSession session = request.getSession();
		System.out.println("Session "+session.getAttribute("user"));

		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		cat.info("Forward the request into the "+ forwardURL +" JPS");	
		request.setAttribute("screen", TDSConstants.openRequestJSP);
		screen = TDSConstants.openRequestJSP;
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);		
	}
	 */

	public void saveOpenRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Save Request function");
		String m_queueId = "";
		int result=0;
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen ="";
		String error="";
		if(adminBO.getORFormat()==1){
			screen=TDSConstants.openRequestJSP01;
		} else if(adminBO.getORFormat()==2){
			screen=TDSConstants.openRequestJSP02;
		} else if(adminBO.getORFormat()==3){
			screen=TDSConstants.openRequestJSP03;
		}else if(adminBO.getORFormat()==4){
			screen=TDSConstants.openRequestJSP04;
		}else {
			screen=TDSConstants.openRequestJSP01;
		}
		if(adminBO.getAddressFill()==3){
			screen="/jsp/OpenRequestFormatOSM.jsp";
		}
		HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode()+"Flags");
		if(companyFlags==null){
			SystemUtils.reloadCompanyFlags(this, adminBO.getMasterAssociateCode());
			companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode()+"Flags");
		}
		request.setAttribute("companyFlags", companyFlags);
		ArrayList drFlag=new ArrayList();
		ArrayList vehFlag=new ArrayList();
		Set hashMapEntries = companyFlags.entrySet();
		Iterator it = hashMapEntries.iterator();
		while(it.hasNext()){
			Map.Entry<String, DriverVehicleBean> companyFlag=(Map.Entry<String, DriverVehicleBean>)it.next();
			if(companyFlag.getValue().getDriverVehicleSW()==1){
				drFlag.add(companyFlag.getValue().getShortDesc());
				drFlag.add(companyFlag.getValue().getKey());
				drFlag.add(companyFlag.getValue().getLongDesc());
			}else{
				vehFlag.add(companyFlag.getValue().getShortDesc());
				vehFlag.add(companyFlag.getValue().getKey());
				vehFlag.add(companyFlag.getValue().getLongDesc());
			}
		}
		request.setAttribute("vecFlag", vehFlag);
		request.setAttribute("drFlag", drFlag);		
		ArrayList<ChargesBO> chargeType=ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),0);
        Collections.rotate(chargeType, -1);
		request.setAttribute("chargeTypes", chargeType);
		OpenRequestBO openRequestBO = new OpenRequestBO();
		//Get the phone number from the request
		String phoneNum = request.getParameter("phoneNo");
		String tripId = request.getParameter("tripId");
		String masterKey=request.getParameter("masterKey");
		//String phoneNumFromShortScreen=request.getParameter("phone");
		openRequestBO.setSstate(adminBO.getState());

		//Check if phone number is not null
		//Got to the database and pick up old requests from the Openrequet table for that phone number
		if(phoneNum != null){
			//Populate an array and populate it into the request
			openRequestBO.setPhone(phoneNum);
			ArrayList al_landMark=RegistrationDAO.getLandMark(TDSValidation.getDBPhoneFormat(phoneNum),adminBO.getMasterAssociateCode());
			if(al_landMark.size()>0){
				request.setAttribute("landMark", al_landMark);
			}
			if(al_landMark.size()>0){
				ArrayList al_list = RegistrationDAO.getpreviousDetails(TDSValidation.getDBPhoneFormat(phoneNum), adminBO);
				if(al_list.size()> 0)
				{
					request.setAttribute("prevopenrequests", al_list);
				}
				ArrayList al_prevaddress = RegistrationDAO.getFromandToAddress(TDSValidation.getDBPhoneFormat(phoneNum), adminBO.getMasterAssociateCode());
				if(al_prevaddress.size()> 0)
				{
					request.setAttribute("prevaddresses", al_prevaddress);
				}
			}
		} else if(tripId != null){
			openRequestBO = ServiceRequestDAO.getOpenRequestByTripID(tripId, adminBO, new ArrayList<String>());
			/*if(!request.getParameter("nextScreen").equals("") && request.getParameter("nextScreen").equals("DashBoard")){
				request.setAttribute("nextScreen", "DashBoard");
			}*/
		}

		// the array will be retrieved by the Openrequest JSP and displayed
		forwardURL = TDSConstants.getMainNewJSP;
		request.setAttribute("screen", screen);
		request.setAttribute("al_q", CheckZone.returnZonesForAssociatoinCode(getServletContext(),adminBO.getMasterAssociateCode()));
		request.setAttribute("openrequest", openRequestBO);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(screen);
		requestDispatcher.forward(request, response);
	}	


	public void getAcceptedRequestAsXML (HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		HttpSession session = request.getSession();
		String utype ="";
		String uname = "";		
		if(session.getAttribute("user")!=null){
			AdminRegistrationBO	adminBO = (AdminRegistrationBO)session.getAttribute("user");
			utype =adminBO.getUsertypeDesc();
			uname=adminBO.getUname();
			//System.out.println("adminBO.getUsertypeDesc()"+adminBO.getUsertypeDesc());
		}		
		ArrayList message = null;
		cat.info("In getOpenRequestAsXml function ");
		String forwardURL = TDSConstants.getMainNewJSP;
		ArrayList al_openRequest = RequestDAO.getAcceptedRequestDataXML(uname);
		if(utype.equalsIgnoreCase("Driver")){
			message= ServiceRequestDAO.getMessage(uname);
			//System.out.println("message "+message);
			if(message.size() >0) {
				request.setAttribute("message", message);
			}
		}

		request.setAttribute("openRequest", al_openRequest);
		request.setAttribute("screen", TDSConstants.getOpenRequestXML);
		forwardURL = TDSConstants.getOpenRequestXML;
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}


	public void nightPendingDetailPayment(HttpServletRequest p_request, HttpServletResponse p_response ) throws ServletException,IOException {
		if(PaymentDAO.insertPendingDetailPayment()) {
			//System.out.println("Successfully inserted");
			p_request.setAttribute("screen", TDSConstants.getSuccessJSP);
			p_request.setAttribute("Page", " inserted pending detail payments");
		} else {
			//System.out.println("Failed to insert pending payment details");
			p_request.setAttribute("screen", TDSConstants.getFailureJSP);
			p_request.setAttribute("Page", " to insert pending detail payments");
		}
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	public void nightPaymentSummary(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		if(PaymentDAO.insertPaymentSummary()) {
			//System.out.println("Successfully inserted Payment Summary");
			p_request.setAttribute("screen", TDSConstants.getSuccessJSP);
			p_request.setAttribute("Page", " inserted payment summary");
		} else {
			//System.out.println("Failed to insert Payment Summary");
			p_request.setAttribute("screen", TDSConstants.getFailureJSP);
			p_request.setAttribute("Page", " insert payment summary");
		}
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	public void nightPendingPayment(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException , IOException {
		if(PaymentDAO.insertPendingPayment()) {
			//System.out.println("Successfully inserted Pending Payment");
			p_request.setAttribute("screen", TDSConstants.getSuccessJSP);
			p_request.setAttribute("Page", " inserted pending payment");
		} else {
			//System.out.println("Failed to insert Payment Summary");
			p_request.setAttribute("screen", TDSConstants.getFailureJSP);
			p_request.setAttribute("Page", " insert pending payment");
		}
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	/*public void nightTaskDetails(HttpServletRequest p_request,HttpServletResponse p_response) throws ServletException, IOException {
		System.out.println("nightTaskDetails method");
		if(PaymentDAO.insertPendingPayment()) {
			System.out.println("Successfully inserted");
			p_request.setAttribute("screen", TDSConstants.getSuccessJSP);
			p_request.setAttribute("Page", "Pending Payment inserted ");
		}
		if(PaymentDAO.insertPaymentSummary()) {
			System.out.println("Successfully inserted");
			p_request.setAttribute("screen", TDSConstants.getSuccessJSP);
			p_request.setAttribute("Page", "Pending Payment inserted ");
		} 
		if(PaymentDAO.insertPendingDetailPayment()) {
			System.out.println("Successfully inserted");
			p_request.setAttribute("screen", TDSConstants.getSuccessJSP);
			p_request.setAttribute("Page", "Pending Payment inserted ");
		}
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}*/

	public void getOpenRequestAsXML (HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		/*HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null){
			AdminRegistrationBO	adminBO = (AdminRegistrationBO)session.getAttribute("user");
			utype =adminBO.getUsertypeDesc();
			uname=adminBO.getUname();
			System.out.println("adminBO.getUsertypeDesc()"+adminBO.getUsertypeDesc());
		}		*/
		cat.info("In getOpenRequestAsXml function ");
		String forwardURL = TDSConstants.getMainNewJSP;
		ArrayList al_openRequest = new ArrayList();
		//al_openRequest = RequestDAO.getOpenRequest();
		/*if(utype.equalsIgnoreCase("Driver")){
			message= ServiceRequestDAO.getMessage(uname);
			System.out.println("message "+message);
			if(message.size() >0) {
				request.setAttribute("message", message);
			}
		}*/

		request.setAttribute("openRequest", al_openRequest);
		request.setAttribute("screen", TDSConstants.getOpenRequestXML);
		forwardURL = TDSConstants.getOpenRequestXML;
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getOpenRequestDetailForAccept(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In getOpen Request Detail into Accept Request ");
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.acceptOpenRequestJSP;
		String m_driverKey = "";
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO = setOpenRequest(openRequestBO, request);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		if(request.getParameter("submitphone") != null) {
			String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO, myString);
			}
//			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "","0", adminBO);
			if(openRequestBO.getIsBeandata() == 1) {
				request.setAttribute("errors", "Tripid is not accepted");
			} else {
				request.setAttribute("errors", "Tripid is accepted");
			}
			request.setAttribute("openrequest", openRequestBO);
		} else if(request.getParameter("driveravil") != null) {
			int result = RequestDAO.checkDriverAvailable(openRequestBO.getDriverid());
			if(result == 0 ) {
				request.setAttribute("errors", "Driver is not available");
			} else if(result == 1){
				request.setAttribute("errors", "Driver is Available");
			}
			request.setAttribute("openrequest", openRequestBO);
		} else if(request.getParameter("acceptOpenRequest") != null) {
			//
			//if(openRequestBO.getIsBeandata() == 1) {
			//System.out.println("date !!!!!!!!!!!!!!!!!"+openRequestBO.getSdate());
			//String driverid = openRequestBO.getDriverid();
			openRequestBO = RequestDAO.getOpenRequestTableData(openRequestBO);
			m_driverKey = RequestDAO.getDriverKey(openRequestBO.getDriverid());
			openRequestBO.setDriverid(m_driverKey);
			int result = ServiceRequestDAO.insertAcceptedRequest(openRequestBO);
			if(result > 0) {
				request.setAttribute("page", " inserted Accepted Request");
				screen  = TDSConstants.getSuccessJSP;
			} else {
				request.setAttribute("page", " insert Accepted Request");
				screen  = TDSConstants.getFailureJSP;
			}

		}

		else if(request.getParameter("updateOpenRequest") != null){
			//System.out.println("In updateOpenRequest");
			m_driverKey = RequestDAO.getDriverKey(openRequestBO.getDriverid());
			openRequestBO.setDriverid(m_driverKey);
			int result = ServiceRequestDAO.changeAcceptedRequest(openRequestBO, "update",adminBO);
			if(result > 0){
				request.setAttribute("page", " updated Accepted Request");
				screen  = TDSConstants.getSuccessJSP;
			} else {
				request.setAttribute("page", " update Accepted Request");
				screen  = TDSConstants.getFailureJSP;
			}

		}
		else if(request.getParameter("deleteOpenRequest") != null){
			//System.out.println("In deleteOpenRequest");
			int result = ServiceRequestDAO.changeAcceptedRequest(openRequestBO, "delete",adminBO);
			if(result > 0){
				request.setAttribute("page", " deleted Accepted Request");
				screen  = TDSConstants.getSuccessJSP;
			} else {
				request.setAttribute("page", " delete Accepted Request");
				screen  = TDSConstants.getFailureJSP;
			}
		}

		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getAcceptedRequest(HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		//System.out.println("in getAcceptedRequest");
		cat.info("In getAcceptedRequest");
		String phoneno="";
		String tripid="";
		String m_driverKey = "";
		int status=0;
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.getAcceptedRequestToEditJSP;
		//System.out.println("update ="+request.getParameter("update"));
		//System.out.println("delete ="+request.getParameter("delete"));
		HashMap hs_data=new HashMap();
		OpenRequestBO openBO=new OpenRequestBO();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		if(request.getParameter("getrecord")!=null){
			//System.out.println("phone ="+request.getParameter("phone"));
			phoneno=request.getParameter("phone");
			screen = TDSConstants.getAcceptedRequestToEditJSP;
			openBO.setPhone(phoneno);
			try{
				//hs_data=RequestDAO.getAcceptedRequest(openBO);
				request.setAttribute("AcceptedData", hs_data);
				System.out.println("al_requestdata =="+hs_data);
			}
			catch(Exception ex){
				cat.info("Exception in the getAcceptedRequest :"+ex.getMessage());
			}
		}

		if(request.getParameter("update")!=null){
			//System.out.println("update");			
			if(request.getParameter("tripid")!=null){
				tripid=request.getParameter("tripid");
				openBO.setTripid(tripid);
				openBO=setOpenRequest(openBO,request);
				m_driverKey = RequestDAO.getDriverKey(openBO.getDriverid());
				openBO.setDriverid(m_driverKey);
				status=RequestDAO.updateAcceptedRequest(openBO,adminBO);
				if(status > 0){
					screen = TDSConstants.getSuccessJSP;
				}
				else{
					screen = TDSConstants.getFailureJSP;
				}
			}
		}
		if(request.getParameter("delete")!=null){
			//System.out.println("delete");
			if(request.getParameter("tripid")!=null){
				tripid=request.getParameter("tripid");
				openBO.setTripid(tripid);
				m_driverKey = RequestDAO.getDriverKey(openBO.getDriverid());
				openBO.setDriverid(m_driverKey);
				status=RequestDAO.deleteAcceptedRequest(openBO,adminBO);
				if(status > 0){
					screen = TDSConstants.getSuccessJSP;
				}
			}
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void openRequestCriteria(HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		cat.info("In openRequestCriteria method");
		String screen = TDSConstants.openRequestCriteriaJSP;
		String forwardURL = TDSConstants.getMainNewJSP;
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getOpenCriteria(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		cat.info("In getopenRequestCriteria method");
		String screen = TDSConstants.openRequestCriteriaJSP;
		String forwardURL = TDSConstants.getMainNewJSP;
		HttpSession session = request.getSession();
		OpenRequestBO openRequestBO = new OpenRequestBO();
		if(request.getParameter("tripid") != null && request.getParameter("tripid") != "") {
			openRequestBO.setTripid(request.getParameter("tripid"));
		}
		if(request.getParameter("phone") != null && request.getParameter("phone") != "") {
			openRequestBO.setPhone(request.getParameter("phone"));
		}
		if(request.getParameter("sdate") != null && request.getParameter("sdate") != "") {
			openRequestBO.setSdate(request.getParameter("sdate"));
		}
		if(request.getParameter("scity") != null && request.getParameter("scity") != "") {
			openRequestBO.setScity(request.getParameter("scity"));
		}
		if(request.getParameter("sstate") != null && request.getParameter("sstate") != "") {
			openRequestBO.setSstate(request.getParameter("sstate"));
		}
		if(request.getParameter("szip") != null && request.getParameter("szip") != "") {
			openRequestBO.setSzip(request.getParameter("szip"));
		} 
		ArrayList al_list = new ArrayList();
		if(request.getParameter("openrequest") != null) {
			al_list = RequestDAO.getOpenRequest(openRequestBO,"");
			//System.out.println(al_list);
			if(al_list != null ){
				request.setAttribute("openSummary", al_list);
				screen = TDSConstants.openRequestCriteriaJSP;
			} else {
				request.setAttribute("openrequest", openRequestBO);
				request.setAttribute("btn", "openrequest");
				request.setAttribute("openSummary", new ArrayList());
			}
		} else if(request.getParameter("acceptrequest") != null) {
			al_list = RequestDAO.getOpenRequest(openRequestBO,"");
			/*if(al_list != null ){
				request.setAttribute("openSummary", al_list);
				screen = TDSConstants.openRequestCriteriaJSP;
			} else {*/
			if(al_list!=null && al_list.size()>0){
				request.setAttribute("openrequest", al_list.get(0));
				request.setAttribute("btn", "acceptrequest");
				request.setAttribute("openSummary", new ArrayList());
				request.setAttribute("al_q", ServiceRequestDAO.getQueueName(openRequestBO.getSlat(), openRequestBO.getSlong(),(AdminRegistrationBO) session.getAttribute("user")));
				screen = TDSConstants.updateopenRequestJSP;
			}  
			//}
		}

		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getAcceptRequestCriteria(HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		cat.info("In getAcceptRequestCriteria method");
		String screen = TDSConstants.acceptRequestCriteriaJSP;
		String forwardURL = TDSConstants.getMainNewJSP;
		OpenRequestBO openRequestBO = new OpenRequestBO();
		if(request.getParameter("tripid") != null && request.getParameter("tripid") != "") {
			openRequestBO.setTripid(request.getParameter("tripid"));
		}
		if(request.getParameter("phone") != null && request.getParameter("phone") != "") {
			openRequestBO.setPhone(request.getParameter("phone"));
		}
		if(request.getParameter("sdate") != null && request.getParameter("sdate") != "") {
			openRequestBO.setSdate(request.getParameter("sdate"));
		}
		if(request.getParameter("scity") != null && request.getParameter("scity") != "") {
			openRequestBO.setScity(request.getParameter("scity"));
		}
		if(request.getParameter("sstate") != null && request.getParameter("sstate") != "") {
			openRequestBO.setSstate(request.getParameter("sstate"));
		}
		if(request.getParameter("szip") != null && request.getParameter("szip") != "") {
			openRequestBO.setSzip(request.getParameter("szip"));
		}
		ArrayList al_list = null;
		if(request.getParameter("acceptrequest") != null) {
			//al_list = RequestDAO.getAcceptRequest(openRequestBO);
			//System.out.println(al_list);
			if(al_list.size() == 1) {
				request.setAttribute("openrequest", al_list.get(0));

				screen = TDSConstants.acceptOpenRequestJSP;
			} else {
				request.setAttribute("openrequest", openRequestBO);
				request.setAttribute("openSummary", al_list);
			}
		}
		request.setAttribute("modify", "modify");
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	public void tripSummaryReport(HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		//System.out.println("In voucherSummaryReoprt");
		String forwardURL = TDSConstants.tripSummaryJSPReport;
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void voucherSummaryReoprt(HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		//System.out.println("In voucherSummaryReoprt");
		String forwardURL = TDSConstants.voucherSummaryJSPReoprt;
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}

	public void getDriverPaymentReport(HttpServletRequest p_request, HttpServletResponse p_resResponse) throws ServletException,IOException {
		//System.out.println(" In getDriver payment Report ");
		p_request.setAttribute("driverList", RequestDAO.getDriverInfo());
		p_request.setAttribute("screen", TDSConstants.driverPaymentJSPReport);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_resResponse);
	}

	public void getCompanyPaymentReport(HttpServletRequest p_request, HttpServletResponse p_resResponse) throws ServletException,IOException {
		//System.out.println(" In get Company payment Report ");
		p_request.setAttribute("companyList", RequestDAO.getCompanyInfo());
		p_request.setAttribute("screen", TDSConstants.companyPaymentJSPReport);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_resResponse);
	}

	public void driverAvailableSummary(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		//System.out.println("In driverAvailableSummary Method");
		cat.info("In driverAvailableSummary Method");
		p_request.setAttribute("driverAvailList", RequestDAO.getAvailableDriver());
		p_request.setAttribute("screen", TDSConstants.driverAvailSummary);
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_response);
	}

	/*
	 * 
	 * Following codes are assign data into corresponding bean aslo code for do some validation 
	 * 
	 */ 

	public OpenRequestBO setOpenRequest(OpenRequestBO requestBO,HttpServletRequest request) {
		//OpenRequestBO requestBO = p_openRequestBO;

		requestBO.setTripSource(TDSConstants.Company);
		requestBO.setUserAddressKey(request.getParameter("userAddress"));
		requestBO.setMasterAddressKey(request.getParameter("masterAddress"));

		requestBO.setEadd1(request.getParameter("eadd1"));
		requestBO.setEadd2(request.getParameter("eadd2"));
		requestBO.setEcity(request.getParameter("ecity"));
		requestBO.setEstate(request.getParameter("estate"));
		requestBO.setEzip(request.getParameter("ezip"));
		requestBO.setPhone(request.getParameter("phone"));
		requestBO.setSadd1(request.getParameter("sadd1"));
		requestBO.setSadd2(request.getParameter("sadd2"));
		requestBO.setScity(request.getParameter("scity"));
		requestBO.setSdate(request.getParameter("sdate"));
		requestBO.setSstate(request.getParameter("sstate"));
		//requestBO.setSstatus(request.getParameter("sstatus"));
		requestBO.setShrs(request.getParameter("shrs"));
		/*requestBO.setSmin(request.getParameter("smin"));*/
		requestBO.setSzip(request.getParameter("szip"));
		//		requestBO.setSaircode(request.getParameter("saircode"));
		//		requestBO.setEaircode(request.getParameter("eaircode"));
		//		requestBO.setSaircodeid(request.getParameter("saircodeid"));
		//		requestBO.setEaircodeid(request.getParameter("eaircodeid"));
		requestBO.setSlandmark(request.getParameter("slandmark"));
		requestBO.setSpecialIns(request.getParameter("specialIns"));
		requestBO.setSintersection(request.getParameter("sintersection"));
		//Getting latitude and longitue from the html screen
		if(request.getParameter("sLongitude").equals("")){
			requestBO.setSlong("0");		
		} else {
			requestBO.setSlong(request.getParameter("sLongitude"));
		}
		if(request.getParameter("sLatitude").equals("")){
			requestBO.setSlat("0");
		} else {
			requestBO.setSlat(request.getParameter("sLatitude"));
		}
		requestBO.setTypeOfRide(request.getParameter("sharedRide"));
		requestBO.setComments(request.getParameter("Comments"));
		requestBO.setDriverid(request.getParameter("driver")==null?"":request.getParameter("driver"));
		requestBO.setTripid(request.getParameter("tripId"));
		requestBO.setAdvanceTime(request.getParameter("advanceTime"));

		requestBO.setPaytype((request.getParameter("paytype")!=null && !request.getParameter("paytype").equals(""))?request.getParameter("paytype"):"Cash");
		//requestBO.setPaytype(request.getParameter("paytype"));
		requestBO.setAcct(request.getParameter("acct"));
		if (request.getParameter("amt") != null){
			if(request.getParameter("amt") != ""){
				requestBO.setAmt(new BigDecimal(request.getParameter("amt").toString()));
			}else{
				requestBO.setAmt(new BigDecimal("0.00"));
			}
		}

		if(request.getParameter("driverid") != null) {
			requestBO.setDriverid(request.getParameter("driverid"));
		}
		if(request.getParameter("tripid") != null) {
			requestBO.setTripid(request.getParameter("tripid"));
		} 
		if(request.getParameter("associateCode") != null) {
			requestBO.setAssociateCode(request.getParameter("associateCode"));
		}
		if(request.getParameter("Slat") != null ) {
			requestBO.setSlat(request.getParameter("Slat"));
		}
		if(request.getParameter("Slong") != null) {
			requestBO.setSlong(request.getParameter("Slong"));
		}
		if(request.getParameter("edlatitude") != null) {
			requestBO.setEdlatitude(request.getParameter("edlatitude"));
		}
		if(request.getParameter("edlongitude") != null) {
			requestBO.setEdlongitude(request.getParameter("edlongitude"));
		}
		if(request.getParameter("name") != null) {
			requestBO.setName(request.getParameter("name"));
		}
		if(request.getParameter("queueno") != null) {
			requestBO.setQueueno(request.getParameter("queueno"));
		}if(request.getParameter("dispatchStatus") !=null) {
			requestBO.setDispatchStatus("true");
		}else {
			requestBO.setDispatchStatus("false");

		}
		//Populate the phone provider of the passenger.
		if(request.getParameter("toSms") != null) {
			requestBO.setPassengerPhoneServiceProvider(request.getParameter("toSms"));
		}



		try{
			if(request.getParameter("drprofileSize")!=null && Integer.parseInt(request.getParameter("drprofileSize")) > 0)
			{
				ArrayList al_l = new ArrayList();
				String drProfile="";
				for(int i=0;i<Integer.parseInt(request.getParameter("drprofileSize"));i++)
				{
					if(request.getParameter("dchk"+i)!=null){
						al_l.add(request.getParameter("dchk"+i));
						drProfile = drProfile + request.getParameter("dchk"+i);
					}
				}
				requestBO.setAl_drList(al_l);
				requestBO.setDrProfile(drProfile);
			}
		}catch (NumberFormatException e) {
			// TODO: handle exception
		}

		try{
			if(request.getParameter("vprofileSize")!=null && Integer.parseInt(request.getParameter("vprofileSize")) > 0)
			{
				ArrayList al_l = new ArrayList();
				String drProfile="";
				for(int i=0;i<Integer.parseInt(request.getParameter("vprofileSize"));i++)
				{
					if(request.getParameter("vchk"+i)!=null){
						al_l.add(request.getParameter("vchk"+i));
						drProfile = drProfile + request.getParameter("vchk"+i);
					}
				}
				requestBO.setAl_vecList(al_l);
				requestBO.setVecProfile(drProfile);
			}
		}catch (NumberFormatException e) {
			// TODO: handle exception
		}
		if (request.getParameter("Sun")==null){
			requestBO.setDays("0");
		}else {
			requestBO.setDays("1");
		}
		if(request.getParameter("Mon")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else{
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Tue")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else{
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Wed")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else {
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Thu")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else{
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Fri")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else {
			requestBO.setDays(requestBO.getDays()+"1");
		}
		if(request.getParameter("Sat")==null){
			requestBO.setDays(requestBO.getDays()+"0");
		}else {
			requestBO.setDays(requestBO.getDays()+"1");
		}



		return requestBO;
	}

	public void schuduleReport(HttpServletRequest p_request, HttpServletResponse p_resResponse) throws ServletException,IOException {
		//System.out.println(" schudule Report Report ");
		//p_request.setAttribute("driverList", RequestDAO.getDriverInfo());
		p_request.setAttribute("screen", "/jsp/schedulereport.jsp");
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_resResponse);
	}
	public void LoginReport(HttpServletRequest p_request, HttpServletResponse p_resResponse) throws ServletException,IOException {
		//System.out.println(" LoginReport Report ");
		//p_request.setAttribute("driverList", RequestDAO.getDriverInfo());
		p_request.setAttribute("screen", "/jsp/DriverLogin.jsp");
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_resResponse);
	}
	public void cabmaintanancereport(HttpServletRequest p_request, HttpServletResponse p_resResponse) throws ServletException,IOException {
		//System.out.println(" cabmaintanancereport Report Report ");
		//p_request.setAttribute("driverList", RequestDAO.getDriverInfo());
		p_request.setAttribute("screen", "/jsp/cabmaintanance.jsp");
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(p_request, p_resResponse);
	}

	public void openRequestHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		OpenRequestBO openRequestHistory =null;
		String errorReturn="";
		//		if(request.getParameter("button").equalsIgnoreCase("YES")){
		//			openRequestHistory =new OpenRequestBO();
		//			//RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		//			openRequestHistory.setSdate(request.getParameter("serviceDate")==null?"":request.getParameter("serviceDate"));
		//			String value= request.getParameter("value");
		//			String fromDate=request.getParameter("fromDate");
		//			String toDate=request.getParameter("toDate");
		//			String fromDateFormatted=TDSValidation.getTimeStampDateFormat(fromDate);
		//			String toDateFormatted=TDSValidation.getTimeStampDateFormat(toDate);
		//			ArrayList<OpenRequestBO>openReqHistory=RequestDAO.openRequestHistory(openRequestHistory,adminBO,value,fromDateFormatted,toDateFormatted);
		//			request.setAttribute("openRequestHistory", openReqHistory);
		//		}else
		if(request.getParameter("source")!=null && request.getParameter("source").equals("createAJob")){
			response.getWriter().write(errorReturn.contains("Status=OK")?errorReturn:"002");
		} else {
			getServletContext().getRequestDispatcher("/Company/OpenRequestHistory.jsp").forward(request, response);
		}
	}

	public void openRequestSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		StringBuffer outData = new StringBuffer("");
		OpenRequestBO openRequestSummary = new OpenRequestBO();
		ArrayList<OpenRequestBO>openReqSummary=new ArrayList<OpenRequestBO>();
		String fromDate=request.getParameter("fDate")==null?"":request.getParameter("fDate");
		String toDate=request.getParameter("tDate")==null?"":request.getParameter("tDate");
		String fromDateFormatted=TDSValidation.getTimeStampDateFormat(fromDate);
		String toDateFormatted=TDSValidation.getTimeStampDateFormat(toDate);
		String value= request.getParameter("search");
		String assocCode = request.getParameter("fleetNo")!=null?request.getParameter("fleetNo"):"";
		HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) this.getServletContext().getAttribute(adminBO.getMasterAssociateCode()+"Flags");
		if(request.getParameter("button").equalsIgnoreCase("YES")){
			if(request.getParameter("account")!=null && request.getParameter("account").equals("YES")){
				openReqSummary=RequestDAO.openRequestSummary(openRequestSummary,adminBO,value,fromDateFormatted,toDateFormatted,1,assocCode);
			}else if(request.getParameter("repeatJobs")!=null && request.getParameter("repeatJobs").equals("YES")){
				openReqSummary=RequestDAO.openRequestSummary(openRequestSummary,adminBO,value,fromDateFormatted,toDateFormatted,2,assocCode);
			}else{
				openReqSummary=RequestDAO.openRequestSummary(openRequestSummary,adminBO,value,fromDateFormatted,toDateFormatted,3,assocCode);
			}
		}
		if(request.getParameter("status").equals("10")){
			outData.append("TripId,Name,Phone Number,Date,Start Address,End Address,Driver,Cab No,Created By,Comments,Special Ins,Payment Info\n");
			for(int i=0;i<openReqSummary.size();i++){
				outData.append(openReqSummary.get(i).getTripid()+","+openReqSummary.get(i).getName().replace(",", " ")+","+openReqSummary.get(i).getPhone()+","+openReqSummary.get(i).getStartTimeStamp()+","
						+""+openReqSummary.get(i).getSadd1().replace(",","")+" "+openReqSummary.get(i).getSadd2().replace(",", "")+" "+openReqSummary.get(i).getScity().replace(",", "")+" "+openReqSummary.get(i).getSstate().replace(",", "")+","
						+""+openReqSummary.get(i).getEadd1().replace(",","")+" "+openReqSummary.get(i).getEadd2().replace(",", "")+" "+openReqSummary.get(i).getEcity().replace(",", "")+" "+openReqSummary.get(i).getEstate().replace(",", "")+","
						+""+openReqSummary.get(i).getDriverid()+","+openReqSummary.get(i).getVehicleNo()+","+openReqSummary.get(i).getCreatedBy()+","+openReqSummary.get(i).getComments().replace(",", "")+","+openReqSummary.get(i).getSpecialIns().replace(",", "")+","+((openReqSummary.get(i).getPaytype().equalsIgnoreCase("VC"))?openReqSummary.get(i).getPaytype().replace(",", "")+" ("+openReqSummary.get(i).getAcct().replace(",", "")+")":openReqSummary.get(i).getPaytype().replace(",", ""))+"\n");
			} 
			response.setContentType("application/csv");
			response.setHeader( "Content-Disposition:", "attachment; filename=" + "\"Reservation.csv\"" );
			response.getWriter().write(outData.toString());
		} else {
			request.setAttribute("allFlags", companyFlags);
			request.setAttribute("openRequestSummary", openReqSummary);
			getServletContext().getRequestDispatcher("/Company/openRequestSummary.jsp").forward(request, response);
		}
	}

	public void openRequestReview(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");
		OpenRequestBO openBo=new OpenRequestBO();
		if(request.getParameter("getJobs")!=null){
			openBo.setRouteNumber(request.getParameter("routeNumber"));
			openBo.setDriverid(request.getParameter("driverId"));
			openBo.setSdate(request.getParameter("fromDate"));
			openBo.setDropTime(request.getParameter("toDate"));
			if(request.getParameter("allocated")!=null){
				openBo.setChckTripStatus(1);	
			} else {
				openBo.setChckTripStatus(0);
			}
			if(request.getParameter("unAllocated")!=null){
				openBo.setNumOfPassengers(1);	
			} else {
				openBo.setNumOfPassengers(0);
			}
		}
		ArrayList<OpenRequestBO> arrList=RequestDAO.getAllOR(adminBO,null);
		//System.out.println("Size--->"+arrList.size());
		ArrayList<DriverLocationBO> drivers =  UtilityDAO.getDriverLocationListByStatus(adminBO.getAssociateCode(), 1,adminBO.getTimeZone(),"");
		request.setAttribute("openBo", openBo);
		request.setAttribute("trips", arrList);
		request.setAttribute("drivers", drivers);
		getServletContext().getRequestDispatcher("/jsp/OpenRequestReview.jsp").forward(request, response);
	}
	/**
	 * @author Venki
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	public void operatorShift(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request
				.getSession().getAttribute("user");
		OperatorShift osBean = new OperatorShift();
		String error="";
		// First Case When Going Into the Screen
		if (request.getParameter("openShift") == null && request.getParameter("startBreak")==null && request.getParameter("endBreak")==null && request.getParameter("CloseShift")==null ) {
			ArrayList<OperatorShift> registerOpen = RequestDAO.readAllOperator(adminBO.getMasterAssociateCode(),"");
			request.setAttribute("csregBO", registerOpen);
			request.setAttribute("screen", "/OperatorShift/OperatorShift.jsp");
		} else if (request.getParameter("openShift") != null) {
			osBean.setAssociateCode(adminBO.getMasterAssociateCode());
			osBean.setOperatorId(request.getParameter("OperatorId"));
			// After enter Operator Id check whether the register is opened or closed
			OperatorShift registerOpen = RequestDAO.readShiftRegisterMaster(
					osBean.getAssociateCode(), osBean.getOperatorId());
			if (registerOpen.isRegisterOpen()) {
				// Register is opened
				error="Shift Already Opened For This Operator";
				ArrayList<OperatorShift> openRegister = RequestDAO.readAllOperator(adminBO.getMasterAssociateCode(),"");
				request.setAttribute("csregBO", openRegister);
				request.setAttribute("error",error);
				request.setAttribute("screen", "/OperatorShift/OperatorShift.jsp");
			} else {
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(request.getParameter("OperatorId"));
				osBean.setStatus("A");
				osBean.setOpenedBy(adminBO.getUid());
				if(request.getParameter("responseForMain")!=null){
					osBean.setOperatorId(adminBO.getUid());
					osBean.setAssociateCode(adminBO.getMasterAssociateCode());
					osBean.setStatus("A");
					osBean.setOpenedBy(adminBO.getUid());
					if(osBean.getOperatorId().equals(adminBO.getUid())){
						adminBO.setShiftMode(1);
					}
				}

				int uniqueKey=RequestDAO.insertShiftRegisterMaster(osBean);
				if(request.getParameter("responseForMain")!=null){
					response.getWriter().write(uniqueKey+"");
					return;
				}else{
					ArrayList<OperatorShift> openRegister = RequestDAO.readAllOperator(adminBO.getAssociateCode(),"");
					request.setAttribute("csregBO", openRegister);
					request.setAttribute("error",error);
					request.setAttribute("screen", "/OperatorShift/OperatorShift.jsp");
				}
			}
		}else if (request.getParameter("startBreak")!=null) {
			if(request.getParameter("responseForMain")!=null){
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setStatus("A");
				osBean.setKey(Integer.parseInt(request.getParameter("key")));
				osBean.setClosedBy(adminBO.getUid());
			}else{
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(request.getParameter("OperatorId"));
				osBean.setStatus(request.getParameter("status"));
				osBean.setKey(Integer.parseInt(request.getParameter("key")));
				osBean.setClosedBy(adminBO.getUid());
			}
			boolean result=RequestDAO.updateShiftRegisterDetail(osBean);
			if(result==true){
				if(osBean.getOperatorId().equals(adminBO.getUid())){
					adminBO.setShiftMode(2);
				}
			}
			if(request.getParameter("responseForMain")!=null){
				response.getWriter().write(result+"");
				return;
			}else{
				ArrayList<OperatorShift> registerOpen = RequestDAO.readAllOperator(adminBO.getAssociateCode(),"");
				request.setAttribute("csregBO", registerOpen);
				request.setAttribute("error",error);
				request.setAttribute("screen", "/OperatorShift/OperatorShift.jsp");
			}
		} else if (request.getParameter("endBreak") != null) {
			if(request.getParameter("responseForMain")!=null){
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setStatus("B");
				osBean.setKey(Integer.parseInt(request.getParameter("key")));
				osBean.setClosedBy(adminBO.getUid());
			}else{
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setStatus(request.getParameter("status"));
				osBean.setOperatorId(request.getParameter("OperatorId"));
				osBean.setKey(Integer.parseInt(request.getParameter("key")));
				osBean.setClosedBy(adminBO.getUid());
			}
			boolean result=RequestDAO.updateShiftRegisterDetail(osBean);
			if(result==true){
				if(osBean.getOperatorId().equals(adminBO.getUid())){
					adminBO.setShiftMode(1);
				}
			}
			if(request.getParameter("responseForMain")!=null){
				response.getWriter().write(result+"");
				return;
			}else{
				ArrayList<OperatorShift> registerOpen = RequestDAO.readAllOperator(adminBO.getAssociateCode(),"");
				request.setAttribute("csregBO", registerOpen);
				request.setAttribute("error",error);
				request.setAttribute("screen", "/OperatorShift/OperatorShift.jsp");
			}
		} else if (request.getParameter("CloseShift") != null) {
			if(request.getParameter("responseForMain")!=null){
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setOperatorId(adminBO.getUid());
				osBean.setStatus("L");
				osBean.setKey(Integer.parseInt(request.getParameter("key")));
				osBean.setClosedBy(adminBO.getUid());
			}else{
				osBean.setAssociateCode(adminBO.getMasterAssociateCode());
				osBean.setStatus(request.getParameter("status"));
				osBean.setOperatorId(request.getParameter("OperatorId"));
				osBean.setKey(Integer.parseInt(request.getParameter("key")));
				osBean.setClosedBy(adminBO.getUid());
			}
			boolean result=RequestDAO.closeShiftRegisterMaster(osBean);
			if(result==true){
				if(osBean.getOperatorId().equals(adminBO.getUid())){
					adminBO.setShiftMode(0);
				}
			}
			if(request.getParameter("responseForMain")!=null){
				response.getWriter().write(result+"");
				return;
			}else{
				ArrayList<OperatorShift> registerOpen = RequestDAO.readAllOperator(adminBO.getAssociateCode(),"");
				request.setAttribute("csregBO", registerOpen);
				request.setAttribute("error",error);
				request.setAttribute("screen", "/OperatorShift/OperatorShift.jsp");
			}
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}


	public void operatorShiftReview(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		OperatorShift osrBean = new OperatorShift();
		// First Case When Going Into the Screen
		if (request.getParameter("getDetails") == null) {
			ArrayList<OperatorShift> registerOpen=new ArrayList<OperatorShift>();
			request.setAttribute("csregBO", registerOpen);
			request.setAttribute("screen", "/OperatorShift/OperatorShiftReview.jsp");
		} else if(request.getParameter("getDetails") != null){
			String operatorId=request.getParameter("operatorId");
			String date=TDSValidation.getTimeStampDateFormat(request.getParameter("date"));
			ArrayList<OperatorShift> registerOpen = RequestDAO.readOperatorShift(adminBO.getAssociateCode(),operatorId,date);
			request.setAttribute("csregBO", registerOpen);
			request.setAttribute("screen", "/OperatorShift/OperatorShiftReview.jsp");
		}
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}


	public void openRequestHistoryWithLogs(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		OpenRequestBO openRequestHistory =new OpenRequestBO();
		ArrayList<ChargesBO> chargeType=ChargesDAO.getChargeTypes(adminBO.getMasterAssociateCode(),0);
		request.setAttribute("chargeTypes", chargeType);
		HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode()+"Flags");
		if(companyFlags==null){
			SystemUtils.reloadCompanyFlags(this, adminBO.getMasterAssociateCode());
			companyFlags = (HashMap<String, DriverVehicleBean>) getServletContext().getAttribute(adminBO.getMasterAssociateCode()+"Flags");
		}
		request.setAttribute("al_q", CheckZone.returnZonesForAssociatoinCode(getServletContext(), adminBO.getMasterAssociateCode()));
		request.setAttribute("companyFlags", companyFlags);
		//		if(request.getParameter("button")!=null){
		//			String value =request.getParameter("value")==null?"":request.getParameter("value");
		//			String fromDate=request.getParameter("fromDate");
		//			String toDate=request.getParameter("toDate");
		//			String fromDateFormatted=TDSValidation.getTimeStampDateFormat(fromDate);
		//			String toDateFormatted=TDSValidation.getTimeStampDateFormat(toDate);
		//			ArrayList<OpenRequestBO>openReqHistory=RequestDAO.openRequestHistory(openRequestHistory,adminBO,value,fromDateFormatted,toDateFormatted);
		//			request.setAttribute("openRequestHistory", openReqHistory);
		request.setAttribute("fDate", "");
		request.setAttribute("tDate", "");
		//		}
		request.setAttribute("screen", "/jsp/OpenRequestHistory.jsp"); 
		getServletContext().getRequestDispatcher("/jsp/OpenRequestHistory.jsp").forward(request, response);
	}
	public void openRequestSummaryMain(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		OpenRequestBO openRequestSummary = new OpenRequestBO();
		String fromDate=request.getParameter("fDate")==null?"":request.getParameter("fDate");
		String toDate=request.getParameter("tDate")==null?"":request.getParameter("tDate");
		String value= request.getParameter("keyWord")==null?"":request.getParameter("keyWord");
		String fromDateFormatted=TDSValidation.getTimeStampDateFormat(fromDate);
		String toDateFormatted=TDSValidation.getTimeStampDateFormat(toDate);
		if(request.getParameter("submit")!=null){
			openRequestSummary = new OpenRequestBO();
			HashMap<String, DriverVehicleBean> companyFlags = (HashMap<String, DriverVehicleBean>) this.getServletContext().getAttribute(adminBO.getMasterAssociateCode()+"Flags");
			ArrayList<OpenRequestBO>openReqSummary=RequestDAO.openRequestSummary(openRequestSummary,adminBO,value,fromDateFormatted,toDateFormatted,3,"");
			request.setAttribute("allFlags", companyFlags);
			request.setAttribute("openRequestSummary", openReqSummary);
			request.setAttribute("value", value);
			request.setAttribute("fDate", fromDate);
			request.setAttribute("tDate", toDate);
		}
		request.setAttribute("screen", "/jsp/openRequestSummary.jsp"); 
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}
	public void fileUploadForOR(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException  {
		//	ArrayList<OpenRequestBO> orFields = new ArrayList<OpenRequestBO>();
		ArrayList<OpenRequestFieldOrderBO> VendorList= new ArrayList<OpenRequestFieldOrderBO>();
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		String fn="";  
		StringBuffer errorMsg = new StringBuffer();
		//int result=0;

		String errorReturn="";
		int newNumOfTrips = 0;
		String lastExternalRouteProcessed = "";
		int currentRouteID = 0;
		//String error="";
		VendorList=SystemPropertiesDAO.getVendorList(adminBO);
		String fleetNo = "0";
		//Starting to Write the file in a specific Folder.
		int n=0;
		if(request.getParameter("uploadFile")!=null){ 

			//The path is given here to store the file.
			String path = "/tmp";				
			try{
				//MultiPart requestrenamed it into name value pair called RemoteIpValue
				MultipartFormDataRequest mrequest = new MultipartFormDataRequest(request);
				//getting the files
				Hashtable files = mrequest.getFiles();
				//Getting the path for the file.
				UploadFile file = ((UploadFile) files.get("filePath")); 

				if (MultipartFormDataRequest.isMultipartFormData(request)){
					// Uses MultipartFormDataRequest to parse the HTTP request.
					if ( (files != null) && (!files.isEmpty()) ){
						UploadBean upBean=new UploadBean();
						upBean.setFolderstore(path);
						upBean.store(mrequest); 
						Vector history = upBean.getHistory();
						UploadParameters up = (UploadParameters) history.elementAt(0);
						if(up.getAltFilename()!=null){
							fn=up.getAltFilename();
						}else{
							fn=((UploadFile) files.get("filePath")).getFileName();
						}
						if (file != null) 
							request.setAttribute("fileName", file.getFileName() );
					}
				}
				// Open the file that is the first 
				// command line parameter
				//csv file containing data
				String strFile = path+"/"+fn;
				//create BufferedReader to read csv file
				BufferedReader br = new BufferedReader( new FileReader(strFile));
				String strLine = "";
				String routeNumber="";
				int routeId=0;
				int numberOfTrips=1;

				//int count = 0;
				String[] values=null;
				FileInputStream fstream = new FileInputStream(strFile);
				// Get the object of DataInputStream
				DataInputStream in = new DataInputStream(fstream);
				br = new BufferedReader(new InputStreamReader(in));
				//Read File Line By Line
				/*File f = new File(strFile);
					Scanner input = new Scanner(f);
					while (input.hasNextLine()) {
						String line = input.nextLine();
						count++;
					}*/
				String vendor=request.getParameter("vendor")==null?"":request.getParameter("vendor");
				String sharedRide=request.getParameter("sharedRide")==null?"":"1";
				//System.out.println("Shared Ride--->"+sharedRide);
				int dontDispatch=0;
				if(request.getParameter("dontDispatch")!=null && !request.getParameter("dontDispatch").equals("") ){
					dontDispatch=1;
				}

				if(request.getParameter("fleetSize")!=null && !request.getParameter("fleetSize").equalsIgnoreCase("0")){
					fleetNo=((request.getParameter("fleetno")!=null && !request.getParameter("fleetno").equalsIgnoreCase("0"))?request.getParameter("fleetno"):"0");
				}else{
					fleetNo="0";
				}
				OpenRequestBO ORBO =new OpenRequestBO();
				OpenRequestFieldOrderBO orFieldPositionBO =new OpenRequestFieldOrderBO();
				orFieldPositionBO= SystemPropertiesDAO.getORFieldsOrder(adminBO.getAssociateCode(),vendor);

				while ((strLine = br.readLine()) != null){
					// Print the content on the console
					if(orFieldPositionBO.getSeparatedBy().equals("1")){
						values=strLine.split(",");
					}else if(orFieldPositionBO.getSeparatedBy().equals("2")){
						values=strLine.split(";");
					}else if(orFieldPositionBO.getSeparatedBy().equals("3")){
						values=strLine.split("\";\"");
					}else if(orFieldPositionBO.getSeparatedBy().equals("4")){
						values=strLine.split("\",\"");
					}
					if(strLine!=null && !strLine.equalsIgnoreCase("")){
						if(!orFieldPositionBO.getTripid().equals("") ){
							if(orFieldPositionBO.getTripid().contains(";")){
								String[] tripId=orFieldPositionBO.getTripid().split(";");
								for(int i=0;i<tripId.length;i++){
									if(i>0){
										ORBO.setTripid(ORBO.getTripid()+" "+values[Integer.parseInt(tripId[i])-1].replace("\"", ""));

									}else{
										ORBO.setTripid(values[Integer.parseInt(tripId[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setTripid(values[Integer.parseInt(orFieldPositionBO.getTripid())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getName().equals("") ){
							if(orFieldPositionBO.getName().contains(";")){
								String[] name=orFieldPositionBO.getName().split(";");
								for(int i=0;i<name.length;i++){
									if(i>0){
										ORBO.setName(ORBO.getName()+" "+values[Integer.parseInt(name[i])-1].replace("\"", ""));
									}else{
										ORBO.setName(values[Integer.parseInt(name[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setName(values[Integer.parseInt(orFieldPositionBO.getName())-1].replace("\"", ""));
							}
						}
						/*	if(!orFieldPositionBO.getRouteNumber().equals("")){
							if(orFieldPositionBO.getRouteNumber().contains(";")){
								String[] routeNumber=orFieldPositionBO.getRouteNumber().split(";");
								for(int i=0;i<routeNumber.length;i++){
									if(i>0){
										ORBO.setRouteNumber(ORBO.getRouteNumber()+" "+values[Integer.parseInt(routeNumber[i])-1].replace("\"", "").replace(" ", "").replace("(", "").replace(")", "").replace("-", ""));
									}else{
										ORBO.setPhone(values[Integer.parseInt(routeNumber[i])-1].replace("\"", "").replace(" ", "").replace("(", "").replace(")", "").replace("-", ""));
									}
								}
							}else{
								ORBO.setRouteNumber(values[Integer.parseInt(orFieldPositionBO.getRouteNumber())-1].replace("\"", "").replace(" ", "").replace("(", "").replace(")", "").replace("-", ""));
							}
						}*/
						if(!orFieldPositionBO.getPhone().equals("")){
							if(orFieldPositionBO.getPhone().contains(";")){
								String[] phone=orFieldPositionBO.getPhone().split(";");
								for(int i=0;i<phone.length;i++){
									if(i>0){
										ORBO.setPhone(ORBO.getPhone()+" "+values[Integer.parseInt(phone[i])-1].replace("\"", "").replace(" ", "").replace("(", "").replace(")", "").replace("-", ""));
									}else{
										ORBO.setPhone(values[Integer.parseInt(phone[i])-1].replace("\"", "").replace(" ", "").replace("(", "").replace(")", "").replace("-", ""));
									}
								}
							}else{
								ORBO.setPhone(values[Integer.parseInt(orFieldPositionBO.getPhone())-1].replace("\"", "").replace(" ", "").replace("(", "").replace(")", "").replace("-", ""));
							}
						}
						if(!orFieldPositionBO.getSadd1().equals("")){
							if(orFieldPositionBO.getSadd1().contains(";")){
								String[] sAdd1=orFieldPositionBO.getSadd1().split(";");
								for(int i=0;i<sAdd1.length;i++){
									if(i>0){
										ORBO.setSadd1(ORBO.getSadd1()+" "+values[Integer.parseInt(sAdd1[i])-1].replace("\"", ""));
									}else{
										ORBO.setSadd1(values[Integer.parseInt(sAdd1[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setSadd1(values[Integer.parseInt(orFieldPositionBO.getSadd1())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getSadd2().equals("")){
							if(orFieldPositionBO.getSadd2().contains(";")){
								String[] sAdd2=orFieldPositionBO.getSadd2().split(";");
								for(int i=0;i<sAdd2.length;i++){
									if(i>0){
										ORBO.setSadd2(ORBO.getSadd2()+" "+values[Integer.parseInt(sAdd2[i])-1].replace("\"", ""));
									}else{
										ORBO.setSadd2(values[Integer.parseInt(sAdd2[i])-1].replace("\"", ""));
									}	
								}
							}else{
								ORBO.setSadd2(values[Integer.parseInt(orFieldPositionBO.getSadd2())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getScity().equals("") ){
							if(orFieldPositionBO.getScity().contains(";")){
								String[] sCity=orFieldPositionBO.getScity().split(";");
								for(int i=0;i<sCity.length;i++){
									if(i>0){
										ORBO.setScity(ORBO.getScity()+" "+values[Integer.parseInt(sCity[i])-1].replace("\"", ""));
									}else{
										ORBO.setScity(values[Integer.parseInt(sCity[i])-1].replace("\"", ""));
									}	
								}
							}else{
								ORBO.setScity(values[Integer.parseInt(orFieldPositionBO.getScity())-1].replace("\"", ""));
							}
						}
						if(orFieldPositionBO.getSlat()!=null && !orFieldPositionBO.getSlat().equals("")){
							if(orFieldPositionBO.getSlat().contains(";")){
								String[] sLat=orFieldPositionBO.getSlat().split(";");
								for(int i=0;i<sLat.length;i++){
									ORBO.setSlat(values[Integer.parseInt(sLat[i])-1].replace("\"", ""));
									if(!ORBO.getSlat().contains(".")){
										Double sLatitude=Double.parseDouble(ORBO.getSlat());
										sLatitude=sLatitude/1000000;
										ORBO.setSlat(sLatitude.toString());
									}
								}
							}else{
								ORBO.setSlat(values[Integer.parseInt(orFieldPositionBO.getSlat())-1].replace("\"", ""));
								if(!ORBO.getSlat().contains(".")){
									Double sLatitude=Double.parseDouble(ORBO.getSlat());
									sLatitude=sLatitude/1000000;
									ORBO.setSlat(sLatitude.toString());
								}
							}
						}else{
							ORBO.setSlat("0");
						}
						if(orFieldPositionBO.getSlong()!=null && !orFieldPositionBO.getSlong().equals("")){
							if(orFieldPositionBO.getSlong().contains(";")){
								String[] sLong=orFieldPositionBO.getSlong().split(";");
								for(int i=0;i<sLong.length;i++){
									ORBO.setSlat(values[Integer.parseInt(sLong[i])-1].replace("\"", ""));
									if(!ORBO.getSlat().contains(".")){
										Double sLongitude=Double.parseDouble(ORBO.getSlat());
										sLongitude=sLongitude/1000000;
										ORBO.setSlat(sLongitude.toString());
									}
								}
							}else{
								ORBO.setSlong(values[Integer.parseInt(orFieldPositionBO.getSlong())-1].replace("\"", ""));
								if(!ORBO.getSlong().contains(".")){
									Double sLongitude=Double.parseDouble(ORBO.getSlong());
									sLongitude=sLongitude/1000000;
									ORBO.setSlong(sLongitude.toString());
								}
							}
						}else{
							ORBO.setSlong("0");
						}
						if(!orFieldPositionBO.getEadd1().equals("")){
							if(orFieldPositionBO.getEadd1().contains(";")){
								String[] eAdd1=orFieldPositionBO.getEadd1().split(";");
								for(int i=0;i<eAdd1.length;i++){
									if(i>0){
										ORBO.setEadd1(ORBO.getEadd1()+" "+values[Integer.parseInt(eAdd1[i])-1].replace("\"", ""));
									}else{
										ORBO.setEadd1(values[Integer.parseInt(eAdd1[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setEadd1(values[Integer.parseInt(orFieldPositionBO.getEadd1())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getEadd2().equals("")){
							if(orFieldPositionBO.getEadd2().contains(";")){
								String[] eAdd2=orFieldPositionBO.getEadd2().split(";");
								for(int i=0;i<eAdd2.length;i++){
									if(i>0){
										ORBO.setEadd2(ORBO.getEadd2()+" "+values[Integer.parseInt(eAdd2[i])-1].replace("\"", ""));
									}else{
										ORBO.setEadd2(values[Integer.parseInt(eAdd2[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setEadd2(values[Integer.parseInt(orFieldPositionBO.getEadd2())-1].replace("\"", ""));
							}
						}
						if(orFieldPositionBO.getEdlatitude()!=null && !orFieldPositionBO.getEdlatitude().equals("")){
							if(orFieldPositionBO.getEdlatitude().contains(";")){
								String[] eLat=orFieldPositionBO.getEdlatitude().split(";");
								for(int i=0;i<eLat.length;i++){
									ORBO.setEdlatitude(values[Integer.parseInt(eLat[i])-1].replace("\"", ""));
									if(!ORBO.getEdlatitude().contains(".")){
										Double eLatitude=Double.parseDouble(ORBO.getEdlatitude());
										eLatitude=eLatitude/1000000;
										ORBO.setEdlatitude(eLatitude.toString());
									}
								}
							}else{
								ORBO.setEdlatitude(values[Integer.parseInt(orFieldPositionBO.getEdlatitude())-1].replace("\"", ""));
								if(!ORBO.getEdlatitude().contains(".")){
									Double eLatitude=Double.parseDouble(ORBO.getEdlatitude());
									eLatitude=eLatitude/1000000;
									ORBO.setEdlatitude(eLatitude.toString());
								}
							}
						}else{
							ORBO.setEdlatitude("0");
						}
						if(orFieldPositionBO.getEdlongitude()!=null && !orFieldPositionBO.getEdlongitude().equals("")){
							if(orFieldPositionBO.getEdlongitude().contains(";")){
								String[] eLong=orFieldPositionBO.getEdlongitude().split(";");
								for(int i=0;i<eLong.length;i++){
									ORBO.setEdlongitude(values[Integer.parseInt(eLong[i])-1].replace("\"", ""));
									if(!ORBO.getEdlongitude().contains(".")){
										Double eLongitude=Double.parseDouble(ORBO.getEdlongitude());
										eLongitude=eLongitude/1000000;
										ORBO.setEdlongitude(eLongitude.toString());
									}
								}
							}else{
								ORBO.setEdlongitude(values[Integer.parseInt(orFieldPositionBO.getEdlongitude())-1].replace("\"", ""));
								if(!ORBO.getEdlongitude().contains(".")){
									Double eLongitude=Double.parseDouble(ORBO.getEdlongitude());
									eLongitude=eLongitude/1000000;				
									ORBO.setEdlongitude(eLongitude.toString());
								}
							}
						}else{
							ORBO.setEdlongitude("0");
						}
						if(!orFieldPositionBO.getEcity().equals("")){
							if(orFieldPositionBO.getEcity().contains(";")){
								String[] eCity=orFieldPositionBO.getEcity().split(";");
								for(int i=0;i<eCity.length;i++){
									if(i>0){
										ORBO.setEcity(ORBO.getEcity()+" "+values[Integer.parseInt(eCity[i])-1].replace("\"", ""));
									}else{
										ORBO.setEcity(values[Integer.parseInt(eCity[i])-1].replace("\"", ""));
									}	
								}
							}else{
								ORBO.setEcity(values[Integer.parseInt(orFieldPositionBO.getEcity())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getShrs().equals("")){
							if(orFieldPositionBO.getShrs().contains(";")){
								String[] shours=orFieldPositionBO.getShrs().split(";");
								for(int i=0;i<shours.length;i++){
									ORBO.setShrs(values[Integer.parseInt(shours[i])-1].replace("\"", ""));
									if(ORBO.getShrs().contains(":")){
										//										String[] hrs=ORBO.getShrs().split(":");
										//										int hours=Integer.parseInt(hrs[0]);
										//										if(hours>=24){
										//											hours = hours -24;
										//										}
										//										String shrs=hours+hrs[1];
										ORBO.setShrs(ORBO.getShrs().replace(":", ""));
									}
								}
							}else{
								ORBO.setShrs(values[Integer.parseInt(orFieldPositionBO.getShrs())-1].replace("\"", ""));
								if(ORBO.getShrs().contains(":")){
									//String[] hrs=ORBO.getShrs().split(":");
									//									int hours=Integer.parseInt(hrs[0]);
									//									if(hours>=24){
									//										hours = hours -24;
									//									}
									//String shrs=hours+hrs[1];
									ORBO.setShrs(ORBO.getShrs().replace(":", ""));
								}
							}
						}
						if(!orFieldPositionBO.getSdate().equals("")){
							//System.out.println("orfieldpositon"+orFieldPositionBO.getSdate());
							if(orFieldPositionBO.getSdate().contains(";")){
								String[] sDate=orFieldPositionBO.getSdate().split(";");
								for(int i=0;i<sDate.length;i++){
									ORBO.setSdate(values[Integer.parseInt(sDate[i])-1].replace("\"", ""));
								}
							}else{
								if(!orFieldPositionBO.getShrs().equals("")){
									ORBO.setSdate(values[Integer.parseInt(orFieldPositionBO.getSdate())-1].replace("\"", ""));
								}else{
									ORBO.setSdate(values[Integer.parseInt(orFieldPositionBO.getSdate())-1].replace("\"", ""));
								}
							}

							/*if(ORBO.getSdate().contains(";")){
								String[] sDate=ORBO.getSdate().split(" ");
								for(int i=0;i<sDate.length;i++){
									System.out.println("sdate_"+i+":"+sDate[i]);
									if(i==0){
										ORBO.setSdate(values[Integer.parseInt(sDate[i])-1].replace("\"", ""));
									}else{
										ORBO.setShrs(values[Integer.parseInt(sDate[i])-1].replace("\"", ""));
										StringBuilder time;
										String hrs;
										String min;
										if(sDate[i].contains("AM")){

										}else if(sDate[i].contains("PM")){
											String[] dateSplit=sDate[i].split(":");
											for(int j=0;j<dateSplit.length;j++){
												if(j==0){
													hrs = (Integer.parseInt(dateSplit[i])+12)+"";
												}else{
													String[] minSpliter = dateSplit[i].split(" ");

												}
											}
										}else{

										}
									}
								}
							}*/

							//System.out.println("orbo date"+ORBO.getSdate());
							String date= TDSValidation.dateFormatmmddyyyy(ORBO.getSdate(),orFieldPositionBO.getDateFormat());
							//ORBO.setSdate(date);

							//System.out.println("date"+date);
							if(orFieldPositionBO.getShrs().equals("")){
								if(date.length()>10){
									//String lenghthOfDate = ORBO.getSdate().length()+"";
									String[] sDate=date.split(" ");
									for(int i=0;i<sDate.length;i++){
										//System.out.println("sdate_"+i+":"+sDate[i]);
										if(!sDate[i].equals("")){
											if(sDate[i].contains("/")){
												if(sDate[i].length()==10){
													ORBO.setSdate(sDate[i]);
												}else{
													String month  = sDate[i].substring(0,sDate[i].indexOf("/"));
													String day = sDate[i].substring(sDate[i].indexOf("/")+1,sDate[i].length()-5);
													String year = sDate[i].substring(sDate[i].length()-4,sDate[i].length());
													//System.out.println("before format");
													//System.out.println("month:day:year"+month+":"+day+":"+year);
													if(month.length()!=2){
														month = 0+month;
													}if(day.length()!=2){
														day = 0+day;
													}
													//System.out.println("after format");
													//System.out.println("month:day:year"+month+":"+day+":"+year);
													ORBO.setSdate(month+"/"+day+"/"+year);
												}
											}else{
												if(sDate[i].contains("AM") || sDate[i].contains("PM")){
													//System.out.println("Befre format");
													int hour= Integer.parseInt(sDate[i].substring(0,sDate[i].indexOf(":")));
													//System.out.println("hour"+hour);
													int min= Integer.parseInt(sDate[i].substring(sDate[i].indexOf(":")+1,sDate[i].length()-5));
													//System.out.println("min"+min);
													if(sDate[i].contains("AM")){
														if(hour==12){
															hour=0;
														}
													}else{
														if(hour!=12){
															hour=hour+12;
														}
													}
													//System.out.println("afterFormat");
													//System.out.println("hour"+hour);
													//System.out.println("min"+min);
													ORBO.setShrs(hour+""+min);
												}else if(sDate[i].contains(":")){
													ORBO.setShrs(sDate[i].replace(":", ""));
												}else{
													ORBO.setShrs(sDate[i]);
												}
											}
										}
									}
								}else{
									ORBO.setSdate(date);
								}
							}else{
								ORBO.setSdate(date);
							}
							//System.out.println("date:time - "+ORBO.getSdate()+":"+ORBO.getShrs());
						}

						if(!orFieldPositionBO.getPaytype().equals("")){
							if(orFieldPositionBO.getPaytype().contains(";")){
								String[] payType=orFieldPositionBO.getPaytype().split(";");
								for(int i=0;i<payType.length;i++){
									ORBO.setPaytype(values[Integer.parseInt(payType[i])-1].replace("\"", ""));
								}
							}else{
								ORBO.setPaytype(values[Integer.parseInt(orFieldPositionBO.getPaytype())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getAcct().equals("")){
							if(orFieldPositionBO.getAcct().contains(";")){
								String[] account=orFieldPositionBO.getAcct().split(";");
								for(int i=0;i<account.length;i++){
									if(i>0){
										ORBO.setAcct(ORBO.getAcct()+" "+values[Integer.parseInt(account[i])-1].replace("\"", ""));
									}else{
										ORBO.setAcct(values[Integer.parseInt(account[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setAcct(values[Integer.parseInt(orFieldPositionBO.getAcct())-1].replace("\"", ""));
							}
						}
						if(orFieldPositionBO.getAmt()!=null && !orFieldPositionBO.getAmt().equals("")){
							if(orFieldPositionBO.getAmt().contains(";")){
								String[] amount=orFieldPositionBO.getAmt().split(";");
								for(int i=0;i<amount.length;i++){
									String value=values[Integer.parseInt(amount[i])-1].replace("\"", "");
									if(!value.equals("")){
										BigDecimal amt =new BigDecimal(value);
										ORBO.setAmt(amt);
									}else{
										ORBO.setAmt(new BigDecimal("0"));
									}
								}
							}else{
								String value=values[Integer.parseInt(orFieldPositionBO.getAmt())-1].replace("\"", "");
								if(!value.equals("")){
									BigDecimal amt =new BigDecimal(value);
									ORBO.setAmt(amt);
								}else{
									ORBO.setAmt(new BigDecimal("0"));
								}
							}
						}else{
							ORBO.setAmt(new BigDecimal("0"));
						}
						if(!sharedRide.equals("")){
							ORBO.setTypeOfRide("1");
						}else{
							ORBO.setTypeOfRide("0");
						}
						if(!orFieldPositionBO.getNumberOfPassengers().equals("")){
							if(orFieldPositionBO.getNumberOfPassengers().contains(";")){
								String[] numOfPass=orFieldPositionBO.getSharedRide().split(";");
								for(int i=0;i<numOfPass.length;i++){
									ORBO.setNumOfPassengers(Integer.parseInt((values[Integer.parseInt(orFieldPositionBO.getNumberOfPassengers())-1])));
								}
							}else{
								ORBO.setNumOfPassengers(Integer.parseInt((values[Integer.parseInt(orFieldPositionBO.getNumberOfPassengers())-1]).replace("\"", "")));
							}
						}else{
							ORBO.setNumOfPassengers(0);
						}
						if(!orFieldPositionBO.getCallerPhone().equals("") ){
							if(orFieldPositionBO.getCallerPhone().contains(";")){
								String[] phone=orFieldPositionBO.getCallerPhone().split(";");
								for(int i=0;i<phone.length;i++){
									if(i>0){
										ORBO.setCallerPhone(ORBO.getName()+" "+values[Integer.parseInt(phone[i])-1].replace("\"", ""));
									}else{
										ORBO.setCallerPhone(values[Integer.parseInt(phone[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setCallerPhone(values[Integer.parseInt(orFieldPositionBO.getCallerPhone())-1].replace("\"", ""));
							}
						}						
						if(!orFieldPositionBO.getPickUpOrder().equals("") ){
							if(orFieldPositionBO.getPickUpOrder().contains(";")){
								String[] pickUp=orFieldPositionBO.getPickUpOrder().split(";");
								for(int i=0;i<pickUp.length;i++){
									if(i>0){
										ORBO.setPickupOrder(ORBO.getName()+" "+values[Integer.parseInt(pickUp[i])-1].replace("\"", ""));
									}else{
										ORBO.setPickupOrder(values[Integer.parseInt(pickUp[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setPickupOrder(values[Integer.parseInt(orFieldPositionBO.getPickUpOrder())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getDropOffOrder().equals("") ){
							if(orFieldPositionBO.getDropOffOrder().contains(";")){
								String[] dropOff=orFieldPositionBO.getDropOffOrder().split(";");
								for(int i=0;i<dropOff.length;i++){
									if(i>0){
										ORBO.setDropOffOrder(ORBO.getName()+" "+values[Integer.parseInt(dropOff[i])-1].replace("\"", ""));
									}else{
										ORBO.setDropOffOrder(values[Integer.parseInt(dropOff[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setDropOffOrder(values[Integer.parseInt(orFieldPositionBO.getDropOffOrder())-1].replace("\"", ""));
							}
						}

						if(!orFieldPositionBO.getRefNumber().equals("") ){
							if(orFieldPositionBO.getRefNumber().contains(";")){
								String[] refNum=orFieldPositionBO.getRefNumber().split(";");
								for(int i=0;i<refNum.length;i++){
									if(i>0){
										ORBO.setRefNumber(ORBO.getName()+" "+values[Integer.parseInt(refNum[i])-1].replace("\"", ""));
									}else{
										ORBO.setRefNumber(values[Integer.parseInt(refNum[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setRefNumber(values[Integer.parseInt(orFieldPositionBO.getRefNumber())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getRefNumber1().equals("") ){
							if(orFieldPositionBO.getRefNumber1().contains(";")){
								String[] refNum=orFieldPositionBO.getRefNumber1().split(";");
								for(int i=0;i<refNum.length;i++){
									if(i>0){
										ORBO.setRefNumber1(ORBO.getName()+" "+values[Integer.parseInt(refNum[i])-1].replace("\"", ""));
									}else{
										ORBO.setRefNumber1(values[Integer.parseInt(refNum[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setRefNumber1(values[Integer.parseInt(orFieldPositionBO.getRefNumber1())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getRefNumber2().equals("") ){
							if(orFieldPositionBO.getRefNumber2().contains(";")){
								String[] refNum=orFieldPositionBO.getRefNumber2().split(";");
								for(int i=0;i<refNum.length;i++){
									if(i>0){
										ORBO.setRefNumber2(ORBO.getName()+" "+values[Integer.parseInt(refNum[i])-1].replace("\"", ""));
									}else{
										ORBO.setRefNumber2(values[Integer.parseInt(refNum[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setRefNumber2(values[Integer.parseInt(orFieldPositionBO.getRefNumber2())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getRefNumber3().equals("") ){
							if(orFieldPositionBO.getRefNumber3().contains(";")){
								String[] refNum=orFieldPositionBO.getRefNumber3().split(";");
								for(int i=0;i<refNum.length;i++){
									if(i>0){
										ORBO.setRefNumber3(ORBO.getName()+" "+values[Integer.parseInt(refNum[i])-1].replace("\"", ""));
									}else{
										ORBO.setRefNumber3(values[Integer.parseInt(refNum[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setRefNumber3(values[Integer.parseInt(orFieldPositionBO.getRefNumber3())-1].replace("\"", ""));
							}
						}

						if(!orFieldPositionBO.getCustomField().equals("") ){
							if(orFieldPositionBO.getCustomField().contains(";")){
								String[] customField=orFieldPositionBO.getCustomField().split(";");
								for(int i=0;i<customField.length;i++){
									if(i>0){
										ORBO.setCustomField(ORBO.getName()+" "+values[Integer.parseInt(customField[i])-1].replace("\"", ""));
									}else{
										ORBO.setCustomField(values[Integer.parseInt(customField[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setCustomField(values[Integer.parseInt(orFieldPositionBO.getCustomField())-1].replace("\"", ""));
							}
						}

						if(!orFieldPositionBO.getOperatorComments().equals("") ){
							if(orFieldPositionBO.getOperatorComments().contains(";")){
								String[] oprComments=orFieldPositionBO.getOperatorComments().split(";");
								for(int i=0;i<oprComments.length;i++){
									if(i>0){
										ORBO.setComments(ORBO.getName()+" "+values[Integer.parseInt(oprComments[i])-1].replace("\"", ""));
									}else{
										ORBO.setComments(values[Integer.parseInt(oprComments[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setComments(values[Integer.parseInt(orFieldPositionBO.getOperatorComments())-1].replace("\"", ""));
							}
						}
						if(!orFieldPositionBO.getDispatchComments().equals("") ){
							if(orFieldPositionBO.getDispatchComments().contains(";")){
								String[] driverComments=orFieldPositionBO.getDispatchComments().split(";");
								for(int i=0;i<driverComments.length;i++){
									if(i>0){
										ORBO.setSpecialIns(ORBO.getName()+" "+values[Integer.parseInt(driverComments[i])-1].replace("\"", ""));
									}else{
										ORBO.setSpecialIns(values[Integer.parseInt(driverComments[i])-1].replace("\"", ""));
									}
								}
							}else{
								ORBO.setSpecialIns(values[Integer.parseInt(orFieldPositionBO.getDispatchComments())-1].replace("\"", ""));
							}
						}
						ORBO.setTripSource(TDSConstants.uploadFile);
						if(fleetNo!=null && !fleetNo.equalsIgnoreCase("0")){
							request.setAttribute("assoCode", fleetNo);
						}
						ORBO.setDays("0000000");
						if(!sharedRide.equals("") && sharedRide!=null){
							ORBO.setTypeOfRide("1");
						}
						ORBO.setDontDispatch(dontDispatch);
						String noPhone =request.getParameter("noPhone")==null?"" : request.getParameter("noPhone");
						if(!noPhone.equals("") && noPhone.equals("1")){
							ORBO.setPhone("0000000000");
						}
						if(!orFieldPositionBO.getRouteNumber().equals("") && ORBO.getTypeOfRide().equals("1")){
							if(orFieldPositionBO.getRouteNumber().contains(";")){
								String[] routeNumbers=orFieldPositionBO.getRouteNumber().split(";");
								for(int i=0;i<routeNumbers.length;i++){
									ORBO.setRouteNumber((values[Integer.parseInt(orFieldPositionBO.getRouteNumber())-1]));
								}
							}else{
								ORBO.setRouteNumber((values[Integer.parseInt(orFieldPositionBO.getRouteNumber())-1]).replace("\"", ""));
							}
						}else{
							ORBO.setRouteNumber("0");
						}
						if(!orFieldPositionBO.getRouteNumber().equals("") && ORBO.getTypeOfRide().equals("1")){
							if(!routeNumber.equals(ORBO.getRouteNumber())){
								routeNumber=ORBO.getRouteNumber();
								if(routeId!=0){
									RequestDAO.routeNumberInsertion(adminBO,ORBO,routeId+"",numberOfTrips);
									numberOfTrips=1;
								}
								routeId=RequestDAO.routeNumberInsertion(adminBO,ORBO,"",0);
								ORBO.setRouteNumber(routeId+"");
							}else{
								numberOfTrips++;
								ORBO.setRouteNumber(routeId+"");
							}
						} 
						ORBO.setAdvanceTime("-1");
						request.setAttribute("openrequestBOfromAction", ORBO);
						errorReturn=OpenRequestUtil.saveOpenRequest(request, response, getServletConfig());
						if(errorReturn.contains("OK")){
							String[] tripIdAndStatus=errorReturn.split(";");
							String[] tripId=tripIdAndStatus[0].split("=");
							if(tripId[1]!=null && !tripId[1].equals("")){
								ORBO.setTripid(tripId[1]);
								if(sharedRide!=null && !sharedRide.equals("")){
									RequestDAO.sharedRideDetailsInsertion(ORBO, adminBO,numberOfTrips+"");
									if(!ORBO.getDropOffOrder().equals("")){
										ORBO.setPickupOrder("");
										RequestDAO.sharedRideDetailsInsertion(ORBO, adminBO,numberOfTrips+"");
									}
								}
							}
						}else if(errorReturn.contains("false")){
							errorMsg = errorMsg.append(  
									ORBO.getRouteNumber() +"'" + 
											ORBO.getName()+"'"+
											ORBO.getSttime()+"," +
											errorReturn + "\n");
						}
					}
					n=n+1;
				}
				if( routeId!=0){
					RequestDAO.routeNumberInsertion(adminBO,ORBO,routeId+"",numberOfTrips);
				}
				in.close();
			}catch (Exception e){//Catch exception if any
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace();
			}
		}
		request.setAttribute("errors", errorMsg);
		request.setAttribute("no_of_jobs", n);
		request.setAttribute("vendorList", VendorList);
		request.setAttribute("screen", "/jsp/FileUploadForOR.jsp"); 
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}





	public void sharedJobLogs(HttpServletRequest request, HttpServletResponse response) throws ServletException ,IOException{
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		String routeNo = request.getParameter("routeNo");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		ArrayList<OpenRequestBO> al_list = SharedRideDAO.getSharedRideLogs(adminBO.getAssociateCode(), routeNo, fromDate, toDate,adminBO.getTimeZone());
		request.setAttribute("routeNo", routeNo);
		request.setAttribute("fDate", fromDate);
		request.setAttribute("tDate", toDate);
		request.setAttribute("sharedJobLogs",al_list);
		request.setAttribute("screen", "/jsp/SharedRideLogs.jsp"); 
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);
	}



	public void reviewDocuments(HttpServletRequest request, HttpServletResponse response) throws ServletException ,IOException{
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		request.setAttribute("docTypeData", DocumentDAO.getExpiredLicenses(adminBO.getAssociateCode()));
		request.setAttribute("screen", "/jsp/expiredLicenses.jsp"); 
		getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request, response);


	}
	public void sharedRide(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		AdminRegistrationBO adminBO  = (AdminRegistrationBO)request.getSession().getAttribute("user");  
		ArrayList<TagSystemBean> al_listCookie = RequestDAO.readCookiesForAll(adminBO.getUid(),adminBO.getMasterAssociateCode());
		request.setAttribute("CookieValues", al_listCookie);
		getServletContext().getRequestDispatcher("/Company/ManiFest.jsp").forward(request, response);

	}
	public void  customerdetail(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException  {
		response.setContentType("text/html;charset=UTF-8");
	//	PrintWriter out= response.getWriter();
		String value=request.getParameter("value");
		String no = request.getParameter("sno");
		String uid=request.getParameter("id");
		String uname = request.getParameter("name");
		String unumber = request.getParameter("phonenumber");
		String uemail = request.getParameter("email");
		String reason = request.getParameter("reason");
		//String status=request.getParameter("status");
		//System.out.println("name is" +uname +"num is" +unumber);
		String name=" ";
		String sno="";
		String id="";
		String email="";
    	String phone="";
    	String status=""; 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String assc = adminBO.getAssociateCode();
		if(request.getParameter("reason")==null){
		//JSONArray jarray=RequestDAO.customerdetail(assc,uname,unumber);
		
		ArrayList<UserDetail> val= RequestDAO.customerdetail(assc,uname,unumber);
	    JSONArray jarray=new JSONArray();
        for(int i=0;i<val.size();i++){
        JSONObject jobject=new JSONObject();
	    sno=val.get(i).getsno();
	    id=val.get(i).getuserid(); 
	    name=val.get(i).getusername();
	    email=val.get(i).getemail();
	    phone=val.get(i).getphone();
	    status=val.get(i).getstatus();
	    try {
			jobject.put("sno",sno);
			jobject.put("id",id);
			jobject.put("name",name);
			jobject.put("email",email);
			jobject.put("phone",phone);
			jobject.put("status",status);
			jobject.put("key",val.get(i).getKey());
			jarray.put(jobject);
			//System.out.println("array is" +"\t"  +jarray);
		  } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    //System.out.println("sno" +sno +"\t" +"userid" +id +"\t" +"username" +name +"\t" +"email" +email +"\t" +"phone" +phone );
	    }
	   // request.setAttribute("val",val);
	    response.setContentType("application/json");
	    response.getWriter().write(jarray.toString());
        //getServletConfig().getServletContext().getRequestDispatcher("/jsp/UserDetail2.jsp").forward(request,response);
		//response.getWriter().write("Success");
	    }
	    else if (request.getParameter("reason").equalsIgnoreCase("update")){
	    	//System.out.println("before dao" +no +"ass is"+assc +uid +uname +uemail +unumber);
	    	int val=RequestDAO.updatedetail(assc,no,uid,uname,uemail,unumber);
	    	if(val == 1){
	    	response.setContentType("text/plain");
	    	response.getWriter().write("success");
	    	}
	    	else{
	    		response.setContentType("text/plain");
		    	response.getWriter().write("not success");
	    	}
	    	
	    }
	    else if(request.getParameter("reason").equalsIgnoreCase("DeActivate")){
	    	//System.out.println("before dao" +value +"ass is"+assc+"Sno is"+no +uid +uname +uemail +unumber);
	    	int val2 = RequestDAO.DeActivate(value,assc, no, uid, uname, uemail, unumber);
	    	if(val2 ==1){
	    		response.setContentType("text/plain");
	    		response.getWriter().write("success");
	    	}else{
	    		response.setContentType("text/plain");
	    		response.getWriter().write("not success");
	    	}
	    }
	}

	
}
