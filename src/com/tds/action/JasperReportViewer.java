package com.tds.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.axis.types.URI.MalformedURIException;

import com.common.util.TDSProperties;


/**
 * Servlet implementation class JasperReportViewer
 */
public class JasperReportViewer {
	private static final long serialVersionUID = 1L;

	public JasperReportViewer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static OutputStream doProcess(HttpServletResponse p_response,HashMap<String, String> param) throws IOException,MalformedURIException 
	{

		String jasperURL = TDSProperties.getValue("jasperURL");
		Iterator it = param.entrySet().iterator();
		String fileName ="";
		String output =  "";
		while (it.hasNext()) {
			Map.Entry pairs = (Map.Entry)it.next();
			jasperURL = jasperURL + "&" + pairs.getKey() + "=" + pairs.getValue();
			if(!pairs.getKey().equals("assocode")&&!pairs.getKey().equals("timeOffset")&&!pairs.getKey().equals("output")&&!pairs.getKey().equals("companyCode")){
				fileName += pairs.getValue() + ":"; 
			}
			if(pairs.getKey().equals("output")){
				output =pairs.getValue()+"";
			}
		}
		//System.out.println("file="+fileName);
		fileName = fileName.substring(0,fileName.length()-1);
		//fileName = "test";
		URL url = new URL(jasperURL);
		//System.out.println("URL for Reports:"+url);
		URLConnection urlc = url.openConnection();  
		//System.out.println("after open connection");
		InputStream in = urlc.getInputStream();  
		//System.out.println("get input stream");

		OutputStream out = p_response.getOutputStream();  
		//System.out.println("get output stream");
		
		p_response.setHeader("Content-Disposition", "attachment;filename=\""+ fileName + "\"");
		p_response.setContentType( "application/"+output );   
		//System.out.println("resp headers set");
		
		byte[] buf = new byte[urlc.getContentLength()];  
		
		//System.out.println("output buffer set"+ urlc.getContentLength());
		int c;  
		while ((c = in.read(buf, 0, buf.length)) > 0) {  
			out.write(buf, 0, c);  
			System.out.println("writing output"+ c);
		}  
		out.flush();  
		return out;
	}

}
