package com.tds.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tds.cmp.bean.OpenRequestHistory;
import com.tds.controller.SystemUnavailableException;
import com.tds.controller.TDSController;
import com.tds.dao.AssistantOperatorDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.AssistantOperatorBo;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.PaymentSettledDetail;
import com.tds.tdsBO.VoucherBO;
import com.common.util.PasswordHash;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.common.util.TDSValidation;

/**
 * Servlet implementation class AssistantOperator
 */
public class AssistantOperator extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;   
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AssistantOperator() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request, response);
	}
	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException 
	{
		String eventParam = "";
		//HttpSession m_session = p_request.getSession();

		if(p_request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = p_request.getParameter(TDSConstants.eventParam);
		}
		//cat.info("Event Param "+eventParam);
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)p_request.getSession().getAttribute("clientUser");

		if(clientBO!=null||(AdminRegistrationBO)p_request.getSession().getAttribute("user") != null){
			//if ((AdminRegistrationBO)p_request.getSession().getAttribute("user") != null) {

		 if(eventParam.equalsIgnoreCase("assistantLogin")) {
			try {
				assistantOperatorLogin(p_request, p_response);
			} catch (SystemUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(eventParam.equalsIgnoreCase("getVoucherDetails")){
			getVoucherDetails(p_request,p_response);
		}else if(eventParam.equalsIgnoreCase("submitJobs")){
			submitJobs(p_request,p_response);
		}else if(eventParam.equalsIgnoreCase("jobHistoryFOrAssistant")){
			JobLogs(p_request,p_response);
		}else if(eventParam.equalsIgnoreCase("jobLogsFOrAssistant")){
			jobLogsAjax(p_request,p_response);
		}else if(eventParam.equalsIgnoreCase("jobDetailsForAssistant")){
			jobdetalsForOperator(p_request,p_response);
		}else if(eventParam.equalsIgnoreCase("jobReservations")){
			jobReservations(p_request,p_response);
		}else if(eventParam.equalsIgnoreCase("verifyVoucher")){
			verifyVoucher(p_request,p_response);
		}else if(eventParam.equalsIgnoreCase("registerUser")){
			registerUser(p_request,p_response);
		}
		else if(eventParam.equalsIgnoreCase("assistantOperatorDetails")) {
			assistantOperatorDetails(p_request, p_response);
		}else if(eventParam.equalsIgnoreCase("registerUser")) {
			registerUser(p_request, p_response);
		}
		}else{
			try {
				if(eventParam.equalsIgnoreCase("registerUser")) {
					registerUser(p_request, p_response);
				}else{
					assistantOperatorLogin(p_request, p_response);
				}
			} catch (SystemUnavailableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void assistantOperatorDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/ClientUsage/AssistantOperatorDetails.jsp");
		requestDispatcher.forward(request, response);
	}
	@SuppressWarnings("deprecation")
	public void submitJobs(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
		if(clientBO!=null){
			String urlString ="";
			String phone = request.getParameter("phone");
			String name = request.getParameter("name");
			String email = request.getParameter("eMail");
			String  sadd1 = request.getParameter("sadd1");
			String  sadd2= request.getParameter("sadd2");
			String  scity= request.getParameter("scity");
			String  sstate= request.getParameter("sstate");
			String  szip= request.getParameter("szip");
			String  eadd1= request.getParameter("eadd1");
			String  eadd2= request.getParameter("eadd2");
			String  ecity= request.getParameter("ecity");
			String  estate= request.getParameter("estate");
			String  ezip= request.getParameter("ezip");
			String  sdate= request.getParameter("sdate");
			String  shrs= request.getParameter("shrs");
			String sLatitude = request.getParameter("sLatitude");
			String  sLongitude= request.getParameter("sLongitude");
			String  eLatitude= request.getParameter("eLatitude");
			String  eLongitude= request.getParameter("eLongitude");
			String  Comments1= request.getParameter("Comments1");
			String  specialIns1= request.getParameter("specialIns1");
			String  ccNum= request.getParameter("ccNum");
			String  ccName= request.getParameter("ccName");
			String expDate = request.getParameter("expDate");
			String  cvvNum= request.getParameter("cvvNum");
			String  ccAmount= request.getParameter("ccAmount");
			String  billAdd= request.getParameter("billAdd");
			String  payType= request.getParameter("payType");
			String  numOfPass= request.getParameter("numOfPass");
			String  drprofileSize= request.getParameter("drprofileSize");
			String  vprofileSize= request.getParameter("vprofileSize");
			String  flightInfo= request.getParameter("flightInfo");
			String compNo = request.getParameter("compCode");
			String voucherNum=request.getParameter("voucherNum");
			String tripId ="";
			urlString =TDSProperties.getValue("AssistantAppUrl")+"/TDS/OpenRequestAjax?event=saveOpenRequestHTML" +
			"&phone="+URLEncoder.encode(phone)+"&name="+URLEncoder.encode(name)+"&sadd1="+URLEncoder.encode(sadd1)+"&sadd2="+URLEncoder.encode(sadd2)+"&scity="+URLEncoder.encode(scity)+"" +
			"&sstate="+URLEncoder.encode(sstate)+"&szip="+URLEncoder.encode(szip)+"&sdate="+URLEncoder.encode(sdate)+"&shrs="+URLEncoder.encode(shrs)+"" +
			"&sLongitude="+URLEncoder.encode(sLongitude)+"&sLatitude="+URLEncoder.encode(sLatitude)+"&tripId="+URLEncoder.encode(tripId)+"&numberOfPassengers="+
			"&masterAddress=&userAddress=&userAddressTo=&eadd1="+URLEncoder.encode(eadd1)+"" +
			"&eadd2="+URLEncoder.encode(eadd2)+"&ecity="+URLEncoder.encode(ecity)+"&estate="+URLEncoder.encode(estate)+"&ezip="+URLEncoder.encode(ezip)+"" +
			"&edlongitude=&edlatitude=&advanceTime=-1&fromDate=&toDate=&paytype="+URLEncoder.encode(payType)+"" +
			"&acct="+voucherNum+"&amt=&driver=&cabNo=&landMarkKey=&slandmark=&elandmark=&Comments="+Comments1+"" +
			"&specialIns="+URLEncoder.encode(specialIns1)+"."+URLEncoder.encode(flightInfo)+"&queueno=" +
			"&numOfCabs=&premiumCustomer=0&eMail="+URLEncoder.encode(email)+"&updateAll=&tripStatus=" +
			"&drprofileSize=&vprofileSize=&associationCode="+compNo+"&createdBy="+clientBO.getUserId()+"&tripSource="+TDSConstants.assistant;

			System.setProperty("http.keepAlive", "true");
			//System.out.println("check uRl:"+urlString);
			String responseNew ="";
			try {
				URL urlV = new URL(urlString.toString());
				HttpURLConnection conn = (HttpURLConnection) urlV.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setConnectTimeout(43000);
				conn.connect();
				if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
					InputStream is = conn.getInputStream();
					StringBuffer b = new StringBuffer();
					BufferedReader r = new BufferedReader(new InputStreamReader(is));
					String line;
					while ((line = r.readLine()) != null)
						b.append(line);
					is.close();
					responseNew = b.toString();
					conn.disconnect();
				}
			}
			catch (Exception e) {
				{
					System.out.println("GetACabException Posting to Internet" + e.getMessage());
				}
			}
			String tripID ="";
			String content ="";
			String[] arrOutput =  responseNew.split(";");
			for(int i=0;i<arrOutput.length;i++){
				String[] eachvalues = arrOutput[i].split("="); 
				if(eachvalues[0].equals("TripId")){
					tripID = eachvalues[1];
				}
			}
			if(tripID!=null&&tripID!=""){
				request.setAttribute("error", "Job has been created,TripID is "+tripID);
			}else{
				request.setAttribute("error", "System failed to create A job !!!");
			}
			request.setAttribute("status", "Login");
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/ClientUsage/AssistantOperatorDetails.jsp");
		requestDispatcher.forward(request, response);
	}
public void assistantOperatorLogin(HttpServletRequest request,HttpServletResponse response)throws ServletException,IOException, SystemUnavailableException{
		AdminRegistrationBO  adminBo = (AdminRegistrationBO)request.getSession().getAttribute("user");
		HttpSession session1 = request.getSession();
		if (adminBo!=null){
			AssistantOperatorBo clientAdminBO=new AssistantOperatorBo();
			clientAdminBO.setUserId(adminBo.getUid());
			clientAdminBO.setPhNo(adminBo.getPhnumber());
			clientAdminBO.setEmail(adminBo.getEmail());
			clientAdminBO.setName(adminBo.getUserNameDisplay());
			clientAdminBO.setTimeZone(adminBo.getTimeZone());
			clientAdminBO.setTimeZone(adminBo.getTimeZone());
			//clientAdminBO.setAssoccode("135");
			session1.setAttribute("clientUser", clientAdminBO);
			request.setAttribute("userName", clientAdminBO.getName());
			request.setAttribute("error", "Login was successful");
			request.setAttribute("status", "Login");
		}else{
			AssistantOperatorBo CMBO=new AssistantOperatorBo();
			HttpSession session = request.getSession();
			AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
			if(request.getParameter("logout")!=null&&request.getParameter("logout").equalsIgnoreCase("Logout")){
				session.invalidate();
				request.setAttribute("error", "you are Logged Out");
				request.setAttribute("status", "Logout");
			}else{
				if(clientBO==null){
					AssistantOperatorBo clientAdminBO=new AssistantOperatorBo();
					CMBO.setPassword(PasswordHash.encrypt(request.getParameter("password")==null?"":request.getParameter("password")));
					CMBO.setUserId(request.getParameter("userId")==null?"":request.getParameter("userId"));
					if(!CMBO.getUserId().equals("") && !CMBO.getPassword().equals("") ){
						AssistantOperatorBo cMobileBO =new AssistantOperatorBo();
						cMobileBO=AssistantOperatorDAO.customerLogin(CMBO);
						if(!cMobileBO.getUserId().equals("")){
							ArrayList<VoucherBO> voucher = AssistantOperatorDAO.getVouchers(cMobileBO.getEmail());
							clientAdminBO.setUserId(cMobileBO.getUserId());
							clientAdminBO.setPhNo(cMobileBO.getPhNo());
							clientAdminBO.setEmail(cMobileBO.getEmail());
							clientAdminBO.setName(cMobileBO.getName());
							clientAdminBO.setTimeZone(cMobileBO.getTimeZone());
							//clientAdminBO.setAssoccode("135");
							session.setAttribute("clientUser", clientAdminBO);
							request.setAttribute("vouchers", voucher);
							request.setAttribute("userName", clientAdminBO.getName());
							request.setAttribute("error", "Login was successful");
							request.setAttribute("status", "Login");
						}else{
							request.setAttribute("error", "Login was Unsuccessful");
							request.setAttribute("status", "Logout");
						}
					}
				}else{
					request.setAttribute("status", "Login");  
				}
			}
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/ClientUsage/AssistantOperatorDetails.jsp");
		requestDispatcher.forward(request, response);		
	}public void registerUser(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
		AssistantOperatorBo asistBo = new AssistantOperatorBo();
		asistBo.setName(request.getParameter("name")!=null?request.getParameter("name"):"");
		asistBo.setUserId(request.getParameter("uId")!=null?request.getParameter("uId"):"");
		try {
			asistBo.setPassword(PasswordHash.encrypt(request.getParameter("pass")!=null?request.getParameter("pass"):""));
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		asistBo.setEmail(request.getParameter("email")!=null?request.getParameter("email"):"");
		asistBo.setAddress(request.getParameter("addr")!=null?request.getParameter("addr"):"");
		asistBo.setCity(request.getParameter("city")!=null?request.getParameter("city"):"");
		asistBo.setZipCode(Integer.parseInt(request.getParameter("zip")!=null?request.getParameter("zip"):"0"));
		asistBo.setPhNo(request.getParameter("phno")!=null?request.getParameter("phno"):"");
		int status = AssistantOperatorDAO.createUser(asistBo);
		response.getWriter().write(status+"");
	}
	public void jobdetalsForOperator(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		ArrayList<OpenRequestBO> Address = null;
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
		JSONArray array = new JSONArray();
		Address = AssistantOperatorDAO.getjobsForAssistants(clientBO.getTimeZone() ,clientBO.getUserId());
		for (int i = 0; i < Address.size(); i++) {
			JSONObject address = new JSONObject();
			try {
				address.put("D", Address.get(i).getDriverid() == null ? "" : Address.get(i).getDriverid());
				address.put("V", Address.get(i).getVehicleNo() == null ? "" : Address.get(i).getVehicleNo());
				address.put("N", Address.get(i).getName() == null ? "" : Address.get(i).getName());
				address.put("T", Address.get(i).getTripid() == null ? "" : Address.get(i).getTripid());
				address.put("Q", Address.get(i).getQueueno() == null ? "" : Address.get(i).getQueueno());
				address.put("A", Address.get(i).getScity() == null ? "" : Address.get(i).getScity());
				address.put("PT", Address.get(i).getPendingTime() == null ? "" : Address.get(i).getPendingTime());
				address.put("P", Address.get(i).getPhone() == null ? "" : Address.get(i).getPhone());
				address.put("S", Address.get(i).getTripStatus() == null ? "" : Address.get(i).getTripStatus());
				address.put("ST", Address.get(i).getRequestTime() == null ? "" : Address.get(i).getRequestTime());
				address.put("SD", Address.get(i).getSdate() == null ? "" : Address.get(i).getSdate());
				address.put("LA", Address.get(i).getSlat() == null ? "" : Address.get(i).getSlat());
				address.put("LO", Address.get(i).getSlong() == null ? "" : Address.get(i).getSlong());
				address.put("DD", Address.get(i).getDontDispatch());
				address.put("SR", Address.get(i).getTypeOfRide() == null ? "" : Address.get(i).getTypeOfRide());
				address.put("AC", Address.get(i).getPaytype() == null ? "" : Address.get(i).getPaytype());
				address.put("PC", Address.get(i).getPremiumCustomer() == 0 ? "" : Address.get(i).getPremiumCustomer());
				address.put("RT", Address.get(i).getRouteNumber() == null ? "" : Address.get(i).getRouteNumber());
				address.put("EDQ", Address.get(i).getEndQueueno() == null ? "" : Address.get(i).getEndQueueno());
				address.put("LMN", Address.get(i).getSlandmark() == null ? "" : Address.get(i).getSlandmark());

				//String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), assoCode, Address.get(i).getDrProfile(), ";", 0);

				address.put("DP", "");
				array.put(address);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}
		String responseString = array.toString();
		System.out.println("Checking JobResponse:"+responseString.toString());
		response.getWriter().write(responseString.toString());

	}
	public void JobLogs(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException{
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
		String fromDate=request.getParameter("startDate");
		String toDate=request.getParameter("endDate");
		String fromDateFormatted=TDSValidation.getTimeStampDateFormat(fromDate);
		String toDateFormatted=TDSValidation.getTimeStampDateFormat(toDate);
		//Double timeOffset =  Double.parseDouble("0.00");
		String value = request.getParameter("searchvalue")!=null? request.getParameter("value"):"";
		ArrayList<OpenRequestBO>Address= AssistantOperatorDAO.getJobHistoryForAssistant(clientBO.getTimeZone(), clientBO.getUserId(),value, fromDateFormatted, toDateFormatted);
		JSONArray array = new JSONArray();
		for (int i = 0; i < Address.size(); i++) {
			JSONObject address = new JSONObject();
			try {
				address.put("D", Address.get(i).getDriverid() == null ? "" : Address.get(i).getDriverid());
				address.put("V", Address.get(i).getVehicleNo() == null ? "" : Address.get(i).getVehicleNo());
				address.put("N", Address.get(i).getName() == null ? "" : Address.get(i).getName());
				address.put("T", Address.get(i).getTripid() == null ? "" : Address.get(i).getTripid());
				address.put("Q", Address.get(i).getQueueno() == null ? "" : Address.get(i).getQueueno());
				address.put("A", Address.get(i).getScity() == null ? "" : Address.get(i).getScity());
				address.put("PT", Address.get(i).getPendingTime() == null ? "" : Address.get(i).getPendingTime());
				address.put("P", Address.get(i).getPhone() == null ? "" : Address.get(i).getPhone());
				address.put("S", Address.get(i).getTripStatus() == null ? "" : Address.get(i).getTripStatus());
				address.put("ST", Address.get(i).getRequestTime() == null ? "" : Address.get(i).getRequestTime());
				address.put("SD", Address.get(i).getSdate() == null ? "" : Address.get(i).getSdate());
				address.put("LA", Address.get(i).getSlat() == null ? "" : Address.get(i).getSlat());
				address.put("LO", Address.get(i).getSlong() == null ? "" : Address.get(i).getSlong());
				address.put("DD", Address.get(i).getDontDispatch());
				address.put("SR", Address.get(i).getTypeOfRide() == null ? "" : Address.get(i).getTypeOfRide());
				address.put("AC", Address.get(i).getPaytype() == null ? "" : Address.get(i).getPaytype());
				address.put("PC", Address.get(i).getPremiumCustomer() == 0 ? "" : Address.get(i).getPremiumCustomer());
				address.put("RT", Address.get(i).getRouteNumber() == null ? "" : Address.get(i).getRouteNumber());

				address.put("EDQ", Address.get(i).getEndQueueno() == null ? "" : Address.get(i).getEndQueueno());
				address.put("LMN", Address.get(i).getSlandmark() == null ? "" : Address.get(i).getSlandmark());

				//String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getAssociateCode(), Address.get(i).getDrProfile(), ";", 0);

				//address.put("DP", driverFlagValue);
				array.put(address);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}
	public void jobLogsAjax(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException{
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
		String tripId=(request.getParameter("tripId")==null?"":request.getParameter("tripId"));
		ArrayList<OpenRequestHistory> ORLogs= new ArrayList<OpenRequestHistory>();

		ORLogs=AssistantOperatorDAO.getJobLogsForAssistant(tripId,clientBO.getTimeZone());
		if(ORLogs.size()==0 ){
			response.getWriter().write("No Records Found");
		} else {
			response.getWriter().write("<table id=\"logs\" width=\"100%\"><tr><td class=\"firstCol\">Action</td>	<td class=\"firstCol\">Created By</td><td class=\"firstCol\">Time</td>"+"</td></tr>");
			for(int i=0;i<ORLogs.size();i++){
				response.getWriter().write("<tr><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">"+ORLogs.get(i).getReason()+"</font></td><td align=\"center\" style=\"background-color: lightgrey\"><font color=\"black\">"+ORLogs.get(i).getUserId()+"</font></td><td align=\"center\" style=\"background-color: lightgrey\" ><font color=\"black\">"+ORLogs.get(i).getTime()+"</font></td></tr>");
			}
			response.getWriter().write("</table>");
		}
	}
	public void jobReservations(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
		String fromDate=request.getParameter("fDate")==null?"":request.getParameter("fDate");
		String toDate=request.getParameter("tDate")==null?"":request.getParameter("tDate");
		String value= request.getParameter("keyWord")==null?"":request.getParameter("value");
		String fromDateFormatted=TDSValidation.getTimeStampDateFormat(fromDate);
		String toDateFormatted=TDSValidation.getTimeStampDateFormat(toDate);
		ArrayList<OpenRequestBO>Address=AssistantOperatorDAO.openRequestSummaryForAssistants(clientBO.getTimeZone(),clientBO.getUserId(),value,fromDateFormatted,toDateFormatted);

		JSONArray array = new JSONArray();
		for (int i = 0; i < Address.size(); i++) {
			JSONObject address = new JSONObject();
			try {
				address.put("D", Address.get(i).getDriverid() == null ? "" : Address.get(i).getDriverid());
				address.put("V", Address.get(i).getVehicleNo() == null ? "" : Address.get(i).getVehicleNo());
				address.put("N", Address.get(i).getName() == null ? "" : Address.get(i).getName());
				address.put("T", Address.get(i).getTripid() == null ? "" : Address.get(i).getTripid());
				address.put("Q", Address.get(i).getQueueno() == null ? "" : Address.get(i).getQueueno());
				address.put("A", Address.get(i).getScity() == null ? "" : Address.get(i).getScity());
				address.put("PT", Address.get(i).getPendingTime() == null ? "" : Address.get(i).getPendingTime());
				address.put("P", Address.get(i).getPhone() == null ? "" : Address.get(i).getPhone());
				address.put("S", Address.get(i).getTripStatus() == null ? "" : Address.get(i).getTripStatus());
				address.put("ST", Address.get(i).getRequestTime() == null ? "" : Address.get(i).getRequestTime());
				address.put("SD", Address.get(i).getSdate() == null ? "" : Address.get(i).getSdate());
				address.put("LA", Address.get(i).getSlat() == null ? "" : Address.get(i).getSlat());
				address.put("LO", Address.get(i).getSlong() == null ? "" : Address.get(i).getSlong());
				address.put("DD", Address.get(i).getDontDispatch());
				address.put("SR", Address.get(i).getTypeOfRide() == null ? "" : Address.get(i).getTypeOfRide());
				address.put("AC", Address.get(i).getPaytype() == null ? "" : Address.get(i).getPaytype());
				address.put("PC", Address.get(i).getPremiumCustomer() == 0 ? "" : Address.get(i).getPremiumCustomer());
				address.put("RT", Address.get(i).getRouteNumber() == null ? "" : Address.get(i).getRouteNumber());

				address.put("EDQ", Address.get(i).getEndQueueno() == null ? "" : Address.get(i).getEndQueueno());
				address.put("LMN", Address.get(i).getSlandmark() == null ? "" : Address.get(i).getSlandmark());

				//String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getAssociateCode(), Address.get(i).getDrProfile(), ";", 0);

				//address.put("DP", driverFlagValue);
				array.put(address);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());

	}
	public void getVoucherDetails(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException{
		AssistantOperatorBo  clientBO = (AssistantOperatorBo)request.getSession().getAttribute("clientUser");
		ArrayList<PaymentSettledDetail> voucher = new ArrayList<PaymentSettledDetail>();
		String fDate = request.getParameter("fDateForVoucher");
		String tDate = request.getParameter("tDateForVoucher");
		PaymentSettledDetail invoiceBo =new PaymentSettledDetail();
		voucher = AssistantOperatorDAO.getVoucherDetails(invoiceBo, fDate, tDate,clientBO.getUserId());

		JSONArray array = new JSONArray();
		for (int i = 0; i < voucher.size(); i++) {
			JSONObject address = new JSONObject();
			try {
				address.put("TX", voucher.get(i).getTransId()== null ? "" : voucher.get(i).getTransId());
				address.put("CC", voucher.get(i).getCostCenter() == null ? "" : voucher.get(i).getCostCenter());
				address.put("RN", voucher.get(i).getRiderName() == null ? "" : voucher.get(i).getRiderName());
				address.put("VNO", voucher.get(i).getVoucherNo() == null ? "" : voucher.get(i).getVoucherNo());
				address.put("T", voucher.get(i).getTotal() == null ? "" : voucher.get(i).getTotal());
				address.put("EXD", voucher.get(i).getExpDate() == null ? "" : voucher.get(i).getExpDate());
				address.put("DES", voucher.get(i).getDescription() == null ? "" : voucher.get(i).getDescription());
				address.put("COMC", voucher.get(i).getCompanyCode() == null ? "" : voucher.get(i).getCompanyCode());
				address.put("CON", voucher.get(i).getContact()== null ? "" : voucher.get(i).getContact());
				address.put("DD", voucher.get(i).getDelayDays() == null ? "" : voucher.get(i).getDelayDays());
				address.put("FR", voucher.get(i).getFreq() == null ? "" : voucher.get(i).getFreq());
				address.put("TI", voucher.get(i).getTripId() == null ? "" : voucher.get(i).getTripId());
				address.put("DID", voucher.get(i).getDriverId() == null ? "" : voucher.get(i).getDriverId());
				address.put("PRS", voucher.get(i).getPaymentRecievedDate());
				address.put("VER", voucher.get(i).getVerified());
				address.put("DPS", voucher.get(i).getDriverPayedStatus());

				//String driverFlagValue = SystemUtils.getCompanyFlagDescription(getServletConfig(), adminBO.getAssociateCode(), Address.get(i).getDrProfile(), ";", 0);

				//address.put("DP", driverFlagValue);
				array.put(address);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}
	public void verifyVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
		String trans = request.getParameter("transId");
		String[] transID = trans.split(";");
		int status = AssistantOperatorDAO.upDateVoucherAsverified(transID);
		response.getWriter().write(status+"");
	}
}
