package com.tds.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.controller.SystemUnavailableException;
import com.tds.controller.TDSController;
import com.tds.dao.DispatchDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.TDSConstants;

public class ServiceRequestAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;
	
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ ServiceRequestAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	/*
	 * Access Get method from JSP page or Browser
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Get Method  ");
		try {
			doProcess(request,response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * Access Post method from JSP page or Browser
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Post Method");
		try {
			doProcess(request, response);
		} catch (SystemUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	 * Access request from both getter and setter methods
	 */
	
	public void doProcess(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException, SystemUnavailableException {
		cat.info("In doProcess method");
		String eventParam = "";
		HttpSession m_session = request.getSession();
		boolean m_sessionStatus = false;
		
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		cat.info("The user requested event as "+ eventParam);
		if(m_session.getAttribute("user") != null ) {
			/*
			 * if(eventParam.equalsIgnoreCase(TDSConstants.updateStartTrip)) {
			 * //updateStartTrip(request, response); } else
			 * if(eventParam.equalsIgnoreCase(TDSConstants.updateEndTrip)) {
			 * //updateEndTrip(request, response); } else
			 * if(eventParam.equalsIgnoreCase(TDSConstants.checkPayment)) {
			 * //checkPayment(request, response); } else
			 * if(eventParam.equalsIgnoreCase
			 * (TDSConstants.updateCompletedRequest)) {
			 * //updateCompletedRequest(request, response); } else
			 * if(eventParam.equalsIgnoreCase(TDSConstants.dOpenRequest)) {
			 * //directOpenRequest(request, response); } else
			 */
			if (eventParam.equalsIgnoreCase(TDSConstants.inProgressRequestXML)) {
				getProgressRequestXmlData(request, response);
			} else if (eventParam
					.equalsIgnoreCase(TDSConstants.inAcceptedRequestXML)) {
				getAcceptedRequestXmlData(request, response);
			} else if (eventParam
					.equalsIgnoreCase(TDSConstants.getDriverAcceptance)) {
				setDriverAcceptance(request, response);
			} else if (eventParam.equalsIgnoreCase(TDSConstants.getLoginXML)) {
				checkUserXML(request, response);
			} else if (eventParam
					.equalsIgnoreCase(TDSConstants.getDriverLocation)) {
				// setdriverLocation(request, response);
			} else if (eventParam
					.equalsIgnoreCase(TDSConstants.checkDriverRequest)) {
				checkDriverRequest(request, response);
			} else if (eventParam
					.equalsIgnoreCase(TDSConstants.changeDriverRequest)) {
				changeDriverRequest(request, response);
			}
			/*
			 * else if(eventParam.equalsIgnoreCase(TDSConstants.insertNoShow)) {
			 * insertNoShow(request, response); }
			 */
			/*else if (eventParam
					.equalsIgnoreCase(TDSConstants.driverQueueAction)) {
				serviceToDriver(request, response);
			} else if (eventParam
					.equalsIgnoreCase(TDSConstants.dropDriverinQueue)) {
				deleteDriverinQueue(request, response);
			}*/
			m_sessionStatus = true;
		}
		if(!m_sessionStatus) {
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request	, response);
		}
		
	}
	
	public void checkDriverRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		cat.info("In checkDriverRequest");
		String driverid="";
		String screen = "";
		String forwardURL = TDSConstants.getMainNewJSP;
		if(request.getParameter("driverid")!=null){
			driverid=request.getParameter("driverid");
		}
		int status = ServiceRequestDAO.checkDriverExist(driverid);
		if(status > 0){
			request.setAttribute("page","Driver Exist");
			screen = TDSConstants.getSuccessJSP;
		}else{
			request.setAttribute("page","Driver Not Exist");
			screen = TDSConstants.getFailureJSP;
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	public void changeDriverRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		AdminRegistrationBO m_adminBO = new AdminRegistrationBO();
		cat.info("In changeDriverRequest");
		
		
		
		String driverid= "";
		String newdriverid= "";
		String tripid = "";
		String screen = "";
		String usertype = "";
		int status = 0;
		String forwardURL = TDSConstants.getMainNewJSP;
		if(request.getParameter("driverid")!=null){
			newdriverid=request.getParameter("driverid");
		}
		if(request.getParameter("tripid")!=null){
			tripid=request.getParameter("tripid");
		}
		HttpSession session = request.getSession();
		if(session.getAttribute("user")!=null){
			AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
			driverid=adminBO.getUname();
			usertype=adminBO.getUsertypeDesc();
			//System.out.println("driverid"+driverid+"usertype"+usertype);
		}
		if(usertype.equalsIgnoreCase("Driver")){
			status = ServiceRequestDAO.changeDriverRequest(driverid,newdriverid,tripid,m_adminBO);
		}		
		
		if(status > 0){
			request.setAttribute("page","Driver Change Sucess");
			screen = TDSConstants.getSuccessJSP;
		}else{
			request.setAttribute("page","Driver Change Failed");
			screen = TDSConstants.getFailureJSP;
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	/*public void updateStartTrip(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		cat.info("In update Start Trip function ");
		OpenRequestBO openRequestBO ;
		String tripID = "";
		String driverID = "";
		String customerPhone = "";
		String longitude = "";
		String latitude = "";
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		if(request.getParameter("tripid") != null ){
			tripID = request.getParameter("tripid");
		}
		if(request.getParameter("driverid") != null ){
			driverID = request.getParameter("driverid");
		}
		if(request.getParameter("phone") != null ){
			customerPhone = request.getParameter("phone");
		}
		if(request.getParameter("stlatitude") != null ){
			longitude = request.getParameter("stlatitude");
		}
		if(request.getParameter("stlongitude") != null ){
			latitude = request.getParameter("stlongitude");
		}
		cat.info("Trip ID "+tripID);
		cat.info("Driver ID "+driverID);
		cat.info("Customer Phone "+customerPhone);
		cat.info("Longitude "+longitude);
		cat.info("Latitude "+latitude);
		HashMap hmp_startTrip = new HashMap();
		hmp_startTrip.put("tripid", tripID);
		hmp_startTrip.put("driverid", driverID);
		hmp_startTrip.put("phone", customerPhone);
		hmp_startTrip.put("stlatitude", longitude);
		hmp_startTrip.put("stlongitude", latitude);
		int result =0 ;
		if(tripID!= "" && driverID != "") {
			openRequestBO = ServiceRequestDAO.checkTripinAcceptedRequest(tripID);
			if(openRequestBO.getIsBeandata() > 0) {
				result = ServiceRequestDAO.setStartTrip(hmp_startTrip,openRequestBO);
			} else {
				cat.info("Failure to updated");
				request.setAttribute("page", "Failure to update in TDS_INPROGRESSREQUEST");
				screen = TDSConstants.getFailureJSP;
			}
		} else {
			cat.info("Failure to updated");
			request.setAttribute("page", "Failure to update in TDS_INPROGRESSREQUEST");
			screen = TDSConstants.getFailureJSP;
		}
		if(result > 0) {
			cat.info("Successfully updated Start Service");
			request.setAttribute("page", "Success to update in TDS_INPROGRESSREQUEST");
			screen = TDSConstants.getSuccessJSP;
		} else {
			cat.info("Failure to updated");
			request.setAttribute("page", "Failure to update in TDS_INPROGRESSREQUEST");
			screen = TDSConstants.getFailureJSP;
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	*/
	/*public void updateEndTrip(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		cat.info("In update End Trip function ");
		OpenRequestBO openRequestBO;
		String tripID = "";
		String driverID = "";
		String longitude = "";
		String latitude = "";
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		if(request.getParameter("tripid") != null ){
			tripID = request.getParameter("tripid");
		}
		if(request.getParameter("driverid") != null ){
			driverID = request.getParameter("driverid");
		}
		
		if(request.getParameter("edlatitude") != null ){
			longitude = request.getParameter("edlatitude");
		}
		if(request.getParameter("edlongitude") != null ){
			latitude = request.getParameter("edlongitude");
		}
		cat.info("Trip ID "+tripID);
		cat.info("Driver ID "+driverID);
		cat.info("Longitude "+longitude);
		cat.info("Latitude "+latitude);
		HashMap hmp_endTrip = new HashMap();
		hmp_endTrip.put("tripid", tripID);
		hmp_endTrip.put("driverid", driverID);
		hmp_endTrip.put("edlatitude", longitude);
		hmp_endTrip.put("edlongitude", latitude);
		int result =0 ;
		if(tripID!= "" && driverID != "") {
			openRequestBO = ServiceRequestDAO.checkDriverandTrip(tripID, driverID);
			System.out.println("openRequestBOassoccode"+openRequestBO.getAssociateCode());
			if(openRequestBO.getIsBeandata() > 0) {
				result = ServiceRequestDAO.setEndTrip(hmp_endTrip,openRequestBO);
			} else {
				cat.info("Failure to updated");
				request.setAttribute("page", "Failure to update in TDS_COMPLETEDREQUEST");
				screen = TDSConstants.getFailureJSP;
			}
		} else {
			cat.info("Failure to updated");
			request.setAttribute("page", "Failure to update in TDS_COMPLETEDREQUEST");
			screen = TDSConstants.getFailureJSP;
		}
		if(result > 0) {
			cat.info("Successfully updated Start Service");
			request.setAttribute("page", "Success to update in TDS_COMPLETEDREQUEST");
			screen = TDSConstants.getSuccessJSP;
		} else {
			cat.info("Failure to updated");
			request.setAttribute("page", "Failure to update in TDS_COMPLETEDREQUEST");
			screen = TDSConstants.getFailureJSP;
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}*/
	
	/*public void checkPayment(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		cat.info("In checkCreditCard method ");
		String paymentType = "";
		String amount = "";
		String cardNo = "";
		String cardType = "";
		String cardName = "";
		String cardExpiryDate = "";
		String cardCuu = "";
		
		String voucherNo = "";
		String voucherName = "";
		
		PrintWriter pw = response.getWriter();
		response.setContentType("text/html");
		
		if(request.getParameter("ptype") != null) {
			paymentType = request.getParameter("ptype");
		}
		
		if(request.getParameter("amount") != null) {
			amount = request.getParameter("amount");
		}
		
		if(paymentType.equalsIgnoreCase("CC")) {
			if(request.getParameter("cno") != null) {
				cardNo = request.getParameter("cno");
			}
			if(request.getParameter("ctype") != null) {
				cardType = request.getParameter("ctype");
			}
			if(request.getParameter("cname") != null) {
				cardName = request.getParameter("cname");
			}
			if(request.getParameter("cexpiry") != null) {
				cardExpiryDate = request.getParameter("cexpiry");
			}
			if(request.getParameter("ccuu") != null) {
				cardCuu = request.getParameter("ccuu");
			}
			cat.info("Card No "+cardNo.substring(cardNo.length()-4,cardNo.length() ));
			cat.info("Card Name "+cardName);
			cat.info("Payment Type "+paymentType);
			cat.info("Amount "+amount);
			
			if(cardNo != "" && cardCuu != "" && cardExpiryDate != "" && cardType != "" && cardName != "") {
				pw.println("Success");
				//forwardURL = TDSConstants.getSuccessJSP;
			} else {
				pw.println("Failure");
				//forwardURL = TDSConstants.getFailureJSP;
			}
		} else if(paymentType.equalsIgnoreCase("CV")){
			if(request.getParameter("vno") != null) {
				voucherNo = request.getParameter("vno");
			}
			if(request.getParameter("vname") != null) {
				voucherName = request.getParameter("vname");
			}
			cat.info("Voucher no "+voucherNo );
			cat.info("Voucher Name "+voucherName);
			if(voucherNo != "" && voucherName != "") {
				int result = 0;
				result = ServiceRequestDAO.checkVoucherNo(voucherNo, voucherName);
				if(result > 0) {
					pw.write("Success");
				} else {
					pw.write("Failure");
				}
			} else {
				pw.write("Failure");
			}
		} else if(paymentType.equalsIgnoreCase("GV")) {
			cat.info("In Government Voucher");
			if(request.getParameter("vno") != null) {
				voucherNo = request.getParameter("vno");
			}
			if(request.getParameter("vname") != null) {
				voucherName = request.getParameter("vname");
			}
			cat.info("Voucher no "+voucherNo );
			cat.info("Voucher Name "+voucherName);
		}
		//String forwardURL = "";
		
		pw.close();
	}*/
	
	/*public void updateCompletedRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		cat.info("In updateCompletedRequest method ");
		String driverid = "";
		String tripid = "";
		String amount = "";
		String paymentType = "";
		String paymentNo = "";
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		String vname="";
		
		boolean m_paymentMade = false;
		String m_messageSubject = "";
		String m_messageContent = "";
		SendingSMS m_sendSMS = null;
		
		if(request.getParameter("driverid") != null) {
			driverid = request.getParameter("driverid");
		}
		if(request.getParameter("tripid") != null) {
			tripid = request.getParameter("tripid");
		}
		if(request.getParameter("amount") != null) {
			amount = request.getParameter("amount");
		}
		if(request.getParameter("ptype") != null) {
			paymentType = request.getParameter("ptype");
		}
		if(request.getParameter("pno") != null) {
			paymentNo = request.getParameter("pno");
		}
		if(request.getParameter("vname") != null){
			vname=request.getParameter("vname");
		}
		System.out.println("Driver id "+driverid+vname);
		cat.info("Driver id "+driverid);
		cat.info("Trip id "+tripid );
		cat.info("Amount "+amount);
		cat.info("Payment Type "+paymentType);
		cat.info("Payment No "+paymentNo);
		
		HashMap hmp_payment = new HashMap();
		hmp_payment.put("driverid", driverid);
		hmp_payment.put("tripid", tripid);
		hmp_payment.put("amount", amount);
		hmp_payment.put("paymentType", paymentType);
		if(paymentType.equalsIgnoreCase("CC")) {
			hmp_payment.put("paymentNo", paymentNo.substring(paymentNo.length()-4, paymentNo.length()));
		} else {
			hmp_payment.put("paymentNo", paymentNo);
		}
		if(driverid != "" && tripid != "" && amount != "" && paymentNo != "" && paymentType != "") {
			System.out.println("In check loop");
			System.out.println("Payment type:"+paymentType);
			if(paymentType.equals("VC" )|| paymentType.equals("GV" )){
				System.out.println("In VC || GV");
				if(vname !=""){
					System.out.println("Vname"+vname+ServiceRequestDAO.checkVoucherNo(paymentNo, vname));
					if(ServiceRequestDAO.checkVoucherNo(paymentNo, vname)==1){
						int result = ServiceRequestDAO.updateCompletedRequest(hmp_payment);
						if(result > 0 ) {
							//cat.info("Try to insert client table");
							System.out.println("~~~~~~~~~~~~In side the Voucher Type~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
							m_paymentMade = true;
							request.setAttribute("page", "Successfully updated TDS_COMPLETEDREQUEST ");
							screen = TDSConstants.getSuccessJSP;
						} else {
							request.setAttribute("page", "Failure to update TDS_COMPLETEDREQEST");
							screen = TDSConstants.getFailureJSP;
						}
					}
					else if(ServiceRequestDAO.checkVoucherNo(paymentNo, vname) == 2 && ServiceRequestDAO.checkVoucherNo(paymentNo, vname) == 0){
						request.setAttribute("page","Not a Valid Voucher No");
						screen = TDSConstants.getFailureJSP;
					}
					else{
						request.setAttribute("page","Voucher No Entred is already in use");
						screen = TDSConstants.getFailureJSP;
					}
				}else{
					request.setAttribute("page","Please Enter Company Name");
					screen = TDSConstants.getFailureJSP;
				}
				
			}
			if(paymentType.equals("CC")){
				System.out.println("Payment:Credit ");
					int result = ServiceRequestDAO.updateCompletedRequest(hmp_payment);
					if(result > 0 ) {
						//cat.info("Try to insert client table");
						System.out.println("Inside in the credit card Details");
						m_paymentMade = true;
						request.setAttribute("page", "Successfully updated TDS_COMPLETEDREQUEST ");
						screen = TDSConstants.getSuccessJSP;
					} else {
						request.setAttribute("page", "Failure to update TDS_COMPLETEDREQEST");
						screen = TDSConstants.getFailureJSP;
					}							
			}
						
		} else {
			request.setAttribute("page","You missied some fields");
			screen = TDSConstants.getFailureJSP;
		}
		if(m_paymentMade) {
			if(paymentType.equalsIgnoreCase("CC")) {
				m_messageSubject = "TDSCCA";
				m_messageContent = m_messageSubject+";"+tripid+";"+"Y;"+amount;
			}  else if(paymentType.equalsIgnoreCase("VC")) {
				m_messageSubject = "TDSVC";
				m_messageContent = m_messageSubject+";"+tripid+";"+"Y;"+amount;
			} else if(paymentType.equalsIgnoreCase("GV")) {
				m_messageSubject = "TDSGV";
				m_messageContent = m_messageSubject+";"+tripid+";"+"Y;"+amount;
			} else  {
				m_messageSubject = "TDSINVALID";
				m_messageContent = m_messageSubject+";"+tripid+";"+"N;"+amount;
			}
			m_sendSMS = new SendingSMS();
			try {
				
				//To ram 9703668956@messaging.sprintpcs.com
				m_sendSMS.sendMail("vimal.a@amshuhu.com", m_messageContent);
			} catch (MessagingException p_messageExp) {
				System.out.println("Exception in payment verification Messaging "+p_messageExp);
				p_messageExp.getStackTrace();
			}
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL) ;
		requestDispatcher.forward(request, response);
	}*/
	

	
	/*public void directOpenRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		System.out.println("Session id "+session.getId());
		cat.info("In directOpenRequest method ");
		String driverid = "";
		String stlongitude = "";
		String stlatitude = "";
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		
		if(request.getParameter("driverid") != null ) {
			driverid = request.getParameter("driverid");
		}
		if(request.getParameter("stlongitude") != null ) {
			stlongitude = request.getParameter("stlongitude");
		}
		if(request.getParameter("stlatitude") != null ) {
			stlatitude = request.getParameter("stlatitude");
		}
		
		HashMap hmp_open = new HashMap();
		hmp_open.put("driverid", driverid);
		hmp_open.put("stlatitude", stlatitude);
		hmp_open.put("stlongitude", stlongitude);
		
		cat.info("Driver id "+driverid);
		cat.info("Latitude "+stlatitude);
		cat.info("Longitude "+stlongitude);
		int result =0;
		if(driverid != "" && stlatitude != "" && stlongitude !="") {
			result = ServiceRequestDAO.insertOpenRequest(hmp_open);
		} else {
			request.setAttribute("page","You missied some fields");
			screen = TDSConstants.getFailureJSP;
		}
		if(result > 0) {
			if(result >0 ) {
				request.setAttribute("page", "Successfully updated TDS_INPROGRESSREQUEST ");
				screen = TDSConstants.getSuccessJSP;
			} else {
				request.setAttribute("page", "Failure to update TDS_INPROGRESSREQUEST");
				screen = TDSConstants.getFailureJSP;
			}
		} 
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL) ;
		requestDispatcher.forward(request, response);
	}*/
	
	public void getProgressRequestXmlData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("In the getInProgressXmlData");
		cat.info("In the getInProgressXmlData");
		String forwardURL = TDSConstants.getInProgressRequestXML;
		ArrayList al_progressdata= null;
		//InProgressRequestBO inprogressBO=new InProgressRequestBO();
		OpenRequestBO openRequestBO=new OpenRequestBO();
		try{
			al_progressdata = ServiceRequestDAO.getProgressDataXML(openRequestBO);
			//System.out.println("al_progressdata ="+al_progressdata);
			if(al_progressdata!=null)
			{	request.setAttribute("inprogressdata", al_progressdata);
			
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			}
		}
		catch(Exception ex){
			//System.out.println("Exception in getInProgressXmlData :"+ex.getMessage());
			cat.info("Exception in getInProgressXmlData :"+ex.getMessage());
		}
	}
	public void getAcceptedRequestXmlData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In getAcceptedRequestXmlData");
		HttpSession session = request.getSession();
		
		String forwardURL = TDSConstants.getAcceptedRequestXML;
		ArrayList al_accepteddata = null;
		try{
			String userid = "";
			if(session.getAttribute("user") != null) {
				AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
				userid = adminBO.getUname();
			}
			al_accepteddata = ServiceRequestDAO.getAcceptedRequestDataXML(userid);
			if(al_accepteddata!=null){
				request.setAttribute("acceptrequestdata", al_accepteddata);
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			}
		}
		catch(Exception ex){
			cat.info("Exception in getAcceptedRequestXmlData :"+ex.getMessage());
		}
	}
	
	public void setDriverAcceptance (HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		cat.info("In setDriverAcceptance method");
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		String tripid = "";
		String driverid = "";
		int m_position = 0;
		int status = 0;
		AdminRegistrationBO adminBO = new AdminRegistrationBO();
		HttpSession session = request.getSession();
		OpenRequestBO openRequestBO = new OpenRequestBO();
		if(request.getParameter("tripid") != null) {
			tripid = request.getParameter("tripid");
			openRequestBO.setTripid(tripid);
		}
		if(session.getAttribute("user") != null) {
			adminBO = (AdminRegistrationBO) session.getAttribute("user");
			driverid = adminBO.getUname();
		}
		//System.out.println("Driver id "+driverid+" Trip id "+tripid);
		if(tripid != "" && driverid != "") {
			String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO, myString);
			}
//			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "","0", adminBO);
			openRequestBO = RequestDAO.getOpenRequestTableData(openRequestBO);
			if(openRequestBO.getIsBeandata() > 0) {
				//System.out.println("In True Condition ");
				openRequestBO.setTripid(tripid);
				openRequestBO.setDriverid(driverid);
				status = ServiceRequestDAO.insertAcceptedRequest( openRequestBO);
				//System.out.println("Insert Status "+status);
				if(status > 0){
					m_position = ServiceRequestDAO.getDriverPosition(driverid, openRequestBO.getQueueno());
					ServiceRequestDAO.deleteDriverFromQueue(driverid, adminBO.getMasterAssociateCode());
					ServiceRequestDAO.updateQueueDetail(ServiceRequestDAO.getQueueList( Thread.currentThread().getName(),m_position), Thread.currentThread().getName(),poolBO);
					cat.info("Successfully Updated AcceptedRequest");
					screen = TDSConstants.getSuccessJSP;
				}
				else {
					cat.info("Failure to update AcceptedRequest");
					request.setAttribute("page","You missied some fields");
					screen = TDSConstants.getFailureJSP;
				}			
			} else {
				cat.info("Already Accepted this Trip");
				request.setAttribute("page","Already Accepted this Trip");
				screen = TDSConstants.getFailureJSP;
			}
		}
		else {
			cat.info("Failure to update AcceptedRequest");
			request.setAttribute("page","You missied some fields");
			screen = TDSConstants.getFailureJSP;
		}	
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	
	public void checkUserXML(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException, SystemUnavailableException {
		HttpSession session = request.getSession();
		cat.info("In Check User XML function ");
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		String userName = request.getParameter("loginName");
		String password = request.getParameter("password");
		AdminRegistrationBO adminBo = null;
		if(userName != "" && password != "") {
			adminBo = RegistrationDAO.getUserAvailable(userName, password,"","","");
			if(adminBo.getIsAvailable() == 1) {
				session.setAttribute("user", adminBo);
				out.println("Success fully loged in ");
			} else {
				out.println("Invalid user name or password");
			}
		}else {
			out.println("You must entered Username or Password");
		}
	}
	
	/**
	 * @param p_request
	 * @param p_response
	 * @author vimal
	 * 
	 * @see Method
	 * This method is set the Location of a Driver or Availability status of a Driver. Also this method
	 * is, force to create the Thread process for each QueueID from the Queue Table.
	 * @throws ServletException
	 * @throws IOException
	 */
	/*public void setdriverLocation(HttpServletRequest p_request,HttpServletResponse p_response) throws ServletException,IOException {
		HttpSession session = p_request.getSession();
		System.out.println(">>>>>>>>>>>>setdriverLocation>>>>>>>>>>>");
		cat.info("In setdriverLocation Http Post function ");
		DriverLocationBO driverLocationBO = new DriverLocationBO();
		String screen = "";
		String userType = "";
		String userid = "";
		String m_assoccode="";
		String m_queueId = "";
		DoQueueProcess m_queueIdProcess;
		String forwardURL = TDSConstants.getMainNewJSP; 
		if(session.getAttribute("user") != null) {
			AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
			userType = adminBO.getUsertypeDesc();
			userid = adminBO.getUname();
			m_assoccode = adminBO.getAssociateCode();
			System.out.println("UserType:"+userType+",Userid"+userid);
		}
		if(p_request.getParameter("latitude") != null) {
			driverLocationBO.setLatitude(p_request.getParameter("latitude"));
			System.out.println("lat="+driverLocationBO.getLatitude());
		} else {
			driverLocationBO.setLatitude("0");
		}
		if(p_request.getParameter("longitude") != null ) {
			driverLocationBO.setLongitude(p_request.getParameter("longitude"));
			System.out.println("long="+driverLocationBO.getLongitude());
		}  else {
			driverLocationBO.setLongitude("0");
		}
		if(p_request.getParameter("availability") != null) {
			driverLocationBO.setStatus(p_request.getParameter("availability"));
			System.out.println("status="+driverLocationBO.getStatus());
		} else {
			driverLocationBO.setStatus("0");
		}
		if(userid!= "") {
			driverLocationBO.setDriverid(userid);
		}
		if(userType.equalsIgnoreCase("Driver") ) {
			int result = 0;
			result = ServiceRequestDAO.checkDriverLocation(driverLocationBO.getDriverid());
			 System.out.println("checkDriverLocation =="+result);
			 
			 // If Condition
			if(result == 0) {  //Driver  not in the DriverLocation Table
				result = ServiceRequestDAO.insertDriverLocation(driverLocationBO);  //Insert Driver location in the TDS_DRIVERLOCATION table
				 System.out.println("insertDriverLocation =="+result);
				if(result >0 ){  //If the Driver was inserted then get Queue for this driver from the location table 
					m_queueId = ServiceRequestDAO.getQueueID(driverLocationBO.getLatitude(), driverLocationBO.getLongitude());
					//The above DAO gives the Queue id from the TDSLocation table using lat and log
					System.out.println("queueid =="+m_queueId);
					//If Driver status is available and queueid is in the location table then insert into Queue table 
					if(!m_queueId.equalsIgnoreCase("0") && driverLocationBO.getStatus().equalsIgnoreCase("y")) {
						result = ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(), m_queueId,m_assoccode);
						//insert into Queue table if //Success 
						if(result > 0 ) {
							//Create a thread for each QueueId;
							System.out.println("Inside the Check Driver Queue ID ");
							if(!_queueIdMap.containsKey(m_queueId))  {
								cat.info("Creating the Thread for each QueueId "+m_queueId);
								System.out.println("Creating the Thread for each QueueId "+m_queueId);
								m_queueIdProcess = new DoQueueProcess(m_queueId);
								m_queueIdProcess.setName(m_queueId);
								m_queueIdProcess.start();
								_queueIdMap.put(m_queueId, m_queueId);
								//m_queueIdProcess.set_queueIdMap(_queueIdMap);
							}
							System.out.println("Data in QueueId Map "+_queueIdMap);
							screen = TDSConstants.getSuccessJSP;
						} else {
							//insert into Queue table if //Failure
							screen = TDSConstants.getFailureJSP;
						}
					//If Driver status is unavailable and queueid is not in the location table then failure to insert in Queue table
					
					} else { 
						screen = TDSConstants.getSuccessJSP;
					}
					screen = TDSConstants.getSuccessJSP;
				// If the Driver was not inserted then get Queue for this driver from the location table 
				} else {
					screen = TDSConstants.getFailureJSP;
				}
			// Else Condition
			} else {  //Driver already in the DriverLocation Table
				result = ServiceRequestDAO.updateDriverLocation(driverLocationBO);
				if(result >0 ){
					if(driverLocationBO.getStatus().equalsIgnoreCase("y")){
						m_queueId = ServiceRequestDAO.getQueueID(driverLocationBO.getLatitude(), driverLocationBO.getLongitude());
						System.out.println("queueid =="+m_queueId);
						if(!m_queueId.equalsIgnoreCase("0") && driverLocationBO.getStatus().equalsIgnoreCase("y")) {
							result = ServiceRequestDAO.setDriverQueue(driverLocationBO.getDriverid(), m_queueId,m_assoccode);
							
							if(result > 0 ) {
								System.out.println("Inside the Check Driver Queue ID ");
								
								//Thread[] b_noOfThreads = Thread.enumerate(tarray)
								
								if(!_queueIdMap.containsKey(m_queueId))  {
									cat.info("Creating the Thread for each QueueId "+m_queueId);
									System.out.println("Creating the Thread for each QueueId "+m_queueId);
									m_queueIdProcess = new DoQueueProcess(m_queueId);
									m_queueIdProcess.setName(m_queueId);
									m_queueIdProcess.start();
									_queueIdMap.put(m_queueId, m_queueId);
									//m_queueIdProcess.set_queueIdMap(_queueIdMap);
								}
								System.out.println("Data in QueueId Map "+_queueIdMap);
								screen = TDSConstants.getSuccessJSP;
							}else {
								screen = TDSConstants.getFailureJSP;
							}
						} else {
							screen = TDSConstants.getSuccessJSP;
						}
					}
					else if(driverLocationBO.getStatus().equalsIgnoreCase("N")){
						int status = ServiceRequestDAO.deleteDriverFromQueue(driverLocationBO.getDriverid());
						System.out.println("delete from queue status =="+status);						
					}
					
					screen = TDSConstants.getSuccessJSP;
				} else {
					screen = TDSConstants.getFailureJSP;
				}
			}
			
		}else {
			screen = TDSConstants.getFailureJSP;
		}
		p_request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(p_request, p_response);
	}*/

	
	/*public void insertNoShow(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		String driver = "";
		int result = 0;
		if(session.getAttribute("user") != null) {
			AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
			driver = adminBO.getUname();
		}
		if(driver != "") {
			String tripid = "";
			if(request.getParameter("tripid") != null) {
				tripid = request.getParameter("tripid").trim();
				OpenRequestBO openRequestBO = ServiceRequestDAO.checkTripinAcceptedRequest(tripid);
				if(openRequestBO.getIsBeandata() > 0) {
					result = ServiceRequestDAO.setNoShow(openRequestBO);
				} else {
					cat.info("Failure to updated");
					request.setAttribute("page", "Failure to update in TDS_NOSHOW");
					screen = TDSConstants.getFailureJSP;
				}
			}
		}
		if(result <= 0 ){
			request.setAttribute("page", "Failure to update in TDS_SHOW");
			screen = TDSConstants.getFailureJSP;
		} else {
			request.setAttribute("page", "Successfully update into TDS_SHOW");
			screen = TDSConstants.getSuccessJSP;
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}*/
	
	
	//public void serviceToDriver()
	
	
	/*public void serviceToDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		System.out.println("In Service to Driver");
		String driver = "";
		ArrayList al_openRequest = null;
		if(session.getAttribute("user") != null) {
			AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
			driver = adminBO.getUname();
		}
		if(driver != "") {
			System.out.println("Driver name "+driver);
			if(driver.equalsIgnoreCase(ServiceRequestDAO.getDriverQueueOrder(driver)) ){
				al_openRequest = (ArrayList) ServiceRequestDAO.checkDriverQueue(driver);
			}
			System.out.println("Data array "+al_openRequest);
		}
		response.setContentType("text/html");
		if(al_openRequest != null && al_openRequest.size() > 0) {
			//for(int count=0;count <= al_openRequest.size(); count++) {
				OpenRequestBO openRequestBO = (OpenRequestBO) al_openRequest.get(0);
				StringBuffer dataBuffer = new StringBuffer();
				dataBuffer.append("Trip Id$$"+openRequestBO.getTripid());
				dataBuffer.append("$$<BR>Address$$"+openRequestBO.getSadd1()+"<BR>"+openRequestBO.getSadd2()+"<br>"+openRequestBO.getScity()+"<BR>"+openRequestBO.getSstate());
				dataBuffer.append("$$<br>Phone$$"+openRequestBO.getPhone());
				response.getWriter().write(dataBuffer.toString());
				
			//}
		} else {
			System.out.println("In Else Part...");
			response.getWriter().write("0");
			System.out.println("In Else Part...");
		}
		//pw.close();
	}*/
	
	/*public void serviceToDriver(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		HttpSession m_session = p_request.getSession();
		cat.info("In Service to Driver");
		System.out.println("In Service to Driver menthod");
		String m_driverId = "";
		ArrayList m_openRequestList = null;
		String m_forwardURL = "";
		if(m_session.getAttribute("user") != null) {
			AdminRegistrationBO adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
			m_driverId = adminBO.getUname();
		}
		if(m_driverId != "") {
			cat.info("Driver name "+m_driverId);
			System.out.println("Driver name "+m_driverId);
			if(m_driverId.equalsIgnoreCase(ServiceRequestDAO.getDriverQueueOrder(m_driverId)) ){
				m_openRequestList = (ArrayList) ServiceRequestDAO.checkOROFDriver(m_driverId);
			}
			System.out.println("Data array "+m_openRequestList);
		}
		if(m_openRequestList != null && m_openRequestList.size() > 0) {
			OpenRequestBO openRequestBO = (OpenRequestBO) m_openRequestList.get(0);
			try {
				Thread.sleep(90000);
			} catch (InterruptedException interruptedException) {
				cat.info("Error in Thread "+interruptedException);
			}
			if(ServiceRequestDAO.checkOpenRequest(openRequestBO.getTripid()) > 0) {
				m_forwardURL = "/control?action=servicerequest&event=dropDriverQueue";
				System.out.println("Drop Driver from DriverQueue Driver Request Data if part");
			} else {
				m_forwardURL = "/control?action=servicerequest&event=driverQueueService";
				System.out.println("In Driver Request Data else part");
			}
		} else {
			m_forwardURL = TDSConstants.getMainNewJSP;
			p_request.setAttribute("screen", TDSConstants.getFailureJSP);
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(m_forwardURL);
		dispatcher.forward(p_request, p_response);		
	}*/
	
	/*public void deleteDriverinQueue(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		HttpSession session = request.getSession();
		String driver = "";
		//String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		if(session.getAttribute("user") != null) {
			AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
			driver = adminBO.getUname();
		}
		int result = ServiceRequestDAO.deleteDriverFromQueue(driver);
		if(result <= 0 ){
			request.setAttribute("page", "Failure to delete TDS_QUEUE");
			screen = TDSConstants.getFailureJSP;
		} else {
			request.setAttribute("page", "Successfully deleted TDS_QUEUE");
			screen = TDSConstants.getSuccessJSP;
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/control?action=servicerequest&event=driverQueueService");
		requestDispatcher.forward(request, response);
	}*/
} 
