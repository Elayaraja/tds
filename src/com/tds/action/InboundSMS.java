package com.tds.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tds.cmp.bean.SMSBean;
import com.tds.dao.PhoneDAO;
import com.common.util.sendSMS;
import com.twilio.sdk.TwilioRestException;

/**
 * Servlet implementation class InboundSMS
 */
public class InboundSMS extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InboundSMS() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//System.out.println("Calling Inbound Method");
		forwardSMS(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		forwardSMS(request,response);
	}

	public void forwardSMS(HttpServletRequest request,HttpServletResponse response) throws IOException{
		String toNum = request.getParameter("to");
		String message = request.getParameter("text");
		String fromNum = request.getParameter("msisdn");
		//System.out.println("Message From--->"+fromNum+" To--->"+toNum+" With MsgId--->"+request.getParameter("messageId"));
		//System.out.println("Message Type--->"+request.getParameter("type")+" Time--->"+request.getParameter("message-timestamp")+" Msg Is--->"+message);
		if(toNum!=null && fromNum!=null && message!=null){
			SMSBean smsBo=PhoneDAO.getSMSParameter(toNum, 2);
			//System.out.println("About To Send Message To--->"+smsBo.getCompNum());
			if(smsBo.getCompNum()!=null && !smsBo.getCompNum().equals("")){
				try {
				int result = PhoneDAO.insertMessagefromMob(fromNum,message,smsBo.getCompCode());
				}catch (Exception e){
					e.printStackTrace();
				}
				message = "Message From "+fromNum+";"+message;
				try {
					//System.out.println("Routing Message--->"+message);
					sendSMS.main(message, smsBo.getCompNum(), smsBo.getCompCode());
				} catch (TwilioRestException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		response.getWriter().write("OK");
	}
}
