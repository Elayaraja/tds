package com.tds.action;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.dao.SecurityDAO;
import com.tds.dao.UserRoleDAO;
import com.tds.dao.UtilityDAO;
import com.tds.security.bean.UserAccessRights;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class for Servlet: UserRole
 *
 */
 public class UserRoleAction extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
    private static final long serialVersionUID = 1L;
	private Category cat = TDSController.cat;
    /* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
   public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ UserRoleAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	} 	
	
	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request,response);
	}  	
	
	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doProcess(request,response);
	}   	
	
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String eventParam = "";
		HttpSession m_session = request.getSession();
		boolean m_sessionStatus = false;
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		cat.info("The user requested event as "+ eventParam);
		if(m_session.getAttribute("user") != null ) {
			if(eventParam.equalsIgnoreCase(TDSConstants.getUserList)) {
				   AdminSummary(request, response);
			}
			//else if(eventParam.equalsIgnoreCase(TDSConstants.getUserRole)) {
			//	getUserRole(request, response);
			//} else if(eventParam.equalsIgnoreCase(TDSConstants.updateUserRole)){
			//	updateUserRole(request, response);
			//}
			m_sessionStatus = true;
		}
		
		if(!m_sessionStatus) {
			getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP).forward(request	, response);
		}
	}
public void AdminSummary(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		String forwardURL = TDSConstants.getMainNewJSP;
		cat.info("In Driver Summary Method");		
		//System.out.println("admin summary");
		String screen = TDSConstants.userListJSP;
		HttpSession session = request.getSession();
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		String userid ="";
		String usertype="";
		userid = request.getParameter("user_id")==null?"":request.getParameter("user_id");
		usertype = request.getParameter("usertype") == null?"":request.getParameter("usertype");
		String drivers=request.getParameter("drivers")==null?"":request.getParameter("drivers");
		String nonDrivers=request.getParameter("nonDrivers")==null?"":request.getParameter("nonDrivers");
		if(request.getParameter("copyOrImage") != null){
			SecurityDAO.copyUserAccess(request.getParameter("userid"),request.getParameter("newUser"));
			request.setAttribute("screen", "/Security/UserAccess1.jsp");
			requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		}

		if(request.getParameter("subevent") == null)
		{
	 		
	 		
	 		if(request.getParameter("Button") == null)
			{
				request.setAttribute("screen", "/Security/UserAccess1.jsp");
				requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				request.setAttribute("al_list", UtilityDAO.getModulesName(request.getParameter("userid")));
			 	requestDispatcher.forward(request, response);

			} else  {
				ArrayList al_access = new ArrayList();
				String size = request.getParameter("size");
				UserAccessRights rights = null;
				for(int i=0;i<Integer.parseInt(size);i++)
				{
					rights = new UserAccessRights();
					rights.setModule_no(request.getParameter("module_no"+i));
					rights.setAcces(request.getParameter("access_"+i)!=null?true:false);
					al_access.add(rights);
				}
				SecurityDAO.setUserAccess(request.getParameter("userid"), al_access);
				response.sendRedirect("control?action=userrolerequest&event=getUserList&subevent=access&module=systemsetupView");
			}
		} else {
			ArrayList al_summary = null;
			double no_limit = 0;
			int plength=5; // Increase or Decrease the numbers in the link sequence at the bottom of the page. Example <12345> can be increased to <12345678910>
			int start =0;
			int end = 10;
			int no_of_rows=10;
			int pstart=1;
			int pend=plength+1;
			int t_length=0;
			AdminRegistrationBO adminbo = new AdminRegistrationBO();
			
			try{
				t_length=UserRoleDAO.AdmintotalRecordLength((((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()),(((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode()));
				no_limit = Math.ceil((t_length/10.0));

				if(request.getParameter("first") == null){
					adminbo.setStart(""+start);
					adminbo.setEnd(""+end);
					al_summary=UserRoleDAO.getUserList(adminbo,(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()),(((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode()));
	 				//System.out.println("In if loop");
					if(no_limit<end){
						pend=(int)no_limit+1;
						request.setAttribute("pend", pend+"");
					}
					else{
						request.setAttribute("pend", pend+"");
					}
					
					
					if(request.getParameter("button").equalsIgnoreCase("search")) { 
					 
						al_summary=UserRoleDAO.getuserData((((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()),userid,usertype,drivers,nonDrivers,((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode()); 
					}
					
					
				}
				else{
					if(request.getParameter("pageNo")!=null || request.getParameter("previous")!=null || request.getParameter("next")!=null){
						if(request.getParameter("pageNo")!=null){
							int pagevalue=Integer.parseInt(request.getParameter("pageNo"));
							pstart=Integer.parseInt(request.getParameter("pstart"));
							pend=Integer.parseInt(request.getParameter("pend"));														
							start=(pagevalue*no_of_rows)-no_of_rows;
							request.setAttribute("pend", pend+"");							
						}

						if(request.getParameter("previous")!=null){
							int pagevalue=Integer.parseInt(request.getParameter("previous"));
							if(pstart<pagevalue){
								if(pstart<(pagevalue-plength)){
									pstart=pagevalue-plength;
									start=pstart;
									if((pstart+plength)<no_limit){
										pend=pstart+plength;
									}
									else{
										pend=(int)no_limit;
									}
								}	
								else if(pstart==(pagevalue-plength)){
									pstart=pagevalue-plength;
									start=pstart; 
									if(pagevalue<no_limit){
										pend=pagevalue;
									}else{
										pend=(int)no_limit;
									}									
								}
								else{
									start=pstart;
								}
							}
							else if(pstart==pagevalue){
								pstart=pagevalue;
								start=pstart;
								if((pagevalue+plength)<no_limit){
									pend=pagevalue+plength;
								}
								else{
									pend=(int)no_limit;
								}
							}
							else{
								start=pstart;
							}
							start=(start*no_of_rows)-no_of_rows;
							request.setAttribute("pend", pend+"");
						}
						if( request.getParameter("next")!=null){
							int pagevalue=Integer.parseInt(request.getParameter("next"));
							if(pagevalue<no_limit){
								if((pagevalue+plength)<=no_limit){
									pstart=pagevalue;
									start=pstart;
								}
								else{
									pstart=pagevalue;	
									start=pstart;
								}
								if((pagevalue+plength)<=no_limit){
									pend=pagevalue+plength;
								}
								else{
									pend=(int)no_limit+1;
								}
							}
							else if(pagevalue==no_limit){
								pstart=pagevalue;
								start=pstart;
								pend=(int)no_limit+1;
							}
							else{
								pstart=pagevalue-1;
								start=pstart;
								if((pstart+plength)<no_limit){
									pend=pstart+plength;
								}
								else{
									pend=(int)no_limit;
								}
								if(pend==pstart){
									pend=pend+1;
								}
							}
							start=(start*no_of_rows)-no_of_rows;
							request.setAttribute("pend", pend+"");
						}
						adminbo.setStart(""+start);
						adminbo.setEnd(""+end);
						al_summary=UserRoleDAO.getUserList(adminbo,(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()),(((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode()));

					}
				}
				for(int i=0;i<t_length;i++){
					if(request.getParameter("page"+i)!=null){
						start=(i*10)-10;
						adminbo.setStart(""+start);
						adminbo.setEnd(""+end);
						al_summary=UserRoleDAO.getUserList(adminbo,(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()),(((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode()));

					}
				}
				
				
				
				
				if(al_summary.size()>0){
					request.setAttribute("pstart", pstart+"");
					request.setAttribute("plength", plength+"");
					request.setAttribute("startValue", start+"");
					request.setAttribute("limit", no_limit+"");
				}
			}
			catch(Exception ex){
				cat.info("The Exception in the method driver summary  : "+ex.getMessage());
			}
			request.setAttribute("screen", screen);
			request.setAttribute("DRBO", adminbo);
			request.setAttribute("Adminsummary", al_summary);
		 
			requestDispatcher.forward(request, response); 
		}
	 	

		
}

 
	/*public void getUserList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In getUserList method");
		System.out.println("In getUserList");
		HttpSession m_session = request.getSession();
	 	ArrayList userListData= null;
		try{
			
			userListData = UserRoleDAO.getUserList(((AdminRegistrationBO) m_session.getAttribute("user")).getAssociateCode());
			 
			String forwardURL = TDSConstants.getMainJSP;
			cat.info("Forward the request into the "+ forwardURL +" JPS");	
			request.setAttribute("screen", TDSConstants.userListJSP);
			request.setAttribute("userlist", userListData);
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
			requestDispatcher.forward(request, response);
		}
		catch(Exception ex){
			cat.info("Exception in getUserList :"+ex.getMessage());
		}
	}
	public void getUserRole(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In getUserRole");
		System.out.println("In getUserRole");
		String forwardURL = TDSConstants.getMainJSP;
		
		AdminRegistrationBO adminBO=new AdminRegistrationBO();
		if(request.getParameter("uname")!=null){
			adminBO.setUname((String)request.getParameter("uname"));
			System.out.println("User name =="+adminBO.getUname());
		}
		adminBO=UserRoleDAO.getUserRole(adminBO);
		//adminBO.setRole(hmp_userRole);
		request.setAttribute("UserRole", adminBO);	
		request.setAttribute("screen", TDSConstants.userRole);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	
	public void updateUserRole(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In updateUserRole");	
		AdminRegistrationBO adminBO=new AdminRegistrationBO();
		ArrayList userListData= null;
		adminBO=getAdminBean(adminBO,request);
		adminBO.setAssociateCode(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode());
						 	
	 	try{
	 		String forwardURL = TDSConstants.getMainJSP;
	 		int status=UserRoleDAO.updateRole(adminBO);
	 		userListData = UserRoleDAO.getUserList(adminBO.getAssociateCode());
	 		request.setAttribute("userlist", userListData);
			if(status==1){
				request.setAttribute("screen", TDSConstants.userListJSP);
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			}else{
				request.setAttribute("screen", TDSConstants.getFailureJSP);
				request.setAttribute("page"," update user rights ");
				RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
				requestDispatcher.forward(request, response);
			}
	 	}catch(Exception ex){
	 		cat.info("Exception in updating the userRole Table "+ex.getMessage());
	 	}
		
	}*/
	/*public AdminRegistrationBO getAdminBean(AdminRegistrationBO adminBO,HttpServletRequest request){
		
		if(request.getParameter("active")!=null){
			adminBO.setActive("1");
			//System.out.println("active = "+adminBO.getActive());
		}
		if(request.getParameter("inactive")!=null){
			adminBO.setActive("0");
			//System.out.println("inactive =="+adminBO.getActive());
		}
		if(request.getParameter("uname")!=null){
			adminBO.setUname((String)request.getParameter("uname"));
			//System.out.println("uname ="+adminBO.getUname());
		}
		if(request.getParameter("utype")!=null){
		adminBO.setUsertype((String)request.getParameter("utype"));
		//System.out.println("utype ="+adminBO.getUsertype());
		}
		if(request.getParameter("driverreg")!=null){
			adminBO.setDriverreg("1");
			//System.out.println("driverreg "+adminBO.getDriverreg());
		}else{
			adminBO.setDriverreg("0");
			//System.out.println("driverreg "+adminBO.getDriverreg());
		}
		if(request.getParameter("userreg")!=null){
			adminBO.setUserreg("1");
			//System.out.println("userreg "+adminBO.getUserreg());
		}else{
			adminBO.setUserreg("0");
			//System.out.println("userreg "+adminBO.getUserreg());
		}
		if(request.getParameter("acceptopenreq")!=null){
			adminBO.setAcceptopenreq("1");
			//System.out.println("acceptopenreq"+adminBO.getAcceptopenreq());
		}else{
			adminBO.setAcceptopenreq("0");
			System.out.println("acceptopenreq"+adminBO.getAcceptopenreq());
		}
		if(request.getParameter("openreqentry")!=null){
			adminBO.setOpenreqentry("1");
			//System.out.println("openreqentry "+adminBO.getOpenreqentry());
		}else{
			adminBO.setOpenreqentry("0");
			//System.out.println("openreqentry "+adminBO.getOpenreqentry());
		}
		if(request.getParameter("createvoucher")!=null){
			adminBO.setCreatevoucher("1");
			//System.out.println("create voucher"+adminBO.getCreatevoucher());
		}else{
			adminBO.setCreatevoucher("0");
			//System.out.println("create voucher"+adminBO.getCreatevoucher());
		}	
		if(request.getParameter("changeopenreq")!=null){
			adminBO.setChangeopenreq("1");
			//System.out.println("changeopenreq"+adminBO.getChangeopenreq());
		}else{
			adminBO.setChangeopenreq("0");
			//System.out.println("changeopenreq"+adminBO.getChangeopenreq());
		}
		if(request.getParameter("changevoucher")!=null){
			adminBO.setChangeVoucher("1");
			//System.out.println("driversummary "+adminBO.getDriversummary());
		}else{
			adminBO.setChangeVoucher("0");
			//System.out.println("driversummary "+adminBO.getDriversummary());
		}
		if(request.getParameter("driversummary")!=null){
			adminBO.setDriversummary("1");
			//System.out.println("driversummary "+adminBO.getDriversummary());
		}else{
			adminBO.setDriversummary("0");
			//System.out.println("driversummary "+adminBO.getDriversummary());
		}	
		if(request.getParameter("userlist")!=null){
			adminBO.setUserlist("1");
		}else{
			adminBO.setUserlist("0");
		}
		if(request.getParameter("openreqxml")!=null){
			adminBO.setOpenreqxml("1");
			//System.out.println("openreqxml"+adminBO.getOpenreqxml());
		}else{
			adminBO.setOpenreqxml("0");
			//System.out.println("openreqxml"+adminBO.getOpenreqxml());
		}
		if(request.getParameter("acceptreqxml")!=null){
			adminBO.setAcceptreqxml("1");
			System.out.println("acceptreqxml"+adminBO.getAcceptreqxml());
		}else{
			adminBO.setAcceptreqxml("0");
			//System.out.println("acceptreqxml"+adminBO.getAcceptreqxml());
		}
		if(request.getParameter("tripsummaryrep")!=null){
			adminBO.setTripsummaryrep("1");
			//System.out.println("tripsummaryrep "+adminBO.getTripsummaryrep());
		}else{
			adminBO.setTripsummaryrep("0");
			//System.out.println("tripsummaryrep "+adminBO.getTripsummaryrep());
		}if(request.getParameter("company")!=null){
			adminBO.setCompanymaster("1");
			//System.out.println("tripsummaryrep "+adminBO.getTripsummaryrep());
		}else{
			adminBO.setCompanymaster("0");
			//System.out.println("tripsummaryrep "+adminBO.getTripsummaryrep());
		}
		if(request.getParameter("voucherreport")!=null){
			adminBO.setVoucherReport("1");
		}else{
			adminBO.setVoucherReport("0");
		}
		
		if(request.getParameter("lostandfound") != null) {
			adminBO.setLostandfound("1");
		} else {
			adminBO.setLostandfound("0");
		}
		
		if(request.getParameter("maintenance") != null) {
			adminBO.setMaintenance("1");
		} else {
			adminBO.setMaintenance("0");
		}
		
		if(request.getParameter("behavior") != null) {
			adminBO.setBehavior("1");
		} else {
			adminBO.setBehavior("0");
		}
		
		if(request.getParameter("queue") != null) {
			adminBO.setQueuecoordinate("1");
		} else {
			adminBO.setQueuecoordinate("0");
		}
		
		if(request.getParameter("maintenancesummary") != null) {
			adminBO.setMaintenanceSummary("1");
		} else {
			adminBO.setMaintenanceSummary("0");
		}
		
		if(request.getParameter("lostandfoundsummary") != null) {
			adminBO.setLostandfoundSummary("1");
		} else {
			adminBO.setLostandfoundSummary("0");
		}
		
		if(request.getParameter("behaviorsummary") != null) {
			adminBO.setBehaviorSummary("1");
		} else {
			adminBO.setBehaviorSummary("0");
		}
		
		if(request.getParameter("queuesummary") != null) {
			adminBO.setQueuecoordinateSummary("1");
		} else {
			adminBO.setQueuecoordinateSummary("0");
		}
		
		if(request.getParameter("driveravailsummary") != null) {
			adminBO.setDriverAvailability("1");
		} else {
			adminBO.setDriverAvailability("0");
		}
		
		if(request.getParameter("driverpaymentreport") != null) {
			adminBO.setDriverPaymentReport("1");
		} else {
			adminBO.setDriverPaymentReport("0");
		}
		
		if(request.getParameter("companypaymentreport") != null) {
			adminBO.setCompanyPaymentReport("1");
		} else {
			adminBO.setCompanyPaymentReport("0");
		}
		return adminBO;
	}*/
}