package com.tds.action;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class ReverseDisbursement
 */
public class ReverseDisbursement extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private final Category cat = TDSController.cat;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ ServiceRequestAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReverseDisbursement() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request, response);
	}
	
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException 
	{
		
		//System.out.println("pradeep doProcess is getting called ");
		
		String eventParam = "";
		
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}

		cat.info("Event Param "+ eventParam);
		//System.out.println("pradeep doProcess is getting called "+eventParam);

		HttpSession session = request.getSession();
		
		if(session.getAttribute("user") != null) {
			
		}
	}

 }

