package com.tds.action;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.controller.TDSController;
import com.tds.dao.DispatchDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.util.CheckUserInformation;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

/**
 * Servlet implementation class OpenRequest
 */
public class OpenRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;

	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ OpenRequest.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}

    /**
     * @see HttpServlet#HttpServlet()
     */
    public OpenRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	
	protected void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	cat.info("In Process Method");
	String eventParam = "";
	HttpSession m_session = request.getSession();
	boolean m_sessionStatus = false;
	if(request.getParameter(TDSConstants.eventParam) != null) {
		eventParam = request.getParameter(TDSConstants.eventParam);
	}

	if(m_session.getAttribute("user") != null) {
		 if(eventParam.equalsIgnoreCase(TDSConstants.getOpenRequestasXML)) {
			 saveOpenRequest(request, response);

		} 
	
	}
}
	public void saveOpenRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Save Request function");
		String m_queueId = "";
		
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		
		//System.out.println("OPEN REQUEST METHOD::");
		HttpSession session = request.getSession();
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = "";
		if(adminBO.getORFormat()==1){
			screen=TDSConstants.openRequestJSP01;
		} else if(adminBO.getORFormat()==2){
			screen=TDSConstants.openRequestJSP02;
		}
		
		request.setAttribute("vecFlag", RegistrationDAO.getCmpyVehicleFlag(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode(), "2"));
		request.setAttribute("drFlag", RegistrationDAO.getCmpyVehicleFlag(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode(), "1"));
		
		OpenRequestBO openRequestBO = new OpenRequestBO();
		//DoQueueProcess m_queueIdProcess;
		openRequestBO = setOpenRequest(openRequestBO, request);
		if(request.getParameter("delete") != null ) {
			//System.out.println("in delete button");
			if(openRequestBO.getTripid().length() > 0 || openRequestBO.getTripid() != "") {
				int result = RequestDAO.deleteOpenRequest(openRequestBO.getTripid(),adminBO.getAssociateCode());
				if(result >0 ) {
					request.setAttribute("page"," deleted  a record from Open Request");
					screen = TDSConstants.getSuccessJSP;
				} else {
					//System.out.println("Fail ");
					request.setAttribute("page"," deleting a record from Open Request");
					screen = TDSConstants.getFailureJSP;
				}
			} 
		}
		else if(request.getParameter("submit") != null) {
			cat.info("In Submit button function");
			//System.out.println("DISPATCH STATUS::"+request.getParameter("dispatchStatus"));
			
			String error = openRequestValidation(openRequestBO);
			if(error.length() > 0) {
				request.setAttribute("errors", error);
				if(adminBO.getORFormat()==1){
					screen=TDSConstants.openRequestJSP01;
				} else if(adminBO.getORFormat()==2){
					screen=TDSConstants.openRequestJSP02;
				}
			} else {
				int result = 0;
				//System.out.println("Trip id "+openRequestBO.getTripid());
				//System.out.println("Queue id "+ openRequestBO.getQueueno());
				//System.out.println("Date "+openRequestBO.getSdate());
				
				if(openRequestBO.getTripid().length() == 0 || openRequestBO.getTripid() == "") {
					if(session.getAttribute("user") != null) {
						//AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
						openRequestBO.setAssociateCode(adminBO.getAssociateCode());
						if(adminBO.getDispatchMethod().equals("e")) {
							openRequestBO.setDispatchMethod("p");
						}
					}
					if(request.getParameter("queueid") != null && request.getParameter("queueid").length() >0 ) {
						openRequestBO.setQueueno(request.getParameter("queueid"));
					} else if(openRequestBO.getSlat().length() > 0 && openRequestBO.getSlong().length() > 0  ) {
						m_queueId = ServiceRequestDAO.getQueueID (openRequestBO.getSlat(),openRequestBO.getSlong());
					}

					 
					cat.info("M Queue ID "+m_queueId); 
					result = RequestDAO.saveOpenRequest(openRequestBO,adminBO,0);
					/*if(!TDSController._queueIdMap.containsKey(m_queueId) && result > 0 && !ServiceRequestDAO.checkQueueId(m_queueId) && !openRequestBO.getDispatchMethod().equals("p")) {
						System.out.println("Creating the Thread for each QueueId "+m_queueId);
						m_queueIdProcess = new DoQueueProcess(m_queueId);
						m_queueIdProcess.setName(m_queueId);
						m_queueIdProcess.start();
						TDSController._queueIdMap.put(m_queueId, m_queueIdProcess);
					}*/
				} else {
					m_queueId = openRequestBO.getQueueno();
					
				}
				if(result > 0) { 
					request.setAttribute("page"," Job Created for   "+openRequestBO.getName()+"  at:"+openRequestBO.getShrs()+" Hrs");
					if(adminBO.getORFormat()==1){
						screen=TDSConstants.openRequestJSP01;
					} else if(adminBO.getORFormat()==2){
						screen=TDSConstants.openRequestJSP02;
					}
 					
 					OpenRequestBO openRequest = new OpenRequestBO();
					//System.out.println("bean check::"+openRequest.getEadd1());
					request.setAttribute("screen", screen);
					request.setAttribute("openrequest", openRequest);
					
					RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(screen);
					requestDispatcher.forward(request, response);
				} else {
					request.setAttribute("page"," update Open Request");
					screen = TDSConstants.getFailureJSP;
				}
		}
		} else if(request.getParameter("btnphone") != null) {
			/*cat.info("In Check Phone no"); 
			HashMap hmp_phone;
			if(TDSProperties.getValue("checkserviceAPI").equalsIgnoreCase("false")) { 
				hmp_phone = CheckUserInformation.checkPhoneNo(openRequestBO.getPhone());
			} else {
				hmp_phone = CheckUserInformation.checkPhoneByAPI(openRequestBO.getPhone());
			}
			System.out.println("Data for a phone "+hmp_phone);
			System.out.println("Phone no "+openRequestBO.getPhone());
			HashMap hmp_address = RequestDAO.checkPhoneisAvailable(openRequestBO.getPhone());
			if(hmp_address.get("result").equals("1")) {
				request.setAttribute("address", hmp_address);
				System.out.println("Address "+ hmp_address);
			}
			if(hmp_phone.get("result").toString().equalsIgnoreCase("1")) {
				openRequestBO.setSadd1(hmp_phone.get("address").toString());
				openRequestBO.setScity(hmp_phone.get("city").toString());
				openRequestBO.setSstate(hmp_phone.get("state").toString());
				openRequestBO.setSzip(hmp_phone.get("zip").toString());
				openRequestBO.setStlatitude(hmp_phone.get("latitude").toString());
				openRequestBO.setStlongitude(hmp_phone.get("longitude").toString());
				if(openRequestBO.getTripid() == "" || openRequestBO.getTripid().length() == 0)
					screen = TDSConstants.openRequestJSP;
				else {
					screen = TDSConstants.updateopenRequestJSP;
				}
			} else {
				request.setAttribute("errors", "Invalid Phone number");
				if(openRequestBO.getTripid() == "" || openRequestBO.getTripid().length() == 0)
					screen = TDSConstants.openRequestJSP;
				else {
					screen = TDSConstants.updateopenRequestJSP;
				}
			}*/
		} else if(request.getParameter("btnfromaddress") != null) {
			//System.out.println("in btnfromaddress Address");
			StringBuffer error = new StringBuffer();
			int result = 0;
			if(openRequestBO.getSadd1().length()>0 && openRequestBO.getScity().length() >0 && openRequestBO.getSstate().length() >0 && openRequestBO.getSzip().length() > 0) {
				result =CheckUserInformation.checkAddress(openRequestBO.getSadd1()+" "+openRequestBO.getSadd2(), openRequestBO.getScity(), openRequestBO.getSstate(), openRequestBO.getSzip());
				//System.out.println(result);
				if(result == 0) {
					error.append("Starting Address is invalid<br>");
				}
			}
			request.setAttribute("errors", error.toString());
			if(openRequestBO.getTripid() == "" || openRequestBO.getTripid().length() == 0)
				if(adminBO.getORFormat()==1){
					screen=TDSConstants.openRequestJSP01;
				} else if(adminBO.getORFormat()==2){
					screen=TDSConstants.openRequestJSP02;
				}
			else {
				screen = TDSConstants.updateopenRequestJSP;
			}
		} else if(request.getParameter("btntoaddress") != null){
				int result = 0;
				StringBuffer error = new StringBuffer();
				//System.out.println("in btnfromaddress Address");
			if(openRequestBO.getEadd1().length() >0 && openRequestBO.getEcity().length() >0 && openRequestBO.getEstate().length() >0 && openRequestBO.getEzip().length() >0) {
				result =CheckUserInformation.checkAddress(openRequestBO.getEadd1()+" "+openRequestBO.getEadd2(), openRequestBO.getEcity()	, openRequestBO.getEstate(),openRequestBO.getEzip()) ;
				if(result == 0) {
					error.append("Ending Address is invalid<br>");
				}	
			}
			request.setAttribute("errors", error.toString());
			if(openRequestBO.getTripid() == "" || openRequestBO.getTripid().length() == 0)
				if(adminBO.getORFormat()==1){
					screen=TDSConstants.openRequestJSP01;
				} else if(adminBO.getORFormat()==2){
					screen=TDSConstants.openRequestJSP02;
				} else {
				screen = TDSConstants.updateopenRequestJSP;
			}
		} else if(request.getParameter("btnsaircode") != null) {
			//System.out.println("IN start air code Check ");
			StringBuffer error = new StringBuffer();
			int result = 0;
//			if(openRequestBO.getSaircode() != "" ) {
//				result  = ConfigDAO.checkAirCode(openRequestBO.getSaircode());
//				if(result == 0) {
//					error.append("Starting AirCode is invalid<br>");
//				}
//			}
			request.setAttribute("errors", error.toString());
			if(openRequestBO.getTripid() == "" || openRequestBO.getTripid().length() == 0)
				if(adminBO.getORFormat()==1){
					screen=TDSConstants.openRequestJSP01;
				} else if(adminBO.getORFormat()==2){
					screen=TDSConstants.openRequestJSP02;
				}else {
				screen = TDSConstants.updateopenRequestJSP;
			}
		} else if(request.getParameter("btneaircode") != null) {
			//System.out.println("IN End air code Check ");
			StringBuffer error = new StringBuffer();
			int result = 0;
//			if(openRequestBO.getEaircode() != "") {
//				result  = ConfigDAO.checkAirCode(openRequestBO.getEaircode());
//				if(result == 0) {
//					error.append("Ending AirCode is invalid<br>");
//				}
//			}
			
			request.setAttribute("errors", error.toString());
			if(openRequestBO.getTripid() == "" || openRequestBO.getTripid().length() == 0)
				if(adminBO.getORFormat()==1){
					screen=TDSConstants.openRequestJSP01;
				} else if(adminBO.getORFormat()==2){
					screen=TDSConstants.openRequestJSP02;
				}
			else {
				screen = TDSConstants.updateopenRequestJSP;
			}
		} 
		else if(request.getParameter("getzone")!=null){
			
			/*System.out.println("IN getZone Check ");
			System.out.println("lat ="+openRequestBO.getStlatitude());
			System.out.println("long ="+openRequestBO.getStlongitude());
			
			if(openRequestBO.getStlatitude()!="" && openRequestBO.getStlongitude()!=""){
				String zone=ServiceRequestDAO.getQueueID(openRequestBO.getStlatitude(), openRequestBO.getStlongitude());
			
				if(zone !=""){
					openRequestBO.setQueueno(zone);
				}				
				if(openRequestBO.getTripid() == "" || openRequestBO.getTripid().length() == 0)
					screen = TDSConstants.openRequestJSP;
				else {
					screen = TDSConstants.updateopenRequestJSP;
				}				
			} */ 
		} else {
			
			openRequestBO = new OpenRequestBO();
			//PrintWriter out = response.getWriter();
			StringBuffer buffer = new StringBuffer();
			//Get the phone number from the request
			String phoneNum = request.getParameter("phoneNo");
			openRequestBO.setSstate(adminBO.getState());
			
			//Check if phone number is not null
				//Got to the database and pick up old requests from the Openrequet table for that phone number
			if(phoneNum != null){
				//Populate an array and populate it into the request
				openRequestBO.setPhone(phoneNum);
				
				ArrayList al_list = new ArrayList();
				al_list  = RegistrationDAO.getpreviousDetails(TDSValidation.getDBPhoneFormat(phoneNum), adminBO);
				if(al_list.size()> 0)
				{
					request.setAttribute("prevopenrequests", al_list);
				}
				ArrayList al_prevaddress = new ArrayList();
				al_prevaddress  = RegistrationDAO.getFromandToAddress(TDSValidation.getDBPhoneFormat(phoneNum), "");
				if(al_prevaddress.size()> 0)
				{
					request.setAttribute("prevaddresses", al_prevaddress);
				}
			}
					
				// the array will be retrieved by the Openrequest JSP and displayed
			
			//
			cat.info("In Open Request function");
		  	forwardURL = TDSConstants.getMainNewJSP;
			cat.info("Forward the request into the "+ forwardURL +" JPS");	
			if(adminBO.getORFormat()==1){
				request.setAttribute("screen", TDSConstants.openRequestJSP01);
				screen=TDSConstants.openRequestJSP01;
			} else if(adminBO.getORFormat()==2){
				request.setAttribute("screen", TDSConstants.openRequestJSP02);
				screen=TDSConstants.openRequestJSP02;
			}	
			request.setAttribute("screen", screen);
			 
		}
		request.setAttribute("al_q", ServiceRequestDAO.getQueueName(openRequestBO.getSlat(), openRequestBO.getSlong(),(AdminRegistrationBO) session.getAttribute("user")));
		request.setAttribute("openrequest", openRequestBO);
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(screen);
		requestDispatcher.forward(request, response);
}
	public OpenRequestBO setOpenRequest(OpenRequestBO p_openRequestBO,HttpServletRequest request) {
		OpenRequestBO requestBO = p_openRequestBO; 
		requestBO.setEadd1(request.getParameter("eadd1"));
		requestBO.setEadd2(request.getParameter("eadd2"));
		requestBO.setEcity(request.getParameter("ecity"));
		requestBO.setEstate(request.getParameter("estate"));
		requestBO.setEzip(request.getParameter("ezip"));
		requestBO.setPhone(request.getParameter("phone"));
		requestBO.setSadd1(request.getParameter("sadd1"));
		requestBO.setSadd2(request.getParameter("sadd2"));
		requestBO.setScity(request.getParameter("scity"));
		requestBO.setSdate(request.getParameter("sdate"));
		requestBO.setSstate(request.getParameter("sstate"));
		//requestBO.setSstatus(request.getParameter("sstatus"));
		requestBO.setShrs(request.getParameter("shrs"));
		/*requestBO.setSmin(request.getParameter("smin"));*/
		requestBO.setSzip(request.getParameter("szip"));
//		requestBO.setSaircode(request.getParameter("saircode"));
//		requestBO.setEaircode(request.getParameter("eaircode"));
//		requestBO.setSaircodeid(request.getParameter("saircodeid"));
//		requestBO.setEaircodeid(request.getParameter("eaircodeid"));
		requestBO.setSlandmark(request.getParameter("slandmark"));
		requestBO.setSpecialIns(request.getParameter("specialIns"));
		requestBO.setSintersection(request.getParameter("sintersection"));
		//GEtting latitude and longitue from the html screen
		requestBO.setSlong(request.getParameter("sLongitude"));
		requestBO.setSlat(request.getParameter("sLatitude"));
		//TODO
		//How to check if address was checked against the yahoo database??
				
		requestBO.setPaytype((request.getParameter("paytype")!=null && !request.getParameter("paytype").equals(""))?request.getParameter("paytype"):"Cash");
		requestBO.setAcct(request.getParameter("acct"));
		if (request.getParameter("amt") != null){
			if(request.getParameter("amt") != ""){
				requestBO.setAmt(new BigDecimal(request.getParameter("amt").toString()));
			}else{
				requestBO.setAmt(new BigDecimal("0.00"));
			}
		}
		
		if(request.getParameter("driverid") != null) {
			requestBO.setDriverid(request.getParameter("driverid"));
		}
		if(request.getParameter("tripid") != null) {
			requestBO.setTripid(request.getParameter("tripid"));
		} 
		if(request.getParameter("associateCode") != null) {
			requestBO.setAssociateCode(request.getParameter("associateCode"));
		}
		if(request.getParameter("stlatitude") != null ) {
			requestBO.setSlat(request.getParameter("stlatitude"));
		}
		if(request.getParameter("stlongitude") != null) {
			requestBO.setSlong(request.getParameter("stlongitude"));
		}
		if(request.getParameter("edlatitude") != null) {
			requestBO.setEdlatitude(request.getParameter("edlatitude"));
		}
		if(request.getParameter("edlongitude") != null) {
			requestBO.setEdlongitude(request.getParameter("edlongitude"));
		}
		if(request.getParameter("name") != null) {
			requestBO.setName(request.getParameter("name"));
		}
		if(request.getParameter("queueno") != null) {
			requestBO.setQueueno(request.getParameter("queueno"));
		}if(request.getParameter("dispatchStatus") !=null) {
			requestBO.setDispatchStatus("true");
		}else {
			requestBO.setDispatchStatus("false");
			
		}
		
		
		
		
		
		try{
		 	if(request.getParameter("drprofileSize")!=null && Integer.parseInt(request.getParameter("drprofileSize")) > 0)
		 	{
		 		ArrayList al_l = new ArrayList();
		 		String drProfile="";
		 		for(int i=0;i<Integer.parseInt(request.getParameter("drprofileSize"));i++)
		 		{
		 			if(request.getParameter("dchk"+i)!=null){
		 				al_l.add(request.getParameter("dchk"+i));
		 				drProfile = drProfile + request.getParameter("dchk"+i);
		 			}
		 		}
		 		requestBO.setAl_drList(al_l);
		 		requestBO.setDrProfile(drProfile);
		 	}
		 	}catch (NumberFormatException e) {
				// TODO: handle exception
			}
		 	
		 	try{
			 	if(request.getParameter("vprofileSize")!=null && Integer.parseInt(request.getParameter("vprofileSize")) > 0)
			 	{
			 		ArrayList al_l = new ArrayList();
			 		String drProfile="";
			 		for(int i=0;i<Integer.parseInt(request.getParameter("vprofileSize"));i++)
			 		{
			 			if(request.getParameter("vchk"+i)!=null){
			 				al_l.add(request.getParameter("vchk"+i));
			 				drProfile = drProfile + request.getParameter("vchk"+i);
			 			}
			 		}
			 		requestBO.setAl_vecList(al_l);
			 		requestBO.setVecProfile(drProfile);
			 	}
			 	}catch (NumberFormatException e) {
					// TODO: handle exception
				}
		
		
		
		return requestBO;
	}

	public String openRequestValidation(OpenRequestBO p_openRequestBO) {
		StringBuffer errors = new StringBuffer();


//		if(p_openRequestBO.getSaircode().equals("") || p_openRequestBO.getSaircode() == "" || p_openRequestBO.getSaircode().length() == 0) {
//			//errors.append("You must enter starting Air Code<br>");
//		}else{
//			if(ConfigDAO.checkAirCode(p_openRequestBO.getSaircode())==1){
//				//nothing
//			}else{
//				errors.append("Please enter Valid starting Air Code<br>");
//			}
//		}
//		
//		if(p_openRequestBO.getEaircode().equals("") || p_openRequestBO.getEaircode() == "" || p_openRequestBO.getEaircode().length() == 0) {
//			//errors.append("You must enter ending Air Code<br>");
//		}else{
//			if(ConfigDAO.checkAirCode(p_openRequestBO.getEaircode())==1){
//				//nothing
//			}else{
//				errors.append("Please enter Valid ending Air Code<br>");
//			}
//		}
		
		if(!TDSValidation.isValidZipCode(p_openRequestBO.getSzip()) && p_openRequestBO.getSzip().length()>0) {
			errors.append("You entered invalid Zip Code(From)<br>");
		}
		/*
		if(!TDSValidation.isValidZipCode(p_openRequestBO.getEzip()) && p_openRequestBO.getEzip().length()>0) {
			errors.append("You entered invalid Zip code(TO)<br>");
		}
		*/
		/*if(!TDSValidation.isValidHour(p_openRequestBO.getShrs()) && p_openRequestBO.getShrs().length()>0) {
			errors.append("You entered invalid Hour<br>");
		}*/
		
       if(!TDSValidation.isOnlyNumbers(p_openRequestBO.getShrs()) || p_openRequestBO.getShrs().length() != 4 ){
			
			errors.append("Pls Provide FourDigit Time Format<br>");
		}
		if(TDSValidation.isOnlyNumbers(p_openRequestBO.getShrs()) && p_openRequestBO.getShrs().length() == 4 ){
			if(!TDSValidation.get24timeFormat(p_openRequestBO.getShrs()))	
			errors.append("You entered invalid Time Format<br>");
		}

		/*if(!TDSValidation.isValidMinutes(p_openRequestBO.getSmin()) && p_openRequestBO.getSmin().length() >0) {
			errors.append("You entered invalid Minutes<br>");
		}*/
		//System.out.println("Date "+p_openRequestBO.getPhone()+TDSValidation.chechPhoneNo(p_openRequestBO.getPhone()));
		if(!TDSValidation.chechPhoneNo(p_openRequestBO.getPhone()) && p_openRequestBO.getPhone().length() >0) {
			errors.append("You entered invalid Phone No<br>");
		} 
				/*if() == 0 && p_openRequestBO.getPhone().length() >0) {
			System.out.println("Phone No "+CheckUserInformation.checkPhoneNo(p_openRequestBO.getPhone()));
			errors.append("You Entered invalid phone no <br>");
		}*/
		
	
		if(p_openRequestBO.getSdate().length() == 10) {
			String userDate[] = p_openRequestBO.getSdate().split("/") ;
			//System.out.println(userDate[0]);
			//System.out.println(userDate[1]);
			//System.out.println(userDate[2]);
			if(!TDSValidation.isValidDate(userDate[1], userDate[0], userDate[2])){
				errors.append("You Enterd invalid Date<br>");
			}
		} else {
			errors.append("Please Provide a Service Date<br>");
		}
		return errors.toString();
	}

	public void getOpenRequestDetailForAccept(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In getOpen Request Detail into Accept Request ");
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.acceptOpenRequestJSP;
		String m_driverKey = "";
		OpenRequestBO openRequestBO = new OpenRequestBO();
		openRequestBO = setOpenRequest(openRequestBO, request);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		if(request.getParameter("submitphone") != null) {
			String jobsCode = DispatchDAO.getCompCode(openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
			if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
			} else {
				ArrayList<String> myString = new ArrayList<String>();
				openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "", "0", adminBO, myString);
			}
//			openRequestBO = RequestDAO.getOpenRequestBean(openRequestBO, "","0", adminBO);
			if(openRequestBO.getIsBeandata() == 1) {
				request.setAttribute("errors", "Tripid is not accepted");
			} else {
				request.setAttribute("errors", "Tripid is accepted");
			}
			request.setAttribute("openrequest", openRequestBO);
		} else if(request.getParameter("driveravil") != null) {
			int result = RequestDAO.checkDriverAvailable(openRequestBO.getDriverid());
			if(result == 0 ) {
				request.setAttribute("errors", "Driver is not available");
			} else if(result == 1){
				request.setAttribute("errors", "Driver is Available");
			}
			request.setAttribute("openrequest", openRequestBO);
		} else if(request.getParameter("acceptOpenRequest") != null) {
			//
			//if(openRequestBO.getIsBeandata() == 1) {
			//System.out.println("date !!!!!!!!!!!!!!!!!"+openRequestBO.getSdate());
			//String driverid = openRequestBO.getDriverid();
			openRequestBO = RequestDAO.getOpenRequestTableData(openRequestBO);
			m_driverKey = RequestDAO.getDriverKey(openRequestBO.getDriverid());
			openRequestBO.setDriverid(m_driverKey);
			int result = ServiceRequestDAO.insertAcceptedRequest(openRequestBO);
			if(result > 0) {
				request.setAttribute("page", " inserted Accepted Request");
				screen  = TDSConstants.getSuccessJSP;
			} else {
				request.setAttribute("page", " insert Accepted Request");
				screen  = TDSConstants.getFailureJSP;
			}
			
		}
		
		else if(request.getParameter("updateOpenRequest") != null){
			//System.out.println("In updateOpenRequest");
			m_driverKey = RequestDAO.getDriverKey(openRequestBO.getDriverid());
			openRequestBO.setDriverid(m_driverKey);
			int result = ServiceRequestDAO.changeAcceptedRequest(openRequestBO, "update",adminBO);
			if(result > 0){
				request.setAttribute("page", " updated Accepted Request");
				screen  = TDSConstants.getSuccessJSP;
			} else {
				request.setAttribute("page", " update Accepted Request");
				screen  = TDSConstants.getFailureJSP;
			}
			
		}
		else if(request.getParameter("deleteOpenRequest") != null){
			//System.out.println("In deleteOpenRequest");
			int result = ServiceRequestDAO.changeAcceptedRequest(openRequestBO, "delete",adminBO);
			if(result > 0){
				request.setAttribute("page", " deleted Accepted Request");
				screen  = TDSConstants.getSuccessJSP;
			} else {
				request.setAttribute("page", " delete Accepted Request");
				screen  = TDSConstants.getFailureJSP;
			}
		}
		
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}
	
	public void getAcceptedRequest(HttpServletRequest request,HttpServletResponse response ) throws ServletException,IOException {
		//System.out.println("in getAcceptedRequest");
		cat.info("In getAcceptedRequest");
		String phoneno="";
		String tripid="";
		String m_driverKey = "";
		int status=0;
		String forwardURL = TDSConstants.getMainNewJSP;
		String screen = TDSConstants.getAcceptedRequestToEditJSP;
		//System.out.println("update ="+request.getParameter("update"));
		//System.out.println("delete ="+request.getParameter("delete"));
		HashMap hs_data=new HashMap();
		OpenRequestBO openBO=new OpenRequestBO();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		
		if(request.getParameter("getrecord")!=null){
			//System.out.println("phone ="+request.getParameter("phone"));
			phoneno=request.getParameter("phone");
			screen = TDSConstants.getAcceptedRequestToEditJSP;
			openBO.setPhone(phoneno);
			try{
				//hs_data=RequestDAO.getAcceptedRequest(openBO);
				request.setAttribute("AcceptedData", hs_data);
				System.out.println("al_requestdata =="+hs_data);
			}
			catch(Exception ex){
				cat.info("Exception in the getAcceptedRequest :"+ex.getMessage());
			}
		}
		
		if(request.getParameter("update")!=null){
			//System.out.println("update");			
			if(request.getParameter("tripid")!=null){
				tripid=request.getParameter("tripid");
				openBO.setTripid(tripid);
				openBO=setOpenRequest(openBO,request);
				m_driverKey = RequestDAO.getDriverKey(openBO.getDriverid());
				openBO.setDriverid(m_driverKey);
				status=RequestDAO.updateAcceptedRequest(openBO,adminBO);
				if(status > 0){
					screen = TDSConstants.getSuccessJSP;
				}
				else{
					screen = TDSConstants.getFailureJSP;
				}
			}
		}
		if(request.getParameter("delete")!=null){
			//System.out.println("delete");
			if(request.getParameter("tripid")!=null){
				tripid=request.getParameter("tripid");
				openBO.setTripid(tripid);
				m_driverKey = RequestDAO.getDriverKey(openBO.getDriverid());
				openBO.setDriverid(m_driverKey);
				status=RequestDAO.deleteAcceptedRequest(openBO,adminBO);
				if(status > 0){
					screen = TDSConstants.getSuccessJSP;
				}
			}
		}
		request.setAttribute("screen", screen);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(forwardURL);
		requestDispatcher.forward(request, response);
	}


}
