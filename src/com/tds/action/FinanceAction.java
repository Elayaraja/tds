package com.tds.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.bean.ChargesBO;
import com.charges.dao.ChargesDAO;
import com.tds.cmp.bean.CashSettlement;
import com.tds.cmp.bean.DriverDisbursment;
import com.tds.cmp.bean.PenalityBean;
import com.tds.controller.MailReport;
import com.tds.controller.TDSController;
import com.tds.dao.EmailSMSDAO;
import com.tds.dao.FinanceDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.ReverseDisbrusementDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.InvoiceBO;
import com.tds.tdsBO.PaymentSettledDetail;
import com.tds.tdsBO.PrepaidCardBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class SystemSetup
 */
public class FinanceAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;

	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name " + ServiceRequestAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FinanceAction() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest
	 * , javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doProcess(request, response);
	}

	public void doProcess(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String eventParam = "";

		if (request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}

		cat.info("Event Param " + eventParam);

		HttpSession session = request.getSession();

		if (session.getAttribute("user") != null) {

			if (eventParam.equalsIgnoreCase("cashSettlement")) {
				setCashSettlement(request, response);
			} else if (eventParam.equalsIgnoreCase("chequeDetails")) {
				setChequeDetails(request, response);
			} else if (eventParam.equalsIgnoreCase("reversePaymentEvent")) {
				reverseDisbursement(request, response);
			} else if (eventParam.equalsIgnoreCase("generateInvoice")) {
				generateInvoice(request, response);
			} else if (eventParam.equalsIgnoreCase("invoicePayment")) {
				invoicePayment(request, response);
			} else if (eventParam.equalsIgnoreCase("getUnInvoice")) {
				getUnInvoice(request, response);
			} else if (eventParam.equalsIgnoreCase("prepaidcard")) {
				getPrepaidCard(request, response);
			} else if (eventParam.equalsIgnoreCase("verifyVoucher")) {
				verifyVoucher(request, response);
			} else if (eventParam.equalsIgnoreCase("grantAccessToAssistant")) {
				grantAccessToAssistant(request, response);
			} else if (eventParam.equalsIgnoreCase("generateUninvoice")) {
				generateUninvoice(request, response);
			}
		}
	}

	public void setCashSettlement(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request
				.getSession().getAttribute("user");
		CashSettlement cspBean = new CashSettlement();
		// Following code use as follows
		// First case: Empty screen is thrown
		if (request.getParameter("CheckRegisterStatus") == null) {
			request.setAttribute("csregBO", cspBean);
			request.setAttribute("screen", "/Financial/CashRegister.jsp");
			RequestDispatcher requestDispatcher = getServletContext()
					.getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);

		}
		// Second Case: Operator id is entered by the user.
		// Based on the status of the operator's register status
		// a open register or a closed register screen is shown to the user
		else if (request.getParameter("CheckRegisterStatus") != null) {
			cspBean.setAssociateCode(adminBO.getAssociateCode());
			cspBean.setOperatorId(request.getParameter("OperatorId"));
			CashSettlement registerOpen = FinanceDAO.readCashRegisterMaster(
					cspBean.getAssociateCode(), cspBean.getOperatorId());
			request.setAttribute("RegisterValues", registerOpen);
			if (registerOpen.isRegisterOpen()) {
				ArrayList<DriverDisbursment> disbursementDetail = FinanceDAO
						.FinanceDetails(adminBO.getAssociateCode(),
								cspBean.getOperatorId());
				request.setAttribute("DisbursementDetails", disbursementDetail);
				CashSettlement cashsettle = FinanceDAO.readCashRegisterMaster(
						adminBO.getAssociateCode(), cspBean.getOperatorId());
				request.setAttribute("CashSettlement", cashsettle);
				request.setAttribute("screen",
						"/Financial/CashRegisterClose.jsp");
			} else {
				// Register is closed
				request.setAttribute("screen",
						"/Financial/CashRegisterOpen.jsp");
			}
			request.setAttribute("csregBO", cspBean);
			RequestDispatcher requestDispatcher = getServletContext()
					.getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);
		}
		// Third Case: Operator id is entered by the user.
		// Based on the status of the operator's register status
		// a open register or a closed register screen is shown to the user
		if (request.getParameter("OpenRegister") != null) {
			cspBean.setAssociateCode(adminBO.getAssociateCode());
			cspBean.setOperatorId(request.getParameter("OperatorId"));
			cspBean.setOpenCash(Integer.parseInt(request
					.getParameter("Opencash")));
			cspBean.setIntialChequeNo(Integer.parseInt(request
					.getParameter("OpenChequeno")));
			cspBean.setOpenedBy(adminBO.getUid());
			cspBean.setClosingCash(Integer.parseInt(request
					.getParameter("Opencash")));
			cspBean.setClosingCheckNo(Integer.parseInt(request
					.getParameter("OpenChequeno")));
			FinanceDAO.insertCashRegisterMaster(cspBean);
		}
		// Fourth Case: Operator id is entered by the user.
		// Based on the status of the operator's register status
		// a open register or a closed register screen is shown to the user
		else if (request.getParameter("CloseRegister") != null) {
			cspBean.setAssociateCode(adminBO.getAssociateCode());
			cspBean.setOperatorId(request.getParameter("OperatorId"));
			cspBean.setOpenCash(Integer.parseInt(request
					.getParameter("Opencash")));
			cspBean.setIntialChequeNo(Integer.parseInt(request
					.getParameter("OpenChequeno")));
			cspBean.setClosingCash(Integer.parseInt(request
					.getParameter("Closingcash")));
			cspBean.setClosingCheckNo(Integer.parseInt(request
					.getParameter("Closingcheckno")));
			cspBean.setClosedBy(adminBO.getUid());
			cspBean.setReceivedCheques(request.getParameter("receivedCheques"));
			FinanceDAO.closeCashRegisterMaster(cspBean);
		}

	}

	public void getUnInvoice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<String> voucher = new ArrayList<String>();
		int option = Integer.parseInt(request.getParameter("verify"));
		String assoccode =(request.getParameter("assoccode")!=null)?request.getParameter("assoccode"):adminBO.getAssociateCode();
		//System.out.println("assocode:"+assoccode);
		voucher = FinanceDAO.getUninvoicedVouchers(adminBO.getMasterAssociateCode(),assoccode, option);
		JSONArray voucherArr = new JSONArray();
		for (int i = 0; i < voucher.size();) {
			JSONObject vObj = new JSONObject();
			try {
				vObj.put("VN", voucher.get(i));
				vObj.put("T", voucher.get(i + 1));
				vObj.put("RN", voucher.get(i + 2));
				voucherArr.put(vObj);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			i = i + 3;
		}
		response.getWriter().write(voucherArr.toString());
	}

	public void generateUninvoice(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String[] voucher = request.getParameter("voucher").split(";");
		int invoiceNo = 0;
		JSONArray array = new JSONArray();
		PaymentSettledDetail voucherBean = new PaymentSettledDetail();
		for(int j=0;j<voucher.length;j++){
			ArrayList<PaymentSettledDetail> voucherDetail = FinanceDAO.getVoucherByVoucher(adminBO, voucher[j]);
			String[] setId = new String[voucherDetail.size()];
			double amount = 0.00;
			for (int i = 0; i < voucherDetail.size(); i++) {
				amount += Double.parseDouble(voucherDetail.get(i).getTotal());
				setId[i] = voucherDetail.get(i).getTransId();
			}
			voucherBean.setVoucherNo(voucherDetail.get(0).getVoucherNo());
			voucherBean.setTotal(amount + "");
			voucherBean.setTotalcount(voucherDetail.size() + "");
			JSONObject invoiceNumbers = new JSONObject();
			try {
				invoiceNo = FinanceDAO.insertInvoice(adminBO, voucherBean, setId);
				invoiceNumbers.put("IN",invoiceNo);
				array.put(invoiceNumbers);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		response.getWriter().write(array.toString());
	}

	private void setChequeDetails(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request
				.getSession().getAttribute("user");
		ArrayList al_list = new ArrayList();
		DriverDisbursment disbursement = new DriverDisbursment();
		if (request.getParameter("Submit") == null) {
			request.setAttribute("DriverDate", disbursement);
			request.setAttribute("screen", "/Financial/ChequeDetail.jsp");
		} else if (request.getParameter("Submit") != null) {
			disbursement.setDate(request.getParameter("Date"));
			disbursement.setDriverid(request.getParameter("DriverId"));
			disbursement
			.setChequeReceived(request.getParameter("received") == null ? ""
					: request.getParameter("received"));
			disbursement
			.setChequeBounced(request.getParameter("bounced") == null ? ""
					: request.getParameter("bounced"));
			ArrayList<DriverDisbursment> chequeDetail = FinanceDAO
					.getChequeDetails(disbursement, adminBO.getAssociateCode());
			request.setAttribute("ChequeDetails", chequeDetail);
			request.setAttribute("screen", "/Financial/ChequeDetail.jsp");
		}
		if (request.getParameter("Received") != null) {
			int numberOfRows = Integer.parseInt(request
					.getParameter("numberOfRows"));
			ArrayList<DriverDisbursment> payIDsToMarkReceived = new ArrayList<DriverDisbursment>();
			// Reading the selected rows from the screen
			for (int rowIterator = 0; rowIterator < numberOfRows; rowIterator++) {
				if (request.getParameter("Row_" + rowIterator) != null) {
					DriverDisbursment payIdFromScreen = new DriverDisbursment();
					payIdFromScreen.setPayment_id(request.getParameter("Row_"
							+ rowIterator));
					payIDsToMarkReceived.add(payIdFromScreen);
				}
			}
			// Getting the details of rows that were selected
			FinanceDAO.updateChecksAsReceivedOrBounced(payIDsToMarkReceived,
					adminBO.getAssociateCode(), 1);
			disbursement.setDate(request.getParameter("Date"));
			disbursement.setDriverid(request.getParameter("DriverId"));
			disbursement
			.setChequeReceived(request.getParameter("received") == null ? ""
					: request.getParameter("received"));
			disbursement
			.setChequeBounced(request.getParameter("bounced") == null ? ""
					: request.getParameter("bounced"));
			ArrayList<DriverDisbursment> chequeDetail = FinanceDAO
					.getChequeDetails(disbursement, adminBO.getAssociateCode());
			request.setAttribute("ChequeDetails", chequeDetail);
			request.setAttribute("screen", "/Financial/ChequeDetail.jsp");
		} else if (request.getParameter("Bounced") != null) {
			// Bounced button pressed for the selected check numbers
			int numberOfRows = Integer.parseInt(request
					.getParameter("numberOfRows"));
			ArrayList<DriverDisbursment> payIDsToMarkBounced = new ArrayList<DriverDisbursment>();
			// Reading the selected rows from the screen
			for (int rowIterator = 0; rowIterator < numberOfRows; rowIterator++) {
				if (request.getParameter("Row_" + rowIterator) != null) {
					DriverDisbursment payIdFromScreen = new DriverDisbursment();
					payIdFromScreen.setPayment_id(request.getParameter("Row_"
							+ rowIterator));
					payIDsToMarkBounced.add(payIdFromScreen);
				}
			}
			// Getting the details of rows that were selected
			FinanceDAO.updateChecksAsReceivedOrBounced(payIDsToMarkBounced,
					adminBO.getAssociateCode(), 2);
			disbursement.setDate(request.getParameter("Date"));
			disbursement.setDriverid(request.getParameter("DriverId"));
			disbursement
			.setChequeReceived(request.getParameter("received") == null ? ""
					: request.getParameter("received"));
			disbursement
			.setChequeBounced(request.getParameter("bounced") == null ? ""
					: request.getParameter("bounced"));
			ArrayList<DriverDisbursment> populatedDataList = FinanceDAO
					.readValuesByPayID(payIDsToMarkBounced,
							adminBO.getAssociateCode());
			ArrayList<DriverDisbursment> chequeDetail = FinanceDAO
					.getChequeDetails(disbursement, adminBO.getAssociateCode());
			request.setAttribute("ChequeDetails", chequeDetail);
			// Populating the vales in array list to be sent to the misc charges
			// screen/table
			for (int rowIterator = 0; rowIterator < populatedDataList.size(); rowIterator++) {
				PenalityBean penaltyBean = new PenalityBean();
				penaltyBean.setB_ass_code(adminBO.getAssociateCode());
				penaltyBean.setB_driver_id(populatedDataList.get(rowIterator)
						.getDriverid());
				penaltyBean.setBtype("2");
				penaltyBean.setB_amount(populatedDataList.get(rowIterator)
						.getAmount());
				penaltyBean.setB_desc("Bounced Check #"
						+ populatedDataList.get(rowIterator).getCheckno()
						+ "provided on "
						+ populatedDataList.get(rowIterator).getDate());
				penaltyBean.setB_p_date(populatedDataList.get(rowIterator)
						.getDate());
				al_list.add(penaltyBean);

				PenalityBean penaltyBeanForBouncedCharge = new PenalityBean();
				penaltyBeanForBouncedCharge.setB_ass_code(adminBO
						.getAssociateCode());
				penaltyBeanForBouncedCharge.setB_driver_id(populatedDataList
						.get(rowIterator).getDriverid());
				penaltyBeanForBouncedCharge.setBtype("2");
				penaltyBeanForBouncedCharge.setB_amount(request
						.getParameter("bounceCharge"));
				penaltyBeanForBouncedCharge
				.setB_desc("Bounced Check Charge for check#"
						+ populatedDataList.get(rowIterator)
						.getCheckno() + "provided on "
						+ populatedDataList.get(rowIterator).getDate());
				penaltyBeanForBouncedCharge.setB_p_date(populatedDataList.get(
						rowIterator).getDate());
				al_list.add(penaltyBeanForBouncedCharge);

			}

			// Making an entry into the Misc Charges screen forbounced check and
			// the corresponding charges.
			PenalityBean penaltyBean = new PenalityBean();
			RegistrationDAO.insertDriverPenality(al_list, "1",
					adminBO.getAssociateCode());
			request.setAttribute("screen", "/Financial/ChequeDetail.jsp");
		}
		RequestDispatcher requestDispatcher = getServletContext()
				.getRequestDispatcher(TDSConstants.getMainJSP);
		requestDispatcher.forward(request, response);

	}

	public void reverseDisbursement(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request
				.getSession().getAttribute("user");
		// ReverseDisbrusementBean revdisABO = new ReverseDisbrusementBean();
		// revdisABO.setUserName(adminBO.getUid());
		ArrayList al_revdis = new ArrayList();

		// if(request.getParameter("Button")!= null){

		// ArrayList al_revdis = null;
		String driverID = request.getParameter("Driverid") == null ? ""
				: request.getParameter("Driverid");
		String amountD = request.getParameter("AmountD") == null ? "" : request
				.getParameter("AmountD");
		String paymentDate = request.getParameter("PaymentDate") == null ? ""
				: request.getParameter("PaymentDate");
		String operatorID = request.getParameter("OperatorID") == null ? ""
				: request.getParameter("OperatorID");
		String chequeORCash = request.getParameter("ChequeORCash") == null ? ""
				: request.getParameter("ChequeORCash");
		String checkNO = request.getParameter("CheckNO") == null ? "" : request
				.getParameter("CheckNO");

		if (request.getParameter("Button") != null) {
			al_revdis = ReverseDisbrusementDAO.getReverseDisbrusement(
					adminBO.getAssociateCode(), driverID, paymentDate,
					operatorID, chequeORCash, amountD, checkNO);
			request.setAttribute("Searched", "Yes");

		}
		// Condition when a reverse button is submitted
		// This will cause the reversalPayID to be submitted.
		else if (request.getParameter("ReversalPayID") != null) {
			// ArrayList al_revdis = null;
			String payID = request.getParameter("ReversalPayID");
			AdminRegistrationBO adminBO1 = (AdminRegistrationBO) request
					.getSession().getAttribute("user");

			boolean updateSuccessFlag = ReverseDisbrusementDAO
					.updateReverseDisbrusement(adminBO.getUid(),
							adminBO1.getAssociateCode(), payID);
			al_revdis = ReverseDisbrusementDAO.getReverseDisbrusement(
					adminBO.getAssociateCode(), driverID, paymentDate,
					operatorID, chequeORCash, amountD, checkNO);

		}
		request.setAttribute("screen", "/Financial/ReversePayment.jsp");
		request.setAttribute("revdisData", al_revdis);
		RequestDispatcher requestDispatcher = getServletContext()
				.getRequestDispatcher(TDSConstants.getMainJSP);
		requestDispatcher.forward(request, response);

	}

	public void verifyVoucher(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		String[] transID = { request.getParameter("transId") };
		int status = FinanceDAO.upDateVoucherAsverified(
				adminBO.getAssociateCode(), transID);
		response.getWriter().write(status + "");
	}

	public void generateInvoice(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException {
		PaymentSettledDetail invoiceBo = new PaymentSettledDetail();
		invoiceBo.setVoucherNo(request.getParameter("voucherNo"));
		invoiceBo.setCostCenter(request.getParameter("cccenter"));
		invoiceBo.setCompanyCode(request.getParameter("ccode"));
		String fDate = request.getParameter("fDate");
		String tDate = request.getParameter("tDate");

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<PaymentSettledDetail> voucher = new ArrayList<PaymentSettledDetail>();
		if (request.getParameter("search") == null) {
			request.setAttribute("voucher", voucher);
			request.setAttribute("voucherNo", "");
			request.setAttribute("verifyCheck", invoiceBo.getVerified());
			request.setAttribute("screen", "/Financial/GenerateInvoice.jsp");
		}
		if (request.getParameter("search") != null && request.getParameter("search").equals("search")) {
			invoiceBo.setVerified(Integer.parseInt(request.getParameter("verifyCheck")));
			voucher = FinanceDAO.getVoucherDetails(adminBO, invoiceBo, 1,fDate, tDate);
			request.setAttribute("voucher", voucher);
			request.setAttribute("voucherNo", invoiceBo.getVoucherNo());
			request.setAttribute("verifyCheck", invoiceBo.getVerified());
			request.setAttribute("screen", "/Financial/GenerateInvoice.jsp");
		}
		if (request.getParameter("verify") != null
				&& request.getParameter("verify").equalsIgnoreCase("verify")) {
			String[] setID = request.getParameterValues("generate");
			if (setID != null) {
				FinanceDAO.upDateVoucherAsverified(adminBO.getAssociateCode(),
						setID);
			}
		}
		if (request.getParameter("generateInvoice") != null	&& request.getParameter("generateInvoice").equalsIgnoreCase("generateInvoice")) {
			String[] setID = request.getParameterValues("generate");
			int invoiceNo = 0;
			if (setID != null) {
				PaymentSettledDetail invoiceBean = FinanceDAO.getVoucherById(adminBO, setID);
				try {
					invoiceNo = FinanceDAO.insertInvoice(adminBO, invoiceBean, setID);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					request.setAttribute("error", e.getMessage());
				}
				request.setAttribute("invoiceNo", invoiceNo);
				if(invoiceBean.getEmailAddress()!=null && !invoiceBean.getEmailAddress().equals("")){
					ArrayList<String> emails=EmailSMSDAO.emailUsers(adminBO.getMasterAssociateCode());
					String url1="http://www.getacabdemo.com";
					if(request.getServerName().contains("getacabdemo.com")){
						url1="http://www.getacabdemo.com";
					} else {
						url1="https://www.gacdispatch.com";
					}
					try {
						MailReport mailPass = new MailReport(url1, invoiceNo+"", invoiceBo.getEmailAddress(), adminBO.getTimeZone(), emails, "invoice" , adminBO.getMasterAssociateCode(),2,"Invoice generated");
						mailPass.start();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		if (request.getParameter("update") != null
				&& request.getParameter("update").equals("update")) {
			invoiceBo
			.setRiderName(request.getParameter("riderName") == null ? ""
					: request.getParameter("riderName"));
			invoiceBo
			.setCostCenter(request.getParameter("costCenter") == null ? ""
					: request.getParameter("costCenter"));
			invoiceBo.setAmount(request.getParameter("amount") == null ? ""
					: request.getParameter("amount"));
			invoiceBo
			.setExpDate(request.getParameter("expiryDate") == null ? ""
					: request.getParameter("expiryDate"));
			invoiceBo.setInvoiceNumber(Integer.parseInt(request
					.getParameter("invoiceNo") == null ? "" : request
							.getParameter("invoiceNo")));
			invoiceBo
			.setCreationDate(request.getParameter("creationDate") == null ? ""
					: request.getParameter("creationDate"));
			invoiceBo
			.setCompanyCode(request.getParameter("companyCode") == null ? ""
					: request.getParameter("companyCode"));
			invoiceBo.setTripId(request.getParameter("tripId") == null ? ""
					: request.getParameter("tripId"));
			invoiceBo.setContact(request.getParameter("contact") == null ? ""
					: request.getParameter("contact"));
			invoiceBo
			.setDelayDays(request.getParameter("delayDays") == null ? ""
					: request.getParameter("delayDays"));
			invoiceBo.setFreq(request.getParameter("frequent") == null ? ""
					: request.getParameter("frequent"));
			invoiceBo
			.setServiceDate(request.getParameter("serviceDate") == null ? ""
					: request.getParameter("serviceDate"));
			invoiceBo.setTransId(request.getParameter("transId") == null ? ""
					: request.getParameter("transId"));
			invoiceBo.setDriverId(request.getParameter("driverId") == null ? ""
					: request.getParameter("driverId"));
			invoiceBo.setTip(request.getParameter("tip") == null ? "" : request
					.getParameter("tip"));
			invoiceBo
			.setRetAmount(request.getParameter("retAmount") == null ? ""
					: request.getParameter("retAmount"));
			invoiceBo.setDescription(request.getParameter("desc") == null ? ""
					: request.getParameter("desc"));
			invoiceBo.setVoucherNo(request.getParameter("acctNum") == null ? ""
					: request.getParameter("acctNum"));
			invoiceBo.setPaymentReceivedStatus(Integer.parseInt(request
					.getParameter("paymentStatus") == null ? "0" : request
							.getParameter("paymentStatus")));
			if (request.getParameter("fieldSize") != null) {
				int totalCharges = Integer.parseInt(request.getParameter(
						"fieldSize").equals("") ? "0" : request
								.getParameter("fieldSize"));
				//System.out.println("Total CHarges--->" + totalCharges);
				String chArrKey[] = new String[totalCharges];
				String chArrAmt[] = new String[totalCharges];
				double amount = 0;
				for (int i = 0; i < totalCharges; i++) {
					if (request.getParameter("chKey_" + i) != null
							&& request.getParameter("chAmt_" + i) != null) {
						chArrKey[i] = request.getParameter("chKey_" + i);
						chArrAmt[i] = request.getParameter("chAmt_" + i);
						amount += Double.parseDouble(request
								.getParameter("chAmt_" + i));
					}
				}
				invoiceBo.setAmount(amount + "");
				invoiceBo.setTotalAmount(amount);
				ChargesDAO.insertDriverChargeSToMobile(
						adminBO.getAssociateCode(), chArrAmt, chArrKey,
						request.getParameter("tripId"), totalCharges,
						request.getParameter("driverId"), "no",adminBO.getMasterAssociateCode());
			}
			FinanceDAO.updateVoucherByVoucherNo(adminBO.getAssociateCode(),
					invoiceBo);
			// invoiceBo.setVoucherNo(request.getParameter("voucherNo"));
			voucher = FinanceDAO.getVoucherDetails(adminBO, invoiceBo, 1, "",
					"");
			request.setAttribute("voucher", voucher);
			request.setAttribute("screen", "/Financial/GenerateInvoice.jsp");
		}
		if (request.getParameter("edit") != null
				&& request.getParameter("edit").equals("yes")) {
			String transId = request.getParameter("transId");
			invoiceBo.setTransId(transId);
			ArrayList<ChargesBO> chargeType = ChargesDAO.getChargeTypes(adminBO
					.getMasterAssociateCode(),0);
			request.setAttribute("chargeTypes", chargeType);
			PaymentSettledDetail invoiceBO = FinanceDAO.editVoucherByVoucherNo(
					adminBO.getAssociateCode(), invoiceBo);
			request.setAttribute("voucher", invoiceBO);
			request.setAttribute("screen", "/Financial/EditInvoice.jsp");
		}
		if (request.getParameter("docId") != null) {
			Blob blobData = null;

			String documentId = request.getParameter("docId") != null ? request
					.getParameter("docId") : "";
					blobData = FinanceDAO.viewSign(documentId);
					OutputStream outStream = response.getOutputStream();
					InputStream in = null;
					try {
						if (blobData != null) {
							in = blobData.getBinaryStream();
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (in != null) {
						int nextByte = in.read();
						while (nextByte != -1) {
							outStream.write(nextByte);
							nextByte = in.read();
						}
					}
					outStream.flush();
		}
		if (request.getParameter("delete") != null) {
			String transId = request.getParameter("transId") == null ? ""
					: request.getParameter("transId");
			String tripId = request.getParameter("tripId") == null ? ""
					: request.getParameter("tripId");
			int status = FinanceDAO
					.deleteVoucherEntry(adminBO, transId, tripId);
			response.getWriter().write(status + "");
			return;
		}

		RequestDispatcher requestDispatcher = getServletContext()
				.getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void invoicePayment(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		InvoiceBO invoiceBO = new InvoiceBO();
		invoiceBO.setVoucherNo(request.getParameter("voucherNo"));

		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		ArrayList<InvoiceBO> voucher = new ArrayList<InvoiceBO>();
		if (request.getParameter("search") == null) {
			request.setAttribute("voucher", voucher);
			request.setAttribute("screen", "/Financial/InvoicePayment.jsp");
		}
		if (request.getParameter("search") != null) {
			int searchBasedOn = Integer.parseInt(request.getParameter("basedOn"));
			voucher = FinanceDAO.getInvoiceDetails(adminBO, invoiceBO, searchBasedOn);
			request.setAttribute("voucher", voucher);
			request.setAttribute("screen", "/Financial/InvoicePayment.jsp");
		}
		if (request.getParameter("delete") != null && !request.getParameter("delete").equals("")) {
			PaymentSettledDetail invoiceBo = new PaymentSettledDetail();
			invoiceBo.setInvoiceNumber(Integer.parseInt(request.getParameter("invoiceNo")));
			FinanceDAO.deleteInvoice(adminBO.getAssociateCode(),invoiceBo.getInvoiceNumber());
			voucher = FinanceDAO.getInvoiceDetails(adminBO, invoiceBO, 0);
			request.setAttribute("voucher", voucher);
			request.setAttribute("screen", "/Financial/InvoicePayment.jsp");
		}
		if (request.getParameter("Detail") != null) {
			PaymentSettledDetail invoiceBo = new PaymentSettledDetail();
			invoiceBo.setInvoiceNumber(Integer.parseInt(request.getParameter("invoiceNo")));
			ArrayList<PaymentSettledDetail> vouchers = FinanceDAO.getVoucherDetails(adminBO, invoiceBo, 2, "", "");
			request.setAttribute("voucher", vouchers);
			request.setAttribute("screen", "/Financial/InvoiceDetails.jsp");
		}

		if (request.getParameter("pay") != null	|| request.getParameter("partlyPay") != null || request.getParameter("cancel") != null) {
			int total = Integer.parseInt(request.getParameter("totalInvoice"));
			invoiceBO.setChequeNumber(request.getParameter("chequeNum"));

			String[] voucherNo = new String[total];
			for (int i = 0; i < total; i++) {
				if (request.getParameter("mark" + i) != null) {
					voucherNo[i] = request.getParameter("mark" + i);
				}
			}
			if (request.getParameter("pay") != null) {
				FinanceDAO.updateInvoice(adminBO, voucherNo, total,invoiceBO.getChequeNumber(), 2);
			} else if (request.getParameter("partlyPay") != null) {
				FinanceDAO.updateInvoice(adminBO, voucherNo, total,invoiceBO.getChequeNumber(), 1);
			}
			// else if(request.getParameter("cancel")!=null){
			// FinanceDAO.updateInvoice(adminBO,
			// voucherNo,total,invoiceBO.getChequeNumber(),3);
			// }
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(request, response);
	}

	public void getPrepaidCard(HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException {
		//System.out.println("finance action");
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");
		PrepaidCardBO prepaidcard1 = new PrepaidCardBO();
		if (request.getParameter("buttonSubmit") != null && request.getParameter("buttonSubmit").equalsIgnoreCase("Update")) {
			String Cardnum = request.getParameter("cardno2");
			String driverID = request.getParameter("driverid");
			String Amount = request.getParameter("amount");
			String pinNum = request.getParameter("pinno");
			FinanceDAO.updateprepaidcard(adminBO.getAssociateCode(), driverID, Amount, pinNum);
			request.setAttribute("screen", "/Financial/PrepaidCard.jsp");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);

		} else if (request.getParameter("buttonSubmit") != null && request.getParameter("buttonSubmit").equalsIgnoreCase("submit")) {
			int numberOfRows = Integer.parseInt(request.getParameter("fieldSize"));
			String[] cardno = new String[numberOfRows + 1];
			String[] driverid = new String[numberOfRows + 1];
			String[] amount = new String[numberOfRows + 1];
			String[] pinno = new String[numberOfRows + 1];

			for (int rowIterator = 1; rowIterator <= numberOfRows; rowIterator++) {
				cardno[rowIterator] = request.getParameter("cardno"	+ rowIterator);
				driverid[rowIterator] = request.getParameter("driverId"	+ rowIterator);
				amount[rowIterator] = request.getParameter("amount"	+ rowIterator);
				pinno[rowIterator] = request.getParameter("pinno" + rowIterator);

			}
			try {
				int result = FinanceDAO.insertprepaidcard(cardno,adminBO.getAssociateCode(), driverid, pinno, amount,numberOfRows);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				request.setAttribute("error", e.getMessage());

			}

			request.setAttribute("screen", "/Financial/PrepaidCard.jsp");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);

		}

		else {
			String cardno = request.getParameter("cardNo");
			// System.out.println("cardno"+cardno);
			String array[] = FinanceDAO.getprepaidcard(adminBO.getAssociateCode(), cardno);
			JSONArray array1 = new JSONArray();
			try {
				JSONObject details = new JSONObject();
				details.put("cno", array[0]);
				details.put("allto", array[1]);
				details.put("pin", array[3]);
				details.put("amt", array[2]);
				array1.put(details);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.getWriter().write(array1.toString());
		}
	}

	public void grantAccessToAssistant(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request
				.getSession().getAttribute("user");
		String emailId = request.getParameter("emailId");
		String accountNo = request.getParameter("accountNo");

		String[] accountNoList = accountNo.split(";");
		if (!accountNo.contains(";") && accountNoList.length == 0) {
			accountNoList[0] = accountNo;
		}
		Boolean status = FinanceDAO.insertAccessForAssistants(accountNoList,
				emailId, adminBO.getAssociateCode());
		response.getWriter().write(status.toString());
	}

}
