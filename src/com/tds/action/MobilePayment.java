package com.tds.action;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.charges.constant.ICCConstant;
import com.charges.dao.ChargesDAO;
import com.common.action.PaymentProcessingAction;
import com.tds.controller.TDSController;
import com.tds.dao.AuditDAO;
import com.tds.dao.ProcessingDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.UtilityDAO;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.VoucherBO;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

public class MobilePayment extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final Category cat = TDSController.cat;
	
	
	@Override
	public void init(ServletConfig config) throws ServletException {
//		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
//		cat.info("Class Name "+ ServiceRequestAction.class.getName());
//		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	
	
	@Override
	public void doGet(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		try {
			doProcess(p_request, p_response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		try {
			doProcess(p_request, p_response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException, JSONException {
		
		String m_eventParam = "";
		
		if(p_request.getParameter(TDSConstants.eventParam) != null) {
			m_eventParam = p_request.getParameter(TDSConstants.eventParam);
		}
		HttpSession m_session = p_request.getSession(false);
	 
		if(m_session!=null && m_session.getAttribute("user") != null) {
			
			if(m_eventParam.equalsIgnoreCase("cca")) {			 
				chckUserCardInfo(p_request,p_response);
			}else if(m_eventParam.equalsIgnoreCase("ccadj")) {
				alterPayment(p_request,p_response);
			} else if(m_eventParam.equalsIgnoreCase("csign")) {			 
				getRiderSign(p_request,p_response);
			}else if(m_eventParam.equalsIgnoreCase("ccv")) {
				voidPayment(p_request,p_response);
			} else if(m_eventParam.equalsIgnoreCase("vvalid")) {			 
				validateVoucher(p_request,p_response);
			} else if(m_eventParam.equalsIgnoreCase("vup")) {			 
				voucherUpdate(p_request,p_response);
			} else if(m_eventParam.equalsIgnoreCase("vsign")) {			 
				getVoucherSign(p_request,p_response);
			} else if(m_eventParam.equalsIgnoreCase("X")) {			 
				getVoucherOrCC(p_request,p_response);
			}
			 
		} else {
			 
			sentErrorReport(p_request,p_response);
			 
		}
		
		
	}
	
	public void getVoucherOrCC(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		String content_length = p_request.getHeader("content-length"); 
	    
		try{
		
			BufferedReader reader1 = p_request.getReader();
		     
		    int a = 0, counter = 0;
		    byte[] bytearray = new byte[Integer.parseInt(content_length)];
		    while((a = reader1.read()) != -1)
		    {
		    	 bytearray[counter] = (byte)a;
		    	 counter++;
		    }
		     
		      
		      String trans_id =  p_request.getParameter("transid")==null?"":p_request.getParameter("transid");
		      String trans_id2 =  p_request.getParameter("transid2")==null?"":p_request.getParameter("transid2");
		      //System.out.println("Transid:"+ trans_id);
		      
		      if(UtilityDAO.isExistOrNot("select * from TDS_VOUCHER_DETAIL where VD_TXN_ID = '"+trans_id+"'"))
		    	  ProcessingDAO.updateVoucherSign(trans_id, bytearray);
		      else{
		    	  //ProcessingDAO.updateSignIntoWork(bytearray,trans_id);
		    	  ChargesDAO.updateSignIntoCC(bytearray,trans_id,trans_id2);
		      }
		      p_response.getWriter().write("Received");
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		} 
	}
	
	
	
	public void sentErrorReport(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		StringBuffer m_resultStrBuffer = new StringBuffer();
		//PrintWriter m_printWriter = p_response.getWriter();
		p_response.setContentType("text/xml");
		
		m_resultStrBuffer.append("<?xml version=\"1.0\"?>");
		m_resultStrBuffer.append("<SessionExp>");
		m_resultStrBuffer.append("<Status>ERROR</Status>");
		m_resultStrBuffer.append("<Description>Your Session Expired</Description>");
		m_resultStrBuffer.append("</SessionExp>");
		p_response.getWriter().write(m_resultStrBuffer.toString());
		
	}
	
	
	public void voucherUpdate(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		 
		StringBuffer m_resultStrBuffer = new StringBuffer();
		HttpSession m_session =  p_request.getSession();
		String tripID = p_request.getParameter("tripid");
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		VoucherBO voucherBO = null;
		int size = p_request.getContentLength();
		BufferedReader reader1 = p_request.getReader();
	    byte[] bytes = new byte[size]; 
	    int a = 0, counter = 0;
	    while((a = reader1.read()) != -1){
		    bytes[counter] = (byte)a;
		    counter++;
	    }
		if(TDSValidation.isDoubleNumber(p_request.getParameter("amt")) && TDSValidation.isDoubleNumber(p_request.getParameter("tip")) && UtilityDAO.isExistOrNot("select * from TDS_GOVT_VOUCHER where VC_VOUCHERNO = '"+p_request.getParameter("vno")+"' and VC_AMOUNT >= "+(Double.parseDouble(p_request.getParameter("amt"))+Double.parseDouble(p_request.getParameter("tip")))+" ") )
		{
			voucherBO = ProcessingDAO.updateVoucherDetail(p_request.getParameter("vno"), m_adminBO.getAssociateCode(), tripID,m_adminBO.getUid(),p_request.getParameter("amt"),p_request.getParameter("tip"),bytes,p_request.getRealPath("/images/nosign.gif"),p_request.getParameter("passengername"),Integer.parseInt(p_request.getParameter("txnId")));
			ProcessingDAO.updateVoucherSign(voucherBO.getVtrans(), bytes);
			if(tripID!=null && !tripID.equals("")){
				ServiceRequestDAO.updateOpenRequestHisStatusAlone(tripID, TDSConstants.paymentReceived + "");
				AuditDAO.insertJobLogs("Voucher Pmt Received", tripID,m_adminBO.getAssociateCode(),TDSConstants.paymentReceived,m_adminBO.getUid(), "0.00000","0.00000",m_adminBO.getMasterAssociateCode());
			}
			if(voucherBO.isV_status()) {
			 	m_resultStrBuffer.append("Approved"+";"+p_request.getParameter("vno")+";"+voucherBO.getVtrans()+";"+Double.parseDouble(voucherBO.getVamount())+";"+Double.parseDouble(voucherBO.getTip()));
			} else {  
				m_resultStrBuffer.append("Declined;Amount not Authorized");
			}
		} else{
			m_resultStrBuffer.append("Declined;Amount not Authorized");
		}
//		if(m_session.getAttribute("tempSession")!=null && m_session.getAttribute("tempSession").equals("True")){
//			m_session.removeAttribute("user");
//			m_session.invalidate();
//		}
		p_response.getWriter().write(m_resultStrBuffer.toString());
		
	}
	
	public void validateVoucher(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		StringBuffer m_resultStrBuffer = new StringBuffer();
		HttpSession m_session =  p_request.getSession(false);
		VoucherBO voucherBO = new VoucherBO();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		String tripID = p_request.getParameter("tripID");
		voucherBO = ProcessingDAO.getVaidityOfVoucher(p_request.getParameter("vno"), m_adminBO.getMasterAssociateCode());
		if(voucherBO!=null && voucherBO.isV_status()&& tripID !=null)
		{
			int txnId = ProcessingDAO.insertVoucherDetail(p_request.getParameter("vno"), m_adminBO.getAssociateCode(), tripID,m_adminBO.getUid(),"0.00","0.00",null,"","",m_adminBO.getMasterAssociateCode());
			m_resultStrBuffer.append("ResVer=1.1;STATUS=APPROVED;TXNID="+txnId+";APPROVALCODE="+txnId+";MESSAGE=abc;TD=abc");			
			m_resultStrBuffer.append("VNO="+p_request.getParameter("vno")+";");
			m_resultStrBuffer.append("RN="+voucherBO.getVname()+";");
			m_resultStrBuffer.append("AAMT="+voucherBO.getVamount()+";");
			m_resultStrBuffer.append("TD="+voucherBO.getVdesc()+";");
			m_resultStrBuffer.append("TA="+voucherBO.getNoTip()+";");
			m_resultStrBuffer.append("TxI="+txnId+";");
			m_resultStrBuffer.append("DC="+voucherBO.getDriverComments() + ";");
			//m_resultStrBuffer.append("OC="+voucherBO.getOperatorComments() + ";");
			
		} else {
			if(voucherBO!=null){
				m_resultStrBuffer.append(voucherBO.getVreason());
			}else{
				m_resultStrBuffer.append("ResVer=1.1;STATUS=DECLINED;TXNID=;APPROVALCODE=;MESSAGE=;TD=");
			}
		}
//		if(m_session.getAttribute("tempSession")!=null && m_session.getAttribute("tempSession").equals("True")){
//			m_session.removeAttribute("user");
//			m_session.invalidate();
//		}
		 p_response.getWriter().write(m_resultStrBuffer.toString());
	}

	public void chckUserCardInfo(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException, JSONException {
		
		PaymentProcessBean processBean = null;
		HttpSession m_session = p_request.getSession();		
		StringBuffer m_resultStrBuffer = new StringBuffer();
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		AdminRegistrationBO m_adminBO;
		m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");  
		PaymentProcessingAction pay = new PaymentProcessingAction();
		
		processBean = magneticStripAndAmtParsing(p_request,p_response);
		processBean.setB_driverid(m_adminBO.getUid());
		processBean.setAdd1(p_request.getParameter("add1")==null?"":p_request.getParameter("add1"));
		processBean.setAdd2(p_request.getParameter("add2")==null?"":p_request.getParameter("add2"));
		processBean.setCity(p_request.getParameter("city")==null?"":p_request.getParameter("city"));
		processBean.setState(p_request.getParameter("state")==null?"":p_request.getParameter("state"));
		processBean.setZip(p_request.getParameter("zip")==null?"":p_request.getParameter("zip"));
		processBean.setCvv(p_request.getParameter("cvv")==null?"":p_request.getParameter("cvv"));
		processBean.setB_cardHolderName(p_request.getParameter("name")==null?"":p_request.getParameter("name"));
		processBean.setMerchid(m_adminBO.getMerchid());
		processBean.setServiceid(m_adminBO.getServiceid());
		processBean.setB_trip_id(p_request.getParameter("tripid")==null?"":p_request.getParameter("tripid"));
		
		processBean = pay.initiatePayProcess(poolBO,processBean,"Mobile",m_adminBO.getMasterAssociateCode()); 
		JSONArray array=new JSONArray();
		JSONObject details=new JSONObject();
		//System.out.println("\ntwo\n");
		if(processBean.getB_approval_status().equalsIgnoreCase("1"))
		{
			PaymentProcessingAction.makePayment(processBean,p_request.getRealPath("/images/nosign.gif"),poolBO, m_adminBO);	
			details.put("ResVer", 1.1);
			details.put("STATUS", "APPROVED");
			details.put("TXNID",(processBean.getB_trans_id().equals("")?"0":processBean.getB_trans_id()));
			details.put("APPROVALCODE",(processBean.getB_approval().equals("")?"0":processBean.getB_approval()));
			details.put("AMOUNT",0.0);
			array.put(details);
		} else if(processBean.getB_approval_status().equalsIgnoreCase("2")) {
			ChargesDAO.insertPaymentIntoCC(processBean, p_request.getRealPath("/images/nosign.gif"), poolBO, m_adminBO,ICCConstant.PREAUTH,ICCConstant.GAC);
			details.put("ResVer", 1.1);
			details.put("STATUS", "DECLINED");
			details.put("TXNID", (processBean.getB_trans_id().equals("")?"0":processBean.getB_trans_id()));
			details.put("APPROVALCODE", (processBean.getB_approval().equals("")?"0":processBean.getB_approval()));
			details.put("AMOUNT",0.0);
			array.put(details);
		} else  {
			details.put("ResVer", 1.1);
			details.put("STATUS", "FAILURE");
			details.put("MESSAGE","Fail to Process Your Request^Reason:^Check Your Data^Check Your Network;XXXXX");
			details.put("AMOUNT",0.0);
			array.put(details);
		}
		//System.out.println("\nthree\n");
		p_response.getWriter().write(array.toString());
	}
	
	public void alterPayment(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
				 		 
		StringBuffer m_resultStrBuffer = new StringBuffer();
		PaymentProcessBean processBean = new PaymentProcessBean();
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) p_request.getSession().getAttribute("user");

		byte[] bytes = p_request.getAttribute("rider_sign")==null?new byte[0]:(byte[])p_request.getAttribute("rider_sign");	
		processBean.setB_amount( p_request.getParameter("amt")==null?"0":p_request.getParameter("amt"));
		processBean.setB_trans_id(p_request.getParameter("tranid")==null?"":p_request.getParameter("tranid"));
		processBean.setB_tip_amt(p_request.getParameter("tip")==null?"0":p_request.getParameter("tip"));
		processBean.setMerchid(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getMerchid());
		processBean.setServiceid(((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getServiceid());
		String transId=processBean.getB_trans_id();
		processBean = PaymentProcessingAction.capturePayment(poolBO,processBean,m_adminBO.getMasterAssociateCode());
		if(processBean.getB_approval_status().equalsIgnoreCase("1"))
		{
			ChargesDAO.alterTransct(processBean.getB_tip_amt(), processBean.getB_amount(), transId,
					processBean.getB_trans_id2(), processBean.getB_cap_trans(), "", bytes,
					((AdminRegistrationBO) p_request.getSession()
							.getAttribute("user")).getUid());
			
			m_resultStrBuffer.append("ResVer=1.1;STATUS=APPROVED;TXNID="+(processBean.getB_cap_trans().equals("")?"0":processBean.getB_cap_trans())+";APPROVALCODE="+(processBean.getB_approval().equals("")?"0":processBean.getB_approval()));
		} else if(processBean.getB_approval_status().equalsIgnoreCase("2"))
		{
			m_resultStrBuffer.append("ResVer=1.1;STATUS=DECLINED;TXNID="+(processBean.getB_cap_trans().equals("")?"0":processBean.getB_cap_trans())+";APPROVALCODE="+(processBean.getB_approval().equals("")?"0":processBean.getB_approval()));
		} else {
			m_resultStrBuffer.append("ResVer=1.1;STATUS=FAILURE;MESSAGE=XXXXX;XXXXX");
		}
	 
		p_response.getWriter().write(m_resultStrBuffer.toString());
		
	}
	
	public void voidPayment(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		StringBuffer m_resultStrBuffer = new StringBuffer();
		PaymentProcessBean processBean = new PaymentProcessBean();
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) p_request.getSession().getAttribute("user");

		processBean.setB_trans_id(p_request.getParameter("transid")==null?"":p_request.getParameter("transid"));
		
		processBean = ChargesDAO.chckReturnOrUndo(processBean);		
		processBean = PaymentProcessingAction.voidPayment(poolBO,processBean,m_adminBO.getMasterAssociateCode());
		
		if(processBean.getB_approval_status().equalsIgnoreCase("1"))
		{
			ChargesDAO.voidTransAction(processBean.getB_trans_id(), processBean.getB_trans_id2(), p_request.getParameter("desc"),((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getUid());
			
			m_resultStrBuffer.append("ResVer=1.1;STATUS=APPROVED;TXNID="+(processBean.getB_trans_id().equals("")?"0":processBean.getB_trans_id())+";APPROVALCODE="+(processBean.getB_approval().equals("")?"0":processBean.getB_approval()));
		} else if(processBean.getB_approval_status().equalsIgnoreCase("2"))
		{
			m_resultStrBuffer.append("ResVer=1.1;STATUS=DECLINED;"+(processBean.getB_trans_id().equals("")?"0":processBean.getB_trans_id())+";APPROVALCODE="+(processBean.getB_approval().equals("")?"0":processBean.getB_approval()));
		} else {
			m_resultStrBuffer.append("ResVer=1.1;STATUS=FAILURE;MESSAGE=XXXXX;XXXXX");
		}
	 
		p_response.getWriter().write(m_resultStrBuffer.toString());
		 
	}

	
	public PaymentProcessBean magneticStripAndAmtParsing(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
				
		PaymentProcessBean processBean = null;
				
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO;
		m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		
	 	
		processBean = getmagneticStripInfo(p_request);
		processBean.setB_asscode(m_adminBO.getAssociateCode());
		processBean.setB_service_key(m_adminBO.getServiceKey());
		processBean.setB_desc(p_request.getParameter("bDesc")==null?"":p_request.getParameter("bDesc"));
		processBean.setB_amount(p_request.getParameter("amt")==null?"0":p_request.getParameter("amt").trim());
		processBean.setB_tip_amt(p_request.getParameter("tip")==null?"0":p_request.getParameter("tip"));
		
	
		
		
		return processBean;
	}
	
	public PaymentProcessBean getmagneticStripInfo(HttpServletRequest p_request) {
		 
		 
		PaymentProcessBean processBean = new PaymentProcessBean();
		String card_no,card_issuer,card_type="",exp_date,card_full_no;
		
		 		
			if(!"".equalsIgnoreCase(p_request.getParameter("track1")))
			{
				//System.out.println("Getting Track 1");
				
				card_issuer = p_request.getParameter("track1").substring(1, 2);
				
				card_type = ProcessingDAO.getCardType(p_request.getParameter("track1").substring(1, 9));
				
				card_no =  p_request.getParameter("track1").split("\\^")[0].substring(p_request.getParameter("track1").split("\\^")[0].length()-4,p_request.getParameter("track1").split("\\^")[0].length());
			 
				exp_date  = p_request.getParameter("track1").split("\\^")[2].substring(2,4)+p_request.getParameter("track1").split("\\^")[2].substring(4,6);
				
				card_full_no = p_request.getParameter("track1").split("\\^")[0].substring(1,p_request.getParameter("track1").split("\\^")[0].length());
				
				processBean.setB_paymentType("A");
				
			} else if(p_request.getParameter("ccno")!=null && !"".equalsIgnoreCase(p_request.getParameter("ccno")) && p_request.getParameter("ccno").length()>12){
				
				//cat.info("Getting Manual");
				
				card_issuer = p_request.getParameter("ccno").substring(0, 1);
				
				card_type = ProcessingDAO.getCardType(p_request.getParameter("ccno").substring(0, 8));
				
				card_no =  p_request.getParameter("ccno").substring(p_request.getParameter("ccno").length()-4,p_request.getParameter("ccno").length());
				
				exp_date = p_request.getParameter("exp")==null?"":p_request.getParameter("exp");
				
				card_full_no = p_request.getParameter("ccno");
				processBean.setB_paymentType("M");
			
			} else if(!p_request.getParameter("track2").equals("")){
				
				cat.info("Getting Track 2");
				
				card_issuer = p_request.getParameter("track2").substring(0, 1);
				
				card_type = ProcessingDAO.getCardType(p_request.getParameter("track2").substring(0, 8));
				
				card_no =  p_request.getParameter("track2").substring(p_request.getParameter("track2").split("=")[1].length()-4,p_request.getParameter("track2").split("=")[1].length());
				
				exp_date = p_request.getParameter("track2").split("=")[1].substring(2,4)+p_request.getParameter("track2").split("=")[1].substring(4,6);
				
				card_full_no = p_request.getParameter("track2").split("=")[0];
				
				processBean.setB_paymentType("A");
			} else {
				card_full_no = "";
				card_issuer = "";
				card_no = "";
				exp_date="";
			}
		 
		processBean.setB_card_full_no(card_full_no);
		processBean.setTrack1(p_request.getParameter("track1"));	
		processBean.setTrack2(p_request.getParameter("track2"));
		processBean.setB_card_issuer(card_issuer);
		processBean.setB_card_no(card_no);
		processBean.setB_card_type("C");
		//processBean.setB_cardExpiryDate(p_request.getParameter("expyear")+p_request.getParameter("expmonth"));
		processBean.setB_cardExpiryDate(exp_date);
		processBean.setB_trip_id(p_request.getParameter("trip")==null?"":p_request.getParameter("trip"));
		/*
		System.out.println(processBean.getB_card_full_no());
		System.out.println(processBean.getTrack1());	
		System.out.println(processBean.getTrack2());
		System.out.println(processBean.getB_card_issuer());
		System.out.println(processBean.getB_card_no());
		System.out.println(processBean.getB_card_type());
		System.out.println(processBean.getB_cardExpiryDate());*/
	 
		return processBean;
	}
	
	public void getVoucherSign(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
	
		String content_length = p_request.getHeader("content-length"); 
	    
		try{
		
			BufferedReader reader1 = p_request.getReader();
		     
		    int a = 0, counter = 0;
		    byte[] bytearray = new byte[Integer.parseInt(content_length)];
		    while((a = reader1.read()) != -1)
		    {
		    	 bytearray[counter] = (byte)a;
		    	 counter++;
		    }
		     
		      
		      String trans_id =  p_request.getParameter("transid")==null?"":p_request.getParameter("transid");
		      //System.out.println("Transid:"+ trans_id);
		      ProcessingDAO.updateVoucherSign(trans_id, bytearray);
				 
		      p_response.getWriter().write("Received");
		      
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		} 
	}
	public void getRiderSign(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		  
	    String content_length = p_request.getHeader("content-length"); 
	    
		try{
		
		BufferedReader reader1 = p_request.getReader();
	     
	    int a = 0, counter = 0;
	    byte[] bytearray = new byte[Integer.parseInt(content_length)];
	    while((a = reader1.read()) != -1)
	    {
	    	 bytearray[counter] = (byte)a;
	    	 counter++;
	    }
	    
	    
	     /* File f=new File("/root/Desktop/Sample_"+System.currentTimeMillis()+".gif");
	      FileOutputStream fop=new FileOutputStream(f);
	      fop.write(bytearray);
	      fop.close();*/
	        
	    
	 	/*ByteArrayInputStream bis = new ByteArrayInputStream(bytearray);
        Iterator readers = ImageIO.getImageReadersByFormatName("gif");
        
        ImageReader reader = (ImageReader) readers.next();
        Object source = bis; // File or InputStream, it seems file is OK
 
        ImageInputStream iis = ImageIO.createImageInputStream(source);
        //Returns an ImageInputStream that will take its input from the given Object
 
        reader.setInput(iis, true);
        ImageReadParam param = reader.getDefaultReadParam();
 
        Image image = reader.read(0, param);
        //got an image file
 
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_RGB);
        //bufferedImage is the RenderedImage to be written
        Graphics2D g2 = bufferedImage.createGraphics();
        g2.drawImage(image, null, null);
        File imageFile = new File("/root/Desktop/snap.gif");
        ImageIO.write(bufferedImage, "gif", imageFile);*/

	 	
	 	 /*BufferedImage imag=ImageIO.read(new ByteArrayInputStream(bytes));
	 	 ImageIO.write(imag, "gif", new File("/root/Desktop","snap.gif"));*/
	 	
		String trans_id =  p_request.getParameter("transid")==null?"":p_request.getParameter("transid");
		String trans_id2 =  p_request.getParameter("transid2")==null?"":p_request.getParameter("transid2");
		
		//ProcessingDAO.updateSignIntoWork(bytearray,trans_id);
		ChargesDAO.updateSignIntoCC(bytearray,trans_id,trans_id2);
		 
		p_response.getWriter().write("Received");
		
		}catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		} 
	}
	
}
