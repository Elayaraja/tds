package com.tds.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.cmp.bean.DriverDeactivation;
import com.tds.cmp.bean.MerchantProfile;
import com.tds.controller.TDSController;
import com.tds.dao.AdministrationDAO;
import com.tds.dao.AuditDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.util.ConfigurationValues;
import com.tds.util.ServiceTxnProcess;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

public class AdministrationAction  extends HttpServlet 
{
	
private final Category cat = TDSController.cat;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ ServiceRequestAction.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	
	@Override
	public void doGet(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException 
	{
		String m_eventParam = "";
		
		if(p_request.getParameter(TDSConstants.eventParam) != null) {
			m_eventParam = p_request.getParameter(TDSConstants.eventParam);
		}
		
		cat.info("Event Param "+m_eventParam);
		HttpSession m_session = p_request.getSession();
		
		 
					 
		if(m_session.getAttribute("user") != null) {
			
			if(m_eventParam.equalsIgnoreCase("driverTempDeActive")) {			 
				driverTempDeActive(p_request,p_response);
			}else if(m_eventParam.equalsIgnoreCase("getDeActiveSummary")) {			 
				getDeActiveSummary(p_request,p_response);
			} else if(m_eventParam.equalsIgnoreCase("activeDriverDeact"))
			{
				activeDriverDeact(p_request,p_response);
			}	
			//else if(m_eventParam.equalsIgnoreCase("dashBoard"))
			//{
				//dashBoard(p_request,p_response);
			//}
			/*}	else if(m_eventParam.equalsIgnoreCase("reportLinks"))
			{
				reportLinks(p_request,p_response);
			}else if(m_eventParam.equalsIgnoreCase("expandOR"))
			{
				expandOR(p_request,p_response);
			}	
			else if(m_eventParam.equalsIgnoreCase("expandBR"))
			{
				expandBR(p_request,p_response);
			}*/	
			/*else if(m_eventParam.equalsIgnoreCase("shortPath"))
			{
				shortPath(p_request,p_response);
			}	*/
			else if(m_eventParam.equalsIgnoreCase("showDriver"))
			{
				showDriverStart(p_request,p_response);
			}  else if(m_eventParam.equalsIgnoreCase("getMerchant"))
			{
				getMerchant(p_request,p_response);
			}  
		} else {
			
			//sentErrorReport(p_request,p_response);
		}
	}
	
	public void getMerchant(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		String Button = p_request.getParameter("Button")==null?"":p_request.getParameter("Button");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		MerchantProfile mProfile = assignMerchantProfile(p_request);
		
		if(!Button.equals(""))
		{
			StringBuffer error = validateMerchantProfile(p_request);
			
			if(!(error.length() > 0))
			{
				ConfigurationValues _CV = null;
				ServiceTxnProcess process = new ServiceTxnProcess();
							
				_CV = process.gettingServiceIdAndSavingMerchantProfile(poolBO, mProfile,((AdminRegistrationBO)p_request.getSession().getAttribute("user")).getServiceid());  
				System.out.println("Merchant:" + _CV._MerchantProfileId);
				
				if(_CV._MerchantProfileId!=null && !_CV._MerchantProfileId.equals(""))
				{
					AdministrationDAO.updateMerchantProfile(_CV._MerchantProfileId, mProfile.getService_id(), mProfile.getAssocode());
					p_request.setAttribute("screen", "/Success.jsp");
					p_request.setAttribute("page", "Merchant Initialized Successfully");
				} else {
					p_request.setAttribute("error", new StringBuffer().append("Merchant Profile is not Initialized, Pls Contact Your Admin <BR>"));
					p_request.setAttribute("screen", "/Company/MerchantProfile.jsp");
				}
				
			} else {
				p_request.setAttribute("error", error);
				p_request.setAttribute("screen", "/Company/MerchantProfile.jsp");
			}
		} else {
			p_request.setAttribute("screen", "/Company/MerchantProfile.jsp");
		}
		 
		p_request.setAttribute("mProfile", mProfile);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		//RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/Company/MerchantProfile.jsp");
    	requestDispatcher.forward(p_request, p_response);
	}
	
	public StringBuffer validateMerchantProfile(HttpServletRequest req)
	{
		StringBuffer error = new StringBuffer();
		
		if(req.getParameter("merch_name").equals(""))
		{
			error.append("Merchant Name is Required<Br>");
		}
		if(req.getParameter("street").equals(""))
		{
			error.append("Street is Required<Br>");
		}
		if(req.getParameter("city").equals(""))
		{
			error.append("City is Required<Br>");
		}
		if(req.getParameter("state").equals(""))
		{
			error.append("State is Required<Br>");
		}
		if(req.getParameter("zipcode").equals(""))
		{
			error.append("zip code is Required<Br>");
		}
		if(req.getParameter("phone_no").equals(""))
		{
			error.append("Phone No is Required<Br>");
		}
		if(req.getParameter("client_no").equals(""))
		{
			error.append(" Client No is Required<Br>");
		}
		if(req.getParameter("sic").equals(""))
		{
			error.append("SIC/MCC is Required<Br>");
		}
		if(req.getParameter("merch_id").equals(""))
		{
			error.append("Merchant ID is Required<Br>");
		}
		if( req.getParameter("terminal_id").equals(""))
		{
			error.append("Terminal ID is Required<Br>");
		}
		if( req.getParameter("profile_id").equals(""))
		{
			error.append("Profile ID is Required<Br>");
		}
		 
		return error;
		
	}
	
	public MerchantProfile assignMerchantProfile(HttpServletRequest req)
	{
		MerchantProfile mProfile = new MerchantProfile();
		
		mProfile.setCity(req.getParameter("city")==null?"":req.getParameter("city"));
		mProfile.setClient_no(req.getParameter("client_no")==null?"":req.getParameter("client_no"));
		mProfile.setMerch_id(req.getParameter("merch_id")==null?"":req.getParameter("merch_id"));
		mProfile.setMerch_name(req.getParameter("merch_name")==null?"":req.getParameter("merch_name"));
		mProfile.setPhone_no(req.getParameter("phone_no")==null?"":req.getParameter("phone_no"));
		mProfile.setService_id(req.getParameter("service_id")==null?"":req.getParameter("service_id"));
		mProfile.setSession_token(req.getParameter("session_token")==null?"":req.getParameter("session_token"));
		mProfile.setSic(req.getParameter("sic")==null?"":req.getParameter("sic"));
		mProfile.setState(req.getParameter("state")==null?"":req.getParameter("state"));
		mProfile.setStreet(req.getParameter("street")==null?"":req.getParameter("street"));
		mProfile.setTerminal_id(req.getParameter("terminal_id")==null?"":req.getParameter("terminal_id"));
		mProfile.setZipcode(req.getParameter("zipcode")==null?"":req.getParameter("zipcode"));
		mProfile.setProfile_id(req.getParameter("profile_id")==null?"":req.getParameter("profile_id"));
		mProfile.setAssocode(req.getParameter("assocode")==null?"":req.getParameter("assocode"));
		return mProfile;
	}
	
	public void showDriverStart(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
			
		HttpSession session = p_request.getSession();
		p_request.setAttribute("full", "1");
		p_request.setAttribute("mapDetail", UtilityDAO.getDriverLocation(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(),p_request.getParameter("driver_id") == null?"":p_request.getParameter("driver_id")));
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/DriverMap/ShowDriverLoaction.jsp");
    	requestDispatcher.forward(p_request, p_response);
    		
	}
/*	public void shortPath(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		RequestDispatcher requestDispatcher = null;
		HttpSession session = p_request.getSession();
		String driver = "";
		ArrayList al_driver  = null;
		ArrayList al_trip = null,al_short= new ArrayList();
		OpenRequestBO requestBO = null;
		
		String Button = p_request.getParameter("Button");

		if(Button == null)
		{
			al_trip = RequestDAO.getAllOpenRequestForAutoAlloc(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
			
			dotheStopProcess(al_trip,((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
						
			al_driver = UtilityDAO.getDriverLocationList(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
			for(int i=0;i<al_trip.size();i++)
			{
				requestBO = (OpenRequestBO)al_trip.get(i);
				requestBO = ShortestPathFiding.getShortestDriver(al_driver,requestBO);
			 	al_short.add(requestBO);
			}
			
			al_trip.clear();
			al_trip = al_short;
			requestDispatcher = getServletContext().getRequestDispatcher( "/Company/ShortestPath.jsp");
			 
		} else {
			
			for(int i=0;i<Integer.parseInt(p_request.getParameter("size")==null?"0":p_request.getParameter("size"));i++)
			{
				if(p_request.getParameter("tripid"+i)!=null)
				{
					requestBO = new OpenRequestBO();
					requestBO.setTripid(p_request.getParameter("tripid"+i));
					requestBO = RequestDAO.getOpenRequestBean(requestBO, "","2");
					if(requestBO.getIsBeandata() > 0) {
						requestBO.setDriverid(p_request.getParameter("driverid"+i));
						int m_status = ServiceRequestDAO.insertAcceptedRequest(requestBO);
						
						if(m_status > 0){
							//System.out.println("Trip Accepted Successfully by" + requestBO.getDriverid());
							ServiceRequestDAO.deleteDriverFromQueue(requestBO.getDriverid(),requestBO.getQueueno());
						} else {
							//System.out.println("Trip Not Accepted by driver"+requestBO.getDriverid());
						}
					} else {
						//System.out.println("Trip Not Accepted No data to fetch:"+requestBO.getTripid());
					}
		 		} else {
		 			AdministrationDAO.stopQueueProcessByOpr(p_request.getParameter("tripidh"+i),"");
		 		}
			}
			
			p_request.setAttribute("page", "Allocated");
			requestDispatcher = getServletContext().getRequestDispatcher( "/Success.jsp");
			 
		}		
		p_request.setAttribute("al_trip", al_trip);
		requestDispatcher.forward(p_request, p_response);
		
	}
*/	
	public void dotheStopProcess(ArrayList al_trip,HttpServletRequest request, HttpServletResponse response)
	{
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");

		OpenRequestBO requestBO = null;
		for(int i=0;i<al_trip.size();i++)
		{
			requestBO = (OpenRequestBO)al_trip.get(i);
			AdministrationDAO.stopQueueProcessByOpr(requestBO.getTripid(),"S", adminBO.getAssociateCode());
			
		}
		AdministrationDAO.deleteQueue(adminBO.getAssociateCode());
	}
	
	/*public void expandBR(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession session = p_request.getSession();
		
		p_request.setAttribute("al_list", AdministrationDAO.getOpnReqDash(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(),1));
		p_request.setAttribute("al_q", UtilityDAO.getQueueForAssoccode(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()));
		p_request.setAttribute("al_d", UtilityDAO.getDriverList(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(), 2));
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/Company/ExpandBroadReq.jsp");
    	requestDispatcher.forward(p_request, p_response);
	}*/
	/*public void expandOR(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		HttpSession session = p_request.getSession();
		p_request.setAttribute("al_list", AdministrationDAO.getOpnReqDash(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(),0));
		p_request.setAttribute("al_q", UtilityDAO.getQueueForAssoccode(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()));
		p_request.setAttribute("al_d", UtilityDAO.getDriverList(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(), 2));
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/Company/ExpandOpenReq.jsp");
    	requestDispatcher.forward(p_request, p_response);
	}*/
	
	/*public void reportLinks(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		p_request.setAttribute("screen", "/Company/ReportLink.jsp");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainJSP);
    	requestDispatcher.forward(p_request, p_response);
	}*/
	
//	public void dashBoard(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
//			
//		//p_request.setAttribute("screen", "/Company/DashBoardView.jsp");
//		
//		 AdminRegistrationBO adminBO = (AdminRegistrationBO)p_request.getSession().getAttribute("user");
//		
//		if(p_request.getParameter("event2")==null)
//		{
//		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/Company/DashBoardView.jsp");
//    	requestDispatcher.forward(p_request, p_response);
//		} else if(p_request.getParameter("event2").equals("reportLinks")){
//			p_request.setAttribute("screen", "/Company/ReportLink.jsp");
//			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainJSP);
//	    	requestDispatcher.forward(p_request, p_response);
//		} else if(p_request.getParameter("event2").equalsIgnoreCase("expandOR"))
//		{
//			HttpSession session = p_request.getSession();
//			p_request.setAttribute("al_list", AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(),0,adminBO.getTimeZone()));
//			p_request.setAttribute("al_q", UtilityDAO.getQueueForAssoccode(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()));
//			p_request.setAttribute("al_d", UtilityDAO.getDriverList(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(), 2));
//			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/Company/ExpandOpenReq.jsp");
//	    	requestDispatcher.forward(p_request, p_response);
//		} else if(p_request.getParameter("event2").equals("expandBR"))
//		{
//			HttpSession session = p_request.getSession();
//			
//			p_request.setAttribute("al_list", AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(),1,adminBO.getTimeZone()));
//			p_request.setAttribute("al_q", UtilityDAO.getQueueForAssoccode(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode()));
//			p_request.setAttribute("al_d", UtilityDAO.getDriverList(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(), 2));
//			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/Company/ExpandBroadReq.jsp");
//	    	requestDispatcher.forward(p_request, p_response);
//		} else if(p_request.getParameter("event2").equals("shortPath")){
//		
//			RequestDispatcher requestDispatcher = null;
//			HttpSession session = p_request.getSession();
//			String driver = "";
//			ArrayList al_driver  = null;
//			ArrayList al_trip = null,al_short= new ArrayList();
//			OpenRequestBO requestBO = null;
//			
//			String Button = p_request.getParameter("Button");
//
//			if(Button == null)
//			{
//				al_trip = RequestDAO.getAllOpenRequestForAutoAlloc(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
//				
//				dotheStopProcess(al_trip,p_request, p_response);
//							
//				al_driver = UtilityDAO.getDriverLocationList(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
//				for(int i=0;i<al_trip.size();i++)
//				{
//					requestBO = (OpenRequestBO)al_trip.get(i);
//					requestBO = ShortestPathFiding.getShortestDriver(al_driver,requestBO);
//				 	al_short.add(requestBO);
//				}
//				
//				al_trip.clear();
//				al_trip = al_short;
//				requestDispatcher = getServletContext().getRequestDispatcher( "/Company/ShortestPath.jsp");
//				 
//			} else {
//				
//				for(int i=0;i<Integer.parseInt(p_request.getParameter("size")==null?"0":p_request.getParameter("size"));i++)
//				{
//					//AdminRegistrationBO adminBO = (AdminRegistrationBO)p_request.getSession().getAttribute("user");
//
//					if(p_request.getParameter("tripid"+i)!=null)
//					{
//						requestBO = new OpenRequestBO();
//						requestBO.setTripid(p_request.getParameter("tripid"+i));
//						requestBO = RequestDAO.getOpenRequestBean(requestBO, "","2", adminBO);
//						if(requestBO.getIsBeandata() > 0) {
//							requestBO.setDriverid(p_request.getParameter("driverid"+i));
//							int m_status = ServiceRequestDAO.insertAcceptedRequest(requestBO);
//							
//							if(m_status > 0){
//								//System.out.println("Trip Accepted Successfully by" + requestBO.getDriverid());
//								ServiceRequestDAO.deleteDriverFromQueue(requestBO.getDriverid(), adminBO.getAssociateCode());
//							} else {
//								//System.out.println("Trip Not Accepted by driver"+requestBO.getDriverid());
//							}
//						} else {
//							//System.out.println("Trip Not Accepted No data to fetch:"+requestBO.getTripid());
//						}
//			 		} else {
//			 			AdministrationDAO.stopQueueProcessByOpr(p_request.getParameter("tripidh"+i),"", adminBO.getAssociateCode());
//			 		}
//				}
//				
//				p_request.setAttribute("page", "Allocated");
//				requestDispatcher = getServletContext().getRequestDispatcher( "/Success.jsp");
//				 
//			}		
//			p_request.setAttribute("al_trip", al_trip);
//			requestDispatcher.forward(p_request, p_response);
//
//		}
//	}
//	
	 
//	public void sentErrorReport(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
//		
//		p_request.setAttribute("screen", "/failuer.jsp");
//     	p_request.setAttribute("page", "Your Session Expired");
//		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainJSP);
//		requestDispatcher.forward(p_request, p_response);
//
//		
//	}
	public void driverTempDeActive(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		System.out.println("Driver Deactivation");
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		DriverDeactivation deactivation = null;
		int key;
		boolean curStatus=false;
		
		StringBuffer error = null;		
		String Button = p_request.getParameter("Button");
		
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		p_request.setAttribute("deactivation", deactivation);
		p_request.setAttribute("al_driver", UtilityDAO.getDriverList(m_adminBO.getAssociateCode(),0));
		
		
		if(Button!=null) 
		{
			deactivation = assignDriverDeactivation(p_request,p_response,m_adminBO.getAssociateCode());
			deactivation.setAssoccode(m_adminBO.getAssociateCode());
			deactivation.setUser(m_adminBO.getUid());
			error = validateDriverDeActiveError(p_request, p_response);
			if(!(error.length()>0))
			{
				//if driver currently login redirect to logout screen
				//need to change(is't correct )
				//curStatus = AdministrationDAO.getCurrentDriverLoginDetails(deactivation.getAssoccode(), deactivation.getDriver_id(), deactivation.getFrom_date(), deactivation.getTo_date(),deactivation.getFrom_time(),deactivation.getTo_time());
				//if(curStatus) { 
				//	p_response.sendRedirect("control?action=registration&event=driverlogout&module=operationView"); 
				//}
				boolean status = AdministrationDAO.insertDriverDeactivate(deactivation, m_adminBO.getTimeZone());
				key = AuditDAO.insertauditRequest(deactivation.getAssoccode(), deactivation.getDriver_id(), deactivation.getDesc(), deactivation.getFrom_date(), deactivation.getTo_date(), "100",deactivation.getFrom_time(),deactivation.getTo_time());
				//p_request.setAttribute("screen", "/Success.jsp");
				p_response.sendRedirect("control?action=admin&event=driverTempDeActive&module=operationView&error=Deactivated Driver. Your Audit trail no is " + key);
	        	//p_request.setAttribute("page", "Deactivated Driver. Your Audit trail no is " + key);
			} else {
				p_request.setAttribute("error", error.toString());
				p_request.setAttribute("screen", "/Company/DriverDeactivation.jsp");
				requestDispatcher.forward(p_request, p_response);
			}
			
		} else {
			p_request.setAttribute("screen", "/Company/DriverDeactivation.jsp");
			requestDispatcher.forward(p_request, p_response);
		}
		
		//System.out.println(getServletConfig().getServletContext().getAttribute("obj"));
		 
    			
	}
	
	public DriverDeactivation assignDriverDeactivation (HttpServletRequest p_request, HttpServletResponse p_response,String assoccode) throws ServletException, IOException {
		DriverDeactivation deactivation = new DriverDeactivation();
		deactivation.setDesc(p_request.getParameter("desc"));
		deactivation.setDriver_id(p_request.getParameter("dd_driver_id"));
		deactivation.setFrom_date(p_request.getParameter("from_date"));
		deactivation.setFrom_time(p_request.getParameter("from_time"));
		if(p_request.getParameter("inDefinite")==null){
			deactivation.setTo_date(p_request.getParameter("to_date"));
			deactivation.setTo_time(p_request.getParameter("to_time"));
		}else{
			deactivation.setTo_date("12/31/2035");
			deactivation.setTo_time("0000");
		}
		deactivation.setAssoccode(assoccode);
		return deactivation;
	}
	public StringBuffer validateDriverDeActiveError(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession m_session = p_request.getSession(); 
		Calendar result = new  GregorianCalendar();  
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO) m_session.getAttribute("user");
		StringBuffer error = new StringBuffer(); 
		String curYear =  (result.getTime().getYear()+1900+""); 
		String curMonth = (result.getTime().getMonth()+1)<=9?("0"+(result.getTime().getMonth()+1)):(result.getTime().getMonth()+1)+"";
		String curDate =  (result.getTime().getDate())<=9?("0"+(result.getTime().getDate())):(result.getTime().getDate())+"";	 	 
		String curDMY = curYear+""+curMonth+""+curDate+"";

		if("".equals(p_request.getParameter("dd_driver_id")))
			error.append("Invalid Driver Id <BR>");
		if("".equals(p_request.getParameter("from_date"))) {
			error.append("Invalid From Date <BR>");
		}
		if(p_request.getParameter("inDefinite")==null){
		if("".equals(p_request.getParameter("to_date")))
			error.append("Invalid To Date <BR>");
		}
		if(!UtilityDAO.Assocodedriveridmatch(p_request.getParameter("dd_driver_id"),m_adminBO.getAssociateCode()))
			error.append("Driver id Not belong to Your Company<BR>");  
		if(!(p_request.getParameter("from_date").equals("")) && (p_request.getParameter("from_date")!=null)) {
			String fromDMY = p_request.getParameter("from_date").substring(6,10)+""+p_request.getParameter("from_date").substring(0,2)+""+p_request.getParameter("from_date").substring(3,5);
			if(Integer.parseInt(fromDMY)<Integer.parseInt(curDMY)) {
				error.append("Previous Date Not Allowed<BR>");	
			}
		}


		if(!TDSValidation.isOnlyNumbers(p_request.getParameter("from_time")) || (p_request.getParameter("from_time").length() != 4 )){

			error.append("Pls Provide FourDigit Time Format In FromTime<br>");
		}
		if(TDSValidation.isOnlyNumbers(p_request.getParameter("from_time")) && p_request.getParameter("from_time").length() == 4 ){
			if(!TDSValidation.get24timeFormat(p_request.getParameter("from_time")))	
				error.append("You entered invalid Time Format<br>");
		}
		if(p_request.getParameter("inDefinite")==null){
		if(!TDSValidation.isOnlyNumbers(p_request.getParameter("to_time")) || (p_request.getParameter("to_time").length() != 4 )){

			error.append("Pls Provide FourDigit Time Format In ToTime<br>");
		}
		

		if(TDSValidation.isOnlyNumbers(p_request.getParameter("to_time")) && p_request.getParameter("to_time").length() == 4 ){
			if(!TDSValidation.get24timeFormat(p_request.getParameter("to_time")))	
				error.append("You entered invalid Time Format<br>");
		}
		}
		return error;

	}

	public void getDeActiveSummary(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		HttpSession session = p_request.getSession(); 
		DriverDeactivation deactivation =new DriverDeactivation();
		AdminRegistrationBO adminBO = (AdminRegistrationBO) session.getAttribute("user");
		if(p_request.getParameter("search")==null && p_request.getParameter("activate")==null){
			p_request.setAttribute("screen", "/Company/DeActivationSummary.jsp");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
	    	requestDispatcher.forward(p_request, p_response);
		}else if(p_request.getParameter("activate")!=null){
		 	boolean status = AdministrationDAO.deleteDriverDeactivation(p_request.getParameter("driverid"),adminBO);
			if(status)
			{
				deactivation.setAssoccode(adminBO.getAssociateCode());
				deactivation.setDriver_id(p_request.getParameter("driverId")==null?"":p_request.getParameter("driverId"));
				deactivation.setFrom_date(p_request.getParameter("fromDate")==null?"":p_request.getParameter("fromDate"));
				deactivation.setTo_date(p_request.getParameter("toDate")==null?"":p_request.getParameter("toDate"));

				ArrayList al_list = null;
				al_list =  AdministrationDAO.getDriverDeactivation(adminBO,deactivation);
			
				p_request.setAttribute("al_list", al_list);
				p_request.setAttribute("page", "Successfully Reactivated driver");	
				p_request.setAttribute("screen", "/Company/DeActivationSummary.jsp");
			} else {
				p_request.setAttribute("page", "Delete");	
				p_request.setAttribute("screen", "/failure.jsp");
			}
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
	    	requestDispatcher.forward(p_request, p_response);
		}else {
			deactivation.setAssoccode(adminBO.getAssociateCode());
			deactivation.setDriver_id(p_request.getParameter("driverId")==null?"":p_request.getParameter("driverId"));
			deactivation.setFrom_date(p_request.getParameter("fromDate")==null?"":p_request.getParameter("fromDate"));
			deactivation.setTo_date(p_request.getParameter("toDate")==null?"":p_request.getParameter("toDate"));
			ArrayList<DriverDeactivation> al_list = AdministrationDAO.getDriverDeactivation(adminBO,deactivation);
			p_request.setAttribute("al_list", al_list);
			p_request.setAttribute("screen", "/Company/DeActivationSummary.jsp");
			RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
	    	requestDispatcher.forward(p_request, p_response);
		}
		
	}
	public void activeDriverDeact(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		AdminRegistrationBO adminBO = (AdminRegistrationBO) p_request.getSession().getAttribute("user");
		
	 	boolean status = AdministrationDAO.deleteDriverDeactivation(p_request.getParameter("driverid"),adminBO);
		if(status)
		{
			DriverDeactivation deactivation =new DriverDeactivation();
			deactivation.setAssoccode(adminBO.getAssociateCode());
			deactivation.setDriver_id(p_request.getParameter("driverId")==null?"":p_request.getParameter("driverId"));
			deactivation.setFrom_date(p_request.getParameter("fromDate")==null?"":p_request.getParameter("fromDate"));
			deactivation.setTo_date(p_request.getParameter("toDate")==null?"":p_request.getParameter("toDate"));

			ArrayList al_list = null;
			al_list =  AdministrationDAO.getDriverDeactivation(adminBO,deactivation);
		
			p_request.setAttribute("al_list", al_list);
			p_request.setAttribute("page", "Successfully Reactivated driver");	
			p_request.setAttribute("screen", "/Company/DeActivationSummary.jsp");
		} else {
			p_request.setAttribute("page", "Delete");	
			p_request.setAttribute("screen", "/failure.jsp");
		}
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
    	requestDispatcher.forward(p_request, p_response);
	}
	
	 
}
