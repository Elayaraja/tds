package com.tds.action;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tds.cmp.bean.FileProperty;
import com.tds.dao.UtilityDAO;
import com.common.util.TDSConstants;

public class FileUpDown extends HttpServlet{

	public void doGet(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	public void doPost(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		doProcess(p_request, p_response);
	}
	
	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException 
	{
		String m_eventParam ="";
		if(p_request.getParameter(TDSConstants.eventParam) != null) {
			 m_eventParam = p_request.getParameter(TDSConstants.eventParam);
		}
		
		 
		HttpSession m_session = p_request.getSession();
	 
		if(m_session.getAttribute("user") != null) {
				
			if(m_eventParam.equalsIgnoreCase("fileattachstart")) {			 
				fileattachstart(p_request,p_response);
			}
			
			if(m_eventParam.equalsIgnoreCase("fileDownload")) {			 
				fileDownloadStart(p_request,p_response);
			}
			
			
		} else {
			sentErrorReport(p_request,p_response);
		}
		
		
	}
	
	public void fileDownloadStart(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException{
		 //System.out.println("This is the Event Handler For File Download..."); 
		 String filename="";
		 filename=request.getParameter("filename")==null?"":request.getParameter("filename");
		 String filepath = "/SC/"+filename;
		 int sts=fileDownLoad(request,response,filepath);	    
		 if(sts==0){ 
		    request.setAttribute("page","**** File Not Found ****");
		    RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
			requestDispatcher.forward(request, response);
		 }
	}

	
	public void sentErrorReport(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		p_request.setAttribute("screen", "/failuer.jsp");
     	p_request.setAttribute("page", "Your Session Expired");
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getMainNewJSP);
		requestDispatcher.forward(p_request, p_response);

		
	}
	
	 
	public int fileDownLoad(HttpServletRequest req,HttpServletResponse res,String main_filepath) throws ServletException,IOException{
	 
	 	//System.out.println("File Path : ");
		//System.out.println(main_filepath);
		
		FileProperty fileProperty = null;
		 int sts=1;
		fileProperty = UtilityDAO.getFileProperty(req.getParameter("fileid"));
		
	 	  /* File file=new File(main_filepath);
	 	   int sts=0; 
	 	   if(file.exists()) {	 		   
		 	  sts=1;
		 	  
		      //convert the file content into a byte array
		      FileInputStream fileInuptStream = new FileInputStream(file);
		      BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInuptStream);
		      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		      
		      int start = 0;
		      int length = 1024;
		      int offset = -1;
		      byte [] buffer = new byte [length];  
		      
		      while((offset = bufferedInputStream.read(buffer, start, length)) != -1) {
		            byteArrayOutputStream.write(buffer, start, offset);
		      }
	 
		      bufferedInputStream.close();  
		      byteArrayOutputStream.flush();     
		      buffer = byteArrayOutputStream.toByteArray();
		      byteArrayOutputStream.close(); */
		        
		       
		       res.addHeader("Content-Disposition", "attachment;filename=\"" + fileProperty.getFilename()+ "\"");
		       
		   
		       res.addHeader("Content-Transfer-Encoding", "binary"); //Make it as Comment
		       res.setContentType("application/compressed");  //Make it as Comment 
		       res.setContentLength((int ) fileProperty.getFilesize());  
		       res.getOutputStream().write(fileProperty.getFile()); 
	 	    
	 	   
	 	   return sts;

	}
	
	public void fileattachstart(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException, IOException {
		
		File filePath = null;
		String contextPath = getServletConfig().getServletContext().getRealPath("");
			//filePath = new File(AERPPropertyProviderIPAddress.getValue("empAttachments",contextPath) +"/EmployeeAttachments/"+empKey+"/"+subDirectory);
		
	    RequestDispatcher dispatcher=getServletContext().getRequestDispatcher("/LogoUpload.jsp");
		dispatcher.forward(p_request, p_response);  


		
	}

}
