package com.tds.action;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Category;

import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.controller.TDSController;
import com.tds.dao.DispatchDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.process.MakePhoneCall;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.common.util.TDSConstants;

/**
 * Servlet implementation class SystemSetup
 */
public class PhoneManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Category cat = TDSController.cat;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ PhoneManager.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		super.init(config);
	}
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PhoneManager() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		String eventParam = "";

		if(request.getSession(false) != null){
			if(request.getSession().getAttribute("user")==null){
				return;
			}
		} else {
			return;
		}

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}

		if(eventParam.equalsIgnoreCase("conference")) {			 
			conference(request,response);
		} 


		//}
	}
	public void conference(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String driverID = request.getParameter("driverID");
		String tripID = request.getParameter("tripID");

		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		String phoneNumber = "1"+ServiceRequestDAO.getPhoneNoByTripID(adminBO.getMasterAssociateCode(), adminBO.getUid(),tripID);
		DriverCabQueueBean driverData = DispatchDAO.getDriverByDriverID(adminBO.getMasterAssociateCode(), driverID, 2);
		
		MakePhoneCall newCall = new MakePhoneCall(driverData.getPhoneNo(), phoneNumber, false);
		newCall.initiateConf();

	 }
}
