package com.tds.bean.docLib;

public class DocBean {
	private int docId;
	private String docName;
	private String tdsCompCode;
	private String docType;
	private String docExpiry;
	private String serverity;
	private String docNumber;
	private String docStatus;
	private String docModule;
	private String docUploadedBy;
	private int driverOrCab;
	
	
	
	public int getDriverOrCab() {
		return driverOrCab;
	}
	public void setDriverOrCab(int driverOrCab) {
		this.driverOrCab = driverOrCab;
	}
	public String getDocUploadedBy() {
		return docUploadedBy;
	}
	public void setDocUploadedBy(String docUploadedBy) {
		this.docUploadedBy = docUploadedBy;
	}
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getTdsCompCode() {
		return tdsCompCode;
	}
	public void setTdsCompCode(String tdsCompCode) {
		this.tdsCompCode = tdsCompCode;
	}
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public String getDocExpiry() {
		return docExpiry;
	}
	public void setDocExpiry(String docExpiry) {
		this.docExpiry = docExpiry;
	}
	public String getServerity() {
		return serverity;
	}
	public void setServerity(String serverity) {
		this.serverity = serverity;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public String getDocStatus() {
		return docStatus;
	}
	public void setDocStatus(String docStatus) {
		this.docStatus = docStatus;
	}
	public String getDocModule() {
		return docModule;
	}
	public void setDocModule(String docModule) {
		this.docModule = docModule;
	}
	
	
}
