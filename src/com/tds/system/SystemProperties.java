package com.tds.system;

public class SystemProperties {
	public static int version = 2;

	public static int getVersion() {
		return version;
	}

	public static void setVersion(int version) {
		SystemProperties.version = version;
	}
	
}
