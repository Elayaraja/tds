package com.tds.security.bean;

public class Module_detail {
	private String module_name="";
	private int module_no=0;
	private String  m_desc = "";
	private String action="";
	private String event="";
	private boolean change=false;
	private boolean bold=false;
	private boolean access=false;
	private String end="";
 
	public boolean isBold() {
		return bold;
	}
	public void setBold(boolean bold) {
		this.bold = bold;
	}
	public void setChange(boolean change) {
		this.change = change;
	}
	public boolean isChange() {
		return change;
	}

	public String getModule_name() {
		return module_name;
	}
	public void setModule_name(String module_name) {
		this.module_name = module_name;
	}
	public int getModule_no() {
		return module_no;
	}
	public void setModule_no(int module_no) {
		this.module_no = module_no;
	}
	public String getM_desc() {
		return m_desc;
	}
	public void setM_desc(String m_desc) {
		this.m_desc = m_desc;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getEvent() {
		return event;
	}
	public void setEvent(String event) {
		this.event = event;
	}
	public boolean isAccess() {
		return access;
	}
	public void setAccess(boolean access) {
		this.access = access;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	
//	
	
}
