package com.tds.security.bean;

public class TagSystemBean {

private String username;
private String password;
private String assoCode;
private String tagId;
private String startDate;
private String ToDate;
private String userId;
private String ipAddress;
private String expDate;
private String name;
private String value;

public String getUserId() {
	return userId;
}
public void setUserId(String userId) {
	this.userId = userId;
}
private int validityDays;
public int getValidityDays() {
	return validityDays;
}
public void setValidityDays(int validityDays) {
	this.validityDays = validityDays;
}
public String getStartDate() {
	return startDate;
}
public void setStartDate(String startDate) {
	this.startDate = startDate;
}
public String getToDate() {
	return ToDate;
}
public void setToDate(String toDate) {
	ToDate = toDate;
}
private boolean passW;

public boolean isPassW() {
	return passW;
}
public void setPassW(boolean passW) {
	this.passW = passW;
}
public String getTagId() {
	return tagId;
}
public void setTagId(String tagId) {
	this.tagId = tagId;
}
public String getAssoCode() {
	return assoCode;
}
public void setAssoCode(String assoCode) {
	this.assoCode = assoCode;
}
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getIpAddress() {
	return ipAddress;
}
public void setIpAddress(String ipAddress) {
	this.ipAddress = ipAddress;
}
public String getExpDate() {
	return expDate;
}
public void setExpDate(String expDate) {
	this.expDate = expDate;
}
public void setName(String name) {
	this.name = name;
}
public String getName() {
	return name;
}
public void setValue(String value) {
	this.value = value;
}
public String getValue() {
	return value;
}


}

