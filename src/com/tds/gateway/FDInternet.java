package com.tds.gateway;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class FDInternet {

	private static String web = "http://localhost:8080";

	public static void setWebAddress(String address) {
		web = address;
	}

	public static String getWebAddress() {
		return web;
	}
	
	
public static String connectToInternet(URL url1, String postData) {

		// String encodedData = ""; // user-supplied
		String response = "<Status StatusCode = \"Retry\"/>";
		HttpURLConnection conn;
		String xml_body;
		PrintStream ps;
		InputStream is;
		StringBuffer b;
		BufferedReader r;
		
		// check response for status
		//while (response.indexOf("<Status StatusCode = \"Retry\"/>") > 0) 
		//{
		
			System.setProperty("http.keepAlive", "true");
			try {
				conn = (HttpURLConnection) url1.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("User-Agent", "vxnapi_xml_3_2_0_19");
				conn.setRequestProperty("Host", "https://staging1.datawire.net/sd");

				conn.setRequestProperty("Connection","Keep-Alive");
				conn.setRequestProperty("Cache-Control","no-cache");
				conn.setRequestProperty("Content-Type","text/xml");

				xml_body ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> " +
				"<!DOCTYPE Request PUBLIC " +
				"<Request Version = \"3\"> " +
				"<ReqClientID> " +
				"<DID></DID> " +
				"<App>vxnapi_xml_3_2_0_19</App> " +
				"<Auth>000199770003994|01099794</Auth> " +
				"<ClientRef>0000001</ClientRef> " +
				"</ReqClientID> " +

				"<Registration> " +
				"<ServiceID> 115 </ServiceID> " +
				"</Registration> " +
				"</Request>";

				conn.setRequestProperty("Content-Length",""+xml_body.length());

				ps = new PrintStream(conn.getOutputStream());
				ps.write(xml_body.getBytes());
				ps.flush();
				ps.close();

				conn.connect();

				if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
					is = conn.getInputStream();
					// read reply
					b = new StringBuffer();
					r = new BufferedReader(new InputStreamReader(is));
					String line;
					while ((line = r.readLine()) != null)
						b.append(line);
					// System.out.println("Reply From Site:" + b.toString());
					is.close();
					response = b.toString();
					conn.disconnect();

				}

			}

			catch (Exception e) {
				response = "GRACIERROR;" + e.getMessage();

			}

		return response;	
		//}
	}

}
