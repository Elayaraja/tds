package com.tds.constants.docLib;

public class DocumentConstant {
	
	public static String STATUS_APPROVED = "APPROVED";
	public static String STATUS_PENDING = "PENDING";
	public static String STATUS_REJECT = "REJECTED";
	public static String STATUS_EXPIRED = "EXPIRED";
	public static String STATUS_RENEWED = "RENEWED";
	
	public static int MAX_SIZE = 5241800;
	public static String MAX_SIZE_DESC = "5MB";
	public static String FILETYPE = "*.pdf;*.jpg;*.jpeg;*.doc;*.txt;*.tif;*.bmp;";
	
	
}

