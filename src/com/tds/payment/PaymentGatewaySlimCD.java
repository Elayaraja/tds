package com.tds.payment;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ibm.icu.text.DecimalFormat;
import com.ibm.icu.text.NumberFormat;
import com.tds.dao.CreditCardDAO;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.tdsBO.CompanyMasterBO;

public class PaymentGatewaySlimCD {



	public static PaymentProcessBean makeProcess(PaymentProcessBean processBean,CompanyMasterBO cmpBean,String txnType) {

		// String encodedData = ""; // user-supplied
		String response = "<Status StatusCode = \"Retry\"/>";
		HttpURLConnection conn;
		PrintStream ps;
		InputStream is;
		StringBuffer b;
		BufferedReader r;
		String gateId="";
		String approvedMessage="";

		System.setProperty("http.keepAlive", "true");
		try {
//			URL url1 = new URL("https://trans.slimcd.com/wswebservices/transact.asmx/PostXML");
			URL url1 = new URL(cmpBean.getEmail());
			conn = (HttpURLConnection) url1.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");

			conn.setRequestProperty("Connection","Keep-Alive");
			conn.setRequestProperty("Cache-Control","no-cache");
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			String extData="";
			String expMonth="12",expYear="99";
			if(!processBean.getB_trans_id().equals("")){
				extData = "&gateid="+processBean.getB_trans_id();
			}
			if(processBean.getB_cardExpiryDate()!=null && !processBean.getB_cardExpiryDate().equals("")){
				expMonth =  processBean.getB_cardExpiryDate().substring(0, 2);
				expYear =  processBean.getB_cardExpiryDate().substring(2, 4);
			}
			double amount = Double.parseDouble(processBean.getB_amount());
		    double tip = Double.parseDouble(processBean.getB_tip_amt());
		    NumberFormat formatter = new DecimalFormat("#0.00");

		    BigDecimal Amt = new BigDecimal(formatter.format(amount)).setScale(2, RoundingMode.UNNECESSARY);
		    BigDecimal tipAmt = new BigDecimal(formatter.format(tip)).setScale(2, RoundingMode.UNNECESSARY);

			String encodedData = "clientid="+cmpBean.getCcUsername()+
					"&password="+cmpBean.getCcPassword()+
					"&siteid="+cmpBean.getSiteId()+
					"&priceid="+cmpBean.getPriceId()+
					"&key="+cmpBean.getKey()+
					"&transtype="+txnType+
					"&cardnumber="+processBean.getB_card_full_no()+
					"&product=JavaPost"+
					"&expmonth="+expMonth+
					"&expyear="+expYear+
					"&ver=1.0"+
					"&amount="+(Amt.add(tipAmt))+
					"&first_name="+URLEncoder.encode(processBean.getB_cardHolderName(),"UTF-8")+
					"&last_name="+
					"&address="+URLEncoder.encode(processBean.getCity(),"UTF-8")+
					"&city="+
					"&state="+
					"&allow_duplicates=no"+
					"&client_transref="+System.currentTimeMillis()+
					"&zip="+processBean.getZip()+extData;
			//			encodedData=URLEncoder.encode(encodedData,"UTF-8");
			System.out.println("Encode Data--->"+encodedData);
			conn.setRequestProperty("Content-Length",""+encodedData.length());

			DataOutputStream requestObject = new DataOutputStream( conn.getOutputStream() );
			requestObject.write(encodedData.getBytes());
			requestObject.flush();
			requestObject.close();

			is = conn.getInputStream();
			b = new StringBuffer();
			r = new BufferedReader(new InputStreamReader(is));
			String line;
			while ((line = r.readLine()) != null)
				b.append(line);
			System.out.println("Reply From Site:" + b.toString());
			is.close();
			response = b.toString();

			String[] successResp=response.split("\"approved\":");
			String[] newResp=successResp[1].split(",");
			approvedMessage=newResp[0].replace(" ", "");
			System.out.println("Response--->"+response);
			if(!approvedMessage.equals("") && (approvedMessage.contains("Y") ||approvedMessage.contains("B"))){
				String[] authcode=response.split("\"gateid\":");
				String[] code=authcode[1].split(",");
				gateId=code[0].replace(" ", "");
				System.out.println("GateId--->"+gateId);
				if(!gateId.equals("") && gateId.length()>2){
					processBean.setAuth_status(true);
					if(!txnType.equals(PaymentCatgories.Void) && !txnType.equals(PaymentCatgories.Reversal) && !txnType.equals(PaymentCatgories.Return)){
						processBean.setB_trans_id(gateId);
					}
					processBean.setB_approval_status("1");
				} else {
					processBean.setAuth_status(false);
					processBean.setB_approval_status("2");
					processBean.setReason("Failed");
				}
			}else {
				processBean.setAuth_status(false);
				processBean.setB_approval_status("2");
				processBean.setReason("Failed");
			}
			conn.disconnect();
		}catch(Exception e){
			response = "GRACIERROR;" + e.getMessage();
			System.out.println(response);
		}
		return processBean;
	}
}



