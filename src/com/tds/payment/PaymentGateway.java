package com.tds.payment;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.ibm.icu.text.DecimalFormat;
import com.ibm.icu.text.NumberFormat;
import com.tds.pp.bean.PaymentProcessBean;
import com.tds.tdsBO.CompanyMasterBO;

public class PaymentGateway {
	
	

	public static PaymentProcessBean makeProcess(PaymentProcessBean processBean,CompanyMasterBO companyBean,String txnType) {

		// String encodedData = ""; // user-supplied
		String response = "<Status StatusCode = \"Retry\"/>";
		HttpURLConnection conn;
		PrintStream ps;
		InputStream is;
		StringBuffer b;
		BufferedReader r;

		 
			System.setProperty("http.keepAlive", "true");
			try {
				URL url1 = new URL(companyBean.getEmail());
				conn = (HttpURLConnection) url1.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");

				conn.setRequestProperty("Connection","Keep-Alive");
				conn.setRequestProperty("Cache-Control","no-cache");
				conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");

				double amount = Double.parseDouble(processBean.getB_amount());
			    double tip = Double.parseDouble(processBean.getB_tip_amt());
			    NumberFormat formatter = new DecimalFormat("#0.00");
			      
			    String extData="";
				if(txnType.equals("Void") || txnType.equals("Return") || txnType.equals("Reversal")){
					extData="&ExtData=<AuthCode>"+processBean.getB_driverid()+"-"+processBean.getB_approval_code()+"</AuthCode>";
				} else {
					extData="&ExtData="+processBean.getB_driverid();
				}
			        //System.out.println("Amount" + formatter.format(amount));
			    BigDecimal Amt = new BigDecimal(formatter.format(amount)).setScale(2, RoundingMode.UNNECESSARY);
			    BigDecimal tipAmt = new BigDecimal(formatter.format(tip)).setScale(2, RoundingMode.UNNECESSARY);
			    
			    String transID = ((processBean.getB_trans_id()!=null && !processBean.getB_trans_id().equals(""))?processBean.getB_trans_id():processBean.getB_trans_id2());
				String captureID = processBean.getB_cap_trans();
			    
				//xml_body ="<?xml version=\"1.0\" encoding=\"UTF-8\"?> " ;
				String encodedData = "UserName=" + URLEncoder.encode(companyBean.getCcUsername(), "UTF-8")+
				"&Password="+companyBean.getCcPassword()+
				"&TransType="+txnType+
				"&Vendor="+companyBean.getCcVendor()+
				"&CardNum="+processBean.getB_card_full_no()+
				"&ExpDate="+processBean.getB_cardExpiryDate()+
				"&MagData="+URLEncoder.encode(getMaganiticData(processBean),"UTF-8")+
				"&NameOnCard="+URLEncoder.encode(processBean.getB_cardHolderName()==null?"":processBean.getB_cardHolderName(),"UTF-8")+
				"&Amount="+(Amt.add(tipAmt))+
				"&InvNum="+processBean.getB_trip_id()+
				"&PNRef=" +((captureID==null || captureID.equals(""))?transID:captureID)+
				"&Zip="+URLEncoder.encode(processBean.getZip(),"UTF-8")+
				"&Street="+URLEncoder.encode(processBean.getCity(),"UTF-8")+
				"&CVNum="+extData;
				System.out.println("Encoded--->"+encodedData);
				conn.setRequestProperty("Content-Length",""+encodedData.length());
				ps = new PrintStream(conn.getOutputStream());
				ps.write(encodedData.getBytes());
				ps.flush();
				ps.close();
				conn.connect();

				if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
					is = conn.getInputStream();
					// read reply
					b = new StringBuffer();
					r = new BufferedReader(new InputStreamReader(is));
					String line;
					while ((line = r.readLine()) != null)
						b.append(line);
					// System.out.println("Reply From Site:" + b.toString());
					is.close();
					response = b.toString();
					conn.disconnect();

				}

			}

			catch (Exception e) {
				response = "GRACIERROR;" + e.getMessage();
				processBean.setAuth_status(false);
			}
			System.out.println("Response--->"+response);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			try{
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				//Document doc = dBuilder.parse(response);
				Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("utf-8"))));
				doc.getDocumentElement().normalize();
				NodeList nList = doc.getElementsByTagName("Response");
				for (int temp = 0; temp < nList.getLength(); temp++) {
					Node nNode = nList.item(temp);
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) nNode;
						if(getTagValue("Result",eElement).equals("0")){
							processBean.setB_approval_code(getTagValue("AuthCode",eElement));
							if(!txnType.equals(PaymentCatgories.Void) && !txnType.equals(PaymentCatgories.Reversal) && !txnType.equals(PaymentCatgories.Return)){
								processBean.setB_trans_id(getTagValue("PNRef",eElement));
					        	processBean.setB_approval_status("1");
					        	if(!txnType.equals(PaymentCatgories.Force)){
					        		processBean.setStatus_code(getTagValue("HostCode",eElement));
					        	}
					        	processBean.setB_approval(getTagValue("Message",eElement));
							}
							processBean.setAuth_status(true);
						} else {
							processBean.setAuth_status(false);
							String reason=getTagValue("RespMSG",eElement);
				        	processBean.setB_approval_status("2");
							processBean.setReason(reason);
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				processBean.setAuth_status(false);
				processBean.setReason(e+"");
			}			
			
			return processBean;
		//}
	}


	private static String getMaganiticData(PaymentProcessBean processBean)
	{
		 if(!processBean.getTrack1().equals(""))
	        {
	            
	        	//CD.setTrack1Data(processBean.getTrack1());
	        	return processBean.getTrack1(); 
	        	    
	        } else {
	        	//CD.setTrack2Data(processBean.getTrack2());
	        	return processBean.getTrack2();
	        }
	}


	  private static String getTagValue(String sTag, Element eElement) {
		  if(eElement.getElementsByTagName(sTag).item(0)!=null){
				NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();
			        Node nValue = (Node) nlList.item(0);
				return nValue.getNodeValue();
		  } else {
			  return "";
		  }
	  }
}



