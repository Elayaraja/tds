package com.tds.payment;

public interface PaymentCatgories {

	String Sale = "Sale";
	String Auth = "Auth";
	String Void = "Void";
	String Force = "Force";
	String RepeatSale = "RepeatSale";
	String Adjustment = "Adjustment";
	String Capture = "Capture";
	String CaptureAll = "CaptureAll";
	String Reversal = "Reversal";
	String Return = "Return";
	String Load="LOAD";
}
