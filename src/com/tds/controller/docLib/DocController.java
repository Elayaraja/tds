package com.tds.controller.docLib;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tds.bean.docLib.DocBean;
import com.tds.constants.docLib.DocumentConstant;
import com.tds.dao.docLib.DocumentDAO;
import com.tds.dao.docLib.DocumentMaterDAO;
import com.tds.tdsBO.AdminRegistrationBO;

public class DocController extends HttpServlet {

	public void showListDocController(HttpServletRequest request) {

	/*	String companyId = (String) request.getParameter("compId");
		String userId = (String) request.getParameter("userId");
		request.setAttribute("compId", companyId);
		request.setAttribute("userId", userId);
*/
		
		HttpSession m_session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
		request.setAttribute("compId", adminBO.getMasterAssociateCode()); 
		request.setAttribute("userId", adminBO.getUid());
		
		DocumentDAO docDao = new DocumentDAO();
		List<DocBean> docBeanList = new ArrayList<DocBean>();
		docBeanList = docDao.getDocumentList("USER", adminBO.getUid(), null);
		request.setAttribute("docList", docBeanList);

	}

	public void showSearchDocuments(HttpServletRequest request) {

		DocumentDAO docDao = new DocumentDAO();
		HttpSession m_session = request.getSession(false);
		AdminRegistrationBO adminBO =(AdminRegistrationBO)m_session.getAttribute("user");
		List<DocBean> docBeanList = new ArrayList<DocBean>();
		DocumentMaterDAO dmDao = new DocumentMaterDAO();
		request.setAttribute("docList", docBeanList);
		request.setAttribute("docTypeData", dmDao.getMasterData(adminBO.getMasterAssociateCode(),0));

	}

	public void searchDocumentList(HttpServletRequest request) {

		String companyId = (String) request.getParameter("searchCompId");
		String docNumber = (String) request.getParameter("searchNumber");
		String docType = (String) request.getParameter("searchDocType");
		int driverOrCab = Integer.parseInt(request.getParameter("driverOrCab")==null?"2":request.getParameter("driverOrCab"));
		String userId = (String)request.getParameter("userId");
		StringBuffer query = new StringBuffer(
				"select *,DATE_FORMAT(DOC_EXP_DATE,'%m/%d/%Y') AS EXPIRY_DATE from TDS_DOCUMENTS where 1=1 ");
		if (companyId != null && !companyId.equals("")) {
			query.append(" and TDS_COMP_CODE='").append(companyId).append("' ");
		}
		if (docNumber != null && !docNumber.equals("")) {
			query.append(" and DOC_NUMBER='").append(docNumber).append("' ");
		}
		if (docType != null && !docType.equals("")) {
			query.append(" and DOC_TYPE='").append(docType).append("' ");
		}
		if (userId != null && !userId.equals("")) {
			query.append(" and DOC_UPLOADED_BY='").append(userId).append("' ");
		}
		if (driverOrCab!=2) {
			query.append(" and DOC_DRIVER_OR_CAB='").append(driverOrCab).append("' ");
		}
		HttpSession m_session = request.getSession(false);
		AdminRegistrationBO adminBO =(AdminRegistrationBO)m_session.getAttribute("user");
		DocumentDAO docDao = new DocumentDAO();
		List<DocBean> docBeanList = new ArrayList<DocBean>();
		DocumentMaterDAO dmDao = new DocumentMaterDAO();
		docBeanList = docDao.getDocumentList("", "", query.toString());
		request.setAttribute("docList", docBeanList);
		request.setAttribute("docTypeData", dmDao.getMasterData(adminBO.getMasterAssociateCode(),0));
	}


	public void rejectDocController(HttpServletRequest request) {

		String docId = request.getParameter("docId") != null ? request
				.getParameter("docId") : "";
		String userId = request.getParameter("userId") != null ? request
				.getParameter("userId") : "N/A";
		DocumentDAO docDAO = new DocumentDAO();
		docDAO.updateDocStatus(docId, DocumentConstant.STATUS_REJECT, userId);
		request.setAttribute("userId", userId);

	}

public void approveDocController(HttpServletRequest request) {

		String userId = (String) request.getParameter("userId");
		DocBean docBean = new DocBean();
		docBean.setDocId((Integer.parseInt((String) request.getParameter("hCompId"))));
		docBean.setDocNumber((String) request.getParameter("docNumber"));
		docBean.setDocType((String) request.getParameter("hType"));
		docBean.setDocModule((String) request.getParameter("hModule"));
		docBean.setDocExpiry((String) request.getParameter("expDate"));
		docBean.setServerity((String) request.getParameter("Severity"));
		docBean.setDocUploadedBy((String) request.getParameter("txtUserId"));
		docBean.setDriverOrCab(Integer.parseInt( request.getParameter("driverOrcab")));
		DocumentDAO docDao = new DocumentDAO();
		docDao.updateDocInfo(docBean, userId);
		request.setAttribute("userId", userId);
		request.setAttribute("reply", "ok");
	}


public static void loadAdminPage(HttpServletRequest request) {
		DocumentDAO docDao = new DocumentDAO();
		List<DocBean> docBeanList = new ArrayList<DocBean>();
		HttpSession m_session = request.getSession(false);
		AdminRegistrationBO adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
		
		docBeanList = docDao.getDocumentList("ADMIN",  adminBO.getMasterAssociateCode(), null);
		DocumentMaterDAO dmDao = new DocumentMaterDAO();
		request.setAttribute("docTypeDataDriver", dmDao.getMasterData(adminBO.getMasterAssociateCode(),1));
		request.setAttribute("docTypeDataVehicle", dmDao.getMasterData(adminBO.getMasterAssociateCode(),2));

		request.setAttribute("docList", docBeanList);
		request.setAttribute("compId", dmDao.getMasterData(adminBO.getMasterAssociateCode(),0));

	}

}
