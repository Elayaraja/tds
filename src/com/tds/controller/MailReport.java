package com.tds.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Category;

import com.lowagie.text.pdf.codec.Base64;
import com.tds.controller.TDSController;
import com.tds.process.Email;

public class MailReport extends Thread {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(MailReport.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given Class is "+MailReport.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	String url = "";
	String tripID = "";
	String mailId="";
	String masterAssoccode="";
	String timeZone="";
	String reportName="";
	int reportType=1;
	String messageToSent="";
	
	ArrayList<String> emails = new ArrayList<String>();
	public MailReport(String _url,String _tripId,String _mailId,String _timeZone,ArrayList<String> _emails,String _reportName,String _masterAssoccode,int _type,String _messageToSent) throws IOException {

		url = _url;
		tripID = _tripId;
		mailId = _mailId;
		timeZone=_timeZone;
		masterAssoccode=_masterAssoccode;
		emails = _emails;
		reportName = _reportName;
		reportType = _type;
		messageToSent = _messageToSent;
	}

	@Override
	public void run(){
		String urlString ="";
		if(reportType==1){
			urlString = url+"/TDS/ReportController?event=reportEvent&ReportName="+reportName+"&output=pdf&tripId="+tripID+"&type=basedMail&assoccode="+masterAssoccode+"&timeOffset="+timeZone;
		} else if(reportType==2) {
			urlString = url+"/TDS/ReportController?event=reportEvent&ReportName="+reportName+"&output=pdf&invoiceNo="+tripID;
		}
		System.out.println("Url--->"+urlString);
		System.setProperty("http.keepAlive", "true");
		try {
			URL urlV = new URL(urlString.toString());
			HttpURLConnection conn = (HttpURLConnection) urlV.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(60000);
			String responseNew="";
			byte bytes[] = null;
			conn.connect();
			System.out.println("Response Code--->"+conn.getResponseCode());
			if (HttpURLConnection.HTTP_OK == conn.getResponseCode()) {
				java.io.InputStream is = conn.getInputStream();
				//Convert InputStream to ByteArray
				bytes = IOUtils.toByteArray(is);
				System.out.println("Byte Size--->"+bytes.length);
				is.close();
				conn.disconnect();
			}
			String username=emails.get(0);
			String password=emails.get(1);
			String host=emails.get(3);
			String port=emails.get(4);
			String protocol=emails.get(5);

			//Convert ByteArray to String
			String stringToSent = Base64.encodeBytes(bytes);
			System.out.println("Before Email--->"+stringToSent);
			Email otherProviderSMS = new Email(host,username,password,username,port,protocol);
			try{
				otherProviderSMS.sendMailPdf(mailId,"Trip_Details",stringToSent, messageToSent+". PFA for more on your trip details");
			} catch (Exception e ){
				System.out.println(e.toString());
			}
		}catch (Exception e) {
			System.out.println("GetACab Exception Posting to Internet" + e.getMessage());
		}
	}
}
