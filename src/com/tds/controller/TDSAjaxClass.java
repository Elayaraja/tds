package com.tds.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.charges.bean.LeaseBean;
import com.charges.bean.MiscBean;
import com.charges.dao.ChargesDAO;
import com.tds.cmp.bean.CabDriverMappingBean;
import com.tds.cmp.bean.CallerIDBean;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.DriverRestrictionBean;
import com.tds.dao.AddressDAO;
import com.tds.dao.AdministrationDAO;
import com.tds.dao.AuditDAO;
import com.tds.dao.CallerIDDAO;
import com.tds.dao.DispatchDAO;
import com.tds.dao.RegistrationDAO;
import com.tds.dao.RequestDAO;
import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SharedRideDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.ZoneDAO;
import com.tds.ivr.IVRDriverCaller;
import com.common.util.Messaging;
import com.tds.process.OneToOneSMS;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.OpenRequestBO;
import com.tds.tdsBO.QueueBean;
import com.common.util.CheckSpecialFlags;
import com.tds.util.CheckUserInformation;
import com.tds.util.MapParser;
import com.common.util.MessageGenerateJSON;
import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
import com.common.util.TDSValidation;

public class TDSAjaxClass extends HttpServlet {

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			doProcess(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Access Post method from JSP page or Browser
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {

		try {
			doProcess(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String eventParam = "";

		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}
		if((AdminRegistrationBO)request.getSession().getAttribute("user")!=null)
		{

			if (eventParam.equalsIgnoreCase("loadFromToAddr")) {
				getFromToAddress(request, response);
			} else if (eventParam.equalsIgnoreCase("loadOther")) {
				loadOther(request, response);
			} else if (eventParam.equalsIgnoreCase("loadQueueValues")) {
				loadQueueValues(request, response);
			} else if (eventParam.equalsIgnoreCase("getDashBoard")) {
				getDashBoardStartup(request, response);
			} else if (eventParam.equalsIgnoreCase("removeDriverinQ")) {
				removeDriverinQ(request, response);
			} else if (eventParam.equalsIgnoreCase("moveDriverinQ")) {
				moveDriverinQ(request, response);
			} else if (eventParam.equalsIgnoreCase("loadCompany")) {
				loadCompany(request, response);
			} else if (eventParam.equalsIgnoreCase("deleteOpen")) {
				deleteOpen(request, response);
			} else if (eventParam.equalsIgnoreCase("deleteAll")) {
				deleteAll(request, response);
			} else if (eventParam.equalsIgnoreCase("doQupdate")) {
				doQupdate(request, response);
			} else if (eventParam.equalsIgnoreCase("updateDriver")) {
				doDriverUpdate(request, response);
			} else if (eventParam.equalsIgnoreCase("allocateOQ")) {
				allocateOQ(request, response);
			} else if (eventParam.equalsIgnoreCase("deleteOpenReq")) {
				deleteOpenReq(request, response);
			} else if (eventParam.equalsIgnoreCase("showDriver")) {
				showDriver(request, response);
			} else if (eventParam.equalsIgnoreCase("doUpdateVoucher")) {
				doUpdateVoucher(request, response);
			} else if (eventParam.equalsIgnoreCase("loadFromAndToAddrs")) {
				loadFromAndToAddrs(request, response);
			} else if (eventParam.equalsIgnoreCase("checkAssCode")) {
				checkAssoCode(request, response);
			} else if (eventParam.equalsIgnoreCase("previousRequest")) {
				previousRequest(request, response);
			} else if (eventParam.equalsIgnoreCase("openrequestDetails")) {
				openrequestDetails(request, response);
			} else if (eventParam.equalsIgnoreCase("getlandmardDetails")) {
				getlandmardDetails(request, response);
			} else if (eventParam.equalsIgnoreCase("getmapdetails")) {
				getMapDetails(request, response);
			} else if (eventParam.equalsIgnoreCase("deletecabMappingDetails")) {
				deleteCabMappingDetails(request, response);
			} else if (eventParam.equalsIgnoreCase("deleteAddress")) {
				deleteAddress(request, response);
			} else if (eventParam.equalsIgnoreCase("getZoneName")) {
				getZoneName(request, response);
			} else if (eventParam.equalsIgnoreCase("callerId")) {
				callerId(request, response);
				//			} else if (eventParam.equalsIgnoreCase("getComments")) {
				//				getComments(request, response);
			} else if(eventParam.equals("getLeaseAmount"))
			{
				getLeaseAmount(request,response);
			}else if(eventParam.equals("getLeaseDetail"))
			{
				getLeaseDetail(request,response);
			}else if(eventParam.equals("getMiscDetail"))
			{
				getMiscDetail(request,response);
			}
			else if(eventParam.equals("removeLease"))
			{
				removeLease(request,response);
			}
			else if(eventParam.equals("removeMisce"))
			{
				removeMisce(request,response);
			}else if (eventParam.equalsIgnoreCase("ciHistory")) {
				ciHistory(request, response);
			}


		}	
	}
	public void removeLease(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception
	{
		ChargesDAO.removeLease(request.getParameter("id"));
		response.getWriter().write(1);
	}
	public void removeMisce(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception
	{
		ChargesDAO.removeMisc(request.getParameter("id"));
		response.getWriter().write(1);
	}
	public void getLeaseDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception
	{
		String htmlText = "";
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<LeaseBean> al_lease =  ChargesDAO.getLeaseBean(adminBO.getAssociateCode(), request.getParameter("driverId"));
		double total = 0.0;
		htmlText = htmlText + "<table  class=\"pretty-table\" width=\"100%\" ><tr><th>Vehicle Type</th><th>Cab No</th><th>Lease Amount</th></tr>";
		for(int i=0;i<al_lease.size();i++)
		{
			LeaseBean leaseBean = al_lease.get(i);
			total = total + leaseBean.getVechAmount();
			htmlText = htmlText + "<tr><td>" + leaseBean.getVechDesc()+"</td><td>"+leaseBean.getVechileNo()+"</td><td>"+leaseBean.getVechAmount()+"" +
					"</td>	" +
					"<td><a href=\"javascript:doNothing()\" onclick=\"fnRemoveLease('"+leaseBean.getLeaseID()+"')\">Remove</a>" +
					"</tr>";
		}
		htmlText = htmlText + "<tr><td><a onclick=\"fnCallLease()\" href=\"javascript:doNothing()\">Add</a></td></tr>";
		htmlText = htmlText + "</table>"+"###"+total;
		response.getWriter().write(htmlText);
	}
	public void getMiscDetail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception
	{
		String htmlText = "";
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<MiscBean> al_misc =  ChargesDAO.getMiscBean(adminBO.getAssociateCode(), request.getParameter("driverId"),adminBO.getMasterAssociateCode(),"");
		double total = 0.0;
		htmlText = htmlText + "<table class=\"pretty-table\" width=\"100%\" ><tr><th>Misc Desc</th><th>Comments</th><th>Misc Amount</th><th>Remove</th></tr>";

		for(int i=0;i<al_misc.size();i++){
			MiscBean miscBean = al_misc.get(i);
			total = total + miscBean.getMiscAmount();
			htmlText = htmlText + "<tr><td>" + miscBean.getMiscDesc()+"</td><td>"+(miscBean.getCustomField()==null?"":miscBean.getCustomField())+"<td>"+miscBean.getMiscAmount()+"</td>" +
					"</td>	" +
					"<td><a href=\"javascript:doNothing()\" onclick=\"fnRemoveMisc('"+miscBean.getMiscid()+"')\">Remove</a>" +
					"</tr>";
		}
		htmlText = htmlText + "<tr><td><a onclick=\"fnCallMisc()\" href=\"javascript:doNothing()\">Add</a></td></tr>";
		htmlText = htmlText + "</table>"+"###"+total;
		response.getWriter().write(htmlText);
	}


	public void getLeaseAmount(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception
	{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		response.getWriter().write(ChargesDAO.getLeaseAmount(adminBO.getMasterAssociateCode(), request.getParameter("vechType")));
	}
	public void getMapDetails(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception
	{
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		//StringBuffer buffer = new StringBuffer();
		//HttpSession session = request.getSession();
		//PrintWriter out = response.getWriter();
		OpenRequestBO openRequestBean=new OpenRequestBO(); 

		if(request.getParameter("address")!=null){
			openRequestBean.setSadd1(request.getParameter("address"));
		}
		ArrayList arr_list = RegistrationDAO.landMarkSummary(adminBO,openRequestBean,0,0,0,0);

		request.setAttribute("zonedetails", arr_list);
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getOpenMap);
		requestDispatcher.forward(request, response);
	}
	/*
	public void getmapdetailsTemp(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,Exception
	{
	System.out.println("ajax class");
	//StringBuffer buffer = new StringBuffer();
	//HttpSession session = request.getSession();
	//PrintWriter out = response.getWriter();




	ArrayList al_q = new ArrayList();
	Geocoder geocoder = new Geocoder();
	ArrayList arr_list = new ArrayList();
	ArrayList arr_list1 = new ArrayList();

	// I'm getting the application ID as a parameter
	geocoder.setApplicationId("mrk2HiHV34GRu1xf3lQ3TKQsUx3BVGgoAY5l5TIUWgpATnj3CGYKgij.cf4FeGBpI4q3Sfva4k4Q");
	//geocoder.setApplicationId("mrk2HiHV34GRu1xf3lQ3TKQsUx3BVGgoAY5l5TIUWgpATnj3CGYKgij.cf4FeGBpI4q3Sfva4k4Q

	String street = request.getParameter("address");
	String city = request.getParameter("city");
	String state = request.getParameter("state");
	String zip = request.getParameter("zip");


	Address address = new Address(street, city, state, zip);
	arr_list = geocoder.geocode(address);

	request.setAttribute("zonedetails", arr_list);



	System.out.println("GET LOT LONGTUDE SIZE:::"+arr_list.size()); 



	RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(TDSConstants.getOpenMap);
	requestDispatcher.forward(request, response);

}
	 */	
	public void previousRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{ 
		String phoneNum = request.getParameter("phone");
		String lineNum = request.getParameter("lineNumber");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		//Retrieve all previous open requests for the given phone number
		if(!TDSValidation.getDBPhoneFormat(phoneNum).equals("")){
			ArrayList al_list = new ArrayList();
			al_list  = RegistrationDAO.getpreviousDetails(TDSValidation.getDBPhoneFormat(phoneNum), adminBO); 
			request.setAttribute("prevopenrequests", al_list);

			//Retrieve all landmark based on phone number
			ArrayList al_landMark=new ArrayList();
			al_landMark=RegistrationDAO.getLandMark(TDSValidation.getDBPhoneFormat(phoneNum),adminBO.getMasterAssociateCode());
			request.setAttribute("landMark", al_landMark);
			//if(al_landMark.size()==0){
				//Retrieve all previous used addresses for the given phone number	
				ArrayList al_prevaddress  = RegistrationDAO.getFromandToAddress(TDSValidation.getDBPhoneFormat(phoneNum), adminBO.getMasterAssociateCode());
				ArrayList<OpenRequestBO> al_previousLM=AddressDAO.getPreviousLM(TDSValidation.getDBPhoneFormat(phoneNum), adminBO.getMasterAssociateCode());
				request.setAttribute("prevaddresses", al_prevaddress);
				request.setAttribute("prevLM", al_previousLM);
			//} 
			if(lineNum.equals("0")){
				getServletContext().getRequestDispatcher(TDSConstants.getOpenJSP).forward(request, response);
			} else {
				getServletContext().getRequestDispatcher(TDSConstants.getOpenShortJSP).forward(request, response);
			}
		} else {
			response.getWriter().write("Error###");
		}

	}

	public void getlandmardDetails(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {

		//System.out.println("get landmark  DETAILS"); 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String screen = TDSConstants.viewlandmarkJSP; 
		//HttpSession session = request.getSession();
		OpenRequestBO openRequestBO = new OpenRequestBO();
		String land="";
		if(request.getParameter("landMark") != null && request.getParameter("landMark") != "") {
			land= request.getParameter("landMark");
		}

		ArrayList al_list = new ArrayList(); 
		al_list = RequestDAO.getlandMarkDetails(land,adminBO.getAssociateCode()); 
		request.setAttribute("landmarkDetails", al_list); 
		screen = TDSConstants.viewlandmarkJSP; 
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(screen);
		requestDispatcher.forward(request, response);
	}

	public void openrequestDetails(HttpServletRequest request,HttpServletResponse response) throws ServletException,IOException {

		//System.out.println("VIEW OPEN REQUEST DETAILS"); 
		String screen = TDSConstants.viewopenRequestJSP; 
		HttpSession session = request.getSession();
		OpenRequestBO openRequestBO = new OpenRequestBO();
		if(request.getParameter("tripid") != null && request.getParameter("tripid") != "") {
			openRequestBO.setTripid(request.getParameter("tripid"));
		}

		ArrayList al_list = new ArrayList(); 
		al_list = RequestDAO.getopenrequestView(((AdminRegistrationBO) session.getAttribute("user")).getAssociateCode(),openRequestBO); 
		request.setAttribute("openrequest", al_list.get(0));
		request.setAttribute("btn", "acceptrequest");
		request.setAttribute("openSummary", new ArrayList());
		request.setAttribute("al_q", ServiceRequestDAO.getQueueName(openRequestBO.getSlat(), openRequestBO.getSlong(),(AdminRegistrationBO) session.getAttribute("user")));
		screen = TDSConstants.viewopenRequestJSP; 
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(screen);
		requestDispatcher.forward(request, response);
	}

	public void loadFromAndToAddrs(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		response.setContentType("text/html; charset=ISO-8859-1");
		PrintWriter out = response.getWriter(); 

		StringBuffer buffer = new StringBuffer();
		//buffer.append("<html><script type='text/javascript' src='Ajax/OpenRequest.js'></script><body>");
		//buffer.append("<table border=1><tr style='color: grey'><td>S.No</td><td>From Address</td><td>To Address<td></tr>");
		buffer.append("<table border=1><tr style='color: grey'><td>S.No</td><td>From</td><td>To</td><td>Address</td></tr>");
		ArrayList al_list = new ArrayList();
		al_list  = RegistrationDAO.getFromandToAddress(TDSValidation.getDBPhoneFormat(request.getParameter("phone")),adminBO.getMasterAssociateCode());
		for(int i=0;i<al_list.size();i++)
		{
			OpenRequestBO requestBO = (OpenRequestBO)al_list.get(i);
			buffer.append("<tr>");
			buffer.append("<td>"+(i+1)+"</td>"); 
			buffer.append("<td><B></B><input type='radio' id='Fcheck"+i+"' name='Fcheck"+i+"'  value='Fcheck'   onClick='appendParentFrom("+i+",this)'></td>");
			buffer.append("<td><B></B><input type='radio' id='Fcheck"+i+"' name='Fcheck"+i+"'   value='Tcheck'  onClick='appendParentFrom("+i+",this)'></td>");
			buffer.append("<td><table>"); 
			buffer.append("<tr>"); 
			buffer.append("<td>"+requestBO.getSadd1()+"<textarea style='display: none;' name='sadd1_"+i+"' id='sadd1_"+i+"'>"+requestBO.getSadd1()+"</textarea>,</td>");
			buffer.append("<td>"+requestBO.getSadd2()+"<input type='hidden' name='sadd2_"+i+"' id='sadd2_"+i+"'       value="+requestBO.getSadd2()+">,</td>");
			buffer.append("<td>"+requestBO.getScity()+"<input type='hidden' name='scity"+i+"' id='scity"+i+"' value="+requestBO.getScity()+">,</td>");
			buffer.append("<td>"+requestBO.getSstate()+"<input type='hidden' name='sstate"+i+"' id='sstate"+i+"' value="+requestBO.getSstate()+">,</td>"); 
			buffer.append("<td>"+requestBO.getSzip()+"<input type='hidden' name='szip"+i+"' id='szip"+i+"' value="+requestBO.getSzip()+"></td>");
			buffer.append("</tr>"); 
			buffer.append("</table></td>"); 	
			buffer.append("</tr>");

			/*buffer.append("<tr>");
		 	buffer.append("<td><B>Address1:</B>"+requestBO.getEadd1()+"<input type='hidden' name='sadd1_"+i+"' id='eadd1_"+i+"' value="+requestBO.getEadd1()+"></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>Address2</B>"+requestBO.getEadd2()+"<input type='hidden' name='sadd2_"+i+"' id='eadd2_"+i+"' value="+requestBO.getEadd2()+"></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>City:</B>"+requestBO.getEcity()+"<input type='hidden' name='scity"+i+"' id='scity"+i+"' value="+requestBO.getEcity()+"></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>State:</B>"+requestBO.getEstate()+"<input type='hidden' name='sstate"+i+"' id='sstate"+i+"' value="+requestBO.getEstate()+"></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>Zipcode</B>"+requestBO.getEzip()+"<input type='hidden' name='szip"+i+"' id='szip"+i+"' value="+requestBO.getEzip()+"></td>");
			buffer.append("</tr>");

			buffer.append("</table></td>");

			buffer.append("<td><B></B><input type='radio' id='Fcheck"+i+"' name='Fcheck"+i+"' onClick='appendParentFrom("+i+")'></td>");
			buffer.append("<td><B></B><input type='radio' id='Tcheck"+i+"' name='Tcheck"+i+"' onClick='appendParentTo("+i+")'></td>");

			buffer.append("</tr>");
			 */	 
			/*buffer.append("<td><table>");


			buffer.append("<tr>");
		 	buffer.append("<td><B></B><input type='radio' id='Tcheck"+i+"' name='Tcheck"+i+"' onClick='appendParentTo("+i+")'></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>Address1:</B>"+requestBO.getEadd1()+"<input type='hidden' name='eadd1_"+i+"' id='eadd1_"+i+"' value="+requestBO.getEadd1()+"></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>Address2</B>"+requestBO.getEadd2()+"<input type='hidden' name='eadd2_"+i+"' id='eadd2_"+i+"' value="+requestBO.getEadd2()+"></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>City:</B>"+requestBO.getEcity()+"<input type='hidden' name='ecity"+i+"' id='ecity"+i+"' value="+requestBO.getEcity()+"></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>State:</B>"+requestBO.getEstate()+"<input type='hidden' name='estate"+i+"' id='estate"+i+"' value="+requestBO.getEstate()+"></td>");
			buffer.append("</tr>");

			buffer.append("<tr>");
		 	buffer.append("<td><B>Zipcode</B>"+requestBO.getEzip()+"<input type='hidden' name='ezip"+i+"' id='ezip"+i+"' value="+requestBO.getEzip()+"></td>");
			buffer.append("</tr>");

			 */

			//buffer.append("</table></td>");

			//buffer.append("</tr>");


		}
		//buffer.append("<tr><td colspan='3' align='center'><input type='button' value='Close' onclick='javascript:window.close();'></td></tr>");

		buffer.append("</table>");

		//buffer.append("</body></html>");
		//response.getWriter().write(buffer.toString());
		out.write(buffer.toString());
	}

	public void doUpdateVoucher(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String date = RegistrationDAO.doUpdateVoucherStatus(((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode(), request.getParameter("costc"));

		response.getWriter().write(date);
	}
	public void checkAssoCode(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{


		int value = RegistrationDAO.checkAsscode(request.getParameter("driverId"), request.getParameter("assCode")); 
		String result = ""+value;


		response.getWriter().write(result.toString());
	}


	public void showDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		HttpSession session = request.getSession();
		Map m_driverDetailMap = null;		
		List m_driverList = UtilityDAO.getDriverLocation(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(),request.getParameter("driver_id") == null?"":request.getParameter("driver_id"));

		StringBuffer buffer = new StringBuffer();

		buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"	xmlns:v=\"urn:schemas-microsoft-com:vml\">");
		buffer.append("<head>");
		buffer.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />");
		buffer.append("<script src='"+TDSProperties.getValue("googleMap")+"' type=\"text/javascript\"></script>");
		buffer.append("<script type=\"text/javascript\">");
		//buffer.append("function samp() {");
		buffer.append("alert('HAI');");
		buffer.append("var latitude =  new Array ();var longitude = new Array ();var driver = new Array ();var extension = new Array ();");
		if(m_driverList != null) {
			//m_driverDetailMap = new HashMap();
			for(int m_driverCount = 0; m_driverCount < m_driverList.size(); m_driverCount++) {
				m_driverDetailMap = (Map)m_driverList.get(m_driverCount);
				buffer.append("latitude["+m_driverCount+"]="+m_driverDetailMap.get("latitude")+";");
				buffer.append("longitude["+m_driverCount+"]="+m_driverDetailMap.get("longitude")+";");
				buffer.append("extension["+m_driverCount+"]="+m_driverDetailMap.get("driverext")+";");
				buffer.append("driver["+m_driverCount+"]="+m_driverDetailMap.get("driverid")+";");
			}			 
			buffer.append("initialize(latitude, longitude, driver, extension);");
		}
		//buffer.append("}");

		buffer.append("function initialize(latitude, longitude, driver, extension) { ");
		buffer.append("if (GBrowserIsCompatible() ) { ");
		buffer.append("var map = new GMap2(document.getElementById(\"map_canvas\"));");
		buffer.append("map.setCenter(new GLatLng(39.238449, -96.329435), 4);");
		buffer.append("map.addControl(new GSmallMapControl());");
		buffer.append("map.addControl(new GMapTypeControl());");
		buffer.append("var baseIcon = new GIcon(G_DEFAULT_ICON);");
		buffer.append("baseIcon.iconSize = new GSize(20, 34);");
		buffer.append("baseIcon.shadowSize = new GSize(37, 34);");
		buffer.append("baseIcon.iconAnchor = new GPoint(9, 34);");
		buffer.append("baseIcon.infoWindowAnchor = new GPoint(9, 2);");
		buffer.append("function createMarker(point, ext, driver) {");
		buffer.append("var letteredIcon = new GIcon(baseIcon);");
		buffer.append("markerOptions = { icon:letteredIcon };");
		buffer.append("var marker = new GMarker(point, markerOptions);");
		buffer.append("GEvent.addListener(marker, \"click\", function() {");
		buffer.append("marker.openInfoWindowHtml(\"1-888-GET-A-CAB \"+driver+\" \" + ext + \"</b>\");");
		buffer.append("});");
		buffer.append("return marker;");
		buffer.append(" }");
		buffer.append("var latlng;");
		buffer.append(" for(var counter = 0; counter < latitude.length; counter++) {" +
				"latlng = new GLatLng(latitude[counter], longitude[counter]);" +
				"map.addOverlay(createMarker(latlng, extension[counter], driver[counter]));" +
				"}");
		buffer.append(" } }");		
		buffer.append("</script>");
		buffer.append("</head>");
		buffer.append("<body>");
		buffer.append("<div id=\"map_canvas\" style=\"width: 800px; height: 500px\"></div>");
		buffer.append("</body></html>");

		response.getWriter().write(buffer.toString());

	}
	public void deleteOpenReq(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession session = request.getSession();
		StringBuffer buffer = new StringBuffer();
		String reason=request.getParameter("answer");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		int status = AdministrationDAO.updateOpenRequest(request.getParameter("tripid"),((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(),request.getParameter("type").equalsIgnoreCase("1")?"0":"1","50",reason,adminBO.getUid());
		if(status == 1)
			if(request.getParameter("type").equalsIgnoreCase("1")){
				AuditDAO.insertauditRequest(request.getParameter("assoccode"), ((AdminRegistrationBO)session.getAttribute("user")).getUname() , request.getParameter("answer"), "", "", "205","","");
				buffer.append(status+"###");
				buffer = getOpnReqinQ(buffer,request.getParameter("assoccode"));
			} else {
				AuditDAO.insertauditRequest(request.getParameter("assoccode"), ((AdminRegistrationBO)session.getAttribute("user")).getUname() , request.getParameter("answer"), "", "", "206","","");
				buffer.append(status+"###");
				buffer = getBroadOpnReq(buffer,request.getParameter("assoccode"));
			}
		else
			buffer.append("0###");
		response.getWriter().write(buffer.toString());
	}
	public StringBuffer getBroadOpnReq(StringBuffer buffer, String assocode)
	{
		ArrayList al_list = null;
		ArrayList al_q = null,al_d = null;

		//al_list = AdministrationDAO.getOpnReqDash(assocode,1);

		buffer.append("<div class='ScrollFormGrid'>");
		buffer.append("<div class='ScrollFormDriverGrid'>");
		buffer.append("<div class='rightCol'>");
		buffer.append("<div class='rightColIn'>");
		buffer.append("<div class='dashboardDV'>");
		buffer.append("<table border=1 class='dashboardDataTable'>");
		buffer.append("<tr><td colspan='6' align='center'><h1>Open Request Details(Broad Cast)</h1></td>" +
				"<td colspan='3' align='center'><font color='blue'><a href='#' onclick=javascript:clearTimeout(TimerID);window.open('control?action=admin&event=dashBoard&event2=expandBR','','top=0,left=0,resizable=no,toolbar=no,addressbar=no,menubar=­no,statu=no,location=yes,fullscreen=yes,scrollbars=yes')><h1>Allocate</h1></a></font></td>" +
				"</tr>");
		buffer.append("<tr>");
		buffer.append("<th width='10%'><B>Trip Id</b></th><th width='10%'><B>Rider Name</b></th><th width='10%'><B>Driver</b></th><th width='10%'><B>Queue</b></th><th width='10%'><B>Status</b></th><th width='10%'><B>Pickup Addr</b></th><th width='10%'><B>Pickup time</b></th><th width='10%'><B>Update</b></th><th></th>");
		buffer.append("</tr>");
		if(al_list!=null && !al_list.isEmpty())
		{
			al_q = UtilityDAO.getQueueForAssoccode(assocode);
			al_d = UtilityDAO.getDriverList(assocode, 2);

			for(int i=0;i<al_list.size();i++)
			{
				OpenRequestBO openRequestBO =(OpenRequestBO)al_list.get(i);

				if(i%2 == 0)
					buffer.append("<tr class='even'>");
				else
					buffer.append("<tr class='odd'>");

				buffer.append("<td><font size='2.5' >"+openRequestBO.getTripid()+"</td><td><font size='2.5' >"+openRequestBO.getName()+"</td>");
				buffer.append("<td>");
				buffer.append("<input type='hidden' id='driver_idB"+i+"' value='"+openRequestBO.getDriverid()+"'>");
				//buffer.append("<select name='driver_idB"+i+"' id='driver_idB"+i+"'>");
				//buffer.append("<option value=''>No Driver</option>");
				for(int j=0;j<al_d.size();j+=2){
					if(al_d.get(j).toString().equalsIgnoreCase(openRequestBO.getDriverid()))
						buffer.append(al_d.get(j+1).toString());
					//buffer.append("<option value='"+al_d.get(j).toString()+"' selected='selected'>"+al_d.get(j+1).toString()+"</option>");
					//	else
					//	buffer.append("<option value='"+al_d.get(j).toString()+"'>"+al_d.get(j+1).toString()+"</option>");
				}
				//buffer.append("</select>");
				buffer.append("</td>");

				buffer.append("<td>");
				buffer.append("<select name='queue_noB"+i+"' id='queue_noB"+i+"'>");
				buffer.append("<option value=''>No Queue</option>");
				for(int j=0;j<al_q.size();j+=2){
					if(al_q.get(j).toString().equalsIgnoreCase(openRequestBO.getQueueno()))
						buffer.append("<option value='"+al_q.get(j).toString()+"' selected='selected'>"+al_q.get(j+1).toString()+"</option>");
					else	
						buffer.append("<option value='"+al_q.get(j).toString()+"'>"+al_q.get(j+1).toString()+"</option>");
				}
				buffer.append("</select>");

				buffer.append("<td><font size='2.5' >"+openRequestBO.getTripStatus()+"</td><td><font size='2.5' >"+openRequestBO.getScity()+"</td>");
				buffer.append("<td><input type='text' id='pickupB"+i+"' value='"+openRequestBO.getRequestTime()+"' size=5></td>");
				buffer.append("<td><div class='btnBlue'><div class='rht'><input type='button' value = 'Update' class='lft' onClick=\"doUpdateBQ('"+i+"','"+openRequestBO.getTripid()+"','"+assocode+"')\"></div></div>");
				//buffer.append("<input type='button' value = 'Allocate' class='button' onClick=\"allocateBQ('"+i+"','"+openRequestBO.getTripid()+"','"+assocode+"')\">");
				buffer.append("</td>");
				buffer.append("<td>" +
						"<select name='close_req'><option value='1'>No show</option><option value='2'>Cust Cancelled</option><option value='3'>Closed Req</option></Select>" +
						"" +
						"<div class='btnBlue'><div class='rht'><input type='button' value='Close' class='lft' onClick=\"deleteOpenBReq('"+openRequestBO.getTripid()+"','"+assocode+"')\"></div></div></td>");


				buffer.append("</tr>");
			}

		}
		buffer.append("</table>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("###");

		return buffer;
	}

	public void allocateOQ(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{ 

		int m_status=0;
		boolean flag = true;
		StringBuffer buffer = new StringBuffer();
		HttpSession session = request.getSession();
		OpenRequestBO m_openRequestBO = null;
		String trip = request.getParameter("triparray");
		String triparray[] = trip.split(",");
		String prev_driverid = "";

		for(int i=0;i<triparray.length;i++)
		{
			//System.out.println(i+":"+triparray[i].toString());
			String tripdetail[] = triparray[i].toString().split(";");


			if(!tripdetail[1].equals("") && !tripdetail[2].equals(""))
			{
				AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

				m_openRequestBO = new OpenRequestBO();
				m_openRequestBO.setTripid(tripdetail[0]);	
				String jobsCode = DispatchDAO.getCompCode(m_openRequestBO.getTripid(), adminBO.getMasterAssociateCode());
				if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
					m_openRequestBO = RequestDAO.getOpenRequestBean(m_openRequestBO, "", "0", adminBO,adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					m_openRequestBO = RequestDAO.getOpenRequestBean(m_openRequestBO, "", "0", adminBO, myString);
				}
//				m_openRequestBO = RequestDAO.getOpenRequestBean(m_openRequestBO, "","1", adminBO);
				if(m_openRequestBO.getIsBeandata() > 0) {
					prev_driverid = m_openRequestBO.getDriverid();
					//System.out.println("In True Condition ");
					m_openRequestBO.setTripid(tripdetail[0]);	
					m_openRequestBO.setDriverid(tripdetail[1]);
					m_openRequestBO.setQueueno(tripdetail[2]);

					m_status =  AdministrationDAO.allocateOPenRequestByOpr(m_openRequestBO.getDriverid(), m_openRequestBO.getQueueno(),m_openRequestBO.getTripid(), adminBO);
					//m_status = ServiceRequestDAO.updateOpenRequestStatus(p_tripid, p_tripStatus, p_driverId);
					if(m_status > 0){
						//ServiceRequestDAO.deleteDriverFromQueue(m_openRequestBO.getDriverid(),m_openRequestBO.getQueueno());
						ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
						OneToOneSMS oneSMS = new OneToOneSMS(m_openRequestBO.getDriverid(), "TDSTDS;FA;"+m_openRequestBO.getTripid()+";" + m_openRequestBO.getSttime()+";"+"" +m_openRequestBO.getDriverid()+";"+System.currentTimeMillis()+";"+m_openRequestBO.getQueueno(),poolBO,1);
						oneSMS.start();

						if(!prev_driverid.equals("") && !prev_driverid.equalsIgnoreCase(m_openRequestBO.getDriverid())){
							oneSMS = new OneToOneSMS(prev_driverid, "TDSTDS;CA;"+m_openRequestBO.getTripid()+";" + m_openRequestBO.getSttime()+";"+"" +m_openRequestBO.getDriverid()+";"+System.currentTimeMillis()+";"+m_openRequestBO.getQueueno(),poolBO,1);
							oneSMS.start();
						}

						AuditDAO.insertauditRequest(((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode(), ((AdminRegistrationBO)session.getAttribute("user")).getUname() , request.getParameter("answer"), "", "", (request.getParameter("type").equalsIgnoreCase("1"))?"201":"202","","");
					} else {			 
						flag = false;
					}			
				} else {
					flag = false;					
				}
			} else {
				flag = false;
			}
		}

		buffer.append(flag?"1":"0");
		buffer.append("###");
		if(request.getParameter("type").equals("1"))
			buffer = getOpnReqinQ(buffer,((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
		else
			buffer = getBroadOpnReq(buffer,((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());

		response.getWriter().write(buffer.toString());

	}
	public void doQupdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		HttpSession session = request.getSession();
		StringBuffer buffer = new StringBuffer();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		//if(request.getParameter("pickup")!=null && request.getParameter("pickup").length() == 7 && TDSValidation.isValid12HrsTime(request.getParameter("pickup").substring(0,5))!=0 && request.getParameter("pickup").substring(5,6).equalsIgnoreCase(" ")){
		if(request.getParameter("pickup")!=null && request.getParameter("pickup").length() == 4 && TDSValidation.is24HrsFormat(request.getParameter("pickup"))){


			int status = AdministrationDAO.updateOPenRequestByOpr(request.getParameter("queue_no"), request.getParameter("pickup"), request.getParameter("trip_id"), request.getParameter("driver_id"), adminBO);

			if(status == 1){
				buffer.append(status+"###");
				if(request.getParameter("type").equalsIgnoreCase("1")){
					buffer = getOpnReqinQ(buffer,adminBO.getAssociateCode());
					AuditDAO.insertauditRequest(request.getParameter("assocode"), adminBO.getUname() , request.getParameter("answer"), "", "", "203","","");
				}else{
					AuditDAO.insertauditRequest(request.getParameter("assocode"), adminBO.getUname() , request.getParameter("answer"), "", "", "204","","");
					buffer = getBroadOpnReq(buffer,adminBO.getAssociateCode());
					buffer = getOpnReqinQ(buffer,adminBO.getAssociateCode());
				}
			} else {
				buffer.append(status+"###"); 
			}
		} else {
			buffer.append(0+"###");
		}
		response.getWriter().write(buffer.toString());

	}
	public void doDriverUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		HttpSession session = request.getSession(false);
		StringBuffer buffer = new StringBuffer();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)session.getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		DriverCabQueueBean cabQueueBean = null;
		boolean closingAJob = false;
		String firstTripOnRoute ="";
		boolean sharedRide = false;
		ArrayList<OpenRequestBO> routeTrips = null;
		boolean restrictionStatus=false;
		int jobStatus = -1;
		String driverID = request.getParameter("driver_id");
		String tripID = request.getParameter("trip_id");
		String pickup = request.getParameter("pickup");
		String cab = request.getParameter("cab");
		String createdBy=request.getParameter("");
		String tripSource=request.getParameter("");
		String prevCab=request.getParameter("prevCab")==null?"":request.getParameter("prevCab");
		boolean makeAVoice =false;

		ArrayList<DriverRestrictionBean> restrictedDrivers=DispatchDAO.readRestrictionForTrip(tripID,adminBO.getMasterAssociateCode());
		if(restrictedDrivers!=null && restrictedDrivers.size()>0){
			for(int i=0;i<restrictedDrivers.size();i++){
				if(restrictedDrivers.get(i).getStatus()==1 && !restrictedDrivers.get(i).getDriverId().equals(driverID)){
					restrictionStatus=true;
				} else if(restrictedDrivers.get(i).getStatus()==2 && restrictedDrivers.get(i).getDriverId().contains(driverID)){
					restrictionStatus=true;
				}
			}
		}

		OpenRequestBO orBo = ServiceRequestDAO.getOpenRequestByTripID(tripID, adminBO, null);
		String companyCode = adminBO.getAssociateCode();
		if(orBo.getAssociateCode().equals(adminBO.getMasterAssociateCode())){
			companyCode = adminBO.getMasterAssociateCode();
		} else if(adminBO.getAssociateCode().equals(adminBO.getMasterAssociateCode())){
			companyCode = orBo.getAssociateCode();
		}
		
		if(driverID.substring(0,1).equalsIgnoreCase("F")){
			jobStatus = TDSConstants.jobAllocated;
			driverID = driverID.substring(1);
			cabQueueBean = DispatchDAO.getDriverByDriverID(companyCode, driverID, 1);
		} else if(driverID.substring(0,1).equalsIgnoreCase("O")){
			if(driverID.substring(1,2).equalsIgnoreCase("P")){
				makeAVoice = true;
				driverID = driverID.substring(2);
				cabQueueBean = DispatchDAO.getDriverByDriverID(companyCode, driverID, 1);
			}else{				
				driverID = driverID.substring(1);
			}
			jobStatus = TDSConstants.performingDispatchProcesses;
		} else if(driverID.equalsIgnoreCase("")){
			jobStatus = TDSConstants.newRequestDispatchProcessesNotStarted;
			driverID = "";
		} else if(driverID.equalsIgnoreCase("NS")){
			jobStatus = TDSConstants.noShowRequest;
			closingAJob = true;
			driverID = "";
			cab="";
		} else if(driverID.equalsIgnoreCase("CC")){
			jobStatus = TDSConstants.customerCancelledRequest;
			closingAJob = true;
			driverID = "";
			cab="";
		} else if(driverID.equalsIgnoreCase("CA")){
			jobStatus = TDSConstants.companyCouldntService;
			closingAJob = true;
			driverID = "";
			cab="";
		} else {
//			jobStatus = TDSConstants.manualAllocation;
			jobStatus = TDSConstants.performingDispatchProcesses;
			cabQueueBean = DispatchDAO.getDriverByDriverID(companyCode, driverID, 1);
		}
		System.out.println("Pre--->"+prevCab+" Cab--->"+orBo.getVehicleNo()+" Driver--->"+orBo.getDriverid());
		if(!orBo.getDriverid().equals(prevCab) && !orBo.getVehicleNo().equals(prevCab)){
			if(driverID.equals(orBo.getDriverid())|| driverID.equals(orBo.getVehicleNo())){
				response.getWriter().write("0;The same driver is already allocated###");
			} else {
				response.getWriter().write("0;Already driver allocated###");
			}
			return;
		}
		if(cabQueueBean==null){
			if(closingAJob){
				ServiceRequestDAO.updateOpenRequestStatus(adminBO.getAssociateCode(),tripID, jobStatus+"", driverID, "",cab,"");
				String jobsCode = DispatchDAO.getCompCode(tripID, adminBO.getMasterAssociateCode());
				if(jobsCode!=null && !jobsCode.equals("") && jobsCode.equals(adminBO.getMasterAssociateCode())){
					DispatchDAO.moveToHistory(tripID, adminBO.getAssociateCode(), "",adminBO.getCompanyList());
				} else {
					ArrayList<String> myString = new ArrayList<String>();
					DispatchDAO.moveToHistory(tripID, adminBO.getAssociateCode(), "",myString);
				}
//				DispatchDAO.moveToHistory(tripID, adminBO.getAssociateCode(),"");
				AuditDAO.insertJobLogs("Operator closing job", tripID,adminBO.getAssociateCode(),jobStatus, adminBO.getUid(),"","",adminBO.getMasterAssociateCode());
				response.getWriter().write("1###");
				return;
			} else{
				response.getWriter().write("0###");
				return;
			}
		}
		if(orBo!=null){
			DriverCabQueueBean cabQueueBo = DispatchDAO.getDriverByDriverID(companyCode, orBo.getDriverid(), 1);
			boolean flag=CheckSpecialFlags.verifyFlags(orBo.getDrProfile(),cabQueueBean.getDrFlag());
			Double acceptDistance=ZoneDAO.getMaxDistance(adminBO.getMasterAssociateCode(),orBo.getQueueno());
			Float distance=calculateDistance(Float.parseFloat(orBo.getSlat()), Float.parseFloat(orBo.getSlong()), Float.parseFloat(cabQueueBean.getCurrentLatitude()+""), Float.parseFloat(cabQueueBean.getCurrentLongitude()+""));
			if(!request.getParameter("driver_id").substring(0,1).equalsIgnoreCase("F")){
				if(distance > acceptDistance){
					response.getWriter().write("0;Driver Is Too Far ("+distance+" Miles Away) From Job###");
					return;
				} if(!flag){
					response.getWriter().write("0;Driver is not satisfying job's speacial request###");
					return;
				}
			}
			String[] message = MessageGenerateJSON.generateMessage(orBo, "OC", System.currentTimeMillis(), adminBO.getAssociateCode());
			if(orBo.getDriverid()!=null&&!orBo.getDriverid().equalsIgnoreCase("")){
				Messaging oneSMS = new Messaging(getServletContext(), cabQueueBo, message, poolBO, adminBO.getAssociateCode());
				oneSMS.start();
			}
		}
		//int status = AdministrationDAO.updateDriverInOpenRequestByOpr(tripID, driverID, adminBO, jobStatus);
		int status = ServiceRequestDAO.updateOpenRequestStatus(adminBO.getAssociateCode(),tripID, jobStatus+"", driverID, "",cab,"");
		if(status==1){
			AuditDAO.insertJobLogs("DasbBoard-Manual Change. Driver ID "+driverID+" & Cab Number"+cab, tripID,adminBO.getAssociateCode(),TDSConstants.manualChange, adminBO.getUid(),"","",adminBO.getMasterAssociateCode());
		}else if(status>1){
			sharedRide = true;
			routeTrips = SharedRideDAO.getOpenRequestForRouteByTripID (tripID, adminBO,"0");
			for(int i =0;i<routeTrips.size();i++){
				AuditDAO.insertJobLogs("DasbBoard-Manual Change. Driver ID "+driverID+" & Cab Number"+cab, routeTrips.get(i).getTripid(),adminBO.getAssociateCode(),TDSConstants.manualChange, adminBO.getUid(),"","",adminBO.getMasterAssociateCode());			
			}	
		}

		if(status >= 1){
			OpenRequestBO openRequestBO = null;
			long msgID = System.currentTimeMillis();
			if(sharedRide && routeTrips.size()>0){
				openRequestBO = routeTrips.get(0);
			} else {
				openRequestBO = ServiceRequestDAO.getOpenRequestByTripID(tripID, adminBO, new ArrayList<String>());
			}
			String[] message = new String[2];
			ArrayList<String> currentQueue = ServiceRequestDAO.checkDriverCurrentlyLogggedInQueue(driverID);
			if(jobStatus == TDSConstants.performingDispatchProcesses || jobStatus == TDSConstants.manualAllocation){
				message = MessageGenerateJSON.generateMessage(openRequestBO, "A", msgID, adminBO.getMasterAssociateCode());
				if(currentQueue!=null && currentQueue.size()>0){
					ServiceRequestDAO.Update_DriverLogOutfromQueue(driverID, cab, currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+driverID+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". On performing dispatch process for trip("+openRequestBO.getTripid()+")");
				}
				ServiceRequestDAO.deleteDriverFromQueue(driverID, adminBO.getMasterAssociateCode());
			} else if ((jobStatus == TDSConstants.jobAllocated)){
				message = MessageGenerateJSON.generateMessageAfterAcceptance(openRequestBO, "FA", msgID, adminBO.getMasterAssociateCode());
				if(currentQueue!=null && currentQueue.size()>0){
					ServiceRequestDAO.Update_DriverLogOutfromQueue(driverID, cab, currentQueue.get(0), adminBO.getAssociateCode(), "2", "Driver("+driverID+") logged out of the zone:"+currentQueue.get(0)+"-"+currentQueue.get(1)+". On force job allocated for trip("+openRequestBO.getTripid()+")");
				}
				ServiceRequestDAO.deleteDriverFromQueue(driverID, adminBO.getMasterAssociateCode());
				//System.out.println("Force Allocating Job & Changing Driver Status To N");
				ServiceRequestDAO.updateDriverAvailability("N", driverID, adminBO.getAssociateCode());
			}
			Messaging oneSMS = new Messaging(getServletContext(),cabQueueBean, message,poolBO,adminBO.getAssociateCode());
			oneSMS.start();
			if(makeAVoice){
				//driverID = driverID.substring(2);
				if(cabQueueBean!=null&& !cabQueueBean.getPhoneNo().equals("")){
					//System.out.println("About to Call DriverID" + driverID);
					String timeZoneOffset =adminBO.getTimeZone();
					String phone =adminBO.getPhonePrefix()+cabQueueBean.getPhoneNo();
					//System.out.println("Phone number is"+phone);
					IVRDriverCaller ivr = new IVRDriverCaller(driverID, tripID, phone, "", "", "", cab,adminBO.getAssociateCode(), timeZoneOffset,adminBO.getMasterAssociateCode(),this);
					ivr.start();
					//System.out.println("calldone");
				}

			}
		}			
		buffer.append(status+"###");
		//if(status >= 1){
		//buffer.append(status+"###");
		//if(request.getParameter("type").equalsIgnoreCase("1")){
		//buffer = getOpnReqinQ(buffer,adminBO.getAssociateCode());
		//AuditDAO.insertauditRequest(adminBO.getAssociateCode(), adminBO.getUname() , request.getParameter("answer"), "", "", "203","","");
		//}else{
		//AuditDAO.insertauditRequest(adminBO.getAssociateCode(), adminBO.getUname() , request.getParameter("answer"), "", "", "204","","");
		//buffer = getBroadOpnReq(buffer,adminBO.getAssociateCode());
		//buffer = getOpnReqinQ(buffer,adminBO.getAssociateCode());
		//}
		//} else {
		//	buffer.append(status+"###"); 
		//}

		response.getWriter().write(buffer.toString());
	}
	public float calculateDistance(float lat1, float lon1, float lat2, float lon2) {
		int earthRadius = 6371;
		float dLat = (float) Math.toRadians(lat2 - lat1);
		float dLon = (float) Math.toRadians(lon2 - lon1);
		float a = (float) (Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(dLon / 2) * Math.sin(dLon / 2));
		float c = (float) (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)));
		float d = earthRadius * c;
		return d;
	}

	public void deleteAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		StringBuffer buffer = new StringBuffer();
		AdministrationDAO.deleteAllOpenRequest(m_adminBO.getAssociateCode());
		buffer = getOldOpnReq(buffer,m_adminBO.getAssociateCode());
		response.getWriter().write(buffer.toString());
	}
	public void deleteOpen(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		StringBuffer buffer = new StringBuffer();
		String reason=request.getParameter("answer");
		AdministrationDAO.updateOpenRequest(request.getParameter("tripid"),request.getParameter("assoccode"),"1","50",reason,m_adminBO.getUid());
		buffer = getOldOpnReq(buffer,m_adminBO.getAssociateCode());
		response.getWriter().write(buffer.toString());
	}
	public void loadCompany(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.getWriter().write(RegistrationDAO.getCopanyName(request.getParameter("vcode"),request.getParameter("assoccode")));
	}
	public void moveDriverinQ(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		StringBuffer buffer = new StringBuffer();
		HttpSession session = request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO)session.getAttribute("user");
		response.setContentType("text/html; charset=ISO-8859-1");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		AdministrationDAO.updateDriverinQ(request.getParameter("driver_id"), m_adminBO);
		String arr[] = new String[1]; 
		arr[0] = request.getParameter("driver_id");
		AuditDAO.insertauditRequest(request.getParameter("assoccode"), ((AdminRegistrationBO)session.getAttribute("user")).getUname() , request.getParameter("answer")+ " moved for the driver" +request.getParameter("driver_id"), "", "", "207","","");
		ArrayList al = UtilityDAO.getDriverNoList(m_adminBO.getAssociateCode(),arr,"0");
		ArrayList<QueueBean> queue= new ArrayList<QueueBean>();
		request.setAttribute("al_list",queue);
		queue = getDriverQueue(buffer,m_adminBO.getAssociateCode(),1,request.getParameter("driver_id"), request, response); 
		response.sendRedirect("AjaxClass?event=getDashBoard&assoccode='"+m_adminBO.getAssociateCode()+"'&stat=1");
	}
	public void removeDriverinQ(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		StringBuffer buffer = new StringBuffer();
		HttpSession session = request.getSession();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		response.setContentType("text/html; charset=ISO-8859-1");
		String driverID = request.getParameter("driver_id")!=null?request.getParameter("driver_id"):"";
		if(!driverID.equals("")){
			String drId=driverID;
			if(adminBO.getDispatchBasedOnVehicleOrDriver()==2){
				drId = RegistrationDAO.getCabNumber("", adminBO.getAssociateCode(), driverID);
			}
			String result = ServiceRequestDAO.getQueueIdByDriverIDandAssoccode(adminBO.getAssociateCode(), drId);
			if(!result.equals("")){
				ServiceRequestDAO.Update_DriverLogOutfromQueue(drId, "", result, adminBO.getAssociateCode(), "2", "Driver("+drId+")forcely logged out of the zone:"+result+" by the Operator:"+adminBO.getUid());
			}
		}
		AdministrationDAO.deleteDriverinQ(request.getParameter("driver_id"), adminBO);
		AuditDAO.insertauditRequest(adminBO.getAssociateCode(),adminBO.getUid(),request.getParameter("driver_id")+" removed by "+ adminBO.getUid() +". Reason: " +request.getParameter("answer"), "", "", "208","","");		ArrayList<QueueBean> queue= new ArrayList<QueueBean>();
		request.setAttribute("al_list",queue);
		queue = getDriverQueue(buffer,adminBO.getAssociateCode(),0,"", request, response); 
		response.sendRedirect("AjaxClass?event=getDashBoard&assoccode='"+adminBO.getAssociateCode()+"'&stat=1");
	}
	public void getDashBoardStartup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		StringBuffer buffer = new StringBuffer();
		ArrayList<QueueBean> queue= new ArrayList<QueueBean>();
		response.setContentType("text/html; charset=ISO-8859-1");
		PrintWriter out = response.getWriter(); 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		if(request.getParameter("stat").equals("1")){
			queue= getDriverQueue(buffer,adminBO.getAssociateCode(),0,"", request, response); 
			request.setAttribute("al_list",queue);
			getServletContext().getRequestDispatcher("/Company/ZoneWaiting.jsp").forward(request, response);
		}
		if(request.getParameter("stat").equals("2")){
			buffer = getOpnReqinQ(buffer,adminBO.getAssociateCode());
			response.getWriter().write(buffer.toString());
			out.write(queue.toString());
		}
		/*if(request.getParameter("stat").equals("3")){
		buffer = getBroadOpnReq(buffer,adminBO.getAssociateCode());
		}*/
		if(request.getParameter("stat").equals("3")){
			//request.setAttribute("al_list",queue);
			ArrayList al_list = null;
			//AdministrationDAO.getOpnReqDash(adminBO.getAssociateCode(),0);
			request.setAttribute("Jobs",al_list);
			getServletContext().getRequestDispatcher("/Company/DispatchViewRam.jsp").forward(request, response);
		}		

	}


	public ArrayList getDriverQueue(StringBuffer buffer, String assocode,int flg,String driver,HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		ArrayList<QueueBean> al_list= new ArrayList<QueueBean>();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		QueueBean queue=new QueueBean();
		al_list.add(queue);
		if(request.getParameter("search")!=null){
			queue.setDriverId(request.getParameter("driverId")==null?"":request.getParameter("driverId"));
			al_list = AdministrationDAO.getDriverinQDash(adminBO.getAssociateCode(),flg,queue,adminBO.getMasterAssociateCode());

		}
		else if(request.getParameter("search")==null){
			//System.out.println("in null");
			al_list = AdministrationDAO.getDriverinQDash(adminBO.getAssociateCode(),flg,queue,adminBO.getMasterAssociateCode());
		}

		return al_list;

	}

	public StringBuffer getOpnReqinQ(StringBuffer buffer, String assocode)
	{

		ArrayList al_list = null;
		ArrayList al_q = null,al_d = null;
		//al_list = AdministrationDAO.getOpnReqDash(assocode,0);
		buffer.append("<div class='ScrollFormGrid'>");
		buffer.append("<div class='ScrollFormDriverGrid'>");
		buffer.append("<div class='rightCol'>");
		buffer.append("<div class='rightColIn'>");
		buffer.append("<div class='dashboardDV'>");
		buffer.append("<table border=1 class='dashboardDataTable'>");
		buffer.append("<tr><td colspan='8' align='center'><h1>Open Request in Queue</h1></td>" +
				"<td colspan='2' align='center'><font color='blue'><a href='#' onclick=javascript:clearTimeout(TimerID);window.open('control?action=admin&event=dashBoard&event2=expandOR','','top=0,left=0,resizable=no,toolbar=no,addressbar=no,menubar=­no,statu=no,location=yes,fullscreen=yes,scrollbars=yes')><h1>Allocate</h1></a></font></td></tr>");
		/*buffer.append("<tr><td colspan='8' align='center'><font color='blue'>Open Request in Queue</font></td>" +
			"<td colspan='2' align='center'><font color='blue'><a href='#' onclick=javascript:clearTimeout(TimerID);window.open('control?action=admin&event=expandOR')>Allocate</a></font></td></tr>");*/
		buffer.append("<tr>");
		buffer.append("<th width='10%'><B>Trip Id</B></th><th width='10%'><B>Rider Name</B></th><th width='10%'><B>Phone No</B></th><th width='10%'><B>Driver id</b></th><th width='10%'><B>Queue</b></th><th width='10%'><B>Status</b></th><th width='10%'><B>Pickup Addr</b></th><th width='10%'><B>Pickup time</b></th><th width='10%'><B>Update</b></th><th></th>");
		buffer.append("</tr>");

		if(al_list!=null && !al_list.isEmpty())
		{
			al_q = UtilityDAO.getQueueForAssoccode(assocode);
			al_d = UtilityDAO.getDriverList(assocode, 2);
			for(int i=0;i<al_list.size();i++)
			{
				OpenRequestBO openRequestBO =(OpenRequestBO)al_list.get(i);
				if(i%2 == 0)
					buffer.append("<tr class='even'>");
				else
					buffer.append("<tr class='odd'>");

				buffer.append("<td><font size='2.5' >"+openRequestBO.getTripid()+"</td><td><font size='2.5' >"+openRequestBO.getName()+"</td><td><font size='2.5' >"+openRequestBO.getPhone()+"</td>");

				buffer.append("<td>");
				buffer.append("<input type='hidden' id='driver_id"+i+"' value='"+openRequestBO.getDriverid()+"'>");
				//buffer.append("<select name='driver_id"+i+"' id='driver_id"+i+"'>");
				//buffer.append("<option value=''>No Driver</option>");
				for(int j=0;j<al_d.size();j+=2){
					if(al_d.get(j).toString().equalsIgnoreCase(openRequestBO.getDriverid()))
						buffer.append(al_d.get(j+1).toString());
					//buffer.append("<option value='"+al_d.get(j).toString()+"' selected='selected'>"+al_d.get(j+1).toString()+"</option>");
					//else
					//buffer.append("<option value='"+al_d.get(j).toString()+"'>"+al_d.get(j+1).toString()+"</option>");
				}
				//buffer.append("</select>");
				buffer.append("</td>");


				buffer.append("<td><font size='2.5' >");
				buffer.append("<select name='queue_no"+i+"' id='queue_no"+i+"'>");
				buffer.append("<option value=''>No Queue</option>");
				for(int j=0;j<al_q.size();j+=2){
					if(al_q.get(j).toString().equalsIgnoreCase(openRequestBO.getQueueno()))
						buffer.append("<option value='"+al_q.get(j).toString()+"' selected='selected'>"+al_q.get(j+1).toString()+"</option>");
					else	
						buffer.append("<option value='"+al_q.get(j).toString()+"'>"+al_q.get(j+1).toString()+"</option>");
				}
				buffer.append("</select>");
				buffer.append("</td>"); 

				//buffer.append(openRequestBO.getQueueno()+"</td>");
				buffer.append("<td><font size='2.5' >"+openRequestBO.getTripStatus()+"</td><td><font size='2.5' >"+openRequestBO.getScity()+"</td>");
				buffer.append("<td><input type='text' id='pickup"+i+"' value='"+openRequestBO.getRequestTime()+"' size=5></td>");
				buffer.append("<td><div class='btnBlue'><div class='rht'><input type='button' value = 'Update' class='lft' onClick=\"doUpdateOQ('"+i+"','"+openRequestBO.getTripid()+"','"+assocode+"')\"></div></div>");
				//buffer.append("<input type='button' value = 'Allocate' class='button' onClick=\"allocateOQ('"+i+"','"+openRequestBO.getTripid()+"','"+assocode+"')\">");
				buffer.append("</td>");
				buffer.append("<td><div class='btnBlue'><div class='rht'><input type='button' value='Close' class='lft' onClick=\"deleteOpenReq('"+openRequestBO.getTripid()+"','"+assocode+"')\"></div></div></td>");

				buffer.append("</tr>");
			}

		}
		buffer.append("</table>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		ArrayList al_list1 = null;
		ArrayList al_q1 = null,al_d1 = null;

		//al_list1 = AdministrationDAO.getOpnReqDash(assocode,1);

		buffer.append("<div class='ScrollFormGrid'>");
		buffer.append("<div class='ScrollFormDriverGrid'>");
		buffer.append("<div class='rightCol'>");
		buffer.append("<div class='rightColIn'>");
		buffer.append("<div class='dashboardDV'>");
		buffer.append("<table border=1 class='dashboardDataTable'>");
		buffer.append("<tr><td colspan='6' align='center'><h1>Open Request Details(Broad Cast)</h1></td>" +
				"<td colspan='3' align='center'><font color='blue'><a href='#' onclick=javascript:clearTimeout(TimerID);window.open('control?action=admin&event=dashBoard&event2=expandBR','','top=0,left=0,resizable=no,toolbar=no,addressbar=no,menubar=­no,statu=no,location=yes,fullscreen=yes,scrollbars=yes')><h1>Allocate</h1></a></font></td>" +
				"</tr>");
		buffer.append("<tr>");
		buffer.append("<th width='10%'><B>Trip Id</b></th><th width='10%'><B>Rider Name</b></th><th width='10%'><B>Driver</b></th><th width='10%'><B>Queue</b></th><th width='10%'><B>Status</b></th><th width='10%'><B>Pickup Addr</b></th><th width='10%'><B>Pickup time</b></th><th width='10%'><B>Update</b></th><th></th>");
		buffer.append("</tr>");
		if(al_list1!=null && !al_list1.isEmpty())
		{
			al_q1 = UtilityDAO.getQueueForAssoccode(assocode);
			al_d1 = UtilityDAO.getDriverList(assocode, 2);

			for(int i=0;i<al_list1.size();i++)
			{
				OpenRequestBO openRequestBO =(OpenRequestBO)al_list1.get(i);

				if(i%2 == 0)
					buffer.append("<tr class='even'>");
				else
					buffer.append("<tr class='odd'>");

				buffer.append("<td><font size='2.5' >"+openRequestBO.getTripid()+"</td><td><font size='2.5' >"+openRequestBO.getName()+"</td>");
				buffer.append("<td>");
				buffer.append("<input type='hidden' id='driver_idB"+i+"' value='"+openRequestBO.getDriverid()+"'>");
				//buffer.append("<select name='driver_idB"+i+"' id='driver_idB"+i+"'>");
				//buffer.append("<option value=''>No Driver</option>");
				for(int j=0;j<al_d1.size();j+=2){
					if(al_d1.get(j).toString().equalsIgnoreCase(openRequestBO.getDriverid()))
						buffer.append(al_d1.get(j+1).toString());
					//buffer.append("<option value='"+al_d.get(j).toString()+"' selected='selected'>"+al_d.get(j+1).toString()+"</option>");
					//	else
					//	buffer.append("<option value='"+al_d.get(j).toString()+"'>"+al_d.get(j+1).toString()+"</option>");
				}
				//buffer.append("</select>");
				buffer.append("</td>");

				buffer.append("<td>");
				buffer.append("<select name='queue_noB"+i+"' id='queue_noB"+i+"'>");
				buffer.append("<option value=''>No Queue</option>");
				for(int j=0;j<al_q1.size();j+=2){
					if(al_q1.get(j).toString().equalsIgnoreCase(openRequestBO.getQueueno()))
						buffer.append("<option value='"+al_q1.get(j).toString()+"' selected='selected'>"+al_q1.get(j+1).toString()+"</option>");
					else	
						buffer.append("<option value='"+al_q1.get(j).toString()+"'>"+al_q1.get(j+1).toString()+"</option>");
				}
				buffer.append("</select>");

				buffer.append("<td><font size='2.5' >"+openRequestBO.getTripStatus()+"</td><td><font size='2.5' >"+openRequestBO.getScity()+"</td>");
				buffer.append("<td><input type='text' id='pickupB"+i+"' value='"+openRequestBO.getRequestTime()+"' size=5></td>");
				buffer.append("<td><div class='btnBlue'><div class='rht'><input type='button' value = 'Update' class='lft' onClick=\"doUpdateBQ('"+i+"','"+openRequestBO.getTripid()+"','"+assocode+"')\"></div></div>");
				//buffer.append("<input type='button' value = 'Allocate' class='button' onClick=\"allocateBQ('"+i+"','"+openRequestBO.getTripid()+"','"+assocode+"')\">");
				buffer.append("</td>");
				buffer.append("<td>" +
						"<select name='close_req'><option value='1'>No show</option><option value='2'>Cust Cancelled</option><option value='3'>Closed Req</option></Select>" +
						"" +
						"<div class='btnBlue'><div class='rht'><input type='button' value='Close' class='lft' onClick=\"deleteOpenBReq('"+openRequestBO.getTripid()+"','"+assocode+"')\"></div></div></td>");


				buffer.append("</tr>");
			}

		}
		buffer.append("</table>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		return buffer;

	}

	public StringBuffer getOldOpnReq(StringBuffer buffer, String assocode)
	{	

		ArrayList al_list = null;
		//al_list = AdministrationDAO.getOpnReqDash(assocode,2);

		buffer.append("<div class='ScrollFormDriverGrid'>");
		buffer.append("<div class='rightCol'>");
		buffer.append("<div class='rightColIn'>");
		buffer.append("<div class='dashboardDV'>");
		buffer.append("<table border=1 class='dashboardDataTable'>");
		buffer.append("<tr><td colspan='8' align='center'><font color='blue'>Other Open Request </font></td></tr>");
		buffer.append("<tr>");
		buffer.append("<th width='10%'><B>Trip Id</B><th><th width='10%'><B>Rider Name</B></th><th width='10%'><B>Phone No</B></th><th width='10%'><B>Driver id</b></th><th width='10%'><B>Queue</b></th><th width='10%'><B>Status</b></th><th width='10%'><B>Pickup Addr</b></th><th width='10%'><B>Pickup time</b></th><th><div class='btnBlue'><div class='rht'><input type='button' value='Close All' class='lft' onClick=\"deleteAllReq('"+assocode+"')\"></div></div></th>");
		buffer.append("</tr>");
		if(al_list!=null && !al_list.isEmpty())
		{
			for(int i=0;i<al_list.size();i++)
			{
				OpenRequestBO openRequestBO =(OpenRequestBO)al_list.get(i);

				if(i%2 == 0)
					buffer.append("<tr class='even'>");
				else
					buffer.append("<tr class='odd'>");
				buffer.append("<td><font size='2.5' >"+openRequestBO.getTripid()+"</td><td><font size='2.5' >"+openRequestBO.getName()+"</td><td><font size='2.5' >"+openRequestBO.getPhone()+"</td><td><font size='2.5' >"+openRequestBO.getDriverUName()+"</td><td><font size='2.5' >"+openRequestBO.getQueueno()+"</td><td><font size='2.5' >"+openRequestBO.getTripStatus()+"</td><td><font size='2.5' >"+openRequestBO.getScity()+"</td><td><font size='2.5' >"+openRequestBO.getRequestTime()+"</td><td>" +
						"<select name='close_req'><option value='1'>No show</option><option value='2'>Cust Cancelled</option><option value='3'>Closed Req</option></Select>" +
						"" +
						"<div class='btnBlue'><div class='rht'><input type='button' value='Close' class='button' onClick=\"deleteReq('"+openRequestBO.getTripid()+"','"+assocode+"')\"></div></div></td>");
				buffer.append("</tr>");
			}
		}
		buffer.append("</table>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");
		buffer.append("</div>");		
		buffer.append("###");

		return buffer;
	}


	public void loadQueueValues(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		//System.out.println("LOAD QUE VALUE AJAX");
		HttpSession session = request.getSession();
		StringBuffer buffer = new StringBuffer();


		ArrayList al_q = new ArrayList();
		/*if(request.getParameter("Slat")!=null &&  request.getParameter("Slong") != null && request.getParameter("Slat")!="" &&  request.getParameter("Slong") != "" ){
			al_q = ServiceRequestDAO.getQueueName(request.getParameter("Slat"), request.getParameter("Slong"),(AdminRegistrationBO)session.getAttribute("user"));
			buffer.append("Zone Found###");
		}else{*/
		String dcodeXML=request.getParameter("dcodeXML");
		//System.out.println(dcodeXML);
		//String latlong =  MapParser.getLatLongForAddr(request.getParameter("sadd1"), request.getParameter("sadd2"), request.getParameter("sstate"), request.getParameter("scity"), request.getParameter("szip"));
		String latlong =  MapParser.getLatLongForAddr(dcodeXML);
		//System.out.println("lllllllllllllllllllllllllaaaaaaaaaaaaaaaaaaaaa"+latlong);
		String[] latlng = latlong.split("^");

		if(latlng.length  == 2)
			al_q = ServiceRequestDAO.getQueueName(latlng[0], latlng[1],(AdminRegistrationBO)session.getAttribute("user"));
		else
			al_q = ServiceRequestDAO.getQueueName("0", "0",(AdminRegistrationBO)session.getAttribute("user"));

		buffer.append((latlng.length == 2?"Zone Found":"Zone Not Found")+"###");
		//}

		buffer.append("<select name='queueno' id='queueno'>	<option value=''>Select----</option>");
		buffer.append("<option value=''>No Queue</option>");
		for(int i=0;i<al_q.size();i=i+2){
			buffer.append("<option value="+al_q.get(i).toString()+">"+al_q.get(i+1).toString()+"</option>");
		}
		buffer.append("</select>");
		response.getWriter().write(buffer.toString());
	}

	public void getFromToAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		StringBuffer buffer = new StringBuffer();

		HashMap hmp_phone;
		if(TDSProperties.getValue("checkserviceAPI").equalsIgnoreCase("false")) { 
			hmp_phone = CheckUserInformation.checkPhoneNo(request.getParameter("phone"));
		} else {
			hmp_phone = CheckUserInformation.checkPhoneByAPI(request.getParameter("phone"));
		}
		//System.out.println("Data for a phone "+hmp_phone);
		//System.out.println("Phone no "+request.getParameter("phone"));
		//ArrayList al_list = RequestDAO.getAddrForPhone(request.getParameter("phone"));

		buffer.append(""+hmp_phone.get("address")+"###");
		buffer.append(""+hmp_phone.get("city")+"###");
		buffer.append(""+hmp_phone.get("state")+"###");
		buffer.append(""+hmp_phone.get("zip")+"###");
		buffer.append(""+hmp_phone.get("latitude")+"###");
		buffer.append(""+hmp_phone.get("longitude")+"###");

		/*if(al_list!=null && !al_list.isEmpty())
		{
			buffer.append("<table id='bodypage' border=1>");
			buffer.append("<tr>");
			buffer.append("<td colspan='6' align='center'>From</td><td colspan='6' align='center'>To</td>");
			buffer.append("</tr>");
			buffer.append("<tr bgcolor='#A5A5A5'>");
			buffer.append("<td>Address1</td><td>Address2</td><td>City</td><td>State</td><td>Zip</td><td></td>");
			buffer.append("<td>Address1</td><td>Address2</td><td>City</td><td>State</td><td>Zip</td><td></td>");
			buffer.append("</tr>");
			for(int i=0;i<al_list.size();i++)
			{

				HashMap  hmp_userData =  (HashMap)al_list.get(i);
				buffer.append("<tr>");
				buffer.append("<td><input type='hidden' name='stadd1_"+i+"' id='stadd1_"+i+"' value='"+hmp_userData.get("stadd1")+"'>"+hmp_userData.get("stadd1")+"</td>" +
						"<td><input type='hidden' name='stadd2_"+i+"' id='stadd2_"+i+"' value='"+hmp_userData.get("stadd2")+"'>"+hmp_userData.get("stadd2")+"</td>" +
						"<td><input type='hidden' name='stcity"+i+"' id='stcity"+i+"' value='"+hmp_userData.get("stcity")+"'>"+hmp_userData.get("stcity")+"</td>" +
						"<td><input type='hidden' name='ststate"+i+"' id='ststate"+i+"' value='"+hmp_userData.get("ststate")+"'>"+hmp_userData.get("ststate")+"</td>" +
						"<td><input type='hidden' name='stzip"+i+"' id='stzip"+i+"' value='"+hmp_userData.get("stzip")+"'>"+hmp_userData.get("stzip")+"</td>" +
						"<td><input type='button' value='From' onclick=setFromAddress('from',"+i+")><input type='button' value='To' onclick=setFromAddress('to',"+i+")></td>");
				buffer.append("<td><input type='hidden' name='edadd1_"+i+"' id='edadd1_"+i+"' value='"+hmp_userData.get("edadd1")+"'>"+hmp_userData.get("edadd1")+"</td>" +
						"<td><input type='hidden' name='edadd2_"+i+"' id='edadd2_"+i+"' value='"+hmp_userData.get("edadd2")+"'>"+hmp_userData.get("edadd2")+"</td>" +
						"<td><input type='hidden' name='edcity"+i+"' id='edcity"+i+"' value='"+hmp_userData.get("edcity")+"'>"+hmp_userData.get("edcity")+"</td>" +
						"<td><input type='hidden' name='edstate"+i+"' id='edstate"+i+"' value='"+hmp_userData.get("edstate")+"'>"+hmp_userData.get("edstate")+"</td>" +
						"<td><input type='hidden' name='edzip"+i+"' id='edzip"+i+"' value='"+hmp_userData.get("edzip")+"'>"+hmp_userData.get("edzip")+"</td>" +
						"<td><input type='button' value='From' onclick=setToAddress('from',"+i+")><input type='button' value='To' onclick=setToAddress('to',"+i+")></td>");
				buffer.append("</tr>");
			}
			buffer.append("</table>");
		}*/

		//System.out.println(buffer.toString());
		response.getWriter().write(buffer.toString());

	}
	public void loadOther(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		StringBuffer buffer = new StringBuffer();
		buffer.append(RequestDAO.getUserInformation(request.getParameter("phone"), request.getParameter("add1"), request.getParameter("add2"), request.getParameter("state"), request.getParameter("city"), request.getParameter("zip")));
		//System.out.println(buffer.toString());
		response.getWriter().write(buffer.toString());
	}
	private void deleteCabMappingDetails(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException,Exception 
	{
		PrintWriter out = response.getWriter();
		CabDriverMappingBean driverMappingBean = new CabDriverMappingBean();
		String userKey=request.getParameter("userkey");
		int returnValue = RegistrationDAO.deleteCabMappingDetails(userKey,((AdminRegistrationBO)request.getSession().getAttribute("user")).getAssociateCode());		
		if(returnValue == 1){
			response.getWriter().write("status=yes");
		} else{
			response.getWriter().write("status=no");
		}

	}
	public void deleteAddress(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{ 	
		HttpSession session =request.getSession(); 
		String userKey = request.getParameter("userKey");
		String masterKey = request.getParameter("masterKey");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		boolean Result=RegistrationDAO.deleteAddress(adminBO.getAssociateCode(),userKey,masterKey);
		if(adminBO.getORFormat()==1){
			getServletContext().getRequestDispatcher(TDSConstants.openRequestJSP01).forward(request, response);
		} else if(adminBO.getORFormat()==2){
			getServletContext().getRequestDispatcher(TDSConstants.openRequestJSP02).forward(request, response);
		}
		//}
	}	public void getZoneName (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		StringBuffer buffer = new StringBuffer();
		String zoneNum=request.getParameter("zoneNum");
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String zoneName=SystemPropertiesDAO.getZoneName(zoneNum, adminBO);
		buffer.append(""+zoneName+"###");
		response.getWriter().write(buffer.toString());
	}
	public void callerId(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{ 
		JSONArray array = new JSONArray();
		//StringBuffer buffer=new StringBuffer();
		//HttpSession session =request.getSession(); 
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		//long timeStart = System.currentTimeMillis();
		ArrayList<CallerIDBean> al_list = CallerIDDAO.getCallerId(adminBO);
		for(int i=0;i<al_list.size();i++){
			try {
				JSONObject address = new JSONObject();
				address.put("L", al_list.get(i).getLineNo()==null?"":al_list.get(i).getLineNo());
				address.put("N", al_list.get(i).getName()==null?"":al_list.get(i).getName());
				address.put("P", al_list.get(i).getPhoneNo()==null?"":al_list.get(i).getPhoneNo().replaceAll("[\\s\\-()]",""));
				address.put("D", al_list.get(i).getDate()==null?"":al_list.get(i).getDate());
				address.put("T", al_list.get(i).getTime()==null?"":al_list.get(i).getTime());
				address.put("A", al_list.get(i).getAcceptedBy()==null?"":al_list.get(i).getAcceptedBy());
				address.put("S", al_list.get(i).getStatus());
				address.put("AD", al_list.get(i).getAcceptedDate()==null?"":al_list.get(i).getAcceptedDate());
				address.put("AT", al_list.get(i).getAcceptedTime()==null?"":al_list.get(i).getAcceptedTime());
				address.put("TD", al_list.get(i).getDiffence()==null?"":al_list.get(i).getDiffence());
				array.put(address);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}
	public void ciHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{ 
		JSONArray array = new JSONArray();
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ArrayList<CallerIDBean> al_list = CallerIDDAO.getCallerIdHistory(adminBO);
		for(int i=0;i<al_list.size();i++){
			try {
				JSONObject ciHistory = new JSONObject();
				ciHistory.put("L", al_list.get(i).getLineNo()==null?"":al_list.get(i).getLineNo());
				ciHistory.put("PaN", al_list.get(i).getName()==null?"":al_list.get(i).getName());
				ciHistory.put("PhN", al_list.get(i).getPhoneNo()==null?"":al_list.get(i).getPhoneNo().replaceAll("[\\s\\-()]",""));
				ciHistory.put("D", al_list.get(i).getDate()==null?"":al_list.get(i).getDate());
				ciHistory.put("T", al_list.get(i).getTime()==null?"":al_list.get(i).getTime());
				array.put(ciHistory);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String responseString = array.toString();
		response.getWriter().write(responseString.toString());
	}

	//	public void getComments(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	//	{ 
	//		StringBuffer buffer=new StringBuffer();
	//		HttpSession session =request.getSession(); 
	//		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
	//		String account=request.getParameter("account");
	//		ArrayList<OpenRequestBO> al_list = RequestDAO.getComments(adminBO,account);
	//		for(int i=0;i<al_list.size();i++){
	//			buffer.append("Comment="+al_list.get(i).getComments()+";Name="+al_list.get(i).getName()+";MasterKey="+al_list.get(i).getMasterAddressKey()+";Pay Type="+al_list.get(i).getPaytype()+"###");
	//		}
	//		response.getWriter().write(buffer.toString());
	//	}
}
