package com.tds.controller;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.tds.dao.ServiceRequestDAO;
import com.tds.dao.SystemPropertiesDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.QueueCoordinatesBO;
import com.common.util.TDSValidation;

public class TDSPopup extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//private Category cat = TDSController.cat;

	@Override
	public void doGet(HttpServletRequest p_request, HttpServletResponse p_response)throws ServletException,IOException {
		doProcess(p_request, p_response);
	}

	@Override
	public void doPost(HttpServletRequest p_request, HttpServletResponse p_response)throws ServletException,IOException {
		doProcess(p_request, p_response);
	}

	public void doProcess(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException{
		//	cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//	cat.info("doProcess method");
		//	cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

		//	cat.info("Input Param "+p_request.getParameter("aircode"));
		if(p_request.getParameter("aircode") != null) {
			//System.out.println("Call Air Code Method");
			getAirPortCode(p_request, p_response, p_request.getParameter("aircode"));
		} else if(p_request.getParameter("driver") != null) {
			getDriverName(p_request, p_response, p_request.getParameter("driver"));
		} else if(p_request.getParameter("queueDes") != null) {
			getQueueId(p_request, p_response);
		} else if(p_request.getParameter("landmark")!=null) {
			getlandMark(p_request,p_response,p_request.getParameter("landmark"));
		}
		/*
		 * The following constraints are used in the following conditions
		 *  0 -- Address1
		 *  1 -- Address2
		 *  2 -- City
		 *  3 -- State 
		 *  4 -- Zip
		 */		
		else if(p_request.getParameter("phoneaddress1") != null) {
			//System.out.println("!!!!!!!!!!!!!!!!!!!"+p_request.getParameter("phone"));
			getUserInformation(p_request, p_response, p_request.getParameter("phoneaddress1"),p_request.getParameter("phone"), 0);
		} else if(p_request.getParameter("phoneaddress2") != null) {
			//System.out.println("!!!!!!!!!!!!!!!!!!!"+p_request.getParameter("phone"));
			getUserInformation(p_request, p_response, p_request.getParameter("phoneaddress2"),p_request.getParameter("phone"), 1);
		} else if(p_request.getParameter("phonecity") != null) {
			//System.out.println("!!!!!!!!!!!!!!!!!!!"+p_request.getParameter("phone"));
			getUserInformation(p_request, p_response, p_request.getParameter("phonecity"),p_request.getParameter("phone"), 2);
		} else if(p_request.getParameter("phonestate") != null) {
			//System.out.println("!!!!!!!!!!!!!!!!!!!"+p_request.getParameter("phone"));
			getUserInformation(p_request, p_response, p_request.getParameter("phonestate"),p_request.getParameter("phone"), 3);
		} else if(p_request.getParameter("phonezip") != null) {
			//System.out.println("!!!!!!!!!!!!!!!!!!!"+p_request.getParameter("phone"));
			getUserInformation(p_request, p_response, p_request.getParameter("phonezip"),p_request.getParameter("phone"), 4);
		}else if(p_request.getParameter("vcostcenter") != null) {
			getvcostcenter(p_request, p_response, p_request.getParameter("vcostcenter"));
		}else if(p_request.getParameter("DRIVERID") != null) {
			//System.out.println("!!!!!!!!!!!!!!!!!!!"+p_request.getParameter("DRIVERID"));
			getDriverIDName(p_request, p_response, p_request.getParameter("DRIVERID"));
		}else if(p_request.getParameter("getAccountList") != null) {
			getAccountList(p_request, p_response, p_request.getParameter("getAccountList"));
		}else if(p_request.getParameter("cabNum") != null) {
			getCabNum(p_request, p_response, p_request.getParameter("cabNum"));
		}else if(p_request.getParameter("UserName") != null) {
			UserName(p_request, p_response, p_request.getParameter("UserName"));
		}else if(p_request.getParameter("employeeNames") != null) {
			getEmployeeNames(p_request, p_response, p_request.getParameter("employeeNames"));
		}else if(p_request.getParameter("getCostCenter") != null) {
			getCostCenter(p_request, p_response, p_request.getParameter("getCostCenter"));
		}else if(p_request.getParameter("ZONES") != null) {
			//System.out.println("!!!!!!!!!!!!!!!!!!!"+p_request.getParameter("DRIVERID"));
			getZones(p_request, p_response, p_request.getParameter("ZONES"));
		}

	}
	public void getDriverIDName(HttpServletRequest p_request, HttpServletResponse p_response, String driver) throws ServletException,IOException {


		HttpSession session = p_request.getSession();
		AdminRegistrationBO m_adminBO = (AdminRegistrationBO)session.getAttribute("user");

		List al_driver = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0; 

		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		al_driver =  UtilityDAO.getDriverList(m_adminBO.getAssociateCode(),driver.trim(),1);

		if(al_driver != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < al_driver.size()  ; m_counter+=2) {
				m_xmlData.append("<item value='"+al_driver.get(m_counter)+"'>"+al_driver.get(m_counter)+"("+al_driver.get(m_counter+1)+")"+"</item>");
			}
			m_xmlData.append("</list>");
		}
		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());


	}

	public void getvcostcenter(HttpServletRequest p_request, HttpServletResponse p_response, String vcostcenter) throws ServletException,IOException {


		HttpSession session = p_request.getSession();

		List al_voucher = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0;

		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		if(vcostcenter.length() > 0) {
			al_voucher = ServiceRequestDAO.getVCompany(vcostcenter,((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
			//System.out.println("Data from DAO "+m_airPortList);
		}
		if(al_voucher != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < al_voucher.size()  ; m_counter++) {
				m_xmlData.append("<item value='"+al_voucher.get(m_counter)+"'>"+al_voucher.get(m_counter)+"("+al_voucher.get(m_counter)+")"+"</item>");
			}
			m_xmlData.append("</list>");
		}
		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());


	}
	public void getAccountList(HttpServletRequest p_request, HttpServletResponse p_response, String getAccountListfor) throws ServletException,IOException {


		HttpSession session = p_request.getSession();

		List al_voucher = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0;

		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		if(getAccountListfor.length() > 0) {
			al_voucher = ServiceRequestDAO.getAccountList(getAccountListfor,((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode());
			//System.out.println("Data from DAO "+m_airvcostcenterPortList);
		}

		if(al_voucher != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < al_voucher.size()  ;m_counter+=2) {
				m_xmlData.append("<item value='"+al_voucher.get(m_counter)+"'>"+al_voucher.get(m_counter)+"("+al_voucher.get(m_counter+1)+")"+"</item>");
			}
			m_xmlData.append("</list>");
		}

		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());


	}public void UserName(HttpServletRequest p_request, HttpServletResponse p_response, String getUserName) throws ServletException,IOException {

		HttpSession session = p_request.getSession();

		List al_voucher = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0;

		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		if(getUserName.length() > 0) {
			al_voucher = ServiceRequestDAO.getUserName(getUserName,((AdminRegistrationBO)session.getAttribute("user")).getMasterAssociateCode());
			//System.out.println("Data from DAO "+m_airvcostcenterPortList);
		}
		if(al_voucher != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < al_voucher.size()  ;m_counter++) {
				m_xmlData.append("<item value='"+al_voucher.get(m_counter)+"'>"+al_voucher.get(m_counter)+"</item>");
			}
			m_xmlData.append("</list>");
		}
		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());


	}public void getCabNum(HttpServletRequest p_request, HttpServletResponse p_response, String getCabNumList) throws ServletException,IOException {

		HttpSession session = p_request.getSession();

		List al_voucher = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0;

		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		if(getCabNumList.length() > 0) {
			al_voucher = ServiceRequestDAO.getCabNum(getCabNumList,((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
			//System.out.println("Data from DAO "+m_airvcostcenterPortList);
		}
		if(al_voucher != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < al_voucher.size()  ;m_counter++) {
				m_xmlData.append("<item value='"+al_voucher.get(m_counter)+"'>"+al_voucher.get(m_counter)+"</item>");
			}
			m_xmlData.append("</list>");
		}
		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());


	}

	public void getlandMark(HttpServletRequest request,HttpServletResponse response,String landMark)  throws ServletException, IOException{
		List m_airPortList = null;
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0;
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/xml");
		response.setHeader("Cache-Control", "no-cache");
		if(landMark.length() > 0) {
			String[] landMarkwithoutSpace=landMark.split(" ");
			if(landMarkwithoutSpace.length>0){
				String landmarkFirstvalue=landMarkwithoutSpace[0];
				String landmarkSecondValue="";
				if(landMarkwithoutSpace.length>1){
					landmarkSecondValue=landMarkwithoutSpace[1];
				}
				m_airPortList = ServiceRequestDAO.getLandMark(landmarkFirstvalue,landmarkSecondValue, adminBO.getMasterAssociateCode());
				//cat.debug("Air port code List");
				//System.out.println("Data from DAO "+m_airPortList);
			}
		}
		if(m_airPortList != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < m_airPortList.size()  ; m_counter+=2) {
				m_xmlData.append("<item value='"+m_airPortList.get(m_counter)+"'>"+m_airPortList.get(m_counter)+"("+m_airPortList.get(m_counter+1)+")</item>");
			}
			m_xmlData.append("</list>");
		}else{
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			m_xmlData.append("</list>");
		}
		response.getWriter().write(m_xmlData.toString());
	}



	public void getAirPortCode(HttpServletRequest p_request, HttpServletResponse p_response, String p_airCode) throws ServletException,IOException {
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//cat.info("getAirPortCode method");
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//System.out.println("Input Param "+p_request.getParameter("aircode"));
		List m_airPortList = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0;
		//System.out.println("Inside Air Code Method");
		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		if(p_airCode.length() > 0) {
			m_airPortList = ServiceRequestDAO.getAirPortCode(p_airCode);
			//cat.debug("Air port code List");
			//System.out.println("Data from DAO "+m_airPortList);
		}
		if(m_airPortList != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < m_airPortList.size()  ; m_counter += 2) {
				m_xmlData.append("<item value='"+m_airPortList.get(m_counter)+"'>"+m_airPortList.get(m_counter+1)+"("+m_airPortList.get(m_counter)+")"+"</item>");
			}
			m_xmlData.append("</list>");
		}
		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());
		//p_response.getWriter().close();
	}

	public void getDriverName(HttpServletRequest p_request, HttpServletResponse p_response, String p_driverId) throws ServletException,IOException {
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//cat.info("getDriverName method");
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		List m_driverList = null;
		StringBuffer m_driverDetail = new StringBuffer();
		HttpSession session = p_request.getSession();

		int m_counter = 0;
		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		if(p_driverId.length() > 0) {
			AdminRegistrationBO m_adminBO = (AdminRegistrationBO) session.getAttribute("user");
			m_driverList = ServiceRequestDAO.getDriverName(p_driverId,m_adminBO.getAssociateCode());
			//System.out.println("Data from DAO "+m_airPortList);
		}
		if(m_driverList != null ) {
			m_driverDetail.append("<?xml version=\"1.0\"?>");
			m_driverDetail.append("<list>");
			for(m_counter = 0; m_counter < m_driverList.size()  ; m_counter ++) {
				m_driverDetail.append("<item value='"+m_driverList.get(m_counter)+"'>"+m_driverList.get(m_counter)+"</item>");
			}
			m_driverDetail.append("</list>");
		}
		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_driverDetail.toString());
	}

	public void getQueueId(HttpServletRequest p_request, HttpServletResponse p_response) throws ServletException,IOException {
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//cat.info("getQueueId method");
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		HttpSession m_session = p_request.getSession();
		AdminRegistrationBO m_adminBO;
		QueueCoordinatesBO m_queueBO;
		StringBuffer m_queueDetail = new StringBuffer();
		List m_queueList = null;
		String m_assocCode = "",m_queueDescription = "";
		if(m_session.getAttribute("user") != null) {
			m_adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
			m_assocCode = m_adminBO.getAssociateCode();
		}
		if(p_request.getParameter("queueDes") != null) {
			m_queueDescription = p_request.getParameter("queueDes");
		}
		if(m_assocCode.length() > 0 && m_queueDescription.length() > 0) {
			m_queueList = SystemPropertiesDAO.getQueueCoOrdinateList("", m_assocCode,m_queueDescription);
			//System.out.println("Data in Queue List "+m_queueList);
		}
		if(m_queueList != null) {
			m_queueDetail.append("<?xml version=\"1.0\"?>");
			m_queueDetail.append("<list>");
			//System.out.println("Data in Queue List "+m_queueList);
			for (Iterator m_queueIter = m_queueList.iterator(); m_queueIter.hasNext();) {
				m_queueBO = (QueueCoordinatesBO) m_queueIter.next();
				m_queueDetail.append("<item value='"+m_queueBO.getQueueId()+"'>"+m_queueBO.getQDescription()+"("+m_queueBO.getQueueId()+")"+"</item>");
			}
			m_queueDetail.append("</list>");
		}
		//cat.info("Data in Queue "+m_queueDetail);

		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		p_response.getWriter().write(m_queueDetail.toString());
	}


	public void getUserInformation(HttpServletRequest p_request, HttpServletResponse p_response,String p_condition,String p_phoneno, int p_constraint) throws ServletException, IOException {
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		//cat.info("getCity method");
		//cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		StringBuffer m_userDetail = new StringBuffer(); 
		List m_userList = null;
		String m_data = "";
		if(p_condition.length() > 0) {
			m_userList = ServiceRequestDAO.getUserInformation(p_condition,TDSValidation.getDBPhoneFormat(p_phoneno), p_constraint);
		}
		if(m_userList != null) {
			m_userDetail.append("<?xml version=\"1.0\"?>");
			m_userDetail.append("<list>");
			for(Iterator m_userIter = m_userList.iterator(); m_userIter.hasNext() ;) {
				m_data = (String)m_userIter.next();
				m_userDetail.append("<item value='"+m_data+"'>"+m_data+"</item>");
			}	
			m_userDetail.append("</list>");
		}
		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		p_response.getWriter().write(m_userDetail.toString());
	}
	public void getEmployeeNames(HttpServletRequest p_request, HttpServletResponse p_response, String driver) throws ServletException,IOException {

		AdminRegistrationBO m_adminBO = (AdminRegistrationBO)p_request.getSession().getAttribute("user");
		List al_driver = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0; 
		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		al_driver =  UtilityDAO.getEmployeeNames(m_adminBO.getAssociateCode(),driver.trim(),1);
		if(al_driver != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < al_driver.size()  ; m_counter+=2) {
				m_xmlData.append("<item value='"+al_driver.get(m_counter)+"'>"+al_driver.get(m_counter)+"("+al_driver.get(m_counter+1)+")"+"</item>");
			}
			m_xmlData.append("</list>");
		}
		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());


	}
	public void getCostCenter(HttpServletRequest p_request, HttpServletResponse p_response, String getCostCenterfor) throws ServletException,IOException {


		HttpSession session = p_request.getSession();

		List al_voucher = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0;

		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		if(getCostCenterfor.length() > 0) {
			al_voucher = ServiceRequestDAO.getCostCenter(getCostCenterfor,((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
			//System.out.println("Data from DAO "+m_airvcostcenterPortList);
		}

		if(al_voucher != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < al_voucher.size()  ;m_counter+=2) {
				m_xmlData.append("<item value='"+al_voucher.get(m_counter)+"'>"+al_voucher.get(m_counter)+"</item>");
			}
			m_xmlData.append("</list>");
		}

		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());


	}
	public void getZones(HttpServletRequest p_request, HttpServletResponse p_response, String getZones) throws ServletException,IOException {


		HttpSession session = p_request.getSession();

		List al_zones = null;
		StringBuffer m_xmlData = new StringBuffer();
		int m_counter = 0;

		p_request.setCharacterEncoding("UTF-8");
		p_response.setContentType("text/xml");
		p_response.setHeader("Cache-Control", "no-cache");
		if(getZones.length() > 0) {
			al_zones = ServiceRequestDAO.getZoneList(getZones,((AdminRegistrationBO)session.getAttribute("user")).getAssociateCode());
			//System.out.println("Data from DAO "+m_airvcostcenterPortList);
		}

		if(al_zones != null ) {
			m_xmlData.append("<?xml version=\"1.0\"?>");
			m_xmlData.append("<list>");
			for(m_counter = 0; m_counter < al_zones.size()  ;m_counter+=2) {
				m_xmlData.append("<item value='"+al_zones.get(m_counter)+"'>"+al_zones.get(m_counter)+"("+al_zones.get(m_counter+1)+")"+"</item>");
			}
			m_xmlData.append("</list>");
		}

		//System.out.println("Data in XML "+m_xmlData.toString());
		p_response.getWriter().write(m_xmlData.toString());


	}


}

