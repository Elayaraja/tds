package com.tds.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.ZoneDAO;

/**
 * Servlet implementation class TestServlet
 */
public class LoadZonesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoadZonesServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		//System.out.println("Z-" +System.currentTimeMillis());

		//System.out.println(" Zone Loading ");
		ArrayList<String> zonesInApplication = ZoneDAO.readAllZones();
		for(int i=1;i<zonesInApplication.size(); i++){
			ArrayList<ZoneTableBeanSP> zonesForAssocCode = ZoneDAO.readZonesBoundries(zonesInApplication.get(i), "", true);
			getServletConfig().getServletContext().setAttribute(zonesInApplication.get(i) + "ZONES", zonesForAssocCode);
		}


	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
