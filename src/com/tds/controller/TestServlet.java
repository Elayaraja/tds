package com.tds.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nsidc.spheres.Point;

import org.asteriskjava.manager.ManagerConnection;
import org.codehaus.jackson.map.util.Comparators;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.headissue.asterisk.jtapi.gjtapi.AsteriskCall;
import com.tds.cmp.bean.DriverCabQueueBean;
import com.tds.cmp.bean.ZoneTableBeanSP;
import com.tds.dao.DispatchDAO;
import com.tds.dao.ReportDAO;
import com.tds.dao.UtilityDAO;
import com.tds.dao.VoiceMessageDAO;
import com.tds.dao.ZoneDAO;
import com.tds.ivr.IVRDriverCaller;
import com.tds.ivr.IVRPassengerCallerOutBound;
import com.tds.process.Email;
import com.tds.process.FCMPush;
import com.common.util.Messaging;
import com.tds.tdsBO.AdminRegistrationBO;
import com.tds.tdsBO.ApplicationPoolBO;
import com.tds.tdsBO.OpenRequestBO;
import com.common.util.MessageGenerateJSON;
import com.common.util.TDSConstants;
import com.common.util.TDSValidation;

/**
 * Servlet implementation class TestServlet
 */
public class TestServlet extends HttpServlet {
	boolean holdTheServlet= true;
	private static final long serialVersionUID = 1L;

	private ManagerConnection managerConnection;


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TestServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		doProcess(request, response);
	}
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException 
	{
		//System.out.println("Being Posted to test server");
		String eventParam = "";

		//		if(request.getSession(false) != null){
		//			if(request.getSession().getAttribute("user")==null){
		//				return;
		//			}
		//		} else {
		//			return;
		//		}
		//		
		if(request.getParameter(TDSConstants.eventParam) != null) {
			eventParam = request.getParameter(TDSConstants.eventParam);
		}

		System.out.println("Event In Test Servlet--->"+ eventParam);

		HttpSession session = request.getSession();

		//if(session.getAttribute("user") != null) {

		if(eventParam.equalsIgnoreCase("UploadVoice")) {			 
			uploadVoiceFromOperator(request,response);
		} else if(eventParam.equalsIgnoreCase("DownloadVoice")) {			 
			downloadVoice(request,response);
		}  else if(eventParam.equalsIgnoreCase("UploadVoiceDriver")) {			 
			uploadVoiceFromDriver(request,response);
		}  else if(eventParam.equalsIgnoreCase("getMessageIDToBePlayed")) {			 
			getMessageIDToBePlayed(request,response);
		} else if(eventParam.equalsIgnoreCase("PlayMessage")){
			getMessage(request,response);
		} else if(eventParam.equalsIgnoreCase("ShowObjects")){
			showObjects(request,response);
		} else if(eventParam.equalsIgnoreCase("makeivrphone")){
			makeIVRPhoneCall(request,response);
		} else if(eventParam.equalsIgnoreCase("gcm")){
			gcm(request,response);
		} else if(eventParam.equalsIgnoreCase("ivrPassenger")){
			ivrPassenger(request,response);
		} else if(eventParam.equalsIgnoreCase("sendEmail")){
			sendEmail(request,response);
		}else if(eventParam.equalsIgnoreCase("showVars")){
			showVariables(request,response);
		}else if(eventParam.equalsIgnoreCase("meterMsg")){
			sendMeterMessage(request,response);
		}else if(eventParam.equalsIgnoreCase("smsText")){
			sendTextMessage(request,response);
		}else if(eventParam.equalsIgnoreCase("enterPhones")){
			enterPhones(request,response);
		}else if(eventParam.equalsIgnoreCase("sessNull")){
			errorForDriver(request,response);
		}else if(eventParam.equalsIgnoreCase("allZonesForDash")){
			try {
				testPolygon(request,response);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(eventParam.equalsIgnoreCase("getDetails")){
			try {
				getDetails(request,response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//}
	}
	public void uploadVoiceFromOperator(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		//String action = request.getParameter("action");
		//String event = request.getParameter("event");
		//String driverID = request.getParameter("driverID");
		//AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");

		int size = request.getContentLength();
		//System.out.println("Length of content from operator:"+ size);
		//byte[] bytes = new byte[size];
		byte[] bytestemp = new byte[1];
		String driverId="";
		String[] driverIDsForMessage = null;
		ByteArrayOutputStream tempByteArray = new ByteArrayOutputStream();
		InputStream servletInput = request.getInputStream();
		//FileOutputStream os = new FileOutputStream("HareRama.wav");

		int readInt=0;

		while (-1 != (readInt =servletInput.read(bytestemp))) {
			tempByteArray.write(bytestemp);
			//os.write(bytestemp);
		}
		if(request.getParameter("driverid")!=null){
			driverId=request.getParameter("driverid");
			driverIDsForMessage = driverId.split(";");
		} 
		int messageKey = VoiceMessageDAO.insertVoiceMessage("103", driverId, tempByteArray.toByteArray(),2);
		//Inform all drivers of the new message

		long msgID = System.currentTimeMillis();
		String[] message = MessageGenerateJSON.generateVoicePushMessage(messageKey+"", msgID);
		ArrayList<DriverCabQueueBean> driverDetails = UtilityDAO.getDriverListForMessaging("103", driverIDsForMessage, 0, true);
		Messaging sendMessage = new Messaging(getServletContext(), driverDetails, message, poolBO, "103");
		sendMessage.run();

		/*int messageKey = VoiceMessageDAO.insertVoiceMessage(adminBO.getAssociateCode(), driverId, tempByteArray.toByteArray(),2);
		//Inform all drivers of the new message

		long msgID = System.currentTimeMillis();
		String[] message = MessageGenerateJSON.generateVoicePushMessage(messageKey+"", msgID);
		ArrayList<DriverCabQueueBean> driverDetails = UtilityDAO.getDriverListForMessaging(adminBO.getAssociateCode(), driverIDsForMessage, 0, true);
		Messaging sendMessage = new Messaging(getServletContext(), driverDetails, message, poolBO, adminBO.getAssociateCode());
		sendMessage.run();
		 */

	}

	public void uploadVoiceFromDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		int size = request.getContentLength();
		//System.out.println("Request size =" +size);
		byte[] bytes = new byte[size];
		byte[] bytestemp = new byte[8172];
		ByteArrayOutputStream tempByteArray = new ByteArrayOutputStream();
		BufferedReader reader1 = request.getReader();
		int readInt=0;
		int totalBytes = 0;
		//System.out.println("B-"+System.currentTimeMillis());
		int a = 0, counter = 0;
		while((a = reader1.read()) != -1){
			bytes[counter] = (byte)a;
			counter++;
		}
		//System.out.println("E-"+System.currentTimeMillis());

		//System.out.println( "ttadmin01Total file size =" +bytes.length);
		int messageKey = VoiceMessageDAO.insertVoiceMessage("103", "ttadmin01", bytes,1);
		//Inform all drivers of the new message

		//long msgID = System.currentTimeMillis();
		//String message = MessageGenerateJSON.generateVoicePushMessage(messageKey+"", msgID);
		//ArrayList<DriverCabQueueBean> driverDetails = UtilityDAO.getDriverListForMessaging(adminBO.getAssociateCode(), null, 0, true);
		//Messaging sendMessage = new Messaging(getServletContext(), driverDetails, message, poolBO);
		//sendMessage.run();


	}

	protected void getMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		//String event = request.getParameter("event");

		String messageKey = request.getParameter("msgKey");


		//System.out.println("About to download voice message"+messageKey);

		byte[] vm = VoiceMessageDAO.getVoiceMessageOperator("103","10300", messageKey);

		if(vm != null){
			//System.out.println("VMSize"+ vm.length);
			ServletOutputStream out = response.getOutputStream();
			response.setContentType("audio/mpeg");
			response.addHeader("Content-Disposition", "attachment; filename="+ messageKey+".mp3");
			response.setContentLength((int) vm.length);
			//while ((int readBytes = vm.read()) != -1)
			//    stream.write(readBytes);	
			out.write(vm);
			//System.out.println("VMWrittenout");
		}
	}

	public void downloadVoice(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");
		String event = request.getParameter("event");

		String messageKey = request.getParameter("msgKey");

		//AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		ApplicationPoolBO poolBO = (ApplicationPoolBO)getServletContext().getAttribute("poolBO");
		String driverID = "10302";
		//System.out.println("About to download voice message"+messageKey+":"+driverID);

		byte[] vm = VoiceMessageDAO.getVoiceMessage("103",driverID, messageKey);

		if(vm != null){
			//System.out.println("VMSize"+ vm.length);
			ServletOutputStream out = response.getOutputStream();
			response.setContentType("audio/mpeg");
			response.addHeader("Content-Disposition", "attachment; filename="+ messageKey);
			response.setContentLength((int) vm.length);
			//while ((int readBytes = vm.read()) != -1)
			//    stream.write(readBytes);
			out.write(vm);
			//System.out.println("VMWrittenout");
		}

	}
	public void sample(){
		//Mp3LameFormatConversionProvider mp3convertor = new Mp3LameFormatConversionProvider();

		//mp3convertor.
	}

	public void getMessageIDToBePlayed(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//System.out.println("Inside post");
		//AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		ArrayList<String> messages = VoiceMessageDAO.getNewMessageIDs("103","ttadmin01", new String[0]);
		StringBuffer buffer = new StringBuffer();
		String[] messageIDs= null;

		if(messages.size()>0){
			messageIDs = new String[messages.size()/2];
			for(int i=0;i<messages.size()-1;i=i+2){
				buffer.append("MK="+(String)messages.get(i)+";DID="+(String)messages.get(i+1)+"^");
				messageIDs[i/2]=(String)messages.get(i);
			}
			VoiceMessageDAO.updateMessageAsRead("103","ttadmin01", messageIDs);
		} else{
			buffer.append("No Messages");
		}

		response.getWriter().write(buffer.toString());
	}
	public void showObjects(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//System.out.println("Inside post");
		//AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");

		ArrayList<String> messages = VoiceMessageDAO.getNewMessageIDs("103","ttadmin01", new String[0]);
		StringBuffer buffer = new StringBuffer();
		String[] messageIDs= null;
		Enumeration e = getServletConfig().getServletContext().getAttributeNames();
		while (e.hasMoreElements()) {
			String name = (String)e.nextElement();
			String value = getServletConfig().getServletContext().getAttribute(name).toString();
			//System.out.println("name is: " + name + " value is: " + value);
			buffer.append("name is: " + name + " value is: " + value+"<p>");

		}

		response.getWriter().write(buffer.toString());
	}

	public void makeIVRPhoneCall(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ManagerConnectionFactory factory = new ManagerConnectionFactory("192.168.3.106", "admin", "amp111");
		String tripId=request.getParameter("tripID");
		String phoneNo=request.getParameter("phone");
		IVRDriverCaller ivr = new IVRDriverCaller("105000", tripId, phoneNo, "", "", "", "00", "105", "3","",this);
		ivr.run();
	}
	public void gcm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ManagerConnectionFactory factory = new ManagerConnectionFactory("192.168.3.106", "admin", "amp111");
		FCMPush g = new FCMPush();
		String key=request.getParameter("key");
		String key2=request.getParameter("key2");

		ArrayList<String> yow = new ArrayList<String>();
		yow.add(key);
		if(key2!=null){
			yow.add(key2);
		}
		FCMPush.sendMessage(getServletContext(), "KH", "ResVer=1.1;Command=TXLO;MSGID=1;MSG=Hello", yow);

	}
	public void ivrPassenger(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ManagerConnectionFactory factory = new ManagerConnectionFactory("192.168.3.106", "admin", "amp111");
		String assocode=request.getParameter("assoccode");
		String phoneNo=request.getParameter("phone");
		String msgId=request.getParameter("msgId");
		IVRPassengerCallerOutBound ivr = new IVRPassengerCallerOutBound(msgId,"",phoneNo,assocode);
		ivr.start();
	}
	public void sendEmail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ManagerConnectionFactory factory = new ManagerConnectionFactory("192.168.3.106", "admin", "amp111");

		Email otherProviderSMS = new Email("smtp.emailsrvr.com","metrotaxi@1888getacab.com","mt2587","metrotaxi@1888getacab.com","465","smtp");
		try{
			//String[] emails = new String[""];
			otherProviderSMS.sendMail("ram@888getacab.com","Hello", "Hello World");
		} catch (Exception e ){
			System.out.println(e.toString());
		}

	}
	public void execLocalCommand(){
		try{
			Runtime.getRuntime().exec("/usr/local/temp/conver.sh");
		} catch (Exception e){
			System.out.println("Exception"+e.toString());
		}

	}
	public void test(){
		AsteriskCall newCall = new AsteriskCall();

	}
	public void showVariables(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuffer output = new StringBuffer();
		Enumeration attributeNames = getServletContext().getAttributeNames();
		while(attributeNames.hasMoreElements()) {
			String varName = (String) attributeNames.nextElement();
			output.append(varName+":");
			output.append(getServletContext().getAttribute(varName)+"---------<br>");
		}
		response.getWriter().write(output.toString());

	}	
	public void sendMeterMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String driver=request.getParameter("driver");
		String msg=request.getParameter("msg");
		String googKey=request.getParameter("goog");

		DriverCabQueueBean cabQueueBean = DispatchDAO.getDriverByDriverID("143", driver, 1);
		//System.out.println("googKey-->"+cabQueueBean.getPushKey());
		ApplicationPoolBO poolBO = (ApplicationPoolBO) getServletContext().getAttribute("poolBO");
		AdminRegistrationBO adminBO = (AdminRegistrationBO) request.getSession().getAttribute("user");


		String[] message = MessageGenerateJSON.generateMeterMessage(msg, "MC", System.currentTimeMillis());
		Messaging oneSMS = new Messaging(getServletContext(),cabQueueBean, message,poolBO,"143");
		oneSMS.start();
		ZoneDAO.insertForAllTest();
		//		PaymentGatewaySlimCD.makeProcess();
	}
	public void sendTextMessage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//		HandleMemoryMessage.removeMessage(getServletContext(), "156", "156100");
		//System.out.println(TDSProperties.getValue("IVRExtension"));
	}
	public void enterPhones(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//		String newVenki=request.getParameter("helloWorld");
		//		System.out.println(newVenki.replaceAll("(?i)venki", "True "));
		//		AddressDAO.enterAllPhones();
		String urlString = request.isSecure()?"https://www.":"http://www."+request.getServerName()+"/TDS/TestServlet?event=smsText";
		//System.out.println(urlString);
		response.getWriter().write(urlString);
	}
	//	public void jsonTest(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException {
	//		JSONArray outerArray = (JSONArray) JSONSerializer.toJSON(OpenRequestUtil.jsonTest1(request, response));
	//		JSONObject json = (JSONObject) outerArray.get(0);
	//		JSONArray jarray = json.getJSONArray("hotels");
	//		for (int i = 0; i < jarray.size(); i++)
	//		{
	//		  JSONObject hotel = jarray.getJSONObject(i);
	//		  String name = hotel.getString("name");
	//		  System.out.println(name);
	//		}
	//	}
	private void getDetails(HttpServletRequest request,HttpServletResponse response)throws ServletException, IOException,Exception  {
		AdminRegistrationBO adminBO = (AdminRegistrationBO)request.getSession().getAttribute("user");
		String fromDate=TDSValidation.getTimeStampDateFormat(request.getParameter("fromDate"));
		String toDate=TDSValidation.getTimeStampDateFormat(request.getParameter("toDate"));
		StringBuffer outData = new StringBuffer("");
		ArrayList<OpenRequestBO>openReqHistory=ReportDAO.getHistoryForReport(adminBO,fromDate,toDate);
		if(openReqHistory!=null && openReqHistory.size()>0){
			outData.append("COMPANY NAME,DATE,PERMIT#,DISPATCH/FLAG DROP,TIME OF TRIP,ORIGINATION,DESTINATION\n");
			for(int i=0;i<openReqHistory.size();i++){
				outData.append("CALIFORNIA CAB,"+openReqHistory.get(i).getSdate()+","+(openReqHistory.get(i).getName()==null?"0000":openReqHistory.get(i).getName())+","+openReqHistory.get(i).getDispatchMethod()+","+openReqHistory.get(i).getShrs()+","+(openReqHistory.get(i).getQueueno().equals("")||openReqHistory.get(i).getQueueno().equals("000")?"3":openReqHistory.get(i).getQueueno())+","+(openReqHistory.get(i).getEndQueueno().equals("")||openReqHistory.get(i).getEndQueueno().equals("000")?"3":openReqHistory.get(i).getEndQueueno())+"\n");
			}
		}
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition","attachment;filename=\"Monthly off Airport Trips California cab.csv\"");
		InputStream in = new ByteArrayInputStream(outData.toString().getBytes("UTF-8"));
		ServletOutputStream out = response.getOutputStream();

		byte[] outputByte = new byte[4096];
		//copy binary contect to output stream
		while(in.read(outputByte, 0, 4096) != -1)
		{
			out.write(outputByte, 0, 4096);
		}
		in.close();
		out.flush();
		out.close();
		//		response.setContentType("text/csv");
		//		response.setHeader( "content-disposition:","filename=\"JobHistory.csv\"" );
		//		response.getWriter().write(outData.toString());
	}

	public void errorForDriver(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String dId = request.getParameter("driverID123")!=null?request.getParameter("driverID123"):"";
		String cab = request.getParameter("cabNo123")!=null?request.getParameter("cabNo123"):"";
		//System.out.println("Context & Session Are Null For Driver--->"+dId+" With Cab--->"+cab);
		response.getWriter().write("");
	}
	public void testPolygon(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JSONException {
		ArrayList<ZoneTableBeanSP> allZones = (ArrayList<ZoneTableBeanSP>) getServletContext().getAttribute((request.getParameter("assoccode")==null?"105":request.getParameter("assoccode") +"Zones"));
		ArrayList<Double> maxMinPoints = ZoneDAO.readMaxMinPoints(request.getParameter("assoccode")==null?"105":request.getParameter("assoccode"));
		int startValue = 0;
		double startTime = System.currentTimeMillis();
		if(maxMinPoints!=null && maxMinPoints.size()>3){
			System.out.println(maxMinPoints.get(3)+"  "+maxMinPoints.get(2));
			System.out.println(maxMinPoints.get(1)+"  "+maxMinPoints.get(0));
			for(double lat= maxMinPoints.get(3);lat < maxMinPoints.get(2);lat=(lat+0.0005)){
				for(double lon= maxMinPoints.get(1);lon < maxMinPoints.get(0);lon= (lon+0.0005)){
					startValue++;
					for(int i = 0; i<allZones.size(); i++){
						if(lon < allZones.get(i).getEastLongitude() && lon > allZones.get(i).getWestLongitude() && lat < allZones.get(i).getNorthLatitude() && lat > allZones.get(i).getSouthLatitude()){
							if(allZones.get(i).getZoneCoord().contains(new Point(lat, lon))){
								break;
							}
						}
					}
				}
			}
		}
		System.out.println("Total Processing Time--->"+(System.currentTimeMillis()-startTime)+" for total--->"+startValue);
		response.getWriter().write("Processed "+startValue+" Points In "+((System.currentTimeMillis()-startTime)/1000)+" Secs");
	}
}