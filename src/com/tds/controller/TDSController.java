package com.tds.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Category;

import com.tds.dao.RequestDAO;
import com.tds.dao.UtilityDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;
/*
 * 
 *  This class will control the over all application
 * 
 */
public class TDSController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public static Category cat;
	public static Map _queueIdMap;


	/*
	 * Access Get method from JSP page or Browser
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */


	@Override
	public void init(ServletConfig config) throws ServletException{
		//ArrayList m_AssocList = ServiceRequestDAO.getAssoccodeForQueue();



		cat = Category.getInstance(TDSController.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("Class Name "+ TDSController.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		/* _queueIdMap = new HashMap();
		String m_queueId = "";
		int m_status = 1; //Thread Request
		int counter;
		DoQueueProcess m_queueIdProcess;
		List m_publicQueueList = ServiceRequestDAO.getPublicTripId(m_status);

		System.out.println("Data in Public Request Queue "+m_publicQueueList);
		for(counter = 0; counter < m_publicQueueList.size(); counter ++) {
			m_queueId = m_publicQueueList.get(counter).toString();
			if(!_queueIdMap.containsKey(m_queueId)) {
				//cat.info("Creating the Thread for each QueueId "+m_queueId);
				System.out.println("Creating the Thread for each QueueId "+m_queueId);
				m_queueIdProcess = new DoQueueProcess(m_queueId);
				m_queueIdProcess.setName(m_queueId);
				System.out.println("Before Start");
				m_queueIdProcess.start();
				_queueIdMap.put(m_queueId, m_queueIdProcess);
				System.out.println("After Start");
			}
		}



		System.out.println("Thread in the Queue "+ _queueIdMap);*/
		super.init(config);
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		cat.info("In Get Method  ");
		doProcess(request,response);
	}

	/*
	 * Access Post method from JSP page or Browser
	 * (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Post Method");
		doProcess(request, response);
	}

	/*
	 * Access request from both getter and setter methods
	 */

	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		cat.info("In Process Method");
		
		request.setCharacterEncoding("UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String action = "";
		String returnURL = "";


		if(request.getParameter(TDSConstants.actionParam) != null) {
			action =  request.getParameter(TDSConstants.actionParam);
		}
		cat.info("In Action Name "+action);
		//System.out.println("In Action Name "+action);
		HttpSession m_session = request.getSession(false);
		if(!action.equals("")) {

			String event =  request.getParameter(TDSConstants.eventParam) == null?"" : request.getParameter(TDSConstants.eventParam);
			//System.out.println("In Event Name "+event);
			if(m_session!= null && m_session.getAttribute("user")!=null)
			{
				AdminRegistrationBO adminBO = (AdminRegistrationBO)m_session.getAttribute("user");
				if (action.equalsIgnoreCase(TDSConstants.getMCCProcess)) {
					returnURL = TDSConstants.mCCProcessURL;
				}
				if(UtilityDAO.isHavingRights(action, event, (adminBO.getUid())))
				{
					//javawebparts.session.SessionSize tempSize = new javawebparts.session.SessionSize(request.getSession());
					//System.out.println("SessionSize="+ tempSize.getSessionSize());
					//System.out.println("SessionSizeReason="+ tempSize.whyFailed());

					if(getServletConfig().getServletContext().getAttribute((adminBO.getUid()+"LO"))!=null || getServletConfig().getServletContext().getAttribute((adminBO.getUname()+"LO"))!=null) {
						getServletConfig().getServletContext().removeAttribute(adminBO.getUid()+"LO");
						//getServletConfig().getServletContext().removeAttribute(adminBO.getUname()+"LO");
						request.removeAttribute("action");
						request.removeAttribute("event");
						request.setAttribute("action", "registration"); 
						request.setAttribute("event", "logout");
						returnURL = TDSConstants.registrationURL;
						action = "registration";
						request.getSession().invalidate();
					}

					if (action.equalsIgnoreCase(TDSConstants.getMCCProcess)) {
						returnURL = TDSConstants.mCCProcessURL;
					} else if (action.equalsIgnoreCase(TDSConstants.registrationAction)) {
						returnURL = TDSConstants.registrationURL;
					} else if (action.equalsIgnoreCase(TDSConstants.requestAction)) {
						returnURL = TDSConstants.requestURL;
					} else if (action.equalsIgnoreCase(TDSConstants.serviceAction)) {
						returnURL = TDSConstants.serviceURL;
					} else if (action.equalsIgnoreCase(TDSConstants.userRoleAction)) {
						returnURL = TDSConstants.userRoleURL;
					} else if (action.equalsIgnoreCase(TDSConstants.getpaymentProcess)) {
						returnURL = TDSConstants.paymentProcessURL;
					}else if (action.equalsIgnoreCase(TDSConstants.getPing)) {
						returnURL = TDSConstants.pingURL;
					}else if (action.equalsIgnoreCase(TDSConstants.getadmin)) {
						returnURL = TDSConstants.adminURL;
					}else if (action.equalsIgnoreCase(TDSConstants.getSystemSetup)) {
						returnURL = TDSConstants.systemSetupURL;
					} else if (action.equalsIgnoreCase(TDSConstants.getFinance)) {
						returnURL = TDSConstants.financeURL;
					} else if (action.equalsIgnoreCase(TDSConstants.customerService)) {
						returnURL = TDSConstants.customerServiceURL;
					} else if(action.equalsIgnoreCase(TDSConstants.getSecurity)) {
						returnURL = TDSConstants.securityURL;
					} else if(action.equalsIgnoreCase(TDSConstants.getCSV)){
						returnURL = TDSConstants.csvURL;
					} else if(action.equalsIgnoreCase(TDSConstants.getCSV)){
						returnURL = TDSConstants.csvURL;
					}else if(action.equalsIgnoreCase("YellowCabMobileAction")){
						returnURL = "/YellowCabMobileAction";
					}else if(action.equalsIgnoreCase("phoneIVR")){
						returnURL = "/PhoneIVR";
					}else if(action.equalsIgnoreCase("chargesAction")){
						returnURL = "/ChargesAction";
					}else if(action.equalsIgnoreCase(TDSConstants.getDocumentUpload)){
						request.setAttribute("compId", adminBO.getAssociateCode()); 
						request.setAttribute("userId", adminBO.getUid());
						returnURL = TDSConstants.documentUploadUrl;
					} else if(action.equalsIgnoreCase(TDSConstants.getDocApproval)){
						request.setAttribute("userId", adminBO.getUid());
						returnURL = TDSConstants.documentApproveUrl;
					}else if(action.equalsIgnoreCase("TDS_DOCUMENT_SEARCH")){
						returnURL = TDSConstants.documentSearchUrl;
					}else if (action.equalsIgnoreCase(TDSConstants.registrationAction)) {
						returnURL = TDSConstants.registrationURL;
					}
				} else {
					if (action.equalsIgnoreCase(TDSConstants.getMobileRequest)) {
						returnURL = TDSConstants.mobileRequestURL;
					} else if (action.equalsIgnoreCase(TDSConstants.getMobilePayment)) {
						returnURL = TDSConstants.mobilePaymentURL;
					} else if (action.equalsIgnoreCase(TDSConstants.registrationAction) && event.equalsIgnoreCase("logout") ) {
						returnURL = TDSConstants.registrationURL;
					}  else if(action.equalsIgnoreCase(TDSConstants.getRedirect))
					{
						returnURL = TDSConstants.reDirectURL;
					}else if(action.equalsIgnoreCase("chargesAction")){
						returnURL = "/ChargesAction";
					}		        	
					else if(action.equalsIgnoreCase(TDSConstants.fileUpload))
					{
						returnURL = TDSConstants.FileURL;
					} else {
						request.setAttribute("page", "You do not have sufficient rights to access");
						request.setAttribute("screen", "/failure.jsp");
						//System.out.println("Session "+m_session.getAttribute("user"));
						returnURL = TDSConstants.getMainNewJSP;
					}
				}
			} else {

				if (action.equalsIgnoreCase(TDSConstants.getMobileRequest)) {
					returnURL = TDSConstants.mobileRequestURL;
				} else if (action.equalsIgnoreCase(TDSConstants.registrationAction)) {
					returnURL = TDSConstants.registrationURL;
				} else if (action.equalsIgnoreCase(TDSConstants.requestAction)) {
					returnURL = TDSConstants.requestURL;
				} 
//				else if (action.equalsIgnoreCase(TDSConstants.getMobilePayment) && (event.equalsIgnoreCase("vvalid")|| event.equalsIgnoreCase("vup"))) {
//					Cookie[] delCookies = request.getCookies();
//					String sessionId = "";
//					for (int i = 0; i < delCookies.length; i++) {
//						if (delCookies[i].getName().equals("JSESSIONID")) {
//							sessionId = delCookies[i].getValue();
//							break;
//						}
//					}
//					if (!sessionId.equals("")) {
//						int sessionValid = MobileDAO.checkSession(sessionId);
//						if (sessionValid == 1) {
//							AdminRegistrationBO m_adAdminRegistrationBO = MobileDAO.changeSession(sessionId, sessionId);
//							HttpSession session = request.getSession();
//							session.setMaxInactiveInterval(10);
//							session.setAttribute("user", m_adAdminRegistrationBO);
//							session.setAttribute("tempSession", "True");
//						}
//					}
//					returnURL=TDSConstants.getMobilePayment;
//				} 
				else {
					//request.setAttribute("mapDetail", RequestDAO.getDriverLocation());
					//System.out.println("Session "+m_session.getAttribute("user"));
					request.setAttribute("screen", TDSConstants.getLoginJSP);
					returnURL = TDSConstants.getMainNewJSP;
				}
			}
		} else {
			request.setAttribute("mapDetail", RequestDAO.getDriverLocation());
			if(m_session!= null && m_session.getAttribute("user")!=null){
				returnURL = TDSConstants.getMainNewJSP;
			}else {
				request.setAttribute("screen", TDSConstants.getLoginJSP);
				returnURL = TDSConstants.getMainNewJSP;
//			} else {
//				returnURL = TDSConstants.getMainWithoutSessionJSP;
			}
		}

		//System.out.println("Forward the request into action class by using the following URL "+returnURL);
		cat.info("Forward the request into action class by using the following URL "+returnURL);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher(returnURL);
		requestDispatcher.forward(request, response);
	}

}
