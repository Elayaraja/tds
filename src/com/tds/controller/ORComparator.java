package com.tds.controller;

import java.io.IOException;
import java.util.Iterator;

import org.apache.log4j.Category;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tds.controller.TDSController;
import com.tds.dao.AuditDAO;
import com.tds.dao.DispatchDAO;
import com.tds.tdsBO.AdminRegistrationBO;
import com.common.util.TDSConstants;

public class ORComparator extends Thread {
	private static Category cat =TDSController.cat;

	static {
		cat = Category.getInstance(ORComparator.class.getName());

		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		cat.info("The given Class is "+ORComparator.class.getName());
		cat.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	JSONArray array = null;
	JSONArray array1 = null;
	JSONObject primaryValues;
	JSONObject secondaryValues;
	String tripId="";
	AdminRegistrationBO adminBO;
	int advanceTime =900;
	public ORComparator(String oldJSON,String newJSON,String trip,AdminRegistrationBO adminBo,int advTime) throws IOException {
		
		try {
			array = new JSONArray(oldJSON);
			primaryValues = array.getJSONObject(0);
			array1 = new JSONArray(newJSON);
			secondaryValues = array1.getJSONObject(0);
			tripId = trip;
			adminBO = adminBo;
			advanceTime = advTime;
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@Override
	public void run(){
		Iterator<String> iter = secondaryValues.keys();
		Iterator<String> iterOld = primaryValues.keys();
		StringBuffer buffer=new StringBuffer();
		boolean updated = false;
	    while (iter.hasNext()) {
	        String key = iter.next();
	        while(iterOld.hasNext()){
	        	String oldKey = iterOld.next();
	        	if(key.equalsIgnoreCase(oldKey)){
	        		try {
						Object value = secondaryValues.get(key);
						Object valueOld = primaryValues.get(oldKey);
						if(value.equals(valueOld)){
							break;
						} else {
							if((key.equals("Date")||key.equals("Time")) && !updated){
								DispatchDAO.updateStartTime(tripId,adminBO.getMasterAssociateCode(),advanceTime);
								updated = true;
							}
							buffer.append(key+":"+valueOld+"-"+value+";");
							break;
							//Enter Into DB
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        	}
	        }
	    }
	    AuditDAO.insertJobLogs("Updated Values: "+buffer, tripId, adminBO.getAssociateCode(), TDSConstants.updateJob, adminBO.getUid(), "", "", adminBO.getMasterAssociateCode());
	}
}
