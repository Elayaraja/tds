package com.tds.controller;

public class FileDocForm {
	private String fileName;
	private String path;
	private String fileType;
	

	
	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileName() {
		return fileName;
	}
	
	public FileDocForm(String fileName,String path,String fileType){
		this.fileName=fileName;
		this.path=path;
		this.fileType=fileType;
		
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	
}
