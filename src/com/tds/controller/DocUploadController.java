package com.tds.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.common.util.TDSConstants;
import com.common.util.TDSProperties;
/*
 * 
 *  This class will control the over all application
 * 
 */
public class DocUploadController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static List<FileDocForm> fileList = new ArrayList<FileDocForm>();
	
	String path = TDSProperties.getValue("FolderStore");
	
	private static final String TMP_DIR_PATH = "/tempUpload/";
	private File tmpDir;
	private static final String DESTINATION_DIR_PATH ="/";
	private File destinationDir;
 
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		tmpDir = new File(TMP_DIR_PATH);
		if(!tmpDir.isDirectory()) {
			throw new ServletException(TMP_DIR_PATH + " is not a directory");
		}
		String realPath = getServletContext().getRealPath(DESTINATION_DIR_PATH);
		destinationDir = new File(realPath);
		if(!destinationDir.isDirectory()) {
			throw new ServletException(DESTINATION_DIR_PATH+" is not a directory");
		}
 
	}
 

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doProcess(request,response);
	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doProcess(request, response);
	}
	
	public void doProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException {
		String action = "";
		String returnURL = "";
		String dirPath="";
		fileList = new ArrayList<FileDocForm>();
 dirPath = getServletContext().getRealPath(DESTINATION_DIR_PATH);
			new File (destinationDir,"TDS").mkdir();
		
	if(request.getParameter(TDSConstants.actionParam) != null) {
		action =  request.getParameter(TDSConstants.actionParam);
	}
	if(request.getParameter("documentType") != null) {
		String saction =  request.getParameter("documentType");
		
		//System.out.println("docuemnt tye[ is "+saction);
	}	
	if(action.equalsIgnoreCase("upload")){
		
		
	    //PrintWriter out = response.getWriter();
	    response.setContentType("text/plain");
	    //out.println("<h1>Servlet File Upload </h1>");
	    //out.println();
 
		DiskFileItemFactory  fileItemFactory = new DiskFileItemFactory ();
		/*
		 *Set the size threshold, above which content will be stored on disk.
		 */
		fileItemFactory.setSizeThreshold(1*1024*1024); //1 MB
		/*
		 * Set the temporary directory to store the uploaded files of size above threshold.
		 */
		fileItemFactory.setRepository(tmpDir);
		String destinDir = "";
		ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
		try {
			/*
			 * Parse the request
			 */
			List items = uploadHandler.parseRequest(request);
			Iterator itr = items.iterator();
			while(itr.hasNext()) {
				FileItem item = (FileItem) itr.next();
				/*
				 * Handle Form Fields.
				 */
				if(item.isFormField()) {
					//out.println("File Name = "+item.getFieldName()+", Value = "+item.getString());
				   if(item.getFieldName().contains("companyCode")){
					   destinDir=item.getString();
				   }
				} else {
					//Handle Uploaded files.
				/*	out.println("Field Name = "+item.getFieldName()+
						", File Name = "+item.getName()+
						", Content type = "+item.getContentType()+
						", File Size = "+item.getSize());
					/*
					 * Write file to the ultimate location.
					 */
					
					
						String realPath2 = getServletContext().getRealPath(DESTINATION_DIR_PATH+"/TDS");
					File realPaFile = new File(realPath2);
					new File( realPaFile,destinDir).mkdir();
					String realPath1 = getServletContext().getRealPath(DESTINATION_DIR_PATH+"/TDS/"+destinDir);
					File f = new File (realPath1);
					//out.println("Destination :"+f.getAbsolutePath());
					File file = new File(f,item.getName());
					item.write(file);
				}
				
				
			}
		//	out.close();
		}catch(FileUploadException ex) {
			log("Error encountered while parsing the request",ex);
		} catch(Exception ex) {
			log("Error encountered while uploading file",ex);
		}
		returnURL="/upload/docUploadFile.jsp";
		
	}else{
		returnURL="/upload/docUploadFile.jsp";
	}
	
	readDir(dirPath+"/TDS");
 	request.setAttribute("DirectoryData", fileList);
	RequestDispatcher requestDispatcher = request.getRequestDispatcher(returnURL);
	requestDispatcher.forward(request, response);
	
	}
	public void readDir(String dirName){
		//System.out.println("HOme >>"+dirName);
		
		File dir = new File(dirName);
		//System.out.println("Starting");
		File dirArry[] = dir.listFiles();
		for(File f:dirArry){
			String[] s = dirName.split("/TDS/");
			String path =f.getAbsolutePath();
			if(s.length>1)
			{
			//System.out.println("Path >>>"+path);
			path=s[1];	
			}
			FileDocForm fileDoce = new FileDocForm(f.getName(),path,getFileType(f.getName()));
			fileList.add(fileDoce);

			//System.out.println(f.getName());
			if(f.isDirectory() && !f.getName().startsWith(".")){
				readDir(dirName+"/"+f.getName());
			}
		}	
		}
	
	public String getFileType(String fileName){

		  int mid= fileName.lastIndexOf(".");
		  String ext=fileName.substring(mid+1,fileName.length());
		if(ext.equalsIgnoreCase("jpg") || ext.equalsIgnoreCase("gif") || ext.equalsIgnoreCase("jpeg") || ext.equalsIgnoreCase("bmp") )
			return "IMG";
		else if(ext.equalsIgnoreCase("pdf") || ext.equalsIgnoreCase("doc"))
			return "PDF";
		return "Others";
	}

	
	}
