package com.tds.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tds.tdsBO.MeterStatusBO;
import com.tds.util.MeterControlUtil;

@Controller
public class MeterController {
	@Autowired
	ServletConfig serveletConfig;
	@RequestMapping(value="/feed")
	public void submitMeterStatus(@ModelAttribute("meterBean") MeterStatusBO meterBean,HttpServletRequest request,HttpServletResponse response) throws UnsupportedEncodingException{
		JSONArray respArray = new JSONArray();
		JSONObject errorCode = new JSONObject();
		request.setAttribute("mainMeter", meterBean);
		//System.out.println("From Name Value--->"+meterBean.getCompanyCode()+" "+meterBean.getDriverId());
		meterBean=MeterControlUtil.enterMeterValues(request,response,meterBean, this.serveletConfig);
		try {
			errorCode.put("Command", meterBean.getRespCode());
			errorCode.put("StatusCode",meterBean.getStatusCode());
			errorCode.put("Message", meterBean.getMessage());
			respArray.put(errorCode);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			response.getWriter().write(respArray.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
